﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2527952324.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// DG.Tweening.Tween
struct Tween_t278478013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIMode
struct  AIMode_t3831275989  : public SingletonMonoBehaviour_1_t2527952324
{
public:
	// UnityEngine.GameObject AIMode::m_Planet
	GameObject_t1756533147 * ___m_Planet_3;
	// UnityEngine.GameObject AIMode::m_gCore
	GameObject_t1756533147 * ___m_gCore_4;
	// UnityEngine.GameObject AIMode::AIButton
	GameObject_t1756533147 * ___AIButton_5;
	// System.Boolean AIMode::AIFlagMode
	bool ___AIFlagMode_6;
	// System.Boolean AIMode::OnAIMode
	bool ___OnAIMode_7;
	// System.Boolean AIMode::AIFlagDrop
	bool ___AIFlagDrop_8;
	// UnityEngine.GameObject AIMode::GoalFlag
	GameObject_t1756533147 * ___GoalFlag_9;
	// System.Int32 AIMode::FlagDirectControl
	int32_t ___FlagDirectControl_10;
	// UnityEngine.RectTransform AIMode::RollControl
	RectTransform_t3349966182 * ___RollControl_11;
	// UnityEngine.RectTransform AIMode::FingerRectTran
	RectTransform_t3349966182 * ___FingerRectTran_12;
	// UnityEngine.RectTransform AIMode::CircleRectTran
	RectTransform_t3349966182 * ___CircleRectTran_13;
	// DG.Tweening.Tween AIMode::fingerTw
	Tween_t278478013 * ___fingerTw_14;
	// DG.Tweening.Tween AIMode::circleTw
	Tween_t278478013 * ___circleTw_15;

public:
	inline static int32_t get_offset_of_m_Planet_3() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___m_Planet_3)); }
	inline GameObject_t1756533147 * get_m_Planet_3() const { return ___m_Planet_3; }
	inline GameObject_t1756533147 ** get_address_of_m_Planet_3() { return &___m_Planet_3; }
	inline void set_m_Planet_3(GameObject_t1756533147 * value)
	{
		___m_Planet_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Planet_3, value);
	}

	inline static int32_t get_offset_of_m_gCore_4() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___m_gCore_4)); }
	inline GameObject_t1756533147 * get_m_gCore_4() const { return ___m_gCore_4; }
	inline GameObject_t1756533147 ** get_address_of_m_gCore_4() { return &___m_gCore_4; }
	inline void set_m_gCore_4(GameObject_t1756533147 * value)
	{
		___m_gCore_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_gCore_4, value);
	}

	inline static int32_t get_offset_of_AIButton_5() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___AIButton_5)); }
	inline GameObject_t1756533147 * get_AIButton_5() const { return ___AIButton_5; }
	inline GameObject_t1756533147 ** get_address_of_AIButton_5() { return &___AIButton_5; }
	inline void set_AIButton_5(GameObject_t1756533147 * value)
	{
		___AIButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___AIButton_5, value);
	}

	inline static int32_t get_offset_of_AIFlagMode_6() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___AIFlagMode_6)); }
	inline bool get_AIFlagMode_6() const { return ___AIFlagMode_6; }
	inline bool* get_address_of_AIFlagMode_6() { return &___AIFlagMode_6; }
	inline void set_AIFlagMode_6(bool value)
	{
		___AIFlagMode_6 = value;
	}

	inline static int32_t get_offset_of_OnAIMode_7() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___OnAIMode_7)); }
	inline bool get_OnAIMode_7() const { return ___OnAIMode_7; }
	inline bool* get_address_of_OnAIMode_7() { return &___OnAIMode_7; }
	inline void set_OnAIMode_7(bool value)
	{
		___OnAIMode_7 = value;
	}

	inline static int32_t get_offset_of_AIFlagDrop_8() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___AIFlagDrop_8)); }
	inline bool get_AIFlagDrop_8() const { return ___AIFlagDrop_8; }
	inline bool* get_address_of_AIFlagDrop_8() { return &___AIFlagDrop_8; }
	inline void set_AIFlagDrop_8(bool value)
	{
		___AIFlagDrop_8 = value;
	}

	inline static int32_t get_offset_of_GoalFlag_9() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___GoalFlag_9)); }
	inline GameObject_t1756533147 * get_GoalFlag_9() const { return ___GoalFlag_9; }
	inline GameObject_t1756533147 ** get_address_of_GoalFlag_9() { return &___GoalFlag_9; }
	inline void set_GoalFlag_9(GameObject_t1756533147 * value)
	{
		___GoalFlag_9 = value;
		Il2CppCodeGenWriteBarrier(&___GoalFlag_9, value);
	}

	inline static int32_t get_offset_of_FlagDirectControl_10() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___FlagDirectControl_10)); }
	inline int32_t get_FlagDirectControl_10() const { return ___FlagDirectControl_10; }
	inline int32_t* get_address_of_FlagDirectControl_10() { return &___FlagDirectControl_10; }
	inline void set_FlagDirectControl_10(int32_t value)
	{
		___FlagDirectControl_10 = value;
	}

	inline static int32_t get_offset_of_RollControl_11() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___RollControl_11)); }
	inline RectTransform_t3349966182 * get_RollControl_11() const { return ___RollControl_11; }
	inline RectTransform_t3349966182 ** get_address_of_RollControl_11() { return &___RollControl_11; }
	inline void set_RollControl_11(RectTransform_t3349966182 * value)
	{
		___RollControl_11 = value;
		Il2CppCodeGenWriteBarrier(&___RollControl_11, value);
	}

	inline static int32_t get_offset_of_FingerRectTran_12() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___FingerRectTran_12)); }
	inline RectTransform_t3349966182 * get_FingerRectTran_12() const { return ___FingerRectTran_12; }
	inline RectTransform_t3349966182 ** get_address_of_FingerRectTran_12() { return &___FingerRectTran_12; }
	inline void set_FingerRectTran_12(RectTransform_t3349966182 * value)
	{
		___FingerRectTran_12 = value;
		Il2CppCodeGenWriteBarrier(&___FingerRectTran_12, value);
	}

	inline static int32_t get_offset_of_CircleRectTran_13() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___CircleRectTran_13)); }
	inline RectTransform_t3349966182 * get_CircleRectTran_13() const { return ___CircleRectTran_13; }
	inline RectTransform_t3349966182 ** get_address_of_CircleRectTran_13() { return &___CircleRectTran_13; }
	inline void set_CircleRectTran_13(RectTransform_t3349966182 * value)
	{
		___CircleRectTran_13 = value;
		Il2CppCodeGenWriteBarrier(&___CircleRectTran_13, value);
	}

	inline static int32_t get_offset_of_fingerTw_14() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___fingerTw_14)); }
	inline Tween_t278478013 * get_fingerTw_14() const { return ___fingerTw_14; }
	inline Tween_t278478013 ** get_address_of_fingerTw_14() { return &___fingerTw_14; }
	inline void set_fingerTw_14(Tween_t278478013 * value)
	{
		___fingerTw_14 = value;
		Il2CppCodeGenWriteBarrier(&___fingerTw_14, value);
	}

	inline static int32_t get_offset_of_circleTw_15() { return static_cast<int32_t>(offsetof(AIMode_t3831275989, ___circleTw_15)); }
	inline Tween_t278478013 * get_circleTw_15() const { return ___circleTw_15; }
	inline Tween_t278478013 ** get_address_of_circleTw_15() { return &___circleTw_15; }
	inline void set_circleTw_15(Tween_t278478013 * value)
	{
		___circleTw_15 = value;
		Il2CppCodeGenWriteBarrier(&___circleTw_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
