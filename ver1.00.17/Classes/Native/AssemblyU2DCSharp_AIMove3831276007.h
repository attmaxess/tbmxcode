﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.Dictionary`2<Module,System.Int32>
struct Dictionary_2_t126436439;
// UnityEngine.Animation
struct Animation_t2068071072;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIMove
struct  AIMove_t3831276007  : public MonoBehaviour_t1158329972
{
public:
	// System.Single AIMove::DesertMoveSpeed
	float ___DesertMoveSpeed_2;
	// System.Single AIMove::IceMoveSpeed
	float ___IceMoveSpeed_3;
	// System.Single AIMove::FlagMoveSpeed
	float ___FlagMoveSpeed_4;
	// System.Single AIMove::DefultMoveSpeed
	float ___DefultMoveSpeed_5;
	// System.Single AIMove::m_fMoveSpeedAI
	float ___m_fMoveSpeedAI_6;
	// System.Single AIMove::m_fAzimuth
	float ___m_fAzimuth_7;
	// System.Int32[] AIMove::m_sBtnStore
	Int32U5BU5D_t3030399641* ___m_sBtnStore_8;
	// System.Int32 AIMove::m_sBSCount
	int32_t ___m_sBSCount_9;
	// System.Boolean AIMove::m_bMoveCorutine
	bool ___m_bMoveCorutine_10;
	// UnityEngine.Rigidbody AIMove::m_fObjRigid
	Rigidbody_t4233889191 * ___m_fObjRigid_11;
	// UnityEngine.GameObject AIMove::m_NavObj
	GameObject_t1756533147 * ___m_NavObj_12;
	// UnityEngine.Vector3 AIMove::m_MoveDirect
	Vector3_t2243707580  ___m_MoveDirect_13;
	// UnityEngine.GameObject AIMove::m_CheckObj
	GameObject_t1756533147 * ___m_CheckObj_14;
	// UnityEngine.Transform AIMove::m_Mobilmo
	Transform_t3275118058 * ___m_Mobilmo_15;
	// System.Single AIMove::m_fMoveValueX
	float ___m_fMoveValueX_16;
	// System.Single AIMove::m_fMoveValueY
	float ___m_fMoveValueY_17;
	// System.Collections.Generic.Dictionary`2<Module,System.Int32> AIMove::m_RandomAnim
	Dictionary_2_t126436439 * ___m_RandomAnim_18;
	// System.Int32 AIMove::m_iCountAIMove
	int32_t ___m_iCountAIMove_19;
	// UnityEngine.Animation AIMove::m_Anim
	Animation_t2068071072 * ___m_Anim_20;

public:
	inline static int32_t get_offset_of_DesertMoveSpeed_2() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___DesertMoveSpeed_2)); }
	inline float get_DesertMoveSpeed_2() const { return ___DesertMoveSpeed_2; }
	inline float* get_address_of_DesertMoveSpeed_2() { return &___DesertMoveSpeed_2; }
	inline void set_DesertMoveSpeed_2(float value)
	{
		___DesertMoveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_IceMoveSpeed_3() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___IceMoveSpeed_3)); }
	inline float get_IceMoveSpeed_3() const { return ___IceMoveSpeed_3; }
	inline float* get_address_of_IceMoveSpeed_3() { return &___IceMoveSpeed_3; }
	inline void set_IceMoveSpeed_3(float value)
	{
		___IceMoveSpeed_3 = value;
	}

	inline static int32_t get_offset_of_FlagMoveSpeed_4() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___FlagMoveSpeed_4)); }
	inline float get_FlagMoveSpeed_4() const { return ___FlagMoveSpeed_4; }
	inline float* get_address_of_FlagMoveSpeed_4() { return &___FlagMoveSpeed_4; }
	inline void set_FlagMoveSpeed_4(float value)
	{
		___FlagMoveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_DefultMoveSpeed_5() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___DefultMoveSpeed_5)); }
	inline float get_DefultMoveSpeed_5() const { return ___DefultMoveSpeed_5; }
	inline float* get_address_of_DefultMoveSpeed_5() { return &___DefultMoveSpeed_5; }
	inline void set_DefultMoveSpeed_5(float value)
	{
		___DefultMoveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_fMoveSpeedAI_6() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_fMoveSpeedAI_6)); }
	inline float get_m_fMoveSpeedAI_6() const { return ___m_fMoveSpeedAI_6; }
	inline float* get_address_of_m_fMoveSpeedAI_6() { return &___m_fMoveSpeedAI_6; }
	inline void set_m_fMoveSpeedAI_6(float value)
	{
		___m_fMoveSpeedAI_6 = value;
	}

	inline static int32_t get_offset_of_m_fAzimuth_7() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_fAzimuth_7)); }
	inline float get_m_fAzimuth_7() const { return ___m_fAzimuth_7; }
	inline float* get_address_of_m_fAzimuth_7() { return &___m_fAzimuth_7; }
	inline void set_m_fAzimuth_7(float value)
	{
		___m_fAzimuth_7 = value;
	}

	inline static int32_t get_offset_of_m_sBtnStore_8() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_sBtnStore_8)); }
	inline Int32U5BU5D_t3030399641* get_m_sBtnStore_8() const { return ___m_sBtnStore_8; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_sBtnStore_8() { return &___m_sBtnStore_8; }
	inline void set_m_sBtnStore_8(Int32U5BU5D_t3030399641* value)
	{
		___m_sBtnStore_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_sBtnStore_8, value);
	}

	inline static int32_t get_offset_of_m_sBSCount_9() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_sBSCount_9)); }
	inline int32_t get_m_sBSCount_9() const { return ___m_sBSCount_9; }
	inline int32_t* get_address_of_m_sBSCount_9() { return &___m_sBSCount_9; }
	inline void set_m_sBSCount_9(int32_t value)
	{
		___m_sBSCount_9 = value;
	}

	inline static int32_t get_offset_of_m_bMoveCorutine_10() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_bMoveCorutine_10)); }
	inline bool get_m_bMoveCorutine_10() const { return ___m_bMoveCorutine_10; }
	inline bool* get_address_of_m_bMoveCorutine_10() { return &___m_bMoveCorutine_10; }
	inline void set_m_bMoveCorutine_10(bool value)
	{
		___m_bMoveCorutine_10 = value;
	}

	inline static int32_t get_offset_of_m_fObjRigid_11() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_fObjRigid_11)); }
	inline Rigidbody_t4233889191 * get_m_fObjRigid_11() const { return ___m_fObjRigid_11; }
	inline Rigidbody_t4233889191 ** get_address_of_m_fObjRigid_11() { return &___m_fObjRigid_11; }
	inline void set_m_fObjRigid_11(Rigidbody_t4233889191 * value)
	{
		___m_fObjRigid_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_fObjRigid_11, value);
	}

	inline static int32_t get_offset_of_m_NavObj_12() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_NavObj_12)); }
	inline GameObject_t1756533147 * get_m_NavObj_12() const { return ___m_NavObj_12; }
	inline GameObject_t1756533147 ** get_address_of_m_NavObj_12() { return &___m_NavObj_12; }
	inline void set_m_NavObj_12(GameObject_t1756533147 * value)
	{
		___m_NavObj_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_NavObj_12, value);
	}

	inline static int32_t get_offset_of_m_MoveDirect_13() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_MoveDirect_13)); }
	inline Vector3_t2243707580  get_m_MoveDirect_13() const { return ___m_MoveDirect_13; }
	inline Vector3_t2243707580 * get_address_of_m_MoveDirect_13() { return &___m_MoveDirect_13; }
	inline void set_m_MoveDirect_13(Vector3_t2243707580  value)
	{
		___m_MoveDirect_13 = value;
	}

	inline static int32_t get_offset_of_m_CheckObj_14() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_CheckObj_14)); }
	inline GameObject_t1756533147 * get_m_CheckObj_14() const { return ___m_CheckObj_14; }
	inline GameObject_t1756533147 ** get_address_of_m_CheckObj_14() { return &___m_CheckObj_14; }
	inline void set_m_CheckObj_14(GameObject_t1756533147 * value)
	{
		___m_CheckObj_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_CheckObj_14, value);
	}

	inline static int32_t get_offset_of_m_Mobilmo_15() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_Mobilmo_15)); }
	inline Transform_t3275118058 * get_m_Mobilmo_15() const { return ___m_Mobilmo_15; }
	inline Transform_t3275118058 ** get_address_of_m_Mobilmo_15() { return &___m_Mobilmo_15; }
	inline void set_m_Mobilmo_15(Transform_t3275118058 * value)
	{
		___m_Mobilmo_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_Mobilmo_15, value);
	}

	inline static int32_t get_offset_of_m_fMoveValueX_16() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_fMoveValueX_16)); }
	inline float get_m_fMoveValueX_16() const { return ___m_fMoveValueX_16; }
	inline float* get_address_of_m_fMoveValueX_16() { return &___m_fMoveValueX_16; }
	inline void set_m_fMoveValueX_16(float value)
	{
		___m_fMoveValueX_16 = value;
	}

	inline static int32_t get_offset_of_m_fMoveValueY_17() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_fMoveValueY_17)); }
	inline float get_m_fMoveValueY_17() const { return ___m_fMoveValueY_17; }
	inline float* get_address_of_m_fMoveValueY_17() { return &___m_fMoveValueY_17; }
	inline void set_m_fMoveValueY_17(float value)
	{
		___m_fMoveValueY_17 = value;
	}

	inline static int32_t get_offset_of_m_RandomAnim_18() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_RandomAnim_18)); }
	inline Dictionary_2_t126436439 * get_m_RandomAnim_18() const { return ___m_RandomAnim_18; }
	inline Dictionary_2_t126436439 ** get_address_of_m_RandomAnim_18() { return &___m_RandomAnim_18; }
	inline void set_m_RandomAnim_18(Dictionary_2_t126436439 * value)
	{
		___m_RandomAnim_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_RandomAnim_18, value);
	}

	inline static int32_t get_offset_of_m_iCountAIMove_19() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_iCountAIMove_19)); }
	inline int32_t get_m_iCountAIMove_19() const { return ___m_iCountAIMove_19; }
	inline int32_t* get_address_of_m_iCountAIMove_19() { return &___m_iCountAIMove_19; }
	inline void set_m_iCountAIMove_19(int32_t value)
	{
		___m_iCountAIMove_19 = value;
	}

	inline static int32_t get_offset_of_m_Anim_20() { return static_cast<int32_t>(offsetof(AIMove_t3831276007, ___m_Anim_20)); }
	inline Animation_t2068071072 * get_m_Anim_20() const { return ___m_Anim_20; }
	inline Animation_t2068071072 ** get_address_of_m_Anim_20() { return &___m_Anim_20; }
	inline void set_m_Anim_20(Animation_t2068071072 * value)
	{
		___m_Anim_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_Anim_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
