﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1292172592.h"

// System.String
struct String_t;
// TitleLogin
struct TitleLogin_t2728597065;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// APIManager
struct  APIManager_t2595496257  : public SingletonMonoBehaviour_1_t1292172592
{
public:
	// System.Boolean APIManager::OFFLINE
	bool ___OFFLINE_3;
	// System.String APIManager::uuidName
	String_t* ___uuidName_4;
	// System.String APIManager::contentTypeName
	String_t* ___contentTypeName_5;
	// System.String APIManager::userAgentName
	String_t* ___userAgentName_6;
	// System.String APIManager::authkeyName
	String_t* ___authkeyName_7;
	// System.String APIManager::uuid
	String_t* ___uuid_8;
	// System.String APIManager::contentType
	String_t* ___contentType_9;
	// System.String APIManager::userAgent
	String_t* ___userAgent_10;
	// System.String APIManager::authkey
	String_t* ___authkey_11;
	// TitleLogin APIManager::_titleLogin
	TitleLogin_t2728597065 * ____titleLogin_12;
	// System.Boolean APIManager::isNotReachable
	bool ___isNotReachable_13;
	// UnityEngine.TextAsset APIManager::m_VersionTextAsset
	TextAsset_t3973159845 * ___m_VersionTextAsset_14;
	// System.String APIManager::m_VersionString
	String_t* ___m_VersionString_15;
	// System.String APIManager::m_DeviceModel
	String_t* ___m_DeviceModel_16;

public:
	inline static int32_t get_offset_of_OFFLINE_3() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___OFFLINE_3)); }
	inline bool get_OFFLINE_3() const { return ___OFFLINE_3; }
	inline bool* get_address_of_OFFLINE_3() { return &___OFFLINE_3; }
	inline void set_OFFLINE_3(bool value)
	{
		___OFFLINE_3 = value;
	}

	inline static int32_t get_offset_of_uuidName_4() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___uuidName_4)); }
	inline String_t* get_uuidName_4() const { return ___uuidName_4; }
	inline String_t** get_address_of_uuidName_4() { return &___uuidName_4; }
	inline void set_uuidName_4(String_t* value)
	{
		___uuidName_4 = value;
		Il2CppCodeGenWriteBarrier(&___uuidName_4, value);
	}

	inline static int32_t get_offset_of_contentTypeName_5() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___contentTypeName_5)); }
	inline String_t* get_contentTypeName_5() const { return ___contentTypeName_5; }
	inline String_t** get_address_of_contentTypeName_5() { return &___contentTypeName_5; }
	inline void set_contentTypeName_5(String_t* value)
	{
		___contentTypeName_5 = value;
		Il2CppCodeGenWriteBarrier(&___contentTypeName_5, value);
	}

	inline static int32_t get_offset_of_userAgentName_6() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___userAgentName_6)); }
	inline String_t* get_userAgentName_6() const { return ___userAgentName_6; }
	inline String_t** get_address_of_userAgentName_6() { return &___userAgentName_6; }
	inline void set_userAgentName_6(String_t* value)
	{
		___userAgentName_6 = value;
		Il2CppCodeGenWriteBarrier(&___userAgentName_6, value);
	}

	inline static int32_t get_offset_of_authkeyName_7() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___authkeyName_7)); }
	inline String_t* get_authkeyName_7() const { return ___authkeyName_7; }
	inline String_t** get_address_of_authkeyName_7() { return &___authkeyName_7; }
	inline void set_authkeyName_7(String_t* value)
	{
		___authkeyName_7 = value;
		Il2CppCodeGenWriteBarrier(&___authkeyName_7, value);
	}

	inline static int32_t get_offset_of_uuid_8() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___uuid_8)); }
	inline String_t* get_uuid_8() const { return ___uuid_8; }
	inline String_t** get_address_of_uuid_8() { return &___uuid_8; }
	inline void set_uuid_8(String_t* value)
	{
		___uuid_8 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_8, value);
	}

	inline static int32_t get_offset_of_contentType_9() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___contentType_9)); }
	inline String_t* get_contentType_9() const { return ___contentType_9; }
	inline String_t** get_address_of_contentType_9() { return &___contentType_9; }
	inline void set_contentType_9(String_t* value)
	{
		___contentType_9 = value;
		Il2CppCodeGenWriteBarrier(&___contentType_9, value);
	}

	inline static int32_t get_offset_of_userAgent_10() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___userAgent_10)); }
	inline String_t* get_userAgent_10() const { return ___userAgent_10; }
	inline String_t** get_address_of_userAgent_10() { return &___userAgent_10; }
	inline void set_userAgent_10(String_t* value)
	{
		___userAgent_10 = value;
		Il2CppCodeGenWriteBarrier(&___userAgent_10, value);
	}

	inline static int32_t get_offset_of_authkey_11() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___authkey_11)); }
	inline String_t* get_authkey_11() const { return ___authkey_11; }
	inline String_t** get_address_of_authkey_11() { return &___authkey_11; }
	inline void set_authkey_11(String_t* value)
	{
		___authkey_11 = value;
		Il2CppCodeGenWriteBarrier(&___authkey_11, value);
	}

	inline static int32_t get_offset_of__titleLogin_12() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ____titleLogin_12)); }
	inline TitleLogin_t2728597065 * get__titleLogin_12() const { return ____titleLogin_12; }
	inline TitleLogin_t2728597065 ** get_address_of__titleLogin_12() { return &____titleLogin_12; }
	inline void set__titleLogin_12(TitleLogin_t2728597065 * value)
	{
		____titleLogin_12 = value;
		Il2CppCodeGenWriteBarrier(&____titleLogin_12, value);
	}

	inline static int32_t get_offset_of_isNotReachable_13() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___isNotReachable_13)); }
	inline bool get_isNotReachable_13() const { return ___isNotReachable_13; }
	inline bool* get_address_of_isNotReachable_13() { return &___isNotReachable_13; }
	inline void set_isNotReachable_13(bool value)
	{
		___isNotReachable_13 = value;
	}

	inline static int32_t get_offset_of_m_VersionTextAsset_14() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___m_VersionTextAsset_14)); }
	inline TextAsset_t3973159845 * get_m_VersionTextAsset_14() const { return ___m_VersionTextAsset_14; }
	inline TextAsset_t3973159845 ** get_address_of_m_VersionTextAsset_14() { return &___m_VersionTextAsset_14; }
	inline void set_m_VersionTextAsset_14(TextAsset_t3973159845 * value)
	{
		___m_VersionTextAsset_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_VersionTextAsset_14, value);
	}

	inline static int32_t get_offset_of_m_VersionString_15() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___m_VersionString_15)); }
	inline String_t* get_m_VersionString_15() const { return ___m_VersionString_15; }
	inline String_t** get_address_of_m_VersionString_15() { return &___m_VersionString_15; }
	inline void set_m_VersionString_15(String_t* value)
	{
		___m_VersionString_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_VersionString_15, value);
	}

	inline static int32_t get_offset_of_m_DeviceModel_16() { return static_cast<int32_t>(offsetof(APIManager_t2595496257, ___m_DeviceModel_16)); }
	inline String_t* get_m_DeviceModel_16() const { return ___m_DeviceModel_16; }
	inline String_t** get_address_of_m_DeviceModel_16() { return &___m_DeviceModel_16; }
	inline void set_m_DeviceModel_16(String_t* value)
	{
		___m_DeviceModel_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_DeviceModel_16, value);
	}
};

struct APIManager_t2595496257_StaticFields
{
public:
	// System.Action APIManager::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_17;
	// System.Action APIManager::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(APIManager_t2595496257_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_18() { return static_cast<int32_t>(offsetof(APIManager_t2595496257_StaticFields, ___U3CU3Ef__amU24cache1_18)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_18() const { return ___U3CU3Ef__amU24cache1_18; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_18() { return &___U3CU3Ef__amU24cache1_18; }
	inline void set_U3CU3Ef__amU24cache1_18(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
