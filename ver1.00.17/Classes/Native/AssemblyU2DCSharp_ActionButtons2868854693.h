﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionButtons
struct  ActionButtons_t2868854693  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ActionButtons::ActionIconButtonPrefab
	GameObject_t1756533147 * ___ActionIconButtonPrefab_2;
	// UnityEngine.GameObject ActionButtons::ActionButton
	GameObject_t1756533147 * ___ActionButton_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ActionButtons::Buttons
	List_1_t1125654279 * ___Buttons_4;
	// UnityEngine.GameObject ActionButtons::ActionTitle
	GameObject_t1756533147 * ___ActionTitle_5;
	// UnityEngine.GameObject ActionButtons::PoseTitle
	GameObject_t1756533147 * ___PoseTitle_6;
	// UnityEngine.GameObject ActionButtons::ActionListPanel
	GameObject_t1756533147 * ___ActionListPanel_7;
	// UnityEngine.UI.Text ActionButtons::ActionButtonName
	Text_t356221433 * ___ActionButtonName_8;

public:
	inline static int32_t get_offset_of_ActionIconButtonPrefab_2() { return static_cast<int32_t>(offsetof(ActionButtons_t2868854693, ___ActionIconButtonPrefab_2)); }
	inline GameObject_t1756533147 * get_ActionIconButtonPrefab_2() const { return ___ActionIconButtonPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_ActionIconButtonPrefab_2() { return &___ActionIconButtonPrefab_2; }
	inline void set_ActionIconButtonPrefab_2(GameObject_t1756533147 * value)
	{
		___ActionIconButtonPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___ActionIconButtonPrefab_2, value);
	}

	inline static int32_t get_offset_of_ActionButton_3() { return static_cast<int32_t>(offsetof(ActionButtons_t2868854693, ___ActionButton_3)); }
	inline GameObject_t1756533147 * get_ActionButton_3() const { return ___ActionButton_3; }
	inline GameObject_t1756533147 ** get_address_of_ActionButton_3() { return &___ActionButton_3; }
	inline void set_ActionButton_3(GameObject_t1756533147 * value)
	{
		___ActionButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButton_3, value);
	}

	inline static int32_t get_offset_of_Buttons_4() { return static_cast<int32_t>(offsetof(ActionButtons_t2868854693, ___Buttons_4)); }
	inline List_1_t1125654279 * get_Buttons_4() const { return ___Buttons_4; }
	inline List_1_t1125654279 ** get_address_of_Buttons_4() { return &___Buttons_4; }
	inline void set_Buttons_4(List_1_t1125654279 * value)
	{
		___Buttons_4 = value;
		Il2CppCodeGenWriteBarrier(&___Buttons_4, value);
	}

	inline static int32_t get_offset_of_ActionTitle_5() { return static_cast<int32_t>(offsetof(ActionButtons_t2868854693, ___ActionTitle_5)); }
	inline GameObject_t1756533147 * get_ActionTitle_5() const { return ___ActionTitle_5; }
	inline GameObject_t1756533147 ** get_address_of_ActionTitle_5() { return &___ActionTitle_5; }
	inline void set_ActionTitle_5(GameObject_t1756533147 * value)
	{
		___ActionTitle_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionTitle_5, value);
	}

	inline static int32_t get_offset_of_PoseTitle_6() { return static_cast<int32_t>(offsetof(ActionButtons_t2868854693, ___PoseTitle_6)); }
	inline GameObject_t1756533147 * get_PoseTitle_6() const { return ___PoseTitle_6; }
	inline GameObject_t1756533147 ** get_address_of_PoseTitle_6() { return &___PoseTitle_6; }
	inline void set_PoseTitle_6(GameObject_t1756533147 * value)
	{
		___PoseTitle_6 = value;
		Il2CppCodeGenWriteBarrier(&___PoseTitle_6, value);
	}

	inline static int32_t get_offset_of_ActionListPanel_7() { return static_cast<int32_t>(offsetof(ActionButtons_t2868854693, ___ActionListPanel_7)); }
	inline GameObject_t1756533147 * get_ActionListPanel_7() const { return ___ActionListPanel_7; }
	inline GameObject_t1756533147 ** get_address_of_ActionListPanel_7() { return &___ActionListPanel_7; }
	inline void set_ActionListPanel_7(GameObject_t1756533147 * value)
	{
		___ActionListPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActionListPanel_7, value);
	}

	inline static int32_t get_offset_of_ActionButtonName_8() { return static_cast<int32_t>(offsetof(ActionButtons_t2868854693, ___ActionButtonName_8)); }
	inline Text_t356221433 * get_ActionButtonName_8() const { return ___ActionButtonName_8; }
	inline Text_t356221433 ** get_address_of_ActionButtonName_8() { return &___ActionButtonName_8; }
	inline void set_ActionButtonName_8(Text_t356221433 * value)
	{
		___ActionButtonName_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButtonName_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
