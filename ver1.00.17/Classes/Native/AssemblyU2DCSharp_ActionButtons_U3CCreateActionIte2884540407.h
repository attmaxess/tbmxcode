﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// ActionButtons
struct ActionButtons_t2868854693;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionButtons/<CreateActionItems>c__AnonStorey0
struct  U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407  : public Il2CppObject
{
public:
	// System.Int32 ActionButtons/<CreateActionItems>c__AnonStorey0::sIndex
	int32_t ___sIndex_0;
	// System.String ActionButtons/<CreateActionItems>c__AnonStorey0::sKey
	String_t* ___sKey_1;
	// ActionButtons ActionButtons/<CreateActionItems>c__AnonStorey0::$this
	ActionButtons_t2868854693 * ___U24this_2;

public:
	inline static int32_t get_offset_of_sIndex_0() { return static_cast<int32_t>(offsetof(U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407, ___sIndex_0)); }
	inline int32_t get_sIndex_0() const { return ___sIndex_0; }
	inline int32_t* get_address_of_sIndex_0() { return &___sIndex_0; }
	inline void set_sIndex_0(int32_t value)
	{
		___sIndex_0 = value;
	}

	inline static int32_t get_offset_of_sKey_1() { return static_cast<int32_t>(offsetof(U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407, ___sKey_1)); }
	inline String_t* get_sKey_1() const { return ___sKey_1; }
	inline String_t** get_address_of_sKey_1() { return &___sKey_1; }
	inline void set_sKey_1(String_t* value)
	{
		___sKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___sKey_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407, ___U24this_2)); }
	inline ActionButtons_t2868854693 * get_U24this_2() const { return ___U24this_2; }
	inline ActionButtons_t2868854693 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ActionButtons_t2868854693 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
