﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// SlideInOut
struct SlideInOut_t958296806;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionConfigPanel
struct  ActionConfigPanel_t3304127690  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ActionConfigPanel::ConfigPanel
	GameObject_t1756533147 * ___ConfigPanel_2;
	// SlideInOut ActionConfigPanel::m_Slider
	SlideInOut_t958296806 * ___m_Slider_3;

public:
	inline static int32_t get_offset_of_ConfigPanel_2() { return static_cast<int32_t>(offsetof(ActionConfigPanel_t3304127690, ___ConfigPanel_2)); }
	inline GameObject_t1756533147 * get_ConfigPanel_2() const { return ___ConfigPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_ConfigPanel_2() { return &___ConfigPanel_2; }
	inline void set_ConfigPanel_2(GameObject_t1756533147 * value)
	{
		___ConfigPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigPanel_2, value);
	}

	inline static int32_t get_offset_of_m_Slider_3() { return static_cast<int32_t>(offsetof(ActionConfigPanel_t3304127690, ___m_Slider_3)); }
	inline SlideInOut_t958296806 * get_m_Slider_3() const { return ___m_Slider_3; }
	inline SlideInOut_t958296806 ** get_address_of_m_Slider_3() { return &___m_Slider_3; }
	inline void set_m_Slider_3(SlideInOut_t958296806 * value)
	{
		___m_Slider_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Slider_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
