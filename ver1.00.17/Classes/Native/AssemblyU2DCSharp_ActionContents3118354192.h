﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionContents
struct  ActionContents_t3118354192  : public Il2CppObject
{
public:
	// UnityEngine.GameObject ActionContents::ActionObjPrefab
	GameObject_t1756533147 * ___ActionObjPrefab_0;
	// UnityEngine.Sprite ActionContents::ActiveButton
	Sprite_t309593783 * ___ActiveButton_1;
	// UnityEngine.Sprite ActionContents::NonActiveButton
	Sprite_t309593783 * ___NonActiveButton_2;

public:
	inline static int32_t get_offset_of_ActionObjPrefab_0() { return static_cast<int32_t>(offsetof(ActionContents_t3118354192, ___ActionObjPrefab_0)); }
	inline GameObject_t1756533147 * get_ActionObjPrefab_0() const { return ___ActionObjPrefab_0; }
	inline GameObject_t1756533147 ** get_address_of_ActionObjPrefab_0() { return &___ActionObjPrefab_0; }
	inline void set_ActionObjPrefab_0(GameObject_t1756533147 * value)
	{
		___ActionObjPrefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___ActionObjPrefab_0, value);
	}

	inline static int32_t get_offset_of_ActiveButton_1() { return static_cast<int32_t>(offsetof(ActionContents_t3118354192, ___ActiveButton_1)); }
	inline Sprite_t309593783 * get_ActiveButton_1() const { return ___ActiveButton_1; }
	inline Sprite_t309593783 ** get_address_of_ActiveButton_1() { return &___ActiveButton_1; }
	inline void set_ActiveButton_1(Sprite_t309593783 * value)
	{
		___ActiveButton_1 = value;
		Il2CppCodeGenWriteBarrier(&___ActiveButton_1, value);
	}

	inline static int32_t get_offset_of_NonActiveButton_2() { return static_cast<int32_t>(offsetof(ActionContents_t3118354192, ___NonActiveButton_2)); }
	inline Sprite_t309593783 * get_NonActiveButton_2() const { return ___NonActiveButton_2; }
	inline Sprite_t309593783 ** get_address_of_NonActiveButton_2() { return &___NonActiveButton_2; }
	inline void set_NonActiveButton_2(Sprite_t309593783 * value)
	{
		___NonActiveButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___NonActiveButton_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
