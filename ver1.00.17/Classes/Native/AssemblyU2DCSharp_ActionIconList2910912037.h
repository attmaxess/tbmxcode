﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionIconList
struct  ActionIconList_t2910912037  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite[] ActionIconList::Icons
	SpriteU5BU5D_t3359083662* ___Icons_2;
	// System.Single[] ActionIconList::Width
	SingleU5BU5D_t577127397* ___Width_3;
	// UnityEngine.UI.Image ActionIconList::ActionIcon
	Image_t2042527209 * ___ActionIcon_4;

public:
	inline static int32_t get_offset_of_Icons_2() { return static_cast<int32_t>(offsetof(ActionIconList_t2910912037, ___Icons_2)); }
	inline SpriteU5BU5D_t3359083662* get_Icons_2() const { return ___Icons_2; }
	inline SpriteU5BU5D_t3359083662** get_address_of_Icons_2() { return &___Icons_2; }
	inline void set_Icons_2(SpriteU5BU5D_t3359083662* value)
	{
		___Icons_2 = value;
		Il2CppCodeGenWriteBarrier(&___Icons_2, value);
	}

	inline static int32_t get_offset_of_Width_3() { return static_cast<int32_t>(offsetof(ActionIconList_t2910912037, ___Width_3)); }
	inline SingleU5BU5D_t577127397* get_Width_3() const { return ___Width_3; }
	inline SingleU5BU5D_t577127397** get_address_of_Width_3() { return &___Width_3; }
	inline void set_Width_3(SingleU5BU5D_t577127397* value)
	{
		___Width_3 = value;
		Il2CppCodeGenWriteBarrier(&___Width_3, value);
	}

	inline static int32_t get_offset_of_ActionIcon_4() { return static_cast<int32_t>(offsetof(ActionIconList_t2910912037, ___ActionIcon_4)); }
	inline Image_t2042527209 * get_ActionIcon_4() const { return ___ActionIcon_4; }
	inline Image_t2042527209 ** get_address_of_ActionIcon_4() { return &___ActionIcon_4; }
	inline void set_ActionIcon_4(Image_t2042527209 * value)
	{
		___ActionIcon_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionIcon_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
