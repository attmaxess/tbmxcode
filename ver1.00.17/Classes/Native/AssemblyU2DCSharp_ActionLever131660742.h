﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionLever
struct  ActionLever_t131660742  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ActionLever::LeverUp
	float ___LeverUp_2;
	// System.Single ActionLever::LeverDown
	float ___LeverDown_3;
	// UnityEngine.Transform ActionLever::Levers
	Transform_t3275118058 * ___Levers_4;

public:
	inline static int32_t get_offset_of_LeverUp_2() { return static_cast<int32_t>(offsetof(ActionLever_t131660742, ___LeverUp_2)); }
	inline float get_LeverUp_2() const { return ___LeverUp_2; }
	inline float* get_address_of_LeverUp_2() { return &___LeverUp_2; }
	inline void set_LeverUp_2(float value)
	{
		___LeverUp_2 = value;
	}

	inline static int32_t get_offset_of_LeverDown_3() { return static_cast<int32_t>(offsetof(ActionLever_t131660742, ___LeverDown_3)); }
	inline float get_LeverDown_3() const { return ___LeverDown_3; }
	inline float* get_address_of_LeverDown_3() { return &___LeverDown_3; }
	inline void set_LeverDown_3(float value)
	{
		___LeverDown_3 = value;
	}

	inline static int32_t get_offset_of_Levers_4() { return static_cast<int32_t>(offsetof(ActionLever_t131660742, ___Levers_4)); }
	inline Transform_t3275118058 * get_Levers_4() const { return ___Levers_4; }
	inline Transform_t3275118058 ** get_address_of_Levers_4() { return &___Levers_4; }
	inline void set_Levers_4(Transform_t3275118058 * value)
	{
		___Levers_4 = value;
		Il2CppCodeGenWriteBarrier(&___Levers_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
