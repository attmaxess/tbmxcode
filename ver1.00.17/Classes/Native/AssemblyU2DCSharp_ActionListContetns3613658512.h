﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionListContetns
struct  ActionListContetns_t3613658512  : public Il2CppObject
{
public:
	// UnityEngine.UI.RawImage ActionListContetns::Background
	RawImage_t2749640213 * ___Background_0;
	// UnityEngine.UI.RawImage ActionListContetns::Image
	RawImage_t2749640213 * ___Image_1;
	// UnityEngine.UI.Text ActionListContetns::Text
	Text_t356221433 * ___Text_2;

public:
	inline static int32_t get_offset_of_Background_0() { return static_cast<int32_t>(offsetof(ActionListContetns_t3613658512, ___Background_0)); }
	inline RawImage_t2749640213 * get_Background_0() const { return ___Background_0; }
	inline RawImage_t2749640213 ** get_address_of_Background_0() { return &___Background_0; }
	inline void set_Background_0(RawImage_t2749640213 * value)
	{
		___Background_0 = value;
		Il2CppCodeGenWriteBarrier(&___Background_0, value);
	}

	inline static int32_t get_offset_of_Image_1() { return static_cast<int32_t>(offsetof(ActionListContetns_t3613658512, ___Image_1)); }
	inline RawImage_t2749640213 * get_Image_1() const { return ___Image_1; }
	inline RawImage_t2749640213 ** get_address_of_Image_1() { return &___Image_1; }
	inline void set_Image_1(RawImage_t2749640213 * value)
	{
		___Image_1 = value;
		Il2CppCodeGenWriteBarrier(&___Image_1, value);
	}

	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(ActionListContetns_t3613658512, ___Text_2)); }
	inline Text_t356221433 * get_Text_2() const { return ___Text_2; }
	inline Text_t356221433 ** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(Text_t356221433 * value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
