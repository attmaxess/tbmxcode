﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen64399510.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Pose[]
struct PoseU5BU5D_t1998674362;
// UnityEngine.UI.Slider[]
struct SliderU5BU5D_t1144817634;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Icon[]
struct IconU5BU5D_t2441691448;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// System.Collections.Generic.List`1<UnityEngine.Texture2D[]>
struct List_1_t2093211384;
// Module
struct Module_t3140434828;
// ActionButtons
struct ActionButtons_t2868854693;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionManager
struct  ActionManager_t1367723175  : public SingletonMonoBehaviour_1_t64399510
{
public:
	// UnityEngine.GameObject ActionManager::ActionSetUI
	GameObject_t1756533147 * ___ActionSetUI_3;
	// Pose[] ActionManager::Poses
	PoseU5BU5D_t1998674362* ___Poses_4;
	// UnityEngine.UI.Slider[] ActionManager::ActionUI
	SliderU5BU5D_t1144817634* ___ActionUI_5;
	// UnityEngine.GameObject ActionManager::ActionButtons
	GameObject_t1756533147 * ___ActionButtons_6;
	// UnityEngine.GameObject ActionManager::NonActionSetBackground
	GameObject_t1756533147 * ___NonActionSetBackground_7;
	// UnityEngine.Sprite ActionManager::PoseSelect
	Sprite_t309593783 * ___PoseSelect_8;
	// UnityEngine.Sprite ActionManager::PoseNonSelect
	Sprite_t309593783 * ___PoseNonSelect_9;
	// UnityEngine.Camera ActionManager::createCamera
	Camera_t189460977 * ___createCamera_10;
	// System.Int32 ActionManager::ActionIconNum
	int32_t ___ActionIconNum_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> ActionManager::IconList
	Dictionary_2_t3943999495 * ___IconList_12;
	// Icon[] ActionManager::m_Icons
	IconU5BU5D_t2441691448* ___m_Icons_13;
	// UnityEngine.RenderTexture ActionManager::posetex
	RenderTexture_t2666733923 * ___posetex_14;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D[]> ActionManager::texturePoses
	List_1_t2093211384 * ___texturePoses_15;
	// UnityEngine.GameObject ActionManager::m_ModuleObj
	GameObject_t1756533147 * ___m_ModuleObj_16;
	// Module ActionManager::m_Module
	Module_t3140434828 * ___m_Module_17;
	// System.Int32 ActionManager::m_sSelectPoseNum
	int32_t ___m_sSelectPoseNum_18;
	// System.Int32 ActionManager::m_sSelectActionNum
	int32_t ___m_sSelectActionNum_19;
	// System.Boolean ActionManager::m_bIsPlay
	bool ___m_bIsPlay_20;
	// ActionButtons ActionManager::m_ActionButtons
	ActionButtons_t2868854693 * ___m_ActionButtons_21;

public:
	inline static int32_t get_offset_of_ActionSetUI_3() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___ActionSetUI_3)); }
	inline GameObject_t1756533147 * get_ActionSetUI_3() const { return ___ActionSetUI_3; }
	inline GameObject_t1756533147 ** get_address_of_ActionSetUI_3() { return &___ActionSetUI_3; }
	inline void set_ActionSetUI_3(GameObject_t1756533147 * value)
	{
		___ActionSetUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActionSetUI_3, value);
	}

	inline static int32_t get_offset_of_Poses_4() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___Poses_4)); }
	inline PoseU5BU5D_t1998674362* get_Poses_4() const { return ___Poses_4; }
	inline PoseU5BU5D_t1998674362** get_address_of_Poses_4() { return &___Poses_4; }
	inline void set_Poses_4(PoseU5BU5D_t1998674362* value)
	{
		___Poses_4 = value;
		Il2CppCodeGenWriteBarrier(&___Poses_4, value);
	}

	inline static int32_t get_offset_of_ActionUI_5() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___ActionUI_5)); }
	inline SliderU5BU5D_t1144817634* get_ActionUI_5() const { return ___ActionUI_5; }
	inline SliderU5BU5D_t1144817634** get_address_of_ActionUI_5() { return &___ActionUI_5; }
	inline void set_ActionUI_5(SliderU5BU5D_t1144817634* value)
	{
		___ActionUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionUI_5, value);
	}

	inline static int32_t get_offset_of_ActionButtons_6() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___ActionButtons_6)); }
	inline GameObject_t1756533147 * get_ActionButtons_6() const { return ___ActionButtons_6; }
	inline GameObject_t1756533147 ** get_address_of_ActionButtons_6() { return &___ActionButtons_6; }
	inline void set_ActionButtons_6(GameObject_t1756533147 * value)
	{
		___ActionButtons_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButtons_6, value);
	}

	inline static int32_t get_offset_of_NonActionSetBackground_7() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___NonActionSetBackground_7)); }
	inline GameObject_t1756533147 * get_NonActionSetBackground_7() const { return ___NonActionSetBackground_7; }
	inline GameObject_t1756533147 ** get_address_of_NonActionSetBackground_7() { return &___NonActionSetBackground_7; }
	inline void set_NonActionSetBackground_7(GameObject_t1756533147 * value)
	{
		___NonActionSetBackground_7 = value;
		Il2CppCodeGenWriteBarrier(&___NonActionSetBackground_7, value);
	}

	inline static int32_t get_offset_of_PoseSelect_8() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___PoseSelect_8)); }
	inline Sprite_t309593783 * get_PoseSelect_8() const { return ___PoseSelect_8; }
	inline Sprite_t309593783 ** get_address_of_PoseSelect_8() { return &___PoseSelect_8; }
	inline void set_PoseSelect_8(Sprite_t309593783 * value)
	{
		___PoseSelect_8 = value;
		Il2CppCodeGenWriteBarrier(&___PoseSelect_8, value);
	}

	inline static int32_t get_offset_of_PoseNonSelect_9() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___PoseNonSelect_9)); }
	inline Sprite_t309593783 * get_PoseNonSelect_9() const { return ___PoseNonSelect_9; }
	inline Sprite_t309593783 ** get_address_of_PoseNonSelect_9() { return &___PoseNonSelect_9; }
	inline void set_PoseNonSelect_9(Sprite_t309593783 * value)
	{
		___PoseNonSelect_9 = value;
		Il2CppCodeGenWriteBarrier(&___PoseNonSelect_9, value);
	}

	inline static int32_t get_offset_of_createCamera_10() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___createCamera_10)); }
	inline Camera_t189460977 * get_createCamera_10() const { return ___createCamera_10; }
	inline Camera_t189460977 ** get_address_of_createCamera_10() { return &___createCamera_10; }
	inline void set_createCamera_10(Camera_t189460977 * value)
	{
		___createCamera_10 = value;
		Il2CppCodeGenWriteBarrier(&___createCamera_10, value);
	}

	inline static int32_t get_offset_of_ActionIconNum_11() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___ActionIconNum_11)); }
	inline int32_t get_ActionIconNum_11() const { return ___ActionIconNum_11; }
	inline int32_t* get_address_of_ActionIconNum_11() { return &___ActionIconNum_11; }
	inline void set_ActionIconNum_11(int32_t value)
	{
		___ActionIconNum_11 = value;
	}

	inline static int32_t get_offset_of_IconList_12() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___IconList_12)); }
	inline Dictionary_2_t3943999495 * get_IconList_12() const { return ___IconList_12; }
	inline Dictionary_2_t3943999495 ** get_address_of_IconList_12() { return &___IconList_12; }
	inline void set_IconList_12(Dictionary_2_t3943999495 * value)
	{
		___IconList_12 = value;
		Il2CppCodeGenWriteBarrier(&___IconList_12, value);
	}

	inline static int32_t get_offset_of_m_Icons_13() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___m_Icons_13)); }
	inline IconU5BU5D_t2441691448* get_m_Icons_13() const { return ___m_Icons_13; }
	inline IconU5BU5D_t2441691448** get_address_of_m_Icons_13() { return &___m_Icons_13; }
	inline void set_m_Icons_13(IconU5BU5D_t2441691448* value)
	{
		___m_Icons_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_Icons_13, value);
	}

	inline static int32_t get_offset_of_posetex_14() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___posetex_14)); }
	inline RenderTexture_t2666733923 * get_posetex_14() const { return ___posetex_14; }
	inline RenderTexture_t2666733923 ** get_address_of_posetex_14() { return &___posetex_14; }
	inline void set_posetex_14(RenderTexture_t2666733923 * value)
	{
		___posetex_14 = value;
		Il2CppCodeGenWriteBarrier(&___posetex_14, value);
	}

	inline static int32_t get_offset_of_texturePoses_15() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___texturePoses_15)); }
	inline List_1_t2093211384 * get_texturePoses_15() const { return ___texturePoses_15; }
	inline List_1_t2093211384 ** get_address_of_texturePoses_15() { return &___texturePoses_15; }
	inline void set_texturePoses_15(List_1_t2093211384 * value)
	{
		___texturePoses_15 = value;
		Il2CppCodeGenWriteBarrier(&___texturePoses_15, value);
	}

	inline static int32_t get_offset_of_m_ModuleObj_16() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___m_ModuleObj_16)); }
	inline GameObject_t1756533147 * get_m_ModuleObj_16() const { return ___m_ModuleObj_16; }
	inline GameObject_t1756533147 ** get_address_of_m_ModuleObj_16() { return &___m_ModuleObj_16; }
	inline void set_m_ModuleObj_16(GameObject_t1756533147 * value)
	{
		___m_ModuleObj_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_ModuleObj_16, value);
	}

	inline static int32_t get_offset_of_m_Module_17() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___m_Module_17)); }
	inline Module_t3140434828 * get_m_Module_17() const { return ___m_Module_17; }
	inline Module_t3140434828 ** get_address_of_m_Module_17() { return &___m_Module_17; }
	inline void set_m_Module_17(Module_t3140434828 * value)
	{
		___m_Module_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_Module_17, value);
	}

	inline static int32_t get_offset_of_m_sSelectPoseNum_18() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___m_sSelectPoseNum_18)); }
	inline int32_t get_m_sSelectPoseNum_18() const { return ___m_sSelectPoseNum_18; }
	inline int32_t* get_address_of_m_sSelectPoseNum_18() { return &___m_sSelectPoseNum_18; }
	inline void set_m_sSelectPoseNum_18(int32_t value)
	{
		___m_sSelectPoseNum_18 = value;
	}

	inline static int32_t get_offset_of_m_sSelectActionNum_19() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___m_sSelectActionNum_19)); }
	inline int32_t get_m_sSelectActionNum_19() const { return ___m_sSelectActionNum_19; }
	inline int32_t* get_address_of_m_sSelectActionNum_19() { return &___m_sSelectActionNum_19; }
	inline void set_m_sSelectActionNum_19(int32_t value)
	{
		___m_sSelectActionNum_19 = value;
	}

	inline static int32_t get_offset_of_m_bIsPlay_20() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___m_bIsPlay_20)); }
	inline bool get_m_bIsPlay_20() const { return ___m_bIsPlay_20; }
	inline bool* get_address_of_m_bIsPlay_20() { return &___m_bIsPlay_20; }
	inline void set_m_bIsPlay_20(bool value)
	{
		___m_bIsPlay_20 = value;
	}

	inline static int32_t get_offset_of_m_ActionButtons_21() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175, ___m_ActionButtons_21)); }
	inline ActionButtons_t2868854693 * get_m_ActionButtons_21() const { return ___m_ActionButtons_21; }
	inline ActionButtons_t2868854693 ** get_address_of_m_ActionButtons_21() { return &___m_ActionButtons_21; }
	inline void set_m_ActionButtons_21(ActionButtons_t2868854693 * value)
	{
		___m_ActionButtons_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionButtons_21, value);
	}
};

struct ActionManager_t1367723175_StaticFields
{
public:
	// System.Action ActionManager::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_22;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(ActionManager_t1367723175_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
