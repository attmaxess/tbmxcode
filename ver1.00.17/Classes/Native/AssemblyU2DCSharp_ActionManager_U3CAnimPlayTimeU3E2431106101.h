﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// ActionManager
struct ActionManager_t1367723175;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionManager/<AnimPlayTime>c__Iterator0
struct  U3CAnimPlayTimeU3Ec__Iterator0_t2431106101  : public Il2CppObject
{
public:
	// System.Single ActionManager/<AnimPlayTime>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Single ActionManager/<AnimPlayTime>c__Iterator0::<fColorValue>__0
	float ___U3CfColorValueU3E__0_1;
	// System.Boolean ActionManager/<AnimPlayTime>c__Iterator0::<bColorReverse>__0
	bool ___U3CbColorReverseU3E__0_2;
	// ActionManager ActionManager/<AnimPlayTime>c__Iterator0::$this
	ActionManager_t1367723175 * ___U24this_3;
	// System.Object ActionManager/<AnimPlayTime>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean ActionManager/<AnimPlayTime>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 ActionManager/<AnimPlayTime>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimPlayTimeU3Ec__Iterator0_t2431106101, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CfColorValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimPlayTimeU3Ec__Iterator0_t2431106101, ___U3CfColorValueU3E__0_1)); }
	inline float get_U3CfColorValueU3E__0_1() const { return ___U3CfColorValueU3E__0_1; }
	inline float* get_address_of_U3CfColorValueU3E__0_1() { return &___U3CfColorValueU3E__0_1; }
	inline void set_U3CfColorValueU3E__0_1(float value)
	{
		___U3CfColorValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CbColorReverseU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimPlayTimeU3Ec__Iterator0_t2431106101, ___U3CbColorReverseU3E__0_2)); }
	inline bool get_U3CbColorReverseU3E__0_2() const { return ___U3CbColorReverseU3E__0_2; }
	inline bool* get_address_of_U3CbColorReverseU3E__0_2() { return &___U3CbColorReverseU3E__0_2; }
	inline void set_U3CbColorReverseU3E__0_2(bool value)
	{
		___U3CbColorReverseU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CAnimPlayTimeU3Ec__Iterator0_t2431106101, ___U24this_3)); }
	inline ActionManager_t1367723175 * get_U24this_3() const { return ___U24this_3; }
	inline ActionManager_t1367723175 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ActionManager_t1367723175 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAnimPlayTimeU3Ec__Iterator0_t2431106101, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAnimPlayTimeU3Ec__Iterator0_t2431106101, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAnimPlayTimeU3Ec__Iterator0_t2431106101, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
