﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Contents
struct Contents_t3730146386;
// ActionListContetns
struct ActionListContetns_t3613658512;
// ActionSetContetns
struct ActionSetContetns_t415653720;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionObj
struct  ActionObj_t2993311507  : public MonoBehaviour_t1158329972
{
public:
	// Contents ActionObj::Contents
	Contents_t3730146386 * ___Contents_2;
	// ActionListContetns ActionObj::ActionListContetns
	ActionListContetns_t3613658512 * ___ActionListContetns_3;
	// ActionSetContetns ActionObj::ActionSetContetns
	ActionSetContetns_t415653720 * ___ActionSetContetns_4;
	// UnityEngine.GameObject ActionObj::Module
	GameObject_t1756533147 * ___Module_5;

public:
	inline static int32_t get_offset_of_Contents_2() { return static_cast<int32_t>(offsetof(ActionObj_t2993311507, ___Contents_2)); }
	inline Contents_t3730146386 * get_Contents_2() const { return ___Contents_2; }
	inline Contents_t3730146386 ** get_address_of_Contents_2() { return &___Contents_2; }
	inline void set_Contents_2(Contents_t3730146386 * value)
	{
		___Contents_2 = value;
		Il2CppCodeGenWriteBarrier(&___Contents_2, value);
	}

	inline static int32_t get_offset_of_ActionListContetns_3() { return static_cast<int32_t>(offsetof(ActionObj_t2993311507, ___ActionListContetns_3)); }
	inline ActionListContetns_t3613658512 * get_ActionListContetns_3() const { return ___ActionListContetns_3; }
	inline ActionListContetns_t3613658512 ** get_address_of_ActionListContetns_3() { return &___ActionListContetns_3; }
	inline void set_ActionListContetns_3(ActionListContetns_t3613658512 * value)
	{
		___ActionListContetns_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActionListContetns_3, value);
	}

	inline static int32_t get_offset_of_ActionSetContetns_4() { return static_cast<int32_t>(offsetof(ActionObj_t2993311507, ___ActionSetContetns_4)); }
	inline ActionSetContetns_t415653720 * get_ActionSetContetns_4() const { return ___ActionSetContetns_4; }
	inline ActionSetContetns_t415653720 ** get_address_of_ActionSetContetns_4() { return &___ActionSetContetns_4; }
	inline void set_ActionSetContetns_4(ActionSetContetns_t415653720 * value)
	{
		___ActionSetContetns_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionSetContetns_4, value);
	}

	inline static int32_t get_offset_of_Module_5() { return static_cast<int32_t>(offsetof(ActionObj_t2993311507, ___Module_5)); }
	inline GameObject_t1756533147 * get_Module_5() const { return ___Module_5; }
	inline GameObject_t1756533147 ** get_address_of_Module_5() { return &___Module_5; }
	inline void set_Module_5(GameObject_t1756533147 * value)
	{
		___Module_5 = value;
		Il2CppCodeGenWriteBarrier(&___Module_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
