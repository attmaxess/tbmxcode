﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionPartsPanelController
struct  ActionPartsPanelController_t3710630626  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ActionPartsPanelController::m_ActionPartsUI
	GameObject_t1756533147 * ___m_ActionPartsUI_2;
	// UnityEngine.GameObject ActionPartsPanelController::m_ActionPartsPanel
	GameObject_t1756533147 * ___m_ActionPartsPanel_3;
	// UnityEngine.GameObject ActionPartsPanelController::m_ActionPartsDnaPanel
	GameObject_t1756533147 * ___m_ActionPartsDnaPanel_4;
	// UnityEngine.GameObject ActionPartsPanelController::m_ActionPartsDnaUI
	GameObject_t1756533147 * ___m_ActionPartsDnaUI_5;
	// UnityEngine.GameObject[] ActionPartsPanelController::m_SelectButtons
	GameObjectU5BU5D_t3057952154* ___m_SelectButtons_6;
	// UnityEngine.Sprite[] ActionPartsPanelController::m_SelectButtonSprite
	SpriteU5BU5D_t3359083662* ___m_SelectButtonSprite_7;
	// UnityEngine.GameObject[] ActionPartsPanelController::m_MenuButtons
	GameObjectU5BU5D_t3057952154* ___m_MenuButtons_8;
	// UnityEngine.GameObject[] ActionPartsPanelController::m_selectPanel
	GameObjectU5BU5D_t3057952154* ___m_selectPanel_9;
	// UnityEngine.UI.Image ActionPartsPanelController::actionCntPanel
	Image_t2042527209 * ___actionCntPanel_10;
	// UnityEngine.UI.Text ActionPartsPanelController::actionCntText
	Text_t356221433 * ___actionCntText_11;

public:
	inline static int32_t get_offset_of_m_ActionPartsUI_2() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_ActionPartsUI_2)); }
	inline GameObject_t1756533147 * get_m_ActionPartsUI_2() const { return ___m_ActionPartsUI_2; }
	inline GameObject_t1756533147 ** get_address_of_m_ActionPartsUI_2() { return &___m_ActionPartsUI_2; }
	inline void set_m_ActionPartsUI_2(GameObject_t1756533147 * value)
	{
		___m_ActionPartsUI_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionPartsUI_2, value);
	}

	inline static int32_t get_offset_of_m_ActionPartsPanel_3() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_ActionPartsPanel_3)); }
	inline GameObject_t1756533147 * get_m_ActionPartsPanel_3() const { return ___m_ActionPartsPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_m_ActionPartsPanel_3() { return &___m_ActionPartsPanel_3; }
	inline void set_m_ActionPartsPanel_3(GameObject_t1756533147 * value)
	{
		___m_ActionPartsPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionPartsPanel_3, value);
	}

	inline static int32_t get_offset_of_m_ActionPartsDnaPanel_4() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_ActionPartsDnaPanel_4)); }
	inline GameObject_t1756533147 * get_m_ActionPartsDnaPanel_4() const { return ___m_ActionPartsDnaPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_m_ActionPartsDnaPanel_4() { return &___m_ActionPartsDnaPanel_4; }
	inline void set_m_ActionPartsDnaPanel_4(GameObject_t1756533147 * value)
	{
		___m_ActionPartsDnaPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionPartsDnaPanel_4, value);
	}

	inline static int32_t get_offset_of_m_ActionPartsDnaUI_5() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_ActionPartsDnaUI_5)); }
	inline GameObject_t1756533147 * get_m_ActionPartsDnaUI_5() const { return ___m_ActionPartsDnaUI_5; }
	inline GameObject_t1756533147 ** get_address_of_m_ActionPartsDnaUI_5() { return &___m_ActionPartsDnaUI_5; }
	inline void set_m_ActionPartsDnaUI_5(GameObject_t1756533147 * value)
	{
		___m_ActionPartsDnaUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionPartsDnaUI_5, value);
	}

	inline static int32_t get_offset_of_m_SelectButtons_6() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_SelectButtons_6)); }
	inline GameObjectU5BU5D_t3057952154* get_m_SelectButtons_6() const { return ___m_SelectButtons_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_SelectButtons_6() { return &___m_SelectButtons_6; }
	inline void set_m_SelectButtons_6(GameObjectU5BU5D_t3057952154* value)
	{
		___m_SelectButtons_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_SelectButtons_6, value);
	}

	inline static int32_t get_offset_of_m_SelectButtonSprite_7() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_SelectButtonSprite_7)); }
	inline SpriteU5BU5D_t3359083662* get_m_SelectButtonSprite_7() const { return ___m_SelectButtonSprite_7; }
	inline SpriteU5BU5D_t3359083662** get_address_of_m_SelectButtonSprite_7() { return &___m_SelectButtonSprite_7; }
	inline void set_m_SelectButtonSprite_7(SpriteU5BU5D_t3359083662* value)
	{
		___m_SelectButtonSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_SelectButtonSprite_7, value);
	}

	inline static int32_t get_offset_of_m_MenuButtons_8() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_MenuButtons_8)); }
	inline GameObjectU5BU5D_t3057952154* get_m_MenuButtons_8() const { return ___m_MenuButtons_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_MenuButtons_8() { return &___m_MenuButtons_8; }
	inline void set_m_MenuButtons_8(GameObjectU5BU5D_t3057952154* value)
	{
		___m_MenuButtons_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_MenuButtons_8, value);
	}

	inline static int32_t get_offset_of_m_selectPanel_9() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___m_selectPanel_9)); }
	inline GameObjectU5BU5D_t3057952154* get_m_selectPanel_9() const { return ___m_selectPanel_9; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_selectPanel_9() { return &___m_selectPanel_9; }
	inline void set_m_selectPanel_9(GameObjectU5BU5D_t3057952154* value)
	{
		___m_selectPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_selectPanel_9, value);
	}

	inline static int32_t get_offset_of_actionCntPanel_10() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___actionCntPanel_10)); }
	inline Image_t2042527209 * get_actionCntPanel_10() const { return ___actionCntPanel_10; }
	inline Image_t2042527209 ** get_address_of_actionCntPanel_10() { return &___actionCntPanel_10; }
	inline void set_actionCntPanel_10(Image_t2042527209 * value)
	{
		___actionCntPanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___actionCntPanel_10, value);
	}

	inline static int32_t get_offset_of_actionCntText_11() { return static_cast<int32_t>(offsetof(ActionPartsPanelController_t3710630626, ___actionCntText_11)); }
	inline Text_t356221433 * get_actionCntText_11() const { return ___actionCntText_11; }
	inline Text_t356221433 ** get_address_of_actionCntText_11() { return &___actionCntText_11; }
	inline void set_actionCntText_11(Text_t356221433 * value)
	{
		___actionCntText_11 = value;
		Il2CppCodeGenWriteBarrier(&___actionCntText_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
