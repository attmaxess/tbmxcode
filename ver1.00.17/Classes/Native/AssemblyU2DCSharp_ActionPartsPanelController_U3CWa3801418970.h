﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// ActionPartsPanelController
struct ActionPartsPanelController_t3710630626;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionPartsPanelController/<WaitRecreateEditModule>c__Iterator0
struct  U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970  : public Il2CppObject
{
public:
	// ActionPartsPanelController ActionPartsPanelController/<WaitRecreateEditModule>c__Iterator0::$this
	ActionPartsPanelController_t3710630626 * ___U24this_0;
	// System.Object ActionPartsPanelController/<WaitRecreateEditModule>c__Iterator0::$current
	Il2CppObject * ___U24current_1;
	// System.Boolean ActionPartsPanelController/<WaitRecreateEditModule>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ActionPartsPanelController/<WaitRecreateEditModule>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970, ___U24this_0)); }
	inline ActionPartsPanelController_t3710630626 * get_U24this_0() const { return ___U24this_0; }
	inline ActionPartsPanelController_t3710630626 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ActionPartsPanelController_t3710630626 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_0, value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970, ___U24current_1)); }
	inline Il2CppObject * get_U24current_1() const { return ___U24current_1; }
	inline Il2CppObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(Il2CppObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_1, value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

struct U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970_StaticFields
{
public:
	// System.Func`1<System.Boolean> ActionPartsPanelController/<WaitRecreateEditModule>c__Iterator0::<>f__am$cache0
	Func_1_t1485000104 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
