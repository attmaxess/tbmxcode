﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// ActionConfigPanel
struct ActionConfigPanel_t3304127690;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// ActionButtons
struct ActionButtons_t2868854693;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t10059812;
// DG.Tweening.Tween
struct Tween_t278478013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionSet
struct  ActionSet_t264428022  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button ActionSet::BackButton
	Button_t2872111280 * ___BackButton_2;
	// UnityEngine.UI.Button ActionSet::CompleteButton
	Button_t2872111280 * ___CompleteButton_3;
	// UnityEngine.UI.Button ActionSet::ActionButton
	Button_t2872111280 * ___ActionButton_4;
	// UnityEngine.UI.Button ActionSet::PlayButton
	Button_t2872111280 * ___PlayButton_5;
	// UnityEngine.UI.Button ActionSet::ConfigButton
	Button_t2872111280 * ___ConfigButton_6;
	// UnityEngine.UI.Button ActionSet::ConfigCompleteButton
	Button_t2872111280 * ___ConfigCompleteButton_7;
	// ActionConfigPanel ActionSet::ActionConfigPanel
	ActionConfigPanel_t3304127690 * ___ActionConfigPanel_8;
	// UnityEngine.GameObject ActionSet::ActionTitle
	GameObject_t1756533147 * ___ActionTitle_9;
	// UnityEngine.GameObject ActionSet::PoseTitle
	GameObject_t1756533147 * ___PoseTitle_10;
	// UnityEngine.GameObject ActionSet::ActionListPanel
	GameObject_t1756533147 * ___ActionListPanel_11;
	// UnityEngine.CanvasGroup ActionSet::PlayButtonGroup
	CanvasGroup_t3296560743 * ___PlayButtonGroup_12;
	// ActionButtons ActionSet::ActionButtons
	ActionButtons_t2868854693 * ___ActionButtons_13;
	// UnityEngine.GameObject ActionSet::MovieObj
	GameObject_t1756533147 * ___MovieObj_14;
	// UnityEngine.Video.VideoPlayer ActionSet::TutorialVideoPlayer
	VideoPlayer_t10059812 * ___TutorialVideoPlayer_15;
	// DG.Tweening.Tween ActionSet::TutorialTW
	Tween_t278478013 * ___TutorialTW_16;
	// System.Boolean ActionSet::m_bFadeOut
	bool ___m_bFadeOut_17;

public:
	inline static int32_t get_offset_of_BackButton_2() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___BackButton_2)); }
	inline Button_t2872111280 * get_BackButton_2() const { return ___BackButton_2; }
	inline Button_t2872111280 ** get_address_of_BackButton_2() { return &___BackButton_2; }
	inline void set_BackButton_2(Button_t2872111280 * value)
	{
		___BackButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___BackButton_2, value);
	}

	inline static int32_t get_offset_of_CompleteButton_3() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___CompleteButton_3)); }
	inline Button_t2872111280 * get_CompleteButton_3() const { return ___CompleteButton_3; }
	inline Button_t2872111280 ** get_address_of_CompleteButton_3() { return &___CompleteButton_3; }
	inline void set_CompleteButton_3(Button_t2872111280 * value)
	{
		___CompleteButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___CompleteButton_3, value);
	}

	inline static int32_t get_offset_of_ActionButton_4() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___ActionButton_4)); }
	inline Button_t2872111280 * get_ActionButton_4() const { return ___ActionButton_4; }
	inline Button_t2872111280 ** get_address_of_ActionButton_4() { return &___ActionButton_4; }
	inline void set_ActionButton_4(Button_t2872111280 * value)
	{
		___ActionButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButton_4, value);
	}

	inline static int32_t get_offset_of_PlayButton_5() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___PlayButton_5)); }
	inline Button_t2872111280 * get_PlayButton_5() const { return ___PlayButton_5; }
	inline Button_t2872111280 ** get_address_of_PlayButton_5() { return &___PlayButton_5; }
	inline void set_PlayButton_5(Button_t2872111280 * value)
	{
		___PlayButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___PlayButton_5, value);
	}

	inline static int32_t get_offset_of_ConfigButton_6() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___ConfigButton_6)); }
	inline Button_t2872111280 * get_ConfigButton_6() const { return ___ConfigButton_6; }
	inline Button_t2872111280 ** get_address_of_ConfigButton_6() { return &___ConfigButton_6; }
	inline void set_ConfigButton_6(Button_t2872111280 * value)
	{
		___ConfigButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigButton_6, value);
	}

	inline static int32_t get_offset_of_ConfigCompleteButton_7() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___ConfigCompleteButton_7)); }
	inline Button_t2872111280 * get_ConfigCompleteButton_7() const { return ___ConfigCompleteButton_7; }
	inline Button_t2872111280 ** get_address_of_ConfigCompleteButton_7() { return &___ConfigCompleteButton_7; }
	inline void set_ConfigCompleteButton_7(Button_t2872111280 * value)
	{
		___ConfigCompleteButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigCompleteButton_7, value);
	}

	inline static int32_t get_offset_of_ActionConfigPanel_8() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___ActionConfigPanel_8)); }
	inline ActionConfigPanel_t3304127690 * get_ActionConfigPanel_8() const { return ___ActionConfigPanel_8; }
	inline ActionConfigPanel_t3304127690 ** get_address_of_ActionConfigPanel_8() { return &___ActionConfigPanel_8; }
	inline void set_ActionConfigPanel_8(ActionConfigPanel_t3304127690 * value)
	{
		___ActionConfigPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionConfigPanel_8, value);
	}

	inline static int32_t get_offset_of_ActionTitle_9() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___ActionTitle_9)); }
	inline GameObject_t1756533147 * get_ActionTitle_9() const { return ___ActionTitle_9; }
	inline GameObject_t1756533147 ** get_address_of_ActionTitle_9() { return &___ActionTitle_9; }
	inline void set_ActionTitle_9(GameObject_t1756533147 * value)
	{
		___ActionTitle_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActionTitle_9, value);
	}

	inline static int32_t get_offset_of_PoseTitle_10() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___PoseTitle_10)); }
	inline GameObject_t1756533147 * get_PoseTitle_10() const { return ___PoseTitle_10; }
	inline GameObject_t1756533147 ** get_address_of_PoseTitle_10() { return &___PoseTitle_10; }
	inline void set_PoseTitle_10(GameObject_t1756533147 * value)
	{
		___PoseTitle_10 = value;
		Il2CppCodeGenWriteBarrier(&___PoseTitle_10, value);
	}

	inline static int32_t get_offset_of_ActionListPanel_11() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___ActionListPanel_11)); }
	inline GameObject_t1756533147 * get_ActionListPanel_11() const { return ___ActionListPanel_11; }
	inline GameObject_t1756533147 ** get_address_of_ActionListPanel_11() { return &___ActionListPanel_11; }
	inline void set_ActionListPanel_11(GameObject_t1756533147 * value)
	{
		___ActionListPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___ActionListPanel_11, value);
	}

	inline static int32_t get_offset_of_PlayButtonGroup_12() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___PlayButtonGroup_12)); }
	inline CanvasGroup_t3296560743 * get_PlayButtonGroup_12() const { return ___PlayButtonGroup_12; }
	inline CanvasGroup_t3296560743 ** get_address_of_PlayButtonGroup_12() { return &___PlayButtonGroup_12; }
	inline void set_PlayButtonGroup_12(CanvasGroup_t3296560743 * value)
	{
		___PlayButtonGroup_12 = value;
		Il2CppCodeGenWriteBarrier(&___PlayButtonGroup_12, value);
	}

	inline static int32_t get_offset_of_ActionButtons_13() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___ActionButtons_13)); }
	inline ActionButtons_t2868854693 * get_ActionButtons_13() const { return ___ActionButtons_13; }
	inline ActionButtons_t2868854693 ** get_address_of_ActionButtons_13() { return &___ActionButtons_13; }
	inline void set_ActionButtons_13(ActionButtons_t2868854693 * value)
	{
		___ActionButtons_13 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButtons_13, value);
	}

	inline static int32_t get_offset_of_MovieObj_14() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___MovieObj_14)); }
	inline GameObject_t1756533147 * get_MovieObj_14() const { return ___MovieObj_14; }
	inline GameObject_t1756533147 ** get_address_of_MovieObj_14() { return &___MovieObj_14; }
	inline void set_MovieObj_14(GameObject_t1756533147 * value)
	{
		___MovieObj_14 = value;
		Il2CppCodeGenWriteBarrier(&___MovieObj_14, value);
	}

	inline static int32_t get_offset_of_TutorialVideoPlayer_15() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___TutorialVideoPlayer_15)); }
	inline VideoPlayer_t10059812 * get_TutorialVideoPlayer_15() const { return ___TutorialVideoPlayer_15; }
	inline VideoPlayer_t10059812 ** get_address_of_TutorialVideoPlayer_15() { return &___TutorialVideoPlayer_15; }
	inline void set_TutorialVideoPlayer_15(VideoPlayer_t10059812 * value)
	{
		___TutorialVideoPlayer_15 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialVideoPlayer_15, value);
	}

	inline static int32_t get_offset_of_TutorialTW_16() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___TutorialTW_16)); }
	inline Tween_t278478013 * get_TutorialTW_16() const { return ___TutorialTW_16; }
	inline Tween_t278478013 ** get_address_of_TutorialTW_16() { return &___TutorialTW_16; }
	inline void set_TutorialTW_16(Tween_t278478013 * value)
	{
		___TutorialTW_16 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialTW_16, value);
	}

	inline static int32_t get_offset_of_m_bFadeOut_17() { return static_cast<int32_t>(offsetof(ActionSet_t264428022, ___m_bFadeOut_17)); }
	inline bool get_m_bFadeOut_17() const { return ___m_bFadeOut_17; }
	inline bool* get_address_of_m_bFadeOut_17() { return &___m_bFadeOut_17; }
	inline void set_m_bFadeOut_17(bool value)
	{
		___m_bFadeOut_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
