﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen290330458.h"

// GoogleAnalyticsV4
struct GoogleAnalyticsV4_t198817271;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsManager
struct  AnalyticsManager_t1593654123  : public SingletonMonoBehaviour_1_t290330458
{
public:
	// GoogleAnalyticsV4 AnalyticsManager::googleAnalytics
	GoogleAnalyticsV4_t198817271 * ___googleAnalytics_3;

public:
	inline static int32_t get_offset_of_googleAnalytics_3() { return static_cast<int32_t>(offsetof(AnalyticsManager_t1593654123, ___googleAnalytics_3)); }
	inline GoogleAnalyticsV4_t198817271 * get_googleAnalytics_3() const { return ___googleAnalytics_3; }
	inline GoogleAnalyticsV4_t198817271 ** get_address_of_googleAnalytics_3() { return &___googleAnalytics_3; }
	inline void set_googleAnalytics_3(GoogleAnalyticsV4_t198817271 * value)
	{
		___googleAnalytics_3 = value;
		Il2CppCodeGenWriteBarrier(&___googleAnalytics_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
