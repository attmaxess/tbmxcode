﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen4213095483.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiAuthenticationsManager
struct  ApiAuthenticationsManager_t1221451852  : public SingletonMonoBehaviour_1_t4213095483
{
public:
	// System.String ApiAuthenticationsManager::authenticationJson
	String_t* ___authenticationJson_3;

public:
	inline static int32_t get_offset_of_authenticationJson_3() { return static_cast<int32_t>(offsetof(ApiAuthenticationsManager_t1221451852, ___authenticationJson_3)); }
	inline String_t* get_authenticationJson_3() const { return ___authenticationJson_3; }
	inline String_t** get_address_of_authenticationJson_3() { return &___authenticationJson_3; }
	inline void set_authenticationJson_3(String_t* value)
	{
		___authenticationJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___authenticationJson_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
