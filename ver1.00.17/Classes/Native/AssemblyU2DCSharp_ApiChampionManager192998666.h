﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3184642297.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiChampionManager
struct  ApiChampionManager_t192998666  : public SingletonMonoBehaviour_1_t3184642297
{
public:
	// System.String ApiChampionManager::championJson
	String_t* ___championJson_3;
	// UnityEngine.GameObject ApiChampionManager::m_CreObj
	GameObject_t1756533147 * ___m_CreObj_4;
	// UnityEngine.Transform[] ApiChampionManager::corePastsChildTra
	TransformU5BU5D_t3764228911* ___corePastsChildTra_5;
	// System.Single ApiChampionManager::childDrillerPositionX
	float ___childDrillerPositionX_6;
	// System.Single ApiChampionManager::childDrillerPositionY
	float ___childDrillerPositionY_7;
	// System.Single ApiChampionManager::childDrillerPositionZ
	float ___childDrillerPositionZ_8;
	// System.Single ApiChampionManager::childCenterRotationX
	float ___childCenterRotationX_9;
	// System.Single ApiChampionManager::childCenterRotationY
	float ___childCenterRotationY_10;
	// System.Single ApiChampionManager::childCenterRotationZ
	float ___childCenterRotationZ_11;
	// System.Single ApiChampionManager::childPartsPositionX
	float ___childPartsPositionX_12;
	// System.Single ApiChampionManager::childPartsPositionY
	float ___childPartsPositionY_13;
	// System.Single ApiChampionManager::childPartsPositionZ
	float ___childPartsPositionZ_14;
	// System.Single ApiChampionManager::childPartsRotationX
	float ___childPartsRotationX_15;
	// System.Single ApiChampionManager::childPartsRotationY
	float ___childPartsRotationY_16;
	// System.Single ApiChampionManager::childPartsRotationZ
	float ___childPartsRotationZ_17;
	// System.Single ApiChampionManager::childPartsScaleX
	float ___childPartsScaleX_18;
	// System.Single ApiChampionManager::childPartsScaleY
	float ___childPartsScaleY_19;
	// System.Single ApiChampionManager::childPartsScaleZ
	float ___childPartsScaleZ_20;
	// System.Int32 ApiChampionManager::parentPartsJointNo
	int32_t ___parentPartsJointNo_21;
	// System.Int32 ApiChampionManager::childPartsJointNo
	int32_t ___childPartsJointNo_22;
	// UnityEngine.GameObject ApiChampionManager::mobilityObject
	GameObject_t1756533147 * ___mobilityObject_23;
	// UnityEngine.GameObject ApiChampionManager::rollObj
	GameObject_t1756533147 * ___rollObj_24;
	// UnityEngine.Transform ApiChampionManager::contentRanking
	Transform_t3275118058 * ___contentRanking_25;
	// UnityEngine.GameObject ApiChampionManager::_mobilityPrefab
	GameObject_t1756533147 * ____mobilityPrefab_26;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ApiChampionManager::rankingObjList
	List_1_t1125654279 * ___rankingObjList_27;
	// System.Int32 ApiChampionManager::mobilityCnt
	int32_t ___mobilityCnt_28;
	// UnityEngine.GameObject ApiChampionManager::mobilmo
	GameObject_t1756533147 * ___mobilmo_29;
	// UnityEngine.GameObject ApiChampionManager::childPartsObj
	GameObject_t1756533147 * ___childPartsObj_30;
	// UnityEngine.GameObject ApiChampionManager::ChildCenter
	GameObject_t1756533147 * ___ChildCenter_31;
	// UnityEngine.GameObject ApiChampionManager::ChildLeap
	GameObject_t1756533147 * ___ChildLeap_32;
	// UnityEngine.GameObject ApiChampionManager::recreatedModuleObj
	GameObject_t1756533147 * ___recreatedModuleObj_33;
	// UnityEngine.GameObject ApiChampionManager::ModuleCenter
	GameObject_t1756533147 * ___ModuleCenter_34;

public:
	inline static int32_t get_offset_of_championJson_3() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___championJson_3)); }
	inline String_t* get_championJson_3() const { return ___championJson_3; }
	inline String_t** get_address_of_championJson_3() { return &___championJson_3; }
	inline void set_championJson_3(String_t* value)
	{
		___championJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___championJson_3, value);
	}

	inline static int32_t get_offset_of_m_CreObj_4() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___m_CreObj_4)); }
	inline GameObject_t1756533147 * get_m_CreObj_4() const { return ___m_CreObj_4; }
	inline GameObject_t1756533147 ** get_address_of_m_CreObj_4() { return &___m_CreObj_4; }
	inline void set_m_CreObj_4(GameObject_t1756533147 * value)
	{
		___m_CreObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_CreObj_4, value);
	}

	inline static int32_t get_offset_of_corePastsChildTra_5() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___corePastsChildTra_5)); }
	inline TransformU5BU5D_t3764228911* get_corePastsChildTra_5() const { return ___corePastsChildTra_5; }
	inline TransformU5BU5D_t3764228911** get_address_of_corePastsChildTra_5() { return &___corePastsChildTra_5; }
	inline void set_corePastsChildTra_5(TransformU5BU5D_t3764228911* value)
	{
		___corePastsChildTra_5 = value;
		Il2CppCodeGenWriteBarrier(&___corePastsChildTra_5, value);
	}

	inline static int32_t get_offset_of_childDrillerPositionX_6() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childDrillerPositionX_6)); }
	inline float get_childDrillerPositionX_6() const { return ___childDrillerPositionX_6; }
	inline float* get_address_of_childDrillerPositionX_6() { return &___childDrillerPositionX_6; }
	inline void set_childDrillerPositionX_6(float value)
	{
		___childDrillerPositionX_6 = value;
	}

	inline static int32_t get_offset_of_childDrillerPositionY_7() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childDrillerPositionY_7)); }
	inline float get_childDrillerPositionY_7() const { return ___childDrillerPositionY_7; }
	inline float* get_address_of_childDrillerPositionY_7() { return &___childDrillerPositionY_7; }
	inline void set_childDrillerPositionY_7(float value)
	{
		___childDrillerPositionY_7 = value;
	}

	inline static int32_t get_offset_of_childDrillerPositionZ_8() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childDrillerPositionZ_8)); }
	inline float get_childDrillerPositionZ_8() const { return ___childDrillerPositionZ_8; }
	inline float* get_address_of_childDrillerPositionZ_8() { return &___childDrillerPositionZ_8; }
	inline void set_childDrillerPositionZ_8(float value)
	{
		___childDrillerPositionZ_8 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationX_9() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childCenterRotationX_9)); }
	inline float get_childCenterRotationX_9() const { return ___childCenterRotationX_9; }
	inline float* get_address_of_childCenterRotationX_9() { return &___childCenterRotationX_9; }
	inline void set_childCenterRotationX_9(float value)
	{
		___childCenterRotationX_9 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationY_10() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childCenterRotationY_10)); }
	inline float get_childCenterRotationY_10() const { return ___childCenterRotationY_10; }
	inline float* get_address_of_childCenterRotationY_10() { return &___childCenterRotationY_10; }
	inline void set_childCenterRotationY_10(float value)
	{
		___childCenterRotationY_10 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationZ_11() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childCenterRotationZ_11)); }
	inline float get_childCenterRotationZ_11() const { return ___childCenterRotationZ_11; }
	inline float* get_address_of_childCenterRotationZ_11() { return &___childCenterRotationZ_11; }
	inline void set_childCenterRotationZ_11(float value)
	{
		___childCenterRotationZ_11 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionX_12() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsPositionX_12)); }
	inline float get_childPartsPositionX_12() const { return ___childPartsPositionX_12; }
	inline float* get_address_of_childPartsPositionX_12() { return &___childPartsPositionX_12; }
	inline void set_childPartsPositionX_12(float value)
	{
		___childPartsPositionX_12 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionY_13() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsPositionY_13)); }
	inline float get_childPartsPositionY_13() const { return ___childPartsPositionY_13; }
	inline float* get_address_of_childPartsPositionY_13() { return &___childPartsPositionY_13; }
	inline void set_childPartsPositionY_13(float value)
	{
		___childPartsPositionY_13 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionZ_14() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsPositionZ_14)); }
	inline float get_childPartsPositionZ_14() const { return ___childPartsPositionZ_14; }
	inline float* get_address_of_childPartsPositionZ_14() { return &___childPartsPositionZ_14; }
	inline void set_childPartsPositionZ_14(float value)
	{
		___childPartsPositionZ_14 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationX_15() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsRotationX_15)); }
	inline float get_childPartsRotationX_15() const { return ___childPartsRotationX_15; }
	inline float* get_address_of_childPartsRotationX_15() { return &___childPartsRotationX_15; }
	inline void set_childPartsRotationX_15(float value)
	{
		___childPartsRotationX_15 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationY_16() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsRotationY_16)); }
	inline float get_childPartsRotationY_16() const { return ___childPartsRotationY_16; }
	inline float* get_address_of_childPartsRotationY_16() { return &___childPartsRotationY_16; }
	inline void set_childPartsRotationY_16(float value)
	{
		___childPartsRotationY_16 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationZ_17() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsRotationZ_17)); }
	inline float get_childPartsRotationZ_17() const { return ___childPartsRotationZ_17; }
	inline float* get_address_of_childPartsRotationZ_17() { return &___childPartsRotationZ_17; }
	inline void set_childPartsRotationZ_17(float value)
	{
		___childPartsRotationZ_17 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleX_18() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsScaleX_18)); }
	inline float get_childPartsScaleX_18() const { return ___childPartsScaleX_18; }
	inline float* get_address_of_childPartsScaleX_18() { return &___childPartsScaleX_18; }
	inline void set_childPartsScaleX_18(float value)
	{
		___childPartsScaleX_18 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleY_19() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsScaleY_19)); }
	inline float get_childPartsScaleY_19() const { return ___childPartsScaleY_19; }
	inline float* get_address_of_childPartsScaleY_19() { return &___childPartsScaleY_19; }
	inline void set_childPartsScaleY_19(float value)
	{
		___childPartsScaleY_19 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleZ_20() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsScaleZ_20)); }
	inline float get_childPartsScaleZ_20() const { return ___childPartsScaleZ_20; }
	inline float* get_address_of_childPartsScaleZ_20() { return &___childPartsScaleZ_20; }
	inline void set_childPartsScaleZ_20(float value)
	{
		___childPartsScaleZ_20 = value;
	}

	inline static int32_t get_offset_of_parentPartsJointNo_21() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___parentPartsJointNo_21)); }
	inline int32_t get_parentPartsJointNo_21() const { return ___parentPartsJointNo_21; }
	inline int32_t* get_address_of_parentPartsJointNo_21() { return &___parentPartsJointNo_21; }
	inline void set_parentPartsJointNo_21(int32_t value)
	{
		___parentPartsJointNo_21 = value;
	}

	inline static int32_t get_offset_of_childPartsJointNo_22() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsJointNo_22)); }
	inline int32_t get_childPartsJointNo_22() const { return ___childPartsJointNo_22; }
	inline int32_t* get_address_of_childPartsJointNo_22() { return &___childPartsJointNo_22; }
	inline void set_childPartsJointNo_22(int32_t value)
	{
		___childPartsJointNo_22 = value;
	}

	inline static int32_t get_offset_of_mobilityObject_23() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___mobilityObject_23)); }
	inline GameObject_t1756533147 * get_mobilityObject_23() const { return ___mobilityObject_23; }
	inline GameObject_t1756533147 ** get_address_of_mobilityObject_23() { return &___mobilityObject_23; }
	inline void set_mobilityObject_23(GameObject_t1756533147 * value)
	{
		___mobilityObject_23 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityObject_23, value);
	}

	inline static int32_t get_offset_of_rollObj_24() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___rollObj_24)); }
	inline GameObject_t1756533147 * get_rollObj_24() const { return ___rollObj_24; }
	inline GameObject_t1756533147 ** get_address_of_rollObj_24() { return &___rollObj_24; }
	inline void set_rollObj_24(GameObject_t1756533147 * value)
	{
		___rollObj_24 = value;
		Il2CppCodeGenWriteBarrier(&___rollObj_24, value);
	}

	inline static int32_t get_offset_of_contentRanking_25() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___contentRanking_25)); }
	inline Transform_t3275118058 * get_contentRanking_25() const { return ___contentRanking_25; }
	inline Transform_t3275118058 ** get_address_of_contentRanking_25() { return &___contentRanking_25; }
	inline void set_contentRanking_25(Transform_t3275118058 * value)
	{
		___contentRanking_25 = value;
		Il2CppCodeGenWriteBarrier(&___contentRanking_25, value);
	}

	inline static int32_t get_offset_of__mobilityPrefab_26() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ____mobilityPrefab_26)); }
	inline GameObject_t1756533147 * get__mobilityPrefab_26() const { return ____mobilityPrefab_26; }
	inline GameObject_t1756533147 ** get_address_of__mobilityPrefab_26() { return &____mobilityPrefab_26; }
	inline void set__mobilityPrefab_26(GameObject_t1756533147 * value)
	{
		____mobilityPrefab_26 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityPrefab_26, value);
	}

	inline static int32_t get_offset_of_rankingObjList_27() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___rankingObjList_27)); }
	inline List_1_t1125654279 * get_rankingObjList_27() const { return ___rankingObjList_27; }
	inline List_1_t1125654279 ** get_address_of_rankingObjList_27() { return &___rankingObjList_27; }
	inline void set_rankingObjList_27(List_1_t1125654279 * value)
	{
		___rankingObjList_27 = value;
		Il2CppCodeGenWriteBarrier(&___rankingObjList_27, value);
	}

	inline static int32_t get_offset_of_mobilityCnt_28() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___mobilityCnt_28)); }
	inline int32_t get_mobilityCnt_28() const { return ___mobilityCnt_28; }
	inline int32_t* get_address_of_mobilityCnt_28() { return &___mobilityCnt_28; }
	inline void set_mobilityCnt_28(int32_t value)
	{
		___mobilityCnt_28 = value;
	}

	inline static int32_t get_offset_of_mobilmo_29() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___mobilmo_29)); }
	inline GameObject_t1756533147 * get_mobilmo_29() const { return ___mobilmo_29; }
	inline GameObject_t1756533147 ** get_address_of_mobilmo_29() { return &___mobilmo_29; }
	inline void set_mobilmo_29(GameObject_t1756533147 * value)
	{
		___mobilmo_29 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmo_29, value);
	}

	inline static int32_t get_offset_of_childPartsObj_30() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___childPartsObj_30)); }
	inline GameObject_t1756533147 * get_childPartsObj_30() const { return ___childPartsObj_30; }
	inline GameObject_t1756533147 ** get_address_of_childPartsObj_30() { return &___childPartsObj_30; }
	inline void set_childPartsObj_30(GameObject_t1756533147 * value)
	{
		___childPartsObj_30 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsObj_30, value);
	}

	inline static int32_t get_offset_of_ChildCenter_31() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___ChildCenter_31)); }
	inline GameObject_t1756533147 * get_ChildCenter_31() const { return ___ChildCenter_31; }
	inline GameObject_t1756533147 ** get_address_of_ChildCenter_31() { return &___ChildCenter_31; }
	inline void set_ChildCenter_31(GameObject_t1756533147 * value)
	{
		___ChildCenter_31 = value;
		Il2CppCodeGenWriteBarrier(&___ChildCenter_31, value);
	}

	inline static int32_t get_offset_of_ChildLeap_32() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___ChildLeap_32)); }
	inline GameObject_t1756533147 * get_ChildLeap_32() const { return ___ChildLeap_32; }
	inline GameObject_t1756533147 ** get_address_of_ChildLeap_32() { return &___ChildLeap_32; }
	inline void set_ChildLeap_32(GameObject_t1756533147 * value)
	{
		___ChildLeap_32 = value;
		Il2CppCodeGenWriteBarrier(&___ChildLeap_32, value);
	}

	inline static int32_t get_offset_of_recreatedModuleObj_33() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___recreatedModuleObj_33)); }
	inline GameObject_t1756533147 * get_recreatedModuleObj_33() const { return ___recreatedModuleObj_33; }
	inline GameObject_t1756533147 ** get_address_of_recreatedModuleObj_33() { return &___recreatedModuleObj_33; }
	inline void set_recreatedModuleObj_33(GameObject_t1756533147 * value)
	{
		___recreatedModuleObj_33 = value;
		Il2CppCodeGenWriteBarrier(&___recreatedModuleObj_33, value);
	}

	inline static int32_t get_offset_of_ModuleCenter_34() { return static_cast<int32_t>(offsetof(ApiChampionManager_t192998666, ___ModuleCenter_34)); }
	inline GameObject_t1756533147 * get_ModuleCenter_34() const { return ___ModuleCenter_34; }
	inline GameObject_t1756533147 ** get_address_of_ModuleCenter_34() { return &___ModuleCenter_34; }
	inline void set_ModuleCenter_34(GameObject_t1756533147 * value)
	{
		___ModuleCenter_34 = value;
		Il2CppCodeGenWriteBarrier(&___ModuleCenter_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
