﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4174688501.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato85408535.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetResultModel
struct GetResultModel_t3280185258;
// GetChampionItemModel
struct GetChampionItemModel_t353148757;
// ChampionItemModel
struct ChampionItemModel_t975870399;
// ChampionMobilityModel
struct ChampionMobilityModel_t1041611931;
// GetChampionMobilityEncountListModel
struct GetChampionMobilityEncountListModel_t3804924867;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// Mobilmo
struct Mobilmo_t370754809;
// UnityEngine.FixedJoint
struct FixedJoint_t3848069458;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GetChampionMobilityChildPartsListModel
struct GetChampionMobilityChildPartsListModel_t1181557729;
// ModuleManager
struct ModuleManager_t1065445307;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// MiniGameChampionsData
struct MiniGameChampionsData_t2446501291;
// Joint[]
struct JointU5BU5D_t171503857;
// ApiChampionManager
struct ApiChampionManager_t192998666;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiChampionManager/<RecreateChampionsMobility>c__Iterator0
struct  U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550  : public Il2CppObject
{
public:
	// System.String ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::json
	String_t* ___json_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_1;
	// GetResultModel ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<_getResultModel>__0
	GetResultModel_t3280185258 * ___U3C_getResultModelU3E__0_2;
	// GetChampionItemModel ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<_getChampionItemModel>__0
	GetChampionItemModel_t353148757 * ___U3C_getChampionItemModelU3E__0_3;
	// ChampionItemModel ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<_championItemModel>__0
	ChampionItemModel_t975870399 * ___U3C_championItemModelU3E__0_4;
	// ChampionMobilityModel ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<_championMobilityModel>__0
	ChampionMobilityModel_t1041611931 * ___U3C_championMobilityModelU3E__0_5;
	// GetChampionMobilityEncountListModel ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<_getChampionMobilityEncountListModel>__0
	GetChampionMobilityEncountListModel_t3804924867 * ___U3C_getChampionMobilityEncountListModelU3E__0_6;
	// System.Collections.Generic.List`1/Enumerator<ChampionItemModel> ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$locvar0
	Enumerator_t4174688501  ___U24locvar0_7;
	// ChampionItemModel ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<itemModel>__1
	ChampionItemModel_t975870399 * ___U3CitemModelU3E__1_8;
	// UnityEngine.Rigidbody ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<mobilmoRigid>__2
	Rigidbody_t4233889191 * ___U3CmobilmoRigidU3E__2_9;
	// Mobilmo ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<m_mobilimo>__2
	Mobilmo_t370754809 * ___U3Cm_mobilimoU3E__2_10;
	// UnityEngine.FixedJoint ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<mobilFixed>__2
	FixedJoint_t3848069458 * ___U3CmobilFixedU3E__2_11;
	// UnityEngine.GameObject ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<ChildObj>__2
	GameObject_t1756533147 * ___U3CChildObjU3E__2_12;
	// UnityEngine.Rigidbody ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<ChildObjRigid>__2
	Rigidbody_t4233889191 * ___U3CChildObjRigidU3E__2_13;
	// UnityEngine.FixedJoint ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<ChildObjFixed>__2
	FixedJoint_t3848069458 * ___U3CChildObjFixedU3E__2_14;
	// UnityEngine.GameObject ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<m_CreObj>__2
	GameObject_t1756533147 * ___U3Cm_CreObjU3E__2_15;
	// System.Collections.Generic.List`1/Enumerator<GetChampionMobilityChildPartsListModel> ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$locvar1
	Enumerator_t85408535  ___U24locvar1_16;
	// GetChampionMobilityChildPartsListModel ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<child>__3
	GetChampionMobilityChildPartsListModel_t1181557729 * ___U3CchildU3E__3_17;
	// UnityEngine.GameObject ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<ModuleLeap>__4
	GameObject_t1756533147 * ___U3CModuleLeapU3E__4_18;
	// ModuleManager ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<moduleMgr>__4
	ModuleManager_t1065445307 * ___U3CmoduleMgrU3E__4_19;
	// System.String ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<moduleJson>__4
	String_t* ___U3CmoduleJsonU3E__4_20;
	// System.Int32 ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<parentPartsJointNo>__4
	int32_t ___U3CparentPartsJointNoU3E__4_21;
	// System.Int32 ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<modulePartsCnt>__4
	int32_t ___U3CmodulePartsCntU3E__4_22;
	// System.String[] ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<partsIdIndex>__4
	StringU5BU5D_t1642385972* ___U3CpartsIdIndexU3E__4_23;
	// System.Collections.Generic.List`1<System.String> ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<partsIdList>__4
	List_1_t1398341365 * ___U3CpartsIdListU3E__4_24;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<modulePartsList>__4
	List_1_t1125654279 * ___U3CmodulePartsListU3E__4_25;
	// UnityEngine.Transform[] ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$locvar4
	TransformU5BU5D_t3764228911* ___U24locvar4_26;
	// System.Int32 ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$locvar5
	int32_t ___U24locvar5_27;
	// UnityEngine.Transform[] ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$locvar6
	TransformU5BU5D_t3764228911* ___U24locvar6_28;
	// System.Int32 ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$locvar7
	int32_t ___U24locvar7_29;
	// UnityEngine.GameObject ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<rankMobility>__2
	GameObject_t1756533147 * ___U3CrankMobilityU3E__2_30;
	// MiniGameChampionsData ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<uiText>__2
	MiniGameChampionsData_t2446501291 * ___U3CuiTextU3E__2_31;
	// System.Int32 ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<rankNo>__2
	int32_t ___U3CrankNoU3E__2_32;
	// System.String ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<resEmblemSt>__2
	String_t* ___U3CresEmblemStU3E__2_33;
	// Joint[] ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<jointParents>__2
	JointU5BU5D_t171503857* ___U3CjointParentsU3E__2_34;
	// ApiChampionManager ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$this
	ApiChampionManager_t192998666 * ___U24this_35;
	// System.Object ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$current
	Il2CppObject * ___U24current_36;
	// System.Boolean ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$disposing
	bool ___U24disposing_37;
	// System.Int32 ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::$PC
	int32_t ___U24PC_38;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___json_0)); }
	inline String_t* get_json_0() const { return ___json_0; }
	inline String_t** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(String_t* value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier(&___json_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CwwwDataU3E__0_1)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_1() const { return ___U3CwwwDataU3E__0_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_1() { return &___U3CwwwDataU3E__0_1; }
	inline void set_U3CwwwDataU3E__0_1(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3C_getResultModelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3C_getResultModelU3E__0_2)); }
	inline GetResultModel_t3280185258 * get_U3C_getResultModelU3E__0_2() const { return ___U3C_getResultModelU3E__0_2; }
	inline GetResultModel_t3280185258 ** get_address_of_U3C_getResultModelU3E__0_2() { return &___U3C_getResultModelU3E__0_2; }
	inline void set_U3C_getResultModelU3E__0_2(GetResultModel_t3280185258 * value)
	{
		___U3C_getResultModelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getResultModelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3C_getChampionItemModelU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3C_getChampionItemModelU3E__0_3)); }
	inline GetChampionItemModel_t353148757 * get_U3C_getChampionItemModelU3E__0_3() const { return ___U3C_getChampionItemModelU3E__0_3; }
	inline GetChampionItemModel_t353148757 ** get_address_of_U3C_getChampionItemModelU3E__0_3() { return &___U3C_getChampionItemModelU3E__0_3; }
	inline void set_U3C_getChampionItemModelU3E__0_3(GetChampionItemModel_t353148757 * value)
	{
		___U3C_getChampionItemModelU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getChampionItemModelU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3C_championItemModelU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3C_championItemModelU3E__0_4)); }
	inline ChampionItemModel_t975870399 * get_U3C_championItemModelU3E__0_4() const { return ___U3C_championItemModelU3E__0_4; }
	inline ChampionItemModel_t975870399 ** get_address_of_U3C_championItemModelU3E__0_4() { return &___U3C_championItemModelU3E__0_4; }
	inline void set_U3C_championItemModelU3E__0_4(ChampionItemModel_t975870399 * value)
	{
		___U3C_championItemModelU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_championItemModelU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U3C_championMobilityModelU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3C_championMobilityModelU3E__0_5)); }
	inline ChampionMobilityModel_t1041611931 * get_U3C_championMobilityModelU3E__0_5() const { return ___U3C_championMobilityModelU3E__0_5; }
	inline ChampionMobilityModel_t1041611931 ** get_address_of_U3C_championMobilityModelU3E__0_5() { return &___U3C_championMobilityModelU3E__0_5; }
	inline void set_U3C_championMobilityModelU3E__0_5(ChampionMobilityModel_t1041611931 * value)
	{
		___U3C_championMobilityModelU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_championMobilityModelU3E__0_5, value);
	}

	inline static int32_t get_offset_of_U3C_getChampionMobilityEncountListModelU3E__0_6() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3C_getChampionMobilityEncountListModelU3E__0_6)); }
	inline GetChampionMobilityEncountListModel_t3804924867 * get_U3C_getChampionMobilityEncountListModelU3E__0_6() const { return ___U3C_getChampionMobilityEncountListModelU3E__0_6; }
	inline GetChampionMobilityEncountListModel_t3804924867 ** get_address_of_U3C_getChampionMobilityEncountListModelU3E__0_6() { return &___U3C_getChampionMobilityEncountListModelU3E__0_6; }
	inline void set_U3C_getChampionMobilityEncountListModelU3E__0_6(GetChampionMobilityEncountListModel_t3804924867 * value)
	{
		___U3C_getChampionMobilityEncountListModelU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getChampionMobilityEncountListModelU3E__0_6, value);
	}

	inline static int32_t get_offset_of_U24locvar0_7() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24locvar0_7)); }
	inline Enumerator_t4174688501  get_U24locvar0_7() const { return ___U24locvar0_7; }
	inline Enumerator_t4174688501 * get_address_of_U24locvar0_7() { return &___U24locvar0_7; }
	inline void set_U24locvar0_7(Enumerator_t4174688501  value)
	{
		___U24locvar0_7 = value;
	}

	inline static int32_t get_offset_of_U3CitemModelU3E__1_8() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CitemModelU3E__1_8)); }
	inline ChampionItemModel_t975870399 * get_U3CitemModelU3E__1_8() const { return ___U3CitemModelU3E__1_8; }
	inline ChampionItemModel_t975870399 ** get_address_of_U3CitemModelU3E__1_8() { return &___U3CitemModelU3E__1_8; }
	inline void set_U3CitemModelU3E__1_8(ChampionItemModel_t975870399 * value)
	{
		___U3CitemModelU3E__1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemModelU3E__1_8, value);
	}

	inline static int32_t get_offset_of_U3CmobilmoRigidU3E__2_9() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CmobilmoRigidU3E__2_9)); }
	inline Rigidbody_t4233889191 * get_U3CmobilmoRigidU3E__2_9() const { return ___U3CmobilmoRigidU3E__2_9; }
	inline Rigidbody_t4233889191 ** get_address_of_U3CmobilmoRigidU3E__2_9() { return &___U3CmobilmoRigidU3E__2_9; }
	inline void set_U3CmobilmoRigidU3E__2_9(Rigidbody_t4233889191 * value)
	{
		___U3CmobilmoRigidU3E__2_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilmoRigidU3E__2_9, value);
	}

	inline static int32_t get_offset_of_U3Cm_mobilimoU3E__2_10() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3Cm_mobilimoU3E__2_10)); }
	inline Mobilmo_t370754809 * get_U3Cm_mobilimoU3E__2_10() const { return ___U3Cm_mobilimoU3E__2_10; }
	inline Mobilmo_t370754809 ** get_address_of_U3Cm_mobilimoU3E__2_10() { return &___U3Cm_mobilimoU3E__2_10; }
	inline void set_U3Cm_mobilimoU3E__2_10(Mobilmo_t370754809 * value)
	{
		___U3Cm_mobilimoU3E__2_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_mobilimoU3E__2_10, value);
	}

	inline static int32_t get_offset_of_U3CmobilFixedU3E__2_11() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CmobilFixedU3E__2_11)); }
	inline FixedJoint_t3848069458 * get_U3CmobilFixedU3E__2_11() const { return ___U3CmobilFixedU3E__2_11; }
	inline FixedJoint_t3848069458 ** get_address_of_U3CmobilFixedU3E__2_11() { return &___U3CmobilFixedU3E__2_11; }
	inline void set_U3CmobilFixedU3E__2_11(FixedJoint_t3848069458 * value)
	{
		___U3CmobilFixedU3E__2_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilFixedU3E__2_11, value);
	}

	inline static int32_t get_offset_of_U3CChildObjU3E__2_12() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CChildObjU3E__2_12)); }
	inline GameObject_t1756533147 * get_U3CChildObjU3E__2_12() const { return ___U3CChildObjU3E__2_12; }
	inline GameObject_t1756533147 ** get_address_of_U3CChildObjU3E__2_12() { return &___U3CChildObjU3E__2_12; }
	inline void set_U3CChildObjU3E__2_12(GameObject_t1756533147 * value)
	{
		___U3CChildObjU3E__2_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChildObjU3E__2_12, value);
	}

	inline static int32_t get_offset_of_U3CChildObjRigidU3E__2_13() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CChildObjRigidU3E__2_13)); }
	inline Rigidbody_t4233889191 * get_U3CChildObjRigidU3E__2_13() const { return ___U3CChildObjRigidU3E__2_13; }
	inline Rigidbody_t4233889191 ** get_address_of_U3CChildObjRigidU3E__2_13() { return &___U3CChildObjRigidU3E__2_13; }
	inline void set_U3CChildObjRigidU3E__2_13(Rigidbody_t4233889191 * value)
	{
		___U3CChildObjRigidU3E__2_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChildObjRigidU3E__2_13, value);
	}

	inline static int32_t get_offset_of_U3CChildObjFixedU3E__2_14() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CChildObjFixedU3E__2_14)); }
	inline FixedJoint_t3848069458 * get_U3CChildObjFixedU3E__2_14() const { return ___U3CChildObjFixedU3E__2_14; }
	inline FixedJoint_t3848069458 ** get_address_of_U3CChildObjFixedU3E__2_14() { return &___U3CChildObjFixedU3E__2_14; }
	inline void set_U3CChildObjFixedU3E__2_14(FixedJoint_t3848069458 * value)
	{
		___U3CChildObjFixedU3E__2_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChildObjFixedU3E__2_14, value);
	}

	inline static int32_t get_offset_of_U3Cm_CreObjU3E__2_15() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3Cm_CreObjU3E__2_15)); }
	inline GameObject_t1756533147 * get_U3Cm_CreObjU3E__2_15() const { return ___U3Cm_CreObjU3E__2_15; }
	inline GameObject_t1756533147 ** get_address_of_U3Cm_CreObjU3E__2_15() { return &___U3Cm_CreObjU3E__2_15; }
	inline void set_U3Cm_CreObjU3E__2_15(GameObject_t1756533147 * value)
	{
		___U3Cm_CreObjU3E__2_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_CreObjU3E__2_15, value);
	}

	inline static int32_t get_offset_of_U24locvar1_16() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24locvar1_16)); }
	inline Enumerator_t85408535  get_U24locvar1_16() const { return ___U24locvar1_16; }
	inline Enumerator_t85408535 * get_address_of_U24locvar1_16() { return &___U24locvar1_16; }
	inline void set_U24locvar1_16(Enumerator_t85408535  value)
	{
		___U24locvar1_16 = value;
	}

	inline static int32_t get_offset_of_U3CchildU3E__3_17() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CchildU3E__3_17)); }
	inline GetChampionMobilityChildPartsListModel_t1181557729 * get_U3CchildU3E__3_17() const { return ___U3CchildU3E__3_17; }
	inline GetChampionMobilityChildPartsListModel_t1181557729 ** get_address_of_U3CchildU3E__3_17() { return &___U3CchildU3E__3_17; }
	inline void set_U3CchildU3E__3_17(GetChampionMobilityChildPartsListModel_t1181557729 * value)
	{
		___U3CchildU3E__3_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildU3E__3_17, value);
	}

	inline static int32_t get_offset_of_U3CModuleLeapU3E__4_18() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CModuleLeapU3E__4_18)); }
	inline GameObject_t1756533147 * get_U3CModuleLeapU3E__4_18() const { return ___U3CModuleLeapU3E__4_18; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleLeapU3E__4_18() { return &___U3CModuleLeapU3E__4_18; }
	inline void set_U3CModuleLeapU3E__4_18(GameObject_t1756533147 * value)
	{
		___U3CModuleLeapU3E__4_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleLeapU3E__4_18, value);
	}

	inline static int32_t get_offset_of_U3CmoduleMgrU3E__4_19() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CmoduleMgrU3E__4_19)); }
	inline ModuleManager_t1065445307 * get_U3CmoduleMgrU3E__4_19() const { return ___U3CmoduleMgrU3E__4_19; }
	inline ModuleManager_t1065445307 ** get_address_of_U3CmoduleMgrU3E__4_19() { return &___U3CmoduleMgrU3E__4_19; }
	inline void set_U3CmoduleMgrU3E__4_19(ModuleManager_t1065445307 * value)
	{
		___U3CmoduleMgrU3E__4_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleMgrU3E__4_19, value);
	}

	inline static int32_t get_offset_of_U3CmoduleJsonU3E__4_20() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CmoduleJsonU3E__4_20)); }
	inline String_t* get_U3CmoduleJsonU3E__4_20() const { return ___U3CmoduleJsonU3E__4_20; }
	inline String_t** get_address_of_U3CmoduleJsonU3E__4_20() { return &___U3CmoduleJsonU3E__4_20; }
	inline void set_U3CmoduleJsonU3E__4_20(String_t* value)
	{
		___U3CmoduleJsonU3E__4_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleJsonU3E__4_20, value);
	}

	inline static int32_t get_offset_of_U3CparentPartsJointNoU3E__4_21() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CparentPartsJointNoU3E__4_21)); }
	inline int32_t get_U3CparentPartsJointNoU3E__4_21() const { return ___U3CparentPartsJointNoU3E__4_21; }
	inline int32_t* get_address_of_U3CparentPartsJointNoU3E__4_21() { return &___U3CparentPartsJointNoU3E__4_21; }
	inline void set_U3CparentPartsJointNoU3E__4_21(int32_t value)
	{
		___U3CparentPartsJointNoU3E__4_21 = value;
	}

	inline static int32_t get_offset_of_U3CmodulePartsCntU3E__4_22() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CmodulePartsCntU3E__4_22)); }
	inline int32_t get_U3CmodulePartsCntU3E__4_22() const { return ___U3CmodulePartsCntU3E__4_22; }
	inline int32_t* get_address_of_U3CmodulePartsCntU3E__4_22() { return &___U3CmodulePartsCntU3E__4_22; }
	inline void set_U3CmodulePartsCntU3E__4_22(int32_t value)
	{
		___U3CmodulePartsCntU3E__4_22 = value;
	}

	inline static int32_t get_offset_of_U3CpartsIdIndexU3E__4_23() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CpartsIdIndexU3E__4_23)); }
	inline StringU5BU5D_t1642385972* get_U3CpartsIdIndexU3E__4_23() const { return ___U3CpartsIdIndexU3E__4_23; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CpartsIdIndexU3E__4_23() { return &___U3CpartsIdIndexU3E__4_23; }
	inline void set_U3CpartsIdIndexU3E__4_23(StringU5BU5D_t1642385972* value)
	{
		___U3CpartsIdIndexU3E__4_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdIndexU3E__4_23, value);
	}

	inline static int32_t get_offset_of_U3CpartsIdListU3E__4_24() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CpartsIdListU3E__4_24)); }
	inline List_1_t1398341365 * get_U3CpartsIdListU3E__4_24() const { return ___U3CpartsIdListU3E__4_24; }
	inline List_1_t1398341365 ** get_address_of_U3CpartsIdListU3E__4_24() { return &___U3CpartsIdListU3E__4_24; }
	inline void set_U3CpartsIdListU3E__4_24(List_1_t1398341365 * value)
	{
		___U3CpartsIdListU3E__4_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdListU3E__4_24, value);
	}

	inline static int32_t get_offset_of_U3CmodulePartsListU3E__4_25() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CmodulePartsListU3E__4_25)); }
	inline List_1_t1125654279 * get_U3CmodulePartsListU3E__4_25() const { return ___U3CmodulePartsListU3E__4_25; }
	inline List_1_t1125654279 ** get_address_of_U3CmodulePartsListU3E__4_25() { return &___U3CmodulePartsListU3E__4_25; }
	inline void set_U3CmodulePartsListU3E__4_25(List_1_t1125654279 * value)
	{
		___U3CmodulePartsListU3E__4_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmodulePartsListU3E__4_25, value);
	}

	inline static int32_t get_offset_of_U24locvar4_26() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24locvar4_26)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar4_26() const { return ___U24locvar4_26; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar4_26() { return &___U24locvar4_26; }
	inline void set_U24locvar4_26(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar4_26 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar4_26, value);
	}

	inline static int32_t get_offset_of_U24locvar5_27() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24locvar5_27)); }
	inline int32_t get_U24locvar5_27() const { return ___U24locvar5_27; }
	inline int32_t* get_address_of_U24locvar5_27() { return &___U24locvar5_27; }
	inline void set_U24locvar5_27(int32_t value)
	{
		___U24locvar5_27 = value;
	}

	inline static int32_t get_offset_of_U24locvar6_28() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24locvar6_28)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar6_28() const { return ___U24locvar6_28; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar6_28() { return &___U24locvar6_28; }
	inline void set_U24locvar6_28(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar6_28 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar6_28, value);
	}

	inline static int32_t get_offset_of_U24locvar7_29() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24locvar7_29)); }
	inline int32_t get_U24locvar7_29() const { return ___U24locvar7_29; }
	inline int32_t* get_address_of_U24locvar7_29() { return &___U24locvar7_29; }
	inline void set_U24locvar7_29(int32_t value)
	{
		___U24locvar7_29 = value;
	}

	inline static int32_t get_offset_of_U3CrankMobilityU3E__2_30() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CrankMobilityU3E__2_30)); }
	inline GameObject_t1756533147 * get_U3CrankMobilityU3E__2_30() const { return ___U3CrankMobilityU3E__2_30; }
	inline GameObject_t1756533147 ** get_address_of_U3CrankMobilityU3E__2_30() { return &___U3CrankMobilityU3E__2_30; }
	inline void set_U3CrankMobilityU3E__2_30(GameObject_t1756533147 * value)
	{
		___U3CrankMobilityU3E__2_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrankMobilityU3E__2_30, value);
	}

	inline static int32_t get_offset_of_U3CuiTextU3E__2_31() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CuiTextU3E__2_31)); }
	inline MiniGameChampionsData_t2446501291 * get_U3CuiTextU3E__2_31() const { return ___U3CuiTextU3E__2_31; }
	inline MiniGameChampionsData_t2446501291 ** get_address_of_U3CuiTextU3E__2_31() { return &___U3CuiTextU3E__2_31; }
	inline void set_U3CuiTextU3E__2_31(MiniGameChampionsData_t2446501291 * value)
	{
		___U3CuiTextU3E__2_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuiTextU3E__2_31, value);
	}

	inline static int32_t get_offset_of_U3CrankNoU3E__2_32() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CrankNoU3E__2_32)); }
	inline int32_t get_U3CrankNoU3E__2_32() const { return ___U3CrankNoU3E__2_32; }
	inline int32_t* get_address_of_U3CrankNoU3E__2_32() { return &___U3CrankNoU3E__2_32; }
	inline void set_U3CrankNoU3E__2_32(int32_t value)
	{
		___U3CrankNoU3E__2_32 = value;
	}

	inline static int32_t get_offset_of_U3CresEmblemStU3E__2_33() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CresEmblemStU3E__2_33)); }
	inline String_t* get_U3CresEmblemStU3E__2_33() const { return ___U3CresEmblemStU3E__2_33; }
	inline String_t** get_address_of_U3CresEmblemStU3E__2_33() { return &___U3CresEmblemStU3E__2_33; }
	inline void set_U3CresEmblemStU3E__2_33(String_t* value)
	{
		___U3CresEmblemStU3E__2_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresEmblemStU3E__2_33, value);
	}

	inline static int32_t get_offset_of_U3CjointParentsU3E__2_34() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U3CjointParentsU3E__2_34)); }
	inline JointU5BU5D_t171503857* get_U3CjointParentsU3E__2_34() const { return ___U3CjointParentsU3E__2_34; }
	inline JointU5BU5D_t171503857** get_address_of_U3CjointParentsU3E__2_34() { return &___U3CjointParentsU3E__2_34; }
	inline void set_U3CjointParentsU3E__2_34(JointU5BU5D_t171503857* value)
	{
		___U3CjointParentsU3E__2_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjointParentsU3E__2_34, value);
	}

	inline static int32_t get_offset_of_U24this_35() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24this_35)); }
	inline ApiChampionManager_t192998666 * get_U24this_35() const { return ___U24this_35; }
	inline ApiChampionManager_t192998666 ** get_address_of_U24this_35() { return &___U24this_35; }
	inline void set_U24this_35(ApiChampionManager_t192998666 * value)
	{
		___U24this_35 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_35, value);
	}

	inline static int32_t get_offset_of_U24current_36() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24current_36)); }
	inline Il2CppObject * get_U24current_36() const { return ___U24current_36; }
	inline Il2CppObject ** get_address_of_U24current_36() { return &___U24current_36; }
	inline void set_U24current_36(Il2CppObject * value)
	{
		___U24current_36 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_36, value);
	}

	inline static int32_t get_offset_of_U24disposing_37() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24disposing_37)); }
	inline bool get_U24disposing_37() const { return ___U24disposing_37; }
	inline bool* get_address_of_U24disposing_37() { return &___U24disposing_37; }
	inline void set_U24disposing_37(bool value)
	{
		___U24disposing_37 = value;
	}

	inline static int32_t get_offset_of_U24PC_38() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550, ___U24PC_38)); }
	inline int32_t get_U24PC_38() const { return ___U24PC_38; }
	inline int32_t* get_address_of_U24PC_38() { return &___U24PC_38; }
	inline void set_U24PC_38(int32_t value)
	{
		___U24PC_38 = value;
	}
};

struct U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550_StaticFields
{
public:
	// System.Func`1<System.Boolean> ApiChampionManager/<RecreateChampionsMobility>c__Iterator0::<>f__am$cache0
	Func_1_t1485000104 * ___U3CU3Ef__amU24cache0_39;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_39() { return static_cast<int32_t>(offsetof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550_StaticFields, ___U3CU3Ef__amU24cache0_39)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__amU24cache0_39() const { return ___U3CU3Ef__amU24cache0_39; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__amU24cache0_39() { return &___U3CU3Ef__amU24cache0_39; }
	inline void set_U3CU3Ef__amU24cache0_39(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__amU24cache0_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
