﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2658155524.h"

// System.String
struct String_t;
// Toast
struct Toast_t3649705739;
// ContactPanel
struct ContactPanel_t2408947310;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiContactManager
struct  ApiContactManager_t3961479189  : public SingletonMonoBehaviour_1_t2658155524
{
public:
	// System.String ApiContactManager::contactJson
	String_t* ___contactJson_3;
	// Toast ApiContactManager::SendToToast
	Toast_t3649705739 * ___SendToToast_4;
	// System.Boolean ApiContactManager::isSended
	bool ___isSended_5;
	// ContactPanel ApiContactManager::m_contactPanel
	ContactPanel_t2408947310 * ___m_contactPanel_6;

public:
	inline static int32_t get_offset_of_contactJson_3() { return static_cast<int32_t>(offsetof(ApiContactManager_t3961479189, ___contactJson_3)); }
	inline String_t* get_contactJson_3() const { return ___contactJson_3; }
	inline String_t** get_address_of_contactJson_3() { return &___contactJson_3; }
	inline void set_contactJson_3(String_t* value)
	{
		___contactJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___contactJson_3, value);
	}

	inline static int32_t get_offset_of_SendToToast_4() { return static_cast<int32_t>(offsetof(ApiContactManager_t3961479189, ___SendToToast_4)); }
	inline Toast_t3649705739 * get_SendToToast_4() const { return ___SendToToast_4; }
	inline Toast_t3649705739 ** get_address_of_SendToToast_4() { return &___SendToToast_4; }
	inline void set_SendToToast_4(Toast_t3649705739 * value)
	{
		___SendToToast_4 = value;
		Il2CppCodeGenWriteBarrier(&___SendToToast_4, value);
	}

	inline static int32_t get_offset_of_isSended_5() { return static_cast<int32_t>(offsetof(ApiContactManager_t3961479189, ___isSended_5)); }
	inline bool get_isSended_5() const { return ___isSended_5; }
	inline bool* get_address_of_isSended_5() { return &___isSended_5; }
	inline void set_isSended_5(bool value)
	{
		___isSended_5 = value;
	}

	inline static int32_t get_offset_of_m_contactPanel_6() { return static_cast<int32_t>(offsetof(ApiContactManager_t3961479189, ___m_contactPanel_6)); }
	inline ContactPanel_t2408947310 * get_m_contactPanel_6() const { return ___m_contactPanel_6; }
	inline ContactPanel_t2408947310 ** get_address_of_m_contactPanel_6() { return &___m_contactPanel_6; }
	inline void set_m_contactPanel_6(ContactPanel_t2408947310 * value)
	{
		___m_contactPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_contactPanel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
