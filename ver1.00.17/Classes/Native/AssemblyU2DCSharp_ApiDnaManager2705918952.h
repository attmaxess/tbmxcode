﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1402595287.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GetDnaChildrenListModel>
struct List_1_t2348066257;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// MobilmoDnaController
struct MobilmoDnaController_t1006271200;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiDnaManager
struct  ApiDnaManager_t2705918952  : public SingletonMonoBehaviour_1_t1402595287
{
public:
	// System.String ApiDnaManager::test_dna_json
	String_t* ___test_dna_json_3;
	// System.Collections.Generic.List`1<GetDnaChildrenListModel> ApiDnaManager::_dnaChildrenListData
	List_1_t2348066257 * ____dnaChildrenListData_4;
	// UnityEngine.Transform ApiDnaManager::_parentMobilityTransform
	Transform_t3275118058 * ____parentMobilityTransform_5;
	// UnityEngine.Transform ApiDnaManager::_myMobilityTransform
	Transform_t3275118058 * ____myMobilityTransform_6;
	// UnityEngine.Transform ApiDnaManager::contentChild
	Transform_t3275118058 * ___contentChild_7;
	// UnityEngine.GameObject ApiDnaManager::_mobilityPrefab
	GameObject_t1756533147 * ____mobilityPrefab_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ApiDnaManager::childItemObjList
	List_1_t1125654279 * ___childItemObjList_9;
	// UnityEngine.GameObject ApiDnaManager::myMobility
	GameObject_t1756533147 * ___myMobility_10;
	// UnityEngine.GameObject ApiDnaManager::mobilityParent
	GameObject_t1756533147 * ___mobilityParent_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ApiDnaManager::dnaMobilmoObjList
	List_1_t1125654279 * ___dnaMobilmoObjList_12;
	// MobilmoDnaController ApiDnaManager::m_mobilmoDnaController
	MobilmoDnaController_t1006271200 * ___m_mobilmoDnaController_13;
	// UnityEngine.UI.Text[] ApiDnaManager::dnaPanelText
	TextU5BU5D_t4216439300* ___dnaPanelText_14;

public:
	inline static int32_t get_offset_of_test_dna_json_3() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___test_dna_json_3)); }
	inline String_t* get_test_dna_json_3() const { return ___test_dna_json_3; }
	inline String_t** get_address_of_test_dna_json_3() { return &___test_dna_json_3; }
	inline void set_test_dna_json_3(String_t* value)
	{
		___test_dna_json_3 = value;
		Il2CppCodeGenWriteBarrier(&___test_dna_json_3, value);
	}

	inline static int32_t get_offset_of__dnaChildrenListData_4() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ____dnaChildrenListData_4)); }
	inline List_1_t2348066257 * get__dnaChildrenListData_4() const { return ____dnaChildrenListData_4; }
	inline List_1_t2348066257 ** get_address_of__dnaChildrenListData_4() { return &____dnaChildrenListData_4; }
	inline void set__dnaChildrenListData_4(List_1_t2348066257 * value)
	{
		____dnaChildrenListData_4 = value;
		Il2CppCodeGenWriteBarrier(&____dnaChildrenListData_4, value);
	}

	inline static int32_t get_offset_of__parentMobilityTransform_5() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ____parentMobilityTransform_5)); }
	inline Transform_t3275118058 * get__parentMobilityTransform_5() const { return ____parentMobilityTransform_5; }
	inline Transform_t3275118058 ** get_address_of__parentMobilityTransform_5() { return &____parentMobilityTransform_5; }
	inline void set__parentMobilityTransform_5(Transform_t3275118058 * value)
	{
		____parentMobilityTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&____parentMobilityTransform_5, value);
	}

	inline static int32_t get_offset_of__myMobilityTransform_6() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ____myMobilityTransform_6)); }
	inline Transform_t3275118058 * get__myMobilityTransform_6() const { return ____myMobilityTransform_6; }
	inline Transform_t3275118058 ** get_address_of__myMobilityTransform_6() { return &____myMobilityTransform_6; }
	inline void set__myMobilityTransform_6(Transform_t3275118058 * value)
	{
		____myMobilityTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&____myMobilityTransform_6, value);
	}

	inline static int32_t get_offset_of_contentChild_7() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___contentChild_7)); }
	inline Transform_t3275118058 * get_contentChild_7() const { return ___contentChild_7; }
	inline Transform_t3275118058 ** get_address_of_contentChild_7() { return &___contentChild_7; }
	inline void set_contentChild_7(Transform_t3275118058 * value)
	{
		___contentChild_7 = value;
		Il2CppCodeGenWriteBarrier(&___contentChild_7, value);
	}

	inline static int32_t get_offset_of__mobilityPrefab_8() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ____mobilityPrefab_8)); }
	inline GameObject_t1756533147 * get__mobilityPrefab_8() const { return ____mobilityPrefab_8; }
	inline GameObject_t1756533147 ** get_address_of__mobilityPrefab_8() { return &____mobilityPrefab_8; }
	inline void set__mobilityPrefab_8(GameObject_t1756533147 * value)
	{
		____mobilityPrefab_8 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityPrefab_8, value);
	}

	inline static int32_t get_offset_of_childItemObjList_9() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___childItemObjList_9)); }
	inline List_1_t1125654279 * get_childItemObjList_9() const { return ___childItemObjList_9; }
	inline List_1_t1125654279 ** get_address_of_childItemObjList_9() { return &___childItemObjList_9; }
	inline void set_childItemObjList_9(List_1_t1125654279 * value)
	{
		___childItemObjList_9 = value;
		Il2CppCodeGenWriteBarrier(&___childItemObjList_9, value);
	}

	inline static int32_t get_offset_of_myMobility_10() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___myMobility_10)); }
	inline GameObject_t1756533147 * get_myMobility_10() const { return ___myMobility_10; }
	inline GameObject_t1756533147 ** get_address_of_myMobility_10() { return &___myMobility_10; }
	inline void set_myMobility_10(GameObject_t1756533147 * value)
	{
		___myMobility_10 = value;
		Il2CppCodeGenWriteBarrier(&___myMobility_10, value);
	}

	inline static int32_t get_offset_of_mobilityParent_11() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___mobilityParent_11)); }
	inline GameObject_t1756533147 * get_mobilityParent_11() const { return ___mobilityParent_11; }
	inline GameObject_t1756533147 ** get_address_of_mobilityParent_11() { return &___mobilityParent_11; }
	inline void set_mobilityParent_11(GameObject_t1756533147 * value)
	{
		___mobilityParent_11 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityParent_11, value);
	}

	inline static int32_t get_offset_of_dnaMobilmoObjList_12() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___dnaMobilmoObjList_12)); }
	inline List_1_t1125654279 * get_dnaMobilmoObjList_12() const { return ___dnaMobilmoObjList_12; }
	inline List_1_t1125654279 ** get_address_of_dnaMobilmoObjList_12() { return &___dnaMobilmoObjList_12; }
	inline void set_dnaMobilmoObjList_12(List_1_t1125654279 * value)
	{
		___dnaMobilmoObjList_12 = value;
		Il2CppCodeGenWriteBarrier(&___dnaMobilmoObjList_12, value);
	}

	inline static int32_t get_offset_of_m_mobilmoDnaController_13() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___m_mobilmoDnaController_13)); }
	inline MobilmoDnaController_t1006271200 * get_m_mobilmoDnaController_13() const { return ___m_mobilmoDnaController_13; }
	inline MobilmoDnaController_t1006271200 ** get_address_of_m_mobilmoDnaController_13() { return &___m_mobilmoDnaController_13; }
	inline void set_m_mobilmoDnaController_13(MobilmoDnaController_t1006271200 * value)
	{
		___m_mobilmoDnaController_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_mobilmoDnaController_13, value);
	}

	inline static int32_t get_offset_of_dnaPanelText_14() { return static_cast<int32_t>(offsetof(ApiDnaManager_t2705918952, ___dnaPanelText_14)); }
	inline TextU5BU5D_t4216439300* get_dnaPanelText_14() const { return ___dnaPanelText_14; }
	inline TextU5BU5D_t4216439300** get_address_of_dnaPanelText_14() { return &___dnaPanelText_14; }
	inline void set_dnaPanelText_14(TextU5BU5D_t4216439300* value)
	{
		___dnaPanelText_14 = value;
		Il2CppCodeGenWriteBarrier(&___dnaPanelText_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
