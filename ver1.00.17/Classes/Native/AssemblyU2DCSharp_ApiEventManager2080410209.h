﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen777086544.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiEventManager
struct  ApiEventManager_t2080410209  : public SingletonMonoBehaviour_1_t777086544
{
public:
	// System.String ApiEventManager::encountJson
	String_t* ___encountJson_3;
	// System.String ApiEventManager::eventhistroyJson
	String_t* ___eventhistroyJson_4;
	// System.String ApiEventManager::rank
	String_t* ___rank_5;
	// System.Int32 ApiEventManager::rankupPoint
	int32_t ___rankupPoint_6;
	// System.Int32 ApiEventManager::prizedMasterId
	int32_t ___prizedMasterId_7;

public:
	inline static int32_t get_offset_of_encountJson_3() { return static_cast<int32_t>(offsetof(ApiEventManager_t2080410209, ___encountJson_3)); }
	inline String_t* get_encountJson_3() const { return ___encountJson_3; }
	inline String_t** get_address_of_encountJson_3() { return &___encountJson_3; }
	inline void set_encountJson_3(String_t* value)
	{
		___encountJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___encountJson_3, value);
	}

	inline static int32_t get_offset_of_eventhistroyJson_4() { return static_cast<int32_t>(offsetof(ApiEventManager_t2080410209, ___eventhistroyJson_4)); }
	inline String_t* get_eventhistroyJson_4() const { return ___eventhistroyJson_4; }
	inline String_t** get_address_of_eventhistroyJson_4() { return &___eventhistroyJson_4; }
	inline void set_eventhistroyJson_4(String_t* value)
	{
		___eventhistroyJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___eventhistroyJson_4, value);
	}

	inline static int32_t get_offset_of_rank_5() { return static_cast<int32_t>(offsetof(ApiEventManager_t2080410209, ___rank_5)); }
	inline String_t* get_rank_5() const { return ___rank_5; }
	inline String_t** get_address_of_rank_5() { return &___rank_5; }
	inline void set_rank_5(String_t* value)
	{
		___rank_5 = value;
		Il2CppCodeGenWriteBarrier(&___rank_5, value);
	}

	inline static int32_t get_offset_of_rankupPoint_6() { return static_cast<int32_t>(offsetof(ApiEventManager_t2080410209, ___rankupPoint_6)); }
	inline int32_t get_rankupPoint_6() const { return ___rankupPoint_6; }
	inline int32_t* get_address_of_rankupPoint_6() { return &___rankupPoint_6; }
	inline void set_rankupPoint_6(int32_t value)
	{
		___rankupPoint_6 = value;
	}

	inline static int32_t get_offset_of_prizedMasterId_7() { return static_cast<int32_t>(offsetof(ApiEventManager_t2080410209, ___prizedMasterId_7)); }
	inline int32_t get_prizedMasterId_7() const { return ___prizedMasterId_7; }
	inline int32_t* get_address_of_prizedMasterId_7() { return &___prizedMasterId_7; }
	inline void set_prizedMasterId_7(int32_t value)
	{
		___prizedMasterId_7 = value;
	}
};

struct ApiEventManager_t2080410209_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> ApiEventManager::<>f__switch$map1
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_8() { return static_cast<int32_t>(offsetof(ApiEventManager_t2080410209_StaticFields, ___U3CU3Ef__switchU24map1_8)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1_8() const { return ___U3CU3Ef__switchU24map1_8; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1_8() { return &___U3CU3Ef__switchU24map1_8; }
	inline void set_U3CU3Ef__switchU24map1_8(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
