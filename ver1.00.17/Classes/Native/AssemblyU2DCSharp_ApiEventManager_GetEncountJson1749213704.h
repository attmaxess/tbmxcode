﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiEventManager/GetEncountJson
struct  GetEncountJson_t1749213704  : public Il2CppObject
{
public:
	// System.Int32 ApiEventManager/GetEncountJson::userId
	int32_t ___userId_0;
	// System.Int32 ApiEventManager/GetEncountJson::encountMobilityId
	int32_t ___encountMobilityId_1;

public:
	inline static int32_t get_offset_of_userId_0() { return static_cast<int32_t>(offsetof(GetEncountJson_t1749213704, ___userId_0)); }
	inline int32_t get_userId_0() const { return ___userId_0; }
	inline int32_t* get_address_of_userId_0() { return &___userId_0; }
	inline void set_userId_0(int32_t value)
	{
		___userId_0 = value;
	}

	inline static int32_t get_offset_of_encountMobilityId_1() { return static_cast<int32_t>(offsetof(GetEncountJson_t1749213704, ___encountMobilityId_1)); }
	inline int32_t get_encountMobilityId_1() const { return ___encountMobilityId_1; }
	inline int32_t* get_address_of_encountMobilityId_1() { return &___encountMobilityId_1; }
	inline void set_encountMobilityId_1(int32_t value)
	{
		___encountMobilityId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
