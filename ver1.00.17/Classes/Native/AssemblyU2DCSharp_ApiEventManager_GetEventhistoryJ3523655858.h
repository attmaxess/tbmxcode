﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiEventManager/GetEventhistoryJson
struct  GetEventhistoryJson_t3523655858  : public Il2CppObject
{
public:
	// System.Int32 ApiEventManager/GetEventhistoryJson::mobilityId
	int32_t ___mobilityId_0;
	// System.Int32 ApiEventManager/GetEventhistoryJson::point
	int32_t ___point_1;
	// System.Single ApiEventManager/GetEventhistoryJson::time
	float ___time_2;
	// System.Single ApiEventManager/GetEventhistoryJson::score
	float ___score_3;
	// System.Int32 ApiEventManager/GetEventhistoryJson::eventId
	int32_t ___eventId_4;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t3523655858, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}

	inline static int32_t get_offset_of_point_1() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t3523655858, ___point_1)); }
	inline int32_t get_point_1() const { return ___point_1; }
	inline int32_t* get_address_of_point_1() { return &___point_1; }
	inline void set_point_1(int32_t value)
	{
		___point_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t3523655858, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_score_3() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t3523655858, ___score_3)); }
	inline float get_score_3() const { return ___score_3; }
	inline float* get_address_of_score_3() { return &___score_3; }
	inline void set_score_3(float value)
	{
		___score_3 = value;
	}

	inline static int32_t get_offset_of_eventId_4() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t3523655858, ___eventId_4)); }
	inline int32_t get_eventId_4() const { return ___eventId_4; }
	inline int32_t* get_address_of_eventId_4() { return &___eventId_4; }
	inline void set_eventId_4(int32_t value)
	{
		___eventId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
