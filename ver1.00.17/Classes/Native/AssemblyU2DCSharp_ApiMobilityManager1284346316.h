﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen4275989947.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<ApiMobilityManager/GetChildPartsList>
struct List_1_t1785551985;
// System.Collections.Generic.Dictionary`2<System.String,ApiMobilityManager/GetChildPartsList>
struct Dictionary_2_t36242819;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiMobilityManager
struct  ApiMobilityManager_t1284346316  : public SingletonMonoBehaviour_1_t4275989947
{
public:
	// System.String ApiMobilityManager::mobilityDelJson
	String_t* ___mobilityDelJson_3;
	// System.String ApiMobilityManager::mobilityNewJson
	String_t* ___mobilityNewJson_4;
	// System.String ApiMobilityManager::mobilityUpdateJson
	String_t* ___mobilityUpdateJson_5;
	// System.String ApiMobilityManager::mobilityPrizeJson
	String_t* ___mobilityPrizeJson_6;
	// System.String ApiMobilityManager::myMobilityJson
	String_t* ___myMobilityJson_7;
	// System.Int32 ApiMobilityManager::createNewMobilityId
	int32_t ___createNewMobilityId_8;
	// System.String ApiMobilityManager::mobilityName
	String_t* ___mobilityName_9;
	// System.Boolean ApiMobilityManager::getAction
	bool ___getAction_10;
	// System.Boolean ApiMobilityManager::getUser
	bool ___getUser_11;
	// System.Int32 ApiMobilityManager::corePartsColorId
	int32_t ___corePartsColorId_12;
	// System.Collections.Generic.List`1<ApiMobilityManager/GetChildPartsList> ApiMobilityManager::newChildPartslist
	List_1_t1785551985 * ___newChildPartslist_13;
	// System.Collections.Generic.Dictionary`2<System.String,ApiMobilityManager/GetChildPartsList> ApiMobilityManager::newMobilityPartsList
	Dictionary_2_t36242819 * ___newMobilityPartsList_14;
	// System.Collections.Generic.List`1<ApiMobilityManager/GetChildPartsList> ApiMobilityManager::_mobilmoValues
	List_1_t1785551985 * ____mobilmoValues_15;

public:
	inline static int32_t get_offset_of_mobilityDelJson_3() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___mobilityDelJson_3)); }
	inline String_t* get_mobilityDelJson_3() const { return ___mobilityDelJson_3; }
	inline String_t** get_address_of_mobilityDelJson_3() { return &___mobilityDelJson_3; }
	inline void set_mobilityDelJson_3(String_t* value)
	{
		___mobilityDelJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityDelJson_3, value);
	}

	inline static int32_t get_offset_of_mobilityNewJson_4() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___mobilityNewJson_4)); }
	inline String_t* get_mobilityNewJson_4() const { return ___mobilityNewJson_4; }
	inline String_t** get_address_of_mobilityNewJson_4() { return &___mobilityNewJson_4; }
	inline void set_mobilityNewJson_4(String_t* value)
	{
		___mobilityNewJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityNewJson_4, value);
	}

	inline static int32_t get_offset_of_mobilityUpdateJson_5() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___mobilityUpdateJson_5)); }
	inline String_t* get_mobilityUpdateJson_5() const { return ___mobilityUpdateJson_5; }
	inline String_t** get_address_of_mobilityUpdateJson_5() { return &___mobilityUpdateJson_5; }
	inline void set_mobilityUpdateJson_5(String_t* value)
	{
		___mobilityUpdateJson_5 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityUpdateJson_5, value);
	}

	inline static int32_t get_offset_of_mobilityPrizeJson_6() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___mobilityPrizeJson_6)); }
	inline String_t* get_mobilityPrizeJson_6() const { return ___mobilityPrizeJson_6; }
	inline String_t** get_address_of_mobilityPrizeJson_6() { return &___mobilityPrizeJson_6; }
	inline void set_mobilityPrizeJson_6(String_t* value)
	{
		___mobilityPrizeJson_6 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityPrizeJson_6, value);
	}

	inline static int32_t get_offset_of_myMobilityJson_7() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___myMobilityJson_7)); }
	inline String_t* get_myMobilityJson_7() const { return ___myMobilityJson_7; }
	inline String_t** get_address_of_myMobilityJson_7() { return &___myMobilityJson_7; }
	inline void set_myMobilityJson_7(String_t* value)
	{
		___myMobilityJson_7 = value;
		Il2CppCodeGenWriteBarrier(&___myMobilityJson_7, value);
	}

	inline static int32_t get_offset_of_createNewMobilityId_8() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___createNewMobilityId_8)); }
	inline int32_t get_createNewMobilityId_8() const { return ___createNewMobilityId_8; }
	inline int32_t* get_address_of_createNewMobilityId_8() { return &___createNewMobilityId_8; }
	inline void set_createNewMobilityId_8(int32_t value)
	{
		___createNewMobilityId_8 = value;
	}

	inline static int32_t get_offset_of_mobilityName_9() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___mobilityName_9)); }
	inline String_t* get_mobilityName_9() const { return ___mobilityName_9; }
	inline String_t** get_address_of_mobilityName_9() { return &___mobilityName_9; }
	inline void set_mobilityName_9(String_t* value)
	{
		___mobilityName_9 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityName_9, value);
	}

	inline static int32_t get_offset_of_getAction_10() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___getAction_10)); }
	inline bool get_getAction_10() const { return ___getAction_10; }
	inline bool* get_address_of_getAction_10() { return &___getAction_10; }
	inline void set_getAction_10(bool value)
	{
		___getAction_10 = value;
	}

	inline static int32_t get_offset_of_getUser_11() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___getUser_11)); }
	inline bool get_getUser_11() const { return ___getUser_11; }
	inline bool* get_address_of_getUser_11() { return &___getUser_11; }
	inline void set_getUser_11(bool value)
	{
		___getUser_11 = value;
	}

	inline static int32_t get_offset_of_corePartsColorId_12() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___corePartsColorId_12)); }
	inline int32_t get_corePartsColorId_12() const { return ___corePartsColorId_12; }
	inline int32_t* get_address_of_corePartsColorId_12() { return &___corePartsColorId_12; }
	inline void set_corePartsColorId_12(int32_t value)
	{
		___corePartsColorId_12 = value;
	}

	inline static int32_t get_offset_of_newChildPartslist_13() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___newChildPartslist_13)); }
	inline List_1_t1785551985 * get_newChildPartslist_13() const { return ___newChildPartslist_13; }
	inline List_1_t1785551985 ** get_address_of_newChildPartslist_13() { return &___newChildPartslist_13; }
	inline void set_newChildPartslist_13(List_1_t1785551985 * value)
	{
		___newChildPartslist_13 = value;
		Il2CppCodeGenWriteBarrier(&___newChildPartslist_13, value);
	}

	inline static int32_t get_offset_of_newMobilityPartsList_14() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ___newMobilityPartsList_14)); }
	inline Dictionary_2_t36242819 * get_newMobilityPartsList_14() const { return ___newMobilityPartsList_14; }
	inline Dictionary_2_t36242819 ** get_address_of_newMobilityPartsList_14() { return &___newMobilityPartsList_14; }
	inline void set_newMobilityPartsList_14(Dictionary_2_t36242819 * value)
	{
		___newMobilityPartsList_14 = value;
		Il2CppCodeGenWriteBarrier(&___newMobilityPartsList_14, value);
	}

	inline static int32_t get_offset_of__mobilmoValues_15() { return static_cast<int32_t>(offsetof(ApiMobilityManager_t1284346316, ____mobilmoValues_15)); }
	inline List_1_t1785551985 * get__mobilmoValues_15() const { return ____mobilmoValues_15; }
	inline List_1_t1785551985 ** get_address_of__mobilmoValues_15() { return &____mobilmoValues_15; }
	inline void set__mobilmoValues_15(List_1_t1785551985 * value)
	{
		____mobilmoValues_15 = value;
		Il2CppCodeGenWriteBarrier(&____mobilmoValues_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
