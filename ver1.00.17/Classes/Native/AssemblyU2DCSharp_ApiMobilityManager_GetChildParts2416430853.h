﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiMobilityManager/GetChildPartsList
struct  GetChildPartsList_t2416430853  : public Il2CppObject
{
public:
	// System.String ApiMobilityManager/GetChildPartsList::partsType
	String_t* ___partsType_0;
	// System.Int32 ApiMobilityManager/GetChildPartsList::moduleId
	int32_t ___moduleId_1;
	// System.String ApiMobilityManager/GetChildPartsList::childPartsName
	String_t* ___childPartsName_2;
	// System.Int32 ApiMobilityManager/GetChildPartsList::childPartsColor
	int32_t ___childPartsColor_3;
	// System.Int32 ApiMobilityManager/GetChildPartsList::childPartsJointNo
	int32_t ___childPartsJointNo_4;
	// System.String ApiMobilityManager/GetChildPartsList::childPartsId
	String_t* ___childPartsId_5;
	// System.String ApiMobilityManager/GetChildPartsList::parentPartsName
	String_t* ___parentPartsName_6;
	// System.Int32 ApiMobilityManager/GetChildPartsList::parentPartsId
	int32_t ___parentPartsId_7;
	// System.Int32 ApiMobilityManager/GetChildPartsList::parentPartsJointNo
	int32_t ___parentPartsJointNo_8;
	// System.String ApiMobilityManager/GetChildPartsList::jointChildPartsName
	String_t* ___jointChildPartsName_9;
	// System.Single ApiMobilityManager/GetChildPartsList::childObjPositionX
	float ___childObjPositionX_10;
	// System.Single ApiMobilityManager/GetChildPartsList::childObjPositionY
	float ___childObjPositionY_11;
	// System.Single ApiMobilityManager/GetChildPartsList::childObjPositionZ
	float ___childObjPositionZ_12;
	// System.Single ApiMobilityManager/GetChildPartsList::childLeapRotationX
	float ___childLeapRotationX_13;
	// System.Single ApiMobilityManager/GetChildPartsList::childLeapRotationY
	float ___childLeapRotationY_14;
	// System.Single ApiMobilityManager/GetChildPartsList::childLeapRotationZ
	float ___childLeapRotationZ_15;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsPositionX
	float ___childPartsPositionX_16;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsPositionY
	float ___childPartsPositionY_17;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsPositionZ
	float ___childPartsPositionZ_18;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsRotationX
	float ___childPartsRotationX_19;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsRotationY
	float ___childPartsRotationY_20;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsRotationZ
	float ___childPartsRotationZ_21;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsScaleX
	float ___childPartsScaleX_22;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsScaleY
	float ___childPartsScaleY_23;
	// System.Single ApiMobilityManager/GetChildPartsList::childPartsScaleZ
	float ___childPartsScaleZ_24;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleScaleX
	float ___moduleScaleX_25;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleScaleY
	float ___moduleScaleY_26;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleScaleZ
	float ___moduleScaleZ_27;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleCenterX
	float ___moduleCenterX_28;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleCenterY
	float ___moduleCenterY_29;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleCenterZ
	float ___moduleCenterZ_30;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleLeapX
	float ___moduleLeapX_31;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleLeapY
	float ___moduleLeapY_32;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleLeapZ
	float ___moduleLeapZ_33;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleRollX
	float ___moduleRollX_34;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleRollY
	float ___moduleRollY_35;
	// System.Single ApiMobilityManager/GetChildPartsList::moduleRollZ
	float ___moduleRollZ_36;
	// System.Int32 ApiMobilityManager/GetChildPartsList::parentModuleId
	int32_t ___parentModuleId_37;

public:
	inline static int32_t get_offset_of_partsType_0() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___partsType_0)); }
	inline String_t* get_partsType_0() const { return ___partsType_0; }
	inline String_t** get_address_of_partsType_0() { return &___partsType_0; }
	inline void set_partsType_0(String_t* value)
	{
		___partsType_0 = value;
		Il2CppCodeGenWriteBarrier(&___partsType_0, value);
	}

	inline static int32_t get_offset_of_moduleId_1() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleId_1)); }
	inline int32_t get_moduleId_1() const { return ___moduleId_1; }
	inline int32_t* get_address_of_moduleId_1() { return &___moduleId_1; }
	inline void set_moduleId_1(int32_t value)
	{
		___moduleId_1 = value;
	}

	inline static int32_t get_offset_of_childPartsName_2() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsName_2)); }
	inline String_t* get_childPartsName_2() const { return ___childPartsName_2; }
	inline String_t** get_address_of_childPartsName_2() { return &___childPartsName_2; }
	inline void set_childPartsName_2(String_t* value)
	{
		___childPartsName_2 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsName_2, value);
	}

	inline static int32_t get_offset_of_childPartsColor_3() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsColor_3)); }
	inline int32_t get_childPartsColor_3() const { return ___childPartsColor_3; }
	inline int32_t* get_address_of_childPartsColor_3() { return &___childPartsColor_3; }
	inline void set_childPartsColor_3(int32_t value)
	{
		___childPartsColor_3 = value;
	}

	inline static int32_t get_offset_of_childPartsJointNo_4() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsJointNo_4)); }
	inline int32_t get_childPartsJointNo_4() const { return ___childPartsJointNo_4; }
	inline int32_t* get_address_of_childPartsJointNo_4() { return &___childPartsJointNo_4; }
	inline void set_childPartsJointNo_4(int32_t value)
	{
		___childPartsJointNo_4 = value;
	}

	inline static int32_t get_offset_of_childPartsId_5() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsId_5)); }
	inline String_t* get_childPartsId_5() const { return ___childPartsId_5; }
	inline String_t** get_address_of_childPartsId_5() { return &___childPartsId_5; }
	inline void set_childPartsId_5(String_t* value)
	{
		___childPartsId_5 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsId_5, value);
	}

	inline static int32_t get_offset_of_parentPartsName_6() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___parentPartsName_6)); }
	inline String_t* get_parentPartsName_6() const { return ___parentPartsName_6; }
	inline String_t** get_address_of_parentPartsName_6() { return &___parentPartsName_6; }
	inline void set_parentPartsName_6(String_t* value)
	{
		___parentPartsName_6 = value;
		Il2CppCodeGenWriteBarrier(&___parentPartsName_6, value);
	}

	inline static int32_t get_offset_of_parentPartsId_7() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___parentPartsId_7)); }
	inline int32_t get_parentPartsId_7() const { return ___parentPartsId_7; }
	inline int32_t* get_address_of_parentPartsId_7() { return &___parentPartsId_7; }
	inline void set_parentPartsId_7(int32_t value)
	{
		___parentPartsId_7 = value;
	}

	inline static int32_t get_offset_of_parentPartsJointNo_8() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___parentPartsJointNo_8)); }
	inline int32_t get_parentPartsJointNo_8() const { return ___parentPartsJointNo_8; }
	inline int32_t* get_address_of_parentPartsJointNo_8() { return &___parentPartsJointNo_8; }
	inline void set_parentPartsJointNo_8(int32_t value)
	{
		___parentPartsJointNo_8 = value;
	}

	inline static int32_t get_offset_of_jointChildPartsName_9() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___jointChildPartsName_9)); }
	inline String_t* get_jointChildPartsName_9() const { return ___jointChildPartsName_9; }
	inline String_t** get_address_of_jointChildPartsName_9() { return &___jointChildPartsName_9; }
	inline void set_jointChildPartsName_9(String_t* value)
	{
		___jointChildPartsName_9 = value;
		Il2CppCodeGenWriteBarrier(&___jointChildPartsName_9, value);
	}

	inline static int32_t get_offset_of_childObjPositionX_10() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childObjPositionX_10)); }
	inline float get_childObjPositionX_10() const { return ___childObjPositionX_10; }
	inline float* get_address_of_childObjPositionX_10() { return &___childObjPositionX_10; }
	inline void set_childObjPositionX_10(float value)
	{
		___childObjPositionX_10 = value;
	}

	inline static int32_t get_offset_of_childObjPositionY_11() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childObjPositionY_11)); }
	inline float get_childObjPositionY_11() const { return ___childObjPositionY_11; }
	inline float* get_address_of_childObjPositionY_11() { return &___childObjPositionY_11; }
	inline void set_childObjPositionY_11(float value)
	{
		___childObjPositionY_11 = value;
	}

	inline static int32_t get_offset_of_childObjPositionZ_12() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childObjPositionZ_12)); }
	inline float get_childObjPositionZ_12() const { return ___childObjPositionZ_12; }
	inline float* get_address_of_childObjPositionZ_12() { return &___childObjPositionZ_12; }
	inline void set_childObjPositionZ_12(float value)
	{
		___childObjPositionZ_12 = value;
	}

	inline static int32_t get_offset_of_childLeapRotationX_13() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childLeapRotationX_13)); }
	inline float get_childLeapRotationX_13() const { return ___childLeapRotationX_13; }
	inline float* get_address_of_childLeapRotationX_13() { return &___childLeapRotationX_13; }
	inline void set_childLeapRotationX_13(float value)
	{
		___childLeapRotationX_13 = value;
	}

	inline static int32_t get_offset_of_childLeapRotationY_14() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childLeapRotationY_14)); }
	inline float get_childLeapRotationY_14() const { return ___childLeapRotationY_14; }
	inline float* get_address_of_childLeapRotationY_14() { return &___childLeapRotationY_14; }
	inline void set_childLeapRotationY_14(float value)
	{
		___childLeapRotationY_14 = value;
	}

	inline static int32_t get_offset_of_childLeapRotationZ_15() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childLeapRotationZ_15)); }
	inline float get_childLeapRotationZ_15() const { return ___childLeapRotationZ_15; }
	inline float* get_address_of_childLeapRotationZ_15() { return &___childLeapRotationZ_15; }
	inline void set_childLeapRotationZ_15(float value)
	{
		___childLeapRotationZ_15 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionX_16() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsPositionX_16)); }
	inline float get_childPartsPositionX_16() const { return ___childPartsPositionX_16; }
	inline float* get_address_of_childPartsPositionX_16() { return &___childPartsPositionX_16; }
	inline void set_childPartsPositionX_16(float value)
	{
		___childPartsPositionX_16 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionY_17() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsPositionY_17)); }
	inline float get_childPartsPositionY_17() const { return ___childPartsPositionY_17; }
	inline float* get_address_of_childPartsPositionY_17() { return &___childPartsPositionY_17; }
	inline void set_childPartsPositionY_17(float value)
	{
		___childPartsPositionY_17 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionZ_18() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsPositionZ_18)); }
	inline float get_childPartsPositionZ_18() const { return ___childPartsPositionZ_18; }
	inline float* get_address_of_childPartsPositionZ_18() { return &___childPartsPositionZ_18; }
	inline void set_childPartsPositionZ_18(float value)
	{
		___childPartsPositionZ_18 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationX_19() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsRotationX_19)); }
	inline float get_childPartsRotationX_19() const { return ___childPartsRotationX_19; }
	inline float* get_address_of_childPartsRotationX_19() { return &___childPartsRotationX_19; }
	inline void set_childPartsRotationX_19(float value)
	{
		___childPartsRotationX_19 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationY_20() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsRotationY_20)); }
	inline float get_childPartsRotationY_20() const { return ___childPartsRotationY_20; }
	inline float* get_address_of_childPartsRotationY_20() { return &___childPartsRotationY_20; }
	inline void set_childPartsRotationY_20(float value)
	{
		___childPartsRotationY_20 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationZ_21() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsRotationZ_21)); }
	inline float get_childPartsRotationZ_21() const { return ___childPartsRotationZ_21; }
	inline float* get_address_of_childPartsRotationZ_21() { return &___childPartsRotationZ_21; }
	inline void set_childPartsRotationZ_21(float value)
	{
		___childPartsRotationZ_21 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleX_22() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsScaleX_22)); }
	inline float get_childPartsScaleX_22() const { return ___childPartsScaleX_22; }
	inline float* get_address_of_childPartsScaleX_22() { return &___childPartsScaleX_22; }
	inline void set_childPartsScaleX_22(float value)
	{
		___childPartsScaleX_22 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleY_23() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsScaleY_23)); }
	inline float get_childPartsScaleY_23() const { return ___childPartsScaleY_23; }
	inline float* get_address_of_childPartsScaleY_23() { return &___childPartsScaleY_23; }
	inline void set_childPartsScaleY_23(float value)
	{
		___childPartsScaleY_23 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleZ_24() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___childPartsScaleZ_24)); }
	inline float get_childPartsScaleZ_24() const { return ___childPartsScaleZ_24; }
	inline float* get_address_of_childPartsScaleZ_24() { return &___childPartsScaleZ_24; }
	inline void set_childPartsScaleZ_24(float value)
	{
		___childPartsScaleZ_24 = value;
	}

	inline static int32_t get_offset_of_moduleScaleX_25() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleScaleX_25)); }
	inline float get_moduleScaleX_25() const { return ___moduleScaleX_25; }
	inline float* get_address_of_moduleScaleX_25() { return &___moduleScaleX_25; }
	inline void set_moduleScaleX_25(float value)
	{
		___moduleScaleX_25 = value;
	}

	inline static int32_t get_offset_of_moduleScaleY_26() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleScaleY_26)); }
	inline float get_moduleScaleY_26() const { return ___moduleScaleY_26; }
	inline float* get_address_of_moduleScaleY_26() { return &___moduleScaleY_26; }
	inline void set_moduleScaleY_26(float value)
	{
		___moduleScaleY_26 = value;
	}

	inline static int32_t get_offset_of_moduleScaleZ_27() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleScaleZ_27)); }
	inline float get_moduleScaleZ_27() const { return ___moduleScaleZ_27; }
	inline float* get_address_of_moduleScaleZ_27() { return &___moduleScaleZ_27; }
	inline void set_moduleScaleZ_27(float value)
	{
		___moduleScaleZ_27 = value;
	}

	inline static int32_t get_offset_of_moduleCenterX_28() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleCenterX_28)); }
	inline float get_moduleCenterX_28() const { return ___moduleCenterX_28; }
	inline float* get_address_of_moduleCenterX_28() { return &___moduleCenterX_28; }
	inline void set_moduleCenterX_28(float value)
	{
		___moduleCenterX_28 = value;
	}

	inline static int32_t get_offset_of_moduleCenterY_29() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleCenterY_29)); }
	inline float get_moduleCenterY_29() const { return ___moduleCenterY_29; }
	inline float* get_address_of_moduleCenterY_29() { return &___moduleCenterY_29; }
	inline void set_moduleCenterY_29(float value)
	{
		___moduleCenterY_29 = value;
	}

	inline static int32_t get_offset_of_moduleCenterZ_30() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleCenterZ_30)); }
	inline float get_moduleCenterZ_30() const { return ___moduleCenterZ_30; }
	inline float* get_address_of_moduleCenterZ_30() { return &___moduleCenterZ_30; }
	inline void set_moduleCenterZ_30(float value)
	{
		___moduleCenterZ_30 = value;
	}

	inline static int32_t get_offset_of_moduleLeapX_31() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleLeapX_31)); }
	inline float get_moduleLeapX_31() const { return ___moduleLeapX_31; }
	inline float* get_address_of_moduleLeapX_31() { return &___moduleLeapX_31; }
	inline void set_moduleLeapX_31(float value)
	{
		___moduleLeapX_31 = value;
	}

	inline static int32_t get_offset_of_moduleLeapY_32() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleLeapY_32)); }
	inline float get_moduleLeapY_32() const { return ___moduleLeapY_32; }
	inline float* get_address_of_moduleLeapY_32() { return &___moduleLeapY_32; }
	inline void set_moduleLeapY_32(float value)
	{
		___moduleLeapY_32 = value;
	}

	inline static int32_t get_offset_of_moduleLeapZ_33() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleLeapZ_33)); }
	inline float get_moduleLeapZ_33() const { return ___moduleLeapZ_33; }
	inline float* get_address_of_moduleLeapZ_33() { return &___moduleLeapZ_33; }
	inline void set_moduleLeapZ_33(float value)
	{
		___moduleLeapZ_33 = value;
	}

	inline static int32_t get_offset_of_moduleRollX_34() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleRollX_34)); }
	inline float get_moduleRollX_34() const { return ___moduleRollX_34; }
	inline float* get_address_of_moduleRollX_34() { return &___moduleRollX_34; }
	inline void set_moduleRollX_34(float value)
	{
		___moduleRollX_34 = value;
	}

	inline static int32_t get_offset_of_moduleRollY_35() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleRollY_35)); }
	inline float get_moduleRollY_35() const { return ___moduleRollY_35; }
	inline float* get_address_of_moduleRollY_35() { return &___moduleRollY_35; }
	inline void set_moduleRollY_35(float value)
	{
		___moduleRollY_35 = value;
	}

	inline static int32_t get_offset_of_moduleRollZ_36() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___moduleRollZ_36)); }
	inline float get_moduleRollZ_36() const { return ___moduleRollZ_36; }
	inline float* get_address_of_moduleRollZ_36() { return &___moduleRollZ_36; }
	inline void set_moduleRollZ_36(float value)
	{
		___moduleRollZ_36 = value;
	}

	inline static int32_t get_offset_of_parentModuleId_37() { return static_cast<int32_t>(offsetof(GetChildPartsList_t2416430853, ___parentModuleId_37)); }
	inline int32_t get_parentModuleId_37() const { return ___parentModuleId_37; }
	inline int32_t* get_address_of_parentModuleId_37() { return &___parentModuleId_37; }
	inline void set_parentModuleId_37(int32_t value)
	{
		___parentModuleId_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
