﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiMobilityManager/GetMobilityDelJson
struct  GetMobilityDelJson_t1883011669  : public Il2CppObject
{
public:
	// System.Int32 ApiMobilityManager/GetMobilityDelJson::mobilityId
	int32_t ___mobilityId_0;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(GetMobilityDelJson_t1883011669, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
