﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiMobilityManager/GetMobilityPrizeJson
struct  GetMobilityPrizeJson_t1651198200  : public Il2CppObject
{
public:
	// System.Int32 ApiMobilityManager/GetMobilityPrizeJson::prizedTypeId
	int32_t ___prizedTypeId_0;
	// System.Int32 ApiMobilityManager/GetMobilityPrizeJson::mobilityId
	int32_t ___mobilityId_1;
	// System.Int32 ApiMobilityManager/GetMobilityPrizeJson::eventId
	int32_t ___eventId_2;
	// System.Int32 ApiMobilityManager/GetMobilityPrizeJson::point
	int32_t ___point_3;
	// System.Single ApiMobilityManager/GetMobilityPrizeJson::time
	float ___time_4;

public:
	inline static int32_t get_offset_of_prizedTypeId_0() { return static_cast<int32_t>(offsetof(GetMobilityPrizeJson_t1651198200, ___prizedTypeId_0)); }
	inline int32_t get_prizedTypeId_0() const { return ___prizedTypeId_0; }
	inline int32_t* get_address_of_prizedTypeId_0() { return &___prizedTypeId_0; }
	inline void set_prizedTypeId_0(int32_t value)
	{
		___prizedTypeId_0 = value;
	}

	inline static int32_t get_offset_of_mobilityId_1() { return static_cast<int32_t>(offsetof(GetMobilityPrizeJson_t1651198200, ___mobilityId_1)); }
	inline int32_t get_mobilityId_1() const { return ___mobilityId_1; }
	inline int32_t* get_address_of_mobilityId_1() { return &___mobilityId_1; }
	inline void set_mobilityId_1(int32_t value)
	{
		___mobilityId_1 = value;
	}

	inline static int32_t get_offset_of_eventId_2() { return static_cast<int32_t>(offsetof(GetMobilityPrizeJson_t1651198200, ___eventId_2)); }
	inline int32_t get_eventId_2() const { return ___eventId_2; }
	inline int32_t* get_address_of_eventId_2() { return &___eventId_2; }
	inline void set_eventId_2(int32_t value)
	{
		___eventId_2 = value;
	}

	inline static int32_t get_offset_of_point_3() { return static_cast<int32_t>(offsetof(GetMobilityPrizeJson_t1651198200, ___point_3)); }
	inline int32_t get_point_3() const { return ___point_3; }
	inline int32_t* get_address_of_point_3() { return &___point_3; }
	inline void set_point_3(int32_t value)
	{
		___point_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(GetMobilityPrizeJson_t1651198200, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
