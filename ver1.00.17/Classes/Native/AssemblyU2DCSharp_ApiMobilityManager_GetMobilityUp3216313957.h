﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiMobilityManager/GetMobilityUpdateJson
struct  GetMobilityUpdateJson_t3216313957  : public Il2CppObject
{
public:
	// System.Int32 ApiMobilityManager/GetMobilityUpdateJson::mobilityId
	int32_t ___mobilityId_0;
	// System.String ApiMobilityManager/GetMobilityUpdateJson::name
	String_t* ___name_1;
	// System.String ApiMobilityManager/GetMobilityUpdateJson::situation
	String_t* ___situation_2;
	// System.String ApiMobilityManager/GetMobilityUpdateJson::description
	String_t* ___description_3;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(GetMobilityUpdateJson_t3216313957, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GetMobilityUpdateJson_t3216313957, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_situation_2() { return static_cast<int32_t>(offsetof(GetMobilityUpdateJson_t3216313957, ___situation_2)); }
	inline String_t* get_situation_2() const { return ___situation_2; }
	inline String_t** get_address_of_situation_2() { return &___situation_2; }
	inline void set_situation_2(String_t* value)
	{
		___situation_2 = value;
		Il2CppCodeGenWriteBarrier(&___situation_2, value);
	}

	inline static int32_t get_offset_of_description_3() { return static_cast<int32_t>(offsetof(GetMobilityUpdateJson_t3216313957, ___description_3)); }
	inline String_t* get_description_3() const { return ___description_3; }
	inline String_t** get_address_of_description_3() { return &___description_3; }
	inline void set_description_3(String_t* value)
	{
		___description_3 = value;
		Il2CppCodeGenWriteBarrier(&___description_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
