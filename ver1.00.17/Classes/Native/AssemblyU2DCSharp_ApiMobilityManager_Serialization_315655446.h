﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<ApiMobilityManager/GetChildPartsList>
struct List_1_t1785551985;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiMobilityManager/Serialization`1<ApiMobilityManager/GetChildPartsList>
struct  Serialization_1_t315655446  : public Il2CppObject
{
public:
	// System.String ApiMobilityManager/Serialization`1::name
	String_t* ___name_0;
	// System.String ApiMobilityManager/Serialization`1::situation
	String_t* ___situation_1;
	// System.String ApiMobilityManager/Serialization`1::description
	String_t* ___description_2;
	// System.Int32 ApiMobilityManager/Serialization`1::parentId
	int32_t ___parentId_3;
	// System.Int32 ApiMobilityManager/Serialization`1::corePartsId
	int32_t ___corePartsId_4;
	// System.Int32 ApiMobilityManager/Serialization`1::corePartsColor
	int32_t ___corePartsColor_5;
	// System.Single ApiMobilityManager/Serialization`1::corePartsScaleX
	float ___corePartsScaleX_6;
	// System.Single ApiMobilityManager/Serialization`1::corePartsScaleY
	float ___corePartsScaleY_7;
	// System.Single ApiMobilityManager/Serialization`1::corePartsScaleZ
	float ___corePartsScaleZ_8;
	// System.String ApiMobilityManager/Serialization`1::corePartsName
	String_t* ___corePartsName_9;
	// System.Boolean ApiMobilityManager/Serialization`1::tutorialFlg
	bool ___tutorialFlg_10;
	// System.Collections.Generic.List`1<T> ApiMobilityManager/Serialization`1::childPartsList
	List_1_t1785551985 * ___childPartsList_11;
	// System.Int32 ApiMobilityManager/Serialization`1::createdUserId
	int32_t ___createdUserId_12;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_situation_1() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___situation_1)); }
	inline String_t* get_situation_1() const { return ___situation_1; }
	inline String_t** get_address_of_situation_1() { return &___situation_1; }
	inline void set_situation_1(String_t* value)
	{
		___situation_1 = value;
		Il2CppCodeGenWriteBarrier(&___situation_1, value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier(&___description_2, value);
	}

	inline static int32_t get_offset_of_parentId_3() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___parentId_3)); }
	inline int32_t get_parentId_3() const { return ___parentId_3; }
	inline int32_t* get_address_of_parentId_3() { return &___parentId_3; }
	inline void set_parentId_3(int32_t value)
	{
		___parentId_3 = value;
	}

	inline static int32_t get_offset_of_corePartsId_4() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___corePartsId_4)); }
	inline int32_t get_corePartsId_4() const { return ___corePartsId_4; }
	inline int32_t* get_address_of_corePartsId_4() { return &___corePartsId_4; }
	inline void set_corePartsId_4(int32_t value)
	{
		___corePartsId_4 = value;
	}

	inline static int32_t get_offset_of_corePartsColor_5() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___corePartsColor_5)); }
	inline int32_t get_corePartsColor_5() const { return ___corePartsColor_5; }
	inline int32_t* get_address_of_corePartsColor_5() { return &___corePartsColor_5; }
	inline void set_corePartsColor_5(int32_t value)
	{
		___corePartsColor_5 = value;
	}

	inline static int32_t get_offset_of_corePartsScaleX_6() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___corePartsScaleX_6)); }
	inline float get_corePartsScaleX_6() const { return ___corePartsScaleX_6; }
	inline float* get_address_of_corePartsScaleX_6() { return &___corePartsScaleX_6; }
	inline void set_corePartsScaleX_6(float value)
	{
		___corePartsScaleX_6 = value;
	}

	inline static int32_t get_offset_of_corePartsScaleY_7() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___corePartsScaleY_7)); }
	inline float get_corePartsScaleY_7() const { return ___corePartsScaleY_7; }
	inline float* get_address_of_corePartsScaleY_7() { return &___corePartsScaleY_7; }
	inline void set_corePartsScaleY_7(float value)
	{
		___corePartsScaleY_7 = value;
	}

	inline static int32_t get_offset_of_corePartsScaleZ_8() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___corePartsScaleZ_8)); }
	inline float get_corePartsScaleZ_8() const { return ___corePartsScaleZ_8; }
	inline float* get_address_of_corePartsScaleZ_8() { return &___corePartsScaleZ_8; }
	inline void set_corePartsScaleZ_8(float value)
	{
		___corePartsScaleZ_8 = value;
	}

	inline static int32_t get_offset_of_corePartsName_9() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___corePartsName_9)); }
	inline String_t* get_corePartsName_9() const { return ___corePartsName_9; }
	inline String_t** get_address_of_corePartsName_9() { return &___corePartsName_9; }
	inline void set_corePartsName_9(String_t* value)
	{
		___corePartsName_9 = value;
		Il2CppCodeGenWriteBarrier(&___corePartsName_9, value);
	}

	inline static int32_t get_offset_of_tutorialFlg_10() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___tutorialFlg_10)); }
	inline bool get_tutorialFlg_10() const { return ___tutorialFlg_10; }
	inline bool* get_address_of_tutorialFlg_10() { return &___tutorialFlg_10; }
	inline void set_tutorialFlg_10(bool value)
	{
		___tutorialFlg_10 = value;
	}

	inline static int32_t get_offset_of_childPartsList_11() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___childPartsList_11)); }
	inline List_1_t1785551985 * get_childPartsList_11() const { return ___childPartsList_11; }
	inline List_1_t1785551985 ** get_address_of_childPartsList_11() { return &___childPartsList_11; }
	inline void set_childPartsList_11(List_1_t1785551985 * value)
	{
		___childPartsList_11 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsList_11, value);
	}

	inline static int32_t get_offset_of_createdUserId_12() { return static_cast<int32_t>(offsetof(Serialization_1_t315655446, ___createdUserId_12)); }
	inline int32_t get_createdUserId_12() const { return ___createdUserId_12; }
	inline int32_t* get_address_of_createdUserId_12() { return &___createdUserId_12; }
	inline void set_createdUserId_12(int32_t value)
	{
		___createdUserId_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
