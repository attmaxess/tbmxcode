﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2636376462.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<ApiModuleManager/GetModuleChildPartsList>
struct List_1_t2567689500;
// System.Collections.Generic.Dictionary`2<System.String,ApiModuleManager/GetModuleChildPartsList>
struct Dictionary_2_t818380334;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleManager
struct  ApiModuleManager_t3939700127  : public SingletonMonoBehaviour_1_t2636376462
{
public:
	// System.String ApiModuleManager::moduleNewJson
	String_t* ___moduleNewJson_3;
	// System.String ApiModuleManager::moduleDelJson
	String_t* ___moduleDelJson_4;
	// System.String ApiModuleManager::getModuleJson
	String_t* ___getModuleJson_5;
	// System.Int32 ApiModuleManager::moduleId
	int32_t ___moduleId_6;
	// System.Collections.Generic.List`1<ApiModuleManager/GetModuleChildPartsList> ApiModuleManager::newModuleChildPartslist
	List_1_t2567689500 * ___newModuleChildPartslist_7;
	// System.Collections.Generic.Dictionary`2<System.String,ApiModuleManager/GetModuleChildPartsList> ApiModuleManager::newModuleChildPartsDic
	Dictionary_2_t818380334 * ___newModuleChildPartsDic_8;
	// System.Collections.Generic.List`1<System.Object> ApiModuleManager::myModuleIdList
	List_1_t2058570427 * ___myModuleIdList_9;
	// System.Collections.Generic.List`1<ApiModuleManager/GetModuleChildPartsList> ApiModuleManager::_values
	List_1_t2567689500 * ____values_10;

public:
	inline static int32_t get_offset_of_moduleNewJson_3() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ___moduleNewJson_3)); }
	inline String_t* get_moduleNewJson_3() const { return ___moduleNewJson_3; }
	inline String_t** get_address_of_moduleNewJson_3() { return &___moduleNewJson_3; }
	inline void set_moduleNewJson_3(String_t* value)
	{
		___moduleNewJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___moduleNewJson_3, value);
	}

	inline static int32_t get_offset_of_moduleDelJson_4() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ___moduleDelJson_4)); }
	inline String_t* get_moduleDelJson_4() const { return ___moduleDelJson_4; }
	inline String_t** get_address_of_moduleDelJson_4() { return &___moduleDelJson_4; }
	inline void set_moduleDelJson_4(String_t* value)
	{
		___moduleDelJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___moduleDelJson_4, value);
	}

	inline static int32_t get_offset_of_getModuleJson_5() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ___getModuleJson_5)); }
	inline String_t* get_getModuleJson_5() const { return ___getModuleJson_5; }
	inline String_t** get_address_of_getModuleJson_5() { return &___getModuleJson_5; }
	inline void set_getModuleJson_5(String_t* value)
	{
		___getModuleJson_5 = value;
		Il2CppCodeGenWriteBarrier(&___getModuleJson_5, value);
	}

	inline static int32_t get_offset_of_moduleId_6() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ___moduleId_6)); }
	inline int32_t get_moduleId_6() const { return ___moduleId_6; }
	inline int32_t* get_address_of_moduleId_6() { return &___moduleId_6; }
	inline void set_moduleId_6(int32_t value)
	{
		___moduleId_6 = value;
	}

	inline static int32_t get_offset_of_newModuleChildPartslist_7() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ___newModuleChildPartslist_7)); }
	inline List_1_t2567689500 * get_newModuleChildPartslist_7() const { return ___newModuleChildPartslist_7; }
	inline List_1_t2567689500 ** get_address_of_newModuleChildPartslist_7() { return &___newModuleChildPartslist_7; }
	inline void set_newModuleChildPartslist_7(List_1_t2567689500 * value)
	{
		___newModuleChildPartslist_7 = value;
		Il2CppCodeGenWriteBarrier(&___newModuleChildPartslist_7, value);
	}

	inline static int32_t get_offset_of_newModuleChildPartsDic_8() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ___newModuleChildPartsDic_8)); }
	inline Dictionary_2_t818380334 * get_newModuleChildPartsDic_8() const { return ___newModuleChildPartsDic_8; }
	inline Dictionary_2_t818380334 ** get_address_of_newModuleChildPartsDic_8() { return &___newModuleChildPartsDic_8; }
	inline void set_newModuleChildPartsDic_8(Dictionary_2_t818380334 * value)
	{
		___newModuleChildPartsDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___newModuleChildPartsDic_8, value);
	}

	inline static int32_t get_offset_of_myModuleIdList_9() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ___myModuleIdList_9)); }
	inline List_1_t2058570427 * get_myModuleIdList_9() const { return ___myModuleIdList_9; }
	inline List_1_t2058570427 ** get_address_of_myModuleIdList_9() { return &___myModuleIdList_9; }
	inline void set_myModuleIdList_9(List_1_t2058570427 * value)
	{
		___myModuleIdList_9 = value;
		Il2CppCodeGenWriteBarrier(&___myModuleIdList_9, value);
	}

	inline static int32_t get_offset_of__values_10() { return static_cast<int32_t>(offsetof(ApiModuleManager_t3939700127, ____values_10)); }
	inline List_1_t2567689500 * get__values_10() const { return ____values_10; }
	inline List_1_t2567689500 ** get_address_of__values_10() { return &____values_10; }
	inline void set__values_10(List_1_t2567689500 * value)
	{
		____values_10 = value;
		Il2CppCodeGenWriteBarrier(&____values_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
