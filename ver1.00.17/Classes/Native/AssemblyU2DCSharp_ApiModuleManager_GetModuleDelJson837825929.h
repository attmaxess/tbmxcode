﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleManager/GetModuleDelJson
struct  GetModuleDelJson_t837825929  : public Il2CppObject
{
public:
	// System.Int32 ApiModuleManager/GetModuleDelJson::moduleId
	int32_t ___moduleId_0;

public:
	inline static int32_t get_offset_of_moduleId_0() { return static_cast<int32_t>(offsetof(GetModuleDelJson_t837825929, ___moduleId_0)); }
	inline int32_t get_moduleId_0() const { return ___moduleId_0; }
	inline int32_t* get_address_of_moduleId_0() { return &___moduleId_0; }
	inline void set_moduleId_0(int32_t value)
	{
		___moduleId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
