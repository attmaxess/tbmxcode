﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleManager/Serialization`1<System.Object>
struct  Serialization_1_t1716271607  : public Il2CppObject
{
public:
	// System.Int32 ApiModuleManager/Serialization`1::parentId
	int32_t ___parentId_0;
	// System.Int32 ApiModuleManager/Serialization`1::corePartsId
	int32_t ___corePartsId_1;
	// System.Int32 ApiModuleManager/Serialization`1::corePartsColor
	int32_t ___corePartsColor_2;
	// System.Single ApiModuleManager/Serialization`1::corePartsScaleX
	float ___corePartsScaleX_3;
	// System.Single ApiModuleManager/Serialization`1::corePartsScaleY
	float ___corePartsScaleY_4;
	// System.Single ApiModuleManager/Serialization`1::corePartsScaleZ
	float ___corePartsScaleZ_5;
	// System.String ApiModuleManager/Serialization`1::corePartsName
	String_t* ___corePartsName_6;
	// System.String ApiModuleManager/Serialization`1::moduleParentPartsChildName
	String_t* ___moduleParentPartsChildName_7;
	// System.Single ApiModuleManager/Serialization`1::moduleRotationX
	float ___moduleRotationX_8;
	// System.Single ApiModuleManager/Serialization`1::moduleRotationY
	float ___moduleRotationY_9;
	// System.Single ApiModuleManager/Serialization`1::moduleRotationZ
	float ___moduleRotationZ_10;
	// System.Collections.Generic.List`1<T> ApiModuleManager/Serialization`1::childPartsList
	List_1_t2058570427 * ___childPartsList_11;
	// System.Int32 ApiModuleManager/Serialization`1::createdUserId
	int32_t ___createdUserId_12;

public:
	inline static int32_t get_offset_of_parentId_0() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___parentId_0)); }
	inline int32_t get_parentId_0() const { return ___parentId_0; }
	inline int32_t* get_address_of_parentId_0() { return &___parentId_0; }
	inline void set_parentId_0(int32_t value)
	{
		___parentId_0 = value;
	}

	inline static int32_t get_offset_of_corePartsId_1() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___corePartsId_1)); }
	inline int32_t get_corePartsId_1() const { return ___corePartsId_1; }
	inline int32_t* get_address_of_corePartsId_1() { return &___corePartsId_1; }
	inline void set_corePartsId_1(int32_t value)
	{
		___corePartsId_1 = value;
	}

	inline static int32_t get_offset_of_corePartsColor_2() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___corePartsColor_2)); }
	inline int32_t get_corePartsColor_2() const { return ___corePartsColor_2; }
	inline int32_t* get_address_of_corePartsColor_2() { return &___corePartsColor_2; }
	inline void set_corePartsColor_2(int32_t value)
	{
		___corePartsColor_2 = value;
	}

	inline static int32_t get_offset_of_corePartsScaleX_3() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___corePartsScaleX_3)); }
	inline float get_corePartsScaleX_3() const { return ___corePartsScaleX_3; }
	inline float* get_address_of_corePartsScaleX_3() { return &___corePartsScaleX_3; }
	inline void set_corePartsScaleX_3(float value)
	{
		___corePartsScaleX_3 = value;
	}

	inline static int32_t get_offset_of_corePartsScaleY_4() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___corePartsScaleY_4)); }
	inline float get_corePartsScaleY_4() const { return ___corePartsScaleY_4; }
	inline float* get_address_of_corePartsScaleY_4() { return &___corePartsScaleY_4; }
	inline void set_corePartsScaleY_4(float value)
	{
		___corePartsScaleY_4 = value;
	}

	inline static int32_t get_offset_of_corePartsScaleZ_5() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___corePartsScaleZ_5)); }
	inline float get_corePartsScaleZ_5() const { return ___corePartsScaleZ_5; }
	inline float* get_address_of_corePartsScaleZ_5() { return &___corePartsScaleZ_5; }
	inline void set_corePartsScaleZ_5(float value)
	{
		___corePartsScaleZ_5 = value;
	}

	inline static int32_t get_offset_of_corePartsName_6() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___corePartsName_6)); }
	inline String_t* get_corePartsName_6() const { return ___corePartsName_6; }
	inline String_t** get_address_of_corePartsName_6() { return &___corePartsName_6; }
	inline void set_corePartsName_6(String_t* value)
	{
		___corePartsName_6 = value;
		Il2CppCodeGenWriteBarrier(&___corePartsName_6, value);
	}

	inline static int32_t get_offset_of_moduleParentPartsChildName_7() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___moduleParentPartsChildName_7)); }
	inline String_t* get_moduleParentPartsChildName_7() const { return ___moduleParentPartsChildName_7; }
	inline String_t** get_address_of_moduleParentPartsChildName_7() { return &___moduleParentPartsChildName_7; }
	inline void set_moduleParentPartsChildName_7(String_t* value)
	{
		___moduleParentPartsChildName_7 = value;
		Il2CppCodeGenWriteBarrier(&___moduleParentPartsChildName_7, value);
	}

	inline static int32_t get_offset_of_moduleRotationX_8() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___moduleRotationX_8)); }
	inline float get_moduleRotationX_8() const { return ___moduleRotationX_8; }
	inline float* get_address_of_moduleRotationX_8() { return &___moduleRotationX_8; }
	inline void set_moduleRotationX_8(float value)
	{
		___moduleRotationX_8 = value;
	}

	inline static int32_t get_offset_of_moduleRotationY_9() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___moduleRotationY_9)); }
	inline float get_moduleRotationY_9() const { return ___moduleRotationY_9; }
	inline float* get_address_of_moduleRotationY_9() { return &___moduleRotationY_9; }
	inline void set_moduleRotationY_9(float value)
	{
		___moduleRotationY_9 = value;
	}

	inline static int32_t get_offset_of_moduleRotationZ_10() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___moduleRotationZ_10)); }
	inline float get_moduleRotationZ_10() const { return ___moduleRotationZ_10; }
	inline float* get_address_of_moduleRotationZ_10() { return &___moduleRotationZ_10; }
	inline void set_moduleRotationZ_10(float value)
	{
		___moduleRotationZ_10 = value;
	}

	inline static int32_t get_offset_of_childPartsList_11() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___childPartsList_11)); }
	inline List_1_t2058570427 * get_childPartsList_11() const { return ___childPartsList_11; }
	inline List_1_t2058570427 ** get_address_of_childPartsList_11() { return &___childPartsList_11; }
	inline void set_childPartsList_11(List_1_t2058570427 * value)
	{
		___childPartsList_11 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsList_11, value);
	}

	inline static int32_t get_offset_of_createdUserId_12() { return static_cast<int32_t>(offsetof(Serialization_1_t1716271607, ___createdUserId_12)); }
	inline int32_t get_createdUserId_12() const { return ___createdUserId_12; }
	inline int32_t* get_address_of_createdUserId_12() { return &___createdUserId_12; }
	inline void set_createdUserId_12(int32_t value)
	{
		___createdUserId_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
