﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1656918728.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve>
struct Dictionary_2_t926353117;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t3936083219;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleMotionManager
struct  ApiModuleMotionManager_t2960242393  : public SingletonMonoBehaviour_1_t1656918728
{
public:
	// System.String ApiModuleMotionManager::moduleMotionNewJson
	String_t* ___moduleMotionNewJson_3;
	// System.Int32 ApiModuleMotionManager::moduleActionId
	int32_t ___moduleActionId_4;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> ApiModuleMotionManager::m_pCurveDic1
	Dictionary_2_t926353117 * ___m_pCurveDic1_5;
	// UnityEngine.AnimationCurve ApiModuleMotionManager::curve
	AnimationCurve_t3306541151 * ___curve_6;
	// UnityEngine.Keyframe[] ApiModuleMotionManager::ks
	KeyframeU5BU5D_t449065829* ___ks_7;
	// System.String ApiModuleMotionManager::path
	String_t* ___path_8;
	// System.String ApiModuleMotionManager::property
	String_t* ___property_9;
	// UnityEngine.AnimationClip[] ApiModuleMotionManager::moduleAnimClip
	AnimationClipU5BU5D_t3936083219* ___moduleAnimClip_10;
	// System.Boolean ApiModuleMotionManager::ischangeClip
	bool ___ischangeClip_11;
	// System.Int32 ApiModuleMotionManager::modulePoseDataCount
	int32_t ___modulePoseDataCount_12;

public:
	inline static int32_t get_offset_of_moduleMotionNewJson_3() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___moduleMotionNewJson_3)); }
	inline String_t* get_moduleMotionNewJson_3() const { return ___moduleMotionNewJson_3; }
	inline String_t** get_address_of_moduleMotionNewJson_3() { return &___moduleMotionNewJson_3; }
	inline void set_moduleMotionNewJson_3(String_t* value)
	{
		___moduleMotionNewJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___moduleMotionNewJson_3, value);
	}

	inline static int32_t get_offset_of_moduleActionId_4() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___moduleActionId_4)); }
	inline int32_t get_moduleActionId_4() const { return ___moduleActionId_4; }
	inline int32_t* get_address_of_moduleActionId_4() { return &___moduleActionId_4; }
	inline void set_moduleActionId_4(int32_t value)
	{
		___moduleActionId_4 = value;
	}

	inline static int32_t get_offset_of_m_pCurveDic1_5() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___m_pCurveDic1_5)); }
	inline Dictionary_2_t926353117 * get_m_pCurveDic1_5() const { return ___m_pCurveDic1_5; }
	inline Dictionary_2_t926353117 ** get_address_of_m_pCurveDic1_5() { return &___m_pCurveDic1_5; }
	inline void set_m_pCurveDic1_5(Dictionary_2_t926353117 * value)
	{
		___m_pCurveDic1_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_pCurveDic1_5, value);
	}

	inline static int32_t get_offset_of_curve_6() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___curve_6)); }
	inline AnimationCurve_t3306541151 * get_curve_6() const { return ___curve_6; }
	inline AnimationCurve_t3306541151 ** get_address_of_curve_6() { return &___curve_6; }
	inline void set_curve_6(AnimationCurve_t3306541151 * value)
	{
		___curve_6 = value;
		Il2CppCodeGenWriteBarrier(&___curve_6, value);
	}

	inline static int32_t get_offset_of_ks_7() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___ks_7)); }
	inline KeyframeU5BU5D_t449065829* get_ks_7() const { return ___ks_7; }
	inline KeyframeU5BU5D_t449065829** get_address_of_ks_7() { return &___ks_7; }
	inline void set_ks_7(KeyframeU5BU5D_t449065829* value)
	{
		___ks_7 = value;
		Il2CppCodeGenWriteBarrier(&___ks_7, value);
	}

	inline static int32_t get_offset_of_path_8() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___path_8)); }
	inline String_t* get_path_8() const { return ___path_8; }
	inline String_t** get_address_of_path_8() { return &___path_8; }
	inline void set_path_8(String_t* value)
	{
		___path_8 = value;
		Il2CppCodeGenWriteBarrier(&___path_8, value);
	}

	inline static int32_t get_offset_of_property_9() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___property_9)); }
	inline String_t* get_property_9() const { return ___property_9; }
	inline String_t** get_address_of_property_9() { return &___property_9; }
	inline void set_property_9(String_t* value)
	{
		___property_9 = value;
		Il2CppCodeGenWriteBarrier(&___property_9, value);
	}

	inline static int32_t get_offset_of_moduleAnimClip_10() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___moduleAnimClip_10)); }
	inline AnimationClipU5BU5D_t3936083219* get_moduleAnimClip_10() const { return ___moduleAnimClip_10; }
	inline AnimationClipU5BU5D_t3936083219** get_address_of_moduleAnimClip_10() { return &___moduleAnimClip_10; }
	inline void set_moduleAnimClip_10(AnimationClipU5BU5D_t3936083219* value)
	{
		___moduleAnimClip_10 = value;
		Il2CppCodeGenWriteBarrier(&___moduleAnimClip_10, value);
	}

	inline static int32_t get_offset_of_ischangeClip_11() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___ischangeClip_11)); }
	inline bool get_ischangeClip_11() const { return ___ischangeClip_11; }
	inline bool* get_address_of_ischangeClip_11() { return &___ischangeClip_11; }
	inline void set_ischangeClip_11(bool value)
	{
		___ischangeClip_11 = value;
	}

	inline static int32_t get_offset_of_modulePoseDataCount_12() { return static_cast<int32_t>(offsetof(ApiModuleMotionManager_t2960242393, ___modulePoseDataCount_12)); }
	inline int32_t get_modulePoseDataCount_12() const { return ___modulePoseDataCount_12; }
	inline int32_t* get_address_of_modulePoseDataCount_12() { return &___modulePoseDataCount_12; }
	inline void set_modulePoseDataCount_12(int32_t value)
	{
		___modulePoseDataCount_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
