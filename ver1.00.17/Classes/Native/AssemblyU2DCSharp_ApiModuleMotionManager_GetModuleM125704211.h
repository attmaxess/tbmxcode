﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<ApiModuleMotionManager/GetModulePoseData>
struct List_1_t490911079;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleMotionManager/GetModuleMotionsData
struct  GetModuleMotionsData_t125704211  : public Il2CppObject
{
public:
	// System.String ApiModuleMotionManager/GetModuleMotionsData::posePath
	String_t* ___posePath_0;
	// System.Collections.Generic.List`1<ApiModuleMotionManager/GetModulePoseData> ApiModuleMotionManager/GetModuleMotionsData::poseData
	List_1_t490911079 * ___poseData_1;

public:
	inline static int32_t get_offset_of_posePath_0() { return static_cast<int32_t>(offsetof(GetModuleMotionsData_t125704211, ___posePath_0)); }
	inline String_t* get_posePath_0() const { return ___posePath_0; }
	inline String_t** get_address_of_posePath_0() { return &___posePath_0; }
	inline void set_posePath_0(String_t* value)
	{
		___posePath_0 = value;
		Il2CppCodeGenWriteBarrier(&___posePath_0, value);
	}

	inline static int32_t get_offset_of_poseData_1() { return static_cast<int32_t>(offsetof(GetModuleMotionsData_t125704211, ___poseData_1)); }
	inline List_1_t490911079 * get_poseData_1() const { return ___poseData_1; }
	inline List_1_t490911079 ** get_address_of_poseData_1() { return &___poseData_1; }
	inline void set_poseData_1(List_1_t490911079 * value)
	{
		___poseData_1 = value;
		Il2CppCodeGenWriteBarrier(&___poseData_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
