﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// ApiModuleMotionManager/GetModuleMotionItemData
struct GetModuleMotionItemData_t3026021381;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleMotionManager/ModuleMotionSerialization`1<ApiModuleMotionManager/GetModuleMotionItemData>
struct  ModuleMotionSerialization_1_t3983659651  : public Il2CppObject
{
public:
	// System.Int32 ApiModuleMotionManager/ModuleMotionSerialization`1::moduleId
	int32_t ___moduleId_0;
	// System.Int32 ApiModuleMotionManager/ModuleMotionSerialization`1::actionId
	int32_t ___actionId_1;
	// T ApiModuleMotionManager/ModuleMotionSerialization`1::item
	GetModuleMotionItemData_t3026021381 * ___item_2;

public:
	inline static int32_t get_offset_of_moduleId_0() { return static_cast<int32_t>(offsetof(ModuleMotionSerialization_1_t3983659651, ___moduleId_0)); }
	inline int32_t get_moduleId_0() const { return ___moduleId_0; }
	inline int32_t* get_address_of_moduleId_0() { return &___moduleId_0; }
	inline void set_moduleId_0(int32_t value)
	{
		___moduleId_0 = value;
	}

	inline static int32_t get_offset_of_actionId_1() { return static_cast<int32_t>(offsetof(ModuleMotionSerialization_1_t3983659651, ___actionId_1)); }
	inline int32_t get_actionId_1() const { return ___actionId_1; }
	inline int32_t* get_address_of_actionId_1() { return &___actionId_1; }
	inline void set_actionId_1(int32_t value)
	{
		___actionId_1 = value;
	}

	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(ModuleMotionSerialization_1_t3983659651, ___item_2)); }
	inline GetModuleMotionItemData_t3026021381 * get_item_2() const { return ___item_2; }
	inline GetModuleMotionItemData_t3026021381 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(GetModuleMotionItemData_t3026021381 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier(&___item_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
