﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1061829171.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetMotionBase
struct GetMotionBase_t403349095;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ApiModuleMotionManager
struct ApiModuleMotionManager_t2960242393;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0
struct  U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661  : public Il2CppObject
{
public:
	// System.Int32 ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::<Num>__0
	int32_t ___U3CNumU3E__0_0;
	// System.String ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::json
	String_t* ___json_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_2;
	// GetMotionBase ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::<_getMotionBase>__0
	GetMotionBase_t403349095 * ___U3C_getMotionBaseU3E__0_3;
	// System.Collections.Generic.List`1/Enumerator<GetMotionModel> ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::$locvar0
	Enumerator_t1061829171  ___U24locvar0_4;
	// UnityEngine.GameObject ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::moduleObj
	GameObject_t1756533147 * ___moduleObj_5;
	// ApiModuleMotionManager ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::$this
	ApiModuleMotionManager_t2960242393 * ___U24this_6;
	// System.Object ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 ApiModuleMotionManager/<OnGetModuleMotionSuccess>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CNumU3E__0_0() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U3CNumU3E__0_0)); }
	inline int32_t get_U3CNumU3E__0_0() const { return ___U3CNumU3E__0_0; }
	inline int32_t* get_address_of_U3CNumU3E__0_0() { return &___U3CNumU3E__0_0; }
	inline void set_U3CNumU3E__0_0(int32_t value)
	{
		___U3CNumU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___json_1)); }
	inline String_t* get_json_1() const { return ___json_1; }
	inline String_t** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(String_t* value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier(&___json_1, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_2() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U3CwwwDataU3E__0_2)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_2() const { return ___U3CwwwDataU3E__0_2; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_2() { return &___U3CwwwDataU3E__0_2; }
	inline void set_U3CwwwDataU3E__0_2(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3C_getMotionBaseU3E__0_3() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U3C_getMotionBaseU3E__0_3)); }
	inline GetMotionBase_t403349095 * get_U3C_getMotionBaseU3E__0_3() const { return ___U3C_getMotionBaseU3E__0_3; }
	inline GetMotionBase_t403349095 ** get_address_of_U3C_getMotionBaseU3E__0_3() { return &___U3C_getMotionBaseU3E__0_3; }
	inline void set_U3C_getMotionBaseU3E__0_3(GetMotionBase_t403349095 * value)
	{
		___U3C_getMotionBaseU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getMotionBaseU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U24locvar0_4)); }
	inline Enumerator_t1061829171  get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline Enumerator_t1061829171 * get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(Enumerator_t1061829171  value)
	{
		___U24locvar0_4 = value;
	}

	inline static int32_t get_offset_of_moduleObj_5() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___moduleObj_5)); }
	inline GameObject_t1756533147 * get_moduleObj_5() const { return ___moduleObj_5; }
	inline GameObject_t1756533147 ** get_address_of_moduleObj_5() { return &___moduleObj_5; }
	inline void set_moduleObj_5(GameObject_t1756533147 * value)
	{
		___moduleObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___moduleObj_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U24this_6)); }
	inline ApiModuleMotionManager_t2960242393 * get_U24this_6() const { return ___U24this_6; }
	inline ApiModuleMotionManager_t2960242393 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ApiModuleMotionManager_t2960242393 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
