﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve>
struct Dictionary_2_t926353117;
// UnityEngine.Animation
struct Animation_t2068071072;
// ApiModuleMotionManager
struct ApiModuleMotionManager_t2960242393;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiModuleMotionManager/<moduleAddClip>c__Iterator1
struct  U3CmoduleAddClipU3Ec__Iterator1_t1418277765  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> ApiModuleMotionManager/<moduleAddClip>c__Iterator1::animDic
	Dictionary_2_t926353117 * ___animDic_0;
	// UnityEngine.Animation ApiModuleMotionManager/<moduleAddClip>c__Iterator1::coreAnim
	Animation_t2068071072 * ___coreAnim_1;
	// ApiModuleMotionManager ApiModuleMotionManager/<moduleAddClip>c__Iterator1::$this
	ApiModuleMotionManager_t2960242393 * ___U24this_2;
	// System.Object ApiModuleMotionManager/<moduleAddClip>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean ApiModuleMotionManager/<moduleAddClip>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 ApiModuleMotionManager/<moduleAddClip>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_animDic_0() { return static_cast<int32_t>(offsetof(U3CmoduleAddClipU3Ec__Iterator1_t1418277765, ___animDic_0)); }
	inline Dictionary_2_t926353117 * get_animDic_0() const { return ___animDic_0; }
	inline Dictionary_2_t926353117 ** get_address_of_animDic_0() { return &___animDic_0; }
	inline void set_animDic_0(Dictionary_2_t926353117 * value)
	{
		___animDic_0 = value;
		Il2CppCodeGenWriteBarrier(&___animDic_0, value);
	}

	inline static int32_t get_offset_of_coreAnim_1() { return static_cast<int32_t>(offsetof(U3CmoduleAddClipU3Ec__Iterator1_t1418277765, ___coreAnim_1)); }
	inline Animation_t2068071072 * get_coreAnim_1() const { return ___coreAnim_1; }
	inline Animation_t2068071072 ** get_address_of_coreAnim_1() { return &___coreAnim_1; }
	inline void set_coreAnim_1(Animation_t2068071072 * value)
	{
		___coreAnim_1 = value;
		Il2CppCodeGenWriteBarrier(&___coreAnim_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CmoduleAddClipU3Ec__Iterator1_t1418277765, ___U24this_2)); }
	inline ApiModuleMotionManager_t2960242393 * get_U24this_2() const { return ___U24this_2; }
	inline ApiModuleMotionManager_t2960242393 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ApiModuleMotionManager_t2960242393 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CmoduleAddClipU3Ec__Iterator1_t1418277765, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CmoduleAddClipU3Ec__Iterator1_t1418277765, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CmoduleAddClipU3Ec__Iterator1_t1418277765, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
