﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1840775948.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiNearmobManager
struct  ApiNearmobManager_t3144099613  : public SingletonMonoBehaviour_1_t1840775948
{
public:
	// System.Single ApiNearmobManager::RespawnTime
	float ___RespawnTime_3;
	// System.String ApiNearmobManager::RecMobNum
	String_t* ___RecMobNum_4;
	// System.Int32 ApiNearmobManager::createdUserId
	int32_t ___createdUserId_5;

public:
	inline static int32_t get_offset_of_RespawnTime_3() { return static_cast<int32_t>(offsetof(ApiNearmobManager_t3144099613, ___RespawnTime_3)); }
	inline float get_RespawnTime_3() const { return ___RespawnTime_3; }
	inline float* get_address_of_RespawnTime_3() { return &___RespawnTime_3; }
	inline void set_RespawnTime_3(float value)
	{
		___RespawnTime_3 = value;
	}

	inline static int32_t get_offset_of_RecMobNum_4() { return static_cast<int32_t>(offsetof(ApiNearmobManager_t3144099613, ___RecMobNum_4)); }
	inline String_t* get_RecMobNum_4() const { return ___RecMobNum_4; }
	inline String_t** get_address_of_RecMobNum_4() { return &___RecMobNum_4; }
	inline void set_RecMobNum_4(String_t* value)
	{
		___RecMobNum_4 = value;
		Il2CppCodeGenWriteBarrier(&___RecMobNum_4, value);
	}

	inline static int32_t get_offset_of_createdUserId_5() { return static_cast<int32_t>(offsetof(ApiNearmobManager_t3144099613, ___createdUserId_5)); }
	inline int32_t get_createdUserId_5() const { return ___createdUserId_5; }
	inline int32_t* get_address_of_createdUserId_5() { return &___createdUserId_5; }
	inline void set_createdUserId_5(int32_t value)
	{
		___createdUserId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
