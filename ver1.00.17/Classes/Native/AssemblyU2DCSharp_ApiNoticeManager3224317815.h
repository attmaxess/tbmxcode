﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1920994150.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.List`1<NoticeListModel>
struct List_1_t686153777;
// System.Collections.Generic.List`1<ApiNoticeManager/GetNoticeDetail>
struct List_1_t3819954433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiNoticeManager
struct  ApiNoticeManager_t3224317815  : public SingletonMonoBehaviour_1_t1920994150
{
public:
	// UnityEngine.UI.Text ApiNoticeManager::Id
	Text_t356221433 * ___Id_3;
	// UnityEngine.UI.Text ApiNoticeManager::Notice
	Text_t356221433 * ___Notice_4;
	// UnityEngine.UI.Text ApiNoticeManager::CreatedAt
	Text_t356221433 * ___CreatedAt_5;
	// UnityEngine.UI.Text ApiNoticeManager::Title
	Text_t356221433 * ___Title_6;
	// UnityEngine.UI.Text ApiNoticeManager::Url
	Text_t356221433 * ___Url_7;
	// System.Collections.Generic.List`1<NoticeListModel> ApiNoticeManager::_noticeListData
	List_1_t686153777 * ____noticeListData_8;
	// System.Collections.Generic.List`1<ApiNoticeManager/GetNoticeDetail> ApiNoticeManager::NoticeDetail
	List_1_t3819954433 * ___NoticeDetail_9;

public:
	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(ApiNoticeManager_t3224317815, ___Id_3)); }
	inline Text_t356221433 * get_Id_3() const { return ___Id_3; }
	inline Text_t356221433 ** get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(Text_t356221433 * value)
	{
		___Id_3 = value;
		Il2CppCodeGenWriteBarrier(&___Id_3, value);
	}

	inline static int32_t get_offset_of_Notice_4() { return static_cast<int32_t>(offsetof(ApiNoticeManager_t3224317815, ___Notice_4)); }
	inline Text_t356221433 * get_Notice_4() const { return ___Notice_4; }
	inline Text_t356221433 ** get_address_of_Notice_4() { return &___Notice_4; }
	inline void set_Notice_4(Text_t356221433 * value)
	{
		___Notice_4 = value;
		Il2CppCodeGenWriteBarrier(&___Notice_4, value);
	}

	inline static int32_t get_offset_of_CreatedAt_5() { return static_cast<int32_t>(offsetof(ApiNoticeManager_t3224317815, ___CreatedAt_5)); }
	inline Text_t356221433 * get_CreatedAt_5() const { return ___CreatedAt_5; }
	inline Text_t356221433 ** get_address_of_CreatedAt_5() { return &___CreatedAt_5; }
	inline void set_CreatedAt_5(Text_t356221433 * value)
	{
		___CreatedAt_5 = value;
		Il2CppCodeGenWriteBarrier(&___CreatedAt_5, value);
	}

	inline static int32_t get_offset_of_Title_6() { return static_cast<int32_t>(offsetof(ApiNoticeManager_t3224317815, ___Title_6)); }
	inline Text_t356221433 * get_Title_6() const { return ___Title_6; }
	inline Text_t356221433 ** get_address_of_Title_6() { return &___Title_6; }
	inline void set_Title_6(Text_t356221433 * value)
	{
		___Title_6 = value;
		Il2CppCodeGenWriteBarrier(&___Title_6, value);
	}

	inline static int32_t get_offset_of_Url_7() { return static_cast<int32_t>(offsetof(ApiNoticeManager_t3224317815, ___Url_7)); }
	inline Text_t356221433 * get_Url_7() const { return ___Url_7; }
	inline Text_t356221433 ** get_address_of_Url_7() { return &___Url_7; }
	inline void set_Url_7(Text_t356221433 * value)
	{
		___Url_7 = value;
		Il2CppCodeGenWriteBarrier(&___Url_7, value);
	}

	inline static int32_t get_offset_of__noticeListData_8() { return static_cast<int32_t>(offsetof(ApiNoticeManager_t3224317815, ____noticeListData_8)); }
	inline List_1_t686153777 * get__noticeListData_8() const { return ____noticeListData_8; }
	inline List_1_t686153777 ** get_address_of__noticeListData_8() { return &____noticeListData_8; }
	inline void set__noticeListData_8(List_1_t686153777 * value)
	{
		____noticeListData_8 = value;
		Il2CppCodeGenWriteBarrier(&____noticeListData_8, value);
	}

	inline static int32_t get_offset_of_NoticeDetail_9() { return static_cast<int32_t>(offsetof(ApiNoticeManager_t3224317815, ___NoticeDetail_9)); }
	inline List_1_t3819954433 * get_NoticeDetail_9() const { return ___NoticeDetail_9; }
	inline List_1_t3819954433 ** get_address_of_NoticeDetail_9() { return &___NoticeDetail_9; }
	inline void set_NoticeDetail_9(List_1_t3819954433 * value)
	{
		___NoticeDetail_9 = value;
		Il2CppCodeGenWriteBarrier(&___NoticeDetail_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
