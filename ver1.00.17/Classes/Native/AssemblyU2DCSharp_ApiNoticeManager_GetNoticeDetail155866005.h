﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiNoticeManager/GetNoticeDetail
struct  GetNoticeDetail_t155866005  : public Il2CppObject
{
public:
	// System.Int32 ApiNoticeManager/GetNoticeDetail::Id
	int32_t ___Id_0;
	// System.String ApiNoticeManager/GetNoticeDetail::Notice
	String_t* ___Notice_1;
	// System.String ApiNoticeManager/GetNoticeDetail::CreatedAt
	String_t* ___CreatedAt_2;
	// System.String ApiNoticeManager/GetNoticeDetail::Title
	String_t* ___Title_3;
	// System.String ApiNoticeManager/GetNoticeDetail::Url
	String_t* ___Url_4;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GetNoticeDetail_t155866005, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Notice_1() { return static_cast<int32_t>(offsetof(GetNoticeDetail_t155866005, ___Notice_1)); }
	inline String_t* get_Notice_1() const { return ___Notice_1; }
	inline String_t** get_address_of_Notice_1() { return &___Notice_1; }
	inline void set_Notice_1(String_t* value)
	{
		___Notice_1 = value;
		Il2CppCodeGenWriteBarrier(&___Notice_1, value);
	}

	inline static int32_t get_offset_of_CreatedAt_2() { return static_cast<int32_t>(offsetof(GetNoticeDetail_t155866005, ___CreatedAt_2)); }
	inline String_t* get_CreatedAt_2() const { return ___CreatedAt_2; }
	inline String_t** get_address_of_CreatedAt_2() { return &___CreatedAt_2; }
	inline void set_CreatedAt_2(String_t* value)
	{
		___CreatedAt_2 = value;
		Il2CppCodeGenWriteBarrier(&___CreatedAt_2, value);
	}

	inline static int32_t get_offset_of_Title_3() { return static_cast<int32_t>(offsetof(GetNoticeDetail_t155866005, ___Title_3)); }
	inline String_t* get_Title_3() const { return ___Title_3; }
	inline String_t** get_address_of_Title_3() { return &___Title_3; }
	inline void set_Title_3(String_t* value)
	{
		___Title_3 = value;
		Il2CppCodeGenWriteBarrier(&___Title_3, value);
	}

	inline static int32_t get_offset_of_Url_4() { return static_cast<int32_t>(offsetof(GetNoticeDetail_t155866005, ___Url_4)); }
	inline String_t* get_Url_4() const { return ___Url_4; }
	inline String_t** get_address_of_Url_4() { return &___Url_4; }
	inline void set_Url_4(String_t* value)
	{
		___Url_4 = value;
		Il2CppCodeGenWriteBarrier(&___Url_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
