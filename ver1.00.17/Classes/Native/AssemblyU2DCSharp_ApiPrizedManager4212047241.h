﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2908723576.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiPrizedManager
struct  ApiPrizedManager_t4212047241  : public SingletonMonoBehaviour_1_t2908723576
{
public:
	// System.String ApiPrizedManager::prizedJson
	String_t* ___prizedJson_3;
	// System.String ApiPrizedManager::eventhistoryJson
	String_t* ___eventhistoryJson_4;

public:
	inline static int32_t get_offset_of_prizedJson_3() { return static_cast<int32_t>(offsetof(ApiPrizedManager_t4212047241, ___prizedJson_3)); }
	inline String_t* get_prizedJson_3() const { return ___prizedJson_3; }
	inline String_t** get_address_of_prizedJson_3() { return &___prizedJson_3; }
	inline void set_prizedJson_3(String_t* value)
	{
		___prizedJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___prizedJson_3, value);
	}

	inline static int32_t get_offset_of_eventhistoryJson_4() { return static_cast<int32_t>(offsetof(ApiPrizedManager_t4212047241, ___eventhistoryJson_4)); }
	inline String_t* get_eventhistoryJson_4() const { return ___eventhistoryJson_4; }
	inline String_t** get_address_of_eventhistoryJson_4() { return &___eventhistoryJson_4; }
	inline void set_eventhistoryJson_4(String_t* value)
	{
		___eventhistoryJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___eventhistoryJson_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
