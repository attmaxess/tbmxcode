﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiPrizedManager/GetEventhistoryJson
struct  GetEventhistoryJson_t4277162862  : public Il2CppObject
{
public:
	// System.Int32 ApiPrizedManager/GetEventhistoryJson::mobilityId
	int32_t ___mobilityId_0;
	// System.Int32 ApiPrizedManager/GetEventhistoryJson::point
	int32_t ___point_1;
	// System.Int32 ApiPrizedManager/GetEventhistoryJson::eventId
	int32_t ___eventId_2;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t4277162862, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}

	inline static int32_t get_offset_of_point_1() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t4277162862, ___point_1)); }
	inline int32_t get_point_1() const { return ___point_1; }
	inline int32_t* get_address_of_point_1() { return &___point_1; }
	inline void set_point_1(int32_t value)
	{
		___point_1 = value;
	}

	inline static int32_t get_offset_of_eventId_2() { return static_cast<int32_t>(offsetof(GetEventhistoryJson_t4277162862, ___eventId_2)); }
	inline int32_t get_eventId_2() const { return ___eventId_2; }
	inline int32_t* get_address_of_eventId_2() { return &___eventId_2; }
	inline void set_eventId_2(int32_t value)
	{
		___eventId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
