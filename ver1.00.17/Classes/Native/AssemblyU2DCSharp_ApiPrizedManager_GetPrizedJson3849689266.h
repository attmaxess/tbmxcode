﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiPrizedManager/GetPrizedJson
struct  GetPrizedJson_t3849689266  : public Il2CppObject
{
public:
	// System.Int32 ApiPrizedManager/GetPrizedJson::prizedMasterId
	int32_t ___prizedMasterId_0;
	// System.Int32 ApiPrizedManager/GetPrizedJson::mobilityId
	int32_t ___mobilityId_1;

public:
	inline static int32_t get_offset_of_prizedMasterId_0() { return static_cast<int32_t>(offsetof(GetPrizedJson_t3849689266, ___prizedMasterId_0)); }
	inline int32_t get_prizedMasterId_0() const { return ___prizedMasterId_0; }
	inline int32_t* get_address_of_prizedMasterId_0() { return &___prizedMasterId_0; }
	inline void set_prizedMasterId_0(int32_t value)
	{
		___prizedMasterId_0 = value;
	}

	inline static int32_t get_offset_of_mobilityId_1() { return static_cast<int32_t>(offsetof(GetPrizedJson_t3849689266, ___mobilityId_1)); }
	inline int32_t get_mobilityId_1() const { return ___mobilityId_1; }
	inline int32_t* get_address_of_mobilityId_1() { return &___mobilityId_1; }
	inline void set_mobilityId_1(int32_t value)
	{
		___mobilityId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
