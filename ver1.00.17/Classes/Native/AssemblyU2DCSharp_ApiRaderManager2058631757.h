﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen755308092.h"

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiRaderManager
struct  ApiRaderManager_t2058631757  : public SingletonMonoBehaviour_1_t755308092
{
public:
	// UnityEngine.Transform[] ApiRaderManager::raderAreaPos
	TransformU5BU5D_t3764228911* ___raderAreaPos_3;
	// UnityEngine.GameObject ApiRaderManager::raderMobilmoObj
	GameObject_t1756533147 * ___raderMobilmoObj_4;
	// UnityEngine.Transform ApiRaderManager::raderArea
	Transform_t3275118058 * ___raderArea_5;
	// System.Int32 ApiRaderManager::raderMobCnt
	int32_t ___raderMobCnt_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ApiRaderManager::raderObjList
	List_1_t1125654279 * ___raderObjList_7;
	// System.Int32 ApiRaderManager::selectedModId
	int32_t ___selectedModId_8;

public:
	inline static int32_t get_offset_of_raderAreaPos_3() { return static_cast<int32_t>(offsetof(ApiRaderManager_t2058631757, ___raderAreaPos_3)); }
	inline TransformU5BU5D_t3764228911* get_raderAreaPos_3() const { return ___raderAreaPos_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_raderAreaPos_3() { return &___raderAreaPos_3; }
	inline void set_raderAreaPos_3(TransformU5BU5D_t3764228911* value)
	{
		___raderAreaPos_3 = value;
		Il2CppCodeGenWriteBarrier(&___raderAreaPos_3, value);
	}

	inline static int32_t get_offset_of_raderMobilmoObj_4() { return static_cast<int32_t>(offsetof(ApiRaderManager_t2058631757, ___raderMobilmoObj_4)); }
	inline GameObject_t1756533147 * get_raderMobilmoObj_4() const { return ___raderMobilmoObj_4; }
	inline GameObject_t1756533147 ** get_address_of_raderMobilmoObj_4() { return &___raderMobilmoObj_4; }
	inline void set_raderMobilmoObj_4(GameObject_t1756533147 * value)
	{
		___raderMobilmoObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___raderMobilmoObj_4, value);
	}

	inline static int32_t get_offset_of_raderArea_5() { return static_cast<int32_t>(offsetof(ApiRaderManager_t2058631757, ___raderArea_5)); }
	inline Transform_t3275118058 * get_raderArea_5() const { return ___raderArea_5; }
	inline Transform_t3275118058 ** get_address_of_raderArea_5() { return &___raderArea_5; }
	inline void set_raderArea_5(Transform_t3275118058 * value)
	{
		___raderArea_5 = value;
		Il2CppCodeGenWriteBarrier(&___raderArea_5, value);
	}

	inline static int32_t get_offset_of_raderMobCnt_6() { return static_cast<int32_t>(offsetof(ApiRaderManager_t2058631757, ___raderMobCnt_6)); }
	inline int32_t get_raderMobCnt_6() const { return ___raderMobCnt_6; }
	inline int32_t* get_address_of_raderMobCnt_6() { return &___raderMobCnt_6; }
	inline void set_raderMobCnt_6(int32_t value)
	{
		___raderMobCnt_6 = value;
	}

	inline static int32_t get_offset_of_raderObjList_7() { return static_cast<int32_t>(offsetof(ApiRaderManager_t2058631757, ___raderObjList_7)); }
	inline List_1_t1125654279 * get_raderObjList_7() const { return ___raderObjList_7; }
	inline List_1_t1125654279 ** get_address_of_raderObjList_7() { return &___raderObjList_7; }
	inline void set_raderObjList_7(List_1_t1125654279 * value)
	{
		___raderObjList_7 = value;
		Il2CppCodeGenWriteBarrier(&___raderObjList_7, value);
	}

	inline static int32_t get_offset_of_selectedModId_8() { return static_cast<int32_t>(offsetof(ApiRaderManager_t2058631757, ___selectedModId_8)); }
	inline int32_t get_selectedModId_8() const { return ___selectedModId_8; }
	inline int32_t* get_address_of_selectedModId_8() { return &___selectedModId_8; }
	inline void set_selectedModId_8(int32_t value)
	{
		___selectedModId_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
