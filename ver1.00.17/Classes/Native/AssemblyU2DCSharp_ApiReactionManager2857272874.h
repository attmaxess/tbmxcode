﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1553949209.h"

// System.String
struct String_t;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<ApiReactionManager/GetReactionList>
struct List_1_t2654601182;
// System.Collections.Generic.Dictionary`2<System.String,ApiReactionManager/GetReactionList>
struct Dictionary_2_t905292016;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiReactionManager
struct  ApiReactionManager_t2857272874  : public SingletonMonoBehaviour_1_t1553949209
{
public:
	// System.String ApiReactionManager::reactionAddJsonData
	String_t* ___reactionAddJsonData_3;
	// System.String ApiReactionManager::reactionNewJson
	String_t* ___reactionNewJson_4;
	// UnityEngine.UI.Toggle ApiReactionManager::FUNNYBtn
	Toggle_t3976754468 * ___FUNNYBtn_5;
	// UnityEngine.UI.Toggle ApiReactionManager::AMAZINGBtn
	Toggle_t3976754468 * ___AMAZINGBtn_6;
	// UnityEngine.UI.Toggle ApiReactionManager::CUTEBtn
	Toggle_t3976754468 * ___CUTEBtn_7;
	// UnityEngine.UI.Toggle ApiReactionManager::COOLBtn
	Toggle_t3976754468 * ___COOLBtn_8;
	// UnityEngine.UI.Toggle ApiReactionManager::LIKEBtn
	Toggle_t3976754468 * ___LIKEBtn_9;
	// System.Int32 ApiReactionManager::m_ifunny
	int32_t ___m_ifunny_10;
	// System.Int32 ApiReactionManager::m_iamazing
	int32_t ___m_iamazing_11;
	// System.Int32 ApiReactionManager::m_icute
	int32_t ___m_icute_12;
	// System.Int32 ApiReactionManager::m_icool
	int32_t ___m_icool_13;
	// System.Int32 ApiReactionManager::m_ilike
	int32_t ___m_ilike_14;
	// System.Int32 ApiReactionManager::reactionAllCnt
	int32_t ___reactionAllCnt_20;
	// UnityEngine.UI.Text ApiReactionManager::heartCntText
	Text_t356221433 * ___heartCntText_21;
	// UnityEngine.GameObject ApiReactionManager::selectRecMobilmoObj
	GameObject_t1756533147 * ___selectRecMobilmoObj_22;
	// System.Int32 ApiReactionManager::recationMobimoId
	int32_t ___recationMobimoId_23;
	// System.Collections.Generic.List`1<ApiReactionManager/GetReactionList> ApiReactionManager::newReactionlist
	List_1_t2654601182 * ___newReactionlist_24;
	// System.Collections.Generic.Dictionary`2<System.String,ApiReactionManager/GetReactionList> ApiReactionManager::newReactionDataList
	Dictionary_2_t905292016 * ___newReactionDataList_25;

public:
	inline static int32_t get_offset_of_reactionAddJsonData_3() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___reactionAddJsonData_3)); }
	inline String_t* get_reactionAddJsonData_3() const { return ___reactionAddJsonData_3; }
	inline String_t** get_address_of_reactionAddJsonData_3() { return &___reactionAddJsonData_3; }
	inline void set_reactionAddJsonData_3(String_t* value)
	{
		___reactionAddJsonData_3 = value;
		Il2CppCodeGenWriteBarrier(&___reactionAddJsonData_3, value);
	}

	inline static int32_t get_offset_of_reactionNewJson_4() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___reactionNewJson_4)); }
	inline String_t* get_reactionNewJson_4() const { return ___reactionNewJson_4; }
	inline String_t** get_address_of_reactionNewJson_4() { return &___reactionNewJson_4; }
	inline void set_reactionNewJson_4(String_t* value)
	{
		___reactionNewJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___reactionNewJson_4, value);
	}

	inline static int32_t get_offset_of_FUNNYBtn_5() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___FUNNYBtn_5)); }
	inline Toggle_t3976754468 * get_FUNNYBtn_5() const { return ___FUNNYBtn_5; }
	inline Toggle_t3976754468 ** get_address_of_FUNNYBtn_5() { return &___FUNNYBtn_5; }
	inline void set_FUNNYBtn_5(Toggle_t3976754468 * value)
	{
		___FUNNYBtn_5 = value;
		Il2CppCodeGenWriteBarrier(&___FUNNYBtn_5, value);
	}

	inline static int32_t get_offset_of_AMAZINGBtn_6() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___AMAZINGBtn_6)); }
	inline Toggle_t3976754468 * get_AMAZINGBtn_6() const { return ___AMAZINGBtn_6; }
	inline Toggle_t3976754468 ** get_address_of_AMAZINGBtn_6() { return &___AMAZINGBtn_6; }
	inline void set_AMAZINGBtn_6(Toggle_t3976754468 * value)
	{
		___AMAZINGBtn_6 = value;
		Il2CppCodeGenWriteBarrier(&___AMAZINGBtn_6, value);
	}

	inline static int32_t get_offset_of_CUTEBtn_7() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___CUTEBtn_7)); }
	inline Toggle_t3976754468 * get_CUTEBtn_7() const { return ___CUTEBtn_7; }
	inline Toggle_t3976754468 ** get_address_of_CUTEBtn_7() { return &___CUTEBtn_7; }
	inline void set_CUTEBtn_7(Toggle_t3976754468 * value)
	{
		___CUTEBtn_7 = value;
		Il2CppCodeGenWriteBarrier(&___CUTEBtn_7, value);
	}

	inline static int32_t get_offset_of_COOLBtn_8() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___COOLBtn_8)); }
	inline Toggle_t3976754468 * get_COOLBtn_8() const { return ___COOLBtn_8; }
	inline Toggle_t3976754468 ** get_address_of_COOLBtn_8() { return &___COOLBtn_8; }
	inline void set_COOLBtn_8(Toggle_t3976754468 * value)
	{
		___COOLBtn_8 = value;
		Il2CppCodeGenWriteBarrier(&___COOLBtn_8, value);
	}

	inline static int32_t get_offset_of_LIKEBtn_9() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___LIKEBtn_9)); }
	inline Toggle_t3976754468 * get_LIKEBtn_9() const { return ___LIKEBtn_9; }
	inline Toggle_t3976754468 ** get_address_of_LIKEBtn_9() { return &___LIKEBtn_9; }
	inline void set_LIKEBtn_9(Toggle_t3976754468 * value)
	{
		___LIKEBtn_9 = value;
		Il2CppCodeGenWriteBarrier(&___LIKEBtn_9, value);
	}

	inline static int32_t get_offset_of_m_ifunny_10() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___m_ifunny_10)); }
	inline int32_t get_m_ifunny_10() const { return ___m_ifunny_10; }
	inline int32_t* get_address_of_m_ifunny_10() { return &___m_ifunny_10; }
	inline void set_m_ifunny_10(int32_t value)
	{
		___m_ifunny_10 = value;
	}

	inline static int32_t get_offset_of_m_iamazing_11() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___m_iamazing_11)); }
	inline int32_t get_m_iamazing_11() const { return ___m_iamazing_11; }
	inline int32_t* get_address_of_m_iamazing_11() { return &___m_iamazing_11; }
	inline void set_m_iamazing_11(int32_t value)
	{
		___m_iamazing_11 = value;
	}

	inline static int32_t get_offset_of_m_icute_12() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___m_icute_12)); }
	inline int32_t get_m_icute_12() const { return ___m_icute_12; }
	inline int32_t* get_address_of_m_icute_12() { return &___m_icute_12; }
	inline void set_m_icute_12(int32_t value)
	{
		___m_icute_12 = value;
	}

	inline static int32_t get_offset_of_m_icool_13() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___m_icool_13)); }
	inline int32_t get_m_icool_13() const { return ___m_icool_13; }
	inline int32_t* get_address_of_m_icool_13() { return &___m_icool_13; }
	inline void set_m_icool_13(int32_t value)
	{
		___m_icool_13 = value;
	}

	inline static int32_t get_offset_of_m_ilike_14() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___m_ilike_14)); }
	inline int32_t get_m_ilike_14() const { return ___m_ilike_14; }
	inline int32_t* get_address_of_m_ilike_14() { return &___m_ilike_14; }
	inline void set_m_ilike_14(int32_t value)
	{
		___m_ilike_14 = value;
	}

	inline static int32_t get_offset_of_reactionAllCnt_20() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___reactionAllCnt_20)); }
	inline int32_t get_reactionAllCnt_20() const { return ___reactionAllCnt_20; }
	inline int32_t* get_address_of_reactionAllCnt_20() { return &___reactionAllCnt_20; }
	inline void set_reactionAllCnt_20(int32_t value)
	{
		___reactionAllCnt_20 = value;
	}

	inline static int32_t get_offset_of_heartCntText_21() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___heartCntText_21)); }
	inline Text_t356221433 * get_heartCntText_21() const { return ___heartCntText_21; }
	inline Text_t356221433 ** get_address_of_heartCntText_21() { return &___heartCntText_21; }
	inline void set_heartCntText_21(Text_t356221433 * value)
	{
		___heartCntText_21 = value;
		Il2CppCodeGenWriteBarrier(&___heartCntText_21, value);
	}

	inline static int32_t get_offset_of_selectRecMobilmoObj_22() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___selectRecMobilmoObj_22)); }
	inline GameObject_t1756533147 * get_selectRecMobilmoObj_22() const { return ___selectRecMobilmoObj_22; }
	inline GameObject_t1756533147 ** get_address_of_selectRecMobilmoObj_22() { return &___selectRecMobilmoObj_22; }
	inline void set_selectRecMobilmoObj_22(GameObject_t1756533147 * value)
	{
		___selectRecMobilmoObj_22 = value;
		Il2CppCodeGenWriteBarrier(&___selectRecMobilmoObj_22, value);
	}

	inline static int32_t get_offset_of_recationMobimoId_23() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___recationMobimoId_23)); }
	inline int32_t get_recationMobimoId_23() const { return ___recationMobimoId_23; }
	inline int32_t* get_address_of_recationMobimoId_23() { return &___recationMobimoId_23; }
	inline void set_recationMobimoId_23(int32_t value)
	{
		___recationMobimoId_23 = value;
	}

	inline static int32_t get_offset_of_newReactionlist_24() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___newReactionlist_24)); }
	inline List_1_t2654601182 * get_newReactionlist_24() const { return ___newReactionlist_24; }
	inline List_1_t2654601182 ** get_address_of_newReactionlist_24() { return &___newReactionlist_24; }
	inline void set_newReactionlist_24(List_1_t2654601182 * value)
	{
		___newReactionlist_24 = value;
		Il2CppCodeGenWriteBarrier(&___newReactionlist_24, value);
	}

	inline static int32_t get_offset_of_newReactionDataList_25() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874, ___newReactionDataList_25)); }
	inline Dictionary_2_t905292016 * get_newReactionDataList_25() const { return ___newReactionDataList_25; }
	inline Dictionary_2_t905292016 ** get_address_of_newReactionDataList_25() { return &___newReactionDataList_25; }
	inline void set_newReactionDataList_25(Dictionary_2_t905292016 * value)
	{
		___newReactionDataList_25 = value;
		Il2CppCodeGenWriteBarrier(&___newReactionDataList_25, value);
	}
};

struct ApiReactionManager_t2857272874_StaticFields
{
public:
	// System.String ApiReactionManager::m_sFunny
	String_t* ___m_sFunny_15;
	// System.String ApiReactionManager::m_sAmazing
	String_t* ___m_sAmazing_16;
	// System.String ApiReactionManager::m_sCute
	String_t* ___m_sCute_17;
	// System.String ApiReactionManager::m_sCool
	String_t* ___m_sCool_18;
	// System.String ApiReactionManager::m_sLike
	String_t* ___m_sLike_19;

public:
	inline static int32_t get_offset_of_m_sFunny_15() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874_StaticFields, ___m_sFunny_15)); }
	inline String_t* get_m_sFunny_15() const { return ___m_sFunny_15; }
	inline String_t** get_address_of_m_sFunny_15() { return &___m_sFunny_15; }
	inline void set_m_sFunny_15(String_t* value)
	{
		___m_sFunny_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_sFunny_15, value);
	}

	inline static int32_t get_offset_of_m_sAmazing_16() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874_StaticFields, ___m_sAmazing_16)); }
	inline String_t* get_m_sAmazing_16() const { return ___m_sAmazing_16; }
	inline String_t** get_address_of_m_sAmazing_16() { return &___m_sAmazing_16; }
	inline void set_m_sAmazing_16(String_t* value)
	{
		___m_sAmazing_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_sAmazing_16, value);
	}

	inline static int32_t get_offset_of_m_sCute_17() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874_StaticFields, ___m_sCute_17)); }
	inline String_t* get_m_sCute_17() const { return ___m_sCute_17; }
	inline String_t** get_address_of_m_sCute_17() { return &___m_sCute_17; }
	inline void set_m_sCute_17(String_t* value)
	{
		___m_sCute_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_sCute_17, value);
	}

	inline static int32_t get_offset_of_m_sCool_18() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874_StaticFields, ___m_sCool_18)); }
	inline String_t* get_m_sCool_18() const { return ___m_sCool_18; }
	inline String_t** get_address_of_m_sCool_18() { return &___m_sCool_18; }
	inline void set_m_sCool_18(String_t* value)
	{
		___m_sCool_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_sCool_18, value);
	}

	inline static int32_t get_offset_of_m_sLike_19() { return static_cast<int32_t>(offsetof(ApiReactionManager_t2857272874_StaticFields, ___m_sLike_19)); }
	inline String_t* get_m_sLike_19() const { return ___m_sLike_19; }
	inline String_t** get_address_of_m_sLike_19() { return &___m_sLike_19; }
	inline void set_m_sLike_19(String_t* value)
	{
		___m_sLike_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_sLike_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
