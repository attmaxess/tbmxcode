﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiReactionManager/GetReactionList
struct  GetReactionList_t3285480050  : public Il2CppObject
{
public:
	// System.String ApiReactionManager/GetReactionList::reactionType
	String_t* ___reactionType_0;
	// System.Int32 ApiReactionManager/GetReactionList::flg
	int32_t ___flg_1;

public:
	inline static int32_t get_offset_of_reactionType_0() { return static_cast<int32_t>(offsetof(GetReactionList_t3285480050, ___reactionType_0)); }
	inline String_t* get_reactionType_0() const { return ___reactionType_0; }
	inline String_t** get_address_of_reactionType_0() { return &___reactionType_0; }
	inline void set_reactionType_0(String_t* value)
	{
		___reactionType_0 = value;
		Il2CppCodeGenWriteBarrier(&___reactionType_0, value);
	}

	inline static int32_t get_offset_of_flg_1() { return static_cast<int32_t>(offsetof(GetReactionList_t3285480050, ___flg_1)); }
	inline int32_t get_flg_1() const { return ___flg_1; }
	inline int32_t* get_address_of_flg_1() { return &___flg_1; }
	inline void set_flg_1(int32_t value)
	{
		___flg_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
