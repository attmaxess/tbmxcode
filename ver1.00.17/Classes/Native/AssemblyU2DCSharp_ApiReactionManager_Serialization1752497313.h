﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<ApiReactionManager/GetReactionList>
struct List_1_t2654601182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiReactionManager/Serialization`1<ApiReactionManager/GetReactionList>
struct  Serialization_1_t1752497313  : public Il2CppObject
{
public:
	// System.Int32 ApiReactionManager/Serialization`1::mobilityId
	int32_t ___mobilityId_0;
	// System.Int32 ApiReactionManager/Serialization`1::userId
	int32_t ___userId_1;
	// System.Collections.Generic.List`1<T> ApiReactionManager/Serialization`1::reactions
	List_1_t2654601182 * ___reactions_2;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(Serialization_1_t1752497313, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(Serialization_1_t1752497313, ___userId_1)); }
	inline int32_t get_userId_1() const { return ___userId_1; }
	inline int32_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(int32_t value)
	{
		___userId_1 = value;
	}

	inline static int32_t get_offset_of_reactions_2() { return static_cast<int32_t>(offsetof(Serialization_1_t1752497313, ___reactions_2)); }
	inline List_1_t2654601182 * get_reactions_2() const { return ___reactions_2; }
	inline List_1_t2654601182 ** get_address_of_reactions_2() { return &___reactions_2; }
	inline void set_reactions_2(List_1_t2654601182 * value)
	{
		___reactions_2 = value;
		Il2CppCodeGenWriteBarrier(&___reactions_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
