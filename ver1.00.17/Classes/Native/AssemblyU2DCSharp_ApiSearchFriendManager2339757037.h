﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1036433372.h"

// UnityEngine.Events.UnityAction`3<System.String,System.String,System.Int32>
struct UnityAction_3_t1370633073;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiSearchFriendManager
struct  ApiSearchFriendManager_t2339757037  : public SingletonMonoBehaviour_1_t1036433372
{
public:
	// System.Boolean ApiSearchFriendManager::isReachedResult
	bool ___isReachedResult_7;

public:
	inline static int32_t get_offset_of_isReachedResult_7() { return static_cast<int32_t>(offsetof(ApiSearchFriendManager_t2339757037, ___isReachedResult_7)); }
	inline bool get_isReachedResult_7() const { return ___isReachedResult_7; }
	inline bool* get_address_of_isReachedResult_7() { return &___isReachedResult_7; }
	inline void set_isReachedResult_7(bool value)
	{
		___isReachedResult_7 = value;
	}
};

struct ApiSearchFriendManager_t2339757037_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<System.String,System.String,System.Int32> ApiSearchFriendManager::OnGetDataUserEvent
	UnityAction_3_t1370633073 * ___OnGetDataUserEvent_3;
	// UnityEngine.Events.UnityAction ApiSearchFriendManager::OnAnalizeDataEvent
	UnityAction_t4025899511 * ___OnAnalizeDataEvent_4;
	// UnityEngine.Events.UnityAction`1<System.Boolean> ApiSearchFriendManager::OnNoResultEvent
	UnityAction_1_t897193173 * ___OnNoResultEvent_5;
	// UnityEngine.Events.UnityAction`1<System.Boolean> ApiSearchFriendManager::ShowHistoryTextEvent
	UnityAction_1_t897193173 * ___ShowHistoryTextEvent_6;

public:
	inline static int32_t get_offset_of_OnGetDataUserEvent_3() { return static_cast<int32_t>(offsetof(ApiSearchFriendManager_t2339757037_StaticFields, ___OnGetDataUserEvent_3)); }
	inline UnityAction_3_t1370633073 * get_OnGetDataUserEvent_3() const { return ___OnGetDataUserEvent_3; }
	inline UnityAction_3_t1370633073 ** get_address_of_OnGetDataUserEvent_3() { return &___OnGetDataUserEvent_3; }
	inline void set_OnGetDataUserEvent_3(UnityAction_3_t1370633073 * value)
	{
		___OnGetDataUserEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnGetDataUserEvent_3, value);
	}

	inline static int32_t get_offset_of_OnAnalizeDataEvent_4() { return static_cast<int32_t>(offsetof(ApiSearchFriendManager_t2339757037_StaticFields, ___OnAnalizeDataEvent_4)); }
	inline UnityAction_t4025899511 * get_OnAnalizeDataEvent_4() const { return ___OnAnalizeDataEvent_4; }
	inline UnityAction_t4025899511 ** get_address_of_OnAnalizeDataEvent_4() { return &___OnAnalizeDataEvent_4; }
	inline void set_OnAnalizeDataEvent_4(UnityAction_t4025899511 * value)
	{
		___OnAnalizeDataEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnAnalizeDataEvent_4, value);
	}

	inline static int32_t get_offset_of_OnNoResultEvent_5() { return static_cast<int32_t>(offsetof(ApiSearchFriendManager_t2339757037_StaticFields, ___OnNoResultEvent_5)); }
	inline UnityAction_1_t897193173 * get_OnNoResultEvent_5() const { return ___OnNoResultEvent_5; }
	inline UnityAction_1_t897193173 ** get_address_of_OnNoResultEvent_5() { return &___OnNoResultEvent_5; }
	inline void set_OnNoResultEvent_5(UnityAction_1_t897193173 * value)
	{
		___OnNoResultEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnNoResultEvent_5, value);
	}

	inline static int32_t get_offset_of_ShowHistoryTextEvent_6() { return static_cast<int32_t>(offsetof(ApiSearchFriendManager_t2339757037_StaticFields, ___ShowHistoryTextEvent_6)); }
	inline UnityAction_1_t897193173 * get_ShowHistoryTextEvent_6() const { return ___ShowHistoryTextEvent_6; }
	inline UnityAction_1_t897193173 ** get_address_of_ShowHistoryTextEvent_6() { return &___ShowHistoryTextEvent_6; }
	inline void set_ShowHistoryTextEvent_6(UnityAction_1_t897193173 * value)
	{
		___ShowHistoryTextEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___ShowHistoryTextEvent_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
