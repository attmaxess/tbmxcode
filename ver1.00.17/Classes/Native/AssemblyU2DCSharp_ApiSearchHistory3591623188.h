﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2288299523.h"

// UnityEngine.Events.UnityAction`3<System.String,System.String,System.Int32>
struct UnityAction_3_t1370633073;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiSearchHistory
struct  ApiSearchHistory_t3591623188  : public SingletonMonoBehaviour_1_t2288299523
{
public:
	// System.Int32 ApiSearchHistory::userId
	int32_t ___userId_4;

public:
	inline static int32_t get_offset_of_userId_4() { return static_cast<int32_t>(offsetof(ApiSearchHistory_t3591623188, ___userId_4)); }
	inline int32_t get_userId_4() const { return ___userId_4; }
	inline int32_t* get_address_of_userId_4() { return &___userId_4; }
	inline void set_userId_4(int32_t value)
	{
		___userId_4 = value;
	}
};

struct ApiSearchHistory_t3591623188_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<System.String,System.String,System.Int32> ApiSearchHistory::OnGetDataUser
	UnityAction_3_t1370633073 * ___OnGetDataUser_3;

public:
	inline static int32_t get_offset_of_OnGetDataUser_3() { return static_cast<int32_t>(offsetof(ApiSearchHistory_t3591623188_StaticFields, ___OnGetDataUser_3)); }
	inline UnityAction_3_t1370633073 * get_OnGetDataUser_3() const { return ___OnGetDataUser_3; }
	inline UnityAction_3_t1370633073 ** get_address_of_OnGetDataUser_3() { return &___OnGetDataUser_3; }
	inline void set_OnGetDataUser_3(UnityAction_3_t1370633073 * value)
	{
		___OnGetDataUser_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnGetDataUser_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
