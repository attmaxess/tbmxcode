﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1611917348.h"

// UnityEngine.Events.UnityAction`3<System.String,System.String,System.Int32>
struct UnityAction_3_t1370633073;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiShowSearchHistory
struct  ApiShowSearchHistory_t2915241013  : public SingletonMonoBehaviour_1_t1611917348
{
public:
	// System.Int32 ApiShowSearchHistory::userId
	int32_t ___userId_5;

public:
	inline static int32_t get_offset_of_userId_5() { return static_cast<int32_t>(offsetof(ApiShowSearchHistory_t2915241013, ___userId_5)); }
	inline int32_t get_userId_5() const { return ___userId_5; }
	inline int32_t* get_address_of_userId_5() { return &___userId_5; }
	inline void set_userId_5(int32_t value)
	{
		___userId_5 = value;
	}
};

struct ApiShowSearchHistory_t2915241013_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<System.String,System.String,System.Int32> ApiShowSearchHistory::OnGetDataUser
	UnityAction_3_t1370633073 * ___OnGetDataUser_3;
	// UnityEngine.Events.UnityAction`1<System.Boolean> ApiShowSearchHistory::ShowHistoryText
	UnityAction_1_t897193173 * ___ShowHistoryText_4;

public:
	inline static int32_t get_offset_of_OnGetDataUser_3() { return static_cast<int32_t>(offsetof(ApiShowSearchHistory_t2915241013_StaticFields, ___OnGetDataUser_3)); }
	inline UnityAction_3_t1370633073 * get_OnGetDataUser_3() const { return ___OnGetDataUser_3; }
	inline UnityAction_3_t1370633073 ** get_address_of_OnGetDataUser_3() { return &___OnGetDataUser_3; }
	inline void set_OnGetDataUser_3(UnityAction_3_t1370633073 * value)
	{
		___OnGetDataUser_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnGetDataUser_3, value);
	}

	inline static int32_t get_offset_of_ShowHistoryText_4() { return static_cast<int32_t>(offsetof(ApiShowSearchHistory_t2915241013_StaticFields, ___ShowHistoryText_4)); }
	inline UnityAction_1_t897193173 * get_ShowHistoryText_4() const { return ___ShowHistoryText_4; }
	inline UnityAction_1_t897193173 ** get_address_of_ShowHistoryText_4() { return &___ShowHistoryText_4; }
	inline void set_ShowHistoryText_4(UnityAction_1_t897193173 * value)
	{
		___ShowHistoryText_4 = value;
		Il2CppCodeGenWriteBarrier(&___ShowHistoryText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
