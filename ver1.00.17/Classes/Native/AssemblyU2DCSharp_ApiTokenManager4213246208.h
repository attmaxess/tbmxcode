﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2909922543.h"

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiTokenManager
struct  ApiTokenManager_t4213246208  : public SingletonMonoBehaviour_1_t2909922543
{
public:
	// System.String ApiTokenManager::deviceToken
	String_t* ___deviceToken_3;
	// System.String ApiTokenManager::tokenJson
	String_t* ___tokenJson_4;
	// System.Boolean ApiTokenManager::tokenSent
	bool ___tokenSent_5;
	// UnityEngine.UI.Text ApiTokenManager::tokendisp
	Text_t356221433 * ___tokendisp_6;

public:
	inline static int32_t get_offset_of_deviceToken_3() { return static_cast<int32_t>(offsetof(ApiTokenManager_t4213246208, ___deviceToken_3)); }
	inline String_t* get_deviceToken_3() const { return ___deviceToken_3; }
	inline String_t** get_address_of_deviceToken_3() { return &___deviceToken_3; }
	inline void set_deviceToken_3(String_t* value)
	{
		___deviceToken_3 = value;
		Il2CppCodeGenWriteBarrier(&___deviceToken_3, value);
	}

	inline static int32_t get_offset_of_tokenJson_4() { return static_cast<int32_t>(offsetof(ApiTokenManager_t4213246208, ___tokenJson_4)); }
	inline String_t* get_tokenJson_4() const { return ___tokenJson_4; }
	inline String_t** get_address_of_tokenJson_4() { return &___tokenJson_4; }
	inline void set_tokenJson_4(String_t* value)
	{
		___tokenJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___tokenJson_4, value);
	}

	inline static int32_t get_offset_of_tokenSent_5() { return static_cast<int32_t>(offsetof(ApiTokenManager_t4213246208, ___tokenSent_5)); }
	inline bool get_tokenSent_5() const { return ___tokenSent_5; }
	inline bool* get_address_of_tokenSent_5() { return &___tokenSent_5; }
	inline void set_tokenSent_5(bool value)
	{
		___tokenSent_5 = value;
	}

	inline static int32_t get_offset_of_tokendisp_6() { return static_cast<int32_t>(offsetof(ApiTokenManager_t4213246208, ___tokendisp_6)); }
	inline Text_t356221433 * get_tokendisp_6() const { return ___tokendisp_6; }
	inline Text_t356221433 ** get_address_of_tokendisp_6() { return &___tokendisp_6; }
	inline void set_tokendisp_6(Text_t356221433 * value)
	{
		___tokendisp_6 = value;
		Il2CppCodeGenWriteBarrier(&___tokendisp_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
