﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1253502145.h"

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UserNameCreator
struct UserNameCreator_t3044989708;
// MobilityNameEditPanel
struct MobilityNameEditPanel_t1220421384;
// System.Collections.Generic.List`1<ApiUserManager/GetUserUpdateJson>
struct List_1_t1632735025;
// UserSetting
struct UserSetting_t3323821485;
// System.Collections.Generic.List`1<ApiUserManager/CountryData>
struct List_1_t504488707;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiUserManager
struct  ApiUserManager_t2556825810  : public SingletonMonoBehaviour_1_t1253502145
{
public:
	// System.String ApiUserManager::usernewJson
	String_t* ___usernewJson_3;
	// System.String ApiUserManager::userupdateJson
	String_t* ___userupdateJson_4;
	// System.Int32 ApiUserManager::loginedUserId
	int32_t ___loginedUserId_5;
	// System.String ApiUserManager::userName
	String_t* ___userName_6;
	// System.Int32 ApiUserManager::languageNo
	int32_t ___languageNo_7;
	// System.Int32 ApiUserManager::countryNo
	int32_t ___countryNo_8;
	// System.String ApiUserManager::userRank
	String_t* ___userRank_9;
	// System.Int32 ApiUserManager::userPoint
	int32_t ___userPoint_10;
	// System.Int32 ApiUserManager::userRankupPoint
	int32_t ___userRankupPoint_11;
	// System.Int32 ApiUserManager::userCopiedCount
	int32_t ___userCopiedCount_12;
	// System.Int32 ApiUserManager::mainMobilityId
	int32_t ___mainMobilityId_13;
	// System.Int32 ApiUserManager::mobilityCount
	int32_t ___mobilityCount_14;
	// System.Single ApiUserManager::startPositionX
	float ___startPositionX_15;
	// System.Single ApiUserManager::startPositionY
	float ___startPositionY_16;
	// System.Single ApiUserManager::startPositionZ
	float ___startPositionZ_17;
	// System.Single ApiUserManager::startRotationX
	float ___startRotationX_18;
	// System.Single ApiUserManager::startRotationY
	float ___startRotationY_19;
	// System.Single ApiUserManager::startRotationZ
	float ___startRotationZ_20;
	// UnityEngine.UI.Text ApiUserManager::_userNameText
	Text_t356221433 * ____userNameText_21;
	// UnityEngine.UI.Text ApiUserManager::_userRankText
	Text_t356221433 * ____userRankText_22;
	// UnityEngine.UI.Text ApiUserManager::_userPointText
	Text_t356221433 * ____userPointText_23;
	// UnityEngine.UI.Text ApiUserManager::_recUserNameText
	Text_t356221433 * ____recUserNameText_24;
	// UnityEngine.UI.Text ApiUserManager::_recUserRankText
	Text_t356221433 * ____recUserRankText_25;
	// UnityEngine.UI.Text ApiUserManager::_recUserCountoryText
	Text_t356221433 * ____recUserCountoryText_26;
	// UnityEngine.UI.Text ApiUserManager::_userRankupPointText
	Text_t356221433 * ____userRankupPointText_27;
	// UnityEngine.UI.Image ApiUserManager::_EmblemImage
	Image_t2042527209 * ____EmblemImage_28;
	// UnityEngine.UI.Image ApiUserManager::_RankGageImage
	Image_t2042527209 * ____RankGageImage_29;
	// UnityEngine.UI.InputField ApiUserManager::userNameInputField
	InputField_t1631627530 * ___userNameInputField_30;
	// System.String ApiUserManager::createNewMobName
	String_t* ___createNewMobName_31;
	// System.String ApiUserManager::editUserName
	String_t* ___editUserName_32;
	// UserNameCreator ApiUserManager::userNameCreator
	UserNameCreator_t3044989708 * ___userNameCreator_33;
	// MobilityNameEditPanel ApiUserManager::NameNewPanel
	MobilityNameEditPanel_t1220421384 * ___NameNewPanel_34;
	// MobilityNameEditPanel ApiUserManager::NameEditPanel
	MobilityNameEditPanel_t1220421384 * ___NameEditPanel_35;
	// System.Collections.Generic.List`1<ApiUserManager/GetUserUpdateJson> ApiUserManager::userUpdateList
	List_1_t1632735025 * ___userUpdateList_36;
	// UserSetting ApiUserManager::m_UserSetting
	UserSetting_t3323821485 * ___m_UserSetting_37;
	// System.Boolean ApiUserManager::isNgword
	bool ___isNgword_38;
	// System.Collections.Generic.List`1<ApiUserManager/CountryData> ApiUserManager::m_countyDataList
	List_1_t504488707 * ___m_countyDataList_39;

public:
	inline static int32_t get_offset_of_usernewJson_3() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___usernewJson_3)); }
	inline String_t* get_usernewJson_3() const { return ___usernewJson_3; }
	inline String_t** get_address_of_usernewJson_3() { return &___usernewJson_3; }
	inline void set_usernewJson_3(String_t* value)
	{
		___usernewJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___usernewJson_3, value);
	}

	inline static int32_t get_offset_of_userupdateJson_4() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userupdateJson_4)); }
	inline String_t* get_userupdateJson_4() const { return ___userupdateJson_4; }
	inline String_t** get_address_of_userupdateJson_4() { return &___userupdateJson_4; }
	inline void set_userupdateJson_4(String_t* value)
	{
		___userupdateJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___userupdateJson_4, value);
	}

	inline static int32_t get_offset_of_loginedUserId_5() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___loginedUserId_5)); }
	inline int32_t get_loginedUserId_5() const { return ___loginedUserId_5; }
	inline int32_t* get_address_of_loginedUserId_5() { return &___loginedUserId_5; }
	inline void set_loginedUserId_5(int32_t value)
	{
		___loginedUserId_5 = value;
	}

	inline static int32_t get_offset_of_userName_6() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userName_6)); }
	inline String_t* get_userName_6() const { return ___userName_6; }
	inline String_t** get_address_of_userName_6() { return &___userName_6; }
	inline void set_userName_6(String_t* value)
	{
		___userName_6 = value;
		Il2CppCodeGenWriteBarrier(&___userName_6, value);
	}

	inline static int32_t get_offset_of_languageNo_7() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___languageNo_7)); }
	inline int32_t get_languageNo_7() const { return ___languageNo_7; }
	inline int32_t* get_address_of_languageNo_7() { return &___languageNo_7; }
	inline void set_languageNo_7(int32_t value)
	{
		___languageNo_7 = value;
	}

	inline static int32_t get_offset_of_countryNo_8() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___countryNo_8)); }
	inline int32_t get_countryNo_8() const { return ___countryNo_8; }
	inline int32_t* get_address_of_countryNo_8() { return &___countryNo_8; }
	inline void set_countryNo_8(int32_t value)
	{
		___countryNo_8 = value;
	}

	inline static int32_t get_offset_of_userRank_9() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userRank_9)); }
	inline String_t* get_userRank_9() const { return ___userRank_9; }
	inline String_t** get_address_of_userRank_9() { return &___userRank_9; }
	inline void set_userRank_9(String_t* value)
	{
		___userRank_9 = value;
		Il2CppCodeGenWriteBarrier(&___userRank_9, value);
	}

	inline static int32_t get_offset_of_userPoint_10() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userPoint_10)); }
	inline int32_t get_userPoint_10() const { return ___userPoint_10; }
	inline int32_t* get_address_of_userPoint_10() { return &___userPoint_10; }
	inline void set_userPoint_10(int32_t value)
	{
		___userPoint_10 = value;
	}

	inline static int32_t get_offset_of_userRankupPoint_11() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userRankupPoint_11)); }
	inline int32_t get_userRankupPoint_11() const { return ___userRankupPoint_11; }
	inline int32_t* get_address_of_userRankupPoint_11() { return &___userRankupPoint_11; }
	inline void set_userRankupPoint_11(int32_t value)
	{
		___userRankupPoint_11 = value;
	}

	inline static int32_t get_offset_of_userCopiedCount_12() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userCopiedCount_12)); }
	inline int32_t get_userCopiedCount_12() const { return ___userCopiedCount_12; }
	inline int32_t* get_address_of_userCopiedCount_12() { return &___userCopiedCount_12; }
	inline void set_userCopiedCount_12(int32_t value)
	{
		___userCopiedCount_12 = value;
	}

	inline static int32_t get_offset_of_mainMobilityId_13() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___mainMobilityId_13)); }
	inline int32_t get_mainMobilityId_13() const { return ___mainMobilityId_13; }
	inline int32_t* get_address_of_mainMobilityId_13() { return &___mainMobilityId_13; }
	inline void set_mainMobilityId_13(int32_t value)
	{
		___mainMobilityId_13 = value;
	}

	inline static int32_t get_offset_of_mobilityCount_14() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___mobilityCount_14)); }
	inline int32_t get_mobilityCount_14() const { return ___mobilityCount_14; }
	inline int32_t* get_address_of_mobilityCount_14() { return &___mobilityCount_14; }
	inline void set_mobilityCount_14(int32_t value)
	{
		___mobilityCount_14 = value;
	}

	inline static int32_t get_offset_of_startPositionX_15() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___startPositionX_15)); }
	inline float get_startPositionX_15() const { return ___startPositionX_15; }
	inline float* get_address_of_startPositionX_15() { return &___startPositionX_15; }
	inline void set_startPositionX_15(float value)
	{
		___startPositionX_15 = value;
	}

	inline static int32_t get_offset_of_startPositionY_16() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___startPositionY_16)); }
	inline float get_startPositionY_16() const { return ___startPositionY_16; }
	inline float* get_address_of_startPositionY_16() { return &___startPositionY_16; }
	inline void set_startPositionY_16(float value)
	{
		___startPositionY_16 = value;
	}

	inline static int32_t get_offset_of_startPositionZ_17() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___startPositionZ_17)); }
	inline float get_startPositionZ_17() const { return ___startPositionZ_17; }
	inline float* get_address_of_startPositionZ_17() { return &___startPositionZ_17; }
	inline void set_startPositionZ_17(float value)
	{
		___startPositionZ_17 = value;
	}

	inline static int32_t get_offset_of_startRotationX_18() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___startRotationX_18)); }
	inline float get_startRotationX_18() const { return ___startRotationX_18; }
	inline float* get_address_of_startRotationX_18() { return &___startRotationX_18; }
	inline void set_startRotationX_18(float value)
	{
		___startRotationX_18 = value;
	}

	inline static int32_t get_offset_of_startRotationY_19() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___startRotationY_19)); }
	inline float get_startRotationY_19() const { return ___startRotationY_19; }
	inline float* get_address_of_startRotationY_19() { return &___startRotationY_19; }
	inline void set_startRotationY_19(float value)
	{
		___startRotationY_19 = value;
	}

	inline static int32_t get_offset_of_startRotationZ_20() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___startRotationZ_20)); }
	inline float get_startRotationZ_20() const { return ___startRotationZ_20; }
	inline float* get_address_of_startRotationZ_20() { return &___startRotationZ_20; }
	inline void set_startRotationZ_20(float value)
	{
		___startRotationZ_20 = value;
	}

	inline static int32_t get_offset_of__userNameText_21() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____userNameText_21)); }
	inline Text_t356221433 * get__userNameText_21() const { return ____userNameText_21; }
	inline Text_t356221433 ** get_address_of__userNameText_21() { return &____userNameText_21; }
	inline void set__userNameText_21(Text_t356221433 * value)
	{
		____userNameText_21 = value;
		Il2CppCodeGenWriteBarrier(&____userNameText_21, value);
	}

	inline static int32_t get_offset_of__userRankText_22() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____userRankText_22)); }
	inline Text_t356221433 * get__userRankText_22() const { return ____userRankText_22; }
	inline Text_t356221433 ** get_address_of__userRankText_22() { return &____userRankText_22; }
	inline void set__userRankText_22(Text_t356221433 * value)
	{
		____userRankText_22 = value;
		Il2CppCodeGenWriteBarrier(&____userRankText_22, value);
	}

	inline static int32_t get_offset_of__userPointText_23() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____userPointText_23)); }
	inline Text_t356221433 * get__userPointText_23() const { return ____userPointText_23; }
	inline Text_t356221433 ** get_address_of__userPointText_23() { return &____userPointText_23; }
	inline void set__userPointText_23(Text_t356221433 * value)
	{
		____userPointText_23 = value;
		Il2CppCodeGenWriteBarrier(&____userPointText_23, value);
	}

	inline static int32_t get_offset_of__recUserNameText_24() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____recUserNameText_24)); }
	inline Text_t356221433 * get__recUserNameText_24() const { return ____recUserNameText_24; }
	inline Text_t356221433 ** get_address_of__recUserNameText_24() { return &____recUserNameText_24; }
	inline void set__recUserNameText_24(Text_t356221433 * value)
	{
		____recUserNameText_24 = value;
		Il2CppCodeGenWriteBarrier(&____recUserNameText_24, value);
	}

	inline static int32_t get_offset_of__recUserRankText_25() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____recUserRankText_25)); }
	inline Text_t356221433 * get__recUserRankText_25() const { return ____recUserRankText_25; }
	inline Text_t356221433 ** get_address_of__recUserRankText_25() { return &____recUserRankText_25; }
	inline void set__recUserRankText_25(Text_t356221433 * value)
	{
		____recUserRankText_25 = value;
		Il2CppCodeGenWriteBarrier(&____recUserRankText_25, value);
	}

	inline static int32_t get_offset_of__recUserCountoryText_26() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____recUserCountoryText_26)); }
	inline Text_t356221433 * get__recUserCountoryText_26() const { return ____recUserCountoryText_26; }
	inline Text_t356221433 ** get_address_of__recUserCountoryText_26() { return &____recUserCountoryText_26; }
	inline void set__recUserCountoryText_26(Text_t356221433 * value)
	{
		____recUserCountoryText_26 = value;
		Il2CppCodeGenWriteBarrier(&____recUserCountoryText_26, value);
	}

	inline static int32_t get_offset_of__userRankupPointText_27() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____userRankupPointText_27)); }
	inline Text_t356221433 * get__userRankupPointText_27() const { return ____userRankupPointText_27; }
	inline Text_t356221433 ** get_address_of__userRankupPointText_27() { return &____userRankupPointText_27; }
	inline void set__userRankupPointText_27(Text_t356221433 * value)
	{
		____userRankupPointText_27 = value;
		Il2CppCodeGenWriteBarrier(&____userRankupPointText_27, value);
	}

	inline static int32_t get_offset_of__EmblemImage_28() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____EmblemImage_28)); }
	inline Image_t2042527209 * get__EmblemImage_28() const { return ____EmblemImage_28; }
	inline Image_t2042527209 ** get_address_of__EmblemImage_28() { return &____EmblemImage_28; }
	inline void set__EmblemImage_28(Image_t2042527209 * value)
	{
		____EmblemImage_28 = value;
		Il2CppCodeGenWriteBarrier(&____EmblemImage_28, value);
	}

	inline static int32_t get_offset_of__RankGageImage_29() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ____RankGageImage_29)); }
	inline Image_t2042527209 * get__RankGageImage_29() const { return ____RankGageImage_29; }
	inline Image_t2042527209 ** get_address_of__RankGageImage_29() { return &____RankGageImage_29; }
	inline void set__RankGageImage_29(Image_t2042527209 * value)
	{
		____RankGageImage_29 = value;
		Il2CppCodeGenWriteBarrier(&____RankGageImage_29, value);
	}

	inline static int32_t get_offset_of_userNameInputField_30() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userNameInputField_30)); }
	inline InputField_t1631627530 * get_userNameInputField_30() const { return ___userNameInputField_30; }
	inline InputField_t1631627530 ** get_address_of_userNameInputField_30() { return &___userNameInputField_30; }
	inline void set_userNameInputField_30(InputField_t1631627530 * value)
	{
		___userNameInputField_30 = value;
		Il2CppCodeGenWriteBarrier(&___userNameInputField_30, value);
	}

	inline static int32_t get_offset_of_createNewMobName_31() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___createNewMobName_31)); }
	inline String_t* get_createNewMobName_31() const { return ___createNewMobName_31; }
	inline String_t** get_address_of_createNewMobName_31() { return &___createNewMobName_31; }
	inline void set_createNewMobName_31(String_t* value)
	{
		___createNewMobName_31 = value;
		Il2CppCodeGenWriteBarrier(&___createNewMobName_31, value);
	}

	inline static int32_t get_offset_of_editUserName_32() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___editUserName_32)); }
	inline String_t* get_editUserName_32() const { return ___editUserName_32; }
	inline String_t** get_address_of_editUserName_32() { return &___editUserName_32; }
	inline void set_editUserName_32(String_t* value)
	{
		___editUserName_32 = value;
		Il2CppCodeGenWriteBarrier(&___editUserName_32, value);
	}

	inline static int32_t get_offset_of_userNameCreator_33() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userNameCreator_33)); }
	inline UserNameCreator_t3044989708 * get_userNameCreator_33() const { return ___userNameCreator_33; }
	inline UserNameCreator_t3044989708 ** get_address_of_userNameCreator_33() { return &___userNameCreator_33; }
	inline void set_userNameCreator_33(UserNameCreator_t3044989708 * value)
	{
		___userNameCreator_33 = value;
		Il2CppCodeGenWriteBarrier(&___userNameCreator_33, value);
	}

	inline static int32_t get_offset_of_NameNewPanel_34() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___NameNewPanel_34)); }
	inline MobilityNameEditPanel_t1220421384 * get_NameNewPanel_34() const { return ___NameNewPanel_34; }
	inline MobilityNameEditPanel_t1220421384 ** get_address_of_NameNewPanel_34() { return &___NameNewPanel_34; }
	inline void set_NameNewPanel_34(MobilityNameEditPanel_t1220421384 * value)
	{
		___NameNewPanel_34 = value;
		Il2CppCodeGenWriteBarrier(&___NameNewPanel_34, value);
	}

	inline static int32_t get_offset_of_NameEditPanel_35() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___NameEditPanel_35)); }
	inline MobilityNameEditPanel_t1220421384 * get_NameEditPanel_35() const { return ___NameEditPanel_35; }
	inline MobilityNameEditPanel_t1220421384 ** get_address_of_NameEditPanel_35() { return &___NameEditPanel_35; }
	inline void set_NameEditPanel_35(MobilityNameEditPanel_t1220421384 * value)
	{
		___NameEditPanel_35 = value;
		Il2CppCodeGenWriteBarrier(&___NameEditPanel_35, value);
	}

	inline static int32_t get_offset_of_userUpdateList_36() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___userUpdateList_36)); }
	inline List_1_t1632735025 * get_userUpdateList_36() const { return ___userUpdateList_36; }
	inline List_1_t1632735025 ** get_address_of_userUpdateList_36() { return &___userUpdateList_36; }
	inline void set_userUpdateList_36(List_1_t1632735025 * value)
	{
		___userUpdateList_36 = value;
		Il2CppCodeGenWriteBarrier(&___userUpdateList_36, value);
	}

	inline static int32_t get_offset_of_m_UserSetting_37() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___m_UserSetting_37)); }
	inline UserSetting_t3323821485 * get_m_UserSetting_37() const { return ___m_UserSetting_37; }
	inline UserSetting_t3323821485 ** get_address_of_m_UserSetting_37() { return &___m_UserSetting_37; }
	inline void set_m_UserSetting_37(UserSetting_t3323821485 * value)
	{
		___m_UserSetting_37 = value;
		Il2CppCodeGenWriteBarrier(&___m_UserSetting_37, value);
	}

	inline static int32_t get_offset_of_isNgword_38() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___isNgword_38)); }
	inline bool get_isNgword_38() const { return ___isNgword_38; }
	inline bool* get_address_of_isNgword_38() { return &___isNgword_38; }
	inline void set_isNgword_38(bool value)
	{
		___isNgword_38 = value;
	}

	inline static int32_t get_offset_of_m_countyDataList_39() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810, ___m_countyDataList_39)); }
	inline List_1_t504488707 * get_m_countyDataList_39() const { return ___m_countyDataList_39; }
	inline List_1_t504488707 ** get_address_of_m_countyDataList_39() { return &___m_countyDataList_39; }
	inline void set_m_countyDataList_39(List_1_t504488707 * value)
	{
		___m_countyDataList_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_countyDataList_39, value);
	}
};

struct ApiUserManager_t2556825810_StaticFields
{
public:
	// System.Action ApiUserManager::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_40;
	// System.Action ApiUserManager::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_41;
	// System.Action ApiUserManager::<>f__am$cache2
	Action_t3226471752 * ___U3CU3Ef__amU24cache2_42;
	// System.Action ApiUserManager::<>f__am$cache3
	Action_t3226471752 * ___U3CU3Ef__amU24cache3_43;
	// System.Action ApiUserManager::<>f__am$cache4
	Action_t3226471752 * ___U3CU3Ef__amU24cache4_44;
	// System.Action ApiUserManager::<>f__am$cache5
	Action_t3226471752 * ___U3CU3Ef__amU24cache5_45;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_40() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810_StaticFields, ___U3CU3Ef__amU24cache0_40)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_40() const { return ___U3CU3Ef__amU24cache0_40; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_40() { return &___U3CU3Ef__amU24cache0_40; }
	inline void set_U3CU3Ef__amU24cache0_40(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_40 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_40, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_41() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810_StaticFields, ___U3CU3Ef__amU24cache1_41)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_41() const { return ___U3CU3Ef__amU24cache1_41; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_41() { return &___U3CU3Ef__amU24cache1_41; }
	inline void set_U3CU3Ef__amU24cache1_41(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_41, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_42() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810_StaticFields, ___U3CU3Ef__amU24cache2_42)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache2_42() const { return ___U3CU3Ef__amU24cache2_42; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache2_42() { return &___U3CU3Ef__amU24cache2_42; }
	inline void set_U3CU3Ef__amU24cache2_42(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache2_42 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_42, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_43() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810_StaticFields, ___U3CU3Ef__amU24cache3_43)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache3_43() const { return ___U3CU3Ef__amU24cache3_43; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache3_43() { return &___U3CU3Ef__amU24cache3_43; }
	inline void set_U3CU3Ef__amU24cache3_43(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache3_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_43, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_44() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810_StaticFields, ___U3CU3Ef__amU24cache4_44)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache4_44() const { return ___U3CU3Ef__amU24cache4_44; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache4_44() { return &___U3CU3Ef__amU24cache4_44; }
	inline void set_U3CU3Ef__amU24cache4_44(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache4_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_44, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_45() { return static_cast<int32_t>(offsetof(ApiUserManager_t2556825810_StaticFields, ___U3CU3Ef__amU24cache5_45)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache5_45() const { return ___U3CU3Ef__amU24cache5_45; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache5_45() { return &___U3CU3Ef__amU24cache5_45; }
	inline void set_U3CU3Ef__amU24cache5_45(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache5_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_45, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
