﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiUserManager/CountryData
struct  CountryData_t1135367575  : public Il2CppObject
{
public:
	// System.String ApiUserManager/CountryData::countryName
	String_t* ___countryName_0;
	// System.Int32 ApiUserManager/CountryData::countryNo
	int32_t ___countryNo_1;

public:
	inline static int32_t get_offset_of_countryName_0() { return static_cast<int32_t>(offsetof(CountryData_t1135367575, ___countryName_0)); }
	inline String_t* get_countryName_0() const { return ___countryName_0; }
	inline String_t** get_address_of_countryName_0() { return &___countryName_0; }
	inline void set_countryName_0(String_t* value)
	{
		___countryName_0 = value;
		Il2CppCodeGenWriteBarrier(&___countryName_0, value);
	}

	inline static int32_t get_offset_of_countryNo_1() { return static_cast<int32_t>(offsetof(CountryData_t1135367575, ___countryNo_1)); }
	inline int32_t get_countryNo_1() const { return ___countryNo_1; }
	inline int32_t* get_address_of_countryNo_1() { return &___countryNo_1; }
	inline void set_countryNo_1(int32_t value)
	{
		___countryNo_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
