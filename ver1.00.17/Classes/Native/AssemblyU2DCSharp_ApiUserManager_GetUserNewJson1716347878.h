﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiUserManager/GetUserNewJson
struct  GetUserNewJson_t1716347878  : public Il2CppObject
{
public:
	// System.String ApiUserManager/GetUserNewJson::name
	String_t* ___name_0;
	// System.Int32 ApiUserManager/GetUserNewJson::countryNo
	int32_t ___countryNo_1;
	// System.Int32 ApiUserManager/GetUserNewJson::languageNo
	int32_t ___languageNo_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GetUserNewJson_t1716347878, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_countryNo_1() { return static_cast<int32_t>(offsetof(GetUserNewJson_t1716347878, ___countryNo_1)); }
	inline int32_t get_countryNo_1() const { return ___countryNo_1; }
	inline int32_t* get_address_of_countryNo_1() { return &___countryNo_1; }
	inline void set_countryNo_1(int32_t value)
	{
		___countryNo_1 = value;
	}

	inline static int32_t get_offset_of_languageNo_2() { return static_cast<int32_t>(offsetof(GetUserNewJson_t1716347878, ___languageNo_2)); }
	inline int32_t get_languageNo_2() const { return ___languageNo_2; }
	inline int32_t* get_address_of_languageNo_2() { return &___languageNo_2; }
	inline void set_languageNo_2(int32_t value)
	{
		___languageNo_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
