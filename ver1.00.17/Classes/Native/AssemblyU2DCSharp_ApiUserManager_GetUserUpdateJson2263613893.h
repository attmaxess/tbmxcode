﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApiUserManager/GetUserUpdateJson
struct  GetUserUpdateJson_t2263613893  : public Il2CppObject
{
public:
	// System.Int32 ApiUserManager/GetUserUpdateJson::userId
	int32_t ___userId_0;
	// System.String ApiUserManager/GetUserUpdateJson::name
	String_t* ___name_1;
	// System.Int32 ApiUserManager/GetUserUpdateJson::countryNo
	int32_t ___countryNo_2;
	// System.Int32 ApiUserManager/GetUserUpdateJson::languageNo
	int32_t ___languageNo_3;
	// System.Int32 ApiUserManager/GetUserUpdateJson::mainmobilityId
	int32_t ___mainmobilityId_4;
	// System.Single ApiUserManager/GetUserUpdateJson::startPositionX
	float ___startPositionX_5;
	// System.Single ApiUserManager/GetUserUpdateJson::startPositionY
	float ___startPositionY_6;
	// System.Single ApiUserManager/GetUserUpdateJson::startPositionZ
	float ___startPositionZ_7;
	// System.Single ApiUserManager/GetUserUpdateJson::startRotationX
	float ___startRotationX_8;
	// System.Single ApiUserManager/GetUserUpdateJson::startRotationY
	float ___startRotationY_9;
	// System.Single ApiUserManager/GetUserUpdateJson::startRotationZ
	float ___startRotationZ_10;

public:
	inline static int32_t get_offset_of_userId_0() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___userId_0)); }
	inline int32_t get_userId_0() const { return ___userId_0; }
	inline int32_t* get_address_of_userId_0() { return &___userId_0; }
	inline void set_userId_0(int32_t value)
	{
		___userId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_countryNo_2() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___countryNo_2)); }
	inline int32_t get_countryNo_2() const { return ___countryNo_2; }
	inline int32_t* get_address_of_countryNo_2() { return &___countryNo_2; }
	inline void set_countryNo_2(int32_t value)
	{
		___countryNo_2 = value;
	}

	inline static int32_t get_offset_of_languageNo_3() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___languageNo_3)); }
	inline int32_t get_languageNo_3() const { return ___languageNo_3; }
	inline int32_t* get_address_of_languageNo_3() { return &___languageNo_3; }
	inline void set_languageNo_3(int32_t value)
	{
		___languageNo_3 = value;
	}

	inline static int32_t get_offset_of_mainmobilityId_4() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___mainmobilityId_4)); }
	inline int32_t get_mainmobilityId_4() const { return ___mainmobilityId_4; }
	inline int32_t* get_address_of_mainmobilityId_4() { return &___mainmobilityId_4; }
	inline void set_mainmobilityId_4(int32_t value)
	{
		___mainmobilityId_4 = value;
	}

	inline static int32_t get_offset_of_startPositionX_5() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___startPositionX_5)); }
	inline float get_startPositionX_5() const { return ___startPositionX_5; }
	inline float* get_address_of_startPositionX_5() { return &___startPositionX_5; }
	inline void set_startPositionX_5(float value)
	{
		___startPositionX_5 = value;
	}

	inline static int32_t get_offset_of_startPositionY_6() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___startPositionY_6)); }
	inline float get_startPositionY_6() const { return ___startPositionY_6; }
	inline float* get_address_of_startPositionY_6() { return &___startPositionY_6; }
	inline void set_startPositionY_6(float value)
	{
		___startPositionY_6 = value;
	}

	inline static int32_t get_offset_of_startPositionZ_7() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___startPositionZ_7)); }
	inline float get_startPositionZ_7() const { return ___startPositionZ_7; }
	inline float* get_address_of_startPositionZ_7() { return &___startPositionZ_7; }
	inline void set_startPositionZ_7(float value)
	{
		___startPositionZ_7 = value;
	}

	inline static int32_t get_offset_of_startRotationX_8() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___startRotationX_8)); }
	inline float get_startRotationX_8() const { return ___startRotationX_8; }
	inline float* get_address_of_startRotationX_8() { return &___startRotationX_8; }
	inline void set_startRotationX_8(float value)
	{
		___startRotationX_8 = value;
	}

	inline static int32_t get_offset_of_startRotationY_9() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___startRotationY_9)); }
	inline float get_startRotationY_9() const { return ___startRotationY_9; }
	inline float* get_address_of_startRotationY_9() { return &___startRotationY_9; }
	inline void set_startRotationY_9(float value)
	{
		___startRotationY_9 = value;
	}

	inline static int32_t get_offset_of_startRotationZ_10() { return static_cast<int32_t>(offsetof(GetUserUpdateJson_t2263613893, ___startRotationZ_10)); }
	inline float get_startRotationZ_10() const { return ___startRotationZ_10; }
	inline float* get_address_of_startRotationZ_10() { return &___startRotationZ_10; }
	inline void set_startRotationZ_10(float value)
	{
		___startRotationZ_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
