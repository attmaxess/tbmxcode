﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// DG.Tweening.Tween[]
struct TweenU5BU5D_t672698288;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaButton
struct  AreaButton_t1637812991  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image AreaButton::Frame
	Image_t2042527209 * ___Frame_2;
	// UnityEngine.CanvasGroup AreaButton::Decided
	CanvasGroup_t3296560743 * ___Decided_3;
	// DG.Tweening.Tween[] AreaButton::m_CrossFadeTween
	TweenU5BU5D_t672698288* ___m_CrossFadeTween_4;

public:
	inline static int32_t get_offset_of_Frame_2() { return static_cast<int32_t>(offsetof(AreaButton_t1637812991, ___Frame_2)); }
	inline Image_t2042527209 * get_Frame_2() const { return ___Frame_2; }
	inline Image_t2042527209 ** get_address_of_Frame_2() { return &___Frame_2; }
	inline void set_Frame_2(Image_t2042527209 * value)
	{
		___Frame_2 = value;
		Il2CppCodeGenWriteBarrier(&___Frame_2, value);
	}

	inline static int32_t get_offset_of_Decided_3() { return static_cast<int32_t>(offsetof(AreaButton_t1637812991, ___Decided_3)); }
	inline CanvasGroup_t3296560743 * get_Decided_3() const { return ___Decided_3; }
	inline CanvasGroup_t3296560743 ** get_address_of_Decided_3() { return &___Decided_3; }
	inline void set_Decided_3(CanvasGroup_t3296560743 * value)
	{
		___Decided_3 = value;
		Il2CppCodeGenWriteBarrier(&___Decided_3, value);
	}

	inline static int32_t get_offset_of_m_CrossFadeTween_4() { return static_cast<int32_t>(offsetof(AreaButton_t1637812991, ___m_CrossFadeTween_4)); }
	inline TweenU5BU5D_t672698288* get_m_CrossFadeTween_4() const { return ___m_CrossFadeTween_4; }
	inline TweenU5BU5D_t672698288** get_address_of_m_CrossFadeTween_4() { return &___m_CrossFadeTween_4; }
	inline void set_m_CrossFadeTween_4(TweenU5BU5D_t672698288* value)
	{
		___m_CrossFadeTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_CrossFadeTween_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
