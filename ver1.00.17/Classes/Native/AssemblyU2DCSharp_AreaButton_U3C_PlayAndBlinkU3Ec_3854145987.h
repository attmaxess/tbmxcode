﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// AreaButton
struct AreaButton_t1637812991;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaButton/<_PlayAndBlink>c__AnonStorey0
struct  U3C_PlayAndBlinkU3Ec__AnonStorey0_t3854145987  : public Il2CppObject
{
public:
	// System.Single AreaButton/<_PlayAndBlink>c__AnonStorey0::blinkTime
	float ___blinkTime_0;
	// AreaButton AreaButton/<_PlayAndBlink>c__AnonStorey0::$this
	AreaButton_t1637812991 * ___U24this_1;

public:
	inline static int32_t get_offset_of_blinkTime_0() { return static_cast<int32_t>(offsetof(U3C_PlayAndBlinkU3Ec__AnonStorey0_t3854145987, ___blinkTime_0)); }
	inline float get_blinkTime_0() const { return ___blinkTime_0; }
	inline float* get_address_of_blinkTime_0() { return &___blinkTime_0; }
	inline void set_blinkTime_0(float value)
	{
		___blinkTime_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3C_PlayAndBlinkU3Ec__AnonStorey0_t3854145987, ___U24this_1)); }
	inline AreaButton_t1637812991 * get_U24this_1() const { return ___U24this_1; }
	inline AreaButton_t1637812991 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AreaButton_t1637812991 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
