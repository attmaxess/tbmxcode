﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_eAREATYPE1739175762.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaGround
struct  AreaGround_t2255653338  : public MonoBehaviour_t1158329972
{
public:
	// eAREATYPE AreaGround::areaType
	int32_t ___areaType_2;
	// System.Single AreaGround::SEtimer
	float ___SEtimer_3;

public:
	inline static int32_t get_offset_of_areaType_2() { return static_cast<int32_t>(offsetof(AreaGround_t2255653338, ___areaType_2)); }
	inline int32_t get_areaType_2() const { return ___areaType_2; }
	inline int32_t* get_address_of_areaType_2() { return &___areaType_2; }
	inline void set_areaType_2(int32_t value)
	{
		___areaType_2 = value;
	}

	inline static int32_t get_offset_of_SEtimer_3() { return static_cast<int32_t>(offsetof(AreaGround_t2255653338, ___SEtimer_3)); }
	inline float get_SEtimer_3() const { return ___SEtimer_3; }
	inline float* get_address_of_SEtimer_3() { return &___SEtimer_3; }
	inline void set_SEtimer_3(float value)
	{
		___SEtimer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
