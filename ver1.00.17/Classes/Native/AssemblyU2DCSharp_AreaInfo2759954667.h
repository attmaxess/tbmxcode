﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.UI.Shadow
struct Shadow_t4269599528;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaInfo
struct  AreaInfo_t2759954667  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.CanvasGroup AreaInfo::Infos
	CanvasGroup_t3296560743 * ___Infos_2;
	// UnityEngine.UI.Image AreaInfo::Icon
	Image_t2042527209 * ___Icon_3;
	// UnityEngine.UI.Text AreaInfo::AreaName
	Text_t356221433 * ___AreaName_4;
	// UnityEngine.UI.Text[] AreaInfo::AreaLeads
	TextU5BU5D_t4216439300* ___AreaLeads_5;
	// UnityEngine.UI.Shadow AreaInfo::AreaNameOutline
	Shadow_t4269599528 * ___AreaNameOutline_6;

public:
	inline static int32_t get_offset_of_Infos_2() { return static_cast<int32_t>(offsetof(AreaInfo_t2759954667, ___Infos_2)); }
	inline CanvasGroup_t3296560743 * get_Infos_2() const { return ___Infos_2; }
	inline CanvasGroup_t3296560743 ** get_address_of_Infos_2() { return &___Infos_2; }
	inline void set_Infos_2(CanvasGroup_t3296560743 * value)
	{
		___Infos_2 = value;
		Il2CppCodeGenWriteBarrier(&___Infos_2, value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(AreaInfo_t2759954667, ___Icon_3)); }
	inline Image_t2042527209 * get_Icon_3() const { return ___Icon_3; }
	inline Image_t2042527209 ** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(Image_t2042527209 * value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier(&___Icon_3, value);
	}

	inline static int32_t get_offset_of_AreaName_4() { return static_cast<int32_t>(offsetof(AreaInfo_t2759954667, ___AreaName_4)); }
	inline Text_t356221433 * get_AreaName_4() const { return ___AreaName_4; }
	inline Text_t356221433 ** get_address_of_AreaName_4() { return &___AreaName_4; }
	inline void set_AreaName_4(Text_t356221433 * value)
	{
		___AreaName_4 = value;
		Il2CppCodeGenWriteBarrier(&___AreaName_4, value);
	}

	inline static int32_t get_offset_of_AreaLeads_5() { return static_cast<int32_t>(offsetof(AreaInfo_t2759954667, ___AreaLeads_5)); }
	inline TextU5BU5D_t4216439300* get_AreaLeads_5() const { return ___AreaLeads_5; }
	inline TextU5BU5D_t4216439300** get_address_of_AreaLeads_5() { return &___AreaLeads_5; }
	inline void set_AreaLeads_5(TextU5BU5D_t4216439300* value)
	{
		___AreaLeads_5 = value;
		Il2CppCodeGenWriteBarrier(&___AreaLeads_5, value);
	}

	inline static int32_t get_offset_of_AreaNameOutline_6() { return static_cast<int32_t>(offsetof(AreaInfo_t2759954667, ___AreaNameOutline_6)); }
	inline Shadow_t4269599528 * get_AreaNameOutline_6() const { return ___AreaNameOutline_6; }
	inline Shadow_t4269599528 ** get_address_of_AreaNameOutline_6() { return &___AreaNameOutline_6; }
	inline void set_AreaNameOutline_6(Shadow_t4269599528 * value)
	{
		___AreaNameOutline_6 = value;
		Il2CppCodeGenWriteBarrier(&___AreaNameOutline_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
