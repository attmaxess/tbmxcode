﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// AreaInfo[]
struct AreaInfoU5BU5D_t4060720906;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaInfoCrossFader
struct  AreaInfoCrossFader_t818510077  : public MonoBehaviour_t1158329972
{
public:
	// AreaInfo[] AreaInfoCrossFader::AreaInfos
	AreaInfoU5BU5D_t4060720906* ___AreaInfos_2;
	// System.Int32 AreaInfoCrossFader::CurrentIndex
	int32_t ___CurrentIndex_3;
	// System.Boolean AreaInfoCrossFader::First
	bool ___First_4;

public:
	inline static int32_t get_offset_of_AreaInfos_2() { return static_cast<int32_t>(offsetof(AreaInfoCrossFader_t818510077, ___AreaInfos_2)); }
	inline AreaInfoU5BU5D_t4060720906* get_AreaInfos_2() const { return ___AreaInfos_2; }
	inline AreaInfoU5BU5D_t4060720906** get_address_of_AreaInfos_2() { return &___AreaInfos_2; }
	inline void set_AreaInfos_2(AreaInfoU5BU5D_t4060720906* value)
	{
		___AreaInfos_2 = value;
		Il2CppCodeGenWriteBarrier(&___AreaInfos_2, value);
	}

	inline static int32_t get_offset_of_CurrentIndex_3() { return static_cast<int32_t>(offsetof(AreaInfoCrossFader_t818510077, ___CurrentIndex_3)); }
	inline int32_t get_CurrentIndex_3() const { return ___CurrentIndex_3; }
	inline int32_t* get_address_of_CurrentIndex_3() { return &___CurrentIndex_3; }
	inline void set_CurrentIndex_3(int32_t value)
	{
		___CurrentIndex_3 = value;
	}

	inline static int32_t get_offset_of_First_4() { return static_cast<int32_t>(offsetof(AreaInfoCrossFader_t818510077, ___First_4)); }
	inline bool get_First_4() const { return ___First_4; }
	inline bool* get_address_of_First_4() { return &___First_4; }
	inline void set_First_4(bool value)
	{
		___First_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
