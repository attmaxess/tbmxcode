﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// AreaInfo
struct AreaInfo_t2759954667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaInfo/<FadeIn>c__AnonStorey0
struct  U3CFadeInU3Ec__AnonStorey0_t3951633184  : public Il2CppObject
{
public:
	// System.Single AreaInfo/<FadeIn>c__AnonStorey0::normal
	float ___normal_0;
	// System.Single AreaInfo/<FadeIn>c__AnonStorey0::time
	float ___time_1;
	// AreaInfo AreaInfo/<FadeIn>c__AnonStorey0::$this
	AreaInfo_t2759954667 * ___U24this_2;

public:
	inline static int32_t get_offset_of_normal_0() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__AnonStorey0_t3951633184, ___normal_0)); }
	inline float get_normal_0() const { return ___normal_0; }
	inline float* get_address_of_normal_0() { return &___normal_0; }
	inline void set_normal_0(float value)
	{
		___normal_0 = value;
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__AnonStorey0_t3951633184, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CFadeInU3Ec__AnonStorey0_t3951633184, ___U24this_2)); }
	inline AreaInfo_t2759954667 * get_U24this_2() const { return ___U24this_2; }
	inline AreaInfo_t2759954667 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AreaInfo_t2759954667 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
