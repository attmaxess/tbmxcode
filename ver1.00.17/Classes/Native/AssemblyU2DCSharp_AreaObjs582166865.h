﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaObjs
struct  AreaObjs_t582166865  : public Il2CppObject
{
public:
	// UnityEngine.GameObject AreaObjs::PartsArea
	GameObject_t1756533147 * ___PartsArea_0;
	// UnityEngine.GameObject AreaObjs::StartObjArea
	GameObject_t1756533147 * ___StartObjArea_1;
	// UnityEngine.GameObject AreaObjs::StartArea
	GameObject_t1756533147 * ___StartArea_2;
	// UnityEngine.GameObject AreaObjs::MobilityArea
	GameObject_t1756533147 * ___MobilityArea_3;
	// UnityEngine.GameObject AreaObjs::CreateArea
	GameObject_t1756533147 * ___CreateArea_4;
	// UnityEngine.GameObject AreaObjs::MiniMapArea
	GameObject_t1756533147 * ___MiniMapArea_5;
	// UnityEngine.GameObject AreaObjs::MyPageArea
	GameObject_t1756533147 * ___MyPageArea_6;

public:
	inline static int32_t get_offset_of_PartsArea_0() { return static_cast<int32_t>(offsetof(AreaObjs_t582166865, ___PartsArea_0)); }
	inline GameObject_t1756533147 * get_PartsArea_0() const { return ___PartsArea_0; }
	inline GameObject_t1756533147 ** get_address_of_PartsArea_0() { return &___PartsArea_0; }
	inline void set_PartsArea_0(GameObject_t1756533147 * value)
	{
		___PartsArea_0 = value;
		Il2CppCodeGenWriteBarrier(&___PartsArea_0, value);
	}

	inline static int32_t get_offset_of_StartObjArea_1() { return static_cast<int32_t>(offsetof(AreaObjs_t582166865, ___StartObjArea_1)); }
	inline GameObject_t1756533147 * get_StartObjArea_1() const { return ___StartObjArea_1; }
	inline GameObject_t1756533147 ** get_address_of_StartObjArea_1() { return &___StartObjArea_1; }
	inline void set_StartObjArea_1(GameObject_t1756533147 * value)
	{
		___StartObjArea_1 = value;
		Il2CppCodeGenWriteBarrier(&___StartObjArea_1, value);
	}

	inline static int32_t get_offset_of_StartArea_2() { return static_cast<int32_t>(offsetof(AreaObjs_t582166865, ___StartArea_2)); }
	inline GameObject_t1756533147 * get_StartArea_2() const { return ___StartArea_2; }
	inline GameObject_t1756533147 ** get_address_of_StartArea_2() { return &___StartArea_2; }
	inline void set_StartArea_2(GameObject_t1756533147 * value)
	{
		___StartArea_2 = value;
		Il2CppCodeGenWriteBarrier(&___StartArea_2, value);
	}

	inline static int32_t get_offset_of_MobilityArea_3() { return static_cast<int32_t>(offsetof(AreaObjs_t582166865, ___MobilityArea_3)); }
	inline GameObject_t1756533147 * get_MobilityArea_3() const { return ___MobilityArea_3; }
	inline GameObject_t1756533147 ** get_address_of_MobilityArea_3() { return &___MobilityArea_3; }
	inline void set_MobilityArea_3(GameObject_t1756533147 * value)
	{
		___MobilityArea_3 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityArea_3, value);
	}

	inline static int32_t get_offset_of_CreateArea_4() { return static_cast<int32_t>(offsetof(AreaObjs_t582166865, ___CreateArea_4)); }
	inline GameObject_t1756533147 * get_CreateArea_4() const { return ___CreateArea_4; }
	inline GameObject_t1756533147 ** get_address_of_CreateArea_4() { return &___CreateArea_4; }
	inline void set_CreateArea_4(GameObject_t1756533147 * value)
	{
		___CreateArea_4 = value;
		Il2CppCodeGenWriteBarrier(&___CreateArea_4, value);
	}

	inline static int32_t get_offset_of_MiniMapArea_5() { return static_cast<int32_t>(offsetof(AreaObjs_t582166865, ___MiniMapArea_5)); }
	inline GameObject_t1756533147 * get_MiniMapArea_5() const { return ___MiniMapArea_5; }
	inline GameObject_t1756533147 ** get_address_of_MiniMapArea_5() { return &___MiniMapArea_5; }
	inline void set_MiniMapArea_5(GameObject_t1756533147 * value)
	{
		___MiniMapArea_5 = value;
		Il2CppCodeGenWriteBarrier(&___MiniMapArea_5, value);
	}

	inline static int32_t get_offset_of_MyPageArea_6() { return static_cast<int32_t>(offsetof(AreaObjs_t582166865, ___MyPageArea_6)); }
	inline GameObject_t1756533147 * get_MyPageArea_6() const { return ___MyPageArea_6; }
	inline GameObject_t1756533147 ** get_address_of_MyPageArea_6() { return &___MyPageArea_6; }
	inline void set_MyPageArea_6(GameObject_t1756533147 * value)
	{
		___MyPageArea_6 = value;
		Il2CppCodeGenWriteBarrier(&___MyPageArea_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
