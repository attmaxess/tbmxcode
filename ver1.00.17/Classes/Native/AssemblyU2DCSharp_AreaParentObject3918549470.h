﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_eAREATYPE1739175762.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaParentObject
struct  AreaParentObject_t3918549470  : public Il2CppObject
{
public:
	// eAREATYPE AreaParentObject::areaType
	int32_t ___areaType_0;
	// UnityEngine.GameObject AreaParentObject::ParentObject
	GameObject_t1756533147 * ___ParentObject_1;

public:
	inline static int32_t get_offset_of_areaType_0() { return static_cast<int32_t>(offsetof(AreaParentObject_t3918549470, ___areaType_0)); }
	inline int32_t get_areaType_0() const { return ___areaType_0; }
	inline int32_t* get_address_of_areaType_0() { return &___areaType_0; }
	inline void set_areaType_0(int32_t value)
	{
		___areaType_0 = value;
	}

	inline static int32_t get_offset_of_ParentObject_1() { return static_cast<int32_t>(offsetof(AreaParentObject_t3918549470, ___ParentObject_1)); }
	inline GameObject_t1756533147 * get_ParentObject_1() const { return ___ParentObject_1; }
	inline GameObject_t1756533147 ** get_address_of_ParentObject_1() { return &___ParentObject_1; }
	inline void set_ParentObject_1(GameObject_t1756533147 * value)
	{
		___ParentObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___ParentObject_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
