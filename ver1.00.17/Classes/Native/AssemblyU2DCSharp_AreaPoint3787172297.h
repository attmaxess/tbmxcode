﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1967201810;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaPoint
struct  AreaPoint_t3787172297  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventTrigger AreaPoint::Trigger
	EventTrigger_t1967201810 * ___Trigger_2;
	// UnityEngine.CanvasGroup AreaPoint::AreaPoints
	CanvasGroup_t3296560743 * ___AreaPoints_3;
	// UnityEngine.CanvasGroup AreaPoint::Unselected
	CanvasGroup_t3296560743 * ___Unselected_4;
	// UnityEngine.CanvasGroup AreaPoint::Selection
	CanvasGroup_t3296560743 * ___Selection_5;
	// UnityEngine.UI.Text AreaPoint::UnselectedName
	Text_t356221433 * ___UnselectedName_6;
	// UnityEngine.UI.Text AreaPoint::SelectionName
	Text_t356221433 * ___SelectionName_7;
	// UnityEngine.LineRenderer AreaPoint::Line
	LineRenderer_t849157671 * ___Line_8;
	// UnityEngine.UI.Button AreaPoint::AreaPointButton
	Button_t2872111280 * ___AreaPointButton_9;

public:
	inline static int32_t get_offset_of_Trigger_2() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___Trigger_2)); }
	inline EventTrigger_t1967201810 * get_Trigger_2() const { return ___Trigger_2; }
	inline EventTrigger_t1967201810 ** get_address_of_Trigger_2() { return &___Trigger_2; }
	inline void set_Trigger_2(EventTrigger_t1967201810 * value)
	{
		___Trigger_2 = value;
		Il2CppCodeGenWriteBarrier(&___Trigger_2, value);
	}

	inline static int32_t get_offset_of_AreaPoints_3() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___AreaPoints_3)); }
	inline CanvasGroup_t3296560743 * get_AreaPoints_3() const { return ___AreaPoints_3; }
	inline CanvasGroup_t3296560743 ** get_address_of_AreaPoints_3() { return &___AreaPoints_3; }
	inline void set_AreaPoints_3(CanvasGroup_t3296560743 * value)
	{
		___AreaPoints_3 = value;
		Il2CppCodeGenWriteBarrier(&___AreaPoints_3, value);
	}

	inline static int32_t get_offset_of_Unselected_4() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___Unselected_4)); }
	inline CanvasGroup_t3296560743 * get_Unselected_4() const { return ___Unselected_4; }
	inline CanvasGroup_t3296560743 ** get_address_of_Unselected_4() { return &___Unselected_4; }
	inline void set_Unselected_4(CanvasGroup_t3296560743 * value)
	{
		___Unselected_4 = value;
		Il2CppCodeGenWriteBarrier(&___Unselected_4, value);
	}

	inline static int32_t get_offset_of_Selection_5() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___Selection_5)); }
	inline CanvasGroup_t3296560743 * get_Selection_5() const { return ___Selection_5; }
	inline CanvasGroup_t3296560743 ** get_address_of_Selection_5() { return &___Selection_5; }
	inline void set_Selection_5(CanvasGroup_t3296560743 * value)
	{
		___Selection_5 = value;
		Il2CppCodeGenWriteBarrier(&___Selection_5, value);
	}

	inline static int32_t get_offset_of_UnselectedName_6() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___UnselectedName_6)); }
	inline Text_t356221433 * get_UnselectedName_6() const { return ___UnselectedName_6; }
	inline Text_t356221433 ** get_address_of_UnselectedName_6() { return &___UnselectedName_6; }
	inline void set_UnselectedName_6(Text_t356221433 * value)
	{
		___UnselectedName_6 = value;
		Il2CppCodeGenWriteBarrier(&___UnselectedName_6, value);
	}

	inline static int32_t get_offset_of_SelectionName_7() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___SelectionName_7)); }
	inline Text_t356221433 * get_SelectionName_7() const { return ___SelectionName_7; }
	inline Text_t356221433 ** get_address_of_SelectionName_7() { return &___SelectionName_7; }
	inline void set_SelectionName_7(Text_t356221433 * value)
	{
		___SelectionName_7 = value;
		Il2CppCodeGenWriteBarrier(&___SelectionName_7, value);
	}

	inline static int32_t get_offset_of_Line_8() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___Line_8)); }
	inline LineRenderer_t849157671 * get_Line_8() const { return ___Line_8; }
	inline LineRenderer_t849157671 ** get_address_of_Line_8() { return &___Line_8; }
	inline void set_Line_8(LineRenderer_t849157671 * value)
	{
		___Line_8 = value;
		Il2CppCodeGenWriteBarrier(&___Line_8, value);
	}

	inline static int32_t get_offset_of_AreaPointButton_9() { return static_cast<int32_t>(offsetof(AreaPoint_t3787172297, ___AreaPointButton_9)); }
	inline Button_t2872111280 * get_AreaPointButton_9() const { return ___AreaPointButton_9; }
	inline Button_t2872111280 ** get_address_of_AreaPointButton_9() { return &___AreaPointButton_9; }
	inline void set_AreaPointButton_9(Button_t2872111280 * value)
	{
		___AreaPointButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___AreaPointButton_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
