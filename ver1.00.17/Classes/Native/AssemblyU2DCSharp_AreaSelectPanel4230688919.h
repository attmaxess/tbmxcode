﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// AreaInfoCrossFader
struct AreaInfoCrossFader_t818510077;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AreaSelectPanel
struct  AreaSelectPanel_t4230688919  : public MonoBehaviour_t1158329972
{
public:
	// AreaInfoCrossFader AreaSelectPanel::m_Fader
	AreaInfoCrossFader_t818510077 * ___m_Fader_2;

public:
	inline static int32_t get_offset_of_m_Fader_2() { return static_cast<int32_t>(offsetof(AreaSelectPanel_t4230688919, ___m_Fader_2)); }
	inline AreaInfoCrossFader_t818510077 * get_m_Fader_2() const { return ___m_Fader_2; }
	inline AreaInfoCrossFader_t818510077 ** get_address_of_m_Fader_2() { return &___m_Fader_2; }
	inline void set_m_Fader_2(AreaInfoCrossFader_t818510077 * value)
	{
		___m_Fader_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Fader_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
