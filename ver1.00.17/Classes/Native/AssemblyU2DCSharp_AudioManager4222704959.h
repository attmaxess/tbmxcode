﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2919381294.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Collections.Generic.Dictionary`2<eAUDIOBGM,UnityEngine.AudioClip>
struct Dictionary_2_t1584483224;
// System.Collections.Generic.Dictionary`2<eAREATYPE,UnityEngine.AudioClip>
struct Dictionary_2_t2511510679;
// System.Collections.Generic.Dictionary`2<eAUDIOSE,UnityEngine.AudioClip>
struct Dictionary_2_t3655030906;
// System.Collections.Generic.Dictionary`2<eAUDIOVOICE,UnityEngine.AudioClip>
struct Dictionary_2_t1408389782;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t4222704959  : public SingletonMonoBehaviour_1_t2919381294
{
public:
	// UnityEngine.AudioSource AudioManager::MainAudio
	AudioSource_t1135106623 * ___MainAudio_3;
	// UnityEngine.AudioClip AudioManager::SelectClip
	AudioClip_t1932558630 * ___SelectClip_4;
	// System.Single AudioManager::m_vMaxVolume
	float ___m_vMaxVolume_5;
	// System.Collections.Generic.Dictionary`2<eAUDIOBGM,UnityEngine.AudioClip> AudioManager::BGMDic
	Dictionary_2_t1584483224 * ___BGMDic_6;
	// System.Collections.Generic.Dictionary`2<eAREATYPE,UnityEngine.AudioClip> AudioManager::AreaBGMDic
	Dictionary_2_t2511510679 * ___AreaBGMDic_7;
	// System.Collections.Generic.Dictionary`2<eAUDIOSE,UnityEngine.AudioClip> AudioManager::SEDic
	Dictionary_2_t3655030906 * ___SEDic_8;
	// System.Collections.Generic.Dictionary`2<eAUDIOVOICE,UnityEngine.AudioClip> AudioManager::VoiceDic
	Dictionary_2_t1408389782 * ___VoiceDic_9;
	// System.Collections.Generic.Dictionary`2<eAREATYPE,UnityEngine.AudioClip> AudioManager::AreaSEDic
	Dictionary_2_t2511510679 * ___AreaSEDic_10;

public:
	inline static int32_t get_offset_of_MainAudio_3() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___MainAudio_3)); }
	inline AudioSource_t1135106623 * get_MainAudio_3() const { return ___MainAudio_3; }
	inline AudioSource_t1135106623 ** get_address_of_MainAudio_3() { return &___MainAudio_3; }
	inline void set_MainAudio_3(AudioSource_t1135106623 * value)
	{
		___MainAudio_3 = value;
		Il2CppCodeGenWriteBarrier(&___MainAudio_3, value);
	}

	inline static int32_t get_offset_of_SelectClip_4() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___SelectClip_4)); }
	inline AudioClip_t1932558630 * get_SelectClip_4() const { return ___SelectClip_4; }
	inline AudioClip_t1932558630 ** get_address_of_SelectClip_4() { return &___SelectClip_4; }
	inline void set_SelectClip_4(AudioClip_t1932558630 * value)
	{
		___SelectClip_4 = value;
		Il2CppCodeGenWriteBarrier(&___SelectClip_4, value);
	}

	inline static int32_t get_offset_of_m_vMaxVolume_5() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___m_vMaxVolume_5)); }
	inline float get_m_vMaxVolume_5() const { return ___m_vMaxVolume_5; }
	inline float* get_address_of_m_vMaxVolume_5() { return &___m_vMaxVolume_5; }
	inline void set_m_vMaxVolume_5(float value)
	{
		___m_vMaxVolume_5 = value;
	}

	inline static int32_t get_offset_of_BGMDic_6() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___BGMDic_6)); }
	inline Dictionary_2_t1584483224 * get_BGMDic_6() const { return ___BGMDic_6; }
	inline Dictionary_2_t1584483224 ** get_address_of_BGMDic_6() { return &___BGMDic_6; }
	inline void set_BGMDic_6(Dictionary_2_t1584483224 * value)
	{
		___BGMDic_6 = value;
		Il2CppCodeGenWriteBarrier(&___BGMDic_6, value);
	}

	inline static int32_t get_offset_of_AreaBGMDic_7() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___AreaBGMDic_7)); }
	inline Dictionary_2_t2511510679 * get_AreaBGMDic_7() const { return ___AreaBGMDic_7; }
	inline Dictionary_2_t2511510679 ** get_address_of_AreaBGMDic_7() { return &___AreaBGMDic_7; }
	inline void set_AreaBGMDic_7(Dictionary_2_t2511510679 * value)
	{
		___AreaBGMDic_7 = value;
		Il2CppCodeGenWriteBarrier(&___AreaBGMDic_7, value);
	}

	inline static int32_t get_offset_of_SEDic_8() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___SEDic_8)); }
	inline Dictionary_2_t3655030906 * get_SEDic_8() const { return ___SEDic_8; }
	inline Dictionary_2_t3655030906 ** get_address_of_SEDic_8() { return &___SEDic_8; }
	inline void set_SEDic_8(Dictionary_2_t3655030906 * value)
	{
		___SEDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___SEDic_8, value);
	}

	inline static int32_t get_offset_of_VoiceDic_9() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___VoiceDic_9)); }
	inline Dictionary_2_t1408389782 * get_VoiceDic_9() const { return ___VoiceDic_9; }
	inline Dictionary_2_t1408389782 ** get_address_of_VoiceDic_9() { return &___VoiceDic_9; }
	inline void set_VoiceDic_9(Dictionary_2_t1408389782 * value)
	{
		___VoiceDic_9 = value;
		Il2CppCodeGenWriteBarrier(&___VoiceDic_9, value);
	}

	inline static int32_t get_offset_of_AreaSEDic_10() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___AreaSEDic_10)); }
	inline Dictionary_2_t2511510679 * get_AreaSEDic_10() const { return ___AreaSEDic_10; }
	inline Dictionary_2_t2511510679 ** get_address_of_AreaSEDic_10() { return &___AreaSEDic_10; }
	inline void set_AreaSEDic_10(Dictionary_2_t2511510679 * value)
	{
		___AreaSEDic_10 = value;
		Il2CppCodeGenWriteBarrier(&___AreaSEDic_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
