﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundFader
struct  BackgroundFader_t837055620  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BackgroundFader::IsComplete
	bool ___IsComplete_2;
	// UnityEngine.UI.Image BackgroundFader::m_Image
	Image_t2042527209 * ___m_Image_3;

public:
	inline static int32_t get_offset_of_IsComplete_2() { return static_cast<int32_t>(offsetof(BackgroundFader_t837055620, ___IsComplete_2)); }
	inline bool get_IsComplete_2() const { return ___IsComplete_2; }
	inline bool* get_address_of_IsComplete_2() { return &___IsComplete_2; }
	inline void set_IsComplete_2(bool value)
	{
		___IsComplete_2 = value;
	}

	inline static int32_t get_offset_of_m_Image_3() { return static_cast<int32_t>(offsetof(BackgroundFader_t837055620, ___m_Image_3)); }
	inline Image_t2042527209 * get_m_Image_3() const { return ___m_Image_3; }
	inline Image_t2042527209 ** get_address_of_m_Image_3() { return &___m_Image_3; }
	inline void set_m_Image_3(Image_t2042527209 * value)
	{
		___m_Image_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Image_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
