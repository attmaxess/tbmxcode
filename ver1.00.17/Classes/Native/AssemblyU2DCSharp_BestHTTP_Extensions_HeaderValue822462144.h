﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>
struct List_1_t191583276;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t1675079469;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.HeaderValue
struct  HeaderValue_t822462144  : public Il2CppObject
{
public:
	// System.String BestHTTP.Extensions.HeaderValue::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_0;
	// System.String BestHTTP.Extensions.HeaderValue::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue> BestHTTP.Extensions.HeaderValue::<Options>k__BackingField
	List_1_t191583276 * ___U3COptionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HeaderValue_t822462144, ___U3CKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_0() const { return ___U3CKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_0() { return &___U3CKeyU3Ek__BackingField_0; }
	inline void set_U3CKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeyU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HeaderValue_t822462144, ___U3CValueU3Ek__BackingField_1)); }
	inline String_t* get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(String_t* value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CValueU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HeaderValue_t822462144, ___U3COptionsU3Ek__BackingField_2)); }
	inline List_1_t191583276 * get_U3COptionsU3Ek__BackingField_2() const { return ___U3COptionsU3Ek__BackingField_2; }
	inline List_1_t191583276 ** get_address_of_U3COptionsU3Ek__BackingField_2() { return &___U3COptionsU3Ek__BackingField_2; }
	inline void set_U3COptionsU3Ek__BackingField_2(List_1_t191583276 * value)
	{
		___U3COptionsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COptionsU3Ek__BackingField_2, value);
	}
};

struct HeaderValue_t822462144_StaticFields
{
public:
	// System.Func`2<System.Char,System.Boolean> BestHTTP.Extensions.HeaderValue::<>f__am$cache0
	Func_2_t1675079469 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(HeaderValue_t822462144_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t1675079469 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t1675079469 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t1675079469 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
