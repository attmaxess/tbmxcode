﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>
struct List_1_t191583276;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.KeyValuePairList
struct  KeyValuePairList_t1715528642  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue> BestHTTP.Extensions.KeyValuePairList::<Values>k__BackingField
	List_1_t191583276 * ___U3CValuesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValuesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(KeyValuePairList_t1715528642, ___U3CValuesU3Ek__BackingField_0)); }
	inline List_1_t191583276 * get_U3CValuesU3Ek__BackingField_0() const { return ___U3CValuesU3Ek__BackingField_0; }
	inline List_1_t191583276 ** get_address_of_U3CValuesU3Ek__BackingField_0() { return &___U3CValuesU3Ek__BackingField_0; }
	inline void set_U3CValuesU3Ek__BackingField_0(List_1_t191583276 * value)
	{
		___U3CValuesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CValuesU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
