﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_ConnectionStates420400692.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_ProtocolVersion4229923159.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "AssemblyU2DCSharp_BestHTTP_SupportedProtocols1503488249.h"

// BestHTTP.SignalR.JsonEncoders.IJsonEncoder
struct IJsonEncoder_t2969913559;
// System.Uri
struct Uri_t19570940;
// BestHTTP.SignalR.NegotiationData
struct NegotiationData_t3059020807;
// BestHTTP.SignalR.Hubs.Hub[]
struct HubU5BU5D_t767056102;
// BestHTTP.SignalR.Transports.TransportBase
struct TransportBase_t148904526;
// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>
struct ObservableDictionary_2_t1611561244;
// BestHTTP.SignalR.Authentication.IAuthenticationProvider
struct IAuthenticationProvider_t675015388;
// BestHTTP.SignalR.OnConnectedDelegate
struct OnConnectedDelegate_t3283761253;
// BestHTTP.SignalR.OnClosedDelegate
struct OnClosedDelegate_t587495364;
// BestHTTP.SignalR.OnErrorDelegate
struct OnErrorDelegate_t3605384424;
// BestHTTP.SignalR.OnStateChanged
struct OnStateChanged_t3950199048;
// BestHTTP.SignalR.OnNonHubMessageDelegate
struct OnNonHubMessageDelegate_t1922405057;
// BestHTTP.SignalR.OnPrepareRequestDelegate
struct OnPrepareRequestDelegate_t3434123680;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1642385972;
// BestHTTP.SignalR.Messages.MultiMessage
struct MultiMessage_t207384742;
// System.String
struct String_t;
// System.Collections.Generic.List`1<BestHTTP.SignalR.Messages.IServerMessage>
struct List_1_t1753264875;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Connection
struct  Connection_t2915781690  : public Il2CppObject
{
public:
	// System.Uri BestHTTP.SignalR.Connection::<Uri>k__BackingField
	Uri_t19570940 * ___U3CUriU3Ek__BackingField_1;
	// BestHTTP.SignalR.ConnectionStates BestHTTP.SignalR.Connection::_state
	int32_t ____state_2;
	// BestHTTP.SignalR.NegotiationData BestHTTP.SignalR.Connection::<NegotiationResult>k__BackingField
	NegotiationData_t3059020807 * ___U3CNegotiationResultU3Ek__BackingField_3;
	// BestHTTP.SignalR.Hubs.Hub[] BestHTTP.SignalR.Connection::<Hubs>k__BackingField
	HubU5BU5D_t767056102* ___U3CHubsU3Ek__BackingField_4;
	// BestHTTP.SignalR.Transports.TransportBase BestHTTP.SignalR.Connection::<Transport>k__BackingField
	TransportBase_t148904526 * ___U3CTransportU3Ek__BackingField_5;
	// BestHTTP.SignalR.ProtocolVersions BestHTTP.SignalR.Connection::<Protocol>k__BackingField
	uint8_t ___U3CProtocolU3Ek__BackingField_6;
	// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String> BestHTTP.SignalR.Connection::additionalQueryParams
	ObservableDictionary_2_t1611561244 * ___additionalQueryParams_7;
	// System.Boolean BestHTTP.SignalR.Connection::<QueryParamsOnlyForHandshake>k__BackingField
	bool ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8;
	// BestHTTP.SignalR.JsonEncoders.IJsonEncoder BestHTTP.SignalR.Connection::<JsonEncoder>k__BackingField
	Il2CppObject * ___U3CJsonEncoderU3Ek__BackingField_9;
	// BestHTTP.SignalR.Authentication.IAuthenticationProvider BestHTTP.SignalR.Connection::<AuthenticationProvider>k__BackingField
	Il2CppObject * ___U3CAuthenticationProviderU3Ek__BackingField_10;
	// System.TimeSpan BestHTTP.SignalR.Connection::<PingInterval>k__BackingField
	TimeSpan_t3430258949  ___U3CPingIntervalU3Ek__BackingField_11;
	// System.TimeSpan BestHTTP.SignalR.Connection::<ReconnectDelay>k__BackingField
	TimeSpan_t3430258949  ___U3CReconnectDelayU3Ek__BackingField_12;
	// BestHTTP.SignalR.OnConnectedDelegate BestHTTP.SignalR.Connection::OnConnected
	OnConnectedDelegate_t3283761253 * ___OnConnected_13;
	// BestHTTP.SignalR.OnClosedDelegate BestHTTP.SignalR.Connection::OnClosed
	OnClosedDelegate_t587495364 * ___OnClosed_14;
	// BestHTTP.SignalR.OnErrorDelegate BestHTTP.SignalR.Connection::OnError
	OnErrorDelegate_t3605384424 * ___OnError_15;
	// BestHTTP.SignalR.OnConnectedDelegate BestHTTP.SignalR.Connection::OnReconnecting
	OnConnectedDelegate_t3283761253 * ___OnReconnecting_16;
	// BestHTTP.SignalR.OnConnectedDelegate BestHTTP.SignalR.Connection::OnReconnected
	OnConnectedDelegate_t3283761253 * ___OnReconnected_17;
	// BestHTTP.SignalR.OnStateChanged BestHTTP.SignalR.Connection::OnStateChanged
	OnStateChanged_t3950199048 * ___OnStateChanged_18;
	// BestHTTP.SignalR.OnNonHubMessageDelegate BestHTTP.SignalR.Connection::OnNonHubMessage
	OnNonHubMessageDelegate_t1922405057 * ___OnNonHubMessage_19;
	// BestHTTP.SignalR.OnPrepareRequestDelegate BestHTTP.SignalR.Connection::<RequestPreparator>k__BackingField
	OnPrepareRequestDelegate_t3434123680 * ___U3CRequestPreparatorU3Ek__BackingField_20;
	// System.Object BestHTTP.SignalR.Connection::SyncRoot
	Il2CppObject * ___SyncRoot_21;
	// System.UInt64 BestHTTP.SignalR.Connection::<ClientMessageCounter>k__BackingField
	uint64_t ___U3CClientMessageCounterU3Ek__BackingField_22;
	// System.String[] BestHTTP.SignalR.Connection::ClientProtocols
	StringU5BU5D_t1642385972* ___ClientProtocols_23;
	// System.UInt64 BestHTTP.SignalR.Connection::RequestCounter
	uint64_t ___RequestCounter_24;
	// BestHTTP.SignalR.Messages.MultiMessage BestHTTP.SignalR.Connection::LastReceivedMessage
	MultiMessage_t207384742 * ___LastReceivedMessage_25;
	// System.String BestHTTP.SignalR.Connection::GroupsToken
	String_t* ___GroupsToken_26;
	// System.Collections.Generic.List`1<BestHTTP.SignalR.Messages.IServerMessage> BestHTTP.SignalR.Connection::BufferedMessages
	List_1_t1753264875 * ___BufferedMessages_27;
	// System.DateTime BestHTTP.SignalR.Connection::LastMessageReceivedAt
	DateTime_t693205669  ___LastMessageReceivedAt_28;
	// System.DateTime BestHTTP.SignalR.Connection::ReconnectStartedAt
	DateTime_t693205669  ___ReconnectStartedAt_29;
	// System.DateTime BestHTTP.SignalR.Connection::ReconnectDelayStartedAt
	DateTime_t693205669  ___ReconnectDelayStartedAt_30;
	// System.Boolean BestHTTP.SignalR.Connection::ReconnectStarted
	bool ___ReconnectStarted_31;
	// System.DateTime BestHTTP.SignalR.Connection::LastPingSentAt
	DateTime_t693205669  ___LastPingSentAt_32;
	// BestHTTP.HTTPRequest BestHTTP.SignalR.Connection::PingRequest
	HTTPRequest_t138485887 * ___PingRequest_33;
	// System.Nullable`1<System.DateTime> BestHTTP.SignalR.Connection::TransportConnectionStartedAt
	Nullable_1_t3251239280  ___TransportConnectionStartedAt_34;
	// System.Text.StringBuilder BestHTTP.SignalR.Connection::queryBuilder
	StringBuilder_t1221177846 * ___queryBuilder_35;
	// System.String BestHTTP.SignalR.Connection::BuiltConnectionData
	String_t* ___BuiltConnectionData_36;
	// System.String BestHTTP.SignalR.Connection::BuiltQueryParams
	String_t* ___BuiltQueryParams_37;
	// BestHTTP.SupportedProtocols BestHTTP.SignalR.Connection::NextProtocolToTry
	int32_t ___NextProtocolToTry_38;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CUriU3Ek__BackingField_1)); }
	inline Uri_t19570940 * get_U3CUriU3Ek__BackingField_1() const { return ___U3CUriU3Ek__BackingField_1; }
	inline Uri_t19570940 ** get_address_of_U3CUriU3Ek__BackingField_1() { return &___U3CUriU3Ek__BackingField_1; }
	inline void set_U3CUriU3Ek__BackingField_1(Uri_t19570940 * value)
	{
		___U3CUriU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUriU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ____state_2)); }
	inline int32_t get__state_2() const { return ____state_2; }
	inline int32_t* get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(int32_t value)
	{
		____state_2 = value;
	}

	inline static int32_t get_offset_of_U3CNegotiationResultU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CNegotiationResultU3Ek__BackingField_3)); }
	inline NegotiationData_t3059020807 * get_U3CNegotiationResultU3Ek__BackingField_3() const { return ___U3CNegotiationResultU3Ek__BackingField_3; }
	inline NegotiationData_t3059020807 ** get_address_of_U3CNegotiationResultU3Ek__BackingField_3() { return &___U3CNegotiationResultU3Ek__BackingField_3; }
	inline void set_U3CNegotiationResultU3Ek__BackingField_3(NegotiationData_t3059020807 * value)
	{
		___U3CNegotiationResultU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNegotiationResultU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CHubsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CHubsU3Ek__BackingField_4)); }
	inline HubU5BU5D_t767056102* get_U3CHubsU3Ek__BackingField_4() const { return ___U3CHubsU3Ek__BackingField_4; }
	inline HubU5BU5D_t767056102** get_address_of_U3CHubsU3Ek__BackingField_4() { return &___U3CHubsU3Ek__BackingField_4; }
	inline void set_U3CHubsU3Ek__BackingField_4(HubU5BU5D_t767056102* value)
	{
		___U3CHubsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHubsU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CTransportU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CTransportU3Ek__BackingField_5)); }
	inline TransportBase_t148904526 * get_U3CTransportU3Ek__BackingField_5() const { return ___U3CTransportU3Ek__BackingField_5; }
	inline TransportBase_t148904526 ** get_address_of_U3CTransportU3Ek__BackingField_5() { return &___U3CTransportU3Ek__BackingField_5; }
	inline void set_U3CTransportU3Ek__BackingField_5(TransportBase_t148904526 * value)
	{
		___U3CTransportU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTransportU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CProtocolU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CProtocolU3Ek__BackingField_6)); }
	inline uint8_t get_U3CProtocolU3Ek__BackingField_6() const { return ___U3CProtocolU3Ek__BackingField_6; }
	inline uint8_t* get_address_of_U3CProtocolU3Ek__BackingField_6() { return &___U3CProtocolU3Ek__BackingField_6; }
	inline void set_U3CProtocolU3Ek__BackingField_6(uint8_t value)
	{
		___U3CProtocolU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_additionalQueryParams_7() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___additionalQueryParams_7)); }
	inline ObservableDictionary_2_t1611561244 * get_additionalQueryParams_7() const { return ___additionalQueryParams_7; }
	inline ObservableDictionary_2_t1611561244 ** get_address_of_additionalQueryParams_7() { return &___additionalQueryParams_7; }
	inline void set_additionalQueryParams_7(ObservableDictionary_2_t1611561244 * value)
	{
		___additionalQueryParams_7 = value;
		Il2CppCodeGenWriteBarrier(&___additionalQueryParams_7, value);
	}

	inline static int32_t get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8)); }
	inline bool get_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8() const { return ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8() { return &___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8; }
	inline void set_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8(bool value)
	{
		___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CJsonEncoderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CJsonEncoderU3Ek__BackingField_9)); }
	inline Il2CppObject * get_U3CJsonEncoderU3Ek__BackingField_9() const { return ___U3CJsonEncoderU3Ek__BackingField_9; }
	inline Il2CppObject ** get_address_of_U3CJsonEncoderU3Ek__BackingField_9() { return &___U3CJsonEncoderU3Ek__BackingField_9; }
	inline void set_U3CJsonEncoderU3Ek__BackingField_9(Il2CppObject * value)
	{
		___U3CJsonEncoderU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CJsonEncoderU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CAuthenticationProviderU3Ek__BackingField_10)); }
	inline Il2CppObject * get_U3CAuthenticationProviderU3Ek__BackingField_10() const { return ___U3CAuthenticationProviderU3Ek__BackingField_10; }
	inline Il2CppObject ** get_address_of_U3CAuthenticationProviderU3Ek__BackingField_10() { return &___U3CAuthenticationProviderU3Ek__BackingField_10; }
	inline void set_U3CAuthenticationProviderU3Ek__BackingField_10(Il2CppObject * value)
	{
		___U3CAuthenticationProviderU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAuthenticationProviderU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CPingIntervalU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CPingIntervalU3Ek__BackingField_11)); }
	inline TimeSpan_t3430258949  get_U3CPingIntervalU3Ek__BackingField_11() const { return ___U3CPingIntervalU3Ek__BackingField_11; }
	inline TimeSpan_t3430258949 * get_address_of_U3CPingIntervalU3Ek__BackingField_11() { return &___U3CPingIntervalU3Ek__BackingField_11; }
	inline void set_U3CPingIntervalU3Ek__BackingField_11(TimeSpan_t3430258949  value)
	{
		___U3CPingIntervalU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectDelayU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CReconnectDelayU3Ek__BackingField_12)); }
	inline TimeSpan_t3430258949  get_U3CReconnectDelayU3Ek__BackingField_12() const { return ___U3CReconnectDelayU3Ek__BackingField_12; }
	inline TimeSpan_t3430258949 * get_address_of_U3CReconnectDelayU3Ek__BackingField_12() { return &___U3CReconnectDelayU3Ek__BackingField_12; }
	inline void set_U3CReconnectDelayU3Ek__BackingField_12(TimeSpan_t3430258949  value)
	{
		___U3CReconnectDelayU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_OnConnected_13() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___OnConnected_13)); }
	inline OnConnectedDelegate_t3283761253 * get_OnConnected_13() const { return ___OnConnected_13; }
	inline OnConnectedDelegate_t3283761253 ** get_address_of_OnConnected_13() { return &___OnConnected_13; }
	inline void set_OnConnected_13(OnConnectedDelegate_t3283761253 * value)
	{
		___OnConnected_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnConnected_13, value);
	}

	inline static int32_t get_offset_of_OnClosed_14() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___OnClosed_14)); }
	inline OnClosedDelegate_t587495364 * get_OnClosed_14() const { return ___OnClosed_14; }
	inline OnClosedDelegate_t587495364 ** get_address_of_OnClosed_14() { return &___OnClosed_14; }
	inline void set_OnClosed_14(OnClosedDelegate_t587495364 * value)
	{
		___OnClosed_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnClosed_14, value);
	}

	inline static int32_t get_offset_of_OnError_15() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___OnError_15)); }
	inline OnErrorDelegate_t3605384424 * get_OnError_15() const { return ___OnError_15; }
	inline OnErrorDelegate_t3605384424 ** get_address_of_OnError_15() { return &___OnError_15; }
	inline void set_OnError_15(OnErrorDelegate_t3605384424 * value)
	{
		___OnError_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnError_15, value);
	}

	inline static int32_t get_offset_of_OnReconnecting_16() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___OnReconnecting_16)); }
	inline OnConnectedDelegate_t3283761253 * get_OnReconnecting_16() const { return ___OnReconnecting_16; }
	inline OnConnectedDelegate_t3283761253 ** get_address_of_OnReconnecting_16() { return &___OnReconnecting_16; }
	inline void set_OnReconnecting_16(OnConnectedDelegate_t3283761253 * value)
	{
		___OnReconnecting_16 = value;
		Il2CppCodeGenWriteBarrier(&___OnReconnecting_16, value);
	}

	inline static int32_t get_offset_of_OnReconnected_17() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___OnReconnected_17)); }
	inline OnConnectedDelegate_t3283761253 * get_OnReconnected_17() const { return ___OnReconnected_17; }
	inline OnConnectedDelegate_t3283761253 ** get_address_of_OnReconnected_17() { return &___OnReconnected_17; }
	inline void set_OnReconnected_17(OnConnectedDelegate_t3283761253 * value)
	{
		___OnReconnected_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnReconnected_17, value);
	}

	inline static int32_t get_offset_of_OnStateChanged_18() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___OnStateChanged_18)); }
	inline OnStateChanged_t3950199048 * get_OnStateChanged_18() const { return ___OnStateChanged_18; }
	inline OnStateChanged_t3950199048 ** get_address_of_OnStateChanged_18() { return &___OnStateChanged_18; }
	inline void set_OnStateChanged_18(OnStateChanged_t3950199048 * value)
	{
		___OnStateChanged_18 = value;
		Il2CppCodeGenWriteBarrier(&___OnStateChanged_18, value);
	}

	inline static int32_t get_offset_of_OnNonHubMessage_19() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___OnNonHubMessage_19)); }
	inline OnNonHubMessageDelegate_t1922405057 * get_OnNonHubMessage_19() const { return ___OnNonHubMessage_19; }
	inline OnNonHubMessageDelegate_t1922405057 ** get_address_of_OnNonHubMessage_19() { return &___OnNonHubMessage_19; }
	inline void set_OnNonHubMessage_19(OnNonHubMessageDelegate_t1922405057 * value)
	{
		___OnNonHubMessage_19 = value;
		Il2CppCodeGenWriteBarrier(&___OnNonHubMessage_19, value);
	}

	inline static int32_t get_offset_of_U3CRequestPreparatorU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CRequestPreparatorU3Ek__BackingField_20)); }
	inline OnPrepareRequestDelegate_t3434123680 * get_U3CRequestPreparatorU3Ek__BackingField_20() const { return ___U3CRequestPreparatorU3Ek__BackingField_20; }
	inline OnPrepareRequestDelegate_t3434123680 ** get_address_of_U3CRequestPreparatorU3Ek__BackingField_20() { return &___U3CRequestPreparatorU3Ek__BackingField_20; }
	inline void set_U3CRequestPreparatorU3Ek__BackingField_20(OnPrepareRequestDelegate_t3434123680 * value)
	{
		___U3CRequestPreparatorU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestPreparatorU3Ek__BackingField_20, value);
	}

	inline static int32_t get_offset_of_SyncRoot_21() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___SyncRoot_21)); }
	inline Il2CppObject * get_SyncRoot_21() const { return ___SyncRoot_21; }
	inline Il2CppObject ** get_address_of_SyncRoot_21() { return &___SyncRoot_21; }
	inline void set_SyncRoot_21(Il2CppObject * value)
	{
		___SyncRoot_21 = value;
		Il2CppCodeGenWriteBarrier(&___SyncRoot_21, value);
	}

	inline static int32_t get_offset_of_U3CClientMessageCounterU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___U3CClientMessageCounterU3Ek__BackingField_22)); }
	inline uint64_t get_U3CClientMessageCounterU3Ek__BackingField_22() const { return ___U3CClientMessageCounterU3Ek__BackingField_22; }
	inline uint64_t* get_address_of_U3CClientMessageCounterU3Ek__BackingField_22() { return &___U3CClientMessageCounterU3Ek__BackingField_22; }
	inline void set_U3CClientMessageCounterU3Ek__BackingField_22(uint64_t value)
	{
		___U3CClientMessageCounterU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_ClientProtocols_23() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___ClientProtocols_23)); }
	inline StringU5BU5D_t1642385972* get_ClientProtocols_23() const { return ___ClientProtocols_23; }
	inline StringU5BU5D_t1642385972** get_address_of_ClientProtocols_23() { return &___ClientProtocols_23; }
	inline void set_ClientProtocols_23(StringU5BU5D_t1642385972* value)
	{
		___ClientProtocols_23 = value;
		Il2CppCodeGenWriteBarrier(&___ClientProtocols_23, value);
	}

	inline static int32_t get_offset_of_RequestCounter_24() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___RequestCounter_24)); }
	inline uint64_t get_RequestCounter_24() const { return ___RequestCounter_24; }
	inline uint64_t* get_address_of_RequestCounter_24() { return &___RequestCounter_24; }
	inline void set_RequestCounter_24(uint64_t value)
	{
		___RequestCounter_24 = value;
	}

	inline static int32_t get_offset_of_LastReceivedMessage_25() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___LastReceivedMessage_25)); }
	inline MultiMessage_t207384742 * get_LastReceivedMessage_25() const { return ___LastReceivedMessage_25; }
	inline MultiMessage_t207384742 ** get_address_of_LastReceivedMessage_25() { return &___LastReceivedMessage_25; }
	inline void set_LastReceivedMessage_25(MultiMessage_t207384742 * value)
	{
		___LastReceivedMessage_25 = value;
		Il2CppCodeGenWriteBarrier(&___LastReceivedMessage_25, value);
	}

	inline static int32_t get_offset_of_GroupsToken_26() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___GroupsToken_26)); }
	inline String_t* get_GroupsToken_26() const { return ___GroupsToken_26; }
	inline String_t** get_address_of_GroupsToken_26() { return &___GroupsToken_26; }
	inline void set_GroupsToken_26(String_t* value)
	{
		___GroupsToken_26 = value;
		Il2CppCodeGenWriteBarrier(&___GroupsToken_26, value);
	}

	inline static int32_t get_offset_of_BufferedMessages_27() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___BufferedMessages_27)); }
	inline List_1_t1753264875 * get_BufferedMessages_27() const { return ___BufferedMessages_27; }
	inline List_1_t1753264875 ** get_address_of_BufferedMessages_27() { return &___BufferedMessages_27; }
	inline void set_BufferedMessages_27(List_1_t1753264875 * value)
	{
		___BufferedMessages_27 = value;
		Il2CppCodeGenWriteBarrier(&___BufferedMessages_27, value);
	}

	inline static int32_t get_offset_of_LastMessageReceivedAt_28() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___LastMessageReceivedAt_28)); }
	inline DateTime_t693205669  get_LastMessageReceivedAt_28() const { return ___LastMessageReceivedAt_28; }
	inline DateTime_t693205669 * get_address_of_LastMessageReceivedAt_28() { return &___LastMessageReceivedAt_28; }
	inline void set_LastMessageReceivedAt_28(DateTime_t693205669  value)
	{
		___LastMessageReceivedAt_28 = value;
	}

	inline static int32_t get_offset_of_ReconnectStartedAt_29() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___ReconnectStartedAt_29)); }
	inline DateTime_t693205669  get_ReconnectStartedAt_29() const { return ___ReconnectStartedAt_29; }
	inline DateTime_t693205669 * get_address_of_ReconnectStartedAt_29() { return &___ReconnectStartedAt_29; }
	inline void set_ReconnectStartedAt_29(DateTime_t693205669  value)
	{
		___ReconnectStartedAt_29 = value;
	}

	inline static int32_t get_offset_of_ReconnectDelayStartedAt_30() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___ReconnectDelayStartedAt_30)); }
	inline DateTime_t693205669  get_ReconnectDelayStartedAt_30() const { return ___ReconnectDelayStartedAt_30; }
	inline DateTime_t693205669 * get_address_of_ReconnectDelayStartedAt_30() { return &___ReconnectDelayStartedAt_30; }
	inline void set_ReconnectDelayStartedAt_30(DateTime_t693205669  value)
	{
		___ReconnectDelayStartedAt_30 = value;
	}

	inline static int32_t get_offset_of_ReconnectStarted_31() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___ReconnectStarted_31)); }
	inline bool get_ReconnectStarted_31() const { return ___ReconnectStarted_31; }
	inline bool* get_address_of_ReconnectStarted_31() { return &___ReconnectStarted_31; }
	inline void set_ReconnectStarted_31(bool value)
	{
		___ReconnectStarted_31 = value;
	}

	inline static int32_t get_offset_of_LastPingSentAt_32() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___LastPingSentAt_32)); }
	inline DateTime_t693205669  get_LastPingSentAt_32() const { return ___LastPingSentAt_32; }
	inline DateTime_t693205669 * get_address_of_LastPingSentAt_32() { return &___LastPingSentAt_32; }
	inline void set_LastPingSentAt_32(DateTime_t693205669  value)
	{
		___LastPingSentAt_32 = value;
	}

	inline static int32_t get_offset_of_PingRequest_33() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___PingRequest_33)); }
	inline HTTPRequest_t138485887 * get_PingRequest_33() const { return ___PingRequest_33; }
	inline HTTPRequest_t138485887 ** get_address_of_PingRequest_33() { return &___PingRequest_33; }
	inline void set_PingRequest_33(HTTPRequest_t138485887 * value)
	{
		___PingRequest_33 = value;
		Il2CppCodeGenWriteBarrier(&___PingRequest_33, value);
	}

	inline static int32_t get_offset_of_TransportConnectionStartedAt_34() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___TransportConnectionStartedAt_34)); }
	inline Nullable_1_t3251239280  get_TransportConnectionStartedAt_34() const { return ___TransportConnectionStartedAt_34; }
	inline Nullable_1_t3251239280 * get_address_of_TransportConnectionStartedAt_34() { return &___TransportConnectionStartedAt_34; }
	inline void set_TransportConnectionStartedAt_34(Nullable_1_t3251239280  value)
	{
		___TransportConnectionStartedAt_34 = value;
	}

	inline static int32_t get_offset_of_queryBuilder_35() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___queryBuilder_35)); }
	inline StringBuilder_t1221177846 * get_queryBuilder_35() const { return ___queryBuilder_35; }
	inline StringBuilder_t1221177846 ** get_address_of_queryBuilder_35() { return &___queryBuilder_35; }
	inline void set_queryBuilder_35(StringBuilder_t1221177846 * value)
	{
		___queryBuilder_35 = value;
		Il2CppCodeGenWriteBarrier(&___queryBuilder_35, value);
	}

	inline static int32_t get_offset_of_BuiltConnectionData_36() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___BuiltConnectionData_36)); }
	inline String_t* get_BuiltConnectionData_36() const { return ___BuiltConnectionData_36; }
	inline String_t** get_address_of_BuiltConnectionData_36() { return &___BuiltConnectionData_36; }
	inline void set_BuiltConnectionData_36(String_t* value)
	{
		___BuiltConnectionData_36 = value;
		Il2CppCodeGenWriteBarrier(&___BuiltConnectionData_36, value);
	}

	inline static int32_t get_offset_of_BuiltQueryParams_37() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___BuiltQueryParams_37)); }
	inline String_t* get_BuiltQueryParams_37() const { return ___BuiltQueryParams_37; }
	inline String_t** get_address_of_BuiltQueryParams_37() { return &___BuiltQueryParams_37; }
	inline void set_BuiltQueryParams_37(String_t* value)
	{
		___BuiltQueryParams_37 = value;
		Il2CppCodeGenWriteBarrier(&___BuiltQueryParams_37, value);
	}

	inline static int32_t get_offset_of_NextProtocolToTry_38() { return static_cast<int32_t>(offsetof(Connection_t2915781690, ___NextProtocolToTry_38)); }
	inline int32_t get_NextProtocolToTry_38() const { return ___NextProtocolToTry_38; }
	inline int32_t* get_address_of_NextProtocolToTry_38() { return &___NextProtocolToTry_38; }
	inline void set_NextProtocolToTry_38(int32_t value)
	{
		___NextProtocolToTry_38 = value;
	}
};

struct Connection_t2915781690_StaticFields
{
public:
	// BestHTTP.SignalR.JsonEncoders.IJsonEncoder BestHTTP.SignalR.Connection::DefaultEncoder
	Il2CppObject * ___DefaultEncoder_0;

public:
	inline static int32_t get_offset_of_DefaultEncoder_0() { return static_cast<int32_t>(offsetof(Connection_t2915781690_StaticFields, ___DefaultEncoder_0)); }
	inline Il2CppObject * get_DefaultEncoder_0() const { return ___DefaultEncoder_0; }
	inline Il2CppObject ** get_address_of_DefaultEncoder_0() { return &___DefaultEncoder_0; }
	inline void set_DefaultEncoder_0(Il2CppObject * value)
	{
		___DefaultEncoder_0 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultEncoder_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
