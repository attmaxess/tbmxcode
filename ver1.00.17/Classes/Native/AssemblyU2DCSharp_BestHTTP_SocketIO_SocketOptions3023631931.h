﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Tra1768294366.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>
struct ObservableDictionary_2_t1611561244;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.SocketOptions
struct  SocketOptions_t3023631931  : public Il2CppObject
{
public:
	// BestHTTP.SocketIO.Transports.TransportTypes BestHTTP.SocketIO.SocketOptions::<ConnectWith>k__BackingField
	int32_t ___U3CConnectWithU3Ek__BackingField_0;
	// System.Boolean BestHTTP.SocketIO.SocketOptions::<Reconnection>k__BackingField
	bool ___U3CReconnectionU3Ek__BackingField_1;
	// System.Int32 BestHTTP.SocketIO.SocketOptions::<ReconnectionAttempts>k__BackingField
	int32_t ___U3CReconnectionAttemptsU3Ek__BackingField_2;
	// System.TimeSpan BestHTTP.SocketIO.SocketOptions::<ReconnectionDelay>k__BackingField
	TimeSpan_t3430258949  ___U3CReconnectionDelayU3Ek__BackingField_3;
	// System.TimeSpan BestHTTP.SocketIO.SocketOptions::<ReconnectionDelayMax>k__BackingField
	TimeSpan_t3430258949  ___U3CReconnectionDelayMaxU3Ek__BackingField_4;
	// System.Single BestHTTP.SocketIO.SocketOptions::randomizationFactor
	float ___randomizationFactor_5;
	// System.TimeSpan BestHTTP.SocketIO.SocketOptions::<Timeout>k__BackingField
	TimeSpan_t3430258949  ___U3CTimeoutU3Ek__BackingField_6;
	// System.Boolean BestHTTP.SocketIO.SocketOptions::<AutoConnect>k__BackingField
	bool ___U3CAutoConnectU3Ek__BackingField_7;
	// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String> BestHTTP.SocketIO.SocketOptions::additionalQueryParams
	ObservableDictionary_2_t1611561244 * ___additionalQueryParams_8;
	// System.Boolean BestHTTP.SocketIO.SocketOptions::<QueryParamsOnlyForHandshake>k__BackingField
	bool ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9;
	// System.String BestHTTP.SocketIO.SocketOptions::BuiltQueryParams
	String_t* ___BuiltQueryParams_10;

public:
	inline static int32_t get_offset_of_U3CConnectWithU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CConnectWithU3Ek__BackingField_0)); }
	inline int32_t get_U3CConnectWithU3Ek__BackingField_0() const { return ___U3CConnectWithU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CConnectWithU3Ek__BackingField_0() { return &___U3CConnectWithU3Ek__BackingField_0; }
	inline void set_U3CConnectWithU3Ek__BackingField_0(int32_t value)
	{
		___U3CConnectWithU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CReconnectionU3Ek__BackingField_1)); }
	inline bool get_U3CReconnectionU3Ek__BackingField_1() const { return ___U3CReconnectionU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CReconnectionU3Ek__BackingField_1() { return &___U3CReconnectionU3Ek__BackingField_1; }
	inline void set_U3CReconnectionU3Ek__BackingField_1(bool value)
	{
		___U3CReconnectionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CReconnectionAttemptsU3Ek__BackingField_2)); }
	inline int32_t get_U3CReconnectionAttemptsU3Ek__BackingField_2() const { return ___U3CReconnectionAttemptsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CReconnectionAttemptsU3Ek__BackingField_2() { return &___U3CReconnectionAttemptsU3Ek__BackingField_2; }
	inline void set_U3CReconnectionAttemptsU3Ek__BackingField_2(int32_t value)
	{
		___U3CReconnectionAttemptsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionDelayU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CReconnectionDelayU3Ek__BackingField_3)); }
	inline TimeSpan_t3430258949  get_U3CReconnectionDelayU3Ek__BackingField_3() const { return ___U3CReconnectionDelayU3Ek__BackingField_3; }
	inline TimeSpan_t3430258949 * get_address_of_U3CReconnectionDelayU3Ek__BackingField_3() { return &___U3CReconnectionDelayU3Ek__BackingField_3; }
	inline void set_U3CReconnectionDelayU3Ek__BackingField_3(TimeSpan_t3430258949  value)
	{
		___U3CReconnectionDelayU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionDelayMaxU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CReconnectionDelayMaxU3Ek__BackingField_4)); }
	inline TimeSpan_t3430258949  get_U3CReconnectionDelayMaxU3Ek__BackingField_4() const { return ___U3CReconnectionDelayMaxU3Ek__BackingField_4; }
	inline TimeSpan_t3430258949 * get_address_of_U3CReconnectionDelayMaxU3Ek__BackingField_4() { return &___U3CReconnectionDelayMaxU3Ek__BackingField_4; }
	inline void set_U3CReconnectionDelayMaxU3Ek__BackingField_4(TimeSpan_t3430258949  value)
	{
		___U3CReconnectionDelayMaxU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_randomizationFactor_5() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___randomizationFactor_5)); }
	inline float get_randomizationFactor_5() const { return ___randomizationFactor_5; }
	inline float* get_address_of_randomizationFactor_5() { return &___randomizationFactor_5; }
	inline void set_randomizationFactor_5(float value)
	{
		___randomizationFactor_5 = value;
	}

	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CTimeoutU3Ek__BackingField_6)); }
	inline TimeSpan_t3430258949  get_U3CTimeoutU3Ek__BackingField_6() const { return ___U3CTimeoutU3Ek__BackingField_6; }
	inline TimeSpan_t3430258949 * get_address_of_U3CTimeoutU3Ek__BackingField_6() { return &___U3CTimeoutU3Ek__BackingField_6; }
	inline void set_U3CTimeoutU3Ek__BackingField_6(TimeSpan_t3430258949  value)
	{
		___U3CTimeoutU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAutoConnectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CAutoConnectU3Ek__BackingField_7)); }
	inline bool get_U3CAutoConnectU3Ek__BackingField_7() const { return ___U3CAutoConnectU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CAutoConnectU3Ek__BackingField_7() { return &___U3CAutoConnectU3Ek__BackingField_7; }
	inline void set_U3CAutoConnectU3Ek__BackingField_7(bool value)
	{
		___U3CAutoConnectU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_additionalQueryParams_8() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___additionalQueryParams_8)); }
	inline ObservableDictionary_2_t1611561244 * get_additionalQueryParams_8() const { return ___additionalQueryParams_8; }
	inline ObservableDictionary_2_t1611561244 ** get_address_of_additionalQueryParams_8() { return &___additionalQueryParams_8; }
	inline void set_additionalQueryParams_8(ObservableDictionary_2_t1611561244 * value)
	{
		___additionalQueryParams_8 = value;
		Il2CppCodeGenWriteBarrier(&___additionalQueryParams_8, value);
	}

	inline static int32_t get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9)); }
	inline bool get_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9() const { return ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9() { return &___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9; }
	inline void set_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9(bool value)
	{
		___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_BuiltQueryParams_10() { return static_cast<int32_t>(offsetof(SocketOptions_t3023631931, ___BuiltQueryParams_10)); }
	inline String_t* get_BuiltQueryParams_10() const { return ___BuiltQueryParams_10; }
	inline String_t** get_address_of_BuiltQueryParams_10() { return &___BuiltQueryParams_10; }
	inline void set_BuiltQueryParams_10(String_t* value)
	{
		___BuiltQueryParams_10 = value;
		Il2CppCodeGenWriteBarrier(&___BuiltQueryParams_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
