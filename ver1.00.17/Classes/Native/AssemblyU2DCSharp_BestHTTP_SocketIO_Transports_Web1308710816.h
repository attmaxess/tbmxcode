﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Tra1004880669.h"

// BestHTTP.SocketIO.SocketManager
struct SocketManager_t3470268644;
// BestHTTP.WebSocket.WebSocket
struct WebSocket_t71448861;
// BestHTTP.SocketIO.Packet
struct Packet_t1309324146;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Transports.WebSocketTransport
struct  WebSocketTransport_t1308710816  : public Il2CppObject
{
public:
	// BestHTTP.SocketIO.Transports.TransportStates BestHTTP.SocketIO.Transports.WebSocketTransport::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_0;
	// BestHTTP.SocketIO.SocketManager BestHTTP.SocketIO.Transports.WebSocketTransport::<Manager>k__BackingField
	SocketManager_t3470268644 * ___U3CManagerU3Ek__BackingField_1;
	// BestHTTP.WebSocket.WebSocket BestHTTP.SocketIO.Transports.WebSocketTransport::<Implementation>k__BackingField
	WebSocket_t71448861 * ___U3CImplementationU3Ek__BackingField_2;
	// BestHTTP.SocketIO.Packet BestHTTP.SocketIO.Transports.WebSocketTransport::PacketWithAttachment
	Packet_t1309324146 * ___PacketWithAttachment_3;
	// System.Byte[] BestHTTP.SocketIO.Transports.WebSocketTransport::Buffer
	ByteU5BU5D_t3397334013* ___Buffer_4;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocketTransport_t1308710816, ___U3CStateU3Ek__BackingField_0)); }
	inline int32_t get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(int32_t value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebSocketTransport_t1308710816, ___U3CManagerU3Ek__BackingField_1)); }
	inline SocketManager_t3470268644 * get_U3CManagerU3Ek__BackingField_1() const { return ___U3CManagerU3Ek__BackingField_1; }
	inline SocketManager_t3470268644 ** get_address_of_U3CManagerU3Ek__BackingField_1() { return &___U3CManagerU3Ek__BackingField_1; }
	inline void set_U3CManagerU3Ek__BackingField_1(SocketManager_t3470268644 * value)
	{
		___U3CManagerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CManagerU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CImplementationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketTransport_t1308710816, ___U3CImplementationU3Ek__BackingField_2)); }
	inline WebSocket_t71448861 * get_U3CImplementationU3Ek__BackingField_2() const { return ___U3CImplementationU3Ek__BackingField_2; }
	inline WebSocket_t71448861 ** get_address_of_U3CImplementationU3Ek__BackingField_2() { return &___U3CImplementationU3Ek__BackingField_2; }
	inline void set_U3CImplementationU3Ek__BackingField_2(WebSocket_t71448861 * value)
	{
		___U3CImplementationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CImplementationU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_PacketWithAttachment_3() { return static_cast<int32_t>(offsetof(WebSocketTransport_t1308710816, ___PacketWithAttachment_3)); }
	inline Packet_t1309324146 * get_PacketWithAttachment_3() const { return ___PacketWithAttachment_3; }
	inline Packet_t1309324146 ** get_address_of_PacketWithAttachment_3() { return &___PacketWithAttachment_3; }
	inline void set_PacketWithAttachment_3(Packet_t1309324146 * value)
	{
		___PacketWithAttachment_3 = value;
		Il2CppCodeGenWriteBarrier(&___PacketWithAttachment_3, value);
	}

	inline static int32_t get_offset_of_Buffer_4() { return static_cast<int32_t>(offsetof(WebSocketTransport_t1308710816, ___Buffer_4)); }
	inline ByteU5BU5D_t3397334013* get_Buffer_4() const { return ___Buffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_Buffer_4() { return &___Buffer_4; }
	inline void set_Buffer_4(ByteU5BU5D_t3397334013* value)
	{
		___Buffer_4 = value;
		Il2CppCodeGenWriteBarrier(&___Buffer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
