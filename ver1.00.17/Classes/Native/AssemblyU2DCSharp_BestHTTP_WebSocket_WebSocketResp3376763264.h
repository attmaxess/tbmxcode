﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_DateTime693205669.h"

// BestHTTP.WebSocket.WebSocket
struct WebSocket_t71448861;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.String>
struct Action_2_t1029759410;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]>
struct Action_2_t2397873190;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,BestHTTP.WebSocket.Frames.WebSocketFrameReader>
struct Action_2_t3844780342;
// System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String>
struct Action_3_t2791643950;
// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader>
struct List_1_t4213362297;
// BestHTTP.WebSocket.Frames.WebSocketFrameReader
struct WebSocketFrameReader_t549273869;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrame>
struct List_1_t3532404526;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.WebSocketResponse
struct  WebSocketResponse_t3376763264  : public HTTPResponse_t62748825
{
public:
	// BestHTTP.WebSocket.WebSocket BestHTTP.WebSocket.WebSocketResponse::<WebSocket>k__BackingField
	WebSocket_t71448861 * ___U3CWebSocketU3Ek__BackingField_31;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.String> BestHTTP.WebSocket.WebSocketResponse::OnText
	Action_2_t1029759410 * ___OnText_32;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]> BestHTTP.WebSocket.WebSocketResponse::OnBinary
	Action_2_t2397873190 * ___OnBinary_33;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::OnIncompleteFrame
	Action_2_t3844780342 * ___OnIncompleteFrame_34;
	// System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String> BestHTTP.WebSocket.WebSocketResponse::OnClosed
	Action_3_t2791643950 * ___OnClosed_35;
	// System.TimeSpan BestHTTP.WebSocket.WebSocketResponse::<PingFrequnecy>k__BackingField
	TimeSpan_t3430258949  ___U3CPingFrequnecyU3Ek__BackingField_36;
	// System.UInt16 BestHTTP.WebSocket.WebSocketResponse::<MaxFragmentSize>k__BackingField
	uint16_t ___U3CMaxFragmentSizeU3Ek__BackingField_37;
	// System.Int32 BestHTTP.WebSocket.WebSocketResponse::_bufferedAmount
	int32_t ____bufferedAmount_38;
	// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::IncompleteFrames
	List_1_t4213362297 * ___IncompleteFrames_39;
	// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::CompletedFrames
	List_1_t4213362297 * ___CompletedFrames_40;
	// BestHTTP.WebSocket.Frames.WebSocketFrameReader BestHTTP.WebSocket.WebSocketResponse::CloseFrame
	WebSocketFrameReader_t549273869 * ___CloseFrame_41;
	// System.Object BestHTTP.WebSocket.WebSocketResponse::FrameLock
	Il2CppObject * ___FrameLock_42;
	// System.Object BestHTTP.WebSocket.WebSocketResponse::SendLock
	Il2CppObject * ___SendLock_43;
	// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrame> BestHTTP.WebSocket.WebSocketResponse::unsentFrames
	List_1_t3532404526 * ___unsentFrames_44;
	// System.Threading.AutoResetEvent BestHTTP.WebSocket.WebSocketResponse::newFrameSignal
	AutoResetEvent_t15112628 * ___newFrameSignal_45;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.WebSocket.WebSocketResponse::sendThreadCreated
	bool ___sendThreadCreated_46;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.WebSocket.WebSocketResponse::closeSent
	bool ___closeSent_47;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.WebSocket.WebSocketResponse::closed
	bool ___closed_48;
	// System.DateTime BestHTTP.WebSocket.WebSocketResponse::lastPing
	DateTime_t693205669  ___lastPing_49;

public:
	inline static int32_t get_offset_of_U3CWebSocketU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___U3CWebSocketU3Ek__BackingField_31)); }
	inline WebSocket_t71448861 * get_U3CWebSocketU3Ek__BackingField_31() const { return ___U3CWebSocketU3Ek__BackingField_31; }
	inline WebSocket_t71448861 ** get_address_of_U3CWebSocketU3Ek__BackingField_31() { return &___U3CWebSocketU3Ek__BackingField_31; }
	inline void set_U3CWebSocketU3Ek__BackingField_31(WebSocket_t71448861 * value)
	{
		___U3CWebSocketU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWebSocketU3Ek__BackingField_31, value);
	}

	inline static int32_t get_offset_of_OnText_32() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnText_32)); }
	inline Action_2_t1029759410 * get_OnText_32() const { return ___OnText_32; }
	inline Action_2_t1029759410 ** get_address_of_OnText_32() { return &___OnText_32; }
	inline void set_OnText_32(Action_2_t1029759410 * value)
	{
		___OnText_32 = value;
		Il2CppCodeGenWriteBarrier(&___OnText_32, value);
	}

	inline static int32_t get_offset_of_OnBinary_33() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnBinary_33)); }
	inline Action_2_t2397873190 * get_OnBinary_33() const { return ___OnBinary_33; }
	inline Action_2_t2397873190 ** get_address_of_OnBinary_33() { return &___OnBinary_33; }
	inline void set_OnBinary_33(Action_2_t2397873190 * value)
	{
		___OnBinary_33 = value;
		Il2CppCodeGenWriteBarrier(&___OnBinary_33, value);
	}

	inline static int32_t get_offset_of_OnIncompleteFrame_34() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnIncompleteFrame_34)); }
	inline Action_2_t3844780342 * get_OnIncompleteFrame_34() const { return ___OnIncompleteFrame_34; }
	inline Action_2_t3844780342 ** get_address_of_OnIncompleteFrame_34() { return &___OnIncompleteFrame_34; }
	inline void set_OnIncompleteFrame_34(Action_2_t3844780342 * value)
	{
		___OnIncompleteFrame_34 = value;
		Il2CppCodeGenWriteBarrier(&___OnIncompleteFrame_34, value);
	}

	inline static int32_t get_offset_of_OnClosed_35() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___OnClosed_35)); }
	inline Action_3_t2791643950 * get_OnClosed_35() const { return ___OnClosed_35; }
	inline Action_3_t2791643950 ** get_address_of_OnClosed_35() { return &___OnClosed_35; }
	inline void set_OnClosed_35(Action_3_t2791643950 * value)
	{
		___OnClosed_35 = value;
		Il2CppCodeGenWriteBarrier(&___OnClosed_35, value);
	}

	inline static int32_t get_offset_of_U3CPingFrequnecyU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___U3CPingFrequnecyU3Ek__BackingField_36)); }
	inline TimeSpan_t3430258949  get_U3CPingFrequnecyU3Ek__BackingField_36() const { return ___U3CPingFrequnecyU3Ek__BackingField_36; }
	inline TimeSpan_t3430258949 * get_address_of_U3CPingFrequnecyU3Ek__BackingField_36() { return &___U3CPingFrequnecyU3Ek__BackingField_36; }
	inline void set_U3CPingFrequnecyU3Ek__BackingField_36(TimeSpan_t3430258949  value)
	{
		___U3CPingFrequnecyU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CMaxFragmentSizeU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___U3CMaxFragmentSizeU3Ek__BackingField_37)); }
	inline uint16_t get_U3CMaxFragmentSizeU3Ek__BackingField_37() const { return ___U3CMaxFragmentSizeU3Ek__BackingField_37; }
	inline uint16_t* get_address_of_U3CMaxFragmentSizeU3Ek__BackingField_37() { return &___U3CMaxFragmentSizeU3Ek__BackingField_37; }
	inline void set_U3CMaxFragmentSizeU3Ek__BackingField_37(uint16_t value)
	{
		___U3CMaxFragmentSizeU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of__bufferedAmount_38() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ____bufferedAmount_38)); }
	inline int32_t get__bufferedAmount_38() const { return ____bufferedAmount_38; }
	inline int32_t* get_address_of__bufferedAmount_38() { return &____bufferedAmount_38; }
	inline void set__bufferedAmount_38(int32_t value)
	{
		____bufferedAmount_38 = value;
	}

	inline static int32_t get_offset_of_IncompleteFrames_39() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___IncompleteFrames_39)); }
	inline List_1_t4213362297 * get_IncompleteFrames_39() const { return ___IncompleteFrames_39; }
	inline List_1_t4213362297 ** get_address_of_IncompleteFrames_39() { return &___IncompleteFrames_39; }
	inline void set_IncompleteFrames_39(List_1_t4213362297 * value)
	{
		___IncompleteFrames_39 = value;
		Il2CppCodeGenWriteBarrier(&___IncompleteFrames_39, value);
	}

	inline static int32_t get_offset_of_CompletedFrames_40() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___CompletedFrames_40)); }
	inline List_1_t4213362297 * get_CompletedFrames_40() const { return ___CompletedFrames_40; }
	inline List_1_t4213362297 ** get_address_of_CompletedFrames_40() { return &___CompletedFrames_40; }
	inline void set_CompletedFrames_40(List_1_t4213362297 * value)
	{
		___CompletedFrames_40 = value;
		Il2CppCodeGenWriteBarrier(&___CompletedFrames_40, value);
	}

	inline static int32_t get_offset_of_CloseFrame_41() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___CloseFrame_41)); }
	inline WebSocketFrameReader_t549273869 * get_CloseFrame_41() const { return ___CloseFrame_41; }
	inline WebSocketFrameReader_t549273869 ** get_address_of_CloseFrame_41() { return &___CloseFrame_41; }
	inline void set_CloseFrame_41(WebSocketFrameReader_t549273869 * value)
	{
		___CloseFrame_41 = value;
		Il2CppCodeGenWriteBarrier(&___CloseFrame_41, value);
	}

	inline static int32_t get_offset_of_FrameLock_42() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___FrameLock_42)); }
	inline Il2CppObject * get_FrameLock_42() const { return ___FrameLock_42; }
	inline Il2CppObject ** get_address_of_FrameLock_42() { return &___FrameLock_42; }
	inline void set_FrameLock_42(Il2CppObject * value)
	{
		___FrameLock_42 = value;
		Il2CppCodeGenWriteBarrier(&___FrameLock_42, value);
	}

	inline static int32_t get_offset_of_SendLock_43() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___SendLock_43)); }
	inline Il2CppObject * get_SendLock_43() const { return ___SendLock_43; }
	inline Il2CppObject ** get_address_of_SendLock_43() { return &___SendLock_43; }
	inline void set_SendLock_43(Il2CppObject * value)
	{
		___SendLock_43 = value;
		Il2CppCodeGenWriteBarrier(&___SendLock_43, value);
	}

	inline static int32_t get_offset_of_unsentFrames_44() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___unsentFrames_44)); }
	inline List_1_t3532404526 * get_unsentFrames_44() const { return ___unsentFrames_44; }
	inline List_1_t3532404526 ** get_address_of_unsentFrames_44() { return &___unsentFrames_44; }
	inline void set_unsentFrames_44(List_1_t3532404526 * value)
	{
		___unsentFrames_44 = value;
		Il2CppCodeGenWriteBarrier(&___unsentFrames_44, value);
	}

	inline static int32_t get_offset_of_newFrameSignal_45() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___newFrameSignal_45)); }
	inline AutoResetEvent_t15112628 * get_newFrameSignal_45() const { return ___newFrameSignal_45; }
	inline AutoResetEvent_t15112628 ** get_address_of_newFrameSignal_45() { return &___newFrameSignal_45; }
	inline void set_newFrameSignal_45(AutoResetEvent_t15112628 * value)
	{
		___newFrameSignal_45 = value;
		Il2CppCodeGenWriteBarrier(&___newFrameSignal_45, value);
	}

	inline static int32_t get_offset_of_sendThreadCreated_46() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___sendThreadCreated_46)); }
	inline bool get_sendThreadCreated_46() const { return ___sendThreadCreated_46; }
	inline bool* get_address_of_sendThreadCreated_46() { return &___sendThreadCreated_46; }
	inline void set_sendThreadCreated_46(bool value)
	{
		___sendThreadCreated_46 = value;
	}

	inline static int32_t get_offset_of_closeSent_47() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___closeSent_47)); }
	inline bool get_closeSent_47() const { return ___closeSent_47; }
	inline bool* get_address_of_closeSent_47() { return &___closeSent_47; }
	inline void set_closeSent_47(bool value)
	{
		___closeSent_47 = value;
	}

	inline static int32_t get_offset_of_closed_48() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___closed_48)); }
	inline bool get_closed_48() const { return ___closed_48; }
	inline bool* get_address_of_closed_48() { return &___closed_48; }
	inline void set_closed_48(bool value)
	{
		___closed_48 = value;
	}

	inline static int32_t get_offset_of_lastPing_49() { return static_cast<int32_t>(offsetof(WebSocketResponse_t3376763264, ___lastPing_49)); }
	inline DateTime_t693205669  get_lastPing_49() const { return ___lastPing_49; }
	inline DateTime_t693205669 * get_address_of_lastPing_49() { return &___lastPing_49; }
	inline void set_lastPing_49(DateTime_t693205669  value)
	{
		___lastPing_49 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
