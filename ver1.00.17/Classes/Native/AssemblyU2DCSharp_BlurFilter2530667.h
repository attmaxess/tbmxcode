﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityStandardAssets.ImageEffects.Blur
struct Blur_t3313275655;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlurFilter
struct  BlurFilter_t2530667  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BlurFilter::Active
	bool ___Active_2;
	// System.Int32 BlurFilter::Repeat
	int32_t ___Repeat_3;
	// System.Single BlurFilter::Spread
	float ___Spread_4;
	// UnityStandardAssets.ImageEffects.Blur BlurFilter::m_Blur
	Blur_t3313275655 * ___m_Blur_5;

public:
	inline static int32_t get_offset_of_Active_2() { return static_cast<int32_t>(offsetof(BlurFilter_t2530667, ___Active_2)); }
	inline bool get_Active_2() const { return ___Active_2; }
	inline bool* get_address_of_Active_2() { return &___Active_2; }
	inline void set_Active_2(bool value)
	{
		___Active_2 = value;
	}

	inline static int32_t get_offset_of_Repeat_3() { return static_cast<int32_t>(offsetof(BlurFilter_t2530667, ___Repeat_3)); }
	inline int32_t get_Repeat_3() const { return ___Repeat_3; }
	inline int32_t* get_address_of_Repeat_3() { return &___Repeat_3; }
	inline void set_Repeat_3(int32_t value)
	{
		___Repeat_3 = value;
	}

	inline static int32_t get_offset_of_Spread_4() { return static_cast<int32_t>(offsetof(BlurFilter_t2530667, ___Spread_4)); }
	inline float get_Spread_4() const { return ___Spread_4; }
	inline float* get_address_of_Spread_4() { return &___Spread_4; }
	inline void set_Spread_4(float value)
	{
		___Spread_4 = value;
	}

	inline static int32_t get_offset_of_m_Blur_5() { return static_cast<int32_t>(offsetof(BlurFilter_t2530667, ___m_Blur_5)); }
	inline Blur_t3313275655 * get_m_Blur_5() const { return ___m_Blur_5; }
	inline Blur_t3313275655 ** get_address_of_m_Blur_5() { return &___m_Blur_5; }
	inline void set_m_Blur_5(Blur_t3313275655 * value)
	{
		___m_Blur_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Blur_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
