﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoatSimulator
struct  BoatSimulator_t958302968  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody BoatSimulator::rigid
	Rigidbody_t4233889191 * ___rigid_2;
	// System.Boolean BoatSimulator::keyPressedW
	bool ___keyPressedW_3;
	// System.Boolean BoatSimulator::keyPressedA
	bool ___keyPressedA_4;
	// System.Boolean BoatSimulator::keyPressedS
	bool ___keyPressedS_5;
	// System.Boolean BoatSimulator::keyPressedD
	bool ___keyPressedD_6;

public:
	inline static int32_t get_offset_of_rigid_2() { return static_cast<int32_t>(offsetof(BoatSimulator_t958302968, ___rigid_2)); }
	inline Rigidbody_t4233889191 * get_rigid_2() const { return ___rigid_2; }
	inline Rigidbody_t4233889191 ** get_address_of_rigid_2() { return &___rigid_2; }
	inline void set_rigid_2(Rigidbody_t4233889191 * value)
	{
		___rigid_2 = value;
		Il2CppCodeGenWriteBarrier(&___rigid_2, value);
	}

	inline static int32_t get_offset_of_keyPressedW_3() { return static_cast<int32_t>(offsetof(BoatSimulator_t958302968, ___keyPressedW_3)); }
	inline bool get_keyPressedW_3() const { return ___keyPressedW_3; }
	inline bool* get_address_of_keyPressedW_3() { return &___keyPressedW_3; }
	inline void set_keyPressedW_3(bool value)
	{
		___keyPressedW_3 = value;
	}

	inline static int32_t get_offset_of_keyPressedA_4() { return static_cast<int32_t>(offsetof(BoatSimulator_t958302968, ___keyPressedA_4)); }
	inline bool get_keyPressedA_4() const { return ___keyPressedA_4; }
	inline bool* get_address_of_keyPressedA_4() { return &___keyPressedA_4; }
	inline void set_keyPressedA_4(bool value)
	{
		___keyPressedA_4 = value;
	}

	inline static int32_t get_offset_of_keyPressedS_5() { return static_cast<int32_t>(offsetof(BoatSimulator_t958302968, ___keyPressedS_5)); }
	inline bool get_keyPressedS_5() const { return ___keyPressedS_5; }
	inline bool* get_address_of_keyPressedS_5() { return &___keyPressedS_5; }
	inline void set_keyPressedS_5(bool value)
	{
		___keyPressedS_5 = value;
	}

	inline static int32_t get_offset_of_keyPressedD_6() { return static_cast<int32_t>(offsetof(BoatSimulator_t958302968, ___keyPressedD_6)); }
	inline bool get_keyPressedD_6() const { return ___keyPressedD_6; }
	inline bool* get_address_of_keyPressedD_6() { return &___keyPressedD_6; }
	inline void set_keyPressedD_6(bool value)
	{
		___keyPressedD_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
