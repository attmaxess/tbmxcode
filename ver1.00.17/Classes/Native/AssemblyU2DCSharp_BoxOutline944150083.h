﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ModifiedShadow522613381.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoxOutline
struct  BoxOutline_t944150083  : public ModifiedShadow_t522613381
{
public:
	// System.Int32 BoxOutline::m_halfSampleCountX
	int32_t ___m_halfSampleCountX_8;
	// System.Int32 BoxOutline::m_halfSampleCountY
	int32_t ___m_halfSampleCountY_9;

public:
	inline static int32_t get_offset_of_m_halfSampleCountX_8() { return static_cast<int32_t>(offsetof(BoxOutline_t944150083, ___m_halfSampleCountX_8)); }
	inline int32_t get_m_halfSampleCountX_8() const { return ___m_halfSampleCountX_8; }
	inline int32_t* get_address_of_m_halfSampleCountX_8() { return &___m_halfSampleCountX_8; }
	inline void set_m_halfSampleCountX_8(int32_t value)
	{
		___m_halfSampleCountX_8 = value;
	}

	inline static int32_t get_offset_of_m_halfSampleCountY_9() { return static_cast<int32_t>(offsetof(BoxOutline_t944150083, ___m_halfSampleCountY_9)); }
	inline int32_t get_m_halfSampleCountY_9() const { return ___m_halfSampleCountY_9; }
	inline int32_t* get_address_of_m_halfSampleCountY_9() { return &___m_halfSampleCountY_9; }
	inline void set_m_halfSampleCountY_9(int32_t value)
	{
		___m_halfSampleCountY_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
