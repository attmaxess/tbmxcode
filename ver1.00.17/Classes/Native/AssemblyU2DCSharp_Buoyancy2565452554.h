﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Vector3[]>
struct List_1_t541432897;
// WaterRipples
struct WaterRipples_t656902750;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Buoyancy
struct  Buoyancy_t2565452554  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Buoyancy::Density
	float ___Density_2;
	// System.Int32 Buoyancy::SlicesPerAxis
	int32_t ___SlicesPerAxis_3;
	// System.Boolean Buoyancy::IsConcave
	bool ___IsConcave_4;
	// System.Int32 Buoyancy::VoxelsLimit
	int32_t ___VoxelsLimit_5;
	// System.Single Buoyancy::WaveVelocity
	float ___WaveVelocity_6;
	// System.Single Buoyancy::voxelHalfHeight
	float ___voxelHalfHeight_9;
	// System.Single Buoyancy::localArchimedesForce
	float ___localArchimedesForce_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Buoyancy::voxels
	List_1_t1612828712 * ___voxels_11;
	// System.Boolean Buoyancy::isMeshCollider
	bool ___isMeshCollider_12;
	// System.Collections.Generic.List`1<UnityEngine.Vector3[]> Buoyancy::forces
	List_1_t541432897 * ___forces_13;
	// WaterRipples Buoyancy::waterRipples
	WaterRipples_t656902750 * ___waterRipples_14;

public:
	inline static int32_t get_offset_of_Density_2() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___Density_2)); }
	inline float get_Density_2() const { return ___Density_2; }
	inline float* get_address_of_Density_2() { return &___Density_2; }
	inline void set_Density_2(float value)
	{
		___Density_2 = value;
	}

	inline static int32_t get_offset_of_SlicesPerAxis_3() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___SlicesPerAxis_3)); }
	inline int32_t get_SlicesPerAxis_3() const { return ___SlicesPerAxis_3; }
	inline int32_t* get_address_of_SlicesPerAxis_3() { return &___SlicesPerAxis_3; }
	inline void set_SlicesPerAxis_3(int32_t value)
	{
		___SlicesPerAxis_3 = value;
	}

	inline static int32_t get_offset_of_IsConcave_4() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___IsConcave_4)); }
	inline bool get_IsConcave_4() const { return ___IsConcave_4; }
	inline bool* get_address_of_IsConcave_4() { return &___IsConcave_4; }
	inline void set_IsConcave_4(bool value)
	{
		___IsConcave_4 = value;
	}

	inline static int32_t get_offset_of_VoxelsLimit_5() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___VoxelsLimit_5)); }
	inline int32_t get_VoxelsLimit_5() const { return ___VoxelsLimit_5; }
	inline int32_t* get_address_of_VoxelsLimit_5() { return &___VoxelsLimit_5; }
	inline void set_VoxelsLimit_5(int32_t value)
	{
		___VoxelsLimit_5 = value;
	}

	inline static int32_t get_offset_of_WaveVelocity_6() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___WaveVelocity_6)); }
	inline float get_WaveVelocity_6() const { return ___WaveVelocity_6; }
	inline float* get_address_of_WaveVelocity_6() { return &___WaveVelocity_6; }
	inline void set_WaveVelocity_6(float value)
	{
		___WaveVelocity_6 = value;
	}

	inline static int32_t get_offset_of_voxelHalfHeight_9() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___voxelHalfHeight_9)); }
	inline float get_voxelHalfHeight_9() const { return ___voxelHalfHeight_9; }
	inline float* get_address_of_voxelHalfHeight_9() { return &___voxelHalfHeight_9; }
	inline void set_voxelHalfHeight_9(float value)
	{
		___voxelHalfHeight_9 = value;
	}

	inline static int32_t get_offset_of_localArchimedesForce_10() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___localArchimedesForce_10)); }
	inline float get_localArchimedesForce_10() const { return ___localArchimedesForce_10; }
	inline float* get_address_of_localArchimedesForce_10() { return &___localArchimedesForce_10; }
	inline void set_localArchimedesForce_10(float value)
	{
		___localArchimedesForce_10 = value;
	}

	inline static int32_t get_offset_of_voxels_11() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___voxels_11)); }
	inline List_1_t1612828712 * get_voxels_11() const { return ___voxels_11; }
	inline List_1_t1612828712 ** get_address_of_voxels_11() { return &___voxels_11; }
	inline void set_voxels_11(List_1_t1612828712 * value)
	{
		___voxels_11 = value;
		Il2CppCodeGenWriteBarrier(&___voxels_11, value);
	}

	inline static int32_t get_offset_of_isMeshCollider_12() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___isMeshCollider_12)); }
	inline bool get_isMeshCollider_12() const { return ___isMeshCollider_12; }
	inline bool* get_address_of_isMeshCollider_12() { return &___isMeshCollider_12; }
	inline void set_isMeshCollider_12(bool value)
	{
		___isMeshCollider_12 = value;
	}

	inline static int32_t get_offset_of_forces_13() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___forces_13)); }
	inline List_1_t541432897 * get_forces_13() const { return ___forces_13; }
	inline List_1_t541432897 ** get_address_of_forces_13() { return &___forces_13; }
	inline void set_forces_13(List_1_t541432897 * value)
	{
		___forces_13 = value;
		Il2CppCodeGenWriteBarrier(&___forces_13, value);
	}

	inline static int32_t get_offset_of_waterRipples_14() { return static_cast<int32_t>(offsetof(Buoyancy_t2565452554, ___waterRipples_14)); }
	inline WaterRipples_t656902750 * get_waterRipples_14() const { return ___waterRipples_14; }
	inline WaterRipples_t656902750 ** get_address_of_waterRipples_14() { return &___waterRipples_14; }
	inline void set_waterRipples_14(WaterRipples_t656902750 * value)
	{
		___waterRipples_14 = value;
		Il2CppCodeGenWriteBarrier(&___waterRipples_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
