﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_eSCENETYPE4122992453.h"

// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonOnClickChangeScene
struct  ButtonOnClickChangeScene_t4293177719  : public MonoBehaviour_t1158329972
{
public:
	// eSCENETYPE ButtonOnClickChangeScene::sceneTypeNext
	int32_t ___sceneTypeNext_2;
	// UnityEngine.UI.Button ButtonOnClickChangeScene::button
	Button_t2872111280 * ___button_3;

public:
	inline static int32_t get_offset_of_sceneTypeNext_2() { return static_cast<int32_t>(offsetof(ButtonOnClickChangeScene_t4293177719, ___sceneTypeNext_2)); }
	inline int32_t get_sceneTypeNext_2() const { return ___sceneTypeNext_2; }
	inline int32_t* get_address_of_sceneTypeNext_2() { return &___sceneTypeNext_2; }
	inline void set_sceneTypeNext_2(int32_t value)
	{
		___sceneTypeNext_2 = value;
	}

	inline static int32_t get_offset_of_button_3() { return static_cast<int32_t>(offsetof(ButtonOnClickChangeScene_t4293177719, ___button_3)); }
	inline Button_t2872111280 * get_button_3() const { return ___button_3; }
	inline Button_t2872111280 ** get_address_of_button_3() { return &___button_3; }
	inline void set_button_3(Button_t2872111280 * value)
	{
		___button_3 = value;
		Il2CppCodeGenWriteBarrier(&___button_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
