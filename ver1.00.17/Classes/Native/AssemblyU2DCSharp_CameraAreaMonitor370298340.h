﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// AreaObjs
struct AreaObjs_t582166865;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAreaMonitor
struct  CameraAreaMonitor_t370298340  : public MonoBehaviour_t1158329972
{
public:
	// AreaObjs CameraAreaMonitor::AreaObjs
	AreaObjs_t582166865 * ___AreaObjs_2;

public:
	inline static int32_t get_offset_of_AreaObjs_2() { return static_cast<int32_t>(offsetof(CameraAreaMonitor_t370298340, ___AreaObjs_2)); }
	inline AreaObjs_t582166865 * get_AreaObjs_2() const { return ___AreaObjs_2; }
	inline AreaObjs_t582166865 ** get_address_of_AreaObjs_2() { return &___AreaObjs_2; }
	inline void set_AreaObjs_2(AreaObjs_t582166865 * value)
	{
		___AreaObjs_2 = value;
		Il2CppCodeGenWriteBarrier(&___AreaObjs_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
