﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1888025061.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// Fujioka.FSEffect
struct FSEffect_t3187928436;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraEffect
struct  CameraEffect_t3191348726  : public SingletonMonoBehaviour_1_t1888025061
{
public:
	// Fujioka.FSEffect CameraEffect::m_FSEffect
	FSEffect_t3187928436 * ___m_FSEffect_3;
	// UnityEngine.Color CameraEffect::m_SkyBoxColorUp
	Color_t2020392075  ___m_SkyBoxColorUp_4;
	// UnityEngine.Color CameraEffect::m_SkyBoxColorDw
	Color_t2020392075  ___m_SkyBoxColorDw_5;

public:
	inline static int32_t get_offset_of_m_FSEffect_3() { return static_cast<int32_t>(offsetof(CameraEffect_t3191348726, ___m_FSEffect_3)); }
	inline FSEffect_t3187928436 * get_m_FSEffect_3() const { return ___m_FSEffect_3; }
	inline FSEffect_t3187928436 ** get_address_of_m_FSEffect_3() { return &___m_FSEffect_3; }
	inline void set_m_FSEffect_3(FSEffect_t3187928436 * value)
	{
		___m_FSEffect_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_FSEffect_3, value);
	}

	inline static int32_t get_offset_of_m_SkyBoxColorUp_4() { return static_cast<int32_t>(offsetof(CameraEffect_t3191348726, ___m_SkyBoxColorUp_4)); }
	inline Color_t2020392075  get_m_SkyBoxColorUp_4() const { return ___m_SkyBoxColorUp_4; }
	inline Color_t2020392075 * get_address_of_m_SkyBoxColorUp_4() { return &___m_SkyBoxColorUp_4; }
	inline void set_m_SkyBoxColorUp_4(Color_t2020392075  value)
	{
		___m_SkyBoxColorUp_4 = value;
	}

	inline static int32_t get_offset_of_m_SkyBoxColorDw_5() { return static_cast<int32_t>(offsetof(CameraEffect_t3191348726, ___m_SkyBoxColorDw_5)); }
	inline Color_t2020392075  get_m_SkyBoxColorDw_5() const { return ___m_SkyBoxColorDw_5; }
	inline Color_t2020392075 * get_address_of_m_SkyBoxColorDw_5() { return &___m_SkyBoxColorDw_5; }
	inline void set_m_SkyBoxColorDw_5(Color_t2020392075  value)
	{
		___m_SkyBoxColorDw_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
