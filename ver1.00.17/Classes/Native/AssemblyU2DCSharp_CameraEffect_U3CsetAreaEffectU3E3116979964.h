﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// CameraEffect
struct CameraEffect_t3191348726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraEffect/<setAreaEffect>c__AnonStorey0
struct  U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964  : public Il2CppObject
{
public:
	// UnityEngine.Color CameraEffect/<setAreaEffect>c__AnonStorey0::defColorUp
	Color_t2020392075  ___defColorUp_0;
	// UnityEngine.Color CameraEffect/<setAreaEffect>c__AnonStorey0::defColorDw
	Color_t2020392075  ___defColorDw_1;
	// CameraEffect CameraEffect/<setAreaEffect>c__AnonStorey0::$this
	CameraEffect_t3191348726 * ___U24this_2;

public:
	inline static int32_t get_offset_of_defColorUp_0() { return static_cast<int32_t>(offsetof(U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964, ___defColorUp_0)); }
	inline Color_t2020392075  get_defColorUp_0() const { return ___defColorUp_0; }
	inline Color_t2020392075 * get_address_of_defColorUp_0() { return &___defColorUp_0; }
	inline void set_defColorUp_0(Color_t2020392075  value)
	{
		___defColorUp_0 = value;
	}

	inline static int32_t get_offset_of_defColorDw_1() { return static_cast<int32_t>(offsetof(U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964, ___defColorDw_1)); }
	inline Color_t2020392075  get_defColorDw_1() const { return ___defColorDw_1; }
	inline Color_t2020392075 * get_address_of_defColorDw_1() { return &___defColorDw_1; }
	inline void set_defColorDw_1(Color_t2020392075  value)
	{
		___defColorDw_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964, ___U24this_2)); }
	inline CameraEffect_t3191348726 * get_U24this_2() const { return ___U24this_2; }
	inline CameraEffect_t3191348726 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CameraEffect_t3191348726 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
