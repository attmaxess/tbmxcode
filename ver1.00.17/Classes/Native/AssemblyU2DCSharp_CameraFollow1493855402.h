﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFollow
struct  CameraFollow_t1493855402  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CameraFollow::Enable
	bool ___Enable_6;
	// System.Boolean CameraFollow::m_bAuto
	bool ___m_bAuto_7;
	// System.Boolean CameraFollow::m_bFollowReady
	bool ___m_bFollowReady_8;
	// System.Boolean CameraFollow::m_bFollowActionReady
	bool ___m_bFollowActionReady_9;
	// System.Boolean CameraFollow::m_bReset
	bool ___m_bReset_10;
	// UnityEngine.Vector3 CameraFollow::m_vCurrentPos
	Vector3_t2243707580  ___m_vCurrentPos_11;
	// UnityEngine.Vector3 CameraFollow::m_vPrevPos
	Vector3_t2243707580  ___m_vPrevPos_12;
	// UnityEngine.Vector3 CameraFollow::m_vMoveDirection
	Vector3_t2243707580  ___m_vMoveDirection_13;
	// System.Single CameraFollow::m_fTimeCountToPrevPos
	float ___m_fTimeCountToPrevPos_14;
	// System.Single CameraFollow::m_fTimeCountToDirection
	float ___m_fTimeCountToDirection_15;
	// System.Single CameraFollow::m_fTimeCountToDefault
	float ___m_fTimeCountToDefault_16;
	// UnityEngine.Rigidbody CameraFollow::m_TargetRigidBody
	Rigidbody_t4233889191 * ___m_TargetRigidBody_17;
	// System.Single CameraFollow::m_fPlanetRadius
	float ___m_fPlanetRadius_18;
	// UnityEngine.Vector3 CameraFollow::m_vPlanetPos
	Vector3_t2243707580  ___m_vPlanetPos_19;
	// UnityEngine.Vector3 CameraFollow::m_vDirectionFromPlanetCenterToMobilmo
	Vector3_t2243707580  ___m_vDirectionFromPlanetCenterToMobilmo_20;
	// UnityEngine.Vector3 CameraFollow::m_vHeightPos
	Vector3_t2243707580  ___m_vHeightPos_21;
	// System.Single CameraFollow::m_fFollowDistanceOffset
	float ___m_fFollowDistanceOffset_22;
	// System.Single CameraFollow::m_fHeightOffset
	float ___m_fHeightOffset_23;
	// System.Single CameraFollow::leapTime
	float ___leapTime_24;

public:
	inline static int32_t get_offset_of_Enable_6() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___Enable_6)); }
	inline bool get_Enable_6() const { return ___Enable_6; }
	inline bool* get_address_of_Enable_6() { return &___Enable_6; }
	inline void set_Enable_6(bool value)
	{
		___Enable_6 = value;
	}

	inline static int32_t get_offset_of_m_bAuto_7() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_bAuto_7)); }
	inline bool get_m_bAuto_7() const { return ___m_bAuto_7; }
	inline bool* get_address_of_m_bAuto_7() { return &___m_bAuto_7; }
	inline void set_m_bAuto_7(bool value)
	{
		___m_bAuto_7 = value;
	}

	inline static int32_t get_offset_of_m_bFollowReady_8() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_bFollowReady_8)); }
	inline bool get_m_bFollowReady_8() const { return ___m_bFollowReady_8; }
	inline bool* get_address_of_m_bFollowReady_8() { return &___m_bFollowReady_8; }
	inline void set_m_bFollowReady_8(bool value)
	{
		___m_bFollowReady_8 = value;
	}

	inline static int32_t get_offset_of_m_bFollowActionReady_9() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_bFollowActionReady_9)); }
	inline bool get_m_bFollowActionReady_9() const { return ___m_bFollowActionReady_9; }
	inline bool* get_address_of_m_bFollowActionReady_9() { return &___m_bFollowActionReady_9; }
	inline void set_m_bFollowActionReady_9(bool value)
	{
		___m_bFollowActionReady_9 = value;
	}

	inline static int32_t get_offset_of_m_bReset_10() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_bReset_10)); }
	inline bool get_m_bReset_10() const { return ___m_bReset_10; }
	inline bool* get_address_of_m_bReset_10() { return &___m_bReset_10; }
	inline void set_m_bReset_10(bool value)
	{
		___m_bReset_10 = value;
	}

	inline static int32_t get_offset_of_m_vCurrentPos_11() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_vCurrentPos_11)); }
	inline Vector3_t2243707580  get_m_vCurrentPos_11() const { return ___m_vCurrentPos_11; }
	inline Vector3_t2243707580 * get_address_of_m_vCurrentPos_11() { return &___m_vCurrentPos_11; }
	inline void set_m_vCurrentPos_11(Vector3_t2243707580  value)
	{
		___m_vCurrentPos_11 = value;
	}

	inline static int32_t get_offset_of_m_vPrevPos_12() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_vPrevPos_12)); }
	inline Vector3_t2243707580  get_m_vPrevPos_12() const { return ___m_vPrevPos_12; }
	inline Vector3_t2243707580 * get_address_of_m_vPrevPos_12() { return &___m_vPrevPos_12; }
	inline void set_m_vPrevPos_12(Vector3_t2243707580  value)
	{
		___m_vPrevPos_12 = value;
	}

	inline static int32_t get_offset_of_m_vMoveDirection_13() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_vMoveDirection_13)); }
	inline Vector3_t2243707580  get_m_vMoveDirection_13() const { return ___m_vMoveDirection_13; }
	inline Vector3_t2243707580 * get_address_of_m_vMoveDirection_13() { return &___m_vMoveDirection_13; }
	inline void set_m_vMoveDirection_13(Vector3_t2243707580  value)
	{
		___m_vMoveDirection_13 = value;
	}

	inline static int32_t get_offset_of_m_fTimeCountToPrevPos_14() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_fTimeCountToPrevPos_14)); }
	inline float get_m_fTimeCountToPrevPos_14() const { return ___m_fTimeCountToPrevPos_14; }
	inline float* get_address_of_m_fTimeCountToPrevPos_14() { return &___m_fTimeCountToPrevPos_14; }
	inline void set_m_fTimeCountToPrevPos_14(float value)
	{
		___m_fTimeCountToPrevPos_14 = value;
	}

	inline static int32_t get_offset_of_m_fTimeCountToDirection_15() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_fTimeCountToDirection_15)); }
	inline float get_m_fTimeCountToDirection_15() const { return ___m_fTimeCountToDirection_15; }
	inline float* get_address_of_m_fTimeCountToDirection_15() { return &___m_fTimeCountToDirection_15; }
	inline void set_m_fTimeCountToDirection_15(float value)
	{
		___m_fTimeCountToDirection_15 = value;
	}

	inline static int32_t get_offset_of_m_fTimeCountToDefault_16() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_fTimeCountToDefault_16)); }
	inline float get_m_fTimeCountToDefault_16() const { return ___m_fTimeCountToDefault_16; }
	inline float* get_address_of_m_fTimeCountToDefault_16() { return &___m_fTimeCountToDefault_16; }
	inline void set_m_fTimeCountToDefault_16(float value)
	{
		___m_fTimeCountToDefault_16 = value;
	}

	inline static int32_t get_offset_of_m_TargetRigidBody_17() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_TargetRigidBody_17)); }
	inline Rigidbody_t4233889191 * get_m_TargetRigidBody_17() const { return ___m_TargetRigidBody_17; }
	inline Rigidbody_t4233889191 ** get_address_of_m_TargetRigidBody_17() { return &___m_TargetRigidBody_17; }
	inline void set_m_TargetRigidBody_17(Rigidbody_t4233889191 * value)
	{
		___m_TargetRigidBody_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_TargetRigidBody_17, value);
	}

	inline static int32_t get_offset_of_m_fPlanetRadius_18() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_fPlanetRadius_18)); }
	inline float get_m_fPlanetRadius_18() const { return ___m_fPlanetRadius_18; }
	inline float* get_address_of_m_fPlanetRadius_18() { return &___m_fPlanetRadius_18; }
	inline void set_m_fPlanetRadius_18(float value)
	{
		___m_fPlanetRadius_18 = value;
	}

	inline static int32_t get_offset_of_m_vPlanetPos_19() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_vPlanetPos_19)); }
	inline Vector3_t2243707580  get_m_vPlanetPos_19() const { return ___m_vPlanetPos_19; }
	inline Vector3_t2243707580 * get_address_of_m_vPlanetPos_19() { return &___m_vPlanetPos_19; }
	inline void set_m_vPlanetPos_19(Vector3_t2243707580  value)
	{
		___m_vPlanetPos_19 = value;
	}

	inline static int32_t get_offset_of_m_vDirectionFromPlanetCenterToMobilmo_20() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_vDirectionFromPlanetCenterToMobilmo_20)); }
	inline Vector3_t2243707580  get_m_vDirectionFromPlanetCenterToMobilmo_20() const { return ___m_vDirectionFromPlanetCenterToMobilmo_20; }
	inline Vector3_t2243707580 * get_address_of_m_vDirectionFromPlanetCenterToMobilmo_20() { return &___m_vDirectionFromPlanetCenterToMobilmo_20; }
	inline void set_m_vDirectionFromPlanetCenterToMobilmo_20(Vector3_t2243707580  value)
	{
		___m_vDirectionFromPlanetCenterToMobilmo_20 = value;
	}

	inline static int32_t get_offset_of_m_vHeightPos_21() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_vHeightPos_21)); }
	inline Vector3_t2243707580  get_m_vHeightPos_21() const { return ___m_vHeightPos_21; }
	inline Vector3_t2243707580 * get_address_of_m_vHeightPos_21() { return &___m_vHeightPos_21; }
	inline void set_m_vHeightPos_21(Vector3_t2243707580  value)
	{
		___m_vHeightPos_21 = value;
	}

	inline static int32_t get_offset_of_m_fFollowDistanceOffset_22() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_fFollowDistanceOffset_22)); }
	inline float get_m_fFollowDistanceOffset_22() const { return ___m_fFollowDistanceOffset_22; }
	inline float* get_address_of_m_fFollowDistanceOffset_22() { return &___m_fFollowDistanceOffset_22; }
	inline void set_m_fFollowDistanceOffset_22(float value)
	{
		___m_fFollowDistanceOffset_22 = value;
	}

	inline static int32_t get_offset_of_m_fHeightOffset_23() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___m_fHeightOffset_23)); }
	inline float get_m_fHeightOffset_23() const { return ___m_fHeightOffset_23; }
	inline float* get_address_of_m_fHeightOffset_23() { return &___m_fHeightOffset_23; }
	inline void set_m_fHeightOffset_23(float value)
	{
		___m_fHeightOffset_23 = value;
	}

	inline static int32_t get_offset_of_leapTime_24() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402, ___leapTime_24)); }
	inline float get_leapTime_24() const { return ___leapTime_24; }
	inline float* get_address_of_leapTime_24() { return &___leapTime_24; }
	inline void set_leapTime_24(float value)
	{
		___leapTime_24 = value;
	}
};

struct CameraFollow_t1493855402_StaticFields
{
public:
	// System.Single CameraFollow::s_fLimitTime
	float ___s_fLimitTime_2;
	// System.Single CameraFollow::s_fLimitDefaultTime
	float ___s_fLimitDefaultTime_3;
	// System.Single CameraFollow::s_fDamping
	float ___s_fDamping_4;
	// System.Single CameraFollow::s_fMoveSpeedLimit
	float ___s_fMoveSpeedLimit_5;

public:
	inline static int32_t get_offset_of_s_fLimitTime_2() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402_StaticFields, ___s_fLimitTime_2)); }
	inline float get_s_fLimitTime_2() const { return ___s_fLimitTime_2; }
	inline float* get_address_of_s_fLimitTime_2() { return &___s_fLimitTime_2; }
	inline void set_s_fLimitTime_2(float value)
	{
		___s_fLimitTime_2 = value;
	}

	inline static int32_t get_offset_of_s_fLimitDefaultTime_3() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402_StaticFields, ___s_fLimitDefaultTime_3)); }
	inline float get_s_fLimitDefaultTime_3() const { return ___s_fLimitDefaultTime_3; }
	inline float* get_address_of_s_fLimitDefaultTime_3() { return &___s_fLimitDefaultTime_3; }
	inline void set_s_fLimitDefaultTime_3(float value)
	{
		___s_fLimitDefaultTime_3 = value;
	}

	inline static int32_t get_offset_of_s_fDamping_4() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402_StaticFields, ___s_fDamping_4)); }
	inline float get_s_fDamping_4() const { return ___s_fDamping_4; }
	inline float* get_address_of_s_fDamping_4() { return &___s_fDamping_4; }
	inline void set_s_fDamping_4(float value)
	{
		___s_fDamping_4 = value;
	}

	inline static int32_t get_offset_of_s_fMoveSpeedLimit_5() { return static_cast<int32_t>(offsetof(CameraFollow_t1493855402_StaticFields, ___s_fMoveSpeedLimit_5)); }
	inline float get_s_fMoveSpeedLimit_5() const { return ___s_fMoveSpeedLimit_5; }
	inline float* get_address_of_s_fMoveSpeedLimit_5() { return &___s_fMoveSpeedLimit_5; }
	inline void set_s_fMoveSpeedLimit_5(float value)
	{
		___s_fMoveSpeedLimit_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
