﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1076535681.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Transform
struct Transform_t3275118058;
// BlurFilter
struct BlurFilter_t2530667;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// UnityStandardAssets.ImageEffects.Blur
struct Blur_t3313275655;
// CameraFollow
struct CameraFollow_t1493855402;
// CameraZoom
struct CameraZoom_t1174393664;
// CameraRotater
struct CameraRotater_t122375930;
// CameraEffect
struct CameraEffect_t3191348726;
// SubCameraHandler
struct SubCameraHandler_t2753085375;
// CameraSettings
struct CameraSettings_t3536359094;
// CameraAreaMonitor
struct CameraAreaMonitor_t370298340;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3858616074;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t3734738918;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraManager
struct  CameraManager_t2379859346  : public SingletonMonoBehaviour_1_t1076535681
{
public:
	// UnityEngine.Camera CameraManager::MainCamera
	Camera_t189460977 * ___MainCamera_3;
	// UnityEngine.Camera CameraManager::SubCamera
	Camera_t189460977 * ___SubCamera_4;
	// UnityEngine.Camera CameraManager::ModelingCamera
	Camera_t189460977 * ___ModelingCamera_5;
	// UnityEngine.Camera CameraManager::FrontSubCamera
	Camera_t189460977 * ___FrontSubCamera_6;
	// UnityEngine.Camera CameraManager::MobilityCamera
	Camera_t189460977 * ___MobilityCamera_7;
	// UnityEngine.Camera CameraManager::PoseCaptureCamera
	Camera_t189460977 * ___PoseCaptureCamera_8;
	// UnityEngine.Camera CameraManager::StartCamera
	Camera_t189460977 * ___StartCamera_9;
	// UnityEngine.Camera CameraManager::MiniMapCamera
	Camera_t189460977 * ___MiniMapCamera_10;
	// UnityEngine.Camera CameraManager::WorldMapCamera
	Camera_t189460977 * ___WorldMapCamera_11;
	// UnityEngine.Camera CameraManager::MiniWorldCamera
	Camera_t189460977 * ___MiniWorldCamera_12;
	// UnityEngine.Transform CameraManager::ModelingCameraCenter
	Transform_t3275118058 * ___ModelingCameraCenter_13;
	// UnityEngine.Transform CameraManager::MobilmoCenter
	Transform_t3275118058 * ___MobilmoCenter_14;
	// UnityEngine.Transform CameraManager::CameraParent
	Transform_t3275118058 * ___CameraParent_15;
	// UnityEngine.Vector3 CameraManager::TitleParentPos
	Vector3_t2243707580  ___TitleParentPos_16;
	// UnityEngine.Vector3 CameraManager::EditParentPos
	Vector3_t2243707580  ___EditParentPos_17;
	// UnityEngine.Vector3 CameraManager::MypageParentPos
	Vector3_t2243707580  ___MypageParentPos_18;
	// BlurFilter CameraManager::BlurFileter
	BlurFilter_t2530667 * ___BlurFileter_19;
	// System.Collections.Generic.List`1<UnityEngine.Transform> CameraManager::ModuleTrans
	List_1_t2644239190 * ___ModuleTrans_20;
	// System.Boolean CameraManager::m_bSubCamera
	bool ___m_bSubCamera_21;
	// System.Boolean CameraManager::m_bMobilityCamera
	bool ___m_bMobilityCamera_22;
	// System.Boolean CameraManager::isCanLogEvevt
	bool ___isCanLogEvevt_23;
	// System.Single CameraManager::m_fUpDownValue
	float ___m_fUpDownValue_24;
	// UnityStandardAssets.ImageEffects.Blur CameraManager::m_blur
	Blur_t3313275655 * ___m_blur_25;
	// CameraFollow CameraManager::CameraFollow
	CameraFollow_t1493855402 * ___CameraFollow_26;
	// CameraZoom CameraManager::CameraZoom
	CameraZoom_t1174393664 * ___CameraZoom_27;
	// CameraRotater CameraManager::CameraRotater
	CameraRotater_t122375930 * ___CameraRotater_28;
	// CameraEffect CameraManager::CameraEffect
	CameraEffect_t3191348726 * ___CameraEffect_29;
	// SubCameraHandler CameraManager::SubCameraHandler
	SubCameraHandler_t2753085375 * ___SubCameraHandler_30;
	// CameraSettings CameraManager::CameraSettings
	CameraSettings_t3536359094 * ___CameraSettings_31;
	// CameraAreaMonitor CameraManager::CameraAreaMonitor
	CameraAreaMonitor_t370298340 * ___CameraAreaMonitor_32;

public:
	inline static int32_t get_offset_of_MainCamera_3() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___MainCamera_3)); }
	inline Camera_t189460977 * get_MainCamera_3() const { return ___MainCamera_3; }
	inline Camera_t189460977 ** get_address_of_MainCamera_3() { return &___MainCamera_3; }
	inline void set_MainCamera_3(Camera_t189460977 * value)
	{
		___MainCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___MainCamera_3, value);
	}

	inline static int32_t get_offset_of_SubCamera_4() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___SubCamera_4)); }
	inline Camera_t189460977 * get_SubCamera_4() const { return ___SubCamera_4; }
	inline Camera_t189460977 ** get_address_of_SubCamera_4() { return &___SubCamera_4; }
	inline void set_SubCamera_4(Camera_t189460977 * value)
	{
		___SubCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___SubCamera_4, value);
	}

	inline static int32_t get_offset_of_ModelingCamera_5() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___ModelingCamera_5)); }
	inline Camera_t189460977 * get_ModelingCamera_5() const { return ___ModelingCamera_5; }
	inline Camera_t189460977 ** get_address_of_ModelingCamera_5() { return &___ModelingCamera_5; }
	inline void set_ModelingCamera_5(Camera_t189460977 * value)
	{
		___ModelingCamera_5 = value;
		Il2CppCodeGenWriteBarrier(&___ModelingCamera_5, value);
	}

	inline static int32_t get_offset_of_FrontSubCamera_6() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___FrontSubCamera_6)); }
	inline Camera_t189460977 * get_FrontSubCamera_6() const { return ___FrontSubCamera_6; }
	inline Camera_t189460977 ** get_address_of_FrontSubCamera_6() { return &___FrontSubCamera_6; }
	inline void set_FrontSubCamera_6(Camera_t189460977 * value)
	{
		___FrontSubCamera_6 = value;
		Il2CppCodeGenWriteBarrier(&___FrontSubCamera_6, value);
	}

	inline static int32_t get_offset_of_MobilityCamera_7() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___MobilityCamera_7)); }
	inline Camera_t189460977 * get_MobilityCamera_7() const { return ___MobilityCamera_7; }
	inline Camera_t189460977 ** get_address_of_MobilityCamera_7() { return &___MobilityCamera_7; }
	inline void set_MobilityCamera_7(Camera_t189460977 * value)
	{
		___MobilityCamera_7 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityCamera_7, value);
	}

	inline static int32_t get_offset_of_PoseCaptureCamera_8() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___PoseCaptureCamera_8)); }
	inline Camera_t189460977 * get_PoseCaptureCamera_8() const { return ___PoseCaptureCamera_8; }
	inline Camera_t189460977 ** get_address_of_PoseCaptureCamera_8() { return &___PoseCaptureCamera_8; }
	inline void set_PoseCaptureCamera_8(Camera_t189460977 * value)
	{
		___PoseCaptureCamera_8 = value;
		Il2CppCodeGenWriteBarrier(&___PoseCaptureCamera_8, value);
	}

	inline static int32_t get_offset_of_StartCamera_9() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___StartCamera_9)); }
	inline Camera_t189460977 * get_StartCamera_9() const { return ___StartCamera_9; }
	inline Camera_t189460977 ** get_address_of_StartCamera_9() { return &___StartCamera_9; }
	inline void set_StartCamera_9(Camera_t189460977 * value)
	{
		___StartCamera_9 = value;
		Il2CppCodeGenWriteBarrier(&___StartCamera_9, value);
	}

	inline static int32_t get_offset_of_MiniMapCamera_10() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___MiniMapCamera_10)); }
	inline Camera_t189460977 * get_MiniMapCamera_10() const { return ___MiniMapCamera_10; }
	inline Camera_t189460977 ** get_address_of_MiniMapCamera_10() { return &___MiniMapCamera_10; }
	inline void set_MiniMapCamera_10(Camera_t189460977 * value)
	{
		___MiniMapCamera_10 = value;
		Il2CppCodeGenWriteBarrier(&___MiniMapCamera_10, value);
	}

	inline static int32_t get_offset_of_WorldMapCamera_11() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___WorldMapCamera_11)); }
	inline Camera_t189460977 * get_WorldMapCamera_11() const { return ___WorldMapCamera_11; }
	inline Camera_t189460977 ** get_address_of_WorldMapCamera_11() { return &___WorldMapCamera_11; }
	inline void set_WorldMapCamera_11(Camera_t189460977 * value)
	{
		___WorldMapCamera_11 = value;
		Il2CppCodeGenWriteBarrier(&___WorldMapCamera_11, value);
	}

	inline static int32_t get_offset_of_MiniWorldCamera_12() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___MiniWorldCamera_12)); }
	inline Camera_t189460977 * get_MiniWorldCamera_12() const { return ___MiniWorldCamera_12; }
	inline Camera_t189460977 ** get_address_of_MiniWorldCamera_12() { return &___MiniWorldCamera_12; }
	inline void set_MiniWorldCamera_12(Camera_t189460977 * value)
	{
		___MiniWorldCamera_12 = value;
		Il2CppCodeGenWriteBarrier(&___MiniWorldCamera_12, value);
	}

	inline static int32_t get_offset_of_ModelingCameraCenter_13() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___ModelingCameraCenter_13)); }
	inline Transform_t3275118058 * get_ModelingCameraCenter_13() const { return ___ModelingCameraCenter_13; }
	inline Transform_t3275118058 ** get_address_of_ModelingCameraCenter_13() { return &___ModelingCameraCenter_13; }
	inline void set_ModelingCameraCenter_13(Transform_t3275118058 * value)
	{
		___ModelingCameraCenter_13 = value;
		Il2CppCodeGenWriteBarrier(&___ModelingCameraCenter_13, value);
	}

	inline static int32_t get_offset_of_MobilmoCenter_14() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___MobilmoCenter_14)); }
	inline Transform_t3275118058 * get_MobilmoCenter_14() const { return ___MobilmoCenter_14; }
	inline Transform_t3275118058 ** get_address_of_MobilmoCenter_14() { return &___MobilmoCenter_14; }
	inline void set_MobilmoCenter_14(Transform_t3275118058 * value)
	{
		___MobilmoCenter_14 = value;
		Il2CppCodeGenWriteBarrier(&___MobilmoCenter_14, value);
	}

	inline static int32_t get_offset_of_CameraParent_15() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___CameraParent_15)); }
	inline Transform_t3275118058 * get_CameraParent_15() const { return ___CameraParent_15; }
	inline Transform_t3275118058 ** get_address_of_CameraParent_15() { return &___CameraParent_15; }
	inline void set_CameraParent_15(Transform_t3275118058 * value)
	{
		___CameraParent_15 = value;
		Il2CppCodeGenWriteBarrier(&___CameraParent_15, value);
	}

	inline static int32_t get_offset_of_TitleParentPos_16() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___TitleParentPos_16)); }
	inline Vector3_t2243707580  get_TitleParentPos_16() const { return ___TitleParentPos_16; }
	inline Vector3_t2243707580 * get_address_of_TitleParentPos_16() { return &___TitleParentPos_16; }
	inline void set_TitleParentPos_16(Vector3_t2243707580  value)
	{
		___TitleParentPos_16 = value;
	}

	inline static int32_t get_offset_of_EditParentPos_17() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___EditParentPos_17)); }
	inline Vector3_t2243707580  get_EditParentPos_17() const { return ___EditParentPos_17; }
	inline Vector3_t2243707580 * get_address_of_EditParentPos_17() { return &___EditParentPos_17; }
	inline void set_EditParentPos_17(Vector3_t2243707580  value)
	{
		___EditParentPos_17 = value;
	}

	inline static int32_t get_offset_of_MypageParentPos_18() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___MypageParentPos_18)); }
	inline Vector3_t2243707580  get_MypageParentPos_18() const { return ___MypageParentPos_18; }
	inline Vector3_t2243707580 * get_address_of_MypageParentPos_18() { return &___MypageParentPos_18; }
	inline void set_MypageParentPos_18(Vector3_t2243707580  value)
	{
		___MypageParentPos_18 = value;
	}

	inline static int32_t get_offset_of_BlurFileter_19() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___BlurFileter_19)); }
	inline BlurFilter_t2530667 * get_BlurFileter_19() const { return ___BlurFileter_19; }
	inline BlurFilter_t2530667 ** get_address_of_BlurFileter_19() { return &___BlurFileter_19; }
	inline void set_BlurFileter_19(BlurFilter_t2530667 * value)
	{
		___BlurFileter_19 = value;
		Il2CppCodeGenWriteBarrier(&___BlurFileter_19, value);
	}

	inline static int32_t get_offset_of_ModuleTrans_20() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___ModuleTrans_20)); }
	inline List_1_t2644239190 * get_ModuleTrans_20() const { return ___ModuleTrans_20; }
	inline List_1_t2644239190 ** get_address_of_ModuleTrans_20() { return &___ModuleTrans_20; }
	inline void set_ModuleTrans_20(List_1_t2644239190 * value)
	{
		___ModuleTrans_20 = value;
		Il2CppCodeGenWriteBarrier(&___ModuleTrans_20, value);
	}

	inline static int32_t get_offset_of_m_bSubCamera_21() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___m_bSubCamera_21)); }
	inline bool get_m_bSubCamera_21() const { return ___m_bSubCamera_21; }
	inline bool* get_address_of_m_bSubCamera_21() { return &___m_bSubCamera_21; }
	inline void set_m_bSubCamera_21(bool value)
	{
		___m_bSubCamera_21 = value;
	}

	inline static int32_t get_offset_of_m_bMobilityCamera_22() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___m_bMobilityCamera_22)); }
	inline bool get_m_bMobilityCamera_22() const { return ___m_bMobilityCamera_22; }
	inline bool* get_address_of_m_bMobilityCamera_22() { return &___m_bMobilityCamera_22; }
	inline void set_m_bMobilityCamera_22(bool value)
	{
		___m_bMobilityCamera_22 = value;
	}

	inline static int32_t get_offset_of_isCanLogEvevt_23() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___isCanLogEvevt_23)); }
	inline bool get_isCanLogEvevt_23() const { return ___isCanLogEvevt_23; }
	inline bool* get_address_of_isCanLogEvevt_23() { return &___isCanLogEvevt_23; }
	inline void set_isCanLogEvevt_23(bool value)
	{
		___isCanLogEvevt_23 = value;
	}

	inline static int32_t get_offset_of_m_fUpDownValue_24() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___m_fUpDownValue_24)); }
	inline float get_m_fUpDownValue_24() const { return ___m_fUpDownValue_24; }
	inline float* get_address_of_m_fUpDownValue_24() { return &___m_fUpDownValue_24; }
	inline void set_m_fUpDownValue_24(float value)
	{
		___m_fUpDownValue_24 = value;
	}

	inline static int32_t get_offset_of_m_blur_25() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___m_blur_25)); }
	inline Blur_t3313275655 * get_m_blur_25() const { return ___m_blur_25; }
	inline Blur_t3313275655 ** get_address_of_m_blur_25() { return &___m_blur_25; }
	inline void set_m_blur_25(Blur_t3313275655 * value)
	{
		___m_blur_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_blur_25, value);
	}

	inline static int32_t get_offset_of_CameraFollow_26() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___CameraFollow_26)); }
	inline CameraFollow_t1493855402 * get_CameraFollow_26() const { return ___CameraFollow_26; }
	inline CameraFollow_t1493855402 ** get_address_of_CameraFollow_26() { return &___CameraFollow_26; }
	inline void set_CameraFollow_26(CameraFollow_t1493855402 * value)
	{
		___CameraFollow_26 = value;
		Il2CppCodeGenWriteBarrier(&___CameraFollow_26, value);
	}

	inline static int32_t get_offset_of_CameraZoom_27() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___CameraZoom_27)); }
	inline CameraZoom_t1174393664 * get_CameraZoom_27() const { return ___CameraZoom_27; }
	inline CameraZoom_t1174393664 ** get_address_of_CameraZoom_27() { return &___CameraZoom_27; }
	inline void set_CameraZoom_27(CameraZoom_t1174393664 * value)
	{
		___CameraZoom_27 = value;
		Il2CppCodeGenWriteBarrier(&___CameraZoom_27, value);
	}

	inline static int32_t get_offset_of_CameraRotater_28() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___CameraRotater_28)); }
	inline CameraRotater_t122375930 * get_CameraRotater_28() const { return ___CameraRotater_28; }
	inline CameraRotater_t122375930 ** get_address_of_CameraRotater_28() { return &___CameraRotater_28; }
	inline void set_CameraRotater_28(CameraRotater_t122375930 * value)
	{
		___CameraRotater_28 = value;
		Il2CppCodeGenWriteBarrier(&___CameraRotater_28, value);
	}

	inline static int32_t get_offset_of_CameraEffect_29() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___CameraEffect_29)); }
	inline CameraEffect_t3191348726 * get_CameraEffect_29() const { return ___CameraEffect_29; }
	inline CameraEffect_t3191348726 ** get_address_of_CameraEffect_29() { return &___CameraEffect_29; }
	inline void set_CameraEffect_29(CameraEffect_t3191348726 * value)
	{
		___CameraEffect_29 = value;
		Il2CppCodeGenWriteBarrier(&___CameraEffect_29, value);
	}

	inline static int32_t get_offset_of_SubCameraHandler_30() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___SubCameraHandler_30)); }
	inline SubCameraHandler_t2753085375 * get_SubCameraHandler_30() const { return ___SubCameraHandler_30; }
	inline SubCameraHandler_t2753085375 ** get_address_of_SubCameraHandler_30() { return &___SubCameraHandler_30; }
	inline void set_SubCameraHandler_30(SubCameraHandler_t2753085375 * value)
	{
		___SubCameraHandler_30 = value;
		Il2CppCodeGenWriteBarrier(&___SubCameraHandler_30, value);
	}

	inline static int32_t get_offset_of_CameraSettings_31() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___CameraSettings_31)); }
	inline CameraSettings_t3536359094 * get_CameraSettings_31() const { return ___CameraSettings_31; }
	inline CameraSettings_t3536359094 ** get_address_of_CameraSettings_31() { return &___CameraSettings_31; }
	inline void set_CameraSettings_31(CameraSettings_t3536359094 * value)
	{
		___CameraSettings_31 = value;
		Il2CppCodeGenWriteBarrier(&___CameraSettings_31, value);
	}

	inline static int32_t get_offset_of_CameraAreaMonitor_32() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___CameraAreaMonitor_32)); }
	inline CameraAreaMonitor_t370298340 * get_CameraAreaMonitor_32() const { return ___CameraAreaMonitor_32; }
	inline CameraAreaMonitor_t370298340 ** get_address_of_CameraAreaMonitor_32() { return &___CameraAreaMonitor_32; }
	inline void set_CameraAreaMonitor_32(CameraAreaMonitor_t370298340 * value)
	{
		___CameraAreaMonitor_32 = value;
		Il2CppCodeGenWriteBarrier(&___CameraAreaMonitor_32, value);
	}
};

struct CameraManager_t2379859346_StaticFields
{
public:
	// DG.Tweening.Core.DOGetter`1<System.Single> CameraManager::<>f__am$cache0
	DOGetter_1_t3858616074 * ___U3CU3Ef__amU24cache0_33;
	// DG.Tweening.Core.DOSetter`1<System.Single> CameraManager::<>f__am$cache1
	DOSetter_1_t3734738918 * ___U3CU3Ef__amU24cache1_34;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_33() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346_StaticFields, ___U3CU3Ef__amU24cache0_33)); }
	inline DOGetter_1_t3858616074 * get_U3CU3Ef__amU24cache0_33() const { return ___U3CU3Ef__amU24cache0_33; }
	inline DOGetter_1_t3858616074 ** get_address_of_U3CU3Ef__amU24cache0_33() { return &___U3CU3Ef__amU24cache0_33; }
	inline void set_U3CU3Ef__amU24cache0_33(DOGetter_1_t3858616074 * value)
	{
		___U3CU3Ef__amU24cache0_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_34() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346_StaticFields, ___U3CU3Ef__amU24cache1_34)); }
	inline DOSetter_1_t3734738918 * get_U3CU3Ef__amU24cache1_34() const { return ___U3CU3Ef__amU24cache1_34; }
	inline DOSetter_1_t3734738918 ** get_address_of_U3CU3Ef__amU24cache1_34() { return &___U3CU3Ef__amU24cache1_34; }
	inline void set_U3CU3Ef__amU24cache1_34(DOSetter_1_t3734738918 * value)
	{
		___U3CU3Ef__amU24cache1_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
