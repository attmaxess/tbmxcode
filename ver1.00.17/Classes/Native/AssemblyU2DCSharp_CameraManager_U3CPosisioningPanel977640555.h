﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// CameraManager
struct CameraManager_t2379859346;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraManager/<PosisioningPanelCamera>c__AnonStorey0
struct  U3CPosisioningPanelCameraU3Ec__AnonStorey0_t977640555  : public Il2CppObject
{
public:
	// System.Single CameraManager/<PosisioningPanelCamera>c__AnonStorey0::fCamX
	float ___fCamX_0;
	// CameraManager CameraManager/<PosisioningPanelCamera>c__AnonStorey0::$this
	CameraManager_t2379859346 * ___U24this_1;

public:
	inline static int32_t get_offset_of_fCamX_0() { return static_cast<int32_t>(offsetof(U3CPosisioningPanelCameraU3Ec__AnonStorey0_t977640555, ___fCamX_0)); }
	inline float get_fCamX_0() const { return ___fCamX_0; }
	inline float* get_address_of_fCamX_0() { return &___fCamX_0; }
	inline void set_fCamX_0(float value)
	{
		___fCamX_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPosisioningPanelCameraU3Ec__AnonStorey0_t977640555, ___U24this_1)); }
	inline CameraManager_t2379859346 * get_U24this_1() const { return ___U24this_1; }
	inline CameraManager_t2379859346 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CameraManager_t2379859346 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
