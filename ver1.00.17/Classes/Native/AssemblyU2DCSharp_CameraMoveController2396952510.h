﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.CharacterController
struct CharacterController_t4094781467;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraMoveController
struct  CameraMoveController_t2396952510  : public MonoBehaviour_t1158329972
{
public:
	// System.Single CameraMoveController::flySpeed
	float ___flySpeed_2;
	// System.Single CameraMoveController::walkSpeed
	float ___walkSpeed_3;
	// System.Single CameraMoveController::rotateSpeed
	float ___rotateSpeed_4;
	// UnityEngine.CharacterController CameraMoveController::characterController
	CharacterController_t4094781467 * ___characterController_5;
	// UnityEngine.Vector3 CameraMoveController::moveHumanStartPos
	Vector3_t2243707580  ___moveHumanStartPos_6;
	// UnityEngine.Vector3 CameraMoveController::moveParallelStartPos
	Vector3_t2243707580  ___moveParallelStartPos_7;
	// UnityEngine.Vector3 CameraMoveController::rotateViewStartPos
	Vector3_t2243707580  ___rotateViewStartPos_8;
	// UnityEngine.Vector3 CameraMoveController::lastInputDirection
	Vector3_t2243707580  ___lastInputDirection_9;

public:
	inline static int32_t get_offset_of_flySpeed_2() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___flySpeed_2)); }
	inline float get_flySpeed_2() const { return ___flySpeed_2; }
	inline float* get_address_of_flySpeed_2() { return &___flySpeed_2; }
	inline void set_flySpeed_2(float value)
	{
		___flySpeed_2 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_3() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___walkSpeed_3)); }
	inline float get_walkSpeed_3() const { return ___walkSpeed_3; }
	inline float* get_address_of_walkSpeed_3() { return &___walkSpeed_3; }
	inline void set_walkSpeed_3(float value)
	{
		___walkSpeed_3 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_4() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___rotateSpeed_4)); }
	inline float get_rotateSpeed_4() const { return ___rotateSpeed_4; }
	inline float* get_address_of_rotateSpeed_4() { return &___rotateSpeed_4; }
	inline void set_rotateSpeed_4(float value)
	{
		___rotateSpeed_4 = value;
	}

	inline static int32_t get_offset_of_characterController_5() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___characterController_5)); }
	inline CharacterController_t4094781467 * get_characterController_5() const { return ___characterController_5; }
	inline CharacterController_t4094781467 ** get_address_of_characterController_5() { return &___characterController_5; }
	inline void set_characterController_5(CharacterController_t4094781467 * value)
	{
		___characterController_5 = value;
		Il2CppCodeGenWriteBarrier(&___characterController_5, value);
	}

	inline static int32_t get_offset_of_moveHumanStartPos_6() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___moveHumanStartPos_6)); }
	inline Vector3_t2243707580  get_moveHumanStartPos_6() const { return ___moveHumanStartPos_6; }
	inline Vector3_t2243707580 * get_address_of_moveHumanStartPos_6() { return &___moveHumanStartPos_6; }
	inline void set_moveHumanStartPos_6(Vector3_t2243707580  value)
	{
		___moveHumanStartPos_6 = value;
	}

	inline static int32_t get_offset_of_moveParallelStartPos_7() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___moveParallelStartPos_7)); }
	inline Vector3_t2243707580  get_moveParallelStartPos_7() const { return ___moveParallelStartPos_7; }
	inline Vector3_t2243707580 * get_address_of_moveParallelStartPos_7() { return &___moveParallelStartPos_7; }
	inline void set_moveParallelStartPos_7(Vector3_t2243707580  value)
	{
		___moveParallelStartPos_7 = value;
	}

	inline static int32_t get_offset_of_rotateViewStartPos_8() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___rotateViewStartPos_8)); }
	inline Vector3_t2243707580  get_rotateViewStartPos_8() const { return ___rotateViewStartPos_8; }
	inline Vector3_t2243707580 * get_address_of_rotateViewStartPos_8() { return &___rotateViewStartPos_8; }
	inline void set_rotateViewStartPos_8(Vector3_t2243707580  value)
	{
		___rotateViewStartPos_8 = value;
	}

	inline static int32_t get_offset_of_lastInputDirection_9() { return static_cast<int32_t>(offsetof(CameraMoveController_t2396952510, ___lastInputDirection_9)); }
	inline Vector3_t2243707580  get_lastInputDirection_9() const { return ___lastInputDirection_9; }
	inline Vector3_t2243707580 * get_address_of_lastInputDirection_9() { return &___lastInputDirection_9; }
	inline void set_lastInputDirection_9(Vector3_t2243707580  value)
	{
		___lastInputDirection_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
