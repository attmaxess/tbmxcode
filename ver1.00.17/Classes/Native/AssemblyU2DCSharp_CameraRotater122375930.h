﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraRotater
struct  CameraRotater_t122375930  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CameraRotater::m_bRotateSwipe
	bool ___m_bRotateSwipe_2;

public:
	inline static int32_t get_offset_of_m_bRotateSwipe_2() { return static_cast<int32_t>(offsetof(CameraRotater_t122375930, ___m_bRotateSwipe_2)); }
	inline bool get_m_bRotateSwipe_2() const { return ___m_bRotateSwipe_2; }
	inline bool* get_address_of_m_bRotateSwipe_2() { return &___m_bRotateSwipe_2; }
	inline void set_m_bRotateSwipe_2(bool value)
	{
		___m_bRotateSwipe_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
