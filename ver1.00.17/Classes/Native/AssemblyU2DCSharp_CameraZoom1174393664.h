﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraZoom
struct  CameraZoom_t1174393664  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct CameraZoom_t1174393664_StaticFields
{
public:
	// System.Single CameraZoom::s_LimitZoomMin
	float ___s_LimitZoomMin_2;
	// System.Single CameraZoom::s_LimitZoomMax
	float ___s_LimitZoomMax_3;
	// System.Single CameraZoom::s_LimitZoomMinToWorld
	float ___s_LimitZoomMinToWorld_4;
	// System.Single CameraZoom::s_LimitZoomMaxToWorld
	float ___s_LimitZoomMaxToWorld_5;

public:
	inline static int32_t get_offset_of_s_LimitZoomMin_2() { return static_cast<int32_t>(offsetof(CameraZoom_t1174393664_StaticFields, ___s_LimitZoomMin_2)); }
	inline float get_s_LimitZoomMin_2() const { return ___s_LimitZoomMin_2; }
	inline float* get_address_of_s_LimitZoomMin_2() { return &___s_LimitZoomMin_2; }
	inline void set_s_LimitZoomMin_2(float value)
	{
		___s_LimitZoomMin_2 = value;
	}

	inline static int32_t get_offset_of_s_LimitZoomMax_3() { return static_cast<int32_t>(offsetof(CameraZoom_t1174393664_StaticFields, ___s_LimitZoomMax_3)); }
	inline float get_s_LimitZoomMax_3() const { return ___s_LimitZoomMax_3; }
	inline float* get_address_of_s_LimitZoomMax_3() { return &___s_LimitZoomMax_3; }
	inline void set_s_LimitZoomMax_3(float value)
	{
		___s_LimitZoomMax_3 = value;
	}

	inline static int32_t get_offset_of_s_LimitZoomMinToWorld_4() { return static_cast<int32_t>(offsetof(CameraZoom_t1174393664_StaticFields, ___s_LimitZoomMinToWorld_4)); }
	inline float get_s_LimitZoomMinToWorld_4() const { return ___s_LimitZoomMinToWorld_4; }
	inline float* get_address_of_s_LimitZoomMinToWorld_4() { return &___s_LimitZoomMinToWorld_4; }
	inline void set_s_LimitZoomMinToWorld_4(float value)
	{
		___s_LimitZoomMinToWorld_4 = value;
	}

	inline static int32_t get_offset_of_s_LimitZoomMaxToWorld_5() { return static_cast<int32_t>(offsetof(CameraZoom_t1174393664_StaticFields, ___s_LimitZoomMaxToWorld_5)); }
	inline float get_s_LimitZoomMaxToWorld_5() const { return ___s_LimitZoomMaxToWorld_5; }
	inline float* get_address_of_s_LimitZoomMaxToWorld_5() { return &___s_LimitZoomMaxToWorld_5; }
	inline void set_s_LimitZoomMaxToWorld_5(float value)
	{
		___s_LimitZoomMaxToWorld_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
