﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CandyBall
struct  CandyBall_t3642802284  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CandyBall::m_bIsMiniGameObj
	bool ___m_bIsMiniGameObj_2;
	// UnityEngine.GameObject CandyBall::CheckFlag
	GameObject_t1756533147 * ___CheckFlag_3;

public:
	inline static int32_t get_offset_of_m_bIsMiniGameObj_2() { return static_cast<int32_t>(offsetof(CandyBall_t3642802284, ___m_bIsMiniGameObj_2)); }
	inline bool get_m_bIsMiniGameObj_2() const { return ___m_bIsMiniGameObj_2; }
	inline bool* get_address_of_m_bIsMiniGameObj_2() { return &___m_bIsMiniGameObj_2; }
	inline void set_m_bIsMiniGameObj_2(bool value)
	{
		___m_bIsMiniGameObj_2 = value;
	}

	inline static int32_t get_offset_of_CheckFlag_3() { return static_cast<int32_t>(offsetof(CandyBall_t3642802284, ___CheckFlag_3)); }
	inline GameObject_t1756533147 * get_CheckFlag_3() const { return ___CheckFlag_3; }
	inline GameObject_t1756533147 ** get_address_of_CheckFlag_3() { return &___CheckFlag_3; }
	inline void set_CheckFlag_3(GameObject_t1756533147 * value)
	{
		___CheckFlag_3 = value;
		Il2CppCodeGenWriteBarrier(&___CheckFlag_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
