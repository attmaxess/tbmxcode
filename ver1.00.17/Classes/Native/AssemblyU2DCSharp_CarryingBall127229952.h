﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<PlaceData>
struct List_1_t845282761;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1490986844;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CarryingBall
struct  CarryingBall_t127229952  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CarryingBall::foldout
	bool ___foldout_2;
	// System.Collections.Generic.List`1<PlaceData> CarryingBall::placeData
	List_1_t845282761 * ___placeData_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CarryingBall::poolBallObject
	List_1_t1125654279 * ___poolBallObject_4;
	// System.Int32 CarryingBall::level
	int32_t ___level_5;
	// System.Single CarryingBall::m_sTime
	float ___m_sTime_6;
	// UnityEngine.GameObject CarryingBall::createBall
	GameObject_t1756533147 * ___createBall_7;
	// System.Int32 CarryingBall::ballNum
	int32_t ___ballNum_8;
	// System.Int32 CarryingBall::goalBallNum
	int32_t ___goalBallNum_9;
	// System.Int32 CarryingBall::m_mobilityId
	int32_t ___m_mobilityId_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CarryingBall::PointUI
	List_1_t1125654279 * ___PointUI_11;
	// UnityEngine.GameObject CarryingBall::CheckObjParent
	GameObject_t1756533147 * ___CheckObjParent_12;
	// UnityEngine.Sprite CarryingBall::NoPointImage
	Sprite_t309593783 * ___NoPointImage_13;
	// UnityEngine.Sprite CarryingBall::GetPointImage
	Sprite_t309593783 * ___GetPointImage_14;
	// UnityEngine.ParticleSystem[] CarryingBall::GoalParticle
	ParticleSystemU5BU5D_t1490986844* ___GoalParticle_15;
	// UnityEngine.Animator CarryingBall::FinishAnimator
	Animator_t69676727 * ___FinishAnimator_16;
	// UnityEngine.Transform CarryingBall::GoalCheckObj
	Transform_t3275118058 * ___GoalCheckObj_17;

public:
	inline static int32_t get_offset_of_foldout_2() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___foldout_2)); }
	inline bool get_foldout_2() const { return ___foldout_2; }
	inline bool* get_address_of_foldout_2() { return &___foldout_2; }
	inline void set_foldout_2(bool value)
	{
		___foldout_2 = value;
	}

	inline static int32_t get_offset_of_placeData_3() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___placeData_3)); }
	inline List_1_t845282761 * get_placeData_3() const { return ___placeData_3; }
	inline List_1_t845282761 ** get_address_of_placeData_3() { return &___placeData_3; }
	inline void set_placeData_3(List_1_t845282761 * value)
	{
		___placeData_3 = value;
		Il2CppCodeGenWriteBarrier(&___placeData_3, value);
	}

	inline static int32_t get_offset_of_poolBallObject_4() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___poolBallObject_4)); }
	inline List_1_t1125654279 * get_poolBallObject_4() const { return ___poolBallObject_4; }
	inline List_1_t1125654279 ** get_address_of_poolBallObject_4() { return &___poolBallObject_4; }
	inline void set_poolBallObject_4(List_1_t1125654279 * value)
	{
		___poolBallObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___poolBallObject_4, value);
	}

	inline static int32_t get_offset_of_level_5() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___level_5)); }
	inline int32_t get_level_5() const { return ___level_5; }
	inline int32_t* get_address_of_level_5() { return &___level_5; }
	inline void set_level_5(int32_t value)
	{
		___level_5 = value;
	}

	inline static int32_t get_offset_of_m_sTime_6() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___m_sTime_6)); }
	inline float get_m_sTime_6() const { return ___m_sTime_6; }
	inline float* get_address_of_m_sTime_6() { return &___m_sTime_6; }
	inline void set_m_sTime_6(float value)
	{
		___m_sTime_6 = value;
	}

	inline static int32_t get_offset_of_createBall_7() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___createBall_7)); }
	inline GameObject_t1756533147 * get_createBall_7() const { return ___createBall_7; }
	inline GameObject_t1756533147 ** get_address_of_createBall_7() { return &___createBall_7; }
	inline void set_createBall_7(GameObject_t1756533147 * value)
	{
		___createBall_7 = value;
		Il2CppCodeGenWriteBarrier(&___createBall_7, value);
	}

	inline static int32_t get_offset_of_ballNum_8() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___ballNum_8)); }
	inline int32_t get_ballNum_8() const { return ___ballNum_8; }
	inline int32_t* get_address_of_ballNum_8() { return &___ballNum_8; }
	inline void set_ballNum_8(int32_t value)
	{
		___ballNum_8 = value;
	}

	inline static int32_t get_offset_of_goalBallNum_9() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___goalBallNum_9)); }
	inline int32_t get_goalBallNum_9() const { return ___goalBallNum_9; }
	inline int32_t* get_address_of_goalBallNum_9() { return &___goalBallNum_9; }
	inline void set_goalBallNum_9(int32_t value)
	{
		___goalBallNum_9 = value;
	}

	inline static int32_t get_offset_of_m_mobilityId_10() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___m_mobilityId_10)); }
	inline int32_t get_m_mobilityId_10() const { return ___m_mobilityId_10; }
	inline int32_t* get_address_of_m_mobilityId_10() { return &___m_mobilityId_10; }
	inline void set_m_mobilityId_10(int32_t value)
	{
		___m_mobilityId_10 = value;
	}

	inline static int32_t get_offset_of_PointUI_11() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___PointUI_11)); }
	inline List_1_t1125654279 * get_PointUI_11() const { return ___PointUI_11; }
	inline List_1_t1125654279 ** get_address_of_PointUI_11() { return &___PointUI_11; }
	inline void set_PointUI_11(List_1_t1125654279 * value)
	{
		___PointUI_11 = value;
		Il2CppCodeGenWriteBarrier(&___PointUI_11, value);
	}

	inline static int32_t get_offset_of_CheckObjParent_12() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___CheckObjParent_12)); }
	inline GameObject_t1756533147 * get_CheckObjParent_12() const { return ___CheckObjParent_12; }
	inline GameObject_t1756533147 ** get_address_of_CheckObjParent_12() { return &___CheckObjParent_12; }
	inline void set_CheckObjParent_12(GameObject_t1756533147 * value)
	{
		___CheckObjParent_12 = value;
		Il2CppCodeGenWriteBarrier(&___CheckObjParent_12, value);
	}

	inline static int32_t get_offset_of_NoPointImage_13() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___NoPointImage_13)); }
	inline Sprite_t309593783 * get_NoPointImage_13() const { return ___NoPointImage_13; }
	inline Sprite_t309593783 ** get_address_of_NoPointImage_13() { return &___NoPointImage_13; }
	inline void set_NoPointImage_13(Sprite_t309593783 * value)
	{
		___NoPointImage_13 = value;
		Il2CppCodeGenWriteBarrier(&___NoPointImage_13, value);
	}

	inline static int32_t get_offset_of_GetPointImage_14() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___GetPointImage_14)); }
	inline Sprite_t309593783 * get_GetPointImage_14() const { return ___GetPointImage_14; }
	inline Sprite_t309593783 ** get_address_of_GetPointImage_14() { return &___GetPointImage_14; }
	inline void set_GetPointImage_14(Sprite_t309593783 * value)
	{
		___GetPointImage_14 = value;
		Il2CppCodeGenWriteBarrier(&___GetPointImage_14, value);
	}

	inline static int32_t get_offset_of_GoalParticle_15() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___GoalParticle_15)); }
	inline ParticleSystemU5BU5D_t1490986844* get_GoalParticle_15() const { return ___GoalParticle_15; }
	inline ParticleSystemU5BU5D_t1490986844** get_address_of_GoalParticle_15() { return &___GoalParticle_15; }
	inline void set_GoalParticle_15(ParticleSystemU5BU5D_t1490986844* value)
	{
		___GoalParticle_15 = value;
		Il2CppCodeGenWriteBarrier(&___GoalParticle_15, value);
	}

	inline static int32_t get_offset_of_FinishAnimator_16() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___FinishAnimator_16)); }
	inline Animator_t69676727 * get_FinishAnimator_16() const { return ___FinishAnimator_16; }
	inline Animator_t69676727 ** get_address_of_FinishAnimator_16() { return &___FinishAnimator_16; }
	inline void set_FinishAnimator_16(Animator_t69676727 * value)
	{
		___FinishAnimator_16 = value;
		Il2CppCodeGenWriteBarrier(&___FinishAnimator_16, value);
	}

	inline static int32_t get_offset_of_GoalCheckObj_17() { return static_cast<int32_t>(offsetof(CarryingBall_t127229952, ___GoalCheckObj_17)); }
	inline Transform_t3275118058 * get_GoalCheckObj_17() const { return ___GoalCheckObj_17; }
	inline Transform_t3275118058 ** get_address_of_GoalCheckObj_17() { return &___GoalCheckObj_17; }
	inline void set_GoalCheckObj_17(Transform_t3275118058 * value)
	{
		___GoalCheckObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___GoalCheckObj_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
