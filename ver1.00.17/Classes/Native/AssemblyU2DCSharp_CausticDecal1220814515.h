﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CausticDecal
struct  CausticDecal_t1220814515  : public MonoBehaviour_t1158329972
{
public:
	// System.Single CausticDecal::maxAngle
	float ___maxAngle_2;
	// System.Single CausticDecal::pushDistance
	float ___pushDistance_3;
	// UnityEngine.LayerMask CausticDecal::affectedLayers
	LayerMask_t3188175821  ___affectedLayers_4;

public:
	inline static int32_t get_offset_of_maxAngle_2() { return static_cast<int32_t>(offsetof(CausticDecal_t1220814515, ___maxAngle_2)); }
	inline float get_maxAngle_2() const { return ___maxAngle_2; }
	inline float* get_address_of_maxAngle_2() { return &___maxAngle_2; }
	inline void set_maxAngle_2(float value)
	{
		___maxAngle_2 = value;
	}

	inline static int32_t get_offset_of_pushDistance_3() { return static_cast<int32_t>(offsetof(CausticDecal_t1220814515, ___pushDistance_3)); }
	inline float get_pushDistance_3() const { return ___pushDistance_3; }
	inline float* get_address_of_pushDistance_3() { return &___pushDistance_3; }
	inline void set_pushDistance_3(float value)
	{
		___pushDistance_3 = value;
	}

	inline static int32_t get_offset_of_affectedLayers_4() { return static_cast<int32_t>(offsetof(CausticDecal_t1220814515, ___affectedLayers_4)); }
	inline LayerMask_t3188175821  get_affectedLayers_4() const { return ___affectedLayers_4; }
	inline LayerMask_t3188175821 * get_address_of_affectedLayers_4() { return &___affectedLayers_4; }
	inline void set_affectedLayers_4(LayerMask_t3188175821  value)
	{
		___affectedLayers_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
