﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Champion
struct  Champion_t3225559621  : public MonoBehaviour_t1158329972
{
public:
	// System.String Champion::test_Champion_Json
	String_t* ___test_Champion_Json_2;

public:
	inline static int32_t get_offset_of_test_Champion_Json_2() { return static_cast<int32_t>(offsetof(Champion_t3225559621, ___test_Champion_Json_2)); }
	inline String_t* get_test_Champion_Json_2() const { return ___test_Champion_Json_2; }
	inline String_t** get_address_of_test_Champion_Json_2() { return &___test_Champion_Json_2; }
	inline void set_test_Champion_Json_2(String_t* value)
	{
		___test_Champion_Json_2 = value;
		Il2CppCodeGenWriteBarrier(&___test_Champion_Json_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
