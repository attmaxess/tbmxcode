﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// ChampionMobilityModel
struct ChampionMobilityModel_t1041611931;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChampionItemModel
struct  ChampionItemModel_t975870399  : public Model_t873752437
{
public:
	// System.String ChampionItemModel::<champion_item_prized_type>k__BackingField
	String_t* ___U3Cchampion_item_prized_typeU3Ek__BackingField_0;
	// System.String ChampionItemModel::<champion_item_event>k__BackingField
	String_t* ___U3Cchampion_item_eventU3Ek__BackingField_1;
	// System.Int32 ChampionItemModel::<champion_item_point>k__BackingField
	int32_t ___U3Cchampion_item_pointU3Ek__BackingField_2;
	// System.String ChampionItemModel::<champion_item_prized_date>k__BackingField
	String_t* ___U3Cchampion_item_prized_dateU3Ek__BackingField_3;
	// System.String ChampionItemModel::<champion_item_user_name>k__BackingField
	String_t* ___U3Cchampion_item_user_nameU3Ek__BackingField_4;
	// System.String ChampionItemModel::<champion_item_user_country>k__BackingField
	String_t* ___U3Cchampion_item_user_countryU3Ek__BackingField_5;
	// System.Single ChampionItemModel::<champion_item_time>k__BackingField
	float ___U3Cchampion_item_timeU3Ek__BackingField_6;
	// System.String ChampionItemModel::<champion_item_user_rank>k__BackingField
	String_t* ___U3Cchampion_item_user_rankU3Ek__BackingField_7;
	// System.Single ChampionItemModel::<champion_item_score>k__BackingField
	float ___U3Cchampion_item_scoreU3Ek__BackingField_8;
	// System.String ChampionItemModel::<champion_item_createdUser_rank>k__BackingField
	String_t* ___U3Cchampion_item_createdUser_rankU3Ek__BackingField_9;
	// ChampionMobilityModel ChampionItemModel::<_champion_mobility_model>k__BackingField
	ChampionMobilityModel_t1041611931 * ___U3C_champion_mobility_modelU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3Cchampion_item_prized_typeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_prized_typeU3Ek__BackingField_0)); }
	inline String_t* get_U3Cchampion_item_prized_typeU3Ek__BackingField_0() const { return ___U3Cchampion_item_prized_typeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cchampion_item_prized_typeU3Ek__BackingField_0() { return &___U3Cchampion_item_prized_typeU3Ek__BackingField_0; }
	inline void set_U3Cchampion_item_prized_typeU3Ek__BackingField_0(String_t* value)
	{
		___U3Cchampion_item_prized_typeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_item_prized_typeU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_item_eventU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_eventU3Ek__BackingField_1)); }
	inline String_t* get_U3Cchampion_item_eventU3Ek__BackingField_1() const { return ___U3Cchampion_item_eventU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cchampion_item_eventU3Ek__BackingField_1() { return &___U3Cchampion_item_eventU3Ek__BackingField_1; }
	inline void set_U3Cchampion_item_eventU3Ek__BackingField_1(String_t* value)
	{
		___U3Cchampion_item_eventU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_item_eventU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_item_pointU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_pointU3Ek__BackingField_2)); }
	inline int32_t get_U3Cchampion_item_pointU3Ek__BackingField_2() const { return ___U3Cchampion_item_pointU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3Cchampion_item_pointU3Ek__BackingField_2() { return &___U3Cchampion_item_pointU3Ek__BackingField_2; }
	inline void set_U3Cchampion_item_pointU3Ek__BackingField_2(int32_t value)
	{
		___U3Cchampion_item_pointU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_item_prized_dateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_prized_dateU3Ek__BackingField_3)); }
	inline String_t* get_U3Cchampion_item_prized_dateU3Ek__BackingField_3() const { return ___U3Cchampion_item_prized_dateU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cchampion_item_prized_dateU3Ek__BackingField_3() { return &___U3Cchampion_item_prized_dateU3Ek__BackingField_3; }
	inline void set_U3Cchampion_item_prized_dateU3Ek__BackingField_3(String_t* value)
	{
		___U3Cchampion_item_prized_dateU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_item_prized_dateU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_item_user_nameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_user_nameU3Ek__BackingField_4)); }
	inline String_t* get_U3Cchampion_item_user_nameU3Ek__BackingField_4() const { return ___U3Cchampion_item_user_nameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cchampion_item_user_nameU3Ek__BackingField_4() { return &___U3Cchampion_item_user_nameU3Ek__BackingField_4; }
	inline void set_U3Cchampion_item_user_nameU3Ek__BackingField_4(String_t* value)
	{
		___U3Cchampion_item_user_nameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_item_user_nameU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_item_user_countryU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_user_countryU3Ek__BackingField_5)); }
	inline String_t* get_U3Cchampion_item_user_countryU3Ek__BackingField_5() const { return ___U3Cchampion_item_user_countryU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3Cchampion_item_user_countryU3Ek__BackingField_5() { return &___U3Cchampion_item_user_countryU3Ek__BackingField_5; }
	inline void set_U3Cchampion_item_user_countryU3Ek__BackingField_5(String_t* value)
	{
		___U3Cchampion_item_user_countryU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_item_user_countryU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_item_timeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_timeU3Ek__BackingField_6)); }
	inline float get_U3Cchampion_item_timeU3Ek__BackingField_6() const { return ___U3Cchampion_item_timeU3Ek__BackingField_6; }
	inline float* get_address_of_U3Cchampion_item_timeU3Ek__BackingField_6() { return &___U3Cchampion_item_timeU3Ek__BackingField_6; }
	inline void set_U3Cchampion_item_timeU3Ek__BackingField_6(float value)
	{
		___U3Cchampion_item_timeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_item_user_rankU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_user_rankU3Ek__BackingField_7)); }
	inline String_t* get_U3Cchampion_item_user_rankU3Ek__BackingField_7() const { return ___U3Cchampion_item_user_rankU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3Cchampion_item_user_rankU3Ek__BackingField_7() { return &___U3Cchampion_item_user_rankU3Ek__BackingField_7; }
	inline void set_U3Cchampion_item_user_rankU3Ek__BackingField_7(String_t* value)
	{
		___U3Cchampion_item_user_rankU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_item_user_rankU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_item_scoreU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_scoreU3Ek__BackingField_8)); }
	inline float get_U3Cchampion_item_scoreU3Ek__BackingField_8() const { return ___U3Cchampion_item_scoreU3Ek__BackingField_8; }
	inline float* get_address_of_U3Cchampion_item_scoreU3Ek__BackingField_8() { return &___U3Cchampion_item_scoreU3Ek__BackingField_8; }
	inline void set_U3Cchampion_item_scoreU3Ek__BackingField_8(float value)
	{
		___U3Cchampion_item_scoreU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_item_createdUser_rankU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3Cchampion_item_createdUser_rankU3Ek__BackingField_9)); }
	inline String_t* get_U3Cchampion_item_createdUser_rankU3Ek__BackingField_9() const { return ___U3Cchampion_item_createdUser_rankU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3Cchampion_item_createdUser_rankU3Ek__BackingField_9() { return &___U3Cchampion_item_createdUser_rankU3Ek__BackingField_9; }
	inline void set_U3Cchampion_item_createdUser_rankU3Ek__BackingField_9(String_t* value)
	{
		___U3Cchampion_item_createdUser_rankU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_item_createdUser_rankU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_modelU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ChampionItemModel_t975870399, ___U3C_champion_mobility_modelU3Ek__BackingField_10)); }
	inline ChampionMobilityModel_t1041611931 * get_U3C_champion_mobility_modelU3Ek__BackingField_10() const { return ___U3C_champion_mobility_modelU3Ek__BackingField_10; }
	inline ChampionMobilityModel_t1041611931 ** get_address_of_U3C_champion_mobility_modelU3Ek__BackingField_10() { return &___U3C_champion_mobility_modelU3Ek__BackingField_10; }
	inline void set_U3C_champion_mobility_modelU3Ek__BackingField_10(ChampionMobilityModel_t1041611931 * value)
	{
		___U3C_champion_mobility_modelU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_modelU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
