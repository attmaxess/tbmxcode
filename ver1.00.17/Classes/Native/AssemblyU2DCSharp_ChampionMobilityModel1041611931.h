﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// GetChampionMobilityReactionModel
struct GetChampionMobilityReactionModel_t2714706038;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<GetChampionMobilityEncountListModel>
struct List_1_t3174045999;
// System.Collections.Generic.List`1<GetChampionMobilityRegisterdMotionListModel>
struct List_1_t930105012;
// System.Collections.Generic.List`1<GetChampionMobilityRecordedMotionList>
struct List_1_t1262682796;
// System.Collections.Generic.List`1<GetChampionMobilityChildPartsListModel>
struct List_1_t550678861;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChampionMobilityModel
struct  ChampionMobilityModel_t1041611931  : public Model_t873752437
{
public:
	// System.Int32 ChampionMobilityModel::<champion_mobility_id>k__BackingField
	int32_t ___U3Cchampion_mobility_idU3Ek__BackingField_0;
	// System.String ChampionMobilityModel::<champion_mobility_date>k__BackingField
	String_t* ___U3Cchampion_mobility_dateU3Ek__BackingField_1;
	// System.String ChampionMobilityModel::<champion_mobility_name>k__BackingField
	String_t* ___U3Cchampion_mobility_nameU3Ek__BackingField_2;
	// System.String ChampionMobilityModel::<champion_mobility_situation>k__BackingField
	String_t* ___U3Cchampion_mobility_situationU3Ek__BackingField_3;
	// System.String ChampionMobilityModel::<champion_mobility_description>k__BackingField
	String_t* ___U3Cchampion_mobility_descriptionU3Ek__BackingField_4;
	// System.Int32 ChampionMobilityModel::<champion_mobility_copied_count>k__BackingField
	int32_t ___U3Cchampion_mobility_copied_countU3Ek__BackingField_5;
	// System.Int32 ChampionMobilityModel::<champion_mobility_parent_id>k__BackingField
	int32_t ___U3Cchampion_mobility_parent_idU3Ek__BackingField_6;
	// System.Int32 ChampionMobilityModel::<champion_mobility_core_parts_id>k__BackingField
	int32_t ___U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7;
	// System.Int32 ChampionMobilityModel::<champion_mobility_core_parts_color>k__BackingField
	int32_t ___U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8;
	// System.Single ChampionMobilityModel::<champion_mobility_core_parts_scale_x>k__BackingField
	float ___U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9;
	// System.Single ChampionMobilityModel::<champion_mobility_core_parts_scale_y>k__BackingField
	float ___U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10;
	// System.Single ChampionMobilityModel::<champion_mobility_core_parts_scale_z>k__BackingField
	float ___U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11;
	// System.String ChampionMobilityModel::<champion_mobility_core_parts_name>k__BackingField
	String_t* ___U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12;
	// System.Int32 ChampionMobilityModel::<champion_mobility_created_user_id>k__BackingField
	int32_t ___U3Cchampion_mobility_created_user_idU3Ek__BackingField_13;
	// GetChampionMobilityReactionModel ChampionMobilityModel::<_champion_mobility_reaction>k__BackingField
	GetChampionMobilityReactionModel_t2714706038 * ___U3C_champion_mobility_reactionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<System.Int32> ChampionMobilityModel::<_champion_mobility_prized_id_list>k__BackingField
	List_1_t1440998580 * ___U3C_champion_mobility_prized_id_listU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<GetChampionMobilityEncountListModel> ChampionMobilityModel::<_champion_mobility_ecount_list>k__BackingField
	List_1_t3174045999 * ___U3C_champion_mobility_ecount_listU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<GetChampionMobilityRegisterdMotionListModel> ChampionMobilityModel::<_champion_mobility_registerd_motion_list>k__BackingField
	List_1_t930105012 * ___U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17;
	// System.Collections.Generic.List`1<GetChampionMobilityRecordedMotionList> ChampionMobilityModel::<_champion_mobility_recorded_motion_list>k__BackingField
	List_1_t1262682796 * ___U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<GetChampionMobilityChildPartsListModel> ChampionMobilityModel::<_champion_mobility_child_parts_list>k__BackingField
	List_1_t550678861 * ___U3C_champion_mobility_child_parts_listU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_U3Cchampion_mobility_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_idU3Ek__BackingField_0)); }
	inline int32_t get_U3Cchampion_mobility_idU3Ek__BackingField_0() const { return ___U3Cchampion_mobility_idU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cchampion_mobility_idU3Ek__BackingField_0() { return &___U3Cchampion_mobility_idU3Ek__BackingField_0; }
	inline void set_U3Cchampion_mobility_idU3Ek__BackingField_0(int32_t value)
	{
		___U3Cchampion_mobility_idU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_dateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_dateU3Ek__BackingField_1)); }
	inline String_t* get_U3Cchampion_mobility_dateU3Ek__BackingField_1() const { return ___U3Cchampion_mobility_dateU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cchampion_mobility_dateU3Ek__BackingField_1() { return &___U3Cchampion_mobility_dateU3Ek__BackingField_1; }
	inline void set_U3Cchampion_mobility_dateU3Ek__BackingField_1(String_t* value)
	{
		___U3Cchampion_mobility_dateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_mobility_dateU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_nameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_nameU3Ek__BackingField_2)); }
	inline String_t* get_U3Cchampion_mobility_nameU3Ek__BackingField_2() const { return ___U3Cchampion_mobility_nameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cchampion_mobility_nameU3Ek__BackingField_2() { return &___U3Cchampion_mobility_nameU3Ek__BackingField_2; }
	inline void set_U3Cchampion_mobility_nameU3Ek__BackingField_2(String_t* value)
	{
		___U3Cchampion_mobility_nameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_mobility_nameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_situationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_situationU3Ek__BackingField_3)); }
	inline String_t* get_U3Cchampion_mobility_situationU3Ek__BackingField_3() const { return ___U3Cchampion_mobility_situationU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cchampion_mobility_situationU3Ek__BackingField_3() { return &___U3Cchampion_mobility_situationU3Ek__BackingField_3; }
	inline void set_U3Cchampion_mobility_situationU3Ek__BackingField_3(String_t* value)
	{
		___U3Cchampion_mobility_situationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_mobility_situationU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_descriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_descriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3Cchampion_mobility_descriptionU3Ek__BackingField_4() const { return ___U3Cchampion_mobility_descriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cchampion_mobility_descriptionU3Ek__BackingField_4() { return &___U3Cchampion_mobility_descriptionU3Ek__BackingField_4; }
	inline void set_U3Cchampion_mobility_descriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3Cchampion_mobility_descriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_mobility_descriptionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_copied_countU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_copied_countU3Ek__BackingField_5)); }
	inline int32_t get_U3Cchampion_mobility_copied_countU3Ek__BackingField_5() const { return ___U3Cchampion_mobility_copied_countU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3Cchampion_mobility_copied_countU3Ek__BackingField_5() { return &___U3Cchampion_mobility_copied_countU3Ek__BackingField_5; }
	inline void set_U3Cchampion_mobility_copied_countU3Ek__BackingField_5(int32_t value)
	{
		___U3Cchampion_mobility_copied_countU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_parent_idU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_parent_idU3Ek__BackingField_6)); }
	inline int32_t get_U3Cchampion_mobility_parent_idU3Ek__BackingField_6() const { return ___U3Cchampion_mobility_parent_idU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3Cchampion_mobility_parent_idU3Ek__BackingField_6() { return &___U3Cchampion_mobility_parent_idU3Ek__BackingField_6; }
	inline void set_U3Cchampion_mobility_parent_idU3Ek__BackingField_6(int32_t value)
	{
		___U3Cchampion_mobility_parent_idU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7)); }
	inline int32_t get_U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7() const { return ___U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7() { return &___U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7; }
	inline void set_U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7(int32_t value)
	{
		___U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8)); }
	inline int32_t get_U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8() const { return ___U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8() { return &___U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8; }
	inline void set_U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8(int32_t value)
	{
		___U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9)); }
	inline float get_U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9() const { return ___U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9; }
	inline float* get_address_of_U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9() { return &___U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9; }
	inline void set_U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9(float value)
	{
		___U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10)); }
	inline float get_U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10() const { return ___U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10; }
	inline float* get_address_of_U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10() { return &___U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10; }
	inline void set_U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10(float value)
	{
		___U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11)); }
	inline float get_U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11() const { return ___U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11; }
	inline float* get_address_of_U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11() { return &___U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11; }
	inline void set_U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11(float value)
	{
		___U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12)); }
	inline String_t* get_U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12() const { return ___U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12() { return &___U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12; }
	inline void set_U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12(String_t* value)
	{
		___U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3Cchampion_mobility_created_user_idU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3Cchampion_mobility_created_user_idU3Ek__BackingField_13)); }
	inline int32_t get_U3Cchampion_mobility_created_user_idU3Ek__BackingField_13() const { return ___U3Cchampion_mobility_created_user_idU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3Cchampion_mobility_created_user_idU3Ek__BackingField_13() { return &___U3Cchampion_mobility_created_user_idU3Ek__BackingField_13; }
	inline void set_U3Cchampion_mobility_created_user_idU3Ek__BackingField_13(int32_t value)
	{
		___U3Cchampion_mobility_created_user_idU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_reactionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3C_champion_mobility_reactionU3Ek__BackingField_14)); }
	inline GetChampionMobilityReactionModel_t2714706038 * get_U3C_champion_mobility_reactionU3Ek__BackingField_14() const { return ___U3C_champion_mobility_reactionU3Ek__BackingField_14; }
	inline GetChampionMobilityReactionModel_t2714706038 ** get_address_of_U3C_champion_mobility_reactionU3Ek__BackingField_14() { return &___U3C_champion_mobility_reactionU3Ek__BackingField_14; }
	inline void set_U3C_champion_mobility_reactionU3Ek__BackingField_14(GetChampionMobilityReactionModel_t2714706038 * value)
	{
		___U3C_champion_mobility_reactionU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_reactionU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_prized_id_listU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3C_champion_mobility_prized_id_listU3Ek__BackingField_15)); }
	inline List_1_t1440998580 * get_U3C_champion_mobility_prized_id_listU3Ek__BackingField_15() const { return ___U3C_champion_mobility_prized_id_listU3Ek__BackingField_15; }
	inline List_1_t1440998580 ** get_address_of_U3C_champion_mobility_prized_id_listU3Ek__BackingField_15() { return &___U3C_champion_mobility_prized_id_listU3Ek__BackingField_15; }
	inline void set_U3C_champion_mobility_prized_id_listU3Ek__BackingField_15(List_1_t1440998580 * value)
	{
		___U3C_champion_mobility_prized_id_listU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_prized_id_listU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_ecount_listU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3C_champion_mobility_ecount_listU3Ek__BackingField_16)); }
	inline List_1_t3174045999 * get_U3C_champion_mobility_ecount_listU3Ek__BackingField_16() const { return ___U3C_champion_mobility_ecount_listU3Ek__BackingField_16; }
	inline List_1_t3174045999 ** get_address_of_U3C_champion_mobility_ecount_listU3Ek__BackingField_16() { return &___U3C_champion_mobility_ecount_listU3Ek__BackingField_16; }
	inline void set_U3C_champion_mobility_ecount_listU3Ek__BackingField_16(List_1_t3174045999 * value)
	{
		___U3C_champion_mobility_ecount_listU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_ecount_listU3Ek__BackingField_16, value);
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17)); }
	inline List_1_t930105012 * get_U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17() const { return ___U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17; }
	inline List_1_t930105012 ** get_address_of_U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17() { return &___U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17; }
	inline void set_U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17(List_1_t930105012 * value)
	{
		___U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18)); }
	inline List_1_t1262682796 * get_U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18() const { return ___U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18; }
	inline List_1_t1262682796 ** get_address_of_U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18() { return &___U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18; }
	inline void set_U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18(List_1_t1262682796 * value)
	{
		___U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_child_parts_listU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ChampionMobilityModel_t1041611931, ___U3C_champion_mobility_child_parts_listU3Ek__BackingField_19)); }
	inline List_1_t550678861 * get_U3C_champion_mobility_child_parts_listU3Ek__BackingField_19() const { return ___U3C_champion_mobility_child_parts_listU3Ek__BackingField_19; }
	inline List_1_t550678861 ** get_address_of_U3C_champion_mobility_child_parts_listU3Ek__BackingField_19() { return &___U3C_champion_mobility_child_parts_listU3Ek__BackingField_19; }
	inline void set_U3C_champion_mobility_child_parts_listU3Ek__BackingField_19(List_1_t550678861 * value)
	{
		___U3C_champion_mobility_child_parts_listU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_child_parts_listU3Ek__BackingField_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
