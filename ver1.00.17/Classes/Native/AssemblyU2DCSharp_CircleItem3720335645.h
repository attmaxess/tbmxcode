﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleItem
struct  CircleItem_t3720335645  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image CircleItem::Image
	Image_t2042527209 * ___Image_2;
	// UnityEngine.RectTransform CircleItem::RectTrans
	RectTransform_t3349966182 * ___RectTrans_3;
	// System.Boolean CircleItem::IsCompleteToFadeIn
	bool ___IsCompleteToFadeIn_4;
	// System.Boolean CircleItem::IsCompleteToFadeOut
	bool ___IsCompleteToFadeOut_5;

public:
	inline static int32_t get_offset_of_Image_2() { return static_cast<int32_t>(offsetof(CircleItem_t3720335645, ___Image_2)); }
	inline Image_t2042527209 * get_Image_2() const { return ___Image_2; }
	inline Image_t2042527209 ** get_address_of_Image_2() { return &___Image_2; }
	inline void set_Image_2(Image_t2042527209 * value)
	{
		___Image_2 = value;
		Il2CppCodeGenWriteBarrier(&___Image_2, value);
	}

	inline static int32_t get_offset_of_RectTrans_3() { return static_cast<int32_t>(offsetof(CircleItem_t3720335645, ___RectTrans_3)); }
	inline RectTransform_t3349966182 * get_RectTrans_3() const { return ___RectTrans_3; }
	inline RectTransform_t3349966182 ** get_address_of_RectTrans_3() { return &___RectTrans_3; }
	inline void set_RectTrans_3(RectTransform_t3349966182 * value)
	{
		___RectTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___RectTrans_3, value);
	}

	inline static int32_t get_offset_of_IsCompleteToFadeIn_4() { return static_cast<int32_t>(offsetof(CircleItem_t3720335645, ___IsCompleteToFadeIn_4)); }
	inline bool get_IsCompleteToFadeIn_4() const { return ___IsCompleteToFadeIn_4; }
	inline bool* get_address_of_IsCompleteToFadeIn_4() { return &___IsCompleteToFadeIn_4; }
	inline void set_IsCompleteToFadeIn_4(bool value)
	{
		___IsCompleteToFadeIn_4 = value;
	}

	inline static int32_t get_offset_of_IsCompleteToFadeOut_5() { return static_cast<int32_t>(offsetof(CircleItem_t3720335645, ___IsCompleteToFadeOut_5)); }
	inline bool get_IsCompleteToFadeOut_5() const { return ___IsCompleteToFadeOut_5; }
	inline bool* get_address_of_IsCompleteToFadeOut_5() { return &___IsCompleteToFadeOut_5; }
	inline void set_IsCompleteToFadeOut_5(bool value)
	{
		___IsCompleteToFadeOut_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
