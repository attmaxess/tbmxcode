﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// CircleItem
struct CircleItem_t3720335645;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleItem/<BlinkOnceToFadeIn>c__AnonStorey0
struct  U3CBlinkOnceToFadeInU3Ec__AnonStorey0_t3578713304  : public Il2CppObject
{
public:
	// System.Single CircleItem/<BlinkOnceToFadeIn>c__AnonStorey0::time
	float ___time_0;
	// System.Single CircleItem/<BlinkOnceToFadeIn>c__AnonStorey0::fadeTime
	float ___fadeTime_1;
	// CircleItem CircleItem/<BlinkOnceToFadeIn>c__AnonStorey0::$this
	CircleItem_t3720335645 * ___U24this_2;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CBlinkOnceToFadeInU3Ec__AnonStorey0_t3578713304, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_fadeTime_1() { return static_cast<int32_t>(offsetof(U3CBlinkOnceToFadeInU3Ec__AnonStorey0_t3578713304, ___fadeTime_1)); }
	inline float get_fadeTime_1() const { return ___fadeTime_1; }
	inline float* get_address_of_fadeTime_1() { return &___fadeTime_1; }
	inline void set_fadeTime_1(float value)
	{
		___fadeTime_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBlinkOnceToFadeInU3Ec__AnonStorey0_t3578713304, ___U24this_2)); }
	inline CircleItem_t3720335645 * get_U24this_2() const { return ___U24this_2; }
	inline CircleItem_t3720335645 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CircleItem_t3720335645 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
