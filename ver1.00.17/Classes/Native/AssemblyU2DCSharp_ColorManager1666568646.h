﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen363244981.h"

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorManager
struct  ColorManager_t1666568646  : public SingletonMonoBehaviour_1_t363244981
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ColorManager::m_selectObject
	List_1_t1125654279 * ___m_selectObject_3;
	// UnityEngine.Material ColorManager::m_selectObjMaterial
	Material_t193706927 * ___m_selectObjMaterial_4;
	// System.Single ColorManager::m_fBright
	float ___m_fBright_5;
	// System.Single ColorManager::m_fBrightSpeed
	float ___m_fBrightSpeed_6;
	// System.Boolean ColorManager::m_bSwithon
	bool ___m_bSwithon_7;
	// System.Boolean ColorManager::m_bColorSelect
	bool ___m_bColorSelect_8;
	// UnityEngine.RectTransform ColorManager::ColorListObj
	RectTransform_t3349966182 * ___ColorListObj_9;
	// UnityEngine.Transform ColorManager::m_DefaultColorButton
	Transform_t3275118058 * ___m_DefaultColorButton_10;
	// UnityEngine.Color[] ColorManager::ColorRGBA
	ColorU5BU5D_t672350442* ___ColorRGBA_11;
	// System.Int32 ColorManager::TutorialSetColorNum
	int32_t ___TutorialSetColorNum_12;

public:
	inline static int32_t get_offset_of_m_selectObject_3() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___m_selectObject_3)); }
	inline List_1_t1125654279 * get_m_selectObject_3() const { return ___m_selectObject_3; }
	inline List_1_t1125654279 ** get_address_of_m_selectObject_3() { return &___m_selectObject_3; }
	inline void set_m_selectObject_3(List_1_t1125654279 * value)
	{
		___m_selectObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_selectObject_3, value);
	}

	inline static int32_t get_offset_of_m_selectObjMaterial_4() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___m_selectObjMaterial_4)); }
	inline Material_t193706927 * get_m_selectObjMaterial_4() const { return ___m_selectObjMaterial_4; }
	inline Material_t193706927 ** get_address_of_m_selectObjMaterial_4() { return &___m_selectObjMaterial_4; }
	inline void set_m_selectObjMaterial_4(Material_t193706927 * value)
	{
		___m_selectObjMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_selectObjMaterial_4, value);
	}

	inline static int32_t get_offset_of_m_fBright_5() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___m_fBright_5)); }
	inline float get_m_fBright_5() const { return ___m_fBright_5; }
	inline float* get_address_of_m_fBright_5() { return &___m_fBright_5; }
	inline void set_m_fBright_5(float value)
	{
		___m_fBright_5 = value;
	}

	inline static int32_t get_offset_of_m_fBrightSpeed_6() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___m_fBrightSpeed_6)); }
	inline float get_m_fBrightSpeed_6() const { return ___m_fBrightSpeed_6; }
	inline float* get_address_of_m_fBrightSpeed_6() { return &___m_fBrightSpeed_6; }
	inline void set_m_fBrightSpeed_6(float value)
	{
		___m_fBrightSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_bSwithon_7() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___m_bSwithon_7)); }
	inline bool get_m_bSwithon_7() const { return ___m_bSwithon_7; }
	inline bool* get_address_of_m_bSwithon_7() { return &___m_bSwithon_7; }
	inline void set_m_bSwithon_7(bool value)
	{
		___m_bSwithon_7 = value;
	}

	inline static int32_t get_offset_of_m_bColorSelect_8() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___m_bColorSelect_8)); }
	inline bool get_m_bColorSelect_8() const { return ___m_bColorSelect_8; }
	inline bool* get_address_of_m_bColorSelect_8() { return &___m_bColorSelect_8; }
	inline void set_m_bColorSelect_8(bool value)
	{
		___m_bColorSelect_8 = value;
	}

	inline static int32_t get_offset_of_ColorListObj_9() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___ColorListObj_9)); }
	inline RectTransform_t3349966182 * get_ColorListObj_9() const { return ___ColorListObj_9; }
	inline RectTransform_t3349966182 ** get_address_of_ColorListObj_9() { return &___ColorListObj_9; }
	inline void set_ColorListObj_9(RectTransform_t3349966182 * value)
	{
		___ColorListObj_9 = value;
		Il2CppCodeGenWriteBarrier(&___ColorListObj_9, value);
	}

	inline static int32_t get_offset_of_m_DefaultColorButton_10() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___m_DefaultColorButton_10)); }
	inline Transform_t3275118058 * get_m_DefaultColorButton_10() const { return ___m_DefaultColorButton_10; }
	inline Transform_t3275118058 ** get_address_of_m_DefaultColorButton_10() { return &___m_DefaultColorButton_10; }
	inline void set_m_DefaultColorButton_10(Transform_t3275118058 * value)
	{
		___m_DefaultColorButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_DefaultColorButton_10, value);
	}

	inline static int32_t get_offset_of_ColorRGBA_11() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___ColorRGBA_11)); }
	inline ColorU5BU5D_t672350442* get_ColorRGBA_11() const { return ___ColorRGBA_11; }
	inline ColorU5BU5D_t672350442** get_address_of_ColorRGBA_11() { return &___ColorRGBA_11; }
	inline void set_ColorRGBA_11(ColorU5BU5D_t672350442* value)
	{
		___ColorRGBA_11 = value;
		Il2CppCodeGenWriteBarrier(&___ColorRGBA_11, value);
	}

	inline static int32_t get_offset_of_TutorialSetColorNum_12() { return static_cast<int32_t>(offsetof(ColorManager_t1666568646, ___TutorialSetColorNum_12)); }
	inline int32_t get_TutorialSetColorNum_12() const { return ___TutorialSetColorNum_12; }
	inline int32_t* get_address_of_TutorialSetColorNum_12() { return &___TutorialSetColorNum_12; }
	inline void set_TutorialSetColorNum_12(int32_t value)
	{
		___TutorialSetColorNum_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
