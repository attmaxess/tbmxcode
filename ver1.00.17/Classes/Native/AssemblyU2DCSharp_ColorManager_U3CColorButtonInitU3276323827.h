﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// ColorManager
struct ColorManager_t1666568646;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorManager/<ColorButtonInit>c__AnonStorey0
struct  U3CColorButtonInitU3Ec__AnonStorey0_t3276323827  : public Il2CppObject
{
public:
	// System.Int32 ColorManager/<ColorButtonInit>c__AnonStorey0::num
	int32_t ___num_0;
	// ColorManager ColorManager/<ColorButtonInit>c__AnonStorey0::$this
	ColorManager_t1666568646 * ___U24this_1;

public:
	inline static int32_t get_offset_of_num_0() { return static_cast<int32_t>(offsetof(U3CColorButtonInitU3Ec__AnonStorey0_t3276323827, ___num_0)); }
	inline int32_t get_num_0() const { return ___num_0; }
	inline int32_t* get_address_of_num_0() { return &___num_0; }
	inline void set_num_0(int32_t value)
	{
		___num_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CColorButtonInitU3Ec__AnonStorey0_t3276323827, ___U24this_1)); }
	inline ColorManager_t1666568646 * get_U24this_1() const { return ___U24this_1; }
	inline ColorManager_t1666568646 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ColorManager_t1666568646 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
