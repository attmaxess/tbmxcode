﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_ColorPicker_ESTATE3746558862.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ColorPicker
struct ColorPicker_t3035206225;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPicker
struct  ColorPicker_t3035206225  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D ColorPicker::colorSpace
	Texture2D_t3542995729 * ___colorSpace_2;
	// UnityEngine.Texture2D ColorPicker::alphaGradient
	Texture2D_t3542995729 * ___alphaGradient_3;
	// System.String ColorPicker::Title
	String_t* ___Title_4;
	// UnityEngine.Vector2 ColorPicker::startPos
	Vector2_t2243707579  ___startPos_5;
	// UnityEngine.GameObject ColorPicker::receiver
	GameObject_t1756533147 * ___receiver_6;
	// System.String ColorPicker::colorSetFunctionName
	String_t* ___colorSetFunctionName_7;
	// System.String ColorPicker::colorGetFunctionName
	String_t* ___colorGetFunctionName_8;
	// System.Boolean ColorPicker::useExternalDrawer
	bool ___useExternalDrawer_9;
	// System.Int32 ColorPicker::drawOrder
	int32_t ___drawOrder_10;
	// UnityEngine.Color ColorPicker::TempColor
	Color_t2020392075  ___TempColor_11;
	// UnityEngine.Color ColorPicker::SelectedColor
	Color_t2020392075  ___SelectedColor_12;
	// ColorPicker/ESTATE ColorPicker::mState
	int32_t ___mState_14;
	// System.Int32 ColorPicker::sizeFull
	int32_t ___sizeFull_15;
	// System.Int32 ColorPicker::sizeHidden
	int32_t ___sizeHidden_16;
	// System.Single ColorPicker::animTime
	float ___animTime_17;
	// System.Single ColorPicker::dt
	float ___dt_18;
	// System.Single ColorPicker::sizeCurr
	float ___sizeCurr_19;
	// System.Single ColorPicker::alphaGradientHeight
	float ___alphaGradientHeight_20;
	// UnityEngine.GUIStyle ColorPicker::titleStyle
	GUIStyle_t1799908754 * ___titleStyle_21;
	// UnityEngine.Color ColorPicker::textColor
	Color_t2020392075  ___textColor_22;
	// UnityEngine.Texture2D ColorPicker::txColorDisplay
	Texture2D_t3542995729 * ___txColorDisplay_23;
	// System.String ColorPicker::txtR
	String_t* ___txtR_24;
	// System.String ColorPicker::txtG
	String_t* ___txtG_25;
	// System.String ColorPicker::txtB
	String_t* ___txtB_26;
	// System.String ColorPicker::txtA
	String_t* ___txtA_27;
	// System.Single ColorPicker::valR
	float ___valR_28;
	// System.Single ColorPicker::valG
	float ___valG_29;
	// System.Single ColorPicker::valB
	float ___valB_30;
	// System.Single ColorPicker::valA
	float ___valA_31;

public:
	inline static int32_t get_offset_of_colorSpace_2() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___colorSpace_2)); }
	inline Texture2D_t3542995729 * get_colorSpace_2() const { return ___colorSpace_2; }
	inline Texture2D_t3542995729 ** get_address_of_colorSpace_2() { return &___colorSpace_2; }
	inline void set_colorSpace_2(Texture2D_t3542995729 * value)
	{
		___colorSpace_2 = value;
		Il2CppCodeGenWriteBarrier(&___colorSpace_2, value);
	}

	inline static int32_t get_offset_of_alphaGradient_3() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___alphaGradient_3)); }
	inline Texture2D_t3542995729 * get_alphaGradient_3() const { return ___alphaGradient_3; }
	inline Texture2D_t3542995729 ** get_address_of_alphaGradient_3() { return &___alphaGradient_3; }
	inline void set_alphaGradient_3(Texture2D_t3542995729 * value)
	{
		___alphaGradient_3 = value;
		Il2CppCodeGenWriteBarrier(&___alphaGradient_3, value);
	}

	inline static int32_t get_offset_of_Title_4() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___Title_4)); }
	inline String_t* get_Title_4() const { return ___Title_4; }
	inline String_t** get_address_of_Title_4() { return &___Title_4; }
	inline void set_Title_4(String_t* value)
	{
		___Title_4 = value;
		Il2CppCodeGenWriteBarrier(&___Title_4, value);
	}

	inline static int32_t get_offset_of_startPos_5() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___startPos_5)); }
	inline Vector2_t2243707579  get_startPos_5() const { return ___startPos_5; }
	inline Vector2_t2243707579 * get_address_of_startPos_5() { return &___startPos_5; }
	inline void set_startPos_5(Vector2_t2243707579  value)
	{
		___startPos_5 = value;
	}

	inline static int32_t get_offset_of_receiver_6() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___receiver_6)); }
	inline GameObject_t1756533147 * get_receiver_6() const { return ___receiver_6; }
	inline GameObject_t1756533147 ** get_address_of_receiver_6() { return &___receiver_6; }
	inline void set_receiver_6(GameObject_t1756533147 * value)
	{
		___receiver_6 = value;
		Il2CppCodeGenWriteBarrier(&___receiver_6, value);
	}

	inline static int32_t get_offset_of_colorSetFunctionName_7() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___colorSetFunctionName_7)); }
	inline String_t* get_colorSetFunctionName_7() const { return ___colorSetFunctionName_7; }
	inline String_t** get_address_of_colorSetFunctionName_7() { return &___colorSetFunctionName_7; }
	inline void set_colorSetFunctionName_7(String_t* value)
	{
		___colorSetFunctionName_7 = value;
		Il2CppCodeGenWriteBarrier(&___colorSetFunctionName_7, value);
	}

	inline static int32_t get_offset_of_colorGetFunctionName_8() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___colorGetFunctionName_8)); }
	inline String_t* get_colorGetFunctionName_8() const { return ___colorGetFunctionName_8; }
	inline String_t** get_address_of_colorGetFunctionName_8() { return &___colorGetFunctionName_8; }
	inline void set_colorGetFunctionName_8(String_t* value)
	{
		___colorGetFunctionName_8 = value;
		Il2CppCodeGenWriteBarrier(&___colorGetFunctionName_8, value);
	}

	inline static int32_t get_offset_of_useExternalDrawer_9() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___useExternalDrawer_9)); }
	inline bool get_useExternalDrawer_9() const { return ___useExternalDrawer_9; }
	inline bool* get_address_of_useExternalDrawer_9() { return &___useExternalDrawer_9; }
	inline void set_useExternalDrawer_9(bool value)
	{
		___useExternalDrawer_9 = value;
	}

	inline static int32_t get_offset_of_drawOrder_10() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___drawOrder_10)); }
	inline int32_t get_drawOrder_10() const { return ___drawOrder_10; }
	inline int32_t* get_address_of_drawOrder_10() { return &___drawOrder_10; }
	inline void set_drawOrder_10(int32_t value)
	{
		___drawOrder_10 = value;
	}

	inline static int32_t get_offset_of_TempColor_11() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___TempColor_11)); }
	inline Color_t2020392075  get_TempColor_11() const { return ___TempColor_11; }
	inline Color_t2020392075 * get_address_of_TempColor_11() { return &___TempColor_11; }
	inline void set_TempColor_11(Color_t2020392075  value)
	{
		___TempColor_11 = value;
	}

	inline static int32_t get_offset_of_SelectedColor_12() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___SelectedColor_12)); }
	inline Color_t2020392075  get_SelectedColor_12() const { return ___SelectedColor_12; }
	inline Color_t2020392075 * get_address_of_SelectedColor_12() { return &___SelectedColor_12; }
	inline void set_SelectedColor_12(Color_t2020392075  value)
	{
		___SelectedColor_12 = value;
	}

	inline static int32_t get_offset_of_mState_14() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___mState_14)); }
	inline int32_t get_mState_14() const { return ___mState_14; }
	inline int32_t* get_address_of_mState_14() { return &___mState_14; }
	inline void set_mState_14(int32_t value)
	{
		___mState_14 = value;
	}

	inline static int32_t get_offset_of_sizeFull_15() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___sizeFull_15)); }
	inline int32_t get_sizeFull_15() const { return ___sizeFull_15; }
	inline int32_t* get_address_of_sizeFull_15() { return &___sizeFull_15; }
	inline void set_sizeFull_15(int32_t value)
	{
		___sizeFull_15 = value;
	}

	inline static int32_t get_offset_of_sizeHidden_16() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___sizeHidden_16)); }
	inline int32_t get_sizeHidden_16() const { return ___sizeHidden_16; }
	inline int32_t* get_address_of_sizeHidden_16() { return &___sizeHidden_16; }
	inline void set_sizeHidden_16(int32_t value)
	{
		___sizeHidden_16 = value;
	}

	inline static int32_t get_offset_of_animTime_17() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___animTime_17)); }
	inline float get_animTime_17() const { return ___animTime_17; }
	inline float* get_address_of_animTime_17() { return &___animTime_17; }
	inline void set_animTime_17(float value)
	{
		___animTime_17 = value;
	}

	inline static int32_t get_offset_of_dt_18() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___dt_18)); }
	inline float get_dt_18() const { return ___dt_18; }
	inline float* get_address_of_dt_18() { return &___dt_18; }
	inline void set_dt_18(float value)
	{
		___dt_18 = value;
	}

	inline static int32_t get_offset_of_sizeCurr_19() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___sizeCurr_19)); }
	inline float get_sizeCurr_19() const { return ___sizeCurr_19; }
	inline float* get_address_of_sizeCurr_19() { return &___sizeCurr_19; }
	inline void set_sizeCurr_19(float value)
	{
		___sizeCurr_19 = value;
	}

	inline static int32_t get_offset_of_alphaGradientHeight_20() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___alphaGradientHeight_20)); }
	inline float get_alphaGradientHeight_20() const { return ___alphaGradientHeight_20; }
	inline float* get_address_of_alphaGradientHeight_20() { return &___alphaGradientHeight_20; }
	inline void set_alphaGradientHeight_20(float value)
	{
		___alphaGradientHeight_20 = value;
	}

	inline static int32_t get_offset_of_titleStyle_21() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___titleStyle_21)); }
	inline GUIStyle_t1799908754 * get_titleStyle_21() const { return ___titleStyle_21; }
	inline GUIStyle_t1799908754 ** get_address_of_titleStyle_21() { return &___titleStyle_21; }
	inline void set_titleStyle_21(GUIStyle_t1799908754 * value)
	{
		___titleStyle_21 = value;
		Il2CppCodeGenWriteBarrier(&___titleStyle_21, value);
	}

	inline static int32_t get_offset_of_textColor_22() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___textColor_22)); }
	inline Color_t2020392075  get_textColor_22() const { return ___textColor_22; }
	inline Color_t2020392075 * get_address_of_textColor_22() { return &___textColor_22; }
	inline void set_textColor_22(Color_t2020392075  value)
	{
		___textColor_22 = value;
	}

	inline static int32_t get_offset_of_txColorDisplay_23() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___txColorDisplay_23)); }
	inline Texture2D_t3542995729 * get_txColorDisplay_23() const { return ___txColorDisplay_23; }
	inline Texture2D_t3542995729 ** get_address_of_txColorDisplay_23() { return &___txColorDisplay_23; }
	inline void set_txColorDisplay_23(Texture2D_t3542995729 * value)
	{
		___txColorDisplay_23 = value;
		Il2CppCodeGenWriteBarrier(&___txColorDisplay_23, value);
	}

	inline static int32_t get_offset_of_txtR_24() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___txtR_24)); }
	inline String_t* get_txtR_24() const { return ___txtR_24; }
	inline String_t** get_address_of_txtR_24() { return &___txtR_24; }
	inline void set_txtR_24(String_t* value)
	{
		___txtR_24 = value;
		Il2CppCodeGenWriteBarrier(&___txtR_24, value);
	}

	inline static int32_t get_offset_of_txtG_25() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___txtG_25)); }
	inline String_t* get_txtG_25() const { return ___txtG_25; }
	inline String_t** get_address_of_txtG_25() { return &___txtG_25; }
	inline void set_txtG_25(String_t* value)
	{
		___txtG_25 = value;
		Il2CppCodeGenWriteBarrier(&___txtG_25, value);
	}

	inline static int32_t get_offset_of_txtB_26() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___txtB_26)); }
	inline String_t* get_txtB_26() const { return ___txtB_26; }
	inline String_t** get_address_of_txtB_26() { return &___txtB_26; }
	inline void set_txtB_26(String_t* value)
	{
		___txtB_26 = value;
		Il2CppCodeGenWriteBarrier(&___txtB_26, value);
	}

	inline static int32_t get_offset_of_txtA_27() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___txtA_27)); }
	inline String_t* get_txtA_27() const { return ___txtA_27; }
	inline String_t** get_address_of_txtA_27() { return &___txtA_27; }
	inline void set_txtA_27(String_t* value)
	{
		___txtA_27 = value;
		Il2CppCodeGenWriteBarrier(&___txtA_27, value);
	}

	inline static int32_t get_offset_of_valR_28() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___valR_28)); }
	inline float get_valR_28() const { return ___valR_28; }
	inline float* get_address_of_valR_28() { return &___valR_28; }
	inline void set_valR_28(float value)
	{
		___valR_28 = value;
	}

	inline static int32_t get_offset_of_valG_29() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___valG_29)); }
	inline float get_valG_29() const { return ___valG_29; }
	inline float* get_address_of_valG_29() { return &___valG_29; }
	inline void set_valG_29(float value)
	{
		___valG_29 = value;
	}

	inline static int32_t get_offset_of_valB_30() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___valB_30)); }
	inline float get_valB_30() const { return ___valB_30; }
	inline float* get_address_of_valB_30() { return &___valB_30; }
	inline void set_valB_30(float value)
	{
		___valB_30 = value;
	}

	inline static int32_t get_offset_of_valA_31() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___valA_31)); }
	inline float get_valA_31() const { return ___valA_31; }
	inline float* get_address_of_valA_31() { return &___valA_31; }
	inline void set_valA_31(float value)
	{
		___valA_31 = value;
	}
};

struct ColorPicker_t3035206225_StaticFields
{
public:
	// ColorPicker ColorPicker::activeColorPicker
	ColorPicker_t3035206225 * ___activeColorPicker_13;

public:
	inline static int32_t get_offset_of_activeColorPicker_13() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225_StaticFields, ___activeColorPicker_13)); }
	inline ColorPicker_t3035206225 * get_activeColorPicker_13() const { return ___activeColorPicker_13; }
	inline ColorPicker_t3035206225 ** get_address_of_activeColorPicker_13() { return &___activeColorPicker_13; }
	inline void set_activeColorPicker_13(ColorPicker_t3035206225 * value)
	{
		___activeColorPicker_13 = value;
		Il2CppCodeGenWriteBarrier(&___activeColorPicker_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
