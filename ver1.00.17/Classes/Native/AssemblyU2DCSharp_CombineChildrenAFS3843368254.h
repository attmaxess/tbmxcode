﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Terrain
struct Terrain_t59182933;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CombineChildrenAFS
struct  CombineChildrenAFS_t3843368254  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CombineChildrenAFS::hideChildren
	bool ___hideChildren_2;
	// UnityEngine.Terrain CombineChildrenAFS::UnderlayingTerrain
	Terrain_t59182933 * ___UnderlayingTerrain_3;
	// System.Single CombineChildrenAFS::GroundMaxDistance
	float ___GroundMaxDistance_4;
	// System.Boolean CombineChildrenAFS::bakeGroundLightingGrass
	bool ___bakeGroundLightingGrass_5;
	// UnityEngine.Color CombineChildrenAFS::HealthyColor
	Color_t2020392075  ___HealthyColor_6;
	// UnityEngine.Color CombineChildrenAFS::DryColor
	Color_t2020392075  ___DryColor_7;
	// System.Single CombineChildrenAFS::NoiseSpread
	float ___NoiseSpread_8;
	// System.Boolean CombineChildrenAFS::bakeGroundLightingFoliage
	bool ___bakeGroundLightingFoliage_9;
	// System.Single CombineChildrenAFS::randomBrightness
	float ___randomBrightness_10;
	// System.Single CombineChildrenAFS::randomPulse
	float ___randomPulse_11;
	// System.Single CombineChildrenAFS::randomBending
	float ___randomBending_12;
	// System.Single CombineChildrenAFS::randomFluttering
	float ___randomFluttering_13;
	// System.Single CombineChildrenAFS::NoiseSpreadFoliage
	float ___NoiseSpreadFoliage_14;
	// System.Boolean CombineChildrenAFS::bakeScale
	bool ___bakeScale_15;
	// System.Boolean CombineChildrenAFS::debugNormals
	bool ___debugNormals_16;
	// System.Boolean CombineChildrenAFS::destroyChildObjectsInPlaymode
	bool ___destroyChildObjectsInPlaymode_17;
	// System.Boolean CombineChildrenAFS::CastShadows
	bool ___CastShadows_18;
	// System.Boolean CombineChildrenAFS::UseLightprobes
	bool ___UseLightprobes_19;
	// System.Single CombineChildrenAFS::RealignGroundMaxDistance
	float ___RealignGroundMaxDistance_20;
	// System.Boolean CombineChildrenAFS::ForceRealignment
	bool ___ForceRealignment_21;
	// System.Boolean CombineChildrenAFS::createUniqueUV2
	bool ___createUniqueUV2_22;
	// System.Boolean CombineChildrenAFS::createUniqueUV2playmode
	bool ___createUniqueUV2playmode_23;
	// System.Boolean CombineChildrenAFS::isStaticallyCombined
	bool ___isStaticallyCombined_24;
	// System.Boolean CombineChildrenAFS::simplyCombine
	bool ___simplyCombine_25;
	// System.Boolean CombineChildrenAFS::useUV4
	bool ___useUV4_26;

public:
	inline static int32_t get_offset_of_hideChildren_2() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___hideChildren_2)); }
	inline bool get_hideChildren_2() const { return ___hideChildren_2; }
	inline bool* get_address_of_hideChildren_2() { return &___hideChildren_2; }
	inline void set_hideChildren_2(bool value)
	{
		___hideChildren_2 = value;
	}

	inline static int32_t get_offset_of_UnderlayingTerrain_3() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___UnderlayingTerrain_3)); }
	inline Terrain_t59182933 * get_UnderlayingTerrain_3() const { return ___UnderlayingTerrain_3; }
	inline Terrain_t59182933 ** get_address_of_UnderlayingTerrain_3() { return &___UnderlayingTerrain_3; }
	inline void set_UnderlayingTerrain_3(Terrain_t59182933 * value)
	{
		___UnderlayingTerrain_3 = value;
		Il2CppCodeGenWriteBarrier(&___UnderlayingTerrain_3, value);
	}

	inline static int32_t get_offset_of_GroundMaxDistance_4() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___GroundMaxDistance_4)); }
	inline float get_GroundMaxDistance_4() const { return ___GroundMaxDistance_4; }
	inline float* get_address_of_GroundMaxDistance_4() { return &___GroundMaxDistance_4; }
	inline void set_GroundMaxDistance_4(float value)
	{
		___GroundMaxDistance_4 = value;
	}

	inline static int32_t get_offset_of_bakeGroundLightingGrass_5() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___bakeGroundLightingGrass_5)); }
	inline bool get_bakeGroundLightingGrass_5() const { return ___bakeGroundLightingGrass_5; }
	inline bool* get_address_of_bakeGroundLightingGrass_5() { return &___bakeGroundLightingGrass_5; }
	inline void set_bakeGroundLightingGrass_5(bool value)
	{
		___bakeGroundLightingGrass_5 = value;
	}

	inline static int32_t get_offset_of_HealthyColor_6() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___HealthyColor_6)); }
	inline Color_t2020392075  get_HealthyColor_6() const { return ___HealthyColor_6; }
	inline Color_t2020392075 * get_address_of_HealthyColor_6() { return &___HealthyColor_6; }
	inline void set_HealthyColor_6(Color_t2020392075  value)
	{
		___HealthyColor_6 = value;
	}

	inline static int32_t get_offset_of_DryColor_7() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___DryColor_7)); }
	inline Color_t2020392075  get_DryColor_7() const { return ___DryColor_7; }
	inline Color_t2020392075 * get_address_of_DryColor_7() { return &___DryColor_7; }
	inline void set_DryColor_7(Color_t2020392075  value)
	{
		___DryColor_7 = value;
	}

	inline static int32_t get_offset_of_NoiseSpread_8() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___NoiseSpread_8)); }
	inline float get_NoiseSpread_8() const { return ___NoiseSpread_8; }
	inline float* get_address_of_NoiseSpread_8() { return &___NoiseSpread_8; }
	inline void set_NoiseSpread_8(float value)
	{
		___NoiseSpread_8 = value;
	}

	inline static int32_t get_offset_of_bakeGroundLightingFoliage_9() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___bakeGroundLightingFoliage_9)); }
	inline bool get_bakeGroundLightingFoliage_9() const { return ___bakeGroundLightingFoliage_9; }
	inline bool* get_address_of_bakeGroundLightingFoliage_9() { return &___bakeGroundLightingFoliage_9; }
	inline void set_bakeGroundLightingFoliage_9(bool value)
	{
		___bakeGroundLightingFoliage_9 = value;
	}

	inline static int32_t get_offset_of_randomBrightness_10() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___randomBrightness_10)); }
	inline float get_randomBrightness_10() const { return ___randomBrightness_10; }
	inline float* get_address_of_randomBrightness_10() { return &___randomBrightness_10; }
	inline void set_randomBrightness_10(float value)
	{
		___randomBrightness_10 = value;
	}

	inline static int32_t get_offset_of_randomPulse_11() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___randomPulse_11)); }
	inline float get_randomPulse_11() const { return ___randomPulse_11; }
	inline float* get_address_of_randomPulse_11() { return &___randomPulse_11; }
	inline void set_randomPulse_11(float value)
	{
		___randomPulse_11 = value;
	}

	inline static int32_t get_offset_of_randomBending_12() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___randomBending_12)); }
	inline float get_randomBending_12() const { return ___randomBending_12; }
	inline float* get_address_of_randomBending_12() { return &___randomBending_12; }
	inline void set_randomBending_12(float value)
	{
		___randomBending_12 = value;
	}

	inline static int32_t get_offset_of_randomFluttering_13() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___randomFluttering_13)); }
	inline float get_randomFluttering_13() const { return ___randomFluttering_13; }
	inline float* get_address_of_randomFluttering_13() { return &___randomFluttering_13; }
	inline void set_randomFluttering_13(float value)
	{
		___randomFluttering_13 = value;
	}

	inline static int32_t get_offset_of_NoiseSpreadFoliage_14() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___NoiseSpreadFoliage_14)); }
	inline float get_NoiseSpreadFoliage_14() const { return ___NoiseSpreadFoliage_14; }
	inline float* get_address_of_NoiseSpreadFoliage_14() { return &___NoiseSpreadFoliage_14; }
	inline void set_NoiseSpreadFoliage_14(float value)
	{
		___NoiseSpreadFoliage_14 = value;
	}

	inline static int32_t get_offset_of_bakeScale_15() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___bakeScale_15)); }
	inline bool get_bakeScale_15() const { return ___bakeScale_15; }
	inline bool* get_address_of_bakeScale_15() { return &___bakeScale_15; }
	inline void set_bakeScale_15(bool value)
	{
		___bakeScale_15 = value;
	}

	inline static int32_t get_offset_of_debugNormals_16() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___debugNormals_16)); }
	inline bool get_debugNormals_16() const { return ___debugNormals_16; }
	inline bool* get_address_of_debugNormals_16() { return &___debugNormals_16; }
	inline void set_debugNormals_16(bool value)
	{
		___debugNormals_16 = value;
	}

	inline static int32_t get_offset_of_destroyChildObjectsInPlaymode_17() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___destroyChildObjectsInPlaymode_17)); }
	inline bool get_destroyChildObjectsInPlaymode_17() const { return ___destroyChildObjectsInPlaymode_17; }
	inline bool* get_address_of_destroyChildObjectsInPlaymode_17() { return &___destroyChildObjectsInPlaymode_17; }
	inline void set_destroyChildObjectsInPlaymode_17(bool value)
	{
		___destroyChildObjectsInPlaymode_17 = value;
	}

	inline static int32_t get_offset_of_CastShadows_18() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___CastShadows_18)); }
	inline bool get_CastShadows_18() const { return ___CastShadows_18; }
	inline bool* get_address_of_CastShadows_18() { return &___CastShadows_18; }
	inline void set_CastShadows_18(bool value)
	{
		___CastShadows_18 = value;
	}

	inline static int32_t get_offset_of_UseLightprobes_19() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___UseLightprobes_19)); }
	inline bool get_UseLightprobes_19() const { return ___UseLightprobes_19; }
	inline bool* get_address_of_UseLightprobes_19() { return &___UseLightprobes_19; }
	inline void set_UseLightprobes_19(bool value)
	{
		___UseLightprobes_19 = value;
	}

	inline static int32_t get_offset_of_RealignGroundMaxDistance_20() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___RealignGroundMaxDistance_20)); }
	inline float get_RealignGroundMaxDistance_20() const { return ___RealignGroundMaxDistance_20; }
	inline float* get_address_of_RealignGroundMaxDistance_20() { return &___RealignGroundMaxDistance_20; }
	inline void set_RealignGroundMaxDistance_20(float value)
	{
		___RealignGroundMaxDistance_20 = value;
	}

	inline static int32_t get_offset_of_ForceRealignment_21() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___ForceRealignment_21)); }
	inline bool get_ForceRealignment_21() const { return ___ForceRealignment_21; }
	inline bool* get_address_of_ForceRealignment_21() { return &___ForceRealignment_21; }
	inline void set_ForceRealignment_21(bool value)
	{
		___ForceRealignment_21 = value;
	}

	inline static int32_t get_offset_of_createUniqueUV2_22() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___createUniqueUV2_22)); }
	inline bool get_createUniqueUV2_22() const { return ___createUniqueUV2_22; }
	inline bool* get_address_of_createUniqueUV2_22() { return &___createUniqueUV2_22; }
	inline void set_createUniqueUV2_22(bool value)
	{
		___createUniqueUV2_22 = value;
	}

	inline static int32_t get_offset_of_createUniqueUV2playmode_23() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___createUniqueUV2playmode_23)); }
	inline bool get_createUniqueUV2playmode_23() const { return ___createUniqueUV2playmode_23; }
	inline bool* get_address_of_createUniqueUV2playmode_23() { return &___createUniqueUV2playmode_23; }
	inline void set_createUniqueUV2playmode_23(bool value)
	{
		___createUniqueUV2playmode_23 = value;
	}

	inline static int32_t get_offset_of_isStaticallyCombined_24() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___isStaticallyCombined_24)); }
	inline bool get_isStaticallyCombined_24() const { return ___isStaticallyCombined_24; }
	inline bool* get_address_of_isStaticallyCombined_24() { return &___isStaticallyCombined_24; }
	inline void set_isStaticallyCombined_24(bool value)
	{
		___isStaticallyCombined_24 = value;
	}

	inline static int32_t get_offset_of_simplyCombine_25() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___simplyCombine_25)); }
	inline bool get_simplyCombine_25() const { return ___simplyCombine_25; }
	inline bool* get_address_of_simplyCombine_25() { return &___simplyCombine_25; }
	inline void set_simplyCombine_25(bool value)
	{
		___simplyCombine_25 = value;
	}

	inline static int32_t get_offset_of_useUV4_26() { return static_cast<int32_t>(offsetof(CombineChildrenAFS_t3843368254, ___useUV4_26)); }
	inline bool get_useUV4_26() const { return ___useUV4_26; }
	inline bool* get_address_of_useUV4_26() { return &___useUV4_26; }
	inline void set_useUV4_26(bool value)
	{
		___useUV4_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
