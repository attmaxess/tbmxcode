﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CombineScript
struct  CombineScript_t1251438134  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material CombineScript::targetMaterial
	Material_t193706927 * ___targetMaterial_2;

public:
	inline static int32_t get_offset_of_targetMaterial_2() { return static_cast<int32_t>(offsetof(CombineScript_t1251438134, ___targetMaterial_2)); }
	inline Material_t193706927 * get_targetMaterial_2() const { return ___targetMaterial_2; }
	inline Material_t193706927 ** get_address_of_targetMaterial_2() { return &___targetMaterial_2; }
	inline void set_targetMaterial_2(Material_t193706927 * value)
	{
		___targetMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetMaterial_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
