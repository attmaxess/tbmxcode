﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.Slider
struct Slider_t297367283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigPanel
struct  ConfigPanel_t537490728  : public Il2CppObject
{
public:
	// UnityEngine.UI.Slider ConfigPanel::Rotation
	Slider_t297367283 * ___Rotation_0;
	// UnityEngine.UI.Slider ConfigPanel::SlopeX
	Slider_t297367283 * ___SlopeX_1;
	// UnityEngine.UI.Slider ConfigPanel::SlopeY
	Slider_t297367283 * ___SlopeY_2;

public:
	inline static int32_t get_offset_of_Rotation_0() { return static_cast<int32_t>(offsetof(ConfigPanel_t537490728, ___Rotation_0)); }
	inline Slider_t297367283 * get_Rotation_0() const { return ___Rotation_0; }
	inline Slider_t297367283 ** get_address_of_Rotation_0() { return &___Rotation_0; }
	inline void set_Rotation_0(Slider_t297367283 * value)
	{
		___Rotation_0 = value;
		Il2CppCodeGenWriteBarrier(&___Rotation_0, value);
	}

	inline static int32_t get_offset_of_SlopeX_1() { return static_cast<int32_t>(offsetof(ConfigPanel_t537490728, ___SlopeX_1)); }
	inline Slider_t297367283 * get_SlopeX_1() const { return ___SlopeX_1; }
	inline Slider_t297367283 ** get_address_of_SlopeX_1() { return &___SlopeX_1; }
	inline void set_SlopeX_1(Slider_t297367283 * value)
	{
		___SlopeX_1 = value;
		Il2CppCodeGenWriteBarrier(&___SlopeX_1, value);
	}

	inline static int32_t get_offset_of_SlopeY_2() { return static_cast<int32_t>(offsetof(ConfigPanel_t537490728, ___SlopeY_2)); }
	inline Slider_t297367283 * get_SlopeY_2() const { return ___SlopeY_2; }
	inline Slider_t297367283 ** get_address_of_SlopeY_2() { return &___SlopeY_2; }
	inline void set_SlopeY_2(Slider_t297367283 * value)
	{
		___SlopeY_2 = value;
		Il2CppCodeGenWriteBarrier(&___SlopeY_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
