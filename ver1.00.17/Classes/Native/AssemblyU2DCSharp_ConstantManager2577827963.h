﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstantManager
struct  ConstantManager_t2577827963  : public Il2CppObject
{
public:

public:
};

struct ConstantManager_t2577827963_StaticFields
{
public:
	// System.String ConstantManager::CreateUserEvent
	String_t* ___CreateUserEvent_0;
	// System.String ConstantManager::SelectCorePartEvent
	String_t* ___SelectCorePartEvent_1;
	// System.String ConstantManager::ResetCameraEvent
	String_t* ___ResetCameraEvent_2;
	// System.String ConstantManager::PartsDeleteEvent
	String_t* ___PartsDeleteEvent_3;
	// System.String ConstantManager::SelectPrimitivePartsEvent
	String_t* ___SelectPrimitivePartsEvent_4;
	// System.String ConstantManager::ChangeColorEvent
	String_t* ___ChangeColorEvent_5;
	// System.String ConstantManager::ChangeRotateEvent
	String_t* ___ChangeRotateEvent_6;
	// System.String ConstantManager::ChangeSlopeXEvent
	String_t* ___ChangeSlopeXEvent_7;
	// System.String ConstantManager::ChangeSlopeYEvent
	String_t* ___ChangeSlopeYEvent_8;
	// System.String ConstantManager::AIModeEvent
	String_t* ___AIModeEvent_9;
	// System.String[] ConstantManager::SelectWorldMapEvent
	StringU5BU5D_t1642385972* ___SelectWorldMapEvent_10;
	// System.String[] ConstantManager::ChallengeEvent
	StringU5BU5D_t1642385972* ___ChallengeEvent_11;
	// System.String[] ConstantManager::LeverControlEvent
	StringU5BU5D_t1642385972* ___LeverControlEvent_12;
	// System.String ConstantManager::RollControlEvent
	String_t* ___RollControlEvent_13;
	// System.String[] ConstantManager::RecordTiming
	StringU5BU5D_t1642385972* ___RecordTiming_14;
	// System.String ConstantManager::EncountMobility
	String_t* ___EncountMobility_15;
	// System.String ConstantManager::CreateEvent
	String_t* ___CreateEvent_16;
	// System.String ConstantManager::CreateMobilmoEvent
	String_t* ___CreateMobilmoEvent_17;
	// System.String ConstantManager::CreateModuleEvent
	String_t* ___CreateModuleEvent_18;
	// System.String ConstantManager::MoveToWorldEvent
	String_t* ___MoveToWorldEvent_19;
	// System.String ConstantManager::MoveToPartsListEvent
	String_t* ___MoveToPartsListEvent_20;
	// System.String ConstantManager::MoveToActionSetEvent
	String_t* ___MoveToActionSetEvent_21;
	// System.String ConstantManager::MoveToBilmoListEvent
	String_t* ___MoveToBilmoListEvent_22;
	// System.String ConstantManager::EditMobilmoEvent
	String_t* ___EditMobilmoEvent_23;
	// System.String ConstantManager::DeleteMobilmoEvent
	String_t* ___DeleteMobilmoEvent_24;
	// System.String ConstantManager::MoveToNewsEvent
	String_t* ___MoveToNewsEvent_25;
	// System.String ConstantManager::MoveToBilmoRaderEvent
	String_t* ___MoveToBilmoRaderEvent_26;
	// System.String ConstantManager::BackToMyPageEvent
	String_t* ___BackToMyPageEvent_27;
	// System.String ConstantManager::MoveToOtherEvent
	String_t* ___MoveToOtherEvent_28;
	// System.String ConstantManager::MoveToSearchPlayer
	String_t* ___MoveToSearchPlayer_29;
	// System.String ConstantManager::GetModuleEvent
	String_t* ___GetModuleEvent_30;
	// System.String ConstantManager::RemakeModule
	String_t* ___RemakeModule_31;
	// System.String ConstantManager::PrizedEventCount
	String_t* ___PrizedEventCount_32;
	// System.String ConstantManager::UserRankUpCount
	String_t* ___UserRankUpCount_33;
	// System.String ConstantManager::ReactionCount
	String_t* ___ReactionCount_34;
	// System.String ConstantManager::TutorialFinishCount
	String_t* ___TutorialFinishCount_35;
	// System.String ConstantManager::MyPageCount
	String_t* ___MyPageCount_36;
	// System.String ConstantManager::RecordFootPrint
	String_t* ___RecordFootPrint_37;

public:
	inline static int32_t get_offset_of_CreateUserEvent_0() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___CreateUserEvent_0)); }
	inline String_t* get_CreateUserEvent_0() const { return ___CreateUserEvent_0; }
	inline String_t** get_address_of_CreateUserEvent_0() { return &___CreateUserEvent_0; }
	inline void set_CreateUserEvent_0(String_t* value)
	{
		___CreateUserEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___CreateUserEvent_0, value);
	}

	inline static int32_t get_offset_of_SelectCorePartEvent_1() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___SelectCorePartEvent_1)); }
	inline String_t* get_SelectCorePartEvent_1() const { return ___SelectCorePartEvent_1; }
	inline String_t** get_address_of_SelectCorePartEvent_1() { return &___SelectCorePartEvent_1; }
	inline void set_SelectCorePartEvent_1(String_t* value)
	{
		___SelectCorePartEvent_1 = value;
		Il2CppCodeGenWriteBarrier(&___SelectCorePartEvent_1, value);
	}

	inline static int32_t get_offset_of_ResetCameraEvent_2() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___ResetCameraEvent_2)); }
	inline String_t* get_ResetCameraEvent_2() const { return ___ResetCameraEvent_2; }
	inline String_t** get_address_of_ResetCameraEvent_2() { return &___ResetCameraEvent_2; }
	inline void set_ResetCameraEvent_2(String_t* value)
	{
		___ResetCameraEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___ResetCameraEvent_2, value);
	}

	inline static int32_t get_offset_of_PartsDeleteEvent_3() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___PartsDeleteEvent_3)); }
	inline String_t* get_PartsDeleteEvent_3() const { return ___PartsDeleteEvent_3; }
	inline String_t** get_address_of_PartsDeleteEvent_3() { return &___PartsDeleteEvent_3; }
	inline void set_PartsDeleteEvent_3(String_t* value)
	{
		___PartsDeleteEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___PartsDeleteEvent_3, value);
	}

	inline static int32_t get_offset_of_SelectPrimitivePartsEvent_4() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___SelectPrimitivePartsEvent_4)); }
	inline String_t* get_SelectPrimitivePartsEvent_4() const { return ___SelectPrimitivePartsEvent_4; }
	inline String_t** get_address_of_SelectPrimitivePartsEvent_4() { return &___SelectPrimitivePartsEvent_4; }
	inline void set_SelectPrimitivePartsEvent_4(String_t* value)
	{
		___SelectPrimitivePartsEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___SelectPrimitivePartsEvent_4, value);
	}

	inline static int32_t get_offset_of_ChangeColorEvent_5() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___ChangeColorEvent_5)); }
	inline String_t* get_ChangeColorEvent_5() const { return ___ChangeColorEvent_5; }
	inline String_t** get_address_of_ChangeColorEvent_5() { return &___ChangeColorEvent_5; }
	inline void set_ChangeColorEvent_5(String_t* value)
	{
		___ChangeColorEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___ChangeColorEvent_5, value);
	}

	inline static int32_t get_offset_of_ChangeRotateEvent_6() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___ChangeRotateEvent_6)); }
	inline String_t* get_ChangeRotateEvent_6() const { return ___ChangeRotateEvent_6; }
	inline String_t** get_address_of_ChangeRotateEvent_6() { return &___ChangeRotateEvent_6; }
	inline void set_ChangeRotateEvent_6(String_t* value)
	{
		___ChangeRotateEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___ChangeRotateEvent_6, value);
	}

	inline static int32_t get_offset_of_ChangeSlopeXEvent_7() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___ChangeSlopeXEvent_7)); }
	inline String_t* get_ChangeSlopeXEvent_7() const { return ___ChangeSlopeXEvent_7; }
	inline String_t** get_address_of_ChangeSlopeXEvent_7() { return &___ChangeSlopeXEvent_7; }
	inline void set_ChangeSlopeXEvent_7(String_t* value)
	{
		___ChangeSlopeXEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___ChangeSlopeXEvent_7, value);
	}

	inline static int32_t get_offset_of_ChangeSlopeYEvent_8() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___ChangeSlopeYEvent_8)); }
	inline String_t* get_ChangeSlopeYEvent_8() const { return ___ChangeSlopeYEvent_8; }
	inline String_t** get_address_of_ChangeSlopeYEvent_8() { return &___ChangeSlopeYEvent_8; }
	inline void set_ChangeSlopeYEvent_8(String_t* value)
	{
		___ChangeSlopeYEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___ChangeSlopeYEvent_8, value);
	}

	inline static int32_t get_offset_of_AIModeEvent_9() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___AIModeEvent_9)); }
	inline String_t* get_AIModeEvent_9() const { return ___AIModeEvent_9; }
	inline String_t** get_address_of_AIModeEvent_9() { return &___AIModeEvent_9; }
	inline void set_AIModeEvent_9(String_t* value)
	{
		___AIModeEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___AIModeEvent_9, value);
	}

	inline static int32_t get_offset_of_SelectWorldMapEvent_10() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___SelectWorldMapEvent_10)); }
	inline StringU5BU5D_t1642385972* get_SelectWorldMapEvent_10() const { return ___SelectWorldMapEvent_10; }
	inline StringU5BU5D_t1642385972** get_address_of_SelectWorldMapEvent_10() { return &___SelectWorldMapEvent_10; }
	inline void set_SelectWorldMapEvent_10(StringU5BU5D_t1642385972* value)
	{
		___SelectWorldMapEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___SelectWorldMapEvent_10, value);
	}

	inline static int32_t get_offset_of_ChallengeEvent_11() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___ChallengeEvent_11)); }
	inline StringU5BU5D_t1642385972* get_ChallengeEvent_11() const { return ___ChallengeEvent_11; }
	inline StringU5BU5D_t1642385972** get_address_of_ChallengeEvent_11() { return &___ChallengeEvent_11; }
	inline void set_ChallengeEvent_11(StringU5BU5D_t1642385972* value)
	{
		___ChallengeEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___ChallengeEvent_11, value);
	}

	inline static int32_t get_offset_of_LeverControlEvent_12() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___LeverControlEvent_12)); }
	inline StringU5BU5D_t1642385972* get_LeverControlEvent_12() const { return ___LeverControlEvent_12; }
	inline StringU5BU5D_t1642385972** get_address_of_LeverControlEvent_12() { return &___LeverControlEvent_12; }
	inline void set_LeverControlEvent_12(StringU5BU5D_t1642385972* value)
	{
		___LeverControlEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___LeverControlEvent_12, value);
	}

	inline static int32_t get_offset_of_RollControlEvent_13() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___RollControlEvent_13)); }
	inline String_t* get_RollControlEvent_13() const { return ___RollControlEvent_13; }
	inline String_t** get_address_of_RollControlEvent_13() { return &___RollControlEvent_13; }
	inline void set_RollControlEvent_13(String_t* value)
	{
		___RollControlEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___RollControlEvent_13, value);
	}

	inline static int32_t get_offset_of_RecordTiming_14() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___RecordTiming_14)); }
	inline StringU5BU5D_t1642385972* get_RecordTiming_14() const { return ___RecordTiming_14; }
	inline StringU5BU5D_t1642385972** get_address_of_RecordTiming_14() { return &___RecordTiming_14; }
	inline void set_RecordTiming_14(StringU5BU5D_t1642385972* value)
	{
		___RecordTiming_14 = value;
		Il2CppCodeGenWriteBarrier(&___RecordTiming_14, value);
	}

	inline static int32_t get_offset_of_EncountMobility_15() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___EncountMobility_15)); }
	inline String_t* get_EncountMobility_15() const { return ___EncountMobility_15; }
	inline String_t** get_address_of_EncountMobility_15() { return &___EncountMobility_15; }
	inline void set_EncountMobility_15(String_t* value)
	{
		___EncountMobility_15 = value;
		Il2CppCodeGenWriteBarrier(&___EncountMobility_15, value);
	}

	inline static int32_t get_offset_of_CreateEvent_16() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___CreateEvent_16)); }
	inline String_t* get_CreateEvent_16() const { return ___CreateEvent_16; }
	inline String_t** get_address_of_CreateEvent_16() { return &___CreateEvent_16; }
	inline void set_CreateEvent_16(String_t* value)
	{
		___CreateEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___CreateEvent_16, value);
	}

	inline static int32_t get_offset_of_CreateMobilmoEvent_17() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___CreateMobilmoEvent_17)); }
	inline String_t* get_CreateMobilmoEvent_17() const { return ___CreateMobilmoEvent_17; }
	inline String_t** get_address_of_CreateMobilmoEvent_17() { return &___CreateMobilmoEvent_17; }
	inline void set_CreateMobilmoEvent_17(String_t* value)
	{
		___CreateMobilmoEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___CreateMobilmoEvent_17, value);
	}

	inline static int32_t get_offset_of_CreateModuleEvent_18() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___CreateModuleEvent_18)); }
	inline String_t* get_CreateModuleEvent_18() const { return ___CreateModuleEvent_18; }
	inline String_t** get_address_of_CreateModuleEvent_18() { return &___CreateModuleEvent_18; }
	inline void set_CreateModuleEvent_18(String_t* value)
	{
		___CreateModuleEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___CreateModuleEvent_18, value);
	}

	inline static int32_t get_offset_of_MoveToWorldEvent_19() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToWorldEvent_19)); }
	inline String_t* get_MoveToWorldEvent_19() const { return ___MoveToWorldEvent_19; }
	inline String_t** get_address_of_MoveToWorldEvent_19() { return &___MoveToWorldEvent_19; }
	inline void set_MoveToWorldEvent_19(String_t* value)
	{
		___MoveToWorldEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToWorldEvent_19, value);
	}

	inline static int32_t get_offset_of_MoveToPartsListEvent_20() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToPartsListEvent_20)); }
	inline String_t* get_MoveToPartsListEvent_20() const { return ___MoveToPartsListEvent_20; }
	inline String_t** get_address_of_MoveToPartsListEvent_20() { return &___MoveToPartsListEvent_20; }
	inline void set_MoveToPartsListEvent_20(String_t* value)
	{
		___MoveToPartsListEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToPartsListEvent_20, value);
	}

	inline static int32_t get_offset_of_MoveToActionSetEvent_21() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToActionSetEvent_21)); }
	inline String_t* get_MoveToActionSetEvent_21() const { return ___MoveToActionSetEvent_21; }
	inline String_t** get_address_of_MoveToActionSetEvent_21() { return &___MoveToActionSetEvent_21; }
	inline void set_MoveToActionSetEvent_21(String_t* value)
	{
		___MoveToActionSetEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToActionSetEvent_21, value);
	}

	inline static int32_t get_offset_of_MoveToBilmoListEvent_22() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToBilmoListEvent_22)); }
	inline String_t* get_MoveToBilmoListEvent_22() const { return ___MoveToBilmoListEvent_22; }
	inline String_t** get_address_of_MoveToBilmoListEvent_22() { return &___MoveToBilmoListEvent_22; }
	inline void set_MoveToBilmoListEvent_22(String_t* value)
	{
		___MoveToBilmoListEvent_22 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToBilmoListEvent_22, value);
	}

	inline static int32_t get_offset_of_EditMobilmoEvent_23() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___EditMobilmoEvent_23)); }
	inline String_t* get_EditMobilmoEvent_23() const { return ___EditMobilmoEvent_23; }
	inline String_t** get_address_of_EditMobilmoEvent_23() { return &___EditMobilmoEvent_23; }
	inline void set_EditMobilmoEvent_23(String_t* value)
	{
		___EditMobilmoEvent_23 = value;
		Il2CppCodeGenWriteBarrier(&___EditMobilmoEvent_23, value);
	}

	inline static int32_t get_offset_of_DeleteMobilmoEvent_24() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___DeleteMobilmoEvent_24)); }
	inline String_t* get_DeleteMobilmoEvent_24() const { return ___DeleteMobilmoEvent_24; }
	inline String_t** get_address_of_DeleteMobilmoEvent_24() { return &___DeleteMobilmoEvent_24; }
	inline void set_DeleteMobilmoEvent_24(String_t* value)
	{
		___DeleteMobilmoEvent_24 = value;
		Il2CppCodeGenWriteBarrier(&___DeleteMobilmoEvent_24, value);
	}

	inline static int32_t get_offset_of_MoveToNewsEvent_25() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToNewsEvent_25)); }
	inline String_t* get_MoveToNewsEvent_25() const { return ___MoveToNewsEvent_25; }
	inline String_t** get_address_of_MoveToNewsEvent_25() { return &___MoveToNewsEvent_25; }
	inline void set_MoveToNewsEvent_25(String_t* value)
	{
		___MoveToNewsEvent_25 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToNewsEvent_25, value);
	}

	inline static int32_t get_offset_of_MoveToBilmoRaderEvent_26() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToBilmoRaderEvent_26)); }
	inline String_t* get_MoveToBilmoRaderEvent_26() const { return ___MoveToBilmoRaderEvent_26; }
	inline String_t** get_address_of_MoveToBilmoRaderEvent_26() { return &___MoveToBilmoRaderEvent_26; }
	inline void set_MoveToBilmoRaderEvent_26(String_t* value)
	{
		___MoveToBilmoRaderEvent_26 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToBilmoRaderEvent_26, value);
	}

	inline static int32_t get_offset_of_BackToMyPageEvent_27() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___BackToMyPageEvent_27)); }
	inline String_t* get_BackToMyPageEvent_27() const { return ___BackToMyPageEvent_27; }
	inline String_t** get_address_of_BackToMyPageEvent_27() { return &___BackToMyPageEvent_27; }
	inline void set_BackToMyPageEvent_27(String_t* value)
	{
		___BackToMyPageEvent_27 = value;
		Il2CppCodeGenWriteBarrier(&___BackToMyPageEvent_27, value);
	}

	inline static int32_t get_offset_of_MoveToOtherEvent_28() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToOtherEvent_28)); }
	inline String_t* get_MoveToOtherEvent_28() const { return ___MoveToOtherEvent_28; }
	inline String_t** get_address_of_MoveToOtherEvent_28() { return &___MoveToOtherEvent_28; }
	inline void set_MoveToOtherEvent_28(String_t* value)
	{
		___MoveToOtherEvent_28 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToOtherEvent_28, value);
	}

	inline static int32_t get_offset_of_MoveToSearchPlayer_29() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MoveToSearchPlayer_29)); }
	inline String_t* get_MoveToSearchPlayer_29() const { return ___MoveToSearchPlayer_29; }
	inline String_t** get_address_of_MoveToSearchPlayer_29() { return &___MoveToSearchPlayer_29; }
	inline void set_MoveToSearchPlayer_29(String_t* value)
	{
		___MoveToSearchPlayer_29 = value;
		Il2CppCodeGenWriteBarrier(&___MoveToSearchPlayer_29, value);
	}

	inline static int32_t get_offset_of_GetModuleEvent_30() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___GetModuleEvent_30)); }
	inline String_t* get_GetModuleEvent_30() const { return ___GetModuleEvent_30; }
	inline String_t** get_address_of_GetModuleEvent_30() { return &___GetModuleEvent_30; }
	inline void set_GetModuleEvent_30(String_t* value)
	{
		___GetModuleEvent_30 = value;
		Il2CppCodeGenWriteBarrier(&___GetModuleEvent_30, value);
	}

	inline static int32_t get_offset_of_RemakeModule_31() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___RemakeModule_31)); }
	inline String_t* get_RemakeModule_31() const { return ___RemakeModule_31; }
	inline String_t** get_address_of_RemakeModule_31() { return &___RemakeModule_31; }
	inline void set_RemakeModule_31(String_t* value)
	{
		___RemakeModule_31 = value;
		Il2CppCodeGenWriteBarrier(&___RemakeModule_31, value);
	}

	inline static int32_t get_offset_of_PrizedEventCount_32() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___PrizedEventCount_32)); }
	inline String_t* get_PrizedEventCount_32() const { return ___PrizedEventCount_32; }
	inline String_t** get_address_of_PrizedEventCount_32() { return &___PrizedEventCount_32; }
	inline void set_PrizedEventCount_32(String_t* value)
	{
		___PrizedEventCount_32 = value;
		Il2CppCodeGenWriteBarrier(&___PrizedEventCount_32, value);
	}

	inline static int32_t get_offset_of_UserRankUpCount_33() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___UserRankUpCount_33)); }
	inline String_t* get_UserRankUpCount_33() const { return ___UserRankUpCount_33; }
	inline String_t** get_address_of_UserRankUpCount_33() { return &___UserRankUpCount_33; }
	inline void set_UserRankUpCount_33(String_t* value)
	{
		___UserRankUpCount_33 = value;
		Il2CppCodeGenWriteBarrier(&___UserRankUpCount_33, value);
	}

	inline static int32_t get_offset_of_ReactionCount_34() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___ReactionCount_34)); }
	inline String_t* get_ReactionCount_34() const { return ___ReactionCount_34; }
	inline String_t** get_address_of_ReactionCount_34() { return &___ReactionCount_34; }
	inline void set_ReactionCount_34(String_t* value)
	{
		___ReactionCount_34 = value;
		Il2CppCodeGenWriteBarrier(&___ReactionCount_34, value);
	}

	inline static int32_t get_offset_of_TutorialFinishCount_35() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___TutorialFinishCount_35)); }
	inline String_t* get_TutorialFinishCount_35() const { return ___TutorialFinishCount_35; }
	inline String_t** get_address_of_TutorialFinishCount_35() { return &___TutorialFinishCount_35; }
	inline void set_TutorialFinishCount_35(String_t* value)
	{
		___TutorialFinishCount_35 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialFinishCount_35, value);
	}

	inline static int32_t get_offset_of_MyPageCount_36() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___MyPageCount_36)); }
	inline String_t* get_MyPageCount_36() const { return ___MyPageCount_36; }
	inline String_t** get_address_of_MyPageCount_36() { return &___MyPageCount_36; }
	inline void set_MyPageCount_36(String_t* value)
	{
		___MyPageCount_36 = value;
		Il2CppCodeGenWriteBarrier(&___MyPageCount_36, value);
	}

	inline static int32_t get_offset_of_RecordFootPrint_37() { return static_cast<int32_t>(offsetof(ConstantManager_t2577827963_StaticFields, ___RecordFootPrint_37)); }
	inline String_t* get_RecordFootPrint_37() const { return ___RecordFootPrint_37; }
	inline String_t** get_address_of_RecordFootPrint_37() { return &___RecordFootPrint_37; }
	inline void set_RecordFootPrint_37(String_t* value)
	{
		___RecordFootPrint_37 = value;
		Il2CppCodeGenWriteBarrier(&___RecordFootPrint_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
