﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ContactField[]
struct ContactFieldU5BU5D_t649129805;
// Toast
struct Toast_t3649705739;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactDialog
struct  ContactDialog_t103082716  : public MonoBehaviour_t1158329972
{
public:
	// ContactField[] ContactDialog::Fields
	ContactFieldU5BU5D_t649129805* ___Fields_2;
	// Toast ContactDialog::SendToToast
	Toast_t3649705739 * ___SendToToast_3;
	// UnityEngine.UI.Text ContactDialog::userNameText
	Text_t356221433 * ___userNameText_4;

public:
	inline static int32_t get_offset_of_Fields_2() { return static_cast<int32_t>(offsetof(ContactDialog_t103082716, ___Fields_2)); }
	inline ContactFieldU5BU5D_t649129805* get_Fields_2() const { return ___Fields_2; }
	inline ContactFieldU5BU5D_t649129805** get_address_of_Fields_2() { return &___Fields_2; }
	inline void set_Fields_2(ContactFieldU5BU5D_t649129805* value)
	{
		___Fields_2 = value;
		Il2CppCodeGenWriteBarrier(&___Fields_2, value);
	}

	inline static int32_t get_offset_of_SendToToast_3() { return static_cast<int32_t>(offsetof(ContactDialog_t103082716, ___SendToToast_3)); }
	inline Toast_t3649705739 * get_SendToToast_3() const { return ___SendToToast_3; }
	inline Toast_t3649705739 ** get_address_of_SendToToast_3() { return &___SendToToast_3; }
	inline void set_SendToToast_3(Toast_t3649705739 * value)
	{
		___SendToToast_3 = value;
		Il2CppCodeGenWriteBarrier(&___SendToToast_3, value);
	}

	inline static int32_t get_offset_of_userNameText_4() { return static_cast<int32_t>(offsetof(ContactDialog_t103082716, ___userNameText_4)); }
	inline Text_t356221433 * get_userNameText_4() const { return ___userNameText_4; }
	inline Text_t356221433 ** get_address_of_userNameText_4() { return &___userNameText_4; }
	inline void set_userNameText_4(Text_t356221433 * value)
	{
		___userNameText_4 = value;
		Il2CppCodeGenWriteBarrier(&___userNameText_4, value);
	}
};

struct ContactDialog_t103082716_StaticFields
{
public:
	// System.Action ContactDialog::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(ContactDialog_t103082716_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
