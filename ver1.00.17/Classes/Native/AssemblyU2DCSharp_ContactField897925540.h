﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactField
struct  ContactField_t897925540  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField ContactField::InputField
	InputField_t1631627530 * ___InputField_2;
	// UnityEngine.GameObject ContactField::Pressed
	GameObject_t1756533147 * ___Pressed_3;
	// System.Boolean ContactField::IsSelected
	bool ___IsSelected_4;

public:
	inline static int32_t get_offset_of_InputField_2() { return static_cast<int32_t>(offsetof(ContactField_t897925540, ___InputField_2)); }
	inline InputField_t1631627530 * get_InputField_2() const { return ___InputField_2; }
	inline InputField_t1631627530 ** get_address_of_InputField_2() { return &___InputField_2; }
	inline void set_InputField_2(InputField_t1631627530 * value)
	{
		___InputField_2 = value;
		Il2CppCodeGenWriteBarrier(&___InputField_2, value);
	}

	inline static int32_t get_offset_of_Pressed_3() { return static_cast<int32_t>(offsetof(ContactField_t897925540, ___Pressed_3)); }
	inline GameObject_t1756533147 * get_Pressed_3() const { return ___Pressed_3; }
	inline GameObject_t1756533147 ** get_address_of_Pressed_3() { return &___Pressed_3; }
	inline void set_Pressed_3(GameObject_t1756533147 * value)
	{
		___Pressed_3 = value;
		Il2CppCodeGenWriteBarrier(&___Pressed_3, value);
	}

	inline static int32_t get_offset_of_IsSelected_4() { return static_cast<int32_t>(offsetof(ContactField_t897925540, ___IsSelected_4)); }
	inline bool get_IsSelected_4() const { return ___IsSelected_4; }
	inline bool* get_address_of_IsSelected_4() { return &___IsSelected_4; }
	inline void set_IsSelected_4(bool value)
	{
		___IsSelected_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
