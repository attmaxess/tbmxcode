﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactPanel
struct  ContactPanel_t2408947310  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform ContactPanel::Label
	RectTransform_t3349966182 * ___Label_2;
	// UnityEngine.RectTransform ContactPanel::ScrollPanel
	RectTransform_t3349966182 * ___ScrollPanel_3;
	// UnityEngine.RectTransform ContactPanel::Dialog
	RectTransform_t3349966182 * ___Dialog_4;
	// System.Boolean ContactPanel::IsScrolled
	bool ___IsScrolled_5;

public:
	inline static int32_t get_offset_of_Label_2() { return static_cast<int32_t>(offsetof(ContactPanel_t2408947310, ___Label_2)); }
	inline RectTransform_t3349966182 * get_Label_2() const { return ___Label_2; }
	inline RectTransform_t3349966182 ** get_address_of_Label_2() { return &___Label_2; }
	inline void set_Label_2(RectTransform_t3349966182 * value)
	{
		___Label_2 = value;
		Il2CppCodeGenWriteBarrier(&___Label_2, value);
	}

	inline static int32_t get_offset_of_ScrollPanel_3() { return static_cast<int32_t>(offsetof(ContactPanel_t2408947310, ___ScrollPanel_3)); }
	inline RectTransform_t3349966182 * get_ScrollPanel_3() const { return ___ScrollPanel_3; }
	inline RectTransform_t3349966182 ** get_address_of_ScrollPanel_3() { return &___ScrollPanel_3; }
	inline void set_ScrollPanel_3(RectTransform_t3349966182 * value)
	{
		___ScrollPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___ScrollPanel_3, value);
	}

	inline static int32_t get_offset_of_Dialog_4() { return static_cast<int32_t>(offsetof(ContactPanel_t2408947310, ___Dialog_4)); }
	inline RectTransform_t3349966182 * get_Dialog_4() const { return ___Dialog_4; }
	inline RectTransform_t3349966182 ** get_address_of_Dialog_4() { return &___Dialog_4; }
	inline void set_Dialog_4(RectTransform_t3349966182 * value)
	{
		___Dialog_4 = value;
		Il2CppCodeGenWriteBarrier(&___Dialog_4, value);
	}

	inline static int32_t get_offset_of_IsScrolled_5() { return static_cast<int32_t>(offsetof(ContactPanel_t2408947310, ___IsScrolled_5)); }
	inline bool get_IsScrolled_5() const { return ___IsScrolled_5; }
	inline bool* get_address_of_IsScrolled_5() { return &___IsScrolled_5; }
	inline void set_IsScrolled_5(bool value)
	{
		___IsScrolled_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
