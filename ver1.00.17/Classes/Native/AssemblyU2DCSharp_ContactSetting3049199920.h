﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactSetting
struct  ContactSetting_t3049199920  : public Il2CppObject
{
public:
	// System.String ContactSetting::Email
	String_t* ___Email_0;
	// System.String ContactSetting::NickName
	String_t* ___NickName_1;
	// System.String ContactSetting::Text
	String_t* ___Text_2;

public:
	inline static int32_t get_offset_of_Email_0() { return static_cast<int32_t>(offsetof(ContactSetting_t3049199920, ___Email_0)); }
	inline String_t* get_Email_0() const { return ___Email_0; }
	inline String_t** get_address_of_Email_0() { return &___Email_0; }
	inline void set_Email_0(String_t* value)
	{
		___Email_0 = value;
		Il2CppCodeGenWriteBarrier(&___Email_0, value);
	}

	inline static int32_t get_offset_of_NickName_1() { return static_cast<int32_t>(offsetof(ContactSetting_t3049199920, ___NickName_1)); }
	inline String_t* get_NickName_1() const { return ___NickName_1; }
	inline String_t** get_address_of_NickName_1() { return &___NickName_1; }
	inline void set_NickName_1(String_t* value)
	{
		___NickName_1 = value;
		Il2CppCodeGenWriteBarrier(&___NickName_1, value);
	}

	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(ContactSetting_t3049199920, ___Text_2)); }
	inline String_t* get_Text_2() const { return ___Text_2; }
	inline String_t** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(String_t* value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
