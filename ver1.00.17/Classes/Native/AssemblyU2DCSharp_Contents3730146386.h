﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Contents
struct  Contents_t3730146386  : public Il2CppObject
{
public:
	// UnityEngine.GameObject Contents::List
	GameObject_t1756533147 * ___List_0;
	// UnityEngine.GameObject Contents::Set
	GameObject_t1756533147 * ___Set_1;

public:
	inline static int32_t get_offset_of_List_0() { return static_cast<int32_t>(offsetof(Contents_t3730146386, ___List_0)); }
	inline GameObject_t1756533147 * get_List_0() const { return ___List_0; }
	inline GameObject_t1756533147 ** get_address_of_List_0() { return &___List_0; }
	inline void set_List_0(GameObject_t1756533147 * value)
	{
		___List_0 = value;
		Il2CppCodeGenWriteBarrier(&___List_0, value);
	}

	inline static int32_t get_offset_of_Set_1() { return static_cast<int32_t>(offsetof(Contents_t3730146386, ___Set_1)); }
	inline GameObject_t1756533147 * get_Set_1() const { return ___Set_1; }
	inline GameObject_t1756533147 ** get_address_of_Set_1() { return &___Set_1; }
	inline void set_Set_1(GameObject_t1756533147 * value)
	{
		___Set_1 = value;
		Il2CppCodeGenWriteBarrier(&___Set_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
