﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoreContent
struct  CoreContent_t4281623508  : public Il2CppObject
{
public:
	// UnityEngine.GameObject CoreContent::Prefab
	GameObject_t1756533147 * ___Prefab_0;

public:
	inline static int32_t get_offset_of_Prefab_0() { return static_cast<int32_t>(offsetof(CoreContent_t4281623508, ___Prefab_0)); }
	inline GameObject_t1756533147 * get_Prefab_0() const { return ___Prefab_0; }
	inline GameObject_t1756533147 ** get_address_of_Prefab_0() { return &___Prefab_0; }
	inline void set_Prefab_0(GameObject_t1756533147 * value)
	{
		___Prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___Prefab_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
