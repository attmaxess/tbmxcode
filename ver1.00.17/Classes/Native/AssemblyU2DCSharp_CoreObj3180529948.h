﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// CoreContent
struct CoreContent_t4281623508;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoreObj
struct  CoreObj_t3180529948  : public Il2CppObject
{
public:
	// CoreContent CoreObj::coreInfo
	CoreContent_t4281623508 * ___coreInfo_0;
	// UnityEngine.GameObject CoreObj::Obj
	GameObject_t1756533147 * ___Obj_1;

public:
	inline static int32_t get_offset_of_coreInfo_0() { return static_cast<int32_t>(offsetof(CoreObj_t3180529948, ___coreInfo_0)); }
	inline CoreContent_t4281623508 * get_coreInfo_0() const { return ___coreInfo_0; }
	inline CoreContent_t4281623508 ** get_address_of_coreInfo_0() { return &___coreInfo_0; }
	inline void set_coreInfo_0(CoreContent_t4281623508 * value)
	{
		___coreInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___coreInfo_0, value);
	}

	inline static int32_t get_offset_of_Obj_1() { return static_cast<int32_t>(offsetof(CoreObj_t3180529948, ___Obj_1)); }
	inline GameObject_t1756533147 * get_Obj_1() const { return ___Obj_1; }
	inline GameObject_t1756533147 ** get_address_of_Obj_1() { return &___Obj_1; }
	inline void set_Obj_1(GameObject_t1756533147 * value)
	{
		___Obj_1 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
