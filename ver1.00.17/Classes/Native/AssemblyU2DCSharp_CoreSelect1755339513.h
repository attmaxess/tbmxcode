﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// CoreContent[]
struct CoreContentU5BU5D_t637898333;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// CoreObj
struct CoreObj_t3180529948;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoreSelect
struct  CoreSelect_t1755339513  : public MonoBehaviour_t1158329972
{
public:
	// CoreContent[] CoreSelect::selectCores
	CoreContentU5BU5D_t637898333* ___selectCores_2;
	// UnityEngine.GameObject CoreSelect::ModelingPanel
	GameObject_t1756533147 * ___ModelingPanel_3;
	// UnityEngine.UI.Image CoreSelect::leftSelect
	Image_t2042527209 * ___leftSelect_4;
	// UnityEngine.UI.Image CoreSelect::rightSelect
	Image_t2042527209 * ___rightSelect_5;
	// CoreObj CoreSelect::preprevObj
	CoreObj_t3180529948 * ___preprevObj_6;
	// CoreObj CoreSelect::prevObj
	CoreObj_t3180529948 * ___prevObj_7;
	// CoreObj CoreSelect::nowObj
	CoreObj_t3180529948 * ___nowObj_8;
	// CoreObj CoreSelect::nextObj
	CoreObj_t3180529948 * ___nextObj_9;
	// CoreObj CoreSelect::nexnextObj
	CoreObj_t3180529948 * ___nexnextObj_10;
	// System.Int32 CoreSelect::selectNum
	int32_t ___selectNum_11;
	// UnityEngine.Vector3 CoreSelect::vPosNow
	Vector3_t2243707580  ___vPosNow_12;
	// UnityEngine.Vector3 CoreSelect::vPosNext
	Vector3_t2243707580  ___vPosNext_13;
	// UnityEngine.Vector3 CoreSelect::vPosPrev
	Vector3_t2243707580  ___vPosPrev_14;
	// UnityEngine.Vector3 CoreSelect::vPosAfterNext
	Vector3_t2243707580  ___vPosAfterNext_15;
	// UnityEngine.Vector3 CoreSelect::vPosBeforePrev
	Vector3_t2243707580  ___vPosBeforePrev_16;

public:
	inline static int32_t get_offset_of_selectCores_2() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___selectCores_2)); }
	inline CoreContentU5BU5D_t637898333* get_selectCores_2() const { return ___selectCores_2; }
	inline CoreContentU5BU5D_t637898333** get_address_of_selectCores_2() { return &___selectCores_2; }
	inline void set_selectCores_2(CoreContentU5BU5D_t637898333* value)
	{
		___selectCores_2 = value;
		Il2CppCodeGenWriteBarrier(&___selectCores_2, value);
	}

	inline static int32_t get_offset_of_ModelingPanel_3() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___ModelingPanel_3)); }
	inline GameObject_t1756533147 * get_ModelingPanel_3() const { return ___ModelingPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_ModelingPanel_3() { return &___ModelingPanel_3; }
	inline void set_ModelingPanel_3(GameObject_t1756533147 * value)
	{
		___ModelingPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___ModelingPanel_3, value);
	}

	inline static int32_t get_offset_of_leftSelect_4() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___leftSelect_4)); }
	inline Image_t2042527209 * get_leftSelect_4() const { return ___leftSelect_4; }
	inline Image_t2042527209 ** get_address_of_leftSelect_4() { return &___leftSelect_4; }
	inline void set_leftSelect_4(Image_t2042527209 * value)
	{
		___leftSelect_4 = value;
		Il2CppCodeGenWriteBarrier(&___leftSelect_4, value);
	}

	inline static int32_t get_offset_of_rightSelect_5() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___rightSelect_5)); }
	inline Image_t2042527209 * get_rightSelect_5() const { return ___rightSelect_5; }
	inline Image_t2042527209 ** get_address_of_rightSelect_5() { return &___rightSelect_5; }
	inline void set_rightSelect_5(Image_t2042527209 * value)
	{
		___rightSelect_5 = value;
		Il2CppCodeGenWriteBarrier(&___rightSelect_5, value);
	}

	inline static int32_t get_offset_of_preprevObj_6() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___preprevObj_6)); }
	inline CoreObj_t3180529948 * get_preprevObj_6() const { return ___preprevObj_6; }
	inline CoreObj_t3180529948 ** get_address_of_preprevObj_6() { return &___preprevObj_6; }
	inline void set_preprevObj_6(CoreObj_t3180529948 * value)
	{
		___preprevObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___preprevObj_6, value);
	}

	inline static int32_t get_offset_of_prevObj_7() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___prevObj_7)); }
	inline CoreObj_t3180529948 * get_prevObj_7() const { return ___prevObj_7; }
	inline CoreObj_t3180529948 ** get_address_of_prevObj_7() { return &___prevObj_7; }
	inline void set_prevObj_7(CoreObj_t3180529948 * value)
	{
		___prevObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___prevObj_7, value);
	}

	inline static int32_t get_offset_of_nowObj_8() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___nowObj_8)); }
	inline CoreObj_t3180529948 * get_nowObj_8() const { return ___nowObj_8; }
	inline CoreObj_t3180529948 ** get_address_of_nowObj_8() { return &___nowObj_8; }
	inline void set_nowObj_8(CoreObj_t3180529948 * value)
	{
		___nowObj_8 = value;
		Il2CppCodeGenWriteBarrier(&___nowObj_8, value);
	}

	inline static int32_t get_offset_of_nextObj_9() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___nextObj_9)); }
	inline CoreObj_t3180529948 * get_nextObj_9() const { return ___nextObj_9; }
	inline CoreObj_t3180529948 ** get_address_of_nextObj_9() { return &___nextObj_9; }
	inline void set_nextObj_9(CoreObj_t3180529948 * value)
	{
		___nextObj_9 = value;
		Il2CppCodeGenWriteBarrier(&___nextObj_9, value);
	}

	inline static int32_t get_offset_of_nexnextObj_10() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___nexnextObj_10)); }
	inline CoreObj_t3180529948 * get_nexnextObj_10() const { return ___nexnextObj_10; }
	inline CoreObj_t3180529948 ** get_address_of_nexnextObj_10() { return &___nexnextObj_10; }
	inline void set_nexnextObj_10(CoreObj_t3180529948 * value)
	{
		___nexnextObj_10 = value;
		Il2CppCodeGenWriteBarrier(&___nexnextObj_10, value);
	}

	inline static int32_t get_offset_of_selectNum_11() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___selectNum_11)); }
	inline int32_t get_selectNum_11() const { return ___selectNum_11; }
	inline int32_t* get_address_of_selectNum_11() { return &___selectNum_11; }
	inline void set_selectNum_11(int32_t value)
	{
		___selectNum_11 = value;
	}

	inline static int32_t get_offset_of_vPosNow_12() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___vPosNow_12)); }
	inline Vector3_t2243707580  get_vPosNow_12() const { return ___vPosNow_12; }
	inline Vector3_t2243707580 * get_address_of_vPosNow_12() { return &___vPosNow_12; }
	inline void set_vPosNow_12(Vector3_t2243707580  value)
	{
		___vPosNow_12 = value;
	}

	inline static int32_t get_offset_of_vPosNext_13() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___vPosNext_13)); }
	inline Vector3_t2243707580  get_vPosNext_13() const { return ___vPosNext_13; }
	inline Vector3_t2243707580 * get_address_of_vPosNext_13() { return &___vPosNext_13; }
	inline void set_vPosNext_13(Vector3_t2243707580  value)
	{
		___vPosNext_13 = value;
	}

	inline static int32_t get_offset_of_vPosPrev_14() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___vPosPrev_14)); }
	inline Vector3_t2243707580  get_vPosPrev_14() const { return ___vPosPrev_14; }
	inline Vector3_t2243707580 * get_address_of_vPosPrev_14() { return &___vPosPrev_14; }
	inline void set_vPosPrev_14(Vector3_t2243707580  value)
	{
		___vPosPrev_14 = value;
	}

	inline static int32_t get_offset_of_vPosAfterNext_15() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___vPosAfterNext_15)); }
	inline Vector3_t2243707580  get_vPosAfterNext_15() const { return ___vPosAfterNext_15; }
	inline Vector3_t2243707580 * get_address_of_vPosAfterNext_15() { return &___vPosAfterNext_15; }
	inline void set_vPosAfterNext_15(Vector3_t2243707580  value)
	{
		___vPosAfterNext_15 = value;
	}

	inline static int32_t get_offset_of_vPosBeforePrev_16() { return static_cast<int32_t>(offsetof(CoreSelect_t1755339513, ___vPosBeforePrev_16)); }
	inline Vector3_t2243707580  get_vPosBeforePrev_16() const { return ___vPosBeforePrev_16; }
	inline Vector3_t2243707580 * get_address_of_vPosBeforePrev_16() { return &___vPosBeforePrev_16; }
	inline void set_vPosBeforePrev_16(Vector3_t2243707580  value)
	{
		___vPosBeforePrev_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
