﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateGrid
struct  CreateGrid_t2336839420  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CreateGrid::model
	GameObject_t1756533147 * ___model_2;

public:
	inline static int32_t get_offset_of_model_2() { return static_cast<int32_t>(offsetof(CreateGrid_t2336839420, ___model_2)); }
	inline GameObject_t1756533147 * get_model_2() const { return ___model_2; }
	inline GameObject_t1756533147 ** get_address_of_model_2() { return &___model_2; }
	inline void set_model_2(GameObject_t1756533147 * value)
	{
		___model_2 = value;
		Il2CppCodeGenWriteBarrier(&___model_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
