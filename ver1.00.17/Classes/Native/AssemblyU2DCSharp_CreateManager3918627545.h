﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2615303880.h"
#include "AssemblyU2DCSharp_eCreateType1183446867.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// PartsConfig[]
struct PartsConfigU5BU5D_t3174870649;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t578636151;
// Module
struct Module_t3140434828;
// MobilimoPartsConect
struct MobilimoPartsConect_t3466715044;
// ModulePartsConect
struct ModulePartsConect_t3833785310;
// Parts
struct Parts_t3804168686;
// RotationController
struct RotationController_t3824016438;
// ColorManager
struct ColorManager_t1666568646;
// MobilmoPanel
struct MobilmoPanel_t222294755;
// ModulePanel
struct ModulePanel_t1015180434;
// SubCameraHandler
struct SubCameraHandler_t2753085375;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,DG.Tweening.Tween>
struct Dictionary_2_t3608823457;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateManager
struct  CreateManager_t3918627545  : public SingletonMonoBehaviour_1_t2615303880
{
public:
	// UnityEngine.Transform CreateManager::m_PartsArea
	Transform_t3275118058 * ___m_PartsArea_3;
	// UnityEngine.Transform CreateManager::m_CreateArea
	Transform_t3275118058 * ___m_CreateArea_4;
	// UnityEngine.Transform CreateManager::m_MobilityArea
	Transform_t3275118058 * ___m_MobilityArea_5;
	// UnityEngine.GameObject CreateManager::GridPrefab
	GameObject_t1756533147 * ___GridPrefab_6;
	// UnityEngine.Sprite CreateManager::SelectJoint
	Sprite_t309593783 * ___SelectJoint_7;
	// UnityEngine.Sprite CreateManager::NoSelectJoint
	Sprite_t309593783 * ___NoSelectJoint_8;
	// UnityEngine.LineRenderer CreateManager::JointLine
	LineRenderer_t849157671 * ___JointLine_9;
	// PartsConfig[] CreateManager::PartsConfigs
	PartsConfigU5BU5D_t3174870649* ___PartsConfigs_10;
	// UnityEngine.UI.Text CreateManager::CrePartsScaleText
	Text_t356221433 * ___CrePartsScaleText_11;
	// System.String[] CreateManager::corePartsName
	StringU5BU5D_t1642385972* ___corePartsName_12;
	// UnityEngine.PhysicMaterial CreateManager::MobilityPhysicMaterial
	PhysicMaterial_t578636151 * ___MobilityPhysicMaterial_13;
	// UnityEngine.GameObject CreateManager::Mobilmo
	GameObject_t1756533147 * ___Mobilmo_14;
	// UnityEngine.GameObject CreateManager::Module
	GameObject_t1756533147 * ___Module_15;
	// UnityEngine.GameObject CreateManager::MobilmoLimitErrorObj
	GameObject_t1756533147 * ___MobilmoLimitErrorObj_16;
	// UnityEngine.GameObject CreateManager::ModuleLimitErrorObj
	GameObject_t1756533147 * ___ModuleLimitErrorObj_17;
	// UnityEngine.GameObject CreateManager::m_ModelParent
	GameObject_t1756533147 * ___m_ModelParent_18;
	// UnityEngine.GameObject CreateManager::m_Core
	GameObject_t1756533147 * ___m_Core_19;
	// Module CreateManager::m_Module
	Module_t3140434828 * ___m_Module_20;
	// UnityEngine.GameObject CreateManager::CreObj
	GameObject_t1756533147 * ___CreObj_21;
	// UnityEngine.GameObject CreateManager::m_GridObj
	GameObject_t1756533147 * ___m_GridObj_22;
	// UnityEngine.GameObject CreateManager::m_SelectParts
	GameObject_t1756533147 * ___m_SelectParts_23;
	// eCreateType CreateManager::m_eCreateType
	int32_t ___m_eCreateType_24;
	// System.Int32 CreateManager::rollCount
	int32_t ___rollCount_25;
	// System.Int32 CreateManager::m_parentModNum
	int32_t ___m_parentModNum_26;
	// MobilimoPartsConect CreateManager::MobilmoConect
	MobilimoPartsConect_t3466715044 * ___MobilmoConect_27;
	// ModulePartsConect CreateManager::ModuleConect
	ModulePartsConect_t3833785310 * ___ModuleConect_28;
	// UnityEngine.Transform CreateManager::m_ClickJoint
	Transform_t3275118058 * ___m_ClickJoint_29;
	// Parts CreateManager::m_ParentParts
	Parts_t3804168686 * ___m_ParentParts_30;
	// System.Int32 CreateManager::m_sParentJointNum
	int32_t ___m_sParentJointNum_31;
	// Parts CreateManager::m_ChildParts
	Parts_t3804168686 * ___m_ChildParts_32;
	// System.Int32 CreateManager::m_sChildJointNum
	int32_t ___m_sChildJointNum_33;
	// System.Int32 CreateManager::m_sPartsNo
	int32_t ___m_sPartsNo_34;
	// RotationController CreateManager::m_RotateInput
	RotationController_t3824016438 * ___m_RotateInput_35;
	// ColorManager CreateManager::m_ColorMgr
	ColorManager_t1666568646 * ___m_ColorMgr_36;
	// MobilmoPanel CreateManager::m_MobilmoPanel
	MobilmoPanel_t222294755 * ___m_MobilmoPanel_37;
	// ModulePanel CreateManager::m_ModulePanel
	ModulePanel_t1015180434 * ___m_ModulePanel_38;
	// SubCameraHandler CreateManager::m_SubCameraHandler
	SubCameraHandler_t2753085375 * ___m_SubCameraHandler_39;
	// System.Int32 CreateManager::m_createModuleId
	int32_t ___m_createModuleId_40;
	// System.Int32 CreateManager::childPartsNo
	int32_t ___childPartsNo_41;
	// System.Int32 CreateManager::m_sTutorialStep
	int32_t ___m_sTutorialStep_42;
	// System.Int32 CreateManager::LimitOverNum
	int32_t ___LimitOverNum_44;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,DG.Tweening.Tween> CreateManager::FlashingTWDic
	Dictionary_2_t3608823457 * ___FlashingTWDic_45;
	// System.Boolean CreateManager::isEditing
	bool ___isEditing_46;
	// UnityEngine.Transform CreateManager::DragSelectTran
	Transform_t3275118058 * ___DragSelectTran_47;
	// System.Int32 CreateManager::modCnt
	int32_t ___modCnt_48;
	// System.Int32 CreateManager::m_parentMoudleId
	int32_t ___m_parentMoudleId_49;
	// System.Collections.Generic.List`1<System.String> CreateManager::_keys
	List_1_t1398341365 * ____keys_50;
	// System.Int32 CreateManager::moduleCnt
	int32_t ___moduleCnt_51;

public:
	inline static int32_t get_offset_of_m_PartsArea_3() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_PartsArea_3)); }
	inline Transform_t3275118058 * get_m_PartsArea_3() const { return ___m_PartsArea_3; }
	inline Transform_t3275118058 ** get_address_of_m_PartsArea_3() { return &___m_PartsArea_3; }
	inline void set_m_PartsArea_3(Transform_t3275118058 * value)
	{
		___m_PartsArea_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_PartsArea_3, value);
	}

	inline static int32_t get_offset_of_m_CreateArea_4() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_CreateArea_4)); }
	inline Transform_t3275118058 * get_m_CreateArea_4() const { return ___m_CreateArea_4; }
	inline Transform_t3275118058 ** get_address_of_m_CreateArea_4() { return &___m_CreateArea_4; }
	inline void set_m_CreateArea_4(Transform_t3275118058 * value)
	{
		___m_CreateArea_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_CreateArea_4, value);
	}

	inline static int32_t get_offset_of_m_MobilityArea_5() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_MobilityArea_5)); }
	inline Transform_t3275118058 * get_m_MobilityArea_5() const { return ___m_MobilityArea_5; }
	inline Transform_t3275118058 ** get_address_of_m_MobilityArea_5() { return &___m_MobilityArea_5; }
	inline void set_m_MobilityArea_5(Transform_t3275118058 * value)
	{
		___m_MobilityArea_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_MobilityArea_5, value);
	}

	inline static int32_t get_offset_of_GridPrefab_6() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___GridPrefab_6)); }
	inline GameObject_t1756533147 * get_GridPrefab_6() const { return ___GridPrefab_6; }
	inline GameObject_t1756533147 ** get_address_of_GridPrefab_6() { return &___GridPrefab_6; }
	inline void set_GridPrefab_6(GameObject_t1756533147 * value)
	{
		___GridPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___GridPrefab_6, value);
	}

	inline static int32_t get_offset_of_SelectJoint_7() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___SelectJoint_7)); }
	inline Sprite_t309593783 * get_SelectJoint_7() const { return ___SelectJoint_7; }
	inline Sprite_t309593783 ** get_address_of_SelectJoint_7() { return &___SelectJoint_7; }
	inline void set_SelectJoint_7(Sprite_t309593783 * value)
	{
		___SelectJoint_7 = value;
		Il2CppCodeGenWriteBarrier(&___SelectJoint_7, value);
	}

	inline static int32_t get_offset_of_NoSelectJoint_8() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___NoSelectJoint_8)); }
	inline Sprite_t309593783 * get_NoSelectJoint_8() const { return ___NoSelectJoint_8; }
	inline Sprite_t309593783 ** get_address_of_NoSelectJoint_8() { return &___NoSelectJoint_8; }
	inline void set_NoSelectJoint_8(Sprite_t309593783 * value)
	{
		___NoSelectJoint_8 = value;
		Il2CppCodeGenWriteBarrier(&___NoSelectJoint_8, value);
	}

	inline static int32_t get_offset_of_JointLine_9() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___JointLine_9)); }
	inline LineRenderer_t849157671 * get_JointLine_9() const { return ___JointLine_9; }
	inline LineRenderer_t849157671 ** get_address_of_JointLine_9() { return &___JointLine_9; }
	inline void set_JointLine_9(LineRenderer_t849157671 * value)
	{
		___JointLine_9 = value;
		Il2CppCodeGenWriteBarrier(&___JointLine_9, value);
	}

	inline static int32_t get_offset_of_PartsConfigs_10() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___PartsConfigs_10)); }
	inline PartsConfigU5BU5D_t3174870649* get_PartsConfigs_10() const { return ___PartsConfigs_10; }
	inline PartsConfigU5BU5D_t3174870649** get_address_of_PartsConfigs_10() { return &___PartsConfigs_10; }
	inline void set_PartsConfigs_10(PartsConfigU5BU5D_t3174870649* value)
	{
		___PartsConfigs_10 = value;
		Il2CppCodeGenWriteBarrier(&___PartsConfigs_10, value);
	}

	inline static int32_t get_offset_of_CrePartsScaleText_11() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___CrePartsScaleText_11)); }
	inline Text_t356221433 * get_CrePartsScaleText_11() const { return ___CrePartsScaleText_11; }
	inline Text_t356221433 ** get_address_of_CrePartsScaleText_11() { return &___CrePartsScaleText_11; }
	inline void set_CrePartsScaleText_11(Text_t356221433 * value)
	{
		___CrePartsScaleText_11 = value;
		Il2CppCodeGenWriteBarrier(&___CrePartsScaleText_11, value);
	}

	inline static int32_t get_offset_of_corePartsName_12() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___corePartsName_12)); }
	inline StringU5BU5D_t1642385972* get_corePartsName_12() const { return ___corePartsName_12; }
	inline StringU5BU5D_t1642385972** get_address_of_corePartsName_12() { return &___corePartsName_12; }
	inline void set_corePartsName_12(StringU5BU5D_t1642385972* value)
	{
		___corePartsName_12 = value;
		Il2CppCodeGenWriteBarrier(&___corePartsName_12, value);
	}

	inline static int32_t get_offset_of_MobilityPhysicMaterial_13() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___MobilityPhysicMaterial_13)); }
	inline PhysicMaterial_t578636151 * get_MobilityPhysicMaterial_13() const { return ___MobilityPhysicMaterial_13; }
	inline PhysicMaterial_t578636151 ** get_address_of_MobilityPhysicMaterial_13() { return &___MobilityPhysicMaterial_13; }
	inline void set_MobilityPhysicMaterial_13(PhysicMaterial_t578636151 * value)
	{
		___MobilityPhysicMaterial_13 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityPhysicMaterial_13, value);
	}

	inline static int32_t get_offset_of_Mobilmo_14() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___Mobilmo_14)); }
	inline GameObject_t1756533147 * get_Mobilmo_14() const { return ___Mobilmo_14; }
	inline GameObject_t1756533147 ** get_address_of_Mobilmo_14() { return &___Mobilmo_14; }
	inline void set_Mobilmo_14(GameObject_t1756533147 * value)
	{
		___Mobilmo_14 = value;
		Il2CppCodeGenWriteBarrier(&___Mobilmo_14, value);
	}

	inline static int32_t get_offset_of_Module_15() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___Module_15)); }
	inline GameObject_t1756533147 * get_Module_15() const { return ___Module_15; }
	inline GameObject_t1756533147 ** get_address_of_Module_15() { return &___Module_15; }
	inline void set_Module_15(GameObject_t1756533147 * value)
	{
		___Module_15 = value;
		Il2CppCodeGenWriteBarrier(&___Module_15, value);
	}

	inline static int32_t get_offset_of_MobilmoLimitErrorObj_16() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___MobilmoLimitErrorObj_16)); }
	inline GameObject_t1756533147 * get_MobilmoLimitErrorObj_16() const { return ___MobilmoLimitErrorObj_16; }
	inline GameObject_t1756533147 ** get_address_of_MobilmoLimitErrorObj_16() { return &___MobilmoLimitErrorObj_16; }
	inline void set_MobilmoLimitErrorObj_16(GameObject_t1756533147 * value)
	{
		___MobilmoLimitErrorObj_16 = value;
		Il2CppCodeGenWriteBarrier(&___MobilmoLimitErrorObj_16, value);
	}

	inline static int32_t get_offset_of_ModuleLimitErrorObj_17() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___ModuleLimitErrorObj_17)); }
	inline GameObject_t1756533147 * get_ModuleLimitErrorObj_17() const { return ___ModuleLimitErrorObj_17; }
	inline GameObject_t1756533147 ** get_address_of_ModuleLimitErrorObj_17() { return &___ModuleLimitErrorObj_17; }
	inline void set_ModuleLimitErrorObj_17(GameObject_t1756533147 * value)
	{
		___ModuleLimitErrorObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___ModuleLimitErrorObj_17, value);
	}

	inline static int32_t get_offset_of_m_ModelParent_18() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_ModelParent_18)); }
	inline GameObject_t1756533147 * get_m_ModelParent_18() const { return ___m_ModelParent_18; }
	inline GameObject_t1756533147 ** get_address_of_m_ModelParent_18() { return &___m_ModelParent_18; }
	inline void set_m_ModelParent_18(GameObject_t1756533147 * value)
	{
		___m_ModelParent_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_ModelParent_18, value);
	}

	inline static int32_t get_offset_of_m_Core_19() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_Core_19)); }
	inline GameObject_t1756533147 * get_m_Core_19() const { return ___m_Core_19; }
	inline GameObject_t1756533147 ** get_address_of_m_Core_19() { return &___m_Core_19; }
	inline void set_m_Core_19(GameObject_t1756533147 * value)
	{
		___m_Core_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_Core_19, value);
	}

	inline static int32_t get_offset_of_m_Module_20() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_Module_20)); }
	inline Module_t3140434828 * get_m_Module_20() const { return ___m_Module_20; }
	inline Module_t3140434828 ** get_address_of_m_Module_20() { return &___m_Module_20; }
	inline void set_m_Module_20(Module_t3140434828 * value)
	{
		___m_Module_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_Module_20, value);
	}

	inline static int32_t get_offset_of_CreObj_21() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___CreObj_21)); }
	inline GameObject_t1756533147 * get_CreObj_21() const { return ___CreObj_21; }
	inline GameObject_t1756533147 ** get_address_of_CreObj_21() { return &___CreObj_21; }
	inline void set_CreObj_21(GameObject_t1756533147 * value)
	{
		___CreObj_21 = value;
		Il2CppCodeGenWriteBarrier(&___CreObj_21, value);
	}

	inline static int32_t get_offset_of_m_GridObj_22() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_GridObj_22)); }
	inline GameObject_t1756533147 * get_m_GridObj_22() const { return ___m_GridObj_22; }
	inline GameObject_t1756533147 ** get_address_of_m_GridObj_22() { return &___m_GridObj_22; }
	inline void set_m_GridObj_22(GameObject_t1756533147 * value)
	{
		___m_GridObj_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_GridObj_22, value);
	}

	inline static int32_t get_offset_of_m_SelectParts_23() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_SelectParts_23)); }
	inline GameObject_t1756533147 * get_m_SelectParts_23() const { return ___m_SelectParts_23; }
	inline GameObject_t1756533147 ** get_address_of_m_SelectParts_23() { return &___m_SelectParts_23; }
	inline void set_m_SelectParts_23(GameObject_t1756533147 * value)
	{
		___m_SelectParts_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_SelectParts_23, value);
	}

	inline static int32_t get_offset_of_m_eCreateType_24() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_eCreateType_24)); }
	inline int32_t get_m_eCreateType_24() const { return ___m_eCreateType_24; }
	inline int32_t* get_address_of_m_eCreateType_24() { return &___m_eCreateType_24; }
	inline void set_m_eCreateType_24(int32_t value)
	{
		___m_eCreateType_24 = value;
	}

	inline static int32_t get_offset_of_rollCount_25() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___rollCount_25)); }
	inline int32_t get_rollCount_25() const { return ___rollCount_25; }
	inline int32_t* get_address_of_rollCount_25() { return &___rollCount_25; }
	inline void set_rollCount_25(int32_t value)
	{
		___rollCount_25 = value;
	}

	inline static int32_t get_offset_of_m_parentModNum_26() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_parentModNum_26)); }
	inline int32_t get_m_parentModNum_26() const { return ___m_parentModNum_26; }
	inline int32_t* get_address_of_m_parentModNum_26() { return &___m_parentModNum_26; }
	inline void set_m_parentModNum_26(int32_t value)
	{
		___m_parentModNum_26 = value;
	}

	inline static int32_t get_offset_of_MobilmoConect_27() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___MobilmoConect_27)); }
	inline MobilimoPartsConect_t3466715044 * get_MobilmoConect_27() const { return ___MobilmoConect_27; }
	inline MobilimoPartsConect_t3466715044 ** get_address_of_MobilmoConect_27() { return &___MobilmoConect_27; }
	inline void set_MobilmoConect_27(MobilimoPartsConect_t3466715044 * value)
	{
		___MobilmoConect_27 = value;
		Il2CppCodeGenWriteBarrier(&___MobilmoConect_27, value);
	}

	inline static int32_t get_offset_of_ModuleConect_28() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___ModuleConect_28)); }
	inline ModulePartsConect_t3833785310 * get_ModuleConect_28() const { return ___ModuleConect_28; }
	inline ModulePartsConect_t3833785310 ** get_address_of_ModuleConect_28() { return &___ModuleConect_28; }
	inline void set_ModuleConect_28(ModulePartsConect_t3833785310 * value)
	{
		___ModuleConect_28 = value;
		Il2CppCodeGenWriteBarrier(&___ModuleConect_28, value);
	}

	inline static int32_t get_offset_of_m_ClickJoint_29() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_ClickJoint_29)); }
	inline Transform_t3275118058 * get_m_ClickJoint_29() const { return ___m_ClickJoint_29; }
	inline Transform_t3275118058 ** get_address_of_m_ClickJoint_29() { return &___m_ClickJoint_29; }
	inline void set_m_ClickJoint_29(Transform_t3275118058 * value)
	{
		___m_ClickJoint_29 = value;
		Il2CppCodeGenWriteBarrier(&___m_ClickJoint_29, value);
	}

	inline static int32_t get_offset_of_m_ParentParts_30() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_ParentParts_30)); }
	inline Parts_t3804168686 * get_m_ParentParts_30() const { return ___m_ParentParts_30; }
	inline Parts_t3804168686 ** get_address_of_m_ParentParts_30() { return &___m_ParentParts_30; }
	inline void set_m_ParentParts_30(Parts_t3804168686 * value)
	{
		___m_ParentParts_30 = value;
		Il2CppCodeGenWriteBarrier(&___m_ParentParts_30, value);
	}

	inline static int32_t get_offset_of_m_sParentJointNum_31() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_sParentJointNum_31)); }
	inline int32_t get_m_sParentJointNum_31() const { return ___m_sParentJointNum_31; }
	inline int32_t* get_address_of_m_sParentJointNum_31() { return &___m_sParentJointNum_31; }
	inline void set_m_sParentJointNum_31(int32_t value)
	{
		___m_sParentJointNum_31 = value;
	}

	inline static int32_t get_offset_of_m_ChildParts_32() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_ChildParts_32)); }
	inline Parts_t3804168686 * get_m_ChildParts_32() const { return ___m_ChildParts_32; }
	inline Parts_t3804168686 ** get_address_of_m_ChildParts_32() { return &___m_ChildParts_32; }
	inline void set_m_ChildParts_32(Parts_t3804168686 * value)
	{
		___m_ChildParts_32 = value;
		Il2CppCodeGenWriteBarrier(&___m_ChildParts_32, value);
	}

	inline static int32_t get_offset_of_m_sChildJointNum_33() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_sChildJointNum_33)); }
	inline int32_t get_m_sChildJointNum_33() const { return ___m_sChildJointNum_33; }
	inline int32_t* get_address_of_m_sChildJointNum_33() { return &___m_sChildJointNum_33; }
	inline void set_m_sChildJointNum_33(int32_t value)
	{
		___m_sChildJointNum_33 = value;
	}

	inline static int32_t get_offset_of_m_sPartsNo_34() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_sPartsNo_34)); }
	inline int32_t get_m_sPartsNo_34() const { return ___m_sPartsNo_34; }
	inline int32_t* get_address_of_m_sPartsNo_34() { return &___m_sPartsNo_34; }
	inline void set_m_sPartsNo_34(int32_t value)
	{
		___m_sPartsNo_34 = value;
	}

	inline static int32_t get_offset_of_m_RotateInput_35() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_RotateInput_35)); }
	inline RotationController_t3824016438 * get_m_RotateInput_35() const { return ___m_RotateInput_35; }
	inline RotationController_t3824016438 ** get_address_of_m_RotateInput_35() { return &___m_RotateInput_35; }
	inline void set_m_RotateInput_35(RotationController_t3824016438 * value)
	{
		___m_RotateInput_35 = value;
		Il2CppCodeGenWriteBarrier(&___m_RotateInput_35, value);
	}

	inline static int32_t get_offset_of_m_ColorMgr_36() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_ColorMgr_36)); }
	inline ColorManager_t1666568646 * get_m_ColorMgr_36() const { return ___m_ColorMgr_36; }
	inline ColorManager_t1666568646 ** get_address_of_m_ColorMgr_36() { return &___m_ColorMgr_36; }
	inline void set_m_ColorMgr_36(ColorManager_t1666568646 * value)
	{
		___m_ColorMgr_36 = value;
		Il2CppCodeGenWriteBarrier(&___m_ColorMgr_36, value);
	}

	inline static int32_t get_offset_of_m_MobilmoPanel_37() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_MobilmoPanel_37)); }
	inline MobilmoPanel_t222294755 * get_m_MobilmoPanel_37() const { return ___m_MobilmoPanel_37; }
	inline MobilmoPanel_t222294755 ** get_address_of_m_MobilmoPanel_37() { return &___m_MobilmoPanel_37; }
	inline void set_m_MobilmoPanel_37(MobilmoPanel_t222294755 * value)
	{
		___m_MobilmoPanel_37 = value;
		Il2CppCodeGenWriteBarrier(&___m_MobilmoPanel_37, value);
	}

	inline static int32_t get_offset_of_m_ModulePanel_38() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_ModulePanel_38)); }
	inline ModulePanel_t1015180434 * get_m_ModulePanel_38() const { return ___m_ModulePanel_38; }
	inline ModulePanel_t1015180434 ** get_address_of_m_ModulePanel_38() { return &___m_ModulePanel_38; }
	inline void set_m_ModulePanel_38(ModulePanel_t1015180434 * value)
	{
		___m_ModulePanel_38 = value;
		Il2CppCodeGenWriteBarrier(&___m_ModulePanel_38, value);
	}

	inline static int32_t get_offset_of_m_SubCameraHandler_39() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_SubCameraHandler_39)); }
	inline SubCameraHandler_t2753085375 * get_m_SubCameraHandler_39() const { return ___m_SubCameraHandler_39; }
	inline SubCameraHandler_t2753085375 ** get_address_of_m_SubCameraHandler_39() { return &___m_SubCameraHandler_39; }
	inline void set_m_SubCameraHandler_39(SubCameraHandler_t2753085375 * value)
	{
		___m_SubCameraHandler_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_SubCameraHandler_39, value);
	}

	inline static int32_t get_offset_of_m_createModuleId_40() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_createModuleId_40)); }
	inline int32_t get_m_createModuleId_40() const { return ___m_createModuleId_40; }
	inline int32_t* get_address_of_m_createModuleId_40() { return &___m_createModuleId_40; }
	inline void set_m_createModuleId_40(int32_t value)
	{
		___m_createModuleId_40 = value;
	}

	inline static int32_t get_offset_of_childPartsNo_41() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___childPartsNo_41)); }
	inline int32_t get_childPartsNo_41() const { return ___childPartsNo_41; }
	inline int32_t* get_address_of_childPartsNo_41() { return &___childPartsNo_41; }
	inline void set_childPartsNo_41(int32_t value)
	{
		___childPartsNo_41 = value;
	}

	inline static int32_t get_offset_of_m_sTutorialStep_42() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_sTutorialStep_42)); }
	inline int32_t get_m_sTutorialStep_42() const { return ___m_sTutorialStep_42; }
	inline int32_t* get_address_of_m_sTutorialStep_42() { return &___m_sTutorialStep_42; }
	inline void set_m_sTutorialStep_42(int32_t value)
	{
		___m_sTutorialStep_42 = value;
	}

	inline static int32_t get_offset_of_LimitOverNum_44() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___LimitOverNum_44)); }
	inline int32_t get_LimitOverNum_44() const { return ___LimitOverNum_44; }
	inline int32_t* get_address_of_LimitOverNum_44() { return &___LimitOverNum_44; }
	inline void set_LimitOverNum_44(int32_t value)
	{
		___LimitOverNum_44 = value;
	}

	inline static int32_t get_offset_of_FlashingTWDic_45() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___FlashingTWDic_45)); }
	inline Dictionary_2_t3608823457 * get_FlashingTWDic_45() const { return ___FlashingTWDic_45; }
	inline Dictionary_2_t3608823457 ** get_address_of_FlashingTWDic_45() { return &___FlashingTWDic_45; }
	inline void set_FlashingTWDic_45(Dictionary_2_t3608823457 * value)
	{
		___FlashingTWDic_45 = value;
		Il2CppCodeGenWriteBarrier(&___FlashingTWDic_45, value);
	}

	inline static int32_t get_offset_of_isEditing_46() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___isEditing_46)); }
	inline bool get_isEditing_46() const { return ___isEditing_46; }
	inline bool* get_address_of_isEditing_46() { return &___isEditing_46; }
	inline void set_isEditing_46(bool value)
	{
		___isEditing_46 = value;
	}

	inline static int32_t get_offset_of_DragSelectTran_47() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___DragSelectTran_47)); }
	inline Transform_t3275118058 * get_DragSelectTran_47() const { return ___DragSelectTran_47; }
	inline Transform_t3275118058 ** get_address_of_DragSelectTran_47() { return &___DragSelectTran_47; }
	inline void set_DragSelectTran_47(Transform_t3275118058 * value)
	{
		___DragSelectTran_47 = value;
		Il2CppCodeGenWriteBarrier(&___DragSelectTran_47, value);
	}

	inline static int32_t get_offset_of_modCnt_48() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___modCnt_48)); }
	inline int32_t get_modCnt_48() const { return ___modCnt_48; }
	inline int32_t* get_address_of_modCnt_48() { return &___modCnt_48; }
	inline void set_modCnt_48(int32_t value)
	{
		___modCnt_48 = value;
	}

	inline static int32_t get_offset_of_m_parentMoudleId_49() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___m_parentMoudleId_49)); }
	inline int32_t get_m_parentMoudleId_49() const { return ___m_parentMoudleId_49; }
	inline int32_t* get_address_of_m_parentMoudleId_49() { return &___m_parentMoudleId_49; }
	inline void set_m_parentMoudleId_49(int32_t value)
	{
		___m_parentMoudleId_49 = value;
	}

	inline static int32_t get_offset_of__keys_50() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ____keys_50)); }
	inline List_1_t1398341365 * get__keys_50() const { return ____keys_50; }
	inline List_1_t1398341365 ** get_address_of__keys_50() { return &____keys_50; }
	inline void set__keys_50(List_1_t1398341365 * value)
	{
		____keys_50 = value;
		Il2CppCodeGenWriteBarrier(&____keys_50, value);
	}

	inline static int32_t get_offset_of_moduleCnt_51() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545, ___moduleCnt_51)); }
	inline int32_t get_moduleCnt_51() const { return ___moduleCnt_51; }
	inline int32_t* get_address_of_moduleCnt_51() { return &___moduleCnt_51; }
	inline void set_moduleCnt_51(int32_t value)
	{
		___moduleCnt_51 = value;
	}
};

struct CreateManager_t3918627545_StaticFields
{
public:
	// System.Int32 CreateManager::LimitPartsNum
	int32_t ___LimitPartsNum_43;
	// System.Action CreateManager::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_52;
	// System.Action CreateManager::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_53;
	// System.Action CreateManager::<>f__am$cache2
	Action_t3226471752 * ___U3CU3Ef__amU24cache2_54;
	// System.Action CreateManager::<>f__am$cache3
	Action_t3226471752 * ___U3CU3Ef__amU24cache3_55;
	// System.Action CreateManager::<>f__am$cache4
	Action_t3226471752 * ___U3CU3Ef__amU24cache4_56;

public:
	inline static int32_t get_offset_of_LimitPartsNum_43() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545_StaticFields, ___LimitPartsNum_43)); }
	inline int32_t get_LimitPartsNum_43() const { return ___LimitPartsNum_43; }
	inline int32_t* get_address_of_LimitPartsNum_43() { return &___LimitPartsNum_43; }
	inline void set_LimitPartsNum_43(int32_t value)
	{
		___LimitPartsNum_43 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_52() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545_StaticFields, ___U3CU3Ef__amU24cache0_52)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_52() const { return ___U3CU3Ef__amU24cache0_52; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_52() { return &___U3CU3Ef__amU24cache0_52; }
	inline void set_U3CU3Ef__amU24cache0_52(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_52, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_53() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545_StaticFields, ___U3CU3Ef__amU24cache1_53)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_53() const { return ___U3CU3Ef__amU24cache1_53; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_53() { return &___U3CU3Ef__amU24cache1_53; }
	inline void set_U3CU3Ef__amU24cache1_53(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_53 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_53, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_54() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545_StaticFields, ___U3CU3Ef__amU24cache2_54)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache2_54() const { return ___U3CU3Ef__amU24cache2_54; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache2_54() { return &___U3CU3Ef__amU24cache2_54; }
	inline void set_U3CU3Ef__amU24cache2_54(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache2_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_54, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_55() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545_StaticFields, ___U3CU3Ef__amU24cache3_55)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache3_55() const { return ___U3CU3Ef__amU24cache3_55; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache3_55() { return &___U3CU3Ef__amU24cache3_55; }
	inline void set_U3CU3Ef__amU24cache3_55(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache3_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_55, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_56() { return static_cast<int32_t>(offsetof(CreateManager_t3918627545_StaticFields, ___U3CU3Ef__amU24cache4_56)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache4_56() const { return ___U3CU3Ef__amU24cache4_56; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache4_56() { return &___U3CU3Ef__amU24cache4_56; }
	inline void set_U3CU3Ef__amU24cache4_56(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache4_56 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
