﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateManager/<EditModuleRotationData>c__Iterator3
struct  U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042  : public Il2CppObject
{
public:
	// UnityEngine.GameObject CreateManager/<EditModuleRotationData>c__Iterator3::selectPartsObj
	GameObject_t1756533147 * ___selectPartsObj_0;
	// UnityEngine.Transform CreateManager/<EditModuleRotationData>c__Iterator3::selectPartsTran
	Transform_t3275118058 * ___selectPartsTran_1;
	// System.Object CreateManager/<EditModuleRotationData>c__Iterator3::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean CreateManager/<EditModuleRotationData>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 CreateManager/<EditModuleRotationData>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_selectPartsObj_0() { return static_cast<int32_t>(offsetof(U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042, ___selectPartsObj_0)); }
	inline GameObject_t1756533147 * get_selectPartsObj_0() const { return ___selectPartsObj_0; }
	inline GameObject_t1756533147 ** get_address_of_selectPartsObj_0() { return &___selectPartsObj_0; }
	inline void set_selectPartsObj_0(GameObject_t1756533147 * value)
	{
		___selectPartsObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___selectPartsObj_0, value);
	}

	inline static int32_t get_offset_of_selectPartsTran_1() { return static_cast<int32_t>(offsetof(U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042, ___selectPartsTran_1)); }
	inline Transform_t3275118058 * get_selectPartsTran_1() const { return ___selectPartsTran_1; }
	inline Transform_t3275118058 ** get_address_of_selectPartsTran_1() { return &___selectPartsTran_1; }
	inline void set_selectPartsTran_1(Transform_t3275118058 * value)
	{
		___selectPartsTran_1 = value;
		Il2CppCodeGenWriteBarrier(&___selectPartsTran_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
