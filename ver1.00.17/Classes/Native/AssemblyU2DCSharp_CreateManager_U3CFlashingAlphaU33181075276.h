﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateManager/<FlashingAlpha>c__AnonStorey5
struct  U3CFlashingAlphaU3Ec__AnonStorey5_t3181075276  : public Il2CppObject
{
public:
	// UnityEngine.Material CreateManager/<FlashingAlpha>c__AnonStorey5::mats
	Material_t193706927 * ___mats_0;
	// UnityEngine.Color CreateManager/<FlashingAlpha>c__AnonStorey5::defColor
	Color_t2020392075  ___defColor_1;

public:
	inline static int32_t get_offset_of_mats_0() { return static_cast<int32_t>(offsetof(U3CFlashingAlphaU3Ec__AnonStorey5_t3181075276, ___mats_0)); }
	inline Material_t193706927 * get_mats_0() const { return ___mats_0; }
	inline Material_t193706927 ** get_address_of_mats_0() { return &___mats_0; }
	inline void set_mats_0(Material_t193706927 * value)
	{
		___mats_0 = value;
		Il2CppCodeGenWriteBarrier(&___mats_0, value);
	}

	inline static int32_t get_offset_of_defColor_1() { return static_cast<int32_t>(offsetof(U3CFlashingAlphaU3Ec__AnonStorey5_t3181075276, ___defColor_1)); }
	inline Color_t2020392075  get_defColor_1() const { return ___defColor_1; }
	inline Color_t2020392075 * get_address_of_defColor_1() { return &___defColor_1; }
	inline void set_defColor_1(Color_t2020392075  value)
	{
		___defColor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
