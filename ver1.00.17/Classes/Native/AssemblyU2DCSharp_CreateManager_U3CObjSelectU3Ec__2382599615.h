﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Joint[]
struct JointU5BU5D_t171503857;
// CreateManager
struct CreateManager_t3918627545;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateManager/<ObjSelect>c__Iterator1
struct  U3CObjSelectU3Ec__Iterator1_t2382599615  : public Il2CppObject
{
public:
	// UnityEngine.GameObject CreateManager/<ObjSelect>c__Iterator1::partsObj
	GameObject_t1756533147 * ___partsObj_0;
	// Joint[] CreateManager/<ObjSelect>c__Iterator1::<jointParents>__0
	JointU5BU5D_t171503857* ___U3CjointParentsU3E__0_1;
	// CreateManager CreateManager/<ObjSelect>c__Iterator1::$this
	CreateManager_t3918627545 * ___U24this_2;
	// System.Object CreateManager/<ObjSelect>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean CreateManager/<ObjSelect>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 CreateManager/<ObjSelect>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_partsObj_0() { return static_cast<int32_t>(offsetof(U3CObjSelectU3Ec__Iterator1_t2382599615, ___partsObj_0)); }
	inline GameObject_t1756533147 * get_partsObj_0() const { return ___partsObj_0; }
	inline GameObject_t1756533147 ** get_address_of_partsObj_0() { return &___partsObj_0; }
	inline void set_partsObj_0(GameObject_t1756533147 * value)
	{
		___partsObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___partsObj_0, value);
	}

	inline static int32_t get_offset_of_U3CjointParentsU3E__0_1() { return static_cast<int32_t>(offsetof(U3CObjSelectU3Ec__Iterator1_t2382599615, ___U3CjointParentsU3E__0_1)); }
	inline JointU5BU5D_t171503857* get_U3CjointParentsU3E__0_1() const { return ___U3CjointParentsU3E__0_1; }
	inline JointU5BU5D_t171503857** get_address_of_U3CjointParentsU3E__0_1() { return &___U3CjointParentsU3E__0_1; }
	inline void set_U3CjointParentsU3E__0_1(JointU5BU5D_t171503857* value)
	{
		___U3CjointParentsU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjointParentsU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CObjSelectU3Ec__Iterator1_t2382599615, ___U24this_2)); }
	inline CreateManager_t3918627545 * get_U24this_2() const { return ___U24this_2; }
	inline CreateManager_t3918627545 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CreateManager_t3918627545 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CObjSelectU3Ec__Iterator1_t2382599615, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CObjSelectU3Ec__Iterator1_t2382599615, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CObjSelectU3Ec__Iterator1_t2382599615, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
