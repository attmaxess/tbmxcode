﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_t454307495;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateManager/<_AnchorSetting>c__Iterator0
struct  U3C_AnchorSettingU3Ec__Iterator0_t2219086597  : public Il2CppObject
{
public:
	// UnityEngine.ConfigurableJoint CreateManager/<_AnchorSetting>c__Iterator0::conJoint
	ConfigurableJoint_t454307495 * ___conJoint_0;
	// UnityEngine.Vector3 CreateManager/<_AnchorSetting>c__Iterator0::value
	Vector3_t2243707580  ___value_1;
	// System.Object CreateManager/<_AnchorSetting>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean CreateManager/<_AnchorSetting>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CreateManager/<_AnchorSetting>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_conJoint_0() { return static_cast<int32_t>(offsetof(U3C_AnchorSettingU3Ec__Iterator0_t2219086597, ___conJoint_0)); }
	inline ConfigurableJoint_t454307495 * get_conJoint_0() const { return ___conJoint_0; }
	inline ConfigurableJoint_t454307495 ** get_address_of_conJoint_0() { return &___conJoint_0; }
	inline void set_conJoint_0(ConfigurableJoint_t454307495 * value)
	{
		___conJoint_0 = value;
		Il2CppCodeGenWriteBarrier(&___conJoint_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(U3C_AnchorSettingU3Ec__Iterator0_t2219086597, ___value_1)); }
	inline Vector3_t2243707580  get_value_1() const { return ___value_1; }
	inline Vector3_t2243707580 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector3_t2243707580  value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3C_AnchorSettingU3Ec__Iterator0_t2219086597, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3C_AnchorSettingU3Ec__Iterator0_t2219086597, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3C_AnchorSettingU3Ec__Iterator0_t2219086597, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
