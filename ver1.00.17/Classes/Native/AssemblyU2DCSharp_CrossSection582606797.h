﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

// UnityEngine.Material[][]
struct MaterialU5BU5DU5BU5D_t889506739;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// SectionSetter
struct SectionSetter_t3682766552;
// Section
struct Section_t3430619939;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CrossSection
struct  CrossSection_t582606797  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material[][] CrossSection::allMaterials
	MaterialU5BU5DU5BU5D_t889506739* ___allMaterials_2;
	// UnityEngine.Material[][] CrossSection::allMatInstances
	MaterialU5BU5DU5BU5D_t889506739* ___allMatInstances_3;
	// UnityEngine.Renderer[] CrossSection::renderers
	RendererU5BU5D_t2810717544* ___renderers_4;
	// SectionSetter CrossSection::setter
	SectionSetter_t3682766552 * ___setter_5;
	// UnityEngine.Vector3 CrossSection::boundsCentre
	Vector3_t2243707580  ___boundsCentre_6;
	// UnityEngine.Vector4 CrossSection::sectionplane
	Vector4_t2243707581  ___sectionplane_7;
	// Section CrossSection::sectVars
	Section_t3430619939 * ___sectVars_8;

public:
	inline static int32_t get_offset_of_allMaterials_2() { return static_cast<int32_t>(offsetof(CrossSection_t582606797, ___allMaterials_2)); }
	inline MaterialU5BU5DU5BU5D_t889506739* get_allMaterials_2() const { return ___allMaterials_2; }
	inline MaterialU5BU5DU5BU5D_t889506739** get_address_of_allMaterials_2() { return &___allMaterials_2; }
	inline void set_allMaterials_2(MaterialU5BU5DU5BU5D_t889506739* value)
	{
		___allMaterials_2 = value;
		Il2CppCodeGenWriteBarrier(&___allMaterials_2, value);
	}

	inline static int32_t get_offset_of_allMatInstances_3() { return static_cast<int32_t>(offsetof(CrossSection_t582606797, ___allMatInstances_3)); }
	inline MaterialU5BU5DU5BU5D_t889506739* get_allMatInstances_3() const { return ___allMatInstances_3; }
	inline MaterialU5BU5DU5BU5D_t889506739** get_address_of_allMatInstances_3() { return &___allMatInstances_3; }
	inline void set_allMatInstances_3(MaterialU5BU5DU5BU5D_t889506739* value)
	{
		___allMatInstances_3 = value;
		Il2CppCodeGenWriteBarrier(&___allMatInstances_3, value);
	}

	inline static int32_t get_offset_of_renderers_4() { return static_cast<int32_t>(offsetof(CrossSection_t582606797, ___renderers_4)); }
	inline RendererU5BU5D_t2810717544* get_renderers_4() const { return ___renderers_4; }
	inline RendererU5BU5D_t2810717544** get_address_of_renderers_4() { return &___renderers_4; }
	inline void set_renderers_4(RendererU5BU5D_t2810717544* value)
	{
		___renderers_4 = value;
		Il2CppCodeGenWriteBarrier(&___renderers_4, value);
	}

	inline static int32_t get_offset_of_setter_5() { return static_cast<int32_t>(offsetof(CrossSection_t582606797, ___setter_5)); }
	inline SectionSetter_t3682766552 * get_setter_5() const { return ___setter_5; }
	inline SectionSetter_t3682766552 ** get_address_of_setter_5() { return &___setter_5; }
	inline void set_setter_5(SectionSetter_t3682766552 * value)
	{
		___setter_5 = value;
		Il2CppCodeGenWriteBarrier(&___setter_5, value);
	}

	inline static int32_t get_offset_of_boundsCentre_6() { return static_cast<int32_t>(offsetof(CrossSection_t582606797, ___boundsCentre_6)); }
	inline Vector3_t2243707580  get_boundsCentre_6() const { return ___boundsCentre_6; }
	inline Vector3_t2243707580 * get_address_of_boundsCentre_6() { return &___boundsCentre_6; }
	inline void set_boundsCentre_6(Vector3_t2243707580  value)
	{
		___boundsCentre_6 = value;
	}

	inline static int32_t get_offset_of_sectionplane_7() { return static_cast<int32_t>(offsetof(CrossSection_t582606797, ___sectionplane_7)); }
	inline Vector4_t2243707581  get_sectionplane_7() const { return ___sectionplane_7; }
	inline Vector4_t2243707581 * get_address_of_sectionplane_7() { return &___sectionplane_7; }
	inline void set_sectionplane_7(Vector4_t2243707581  value)
	{
		___sectionplane_7 = value;
	}

	inline static int32_t get_offset_of_sectVars_8() { return static_cast<int32_t>(offsetof(CrossSection_t582606797, ___sectVars_8)); }
	inline Section_t3430619939 * get_sectVars_8() const { return ___sectVars_8; }
	inline Section_t3430619939 ** get_address_of_sectVars_8() { return &___sectVars_8; }
	inline void set_sectVars_8(Section_t3430619939 * value)
	{
		___sectVars_8 = value;
		Il2CppCodeGenWriteBarrier(&___sectVars_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
