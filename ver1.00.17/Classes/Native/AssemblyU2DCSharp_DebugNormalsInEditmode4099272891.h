﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// CombineChildrenAFS
struct CombineChildrenAFS_t3843368254;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugNormalsInEditmode
struct  DebugNormalsInEditmode_t4099272891  : public MonoBehaviour_t1158329972
{
public:
	// CombineChildrenAFS DebugNormalsInEditmode::cc
	CombineChildrenAFS_t3843368254 * ___cc_2;

public:
	inline static int32_t get_offset_of_cc_2() { return static_cast<int32_t>(offsetof(DebugNormalsInEditmode_t4099272891, ___cc_2)); }
	inline CombineChildrenAFS_t3843368254 * get_cc_2() const { return ___cc_2; }
	inline CombineChildrenAFS_t3843368254 ** get_address_of_cc_2() { return &___cc_2; }
	inline void set_cc_2(CombineChildrenAFS_t3843368254 * value)
	{
		___cc_2 = value;
		Il2CppCodeGenWriteBarrier(&___cc_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
