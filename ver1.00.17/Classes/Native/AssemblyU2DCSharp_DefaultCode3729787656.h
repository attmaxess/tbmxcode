﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

// System.String
struct String_t;
// DefaultCodeData[]
struct DefaultCodeDataU5BU5D_t743427209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultCode
struct  DefaultCode_t3729787656  : public ScriptableObject_t1975622470
{
public:
	// System.String DefaultCode::SheetName
	String_t* ___SheetName_2;
	// System.String DefaultCode::WorksheetName
	String_t* ___WorksheetName_3;
	// DefaultCodeData[] DefaultCode::dataArray
	DefaultCodeDataU5BU5D_t743427209* ___dataArray_4;

public:
	inline static int32_t get_offset_of_SheetName_2() { return static_cast<int32_t>(offsetof(DefaultCode_t3729787656, ___SheetName_2)); }
	inline String_t* get_SheetName_2() const { return ___SheetName_2; }
	inline String_t** get_address_of_SheetName_2() { return &___SheetName_2; }
	inline void set_SheetName_2(String_t* value)
	{
		___SheetName_2 = value;
		Il2CppCodeGenWriteBarrier(&___SheetName_2, value);
	}

	inline static int32_t get_offset_of_WorksheetName_3() { return static_cast<int32_t>(offsetof(DefaultCode_t3729787656, ___WorksheetName_3)); }
	inline String_t* get_WorksheetName_3() const { return ___WorksheetName_3; }
	inline String_t** get_address_of_WorksheetName_3() { return &___WorksheetName_3; }
	inline void set_WorksheetName_3(String_t* value)
	{
		___WorksheetName_3 = value;
		Il2CppCodeGenWriteBarrier(&___WorksheetName_3, value);
	}

	inline static int32_t get_offset_of_dataArray_4() { return static_cast<int32_t>(offsetof(DefaultCode_t3729787656, ___dataArray_4)); }
	inline DefaultCodeDataU5BU5D_t743427209* get_dataArray_4() const { return ___dataArray_4; }
	inline DefaultCodeDataU5BU5D_t743427209** get_address_of_dataArray_4() { return &___dataArray_4; }
	inline void set_dataArray_4(DefaultCodeDataU5BU5D_t743427209* value)
	{
		___dataArray_4 = value;
		Il2CppCodeGenWriteBarrier(&___dataArray_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
