﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.ComponentMobId
struct  ComponentMobId_t880173240  : public Il2CppObject
{
public:

public:
};

struct ComponentMobId_t880173240_StaticFields
{
public:
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_ID
	String_t* ___COMPONENT_ID_0;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_NAME
	String_t* ___COMPONENT_NAME_1;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_DESCRIPTION
	String_t* ___COMPONENT_DESCRIPTION_2;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_CATEGORY
	String_t* ___COMPONENT_CATEGORY_3;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_MASS
	String_t* ___COMPONENT_MASS_4;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_FORCE
	String_t* ___COMPONENT_FORCE_5;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_BOUNCE
	String_t* ___COMPONENT_BOUNCE_6;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_FLOATFORCE
	String_t* ___COMPONENT_FLOATFORCE_7;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_FLOATBOUNCE
	String_t* ___COMPONENT_FLOATBOUNCE_8;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_FLOATFRICTION
	String_t* ___COMPONENT_FLOATFRICTION_9;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_FRICTION
	String_t* ___COMPONENT_FRICTION_10;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_ELASTICITYSPEED
	String_t* ___COMPONENT_ELASTICITYSPEED_11;
	// System.String DefsJsonKey.ComponentMobId::COMPONENT_ROTATIONSPEED
	String_t* ___COMPONENT_ROTATIONSPEED_12;

public:
	inline static int32_t get_offset_of_COMPONENT_ID_0() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_ID_0)); }
	inline String_t* get_COMPONENT_ID_0() const { return ___COMPONENT_ID_0; }
	inline String_t** get_address_of_COMPONENT_ID_0() { return &___COMPONENT_ID_0; }
	inline void set_COMPONENT_ID_0(String_t* value)
	{
		___COMPONENT_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_ID_0, value);
	}

	inline static int32_t get_offset_of_COMPONENT_NAME_1() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_NAME_1)); }
	inline String_t* get_COMPONENT_NAME_1() const { return ___COMPONENT_NAME_1; }
	inline String_t** get_address_of_COMPONENT_NAME_1() { return &___COMPONENT_NAME_1; }
	inline void set_COMPONENT_NAME_1(String_t* value)
	{
		___COMPONENT_NAME_1 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_NAME_1, value);
	}

	inline static int32_t get_offset_of_COMPONENT_DESCRIPTION_2() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_DESCRIPTION_2)); }
	inline String_t* get_COMPONENT_DESCRIPTION_2() const { return ___COMPONENT_DESCRIPTION_2; }
	inline String_t** get_address_of_COMPONENT_DESCRIPTION_2() { return &___COMPONENT_DESCRIPTION_2; }
	inline void set_COMPONENT_DESCRIPTION_2(String_t* value)
	{
		___COMPONENT_DESCRIPTION_2 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_DESCRIPTION_2, value);
	}

	inline static int32_t get_offset_of_COMPONENT_CATEGORY_3() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_CATEGORY_3)); }
	inline String_t* get_COMPONENT_CATEGORY_3() const { return ___COMPONENT_CATEGORY_3; }
	inline String_t** get_address_of_COMPONENT_CATEGORY_3() { return &___COMPONENT_CATEGORY_3; }
	inline void set_COMPONENT_CATEGORY_3(String_t* value)
	{
		___COMPONENT_CATEGORY_3 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_CATEGORY_3, value);
	}

	inline static int32_t get_offset_of_COMPONENT_MASS_4() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_MASS_4)); }
	inline String_t* get_COMPONENT_MASS_4() const { return ___COMPONENT_MASS_4; }
	inline String_t** get_address_of_COMPONENT_MASS_4() { return &___COMPONENT_MASS_4; }
	inline void set_COMPONENT_MASS_4(String_t* value)
	{
		___COMPONENT_MASS_4 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_MASS_4, value);
	}

	inline static int32_t get_offset_of_COMPONENT_FORCE_5() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_FORCE_5)); }
	inline String_t* get_COMPONENT_FORCE_5() const { return ___COMPONENT_FORCE_5; }
	inline String_t** get_address_of_COMPONENT_FORCE_5() { return &___COMPONENT_FORCE_5; }
	inline void set_COMPONENT_FORCE_5(String_t* value)
	{
		___COMPONENT_FORCE_5 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_FORCE_5, value);
	}

	inline static int32_t get_offset_of_COMPONENT_BOUNCE_6() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_BOUNCE_6)); }
	inline String_t* get_COMPONENT_BOUNCE_6() const { return ___COMPONENT_BOUNCE_6; }
	inline String_t** get_address_of_COMPONENT_BOUNCE_6() { return &___COMPONENT_BOUNCE_6; }
	inline void set_COMPONENT_BOUNCE_6(String_t* value)
	{
		___COMPONENT_BOUNCE_6 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_BOUNCE_6, value);
	}

	inline static int32_t get_offset_of_COMPONENT_FLOATFORCE_7() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_FLOATFORCE_7)); }
	inline String_t* get_COMPONENT_FLOATFORCE_7() const { return ___COMPONENT_FLOATFORCE_7; }
	inline String_t** get_address_of_COMPONENT_FLOATFORCE_7() { return &___COMPONENT_FLOATFORCE_7; }
	inline void set_COMPONENT_FLOATFORCE_7(String_t* value)
	{
		___COMPONENT_FLOATFORCE_7 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_FLOATFORCE_7, value);
	}

	inline static int32_t get_offset_of_COMPONENT_FLOATBOUNCE_8() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_FLOATBOUNCE_8)); }
	inline String_t* get_COMPONENT_FLOATBOUNCE_8() const { return ___COMPONENT_FLOATBOUNCE_8; }
	inline String_t** get_address_of_COMPONENT_FLOATBOUNCE_8() { return &___COMPONENT_FLOATBOUNCE_8; }
	inline void set_COMPONENT_FLOATBOUNCE_8(String_t* value)
	{
		___COMPONENT_FLOATBOUNCE_8 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_FLOATBOUNCE_8, value);
	}

	inline static int32_t get_offset_of_COMPONENT_FLOATFRICTION_9() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_FLOATFRICTION_9)); }
	inline String_t* get_COMPONENT_FLOATFRICTION_9() const { return ___COMPONENT_FLOATFRICTION_9; }
	inline String_t** get_address_of_COMPONENT_FLOATFRICTION_9() { return &___COMPONENT_FLOATFRICTION_9; }
	inline void set_COMPONENT_FLOATFRICTION_9(String_t* value)
	{
		___COMPONENT_FLOATFRICTION_9 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_FLOATFRICTION_9, value);
	}

	inline static int32_t get_offset_of_COMPONENT_FRICTION_10() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_FRICTION_10)); }
	inline String_t* get_COMPONENT_FRICTION_10() const { return ___COMPONENT_FRICTION_10; }
	inline String_t** get_address_of_COMPONENT_FRICTION_10() { return &___COMPONENT_FRICTION_10; }
	inline void set_COMPONENT_FRICTION_10(String_t* value)
	{
		___COMPONENT_FRICTION_10 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_FRICTION_10, value);
	}

	inline static int32_t get_offset_of_COMPONENT_ELASTICITYSPEED_11() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_ELASTICITYSPEED_11)); }
	inline String_t* get_COMPONENT_ELASTICITYSPEED_11() const { return ___COMPONENT_ELASTICITYSPEED_11; }
	inline String_t** get_address_of_COMPONENT_ELASTICITYSPEED_11() { return &___COMPONENT_ELASTICITYSPEED_11; }
	inline void set_COMPONENT_ELASTICITYSPEED_11(String_t* value)
	{
		___COMPONENT_ELASTICITYSPEED_11 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_ELASTICITYSPEED_11, value);
	}

	inline static int32_t get_offset_of_COMPONENT_ROTATIONSPEED_12() { return static_cast<int32_t>(offsetof(ComponentMobId_t880173240_StaticFields, ___COMPONENT_ROTATIONSPEED_12)); }
	inline String_t* get_COMPONENT_ROTATIONSPEED_12() const { return ___COMPONENT_ROTATIONSPEED_12; }
	inline String_t** get_address_of_COMPONENT_ROTATIONSPEED_12() { return &___COMPONENT_ROTATIONSPEED_12; }
	inline void set_COMPONENT_ROTATIONSPEED_12(String_t* value)
	{
		___COMPONENT_ROTATIONSPEED_12 = value;
		Il2CppCodeGenWriteBarrier(&___COMPONENT_ROTATIONSPEED_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
