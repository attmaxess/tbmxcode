﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.DnaMobId
struct  DnaMobId_t3277721392  : public Il2CppObject
{
public:

public:
};

struct DnaMobId_t3277721392_StaticFields
{
public:
	// System.String DefsJsonKey.DnaMobId::DNA_PARENT_ID
	String_t* ___DNA_PARENT_ID_0;
	// System.String DefsJsonKey.DnaMobId::DNA_PARENT_NAME
	String_t* ___DNA_PARENT_NAME_1;
	// System.String DefsJsonKey.DnaMobId::DNA_MOB_ID
	String_t* ___DNA_MOB_ID_2;
	// System.String DefsJsonKey.DnaMobId::DNA_MOB_NAME
	String_t* ___DNA_MOB_NAME_3;
	// System.String DefsJsonKey.DnaMobId::DNA_CHILD_ID
	String_t* ___DNA_CHILD_ID_4;
	// System.String DefsJsonKey.DnaMobId::DNA_CHILD_NAME
	String_t* ___DNA_CHILD_NAME_5;

public:
	inline static int32_t get_offset_of_DNA_PARENT_ID_0() { return static_cast<int32_t>(offsetof(DnaMobId_t3277721392_StaticFields, ___DNA_PARENT_ID_0)); }
	inline String_t* get_DNA_PARENT_ID_0() const { return ___DNA_PARENT_ID_0; }
	inline String_t** get_address_of_DNA_PARENT_ID_0() { return &___DNA_PARENT_ID_0; }
	inline void set_DNA_PARENT_ID_0(String_t* value)
	{
		___DNA_PARENT_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___DNA_PARENT_ID_0, value);
	}

	inline static int32_t get_offset_of_DNA_PARENT_NAME_1() { return static_cast<int32_t>(offsetof(DnaMobId_t3277721392_StaticFields, ___DNA_PARENT_NAME_1)); }
	inline String_t* get_DNA_PARENT_NAME_1() const { return ___DNA_PARENT_NAME_1; }
	inline String_t** get_address_of_DNA_PARENT_NAME_1() { return &___DNA_PARENT_NAME_1; }
	inline void set_DNA_PARENT_NAME_1(String_t* value)
	{
		___DNA_PARENT_NAME_1 = value;
		Il2CppCodeGenWriteBarrier(&___DNA_PARENT_NAME_1, value);
	}

	inline static int32_t get_offset_of_DNA_MOB_ID_2() { return static_cast<int32_t>(offsetof(DnaMobId_t3277721392_StaticFields, ___DNA_MOB_ID_2)); }
	inline String_t* get_DNA_MOB_ID_2() const { return ___DNA_MOB_ID_2; }
	inline String_t** get_address_of_DNA_MOB_ID_2() { return &___DNA_MOB_ID_2; }
	inline void set_DNA_MOB_ID_2(String_t* value)
	{
		___DNA_MOB_ID_2 = value;
		Il2CppCodeGenWriteBarrier(&___DNA_MOB_ID_2, value);
	}

	inline static int32_t get_offset_of_DNA_MOB_NAME_3() { return static_cast<int32_t>(offsetof(DnaMobId_t3277721392_StaticFields, ___DNA_MOB_NAME_3)); }
	inline String_t* get_DNA_MOB_NAME_3() const { return ___DNA_MOB_NAME_3; }
	inline String_t** get_address_of_DNA_MOB_NAME_3() { return &___DNA_MOB_NAME_3; }
	inline void set_DNA_MOB_NAME_3(String_t* value)
	{
		___DNA_MOB_NAME_3 = value;
		Il2CppCodeGenWriteBarrier(&___DNA_MOB_NAME_3, value);
	}

	inline static int32_t get_offset_of_DNA_CHILD_ID_4() { return static_cast<int32_t>(offsetof(DnaMobId_t3277721392_StaticFields, ___DNA_CHILD_ID_4)); }
	inline String_t* get_DNA_CHILD_ID_4() const { return ___DNA_CHILD_ID_4; }
	inline String_t** get_address_of_DNA_CHILD_ID_4() { return &___DNA_CHILD_ID_4; }
	inline void set_DNA_CHILD_ID_4(String_t* value)
	{
		___DNA_CHILD_ID_4 = value;
		Il2CppCodeGenWriteBarrier(&___DNA_CHILD_ID_4, value);
	}

	inline static int32_t get_offset_of_DNA_CHILD_NAME_5() { return static_cast<int32_t>(offsetof(DnaMobId_t3277721392_StaticFields, ___DNA_CHILD_NAME_5)); }
	inline String_t* get_DNA_CHILD_NAME_5() const { return ___DNA_CHILD_NAME_5; }
	inline String_t** get_address_of_DNA_CHILD_NAME_5() { return &___DNA_CHILD_NAME_5; }
	inline void set_DNA_CHILD_NAME_5(String_t* value)
	{
		___DNA_CHILD_NAME_5 = value;
		Il2CppCodeGenWriteBarrier(&___DNA_CHILD_NAME_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
