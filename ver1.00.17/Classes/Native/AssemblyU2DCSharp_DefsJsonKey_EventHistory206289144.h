﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.EventHistory
struct  EventHistory_t206289144  : public Il2CppObject
{
public:

public:
};

struct EventHistory_t206289144_StaticFields
{
public:
	// System.String DefsJsonKey.EventHistory::EVENT_RANK
	String_t* ___EVENT_RANK_0;
	// System.String DefsJsonKey.EventHistory::EVENT_RANKUPPOINT
	String_t* ___EVENT_RANKUPPOINT_1;
	// System.String DefsJsonKey.EventHistory::EVENT_PRIZEDMASTER_ID
	String_t* ___EVENT_PRIZEDMASTER_ID_2;

public:
	inline static int32_t get_offset_of_EVENT_RANK_0() { return static_cast<int32_t>(offsetof(EventHistory_t206289144_StaticFields, ___EVENT_RANK_0)); }
	inline String_t* get_EVENT_RANK_0() const { return ___EVENT_RANK_0; }
	inline String_t** get_address_of_EVENT_RANK_0() { return &___EVENT_RANK_0; }
	inline void set_EVENT_RANK_0(String_t* value)
	{
		___EVENT_RANK_0 = value;
		Il2CppCodeGenWriteBarrier(&___EVENT_RANK_0, value);
	}

	inline static int32_t get_offset_of_EVENT_RANKUPPOINT_1() { return static_cast<int32_t>(offsetof(EventHistory_t206289144_StaticFields, ___EVENT_RANKUPPOINT_1)); }
	inline String_t* get_EVENT_RANKUPPOINT_1() const { return ___EVENT_RANKUPPOINT_1; }
	inline String_t** get_address_of_EVENT_RANKUPPOINT_1() { return &___EVENT_RANKUPPOINT_1; }
	inline void set_EVENT_RANKUPPOINT_1(String_t* value)
	{
		___EVENT_RANKUPPOINT_1 = value;
		Il2CppCodeGenWriteBarrier(&___EVENT_RANKUPPOINT_1, value);
	}

	inline static int32_t get_offset_of_EVENT_PRIZEDMASTER_ID_2() { return static_cast<int32_t>(offsetof(EventHistory_t206289144_StaticFields, ___EVENT_PRIZEDMASTER_ID_2)); }
	inline String_t* get_EVENT_PRIZEDMASTER_ID_2() const { return ___EVENT_PRIZEDMASTER_ID_2; }
	inline String_t** get_address_of_EVENT_PRIZEDMASTER_ID_2() { return &___EVENT_PRIZEDMASTER_ID_2; }
	inline void set_EVENT_PRIZEDMASTER_ID_2(String_t* value)
	{
		___EVENT_PRIZEDMASTER_ID_2 = value;
		Il2CppCodeGenWriteBarrier(&___EVENT_PRIZEDMASTER_ID_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
