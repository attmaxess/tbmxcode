﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.Feed
struct  Feed_t2527569476  : public Il2CppObject
{
public:

public:
};

struct Feed_t2527569476_StaticFields
{
public:
	// System.String DefsJsonKey.Feed::FEED_ID
	String_t* ___FEED_ID_0;
	// System.String DefsJsonKey.Feed::FEED_DATE
	String_t* ___FEED_DATE_1;
	// System.String DefsJsonKey.Feed::FEED_NAME
	String_t* ___FEED_NAME_2;
	// System.String DefsJsonKey.Feed::FEED_SITUATION
	String_t* ___FEED_SITUATION_3;
	// System.String DefsJsonKey.Feed::FEED_ALLCOUNT
	String_t* ___FEED_ALLCOUNT_4;
	// System.String DefsJsonKey.Feed::FEED_USER_ID
	String_t* ___FEED_USER_ID_5;
	// System.String DefsJsonKey.Feed::FEED_COPIEDCOUNT
	String_t* ___FEED_COPIEDCOUNT_6;
	// System.String DefsJsonKey.Feed::FEED_THUMBNAIL
	String_t* ___FEED_THUMBNAIL_7;

public:
	inline static int32_t get_offset_of_FEED_ID_0() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_ID_0)); }
	inline String_t* get_FEED_ID_0() const { return ___FEED_ID_0; }
	inline String_t** get_address_of_FEED_ID_0() { return &___FEED_ID_0; }
	inline void set_FEED_ID_0(String_t* value)
	{
		___FEED_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_ID_0, value);
	}

	inline static int32_t get_offset_of_FEED_DATE_1() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_DATE_1)); }
	inline String_t* get_FEED_DATE_1() const { return ___FEED_DATE_1; }
	inline String_t** get_address_of_FEED_DATE_1() { return &___FEED_DATE_1; }
	inline void set_FEED_DATE_1(String_t* value)
	{
		___FEED_DATE_1 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_DATE_1, value);
	}

	inline static int32_t get_offset_of_FEED_NAME_2() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_NAME_2)); }
	inline String_t* get_FEED_NAME_2() const { return ___FEED_NAME_2; }
	inline String_t** get_address_of_FEED_NAME_2() { return &___FEED_NAME_2; }
	inline void set_FEED_NAME_2(String_t* value)
	{
		___FEED_NAME_2 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_NAME_2, value);
	}

	inline static int32_t get_offset_of_FEED_SITUATION_3() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_SITUATION_3)); }
	inline String_t* get_FEED_SITUATION_3() const { return ___FEED_SITUATION_3; }
	inline String_t** get_address_of_FEED_SITUATION_3() { return &___FEED_SITUATION_3; }
	inline void set_FEED_SITUATION_3(String_t* value)
	{
		___FEED_SITUATION_3 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_SITUATION_3, value);
	}

	inline static int32_t get_offset_of_FEED_ALLCOUNT_4() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_ALLCOUNT_4)); }
	inline String_t* get_FEED_ALLCOUNT_4() const { return ___FEED_ALLCOUNT_4; }
	inline String_t** get_address_of_FEED_ALLCOUNT_4() { return &___FEED_ALLCOUNT_4; }
	inline void set_FEED_ALLCOUNT_4(String_t* value)
	{
		___FEED_ALLCOUNT_4 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_ALLCOUNT_4, value);
	}

	inline static int32_t get_offset_of_FEED_USER_ID_5() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_USER_ID_5)); }
	inline String_t* get_FEED_USER_ID_5() const { return ___FEED_USER_ID_5; }
	inline String_t** get_address_of_FEED_USER_ID_5() { return &___FEED_USER_ID_5; }
	inline void set_FEED_USER_ID_5(String_t* value)
	{
		___FEED_USER_ID_5 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_USER_ID_5, value);
	}

	inline static int32_t get_offset_of_FEED_COPIEDCOUNT_6() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_COPIEDCOUNT_6)); }
	inline String_t* get_FEED_COPIEDCOUNT_6() const { return ___FEED_COPIEDCOUNT_6; }
	inline String_t** get_address_of_FEED_COPIEDCOUNT_6() { return &___FEED_COPIEDCOUNT_6; }
	inline void set_FEED_COPIEDCOUNT_6(String_t* value)
	{
		___FEED_COPIEDCOUNT_6 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_COPIEDCOUNT_6, value);
	}

	inline static int32_t get_offset_of_FEED_THUMBNAIL_7() { return static_cast<int32_t>(offsetof(Feed_t2527569476_StaticFields, ___FEED_THUMBNAIL_7)); }
	inline String_t* get_FEED_THUMBNAIL_7() const { return ___FEED_THUMBNAIL_7; }
	inline String_t** get_address_of_FEED_THUMBNAIL_7() { return &___FEED_THUMBNAIL_7; }
	inline void set_FEED_THUMBNAIL_7(String_t* value)
	{
		___FEED_THUMBNAIL_7 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_THUMBNAIL_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
