﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.FeedReaction
struct  FeedReaction_t1608187089  : public Il2CppObject
{
public:

public:
};

struct FeedReaction_t1608187089_StaticFields
{
public:
	// System.String DefsJsonKey.FeedReaction::FEED_REACTION_INFO
	String_t* ___FEED_REACTION_INFO_0;
	// System.String DefsJsonKey.FeedReaction::FEED_REACTION_TYPE
	String_t* ___FEED_REACTION_TYPE_1;
	// System.String DefsJsonKey.FeedReaction::FEED_REACTION_YOURREACTIONS
	String_t* ___FEED_REACTION_YOURREACTIONS_2;

public:
	inline static int32_t get_offset_of_FEED_REACTION_INFO_0() { return static_cast<int32_t>(offsetof(FeedReaction_t1608187089_StaticFields, ___FEED_REACTION_INFO_0)); }
	inline String_t* get_FEED_REACTION_INFO_0() const { return ___FEED_REACTION_INFO_0; }
	inline String_t** get_address_of_FEED_REACTION_INFO_0() { return &___FEED_REACTION_INFO_0; }
	inline void set_FEED_REACTION_INFO_0(String_t* value)
	{
		___FEED_REACTION_INFO_0 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_REACTION_INFO_0, value);
	}

	inline static int32_t get_offset_of_FEED_REACTION_TYPE_1() { return static_cast<int32_t>(offsetof(FeedReaction_t1608187089_StaticFields, ___FEED_REACTION_TYPE_1)); }
	inline String_t* get_FEED_REACTION_TYPE_1() const { return ___FEED_REACTION_TYPE_1; }
	inline String_t** get_address_of_FEED_REACTION_TYPE_1() { return &___FEED_REACTION_TYPE_1; }
	inline void set_FEED_REACTION_TYPE_1(String_t* value)
	{
		___FEED_REACTION_TYPE_1 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_REACTION_TYPE_1, value);
	}

	inline static int32_t get_offset_of_FEED_REACTION_YOURREACTIONS_2() { return static_cast<int32_t>(offsetof(FeedReaction_t1608187089_StaticFields, ___FEED_REACTION_YOURREACTIONS_2)); }
	inline String_t* get_FEED_REACTION_YOURREACTIONS_2() const { return ___FEED_REACTION_YOURREACTIONS_2; }
	inline String_t** get_address_of_FEED_REACTION_YOURREACTIONS_2() { return &___FEED_REACTION_YOURREACTIONS_2; }
	inline void set_FEED_REACTION_YOURREACTIONS_2(String_t* value)
	{
		___FEED_REACTION_YOURREACTIONS_2 = value;
		Il2CppCodeGenWriteBarrier(&___FEED_REACTION_YOURREACTIONS_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
