﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.GetCountries
struct  GetCountries_t4172618058  : public Il2CppObject
{
public:

public:
};

struct GetCountries_t4172618058_StaticFields
{
public:
	// System.String DefsJsonKey.GetCountries::COUNTRY_NAME
	String_t* ___COUNTRY_NAME_0;
	// System.String DefsJsonKey.GetCountries::COUNTRY_NO
	String_t* ___COUNTRY_NO_1;

public:
	inline static int32_t get_offset_of_COUNTRY_NAME_0() { return static_cast<int32_t>(offsetof(GetCountries_t4172618058_StaticFields, ___COUNTRY_NAME_0)); }
	inline String_t* get_COUNTRY_NAME_0() const { return ___COUNTRY_NAME_0; }
	inline String_t** get_address_of_COUNTRY_NAME_0() { return &___COUNTRY_NAME_0; }
	inline void set_COUNTRY_NAME_0(String_t* value)
	{
		___COUNTRY_NAME_0 = value;
		Il2CppCodeGenWriteBarrier(&___COUNTRY_NAME_0, value);
	}

	inline static int32_t get_offset_of_COUNTRY_NO_1() { return static_cast<int32_t>(offsetof(GetCountries_t4172618058_StaticFields, ___COUNTRY_NO_1)); }
	inline String_t* get_COUNTRY_NO_1() const { return ___COUNTRY_NO_1; }
	inline String_t** get_address_of_COUNTRY_NO_1() { return &___COUNTRY_NO_1; }
	inline void set_COUNTRY_NO_1(String_t* value)
	{
		___COUNTRY_NO_1 = value;
		Il2CppCodeGenWriteBarrier(&___COUNTRY_NO_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
