﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.GetNoticeList
struct  GetNoticeList_t1293990008  : public Il2CppObject
{
public:

public:
};

struct GetNoticeList_t1293990008_StaticFields
{
public:
	// System.String DefsJsonKey.GetNoticeList::GET_NOTICE_LIST_DATA
	String_t* ___GET_NOTICE_LIST_DATA_0;

public:
	inline static int32_t get_offset_of_GET_NOTICE_LIST_DATA_0() { return static_cast<int32_t>(offsetof(GetNoticeList_t1293990008_StaticFields, ___GET_NOTICE_LIST_DATA_0)); }
	inline String_t* get_GET_NOTICE_LIST_DATA_0() const { return ___GET_NOTICE_LIST_DATA_0; }
	inline String_t** get_address_of_GET_NOTICE_LIST_DATA_0() { return &___GET_NOTICE_LIST_DATA_0; }
	inline void set_GET_NOTICE_LIST_DATA_0(String_t* value)
	{
		___GET_NOTICE_LIST_DATA_0 = value;
		Il2CppCodeGenWriteBarrier(&___GET_NOTICE_LIST_DATA_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
