﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.ItemData
struct  ItemData_t3591108021  : public Il2CppObject
{
public:

public:
};

struct ItemData_t3591108021_StaticFields
{
public:
	// System.String DefsJsonKey.ItemData::ITEMDATA_INFO
	String_t* ___ITEMDATA_INFO_0;
	// System.String DefsJsonKey.ItemData::ITEMS_INFO
	String_t* ___ITEMS_INFO_1;

public:
	inline static int32_t get_offset_of_ITEMDATA_INFO_0() { return static_cast<int32_t>(offsetof(ItemData_t3591108021_StaticFields, ___ITEMDATA_INFO_0)); }
	inline String_t* get_ITEMDATA_INFO_0() const { return ___ITEMDATA_INFO_0; }
	inline String_t** get_address_of_ITEMDATA_INFO_0() { return &___ITEMDATA_INFO_0; }
	inline void set_ITEMDATA_INFO_0(String_t* value)
	{
		___ITEMDATA_INFO_0 = value;
		Il2CppCodeGenWriteBarrier(&___ITEMDATA_INFO_0, value);
	}

	inline static int32_t get_offset_of_ITEMS_INFO_1() { return static_cast<int32_t>(offsetof(ItemData_t3591108021_StaticFields, ___ITEMS_INFO_1)); }
	inline String_t* get_ITEMS_INFO_1() const { return ___ITEMS_INFO_1; }
	inline String_t** get_address_of_ITEMS_INFO_1() { return &___ITEMS_INFO_1; }
	inline void set_ITEMS_INFO_1(String_t* value)
	{
		___ITEMS_INFO_1 = value;
		Il2CppCodeGenWriteBarrier(&___ITEMS_INFO_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
