﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.MigrateCode
struct  MigrateCode_t2039064722  : public Il2CppObject
{
public:

public:
};

struct MigrateCode_t2039064722_StaticFields
{
public:
	// System.String DefsJsonKey.MigrateCode::MIGRATE_CODE
	String_t* ___MIGRATE_CODE_0;

public:
	inline static int32_t get_offset_of_MIGRATE_CODE_0() { return static_cast<int32_t>(offsetof(MigrateCode_t2039064722_StaticFields, ___MIGRATE_CODE_0)); }
	inline String_t* get_MIGRATE_CODE_0() const { return ___MIGRATE_CODE_0; }
	inline String_t** get_address_of_MIGRATE_CODE_0() { return &___MIGRATE_CODE_0; }
	inline void set_MIGRATE_CODE_0(String_t* value)
	{
		___MIGRATE_CODE_0 = value;
		Il2CppCodeGenWriteBarrier(&___MIGRATE_CODE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
