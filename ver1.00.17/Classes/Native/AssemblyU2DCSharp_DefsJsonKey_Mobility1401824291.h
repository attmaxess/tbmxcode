﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.Mobility
struct  Mobility_t1401824291  : public Il2CppObject
{
public:

public:
};

struct Mobility_t1401824291_StaticFields
{
public:
	// System.String DefsJsonKey.Mobility::MOBILITY_MOBILITY_ID
	String_t* ___MOBILITY_MOBILITY_ID_0;
	// System.String DefsJsonKey.Mobility::MOBILITY_DATE
	String_t* ___MOBILITY_DATE_1;
	// System.String DefsJsonKey.Mobility::MOBILITY_NAME
	String_t* ___MOBILITY_NAME_2;
	// System.String DefsJsonKey.Mobility::MOBILITY_SITUATION
	String_t* ___MOBILITY_SITUATION_3;
	// System.String DefsJsonKey.Mobility::MOBILITY_REACTION
	String_t* ___MOBILITY_REACTION_4;
	// System.String DefsJsonKey.Mobility::MOBILITY_REACTION_ALLCOUNT
	String_t* ___MOBILITY_REACTION_ALLCOUNT_5;
	// System.String DefsJsonKey.Mobility::MOBILITY_REACTION_ITEMS_REACTIONTYPE
	String_t* ___MOBILITY_REACTION_ITEMS_REACTIONTYPE_6;
	// System.String DefsJsonKey.Mobility::MOBILITY_REACTION_ITEMS_COUNT
	String_t* ___MOBILITY_REACTION_ITEMS_COUNT_7;
	// System.String DefsJsonKey.Mobility::MOBILITY_DESCRIPTION
	String_t* ___MOBILITY_DESCRIPTION_8;
	// System.String DefsJsonKey.Mobility::MOBILITY_PRIZEDLIST
	String_t* ___MOBILITY_PRIZEDLIST_9;
	// System.String DefsJsonKey.Mobility::MOBILITY_ENCOUNTLIST
	String_t* ___MOBILITY_ENCOUNTLIST_10;
	// System.String DefsJsonKey.Mobility::MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID
	String_t* ___MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11;
	// System.String DefsJsonKey.Mobility::MOBILITY_ENCOUNTLIST_POINT
	String_t* ___MOBILITY_ENCOUNTLIST_POINT_12;
	// System.String DefsJsonKey.Mobility::MOBILITY_ENCOUNTLIST_ENCOUNT_TIME
	String_t* ___MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13;
	// System.String DefsJsonKey.Mobility::MOBILITY_REGISTERMOTIONLIST
	String_t* ___MOBILITY_REGISTERMOTIONLIST_14;
	// System.String DefsJsonKey.Mobility::MOBILITY_REGISTERMOTIONLIST_MOTION_ID
	String_t* ___MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15;
	// System.String DefsJsonKey.Mobility::MOBILITY_REGISTERMOTIONLIST_MOTIONDATA
	String_t* ___MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16;
	// System.String DefsJsonKey.Mobility::MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID
	String_t* ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17;
	// System.String DefsJsonKey.Mobility::MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH
	String_t* ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18;
	// System.String DefsJsonKey.Mobility::MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA
	String_t* ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19;
	// System.String DefsJsonKey.Mobility::MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME
	String_t* ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20;
	// System.String DefsJsonKey.Mobility::MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE
	String_t* ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_22;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_MOTIONS
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29;
	// System.String DefsJsonKey.Mobility::MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID
	String_t* ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30;
	// System.String DefsJsonKey.Mobility::MOBILITY_PARENT_ID
	String_t* ___MOBILITY_PARENT_ID_31;
	// System.String DefsJsonKey.Mobility::MOBILITY_COREPARTS_ID
	String_t* ___MOBILITY_COREPARTS_ID_32;
	// System.String DefsJsonKey.Mobility::MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID
	String_t* ___MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33;
	// System.String DefsJsonKey.Mobility::MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID
	String_t* ___MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34;
	// System.String DefsJsonKey.Mobility::MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO
	String_t* ___MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35;
	// System.String DefsJsonKey.Mobility::MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO
	String_t* ___MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36;
	// System.String DefsJsonKey.Mobility::MOBILITY_CHILDPARTSLIST
	String_t* ___MOBILITY_CHILDPARTSLIST_37;
	// System.String DefsJsonKey.Mobility::MOBILITY_CREATEDUSER_ID
	String_t* ___MOBILITY_CREATEDUSER_ID_38;
	// System.String DefsJsonKey.Mobility::MOBILITY_CREATEDUSER_NAME
	String_t* ___MOBILITY_CREATEDUSER_NAME_39;
	// System.String DefsJsonKey.Mobility::MOBILITY_CREATEDUSER_RANK
	String_t* ___MOBILITY_CREATEDUSER_RANK_40;

public:
	inline static int32_t get_offset_of_MOBILITY_MOBILITY_ID_0() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_MOBILITY_ID_0)); }
	inline String_t* get_MOBILITY_MOBILITY_ID_0() const { return ___MOBILITY_MOBILITY_ID_0; }
	inline String_t** get_address_of_MOBILITY_MOBILITY_ID_0() { return &___MOBILITY_MOBILITY_ID_0; }
	inline void set_MOBILITY_MOBILITY_ID_0(String_t* value)
	{
		___MOBILITY_MOBILITY_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_MOBILITY_ID_0, value);
	}

	inline static int32_t get_offset_of_MOBILITY_DATE_1() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_DATE_1)); }
	inline String_t* get_MOBILITY_DATE_1() const { return ___MOBILITY_DATE_1; }
	inline String_t** get_address_of_MOBILITY_DATE_1() { return &___MOBILITY_DATE_1; }
	inline void set_MOBILITY_DATE_1(String_t* value)
	{
		___MOBILITY_DATE_1 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_DATE_1, value);
	}

	inline static int32_t get_offset_of_MOBILITY_NAME_2() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_NAME_2)); }
	inline String_t* get_MOBILITY_NAME_2() const { return ___MOBILITY_NAME_2; }
	inline String_t** get_address_of_MOBILITY_NAME_2() { return &___MOBILITY_NAME_2; }
	inline void set_MOBILITY_NAME_2(String_t* value)
	{
		___MOBILITY_NAME_2 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_NAME_2, value);
	}

	inline static int32_t get_offset_of_MOBILITY_SITUATION_3() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_SITUATION_3)); }
	inline String_t* get_MOBILITY_SITUATION_3() const { return ___MOBILITY_SITUATION_3; }
	inline String_t** get_address_of_MOBILITY_SITUATION_3() { return &___MOBILITY_SITUATION_3; }
	inline void set_MOBILITY_SITUATION_3(String_t* value)
	{
		___MOBILITY_SITUATION_3 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_SITUATION_3, value);
	}

	inline static int32_t get_offset_of_MOBILITY_REACTION_4() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_REACTION_4)); }
	inline String_t* get_MOBILITY_REACTION_4() const { return ___MOBILITY_REACTION_4; }
	inline String_t** get_address_of_MOBILITY_REACTION_4() { return &___MOBILITY_REACTION_4; }
	inline void set_MOBILITY_REACTION_4(String_t* value)
	{
		___MOBILITY_REACTION_4 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_REACTION_4, value);
	}

	inline static int32_t get_offset_of_MOBILITY_REACTION_ALLCOUNT_5() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_REACTION_ALLCOUNT_5)); }
	inline String_t* get_MOBILITY_REACTION_ALLCOUNT_5() const { return ___MOBILITY_REACTION_ALLCOUNT_5; }
	inline String_t** get_address_of_MOBILITY_REACTION_ALLCOUNT_5() { return &___MOBILITY_REACTION_ALLCOUNT_5; }
	inline void set_MOBILITY_REACTION_ALLCOUNT_5(String_t* value)
	{
		___MOBILITY_REACTION_ALLCOUNT_5 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_REACTION_ALLCOUNT_5, value);
	}

	inline static int32_t get_offset_of_MOBILITY_REACTION_ITEMS_REACTIONTYPE_6() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_REACTION_ITEMS_REACTIONTYPE_6)); }
	inline String_t* get_MOBILITY_REACTION_ITEMS_REACTIONTYPE_6() const { return ___MOBILITY_REACTION_ITEMS_REACTIONTYPE_6; }
	inline String_t** get_address_of_MOBILITY_REACTION_ITEMS_REACTIONTYPE_6() { return &___MOBILITY_REACTION_ITEMS_REACTIONTYPE_6; }
	inline void set_MOBILITY_REACTION_ITEMS_REACTIONTYPE_6(String_t* value)
	{
		___MOBILITY_REACTION_ITEMS_REACTIONTYPE_6 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_REACTION_ITEMS_REACTIONTYPE_6, value);
	}

	inline static int32_t get_offset_of_MOBILITY_REACTION_ITEMS_COUNT_7() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_REACTION_ITEMS_COUNT_7)); }
	inline String_t* get_MOBILITY_REACTION_ITEMS_COUNT_7() const { return ___MOBILITY_REACTION_ITEMS_COUNT_7; }
	inline String_t** get_address_of_MOBILITY_REACTION_ITEMS_COUNT_7() { return &___MOBILITY_REACTION_ITEMS_COUNT_7; }
	inline void set_MOBILITY_REACTION_ITEMS_COUNT_7(String_t* value)
	{
		___MOBILITY_REACTION_ITEMS_COUNT_7 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_REACTION_ITEMS_COUNT_7, value);
	}

	inline static int32_t get_offset_of_MOBILITY_DESCRIPTION_8() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_DESCRIPTION_8)); }
	inline String_t* get_MOBILITY_DESCRIPTION_8() const { return ___MOBILITY_DESCRIPTION_8; }
	inline String_t** get_address_of_MOBILITY_DESCRIPTION_8() { return &___MOBILITY_DESCRIPTION_8; }
	inline void set_MOBILITY_DESCRIPTION_8(String_t* value)
	{
		___MOBILITY_DESCRIPTION_8 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_DESCRIPTION_8, value);
	}

	inline static int32_t get_offset_of_MOBILITY_PRIZEDLIST_9() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_PRIZEDLIST_9)); }
	inline String_t* get_MOBILITY_PRIZEDLIST_9() const { return ___MOBILITY_PRIZEDLIST_9; }
	inline String_t** get_address_of_MOBILITY_PRIZEDLIST_9() { return &___MOBILITY_PRIZEDLIST_9; }
	inline void set_MOBILITY_PRIZEDLIST_9(String_t* value)
	{
		___MOBILITY_PRIZEDLIST_9 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_PRIZEDLIST_9, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ENCOUNTLIST_10() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ENCOUNTLIST_10)); }
	inline String_t* get_MOBILITY_ENCOUNTLIST_10() const { return ___MOBILITY_ENCOUNTLIST_10; }
	inline String_t** get_address_of_MOBILITY_ENCOUNTLIST_10() { return &___MOBILITY_ENCOUNTLIST_10; }
	inline void set_MOBILITY_ENCOUNTLIST_10(String_t* value)
	{
		___MOBILITY_ENCOUNTLIST_10 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ENCOUNTLIST_10, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11)); }
	inline String_t* get_MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11() const { return ___MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11; }
	inline String_t** get_address_of_MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11() { return &___MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11; }
	inline void set_MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11(String_t* value)
	{
		___MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ENCOUNTLIST_ENCOUNTMOBILITY_ID_11, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ENCOUNTLIST_POINT_12() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ENCOUNTLIST_POINT_12)); }
	inline String_t* get_MOBILITY_ENCOUNTLIST_POINT_12() const { return ___MOBILITY_ENCOUNTLIST_POINT_12; }
	inline String_t** get_address_of_MOBILITY_ENCOUNTLIST_POINT_12() { return &___MOBILITY_ENCOUNTLIST_POINT_12; }
	inline void set_MOBILITY_ENCOUNTLIST_POINT_12(String_t* value)
	{
		___MOBILITY_ENCOUNTLIST_POINT_12 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ENCOUNTLIST_POINT_12, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13)); }
	inline String_t* get_MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13() const { return ___MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13; }
	inline String_t** get_address_of_MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13() { return &___MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13; }
	inline void set_MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13(String_t* value)
	{
		___MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ENCOUNTLIST_ENCOUNT_TIME_13, value);
	}

	inline static int32_t get_offset_of_MOBILITY_REGISTERMOTIONLIST_14() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_REGISTERMOTIONLIST_14)); }
	inline String_t* get_MOBILITY_REGISTERMOTIONLIST_14() const { return ___MOBILITY_REGISTERMOTIONLIST_14; }
	inline String_t** get_address_of_MOBILITY_REGISTERMOTIONLIST_14() { return &___MOBILITY_REGISTERMOTIONLIST_14; }
	inline void set_MOBILITY_REGISTERMOTIONLIST_14(String_t* value)
	{
		___MOBILITY_REGISTERMOTIONLIST_14 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_REGISTERMOTIONLIST_14, value);
	}

	inline static int32_t get_offset_of_MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15)); }
	inline String_t* get_MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15() const { return ___MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15; }
	inline String_t** get_address_of_MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15() { return &___MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15; }
	inline void set_MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15(String_t* value)
	{
		___MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_REGISTERMOTIONLIST_MOTION_ID_15, value);
	}

	inline static int32_t get_offset_of_MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16)); }
	inline String_t* get_MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16() const { return ___MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16; }
	inline String_t** get_address_of_MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16() { return &___MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16; }
	inline void set_MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16(String_t* value)
	{
		___MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_REGISTERMOTIONLIST_MOTIONDATA_16, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17)); }
	inline String_t* get_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17() const { return ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17; }
	inline String_t** get_address_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17() { return &___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17; }
	inline void set_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17(String_t* value)
	{
		___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_ID_17, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18)); }
	inline String_t* get_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18() const { return ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18; }
	inline String_t** get_address_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18() { return &___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18; }
	inline void set_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18(String_t* value)
	{
		___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_PATH_18, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19)); }
	inline String_t* get_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19() const { return ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19; }
	inline String_t** get_address_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19() { return &___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19; }
	inline void set_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19(String_t* value)
	{
		___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_19, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20)); }
	inline String_t* get_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20() const { return ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20; }
	inline String_t** get_address_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20() { return &___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20; }
	inline void set_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20(String_t* value)
	{
		___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_TIME_20, value);
	}

	inline static int32_t get_offset_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21)); }
	inline String_t* get_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21() const { return ___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21; }
	inline String_t** get_address_of_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21() { return &___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21; }
	inline void set_MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21(String_t* value)
	{
		___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_ITEMS_REGISTERDMOTIONLIST_MOTION_DATA_POSE_DATA_VALUE_21, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_22() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_22)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_22() const { return ___MOBILITY_RECORDEDMOTIONLIST_22; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_22() { return &___MOBILITY_RECORDEDMOTIONLIST_22; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_22(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_22 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_22, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23() const { return ___MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23() { return &___MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_RECORDED_ID_23, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24() const { return ___MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24() { return &___MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_RECORDED_DATA_24, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25() const { return ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25() { return &___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_X_25, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26() const { return ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26() { return &___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Y_26, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27() const { return ___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27() { return &___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_STARTPOINT_Z_27, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28() const { return ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28() { return &___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_28, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29() const { return ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29() { return &___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTIONSTARTTIME_29, value);
	}

	inline static int32_t get_offset_of_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30)); }
	inline String_t* get_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30() const { return ___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30; }
	inline String_t** get_address_of_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30() { return &___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30; }
	inline void set_MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30(String_t* value)
	{
		___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_RECORDEDMOTIONLIST_MOTIONS_MOTION_ID_30, value);
	}

	inline static int32_t get_offset_of_MOBILITY_PARENT_ID_31() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_PARENT_ID_31)); }
	inline String_t* get_MOBILITY_PARENT_ID_31() const { return ___MOBILITY_PARENT_ID_31; }
	inline String_t** get_address_of_MOBILITY_PARENT_ID_31() { return &___MOBILITY_PARENT_ID_31; }
	inline void set_MOBILITY_PARENT_ID_31(String_t* value)
	{
		___MOBILITY_PARENT_ID_31 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_PARENT_ID_31, value);
	}

	inline static int32_t get_offset_of_MOBILITY_COREPARTS_ID_32() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_COREPARTS_ID_32)); }
	inline String_t* get_MOBILITY_COREPARTS_ID_32() const { return ___MOBILITY_COREPARTS_ID_32; }
	inline String_t** get_address_of_MOBILITY_COREPARTS_ID_32() { return &___MOBILITY_COREPARTS_ID_32; }
	inline void set_MOBILITY_COREPARTS_ID_32(String_t* value)
	{
		___MOBILITY_COREPARTS_ID_32 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_COREPARTS_ID_32, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33)); }
	inline String_t* get_MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33() const { return ___MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33; }
	inline String_t** get_address_of_MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33() { return &___MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33; }
	inline void set_MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33(String_t* value)
	{
		___MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CHILDPARTSLIST_CHILDPARTS_ID_33, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34)); }
	inline String_t* get_MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34() const { return ___MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34; }
	inline String_t** get_address_of_MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34() { return &___MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34; }
	inline void set_MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34(String_t* value)
	{
		___MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CHILDPARTSLIST_PARENTPARTS_ID_34, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35)); }
	inline String_t* get_MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35() const { return ___MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35; }
	inline String_t** get_address_of_MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35() { return &___MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35; }
	inline void set_MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35(String_t* value)
	{
		___MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CHILDPARTSLIST_PARENTPARTSJOINT_NO_35, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36)); }
	inline String_t* get_MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36() const { return ___MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36; }
	inline String_t** get_address_of_MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36() { return &___MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36; }
	inline void set_MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36(String_t* value)
	{
		___MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CHILDPARTSLIST_CHILDPARTSJPOINT_NO_36, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CHILDPARTSLIST_37() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CHILDPARTSLIST_37)); }
	inline String_t* get_MOBILITY_CHILDPARTSLIST_37() const { return ___MOBILITY_CHILDPARTSLIST_37; }
	inline String_t** get_address_of_MOBILITY_CHILDPARTSLIST_37() { return &___MOBILITY_CHILDPARTSLIST_37; }
	inline void set_MOBILITY_CHILDPARTSLIST_37(String_t* value)
	{
		___MOBILITY_CHILDPARTSLIST_37 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CHILDPARTSLIST_37, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CREATEDUSER_ID_38() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CREATEDUSER_ID_38)); }
	inline String_t* get_MOBILITY_CREATEDUSER_ID_38() const { return ___MOBILITY_CREATEDUSER_ID_38; }
	inline String_t** get_address_of_MOBILITY_CREATEDUSER_ID_38() { return &___MOBILITY_CREATEDUSER_ID_38; }
	inline void set_MOBILITY_CREATEDUSER_ID_38(String_t* value)
	{
		___MOBILITY_CREATEDUSER_ID_38 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CREATEDUSER_ID_38, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CREATEDUSER_NAME_39() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CREATEDUSER_NAME_39)); }
	inline String_t* get_MOBILITY_CREATEDUSER_NAME_39() const { return ___MOBILITY_CREATEDUSER_NAME_39; }
	inline String_t** get_address_of_MOBILITY_CREATEDUSER_NAME_39() { return &___MOBILITY_CREATEDUSER_NAME_39; }
	inline void set_MOBILITY_CREATEDUSER_NAME_39(String_t* value)
	{
		___MOBILITY_CREATEDUSER_NAME_39 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CREATEDUSER_NAME_39, value);
	}

	inline static int32_t get_offset_of_MOBILITY_CREATEDUSER_RANK_40() { return static_cast<int32_t>(offsetof(Mobility_t1401824291_StaticFields, ___MOBILITY_CREATEDUSER_RANK_40)); }
	inline String_t* get_MOBILITY_CREATEDUSER_RANK_40() const { return ___MOBILITY_CREATEDUSER_RANK_40; }
	inline String_t** get_address_of_MOBILITY_CREATEDUSER_RANK_40() { return &___MOBILITY_CREATEDUSER_RANK_40; }
	inline void set_MOBILITY_CREATEDUSER_RANK_40(String_t* value)
	{
		___MOBILITY_CREATEDUSER_RANK_40 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_CREATEDUSER_RANK_40, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
