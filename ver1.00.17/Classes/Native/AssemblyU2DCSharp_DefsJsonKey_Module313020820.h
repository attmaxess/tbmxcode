﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.Module
struct  Module_t313020820  : public Il2CppObject
{
public:

public:
};

struct Module_t313020820_StaticFields
{
public:
	// System.String DefsJsonKey.Module::MODULE_NEW_ID
	String_t* ___MODULE_NEW_ID_0;
	// System.String DefsJsonKey.Module::MODULE_ID
	String_t* ___MODULE_ID_1;
	// System.String DefsJsonKey.Module::MODULE_CREATE_USER_ID
	String_t* ___MODULE_CREATE_USER_ID_2;
	// System.String DefsJsonKey.Module::MODULE_PARENT_ID
	String_t* ___MODULE_PARENT_ID_3;
	// System.String DefsJsonKey.Module::MODULE_COREPARTS_ID
	String_t* ___MODULE_COREPARTS_ID_4;
	// System.String DefsJsonKey.Module::MODULE_COREPARTS_COLOR
	String_t* ___MODULE_COREPARTS_COLOR_5;
	// System.String DefsJsonKey.Module::MODULE_COREPARTS_NAME
	String_t* ___MODULE_COREPARTS_NAME_6;
	// System.String DefsJsonKey.Module::MODULE_COREPARTS_SCALE_X
	String_t* ___MODULE_COREPARTS_SCALE_X_7;
	// System.String DefsJsonKey.Module::MODULE_COREPARTS_SCALE_Y
	String_t* ___MODULE_COREPARTS_SCALE_Y_8;
	// System.String DefsJsonKey.Module::MODULE_COREPARTS_SCALE_Z
	String_t* ___MODULE_COREPARTS_SCALE_Z_9;
	// System.String DefsJsonKey.Module::MODULE_PARENTPARTSCHILD_NAME
	String_t* ___MODULE_PARENTPARTSCHILD_NAME_10;
	// System.String DefsJsonKey.Module::MODULE_MOBILMO_ROTATION_X
	String_t* ___MODULE_MOBILMO_ROTATION_X_11;
	// System.String DefsJsonKey.Module::MODULE_MOBILMO_ROTATION_Y
	String_t* ___MODULE_MOBILMO_ROTATION_Y_12;
	// System.String DefsJsonKey.Module::MODULE_MOBILMO_ROTATION_Z
	String_t* ___MODULE_MOBILMO_ROTATION_Z_13;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTSLIST
	String_t* ___MODULE_CHILDPARTSLIST_14;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_ID
	String_t* ___MODULE_CHILDPARTS_ID_15;
	// System.String DefsJsonKey.Module::MODULE_CHILDOBJ_POSITION_X
	String_t* ___MODULE_CHILDOBJ_POSITION_X_16;
	// System.String DefsJsonKey.Module::MODULE_CHILDOBJ_POSITION_Y
	String_t* ___MODULE_CHILDOBJ_POSITION_Y_17;
	// System.String DefsJsonKey.Module::MODULE_CHILDOBJ_POSITION_Z
	String_t* ___MODULE_CHILDOBJ_POSITION_Z_18;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_COLOR
	String_t* ___MODULE_CHILDPARTS_COLOR_19;
	// System.String DefsJsonKey.Module::MODULE_PARENTPARTS_ID
	String_t* ___MODULE_PARENTPARTS_ID_20;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_NAME
	String_t* ___MODULE_CHILDPARTS_NAME_21;
	// System.String DefsJsonKey.Module::MODULE_PARENTPARTS_NAME
	String_t* ___MODULE_PARENTPARTS_NAME_22;
	// System.String DefsJsonKey.Module::MODULE_PARENTPARTS_JOINT_NO
	String_t* ___MODULE_PARENTPARTS_JOINT_NO_23;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_JOINT_NO
	String_t* ___MODULE_CHILDPARTS_JOINT_NO_24;
	// System.String DefsJsonKey.Module::MODULE_CHILDCENTER_ROTATION_X
	String_t* ___MODULE_CHILDCENTER_ROTATION_X_25;
	// System.String DefsJsonKey.Module::MODULE_CHILDCENTER_ROTATION_Y
	String_t* ___MODULE_CHILDCENTER_ROTATION_Y_26;
	// System.String DefsJsonKey.Module::MODULE_CHILDCENTER_ROTATION_Z
	String_t* ___MODULE_CHILDCENTER_ROTATION_Z_27;
	// System.String DefsJsonKey.Module::MODULE_CHILDCENTER_POSITION_X
	String_t* ___MODULE_CHILDCENTER_POSITION_X_28;
	// System.String DefsJsonKey.Module::MODULE_CHILDCENTER_POSITION_Y
	String_t* ___MODULE_CHILDCENTER_POSITION_Y_29;
	// System.String DefsJsonKey.Module::MODULE_CHILDCENTER_POSITION_Z
	String_t* ___MODULE_CHILDCENTER_POSITION_Z_30;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_POSITION_X
	String_t* ___MODULE_CHILDPARTS_POSITION_X_31;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_POSITION_Y
	String_t* ___MODULE_CHILDPARTS_POSITION_Y_32;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_POSITION_Z
	String_t* ___MODULE_CHILDPARTS_POSITION_Z_33;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_ROTATION_X
	String_t* ___MODULE_CHILDPARTS_ROTATION_X_34;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_ROTATION_Y
	String_t* ___MODULE_CHILDPARTS_ROTATION_Y_35;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_ROTATION_Z
	String_t* ___MODULE_CHILDPARTS_ROTATION_Z_36;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_SCALE_X
	String_t* ___MODULE_CHILDPARTS_SCALE_X_37;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_SCALE_Y
	String_t* ___MODULE_CHILDPARTS_SCALE_Y_38;
	// System.String DefsJsonKey.Module::MODULE_CHILDPARTS_SCALE_Z
	String_t* ___MODULE_CHILDPARTS_SCALE_Z_39;
	// System.String DefsJsonKey.Module::MODULE_CHILDLEAP_ROTATION_X
	String_t* ___MODULE_CHILDLEAP_ROTATION_X_40;
	// System.String DefsJsonKey.Module::MODULE_CHILDLEAP_ROTATION_Y
	String_t* ___MODULE_CHILDLEAP_ROTATION_Y_41;
	// System.String DefsJsonKey.Module::MODULE_CHILDLEAP_ROTATION_Z
	String_t* ___MODULE_CHILDLEAP_ROTATION_Z_42;
	// System.String DefsJsonKey.Module::MODULE_LEAP_FIXED
	String_t* ___MODULE_LEAP_FIXED_43;

public:
	inline static int32_t get_offset_of_MODULE_NEW_ID_0() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_NEW_ID_0)); }
	inline String_t* get_MODULE_NEW_ID_0() const { return ___MODULE_NEW_ID_0; }
	inline String_t** get_address_of_MODULE_NEW_ID_0() { return &___MODULE_NEW_ID_0; }
	inline void set_MODULE_NEW_ID_0(String_t* value)
	{
		___MODULE_NEW_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_NEW_ID_0, value);
	}

	inline static int32_t get_offset_of_MODULE_ID_1() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_ID_1)); }
	inline String_t* get_MODULE_ID_1() const { return ___MODULE_ID_1; }
	inline String_t** get_address_of_MODULE_ID_1() { return &___MODULE_ID_1; }
	inline void set_MODULE_ID_1(String_t* value)
	{
		___MODULE_ID_1 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_ID_1, value);
	}

	inline static int32_t get_offset_of_MODULE_CREATE_USER_ID_2() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CREATE_USER_ID_2)); }
	inline String_t* get_MODULE_CREATE_USER_ID_2() const { return ___MODULE_CREATE_USER_ID_2; }
	inline String_t** get_address_of_MODULE_CREATE_USER_ID_2() { return &___MODULE_CREATE_USER_ID_2; }
	inline void set_MODULE_CREATE_USER_ID_2(String_t* value)
	{
		___MODULE_CREATE_USER_ID_2 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CREATE_USER_ID_2, value);
	}

	inline static int32_t get_offset_of_MODULE_PARENT_ID_3() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_PARENT_ID_3)); }
	inline String_t* get_MODULE_PARENT_ID_3() const { return ___MODULE_PARENT_ID_3; }
	inline String_t** get_address_of_MODULE_PARENT_ID_3() { return &___MODULE_PARENT_ID_3; }
	inline void set_MODULE_PARENT_ID_3(String_t* value)
	{
		___MODULE_PARENT_ID_3 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_PARENT_ID_3, value);
	}

	inline static int32_t get_offset_of_MODULE_COREPARTS_ID_4() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_COREPARTS_ID_4)); }
	inline String_t* get_MODULE_COREPARTS_ID_4() const { return ___MODULE_COREPARTS_ID_4; }
	inline String_t** get_address_of_MODULE_COREPARTS_ID_4() { return &___MODULE_COREPARTS_ID_4; }
	inline void set_MODULE_COREPARTS_ID_4(String_t* value)
	{
		___MODULE_COREPARTS_ID_4 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_COREPARTS_ID_4, value);
	}

	inline static int32_t get_offset_of_MODULE_COREPARTS_COLOR_5() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_COREPARTS_COLOR_5)); }
	inline String_t* get_MODULE_COREPARTS_COLOR_5() const { return ___MODULE_COREPARTS_COLOR_5; }
	inline String_t** get_address_of_MODULE_COREPARTS_COLOR_5() { return &___MODULE_COREPARTS_COLOR_5; }
	inline void set_MODULE_COREPARTS_COLOR_5(String_t* value)
	{
		___MODULE_COREPARTS_COLOR_5 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_COREPARTS_COLOR_5, value);
	}

	inline static int32_t get_offset_of_MODULE_COREPARTS_NAME_6() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_COREPARTS_NAME_6)); }
	inline String_t* get_MODULE_COREPARTS_NAME_6() const { return ___MODULE_COREPARTS_NAME_6; }
	inline String_t** get_address_of_MODULE_COREPARTS_NAME_6() { return &___MODULE_COREPARTS_NAME_6; }
	inline void set_MODULE_COREPARTS_NAME_6(String_t* value)
	{
		___MODULE_COREPARTS_NAME_6 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_COREPARTS_NAME_6, value);
	}

	inline static int32_t get_offset_of_MODULE_COREPARTS_SCALE_X_7() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_COREPARTS_SCALE_X_7)); }
	inline String_t* get_MODULE_COREPARTS_SCALE_X_7() const { return ___MODULE_COREPARTS_SCALE_X_7; }
	inline String_t** get_address_of_MODULE_COREPARTS_SCALE_X_7() { return &___MODULE_COREPARTS_SCALE_X_7; }
	inline void set_MODULE_COREPARTS_SCALE_X_7(String_t* value)
	{
		___MODULE_COREPARTS_SCALE_X_7 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_COREPARTS_SCALE_X_7, value);
	}

	inline static int32_t get_offset_of_MODULE_COREPARTS_SCALE_Y_8() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_COREPARTS_SCALE_Y_8)); }
	inline String_t* get_MODULE_COREPARTS_SCALE_Y_8() const { return ___MODULE_COREPARTS_SCALE_Y_8; }
	inline String_t** get_address_of_MODULE_COREPARTS_SCALE_Y_8() { return &___MODULE_COREPARTS_SCALE_Y_8; }
	inline void set_MODULE_COREPARTS_SCALE_Y_8(String_t* value)
	{
		___MODULE_COREPARTS_SCALE_Y_8 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_COREPARTS_SCALE_Y_8, value);
	}

	inline static int32_t get_offset_of_MODULE_COREPARTS_SCALE_Z_9() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_COREPARTS_SCALE_Z_9)); }
	inline String_t* get_MODULE_COREPARTS_SCALE_Z_9() const { return ___MODULE_COREPARTS_SCALE_Z_9; }
	inline String_t** get_address_of_MODULE_COREPARTS_SCALE_Z_9() { return &___MODULE_COREPARTS_SCALE_Z_9; }
	inline void set_MODULE_COREPARTS_SCALE_Z_9(String_t* value)
	{
		___MODULE_COREPARTS_SCALE_Z_9 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_COREPARTS_SCALE_Z_9, value);
	}

	inline static int32_t get_offset_of_MODULE_PARENTPARTSCHILD_NAME_10() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_PARENTPARTSCHILD_NAME_10)); }
	inline String_t* get_MODULE_PARENTPARTSCHILD_NAME_10() const { return ___MODULE_PARENTPARTSCHILD_NAME_10; }
	inline String_t** get_address_of_MODULE_PARENTPARTSCHILD_NAME_10() { return &___MODULE_PARENTPARTSCHILD_NAME_10; }
	inline void set_MODULE_PARENTPARTSCHILD_NAME_10(String_t* value)
	{
		___MODULE_PARENTPARTSCHILD_NAME_10 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_PARENTPARTSCHILD_NAME_10, value);
	}

	inline static int32_t get_offset_of_MODULE_MOBILMO_ROTATION_X_11() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_MOBILMO_ROTATION_X_11)); }
	inline String_t* get_MODULE_MOBILMO_ROTATION_X_11() const { return ___MODULE_MOBILMO_ROTATION_X_11; }
	inline String_t** get_address_of_MODULE_MOBILMO_ROTATION_X_11() { return &___MODULE_MOBILMO_ROTATION_X_11; }
	inline void set_MODULE_MOBILMO_ROTATION_X_11(String_t* value)
	{
		___MODULE_MOBILMO_ROTATION_X_11 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_MOBILMO_ROTATION_X_11, value);
	}

	inline static int32_t get_offset_of_MODULE_MOBILMO_ROTATION_Y_12() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_MOBILMO_ROTATION_Y_12)); }
	inline String_t* get_MODULE_MOBILMO_ROTATION_Y_12() const { return ___MODULE_MOBILMO_ROTATION_Y_12; }
	inline String_t** get_address_of_MODULE_MOBILMO_ROTATION_Y_12() { return &___MODULE_MOBILMO_ROTATION_Y_12; }
	inline void set_MODULE_MOBILMO_ROTATION_Y_12(String_t* value)
	{
		___MODULE_MOBILMO_ROTATION_Y_12 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_MOBILMO_ROTATION_Y_12, value);
	}

	inline static int32_t get_offset_of_MODULE_MOBILMO_ROTATION_Z_13() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_MOBILMO_ROTATION_Z_13)); }
	inline String_t* get_MODULE_MOBILMO_ROTATION_Z_13() const { return ___MODULE_MOBILMO_ROTATION_Z_13; }
	inline String_t** get_address_of_MODULE_MOBILMO_ROTATION_Z_13() { return &___MODULE_MOBILMO_ROTATION_Z_13; }
	inline void set_MODULE_MOBILMO_ROTATION_Z_13(String_t* value)
	{
		___MODULE_MOBILMO_ROTATION_Z_13 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_MOBILMO_ROTATION_Z_13, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTSLIST_14() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTSLIST_14)); }
	inline String_t* get_MODULE_CHILDPARTSLIST_14() const { return ___MODULE_CHILDPARTSLIST_14; }
	inline String_t** get_address_of_MODULE_CHILDPARTSLIST_14() { return &___MODULE_CHILDPARTSLIST_14; }
	inline void set_MODULE_CHILDPARTSLIST_14(String_t* value)
	{
		___MODULE_CHILDPARTSLIST_14 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTSLIST_14, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_ID_15() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_ID_15)); }
	inline String_t* get_MODULE_CHILDPARTS_ID_15() const { return ___MODULE_CHILDPARTS_ID_15; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_ID_15() { return &___MODULE_CHILDPARTS_ID_15; }
	inline void set_MODULE_CHILDPARTS_ID_15(String_t* value)
	{
		___MODULE_CHILDPARTS_ID_15 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_ID_15, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDOBJ_POSITION_X_16() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDOBJ_POSITION_X_16)); }
	inline String_t* get_MODULE_CHILDOBJ_POSITION_X_16() const { return ___MODULE_CHILDOBJ_POSITION_X_16; }
	inline String_t** get_address_of_MODULE_CHILDOBJ_POSITION_X_16() { return &___MODULE_CHILDOBJ_POSITION_X_16; }
	inline void set_MODULE_CHILDOBJ_POSITION_X_16(String_t* value)
	{
		___MODULE_CHILDOBJ_POSITION_X_16 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDOBJ_POSITION_X_16, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDOBJ_POSITION_Y_17() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDOBJ_POSITION_Y_17)); }
	inline String_t* get_MODULE_CHILDOBJ_POSITION_Y_17() const { return ___MODULE_CHILDOBJ_POSITION_Y_17; }
	inline String_t** get_address_of_MODULE_CHILDOBJ_POSITION_Y_17() { return &___MODULE_CHILDOBJ_POSITION_Y_17; }
	inline void set_MODULE_CHILDOBJ_POSITION_Y_17(String_t* value)
	{
		___MODULE_CHILDOBJ_POSITION_Y_17 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDOBJ_POSITION_Y_17, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDOBJ_POSITION_Z_18() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDOBJ_POSITION_Z_18)); }
	inline String_t* get_MODULE_CHILDOBJ_POSITION_Z_18() const { return ___MODULE_CHILDOBJ_POSITION_Z_18; }
	inline String_t** get_address_of_MODULE_CHILDOBJ_POSITION_Z_18() { return &___MODULE_CHILDOBJ_POSITION_Z_18; }
	inline void set_MODULE_CHILDOBJ_POSITION_Z_18(String_t* value)
	{
		___MODULE_CHILDOBJ_POSITION_Z_18 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDOBJ_POSITION_Z_18, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_COLOR_19() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_COLOR_19)); }
	inline String_t* get_MODULE_CHILDPARTS_COLOR_19() const { return ___MODULE_CHILDPARTS_COLOR_19; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_COLOR_19() { return &___MODULE_CHILDPARTS_COLOR_19; }
	inline void set_MODULE_CHILDPARTS_COLOR_19(String_t* value)
	{
		___MODULE_CHILDPARTS_COLOR_19 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_COLOR_19, value);
	}

	inline static int32_t get_offset_of_MODULE_PARENTPARTS_ID_20() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_PARENTPARTS_ID_20)); }
	inline String_t* get_MODULE_PARENTPARTS_ID_20() const { return ___MODULE_PARENTPARTS_ID_20; }
	inline String_t** get_address_of_MODULE_PARENTPARTS_ID_20() { return &___MODULE_PARENTPARTS_ID_20; }
	inline void set_MODULE_PARENTPARTS_ID_20(String_t* value)
	{
		___MODULE_PARENTPARTS_ID_20 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_PARENTPARTS_ID_20, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_NAME_21() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_NAME_21)); }
	inline String_t* get_MODULE_CHILDPARTS_NAME_21() const { return ___MODULE_CHILDPARTS_NAME_21; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_NAME_21() { return &___MODULE_CHILDPARTS_NAME_21; }
	inline void set_MODULE_CHILDPARTS_NAME_21(String_t* value)
	{
		___MODULE_CHILDPARTS_NAME_21 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_NAME_21, value);
	}

	inline static int32_t get_offset_of_MODULE_PARENTPARTS_NAME_22() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_PARENTPARTS_NAME_22)); }
	inline String_t* get_MODULE_PARENTPARTS_NAME_22() const { return ___MODULE_PARENTPARTS_NAME_22; }
	inline String_t** get_address_of_MODULE_PARENTPARTS_NAME_22() { return &___MODULE_PARENTPARTS_NAME_22; }
	inline void set_MODULE_PARENTPARTS_NAME_22(String_t* value)
	{
		___MODULE_PARENTPARTS_NAME_22 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_PARENTPARTS_NAME_22, value);
	}

	inline static int32_t get_offset_of_MODULE_PARENTPARTS_JOINT_NO_23() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_PARENTPARTS_JOINT_NO_23)); }
	inline String_t* get_MODULE_PARENTPARTS_JOINT_NO_23() const { return ___MODULE_PARENTPARTS_JOINT_NO_23; }
	inline String_t** get_address_of_MODULE_PARENTPARTS_JOINT_NO_23() { return &___MODULE_PARENTPARTS_JOINT_NO_23; }
	inline void set_MODULE_PARENTPARTS_JOINT_NO_23(String_t* value)
	{
		___MODULE_PARENTPARTS_JOINT_NO_23 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_PARENTPARTS_JOINT_NO_23, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_JOINT_NO_24() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_JOINT_NO_24)); }
	inline String_t* get_MODULE_CHILDPARTS_JOINT_NO_24() const { return ___MODULE_CHILDPARTS_JOINT_NO_24; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_JOINT_NO_24() { return &___MODULE_CHILDPARTS_JOINT_NO_24; }
	inline void set_MODULE_CHILDPARTS_JOINT_NO_24(String_t* value)
	{
		___MODULE_CHILDPARTS_JOINT_NO_24 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_JOINT_NO_24, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDCENTER_ROTATION_X_25() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDCENTER_ROTATION_X_25)); }
	inline String_t* get_MODULE_CHILDCENTER_ROTATION_X_25() const { return ___MODULE_CHILDCENTER_ROTATION_X_25; }
	inline String_t** get_address_of_MODULE_CHILDCENTER_ROTATION_X_25() { return &___MODULE_CHILDCENTER_ROTATION_X_25; }
	inline void set_MODULE_CHILDCENTER_ROTATION_X_25(String_t* value)
	{
		___MODULE_CHILDCENTER_ROTATION_X_25 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDCENTER_ROTATION_X_25, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDCENTER_ROTATION_Y_26() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDCENTER_ROTATION_Y_26)); }
	inline String_t* get_MODULE_CHILDCENTER_ROTATION_Y_26() const { return ___MODULE_CHILDCENTER_ROTATION_Y_26; }
	inline String_t** get_address_of_MODULE_CHILDCENTER_ROTATION_Y_26() { return &___MODULE_CHILDCENTER_ROTATION_Y_26; }
	inline void set_MODULE_CHILDCENTER_ROTATION_Y_26(String_t* value)
	{
		___MODULE_CHILDCENTER_ROTATION_Y_26 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDCENTER_ROTATION_Y_26, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDCENTER_ROTATION_Z_27() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDCENTER_ROTATION_Z_27)); }
	inline String_t* get_MODULE_CHILDCENTER_ROTATION_Z_27() const { return ___MODULE_CHILDCENTER_ROTATION_Z_27; }
	inline String_t** get_address_of_MODULE_CHILDCENTER_ROTATION_Z_27() { return &___MODULE_CHILDCENTER_ROTATION_Z_27; }
	inline void set_MODULE_CHILDCENTER_ROTATION_Z_27(String_t* value)
	{
		___MODULE_CHILDCENTER_ROTATION_Z_27 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDCENTER_ROTATION_Z_27, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDCENTER_POSITION_X_28() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDCENTER_POSITION_X_28)); }
	inline String_t* get_MODULE_CHILDCENTER_POSITION_X_28() const { return ___MODULE_CHILDCENTER_POSITION_X_28; }
	inline String_t** get_address_of_MODULE_CHILDCENTER_POSITION_X_28() { return &___MODULE_CHILDCENTER_POSITION_X_28; }
	inline void set_MODULE_CHILDCENTER_POSITION_X_28(String_t* value)
	{
		___MODULE_CHILDCENTER_POSITION_X_28 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDCENTER_POSITION_X_28, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDCENTER_POSITION_Y_29() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDCENTER_POSITION_Y_29)); }
	inline String_t* get_MODULE_CHILDCENTER_POSITION_Y_29() const { return ___MODULE_CHILDCENTER_POSITION_Y_29; }
	inline String_t** get_address_of_MODULE_CHILDCENTER_POSITION_Y_29() { return &___MODULE_CHILDCENTER_POSITION_Y_29; }
	inline void set_MODULE_CHILDCENTER_POSITION_Y_29(String_t* value)
	{
		___MODULE_CHILDCENTER_POSITION_Y_29 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDCENTER_POSITION_Y_29, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDCENTER_POSITION_Z_30() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDCENTER_POSITION_Z_30)); }
	inline String_t* get_MODULE_CHILDCENTER_POSITION_Z_30() const { return ___MODULE_CHILDCENTER_POSITION_Z_30; }
	inline String_t** get_address_of_MODULE_CHILDCENTER_POSITION_Z_30() { return &___MODULE_CHILDCENTER_POSITION_Z_30; }
	inline void set_MODULE_CHILDCENTER_POSITION_Z_30(String_t* value)
	{
		___MODULE_CHILDCENTER_POSITION_Z_30 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDCENTER_POSITION_Z_30, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_POSITION_X_31() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_POSITION_X_31)); }
	inline String_t* get_MODULE_CHILDPARTS_POSITION_X_31() const { return ___MODULE_CHILDPARTS_POSITION_X_31; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_POSITION_X_31() { return &___MODULE_CHILDPARTS_POSITION_X_31; }
	inline void set_MODULE_CHILDPARTS_POSITION_X_31(String_t* value)
	{
		___MODULE_CHILDPARTS_POSITION_X_31 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_POSITION_X_31, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_POSITION_Y_32() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_POSITION_Y_32)); }
	inline String_t* get_MODULE_CHILDPARTS_POSITION_Y_32() const { return ___MODULE_CHILDPARTS_POSITION_Y_32; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_POSITION_Y_32() { return &___MODULE_CHILDPARTS_POSITION_Y_32; }
	inline void set_MODULE_CHILDPARTS_POSITION_Y_32(String_t* value)
	{
		___MODULE_CHILDPARTS_POSITION_Y_32 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_POSITION_Y_32, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_POSITION_Z_33() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_POSITION_Z_33)); }
	inline String_t* get_MODULE_CHILDPARTS_POSITION_Z_33() const { return ___MODULE_CHILDPARTS_POSITION_Z_33; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_POSITION_Z_33() { return &___MODULE_CHILDPARTS_POSITION_Z_33; }
	inline void set_MODULE_CHILDPARTS_POSITION_Z_33(String_t* value)
	{
		___MODULE_CHILDPARTS_POSITION_Z_33 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_POSITION_Z_33, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_ROTATION_X_34() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_ROTATION_X_34)); }
	inline String_t* get_MODULE_CHILDPARTS_ROTATION_X_34() const { return ___MODULE_CHILDPARTS_ROTATION_X_34; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_ROTATION_X_34() { return &___MODULE_CHILDPARTS_ROTATION_X_34; }
	inline void set_MODULE_CHILDPARTS_ROTATION_X_34(String_t* value)
	{
		___MODULE_CHILDPARTS_ROTATION_X_34 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_ROTATION_X_34, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_ROTATION_Y_35() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_ROTATION_Y_35)); }
	inline String_t* get_MODULE_CHILDPARTS_ROTATION_Y_35() const { return ___MODULE_CHILDPARTS_ROTATION_Y_35; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_ROTATION_Y_35() { return &___MODULE_CHILDPARTS_ROTATION_Y_35; }
	inline void set_MODULE_CHILDPARTS_ROTATION_Y_35(String_t* value)
	{
		___MODULE_CHILDPARTS_ROTATION_Y_35 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_ROTATION_Y_35, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_ROTATION_Z_36() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_ROTATION_Z_36)); }
	inline String_t* get_MODULE_CHILDPARTS_ROTATION_Z_36() const { return ___MODULE_CHILDPARTS_ROTATION_Z_36; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_ROTATION_Z_36() { return &___MODULE_CHILDPARTS_ROTATION_Z_36; }
	inline void set_MODULE_CHILDPARTS_ROTATION_Z_36(String_t* value)
	{
		___MODULE_CHILDPARTS_ROTATION_Z_36 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_ROTATION_Z_36, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_SCALE_X_37() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_SCALE_X_37)); }
	inline String_t* get_MODULE_CHILDPARTS_SCALE_X_37() const { return ___MODULE_CHILDPARTS_SCALE_X_37; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_SCALE_X_37() { return &___MODULE_CHILDPARTS_SCALE_X_37; }
	inline void set_MODULE_CHILDPARTS_SCALE_X_37(String_t* value)
	{
		___MODULE_CHILDPARTS_SCALE_X_37 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_SCALE_X_37, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_SCALE_Y_38() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_SCALE_Y_38)); }
	inline String_t* get_MODULE_CHILDPARTS_SCALE_Y_38() const { return ___MODULE_CHILDPARTS_SCALE_Y_38; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_SCALE_Y_38() { return &___MODULE_CHILDPARTS_SCALE_Y_38; }
	inline void set_MODULE_CHILDPARTS_SCALE_Y_38(String_t* value)
	{
		___MODULE_CHILDPARTS_SCALE_Y_38 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_SCALE_Y_38, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDPARTS_SCALE_Z_39() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDPARTS_SCALE_Z_39)); }
	inline String_t* get_MODULE_CHILDPARTS_SCALE_Z_39() const { return ___MODULE_CHILDPARTS_SCALE_Z_39; }
	inline String_t** get_address_of_MODULE_CHILDPARTS_SCALE_Z_39() { return &___MODULE_CHILDPARTS_SCALE_Z_39; }
	inline void set_MODULE_CHILDPARTS_SCALE_Z_39(String_t* value)
	{
		___MODULE_CHILDPARTS_SCALE_Z_39 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDPARTS_SCALE_Z_39, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDLEAP_ROTATION_X_40() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDLEAP_ROTATION_X_40)); }
	inline String_t* get_MODULE_CHILDLEAP_ROTATION_X_40() const { return ___MODULE_CHILDLEAP_ROTATION_X_40; }
	inline String_t** get_address_of_MODULE_CHILDLEAP_ROTATION_X_40() { return &___MODULE_CHILDLEAP_ROTATION_X_40; }
	inline void set_MODULE_CHILDLEAP_ROTATION_X_40(String_t* value)
	{
		___MODULE_CHILDLEAP_ROTATION_X_40 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDLEAP_ROTATION_X_40, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDLEAP_ROTATION_Y_41() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDLEAP_ROTATION_Y_41)); }
	inline String_t* get_MODULE_CHILDLEAP_ROTATION_Y_41() const { return ___MODULE_CHILDLEAP_ROTATION_Y_41; }
	inline String_t** get_address_of_MODULE_CHILDLEAP_ROTATION_Y_41() { return &___MODULE_CHILDLEAP_ROTATION_Y_41; }
	inline void set_MODULE_CHILDLEAP_ROTATION_Y_41(String_t* value)
	{
		___MODULE_CHILDLEAP_ROTATION_Y_41 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDLEAP_ROTATION_Y_41, value);
	}

	inline static int32_t get_offset_of_MODULE_CHILDLEAP_ROTATION_Z_42() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_CHILDLEAP_ROTATION_Z_42)); }
	inline String_t* get_MODULE_CHILDLEAP_ROTATION_Z_42() const { return ___MODULE_CHILDLEAP_ROTATION_Z_42; }
	inline String_t** get_address_of_MODULE_CHILDLEAP_ROTATION_Z_42() { return &___MODULE_CHILDLEAP_ROTATION_Z_42; }
	inline void set_MODULE_CHILDLEAP_ROTATION_Z_42(String_t* value)
	{
		___MODULE_CHILDLEAP_ROTATION_Z_42 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_CHILDLEAP_ROTATION_Z_42, value);
	}

	inline static int32_t get_offset_of_MODULE_LEAP_FIXED_43() { return static_cast<int32_t>(offsetof(Module_t313020820_StaticFields, ___MODULE_LEAP_FIXED_43)); }
	inline String_t* get_MODULE_LEAP_FIXED_43() const { return ___MODULE_LEAP_FIXED_43; }
	inline String_t** get_address_of_MODULE_LEAP_FIXED_43() { return &___MODULE_LEAP_FIXED_43; }
	inline void set_MODULE_LEAP_FIXED_43(String_t* value)
	{
		___MODULE_LEAP_FIXED_43 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_LEAP_FIXED_43, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
