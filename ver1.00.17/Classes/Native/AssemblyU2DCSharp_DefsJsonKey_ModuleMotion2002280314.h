﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.ModuleMotion
struct  ModuleMotion_t2002280314  : public Il2CppObject
{
public:

public:
};

struct ModuleMotion_t2002280314_StaticFields
{
public:
	// System.String DefsJsonKey.ModuleMotion::MODULE_REGISTERMOTION
	String_t* ___MODULE_REGISTERMOTION_0;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MODULE_ID
	String_t* ___MOTION_ITEM_MODULE_ID_1;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_ACTION_ID
	String_t* ___MOTION_ITEM_ACTION_ID_2;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MOTION_ID
	String_t* ___MOTION_ITEM_MOTION_ID_3;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MOTION_MOTION_DATA
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_4;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8;
	// System.String DefsJsonKey.ModuleMotion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9;

public:
	inline static int32_t get_offset_of_MODULE_REGISTERMOTION_0() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MODULE_REGISTERMOTION_0)); }
	inline String_t* get_MODULE_REGISTERMOTION_0() const { return ___MODULE_REGISTERMOTION_0; }
	inline String_t** get_address_of_MODULE_REGISTERMOTION_0() { return &___MODULE_REGISTERMOTION_0; }
	inline void set_MODULE_REGISTERMOTION_0(String_t* value)
	{
		___MODULE_REGISTERMOTION_0 = value;
		Il2CppCodeGenWriteBarrier(&___MODULE_REGISTERMOTION_0, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MODULE_ID_1() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MODULE_ID_1)); }
	inline String_t* get_MOTION_ITEM_MODULE_ID_1() const { return ___MOTION_ITEM_MODULE_ID_1; }
	inline String_t** get_address_of_MOTION_ITEM_MODULE_ID_1() { return &___MOTION_ITEM_MODULE_ID_1; }
	inline void set_MOTION_ITEM_MODULE_ID_1(String_t* value)
	{
		___MOTION_ITEM_MODULE_ID_1 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MODULE_ID_1, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_ACTION_ID_2() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_ACTION_ID_2)); }
	inline String_t* get_MOTION_ITEM_ACTION_ID_2() const { return ___MOTION_ITEM_ACTION_ID_2; }
	inline String_t** get_address_of_MOTION_ITEM_ACTION_ID_2() { return &___MOTION_ITEM_ACTION_ID_2; }
	inline void set_MOTION_ITEM_ACTION_ID_2(String_t* value)
	{
		___MOTION_ITEM_ACTION_ID_2 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_ACTION_ID_2, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_ID_3() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MOTION_ID_3)); }
	inline String_t* get_MOTION_ITEM_MOTION_ID_3() const { return ___MOTION_ITEM_MOTION_ID_3; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_ID_3() { return &___MOTION_ITEM_MOTION_ID_3; }
	inline void set_MOTION_ITEM_MOTION_ID_3(String_t* value)
	{
		___MOTION_ITEM_MOTION_ID_3 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_ID_3, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_4() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_4)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_4() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_4; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_4() { return &___MOTION_ITEM_MOTION_MOTION_DATA_4; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_4(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_4 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_4, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9() { return static_cast<int32_t>(offsetof(ModuleMotion_t2002280314_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
