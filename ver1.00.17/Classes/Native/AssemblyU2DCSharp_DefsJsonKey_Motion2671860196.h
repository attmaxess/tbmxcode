﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.Motion
struct  Motion_t2671860196  : public Il2CppObject
{
public:

public:
};

struct Motion_t2671860196_StaticFields
{
public:
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOBILITY_ID
	String_t* ___MOTION_ITEM_MOBILITY_ID_0;
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOTION_ID
	String_t* ___MOTION_ITEM_MOTION_ID_1;
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOTION_MOTION_DATA
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_2;
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3;
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4;
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5;
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6;
	// System.String DefsJsonKey.Motion::MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE
	String_t* ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7;

public:
	inline static int32_t get_offset_of_MOTION_ITEM_MOBILITY_ID_0() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOBILITY_ID_0)); }
	inline String_t* get_MOTION_ITEM_MOBILITY_ID_0() const { return ___MOTION_ITEM_MOBILITY_ID_0; }
	inline String_t** get_address_of_MOTION_ITEM_MOBILITY_ID_0() { return &___MOTION_ITEM_MOBILITY_ID_0; }
	inline void set_MOTION_ITEM_MOBILITY_ID_0(String_t* value)
	{
		___MOTION_ITEM_MOBILITY_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOBILITY_ID_0, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_ID_1() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOTION_ID_1)); }
	inline String_t* get_MOTION_ITEM_MOTION_ID_1() const { return ___MOTION_ITEM_MOTION_ID_1; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_ID_1() { return &___MOTION_ITEM_MOTION_ID_1; }
	inline void set_MOTION_ITEM_MOTION_ID_1(String_t* value)
	{
		___MOTION_ITEM_MOTION_ID_1 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_ID_1, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_2() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_2)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_2() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_2; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_2() { return &___MOTION_ITEM_MOTION_MOTION_DATA_2; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_2(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_2 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_2, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_3, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_4, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_5, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_6, value);
	}

	inline static int32_t get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7() { return static_cast<int32_t>(offsetof(Motion_t2671860196_StaticFields, ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7)); }
	inline String_t* get_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7() const { return ___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7; }
	inline String_t** get_address_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7() { return &___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7; }
	inline void set_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7(String_t* value)
	{
		___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7 = value;
		Il2CppCodeGenWriteBarrier(&___MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
