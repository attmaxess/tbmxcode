﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.NgWord
struct  NgWord_t450572389  : public Il2CppObject
{
public:

public:
};

struct NgWord_t450572389_StaticFields
{
public:
	// System.String DefsJsonKey.NgWord::NG_WORD
	String_t* ___NG_WORD_0;

public:
	inline static int32_t get_offset_of_NG_WORD_0() { return static_cast<int32_t>(offsetof(NgWord_t450572389_StaticFields, ___NG_WORD_0)); }
	inline String_t* get_NG_WORD_0() const { return ___NG_WORD_0; }
	inline String_t** get_address_of_NG_WORD_0() { return &___NG_WORD_0; }
	inline void set_NG_WORD_0(String_t* value)
	{
		___NG_WORD_0 = value;
		Il2CppCodeGenWriteBarrier(&___NG_WORD_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
