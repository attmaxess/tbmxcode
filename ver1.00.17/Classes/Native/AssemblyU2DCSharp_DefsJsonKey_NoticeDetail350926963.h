﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.NoticeDetail
struct  NoticeDetail_t350926963  : public Il2CppObject
{
public:

public:
};

struct NoticeDetail_t350926963_StaticFields
{
public:
	// System.String DefsJsonKey.NoticeDetail::NOTICE_DETAIL_NOTICE_ID
	String_t* ___NOTICE_DETAIL_NOTICE_ID_0;
	// System.String DefsJsonKey.NoticeDetail::NOTICE_DETAIL_TITLE
	String_t* ___NOTICE_DETAIL_TITLE_1;
	// System.String DefsJsonKey.NoticeDetail::NOTICE_DETAIL_NOTICE
	String_t* ___NOTICE_DETAIL_NOTICE_2;
	// System.String DefsJsonKey.NoticeDetail::NOTICE_DETAIL_URL
	String_t* ___NOTICE_DETAIL_URL_3;
	// System.String DefsJsonKey.NoticeDetail::NOTICE_DETAIL_CREATEDAT
	String_t* ___NOTICE_DETAIL_CREATEDAT_4;

public:
	inline static int32_t get_offset_of_NOTICE_DETAIL_NOTICE_ID_0() { return static_cast<int32_t>(offsetof(NoticeDetail_t350926963_StaticFields, ___NOTICE_DETAIL_NOTICE_ID_0)); }
	inline String_t* get_NOTICE_DETAIL_NOTICE_ID_0() const { return ___NOTICE_DETAIL_NOTICE_ID_0; }
	inline String_t** get_address_of_NOTICE_DETAIL_NOTICE_ID_0() { return &___NOTICE_DETAIL_NOTICE_ID_0; }
	inline void set_NOTICE_DETAIL_NOTICE_ID_0(String_t* value)
	{
		___NOTICE_DETAIL_NOTICE_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___NOTICE_DETAIL_NOTICE_ID_0, value);
	}

	inline static int32_t get_offset_of_NOTICE_DETAIL_TITLE_1() { return static_cast<int32_t>(offsetof(NoticeDetail_t350926963_StaticFields, ___NOTICE_DETAIL_TITLE_1)); }
	inline String_t* get_NOTICE_DETAIL_TITLE_1() const { return ___NOTICE_DETAIL_TITLE_1; }
	inline String_t** get_address_of_NOTICE_DETAIL_TITLE_1() { return &___NOTICE_DETAIL_TITLE_1; }
	inline void set_NOTICE_DETAIL_TITLE_1(String_t* value)
	{
		___NOTICE_DETAIL_TITLE_1 = value;
		Il2CppCodeGenWriteBarrier(&___NOTICE_DETAIL_TITLE_1, value);
	}

	inline static int32_t get_offset_of_NOTICE_DETAIL_NOTICE_2() { return static_cast<int32_t>(offsetof(NoticeDetail_t350926963_StaticFields, ___NOTICE_DETAIL_NOTICE_2)); }
	inline String_t* get_NOTICE_DETAIL_NOTICE_2() const { return ___NOTICE_DETAIL_NOTICE_2; }
	inline String_t** get_address_of_NOTICE_DETAIL_NOTICE_2() { return &___NOTICE_DETAIL_NOTICE_2; }
	inline void set_NOTICE_DETAIL_NOTICE_2(String_t* value)
	{
		___NOTICE_DETAIL_NOTICE_2 = value;
		Il2CppCodeGenWriteBarrier(&___NOTICE_DETAIL_NOTICE_2, value);
	}

	inline static int32_t get_offset_of_NOTICE_DETAIL_URL_3() { return static_cast<int32_t>(offsetof(NoticeDetail_t350926963_StaticFields, ___NOTICE_DETAIL_URL_3)); }
	inline String_t* get_NOTICE_DETAIL_URL_3() const { return ___NOTICE_DETAIL_URL_3; }
	inline String_t** get_address_of_NOTICE_DETAIL_URL_3() { return &___NOTICE_DETAIL_URL_3; }
	inline void set_NOTICE_DETAIL_URL_3(String_t* value)
	{
		___NOTICE_DETAIL_URL_3 = value;
		Il2CppCodeGenWriteBarrier(&___NOTICE_DETAIL_URL_3, value);
	}

	inline static int32_t get_offset_of_NOTICE_DETAIL_CREATEDAT_4() { return static_cast<int32_t>(offsetof(NoticeDetail_t350926963_StaticFields, ___NOTICE_DETAIL_CREATEDAT_4)); }
	inline String_t* get_NOTICE_DETAIL_CREATEDAT_4() const { return ___NOTICE_DETAIL_CREATEDAT_4; }
	inline String_t** get_address_of_NOTICE_DETAIL_CREATEDAT_4() { return &___NOTICE_DETAIL_CREATEDAT_4; }
	inline void set_NOTICE_DETAIL_CREATEDAT_4(String_t* value)
	{
		___NOTICE_DETAIL_CREATEDAT_4 = value;
		Il2CppCodeGenWriteBarrier(&___NOTICE_DETAIL_CREATEDAT_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
