﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.PartsCategory
struct  PartsCategory_t617187210  : public Il2CppObject
{
public:

public:
};

struct PartsCategory_t617187210_StaticFields
{
public:
	// System.String DefsJsonKey.PartsCategory::PARTSCATEGORY_PARTSCAT_ID
	String_t* ___PARTSCATEGORY_PARTSCAT_ID_0;
	// System.String DefsJsonKey.PartsCategory::PARTSCATEGORY_PARTSCAT_NAME
	String_t* ___PARTSCATEGORY_PARTSCAT_NAME_1;

public:
	inline static int32_t get_offset_of_PARTSCATEGORY_PARTSCAT_ID_0() { return static_cast<int32_t>(offsetof(PartsCategory_t617187210_StaticFields, ___PARTSCATEGORY_PARTSCAT_ID_0)); }
	inline String_t* get_PARTSCATEGORY_PARTSCAT_ID_0() const { return ___PARTSCATEGORY_PARTSCAT_ID_0; }
	inline String_t** get_address_of_PARTSCATEGORY_PARTSCAT_ID_0() { return &___PARTSCATEGORY_PARTSCAT_ID_0; }
	inline void set_PARTSCATEGORY_PARTSCAT_ID_0(String_t* value)
	{
		___PARTSCATEGORY_PARTSCAT_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___PARTSCATEGORY_PARTSCAT_ID_0, value);
	}

	inline static int32_t get_offset_of_PARTSCATEGORY_PARTSCAT_NAME_1() { return static_cast<int32_t>(offsetof(PartsCategory_t617187210_StaticFields, ___PARTSCATEGORY_PARTSCAT_NAME_1)); }
	inline String_t* get_PARTSCATEGORY_PARTSCAT_NAME_1() const { return ___PARTSCATEGORY_PARTSCAT_NAME_1; }
	inline String_t** get_address_of_PARTSCATEGORY_PARTSCAT_NAME_1() { return &___PARTSCATEGORY_PARTSCAT_NAME_1; }
	inline void set_PARTSCATEGORY_PARTSCAT_NAME_1(String_t* value)
	{
		___PARTSCATEGORY_PARTSCAT_NAME_1 = value;
		Il2CppCodeGenWriteBarrier(&___PARTSCATEGORY_PARTSCAT_NAME_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
