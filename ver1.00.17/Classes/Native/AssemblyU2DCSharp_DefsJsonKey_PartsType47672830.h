﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.PartsType
struct  PartsType_t47672830  : public Il2CppObject
{
public:

public:
};

struct PartsType_t47672830_StaticFields
{
public:
	// System.String DefsJsonKey.PartsType::PARTSTYPE_INFO
	String_t* ___PARTSTYPE_INFO_0;
	// System.String DefsJsonKey.PartsType::PARTS_ID
	String_t* ___PARTS_ID_1;
	// System.String DefsJsonKey.PartsType::PARTS_NAME
	String_t* ___PARTS_NAME_2;
	// System.String DefsJsonKey.PartsType::PARTS_DESCRIPTION
	String_t* ___PARTS_DESCRIPTION_3;
	// System.String DefsJsonKey.PartsType::PARTS_CATEGORY
	String_t* ___PARTS_CATEGORY_4;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_MASS
	String_t* ___PARTS_PARAMETER_MASS_5;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_FORCE
	String_t* ___PARTS_PARAMETER_FORCE_6;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_BOUNCE
	String_t* ___PARTS_PARAMETER_BOUNCE_7;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_FLOATFORCE
	String_t* ___PARTS_PARAMETER_FLOATFORCE_8;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_FLOATBOUNCE
	String_t* ___PARTS_PARAMETER_FLOATBOUNCE_9;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_FLOATFRICTION
	String_t* ___PARTS_PARAMETER_FLOATFRICTION_10;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_FRICTION
	String_t* ___PARTS_PARAMETER_FRICTION_11;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_ELASTICITYSPEED
	String_t* ___PARTS_PARAMETER_ELASTICITYSPEED_12;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_ROTATIONSPEED
	String_t* ___PARTS_PARAMETER_ROTATIONSPEED_13;
	// System.String DefsJsonKey.PartsType::PARTS_PARAMETER_INFO
	String_t* ___PARTS_PARAMETER_INFO_14;

public:
	inline static int32_t get_offset_of_PARTSTYPE_INFO_0() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTSTYPE_INFO_0)); }
	inline String_t* get_PARTSTYPE_INFO_0() const { return ___PARTSTYPE_INFO_0; }
	inline String_t** get_address_of_PARTSTYPE_INFO_0() { return &___PARTSTYPE_INFO_0; }
	inline void set_PARTSTYPE_INFO_0(String_t* value)
	{
		___PARTSTYPE_INFO_0 = value;
		Il2CppCodeGenWriteBarrier(&___PARTSTYPE_INFO_0, value);
	}

	inline static int32_t get_offset_of_PARTS_ID_1() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_ID_1)); }
	inline String_t* get_PARTS_ID_1() const { return ___PARTS_ID_1; }
	inline String_t** get_address_of_PARTS_ID_1() { return &___PARTS_ID_1; }
	inline void set_PARTS_ID_1(String_t* value)
	{
		___PARTS_ID_1 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_ID_1, value);
	}

	inline static int32_t get_offset_of_PARTS_NAME_2() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_NAME_2)); }
	inline String_t* get_PARTS_NAME_2() const { return ___PARTS_NAME_2; }
	inline String_t** get_address_of_PARTS_NAME_2() { return &___PARTS_NAME_2; }
	inline void set_PARTS_NAME_2(String_t* value)
	{
		___PARTS_NAME_2 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_NAME_2, value);
	}

	inline static int32_t get_offset_of_PARTS_DESCRIPTION_3() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_DESCRIPTION_3)); }
	inline String_t* get_PARTS_DESCRIPTION_3() const { return ___PARTS_DESCRIPTION_3; }
	inline String_t** get_address_of_PARTS_DESCRIPTION_3() { return &___PARTS_DESCRIPTION_3; }
	inline void set_PARTS_DESCRIPTION_3(String_t* value)
	{
		___PARTS_DESCRIPTION_3 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_DESCRIPTION_3, value);
	}

	inline static int32_t get_offset_of_PARTS_CATEGORY_4() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_CATEGORY_4)); }
	inline String_t* get_PARTS_CATEGORY_4() const { return ___PARTS_CATEGORY_4; }
	inline String_t** get_address_of_PARTS_CATEGORY_4() { return &___PARTS_CATEGORY_4; }
	inline void set_PARTS_CATEGORY_4(String_t* value)
	{
		___PARTS_CATEGORY_4 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_CATEGORY_4, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_MASS_5() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_MASS_5)); }
	inline String_t* get_PARTS_PARAMETER_MASS_5() const { return ___PARTS_PARAMETER_MASS_5; }
	inline String_t** get_address_of_PARTS_PARAMETER_MASS_5() { return &___PARTS_PARAMETER_MASS_5; }
	inline void set_PARTS_PARAMETER_MASS_5(String_t* value)
	{
		___PARTS_PARAMETER_MASS_5 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_MASS_5, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_FORCE_6() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_FORCE_6)); }
	inline String_t* get_PARTS_PARAMETER_FORCE_6() const { return ___PARTS_PARAMETER_FORCE_6; }
	inline String_t** get_address_of_PARTS_PARAMETER_FORCE_6() { return &___PARTS_PARAMETER_FORCE_6; }
	inline void set_PARTS_PARAMETER_FORCE_6(String_t* value)
	{
		___PARTS_PARAMETER_FORCE_6 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_FORCE_6, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_BOUNCE_7() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_BOUNCE_7)); }
	inline String_t* get_PARTS_PARAMETER_BOUNCE_7() const { return ___PARTS_PARAMETER_BOUNCE_7; }
	inline String_t** get_address_of_PARTS_PARAMETER_BOUNCE_7() { return &___PARTS_PARAMETER_BOUNCE_7; }
	inline void set_PARTS_PARAMETER_BOUNCE_7(String_t* value)
	{
		___PARTS_PARAMETER_BOUNCE_7 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_BOUNCE_7, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_FLOATFORCE_8() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_FLOATFORCE_8)); }
	inline String_t* get_PARTS_PARAMETER_FLOATFORCE_8() const { return ___PARTS_PARAMETER_FLOATFORCE_8; }
	inline String_t** get_address_of_PARTS_PARAMETER_FLOATFORCE_8() { return &___PARTS_PARAMETER_FLOATFORCE_8; }
	inline void set_PARTS_PARAMETER_FLOATFORCE_8(String_t* value)
	{
		___PARTS_PARAMETER_FLOATFORCE_8 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_FLOATFORCE_8, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_FLOATBOUNCE_9() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_FLOATBOUNCE_9)); }
	inline String_t* get_PARTS_PARAMETER_FLOATBOUNCE_9() const { return ___PARTS_PARAMETER_FLOATBOUNCE_9; }
	inline String_t** get_address_of_PARTS_PARAMETER_FLOATBOUNCE_9() { return &___PARTS_PARAMETER_FLOATBOUNCE_9; }
	inline void set_PARTS_PARAMETER_FLOATBOUNCE_9(String_t* value)
	{
		___PARTS_PARAMETER_FLOATBOUNCE_9 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_FLOATBOUNCE_9, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_FLOATFRICTION_10() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_FLOATFRICTION_10)); }
	inline String_t* get_PARTS_PARAMETER_FLOATFRICTION_10() const { return ___PARTS_PARAMETER_FLOATFRICTION_10; }
	inline String_t** get_address_of_PARTS_PARAMETER_FLOATFRICTION_10() { return &___PARTS_PARAMETER_FLOATFRICTION_10; }
	inline void set_PARTS_PARAMETER_FLOATFRICTION_10(String_t* value)
	{
		___PARTS_PARAMETER_FLOATFRICTION_10 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_FLOATFRICTION_10, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_FRICTION_11() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_FRICTION_11)); }
	inline String_t* get_PARTS_PARAMETER_FRICTION_11() const { return ___PARTS_PARAMETER_FRICTION_11; }
	inline String_t** get_address_of_PARTS_PARAMETER_FRICTION_11() { return &___PARTS_PARAMETER_FRICTION_11; }
	inline void set_PARTS_PARAMETER_FRICTION_11(String_t* value)
	{
		___PARTS_PARAMETER_FRICTION_11 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_FRICTION_11, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_ELASTICITYSPEED_12() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_ELASTICITYSPEED_12)); }
	inline String_t* get_PARTS_PARAMETER_ELASTICITYSPEED_12() const { return ___PARTS_PARAMETER_ELASTICITYSPEED_12; }
	inline String_t** get_address_of_PARTS_PARAMETER_ELASTICITYSPEED_12() { return &___PARTS_PARAMETER_ELASTICITYSPEED_12; }
	inline void set_PARTS_PARAMETER_ELASTICITYSPEED_12(String_t* value)
	{
		___PARTS_PARAMETER_ELASTICITYSPEED_12 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_ELASTICITYSPEED_12, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_ROTATIONSPEED_13() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_ROTATIONSPEED_13)); }
	inline String_t* get_PARTS_PARAMETER_ROTATIONSPEED_13() const { return ___PARTS_PARAMETER_ROTATIONSPEED_13; }
	inline String_t** get_address_of_PARTS_PARAMETER_ROTATIONSPEED_13() { return &___PARTS_PARAMETER_ROTATIONSPEED_13; }
	inline void set_PARTS_PARAMETER_ROTATIONSPEED_13(String_t* value)
	{
		___PARTS_PARAMETER_ROTATIONSPEED_13 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_ROTATIONSPEED_13, value);
	}

	inline static int32_t get_offset_of_PARTS_PARAMETER_INFO_14() { return static_cast<int32_t>(offsetof(PartsType_t47672830_StaticFields, ___PARTS_PARAMETER_INFO_14)); }
	inline String_t* get_PARTS_PARAMETER_INFO_14() const { return ___PARTS_PARAMETER_INFO_14; }
	inline String_t** get_address_of_PARTS_PARAMETER_INFO_14() { return &___PARTS_PARAMETER_INFO_14; }
	inline void set_PARTS_PARAMETER_INFO_14(String_t* value)
	{
		___PARTS_PARAMETER_INFO_14 = value;
		Il2CppCodeGenWriteBarrier(&___PARTS_PARAMETER_INFO_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
