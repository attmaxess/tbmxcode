﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.PostMobilityNew
struct  PostMobilityNew_t1434085915  : public Il2CppObject
{
public:

public:
};

struct PostMobilityNew_t1434085915_StaticFields
{
public:
	// System.String DefsJsonKey.PostMobilityNew::MOBILITY_NEW_ID
	String_t* ___MOBILITY_NEW_ID_0;

public:
	inline static int32_t get_offset_of_MOBILITY_NEW_ID_0() { return static_cast<int32_t>(offsetof(PostMobilityNew_t1434085915_StaticFields, ___MOBILITY_NEW_ID_0)); }
	inline String_t* get_MOBILITY_NEW_ID_0() const { return ___MOBILITY_NEW_ID_0; }
	inline String_t** get_address_of_MOBILITY_NEW_ID_0() { return &___MOBILITY_NEW_ID_0; }
	inline void set_MOBILITY_NEW_ID_0(String_t* value)
	{
		___MOBILITY_NEW_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___MOBILITY_NEW_ID_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
