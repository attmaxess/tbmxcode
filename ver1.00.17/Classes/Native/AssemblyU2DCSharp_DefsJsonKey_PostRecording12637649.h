﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.PostRecording
struct  PostRecording_t12637649  : public Il2CppObject
{
public:

public:
};

struct PostRecording_t12637649_StaticFields
{
public:
	// System.String DefsJsonKey.PostRecording::RECORDED_ID
	String_t* ___RECORDED_ID_0;

public:
	inline static int32_t get_offset_of_RECORDED_ID_0() { return static_cast<int32_t>(offsetof(PostRecording_t12637649_StaticFields, ___RECORDED_ID_0)); }
	inline String_t* get_RECORDED_ID_0() const { return ___RECORDED_ID_0; }
	inline String_t** get_address_of_RECORDED_ID_0() { return &___RECORDED_ID_0; }
	inline void set_RECORDED_ID_0(String_t* value)
	{
		___RECORDED_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDED_ID_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
