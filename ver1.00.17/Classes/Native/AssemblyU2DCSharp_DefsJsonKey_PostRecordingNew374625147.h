﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.PostRecordingNew
struct  PostRecordingNew_t374625147  : public Il2CppObject
{
public:

public:
};

struct PostRecordingNew_t374625147_StaticFields
{
public:
	// System.String DefsJsonKey.PostRecordingNew::RECORDING_NEW_ID
	String_t* ___RECORDING_NEW_ID_0;

public:
	inline static int32_t get_offset_of_RECORDING_NEW_ID_0() { return static_cast<int32_t>(offsetof(PostRecordingNew_t374625147_StaticFields, ___RECORDING_NEW_ID_0)); }
	inline String_t* get_RECORDING_NEW_ID_0() const { return ___RECORDING_NEW_ID_0; }
	inline String_t** get_address_of_RECORDING_NEW_ID_0() { return &___RECORDING_NEW_ID_0; }
	inline void set_RECORDING_NEW_ID_0(String_t* value)
	{
		___RECORDING_NEW_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_NEW_ID_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
