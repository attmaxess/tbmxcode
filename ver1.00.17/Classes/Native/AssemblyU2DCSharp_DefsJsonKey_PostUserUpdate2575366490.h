﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.PostUserUpdate
struct  PostUserUpdate_t2575366490  : public Il2CppObject
{
public:

public:
};

struct PostUserUpdate_t2575366490_StaticFields
{
public:
	// System.String DefsJsonKey.PostUserUpdate::USER_UPDATE_RANK
	String_t* ___USER_UPDATE_RANK_0;
	// System.String DefsJsonKey.PostUserUpdate::USER_UPDATE_RANKUPPOINT
	String_t* ___USER_UPDATE_RANKUPPOINT_1;

public:
	inline static int32_t get_offset_of_USER_UPDATE_RANK_0() { return static_cast<int32_t>(offsetof(PostUserUpdate_t2575366490_StaticFields, ___USER_UPDATE_RANK_0)); }
	inline String_t* get_USER_UPDATE_RANK_0() const { return ___USER_UPDATE_RANK_0; }
	inline String_t** get_address_of_USER_UPDATE_RANK_0() { return &___USER_UPDATE_RANK_0; }
	inline void set_USER_UPDATE_RANK_0(String_t* value)
	{
		___USER_UPDATE_RANK_0 = value;
		Il2CppCodeGenWriteBarrier(&___USER_UPDATE_RANK_0, value);
	}

	inline static int32_t get_offset_of_USER_UPDATE_RANKUPPOINT_1() { return static_cast<int32_t>(offsetof(PostUserUpdate_t2575366490_StaticFields, ___USER_UPDATE_RANKUPPOINT_1)); }
	inline String_t* get_USER_UPDATE_RANKUPPOINT_1() const { return ___USER_UPDATE_RANKUPPOINT_1; }
	inline String_t** get_address_of_USER_UPDATE_RANKUPPOINT_1() { return &___USER_UPDATE_RANKUPPOINT_1; }
	inline void set_USER_UPDATE_RANKUPPOINT_1(String_t* value)
	{
		___USER_UPDATE_RANKUPPOINT_1 = value;
		Il2CppCodeGenWriteBarrier(&___USER_UPDATE_RANKUPPOINT_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
