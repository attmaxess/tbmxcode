﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.Recording
struct  Recording_t677626451  : public Il2CppObject
{
public:

public:
};

struct Recording_t677626451_StaticFields
{
public:
	// System.String DefsJsonKey.Recording::RECORDING_RECORDED_ID
	String_t* ___RECORDING_RECORDED_ID_0;
	// System.String DefsJsonKey.Recording::RECORDING_RECORDED_DATA
	String_t* ___RECORDING_RECORDED_DATA_1;
	// System.String DefsJsonKey.Recording::RECORDING_STARTPOINT_X
	String_t* ___RECORDING_STARTPOINT_X_2;
	// System.String DefsJsonKey.Recording::RECORDING_STARTPOINT_Y
	String_t* ___RECORDING_STARTPOINT_Y_3;
	// System.String DefsJsonKey.Recording::RECORDING_STARTPOINT_Z
	String_t* ___RECORDING_STARTPOINT_Z_4;
	// System.String DefsJsonKey.Recording::RECORDING_MOTIONS_INFO
	String_t* ___RECORDING_MOTIONS_INFO_5;
	// System.String DefsJsonKey.Recording::RECORDING_MOTIONS_MOTIONSTARTTIME
	String_t* ___RECORDING_MOTIONS_MOTIONSTARTTIME_6;
	// System.String DefsJsonKey.Recording::RECORDING_MOTION_ID
	String_t* ___RECORDING_MOTION_ID_7;

public:
	inline static int32_t get_offset_of_RECORDING_RECORDED_ID_0() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_RECORDED_ID_0)); }
	inline String_t* get_RECORDING_RECORDED_ID_0() const { return ___RECORDING_RECORDED_ID_0; }
	inline String_t** get_address_of_RECORDING_RECORDED_ID_0() { return &___RECORDING_RECORDED_ID_0; }
	inline void set_RECORDING_RECORDED_ID_0(String_t* value)
	{
		___RECORDING_RECORDED_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_RECORDED_ID_0, value);
	}

	inline static int32_t get_offset_of_RECORDING_RECORDED_DATA_1() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_RECORDED_DATA_1)); }
	inline String_t* get_RECORDING_RECORDED_DATA_1() const { return ___RECORDING_RECORDED_DATA_1; }
	inline String_t** get_address_of_RECORDING_RECORDED_DATA_1() { return &___RECORDING_RECORDED_DATA_1; }
	inline void set_RECORDING_RECORDED_DATA_1(String_t* value)
	{
		___RECORDING_RECORDED_DATA_1 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_RECORDED_DATA_1, value);
	}

	inline static int32_t get_offset_of_RECORDING_STARTPOINT_X_2() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_STARTPOINT_X_2)); }
	inline String_t* get_RECORDING_STARTPOINT_X_2() const { return ___RECORDING_STARTPOINT_X_2; }
	inline String_t** get_address_of_RECORDING_STARTPOINT_X_2() { return &___RECORDING_STARTPOINT_X_2; }
	inline void set_RECORDING_STARTPOINT_X_2(String_t* value)
	{
		___RECORDING_STARTPOINT_X_2 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_STARTPOINT_X_2, value);
	}

	inline static int32_t get_offset_of_RECORDING_STARTPOINT_Y_3() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_STARTPOINT_Y_3)); }
	inline String_t* get_RECORDING_STARTPOINT_Y_3() const { return ___RECORDING_STARTPOINT_Y_3; }
	inline String_t** get_address_of_RECORDING_STARTPOINT_Y_3() { return &___RECORDING_STARTPOINT_Y_3; }
	inline void set_RECORDING_STARTPOINT_Y_3(String_t* value)
	{
		___RECORDING_STARTPOINT_Y_3 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_STARTPOINT_Y_3, value);
	}

	inline static int32_t get_offset_of_RECORDING_STARTPOINT_Z_4() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_STARTPOINT_Z_4)); }
	inline String_t* get_RECORDING_STARTPOINT_Z_4() const { return ___RECORDING_STARTPOINT_Z_4; }
	inline String_t** get_address_of_RECORDING_STARTPOINT_Z_4() { return &___RECORDING_STARTPOINT_Z_4; }
	inline void set_RECORDING_STARTPOINT_Z_4(String_t* value)
	{
		___RECORDING_STARTPOINT_Z_4 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_STARTPOINT_Z_4, value);
	}

	inline static int32_t get_offset_of_RECORDING_MOTIONS_INFO_5() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_MOTIONS_INFO_5)); }
	inline String_t* get_RECORDING_MOTIONS_INFO_5() const { return ___RECORDING_MOTIONS_INFO_5; }
	inline String_t** get_address_of_RECORDING_MOTIONS_INFO_5() { return &___RECORDING_MOTIONS_INFO_5; }
	inline void set_RECORDING_MOTIONS_INFO_5(String_t* value)
	{
		___RECORDING_MOTIONS_INFO_5 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_MOTIONS_INFO_5, value);
	}

	inline static int32_t get_offset_of_RECORDING_MOTIONS_MOTIONSTARTTIME_6() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_MOTIONS_MOTIONSTARTTIME_6)); }
	inline String_t* get_RECORDING_MOTIONS_MOTIONSTARTTIME_6() const { return ___RECORDING_MOTIONS_MOTIONSTARTTIME_6; }
	inline String_t** get_address_of_RECORDING_MOTIONS_MOTIONSTARTTIME_6() { return &___RECORDING_MOTIONS_MOTIONSTARTTIME_6; }
	inline void set_RECORDING_MOTIONS_MOTIONSTARTTIME_6(String_t* value)
	{
		___RECORDING_MOTIONS_MOTIONSTARTTIME_6 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_MOTIONS_MOTIONSTARTTIME_6, value);
	}

	inline static int32_t get_offset_of_RECORDING_MOTION_ID_7() { return static_cast<int32_t>(offsetof(Recording_t677626451_StaticFields, ___RECORDING_MOTION_ID_7)); }
	inline String_t* get_RECORDING_MOTION_ID_7() const { return ___RECORDING_MOTION_ID_7; }
	inline String_t** get_address_of_RECORDING_MOTION_ID_7() { return &___RECORDING_MOTION_ID_7; }
	inline void set_RECORDING_MOTION_ID_7(String_t* value)
	{
		___RECORDING_MOTION_ID_7 = value;
		Il2CppCodeGenWriteBarrier(&___RECORDING_MOTION_ID_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
