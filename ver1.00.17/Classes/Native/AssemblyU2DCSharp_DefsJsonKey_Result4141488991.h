﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.Result
struct  Result_t4141488991  : public Il2CppObject
{
public:

public:
};

struct Result_t4141488991_StaticFields
{
public:
	// System.String DefsJsonKey.Result::RESULT_INFO
	String_t* ___RESULT_INFO_0;
	// System.String DefsJsonKey.Result::RESULT_CODE
	String_t* ___RESULT_CODE_1;
	// System.String DefsJsonKey.Result::RESULT_MESSAGE
	String_t* ___RESULT_MESSAGE_2;
	// System.String DefsJsonKey.Result::RESULT_STATUS
	String_t* ___RESULT_STATUS_3;

public:
	inline static int32_t get_offset_of_RESULT_INFO_0() { return static_cast<int32_t>(offsetof(Result_t4141488991_StaticFields, ___RESULT_INFO_0)); }
	inline String_t* get_RESULT_INFO_0() const { return ___RESULT_INFO_0; }
	inline String_t** get_address_of_RESULT_INFO_0() { return &___RESULT_INFO_0; }
	inline void set_RESULT_INFO_0(String_t* value)
	{
		___RESULT_INFO_0 = value;
		Il2CppCodeGenWriteBarrier(&___RESULT_INFO_0, value);
	}

	inline static int32_t get_offset_of_RESULT_CODE_1() { return static_cast<int32_t>(offsetof(Result_t4141488991_StaticFields, ___RESULT_CODE_1)); }
	inline String_t* get_RESULT_CODE_1() const { return ___RESULT_CODE_1; }
	inline String_t** get_address_of_RESULT_CODE_1() { return &___RESULT_CODE_1; }
	inline void set_RESULT_CODE_1(String_t* value)
	{
		___RESULT_CODE_1 = value;
		Il2CppCodeGenWriteBarrier(&___RESULT_CODE_1, value);
	}

	inline static int32_t get_offset_of_RESULT_MESSAGE_2() { return static_cast<int32_t>(offsetof(Result_t4141488991_StaticFields, ___RESULT_MESSAGE_2)); }
	inline String_t* get_RESULT_MESSAGE_2() const { return ___RESULT_MESSAGE_2; }
	inline String_t** get_address_of_RESULT_MESSAGE_2() { return &___RESULT_MESSAGE_2; }
	inline void set_RESULT_MESSAGE_2(String_t* value)
	{
		___RESULT_MESSAGE_2 = value;
		Il2CppCodeGenWriteBarrier(&___RESULT_MESSAGE_2, value);
	}

	inline static int32_t get_offset_of_RESULT_STATUS_3() { return static_cast<int32_t>(offsetof(Result_t4141488991_StaticFields, ___RESULT_STATUS_3)); }
	inline String_t* get_RESULT_STATUS_3() const { return ___RESULT_STATUS_3; }
	inline String_t** get_address_of_RESULT_STATUS_3() { return &___RESULT_STATUS_3; }
	inline void set_RESULT_STATUS_3(String_t* value)
	{
		___RESULT_STATUS_3 = value;
		Il2CppCodeGenWriteBarrier(&___RESULT_STATUS_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
