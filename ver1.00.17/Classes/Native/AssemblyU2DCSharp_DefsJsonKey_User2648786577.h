﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.User
struct  User_t2648786577  : public Il2CppObject
{
public:

public:
};

struct User_t2648786577_StaticFields
{
public:
	// System.String DefsJsonKey.User::User_ID
	String_t* ___User_ID_0;
	// System.String DefsJsonKey.User::User_NAME
	String_t* ___User_NAME_1;
	// System.String DefsJsonKey.User::User_COUNTRY_NO
	String_t* ___User_COUNTRY_NO_2;
	// System.String DefsJsonKey.User::User_LANGUAGE_NO
	String_t* ___User_LANGUAGE_NO_3;
	// System.String DefsJsonKey.User::User_RANK
	String_t* ___User_RANK_4;
	// System.String DefsJsonKey.User::User_POINT
	String_t* ___User_POINT_5;
	// System.String DefsJsonKey.User::User_RANKUP_POINT
	String_t* ___User_RANKUP_POINT_6;
	// System.String DefsJsonKey.User::User_MOBILTY_COUNT
	String_t* ___User_MOBILTY_COUNT_7;
	// System.String DefsJsonKey.User::User_COPIED_COUNT
	String_t* ___User_COPIED_COUNT_8;
	// System.String DefsJsonKey.User::User_MAIN_MOBID
	String_t* ___User_MAIN_MOBID_9;
	// System.String DefsJsonKey.User::User_START_POSITION_X
	String_t* ___User_START_POSITION_X_10;
	// System.String DefsJsonKey.User::User_START_POSITION_Y
	String_t* ___User_START_POSITION_Y_11;
	// System.String DefsJsonKey.User::User_START_POSITION_Z
	String_t* ___User_START_POSITION_Z_12;
	// System.String DefsJsonKey.User::User_START_ROTATION_X
	String_t* ___User_START_ROTATION_X_13;
	// System.String DefsJsonKey.User::User_START_ROTATION_Y
	String_t* ___User_START_ROTATION_Y_14;
	// System.String DefsJsonKey.User::User_START_ROTATION_Z
	String_t* ___User_START_ROTATION_Z_15;

public:
	inline static int32_t get_offset_of_User_ID_0() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_ID_0)); }
	inline String_t* get_User_ID_0() const { return ___User_ID_0; }
	inline String_t** get_address_of_User_ID_0() { return &___User_ID_0; }
	inline void set_User_ID_0(String_t* value)
	{
		___User_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___User_ID_0, value);
	}

	inline static int32_t get_offset_of_User_NAME_1() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_NAME_1)); }
	inline String_t* get_User_NAME_1() const { return ___User_NAME_1; }
	inline String_t** get_address_of_User_NAME_1() { return &___User_NAME_1; }
	inline void set_User_NAME_1(String_t* value)
	{
		___User_NAME_1 = value;
		Il2CppCodeGenWriteBarrier(&___User_NAME_1, value);
	}

	inline static int32_t get_offset_of_User_COUNTRY_NO_2() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_COUNTRY_NO_2)); }
	inline String_t* get_User_COUNTRY_NO_2() const { return ___User_COUNTRY_NO_2; }
	inline String_t** get_address_of_User_COUNTRY_NO_2() { return &___User_COUNTRY_NO_2; }
	inline void set_User_COUNTRY_NO_2(String_t* value)
	{
		___User_COUNTRY_NO_2 = value;
		Il2CppCodeGenWriteBarrier(&___User_COUNTRY_NO_2, value);
	}

	inline static int32_t get_offset_of_User_LANGUAGE_NO_3() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_LANGUAGE_NO_3)); }
	inline String_t* get_User_LANGUAGE_NO_3() const { return ___User_LANGUAGE_NO_3; }
	inline String_t** get_address_of_User_LANGUAGE_NO_3() { return &___User_LANGUAGE_NO_3; }
	inline void set_User_LANGUAGE_NO_3(String_t* value)
	{
		___User_LANGUAGE_NO_3 = value;
		Il2CppCodeGenWriteBarrier(&___User_LANGUAGE_NO_3, value);
	}

	inline static int32_t get_offset_of_User_RANK_4() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_RANK_4)); }
	inline String_t* get_User_RANK_4() const { return ___User_RANK_4; }
	inline String_t** get_address_of_User_RANK_4() { return &___User_RANK_4; }
	inline void set_User_RANK_4(String_t* value)
	{
		___User_RANK_4 = value;
		Il2CppCodeGenWriteBarrier(&___User_RANK_4, value);
	}

	inline static int32_t get_offset_of_User_POINT_5() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_POINT_5)); }
	inline String_t* get_User_POINT_5() const { return ___User_POINT_5; }
	inline String_t** get_address_of_User_POINT_5() { return &___User_POINT_5; }
	inline void set_User_POINT_5(String_t* value)
	{
		___User_POINT_5 = value;
		Il2CppCodeGenWriteBarrier(&___User_POINT_5, value);
	}

	inline static int32_t get_offset_of_User_RANKUP_POINT_6() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_RANKUP_POINT_6)); }
	inline String_t* get_User_RANKUP_POINT_6() const { return ___User_RANKUP_POINT_6; }
	inline String_t** get_address_of_User_RANKUP_POINT_6() { return &___User_RANKUP_POINT_6; }
	inline void set_User_RANKUP_POINT_6(String_t* value)
	{
		___User_RANKUP_POINT_6 = value;
		Il2CppCodeGenWriteBarrier(&___User_RANKUP_POINT_6, value);
	}

	inline static int32_t get_offset_of_User_MOBILTY_COUNT_7() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_MOBILTY_COUNT_7)); }
	inline String_t* get_User_MOBILTY_COUNT_7() const { return ___User_MOBILTY_COUNT_7; }
	inline String_t** get_address_of_User_MOBILTY_COUNT_7() { return &___User_MOBILTY_COUNT_7; }
	inline void set_User_MOBILTY_COUNT_7(String_t* value)
	{
		___User_MOBILTY_COUNT_7 = value;
		Il2CppCodeGenWriteBarrier(&___User_MOBILTY_COUNT_7, value);
	}

	inline static int32_t get_offset_of_User_COPIED_COUNT_8() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_COPIED_COUNT_8)); }
	inline String_t* get_User_COPIED_COUNT_8() const { return ___User_COPIED_COUNT_8; }
	inline String_t** get_address_of_User_COPIED_COUNT_8() { return &___User_COPIED_COUNT_8; }
	inline void set_User_COPIED_COUNT_8(String_t* value)
	{
		___User_COPIED_COUNT_8 = value;
		Il2CppCodeGenWriteBarrier(&___User_COPIED_COUNT_8, value);
	}

	inline static int32_t get_offset_of_User_MAIN_MOBID_9() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_MAIN_MOBID_9)); }
	inline String_t* get_User_MAIN_MOBID_9() const { return ___User_MAIN_MOBID_9; }
	inline String_t** get_address_of_User_MAIN_MOBID_9() { return &___User_MAIN_MOBID_9; }
	inline void set_User_MAIN_MOBID_9(String_t* value)
	{
		___User_MAIN_MOBID_9 = value;
		Il2CppCodeGenWriteBarrier(&___User_MAIN_MOBID_9, value);
	}

	inline static int32_t get_offset_of_User_START_POSITION_X_10() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_START_POSITION_X_10)); }
	inline String_t* get_User_START_POSITION_X_10() const { return ___User_START_POSITION_X_10; }
	inline String_t** get_address_of_User_START_POSITION_X_10() { return &___User_START_POSITION_X_10; }
	inline void set_User_START_POSITION_X_10(String_t* value)
	{
		___User_START_POSITION_X_10 = value;
		Il2CppCodeGenWriteBarrier(&___User_START_POSITION_X_10, value);
	}

	inline static int32_t get_offset_of_User_START_POSITION_Y_11() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_START_POSITION_Y_11)); }
	inline String_t* get_User_START_POSITION_Y_11() const { return ___User_START_POSITION_Y_11; }
	inline String_t** get_address_of_User_START_POSITION_Y_11() { return &___User_START_POSITION_Y_11; }
	inline void set_User_START_POSITION_Y_11(String_t* value)
	{
		___User_START_POSITION_Y_11 = value;
		Il2CppCodeGenWriteBarrier(&___User_START_POSITION_Y_11, value);
	}

	inline static int32_t get_offset_of_User_START_POSITION_Z_12() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_START_POSITION_Z_12)); }
	inline String_t* get_User_START_POSITION_Z_12() const { return ___User_START_POSITION_Z_12; }
	inline String_t** get_address_of_User_START_POSITION_Z_12() { return &___User_START_POSITION_Z_12; }
	inline void set_User_START_POSITION_Z_12(String_t* value)
	{
		___User_START_POSITION_Z_12 = value;
		Il2CppCodeGenWriteBarrier(&___User_START_POSITION_Z_12, value);
	}

	inline static int32_t get_offset_of_User_START_ROTATION_X_13() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_START_ROTATION_X_13)); }
	inline String_t* get_User_START_ROTATION_X_13() const { return ___User_START_ROTATION_X_13; }
	inline String_t** get_address_of_User_START_ROTATION_X_13() { return &___User_START_ROTATION_X_13; }
	inline void set_User_START_ROTATION_X_13(String_t* value)
	{
		___User_START_ROTATION_X_13 = value;
		Il2CppCodeGenWriteBarrier(&___User_START_ROTATION_X_13, value);
	}

	inline static int32_t get_offset_of_User_START_ROTATION_Y_14() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_START_ROTATION_Y_14)); }
	inline String_t* get_User_START_ROTATION_Y_14() const { return ___User_START_ROTATION_Y_14; }
	inline String_t** get_address_of_User_START_ROTATION_Y_14() { return &___User_START_ROTATION_Y_14; }
	inline void set_User_START_ROTATION_Y_14(String_t* value)
	{
		___User_START_ROTATION_Y_14 = value;
		Il2CppCodeGenWriteBarrier(&___User_START_ROTATION_Y_14, value);
	}

	inline static int32_t get_offset_of_User_START_ROTATION_Z_15() { return static_cast<int32_t>(offsetof(User_t2648786577_StaticFields, ___User_START_ROTATION_Z_15)); }
	inline String_t* get_User_START_ROTATION_Z_15() const { return ___User_START_ROTATION_Z_15; }
	inline String_t** get_address_of_User_START_ROTATION_Z_15() { return &___User_START_ROTATION_Z_15; }
	inline void set_User_START_ROTATION_Z_15(String_t* value)
	{
		___User_START_ROTATION_Z_15 = value;
		Il2CppCodeGenWriteBarrier(&___User_START_ROTATION_Z_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
