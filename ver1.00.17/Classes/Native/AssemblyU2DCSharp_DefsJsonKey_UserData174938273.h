﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefsJsonKey.UserData
struct  UserData_t174938273  : public Il2CppObject
{
public:

public:
};

struct UserData_t174938273_StaticFields
{
public:
	// System.String DefsJsonKey.UserData::User_ID
	String_t* ___User_ID_0;
	// System.String DefsJsonKey.UserData::User_NAME
	String_t* ___User_NAME_1;
	// System.String DefsJsonKey.UserData::User_RANK
	String_t* ___User_RANK_2;
	// System.String DefsJsonKey.UserData::User_REACTIONTYPE
	String_t* ___User_REACTIONTYPE_3;

public:
	inline static int32_t get_offset_of_User_ID_0() { return static_cast<int32_t>(offsetof(UserData_t174938273_StaticFields, ___User_ID_0)); }
	inline String_t* get_User_ID_0() const { return ___User_ID_0; }
	inline String_t** get_address_of_User_ID_0() { return &___User_ID_0; }
	inline void set_User_ID_0(String_t* value)
	{
		___User_ID_0 = value;
		Il2CppCodeGenWriteBarrier(&___User_ID_0, value);
	}

	inline static int32_t get_offset_of_User_NAME_1() { return static_cast<int32_t>(offsetof(UserData_t174938273_StaticFields, ___User_NAME_1)); }
	inline String_t* get_User_NAME_1() const { return ___User_NAME_1; }
	inline String_t** get_address_of_User_NAME_1() { return &___User_NAME_1; }
	inline void set_User_NAME_1(String_t* value)
	{
		___User_NAME_1 = value;
		Il2CppCodeGenWriteBarrier(&___User_NAME_1, value);
	}

	inline static int32_t get_offset_of_User_RANK_2() { return static_cast<int32_t>(offsetof(UserData_t174938273_StaticFields, ___User_RANK_2)); }
	inline String_t* get_User_RANK_2() const { return ___User_RANK_2; }
	inline String_t** get_address_of_User_RANK_2() { return &___User_RANK_2; }
	inline void set_User_RANK_2(String_t* value)
	{
		___User_RANK_2 = value;
		Il2CppCodeGenWriteBarrier(&___User_RANK_2, value);
	}

	inline static int32_t get_offset_of_User_REACTIONTYPE_3() { return static_cast<int32_t>(offsetof(UserData_t174938273_StaticFields, ___User_REACTIONTYPE_3)); }
	inline String_t* get_User_REACTIONTYPE_3() const { return ___User_REACTIONTYPE_3; }
	inline String_t** get_address_of_User_REACTIONTYPE_3() { return &___User_REACTIONTYPE_3; }
	inline void set_User_REACTIONTYPE_3(String_t* value)
	{
		___User_REACTIONTYPE_3 = value;
		Il2CppCodeGenWriteBarrier(&___User_REACTIONTYPE_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
