﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoGUIWater
struct  DemoGUIWater_t2357801351  : public MonoBehaviour_t1158329972
{
public:
	// System.Single DemoGUIWater::UpdateInterval
	float ___UpdateInterval_2;
	// System.Int32 DemoGUIWater::MaxScenes
	int32_t ___MaxScenes_3;
	// System.Boolean DemoGUIWater::IsMobileScene
	bool ___IsMobileScene_4;
	// UnityEngine.Light DemoGUIWater::Sun
	Light_t494725636 * ___Sun_5;
	// UnityEngine.GameObject DemoGUIWater::SunTransform
	GameObject_t1756533147 * ___SunTransform_6;
	// UnityEngine.GameObject DemoGUIWater::Boat
	GameObject_t1756533147 * ___Boat_7;
	// UnityEngine.GameObject DemoGUIWater::water1
	GameObject_t1756533147 * ___water1_8;
	// UnityEngine.GameObject DemoGUIWater::water2
	GameObject_t1756533147 * ___water2_9;
	// System.Single DemoGUIWater::angle
	float ___angle_10;
	// System.Boolean DemoGUIWater::canUpdateTestMaterial
	bool ___canUpdateTestMaterial_11;
	// UnityEngine.GameObject DemoGUIWater::cam
	GameObject_t1756533147 * ___cam_12;
	// UnityEngine.GUIStyle DemoGUIWater::guiStyleHeader
	GUIStyle_t1799908754 * ___guiStyleHeader_13;
	// UnityEngine.Material DemoGUIWater::currentWaterMaterial
	Material_t193706927 * ___currentWaterMaterial_14;
	// UnityEngine.Material DemoGUIWater::causticMaterial
	Material_t193706927 * ___causticMaterial_15;
	// UnityEngine.GameObject DemoGUIWater::currentWater
	GameObject_t1756533147 * ___currentWater_16;
	// System.Single DemoGUIWater::transparent
	float ___transparent_17;
	// System.Single DemoGUIWater::fadeBlend
	float ___fadeBlend_18;
	// System.Single DemoGUIWater::refl
	float ___refl_19;
	// System.Single DemoGUIWater::refraction
	float ___refraction_20;
	// System.Single DemoGUIWater::waterWaveScaleXZ
	float ___waterWaveScaleXZ_21;
	// UnityEngine.Vector4 DemoGUIWater::waterDirection
	Vector4_t2243707581  ___waterDirection_22;
	// UnityEngine.Vector4 DemoGUIWater::causticDirection
	Vector4_t2243707581  ___causticDirection_23;
	// UnityEngine.Vector4 DemoGUIWater::foamDirection
	Vector4_t2243707581  ___foamDirection_24;
	// UnityEngine.Vector4 DemoGUIWater::ABDirection
	Vector4_t2243707581  ___ABDirection_25;
	// UnityEngine.Vector4 DemoGUIWater::CDDirection
	Vector4_t2243707581  ___CDDirection_26;
	// System.Single DemoGUIWater::direction
	float ___direction_27;
	// UnityEngine.Color DemoGUIWater::reflectionColor
	Color_t2020392075  ___reflectionColor_28;
	// UnityEngine.Vector3 DemoGUIWater::oldCausticScale
	Vector3_t2243707580  ___oldCausticScale_29;
	// System.Single DemoGUIWater::oldTextureScale
	float ___oldTextureScale_30;
	// System.Single DemoGUIWater::oldWaveScale
	float ___oldWaveScale_31;
	// UnityEngine.GameObject DemoGUIWater::caustic
	GameObject_t1756533147 * ___caustic_32;
	// System.Single DemoGUIWater::startSunIntencity
	float ___startSunIntencity_33;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_MaxScenes_3() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___MaxScenes_3)); }
	inline int32_t get_MaxScenes_3() const { return ___MaxScenes_3; }
	inline int32_t* get_address_of_MaxScenes_3() { return &___MaxScenes_3; }
	inline void set_MaxScenes_3(int32_t value)
	{
		___MaxScenes_3 = value;
	}

	inline static int32_t get_offset_of_IsMobileScene_4() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___IsMobileScene_4)); }
	inline bool get_IsMobileScene_4() const { return ___IsMobileScene_4; }
	inline bool* get_address_of_IsMobileScene_4() { return &___IsMobileScene_4; }
	inline void set_IsMobileScene_4(bool value)
	{
		___IsMobileScene_4 = value;
	}

	inline static int32_t get_offset_of_Sun_5() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___Sun_5)); }
	inline Light_t494725636 * get_Sun_5() const { return ___Sun_5; }
	inline Light_t494725636 ** get_address_of_Sun_5() { return &___Sun_5; }
	inline void set_Sun_5(Light_t494725636 * value)
	{
		___Sun_5 = value;
		Il2CppCodeGenWriteBarrier(&___Sun_5, value);
	}

	inline static int32_t get_offset_of_SunTransform_6() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___SunTransform_6)); }
	inline GameObject_t1756533147 * get_SunTransform_6() const { return ___SunTransform_6; }
	inline GameObject_t1756533147 ** get_address_of_SunTransform_6() { return &___SunTransform_6; }
	inline void set_SunTransform_6(GameObject_t1756533147 * value)
	{
		___SunTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___SunTransform_6, value);
	}

	inline static int32_t get_offset_of_Boat_7() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___Boat_7)); }
	inline GameObject_t1756533147 * get_Boat_7() const { return ___Boat_7; }
	inline GameObject_t1756533147 ** get_address_of_Boat_7() { return &___Boat_7; }
	inline void set_Boat_7(GameObject_t1756533147 * value)
	{
		___Boat_7 = value;
		Il2CppCodeGenWriteBarrier(&___Boat_7, value);
	}

	inline static int32_t get_offset_of_water1_8() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___water1_8)); }
	inline GameObject_t1756533147 * get_water1_8() const { return ___water1_8; }
	inline GameObject_t1756533147 ** get_address_of_water1_8() { return &___water1_8; }
	inline void set_water1_8(GameObject_t1756533147 * value)
	{
		___water1_8 = value;
		Il2CppCodeGenWriteBarrier(&___water1_8, value);
	}

	inline static int32_t get_offset_of_water2_9() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___water2_9)); }
	inline GameObject_t1756533147 * get_water2_9() const { return ___water2_9; }
	inline GameObject_t1756533147 ** get_address_of_water2_9() { return &___water2_9; }
	inline void set_water2_9(GameObject_t1756533147 * value)
	{
		___water2_9 = value;
		Il2CppCodeGenWriteBarrier(&___water2_9, value);
	}

	inline static int32_t get_offset_of_angle_10() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___angle_10)); }
	inline float get_angle_10() const { return ___angle_10; }
	inline float* get_address_of_angle_10() { return &___angle_10; }
	inline void set_angle_10(float value)
	{
		___angle_10 = value;
	}

	inline static int32_t get_offset_of_canUpdateTestMaterial_11() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___canUpdateTestMaterial_11)); }
	inline bool get_canUpdateTestMaterial_11() const { return ___canUpdateTestMaterial_11; }
	inline bool* get_address_of_canUpdateTestMaterial_11() { return &___canUpdateTestMaterial_11; }
	inline void set_canUpdateTestMaterial_11(bool value)
	{
		___canUpdateTestMaterial_11 = value;
	}

	inline static int32_t get_offset_of_cam_12() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___cam_12)); }
	inline GameObject_t1756533147 * get_cam_12() const { return ___cam_12; }
	inline GameObject_t1756533147 ** get_address_of_cam_12() { return &___cam_12; }
	inline void set_cam_12(GameObject_t1756533147 * value)
	{
		___cam_12 = value;
		Il2CppCodeGenWriteBarrier(&___cam_12, value);
	}

	inline static int32_t get_offset_of_guiStyleHeader_13() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___guiStyleHeader_13)); }
	inline GUIStyle_t1799908754 * get_guiStyleHeader_13() const { return ___guiStyleHeader_13; }
	inline GUIStyle_t1799908754 ** get_address_of_guiStyleHeader_13() { return &___guiStyleHeader_13; }
	inline void set_guiStyleHeader_13(GUIStyle_t1799908754 * value)
	{
		___guiStyleHeader_13 = value;
		Il2CppCodeGenWriteBarrier(&___guiStyleHeader_13, value);
	}

	inline static int32_t get_offset_of_currentWaterMaterial_14() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___currentWaterMaterial_14)); }
	inline Material_t193706927 * get_currentWaterMaterial_14() const { return ___currentWaterMaterial_14; }
	inline Material_t193706927 ** get_address_of_currentWaterMaterial_14() { return &___currentWaterMaterial_14; }
	inline void set_currentWaterMaterial_14(Material_t193706927 * value)
	{
		___currentWaterMaterial_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentWaterMaterial_14, value);
	}

	inline static int32_t get_offset_of_causticMaterial_15() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___causticMaterial_15)); }
	inline Material_t193706927 * get_causticMaterial_15() const { return ___causticMaterial_15; }
	inline Material_t193706927 ** get_address_of_causticMaterial_15() { return &___causticMaterial_15; }
	inline void set_causticMaterial_15(Material_t193706927 * value)
	{
		___causticMaterial_15 = value;
		Il2CppCodeGenWriteBarrier(&___causticMaterial_15, value);
	}

	inline static int32_t get_offset_of_currentWater_16() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___currentWater_16)); }
	inline GameObject_t1756533147 * get_currentWater_16() const { return ___currentWater_16; }
	inline GameObject_t1756533147 ** get_address_of_currentWater_16() { return &___currentWater_16; }
	inline void set_currentWater_16(GameObject_t1756533147 * value)
	{
		___currentWater_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentWater_16, value);
	}

	inline static int32_t get_offset_of_transparent_17() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___transparent_17)); }
	inline float get_transparent_17() const { return ___transparent_17; }
	inline float* get_address_of_transparent_17() { return &___transparent_17; }
	inline void set_transparent_17(float value)
	{
		___transparent_17 = value;
	}

	inline static int32_t get_offset_of_fadeBlend_18() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___fadeBlend_18)); }
	inline float get_fadeBlend_18() const { return ___fadeBlend_18; }
	inline float* get_address_of_fadeBlend_18() { return &___fadeBlend_18; }
	inline void set_fadeBlend_18(float value)
	{
		___fadeBlend_18 = value;
	}

	inline static int32_t get_offset_of_refl_19() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___refl_19)); }
	inline float get_refl_19() const { return ___refl_19; }
	inline float* get_address_of_refl_19() { return &___refl_19; }
	inline void set_refl_19(float value)
	{
		___refl_19 = value;
	}

	inline static int32_t get_offset_of_refraction_20() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___refraction_20)); }
	inline float get_refraction_20() const { return ___refraction_20; }
	inline float* get_address_of_refraction_20() { return &___refraction_20; }
	inline void set_refraction_20(float value)
	{
		___refraction_20 = value;
	}

	inline static int32_t get_offset_of_waterWaveScaleXZ_21() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___waterWaveScaleXZ_21)); }
	inline float get_waterWaveScaleXZ_21() const { return ___waterWaveScaleXZ_21; }
	inline float* get_address_of_waterWaveScaleXZ_21() { return &___waterWaveScaleXZ_21; }
	inline void set_waterWaveScaleXZ_21(float value)
	{
		___waterWaveScaleXZ_21 = value;
	}

	inline static int32_t get_offset_of_waterDirection_22() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___waterDirection_22)); }
	inline Vector4_t2243707581  get_waterDirection_22() const { return ___waterDirection_22; }
	inline Vector4_t2243707581 * get_address_of_waterDirection_22() { return &___waterDirection_22; }
	inline void set_waterDirection_22(Vector4_t2243707581  value)
	{
		___waterDirection_22 = value;
	}

	inline static int32_t get_offset_of_causticDirection_23() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___causticDirection_23)); }
	inline Vector4_t2243707581  get_causticDirection_23() const { return ___causticDirection_23; }
	inline Vector4_t2243707581 * get_address_of_causticDirection_23() { return &___causticDirection_23; }
	inline void set_causticDirection_23(Vector4_t2243707581  value)
	{
		___causticDirection_23 = value;
	}

	inline static int32_t get_offset_of_foamDirection_24() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___foamDirection_24)); }
	inline Vector4_t2243707581  get_foamDirection_24() const { return ___foamDirection_24; }
	inline Vector4_t2243707581 * get_address_of_foamDirection_24() { return &___foamDirection_24; }
	inline void set_foamDirection_24(Vector4_t2243707581  value)
	{
		___foamDirection_24 = value;
	}

	inline static int32_t get_offset_of_ABDirection_25() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___ABDirection_25)); }
	inline Vector4_t2243707581  get_ABDirection_25() const { return ___ABDirection_25; }
	inline Vector4_t2243707581 * get_address_of_ABDirection_25() { return &___ABDirection_25; }
	inline void set_ABDirection_25(Vector4_t2243707581  value)
	{
		___ABDirection_25 = value;
	}

	inline static int32_t get_offset_of_CDDirection_26() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___CDDirection_26)); }
	inline Vector4_t2243707581  get_CDDirection_26() const { return ___CDDirection_26; }
	inline Vector4_t2243707581 * get_address_of_CDDirection_26() { return &___CDDirection_26; }
	inline void set_CDDirection_26(Vector4_t2243707581  value)
	{
		___CDDirection_26 = value;
	}

	inline static int32_t get_offset_of_direction_27() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___direction_27)); }
	inline float get_direction_27() const { return ___direction_27; }
	inline float* get_address_of_direction_27() { return &___direction_27; }
	inline void set_direction_27(float value)
	{
		___direction_27 = value;
	}

	inline static int32_t get_offset_of_reflectionColor_28() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___reflectionColor_28)); }
	inline Color_t2020392075  get_reflectionColor_28() const { return ___reflectionColor_28; }
	inline Color_t2020392075 * get_address_of_reflectionColor_28() { return &___reflectionColor_28; }
	inline void set_reflectionColor_28(Color_t2020392075  value)
	{
		___reflectionColor_28 = value;
	}

	inline static int32_t get_offset_of_oldCausticScale_29() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___oldCausticScale_29)); }
	inline Vector3_t2243707580  get_oldCausticScale_29() const { return ___oldCausticScale_29; }
	inline Vector3_t2243707580 * get_address_of_oldCausticScale_29() { return &___oldCausticScale_29; }
	inline void set_oldCausticScale_29(Vector3_t2243707580  value)
	{
		___oldCausticScale_29 = value;
	}

	inline static int32_t get_offset_of_oldTextureScale_30() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___oldTextureScale_30)); }
	inline float get_oldTextureScale_30() const { return ___oldTextureScale_30; }
	inline float* get_address_of_oldTextureScale_30() { return &___oldTextureScale_30; }
	inline void set_oldTextureScale_30(float value)
	{
		___oldTextureScale_30 = value;
	}

	inline static int32_t get_offset_of_oldWaveScale_31() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___oldWaveScale_31)); }
	inline float get_oldWaveScale_31() const { return ___oldWaveScale_31; }
	inline float* get_address_of_oldWaveScale_31() { return &___oldWaveScale_31; }
	inline void set_oldWaveScale_31(float value)
	{
		___oldWaveScale_31 = value;
	}

	inline static int32_t get_offset_of_caustic_32() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___caustic_32)); }
	inline GameObject_t1756533147 * get_caustic_32() const { return ___caustic_32; }
	inline GameObject_t1756533147 ** get_address_of_caustic_32() { return &___caustic_32; }
	inline void set_caustic_32(GameObject_t1756533147 * value)
	{
		___caustic_32 = value;
		Il2CppCodeGenWriteBarrier(&___caustic_32, value);
	}

	inline static int32_t get_offset_of_startSunIntencity_33() { return static_cast<int32_t>(offsetof(DemoGUIWater_t2357801351, ___startSunIntencity_33)); }
	inline float get_startSunIntencity_33() const { return ___startSunIntencity_33; }
	inline float* get_address_of_startSunIntencity_33() { return &___startSunIntencity_33; }
	inline void set_startSunIntencity_33(float value)
	{
		___startSunIntencity_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
