﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoWorld
struct  DemoWorld_t457704051  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject DemoWorld::m_DemoField
	GameObject_t1756533147 * ___m_DemoField_2;
	// UnityEngine.GameObject DemoWorld::FieldPrefab
	GameObject_t1756533147 * ___FieldPrefab_3;

public:
	inline static int32_t get_offset_of_m_DemoField_2() { return static_cast<int32_t>(offsetof(DemoWorld_t457704051, ___m_DemoField_2)); }
	inline GameObject_t1756533147 * get_m_DemoField_2() const { return ___m_DemoField_2; }
	inline GameObject_t1756533147 ** get_address_of_m_DemoField_2() { return &___m_DemoField_2; }
	inline void set_m_DemoField_2(GameObject_t1756533147 * value)
	{
		___m_DemoField_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_DemoField_2, value);
	}

	inline static int32_t get_offset_of_FieldPrefab_3() { return static_cast<int32_t>(offsetof(DemoWorld_t457704051, ___FieldPrefab_3)); }
	inline GameObject_t1756533147 * get_FieldPrefab_3() const { return ___FieldPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_FieldPrefab_3() { return &___FieldPrefab_3; }
	inline void set_FieldPrefab_3(GameObject_t1756533147 * value)
	{
		___FieldPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___FieldPrefab_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
