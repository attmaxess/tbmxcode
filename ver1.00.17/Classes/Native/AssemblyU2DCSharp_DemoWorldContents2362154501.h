﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// DemoWorld
struct DemoWorld_t457704051;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoWorldContents
struct  DemoWorldContents_t2362154501  : public MonoBehaviour_t1158329972
{
public:
	// DemoWorld DemoWorldContents::DemoWorld
	DemoWorld_t457704051 * ___DemoWorld_2;

public:
	inline static int32_t get_offset_of_DemoWorld_2() { return static_cast<int32_t>(offsetof(DemoWorldContents_t2362154501, ___DemoWorld_2)); }
	inline DemoWorld_t457704051 * get_DemoWorld_2() const { return ___DemoWorld_2; }
	inline DemoWorld_t457704051 ** get_address_of_DemoWorld_2() { return &___DemoWorld_2; }
	inline void set_DemoWorld_2(DemoWorld_t457704051 * value)
	{
		___DemoWorld_2 = value;
		Il2CppCodeGenWriteBarrier(&___DemoWorld_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
