﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_DIALOG_BACKGROUND_COLOR3428445225.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Material
struct Material_t193706927;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dialog
struct  Dialog_t1378192732  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image Dialog::Background
	Image_t2042527209 * ___Background_2;
	// System.Single Dialog::FadeTime
	float ___FadeTime_3;
	// UnityEngine.UI.Text Dialog::TitleText
	Text_t356221433 * ___TitleText_4;
	// UnityEngine.UI.Text Dialog::InfoText
	Text_t356221433 * ___InfoText_5;
	// UnityEngine.GameObject Dialog::Panel
	GameObject_t1756533147 * ___Panel_6;
	// UnityEngine.UI.Button Dialog::OkButton
	Button_t2872111280 * ___OkButton_7;
	// UnityEngine.UI.Button Dialog::NgButton
	Button_t2872111280 * ___NgButton_8;
	// UnityEngine.UI.Button Dialog::BackButton
	Button_t2872111280 * ___BackButton_9;
	// UnityEngine.UI.Image Dialog::BackgroundBlur
	Image_t2042527209 * ___BackgroundBlur_10;
	// UnityEngine.Material Dialog::Material
	Material_t193706927 * ___Material_11;
	// System.Action Dialog::OkAction
	Action_t3226471752 * ___OkAction_12;
	// System.Action Dialog::NgAction
	Action_t3226471752 * ___NgAction_13;
	// DIALOG_BACKGROUND_COLOR Dialog::m_DialogBackground
	int32_t ___m_DialogBackground_14;
	// System.Boolean Dialog::m_bFade
	bool ___m_bFade_15;
	// System.Single Dialog::m_fMaterialRadius
	float ___m_fMaterialRadius_16;

public:
	inline static int32_t get_offset_of_Background_2() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___Background_2)); }
	inline Image_t2042527209 * get_Background_2() const { return ___Background_2; }
	inline Image_t2042527209 ** get_address_of_Background_2() { return &___Background_2; }
	inline void set_Background_2(Image_t2042527209 * value)
	{
		___Background_2 = value;
		Il2CppCodeGenWriteBarrier(&___Background_2, value);
	}

	inline static int32_t get_offset_of_FadeTime_3() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___FadeTime_3)); }
	inline float get_FadeTime_3() const { return ___FadeTime_3; }
	inline float* get_address_of_FadeTime_3() { return &___FadeTime_3; }
	inline void set_FadeTime_3(float value)
	{
		___FadeTime_3 = value;
	}

	inline static int32_t get_offset_of_TitleText_4() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___TitleText_4)); }
	inline Text_t356221433 * get_TitleText_4() const { return ___TitleText_4; }
	inline Text_t356221433 ** get_address_of_TitleText_4() { return &___TitleText_4; }
	inline void set_TitleText_4(Text_t356221433 * value)
	{
		___TitleText_4 = value;
		Il2CppCodeGenWriteBarrier(&___TitleText_4, value);
	}

	inline static int32_t get_offset_of_InfoText_5() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___InfoText_5)); }
	inline Text_t356221433 * get_InfoText_5() const { return ___InfoText_5; }
	inline Text_t356221433 ** get_address_of_InfoText_5() { return &___InfoText_5; }
	inline void set_InfoText_5(Text_t356221433 * value)
	{
		___InfoText_5 = value;
		Il2CppCodeGenWriteBarrier(&___InfoText_5, value);
	}

	inline static int32_t get_offset_of_Panel_6() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___Panel_6)); }
	inline GameObject_t1756533147 * get_Panel_6() const { return ___Panel_6; }
	inline GameObject_t1756533147 ** get_address_of_Panel_6() { return &___Panel_6; }
	inline void set_Panel_6(GameObject_t1756533147 * value)
	{
		___Panel_6 = value;
		Il2CppCodeGenWriteBarrier(&___Panel_6, value);
	}

	inline static int32_t get_offset_of_OkButton_7() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___OkButton_7)); }
	inline Button_t2872111280 * get_OkButton_7() const { return ___OkButton_7; }
	inline Button_t2872111280 ** get_address_of_OkButton_7() { return &___OkButton_7; }
	inline void set_OkButton_7(Button_t2872111280 * value)
	{
		___OkButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___OkButton_7, value);
	}

	inline static int32_t get_offset_of_NgButton_8() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___NgButton_8)); }
	inline Button_t2872111280 * get_NgButton_8() const { return ___NgButton_8; }
	inline Button_t2872111280 ** get_address_of_NgButton_8() { return &___NgButton_8; }
	inline void set_NgButton_8(Button_t2872111280 * value)
	{
		___NgButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___NgButton_8, value);
	}

	inline static int32_t get_offset_of_BackButton_9() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___BackButton_9)); }
	inline Button_t2872111280 * get_BackButton_9() const { return ___BackButton_9; }
	inline Button_t2872111280 ** get_address_of_BackButton_9() { return &___BackButton_9; }
	inline void set_BackButton_9(Button_t2872111280 * value)
	{
		___BackButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___BackButton_9, value);
	}

	inline static int32_t get_offset_of_BackgroundBlur_10() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___BackgroundBlur_10)); }
	inline Image_t2042527209 * get_BackgroundBlur_10() const { return ___BackgroundBlur_10; }
	inline Image_t2042527209 ** get_address_of_BackgroundBlur_10() { return &___BackgroundBlur_10; }
	inline void set_BackgroundBlur_10(Image_t2042527209 * value)
	{
		___BackgroundBlur_10 = value;
		Il2CppCodeGenWriteBarrier(&___BackgroundBlur_10, value);
	}

	inline static int32_t get_offset_of_Material_11() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___Material_11)); }
	inline Material_t193706927 * get_Material_11() const { return ___Material_11; }
	inline Material_t193706927 ** get_address_of_Material_11() { return &___Material_11; }
	inline void set_Material_11(Material_t193706927 * value)
	{
		___Material_11 = value;
		Il2CppCodeGenWriteBarrier(&___Material_11, value);
	}

	inline static int32_t get_offset_of_OkAction_12() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___OkAction_12)); }
	inline Action_t3226471752 * get_OkAction_12() const { return ___OkAction_12; }
	inline Action_t3226471752 ** get_address_of_OkAction_12() { return &___OkAction_12; }
	inline void set_OkAction_12(Action_t3226471752 * value)
	{
		___OkAction_12 = value;
		Il2CppCodeGenWriteBarrier(&___OkAction_12, value);
	}

	inline static int32_t get_offset_of_NgAction_13() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___NgAction_13)); }
	inline Action_t3226471752 * get_NgAction_13() const { return ___NgAction_13; }
	inline Action_t3226471752 ** get_address_of_NgAction_13() { return &___NgAction_13; }
	inline void set_NgAction_13(Action_t3226471752 * value)
	{
		___NgAction_13 = value;
		Il2CppCodeGenWriteBarrier(&___NgAction_13, value);
	}

	inline static int32_t get_offset_of_m_DialogBackground_14() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___m_DialogBackground_14)); }
	inline int32_t get_m_DialogBackground_14() const { return ___m_DialogBackground_14; }
	inline int32_t* get_address_of_m_DialogBackground_14() { return &___m_DialogBackground_14; }
	inline void set_m_DialogBackground_14(int32_t value)
	{
		___m_DialogBackground_14 = value;
	}

	inline static int32_t get_offset_of_m_bFade_15() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___m_bFade_15)); }
	inline bool get_m_bFade_15() const { return ___m_bFade_15; }
	inline bool* get_address_of_m_bFade_15() { return &___m_bFade_15; }
	inline void set_m_bFade_15(bool value)
	{
		___m_bFade_15 = value;
	}

	inline static int32_t get_offset_of_m_fMaterialRadius_16() { return static_cast<int32_t>(offsetof(Dialog_t1378192732, ___m_fMaterialRadius_16)); }
	inline float get_m_fMaterialRadius_16() const { return ___m_fMaterialRadius_16; }
	inline float* get_address_of_m_fMaterialRadius_16() { return &___m_fMaterialRadius_16; }
	inline void set_m_fMaterialRadius_16(float value)
	{
		___m_fMaterialRadius_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
