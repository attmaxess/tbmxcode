﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_FilterMode10814199.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_RenderingPath1538007819.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DistortionMobileCamera
struct  DistortionMobileCamera_t1665251130  : public MonoBehaviour_t1158329972
{
public:
	// System.Single DistortionMobileCamera::TextureScale
	float ___TextureScale_2;
	// UnityEngine.RenderTextureFormat DistortionMobileCamera::RenderTextureFormat
	int32_t ___RenderTextureFormat_3;
	// UnityEngine.FilterMode DistortionMobileCamera::FilterMode
	int32_t ___FilterMode_4;
	// UnityEngine.LayerMask DistortionMobileCamera::CullingMask
	LayerMask_t3188175821  ___CullingMask_5;
	// UnityEngine.RenderingPath DistortionMobileCamera::RenderingPath
	int32_t ___RenderingPath_6;
	// System.Int32 DistortionMobileCamera::FPSWhenMoveCamera
	int32_t ___FPSWhenMoveCamera_7;
	// System.Int32 DistortionMobileCamera::FPSWhenStaticCamera
	int32_t ___FPSWhenStaticCamera_8;
	// UnityEngine.RenderTexture DistortionMobileCamera::renderTexture
	RenderTexture_t2666733923 * ___renderTexture_9;
	// UnityEngine.Camera DistortionMobileCamera::cameraInstance
	Camera_t189460977 * ___cameraInstance_10;
	// UnityEngine.GameObject DistortionMobileCamera::goCamera
	GameObject_t1756533147 * ___goCamera_11;
	// UnityEngine.Vector3 DistortionMobileCamera::oldPosition
	Vector3_t2243707580  ___oldPosition_12;
	// UnityEngine.Quaternion DistortionMobileCamera::oldRotation
	Quaternion_t4030073918  ___oldRotation_13;
	// UnityEngine.Transform DistortionMobileCamera::instanceCameraTransform
	Transform_t3275118058 * ___instanceCameraTransform_14;
	// System.Boolean DistortionMobileCamera::canUpdateCamera
	bool ___canUpdateCamera_15;
	// System.Boolean DistortionMobileCamera::isStaticUpdate
	bool ___isStaticUpdate_16;
	// UnityEngine.WaitForSeconds DistortionMobileCamera::fpsMove
	WaitForSeconds_t3839502067 * ___fpsMove_17;
	// UnityEngine.WaitForSeconds DistortionMobileCamera::fpsStatic
	WaitForSeconds_t3839502067 * ___fpsStatic_18;
	// System.Int32 DistortionMobileCamera::frameCountWhenCameraIsStatic
	int32_t ___frameCountWhenCameraIsStatic_20;

public:
	inline static int32_t get_offset_of_TextureScale_2() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___TextureScale_2)); }
	inline float get_TextureScale_2() const { return ___TextureScale_2; }
	inline float* get_address_of_TextureScale_2() { return &___TextureScale_2; }
	inline void set_TextureScale_2(float value)
	{
		___TextureScale_2 = value;
	}

	inline static int32_t get_offset_of_RenderTextureFormat_3() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___RenderTextureFormat_3)); }
	inline int32_t get_RenderTextureFormat_3() const { return ___RenderTextureFormat_3; }
	inline int32_t* get_address_of_RenderTextureFormat_3() { return &___RenderTextureFormat_3; }
	inline void set_RenderTextureFormat_3(int32_t value)
	{
		___RenderTextureFormat_3 = value;
	}

	inline static int32_t get_offset_of_FilterMode_4() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___FilterMode_4)); }
	inline int32_t get_FilterMode_4() const { return ___FilterMode_4; }
	inline int32_t* get_address_of_FilterMode_4() { return &___FilterMode_4; }
	inline void set_FilterMode_4(int32_t value)
	{
		___FilterMode_4 = value;
	}

	inline static int32_t get_offset_of_CullingMask_5() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___CullingMask_5)); }
	inline LayerMask_t3188175821  get_CullingMask_5() const { return ___CullingMask_5; }
	inline LayerMask_t3188175821 * get_address_of_CullingMask_5() { return &___CullingMask_5; }
	inline void set_CullingMask_5(LayerMask_t3188175821  value)
	{
		___CullingMask_5 = value;
	}

	inline static int32_t get_offset_of_RenderingPath_6() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___RenderingPath_6)); }
	inline int32_t get_RenderingPath_6() const { return ___RenderingPath_6; }
	inline int32_t* get_address_of_RenderingPath_6() { return &___RenderingPath_6; }
	inline void set_RenderingPath_6(int32_t value)
	{
		___RenderingPath_6 = value;
	}

	inline static int32_t get_offset_of_FPSWhenMoveCamera_7() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___FPSWhenMoveCamera_7)); }
	inline int32_t get_FPSWhenMoveCamera_7() const { return ___FPSWhenMoveCamera_7; }
	inline int32_t* get_address_of_FPSWhenMoveCamera_7() { return &___FPSWhenMoveCamera_7; }
	inline void set_FPSWhenMoveCamera_7(int32_t value)
	{
		___FPSWhenMoveCamera_7 = value;
	}

	inline static int32_t get_offset_of_FPSWhenStaticCamera_8() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___FPSWhenStaticCamera_8)); }
	inline int32_t get_FPSWhenStaticCamera_8() const { return ___FPSWhenStaticCamera_8; }
	inline int32_t* get_address_of_FPSWhenStaticCamera_8() { return &___FPSWhenStaticCamera_8; }
	inline void set_FPSWhenStaticCamera_8(int32_t value)
	{
		___FPSWhenStaticCamera_8 = value;
	}

	inline static int32_t get_offset_of_renderTexture_9() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___renderTexture_9)); }
	inline RenderTexture_t2666733923 * get_renderTexture_9() const { return ___renderTexture_9; }
	inline RenderTexture_t2666733923 ** get_address_of_renderTexture_9() { return &___renderTexture_9; }
	inline void set_renderTexture_9(RenderTexture_t2666733923 * value)
	{
		___renderTexture_9 = value;
		Il2CppCodeGenWriteBarrier(&___renderTexture_9, value);
	}

	inline static int32_t get_offset_of_cameraInstance_10() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___cameraInstance_10)); }
	inline Camera_t189460977 * get_cameraInstance_10() const { return ___cameraInstance_10; }
	inline Camera_t189460977 ** get_address_of_cameraInstance_10() { return &___cameraInstance_10; }
	inline void set_cameraInstance_10(Camera_t189460977 * value)
	{
		___cameraInstance_10 = value;
		Il2CppCodeGenWriteBarrier(&___cameraInstance_10, value);
	}

	inline static int32_t get_offset_of_goCamera_11() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___goCamera_11)); }
	inline GameObject_t1756533147 * get_goCamera_11() const { return ___goCamera_11; }
	inline GameObject_t1756533147 ** get_address_of_goCamera_11() { return &___goCamera_11; }
	inline void set_goCamera_11(GameObject_t1756533147 * value)
	{
		___goCamera_11 = value;
		Il2CppCodeGenWriteBarrier(&___goCamera_11, value);
	}

	inline static int32_t get_offset_of_oldPosition_12() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___oldPosition_12)); }
	inline Vector3_t2243707580  get_oldPosition_12() const { return ___oldPosition_12; }
	inline Vector3_t2243707580 * get_address_of_oldPosition_12() { return &___oldPosition_12; }
	inline void set_oldPosition_12(Vector3_t2243707580  value)
	{
		___oldPosition_12 = value;
	}

	inline static int32_t get_offset_of_oldRotation_13() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___oldRotation_13)); }
	inline Quaternion_t4030073918  get_oldRotation_13() const { return ___oldRotation_13; }
	inline Quaternion_t4030073918 * get_address_of_oldRotation_13() { return &___oldRotation_13; }
	inline void set_oldRotation_13(Quaternion_t4030073918  value)
	{
		___oldRotation_13 = value;
	}

	inline static int32_t get_offset_of_instanceCameraTransform_14() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___instanceCameraTransform_14)); }
	inline Transform_t3275118058 * get_instanceCameraTransform_14() const { return ___instanceCameraTransform_14; }
	inline Transform_t3275118058 ** get_address_of_instanceCameraTransform_14() { return &___instanceCameraTransform_14; }
	inline void set_instanceCameraTransform_14(Transform_t3275118058 * value)
	{
		___instanceCameraTransform_14 = value;
		Il2CppCodeGenWriteBarrier(&___instanceCameraTransform_14, value);
	}

	inline static int32_t get_offset_of_canUpdateCamera_15() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___canUpdateCamera_15)); }
	inline bool get_canUpdateCamera_15() const { return ___canUpdateCamera_15; }
	inline bool* get_address_of_canUpdateCamera_15() { return &___canUpdateCamera_15; }
	inline void set_canUpdateCamera_15(bool value)
	{
		___canUpdateCamera_15 = value;
	}

	inline static int32_t get_offset_of_isStaticUpdate_16() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___isStaticUpdate_16)); }
	inline bool get_isStaticUpdate_16() const { return ___isStaticUpdate_16; }
	inline bool* get_address_of_isStaticUpdate_16() { return &___isStaticUpdate_16; }
	inline void set_isStaticUpdate_16(bool value)
	{
		___isStaticUpdate_16 = value;
	}

	inline static int32_t get_offset_of_fpsMove_17() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___fpsMove_17)); }
	inline WaitForSeconds_t3839502067 * get_fpsMove_17() const { return ___fpsMove_17; }
	inline WaitForSeconds_t3839502067 ** get_address_of_fpsMove_17() { return &___fpsMove_17; }
	inline void set_fpsMove_17(WaitForSeconds_t3839502067 * value)
	{
		___fpsMove_17 = value;
		Il2CppCodeGenWriteBarrier(&___fpsMove_17, value);
	}

	inline static int32_t get_offset_of_fpsStatic_18() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___fpsStatic_18)); }
	inline WaitForSeconds_t3839502067 * get_fpsStatic_18() const { return ___fpsStatic_18; }
	inline WaitForSeconds_t3839502067 ** get_address_of_fpsStatic_18() { return &___fpsStatic_18; }
	inline void set_fpsStatic_18(WaitForSeconds_t3839502067 * value)
	{
		___fpsStatic_18 = value;
		Il2CppCodeGenWriteBarrier(&___fpsStatic_18, value);
	}

	inline static int32_t get_offset_of_frameCountWhenCameraIsStatic_20() { return static_cast<int32_t>(offsetof(DistortionMobileCamera_t1665251130, ___frameCountWhenCameraIsStatic_20)); }
	inline int32_t get_frameCountWhenCameraIsStatic_20() const { return ___frameCountWhenCameraIsStatic_20; }
	inline int32_t* get_address_of_frameCountWhenCameraIsStatic_20() { return &___frameCountWhenCameraIsStatic_20; }
	inline void set_frameCountWhenCameraIsStatic_20(int32_t value)
	{
		___frameCountWhenCameraIsStatic_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
