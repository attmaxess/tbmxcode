﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Camera
struct Camera_t189460977;
// DistortionMobileCamera
struct DistortionMobileCamera_t1665251130;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DistortionMobileCamera/<Initialize>c__Iterator2
struct  U3CInitializeU3Ec__Iterator2_t2039585614  : public Il2CppObject
{
public:
	// UnityEngine.Camera DistortionMobileCamera/<Initialize>c__Iterator2::<cam>__0
	Camera_t189460977 * ___U3CcamU3E__0_0;
	// DistortionMobileCamera DistortionMobileCamera/<Initialize>c__Iterator2::$this
	DistortionMobileCamera_t1665251130 * ___U24this_1;
	// System.Object DistortionMobileCamera/<Initialize>c__Iterator2::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean DistortionMobileCamera/<Initialize>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 DistortionMobileCamera/<Initialize>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcamU3E__0_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator2_t2039585614, ___U3CcamU3E__0_0)); }
	inline Camera_t189460977 * get_U3CcamU3E__0_0() const { return ___U3CcamU3E__0_0; }
	inline Camera_t189460977 ** get_address_of_U3CcamU3E__0_0() { return &___U3CcamU3E__0_0; }
	inline void set_U3CcamU3E__0_0(Camera_t189460977 * value)
	{
		___U3CcamU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcamU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator2_t2039585614, ___U24this_1)); }
	inline DistortionMobileCamera_t1665251130 * get_U24this_1() const { return ___U24this_1; }
	inline DistortionMobileCamera_t1665251130 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DistortionMobileCamera_t1665251130 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator2_t2039585614, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator2_t2039585614, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator2_t2039585614, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
