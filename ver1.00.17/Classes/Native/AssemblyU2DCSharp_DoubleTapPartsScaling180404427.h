﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_eSCALINGSELECTSTATE1537437653.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoubleTapPartsScaling
struct  DoubleTapPartsScaling_t180404427  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DoubleTapPartsScaling::IsPunchMoving
	bool ___IsPunchMoving_2;
	// System.Single[] DoubleTapPartsScaling::m_fRaito
	SingleU5BU5D_t577127397* ___m_fRaito_3;
	// eSCALINGSELECTSTATE DoubleTapPartsScaling::m_ScalingSelectState
	int32_t ___m_ScalingSelectState_4;
	// System.Boolean DoubleTapPartsScaling::m_bChild
	bool ___m_bChild_5;
	// UnityEngine.GameObject DoubleTapPartsScaling::m_CoreObj
	GameObject_t1756533147 * ___m_CoreObj_6;
	// UnityEngine.GameObject DoubleTapPartsScaling::m_PrevObj
	GameObject_t1756533147 * ___m_PrevObj_7;
	// System.Single DoubleTapPartsScaling::m_fCurrentRaito
	float ___m_fCurrentRaito_8;
	// System.Int32 DoubleTapPartsScaling::m_sRaitoIndex
	int32_t ___m_sRaitoIndex_9;
	// UnityEngine.Vector3 DoubleTapPartsScaling::m_vOriginalScale
	Vector3_t2243707580  ___m_vOriginalScale_10;
	// System.Boolean DoubleTapPartsScaling::m_bOriginal
	bool ___m_bOriginal_11;
	// System.Single DoubleTapPartsScaling::m_fDefScale
	float ___m_fDefScale_12;

public:
	inline static int32_t get_offset_of_IsPunchMoving_2() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___IsPunchMoving_2)); }
	inline bool get_IsPunchMoving_2() const { return ___IsPunchMoving_2; }
	inline bool* get_address_of_IsPunchMoving_2() { return &___IsPunchMoving_2; }
	inline void set_IsPunchMoving_2(bool value)
	{
		___IsPunchMoving_2 = value;
	}

	inline static int32_t get_offset_of_m_fRaito_3() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_fRaito_3)); }
	inline SingleU5BU5D_t577127397* get_m_fRaito_3() const { return ___m_fRaito_3; }
	inline SingleU5BU5D_t577127397** get_address_of_m_fRaito_3() { return &___m_fRaito_3; }
	inline void set_m_fRaito_3(SingleU5BU5D_t577127397* value)
	{
		___m_fRaito_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_fRaito_3, value);
	}

	inline static int32_t get_offset_of_m_ScalingSelectState_4() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_ScalingSelectState_4)); }
	inline int32_t get_m_ScalingSelectState_4() const { return ___m_ScalingSelectState_4; }
	inline int32_t* get_address_of_m_ScalingSelectState_4() { return &___m_ScalingSelectState_4; }
	inline void set_m_ScalingSelectState_4(int32_t value)
	{
		___m_ScalingSelectState_4 = value;
	}

	inline static int32_t get_offset_of_m_bChild_5() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_bChild_5)); }
	inline bool get_m_bChild_5() const { return ___m_bChild_5; }
	inline bool* get_address_of_m_bChild_5() { return &___m_bChild_5; }
	inline void set_m_bChild_5(bool value)
	{
		___m_bChild_5 = value;
	}

	inline static int32_t get_offset_of_m_CoreObj_6() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_CoreObj_6)); }
	inline GameObject_t1756533147 * get_m_CoreObj_6() const { return ___m_CoreObj_6; }
	inline GameObject_t1756533147 ** get_address_of_m_CoreObj_6() { return &___m_CoreObj_6; }
	inline void set_m_CoreObj_6(GameObject_t1756533147 * value)
	{
		___m_CoreObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_CoreObj_6, value);
	}

	inline static int32_t get_offset_of_m_PrevObj_7() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_PrevObj_7)); }
	inline GameObject_t1756533147 * get_m_PrevObj_7() const { return ___m_PrevObj_7; }
	inline GameObject_t1756533147 ** get_address_of_m_PrevObj_7() { return &___m_PrevObj_7; }
	inline void set_m_PrevObj_7(GameObject_t1756533147 * value)
	{
		___m_PrevObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_PrevObj_7, value);
	}

	inline static int32_t get_offset_of_m_fCurrentRaito_8() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_fCurrentRaito_8)); }
	inline float get_m_fCurrentRaito_8() const { return ___m_fCurrentRaito_8; }
	inline float* get_address_of_m_fCurrentRaito_8() { return &___m_fCurrentRaito_8; }
	inline void set_m_fCurrentRaito_8(float value)
	{
		___m_fCurrentRaito_8 = value;
	}

	inline static int32_t get_offset_of_m_sRaitoIndex_9() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_sRaitoIndex_9)); }
	inline int32_t get_m_sRaitoIndex_9() const { return ___m_sRaitoIndex_9; }
	inline int32_t* get_address_of_m_sRaitoIndex_9() { return &___m_sRaitoIndex_9; }
	inline void set_m_sRaitoIndex_9(int32_t value)
	{
		___m_sRaitoIndex_9 = value;
	}

	inline static int32_t get_offset_of_m_vOriginalScale_10() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_vOriginalScale_10)); }
	inline Vector3_t2243707580  get_m_vOriginalScale_10() const { return ___m_vOriginalScale_10; }
	inline Vector3_t2243707580 * get_address_of_m_vOriginalScale_10() { return &___m_vOriginalScale_10; }
	inline void set_m_vOriginalScale_10(Vector3_t2243707580  value)
	{
		___m_vOriginalScale_10 = value;
	}

	inline static int32_t get_offset_of_m_bOriginal_11() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_bOriginal_11)); }
	inline bool get_m_bOriginal_11() const { return ___m_bOriginal_11; }
	inline bool* get_address_of_m_bOriginal_11() { return &___m_bOriginal_11; }
	inline void set_m_bOriginal_11(bool value)
	{
		___m_bOriginal_11 = value;
	}

	inline static int32_t get_offset_of_m_fDefScale_12() { return static_cast<int32_t>(offsetof(DoubleTapPartsScaling_t180404427, ___m_fDefScale_12)); }
	inline float get_m_fDefScale_12() const { return ___m_fDefScale_12; }
	inline float* get_address_of_m_fDefScale_12() { return &___m_fDefScale_12; }
	inline void set_m_fDefScale_12(float value)
	{
		___m_fDefScale_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
