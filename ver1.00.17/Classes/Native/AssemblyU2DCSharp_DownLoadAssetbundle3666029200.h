﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2362705535.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// PreloadingManager
struct PreloadingManager_t3491463554;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownLoadAssetbundle
struct  DownLoadAssetbundle_t3666029200  : public SingletonMonoBehaviour_1_t2362705535
{
public:
	// System.String DownLoadAssetbundle::file_SaveUrl
	String_t* ___file_SaveUrl_3;
	// System.String[] DownLoadAssetbundle::downLoadAssetName
	StringU5BU5D_t1642385972* ___downLoadAssetName_4;
	// PreloadingManager DownLoadAssetbundle::m_preLoadingManager
	PreloadingManager_t3491463554 * ___m_preLoadingManager_5;
	// System.Int32 DownLoadAssetbundle::downLoadCnt
	int32_t ___downLoadCnt_6;

public:
	inline static int32_t get_offset_of_file_SaveUrl_3() { return static_cast<int32_t>(offsetof(DownLoadAssetbundle_t3666029200, ___file_SaveUrl_3)); }
	inline String_t* get_file_SaveUrl_3() const { return ___file_SaveUrl_3; }
	inline String_t** get_address_of_file_SaveUrl_3() { return &___file_SaveUrl_3; }
	inline void set_file_SaveUrl_3(String_t* value)
	{
		___file_SaveUrl_3 = value;
		Il2CppCodeGenWriteBarrier(&___file_SaveUrl_3, value);
	}

	inline static int32_t get_offset_of_downLoadAssetName_4() { return static_cast<int32_t>(offsetof(DownLoadAssetbundle_t3666029200, ___downLoadAssetName_4)); }
	inline StringU5BU5D_t1642385972* get_downLoadAssetName_4() const { return ___downLoadAssetName_4; }
	inline StringU5BU5D_t1642385972** get_address_of_downLoadAssetName_4() { return &___downLoadAssetName_4; }
	inline void set_downLoadAssetName_4(StringU5BU5D_t1642385972* value)
	{
		___downLoadAssetName_4 = value;
		Il2CppCodeGenWriteBarrier(&___downLoadAssetName_4, value);
	}

	inline static int32_t get_offset_of_m_preLoadingManager_5() { return static_cast<int32_t>(offsetof(DownLoadAssetbundle_t3666029200, ___m_preLoadingManager_5)); }
	inline PreloadingManager_t3491463554 * get_m_preLoadingManager_5() const { return ___m_preLoadingManager_5; }
	inline PreloadingManager_t3491463554 ** get_address_of_m_preLoadingManager_5() { return &___m_preLoadingManager_5; }
	inline void set_m_preLoadingManager_5(PreloadingManager_t3491463554 * value)
	{
		___m_preLoadingManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_preLoadingManager_5, value);
	}

	inline static int32_t get_offset_of_downLoadCnt_6() { return static_cast<int32_t>(offsetof(DownLoadAssetbundle_t3666029200, ___downLoadCnt_6)); }
	inline int32_t get_downLoadCnt_6() const { return ___downLoadCnt_6; }
	inline int32_t* get_address_of_downLoadCnt_6() { return &___downLoadCnt_6; }
	inline void set_downLoadCnt_6(int32_t value)
	{
		___downLoadCnt_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
