﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// DownLoadAssetbundle
struct DownLoadAssetbundle_t3666029200;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownLoadAssetbundle/<DownFile>c__Iterator0
struct  U3CDownFileU3Ec__Iterator0_t1466681690  : public Il2CppObject
{
public:
	// System.String DownLoadAssetbundle/<DownFile>c__Iterator0::url
	String_t* ___url_0;
	// System.String DownLoadAssetbundle/<DownFile>c__Iterator0::name
	String_t* ___name_1;
	// UnityEngine.WWW DownLoadAssetbundle/<DownFile>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_2;
	// DownLoadAssetbundle DownLoadAssetbundle/<DownFile>c__Iterator0::$this
	DownLoadAssetbundle_t3666029200 * ___U24this_3;
	// System.Object DownLoadAssetbundle/<DownFile>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean DownLoadAssetbundle/<DownFile>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 DownLoadAssetbundle/<DownFile>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CDownFileU3Ec__Iterator0_t1466681690, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CDownFileU3Ec__Iterator0_t1466681690, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDownFileU3Ec__Iterator0_t1466681690, ___U3CwwwU3E__0_2)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDownFileU3Ec__Iterator0_t1466681690, ___U24this_3)); }
	inline DownLoadAssetbundle_t3666029200 * get_U24this_3() const { return ___U24this_3; }
	inline DownLoadAssetbundle_t3666029200 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(DownLoadAssetbundle_t3666029200 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDownFileU3Ec__Iterator0_t1466681690, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDownFileU3Ec__Iterator0_t1466681690, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDownFileU3Ec__Iterator0_t1466681690, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
