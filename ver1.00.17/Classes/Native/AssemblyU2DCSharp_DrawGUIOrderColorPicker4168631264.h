﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ColorPicker[]
struct ColorPickerU5BU5D_t1367436620;
// System.Collections.Generic.List`1<ColorPicker>
struct List_1_t2404327357;
// System.Func`2<ColorPicker,System.Int32>
struct Func_2_t4255702116;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawGUIOrderColorPicker
struct  DrawGUIOrderColorPicker_t4168631264  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DrawGUIOrderColorPicker::loadFromScene
	bool ___loadFromScene_2;
	// ColorPicker[] DrawGUIOrderColorPicker::colorPicker
	ColorPickerU5BU5D_t1367436620* ___colorPicker_3;
	// System.Collections.Generic.List`1<ColorPicker> DrawGUIOrderColorPicker::mColorPickerList
	List_1_t2404327357 * ___mColorPickerList_4;

public:
	inline static int32_t get_offset_of_loadFromScene_2() { return static_cast<int32_t>(offsetof(DrawGUIOrderColorPicker_t4168631264, ___loadFromScene_2)); }
	inline bool get_loadFromScene_2() const { return ___loadFromScene_2; }
	inline bool* get_address_of_loadFromScene_2() { return &___loadFromScene_2; }
	inline void set_loadFromScene_2(bool value)
	{
		___loadFromScene_2 = value;
	}

	inline static int32_t get_offset_of_colorPicker_3() { return static_cast<int32_t>(offsetof(DrawGUIOrderColorPicker_t4168631264, ___colorPicker_3)); }
	inline ColorPickerU5BU5D_t1367436620* get_colorPicker_3() const { return ___colorPicker_3; }
	inline ColorPickerU5BU5D_t1367436620** get_address_of_colorPicker_3() { return &___colorPicker_3; }
	inline void set_colorPicker_3(ColorPickerU5BU5D_t1367436620* value)
	{
		___colorPicker_3 = value;
		Il2CppCodeGenWriteBarrier(&___colorPicker_3, value);
	}

	inline static int32_t get_offset_of_mColorPickerList_4() { return static_cast<int32_t>(offsetof(DrawGUIOrderColorPicker_t4168631264, ___mColorPickerList_4)); }
	inline List_1_t2404327357 * get_mColorPickerList_4() const { return ___mColorPickerList_4; }
	inline List_1_t2404327357 ** get_address_of_mColorPickerList_4() { return &___mColorPickerList_4; }
	inline void set_mColorPickerList_4(List_1_t2404327357 * value)
	{
		___mColorPickerList_4 = value;
		Il2CppCodeGenWriteBarrier(&___mColorPickerList_4, value);
	}
};

struct DrawGUIOrderColorPicker_t4168631264_StaticFields
{
public:
	// System.Func`2<ColorPicker,System.Int32> DrawGUIOrderColorPicker::<>f__am$cache0
	Func_2_t4255702116 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(DrawGUIOrderColorPicker_t4168631264_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t4255702116 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t4255702116 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t4255702116 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
