﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditMobilmo
struct  EditMobilmo_t2535788613  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject EditMobilmo::editModuleObject
	GameObject_t1756533147 * ___editModuleObject_2;
	// UnityEngine.GameObject EditMobilmo::mobilmo
	GameObject_t1756533147 * ___mobilmo_3;
	// UnityEngine.GameObject EditMobilmo::m_CreObj
	GameObject_t1756533147 * ___m_CreObj_4;
	// System.Int32 EditMobilmo::m_moduleCount
	int32_t ___m_moduleCount_5;

public:
	inline static int32_t get_offset_of_editModuleObject_2() { return static_cast<int32_t>(offsetof(EditMobilmo_t2535788613, ___editModuleObject_2)); }
	inline GameObject_t1756533147 * get_editModuleObject_2() const { return ___editModuleObject_2; }
	inline GameObject_t1756533147 ** get_address_of_editModuleObject_2() { return &___editModuleObject_2; }
	inline void set_editModuleObject_2(GameObject_t1756533147 * value)
	{
		___editModuleObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___editModuleObject_2, value);
	}

	inline static int32_t get_offset_of_mobilmo_3() { return static_cast<int32_t>(offsetof(EditMobilmo_t2535788613, ___mobilmo_3)); }
	inline GameObject_t1756533147 * get_mobilmo_3() const { return ___mobilmo_3; }
	inline GameObject_t1756533147 ** get_address_of_mobilmo_3() { return &___mobilmo_3; }
	inline void set_mobilmo_3(GameObject_t1756533147 * value)
	{
		___mobilmo_3 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmo_3, value);
	}

	inline static int32_t get_offset_of_m_CreObj_4() { return static_cast<int32_t>(offsetof(EditMobilmo_t2535788613, ___m_CreObj_4)); }
	inline GameObject_t1756533147 * get_m_CreObj_4() const { return ___m_CreObj_4; }
	inline GameObject_t1756533147 ** get_address_of_m_CreObj_4() { return &___m_CreObj_4; }
	inline void set_m_CreObj_4(GameObject_t1756533147 * value)
	{
		___m_CreObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_CreObj_4, value);
	}

	inline static int32_t get_offset_of_m_moduleCount_5() { return static_cast<int32_t>(offsetof(EditMobilmo_t2535788613, ___m_moduleCount_5)); }
	inline int32_t get_m_moduleCount_5() const { return ___m_moduleCount_5; }
	inline int32_t* get_address_of_m_moduleCount_5() { return &___m_moduleCount_5; }
	inline void set_m_moduleCount_5(int32_t value)
	{
		___m_moduleCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
