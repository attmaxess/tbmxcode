﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1595687184.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetMobilityModel
struct GetMobilityModel_t1064189486;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// Mobilmo
struct Mobilmo_t370754809;
// UnityEngine.FixedJoint
struct FixedJoint_t3848069458;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// GetMobilityChildPartsListModel
struct GetMobilityChildPartsListModel_t2691836378;
// ModuleManager
struct ModuleManager_t1065445307;
// Joint[]
struct JointU5BU5D_t171503857;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// EditMobilmo
struct EditMobilmo_t2535788613;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditMobilmo/<reEditMobilmo>c__Iterator0
struct  U3CreEditMobilmoU3Ec__Iterator0_t4018953849  : public Il2CppObject
{
public:
	// System.String EditMobilmo/<reEditMobilmo>c__Iterator0::json
	String_t* ___json_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> EditMobilmo/<reEditMobilmo>c__Iterator0::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_1;
	// GetMobilityModel EditMobilmo/<reEditMobilmo>c__Iterator0::<_getMobilityModel>__0
	GetMobilityModel_t1064189486 * ___U3C_getMobilityModelU3E__0_2;
	// UnityEngine.Rigidbody EditMobilmo/<reEditMobilmo>c__Iterator0::<mobilmoRigid>__0
	Rigidbody_t4233889191 * ___U3CmobilmoRigidU3E__0_3;
	// Mobilmo EditMobilmo/<reEditMobilmo>c__Iterator0::<m_mobilimo>__0
	Mobilmo_t370754809 * ___U3Cm_mobilimoU3E__0_4;
	// UnityEngine.FixedJoint EditMobilmo/<reEditMobilmo>c__Iterator0::<mobilFixed>__0
	FixedJoint_t3848069458 * ___U3CmobilFixedU3E__0_5;
	// UnityEngine.GameObject EditMobilmo/<reEditMobilmo>c__Iterator0::<coreChildObj>__0
	GameObject_t1756533147 * ___U3CcoreChildObjU3E__0_6;
	// UnityEngine.Material EditMobilmo/<reEditMobilmo>c__Iterator0::<coreMat>__0
	Material_t193706927 * ___U3CcoreMatU3E__0_7;
	// System.Collections.Generic.List`1/Enumerator<GetMobilityChildPartsListModel> EditMobilmo/<reEditMobilmo>c__Iterator0::$locvar0
	Enumerator_t1595687184  ___U24locvar0_8;
	// GetMobilityChildPartsListModel EditMobilmo/<reEditMobilmo>c__Iterator0::<child>__1
	GetMobilityChildPartsListModel_t2691836378 * ___U3CchildU3E__1_9;
	// UnityEngine.GameObject EditMobilmo/<reEditMobilmo>c__Iterator0::<ModuleCenter>__2
	GameObject_t1756533147 * ___U3CModuleCenterU3E__2_10;
	// UnityEngine.GameObject EditMobilmo/<reEditMobilmo>c__Iterator0::<ModuleLeap>__2
	GameObject_t1756533147 * ___U3CModuleLeapU3E__2_11;
	// UnityEngine.GameObject EditMobilmo/<reEditMobilmo>c__Iterator0::<ModuleRoll>__2
	GameObject_t1756533147 * ___U3CModuleRollU3E__2_12;
	// ModuleManager EditMobilmo/<reEditMobilmo>c__Iterator0::<moduleMgr>__2
	ModuleManager_t1065445307 * ___U3CmoduleMgrU3E__2_13;
	// System.Int32 EditMobilmo/<reEditMobilmo>c__Iterator0::<parentPartsJointNo>__2
	int32_t ___U3CparentPartsJointNoU3E__2_14;
	// System.String EditMobilmo/<reEditMobilmo>c__Iterator0::<moduleJson>__2
	String_t* ___U3CmoduleJsonU3E__2_15;
	// Joint[] EditMobilmo/<reEditMobilmo>c__Iterator0::<jointParents>__2
	JointU5BU5D_t171503857* ___U3CjointParentsU3E__2_16;
	// System.Int32 EditMobilmo/<reEditMobilmo>c__Iterator0::<modulePartsCnt>__2
	int32_t ___U3CmodulePartsCntU3E__2_17;
	// System.String[] EditMobilmo/<reEditMobilmo>c__Iterator0::<partsIdIndex>__2
	StringU5BU5D_t1642385972* ___U3CpartsIdIndexU3E__2_18;
	// System.Collections.Generic.List`1<System.String> EditMobilmo/<reEditMobilmo>c__Iterator0::<partsIdList>__2
	List_1_t1398341365 * ___U3CpartsIdListU3E__2_19;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EditMobilmo/<reEditMobilmo>c__Iterator0::<modulePartsList>__2
	List_1_t1125654279 * ___U3CmodulePartsListU3E__2_20;
	// UnityEngine.Transform[] EditMobilmo/<reEditMobilmo>c__Iterator0::$locvar5
	TransformU5BU5D_t3764228911* ___U24locvar5_21;
	// System.Int32 EditMobilmo/<reEditMobilmo>c__Iterator0::$locvar6
	int32_t ___U24locvar6_22;
	// UnityEngine.Transform[] EditMobilmo/<reEditMobilmo>c__Iterator0::$locvar7
	TransformU5BU5D_t3764228911* ___U24locvar7_23;
	// System.Int32 EditMobilmo/<reEditMobilmo>c__Iterator0::$locvar8
	int32_t ___U24locvar8_24;
	// UnityEngine.Transform[] EditMobilmo/<reEditMobilmo>c__Iterator0::$locvar11
	TransformU5BU5D_t3764228911* ___U24locvar11_25;
	// System.Int32 EditMobilmo/<reEditMobilmo>c__Iterator0::$locvar12
	int32_t ___U24locvar12_26;
	// EditMobilmo EditMobilmo/<reEditMobilmo>c__Iterator0::$this
	EditMobilmo_t2535788613 * ___U24this_27;
	// System.Object EditMobilmo/<reEditMobilmo>c__Iterator0::$current
	Il2CppObject * ___U24current_28;
	// System.Boolean EditMobilmo/<reEditMobilmo>c__Iterator0::$disposing
	bool ___U24disposing_29;
	// System.Int32 EditMobilmo/<reEditMobilmo>c__Iterator0::$PC
	int32_t ___U24PC_30;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___json_0)); }
	inline String_t* get_json_0() const { return ___json_0; }
	inline String_t** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(String_t* value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier(&___json_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CwwwDataU3E__0_1)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_1() const { return ___U3CwwwDataU3E__0_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_1() { return &___U3CwwwDataU3E__0_1; }
	inline void set_U3CwwwDataU3E__0_1(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3C_getMobilityModelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3C_getMobilityModelU3E__0_2)); }
	inline GetMobilityModel_t1064189486 * get_U3C_getMobilityModelU3E__0_2() const { return ___U3C_getMobilityModelU3E__0_2; }
	inline GetMobilityModel_t1064189486 ** get_address_of_U3C_getMobilityModelU3E__0_2() { return &___U3C_getMobilityModelU3E__0_2; }
	inline void set_U3C_getMobilityModelU3E__0_2(GetMobilityModel_t1064189486 * value)
	{
		___U3C_getMobilityModelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getMobilityModelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CmobilmoRigidU3E__0_3() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CmobilmoRigidU3E__0_3)); }
	inline Rigidbody_t4233889191 * get_U3CmobilmoRigidU3E__0_3() const { return ___U3CmobilmoRigidU3E__0_3; }
	inline Rigidbody_t4233889191 ** get_address_of_U3CmobilmoRigidU3E__0_3() { return &___U3CmobilmoRigidU3E__0_3; }
	inline void set_U3CmobilmoRigidU3E__0_3(Rigidbody_t4233889191 * value)
	{
		___U3CmobilmoRigidU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilmoRigidU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3Cm_mobilimoU3E__0_4() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3Cm_mobilimoU3E__0_4)); }
	inline Mobilmo_t370754809 * get_U3Cm_mobilimoU3E__0_4() const { return ___U3Cm_mobilimoU3E__0_4; }
	inline Mobilmo_t370754809 ** get_address_of_U3Cm_mobilimoU3E__0_4() { return &___U3Cm_mobilimoU3E__0_4; }
	inline void set_U3Cm_mobilimoU3E__0_4(Mobilmo_t370754809 * value)
	{
		___U3Cm_mobilimoU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_mobilimoU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U3CmobilFixedU3E__0_5() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CmobilFixedU3E__0_5)); }
	inline FixedJoint_t3848069458 * get_U3CmobilFixedU3E__0_5() const { return ___U3CmobilFixedU3E__0_5; }
	inline FixedJoint_t3848069458 ** get_address_of_U3CmobilFixedU3E__0_5() { return &___U3CmobilFixedU3E__0_5; }
	inline void set_U3CmobilFixedU3E__0_5(FixedJoint_t3848069458 * value)
	{
		___U3CmobilFixedU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilFixedU3E__0_5, value);
	}

	inline static int32_t get_offset_of_U3CcoreChildObjU3E__0_6() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CcoreChildObjU3E__0_6)); }
	inline GameObject_t1756533147 * get_U3CcoreChildObjU3E__0_6() const { return ___U3CcoreChildObjU3E__0_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CcoreChildObjU3E__0_6() { return &___U3CcoreChildObjU3E__0_6; }
	inline void set_U3CcoreChildObjU3E__0_6(GameObject_t1756533147 * value)
	{
		___U3CcoreChildObjU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreChildObjU3E__0_6, value);
	}

	inline static int32_t get_offset_of_U3CcoreMatU3E__0_7() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CcoreMatU3E__0_7)); }
	inline Material_t193706927 * get_U3CcoreMatU3E__0_7() const { return ___U3CcoreMatU3E__0_7; }
	inline Material_t193706927 ** get_address_of_U3CcoreMatU3E__0_7() { return &___U3CcoreMatU3E__0_7; }
	inline void set_U3CcoreMatU3E__0_7(Material_t193706927 * value)
	{
		___U3CcoreMatU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreMatU3E__0_7, value);
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24locvar0_8)); }
	inline Enumerator_t1595687184  get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline Enumerator_t1595687184 * get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(Enumerator_t1595687184  value)
	{
		___U24locvar0_8 = value;
	}

	inline static int32_t get_offset_of_U3CchildU3E__1_9() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CchildU3E__1_9)); }
	inline GetMobilityChildPartsListModel_t2691836378 * get_U3CchildU3E__1_9() const { return ___U3CchildU3E__1_9; }
	inline GetMobilityChildPartsListModel_t2691836378 ** get_address_of_U3CchildU3E__1_9() { return &___U3CchildU3E__1_9; }
	inline void set_U3CchildU3E__1_9(GetMobilityChildPartsListModel_t2691836378 * value)
	{
		___U3CchildU3E__1_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildU3E__1_9, value);
	}

	inline static int32_t get_offset_of_U3CModuleCenterU3E__2_10() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CModuleCenterU3E__2_10)); }
	inline GameObject_t1756533147 * get_U3CModuleCenterU3E__2_10() const { return ___U3CModuleCenterU3E__2_10; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleCenterU3E__2_10() { return &___U3CModuleCenterU3E__2_10; }
	inline void set_U3CModuleCenterU3E__2_10(GameObject_t1756533147 * value)
	{
		___U3CModuleCenterU3E__2_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleCenterU3E__2_10, value);
	}

	inline static int32_t get_offset_of_U3CModuleLeapU3E__2_11() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CModuleLeapU3E__2_11)); }
	inline GameObject_t1756533147 * get_U3CModuleLeapU3E__2_11() const { return ___U3CModuleLeapU3E__2_11; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleLeapU3E__2_11() { return &___U3CModuleLeapU3E__2_11; }
	inline void set_U3CModuleLeapU3E__2_11(GameObject_t1756533147 * value)
	{
		___U3CModuleLeapU3E__2_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleLeapU3E__2_11, value);
	}

	inline static int32_t get_offset_of_U3CModuleRollU3E__2_12() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CModuleRollU3E__2_12)); }
	inline GameObject_t1756533147 * get_U3CModuleRollU3E__2_12() const { return ___U3CModuleRollU3E__2_12; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleRollU3E__2_12() { return &___U3CModuleRollU3E__2_12; }
	inline void set_U3CModuleRollU3E__2_12(GameObject_t1756533147 * value)
	{
		___U3CModuleRollU3E__2_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleRollU3E__2_12, value);
	}

	inline static int32_t get_offset_of_U3CmoduleMgrU3E__2_13() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CmoduleMgrU3E__2_13)); }
	inline ModuleManager_t1065445307 * get_U3CmoduleMgrU3E__2_13() const { return ___U3CmoduleMgrU3E__2_13; }
	inline ModuleManager_t1065445307 ** get_address_of_U3CmoduleMgrU3E__2_13() { return &___U3CmoduleMgrU3E__2_13; }
	inline void set_U3CmoduleMgrU3E__2_13(ModuleManager_t1065445307 * value)
	{
		___U3CmoduleMgrU3E__2_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleMgrU3E__2_13, value);
	}

	inline static int32_t get_offset_of_U3CparentPartsJointNoU3E__2_14() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CparentPartsJointNoU3E__2_14)); }
	inline int32_t get_U3CparentPartsJointNoU3E__2_14() const { return ___U3CparentPartsJointNoU3E__2_14; }
	inline int32_t* get_address_of_U3CparentPartsJointNoU3E__2_14() { return &___U3CparentPartsJointNoU3E__2_14; }
	inline void set_U3CparentPartsJointNoU3E__2_14(int32_t value)
	{
		___U3CparentPartsJointNoU3E__2_14 = value;
	}

	inline static int32_t get_offset_of_U3CmoduleJsonU3E__2_15() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CmoduleJsonU3E__2_15)); }
	inline String_t* get_U3CmoduleJsonU3E__2_15() const { return ___U3CmoduleJsonU3E__2_15; }
	inline String_t** get_address_of_U3CmoduleJsonU3E__2_15() { return &___U3CmoduleJsonU3E__2_15; }
	inline void set_U3CmoduleJsonU3E__2_15(String_t* value)
	{
		___U3CmoduleJsonU3E__2_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleJsonU3E__2_15, value);
	}

	inline static int32_t get_offset_of_U3CjointParentsU3E__2_16() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CjointParentsU3E__2_16)); }
	inline JointU5BU5D_t171503857* get_U3CjointParentsU3E__2_16() const { return ___U3CjointParentsU3E__2_16; }
	inline JointU5BU5D_t171503857** get_address_of_U3CjointParentsU3E__2_16() { return &___U3CjointParentsU3E__2_16; }
	inline void set_U3CjointParentsU3E__2_16(JointU5BU5D_t171503857* value)
	{
		___U3CjointParentsU3E__2_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjointParentsU3E__2_16, value);
	}

	inline static int32_t get_offset_of_U3CmodulePartsCntU3E__2_17() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CmodulePartsCntU3E__2_17)); }
	inline int32_t get_U3CmodulePartsCntU3E__2_17() const { return ___U3CmodulePartsCntU3E__2_17; }
	inline int32_t* get_address_of_U3CmodulePartsCntU3E__2_17() { return &___U3CmodulePartsCntU3E__2_17; }
	inline void set_U3CmodulePartsCntU3E__2_17(int32_t value)
	{
		___U3CmodulePartsCntU3E__2_17 = value;
	}

	inline static int32_t get_offset_of_U3CpartsIdIndexU3E__2_18() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CpartsIdIndexU3E__2_18)); }
	inline StringU5BU5D_t1642385972* get_U3CpartsIdIndexU3E__2_18() const { return ___U3CpartsIdIndexU3E__2_18; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CpartsIdIndexU3E__2_18() { return &___U3CpartsIdIndexU3E__2_18; }
	inline void set_U3CpartsIdIndexU3E__2_18(StringU5BU5D_t1642385972* value)
	{
		___U3CpartsIdIndexU3E__2_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdIndexU3E__2_18, value);
	}

	inline static int32_t get_offset_of_U3CpartsIdListU3E__2_19() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CpartsIdListU3E__2_19)); }
	inline List_1_t1398341365 * get_U3CpartsIdListU3E__2_19() const { return ___U3CpartsIdListU3E__2_19; }
	inline List_1_t1398341365 ** get_address_of_U3CpartsIdListU3E__2_19() { return &___U3CpartsIdListU3E__2_19; }
	inline void set_U3CpartsIdListU3E__2_19(List_1_t1398341365 * value)
	{
		___U3CpartsIdListU3E__2_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdListU3E__2_19, value);
	}

	inline static int32_t get_offset_of_U3CmodulePartsListU3E__2_20() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U3CmodulePartsListU3E__2_20)); }
	inline List_1_t1125654279 * get_U3CmodulePartsListU3E__2_20() const { return ___U3CmodulePartsListU3E__2_20; }
	inline List_1_t1125654279 ** get_address_of_U3CmodulePartsListU3E__2_20() { return &___U3CmodulePartsListU3E__2_20; }
	inline void set_U3CmodulePartsListU3E__2_20(List_1_t1125654279 * value)
	{
		___U3CmodulePartsListU3E__2_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmodulePartsListU3E__2_20, value);
	}

	inline static int32_t get_offset_of_U24locvar5_21() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24locvar5_21)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar5_21() const { return ___U24locvar5_21; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar5_21() { return &___U24locvar5_21; }
	inline void set_U24locvar5_21(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar5_21 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar5_21, value);
	}

	inline static int32_t get_offset_of_U24locvar6_22() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24locvar6_22)); }
	inline int32_t get_U24locvar6_22() const { return ___U24locvar6_22; }
	inline int32_t* get_address_of_U24locvar6_22() { return &___U24locvar6_22; }
	inline void set_U24locvar6_22(int32_t value)
	{
		___U24locvar6_22 = value;
	}

	inline static int32_t get_offset_of_U24locvar7_23() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24locvar7_23)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar7_23() const { return ___U24locvar7_23; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar7_23() { return &___U24locvar7_23; }
	inline void set_U24locvar7_23(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar7_23 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar7_23, value);
	}

	inline static int32_t get_offset_of_U24locvar8_24() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24locvar8_24)); }
	inline int32_t get_U24locvar8_24() const { return ___U24locvar8_24; }
	inline int32_t* get_address_of_U24locvar8_24() { return &___U24locvar8_24; }
	inline void set_U24locvar8_24(int32_t value)
	{
		___U24locvar8_24 = value;
	}

	inline static int32_t get_offset_of_U24locvar11_25() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24locvar11_25)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar11_25() const { return ___U24locvar11_25; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar11_25() { return &___U24locvar11_25; }
	inline void set_U24locvar11_25(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar11_25 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar11_25, value);
	}

	inline static int32_t get_offset_of_U24locvar12_26() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24locvar12_26)); }
	inline int32_t get_U24locvar12_26() const { return ___U24locvar12_26; }
	inline int32_t* get_address_of_U24locvar12_26() { return &___U24locvar12_26; }
	inline void set_U24locvar12_26(int32_t value)
	{
		___U24locvar12_26 = value;
	}

	inline static int32_t get_offset_of_U24this_27() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24this_27)); }
	inline EditMobilmo_t2535788613 * get_U24this_27() const { return ___U24this_27; }
	inline EditMobilmo_t2535788613 ** get_address_of_U24this_27() { return &___U24this_27; }
	inline void set_U24this_27(EditMobilmo_t2535788613 * value)
	{
		___U24this_27 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_27, value);
	}

	inline static int32_t get_offset_of_U24current_28() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24current_28)); }
	inline Il2CppObject * get_U24current_28() const { return ___U24current_28; }
	inline Il2CppObject ** get_address_of_U24current_28() { return &___U24current_28; }
	inline void set_U24current_28(Il2CppObject * value)
	{
		___U24current_28 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_28, value);
	}

	inline static int32_t get_offset_of_U24disposing_29() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24disposing_29)); }
	inline bool get_U24disposing_29() const { return ___U24disposing_29; }
	inline bool* get_address_of_U24disposing_29() { return &___U24disposing_29; }
	inline void set_U24disposing_29(bool value)
	{
		___U24disposing_29 = value;
	}

	inline static int32_t get_offset_of_U24PC_30() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849, ___U24PC_30)); }
	inline int32_t get_U24PC_30() const { return ___U24PC_30; }
	inline int32_t* get_address_of_U24PC_30() { return &___U24PC_30; }
	inline void set_U24PC_30(int32_t value)
	{
		___U24PC_30 = value;
	}
};

struct U3CreEditMobilmoU3Ec__Iterator0_t4018953849_StaticFields
{
public:
	// System.Func`1<System.Boolean> EditMobilmo/<reEditMobilmo>c__Iterator0::<>f__am$cache0
	Func_1_t1485000104 * ___U3CU3Ef__amU24cache0_31;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_31() { return static_cast<int32_t>(offsetof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849_StaticFields, ___U3CU3Ef__amU24cache0_31)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__amU24cache0_31() const { return ___U3CU3Ef__amU24cache0_31; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__amU24cache0_31() { return &___U3CU3Ef__amU24cache0_31; }
	inline void set_U3CU3Ef__amU24cache0_31(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__amU24cache0_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
