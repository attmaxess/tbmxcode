﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<EnhancedScrollerDemos.CellEvents.Data>
struct List_1_t1799016890;
// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct EnhancedScrollerCellView_t1104668249;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.CellEvents.Controller
struct  Controller_t1811151286  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<EnhancedScrollerDemos.CellEvents.Data> EnhancedScrollerDemos.CellEvents.Controller::_data
	List_1_t1799016890 * ____data_2;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.CellEvents.Controller::scroller
	EnhancedScroller_t2375706558 * ___scroller_3;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.CellEvents.Controller::cellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___cellViewPrefab_4;
	// System.Single EnhancedScrollerDemos.CellEvents.Controller::cellSize
	float ___cellSize_5;

public:
	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(Controller_t1811151286, ____data_2)); }
	inline List_1_t1799016890 * get__data_2() const { return ____data_2; }
	inline List_1_t1799016890 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(List_1_t1799016890 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier(&____data_2, value);
	}

	inline static int32_t get_offset_of_scroller_3() { return static_cast<int32_t>(offsetof(Controller_t1811151286, ___scroller_3)); }
	inline EnhancedScroller_t2375706558 * get_scroller_3() const { return ___scroller_3; }
	inline EnhancedScroller_t2375706558 ** get_address_of_scroller_3() { return &___scroller_3; }
	inline void set_scroller_3(EnhancedScroller_t2375706558 * value)
	{
		___scroller_3 = value;
		Il2CppCodeGenWriteBarrier(&___scroller_3, value);
	}

	inline static int32_t get_offset_of_cellViewPrefab_4() { return static_cast<int32_t>(offsetof(Controller_t1811151286, ___cellViewPrefab_4)); }
	inline EnhancedScrollerCellView_t1104668249 * get_cellViewPrefab_4() const { return ___cellViewPrefab_4; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_cellViewPrefab_4() { return &___cellViewPrefab_4; }
	inline void set_cellViewPrefab_4(EnhancedScrollerCellView_t1104668249 * value)
	{
		___cellViewPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___cellViewPrefab_4, value);
	}

	inline static int32_t get_offset_of_cellSize_5() { return static_cast<int32_t>(offsetof(Controller_t1811151286, ___cellSize_5)); }
	inline float get_cellSize_5() const { return ___cellSize_5; }
	inline float* get_address_of_cellSize_5() { return &___cellSize_5; }
	inline void set_cellSize_5(float value)
	{
		___cellSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
