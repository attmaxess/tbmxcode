﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// EnhancedScrollerDemos.CellEvents.Data
struct Data_t2429895758;
// UnityEngine.UI.Text
struct Text_t356221433;
// EnhancedScrollerDemos.CellEvents.CellButtonTextClickedDelegate
struct CellButtonTextClickedDelegate_t414167209;
// EnhancedScrollerDemos.CellEvents.CellButtonIntegerClickedDelegate
struct CellButtonIntegerClickedDelegate_t3281606456;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.CellEvents.CellView
struct  CellView_t3955020393  : public EnhancedScrollerCellView_t1104668249
{
public:
	// EnhancedScrollerDemos.CellEvents.Data EnhancedScrollerDemos.CellEvents.CellView::_data
	Data_t2429895758 * ____data_6;
	// UnityEngine.UI.Text EnhancedScrollerDemos.CellEvents.CellView::someTextText
	Text_t356221433 * ___someTextText_7;
	// EnhancedScrollerDemos.CellEvents.CellButtonTextClickedDelegate EnhancedScrollerDemos.CellEvents.CellView::cellButtonTextClicked
	CellButtonTextClickedDelegate_t414167209 * ___cellButtonTextClicked_8;
	// EnhancedScrollerDemos.CellEvents.CellButtonIntegerClickedDelegate EnhancedScrollerDemos.CellEvents.CellView::cellButtonFixedIntegerClicked
	CellButtonIntegerClickedDelegate_t3281606456 * ___cellButtonFixedIntegerClicked_9;
	// EnhancedScrollerDemos.CellEvents.CellButtonIntegerClickedDelegate EnhancedScrollerDemos.CellEvents.CellView::cellButtonDataIntegerClicked
	CellButtonIntegerClickedDelegate_t3281606456 * ___cellButtonDataIntegerClicked_10;

public:
	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(CellView_t3955020393, ____data_6)); }
	inline Data_t2429895758 * get__data_6() const { return ____data_6; }
	inline Data_t2429895758 ** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(Data_t2429895758 * value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier(&____data_6, value);
	}

	inline static int32_t get_offset_of_someTextText_7() { return static_cast<int32_t>(offsetof(CellView_t3955020393, ___someTextText_7)); }
	inline Text_t356221433 * get_someTextText_7() const { return ___someTextText_7; }
	inline Text_t356221433 ** get_address_of_someTextText_7() { return &___someTextText_7; }
	inline void set_someTextText_7(Text_t356221433 * value)
	{
		___someTextText_7 = value;
		Il2CppCodeGenWriteBarrier(&___someTextText_7, value);
	}

	inline static int32_t get_offset_of_cellButtonTextClicked_8() { return static_cast<int32_t>(offsetof(CellView_t3955020393, ___cellButtonTextClicked_8)); }
	inline CellButtonTextClickedDelegate_t414167209 * get_cellButtonTextClicked_8() const { return ___cellButtonTextClicked_8; }
	inline CellButtonTextClickedDelegate_t414167209 ** get_address_of_cellButtonTextClicked_8() { return &___cellButtonTextClicked_8; }
	inline void set_cellButtonTextClicked_8(CellButtonTextClickedDelegate_t414167209 * value)
	{
		___cellButtonTextClicked_8 = value;
		Il2CppCodeGenWriteBarrier(&___cellButtonTextClicked_8, value);
	}

	inline static int32_t get_offset_of_cellButtonFixedIntegerClicked_9() { return static_cast<int32_t>(offsetof(CellView_t3955020393, ___cellButtonFixedIntegerClicked_9)); }
	inline CellButtonIntegerClickedDelegate_t3281606456 * get_cellButtonFixedIntegerClicked_9() const { return ___cellButtonFixedIntegerClicked_9; }
	inline CellButtonIntegerClickedDelegate_t3281606456 ** get_address_of_cellButtonFixedIntegerClicked_9() { return &___cellButtonFixedIntegerClicked_9; }
	inline void set_cellButtonFixedIntegerClicked_9(CellButtonIntegerClickedDelegate_t3281606456 * value)
	{
		___cellButtonFixedIntegerClicked_9 = value;
		Il2CppCodeGenWriteBarrier(&___cellButtonFixedIntegerClicked_9, value);
	}

	inline static int32_t get_offset_of_cellButtonDataIntegerClicked_10() { return static_cast<int32_t>(offsetof(CellView_t3955020393, ___cellButtonDataIntegerClicked_10)); }
	inline CellButtonIntegerClickedDelegate_t3281606456 * get_cellButtonDataIntegerClicked_10() const { return ___cellButtonDataIntegerClicked_10; }
	inline CellButtonIntegerClickedDelegate_t3281606456 ** get_address_of_cellButtonDataIntegerClicked_10() { return &___cellButtonDataIntegerClicked_10; }
	inline void set_cellButtonDataIntegerClicked_10(CellButtonIntegerClickedDelegate_t3281606456 * value)
	{
		___cellButtonDataIntegerClicked_10 = value;
		Il2CppCodeGenWriteBarrier(&___cellButtonDataIntegerClicked_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
