﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.JumpToDemo.CellView
struct  CellView_t248633595  : public EnhancedScrollerCellView_t1104668249
{
public:
	// UnityEngine.UI.Text EnhancedScrollerDemos.JumpToDemo.CellView::cellText
	Text_t356221433 * ___cellText_6;

public:
	inline static int32_t get_offset_of_cellText_6() { return static_cast<int32_t>(offsetof(CellView_t248633595, ___cellText_6)); }
	inline Text_t356221433 * get_cellText_6() const { return ___cellText_6; }
	inline Text_t356221433 ** get_address_of_cellText_6() { return &___cellText_6; }
	inline void set_cellText_6(Text_t356221433 * value)
	{
		___cellText_6 = value;
		Il2CppCodeGenWriteBarrier(&___cellText_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
