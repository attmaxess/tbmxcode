﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enhan993038288.h"

// System.Collections.Generic.List`1<EnhancedScrollerDemos.JumpToDemo.Data>
struct List_1_t3380412494;
// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct EnhancedScrollerCellView_t1104668249;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.JumpToDemo.Controller
struct  Controller_t362126806  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<EnhancedScrollerDemos.JumpToDemo.Data> EnhancedScrollerDemos.JumpToDemo.Controller::_data
	List_1_t3380412494 * ____data_2;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.JumpToDemo.Controller::vScroller
	EnhancedScroller_t2375706558 * ___vScroller_3;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.JumpToDemo.Controller::hScroller
	EnhancedScroller_t2375706558 * ___hScroller_4;
	// UnityEngine.UI.InputField EnhancedScrollerDemos.JumpToDemo.Controller::jumpIndexInput
	InputField_t1631627530 * ___jumpIndexInput_5;
	// UnityEngine.UI.Toggle EnhancedScrollerDemos.JumpToDemo.Controller::useSpacingToggle
	Toggle_t3976754468 * ___useSpacingToggle_6;
	// UnityEngine.UI.Slider EnhancedScrollerDemos.JumpToDemo.Controller::scrollerOffsetSlider
	Slider_t297367283 * ___scrollerOffsetSlider_7;
	// UnityEngine.UI.Slider EnhancedScrollerDemos.JumpToDemo.Controller::cellOffsetSlider
	Slider_t297367283 * ___cellOffsetSlider_8;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.JumpToDemo.Controller::cellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___cellViewPrefab_9;
	// EnhancedUI.EnhancedScroller.EnhancedScroller/TweenType EnhancedScrollerDemos.JumpToDemo.Controller::vScrollerTweenType
	int32_t ___vScrollerTweenType_10;
	// System.Single EnhancedScrollerDemos.JumpToDemo.Controller::vScrollerTweenTime
	float ___vScrollerTweenTime_11;
	// EnhancedUI.EnhancedScroller.EnhancedScroller/TweenType EnhancedScrollerDemos.JumpToDemo.Controller::hScrollerTweenType
	int32_t ___hScrollerTweenType_12;
	// System.Single EnhancedScrollerDemos.JumpToDemo.Controller::hScrollerTweenTime
	float ___hScrollerTweenTime_13;

public:
	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(Controller_t362126806, ____data_2)); }
	inline List_1_t3380412494 * get__data_2() const { return ____data_2; }
	inline List_1_t3380412494 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(List_1_t3380412494 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier(&____data_2, value);
	}

	inline static int32_t get_offset_of_vScroller_3() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___vScroller_3)); }
	inline EnhancedScroller_t2375706558 * get_vScroller_3() const { return ___vScroller_3; }
	inline EnhancedScroller_t2375706558 ** get_address_of_vScroller_3() { return &___vScroller_3; }
	inline void set_vScroller_3(EnhancedScroller_t2375706558 * value)
	{
		___vScroller_3 = value;
		Il2CppCodeGenWriteBarrier(&___vScroller_3, value);
	}

	inline static int32_t get_offset_of_hScroller_4() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___hScroller_4)); }
	inline EnhancedScroller_t2375706558 * get_hScroller_4() const { return ___hScroller_4; }
	inline EnhancedScroller_t2375706558 ** get_address_of_hScroller_4() { return &___hScroller_4; }
	inline void set_hScroller_4(EnhancedScroller_t2375706558 * value)
	{
		___hScroller_4 = value;
		Il2CppCodeGenWriteBarrier(&___hScroller_4, value);
	}

	inline static int32_t get_offset_of_jumpIndexInput_5() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___jumpIndexInput_5)); }
	inline InputField_t1631627530 * get_jumpIndexInput_5() const { return ___jumpIndexInput_5; }
	inline InputField_t1631627530 ** get_address_of_jumpIndexInput_5() { return &___jumpIndexInput_5; }
	inline void set_jumpIndexInput_5(InputField_t1631627530 * value)
	{
		___jumpIndexInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___jumpIndexInput_5, value);
	}

	inline static int32_t get_offset_of_useSpacingToggle_6() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___useSpacingToggle_6)); }
	inline Toggle_t3976754468 * get_useSpacingToggle_6() const { return ___useSpacingToggle_6; }
	inline Toggle_t3976754468 ** get_address_of_useSpacingToggle_6() { return &___useSpacingToggle_6; }
	inline void set_useSpacingToggle_6(Toggle_t3976754468 * value)
	{
		___useSpacingToggle_6 = value;
		Il2CppCodeGenWriteBarrier(&___useSpacingToggle_6, value);
	}

	inline static int32_t get_offset_of_scrollerOffsetSlider_7() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___scrollerOffsetSlider_7)); }
	inline Slider_t297367283 * get_scrollerOffsetSlider_7() const { return ___scrollerOffsetSlider_7; }
	inline Slider_t297367283 ** get_address_of_scrollerOffsetSlider_7() { return &___scrollerOffsetSlider_7; }
	inline void set_scrollerOffsetSlider_7(Slider_t297367283 * value)
	{
		___scrollerOffsetSlider_7 = value;
		Il2CppCodeGenWriteBarrier(&___scrollerOffsetSlider_7, value);
	}

	inline static int32_t get_offset_of_cellOffsetSlider_8() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___cellOffsetSlider_8)); }
	inline Slider_t297367283 * get_cellOffsetSlider_8() const { return ___cellOffsetSlider_8; }
	inline Slider_t297367283 ** get_address_of_cellOffsetSlider_8() { return &___cellOffsetSlider_8; }
	inline void set_cellOffsetSlider_8(Slider_t297367283 * value)
	{
		___cellOffsetSlider_8 = value;
		Il2CppCodeGenWriteBarrier(&___cellOffsetSlider_8, value);
	}

	inline static int32_t get_offset_of_cellViewPrefab_9() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___cellViewPrefab_9)); }
	inline EnhancedScrollerCellView_t1104668249 * get_cellViewPrefab_9() const { return ___cellViewPrefab_9; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_cellViewPrefab_9() { return &___cellViewPrefab_9; }
	inline void set_cellViewPrefab_9(EnhancedScrollerCellView_t1104668249 * value)
	{
		___cellViewPrefab_9 = value;
		Il2CppCodeGenWriteBarrier(&___cellViewPrefab_9, value);
	}

	inline static int32_t get_offset_of_vScrollerTweenType_10() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___vScrollerTweenType_10)); }
	inline int32_t get_vScrollerTweenType_10() const { return ___vScrollerTweenType_10; }
	inline int32_t* get_address_of_vScrollerTweenType_10() { return &___vScrollerTweenType_10; }
	inline void set_vScrollerTweenType_10(int32_t value)
	{
		___vScrollerTweenType_10 = value;
	}

	inline static int32_t get_offset_of_vScrollerTweenTime_11() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___vScrollerTweenTime_11)); }
	inline float get_vScrollerTweenTime_11() const { return ___vScrollerTweenTime_11; }
	inline float* get_address_of_vScrollerTweenTime_11() { return &___vScrollerTweenTime_11; }
	inline void set_vScrollerTweenTime_11(float value)
	{
		___vScrollerTweenTime_11 = value;
	}

	inline static int32_t get_offset_of_hScrollerTweenType_12() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___hScrollerTweenType_12)); }
	inline int32_t get_hScrollerTweenType_12() const { return ___hScrollerTweenType_12; }
	inline int32_t* get_address_of_hScrollerTweenType_12() { return &___hScrollerTweenType_12; }
	inline void set_hScrollerTweenType_12(int32_t value)
	{
		___hScrollerTweenType_12 = value;
	}

	inline static int32_t get_offset_of_hScrollerTweenTime_13() { return static_cast<int32_t>(offsetof(Controller_t362126806, ___hScrollerTweenTime_13)); }
	inline float get_hScrollerTweenTime_13() const { return ___hScrollerTweenTime_13; }
	inline float* get_address_of_hScrollerTweenTime_13() { return &___hScrollerTweenTime_13; }
	inline void set_hScrollerTweenTime_13(float value)
	{
		___hScrollerTweenTime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
