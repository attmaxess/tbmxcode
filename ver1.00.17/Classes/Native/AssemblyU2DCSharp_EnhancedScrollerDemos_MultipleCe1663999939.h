﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedScrollerDemos_MultipleCe2523487274.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.MultipleCellTypesDemo.HeaderData
struct  HeaderData_t1663999939  : public Data_t2523487274
{
public:
	// System.String EnhancedScrollerDemos.MultipleCellTypesDemo.HeaderData::category
	String_t* ___category_0;

public:
	inline static int32_t get_offset_of_category_0() { return static_cast<int32_t>(offsetof(HeaderData_t1663999939, ___category_0)); }
	inline String_t* get_category_0() const { return ___category_0; }
	inline String_t** get_address_of_category_0() { return &___category_0; }
	inline void set_category_0(String_t* value)
	{
		___category_0 = value;
		Il2CppCodeGenWriteBarrier(&___category_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
