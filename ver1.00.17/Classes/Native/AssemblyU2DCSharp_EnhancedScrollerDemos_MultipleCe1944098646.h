﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedScrollerDemos_MultipleCe3194962127.h"

// EnhancedScrollerDemos.MultipleCellTypesDemo.HeaderData
struct HeaderData_t1663999939;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewHeader
struct  CellViewHeader_t1944098646  : public CellView_t3194962127
{
public:
	// EnhancedScrollerDemos.MultipleCellTypesDemo.HeaderData EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewHeader::_headerData
	HeaderData_t1663999939 * ____headerData_7;
	// UnityEngine.UI.Text EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewHeader::categoryText
	Text_t356221433 * ___categoryText_8;

public:
	inline static int32_t get_offset_of__headerData_7() { return static_cast<int32_t>(offsetof(CellViewHeader_t1944098646, ____headerData_7)); }
	inline HeaderData_t1663999939 * get__headerData_7() const { return ____headerData_7; }
	inline HeaderData_t1663999939 ** get_address_of__headerData_7() { return &____headerData_7; }
	inline void set__headerData_7(HeaderData_t1663999939 * value)
	{
		____headerData_7 = value;
		Il2CppCodeGenWriteBarrier(&____headerData_7, value);
	}

	inline static int32_t get_offset_of_categoryText_8() { return static_cast<int32_t>(offsetof(CellViewHeader_t1944098646, ___categoryText_8)); }
	inline Text_t356221433 * get_categoryText_8() const { return ___categoryText_8; }
	inline Text_t356221433 ** get_address_of_categoryText_8() { return &___categoryText_8; }
	inline void set_categoryText_8(Text_t356221433 * value)
	{
		___categoryText_8 = value;
		Il2CppCodeGenWriteBarrier(&___categoryText_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
