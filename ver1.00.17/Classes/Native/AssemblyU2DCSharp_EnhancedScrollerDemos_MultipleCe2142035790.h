﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedScrollerDemos_MultipleCe2523487274.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.MultipleCellTypesDemo.RowData
struct  RowData_t2142035790  : public Data_t2523487274
{
public:
	// System.String EnhancedScrollerDemos.MultipleCellTypesDemo.RowData::userName
	String_t* ___userName_0;
	// System.String EnhancedScrollerDemos.MultipleCellTypesDemo.RowData::userAvatarSpritePath
	String_t* ___userAvatarSpritePath_1;
	// System.UInt64 EnhancedScrollerDemos.MultipleCellTypesDemo.RowData::userHighScore
	uint64_t ___userHighScore_2;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(RowData_t2142035790, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier(&___userName_0, value);
	}

	inline static int32_t get_offset_of_userAvatarSpritePath_1() { return static_cast<int32_t>(offsetof(RowData_t2142035790, ___userAvatarSpritePath_1)); }
	inline String_t* get_userAvatarSpritePath_1() const { return ___userAvatarSpritePath_1; }
	inline String_t** get_address_of_userAvatarSpritePath_1() { return &___userAvatarSpritePath_1; }
	inline void set_userAvatarSpritePath_1(String_t* value)
	{
		___userAvatarSpritePath_1 = value;
		Il2CppCodeGenWriteBarrier(&___userAvatarSpritePath_1, value);
	}

	inline static int32_t get_offset_of_userHighScore_2() { return static_cast<int32_t>(offsetof(RowData_t2142035790, ___userHighScore_2)); }
	inline uint64_t get_userHighScore_2() const { return ___userHighScore_2; }
	inline uint64_t* get_address_of_userHighScore_2() { return &___userHighScore_2; }
	inline void set_userHighScore_2(uint64_t value)
	{
		___userHighScore_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
