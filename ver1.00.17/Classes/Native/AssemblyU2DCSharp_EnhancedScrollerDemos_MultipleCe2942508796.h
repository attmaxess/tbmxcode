﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// EnhancedUI.SmallList`1<EnhancedScrollerDemos.MultipleCellTypesDemo.Data>
struct SmallList_1_t288699201;
// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct EnhancedScrollerCellView_t1104668249;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.MultipleCellTypesDemo.MultipleCellTypesDemo
struct  MultipleCellTypesDemo_t2942508796  : public MonoBehaviour_t1158329972
{
public:
	// EnhancedUI.SmallList`1<EnhancedScrollerDemos.MultipleCellTypesDemo.Data> EnhancedScrollerDemos.MultipleCellTypesDemo.MultipleCellTypesDemo::_data
	SmallList_1_t288699201 * ____data_2;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.MultipleCellTypesDemo.MultipleCellTypesDemo::scroller
	EnhancedScroller_t2375706558 * ___scroller_3;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.MultipleCellTypesDemo.MultipleCellTypesDemo::headerCellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___headerCellViewPrefab_4;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.MultipleCellTypesDemo.MultipleCellTypesDemo::rowCellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___rowCellViewPrefab_5;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.MultipleCellTypesDemo.MultipleCellTypesDemo::footerCellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___footerCellViewPrefab_6;
	// System.String EnhancedScrollerDemos.MultipleCellTypesDemo.MultipleCellTypesDemo::resourcePath
	String_t* ___resourcePath_7;

public:
	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(MultipleCellTypesDemo_t2942508796, ____data_2)); }
	inline SmallList_1_t288699201 * get__data_2() const { return ____data_2; }
	inline SmallList_1_t288699201 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(SmallList_1_t288699201 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier(&____data_2, value);
	}

	inline static int32_t get_offset_of_scroller_3() { return static_cast<int32_t>(offsetof(MultipleCellTypesDemo_t2942508796, ___scroller_3)); }
	inline EnhancedScroller_t2375706558 * get_scroller_3() const { return ___scroller_3; }
	inline EnhancedScroller_t2375706558 ** get_address_of_scroller_3() { return &___scroller_3; }
	inline void set_scroller_3(EnhancedScroller_t2375706558 * value)
	{
		___scroller_3 = value;
		Il2CppCodeGenWriteBarrier(&___scroller_3, value);
	}

	inline static int32_t get_offset_of_headerCellViewPrefab_4() { return static_cast<int32_t>(offsetof(MultipleCellTypesDemo_t2942508796, ___headerCellViewPrefab_4)); }
	inline EnhancedScrollerCellView_t1104668249 * get_headerCellViewPrefab_4() const { return ___headerCellViewPrefab_4; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_headerCellViewPrefab_4() { return &___headerCellViewPrefab_4; }
	inline void set_headerCellViewPrefab_4(EnhancedScrollerCellView_t1104668249 * value)
	{
		___headerCellViewPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___headerCellViewPrefab_4, value);
	}

	inline static int32_t get_offset_of_rowCellViewPrefab_5() { return static_cast<int32_t>(offsetof(MultipleCellTypesDemo_t2942508796, ___rowCellViewPrefab_5)); }
	inline EnhancedScrollerCellView_t1104668249 * get_rowCellViewPrefab_5() const { return ___rowCellViewPrefab_5; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_rowCellViewPrefab_5() { return &___rowCellViewPrefab_5; }
	inline void set_rowCellViewPrefab_5(EnhancedScrollerCellView_t1104668249 * value)
	{
		___rowCellViewPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___rowCellViewPrefab_5, value);
	}

	inline static int32_t get_offset_of_footerCellViewPrefab_6() { return static_cast<int32_t>(offsetof(MultipleCellTypesDemo_t2942508796, ___footerCellViewPrefab_6)); }
	inline EnhancedScrollerCellView_t1104668249 * get_footerCellViewPrefab_6() const { return ___footerCellViewPrefab_6; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_footerCellViewPrefab_6() { return &___footerCellViewPrefab_6; }
	inline void set_footerCellViewPrefab_6(EnhancedScrollerCellView_t1104668249 * value)
	{
		___footerCellViewPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___footerCellViewPrefab_6, value);
	}

	inline static int32_t get_offset_of_resourcePath_7() { return static_cast<int32_t>(offsetof(MultipleCellTypesDemo_t2942508796, ___resourcePath_7)); }
	inline String_t* get_resourcePath_7() const { return ___resourcePath_7; }
	inline String_t** get_address_of_resourcePath_7() { return &___resourcePath_7; }
	inline void set_resourcePath_7(String_t* value)
	{
		___resourcePath_7 = value;
		Il2CppCodeGenWriteBarrier(&___resourcePath_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
