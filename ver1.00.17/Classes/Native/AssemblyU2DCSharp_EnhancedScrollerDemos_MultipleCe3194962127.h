﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// EnhancedScrollerDemos.MultipleCellTypesDemo.Data
struct Data_t2523487274;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.MultipleCellTypesDemo.CellView
struct  CellView_t3194962127  : public EnhancedScrollerCellView_t1104668249
{
public:
	// EnhancedScrollerDemos.MultipleCellTypesDemo.Data EnhancedScrollerDemos.MultipleCellTypesDemo.CellView::_data
	Data_t2523487274 * ____data_6;

public:
	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(CellView_t3194962127, ____data_6)); }
	inline Data_t2523487274 * get__data_6() const { return ____data_6; }
	inline Data_t2523487274 ** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(Data_t2523487274 * value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier(&____data_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
