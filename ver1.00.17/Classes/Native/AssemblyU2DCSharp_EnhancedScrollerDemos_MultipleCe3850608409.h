﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedScrollerDemos_MultipleCe3194962127.h"

// EnhancedScrollerDemos.MultipleCellTypesDemo.RowData
struct RowData_t2142035790;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewRow
struct  CellViewRow_t3850608409  : public CellView_t3194962127
{
public:
	// EnhancedScrollerDemos.MultipleCellTypesDemo.RowData EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewRow::_rowData
	RowData_t2142035790 * ____rowData_7;
	// UnityEngine.UI.Text EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewRow::userNameText
	Text_t356221433 * ___userNameText_8;
	// UnityEngine.UI.Image EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewRow::userAvatarImage
	Image_t2042527209 * ___userAvatarImage_9;
	// UnityEngine.UI.Text EnhancedScrollerDemos.MultipleCellTypesDemo.CellViewRow::userHighScoreText
	Text_t356221433 * ___userHighScoreText_10;

public:
	inline static int32_t get_offset_of__rowData_7() { return static_cast<int32_t>(offsetof(CellViewRow_t3850608409, ____rowData_7)); }
	inline RowData_t2142035790 * get__rowData_7() const { return ____rowData_7; }
	inline RowData_t2142035790 ** get_address_of__rowData_7() { return &____rowData_7; }
	inline void set__rowData_7(RowData_t2142035790 * value)
	{
		____rowData_7 = value;
		Il2CppCodeGenWriteBarrier(&____rowData_7, value);
	}

	inline static int32_t get_offset_of_userNameText_8() { return static_cast<int32_t>(offsetof(CellViewRow_t3850608409, ___userNameText_8)); }
	inline Text_t356221433 * get_userNameText_8() const { return ___userNameText_8; }
	inline Text_t356221433 ** get_address_of_userNameText_8() { return &___userNameText_8; }
	inline void set_userNameText_8(Text_t356221433 * value)
	{
		___userNameText_8 = value;
		Il2CppCodeGenWriteBarrier(&___userNameText_8, value);
	}

	inline static int32_t get_offset_of_userAvatarImage_9() { return static_cast<int32_t>(offsetof(CellViewRow_t3850608409, ___userAvatarImage_9)); }
	inline Image_t2042527209 * get_userAvatarImage_9() const { return ___userAvatarImage_9; }
	inline Image_t2042527209 ** get_address_of_userAvatarImage_9() { return &___userAvatarImage_9; }
	inline void set_userAvatarImage_9(Image_t2042527209 * value)
	{
		___userAvatarImage_9 = value;
		Il2CppCodeGenWriteBarrier(&___userAvatarImage_9, value);
	}

	inline static int32_t get_offset_of_userHighScoreText_10() { return static_cast<int32_t>(offsetof(CellViewRow_t3850608409, ___userHighScoreText_10)); }
	inline Text_t356221433 * get_userHighScoreText_10() const { return ___userHighScoreText_10; }
	inline Text_t356221433 ** get_address_of_userHighScoreText_10() { return &___userHighScoreText_10; }
	inline void set_userHighScoreText_10(Text_t356221433 * value)
	{
		___userHighScoreText_10 = value;
		Il2CppCodeGenWriteBarrier(&___userHighScoreText_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
