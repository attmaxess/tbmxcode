﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.RefreshDemo.Data
struct  Data_t1832928442  : public Il2CppObject
{
public:
	// System.String EnhancedScrollerDemos.RefreshDemo.Data::someText
	String_t* ___someText_0;

public:
	inline static int32_t get_offset_of_someText_0() { return static_cast<int32_t>(offsetof(Data_t1832928442, ___someText_0)); }
	inline String_t* get_someText_0() const { return ___someText_0; }
	inline String_t** get_address_of_someText_0() { return &___someText_0; }
	inline void set_someText_0(String_t* value)
	{
		___someText_0 = value;
		Il2CppCodeGenWriteBarrier(&___someText_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
