﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// EnhancedScrollerDemos.RefreshDemo.Data
struct Data_t1832928442;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.RefreshDemo.CellView
struct  CellView_t3268695897  : public EnhancedScrollerCellView_t1104668249
{
public:
	// EnhancedScrollerDemos.RefreshDemo.Data EnhancedScrollerDemos.RefreshDemo.CellView::_data
	Data_t1832928442 * ____data_6;
	// UnityEngine.UI.Text EnhancedScrollerDemos.RefreshDemo.CellView::someTextText
	Text_t356221433 * ___someTextText_7;

public:
	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(CellView_t3268695897, ____data_6)); }
	inline Data_t1832928442 * get__data_6() const { return ____data_6; }
	inline Data_t1832928442 ** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(Data_t1832928442 * value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier(&____data_6, value);
	}

	inline static int32_t get_offset_of_someTextText_7() { return static_cast<int32_t>(offsetof(CellView_t3268695897, ___someTextText_7)); }
	inline Text_t356221433 * get_someTextText_7() const { return ___someTextText_7; }
	inline Text_t356221433 ** get_address_of_someTextText_7() { return &___someTextText_7; }
	inline void set_someTextText_7(Text_t356221433 * value)
	{
		___someTextText_7 = value;
		Il2CppCodeGenWriteBarrier(&___someTextText_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
