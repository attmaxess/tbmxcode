﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.RemoteResourcesDemo.CellView
struct  CellView_t3730766081  : public EnhancedScrollerCellView_t1104668249
{
public:
	// UnityEngine.UI.Image EnhancedScrollerDemos.RemoteResourcesDemo.CellView::cellImage
	Image_t2042527209 * ___cellImage_6;
	// UnityEngine.Sprite EnhancedScrollerDemos.RemoteResourcesDemo.CellView::defaultSprite
	Sprite_t309593783 * ___defaultSprite_7;

public:
	inline static int32_t get_offset_of_cellImage_6() { return static_cast<int32_t>(offsetof(CellView_t3730766081, ___cellImage_6)); }
	inline Image_t2042527209 * get_cellImage_6() const { return ___cellImage_6; }
	inline Image_t2042527209 ** get_address_of_cellImage_6() { return &___cellImage_6; }
	inline void set_cellImage_6(Image_t2042527209 * value)
	{
		___cellImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___cellImage_6, value);
	}

	inline static int32_t get_offset_of_defaultSprite_7() { return static_cast<int32_t>(offsetof(CellView_t3730766081, ___defaultSprite_7)); }
	inline Sprite_t309593783 * get_defaultSprite_7() const { return ___defaultSprite_7; }
	inline Sprite_t309593783 ** get_address_of_defaultSprite_7() { return &___defaultSprite_7; }
	inline void set_defaultSprite_7(Sprite_t309593783 * value)
	{
		___defaultSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___defaultSprite_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
