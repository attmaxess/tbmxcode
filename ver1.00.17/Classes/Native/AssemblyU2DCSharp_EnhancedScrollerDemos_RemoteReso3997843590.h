﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.RemoteResourcesDemo.Data
struct  Data_t3997843590  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 EnhancedScrollerDemos.RemoteResourcesDemo.Data::imageDimensions
	Vector2_t2243707579  ___imageDimensions_0;
	// System.String EnhancedScrollerDemos.RemoteResourcesDemo.Data::imageUrl
	String_t* ___imageUrl_1;

public:
	inline static int32_t get_offset_of_imageDimensions_0() { return static_cast<int32_t>(offsetof(Data_t3997843590, ___imageDimensions_0)); }
	inline Vector2_t2243707579  get_imageDimensions_0() const { return ___imageDimensions_0; }
	inline Vector2_t2243707579 * get_address_of_imageDimensions_0() { return &___imageDimensions_0; }
	inline void set_imageDimensions_0(Vector2_t2243707579  value)
	{
		___imageDimensions_0 = value;
	}

	inline static int32_t get_offset_of_imageUrl_1() { return static_cast<int32_t>(offsetof(Data_t3997843590, ___imageUrl_1)); }
	inline String_t* get_imageUrl_1() const { return ___imageUrl_1; }
	inline String_t** get_address_of_imageUrl_1() { return &___imageUrl_1; }
	inline void set_imageUrl_1(String_t* value)
	{
		___imageUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&___imageUrl_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
