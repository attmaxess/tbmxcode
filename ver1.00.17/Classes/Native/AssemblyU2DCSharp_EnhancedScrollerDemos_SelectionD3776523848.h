﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// EnhancedScrollerDemos.SelectionDemo.SelectedChangedDelegate
struct SelectedChangedDelegate_t1686392006;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SelectionDemo.InventoryData
struct  InventoryData_t3776523848  : public Il2CppObject
{
public:
	// System.String EnhancedScrollerDemos.SelectionDemo.InventoryData::itemName
	String_t* ___itemName_0;
	// System.Int32 EnhancedScrollerDemos.SelectionDemo.InventoryData::itemCost
	int32_t ___itemCost_1;
	// System.Int32 EnhancedScrollerDemos.SelectionDemo.InventoryData::itemDamage
	int32_t ___itemDamage_2;
	// System.Int32 EnhancedScrollerDemos.SelectionDemo.InventoryData::itemDefense
	int32_t ___itemDefense_3;
	// System.Int32 EnhancedScrollerDemos.SelectionDemo.InventoryData::itemWeight
	int32_t ___itemWeight_4;
	// System.String EnhancedScrollerDemos.SelectionDemo.InventoryData::itemDescription
	String_t* ___itemDescription_5;
	// System.String EnhancedScrollerDemos.SelectionDemo.InventoryData::spritePath
	String_t* ___spritePath_6;
	// EnhancedScrollerDemos.SelectionDemo.SelectedChangedDelegate EnhancedScrollerDemos.SelectionDemo.InventoryData::selectedChanged
	SelectedChangedDelegate_t1686392006 * ___selectedChanged_7;
	// System.Boolean EnhancedScrollerDemos.SelectionDemo.InventoryData::_selected
	bool ____selected_8;

public:
	inline static int32_t get_offset_of_itemName_0() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___itemName_0)); }
	inline String_t* get_itemName_0() const { return ___itemName_0; }
	inline String_t** get_address_of_itemName_0() { return &___itemName_0; }
	inline void set_itemName_0(String_t* value)
	{
		___itemName_0 = value;
		Il2CppCodeGenWriteBarrier(&___itemName_0, value);
	}

	inline static int32_t get_offset_of_itemCost_1() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___itemCost_1)); }
	inline int32_t get_itemCost_1() const { return ___itemCost_1; }
	inline int32_t* get_address_of_itemCost_1() { return &___itemCost_1; }
	inline void set_itemCost_1(int32_t value)
	{
		___itemCost_1 = value;
	}

	inline static int32_t get_offset_of_itemDamage_2() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___itemDamage_2)); }
	inline int32_t get_itemDamage_2() const { return ___itemDamage_2; }
	inline int32_t* get_address_of_itemDamage_2() { return &___itemDamage_2; }
	inline void set_itemDamage_2(int32_t value)
	{
		___itemDamage_2 = value;
	}

	inline static int32_t get_offset_of_itemDefense_3() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___itemDefense_3)); }
	inline int32_t get_itemDefense_3() const { return ___itemDefense_3; }
	inline int32_t* get_address_of_itemDefense_3() { return &___itemDefense_3; }
	inline void set_itemDefense_3(int32_t value)
	{
		___itemDefense_3 = value;
	}

	inline static int32_t get_offset_of_itemWeight_4() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___itemWeight_4)); }
	inline int32_t get_itemWeight_4() const { return ___itemWeight_4; }
	inline int32_t* get_address_of_itemWeight_4() { return &___itemWeight_4; }
	inline void set_itemWeight_4(int32_t value)
	{
		___itemWeight_4 = value;
	}

	inline static int32_t get_offset_of_itemDescription_5() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___itemDescription_5)); }
	inline String_t* get_itemDescription_5() const { return ___itemDescription_5; }
	inline String_t** get_address_of_itemDescription_5() { return &___itemDescription_5; }
	inline void set_itemDescription_5(String_t* value)
	{
		___itemDescription_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemDescription_5, value);
	}

	inline static int32_t get_offset_of_spritePath_6() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___spritePath_6)); }
	inline String_t* get_spritePath_6() const { return ___spritePath_6; }
	inline String_t** get_address_of_spritePath_6() { return &___spritePath_6; }
	inline void set_spritePath_6(String_t* value)
	{
		___spritePath_6 = value;
		Il2CppCodeGenWriteBarrier(&___spritePath_6, value);
	}

	inline static int32_t get_offset_of_selectedChanged_7() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ___selectedChanged_7)); }
	inline SelectedChangedDelegate_t1686392006 * get_selectedChanged_7() const { return ___selectedChanged_7; }
	inline SelectedChangedDelegate_t1686392006 ** get_address_of_selectedChanged_7() { return &___selectedChanged_7; }
	inline void set_selectedChanged_7(SelectedChangedDelegate_t1686392006 * value)
	{
		___selectedChanged_7 = value;
		Il2CppCodeGenWriteBarrier(&___selectedChanged_7, value);
	}

	inline static int32_t get_offset_of__selected_8() { return static_cast<int32_t>(offsetof(InventoryData_t3776523848, ____selected_8)); }
	inline bool get__selected_8() const { return ____selected_8; }
	inline bool* get_address_of__selected_8() { return &____selected_8; }
	inline void set__selected_8(bool value)
	{
		____selected_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
