﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// EnhancedScrollerDemos.SelectionDemo.InventoryData
struct InventoryData_t3776523848;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// EnhancedScrollerDemos.SelectionDemo.SelectedDelegate
struct SelectedDelegate_t1087907680;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SelectionDemo.InventoryCellView
struct  InventoryCellView_t3969354013  : public EnhancedScrollerCellView_t1104668249
{
public:
	// EnhancedScrollerDemos.SelectionDemo.InventoryData EnhancedScrollerDemos.SelectionDemo.InventoryCellView::_data
	InventoryData_t3776523848 * ____data_6;
	// UnityEngine.UI.Image EnhancedScrollerDemos.SelectionDemo.InventoryCellView::selectionPanel
	Image_t2042527209 * ___selectionPanel_7;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SelectionDemo.InventoryCellView::itemNameText
	Text_t356221433 * ___itemNameText_8;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SelectionDemo.InventoryCellView::itemCostText
	Text_t356221433 * ___itemCostText_9;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SelectionDemo.InventoryCellView::itemDamageText
	Text_t356221433 * ___itemDamageText_10;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SelectionDemo.InventoryCellView::itemDefenseText
	Text_t356221433 * ___itemDefenseText_11;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SelectionDemo.InventoryCellView::itemWeightText
	Text_t356221433 * ___itemWeightText_12;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SelectionDemo.InventoryCellView::itemDescriptionText
	Text_t356221433 * ___itemDescriptionText_13;
	// UnityEngine.UI.Image EnhancedScrollerDemos.SelectionDemo.InventoryCellView::image
	Image_t2042527209 * ___image_14;
	// UnityEngine.Color EnhancedScrollerDemos.SelectionDemo.InventoryCellView::selectedColor
	Color_t2020392075  ___selectedColor_15;
	// UnityEngine.Color EnhancedScrollerDemos.SelectionDemo.InventoryCellView::unSelectedColor
	Color_t2020392075  ___unSelectedColor_16;
	// System.Int32 EnhancedScrollerDemos.SelectionDemo.InventoryCellView::<DataIndex>k__BackingField
	int32_t ___U3CDataIndexU3Ek__BackingField_17;
	// EnhancedScrollerDemos.SelectionDemo.SelectedDelegate EnhancedScrollerDemos.SelectionDemo.InventoryCellView::selected
	SelectedDelegate_t1087907680 * ___selected_18;

public:
	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ____data_6)); }
	inline InventoryData_t3776523848 * get__data_6() const { return ____data_6; }
	inline InventoryData_t3776523848 ** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(InventoryData_t3776523848 * value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier(&____data_6, value);
	}

	inline static int32_t get_offset_of_selectionPanel_7() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___selectionPanel_7)); }
	inline Image_t2042527209 * get_selectionPanel_7() const { return ___selectionPanel_7; }
	inline Image_t2042527209 ** get_address_of_selectionPanel_7() { return &___selectionPanel_7; }
	inline void set_selectionPanel_7(Image_t2042527209 * value)
	{
		___selectionPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___selectionPanel_7, value);
	}

	inline static int32_t get_offset_of_itemNameText_8() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___itemNameText_8)); }
	inline Text_t356221433 * get_itemNameText_8() const { return ___itemNameText_8; }
	inline Text_t356221433 ** get_address_of_itemNameText_8() { return &___itemNameText_8; }
	inline void set_itemNameText_8(Text_t356221433 * value)
	{
		___itemNameText_8 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameText_8, value);
	}

	inline static int32_t get_offset_of_itemCostText_9() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___itemCostText_9)); }
	inline Text_t356221433 * get_itemCostText_9() const { return ___itemCostText_9; }
	inline Text_t356221433 ** get_address_of_itemCostText_9() { return &___itemCostText_9; }
	inline void set_itemCostText_9(Text_t356221433 * value)
	{
		___itemCostText_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemCostText_9, value);
	}

	inline static int32_t get_offset_of_itemDamageText_10() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___itemDamageText_10)); }
	inline Text_t356221433 * get_itemDamageText_10() const { return ___itemDamageText_10; }
	inline Text_t356221433 ** get_address_of_itemDamageText_10() { return &___itemDamageText_10; }
	inline void set_itemDamageText_10(Text_t356221433 * value)
	{
		___itemDamageText_10 = value;
		Il2CppCodeGenWriteBarrier(&___itemDamageText_10, value);
	}

	inline static int32_t get_offset_of_itemDefenseText_11() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___itemDefenseText_11)); }
	inline Text_t356221433 * get_itemDefenseText_11() const { return ___itemDefenseText_11; }
	inline Text_t356221433 ** get_address_of_itemDefenseText_11() { return &___itemDefenseText_11; }
	inline void set_itemDefenseText_11(Text_t356221433 * value)
	{
		___itemDefenseText_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemDefenseText_11, value);
	}

	inline static int32_t get_offset_of_itemWeightText_12() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___itemWeightText_12)); }
	inline Text_t356221433 * get_itemWeightText_12() const { return ___itemWeightText_12; }
	inline Text_t356221433 ** get_address_of_itemWeightText_12() { return &___itemWeightText_12; }
	inline void set_itemWeightText_12(Text_t356221433 * value)
	{
		___itemWeightText_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemWeightText_12, value);
	}

	inline static int32_t get_offset_of_itemDescriptionText_13() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___itemDescriptionText_13)); }
	inline Text_t356221433 * get_itemDescriptionText_13() const { return ___itemDescriptionText_13; }
	inline Text_t356221433 ** get_address_of_itemDescriptionText_13() { return &___itemDescriptionText_13; }
	inline void set_itemDescriptionText_13(Text_t356221433 * value)
	{
		___itemDescriptionText_13 = value;
		Il2CppCodeGenWriteBarrier(&___itemDescriptionText_13, value);
	}

	inline static int32_t get_offset_of_image_14() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___image_14)); }
	inline Image_t2042527209 * get_image_14() const { return ___image_14; }
	inline Image_t2042527209 ** get_address_of_image_14() { return &___image_14; }
	inline void set_image_14(Image_t2042527209 * value)
	{
		___image_14 = value;
		Il2CppCodeGenWriteBarrier(&___image_14, value);
	}

	inline static int32_t get_offset_of_selectedColor_15() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___selectedColor_15)); }
	inline Color_t2020392075  get_selectedColor_15() const { return ___selectedColor_15; }
	inline Color_t2020392075 * get_address_of_selectedColor_15() { return &___selectedColor_15; }
	inline void set_selectedColor_15(Color_t2020392075  value)
	{
		___selectedColor_15 = value;
	}

	inline static int32_t get_offset_of_unSelectedColor_16() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___unSelectedColor_16)); }
	inline Color_t2020392075  get_unSelectedColor_16() const { return ___unSelectedColor_16; }
	inline Color_t2020392075 * get_address_of_unSelectedColor_16() { return &___unSelectedColor_16; }
	inline void set_unSelectedColor_16(Color_t2020392075  value)
	{
		___unSelectedColor_16 = value;
	}

	inline static int32_t get_offset_of_U3CDataIndexU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___U3CDataIndexU3Ek__BackingField_17)); }
	inline int32_t get_U3CDataIndexU3Ek__BackingField_17() const { return ___U3CDataIndexU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CDataIndexU3Ek__BackingField_17() { return &___U3CDataIndexU3Ek__BackingField_17; }
	inline void set_U3CDataIndexU3Ek__BackingField_17(int32_t value)
	{
		___U3CDataIndexU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_selected_18() { return static_cast<int32_t>(offsetof(InventoryCellView_t3969354013, ___selected_18)); }
	inline SelectedDelegate_t1087907680 * get_selected_18() const { return ___selected_18; }
	inline SelectedDelegate_t1087907680 ** get_address_of_selected_18() { return &___selected_18; }
	inline void set_selected_18(SelectedDelegate_t1087907680 * value)
	{
		___selected_18 = value;
		Il2CppCodeGenWriteBarrier(&___selected_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
