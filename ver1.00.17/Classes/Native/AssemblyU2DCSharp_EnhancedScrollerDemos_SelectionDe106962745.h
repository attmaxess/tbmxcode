﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// EnhancedUI.SmallList`1<EnhancedScrollerDemos.SelectionDemo.InventoryData>
struct SmallList_1_t1541735775;
// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct EnhancedScrollerCellView_t1104668249;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SelectionDemo.SelectionDemo
struct  SelectionDemo_t106962745  : public MonoBehaviour_t1158329972
{
public:
	// EnhancedUI.SmallList`1<EnhancedScrollerDemos.SelectionDemo.InventoryData> EnhancedScrollerDemos.SelectionDemo.SelectionDemo::_data
	SmallList_1_t1541735775 * ____data_2;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.SelectionDemo.SelectionDemo::vScroller
	EnhancedScroller_t2375706558 * ___vScroller_3;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.SelectionDemo.SelectionDemo::hScroller
	EnhancedScroller_t2375706558 * ___hScroller_4;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.SelectionDemo.SelectionDemo::vCellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___vCellViewPrefab_5;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.SelectionDemo.SelectionDemo::hCellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___hCellViewPrefab_6;
	// UnityEngine.UI.Image EnhancedScrollerDemos.SelectionDemo.SelectionDemo::selectedImage
	Image_t2042527209 * ___selectedImage_7;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SelectionDemo.SelectionDemo::selectedImageText
	Text_t356221433 * ___selectedImageText_8;
	// System.String EnhancedScrollerDemos.SelectionDemo.SelectionDemo::resourcePath
	String_t* ___resourcePath_9;

public:
	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ____data_2)); }
	inline SmallList_1_t1541735775 * get__data_2() const { return ____data_2; }
	inline SmallList_1_t1541735775 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(SmallList_1_t1541735775 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier(&____data_2, value);
	}

	inline static int32_t get_offset_of_vScroller_3() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ___vScroller_3)); }
	inline EnhancedScroller_t2375706558 * get_vScroller_3() const { return ___vScroller_3; }
	inline EnhancedScroller_t2375706558 ** get_address_of_vScroller_3() { return &___vScroller_3; }
	inline void set_vScroller_3(EnhancedScroller_t2375706558 * value)
	{
		___vScroller_3 = value;
		Il2CppCodeGenWriteBarrier(&___vScroller_3, value);
	}

	inline static int32_t get_offset_of_hScroller_4() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ___hScroller_4)); }
	inline EnhancedScroller_t2375706558 * get_hScroller_4() const { return ___hScroller_4; }
	inline EnhancedScroller_t2375706558 ** get_address_of_hScroller_4() { return &___hScroller_4; }
	inline void set_hScroller_4(EnhancedScroller_t2375706558 * value)
	{
		___hScroller_4 = value;
		Il2CppCodeGenWriteBarrier(&___hScroller_4, value);
	}

	inline static int32_t get_offset_of_vCellViewPrefab_5() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ___vCellViewPrefab_5)); }
	inline EnhancedScrollerCellView_t1104668249 * get_vCellViewPrefab_5() const { return ___vCellViewPrefab_5; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_vCellViewPrefab_5() { return &___vCellViewPrefab_5; }
	inline void set_vCellViewPrefab_5(EnhancedScrollerCellView_t1104668249 * value)
	{
		___vCellViewPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___vCellViewPrefab_5, value);
	}

	inline static int32_t get_offset_of_hCellViewPrefab_6() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ___hCellViewPrefab_6)); }
	inline EnhancedScrollerCellView_t1104668249 * get_hCellViewPrefab_6() const { return ___hCellViewPrefab_6; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_hCellViewPrefab_6() { return &___hCellViewPrefab_6; }
	inline void set_hCellViewPrefab_6(EnhancedScrollerCellView_t1104668249 * value)
	{
		___hCellViewPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___hCellViewPrefab_6, value);
	}

	inline static int32_t get_offset_of_selectedImage_7() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ___selectedImage_7)); }
	inline Image_t2042527209 * get_selectedImage_7() const { return ___selectedImage_7; }
	inline Image_t2042527209 ** get_address_of_selectedImage_7() { return &___selectedImage_7; }
	inline void set_selectedImage_7(Image_t2042527209 * value)
	{
		___selectedImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___selectedImage_7, value);
	}

	inline static int32_t get_offset_of_selectedImageText_8() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ___selectedImageText_8)); }
	inline Text_t356221433 * get_selectedImageText_8() const { return ___selectedImageText_8; }
	inline Text_t356221433 ** get_address_of_selectedImageText_8() { return &___selectedImageText_8; }
	inline void set_selectedImageText_8(Text_t356221433 * value)
	{
		___selectedImageText_8 = value;
		Il2CppCodeGenWriteBarrier(&___selectedImageText_8, value);
	}

	inline static int32_t get_offset_of_resourcePath_9() { return static_cast<int32_t>(offsetof(SelectionDemo_t106962745, ___resourcePath_9)); }
	inline String_t* get_resourcePath_9() const { return ___resourcePath_9; }
	inline String_t** get_address_of_resourcePath_9() { return &___resourcePath_9; }
	inline void set_resourcePath_9(String_t* value)
	{
		___resourcePath_9 = value;
		Il2CppCodeGenWriteBarrier(&___resourcePath_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
