﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_EnhancedScrollerDemos_SnappingDe1293755907.h"

// EnhancedScrollerDemos.SnappingDemo.SlotController[]
struct SlotControllerU5BU5D_t1266370615;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// EnhancedScrollerDemos.SnappingDemo.PlayWin
struct PlayWin_t659851222;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SnappingDemo.SnappingDemo
struct  SnappingDemo_t1564888727  : public MonoBehaviour_t1158329972
{
public:
	// EnhancedScrollerDemos.SnappingDemo.SlotController[] EnhancedScrollerDemos.SnappingDemo.SnappingDemo::_slotControllers
	SlotControllerU5BU5D_t1266370615* ____slotControllers_2;
	// System.Int32[] EnhancedScrollerDemos.SnappingDemo.SnappingDemo::_snappedDataIndices
	Int32U5BU5D_t3030399641* ____snappedDataIndices_3;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::_credits
	int32_t ____credits_4;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::_snapCount
	int32_t ____snapCount_5;
	// EnhancedScrollerDemos.SnappingDemo.SnappingDemo/GameStateEnum EnhancedScrollerDemos.SnappingDemo.SnappingDemo::_gameState
	int32_t ____gameState_6;
	// System.Single EnhancedScrollerDemos.SnappingDemo.SnappingDemo::minVelocity
	float ___minVelocity_7;
	// System.Single EnhancedScrollerDemos.SnappingDemo.SnappingDemo::maxVelocity
	float ___maxVelocity_8;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::cherryIndex
	int32_t ___cherryIndex_9;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::sevenIndex
	int32_t ___sevenIndex_10;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::tripleBarIndex
	int32_t ___tripleBarIndex_11;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::bigWinIndex
	int32_t ___bigWinIndex_12;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::blankIndex
	int32_t ___blankIndex_13;
	// UnityEngine.Sprite[] EnhancedScrollerDemos.SnappingDemo.SnappingDemo::slotSprites
	SpriteU5BU5D_t3359083662* ___slotSprites_14;
	// UnityEngine.UI.Button EnhancedScrollerDemos.SnappingDemo.SnappingDemo::pullLeverButton
	Button_t2872111280 * ___pullLeverButton_15;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SnappingDemo.SnappingDemo::creditsText
	Text_t356221433 * ___creditsText_16;
	// System.Int32 EnhancedScrollerDemos.SnappingDemo.SnappingDemo::startingCredits
	int32_t ___startingCredits_17;
	// UnityEngine.GameObject EnhancedScrollerDemos.SnappingDemo.SnappingDemo::playingPanel
	GameObject_t1756533147 * ___playingPanel_18;
	// UnityEngine.GameObject EnhancedScrollerDemos.SnappingDemo.SnappingDemo::gameOverPanel
	GameObject_t1756533147 * ___gameOverPanel_19;
	// EnhancedScrollerDemos.SnappingDemo.PlayWin EnhancedScrollerDemos.SnappingDemo.SnappingDemo::playWin
	PlayWin_t659851222 * ___playWin_20;

public:
	inline static int32_t get_offset_of__slotControllers_2() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ____slotControllers_2)); }
	inline SlotControllerU5BU5D_t1266370615* get__slotControllers_2() const { return ____slotControllers_2; }
	inline SlotControllerU5BU5D_t1266370615** get_address_of__slotControllers_2() { return &____slotControllers_2; }
	inline void set__slotControllers_2(SlotControllerU5BU5D_t1266370615* value)
	{
		____slotControllers_2 = value;
		Il2CppCodeGenWriteBarrier(&____slotControllers_2, value);
	}

	inline static int32_t get_offset_of__snappedDataIndices_3() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ____snappedDataIndices_3)); }
	inline Int32U5BU5D_t3030399641* get__snappedDataIndices_3() const { return ____snappedDataIndices_3; }
	inline Int32U5BU5D_t3030399641** get_address_of__snappedDataIndices_3() { return &____snappedDataIndices_3; }
	inline void set__snappedDataIndices_3(Int32U5BU5D_t3030399641* value)
	{
		____snappedDataIndices_3 = value;
		Il2CppCodeGenWriteBarrier(&____snappedDataIndices_3, value);
	}

	inline static int32_t get_offset_of__credits_4() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ____credits_4)); }
	inline int32_t get__credits_4() const { return ____credits_4; }
	inline int32_t* get_address_of__credits_4() { return &____credits_4; }
	inline void set__credits_4(int32_t value)
	{
		____credits_4 = value;
	}

	inline static int32_t get_offset_of__snapCount_5() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ____snapCount_5)); }
	inline int32_t get__snapCount_5() const { return ____snapCount_5; }
	inline int32_t* get_address_of__snapCount_5() { return &____snapCount_5; }
	inline void set__snapCount_5(int32_t value)
	{
		____snapCount_5 = value;
	}

	inline static int32_t get_offset_of__gameState_6() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ____gameState_6)); }
	inline int32_t get__gameState_6() const { return ____gameState_6; }
	inline int32_t* get_address_of__gameState_6() { return &____gameState_6; }
	inline void set__gameState_6(int32_t value)
	{
		____gameState_6 = value;
	}

	inline static int32_t get_offset_of_minVelocity_7() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___minVelocity_7)); }
	inline float get_minVelocity_7() const { return ___minVelocity_7; }
	inline float* get_address_of_minVelocity_7() { return &___minVelocity_7; }
	inline void set_minVelocity_7(float value)
	{
		___minVelocity_7 = value;
	}

	inline static int32_t get_offset_of_maxVelocity_8() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___maxVelocity_8)); }
	inline float get_maxVelocity_8() const { return ___maxVelocity_8; }
	inline float* get_address_of_maxVelocity_8() { return &___maxVelocity_8; }
	inline void set_maxVelocity_8(float value)
	{
		___maxVelocity_8 = value;
	}

	inline static int32_t get_offset_of_cherryIndex_9() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___cherryIndex_9)); }
	inline int32_t get_cherryIndex_9() const { return ___cherryIndex_9; }
	inline int32_t* get_address_of_cherryIndex_9() { return &___cherryIndex_9; }
	inline void set_cherryIndex_9(int32_t value)
	{
		___cherryIndex_9 = value;
	}

	inline static int32_t get_offset_of_sevenIndex_10() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___sevenIndex_10)); }
	inline int32_t get_sevenIndex_10() const { return ___sevenIndex_10; }
	inline int32_t* get_address_of_sevenIndex_10() { return &___sevenIndex_10; }
	inline void set_sevenIndex_10(int32_t value)
	{
		___sevenIndex_10 = value;
	}

	inline static int32_t get_offset_of_tripleBarIndex_11() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___tripleBarIndex_11)); }
	inline int32_t get_tripleBarIndex_11() const { return ___tripleBarIndex_11; }
	inline int32_t* get_address_of_tripleBarIndex_11() { return &___tripleBarIndex_11; }
	inline void set_tripleBarIndex_11(int32_t value)
	{
		___tripleBarIndex_11 = value;
	}

	inline static int32_t get_offset_of_bigWinIndex_12() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___bigWinIndex_12)); }
	inline int32_t get_bigWinIndex_12() const { return ___bigWinIndex_12; }
	inline int32_t* get_address_of_bigWinIndex_12() { return &___bigWinIndex_12; }
	inline void set_bigWinIndex_12(int32_t value)
	{
		___bigWinIndex_12 = value;
	}

	inline static int32_t get_offset_of_blankIndex_13() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___blankIndex_13)); }
	inline int32_t get_blankIndex_13() const { return ___blankIndex_13; }
	inline int32_t* get_address_of_blankIndex_13() { return &___blankIndex_13; }
	inline void set_blankIndex_13(int32_t value)
	{
		___blankIndex_13 = value;
	}

	inline static int32_t get_offset_of_slotSprites_14() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___slotSprites_14)); }
	inline SpriteU5BU5D_t3359083662* get_slotSprites_14() const { return ___slotSprites_14; }
	inline SpriteU5BU5D_t3359083662** get_address_of_slotSprites_14() { return &___slotSprites_14; }
	inline void set_slotSprites_14(SpriteU5BU5D_t3359083662* value)
	{
		___slotSprites_14 = value;
		Il2CppCodeGenWriteBarrier(&___slotSprites_14, value);
	}

	inline static int32_t get_offset_of_pullLeverButton_15() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___pullLeverButton_15)); }
	inline Button_t2872111280 * get_pullLeverButton_15() const { return ___pullLeverButton_15; }
	inline Button_t2872111280 ** get_address_of_pullLeverButton_15() { return &___pullLeverButton_15; }
	inline void set_pullLeverButton_15(Button_t2872111280 * value)
	{
		___pullLeverButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___pullLeverButton_15, value);
	}

	inline static int32_t get_offset_of_creditsText_16() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___creditsText_16)); }
	inline Text_t356221433 * get_creditsText_16() const { return ___creditsText_16; }
	inline Text_t356221433 ** get_address_of_creditsText_16() { return &___creditsText_16; }
	inline void set_creditsText_16(Text_t356221433 * value)
	{
		___creditsText_16 = value;
		Il2CppCodeGenWriteBarrier(&___creditsText_16, value);
	}

	inline static int32_t get_offset_of_startingCredits_17() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___startingCredits_17)); }
	inline int32_t get_startingCredits_17() const { return ___startingCredits_17; }
	inline int32_t* get_address_of_startingCredits_17() { return &___startingCredits_17; }
	inline void set_startingCredits_17(int32_t value)
	{
		___startingCredits_17 = value;
	}

	inline static int32_t get_offset_of_playingPanel_18() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___playingPanel_18)); }
	inline GameObject_t1756533147 * get_playingPanel_18() const { return ___playingPanel_18; }
	inline GameObject_t1756533147 ** get_address_of_playingPanel_18() { return &___playingPanel_18; }
	inline void set_playingPanel_18(GameObject_t1756533147 * value)
	{
		___playingPanel_18 = value;
		Il2CppCodeGenWriteBarrier(&___playingPanel_18, value);
	}

	inline static int32_t get_offset_of_gameOverPanel_19() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___gameOverPanel_19)); }
	inline GameObject_t1756533147 * get_gameOverPanel_19() const { return ___gameOverPanel_19; }
	inline GameObject_t1756533147 ** get_address_of_gameOverPanel_19() { return &___gameOverPanel_19; }
	inline void set_gameOverPanel_19(GameObject_t1756533147 * value)
	{
		___gameOverPanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameOverPanel_19, value);
	}

	inline static int32_t get_offset_of_playWin_20() { return static_cast<int32_t>(offsetof(SnappingDemo_t1564888727, ___playWin_20)); }
	inline PlayWin_t659851222 * get_playWin_20() const { return ___playWin_20; }
	inline PlayWin_t659851222 ** get_address_of_playWin_20() { return &___playWin_20; }
	inline void set_playWin_20(PlayWin_t659851222 * value)
	{
		___playWin_20 = value;
		Il2CppCodeGenWriteBarrier(&___playWin_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
