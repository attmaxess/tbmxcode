﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SnappingDemo.SlotData
struct  SlotData_t2756082054  : public Il2CppObject
{
public:
	// UnityEngine.Sprite EnhancedScrollerDemos.SnappingDemo.SlotData::sprite
	Sprite_t309593783 * ___sprite_0;

public:
	inline static int32_t get_offset_of_sprite_0() { return static_cast<int32_t>(offsetof(SlotData_t2756082054, ___sprite_0)); }
	inline Sprite_t309593783 * get_sprite_0() const { return ___sprite_0; }
	inline Sprite_t309593783 ** get_address_of_sprite_0() { return &___sprite_0; }
	inline void set_sprite_0(Sprite_t309593783 * value)
	{
		___sprite_0 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
