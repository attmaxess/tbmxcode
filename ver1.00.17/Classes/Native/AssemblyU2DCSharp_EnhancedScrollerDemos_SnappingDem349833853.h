﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SnappingDemo.SlotCellView
struct  SlotCellView_t349833853  : public EnhancedScrollerCellView_t1104668249
{
public:
	// UnityEngine.UI.Image EnhancedScrollerDemos.SnappingDemo.SlotCellView::slotImage
	Image_t2042527209 * ___slotImage_6;

public:
	inline static int32_t get_offset_of_slotImage_6() { return static_cast<int32_t>(offsetof(SlotCellView_t349833853, ___slotImage_6)); }
	inline Image_t2042527209 * get_slotImage_6() const { return ___slotImage_6; }
	inline Image_t2042527209 ** get_address_of_slotImage_6() { return &___slotImage_6; }
	inline void set_slotImage_6(Image_t2042527209 * value)
	{
		___slotImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___slotImage_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
