﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SnappingDemo.PlayWin
struct  PlayWin_t659851222  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform EnhancedScrollerDemos.SnappingDemo.PlayWin::_transform
	Transform_t3275118058 * ____transform_2;
	// System.Single EnhancedScrollerDemos.SnappingDemo.PlayWin::_timeLeft
	float ____timeLeft_3;
	// UnityEngine.UI.Text EnhancedScrollerDemos.SnappingDemo.PlayWin::scoreText
	Text_t356221433 * ___scoreText_4;
	// System.Single EnhancedScrollerDemos.SnappingDemo.PlayWin::zoomTime
	float ___zoomTime_5;
	// System.Single EnhancedScrollerDemos.SnappingDemo.PlayWin::holdTime
	float ___holdTime_6;
	// System.Single EnhancedScrollerDemos.SnappingDemo.PlayWin::unZoomTime
	float ___unZoomTime_7;

public:
	inline static int32_t get_offset_of__transform_2() { return static_cast<int32_t>(offsetof(PlayWin_t659851222, ____transform_2)); }
	inline Transform_t3275118058 * get__transform_2() const { return ____transform_2; }
	inline Transform_t3275118058 ** get_address_of__transform_2() { return &____transform_2; }
	inline void set__transform_2(Transform_t3275118058 * value)
	{
		____transform_2 = value;
		Il2CppCodeGenWriteBarrier(&____transform_2, value);
	}

	inline static int32_t get_offset_of__timeLeft_3() { return static_cast<int32_t>(offsetof(PlayWin_t659851222, ____timeLeft_3)); }
	inline float get__timeLeft_3() const { return ____timeLeft_3; }
	inline float* get_address_of__timeLeft_3() { return &____timeLeft_3; }
	inline void set__timeLeft_3(float value)
	{
		____timeLeft_3 = value;
	}

	inline static int32_t get_offset_of_scoreText_4() { return static_cast<int32_t>(offsetof(PlayWin_t659851222, ___scoreText_4)); }
	inline Text_t356221433 * get_scoreText_4() const { return ___scoreText_4; }
	inline Text_t356221433 ** get_address_of_scoreText_4() { return &___scoreText_4; }
	inline void set_scoreText_4(Text_t356221433 * value)
	{
		___scoreText_4 = value;
		Il2CppCodeGenWriteBarrier(&___scoreText_4, value);
	}

	inline static int32_t get_offset_of_zoomTime_5() { return static_cast<int32_t>(offsetof(PlayWin_t659851222, ___zoomTime_5)); }
	inline float get_zoomTime_5() const { return ___zoomTime_5; }
	inline float* get_address_of_zoomTime_5() { return &___zoomTime_5; }
	inline void set_zoomTime_5(float value)
	{
		___zoomTime_5 = value;
	}

	inline static int32_t get_offset_of_holdTime_6() { return static_cast<int32_t>(offsetof(PlayWin_t659851222, ___holdTime_6)); }
	inline float get_holdTime_6() const { return ___holdTime_6; }
	inline float* get_address_of_holdTime_6() { return &___holdTime_6; }
	inline void set_holdTime_6(float value)
	{
		___holdTime_6 = value;
	}

	inline static int32_t get_offset_of_unZoomTime_7() { return static_cast<int32_t>(offsetof(PlayWin_t659851222, ___unZoomTime_7)); }
	inline float get_unZoomTime_7() const { return ___unZoomTime_7; }
	inline float* get_address_of_unZoomTime_7() { return &___unZoomTime_7; }
	inline void set_unZoomTime_7(float value)
	{
		___unZoomTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
