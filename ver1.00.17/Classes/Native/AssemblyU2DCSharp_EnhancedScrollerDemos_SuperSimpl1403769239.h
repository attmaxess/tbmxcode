﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// EnhancedUI.SmallList`1<EnhancedScrollerDemos.SuperSimpleDemo.Data>
struct SmallList_1_t2467940949;
// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct EnhancedScrollerCellView_t1104668249;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SuperSimpleDemo.SimpleDemo
struct  SimpleDemo_t1403769239  : public MonoBehaviour_t1158329972
{
public:
	// EnhancedUI.SmallList`1<EnhancedScrollerDemos.SuperSimpleDemo.Data> EnhancedScrollerDemos.SuperSimpleDemo.SimpleDemo::_data
	SmallList_1_t2467940949 * ____data_2;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.SuperSimpleDemo.SimpleDemo::scroller
	EnhancedScroller_t2375706558 * ___scroller_3;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.SuperSimpleDemo.SimpleDemo::cellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___cellViewPrefab_4;

public:
	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(SimpleDemo_t1403769239, ____data_2)); }
	inline SmallList_1_t2467940949 * get__data_2() const { return ____data_2; }
	inline SmallList_1_t2467940949 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(SmallList_1_t2467940949 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier(&____data_2, value);
	}

	inline static int32_t get_offset_of_scroller_3() { return static_cast<int32_t>(offsetof(SimpleDemo_t1403769239, ___scroller_3)); }
	inline EnhancedScroller_t2375706558 * get_scroller_3() const { return ___scroller_3; }
	inline EnhancedScroller_t2375706558 ** get_address_of_scroller_3() { return &___scroller_3; }
	inline void set_scroller_3(EnhancedScroller_t2375706558 * value)
	{
		___scroller_3 = value;
		Il2CppCodeGenWriteBarrier(&___scroller_3, value);
	}

	inline static int32_t get_offset_of_cellViewPrefab_4() { return static_cast<int32_t>(offsetof(SimpleDemo_t1403769239, ___cellViewPrefab_4)); }
	inline EnhancedScrollerCellView_t1104668249 * get_cellViewPrefab_4() const { return ___cellViewPrefab_4; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_cellViewPrefab_4() { return &___cellViewPrefab_4; }
	inline void set_cellViewPrefab_4(EnhancedScrollerCellView_t1104668249 * value)
	{
		___cellViewPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___cellViewPrefab_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
