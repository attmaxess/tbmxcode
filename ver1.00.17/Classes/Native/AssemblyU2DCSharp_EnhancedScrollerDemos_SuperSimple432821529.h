﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.SuperSimpleDemo.CellView
struct  CellView_t432821529  : public EnhancedScrollerCellView_t1104668249
{
public:
	// UnityEngine.UI.Text EnhancedScrollerDemos.SuperSimpleDemo.CellView::someTextText
	Text_t356221433 * ___someTextText_6;

public:
	inline static int32_t get_offset_of_someTextText_6() { return static_cast<int32_t>(offsetof(CellView_t432821529, ___someTextText_6)); }
	inline Text_t356221433 * get_someTextText_6() const { return ___someTextText_6; }
	inline Text_t356221433 ** get_address_of_someTextText_6() { return &___someTextText_6; }
	inline void set_someTextText_6(Text_t356221433 * value)
	{
		___someTextText_6 = value;
		Il2CppCodeGenWriteBarrier(&___someTextText_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
