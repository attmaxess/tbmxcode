﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<EnhancedScrollerDemos.ViewDrivenCellSizes.Data>
struct List_1_t3543312634;
// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct EnhancedScrollerCellView_t1104668249;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.ViewDrivenCellSizes.Controller
struct  Controller_t1907446394  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<EnhancedScrollerDemos.ViewDrivenCellSizes.Data> EnhancedScrollerDemos.ViewDrivenCellSizes.Controller::_data
	List_1_t3543312634 * ____data_2;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedScrollerDemos.ViewDrivenCellSizes.Controller::scroller
	EnhancedScroller_t2375706558 * ___scroller_3;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView EnhancedScrollerDemos.ViewDrivenCellSizes.Controller::cellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___cellViewPrefab_4;

public:
	inline static int32_t get_offset_of__data_2() { return static_cast<int32_t>(offsetof(Controller_t1907446394, ____data_2)); }
	inline List_1_t3543312634 * get__data_2() const { return ____data_2; }
	inline List_1_t3543312634 ** get_address_of__data_2() { return &____data_2; }
	inline void set__data_2(List_1_t3543312634 * value)
	{
		____data_2 = value;
		Il2CppCodeGenWriteBarrier(&____data_2, value);
	}

	inline static int32_t get_offset_of_scroller_3() { return static_cast<int32_t>(offsetof(Controller_t1907446394, ___scroller_3)); }
	inline EnhancedScroller_t2375706558 * get_scroller_3() const { return ___scroller_3; }
	inline EnhancedScroller_t2375706558 ** get_address_of_scroller_3() { return &___scroller_3; }
	inline void set_scroller_3(EnhancedScroller_t2375706558 * value)
	{
		___scroller_3 = value;
		Il2CppCodeGenWriteBarrier(&___scroller_3, value);
	}

	inline static int32_t get_offset_of_cellViewPrefab_4() { return static_cast<int32_t>(offsetof(Controller_t1907446394, ___cellViewPrefab_4)); }
	inline EnhancedScrollerCellView_t1104668249 * get_cellViewPrefab_4() const { return ___cellViewPrefab_4; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_cellViewPrefab_4() { return &___cellViewPrefab_4; }
	inline void set_cellViewPrefab_4(EnhancedScrollerCellView_t1104668249 * value)
	{
		___cellViewPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___cellViewPrefab_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
