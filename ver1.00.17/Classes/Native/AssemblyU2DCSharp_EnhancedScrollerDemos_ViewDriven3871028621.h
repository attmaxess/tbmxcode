﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.ViewDrivenCellSizes.CellView
struct  CellView_t3871028621  : public EnhancedScrollerCellView_t1104668249
{
public:
	// UnityEngine.UI.Text EnhancedScrollerDemos.ViewDrivenCellSizes.CellView::someTextText
	Text_t356221433 * ___someTextText_6;
	// UnityEngine.RectTransform EnhancedScrollerDemos.ViewDrivenCellSizes.CellView::textRectTransform
	RectTransform_t3349966182 * ___textRectTransform_7;
	// UnityEngine.RectOffset EnhancedScrollerDemos.ViewDrivenCellSizes.CellView::textBuffer
	RectOffset_t3387826427 * ___textBuffer_8;

public:
	inline static int32_t get_offset_of_someTextText_6() { return static_cast<int32_t>(offsetof(CellView_t3871028621, ___someTextText_6)); }
	inline Text_t356221433 * get_someTextText_6() const { return ___someTextText_6; }
	inline Text_t356221433 ** get_address_of_someTextText_6() { return &___someTextText_6; }
	inline void set_someTextText_6(Text_t356221433 * value)
	{
		___someTextText_6 = value;
		Il2CppCodeGenWriteBarrier(&___someTextText_6, value);
	}

	inline static int32_t get_offset_of_textRectTransform_7() { return static_cast<int32_t>(offsetof(CellView_t3871028621, ___textRectTransform_7)); }
	inline RectTransform_t3349966182 * get_textRectTransform_7() const { return ___textRectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_textRectTransform_7() { return &___textRectTransform_7; }
	inline void set_textRectTransform_7(RectTransform_t3349966182 * value)
	{
		___textRectTransform_7 = value;
		Il2CppCodeGenWriteBarrier(&___textRectTransform_7, value);
	}

	inline static int32_t get_offset_of_textBuffer_8() { return static_cast<int32_t>(offsetof(CellView_t3871028621, ___textBuffer_8)); }
	inline RectOffset_t3387826427 * get_textBuffer_8() const { return ___textBuffer_8; }
	inline RectOffset_t3387826427 ** get_address_of_textBuffer_8() { return &___textBuffer_8; }
	inline void set_textBuffer_8(RectOffset_t3387826427 * value)
	{
		___textBuffer_8 = value;
		Il2CppCodeGenWriteBarrier(&___textBuffer_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
