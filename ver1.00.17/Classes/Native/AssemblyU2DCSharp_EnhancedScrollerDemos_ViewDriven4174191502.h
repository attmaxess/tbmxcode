﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedScrollerDemos.ViewDrivenCellSizes.Data
struct  Data_t4174191502  : public Il2CppObject
{
public:
	// System.String EnhancedScrollerDemos.ViewDrivenCellSizes.Data::someText
	String_t* ___someText_0;
	// System.Single EnhancedScrollerDemos.ViewDrivenCellSizes.Data::cellSize
	float ___cellSize_1;

public:
	inline static int32_t get_offset_of_someText_0() { return static_cast<int32_t>(offsetof(Data_t4174191502, ___someText_0)); }
	inline String_t* get_someText_0() const { return ___someText_0; }
	inline String_t** get_address_of_someText_0() { return &___someText_0; }
	inline void set_someText_0(String_t* value)
	{
		___someText_0 = value;
		Il2CppCodeGenWriteBarrier(&___someText_0, value);
	}

	inline static int32_t get_offset_of_cellSize_1() { return static_cast<int32_t>(offsetof(Data_t4174191502, ___cellSize_1)); }
	inline float get_cellSize_1() const { return ___cellSize_1; }
	inline float* get_address_of_cellSize_1() { return &___cellSize_1; }
	inline void set_cellSize_1(float value)
	{
		___cellSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
