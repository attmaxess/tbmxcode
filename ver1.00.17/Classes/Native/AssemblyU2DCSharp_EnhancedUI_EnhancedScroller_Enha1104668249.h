﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct  EnhancedScrollerCellView_t1104668249  : public MonoBehaviour_t1158329972
{
public:
	// System.String EnhancedUI.EnhancedScroller.EnhancedScrollerCellView::cellIdentifier
	String_t* ___cellIdentifier_2;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScrollerCellView::cellIndex
	int32_t ___cellIndex_3;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScrollerCellView::dataIndex
	int32_t ___dataIndex_4;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScrollerCellView::active
	bool ___active_5;

public:
	inline static int32_t get_offset_of_cellIdentifier_2() { return static_cast<int32_t>(offsetof(EnhancedScrollerCellView_t1104668249, ___cellIdentifier_2)); }
	inline String_t* get_cellIdentifier_2() const { return ___cellIdentifier_2; }
	inline String_t** get_address_of_cellIdentifier_2() { return &___cellIdentifier_2; }
	inline void set_cellIdentifier_2(String_t* value)
	{
		___cellIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier(&___cellIdentifier_2, value);
	}

	inline static int32_t get_offset_of_cellIndex_3() { return static_cast<int32_t>(offsetof(EnhancedScrollerCellView_t1104668249, ___cellIndex_3)); }
	inline int32_t get_cellIndex_3() const { return ___cellIndex_3; }
	inline int32_t* get_address_of_cellIndex_3() { return &___cellIndex_3; }
	inline void set_cellIndex_3(int32_t value)
	{
		___cellIndex_3 = value;
	}

	inline static int32_t get_offset_of_dataIndex_4() { return static_cast<int32_t>(offsetof(EnhancedScrollerCellView_t1104668249, ___dataIndex_4)); }
	inline int32_t get_dataIndex_4() const { return ___dataIndex_4; }
	inline int32_t* get_address_of_dataIndex_4() { return &___dataIndex_4; }
	inline void set_dataIndex_4(int32_t value)
	{
		___dataIndex_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(EnhancedScrollerCellView_t1104668249, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
