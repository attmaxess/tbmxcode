﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enhan993038288.h"

// System.Action
struct Action_t3226471752;
// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0
struct  U3CTweenPositionU3Ec__Iterator0_t1678467666  : public Il2CppObject
{
public:
	// EnhancedUI.EnhancedScroller.EnhancedScroller/TweenType EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::tweenType
	int32_t ___tweenType_0;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::time
	float ___time_1;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::end
	float ___end_2;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::<newPosition>__1
	float ___U3CnewPositionU3E__1_3;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::start
	float ___start_4;
	// System.Action EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::tweenComplete
	Action_t3226471752 * ___tweenComplete_5;
	// EnhancedUI.EnhancedScroller.EnhancedScroller EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::$this
	EnhancedScroller_t2375706558 * ___U24this_6;
	// System.Object EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScroller/<TweenPosition>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___end_2)); }
	inline float get_end_2() const { return ___end_2; }
	inline float* get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(float value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_U3CnewPositionU3E__1_3() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___U3CnewPositionU3E__1_3)); }
	inline float get_U3CnewPositionU3E__1_3() const { return ___U3CnewPositionU3E__1_3; }
	inline float* get_address_of_U3CnewPositionU3E__1_3() { return &___U3CnewPositionU3E__1_3; }
	inline void set_U3CnewPositionU3E__1_3(float value)
	{
		___U3CnewPositionU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_start_4() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___start_4)); }
	inline float get_start_4() const { return ___start_4; }
	inline float* get_address_of_start_4() { return &___start_4; }
	inline void set_start_4(float value)
	{
		___start_4 = value;
	}

	inline static int32_t get_offset_of_tweenComplete_5() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___tweenComplete_5)); }
	inline Action_t3226471752 * get_tweenComplete_5() const { return ___tweenComplete_5; }
	inline Action_t3226471752 ** get_address_of_tweenComplete_5() { return &___tweenComplete_5; }
	inline void set_tweenComplete_5(Action_t3226471752 * value)
	{
		___tweenComplete_5 = value;
		Il2CppCodeGenWriteBarrier(&___tweenComplete_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___U24this_6)); }
	inline EnhancedScroller_t2375706558 * get_U24this_6() const { return ___U24this_6; }
	inline EnhancedScroller_t2375706558 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(EnhancedScroller_t2375706558 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CTweenPositionU3Ec__Iterator0_t1678467666, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
