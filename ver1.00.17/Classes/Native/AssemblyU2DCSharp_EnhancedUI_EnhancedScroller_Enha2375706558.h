﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1134221400.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha2445864820.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enhan993038288.h"

// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// EnhancedUI.EnhancedScroller.CellViewVisibilityChangedDelegate
struct CellViewVisibilityChangedDelegate_t502957434;
// EnhancedUI.EnhancedScroller.CellViewWillRecycleDelegate
struct CellViewWillRecycleDelegate_t3713803619;
// EnhancedUI.EnhancedScroller.ScrollerScrolledDelegate
struct ScrollerScrolledDelegate_t3009238051;
// EnhancedUI.EnhancedScroller.ScrollerSnappedDelegate
struct ScrollerSnappedDelegate_t1271485394;
// EnhancedUI.EnhancedScroller.ScrollerScrollingChangedDelegate
struct ScrollerScrollingChangedDelegate_t2129781406;
// EnhancedUI.EnhancedScroller.ScrollerTweeningChangedDelegate
struct ScrollerTweeningChangedDelegate_t2022402836;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t1968298610;
// EnhancedUI.EnhancedScroller.IEnhancedScrollerDelegate
struct IEnhancedScrollerDelegate_t2066056166;
// EnhancedUI.SmallList`1<EnhancedUI.EnhancedScroller.EnhancedScrollerCellView>
struct SmallList_1_t3164847472;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// EnhancedUI.SmallList`1<System.Single>
struct SmallList_1_t4136689155;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedUI.EnhancedScroller.EnhancedScroller
struct  EnhancedScroller_t2375706558  : public MonoBehaviour_t1158329972
{
public:
	// EnhancedUI.EnhancedScroller.EnhancedScroller/ScrollDirectionEnum EnhancedUI.EnhancedScroller.EnhancedScroller::scrollDirection
	int32_t ___scrollDirection_2;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::spacing
	float ___spacing_3;
	// UnityEngine.RectOffset EnhancedUI.EnhancedScroller.EnhancedScroller::padding
	RectOffset_t3387826427 * ___padding_4;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::loop
	bool ___loop_5;
	// EnhancedUI.EnhancedScroller.EnhancedScroller/ScrollbarVisibilityEnum EnhancedUI.EnhancedScroller.EnhancedScroller::scrollbarVisibility
	int32_t ___scrollbarVisibility_6;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::snapping
	bool ___snapping_7;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::snapVelocityThreshold
	float ___snapVelocityThreshold_8;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::snapWatchOffset
	float ___snapWatchOffset_9;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::snapJumpToOffset
	float ___snapJumpToOffset_10;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::snapCellCenterOffset
	float ___snapCellCenterOffset_11;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::snapUseCellSpacing
	bool ___snapUseCellSpacing_12;
	// EnhancedUI.EnhancedScroller.EnhancedScroller/TweenType EnhancedUI.EnhancedScroller.EnhancedScroller::snapTweenType
	int32_t ___snapTweenType_13;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::snapTweenTime
	float ___snapTweenTime_14;
	// EnhancedUI.EnhancedScroller.CellViewVisibilityChangedDelegate EnhancedUI.EnhancedScroller.EnhancedScroller::cellViewVisibilityChanged
	CellViewVisibilityChangedDelegate_t502957434 * ___cellViewVisibilityChanged_15;
	// EnhancedUI.EnhancedScroller.CellViewWillRecycleDelegate EnhancedUI.EnhancedScroller.EnhancedScroller::cellViewWillRecycle
	CellViewWillRecycleDelegate_t3713803619 * ___cellViewWillRecycle_16;
	// EnhancedUI.EnhancedScroller.ScrollerScrolledDelegate EnhancedUI.EnhancedScroller.EnhancedScroller::scrollerScrolled
	ScrollerScrolledDelegate_t3009238051 * ___scrollerScrolled_17;
	// EnhancedUI.EnhancedScroller.ScrollerSnappedDelegate EnhancedUI.EnhancedScroller.EnhancedScroller::scrollerSnapped
	ScrollerSnappedDelegate_t1271485394 * ___scrollerSnapped_18;
	// EnhancedUI.EnhancedScroller.ScrollerScrollingChangedDelegate EnhancedUI.EnhancedScroller.EnhancedScroller::scrollerScrollingChanged
	ScrollerScrollingChangedDelegate_t2129781406 * ___scrollerScrollingChanged_19;
	// EnhancedUI.EnhancedScroller.ScrollerTweeningChangedDelegate EnhancedUI.EnhancedScroller.EnhancedScroller::scrollerTweeningChanged
	ScrollerTweeningChangedDelegate_t2022402836 * ___scrollerTweeningChanged_20;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::<IsScrolling>k__BackingField
	bool ___U3CIsScrollingU3Ek__BackingField_21;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::<IsTweening>k__BackingField
	bool ___U3CIsTweeningU3Ek__BackingField_22;
	// UnityEngine.UI.ScrollRect EnhancedUI.EnhancedScroller.EnhancedScroller::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_23;
	// UnityEngine.RectTransform EnhancedUI.EnhancedScroller.EnhancedScroller::_scrollRectTransform
	RectTransform_t3349966182 * ____scrollRectTransform_24;
	// UnityEngine.UI.Scrollbar EnhancedUI.EnhancedScroller.EnhancedScroller::_scrollbar
	Scrollbar_t3248359358 * ____scrollbar_25;
	// UnityEngine.RectTransform EnhancedUI.EnhancedScroller.EnhancedScroller::_container
	RectTransform_t3349966182 * ____container_26;
	// UnityEngine.UI.HorizontalOrVerticalLayoutGroup EnhancedUI.EnhancedScroller.EnhancedScroller::_layoutGroup
	HorizontalOrVerticalLayoutGroup_t1968298610 * ____layoutGroup_27;
	// EnhancedUI.EnhancedScroller.IEnhancedScrollerDelegate EnhancedUI.EnhancedScroller.EnhancedScroller::_delegate
	Il2CppObject * ____delegate_28;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::_reloadData
	bool ____reloadData_29;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::_refreshActive
	bool ____refreshActive_30;
	// EnhancedUI.SmallList`1<EnhancedUI.EnhancedScroller.EnhancedScrollerCellView> EnhancedUI.EnhancedScroller.EnhancedScroller::_recycledCellViews
	SmallList_1_t3164847472 * ____recycledCellViews_31;
	// UnityEngine.UI.LayoutElement EnhancedUI.EnhancedScroller.EnhancedScroller::_firstPadder
	LayoutElement_t2808691390 * ____firstPadder_32;
	// UnityEngine.UI.LayoutElement EnhancedUI.EnhancedScroller.EnhancedScroller::_lastPadder
	LayoutElement_t2808691390 * ____lastPadder_33;
	// UnityEngine.RectTransform EnhancedUI.EnhancedScroller.EnhancedScroller::_recycledCellViewContainer
	RectTransform_t3349966182 * ____recycledCellViewContainer_34;
	// EnhancedUI.SmallList`1<System.Single> EnhancedUI.EnhancedScroller.EnhancedScroller::_cellViewSizeArray
	SmallList_1_t4136689155 * ____cellViewSizeArray_35;
	// EnhancedUI.SmallList`1<System.Single> EnhancedUI.EnhancedScroller.EnhancedScroller::_cellViewOffsetArray
	SmallList_1_t4136689155 * ____cellViewOffsetArray_36;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::_scrollPosition
	float ____scrollPosition_37;
	// EnhancedUI.SmallList`1<EnhancedUI.EnhancedScroller.EnhancedScrollerCellView> EnhancedUI.EnhancedScroller.EnhancedScroller::_activeCellViews
	SmallList_1_t3164847472 * ____activeCellViews_38;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScroller::_activeCellViewsStartIndex
	int32_t ____activeCellViewsStartIndex_39;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScroller::_activeCellViewsEndIndex
	int32_t ____activeCellViewsEndIndex_40;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScroller::_loopFirstCellIndex
	int32_t ____loopFirstCellIndex_41;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScroller::_loopLastCellIndex
	int32_t ____loopLastCellIndex_42;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::_loopFirstScrollPosition
	float ____loopFirstScrollPosition_43;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::_loopLastScrollPosition
	float ____loopLastScrollPosition_44;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::_loopFirstJumpTrigger
	float ____loopFirstJumpTrigger_45;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::_loopLastJumpTrigger
	float ____loopLastJumpTrigger_46;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::_lastScrollRectSize
	float ____lastScrollRectSize_47;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::_lastLoop
	bool ____lastLoop_48;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScroller::_snapCellViewIndex
	int32_t ____snapCellViewIndex_49;
	// System.Int32 EnhancedUI.EnhancedScroller.EnhancedScroller::_snapDataIndex
	int32_t ____snapDataIndex_50;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::_snapJumping
	bool ____snapJumping_51;
	// System.Boolean EnhancedUI.EnhancedScroller.EnhancedScroller::_snapInertia
	bool ____snapInertia_52;
	// EnhancedUI.EnhancedScroller.EnhancedScroller/ScrollbarVisibilityEnum EnhancedUI.EnhancedScroller.EnhancedScroller::_lastScrollbarVisibility
	int32_t ____lastScrollbarVisibility_53;
	// System.Single EnhancedUI.EnhancedScroller.EnhancedScroller::_tweenTimeLeft
	float ____tweenTimeLeft_54;

public:
	inline static int32_t get_offset_of_scrollDirection_2() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___scrollDirection_2)); }
	inline int32_t get_scrollDirection_2() const { return ___scrollDirection_2; }
	inline int32_t* get_address_of_scrollDirection_2() { return &___scrollDirection_2; }
	inline void set_scrollDirection_2(int32_t value)
	{
		___scrollDirection_2 = value;
	}

	inline static int32_t get_offset_of_spacing_3() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___spacing_3)); }
	inline float get_spacing_3() const { return ___spacing_3; }
	inline float* get_address_of_spacing_3() { return &___spacing_3; }
	inline void set_spacing_3(float value)
	{
		___spacing_3 = value;
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___padding_4)); }
	inline RectOffset_t3387826427 * get_padding_4() const { return ___padding_4; }
	inline RectOffset_t3387826427 ** get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(RectOffset_t3387826427 * value)
	{
		___padding_4 = value;
		Il2CppCodeGenWriteBarrier(&___padding_4, value);
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_scrollbarVisibility_6() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___scrollbarVisibility_6)); }
	inline int32_t get_scrollbarVisibility_6() const { return ___scrollbarVisibility_6; }
	inline int32_t* get_address_of_scrollbarVisibility_6() { return &___scrollbarVisibility_6; }
	inline void set_scrollbarVisibility_6(int32_t value)
	{
		___scrollbarVisibility_6 = value;
	}

	inline static int32_t get_offset_of_snapping_7() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapping_7)); }
	inline bool get_snapping_7() const { return ___snapping_7; }
	inline bool* get_address_of_snapping_7() { return &___snapping_7; }
	inline void set_snapping_7(bool value)
	{
		___snapping_7 = value;
	}

	inline static int32_t get_offset_of_snapVelocityThreshold_8() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapVelocityThreshold_8)); }
	inline float get_snapVelocityThreshold_8() const { return ___snapVelocityThreshold_8; }
	inline float* get_address_of_snapVelocityThreshold_8() { return &___snapVelocityThreshold_8; }
	inline void set_snapVelocityThreshold_8(float value)
	{
		___snapVelocityThreshold_8 = value;
	}

	inline static int32_t get_offset_of_snapWatchOffset_9() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapWatchOffset_9)); }
	inline float get_snapWatchOffset_9() const { return ___snapWatchOffset_9; }
	inline float* get_address_of_snapWatchOffset_9() { return &___snapWatchOffset_9; }
	inline void set_snapWatchOffset_9(float value)
	{
		___snapWatchOffset_9 = value;
	}

	inline static int32_t get_offset_of_snapJumpToOffset_10() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapJumpToOffset_10)); }
	inline float get_snapJumpToOffset_10() const { return ___snapJumpToOffset_10; }
	inline float* get_address_of_snapJumpToOffset_10() { return &___snapJumpToOffset_10; }
	inline void set_snapJumpToOffset_10(float value)
	{
		___snapJumpToOffset_10 = value;
	}

	inline static int32_t get_offset_of_snapCellCenterOffset_11() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapCellCenterOffset_11)); }
	inline float get_snapCellCenterOffset_11() const { return ___snapCellCenterOffset_11; }
	inline float* get_address_of_snapCellCenterOffset_11() { return &___snapCellCenterOffset_11; }
	inline void set_snapCellCenterOffset_11(float value)
	{
		___snapCellCenterOffset_11 = value;
	}

	inline static int32_t get_offset_of_snapUseCellSpacing_12() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapUseCellSpacing_12)); }
	inline bool get_snapUseCellSpacing_12() const { return ___snapUseCellSpacing_12; }
	inline bool* get_address_of_snapUseCellSpacing_12() { return &___snapUseCellSpacing_12; }
	inline void set_snapUseCellSpacing_12(bool value)
	{
		___snapUseCellSpacing_12 = value;
	}

	inline static int32_t get_offset_of_snapTweenType_13() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapTweenType_13)); }
	inline int32_t get_snapTweenType_13() const { return ___snapTweenType_13; }
	inline int32_t* get_address_of_snapTweenType_13() { return &___snapTweenType_13; }
	inline void set_snapTweenType_13(int32_t value)
	{
		___snapTweenType_13 = value;
	}

	inline static int32_t get_offset_of_snapTweenTime_14() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___snapTweenTime_14)); }
	inline float get_snapTweenTime_14() const { return ___snapTweenTime_14; }
	inline float* get_address_of_snapTweenTime_14() { return &___snapTweenTime_14; }
	inline void set_snapTweenTime_14(float value)
	{
		___snapTweenTime_14 = value;
	}

	inline static int32_t get_offset_of_cellViewVisibilityChanged_15() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___cellViewVisibilityChanged_15)); }
	inline CellViewVisibilityChangedDelegate_t502957434 * get_cellViewVisibilityChanged_15() const { return ___cellViewVisibilityChanged_15; }
	inline CellViewVisibilityChangedDelegate_t502957434 ** get_address_of_cellViewVisibilityChanged_15() { return &___cellViewVisibilityChanged_15; }
	inline void set_cellViewVisibilityChanged_15(CellViewVisibilityChangedDelegate_t502957434 * value)
	{
		___cellViewVisibilityChanged_15 = value;
		Il2CppCodeGenWriteBarrier(&___cellViewVisibilityChanged_15, value);
	}

	inline static int32_t get_offset_of_cellViewWillRecycle_16() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___cellViewWillRecycle_16)); }
	inline CellViewWillRecycleDelegate_t3713803619 * get_cellViewWillRecycle_16() const { return ___cellViewWillRecycle_16; }
	inline CellViewWillRecycleDelegate_t3713803619 ** get_address_of_cellViewWillRecycle_16() { return &___cellViewWillRecycle_16; }
	inline void set_cellViewWillRecycle_16(CellViewWillRecycleDelegate_t3713803619 * value)
	{
		___cellViewWillRecycle_16 = value;
		Il2CppCodeGenWriteBarrier(&___cellViewWillRecycle_16, value);
	}

	inline static int32_t get_offset_of_scrollerScrolled_17() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___scrollerScrolled_17)); }
	inline ScrollerScrolledDelegate_t3009238051 * get_scrollerScrolled_17() const { return ___scrollerScrolled_17; }
	inline ScrollerScrolledDelegate_t3009238051 ** get_address_of_scrollerScrolled_17() { return &___scrollerScrolled_17; }
	inline void set_scrollerScrolled_17(ScrollerScrolledDelegate_t3009238051 * value)
	{
		___scrollerScrolled_17 = value;
		Il2CppCodeGenWriteBarrier(&___scrollerScrolled_17, value);
	}

	inline static int32_t get_offset_of_scrollerSnapped_18() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___scrollerSnapped_18)); }
	inline ScrollerSnappedDelegate_t1271485394 * get_scrollerSnapped_18() const { return ___scrollerSnapped_18; }
	inline ScrollerSnappedDelegate_t1271485394 ** get_address_of_scrollerSnapped_18() { return &___scrollerSnapped_18; }
	inline void set_scrollerSnapped_18(ScrollerSnappedDelegate_t1271485394 * value)
	{
		___scrollerSnapped_18 = value;
		Il2CppCodeGenWriteBarrier(&___scrollerSnapped_18, value);
	}

	inline static int32_t get_offset_of_scrollerScrollingChanged_19() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___scrollerScrollingChanged_19)); }
	inline ScrollerScrollingChangedDelegate_t2129781406 * get_scrollerScrollingChanged_19() const { return ___scrollerScrollingChanged_19; }
	inline ScrollerScrollingChangedDelegate_t2129781406 ** get_address_of_scrollerScrollingChanged_19() { return &___scrollerScrollingChanged_19; }
	inline void set_scrollerScrollingChanged_19(ScrollerScrollingChangedDelegate_t2129781406 * value)
	{
		___scrollerScrollingChanged_19 = value;
		Il2CppCodeGenWriteBarrier(&___scrollerScrollingChanged_19, value);
	}

	inline static int32_t get_offset_of_scrollerTweeningChanged_20() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___scrollerTweeningChanged_20)); }
	inline ScrollerTweeningChangedDelegate_t2022402836 * get_scrollerTweeningChanged_20() const { return ___scrollerTweeningChanged_20; }
	inline ScrollerTweeningChangedDelegate_t2022402836 ** get_address_of_scrollerTweeningChanged_20() { return &___scrollerTweeningChanged_20; }
	inline void set_scrollerTweeningChanged_20(ScrollerTweeningChangedDelegate_t2022402836 * value)
	{
		___scrollerTweeningChanged_20 = value;
		Il2CppCodeGenWriteBarrier(&___scrollerTweeningChanged_20, value);
	}

	inline static int32_t get_offset_of_U3CIsScrollingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___U3CIsScrollingU3Ek__BackingField_21)); }
	inline bool get_U3CIsScrollingU3Ek__BackingField_21() const { return ___U3CIsScrollingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CIsScrollingU3Ek__BackingField_21() { return &___U3CIsScrollingU3Ek__BackingField_21; }
	inline void set_U3CIsScrollingU3Ek__BackingField_21(bool value)
	{
		___U3CIsScrollingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CIsTweeningU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ___U3CIsTweeningU3Ek__BackingField_22)); }
	inline bool get_U3CIsTweeningU3Ek__BackingField_22() const { return ___U3CIsTweeningU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CIsTweeningU3Ek__BackingField_22() { return &___U3CIsTweeningU3Ek__BackingField_22; }
	inline void set_U3CIsTweeningU3Ek__BackingField_22(bool value)
	{
		___U3CIsTweeningU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of__scrollRect_23() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____scrollRect_23)); }
	inline ScrollRect_t1199013257 * get__scrollRect_23() const { return ____scrollRect_23; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_23() { return &____scrollRect_23; }
	inline void set__scrollRect_23(ScrollRect_t1199013257 * value)
	{
		____scrollRect_23 = value;
		Il2CppCodeGenWriteBarrier(&____scrollRect_23, value);
	}

	inline static int32_t get_offset_of__scrollRectTransform_24() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____scrollRectTransform_24)); }
	inline RectTransform_t3349966182 * get__scrollRectTransform_24() const { return ____scrollRectTransform_24; }
	inline RectTransform_t3349966182 ** get_address_of__scrollRectTransform_24() { return &____scrollRectTransform_24; }
	inline void set__scrollRectTransform_24(RectTransform_t3349966182 * value)
	{
		____scrollRectTransform_24 = value;
		Il2CppCodeGenWriteBarrier(&____scrollRectTransform_24, value);
	}

	inline static int32_t get_offset_of__scrollbar_25() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____scrollbar_25)); }
	inline Scrollbar_t3248359358 * get__scrollbar_25() const { return ____scrollbar_25; }
	inline Scrollbar_t3248359358 ** get_address_of__scrollbar_25() { return &____scrollbar_25; }
	inline void set__scrollbar_25(Scrollbar_t3248359358 * value)
	{
		____scrollbar_25 = value;
		Il2CppCodeGenWriteBarrier(&____scrollbar_25, value);
	}

	inline static int32_t get_offset_of__container_26() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____container_26)); }
	inline RectTransform_t3349966182 * get__container_26() const { return ____container_26; }
	inline RectTransform_t3349966182 ** get_address_of__container_26() { return &____container_26; }
	inline void set__container_26(RectTransform_t3349966182 * value)
	{
		____container_26 = value;
		Il2CppCodeGenWriteBarrier(&____container_26, value);
	}

	inline static int32_t get_offset_of__layoutGroup_27() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____layoutGroup_27)); }
	inline HorizontalOrVerticalLayoutGroup_t1968298610 * get__layoutGroup_27() const { return ____layoutGroup_27; }
	inline HorizontalOrVerticalLayoutGroup_t1968298610 ** get_address_of__layoutGroup_27() { return &____layoutGroup_27; }
	inline void set__layoutGroup_27(HorizontalOrVerticalLayoutGroup_t1968298610 * value)
	{
		____layoutGroup_27 = value;
		Il2CppCodeGenWriteBarrier(&____layoutGroup_27, value);
	}

	inline static int32_t get_offset_of__delegate_28() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____delegate_28)); }
	inline Il2CppObject * get__delegate_28() const { return ____delegate_28; }
	inline Il2CppObject ** get_address_of__delegate_28() { return &____delegate_28; }
	inline void set__delegate_28(Il2CppObject * value)
	{
		____delegate_28 = value;
		Il2CppCodeGenWriteBarrier(&____delegate_28, value);
	}

	inline static int32_t get_offset_of__reloadData_29() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____reloadData_29)); }
	inline bool get__reloadData_29() const { return ____reloadData_29; }
	inline bool* get_address_of__reloadData_29() { return &____reloadData_29; }
	inline void set__reloadData_29(bool value)
	{
		____reloadData_29 = value;
	}

	inline static int32_t get_offset_of__refreshActive_30() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____refreshActive_30)); }
	inline bool get__refreshActive_30() const { return ____refreshActive_30; }
	inline bool* get_address_of__refreshActive_30() { return &____refreshActive_30; }
	inline void set__refreshActive_30(bool value)
	{
		____refreshActive_30 = value;
	}

	inline static int32_t get_offset_of__recycledCellViews_31() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____recycledCellViews_31)); }
	inline SmallList_1_t3164847472 * get__recycledCellViews_31() const { return ____recycledCellViews_31; }
	inline SmallList_1_t3164847472 ** get_address_of__recycledCellViews_31() { return &____recycledCellViews_31; }
	inline void set__recycledCellViews_31(SmallList_1_t3164847472 * value)
	{
		____recycledCellViews_31 = value;
		Il2CppCodeGenWriteBarrier(&____recycledCellViews_31, value);
	}

	inline static int32_t get_offset_of__firstPadder_32() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____firstPadder_32)); }
	inline LayoutElement_t2808691390 * get__firstPadder_32() const { return ____firstPadder_32; }
	inline LayoutElement_t2808691390 ** get_address_of__firstPadder_32() { return &____firstPadder_32; }
	inline void set__firstPadder_32(LayoutElement_t2808691390 * value)
	{
		____firstPadder_32 = value;
		Il2CppCodeGenWriteBarrier(&____firstPadder_32, value);
	}

	inline static int32_t get_offset_of__lastPadder_33() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____lastPadder_33)); }
	inline LayoutElement_t2808691390 * get__lastPadder_33() const { return ____lastPadder_33; }
	inline LayoutElement_t2808691390 ** get_address_of__lastPadder_33() { return &____lastPadder_33; }
	inline void set__lastPadder_33(LayoutElement_t2808691390 * value)
	{
		____lastPadder_33 = value;
		Il2CppCodeGenWriteBarrier(&____lastPadder_33, value);
	}

	inline static int32_t get_offset_of__recycledCellViewContainer_34() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____recycledCellViewContainer_34)); }
	inline RectTransform_t3349966182 * get__recycledCellViewContainer_34() const { return ____recycledCellViewContainer_34; }
	inline RectTransform_t3349966182 ** get_address_of__recycledCellViewContainer_34() { return &____recycledCellViewContainer_34; }
	inline void set__recycledCellViewContainer_34(RectTransform_t3349966182 * value)
	{
		____recycledCellViewContainer_34 = value;
		Il2CppCodeGenWriteBarrier(&____recycledCellViewContainer_34, value);
	}

	inline static int32_t get_offset_of__cellViewSizeArray_35() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____cellViewSizeArray_35)); }
	inline SmallList_1_t4136689155 * get__cellViewSizeArray_35() const { return ____cellViewSizeArray_35; }
	inline SmallList_1_t4136689155 ** get_address_of__cellViewSizeArray_35() { return &____cellViewSizeArray_35; }
	inline void set__cellViewSizeArray_35(SmallList_1_t4136689155 * value)
	{
		____cellViewSizeArray_35 = value;
		Il2CppCodeGenWriteBarrier(&____cellViewSizeArray_35, value);
	}

	inline static int32_t get_offset_of__cellViewOffsetArray_36() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____cellViewOffsetArray_36)); }
	inline SmallList_1_t4136689155 * get__cellViewOffsetArray_36() const { return ____cellViewOffsetArray_36; }
	inline SmallList_1_t4136689155 ** get_address_of__cellViewOffsetArray_36() { return &____cellViewOffsetArray_36; }
	inline void set__cellViewOffsetArray_36(SmallList_1_t4136689155 * value)
	{
		____cellViewOffsetArray_36 = value;
		Il2CppCodeGenWriteBarrier(&____cellViewOffsetArray_36, value);
	}

	inline static int32_t get_offset_of__scrollPosition_37() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____scrollPosition_37)); }
	inline float get__scrollPosition_37() const { return ____scrollPosition_37; }
	inline float* get_address_of__scrollPosition_37() { return &____scrollPosition_37; }
	inline void set__scrollPosition_37(float value)
	{
		____scrollPosition_37 = value;
	}

	inline static int32_t get_offset_of__activeCellViews_38() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____activeCellViews_38)); }
	inline SmallList_1_t3164847472 * get__activeCellViews_38() const { return ____activeCellViews_38; }
	inline SmallList_1_t3164847472 ** get_address_of__activeCellViews_38() { return &____activeCellViews_38; }
	inline void set__activeCellViews_38(SmallList_1_t3164847472 * value)
	{
		____activeCellViews_38 = value;
		Il2CppCodeGenWriteBarrier(&____activeCellViews_38, value);
	}

	inline static int32_t get_offset_of__activeCellViewsStartIndex_39() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____activeCellViewsStartIndex_39)); }
	inline int32_t get__activeCellViewsStartIndex_39() const { return ____activeCellViewsStartIndex_39; }
	inline int32_t* get_address_of__activeCellViewsStartIndex_39() { return &____activeCellViewsStartIndex_39; }
	inline void set__activeCellViewsStartIndex_39(int32_t value)
	{
		____activeCellViewsStartIndex_39 = value;
	}

	inline static int32_t get_offset_of__activeCellViewsEndIndex_40() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____activeCellViewsEndIndex_40)); }
	inline int32_t get__activeCellViewsEndIndex_40() const { return ____activeCellViewsEndIndex_40; }
	inline int32_t* get_address_of__activeCellViewsEndIndex_40() { return &____activeCellViewsEndIndex_40; }
	inline void set__activeCellViewsEndIndex_40(int32_t value)
	{
		____activeCellViewsEndIndex_40 = value;
	}

	inline static int32_t get_offset_of__loopFirstCellIndex_41() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____loopFirstCellIndex_41)); }
	inline int32_t get__loopFirstCellIndex_41() const { return ____loopFirstCellIndex_41; }
	inline int32_t* get_address_of__loopFirstCellIndex_41() { return &____loopFirstCellIndex_41; }
	inline void set__loopFirstCellIndex_41(int32_t value)
	{
		____loopFirstCellIndex_41 = value;
	}

	inline static int32_t get_offset_of__loopLastCellIndex_42() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____loopLastCellIndex_42)); }
	inline int32_t get__loopLastCellIndex_42() const { return ____loopLastCellIndex_42; }
	inline int32_t* get_address_of__loopLastCellIndex_42() { return &____loopLastCellIndex_42; }
	inline void set__loopLastCellIndex_42(int32_t value)
	{
		____loopLastCellIndex_42 = value;
	}

	inline static int32_t get_offset_of__loopFirstScrollPosition_43() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____loopFirstScrollPosition_43)); }
	inline float get__loopFirstScrollPosition_43() const { return ____loopFirstScrollPosition_43; }
	inline float* get_address_of__loopFirstScrollPosition_43() { return &____loopFirstScrollPosition_43; }
	inline void set__loopFirstScrollPosition_43(float value)
	{
		____loopFirstScrollPosition_43 = value;
	}

	inline static int32_t get_offset_of__loopLastScrollPosition_44() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____loopLastScrollPosition_44)); }
	inline float get__loopLastScrollPosition_44() const { return ____loopLastScrollPosition_44; }
	inline float* get_address_of__loopLastScrollPosition_44() { return &____loopLastScrollPosition_44; }
	inline void set__loopLastScrollPosition_44(float value)
	{
		____loopLastScrollPosition_44 = value;
	}

	inline static int32_t get_offset_of__loopFirstJumpTrigger_45() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____loopFirstJumpTrigger_45)); }
	inline float get__loopFirstJumpTrigger_45() const { return ____loopFirstJumpTrigger_45; }
	inline float* get_address_of__loopFirstJumpTrigger_45() { return &____loopFirstJumpTrigger_45; }
	inline void set__loopFirstJumpTrigger_45(float value)
	{
		____loopFirstJumpTrigger_45 = value;
	}

	inline static int32_t get_offset_of__loopLastJumpTrigger_46() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____loopLastJumpTrigger_46)); }
	inline float get__loopLastJumpTrigger_46() const { return ____loopLastJumpTrigger_46; }
	inline float* get_address_of__loopLastJumpTrigger_46() { return &____loopLastJumpTrigger_46; }
	inline void set__loopLastJumpTrigger_46(float value)
	{
		____loopLastJumpTrigger_46 = value;
	}

	inline static int32_t get_offset_of__lastScrollRectSize_47() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____lastScrollRectSize_47)); }
	inline float get__lastScrollRectSize_47() const { return ____lastScrollRectSize_47; }
	inline float* get_address_of__lastScrollRectSize_47() { return &____lastScrollRectSize_47; }
	inline void set__lastScrollRectSize_47(float value)
	{
		____lastScrollRectSize_47 = value;
	}

	inline static int32_t get_offset_of__lastLoop_48() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____lastLoop_48)); }
	inline bool get__lastLoop_48() const { return ____lastLoop_48; }
	inline bool* get_address_of__lastLoop_48() { return &____lastLoop_48; }
	inline void set__lastLoop_48(bool value)
	{
		____lastLoop_48 = value;
	}

	inline static int32_t get_offset_of__snapCellViewIndex_49() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____snapCellViewIndex_49)); }
	inline int32_t get__snapCellViewIndex_49() const { return ____snapCellViewIndex_49; }
	inline int32_t* get_address_of__snapCellViewIndex_49() { return &____snapCellViewIndex_49; }
	inline void set__snapCellViewIndex_49(int32_t value)
	{
		____snapCellViewIndex_49 = value;
	}

	inline static int32_t get_offset_of__snapDataIndex_50() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____snapDataIndex_50)); }
	inline int32_t get__snapDataIndex_50() const { return ____snapDataIndex_50; }
	inline int32_t* get_address_of__snapDataIndex_50() { return &____snapDataIndex_50; }
	inline void set__snapDataIndex_50(int32_t value)
	{
		____snapDataIndex_50 = value;
	}

	inline static int32_t get_offset_of__snapJumping_51() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____snapJumping_51)); }
	inline bool get__snapJumping_51() const { return ____snapJumping_51; }
	inline bool* get_address_of__snapJumping_51() { return &____snapJumping_51; }
	inline void set__snapJumping_51(bool value)
	{
		____snapJumping_51 = value;
	}

	inline static int32_t get_offset_of__snapInertia_52() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____snapInertia_52)); }
	inline bool get__snapInertia_52() const { return ____snapInertia_52; }
	inline bool* get_address_of__snapInertia_52() { return &____snapInertia_52; }
	inline void set__snapInertia_52(bool value)
	{
		____snapInertia_52 = value;
	}

	inline static int32_t get_offset_of__lastScrollbarVisibility_53() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____lastScrollbarVisibility_53)); }
	inline int32_t get__lastScrollbarVisibility_53() const { return ____lastScrollbarVisibility_53; }
	inline int32_t* get_address_of__lastScrollbarVisibility_53() { return &____lastScrollbarVisibility_53; }
	inline void set__lastScrollbarVisibility_53(int32_t value)
	{
		____lastScrollbarVisibility_53 = value;
	}

	inline static int32_t get_offset_of__tweenTimeLeft_54() { return static_cast<int32_t>(offsetof(EnhancedScroller_t2375706558, ____tweenTimeLeft_54)); }
	inline float get__tweenTimeLeft_54() const { return ____tweenTimeLeft_54; }
	inline float* get_address_of__tweenTimeLeft_54() { return &____tweenTimeLeft_54; }
	inline void set__tweenTimeLeft_54(float value)
	{
		____tweenTimeLeft_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
