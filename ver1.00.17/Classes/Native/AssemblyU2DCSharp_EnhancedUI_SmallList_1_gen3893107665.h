﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// EnhancedScrollerDemos.RefreshDemo.Data[]
struct DataU5BU5D_t2535227167;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnhancedUI.SmallList`1<EnhancedScrollerDemos.RefreshDemo.Data>
struct  SmallList_1_t3893107665  : public Il2CppObject
{
public:
	// T[] EnhancedUI.SmallList`1::data
	DataU5BU5D_t2535227167* ___data_0;
	// System.Int32 EnhancedUI.SmallList`1::Count
	int32_t ___Count_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(SmallList_1_t3893107665, ___data_0)); }
	inline DataU5BU5D_t2535227167* get_data_0() const { return ___data_0; }
	inline DataU5BU5D_t2535227167** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(DataU5BU5D_t2535227167* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of_Count_1() { return static_cast<int32_t>(offsetof(SmallList_1_t3893107665, ___Count_1)); }
	inline int32_t get_Count_1() const { return ___Count_1; }
	inline int32_t* get_address_of_Count_1() { return &___Count_1; }
	inline void set_Count_1(int32_t value)
	{
		___Count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
