﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumStringBase
struct  EnumStringBase_t3052423213  : public Il2CppObject
{
public:
	// System.String EnumStringBase::m_text
	String_t* ___m_text_0;

public:
	inline static int32_t get_offset_of_m_text_0() { return static_cast<int32_t>(offsetof(EnumStringBase_t3052423213, ___m_text_0)); }
	inline String_t* get_m_text_0() const { return ___m_text_0; }
	inline String_t** get_address_of_m_text_0() { return &___m_text_0; }
	inline void set_m_text_0(String_t* value)
	{
		___m_text_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_text_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
