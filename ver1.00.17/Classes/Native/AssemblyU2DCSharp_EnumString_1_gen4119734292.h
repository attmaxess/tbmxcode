﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnumStringBase3052423213.h"
#include "mscorlib_System_Nullable_1_gen1611487549.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumString`1<Localization.LocalizeKey>
struct  EnumString_1_t4119734292  : public EnumStringBase_t3052423213
{
public:
	// System.Nullable`1<T> EnumString`1::m_Cache
	Nullable_1_t1611487549  ___m_Cache_1;

public:
	inline static int32_t get_offset_of_m_Cache_1() { return static_cast<int32_t>(offsetof(EnumString_1_t4119734292, ___m_Cache_1)); }
	inline Nullable_1_t1611487549  get_m_Cache_1() const { return ___m_Cache_1; }
	inline Nullable_1_t1611487549 * get_address_of_m_Cache_1() { return &___m_Cache_1; }
	inline void set_m_Cache_1(Nullable_1_t1611487549  value)
	{
		___m_Cache_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
