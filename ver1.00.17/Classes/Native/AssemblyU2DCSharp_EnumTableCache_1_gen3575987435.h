﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.String,Localization.LocalizeKey>
struct Dictionary_2_t968233200;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnumTableCache`1<Localization.LocalizeKey>
struct  EnumTableCache_1_t3575987435  : public Il2CppObject
{
public:

public:
};

struct EnumTableCache_1_t3575987435_StaticFields
{
public:
	// System.Boolean EnumTableCache`1::m_Initialized
	bool ___m_Initialized_0;
	// System.Collections.Generic.Dictionary`2<System.String,T> EnumTableCache`1::m_Table
	Dictionary_2_t968233200 * ___m_Table_1;

public:
	inline static int32_t get_offset_of_m_Initialized_0() { return static_cast<int32_t>(offsetof(EnumTableCache_1_t3575987435_StaticFields, ___m_Initialized_0)); }
	inline bool get_m_Initialized_0() const { return ___m_Initialized_0; }
	inline bool* get_address_of_m_Initialized_0() { return &___m_Initialized_0; }
	inline void set_m_Initialized_0(bool value)
	{
		___m_Initialized_0 = value;
	}

	inline static int32_t get_offset_of_m_Table_1() { return static_cast<int32_t>(offsetof(EnumTableCache_1_t3575987435_StaticFields, ___m_Table_1)); }
	inline Dictionary_2_t968233200 * get_m_Table_1() const { return ___m_Table_1; }
	inline Dictionary_2_t968233200 ** get_address_of_m_Table_1() { return &___m_Table_1; }
	inline void set_m_Table_1(Dictionary_2_t968233200 * value)
	{
		___m_Table_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Table_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
