﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2693961299.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EvasionNav
struct  EvasionNav_t3997284964  : public SingletonMonoBehaviour_1_t2693961299
{
public:
	// System.Single EvasionNav::NavAngle
	float ___NavAngle_3;

public:
	inline static int32_t get_offset_of_NavAngle_3() { return static_cast<int32_t>(offsetof(EvasionNav_t3997284964, ___NavAngle_3)); }
	inline float get_NavAngle_3() const { return ___NavAngle_3; }
	inline float* get_address_of_NavAngle_3() { return &___NavAngle_3; }
	inline void set_NavAngle_3(float value)
	{
		___NavAngle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
