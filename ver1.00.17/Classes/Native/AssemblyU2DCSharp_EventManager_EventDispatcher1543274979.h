﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<eEventID,System.Collections.Generic.List`1<System.Action`2<UnityEngine.MonoBehaviour,System.Object>>>
struct Dictionary_2_t455278295;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventManager.EventDispatcher
struct  EventDispatcher_t1543274979  : public Il2CppObject
{
public:

public:
};

struct EventDispatcher_t1543274979_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<eEventID,System.Collections.Generic.List`1<System.Action`2<UnityEngine.MonoBehaviour,System.Object>>> EventManager.EventDispatcher::_listenersDict
	Dictionary_2_t455278295 * ____listenersDict_0;

public:
	inline static int32_t get_offset_of__listenersDict_0() { return static_cast<int32_t>(offsetof(EventDispatcher_t1543274979_StaticFields, ____listenersDict_0)); }
	inline Dictionary_2_t455278295 * get__listenersDict_0() const { return ____listenersDict_0; }
	inline Dictionary_2_t455278295 ** get_address_of__listenersDict_0() { return &____listenersDict_0; }
	inline void set__listenersDict_0(Dictionary_2_t455278295 * value)
	{
		____listenersDict_0 = value;
		Il2CppCodeGenWriteBarrier(&____listenersDict_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
