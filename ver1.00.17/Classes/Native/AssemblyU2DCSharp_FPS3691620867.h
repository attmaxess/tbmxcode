﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPS
struct  FPS_t3691620867  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GUIStyle FPS::guiStyleHeader
	GUIStyle_t1799908754 * ___guiStyleHeader_2;
	// System.Single FPS::timeleft
	float ___timeleft_3;
	// System.Single FPS::fps
	float ___fps_4;
	// System.Int32 FPS::frames
	int32_t ___frames_5;

public:
	inline static int32_t get_offset_of_guiStyleHeader_2() { return static_cast<int32_t>(offsetof(FPS_t3691620867, ___guiStyleHeader_2)); }
	inline GUIStyle_t1799908754 * get_guiStyleHeader_2() const { return ___guiStyleHeader_2; }
	inline GUIStyle_t1799908754 ** get_address_of_guiStyleHeader_2() { return &___guiStyleHeader_2; }
	inline void set_guiStyleHeader_2(GUIStyle_t1799908754 * value)
	{
		___guiStyleHeader_2 = value;
		Il2CppCodeGenWriteBarrier(&___guiStyleHeader_2, value);
	}

	inline static int32_t get_offset_of_timeleft_3() { return static_cast<int32_t>(offsetof(FPS_t3691620867, ___timeleft_3)); }
	inline float get_timeleft_3() const { return ___timeleft_3; }
	inline float* get_address_of_timeleft_3() { return &___timeleft_3; }
	inline void set_timeleft_3(float value)
	{
		___timeleft_3 = value;
	}

	inline static int32_t get_offset_of_fps_4() { return static_cast<int32_t>(offsetof(FPS_t3691620867, ___fps_4)); }
	inline float get_fps_4() const { return ___fps_4; }
	inline float* get_address_of_fps_4() { return &___fps_4; }
	inline void set_fps_4(float value)
	{
		___fps_4 = value;
	}

	inline static int32_t get_offset_of_frames_5() { return static_cast<int32_t>(offsetof(FPS_t3691620867, ___frames_5)); }
	inline int32_t get_frames_5() const { return ___frames_5; }
	inline int32_t* get_address_of_frames_5() { return &___frames_5; }
	inline void set_frames_5(int32_t value)
	{
		___frames_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
