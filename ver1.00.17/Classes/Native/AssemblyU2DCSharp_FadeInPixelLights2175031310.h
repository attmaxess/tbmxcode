﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Light
struct Light_t494725636;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeInPixelLights
struct  FadeInPixelLights_t2175031310  : public MonoBehaviour_t1158329972
{
public:
	// System.Single FadeInPixelLights::MaxDistance
	float ___MaxDistance_2;
	// System.Single FadeInPixelLights::Fadelength
	float ___Fadelength_3;
	// System.Single FadeInPixelLights::Intensity
	float ___Intensity_4;
	// System.Single FadeInPixelLights::distance
	float ___distance_5;
	// System.Single FadeInPixelLights::factor
	float ___factor_6;
	// UnityEngine.Light FadeInPixelLights::lt
	Light_t494725636 * ___lt_7;

public:
	inline static int32_t get_offset_of_MaxDistance_2() { return static_cast<int32_t>(offsetof(FadeInPixelLights_t2175031310, ___MaxDistance_2)); }
	inline float get_MaxDistance_2() const { return ___MaxDistance_2; }
	inline float* get_address_of_MaxDistance_2() { return &___MaxDistance_2; }
	inline void set_MaxDistance_2(float value)
	{
		___MaxDistance_2 = value;
	}

	inline static int32_t get_offset_of_Fadelength_3() { return static_cast<int32_t>(offsetof(FadeInPixelLights_t2175031310, ___Fadelength_3)); }
	inline float get_Fadelength_3() const { return ___Fadelength_3; }
	inline float* get_address_of_Fadelength_3() { return &___Fadelength_3; }
	inline void set_Fadelength_3(float value)
	{
		___Fadelength_3 = value;
	}

	inline static int32_t get_offset_of_Intensity_4() { return static_cast<int32_t>(offsetof(FadeInPixelLights_t2175031310, ___Intensity_4)); }
	inline float get_Intensity_4() const { return ___Intensity_4; }
	inline float* get_address_of_Intensity_4() { return &___Intensity_4; }
	inline void set_Intensity_4(float value)
	{
		___Intensity_4 = value;
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(FadeInPixelLights_t2175031310, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_factor_6() { return static_cast<int32_t>(offsetof(FadeInPixelLights_t2175031310, ___factor_6)); }
	inline float get_factor_6() const { return ___factor_6; }
	inline float* get_address_of_factor_6() { return &___factor_6; }
	inline void set_factor_6(float value)
	{
		___factor_6 = value;
	}

	inline static int32_t get_offset_of_lt_7() { return static_cast<int32_t>(offsetof(FadeInPixelLights_t2175031310, ___lt_7)); }
	inline Light_t494725636 * get_lt_7() const { return ___lt_7; }
	inline Light_t494725636 ** get_address_of_lt_7() { return &___lt_7; }
	inline void set_lt_7(Light_t494725636 * value)
	{
		___lt_7 = value;
		Il2CppCodeGenWriteBarrier(&___lt_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
