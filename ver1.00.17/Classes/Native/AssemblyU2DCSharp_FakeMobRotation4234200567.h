﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FakeMobRotation
struct  FakeMobRotation_t4234200567  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean FakeMobRotation::x
	bool ___x_2;
	// System.Boolean FakeMobRotation::y
	bool ___y_3;
	// System.Boolean FakeMobRotation::z
	bool ___z_4;
	// System.Single FakeMobRotation::rotateVal
	float ___rotateVal_5;
	// System.Boolean FakeMobRotation::hands
	bool ___hands_6;
	// System.Boolean FakeMobRotation::movechange
	bool ___movechange_7;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(FakeMobRotation_t4234200567, ___x_2)); }
	inline bool get_x_2() const { return ___x_2; }
	inline bool* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(bool value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(FakeMobRotation_t4234200567, ___y_3)); }
	inline bool get_y_3() const { return ___y_3; }
	inline bool* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(bool value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(FakeMobRotation_t4234200567, ___z_4)); }
	inline bool get_z_4() const { return ___z_4; }
	inline bool* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(bool value)
	{
		___z_4 = value;
	}

	inline static int32_t get_offset_of_rotateVal_5() { return static_cast<int32_t>(offsetof(FakeMobRotation_t4234200567, ___rotateVal_5)); }
	inline float get_rotateVal_5() const { return ___rotateVal_5; }
	inline float* get_address_of_rotateVal_5() { return &___rotateVal_5; }
	inline void set_rotateVal_5(float value)
	{
		___rotateVal_5 = value;
	}

	inline static int32_t get_offset_of_hands_6() { return static_cast<int32_t>(offsetof(FakeMobRotation_t4234200567, ___hands_6)); }
	inline bool get_hands_6() const { return ___hands_6; }
	inline bool* get_address_of_hands_6() { return &___hands_6; }
	inline void set_hands_6(bool value)
	{
		___hands_6 = value;
	}

	inline static int32_t get_offset_of_movechange_7() { return static_cast<int32_t>(offsetof(FakeMobRotation_t4234200567, ___movechange_7)); }
	inline bool get_movechange_7() const { return ___movechange_7; }
	inline bool* get_address_of_movechange_7() { return &___movechange_7; }
	inline void set_movechange_7(bool value)
	{
		___movechange_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
