﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeedbackEffect
struct  FeedbackEffect_t3658613102  : public MonoBehaviour_t1158329972
{
public:
	// System.Single FeedbackEffect::Blend
	float ___Blend_2;
	// UnityEngine.Shader FeedbackEffect::Shader
	Shader_t2430389951 * ___Shader_3;
	// UnityEngine.Vector4 FeedbackEffect::Center
	Vector4_t2243707581  ___Center_4;
	// System.Single FeedbackEffect::m_fAspect
	float ___m_fAspect_5;
	// System.Single FeedbackEffect::m_fAlpha
	float ___m_fAlpha_6;
	// System.Single FeedbackEffect::m_fScale
	float ___m_fScale_7;
	// System.Single FeedbackEffect::m_fRotation
	float ___m_fRotation_8;
	// UnityEngine.RenderTexture FeedbackEffect::m_AccumTexture
	RenderTexture_t2666733923 * ___m_AccumTexture_9;
	// System.Int32 FeedbackEffect::m_sMainTexID
	int32_t ___m_sMainTexID_10;
	// System.Int32 FeedbackEffect::m_sBlendID
	int32_t ___m_sBlendID_11;
	// System.Int32 FeedbackEffect::m_sRotateID
	int32_t ___m_sRotateID_12;
	// System.Int32 FeedbackEffect::m_sCenterID
	int32_t ___m_sCenterID_13;
	// UnityEngine.Material FeedbackEffect::m_Material
	Material_t193706927 * ___m_Material_14;

public:
	inline static int32_t get_offset_of_Blend_2() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___Blend_2)); }
	inline float get_Blend_2() const { return ___Blend_2; }
	inline float* get_address_of_Blend_2() { return &___Blend_2; }
	inline void set_Blend_2(float value)
	{
		___Blend_2 = value;
	}

	inline static int32_t get_offset_of_Shader_3() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___Shader_3)); }
	inline Shader_t2430389951 * get_Shader_3() const { return ___Shader_3; }
	inline Shader_t2430389951 ** get_address_of_Shader_3() { return &___Shader_3; }
	inline void set_Shader_3(Shader_t2430389951 * value)
	{
		___Shader_3 = value;
		Il2CppCodeGenWriteBarrier(&___Shader_3, value);
	}

	inline static int32_t get_offset_of_Center_4() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___Center_4)); }
	inline Vector4_t2243707581  get_Center_4() const { return ___Center_4; }
	inline Vector4_t2243707581 * get_address_of_Center_4() { return &___Center_4; }
	inline void set_Center_4(Vector4_t2243707581  value)
	{
		___Center_4 = value;
	}

	inline static int32_t get_offset_of_m_fAspect_5() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_fAspect_5)); }
	inline float get_m_fAspect_5() const { return ___m_fAspect_5; }
	inline float* get_address_of_m_fAspect_5() { return &___m_fAspect_5; }
	inline void set_m_fAspect_5(float value)
	{
		___m_fAspect_5 = value;
	}

	inline static int32_t get_offset_of_m_fAlpha_6() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_fAlpha_6)); }
	inline float get_m_fAlpha_6() const { return ___m_fAlpha_6; }
	inline float* get_address_of_m_fAlpha_6() { return &___m_fAlpha_6; }
	inline void set_m_fAlpha_6(float value)
	{
		___m_fAlpha_6 = value;
	}

	inline static int32_t get_offset_of_m_fScale_7() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_fScale_7)); }
	inline float get_m_fScale_7() const { return ___m_fScale_7; }
	inline float* get_address_of_m_fScale_7() { return &___m_fScale_7; }
	inline void set_m_fScale_7(float value)
	{
		___m_fScale_7 = value;
	}

	inline static int32_t get_offset_of_m_fRotation_8() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_fRotation_8)); }
	inline float get_m_fRotation_8() const { return ___m_fRotation_8; }
	inline float* get_address_of_m_fRotation_8() { return &___m_fRotation_8; }
	inline void set_m_fRotation_8(float value)
	{
		___m_fRotation_8 = value;
	}

	inline static int32_t get_offset_of_m_AccumTexture_9() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_AccumTexture_9)); }
	inline RenderTexture_t2666733923 * get_m_AccumTexture_9() const { return ___m_AccumTexture_9; }
	inline RenderTexture_t2666733923 ** get_address_of_m_AccumTexture_9() { return &___m_AccumTexture_9; }
	inline void set_m_AccumTexture_9(RenderTexture_t2666733923 * value)
	{
		___m_AccumTexture_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_AccumTexture_9, value);
	}

	inline static int32_t get_offset_of_m_sMainTexID_10() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_sMainTexID_10)); }
	inline int32_t get_m_sMainTexID_10() const { return ___m_sMainTexID_10; }
	inline int32_t* get_address_of_m_sMainTexID_10() { return &___m_sMainTexID_10; }
	inline void set_m_sMainTexID_10(int32_t value)
	{
		___m_sMainTexID_10 = value;
	}

	inline static int32_t get_offset_of_m_sBlendID_11() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_sBlendID_11)); }
	inline int32_t get_m_sBlendID_11() const { return ___m_sBlendID_11; }
	inline int32_t* get_address_of_m_sBlendID_11() { return &___m_sBlendID_11; }
	inline void set_m_sBlendID_11(int32_t value)
	{
		___m_sBlendID_11 = value;
	}

	inline static int32_t get_offset_of_m_sRotateID_12() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_sRotateID_12)); }
	inline int32_t get_m_sRotateID_12() const { return ___m_sRotateID_12; }
	inline int32_t* get_address_of_m_sRotateID_12() { return &___m_sRotateID_12; }
	inline void set_m_sRotateID_12(int32_t value)
	{
		___m_sRotateID_12 = value;
	}

	inline static int32_t get_offset_of_m_sCenterID_13() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_sCenterID_13)); }
	inline int32_t get_m_sCenterID_13() const { return ___m_sCenterID_13; }
	inline int32_t* get_address_of_m_sCenterID_13() { return &___m_sCenterID_13; }
	inline void set_m_sCenterID_13(int32_t value)
	{
		___m_sCenterID_13 = value;
	}

	inline static int32_t get_offset_of_m_Material_14() { return static_cast<int32_t>(offsetof(FeedbackEffect_t3658613102, ___m_Material_14)); }
	inline Material_t193706927 * get_m_Material_14() const { return ___m_Material_14; }
	inline Material_t193706927 ** get_address_of_m_Material_14() { return &___m_Material_14; }
	inline void set_m_Material_14(Material_t193706927 * value)
	{
		___m_Material_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_Material_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
