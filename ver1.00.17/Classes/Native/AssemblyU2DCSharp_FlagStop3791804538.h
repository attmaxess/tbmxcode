﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlagStop
struct  FlagStop_t3791804538  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject FlagStop::PlayerObj
	GameObject_t1756533147 * ___PlayerObj_2;
	// UnityEngine.GameObject FlagStop::planet
	GameObject_t1756533147 * ___planet_3;
	// System.Single FlagStop::accelerationScale
	float ___accelerationScale_4;

public:
	inline static int32_t get_offset_of_PlayerObj_2() { return static_cast<int32_t>(offsetof(FlagStop_t3791804538, ___PlayerObj_2)); }
	inline GameObject_t1756533147 * get_PlayerObj_2() const { return ___PlayerObj_2; }
	inline GameObject_t1756533147 ** get_address_of_PlayerObj_2() { return &___PlayerObj_2; }
	inline void set_PlayerObj_2(GameObject_t1756533147 * value)
	{
		___PlayerObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerObj_2, value);
	}

	inline static int32_t get_offset_of_planet_3() { return static_cast<int32_t>(offsetof(FlagStop_t3791804538, ___planet_3)); }
	inline GameObject_t1756533147 * get_planet_3() const { return ___planet_3; }
	inline GameObject_t1756533147 ** get_address_of_planet_3() { return &___planet_3; }
	inline void set_planet_3(GameObject_t1756533147 * value)
	{
		___planet_3 = value;
		Il2CppCodeGenWriteBarrier(&___planet_3, value);
	}

	inline static int32_t get_offset_of_accelerationScale_4() { return static_cast<int32_t>(offsetof(FlagStop_t3791804538, ___accelerationScale_4)); }
	inline float get_accelerationScale_4() const { return ___accelerationScale_4; }
	inline float* get_address_of_accelerationScale_4() { return &___accelerationScale_4; }
	inline void set_accelerationScale_4(float value)
	{
		___accelerationScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
