﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlowLayoutGroup
struct  FlowLayoutGroup_t655567405  : public LayoutGroup_t3962498969
{
public:
	// UnityEngine.Vector2 FlowLayoutGroup::m_CellSize
	Vector2_t2243707579  ___m_CellSize_10;
	// UnityEngine.Vector2 FlowLayoutGroup::m_Spacing
	Vector2_t2243707579  ___m_Spacing_11;
	// System.Int32 FlowLayoutGroup::cellsPerMainAxis
	int32_t ___cellsPerMainAxis_12;
	// System.Int32 FlowLayoutGroup::actualCellCountX
	int32_t ___actualCellCountX_13;
	// System.Int32 FlowLayoutGroup::actualCellCountY
	int32_t ___actualCellCountY_14;
	// System.Int32 FlowLayoutGroup::positionX
	int32_t ___positionX_15;
	// System.Int32 FlowLayoutGroup::positionY
	int32_t ___positionY_16;
	// System.Single FlowLayoutGroup::totalWidth
	float ___totalWidth_17;
	// System.Single FlowLayoutGroup::totalHeight
	float ___totalHeight_18;
	// System.Single FlowLayoutGroup::lastMaxHeight
	float ___lastMaxHeight_19;

public:
	inline static int32_t get_offset_of_m_CellSize_10() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___m_CellSize_10)); }
	inline Vector2_t2243707579  get_m_CellSize_10() const { return ___m_CellSize_10; }
	inline Vector2_t2243707579 * get_address_of_m_CellSize_10() { return &___m_CellSize_10; }
	inline void set_m_CellSize_10(Vector2_t2243707579  value)
	{
		___m_CellSize_10 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_11() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___m_Spacing_11)); }
	inline Vector2_t2243707579  get_m_Spacing_11() const { return ___m_Spacing_11; }
	inline Vector2_t2243707579 * get_address_of_m_Spacing_11() { return &___m_Spacing_11; }
	inline void set_m_Spacing_11(Vector2_t2243707579  value)
	{
		___m_Spacing_11 = value;
	}

	inline static int32_t get_offset_of_cellsPerMainAxis_12() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___cellsPerMainAxis_12)); }
	inline int32_t get_cellsPerMainAxis_12() const { return ___cellsPerMainAxis_12; }
	inline int32_t* get_address_of_cellsPerMainAxis_12() { return &___cellsPerMainAxis_12; }
	inline void set_cellsPerMainAxis_12(int32_t value)
	{
		___cellsPerMainAxis_12 = value;
	}

	inline static int32_t get_offset_of_actualCellCountX_13() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___actualCellCountX_13)); }
	inline int32_t get_actualCellCountX_13() const { return ___actualCellCountX_13; }
	inline int32_t* get_address_of_actualCellCountX_13() { return &___actualCellCountX_13; }
	inline void set_actualCellCountX_13(int32_t value)
	{
		___actualCellCountX_13 = value;
	}

	inline static int32_t get_offset_of_actualCellCountY_14() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___actualCellCountY_14)); }
	inline int32_t get_actualCellCountY_14() const { return ___actualCellCountY_14; }
	inline int32_t* get_address_of_actualCellCountY_14() { return &___actualCellCountY_14; }
	inline void set_actualCellCountY_14(int32_t value)
	{
		___actualCellCountY_14 = value;
	}

	inline static int32_t get_offset_of_positionX_15() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___positionX_15)); }
	inline int32_t get_positionX_15() const { return ___positionX_15; }
	inline int32_t* get_address_of_positionX_15() { return &___positionX_15; }
	inline void set_positionX_15(int32_t value)
	{
		___positionX_15 = value;
	}

	inline static int32_t get_offset_of_positionY_16() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___positionY_16)); }
	inline int32_t get_positionY_16() const { return ___positionY_16; }
	inline int32_t* get_address_of_positionY_16() { return &___positionY_16; }
	inline void set_positionY_16(int32_t value)
	{
		___positionY_16 = value;
	}

	inline static int32_t get_offset_of_totalWidth_17() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___totalWidth_17)); }
	inline float get_totalWidth_17() const { return ___totalWidth_17; }
	inline float* get_address_of_totalWidth_17() { return &___totalWidth_17; }
	inline void set_totalWidth_17(float value)
	{
		___totalWidth_17 = value;
	}

	inline static int32_t get_offset_of_totalHeight_18() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___totalHeight_18)); }
	inline float get_totalHeight_18() const { return ___totalHeight_18; }
	inline float* get_address_of_totalHeight_18() { return &___totalHeight_18; }
	inline void set_totalHeight_18(float value)
	{
		___totalHeight_18 = value;
	}

	inline static int32_t get_offset_of_lastMaxHeight_19() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t655567405, ___lastMaxHeight_19)); }
	inline float get_lastMaxHeight_19() const { return ___lastMaxHeight_19; }
	inline float* get_address_of_lastMaxHeight_19() { return &___lastMaxHeight_19; }
	inline void set_lastMaxHeight_19(float value)
	{
		___lastMaxHeight_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
