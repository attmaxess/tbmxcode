﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// DG.Tweening.Tween
struct Tween_t278478013;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3858616074;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t3734738918;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FruitsSlider
struct  FruitsSlider_t2064329978  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject FruitsSlider::JumpArea
	GameObject_t1756533147 * ___JumpArea_2;
	// UnityEngine.GameObject FruitsSlider::JumpButtonUI
	GameObject_t1756533147 * ___JumpButtonUI_3;
	// UnityEngine.UI.Image FruitsSlider::JumpGuage
	Image_t2042527209 * ___JumpGuage_4;
	// UnityEngine.Vector3 FruitsSlider::JumpPos
	Vector3_t2243707580  ___JumpPos_5;
	// System.Single FruitsSlider::JumpScore
	float ___JumpScore_6;
	// System.Boolean FruitsSlider::m_bIsJump
	bool ___m_bIsJump_7;
	// System.Boolean FruitsSlider::m_bMobilmoCol
	bool ___m_bMobilmoCol_8;
	// System.Boolean FruitsSlider::m_bCameraMove
	bool ___m_bCameraMove_9;
	// DG.Tweening.Tween FruitsSlider::JumpTw
	Tween_t278478013 * ___JumpTw_10;
	// DG.Tweening.Tween FruitsSlider::TimeScaleTw
	Tween_t278478013 * ___TimeScaleTw_11;
	// System.Int32 FruitsSlider::m_mobilityId
	int32_t ___m_mobilityId_12;

public:
	inline static int32_t get_offset_of_JumpArea_2() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___JumpArea_2)); }
	inline GameObject_t1756533147 * get_JumpArea_2() const { return ___JumpArea_2; }
	inline GameObject_t1756533147 ** get_address_of_JumpArea_2() { return &___JumpArea_2; }
	inline void set_JumpArea_2(GameObject_t1756533147 * value)
	{
		___JumpArea_2 = value;
		Il2CppCodeGenWriteBarrier(&___JumpArea_2, value);
	}

	inline static int32_t get_offset_of_JumpButtonUI_3() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___JumpButtonUI_3)); }
	inline GameObject_t1756533147 * get_JumpButtonUI_3() const { return ___JumpButtonUI_3; }
	inline GameObject_t1756533147 ** get_address_of_JumpButtonUI_3() { return &___JumpButtonUI_3; }
	inline void set_JumpButtonUI_3(GameObject_t1756533147 * value)
	{
		___JumpButtonUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___JumpButtonUI_3, value);
	}

	inline static int32_t get_offset_of_JumpGuage_4() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___JumpGuage_4)); }
	inline Image_t2042527209 * get_JumpGuage_4() const { return ___JumpGuage_4; }
	inline Image_t2042527209 ** get_address_of_JumpGuage_4() { return &___JumpGuage_4; }
	inline void set_JumpGuage_4(Image_t2042527209 * value)
	{
		___JumpGuage_4 = value;
		Il2CppCodeGenWriteBarrier(&___JumpGuage_4, value);
	}

	inline static int32_t get_offset_of_JumpPos_5() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___JumpPos_5)); }
	inline Vector3_t2243707580  get_JumpPos_5() const { return ___JumpPos_5; }
	inline Vector3_t2243707580 * get_address_of_JumpPos_5() { return &___JumpPos_5; }
	inline void set_JumpPos_5(Vector3_t2243707580  value)
	{
		___JumpPos_5 = value;
	}

	inline static int32_t get_offset_of_JumpScore_6() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___JumpScore_6)); }
	inline float get_JumpScore_6() const { return ___JumpScore_6; }
	inline float* get_address_of_JumpScore_6() { return &___JumpScore_6; }
	inline void set_JumpScore_6(float value)
	{
		___JumpScore_6 = value;
	}

	inline static int32_t get_offset_of_m_bIsJump_7() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___m_bIsJump_7)); }
	inline bool get_m_bIsJump_7() const { return ___m_bIsJump_7; }
	inline bool* get_address_of_m_bIsJump_7() { return &___m_bIsJump_7; }
	inline void set_m_bIsJump_7(bool value)
	{
		___m_bIsJump_7 = value;
	}

	inline static int32_t get_offset_of_m_bMobilmoCol_8() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___m_bMobilmoCol_8)); }
	inline bool get_m_bMobilmoCol_8() const { return ___m_bMobilmoCol_8; }
	inline bool* get_address_of_m_bMobilmoCol_8() { return &___m_bMobilmoCol_8; }
	inline void set_m_bMobilmoCol_8(bool value)
	{
		___m_bMobilmoCol_8 = value;
	}

	inline static int32_t get_offset_of_m_bCameraMove_9() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___m_bCameraMove_9)); }
	inline bool get_m_bCameraMove_9() const { return ___m_bCameraMove_9; }
	inline bool* get_address_of_m_bCameraMove_9() { return &___m_bCameraMove_9; }
	inline void set_m_bCameraMove_9(bool value)
	{
		___m_bCameraMove_9 = value;
	}

	inline static int32_t get_offset_of_JumpTw_10() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___JumpTw_10)); }
	inline Tween_t278478013 * get_JumpTw_10() const { return ___JumpTw_10; }
	inline Tween_t278478013 ** get_address_of_JumpTw_10() { return &___JumpTw_10; }
	inline void set_JumpTw_10(Tween_t278478013 * value)
	{
		___JumpTw_10 = value;
		Il2CppCodeGenWriteBarrier(&___JumpTw_10, value);
	}

	inline static int32_t get_offset_of_TimeScaleTw_11() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___TimeScaleTw_11)); }
	inline Tween_t278478013 * get_TimeScaleTw_11() const { return ___TimeScaleTw_11; }
	inline Tween_t278478013 ** get_address_of_TimeScaleTw_11() { return &___TimeScaleTw_11; }
	inline void set_TimeScaleTw_11(Tween_t278478013 * value)
	{
		___TimeScaleTw_11 = value;
		Il2CppCodeGenWriteBarrier(&___TimeScaleTw_11, value);
	}

	inline static int32_t get_offset_of_m_mobilityId_12() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978, ___m_mobilityId_12)); }
	inline int32_t get_m_mobilityId_12() const { return ___m_mobilityId_12; }
	inline int32_t* get_address_of_m_mobilityId_12() { return &___m_mobilityId_12; }
	inline void set_m_mobilityId_12(int32_t value)
	{
		___m_mobilityId_12 = value;
	}
};

struct FruitsSlider_t2064329978_StaticFields
{
public:
	// DG.Tweening.Core.DOGetter`1<System.Single> FruitsSlider::<>f__mg$cache0
	DOGetter_1_t3858616074 * ___U3CU3Ef__mgU24cache0_13;
	// DG.Tweening.Core.DOSetter`1<System.Single> FruitsSlider::<>f__am$cache0
	DOSetter_1_t3734738918 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline DOGetter_1_t3858616074 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline DOGetter_1_t3858616074 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(DOGetter_1_t3858616074 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(FruitsSlider_t2064329978_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline DOSetter_1_t3734738918 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline DOSetter_1_t3734738918 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(DOSetter_1_t3734738918 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
