﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.LensFlare
struct LensFlare_t529161798;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSDirLight
struct  FSDirLight_t1852219918  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Fujioka.FSDirLight::go_
	GameObject_t1756533147 * ___go__2;
	// UnityEngine.Light Fujioka.FSDirLight::dirLight
	Light_t494725636 * ___dirLight_3;
	// UnityEngine.LensFlare Fujioka.FSDirLight::flare
	LensFlare_t529161798 * ___flare_4;

public:
	inline static int32_t get_offset_of_go__2() { return static_cast<int32_t>(offsetof(FSDirLight_t1852219918, ___go__2)); }
	inline GameObject_t1756533147 * get_go__2() const { return ___go__2; }
	inline GameObject_t1756533147 ** get_address_of_go__2() { return &___go__2; }
	inline void set_go__2(GameObject_t1756533147 * value)
	{
		___go__2 = value;
		Il2CppCodeGenWriteBarrier(&___go__2, value);
	}

	inline static int32_t get_offset_of_dirLight_3() { return static_cast<int32_t>(offsetof(FSDirLight_t1852219918, ___dirLight_3)); }
	inline Light_t494725636 * get_dirLight_3() const { return ___dirLight_3; }
	inline Light_t494725636 ** get_address_of_dirLight_3() { return &___dirLight_3; }
	inline void set_dirLight_3(Light_t494725636 * value)
	{
		___dirLight_3 = value;
		Il2CppCodeGenWriteBarrier(&___dirLight_3, value);
	}

	inline static int32_t get_offset_of_flare_4() { return static_cast<int32_t>(offsetof(FSDirLight_t1852219918, ___flare_4)); }
	inline LensFlare_t529161798 * get_flare_4() const { return ___flare_4; }
	inline LensFlare_t529161798 ** get_address_of_flare_4() { return &___flare_4; }
	inline void set_flare_4(LensFlare_t529161798 * value)
	{
		___flare_4 = value;
		Il2CppCodeGenWriteBarrier(&___flare_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
