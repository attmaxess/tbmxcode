﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Fujioka_FSEffect_ENABLE567653665.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_Fujioka_FSEffect_ParamBackup1404423237.h"

// System.String
struct String_t;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Camera
struct Camera_t189460977;
// Fujioka.FSDirLight
struct FSDirLight_t1852219918;
// Fujioka.FSSpotLight
struct FSSpotLight_t2693339535;
// Fujioka.FSPlanet
struct FSPlanet_t2551900013;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSEffect
struct  FSEffect_t3187928436  : public MonoBehaviour_t1158329972
{
public:
	// Fujioka.FSEffect/ENABLE Fujioka.FSEffect::enableFlags
	int32_t ___enableFlags_2;
	// System.Int32 Fujioka.FSEffect::SHADER_PROPERTY_STARTLUT
	int32_t ___SHADER_PROPERTY_STARTLUT_3;
	// System.Int32 Fujioka.FSEffect::SHADER_PROPERTY_ENDLUT
	int32_t ___SHADER_PROPERTY_ENDLUT_4;
	// System.Boolean Fujioka.FSEffect::atmosphere
	bool ___atmosphere_7;
	// UnityEngine.Color Fujioka.FSEffect::ambientGrdColor
	Color_t2020392075  ___ambientGrdColor_8;
	// UnityEngine.Color Fujioka.FSEffect::ambientColor
	Color_t2020392075  ___ambientColor_9;
	// UnityEngine.Color Fujioka.FSEffect::atmosNearColor
	Color_t2020392075  ___atmosNearColor_10;
	// UnityEngine.Color Fujioka.FSEffect::atmosFarColor
	Color_t2020392075  ___atmosFarColor_11;
	// System.Single Fujioka.FSEffect::atmosIntensity
	float ___atmosIntensity_12;
	// System.Single Fujioka.FSEffect::atmosStartDist
	float ___atmosStartDist_13;
	// System.Single Fujioka.FSEffect::atmosEndDist
	float ___atmosEndDist_14;
	// System.Boolean Fujioka.FSEffect::lightMap
	bool ___lightMap_15;
	// System.Boolean Fujioka.FSEffect::metallic
	bool ___metallic_16;
	// System.Boolean Fujioka.FSEffect::dirLight
	bool ___dirLight_17;
	// UnityEngine.Color Fujioka.FSEffect::lightColor
	Color_t2020392075  ___lightColor_18;
	// System.Single Fujioka.FSEffect::lightIntensity
	float ___lightIntensity_19;
	// System.Single Fujioka.FSEffect::shadowStrength
	float ___shadowStrength_20;
	// System.Single Fujioka.FSEffect::shadowDistance
	float ___shadowDistance_21;
	// System.Single Fujioka.FSEffect::lightFlareBrightness
	float ___lightFlareBrightness_22;
	// UnityEngine.Material Fujioka.FSEffect::skybox
	Material_t193706927 * ___skybox_23;
	// UnityEngine.Color Fujioka.FSEffect::skyUpColor
	Color_t2020392075  ___skyUpColor_24;
	// UnityEngine.Color Fujioka.FSEffect::skyDwColor
	Color_t2020392075  ___skyDwColor_25;
	// System.Single Fujioka.FSEffect::skyGradientStart
	float ___skyGradientStart_26;
	// System.Single Fujioka.FSEffect::skyGradientRange
	float ___skyGradientRange_27;
	// System.Single Fujioka.FSEffect::skyGradientBloom
	float ___skyGradientBloom_28;
	// System.Boolean Fujioka.FSEffect::softFocus
	bool ___softFocus_29;
	// System.Single Fujioka.FSEffect::bufferDownPercent
	float ___bufferDownPercent_30;
	// System.Single Fujioka.FSEffect::softBlurSize
	float ___softBlurSize_31;
	// System.Boolean Fujioka.FSEffect::bloom
	bool ___bloom_32;
	// System.Single Fujioka.FSEffect::bloomBlurSize
	float ___bloomBlurSize_33;
	// System.Single Fujioka.FSEffect::bloomThreshold
	float ___bloomThreshold_34;
	// System.Single Fujioka.FSEffect::bloomIntensity
	float ___bloomIntensity_35;
	// System.Boolean Fujioka.FSEffect::colorGrading
	bool ___colorGrading_36;
	// System.Single Fujioka.FSEffect::colorGradingLerpAmount
	float ___colorGradingLerpAmount_37;
	// UnityEngine.Texture Fujioka.FSEffect::colorGradingStartLUT
	Texture_t2243626319 * ___colorGradingStartLUT_38;
	// UnityEngine.Texture Fujioka.FSEffect::colorGradingEndLUT
	Texture_t2243626319 * ___colorGradingEndLUT_39;
	// System.Boolean Fujioka.FSEffect::antiAlias
	bool ___antiAlias_40;
	// UnityEngine.Material Fujioka.FSEffect::colorGradingMaterial
	Material_t193706927 * ___colorGradingMaterial_41;
	// UnityEngine.Material Fujioka.FSEffect::gaussianBlurMaterial
	Material_t193706927 * ___gaussianBlurMaterial_42;
	// UnityEngine.Camera Fujioka.FSEffect::camera_
	Camera_t189460977 * ___camera__43;
	// Fujioka.FSDirLight Fujioka.FSEffect::dirLightCtrl
	FSDirLight_t1852219918 * ___dirLightCtrl_44;
	// Fujioka.FSSpotLight Fujioka.FSEffect::spotLightCtrl
	FSSpotLight_t2693339535 * ___spotLightCtrl_45;
	// Fujioka.FSPlanet Fujioka.FSEffect::planetCtrl
	FSPlanet_t2551900013 * ___planetCtrl_46;
	// UnityEngine.Transform Fujioka.FSEffect::dirLightTrans
	Transform_t3275118058 * ___dirLightTrans_47;
	// UnityEngine.Quaternion Fujioka.FSEffect::dirLightRot
	Quaternion_t4030073918  ___dirLightRot_48;
	// Fujioka.FSEffect/ParamBackup Fujioka.FSEffect::backup
	ParamBackup_t1404423237  ___backup_49;

public:
	inline static int32_t get_offset_of_enableFlags_2() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___enableFlags_2)); }
	inline int32_t get_enableFlags_2() const { return ___enableFlags_2; }
	inline int32_t* get_address_of_enableFlags_2() { return &___enableFlags_2; }
	inline void set_enableFlags_2(int32_t value)
	{
		___enableFlags_2 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_STARTLUT_3() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___SHADER_PROPERTY_STARTLUT_3)); }
	inline int32_t get_SHADER_PROPERTY_STARTLUT_3() const { return ___SHADER_PROPERTY_STARTLUT_3; }
	inline int32_t* get_address_of_SHADER_PROPERTY_STARTLUT_3() { return &___SHADER_PROPERTY_STARTLUT_3; }
	inline void set_SHADER_PROPERTY_STARTLUT_3(int32_t value)
	{
		___SHADER_PROPERTY_STARTLUT_3 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_ENDLUT_4() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___SHADER_PROPERTY_ENDLUT_4)); }
	inline int32_t get_SHADER_PROPERTY_ENDLUT_4() const { return ___SHADER_PROPERTY_ENDLUT_4; }
	inline int32_t* get_address_of_SHADER_PROPERTY_ENDLUT_4() { return &___SHADER_PROPERTY_ENDLUT_4; }
	inline void set_SHADER_PROPERTY_ENDLUT_4(int32_t value)
	{
		___SHADER_PROPERTY_ENDLUT_4 = value;
	}

	inline static int32_t get_offset_of_atmosphere_7() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___atmosphere_7)); }
	inline bool get_atmosphere_7() const { return ___atmosphere_7; }
	inline bool* get_address_of_atmosphere_7() { return &___atmosphere_7; }
	inline void set_atmosphere_7(bool value)
	{
		___atmosphere_7 = value;
	}

	inline static int32_t get_offset_of_ambientGrdColor_8() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___ambientGrdColor_8)); }
	inline Color_t2020392075  get_ambientGrdColor_8() const { return ___ambientGrdColor_8; }
	inline Color_t2020392075 * get_address_of_ambientGrdColor_8() { return &___ambientGrdColor_8; }
	inline void set_ambientGrdColor_8(Color_t2020392075  value)
	{
		___ambientGrdColor_8 = value;
	}

	inline static int32_t get_offset_of_ambientColor_9() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___ambientColor_9)); }
	inline Color_t2020392075  get_ambientColor_9() const { return ___ambientColor_9; }
	inline Color_t2020392075 * get_address_of_ambientColor_9() { return &___ambientColor_9; }
	inline void set_ambientColor_9(Color_t2020392075  value)
	{
		___ambientColor_9 = value;
	}

	inline static int32_t get_offset_of_atmosNearColor_10() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___atmosNearColor_10)); }
	inline Color_t2020392075  get_atmosNearColor_10() const { return ___atmosNearColor_10; }
	inline Color_t2020392075 * get_address_of_atmosNearColor_10() { return &___atmosNearColor_10; }
	inline void set_atmosNearColor_10(Color_t2020392075  value)
	{
		___atmosNearColor_10 = value;
	}

	inline static int32_t get_offset_of_atmosFarColor_11() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___atmosFarColor_11)); }
	inline Color_t2020392075  get_atmosFarColor_11() const { return ___atmosFarColor_11; }
	inline Color_t2020392075 * get_address_of_atmosFarColor_11() { return &___atmosFarColor_11; }
	inline void set_atmosFarColor_11(Color_t2020392075  value)
	{
		___atmosFarColor_11 = value;
	}

	inline static int32_t get_offset_of_atmosIntensity_12() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___atmosIntensity_12)); }
	inline float get_atmosIntensity_12() const { return ___atmosIntensity_12; }
	inline float* get_address_of_atmosIntensity_12() { return &___atmosIntensity_12; }
	inline void set_atmosIntensity_12(float value)
	{
		___atmosIntensity_12 = value;
	}

	inline static int32_t get_offset_of_atmosStartDist_13() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___atmosStartDist_13)); }
	inline float get_atmosStartDist_13() const { return ___atmosStartDist_13; }
	inline float* get_address_of_atmosStartDist_13() { return &___atmosStartDist_13; }
	inline void set_atmosStartDist_13(float value)
	{
		___atmosStartDist_13 = value;
	}

	inline static int32_t get_offset_of_atmosEndDist_14() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___atmosEndDist_14)); }
	inline float get_atmosEndDist_14() const { return ___atmosEndDist_14; }
	inline float* get_address_of_atmosEndDist_14() { return &___atmosEndDist_14; }
	inline void set_atmosEndDist_14(float value)
	{
		___atmosEndDist_14 = value;
	}

	inline static int32_t get_offset_of_lightMap_15() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___lightMap_15)); }
	inline bool get_lightMap_15() const { return ___lightMap_15; }
	inline bool* get_address_of_lightMap_15() { return &___lightMap_15; }
	inline void set_lightMap_15(bool value)
	{
		___lightMap_15 = value;
	}

	inline static int32_t get_offset_of_metallic_16() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___metallic_16)); }
	inline bool get_metallic_16() const { return ___metallic_16; }
	inline bool* get_address_of_metallic_16() { return &___metallic_16; }
	inline void set_metallic_16(bool value)
	{
		___metallic_16 = value;
	}

	inline static int32_t get_offset_of_dirLight_17() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___dirLight_17)); }
	inline bool get_dirLight_17() const { return ___dirLight_17; }
	inline bool* get_address_of_dirLight_17() { return &___dirLight_17; }
	inline void set_dirLight_17(bool value)
	{
		___dirLight_17 = value;
	}

	inline static int32_t get_offset_of_lightColor_18() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___lightColor_18)); }
	inline Color_t2020392075  get_lightColor_18() const { return ___lightColor_18; }
	inline Color_t2020392075 * get_address_of_lightColor_18() { return &___lightColor_18; }
	inline void set_lightColor_18(Color_t2020392075  value)
	{
		___lightColor_18 = value;
	}

	inline static int32_t get_offset_of_lightIntensity_19() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___lightIntensity_19)); }
	inline float get_lightIntensity_19() const { return ___lightIntensity_19; }
	inline float* get_address_of_lightIntensity_19() { return &___lightIntensity_19; }
	inline void set_lightIntensity_19(float value)
	{
		___lightIntensity_19 = value;
	}

	inline static int32_t get_offset_of_shadowStrength_20() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___shadowStrength_20)); }
	inline float get_shadowStrength_20() const { return ___shadowStrength_20; }
	inline float* get_address_of_shadowStrength_20() { return &___shadowStrength_20; }
	inline void set_shadowStrength_20(float value)
	{
		___shadowStrength_20 = value;
	}

	inline static int32_t get_offset_of_shadowDistance_21() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___shadowDistance_21)); }
	inline float get_shadowDistance_21() const { return ___shadowDistance_21; }
	inline float* get_address_of_shadowDistance_21() { return &___shadowDistance_21; }
	inline void set_shadowDistance_21(float value)
	{
		___shadowDistance_21 = value;
	}

	inline static int32_t get_offset_of_lightFlareBrightness_22() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___lightFlareBrightness_22)); }
	inline float get_lightFlareBrightness_22() const { return ___lightFlareBrightness_22; }
	inline float* get_address_of_lightFlareBrightness_22() { return &___lightFlareBrightness_22; }
	inline void set_lightFlareBrightness_22(float value)
	{
		___lightFlareBrightness_22 = value;
	}

	inline static int32_t get_offset_of_skybox_23() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___skybox_23)); }
	inline Material_t193706927 * get_skybox_23() const { return ___skybox_23; }
	inline Material_t193706927 ** get_address_of_skybox_23() { return &___skybox_23; }
	inline void set_skybox_23(Material_t193706927 * value)
	{
		___skybox_23 = value;
		Il2CppCodeGenWriteBarrier(&___skybox_23, value);
	}

	inline static int32_t get_offset_of_skyUpColor_24() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___skyUpColor_24)); }
	inline Color_t2020392075  get_skyUpColor_24() const { return ___skyUpColor_24; }
	inline Color_t2020392075 * get_address_of_skyUpColor_24() { return &___skyUpColor_24; }
	inline void set_skyUpColor_24(Color_t2020392075  value)
	{
		___skyUpColor_24 = value;
	}

	inline static int32_t get_offset_of_skyDwColor_25() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___skyDwColor_25)); }
	inline Color_t2020392075  get_skyDwColor_25() const { return ___skyDwColor_25; }
	inline Color_t2020392075 * get_address_of_skyDwColor_25() { return &___skyDwColor_25; }
	inline void set_skyDwColor_25(Color_t2020392075  value)
	{
		___skyDwColor_25 = value;
	}

	inline static int32_t get_offset_of_skyGradientStart_26() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___skyGradientStart_26)); }
	inline float get_skyGradientStart_26() const { return ___skyGradientStart_26; }
	inline float* get_address_of_skyGradientStart_26() { return &___skyGradientStart_26; }
	inline void set_skyGradientStart_26(float value)
	{
		___skyGradientStart_26 = value;
	}

	inline static int32_t get_offset_of_skyGradientRange_27() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___skyGradientRange_27)); }
	inline float get_skyGradientRange_27() const { return ___skyGradientRange_27; }
	inline float* get_address_of_skyGradientRange_27() { return &___skyGradientRange_27; }
	inline void set_skyGradientRange_27(float value)
	{
		___skyGradientRange_27 = value;
	}

	inline static int32_t get_offset_of_skyGradientBloom_28() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___skyGradientBloom_28)); }
	inline float get_skyGradientBloom_28() const { return ___skyGradientBloom_28; }
	inline float* get_address_of_skyGradientBloom_28() { return &___skyGradientBloom_28; }
	inline void set_skyGradientBloom_28(float value)
	{
		___skyGradientBloom_28 = value;
	}

	inline static int32_t get_offset_of_softFocus_29() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___softFocus_29)); }
	inline bool get_softFocus_29() const { return ___softFocus_29; }
	inline bool* get_address_of_softFocus_29() { return &___softFocus_29; }
	inline void set_softFocus_29(bool value)
	{
		___softFocus_29 = value;
	}

	inline static int32_t get_offset_of_bufferDownPercent_30() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___bufferDownPercent_30)); }
	inline float get_bufferDownPercent_30() const { return ___bufferDownPercent_30; }
	inline float* get_address_of_bufferDownPercent_30() { return &___bufferDownPercent_30; }
	inline void set_bufferDownPercent_30(float value)
	{
		___bufferDownPercent_30 = value;
	}

	inline static int32_t get_offset_of_softBlurSize_31() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___softBlurSize_31)); }
	inline float get_softBlurSize_31() const { return ___softBlurSize_31; }
	inline float* get_address_of_softBlurSize_31() { return &___softBlurSize_31; }
	inline void set_softBlurSize_31(float value)
	{
		___softBlurSize_31 = value;
	}

	inline static int32_t get_offset_of_bloom_32() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___bloom_32)); }
	inline bool get_bloom_32() const { return ___bloom_32; }
	inline bool* get_address_of_bloom_32() { return &___bloom_32; }
	inline void set_bloom_32(bool value)
	{
		___bloom_32 = value;
	}

	inline static int32_t get_offset_of_bloomBlurSize_33() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___bloomBlurSize_33)); }
	inline float get_bloomBlurSize_33() const { return ___bloomBlurSize_33; }
	inline float* get_address_of_bloomBlurSize_33() { return &___bloomBlurSize_33; }
	inline void set_bloomBlurSize_33(float value)
	{
		___bloomBlurSize_33 = value;
	}

	inline static int32_t get_offset_of_bloomThreshold_34() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___bloomThreshold_34)); }
	inline float get_bloomThreshold_34() const { return ___bloomThreshold_34; }
	inline float* get_address_of_bloomThreshold_34() { return &___bloomThreshold_34; }
	inline void set_bloomThreshold_34(float value)
	{
		___bloomThreshold_34 = value;
	}

	inline static int32_t get_offset_of_bloomIntensity_35() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___bloomIntensity_35)); }
	inline float get_bloomIntensity_35() const { return ___bloomIntensity_35; }
	inline float* get_address_of_bloomIntensity_35() { return &___bloomIntensity_35; }
	inline void set_bloomIntensity_35(float value)
	{
		___bloomIntensity_35 = value;
	}

	inline static int32_t get_offset_of_colorGrading_36() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___colorGrading_36)); }
	inline bool get_colorGrading_36() const { return ___colorGrading_36; }
	inline bool* get_address_of_colorGrading_36() { return &___colorGrading_36; }
	inline void set_colorGrading_36(bool value)
	{
		___colorGrading_36 = value;
	}

	inline static int32_t get_offset_of_colorGradingLerpAmount_37() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___colorGradingLerpAmount_37)); }
	inline float get_colorGradingLerpAmount_37() const { return ___colorGradingLerpAmount_37; }
	inline float* get_address_of_colorGradingLerpAmount_37() { return &___colorGradingLerpAmount_37; }
	inline void set_colorGradingLerpAmount_37(float value)
	{
		___colorGradingLerpAmount_37 = value;
	}

	inline static int32_t get_offset_of_colorGradingStartLUT_38() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___colorGradingStartLUT_38)); }
	inline Texture_t2243626319 * get_colorGradingStartLUT_38() const { return ___colorGradingStartLUT_38; }
	inline Texture_t2243626319 ** get_address_of_colorGradingStartLUT_38() { return &___colorGradingStartLUT_38; }
	inline void set_colorGradingStartLUT_38(Texture_t2243626319 * value)
	{
		___colorGradingStartLUT_38 = value;
		Il2CppCodeGenWriteBarrier(&___colorGradingStartLUT_38, value);
	}

	inline static int32_t get_offset_of_colorGradingEndLUT_39() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___colorGradingEndLUT_39)); }
	inline Texture_t2243626319 * get_colorGradingEndLUT_39() const { return ___colorGradingEndLUT_39; }
	inline Texture_t2243626319 ** get_address_of_colorGradingEndLUT_39() { return &___colorGradingEndLUT_39; }
	inline void set_colorGradingEndLUT_39(Texture_t2243626319 * value)
	{
		___colorGradingEndLUT_39 = value;
		Il2CppCodeGenWriteBarrier(&___colorGradingEndLUT_39, value);
	}

	inline static int32_t get_offset_of_antiAlias_40() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___antiAlias_40)); }
	inline bool get_antiAlias_40() const { return ___antiAlias_40; }
	inline bool* get_address_of_antiAlias_40() { return &___antiAlias_40; }
	inline void set_antiAlias_40(bool value)
	{
		___antiAlias_40 = value;
	}

	inline static int32_t get_offset_of_colorGradingMaterial_41() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___colorGradingMaterial_41)); }
	inline Material_t193706927 * get_colorGradingMaterial_41() const { return ___colorGradingMaterial_41; }
	inline Material_t193706927 ** get_address_of_colorGradingMaterial_41() { return &___colorGradingMaterial_41; }
	inline void set_colorGradingMaterial_41(Material_t193706927 * value)
	{
		___colorGradingMaterial_41 = value;
		Il2CppCodeGenWriteBarrier(&___colorGradingMaterial_41, value);
	}

	inline static int32_t get_offset_of_gaussianBlurMaterial_42() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___gaussianBlurMaterial_42)); }
	inline Material_t193706927 * get_gaussianBlurMaterial_42() const { return ___gaussianBlurMaterial_42; }
	inline Material_t193706927 ** get_address_of_gaussianBlurMaterial_42() { return &___gaussianBlurMaterial_42; }
	inline void set_gaussianBlurMaterial_42(Material_t193706927 * value)
	{
		___gaussianBlurMaterial_42 = value;
		Il2CppCodeGenWriteBarrier(&___gaussianBlurMaterial_42, value);
	}

	inline static int32_t get_offset_of_camera__43() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___camera__43)); }
	inline Camera_t189460977 * get_camera__43() const { return ___camera__43; }
	inline Camera_t189460977 ** get_address_of_camera__43() { return &___camera__43; }
	inline void set_camera__43(Camera_t189460977 * value)
	{
		___camera__43 = value;
		Il2CppCodeGenWriteBarrier(&___camera__43, value);
	}

	inline static int32_t get_offset_of_dirLightCtrl_44() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___dirLightCtrl_44)); }
	inline FSDirLight_t1852219918 * get_dirLightCtrl_44() const { return ___dirLightCtrl_44; }
	inline FSDirLight_t1852219918 ** get_address_of_dirLightCtrl_44() { return &___dirLightCtrl_44; }
	inline void set_dirLightCtrl_44(FSDirLight_t1852219918 * value)
	{
		___dirLightCtrl_44 = value;
		Il2CppCodeGenWriteBarrier(&___dirLightCtrl_44, value);
	}

	inline static int32_t get_offset_of_spotLightCtrl_45() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___spotLightCtrl_45)); }
	inline FSSpotLight_t2693339535 * get_spotLightCtrl_45() const { return ___spotLightCtrl_45; }
	inline FSSpotLight_t2693339535 ** get_address_of_spotLightCtrl_45() { return &___spotLightCtrl_45; }
	inline void set_spotLightCtrl_45(FSSpotLight_t2693339535 * value)
	{
		___spotLightCtrl_45 = value;
		Il2CppCodeGenWriteBarrier(&___spotLightCtrl_45, value);
	}

	inline static int32_t get_offset_of_planetCtrl_46() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___planetCtrl_46)); }
	inline FSPlanet_t2551900013 * get_planetCtrl_46() const { return ___planetCtrl_46; }
	inline FSPlanet_t2551900013 ** get_address_of_planetCtrl_46() { return &___planetCtrl_46; }
	inline void set_planetCtrl_46(FSPlanet_t2551900013 * value)
	{
		___planetCtrl_46 = value;
		Il2CppCodeGenWriteBarrier(&___planetCtrl_46, value);
	}

	inline static int32_t get_offset_of_dirLightTrans_47() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___dirLightTrans_47)); }
	inline Transform_t3275118058 * get_dirLightTrans_47() const { return ___dirLightTrans_47; }
	inline Transform_t3275118058 ** get_address_of_dirLightTrans_47() { return &___dirLightTrans_47; }
	inline void set_dirLightTrans_47(Transform_t3275118058 * value)
	{
		___dirLightTrans_47 = value;
		Il2CppCodeGenWriteBarrier(&___dirLightTrans_47, value);
	}

	inline static int32_t get_offset_of_dirLightRot_48() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___dirLightRot_48)); }
	inline Quaternion_t4030073918  get_dirLightRot_48() const { return ___dirLightRot_48; }
	inline Quaternion_t4030073918 * get_address_of_dirLightRot_48() { return &___dirLightRot_48; }
	inline void set_dirLightRot_48(Quaternion_t4030073918  value)
	{
		___dirLightRot_48 = value;
	}

	inline static int32_t get_offset_of_backup_49() { return static_cast<int32_t>(offsetof(FSEffect_t3187928436, ___backup_49)); }
	inline ParamBackup_t1404423237  get_backup_49() const { return ___backup_49; }
	inline ParamBackup_t1404423237 * get_address_of_backup_49() { return &___backup_49; }
	inline void set_backup_49(ParamBackup_t1404423237  value)
	{
		___backup_49 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
