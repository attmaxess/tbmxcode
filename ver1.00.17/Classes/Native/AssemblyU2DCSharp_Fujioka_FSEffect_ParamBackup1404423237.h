﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_ShadowProjection2972094683.h"
#include "UnityEngine_UnityEngine_FogMode2386547659.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSEffect/ParamBackup
struct  ParamBackup_t1404423237 
{
public:
	// System.Single Fujioka.FSEffect/ParamBackup::shadowDistance
	float ___shadowDistance_0;
	// UnityEngine.ShadowProjection Fujioka.FSEffect/ParamBackup::shadowProjection
	int32_t ___shadowProjection_1;
	// UnityEngine.FogMode Fujioka.FSEffect/ParamBackup::fogMode
	int32_t ___fogMode_2;
	// UnityEngine.Material Fujioka.FSEffect/ParamBackup::skyboxMaterial
	Material_t193706927 * ___skyboxMaterial_3;
	// UnityEngine.CameraClearFlags Fujioka.FSEffect/ParamBackup::clearFlags
	int32_t ___clearFlags_4;

public:
	inline static int32_t get_offset_of_shadowDistance_0() { return static_cast<int32_t>(offsetof(ParamBackup_t1404423237, ___shadowDistance_0)); }
	inline float get_shadowDistance_0() const { return ___shadowDistance_0; }
	inline float* get_address_of_shadowDistance_0() { return &___shadowDistance_0; }
	inline void set_shadowDistance_0(float value)
	{
		___shadowDistance_0 = value;
	}

	inline static int32_t get_offset_of_shadowProjection_1() { return static_cast<int32_t>(offsetof(ParamBackup_t1404423237, ___shadowProjection_1)); }
	inline int32_t get_shadowProjection_1() const { return ___shadowProjection_1; }
	inline int32_t* get_address_of_shadowProjection_1() { return &___shadowProjection_1; }
	inline void set_shadowProjection_1(int32_t value)
	{
		___shadowProjection_1 = value;
	}

	inline static int32_t get_offset_of_fogMode_2() { return static_cast<int32_t>(offsetof(ParamBackup_t1404423237, ___fogMode_2)); }
	inline int32_t get_fogMode_2() const { return ___fogMode_2; }
	inline int32_t* get_address_of_fogMode_2() { return &___fogMode_2; }
	inline void set_fogMode_2(int32_t value)
	{
		___fogMode_2 = value;
	}

	inline static int32_t get_offset_of_skyboxMaterial_3() { return static_cast<int32_t>(offsetof(ParamBackup_t1404423237, ___skyboxMaterial_3)); }
	inline Material_t193706927 * get_skyboxMaterial_3() const { return ___skyboxMaterial_3; }
	inline Material_t193706927 ** get_address_of_skyboxMaterial_3() { return &___skyboxMaterial_3; }
	inline void set_skyboxMaterial_3(Material_t193706927 * value)
	{
		___skyboxMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___skyboxMaterial_3, value);
	}

	inline static int32_t get_offset_of_clearFlags_4() { return static_cast<int32_t>(offsetof(ParamBackup_t1404423237, ___clearFlags_4)); }
	inline int32_t get_clearFlags_4() const { return ___clearFlags_4; }
	inline int32_t* get_address_of_clearFlags_4() { return &___clearFlags_4; }
	inline void set_clearFlags_4(int32_t value)
	{
		___clearFlags_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Fujioka.FSEffect/ParamBackup
struct ParamBackup_t1404423237_marshaled_pinvoke
{
	float ___shadowDistance_0;
	int32_t ___shadowProjection_1;
	int32_t ___fogMode_2;
	Material_t193706927 * ___skyboxMaterial_3;
	int32_t ___clearFlags_4;
};
// Native definition for COM marshalling of Fujioka.FSEffect/ParamBackup
struct ParamBackup_t1404423237_marshaled_com
{
	float ___shadowDistance_0;
	int32_t ___shadowProjection_1;
	int32_t ___fogMode_2;
	Material_t193706927 * ___skyboxMaterial_3;
	int32_t ___clearFlags_4;
};
