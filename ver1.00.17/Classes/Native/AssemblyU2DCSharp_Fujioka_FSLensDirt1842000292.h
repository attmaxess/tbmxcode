﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSLensDirt
struct  FSLensDirt_t1842000292  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Fujioka.FSLensDirt::SHADER_PROPERTY_PARAM
	int32_t ___SHADER_PROPERTY_PARAM_2;
	// System.Int32 Fujioka.FSLensDirt::SHADER_PROPERTY_MAINTEX
	int32_t ___SHADER_PROPERTY_MAINTEX_3;
	// UnityEngine.Camera Fujioka.FSLensDirt::cam
	Camera_t189460977 * ___cam_4;
	// UnityEngine.GameObject Fujioka.FSLensDirt::go_
	GameObject_t1756533147 * ___go__5;
	// UnityEngine.Transform Fujioka.FSLensDirt::trans_
	Transform_t3275118058 * ___trans__6;
	// UnityEngine.Material Fujioka.FSLensDirt::material
	Material_t193706927 * ___material_7;

public:
	inline static int32_t get_offset_of_SHADER_PROPERTY_PARAM_2() { return static_cast<int32_t>(offsetof(FSLensDirt_t1842000292, ___SHADER_PROPERTY_PARAM_2)); }
	inline int32_t get_SHADER_PROPERTY_PARAM_2() const { return ___SHADER_PROPERTY_PARAM_2; }
	inline int32_t* get_address_of_SHADER_PROPERTY_PARAM_2() { return &___SHADER_PROPERTY_PARAM_2; }
	inline void set_SHADER_PROPERTY_PARAM_2(int32_t value)
	{
		___SHADER_PROPERTY_PARAM_2 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_MAINTEX_3() { return static_cast<int32_t>(offsetof(FSLensDirt_t1842000292, ___SHADER_PROPERTY_MAINTEX_3)); }
	inline int32_t get_SHADER_PROPERTY_MAINTEX_3() const { return ___SHADER_PROPERTY_MAINTEX_3; }
	inline int32_t* get_address_of_SHADER_PROPERTY_MAINTEX_3() { return &___SHADER_PROPERTY_MAINTEX_3; }
	inline void set_SHADER_PROPERTY_MAINTEX_3(int32_t value)
	{
		___SHADER_PROPERTY_MAINTEX_3 = value;
	}

	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(FSLensDirt_t1842000292, ___cam_4)); }
	inline Camera_t189460977 * get_cam_4() const { return ___cam_4; }
	inline Camera_t189460977 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Camera_t189460977 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier(&___cam_4, value);
	}

	inline static int32_t get_offset_of_go__5() { return static_cast<int32_t>(offsetof(FSLensDirt_t1842000292, ___go__5)); }
	inline GameObject_t1756533147 * get_go__5() const { return ___go__5; }
	inline GameObject_t1756533147 ** get_address_of_go__5() { return &___go__5; }
	inline void set_go__5(GameObject_t1756533147 * value)
	{
		___go__5 = value;
		Il2CppCodeGenWriteBarrier(&___go__5, value);
	}

	inline static int32_t get_offset_of_trans__6() { return static_cast<int32_t>(offsetof(FSLensDirt_t1842000292, ___trans__6)); }
	inline Transform_t3275118058 * get_trans__6() const { return ___trans__6; }
	inline Transform_t3275118058 ** get_address_of_trans__6() { return &___trans__6; }
	inline void set_trans__6(Transform_t3275118058 * value)
	{
		___trans__6 = value;
		Il2CppCodeGenWriteBarrier(&___trans__6, value);
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(FSLensDirt_t1842000292, ___material_7)); }
	inline Material_t193706927 * get_material_7() const { return ___material_7; }
	inline Material_t193706927 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t193706927 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier(&___material_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
