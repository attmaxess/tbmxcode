﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Fujioka.FSDirLight
struct FSDirLight_t1852219918;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSPlanet
struct  FSPlanet_t2551900013  : public MonoBehaviour_t1158329972
{
public:
	// Fujioka.FSDirLight Fujioka.FSPlanet::dirLight
	FSDirLight_t1852219918 * ___dirLight_2;

public:
	inline static int32_t get_offset_of_dirLight_2() { return static_cast<int32_t>(offsetof(FSPlanet_t2551900013, ___dirLight_2)); }
	inline FSDirLight_t1852219918 * get_dirLight_2() const { return ___dirLight_2; }
	inline FSDirLight_t1852219918 ** get_address_of_dirLight_2() { return &___dirLight_2; }
	inline void set_dirLight_2(FSDirLight_t1852219918 * value)
	{
		___dirLight_2 = value;
		Il2CppCodeGenWriteBarrier(&___dirLight_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
