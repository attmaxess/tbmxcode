﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.MaterialPropertyBlock[]
struct MaterialPropertyBlockU5BU5D_t2769163696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSSkyDome
struct  FSSkyDome_t3093560541  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_STARTTEX
	int32_t ___SHADER_PROPERTY_STARTTEX_2;
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_ENDTEX
	int32_t ___SHADER_PROPERTY_ENDTEX_3;
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_LERPAMOUNT
	int32_t ___SHADER_PROPERTY_LERPAMOUNT_4;
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_ROTATEMATRIX
	int32_t ___SHADER_PROPERTY_ROTATEMATRIX_5;
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_SCALEMATRIX
	int32_t ___SHADER_PROPERTY_SCALEMATRIX_6;
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_MAINCOLOR
	int32_t ___SHADER_PROPERTY_MAINCOLOR_7;
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_GRADCOLOR
	int32_t ___SHADER_PROPERTY_GRADCOLOR_8;
	// System.Int32 Fujioka.FSSkyDome::SHADER_PROPERTY_GRADIENT
	int32_t ___SHADER_PROPERTY_GRADIENT_9;
	// UnityEngine.GameObject Fujioka.FSSkyDome::go_
	GameObject_t1756533147 * ___go__10;
	// UnityEngine.Transform Fujioka.FSSkyDome::trans_
	Transform_t3275118058 * ___trans__11;
	// UnityEngine.Vector3 Fujioka.FSSkyDome::perspectiveScale
	Vector3_t2243707580  ___perspectiveScale_12;
	// System.Int32 Fujioka.FSSkyDome::shellLength
	int32_t ___shellLength_13;
	// UnityEngine.Renderer[] Fujioka.FSSkyDome::shell
	RendererU5BU5D_t2810717544* ___shell_14;
	// UnityEngine.Vector3[] Fujioka.FSSkyDome::shellAxis
	Vector3U5BU5D_t1172311765* ___shellAxis_15;
	// System.Single[] Fujioka.FSSkyDome::shellOmega
	SingleU5BU5D_t577127397* ___shellOmega_16;
	// System.Single[] Fujioka.FSSkyDome::shellAngle
	SingleU5BU5D_t577127397* ___shellAngle_17;
	// UnityEngine.MaterialPropertyBlock[] Fujioka.FSSkyDome::shellMpb
	MaterialPropertyBlockU5BU5D_t2769163696* ___shellMpb_18;

public:
	inline static int32_t get_offset_of_SHADER_PROPERTY_STARTTEX_2() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_STARTTEX_2)); }
	inline int32_t get_SHADER_PROPERTY_STARTTEX_2() const { return ___SHADER_PROPERTY_STARTTEX_2; }
	inline int32_t* get_address_of_SHADER_PROPERTY_STARTTEX_2() { return &___SHADER_PROPERTY_STARTTEX_2; }
	inline void set_SHADER_PROPERTY_STARTTEX_2(int32_t value)
	{
		___SHADER_PROPERTY_STARTTEX_2 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_ENDTEX_3() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_ENDTEX_3)); }
	inline int32_t get_SHADER_PROPERTY_ENDTEX_3() const { return ___SHADER_PROPERTY_ENDTEX_3; }
	inline int32_t* get_address_of_SHADER_PROPERTY_ENDTEX_3() { return &___SHADER_PROPERTY_ENDTEX_3; }
	inline void set_SHADER_PROPERTY_ENDTEX_3(int32_t value)
	{
		___SHADER_PROPERTY_ENDTEX_3 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_LERPAMOUNT_4() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_LERPAMOUNT_4)); }
	inline int32_t get_SHADER_PROPERTY_LERPAMOUNT_4() const { return ___SHADER_PROPERTY_LERPAMOUNT_4; }
	inline int32_t* get_address_of_SHADER_PROPERTY_LERPAMOUNT_4() { return &___SHADER_PROPERTY_LERPAMOUNT_4; }
	inline void set_SHADER_PROPERTY_LERPAMOUNT_4(int32_t value)
	{
		___SHADER_PROPERTY_LERPAMOUNT_4 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_ROTATEMATRIX_5() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_ROTATEMATRIX_5)); }
	inline int32_t get_SHADER_PROPERTY_ROTATEMATRIX_5() const { return ___SHADER_PROPERTY_ROTATEMATRIX_5; }
	inline int32_t* get_address_of_SHADER_PROPERTY_ROTATEMATRIX_5() { return &___SHADER_PROPERTY_ROTATEMATRIX_5; }
	inline void set_SHADER_PROPERTY_ROTATEMATRIX_5(int32_t value)
	{
		___SHADER_PROPERTY_ROTATEMATRIX_5 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_SCALEMATRIX_6() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_SCALEMATRIX_6)); }
	inline int32_t get_SHADER_PROPERTY_SCALEMATRIX_6() const { return ___SHADER_PROPERTY_SCALEMATRIX_6; }
	inline int32_t* get_address_of_SHADER_PROPERTY_SCALEMATRIX_6() { return &___SHADER_PROPERTY_SCALEMATRIX_6; }
	inline void set_SHADER_PROPERTY_SCALEMATRIX_6(int32_t value)
	{
		___SHADER_PROPERTY_SCALEMATRIX_6 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_MAINCOLOR_7() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_MAINCOLOR_7)); }
	inline int32_t get_SHADER_PROPERTY_MAINCOLOR_7() const { return ___SHADER_PROPERTY_MAINCOLOR_7; }
	inline int32_t* get_address_of_SHADER_PROPERTY_MAINCOLOR_7() { return &___SHADER_PROPERTY_MAINCOLOR_7; }
	inline void set_SHADER_PROPERTY_MAINCOLOR_7(int32_t value)
	{
		___SHADER_PROPERTY_MAINCOLOR_7 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_GRADCOLOR_8() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_GRADCOLOR_8)); }
	inline int32_t get_SHADER_PROPERTY_GRADCOLOR_8() const { return ___SHADER_PROPERTY_GRADCOLOR_8; }
	inline int32_t* get_address_of_SHADER_PROPERTY_GRADCOLOR_8() { return &___SHADER_PROPERTY_GRADCOLOR_8; }
	inline void set_SHADER_PROPERTY_GRADCOLOR_8(int32_t value)
	{
		___SHADER_PROPERTY_GRADCOLOR_8 = value;
	}

	inline static int32_t get_offset_of_SHADER_PROPERTY_GRADIENT_9() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___SHADER_PROPERTY_GRADIENT_9)); }
	inline int32_t get_SHADER_PROPERTY_GRADIENT_9() const { return ___SHADER_PROPERTY_GRADIENT_9; }
	inline int32_t* get_address_of_SHADER_PROPERTY_GRADIENT_9() { return &___SHADER_PROPERTY_GRADIENT_9; }
	inline void set_SHADER_PROPERTY_GRADIENT_9(int32_t value)
	{
		___SHADER_PROPERTY_GRADIENT_9 = value;
	}

	inline static int32_t get_offset_of_go__10() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___go__10)); }
	inline GameObject_t1756533147 * get_go__10() const { return ___go__10; }
	inline GameObject_t1756533147 ** get_address_of_go__10() { return &___go__10; }
	inline void set_go__10(GameObject_t1756533147 * value)
	{
		___go__10 = value;
		Il2CppCodeGenWriteBarrier(&___go__10, value);
	}

	inline static int32_t get_offset_of_trans__11() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___trans__11)); }
	inline Transform_t3275118058 * get_trans__11() const { return ___trans__11; }
	inline Transform_t3275118058 ** get_address_of_trans__11() { return &___trans__11; }
	inline void set_trans__11(Transform_t3275118058 * value)
	{
		___trans__11 = value;
		Il2CppCodeGenWriteBarrier(&___trans__11, value);
	}

	inline static int32_t get_offset_of_perspectiveScale_12() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___perspectiveScale_12)); }
	inline Vector3_t2243707580  get_perspectiveScale_12() const { return ___perspectiveScale_12; }
	inline Vector3_t2243707580 * get_address_of_perspectiveScale_12() { return &___perspectiveScale_12; }
	inline void set_perspectiveScale_12(Vector3_t2243707580  value)
	{
		___perspectiveScale_12 = value;
	}

	inline static int32_t get_offset_of_shellLength_13() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___shellLength_13)); }
	inline int32_t get_shellLength_13() const { return ___shellLength_13; }
	inline int32_t* get_address_of_shellLength_13() { return &___shellLength_13; }
	inline void set_shellLength_13(int32_t value)
	{
		___shellLength_13 = value;
	}

	inline static int32_t get_offset_of_shell_14() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___shell_14)); }
	inline RendererU5BU5D_t2810717544* get_shell_14() const { return ___shell_14; }
	inline RendererU5BU5D_t2810717544** get_address_of_shell_14() { return &___shell_14; }
	inline void set_shell_14(RendererU5BU5D_t2810717544* value)
	{
		___shell_14 = value;
		Il2CppCodeGenWriteBarrier(&___shell_14, value);
	}

	inline static int32_t get_offset_of_shellAxis_15() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___shellAxis_15)); }
	inline Vector3U5BU5D_t1172311765* get_shellAxis_15() const { return ___shellAxis_15; }
	inline Vector3U5BU5D_t1172311765** get_address_of_shellAxis_15() { return &___shellAxis_15; }
	inline void set_shellAxis_15(Vector3U5BU5D_t1172311765* value)
	{
		___shellAxis_15 = value;
		Il2CppCodeGenWriteBarrier(&___shellAxis_15, value);
	}

	inline static int32_t get_offset_of_shellOmega_16() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___shellOmega_16)); }
	inline SingleU5BU5D_t577127397* get_shellOmega_16() const { return ___shellOmega_16; }
	inline SingleU5BU5D_t577127397** get_address_of_shellOmega_16() { return &___shellOmega_16; }
	inline void set_shellOmega_16(SingleU5BU5D_t577127397* value)
	{
		___shellOmega_16 = value;
		Il2CppCodeGenWriteBarrier(&___shellOmega_16, value);
	}

	inline static int32_t get_offset_of_shellAngle_17() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___shellAngle_17)); }
	inline SingleU5BU5D_t577127397* get_shellAngle_17() const { return ___shellAngle_17; }
	inline SingleU5BU5D_t577127397** get_address_of_shellAngle_17() { return &___shellAngle_17; }
	inline void set_shellAngle_17(SingleU5BU5D_t577127397* value)
	{
		___shellAngle_17 = value;
		Il2CppCodeGenWriteBarrier(&___shellAngle_17, value);
	}

	inline static int32_t get_offset_of_shellMpb_18() { return static_cast<int32_t>(offsetof(FSSkyDome_t3093560541, ___shellMpb_18)); }
	inline MaterialPropertyBlockU5BU5D_t2769163696* get_shellMpb_18() const { return ___shellMpb_18; }
	inline MaterialPropertyBlockU5BU5D_t2769163696** get_address_of_shellMpb_18() { return &___shellMpb_18; }
	inline void set_shellMpb_18(MaterialPropertyBlockU5BU5D_t2769163696* value)
	{
		___shellMpb_18 = value;
		Il2CppCodeGenWriteBarrier(&___shellMpb_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
