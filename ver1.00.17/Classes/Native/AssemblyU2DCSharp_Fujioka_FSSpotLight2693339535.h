﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.LensFlare
struct LensFlare_t529161798;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSSpotLight
struct  FSSpotLight_t2693339535  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Fujioka.FSSpotLight::maxFarDistance
	float ___maxFarDistance_2;
	// UnityEngine.Transform Fujioka.FSSpotLight::cameraTrans
	Transform_t3275118058 * ___cameraTrans_3;
	// UnityEngine.Transform Fujioka.FSSpotLight::trans_
	Transform_t3275118058 * ___trans__4;
	// UnityEngine.Light Fujioka.FSSpotLight::spotLight
	Light_t494725636 * ___spotLight_5;
	// UnityEngine.LensFlare Fujioka.FSSpotLight::lensFlare
	LensFlare_t529161798 * ___lensFlare_6;

public:
	inline static int32_t get_offset_of_maxFarDistance_2() { return static_cast<int32_t>(offsetof(FSSpotLight_t2693339535, ___maxFarDistance_2)); }
	inline float get_maxFarDistance_2() const { return ___maxFarDistance_2; }
	inline float* get_address_of_maxFarDistance_2() { return &___maxFarDistance_2; }
	inline void set_maxFarDistance_2(float value)
	{
		___maxFarDistance_2 = value;
	}

	inline static int32_t get_offset_of_cameraTrans_3() { return static_cast<int32_t>(offsetof(FSSpotLight_t2693339535, ___cameraTrans_3)); }
	inline Transform_t3275118058 * get_cameraTrans_3() const { return ___cameraTrans_3; }
	inline Transform_t3275118058 ** get_address_of_cameraTrans_3() { return &___cameraTrans_3; }
	inline void set_cameraTrans_3(Transform_t3275118058 * value)
	{
		___cameraTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___cameraTrans_3, value);
	}

	inline static int32_t get_offset_of_trans__4() { return static_cast<int32_t>(offsetof(FSSpotLight_t2693339535, ___trans__4)); }
	inline Transform_t3275118058 * get_trans__4() const { return ___trans__4; }
	inline Transform_t3275118058 ** get_address_of_trans__4() { return &___trans__4; }
	inline void set_trans__4(Transform_t3275118058 * value)
	{
		___trans__4 = value;
		Il2CppCodeGenWriteBarrier(&___trans__4, value);
	}

	inline static int32_t get_offset_of_spotLight_5() { return static_cast<int32_t>(offsetof(FSSpotLight_t2693339535, ___spotLight_5)); }
	inline Light_t494725636 * get_spotLight_5() const { return ___spotLight_5; }
	inline Light_t494725636 ** get_address_of_spotLight_5() { return &___spotLight_5; }
	inline void set_spotLight_5(Light_t494725636 * value)
	{
		___spotLight_5 = value;
		Il2CppCodeGenWriteBarrier(&___spotLight_5, value);
	}

	inline static int32_t get_offset_of_lensFlare_6() { return static_cast<int32_t>(offsetof(FSSpotLight_t2693339535, ___lensFlare_6)); }
	inline LensFlare_t529161798 * get_lensFlare_6() const { return ___lensFlare_6; }
	inline LensFlare_t529161798 ** get_address_of_lensFlare_6() { return &___lensFlare_6; }
	inline void set_lensFlare_6(LensFlare_t529161798 * value)
	{
		___lensFlare_6 = value;
		Il2CppCodeGenWriteBarrier(&___lensFlare_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
