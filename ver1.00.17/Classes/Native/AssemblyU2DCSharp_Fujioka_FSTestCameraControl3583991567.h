﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// Fujioka.FSEffect
struct FSEffect_t3187928436;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fujioka.FSTestCameraControl
struct  FSTestCameraControl_t3583991567  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Fujioka.FSTestCameraControl::trans_
	Transform_t3275118058 * ___trans__3;
	// UnityEngine.Rigidbody Fujioka.FSTestCameraControl::rigid
	Rigidbody_t4233889191 * ___rigid_4;
	// UnityEngine.Vector3 Fujioka.FSTestCameraControl::targetUp
	Vector3_t2243707580  ___targetUp_5;
	// UnityEngine.Vector3 Fujioka.FSTestCameraControl::upVelocity
	Vector3_t2243707580  ___upVelocity_6;
	// UnityEngine.Vector3 Fujioka.FSTestCameraControl::rootUpVelocity
	Vector3_t2243707580  ___rootUpVelocity_7;
	// UnityEngine.Transform Fujioka.FSTestCameraControl::root
	Transform_t3275118058 * ___root_8;
	// UnityEngine.Transform Fujioka.FSTestCameraControl::camTrans
	Transform_t3275118058 * ___camTrans_9;
	// UnityEngine.Vector2 Fujioka.FSTestCameraControl::camRot
	Vector2_t2243707579  ___camRot_10;
	// UnityEngine.Quaternion Fujioka.FSTestCameraControl::defaultCamRot
	Quaternion_t4030073918  ___defaultCamRot_11;
	// UnityEngine.Vector3 Fujioka.FSTestCameraControl::defaultCamPosition
	Vector3_t2243707580  ___defaultCamPosition_12;
	// Fujioka.FSEffect Fujioka.FSTestCameraControl::fsEffect
	FSEffect_t3187928436 * ___fsEffect_13;

public:
	inline static int32_t get_offset_of_trans__3() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___trans__3)); }
	inline Transform_t3275118058 * get_trans__3() const { return ___trans__3; }
	inline Transform_t3275118058 ** get_address_of_trans__3() { return &___trans__3; }
	inline void set_trans__3(Transform_t3275118058 * value)
	{
		___trans__3 = value;
		Il2CppCodeGenWriteBarrier(&___trans__3, value);
	}

	inline static int32_t get_offset_of_rigid_4() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___rigid_4)); }
	inline Rigidbody_t4233889191 * get_rigid_4() const { return ___rigid_4; }
	inline Rigidbody_t4233889191 ** get_address_of_rigid_4() { return &___rigid_4; }
	inline void set_rigid_4(Rigidbody_t4233889191 * value)
	{
		___rigid_4 = value;
		Il2CppCodeGenWriteBarrier(&___rigid_4, value);
	}

	inline static int32_t get_offset_of_targetUp_5() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___targetUp_5)); }
	inline Vector3_t2243707580  get_targetUp_5() const { return ___targetUp_5; }
	inline Vector3_t2243707580 * get_address_of_targetUp_5() { return &___targetUp_5; }
	inline void set_targetUp_5(Vector3_t2243707580  value)
	{
		___targetUp_5 = value;
	}

	inline static int32_t get_offset_of_upVelocity_6() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___upVelocity_6)); }
	inline Vector3_t2243707580  get_upVelocity_6() const { return ___upVelocity_6; }
	inline Vector3_t2243707580 * get_address_of_upVelocity_6() { return &___upVelocity_6; }
	inline void set_upVelocity_6(Vector3_t2243707580  value)
	{
		___upVelocity_6 = value;
	}

	inline static int32_t get_offset_of_rootUpVelocity_7() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___rootUpVelocity_7)); }
	inline Vector3_t2243707580  get_rootUpVelocity_7() const { return ___rootUpVelocity_7; }
	inline Vector3_t2243707580 * get_address_of_rootUpVelocity_7() { return &___rootUpVelocity_7; }
	inline void set_rootUpVelocity_7(Vector3_t2243707580  value)
	{
		___rootUpVelocity_7 = value;
	}

	inline static int32_t get_offset_of_root_8() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___root_8)); }
	inline Transform_t3275118058 * get_root_8() const { return ___root_8; }
	inline Transform_t3275118058 ** get_address_of_root_8() { return &___root_8; }
	inline void set_root_8(Transform_t3275118058 * value)
	{
		___root_8 = value;
		Il2CppCodeGenWriteBarrier(&___root_8, value);
	}

	inline static int32_t get_offset_of_camTrans_9() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___camTrans_9)); }
	inline Transform_t3275118058 * get_camTrans_9() const { return ___camTrans_9; }
	inline Transform_t3275118058 ** get_address_of_camTrans_9() { return &___camTrans_9; }
	inline void set_camTrans_9(Transform_t3275118058 * value)
	{
		___camTrans_9 = value;
		Il2CppCodeGenWriteBarrier(&___camTrans_9, value);
	}

	inline static int32_t get_offset_of_camRot_10() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___camRot_10)); }
	inline Vector2_t2243707579  get_camRot_10() const { return ___camRot_10; }
	inline Vector2_t2243707579 * get_address_of_camRot_10() { return &___camRot_10; }
	inline void set_camRot_10(Vector2_t2243707579  value)
	{
		___camRot_10 = value;
	}

	inline static int32_t get_offset_of_defaultCamRot_11() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___defaultCamRot_11)); }
	inline Quaternion_t4030073918  get_defaultCamRot_11() const { return ___defaultCamRot_11; }
	inline Quaternion_t4030073918 * get_address_of_defaultCamRot_11() { return &___defaultCamRot_11; }
	inline void set_defaultCamRot_11(Quaternion_t4030073918  value)
	{
		___defaultCamRot_11 = value;
	}

	inline static int32_t get_offset_of_defaultCamPosition_12() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___defaultCamPosition_12)); }
	inline Vector3_t2243707580  get_defaultCamPosition_12() const { return ___defaultCamPosition_12; }
	inline Vector3_t2243707580 * get_address_of_defaultCamPosition_12() { return &___defaultCamPosition_12; }
	inline void set_defaultCamPosition_12(Vector3_t2243707580  value)
	{
		___defaultCamPosition_12 = value;
	}

	inline static int32_t get_offset_of_fsEffect_13() { return static_cast<int32_t>(offsetof(FSTestCameraControl_t3583991567, ___fsEffect_13)); }
	inline FSEffect_t3187928436 * get_fsEffect_13() const { return ___fsEffect_13; }
	inline FSEffect_t3187928436 ** get_address_of_fsEffect_13() { return &___fsEffect_13; }
	inline void set_fsEffect_13(FSEffect_t3187928436 * value)
	{
		___fsEffect_13 = value;
		Il2CppCodeGenWriteBarrier(&___fsEffect_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
