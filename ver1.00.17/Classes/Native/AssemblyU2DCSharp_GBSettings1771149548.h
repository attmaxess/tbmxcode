﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_GBSettings_RaycastMode1488478401.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GBSettings
struct  GBSettings_t1771149548  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GBSettings::brushSize
	float ___brushSize_2;
	// System.Boolean GBSettings::fireFromCamera
	bool ___fireFromCamera_3;
	// System.Single GBSettings::minScale
	float ___minScale_4;
	// System.Single GBSettings::maxScale
	float ___maxScale_5;
	// System.Single GBSettings::spacing
	float ___spacing_6;
	// System.Boolean GBSettings::preventOverlap
	bool ___preventOverlap_7;
	// System.Single GBSettings::yOffset
	float ___yOffset_8;
	// System.Boolean GBSettings::alignToNormal
	bool ___alignToNormal_9;
	// System.Boolean GBSettings::randomRotX
	bool ___randomRotX_10;
	// System.Boolean GBSettings::randomRotY
	bool ___randomRotY_11;
	// System.Boolean GBSettings::randomRotZ
	bool ___randomRotZ_12;
	// System.Boolean GBSettings::brushActive
	bool ___brushActive_13;
	// UnityEngine.Vector3 GBSettings::randomRotation
	Vector3_t2243707580  ___randomRotation_14;
	// System.Boolean GBSettings::delete
	bool ___delete_15;
	// GBSettings/RaycastMode GBSettings::raycastMode
	int32_t ___raycastMode_16;
	// UnityEngine.GameObject GBSettings::parentObject
	GameObject_t1756533147 * ___parentObject_17;
	// System.Single GBSettings::minBrushSize
	float ___minBrushSize_18;
	// System.Single GBSettings::maxBrushSize
	float ___maxBrushSize_19;
	// System.Single GBSettings::minMinScale
	float ___minMinScale_20;
	// System.Single GBSettings::maxMinScale
	float ___maxMinScale_21;
	// System.Single GBSettings::minMaxScale
	float ___minMaxScale_22;
	// System.Single GBSettings::maxMaxScale
	float ___maxMaxScale_23;
	// System.Single GBSettings::minYOffset
	float ___minYOffset_24;
	// System.Single GBSettings::maxYOffset
	float ___maxYOffset_25;
	// System.Single GBSettings::minSpacing
	float ___minSpacing_26;
	// System.Single GBSettings::maxSpacing
	float ___maxSpacing_27;
	// UnityEngine.Vector3 GBSettings::gizmoPos
	Vector3_t2243707580  ___gizmoPos_28;
	// System.Boolean GBSettings::gizmoActive
	bool ___gizmoActive_29;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GBSettings::activeGeometry
	List_1_t1125654279 * ___activeGeometry_30;
	// System.Collections.Generic.List`1<System.String> GBSettings::activeGeometryPaths
	List_1_t1398341365 * ___activeGeometryPaths_31;

public:
	inline static int32_t get_offset_of_brushSize_2() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___brushSize_2)); }
	inline float get_brushSize_2() const { return ___brushSize_2; }
	inline float* get_address_of_brushSize_2() { return &___brushSize_2; }
	inline void set_brushSize_2(float value)
	{
		___brushSize_2 = value;
	}

	inline static int32_t get_offset_of_fireFromCamera_3() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___fireFromCamera_3)); }
	inline bool get_fireFromCamera_3() const { return ___fireFromCamera_3; }
	inline bool* get_address_of_fireFromCamera_3() { return &___fireFromCamera_3; }
	inline void set_fireFromCamera_3(bool value)
	{
		___fireFromCamera_3 = value;
	}

	inline static int32_t get_offset_of_minScale_4() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___minScale_4)); }
	inline float get_minScale_4() const { return ___minScale_4; }
	inline float* get_address_of_minScale_4() { return &___minScale_4; }
	inline void set_minScale_4(float value)
	{
		___minScale_4 = value;
	}

	inline static int32_t get_offset_of_maxScale_5() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___maxScale_5)); }
	inline float get_maxScale_5() const { return ___maxScale_5; }
	inline float* get_address_of_maxScale_5() { return &___maxScale_5; }
	inline void set_maxScale_5(float value)
	{
		___maxScale_5 = value;
	}

	inline static int32_t get_offset_of_spacing_6() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___spacing_6)); }
	inline float get_spacing_6() const { return ___spacing_6; }
	inline float* get_address_of_spacing_6() { return &___spacing_6; }
	inline void set_spacing_6(float value)
	{
		___spacing_6 = value;
	}

	inline static int32_t get_offset_of_preventOverlap_7() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___preventOverlap_7)); }
	inline bool get_preventOverlap_7() const { return ___preventOverlap_7; }
	inline bool* get_address_of_preventOverlap_7() { return &___preventOverlap_7; }
	inline void set_preventOverlap_7(bool value)
	{
		___preventOverlap_7 = value;
	}

	inline static int32_t get_offset_of_yOffset_8() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___yOffset_8)); }
	inline float get_yOffset_8() const { return ___yOffset_8; }
	inline float* get_address_of_yOffset_8() { return &___yOffset_8; }
	inline void set_yOffset_8(float value)
	{
		___yOffset_8 = value;
	}

	inline static int32_t get_offset_of_alignToNormal_9() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___alignToNormal_9)); }
	inline bool get_alignToNormal_9() const { return ___alignToNormal_9; }
	inline bool* get_address_of_alignToNormal_9() { return &___alignToNormal_9; }
	inline void set_alignToNormal_9(bool value)
	{
		___alignToNormal_9 = value;
	}

	inline static int32_t get_offset_of_randomRotX_10() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___randomRotX_10)); }
	inline bool get_randomRotX_10() const { return ___randomRotX_10; }
	inline bool* get_address_of_randomRotX_10() { return &___randomRotX_10; }
	inline void set_randomRotX_10(bool value)
	{
		___randomRotX_10 = value;
	}

	inline static int32_t get_offset_of_randomRotY_11() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___randomRotY_11)); }
	inline bool get_randomRotY_11() const { return ___randomRotY_11; }
	inline bool* get_address_of_randomRotY_11() { return &___randomRotY_11; }
	inline void set_randomRotY_11(bool value)
	{
		___randomRotY_11 = value;
	}

	inline static int32_t get_offset_of_randomRotZ_12() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___randomRotZ_12)); }
	inline bool get_randomRotZ_12() const { return ___randomRotZ_12; }
	inline bool* get_address_of_randomRotZ_12() { return &___randomRotZ_12; }
	inline void set_randomRotZ_12(bool value)
	{
		___randomRotZ_12 = value;
	}

	inline static int32_t get_offset_of_brushActive_13() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___brushActive_13)); }
	inline bool get_brushActive_13() const { return ___brushActive_13; }
	inline bool* get_address_of_brushActive_13() { return &___brushActive_13; }
	inline void set_brushActive_13(bool value)
	{
		___brushActive_13 = value;
	}

	inline static int32_t get_offset_of_randomRotation_14() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___randomRotation_14)); }
	inline Vector3_t2243707580  get_randomRotation_14() const { return ___randomRotation_14; }
	inline Vector3_t2243707580 * get_address_of_randomRotation_14() { return &___randomRotation_14; }
	inline void set_randomRotation_14(Vector3_t2243707580  value)
	{
		___randomRotation_14 = value;
	}

	inline static int32_t get_offset_of_delete_15() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___delete_15)); }
	inline bool get_delete_15() const { return ___delete_15; }
	inline bool* get_address_of_delete_15() { return &___delete_15; }
	inline void set_delete_15(bool value)
	{
		___delete_15 = value;
	}

	inline static int32_t get_offset_of_raycastMode_16() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___raycastMode_16)); }
	inline int32_t get_raycastMode_16() const { return ___raycastMode_16; }
	inline int32_t* get_address_of_raycastMode_16() { return &___raycastMode_16; }
	inline void set_raycastMode_16(int32_t value)
	{
		___raycastMode_16 = value;
	}

	inline static int32_t get_offset_of_parentObject_17() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___parentObject_17)); }
	inline GameObject_t1756533147 * get_parentObject_17() const { return ___parentObject_17; }
	inline GameObject_t1756533147 ** get_address_of_parentObject_17() { return &___parentObject_17; }
	inline void set_parentObject_17(GameObject_t1756533147 * value)
	{
		___parentObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___parentObject_17, value);
	}

	inline static int32_t get_offset_of_minBrushSize_18() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___minBrushSize_18)); }
	inline float get_minBrushSize_18() const { return ___minBrushSize_18; }
	inline float* get_address_of_minBrushSize_18() { return &___minBrushSize_18; }
	inline void set_minBrushSize_18(float value)
	{
		___minBrushSize_18 = value;
	}

	inline static int32_t get_offset_of_maxBrushSize_19() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___maxBrushSize_19)); }
	inline float get_maxBrushSize_19() const { return ___maxBrushSize_19; }
	inline float* get_address_of_maxBrushSize_19() { return &___maxBrushSize_19; }
	inline void set_maxBrushSize_19(float value)
	{
		___maxBrushSize_19 = value;
	}

	inline static int32_t get_offset_of_minMinScale_20() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___minMinScale_20)); }
	inline float get_minMinScale_20() const { return ___minMinScale_20; }
	inline float* get_address_of_minMinScale_20() { return &___minMinScale_20; }
	inline void set_minMinScale_20(float value)
	{
		___minMinScale_20 = value;
	}

	inline static int32_t get_offset_of_maxMinScale_21() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___maxMinScale_21)); }
	inline float get_maxMinScale_21() const { return ___maxMinScale_21; }
	inline float* get_address_of_maxMinScale_21() { return &___maxMinScale_21; }
	inline void set_maxMinScale_21(float value)
	{
		___maxMinScale_21 = value;
	}

	inline static int32_t get_offset_of_minMaxScale_22() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___minMaxScale_22)); }
	inline float get_minMaxScale_22() const { return ___minMaxScale_22; }
	inline float* get_address_of_minMaxScale_22() { return &___minMaxScale_22; }
	inline void set_minMaxScale_22(float value)
	{
		___minMaxScale_22 = value;
	}

	inline static int32_t get_offset_of_maxMaxScale_23() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___maxMaxScale_23)); }
	inline float get_maxMaxScale_23() const { return ___maxMaxScale_23; }
	inline float* get_address_of_maxMaxScale_23() { return &___maxMaxScale_23; }
	inline void set_maxMaxScale_23(float value)
	{
		___maxMaxScale_23 = value;
	}

	inline static int32_t get_offset_of_minYOffset_24() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___minYOffset_24)); }
	inline float get_minYOffset_24() const { return ___minYOffset_24; }
	inline float* get_address_of_minYOffset_24() { return &___minYOffset_24; }
	inline void set_minYOffset_24(float value)
	{
		___minYOffset_24 = value;
	}

	inline static int32_t get_offset_of_maxYOffset_25() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___maxYOffset_25)); }
	inline float get_maxYOffset_25() const { return ___maxYOffset_25; }
	inline float* get_address_of_maxYOffset_25() { return &___maxYOffset_25; }
	inline void set_maxYOffset_25(float value)
	{
		___maxYOffset_25 = value;
	}

	inline static int32_t get_offset_of_minSpacing_26() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___minSpacing_26)); }
	inline float get_minSpacing_26() const { return ___minSpacing_26; }
	inline float* get_address_of_minSpacing_26() { return &___minSpacing_26; }
	inline void set_minSpacing_26(float value)
	{
		___minSpacing_26 = value;
	}

	inline static int32_t get_offset_of_maxSpacing_27() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___maxSpacing_27)); }
	inline float get_maxSpacing_27() const { return ___maxSpacing_27; }
	inline float* get_address_of_maxSpacing_27() { return &___maxSpacing_27; }
	inline void set_maxSpacing_27(float value)
	{
		___maxSpacing_27 = value;
	}

	inline static int32_t get_offset_of_gizmoPos_28() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___gizmoPos_28)); }
	inline Vector3_t2243707580  get_gizmoPos_28() const { return ___gizmoPos_28; }
	inline Vector3_t2243707580 * get_address_of_gizmoPos_28() { return &___gizmoPos_28; }
	inline void set_gizmoPos_28(Vector3_t2243707580  value)
	{
		___gizmoPos_28 = value;
	}

	inline static int32_t get_offset_of_gizmoActive_29() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___gizmoActive_29)); }
	inline bool get_gizmoActive_29() const { return ___gizmoActive_29; }
	inline bool* get_address_of_gizmoActive_29() { return &___gizmoActive_29; }
	inline void set_gizmoActive_29(bool value)
	{
		___gizmoActive_29 = value;
	}

	inline static int32_t get_offset_of_activeGeometry_30() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___activeGeometry_30)); }
	inline List_1_t1125654279 * get_activeGeometry_30() const { return ___activeGeometry_30; }
	inline List_1_t1125654279 ** get_address_of_activeGeometry_30() { return &___activeGeometry_30; }
	inline void set_activeGeometry_30(List_1_t1125654279 * value)
	{
		___activeGeometry_30 = value;
		Il2CppCodeGenWriteBarrier(&___activeGeometry_30, value);
	}

	inline static int32_t get_offset_of_activeGeometryPaths_31() { return static_cast<int32_t>(offsetof(GBSettings_t1771149548, ___activeGeometryPaths_31)); }
	inline List_1_t1398341365 * get_activeGeometryPaths_31() const { return ___activeGeometryPaths_31; }
	inline List_1_t1398341365 ** get_address_of_activeGeometryPaths_31() { return &___activeGeometryPaths_31; }
	inline void set_activeGeometryPaths_31(List_1_t1398341365 * value)
	{
		___activeGeometryPaths_31 = value;
		Il2CppCodeGenWriteBarrier(&___activeGeometryPaths_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
