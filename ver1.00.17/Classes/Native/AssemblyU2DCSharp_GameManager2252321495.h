﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen948997830.h"

// TitleAnimator
struct TitleAnimator_t1342483085;
// CameraFollow
struct CameraFollow_t1493855402;
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t1567160305;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Light[]
struct LightU5BU5D_t1037744493;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t3881993182;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public SingletonMonoBehaviour_1_t948997830
{
public:
	// System.Boolean GameManager::TitleAnimation
	bool ___TitleAnimation_3;
	// System.Boolean GameManager::CameraFollow
	bool ___CameraFollow_4;
	// System.Boolean GameManager::SkipRegisterAnimation
	bool ___SkipRegisterAnimation_5;
	// System.Boolean GameManager::SkipDynamicTutorial
	bool ___SkipDynamicTutorial_6;
	// System.Boolean GameManager::DebugLogON
	bool ___DebugLogON_7;
	// TitleAnimator GameManager::m_TitleAnimator
	TitleAnimator_t1342483085 * ___m_TitleAnimator_8;
	// CameraFollow GameManager::m_CameraFollow
	CameraFollow_t1493855402 * ___m_CameraFollow_9;
	// UnityEngine.MeshRenderer[] GameManager::m_bgObj
	MeshRendererU5BU5D_t1567160305* ___m_bgObj_10;
	// UnityEngine.GameObject[] GameManager::m_miniGameCharas
	GameObjectU5BU5D_t3057952154* ___m_miniGameCharas_11;
	// UnityEngine.Light[] GameManager::allLightSources
	LightU5BU5D_t1037744493* ___allLightSources_12;
	// UnityEngine.Rigidbody[] GameManager::allRigidbodys
	RigidbodyU5BU5D_t3881993182* ___allRigidbodys_13;
	// UnityEngine.Collider[] GameManager::allColliders
	ColliderU5BU5D_t462843629* ___allColliders_14;
	// UnityEngine.GameObject GameManager::m_collisionObj
	GameObject_t1756533147 * ___m_collisionObj_15;

public:
	inline static int32_t get_offset_of_TitleAnimation_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___TitleAnimation_3)); }
	inline bool get_TitleAnimation_3() const { return ___TitleAnimation_3; }
	inline bool* get_address_of_TitleAnimation_3() { return &___TitleAnimation_3; }
	inline void set_TitleAnimation_3(bool value)
	{
		___TitleAnimation_3 = value;
	}

	inline static int32_t get_offset_of_CameraFollow_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___CameraFollow_4)); }
	inline bool get_CameraFollow_4() const { return ___CameraFollow_4; }
	inline bool* get_address_of_CameraFollow_4() { return &___CameraFollow_4; }
	inline void set_CameraFollow_4(bool value)
	{
		___CameraFollow_4 = value;
	}

	inline static int32_t get_offset_of_SkipRegisterAnimation_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___SkipRegisterAnimation_5)); }
	inline bool get_SkipRegisterAnimation_5() const { return ___SkipRegisterAnimation_5; }
	inline bool* get_address_of_SkipRegisterAnimation_5() { return &___SkipRegisterAnimation_5; }
	inline void set_SkipRegisterAnimation_5(bool value)
	{
		___SkipRegisterAnimation_5 = value;
	}

	inline static int32_t get_offset_of_SkipDynamicTutorial_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___SkipDynamicTutorial_6)); }
	inline bool get_SkipDynamicTutorial_6() const { return ___SkipDynamicTutorial_6; }
	inline bool* get_address_of_SkipDynamicTutorial_6() { return &___SkipDynamicTutorial_6; }
	inline void set_SkipDynamicTutorial_6(bool value)
	{
		___SkipDynamicTutorial_6 = value;
	}

	inline static int32_t get_offset_of_DebugLogON_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___DebugLogON_7)); }
	inline bool get_DebugLogON_7() const { return ___DebugLogON_7; }
	inline bool* get_address_of_DebugLogON_7() { return &___DebugLogON_7; }
	inline void set_DebugLogON_7(bool value)
	{
		___DebugLogON_7 = value;
	}

	inline static int32_t get_offset_of_m_TitleAnimator_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___m_TitleAnimator_8)); }
	inline TitleAnimator_t1342483085 * get_m_TitleAnimator_8() const { return ___m_TitleAnimator_8; }
	inline TitleAnimator_t1342483085 ** get_address_of_m_TitleAnimator_8() { return &___m_TitleAnimator_8; }
	inline void set_m_TitleAnimator_8(TitleAnimator_t1342483085 * value)
	{
		___m_TitleAnimator_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_TitleAnimator_8, value);
	}

	inline static int32_t get_offset_of_m_CameraFollow_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___m_CameraFollow_9)); }
	inline CameraFollow_t1493855402 * get_m_CameraFollow_9() const { return ___m_CameraFollow_9; }
	inline CameraFollow_t1493855402 ** get_address_of_m_CameraFollow_9() { return &___m_CameraFollow_9; }
	inline void set_m_CameraFollow_9(CameraFollow_t1493855402 * value)
	{
		___m_CameraFollow_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_CameraFollow_9, value);
	}

	inline static int32_t get_offset_of_m_bgObj_10() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___m_bgObj_10)); }
	inline MeshRendererU5BU5D_t1567160305* get_m_bgObj_10() const { return ___m_bgObj_10; }
	inline MeshRendererU5BU5D_t1567160305** get_address_of_m_bgObj_10() { return &___m_bgObj_10; }
	inline void set_m_bgObj_10(MeshRendererU5BU5D_t1567160305* value)
	{
		___m_bgObj_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_bgObj_10, value);
	}

	inline static int32_t get_offset_of_m_miniGameCharas_11() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___m_miniGameCharas_11)); }
	inline GameObjectU5BU5D_t3057952154* get_m_miniGameCharas_11() const { return ___m_miniGameCharas_11; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_miniGameCharas_11() { return &___m_miniGameCharas_11; }
	inline void set_m_miniGameCharas_11(GameObjectU5BU5D_t3057952154* value)
	{
		___m_miniGameCharas_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_miniGameCharas_11, value);
	}

	inline static int32_t get_offset_of_allLightSources_12() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___allLightSources_12)); }
	inline LightU5BU5D_t1037744493* get_allLightSources_12() const { return ___allLightSources_12; }
	inline LightU5BU5D_t1037744493** get_address_of_allLightSources_12() { return &___allLightSources_12; }
	inline void set_allLightSources_12(LightU5BU5D_t1037744493* value)
	{
		___allLightSources_12 = value;
		Il2CppCodeGenWriteBarrier(&___allLightSources_12, value);
	}

	inline static int32_t get_offset_of_allRigidbodys_13() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___allRigidbodys_13)); }
	inline RigidbodyU5BU5D_t3881993182* get_allRigidbodys_13() const { return ___allRigidbodys_13; }
	inline RigidbodyU5BU5D_t3881993182** get_address_of_allRigidbodys_13() { return &___allRigidbodys_13; }
	inline void set_allRigidbodys_13(RigidbodyU5BU5D_t3881993182* value)
	{
		___allRigidbodys_13 = value;
		Il2CppCodeGenWriteBarrier(&___allRigidbodys_13, value);
	}

	inline static int32_t get_offset_of_allColliders_14() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___allColliders_14)); }
	inline ColliderU5BU5D_t462843629* get_allColliders_14() const { return ___allColliders_14; }
	inline ColliderU5BU5D_t462843629** get_address_of_allColliders_14() { return &___allColliders_14; }
	inline void set_allColliders_14(ColliderU5BU5D_t462843629* value)
	{
		___allColliders_14 = value;
		Il2CppCodeGenWriteBarrier(&___allColliders_14, value);
	}

	inline static int32_t get_offset_of_m_collisionObj_15() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___m_collisionObj_15)); }
	inline GameObject_t1756533147 * get_m_collisionObj_15() const { return ___m_collisionObj_15; }
	inline GameObject_t1756533147 ** get_address_of_m_collisionObj_15() { return &___m_collisionObj_15; }
	inline void set_m_collisionObj_15(GameObject_t1756533147 * value)
	{
		___m_collisionObj_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_collisionObj_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
