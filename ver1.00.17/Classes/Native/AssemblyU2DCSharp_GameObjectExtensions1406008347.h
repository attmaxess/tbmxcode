﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject>
struct Func_2_t2042182810;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectExtensions
struct  GameObjectExtensions_t1406008347  : public Il2CppObject
{
public:

public:
};

struct GameObjectExtensions_t1406008347_StaticFields
{
public:
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> GameObjectExtensions::<>f__am$cache0
	Func_2_t2042182810 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> GameObjectExtensions::<>f__am$cache1
	Func_2_t2042182810 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(GameObjectExtensions_t1406008347_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t2042182810 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t2042182810 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t2042182810 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(GameObjectExtensions_t1406008347_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t2042182810 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t2042182810 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t2042182810 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
