﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectExtensions/<GetChildren>c__AnonStorey0
struct  U3CGetChildrenU3Ec__AnonStorey0_t1107910596  : public Il2CppObject
{
public:
	// UnityEngine.GameObject GameObjectExtensions/<GetChildren>c__AnonStorey0::self
	GameObject_t1756533147 * ___self_0;

public:
	inline static int32_t get_offset_of_self_0() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ec__AnonStorey0_t1107910596, ___self_0)); }
	inline GameObject_t1756533147 * get_self_0() const { return ___self_0; }
	inline GameObject_t1756533147 ** get_address_of_self_0() { return &___self_0; }
	inline void set_self_0(GameObject_t1756533147 * value)
	{
		___self_0 = value;
		Il2CppCodeGenWriteBarrier(&___self_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
