﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Component
struct Component_t3819376471;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectExtensions/<GetChildren>c__AnonStorey1
struct  U3CGetChildrenU3Ec__AnonStorey1_t2673994537  : public Il2CppObject
{
public:
	// UnityEngine.Component GameObjectExtensions/<GetChildren>c__AnonStorey1::self
	Component_t3819376471 * ___self_0;

public:
	inline static int32_t get_offset_of_self_0() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ec__AnonStorey1_t2673994537, ___self_0)); }
	inline Component_t3819376471 * get_self_0() const { return ___self_0; }
	inline Component_t3819376471 ** get_address_of_self_0() { return &___self_0; }
	inline void set_self_0(Component_t3819376471 * value)
	{
		___self_0 = value;
		Il2CppCodeGenWriteBarrier(&___self_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
