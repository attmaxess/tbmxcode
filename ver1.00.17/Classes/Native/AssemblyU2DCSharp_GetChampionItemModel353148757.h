﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<ChampionItemModel>
struct List_1_t344991531;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetChampionItemModel
struct  GetChampionItemModel_t353148757  : public Model_t873752437
{
public:
	// System.Collections.Generic.List`1<ChampionItemModel> GetChampionItemModel::<_champion_itemModel_list>k__BackingField
	List_1_t344991531 * ___U3C_champion_itemModel_listU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3C_champion_itemModel_listU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetChampionItemModel_t353148757, ___U3C_champion_itemModel_listU3Ek__BackingField_0)); }
	inline List_1_t344991531 * get_U3C_champion_itemModel_listU3Ek__BackingField_0() const { return ___U3C_champion_itemModel_listU3Ek__BackingField_0; }
	inline List_1_t344991531 ** get_address_of_U3C_champion_itemModel_listU3Ek__BackingField_0() { return &___U3C_champion_itemModel_listU3Ek__BackingField_0; }
	inline void set_U3C_champion_itemModel_listU3Ek__BackingField_0(List_1_t344991531 * value)
	{
		___U3C_champion_itemModel_listU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_itemModel_listU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
