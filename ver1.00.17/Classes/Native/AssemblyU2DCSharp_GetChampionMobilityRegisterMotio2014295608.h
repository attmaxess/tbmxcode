﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<GetChampionMobilityRegisterMotionListPoseData>
struct List_1_t3060896492;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetChampionMobilityRegisterMotionListMotionDataModel
struct  GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608  : public Model_t873752437
{
public:
	// System.Int32 GetChampionMobilityRegisterMotionListMotionDataModel::<champion_mobility_registerdMotionList_motionData_pose_Id>k__BackingField
	int32_t ___U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<GetChampionMobilityRegisterMotionListPoseData> GetChampionMobilityRegisterMotionListMotionDataModel::<_champion_mobility_registerMotionList_poseData>k__BackingField
	List_1_t3060896492 * ___U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608, ___U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0() const { return ___U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0() { return &___U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0; }
	inline void set_U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608, ___U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1)); }
	inline List_1_t3060896492 * get_U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1() const { return ___U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1; }
	inline List_1_t3060896492 ** get_address_of_U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1() { return &___U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1; }
	inline void set_U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1(List_1_t3060896492 * value)
	{
		___U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
