﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<GetChampionMobilityRegisterMotionListMotionDataModel>
struct List_1_t1383416740;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetChampionMobilityRegisterdMotionListModel
struct  GetChampionMobilityRegisterdMotionListModel_t1560983880  : public Model_t873752437
{
public:
	// System.Int32 GetChampionMobilityRegisterdMotionListModel::<champion_mobility_registerdMotionList_motion_Id>k__BackingField
	int32_t ___U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<GetChampionMobilityRegisterMotionListMotionDataModel> GetChampionMobilityRegisterdMotionListModel::<_champion_mobility_registerMotionList_motionData>k__BackingField
	List_1_t1383416740 * ___U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetChampionMobilityRegisterdMotionListModel_t1560983880, ___U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0() const { return ___U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0() { return &___U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0; }
	inline void set_U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetChampionMobilityRegisterdMotionListModel_t1560983880, ___U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1)); }
	inline List_1_t1383416740 * get_U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1() const { return ___U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1; }
	inline List_1_t1383416740 ** get_address_of_U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1() { return &___U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1; }
	inline void set_U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1(List_1_t1383416740 * value)
	{
		___U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
