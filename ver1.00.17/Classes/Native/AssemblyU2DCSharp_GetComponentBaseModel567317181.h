﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<GetComponentModel>
struct List_1_t969947876;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetComponentBaseModel
struct  GetComponentBaseModel_t567317181  : public Model_t873752437
{
public:
	// System.Collections.Generic.List`1<GetComponentModel> GetComponentBaseModel::<get_component_model_list>k__BackingField
	List_1_t969947876 * ___U3Cget_component_model_listU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Cget_component_model_listU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetComponentBaseModel_t567317181, ___U3Cget_component_model_listU3Ek__BackingField_0)); }
	inline List_1_t969947876 * get_U3Cget_component_model_listU3Ek__BackingField_0() const { return ___U3Cget_component_model_listU3Ek__BackingField_0; }
	inline List_1_t969947876 ** get_address_of_U3Cget_component_model_listU3Ek__BackingField_0() { return &___U3Cget_component_model_listU3Ek__BackingField_0; }
	inline void set_U3Cget_component_model_listU3Ek__BackingField_0(List_1_t969947876 * value)
	{
		___U3Cget_component_model_listU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cget_component_model_listU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
