﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetComponentListModel
struct  GetComponentListModel_t2407555168  : public Model_t873752437
{
public:
	// System.Single GetComponentListModel::<_component_Parameter_Mass>k__BackingField
	float ___U3C_component_Parameter_MassU3Ek__BackingField_0;
	// System.Single GetComponentListModel::<_component_Parameter_Force>k__BackingField
	float ___U3C_component_Parameter_ForceU3Ek__BackingField_1;
	// System.Single GetComponentListModel::<_component_Parameter_Bounce>k__BackingField
	float ___U3C_component_Parameter_BounceU3Ek__BackingField_2;
	// System.Single GetComponentListModel::<_component_Parameter_FloatForce>k__BackingField
	float ___U3C_component_Parameter_FloatForceU3Ek__BackingField_3;
	// System.Single GetComponentListModel::<_component_Parameter_ElasticitySpeed>k__BackingField
	float ___U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4;
	// System.Single GetComponentListModel::<_component_Parameter_FloatBounce>k__BackingField
	float ___U3C_component_Parameter_FloatBounceU3Ek__BackingField_5;
	// System.Single GetComponentListModel::<_component_Parameter_FloatFriction>k__BackingField
	float ___U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6;
	// System.Single GetComponentListModel::<_component_Parameter_Friction>k__BackingField
	float ___U3C_component_Parameter_FrictionU3Ek__BackingField_7;
	// System.Single GetComponentListModel::<_component_Parameter_RotationSpeed>k__BackingField
	float ___U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3C_component_Parameter_MassU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_MassU3Ek__BackingField_0)); }
	inline float get_U3C_component_Parameter_MassU3Ek__BackingField_0() const { return ___U3C_component_Parameter_MassU3Ek__BackingField_0; }
	inline float* get_address_of_U3C_component_Parameter_MassU3Ek__BackingField_0() { return &___U3C_component_Parameter_MassU3Ek__BackingField_0; }
	inline void set_U3C_component_Parameter_MassU3Ek__BackingField_0(float value)
	{
		___U3C_component_Parameter_MassU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_ForceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_ForceU3Ek__BackingField_1)); }
	inline float get_U3C_component_Parameter_ForceU3Ek__BackingField_1() const { return ___U3C_component_Parameter_ForceU3Ek__BackingField_1; }
	inline float* get_address_of_U3C_component_Parameter_ForceU3Ek__BackingField_1() { return &___U3C_component_Parameter_ForceU3Ek__BackingField_1; }
	inline void set_U3C_component_Parameter_ForceU3Ek__BackingField_1(float value)
	{
		___U3C_component_Parameter_ForceU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_BounceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_BounceU3Ek__BackingField_2)); }
	inline float get_U3C_component_Parameter_BounceU3Ek__BackingField_2() const { return ___U3C_component_Parameter_BounceU3Ek__BackingField_2; }
	inline float* get_address_of_U3C_component_Parameter_BounceU3Ek__BackingField_2() { return &___U3C_component_Parameter_BounceU3Ek__BackingField_2; }
	inline void set_U3C_component_Parameter_BounceU3Ek__BackingField_2(float value)
	{
		___U3C_component_Parameter_BounceU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_FloatForceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_FloatForceU3Ek__BackingField_3)); }
	inline float get_U3C_component_Parameter_FloatForceU3Ek__BackingField_3() const { return ___U3C_component_Parameter_FloatForceU3Ek__BackingField_3; }
	inline float* get_address_of_U3C_component_Parameter_FloatForceU3Ek__BackingField_3() { return &___U3C_component_Parameter_FloatForceU3Ek__BackingField_3; }
	inline void set_U3C_component_Parameter_FloatForceU3Ek__BackingField_3(float value)
	{
		___U3C_component_Parameter_FloatForceU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4)); }
	inline float get_U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4() const { return ___U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4; }
	inline float* get_address_of_U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4() { return &___U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4; }
	inline void set_U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4(float value)
	{
		___U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_FloatBounceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_FloatBounceU3Ek__BackingField_5)); }
	inline float get_U3C_component_Parameter_FloatBounceU3Ek__BackingField_5() const { return ___U3C_component_Parameter_FloatBounceU3Ek__BackingField_5; }
	inline float* get_address_of_U3C_component_Parameter_FloatBounceU3Ek__BackingField_5() { return &___U3C_component_Parameter_FloatBounceU3Ek__BackingField_5; }
	inline void set_U3C_component_Parameter_FloatBounceU3Ek__BackingField_5(float value)
	{
		___U3C_component_Parameter_FloatBounceU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6)); }
	inline float get_U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6() const { return ___U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6; }
	inline float* get_address_of_U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6() { return &___U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6; }
	inline void set_U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6(float value)
	{
		___U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_FrictionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_FrictionU3Ek__BackingField_7)); }
	inline float get_U3C_component_Parameter_FrictionU3Ek__BackingField_7() const { return ___U3C_component_Parameter_FrictionU3Ek__BackingField_7; }
	inline float* get_address_of_U3C_component_Parameter_FrictionU3Ek__BackingField_7() { return &___U3C_component_Parameter_FrictionU3Ek__BackingField_7; }
	inline void set_U3C_component_Parameter_FrictionU3Ek__BackingField_7(float value)
	{
		___U3C_component_Parameter_FrictionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GetComponentListModel_t2407555168, ___U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8)); }
	inline float get_U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8() const { return ___U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8; }
	inline float* get_address_of_U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8() { return &___U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8; }
	inline void set_U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8(float value)
	{
		___U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
