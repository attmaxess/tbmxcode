﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// GetComponentListModel
struct GetComponentListModel_t2407555168;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetComponentModel
struct  GetComponentModel_t1600826744  : public Model_t873752437
{
public:
	// System.Int32 GetComponentModel::<_component_Id>k__BackingField
	int32_t ___U3C_component_IdU3Ek__BackingField_0;
	// System.String GetComponentModel::<_component_Name>k__BackingField
	String_t* ___U3C_component_NameU3Ek__BackingField_1;
	// System.String GetComponentModel::<_component_Description>k__BackingField
	String_t* ___U3C_component_DescriptionU3Ek__BackingField_2;
	// System.Int32 GetComponentModel::<_component_Category_Id>k__BackingField
	int32_t ___U3C_component_Category_IdU3Ek__BackingField_3;
	// GetComponentListModel GetComponentModel::_parameter_list
	GetComponentListModel_t2407555168 * ____parameter_list_4;

public:
	inline static int32_t get_offset_of_U3C_component_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetComponentModel_t1600826744, ___U3C_component_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3C_component_IdU3Ek__BackingField_0() const { return ___U3C_component_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3C_component_IdU3Ek__BackingField_0() { return &___U3C_component_IdU3Ek__BackingField_0; }
	inline void set_U3C_component_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3C_component_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_component_NameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetComponentModel_t1600826744, ___U3C_component_NameU3Ek__BackingField_1)); }
	inline String_t* get_U3C_component_NameU3Ek__BackingField_1() const { return ___U3C_component_NameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3C_component_NameU3Ek__BackingField_1() { return &___U3C_component_NameU3Ek__BackingField_1; }
	inline void set_U3C_component_NameU3Ek__BackingField_1(String_t* value)
	{
		___U3C_component_NameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_component_NameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3C_component_DescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetComponentModel_t1600826744, ___U3C_component_DescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3C_component_DescriptionU3Ek__BackingField_2() const { return ___U3C_component_DescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3C_component_DescriptionU3Ek__BackingField_2() { return &___U3C_component_DescriptionU3Ek__BackingField_2; }
	inline void set_U3C_component_DescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3C_component_DescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_component_DescriptionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3C_component_Category_IdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetComponentModel_t1600826744, ___U3C_component_Category_IdU3Ek__BackingField_3)); }
	inline int32_t get_U3C_component_Category_IdU3Ek__BackingField_3() const { return ___U3C_component_Category_IdU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3C_component_Category_IdU3Ek__BackingField_3() { return &___U3C_component_Category_IdU3Ek__BackingField_3; }
	inline void set_U3C_component_Category_IdU3Ek__BackingField_3(int32_t value)
	{
		___U3C_component_Category_IdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of__parameter_list_4() { return static_cast<int32_t>(offsetof(GetComponentModel_t1600826744, ____parameter_list_4)); }
	inline GetComponentListModel_t2407555168 * get__parameter_list_4() const { return ____parameter_list_4; }
	inline GetComponentListModel_t2407555168 ** get_address_of__parameter_list_4() { return &____parameter_list_4; }
	inline void set__parameter_list_4(GetComponentListModel_t2407555168 * value)
	{
		____parameter_list_4 = value;
		Il2CppCodeGenWriteBarrier(&____parameter_list_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
