﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetDnaChildrenListModel
struct  GetDnaChildrenListModel_t2978945125  : public Model_t873752437
{
public:
	// System.Int32 GetDnaChildrenListModel::<_dna_Children_ChildId>k__BackingField
	int32_t ___U3C_dna_Children_ChildIdU3Ek__BackingField_0;
	// System.String GetDnaChildrenListModel::<_dna_Children_ChildName>k__BackingField
	String_t* ___U3C_dna_Children_ChildNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3C_dna_Children_ChildIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetDnaChildrenListModel_t2978945125, ___U3C_dna_Children_ChildIdU3Ek__BackingField_0)); }
	inline int32_t get_U3C_dna_Children_ChildIdU3Ek__BackingField_0() const { return ___U3C_dna_Children_ChildIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3C_dna_Children_ChildIdU3Ek__BackingField_0() { return &___U3C_dna_Children_ChildIdU3Ek__BackingField_0; }
	inline void set_U3C_dna_Children_ChildIdU3Ek__BackingField_0(int32_t value)
	{
		___U3C_dna_Children_ChildIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_dna_Children_ChildNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetDnaChildrenListModel_t2978945125, ___U3C_dna_Children_ChildNameU3Ek__BackingField_1)); }
	inline String_t* get_U3C_dna_Children_ChildNameU3Ek__BackingField_1() const { return ___U3C_dna_Children_ChildNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3C_dna_Children_ChildNameU3Ek__BackingField_1() { return &___U3C_dna_Children_ChildNameU3Ek__BackingField_1; }
	inline void set_U3C_dna_Children_ChildNameU3Ek__BackingField_1(String_t* value)
	{
		___U3C_dna_Children_ChildNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_dna_Children_ChildNameU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
