﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GetDnaChildrenListModel>
struct List_1_t2348066257;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetDnaModel
struct  GetDnaModel_t3440451250  : public Model_t873752437
{
public:
	// System.Int32 GetDnaModel::<_dna_ParentId>k__BackingField
	int32_t ___U3C_dna_ParentIdU3Ek__BackingField_0;
	// System.String GetDnaModel::<_dna_ParentName>k__BackingField
	String_t* ___U3C_dna_ParentNameU3Ek__BackingField_1;
	// System.Int32 GetDnaModel::<_dna_MobId>k__BackingField
	int32_t ___U3C_dna_MobIdU3Ek__BackingField_2;
	// System.String GetDnaModel::<_dna_MobName>k__BackingField
	String_t* ___U3C_dna_MobNameU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<GetDnaChildrenListModel> GetDnaModel::<_dna_children_list>k__BackingField
	List_1_t2348066257 * ___U3C_dna_children_listU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3C_dna_ParentIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetDnaModel_t3440451250, ___U3C_dna_ParentIdU3Ek__BackingField_0)); }
	inline int32_t get_U3C_dna_ParentIdU3Ek__BackingField_0() const { return ___U3C_dna_ParentIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3C_dna_ParentIdU3Ek__BackingField_0() { return &___U3C_dna_ParentIdU3Ek__BackingField_0; }
	inline void set_U3C_dna_ParentIdU3Ek__BackingField_0(int32_t value)
	{
		___U3C_dna_ParentIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_dna_ParentNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetDnaModel_t3440451250, ___U3C_dna_ParentNameU3Ek__BackingField_1)); }
	inline String_t* get_U3C_dna_ParentNameU3Ek__BackingField_1() const { return ___U3C_dna_ParentNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3C_dna_ParentNameU3Ek__BackingField_1() { return &___U3C_dna_ParentNameU3Ek__BackingField_1; }
	inline void set_U3C_dna_ParentNameU3Ek__BackingField_1(String_t* value)
	{
		___U3C_dna_ParentNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_dna_ParentNameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3C_dna_MobIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetDnaModel_t3440451250, ___U3C_dna_MobIdU3Ek__BackingField_2)); }
	inline int32_t get_U3C_dna_MobIdU3Ek__BackingField_2() const { return ___U3C_dna_MobIdU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3C_dna_MobIdU3Ek__BackingField_2() { return &___U3C_dna_MobIdU3Ek__BackingField_2; }
	inline void set_U3C_dna_MobIdU3Ek__BackingField_2(int32_t value)
	{
		___U3C_dna_MobIdU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3C_dna_MobNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetDnaModel_t3440451250, ___U3C_dna_MobNameU3Ek__BackingField_3)); }
	inline String_t* get_U3C_dna_MobNameU3Ek__BackingField_3() const { return ___U3C_dna_MobNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3C_dna_MobNameU3Ek__BackingField_3() { return &___U3C_dna_MobNameU3Ek__BackingField_3; }
	inline void set_U3C_dna_MobNameU3Ek__BackingField_3(String_t* value)
	{
		___U3C_dna_MobNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_dna_MobNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3C_dna_children_listU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetDnaModel_t3440451250, ___U3C_dna_children_listU3Ek__BackingField_4)); }
	inline List_1_t2348066257 * get_U3C_dna_children_listU3Ek__BackingField_4() const { return ___U3C_dna_children_listU3Ek__BackingField_4; }
	inline List_1_t2348066257 ** get_address_of_U3C_dna_children_listU3Ek__BackingField_4() { return &___U3C_dna_children_listU3Ek__BackingField_4; }
	inline void set_U3C_dna_children_listU3Ek__BackingField_4(List_1_t2348066257 * value)
	{
		___U3C_dna_children_listU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_dna_children_listU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
