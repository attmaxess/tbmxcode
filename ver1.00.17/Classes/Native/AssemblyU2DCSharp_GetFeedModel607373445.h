﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetFeedModel
struct  GetFeedModel_t607373445  : public Model_t873752437
{
public:
	// System.Int32 GetFeedModel::<_feed_Id>k__BackingField
	int32_t ___U3C_feed_IdU3Ek__BackingField_0;
	// System.String GetFeedModel::<_feed_Date>k__BackingField
	String_t* ___U3C_feed_DateU3Ek__BackingField_1;
	// System.String GetFeedModel::<_feed_Name>k__BackingField
	String_t* ___U3C_feed_NameU3Ek__BackingField_2;
	// System.Int32 GetFeedModel::<_feed_Situation>k__BackingField
	int32_t ___U3C_feed_SituationU3Ek__BackingField_3;
	// System.Int32 GetFeedModel::<_feed_Reaction_AllCount>k__BackingField
	int32_t ___U3C_feed_Reaction_AllCountU3Ek__BackingField_4;
	// System.Int32 GetFeedModel::<_feed_UserId>k__BackingField
	int32_t ___U3C_feed_UserIdU3Ek__BackingField_5;
	// System.Int32 GetFeedModel::<_feed_CopiedCount>k__BackingField
	int32_t ___U3C_feed_CopiedCountU3Ek__BackingField_6;
	// System.String GetFeedModel::<_feed_Thumbnail>k__BackingField
	String_t* ___U3C_feed_ThumbnailU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3C_feed_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3C_feed_IdU3Ek__BackingField_0() const { return ___U3C_feed_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3C_feed_IdU3Ek__BackingField_0() { return &___U3C_feed_IdU3Ek__BackingField_0; }
	inline void set_U3C_feed_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3C_feed_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_feed_DateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_DateU3Ek__BackingField_1)); }
	inline String_t* get_U3C_feed_DateU3Ek__BackingField_1() const { return ___U3C_feed_DateU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3C_feed_DateU3Ek__BackingField_1() { return &___U3C_feed_DateU3Ek__BackingField_1; }
	inline void set_U3C_feed_DateU3Ek__BackingField_1(String_t* value)
	{
		___U3C_feed_DateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_DateU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3C_feed_NameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_NameU3Ek__BackingField_2)); }
	inline String_t* get_U3C_feed_NameU3Ek__BackingField_2() const { return ___U3C_feed_NameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3C_feed_NameU3Ek__BackingField_2() { return &___U3C_feed_NameU3Ek__BackingField_2; }
	inline void set_U3C_feed_NameU3Ek__BackingField_2(String_t* value)
	{
		___U3C_feed_NameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_NameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3C_feed_SituationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_SituationU3Ek__BackingField_3)); }
	inline int32_t get_U3C_feed_SituationU3Ek__BackingField_3() const { return ___U3C_feed_SituationU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3C_feed_SituationU3Ek__BackingField_3() { return &___U3C_feed_SituationU3Ek__BackingField_3; }
	inline void set_U3C_feed_SituationU3Ek__BackingField_3(int32_t value)
	{
		___U3C_feed_SituationU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3C_feed_Reaction_AllCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_Reaction_AllCountU3Ek__BackingField_4)); }
	inline int32_t get_U3C_feed_Reaction_AllCountU3Ek__BackingField_4() const { return ___U3C_feed_Reaction_AllCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3C_feed_Reaction_AllCountU3Ek__BackingField_4() { return &___U3C_feed_Reaction_AllCountU3Ek__BackingField_4; }
	inline void set_U3C_feed_Reaction_AllCountU3Ek__BackingField_4(int32_t value)
	{
		___U3C_feed_Reaction_AllCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3C_feed_UserIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_UserIdU3Ek__BackingField_5)); }
	inline int32_t get_U3C_feed_UserIdU3Ek__BackingField_5() const { return ___U3C_feed_UserIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3C_feed_UserIdU3Ek__BackingField_5() { return &___U3C_feed_UserIdU3Ek__BackingField_5; }
	inline void set_U3C_feed_UserIdU3Ek__BackingField_5(int32_t value)
	{
		___U3C_feed_UserIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3C_feed_CopiedCountU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_CopiedCountU3Ek__BackingField_6)); }
	inline int32_t get_U3C_feed_CopiedCountU3Ek__BackingField_6() const { return ___U3C_feed_CopiedCountU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3C_feed_CopiedCountU3Ek__BackingField_6() { return &___U3C_feed_CopiedCountU3Ek__BackingField_6; }
	inline void set_U3C_feed_CopiedCountU3Ek__BackingField_6(int32_t value)
	{
		___U3C_feed_CopiedCountU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3C_feed_ThumbnailU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetFeedModel_t607373445, ___U3C_feed_ThumbnailU3Ek__BackingField_7)); }
	inline String_t* get_U3C_feed_ThumbnailU3Ek__BackingField_7() const { return ___U3C_feed_ThumbnailU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3C_feed_ThumbnailU3Ek__BackingField_7() { return &___U3C_feed_ThumbnailU3Ek__BackingField_7; }
	inline void set_U3C_feed_ThumbnailU3Ek__BackingField_7(String_t* value)
	{
		___U3C_feed_ThumbnailU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_ThumbnailU3Ek__BackingField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
