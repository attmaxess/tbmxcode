﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetImportantlModel
struct  GetImportantlModel_t3127191609  : public Model_t873752437
{
public:
	// System.Int32 GetImportantlModel::<_important_Detail_Notice_Id>k__BackingField
	int32_t ___U3C_important_Detail_Notice_IdU3Ek__BackingField_0;
	// System.String GetImportantlModel::<_important_Title>k__BackingField
	String_t* ___U3C_important_TitleU3Ek__BackingField_1;
	// System.String GetImportantlModel::<_important_Notice>k__BackingField
	String_t* ___U3C_important_NoticeU3Ek__BackingField_2;
	// System.String GetImportantlModel::<_important_CreateAt>k__BackingField
	String_t* ___U3C_important_CreateAtU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3C_important_Detail_Notice_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetImportantlModel_t3127191609, ___U3C_important_Detail_Notice_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3C_important_Detail_Notice_IdU3Ek__BackingField_0() const { return ___U3C_important_Detail_Notice_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3C_important_Detail_Notice_IdU3Ek__BackingField_0() { return &___U3C_important_Detail_Notice_IdU3Ek__BackingField_0; }
	inline void set_U3C_important_Detail_Notice_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3C_important_Detail_Notice_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_important_TitleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetImportantlModel_t3127191609, ___U3C_important_TitleU3Ek__BackingField_1)); }
	inline String_t* get_U3C_important_TitleU3Ek__BackingField_1() const { return ___U3C_important_TitleU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3C_important_TitleU3Ek__BackingField_1() { return &___U3C_important_TitleU3Ek__BackingField_1; }
	inline void set_U3C_important_TitleU3Ek__BackingField_1(String_t* value)
	{
		___U3C_important_TitleU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_important_TitleU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3C_important_NoticeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetImportantlModel_t3127191609, ___U3C_important_NoticeU3Ek__BackingField_2)); }
	inline String_t* get_U3C_important_NoticeU3Ek__BackingField_2() const { return ___U3C_important_NoticeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3C_important_NoticeU3Ek__BackingField_2() { return &___U3C_important_NoticeU3Ek__BackingField_2; }
	inline void set_U3C_important_NoticeU3Ek__BackingField_2(String_t* value)
	{
		___U3C_important_NoticeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_important_NoticeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3C_important_CreateAtU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetImportantlModel_t3127191609, ___U3C_important_CreateAtU3Ek__BackingField_3)); }
	inline String_t* get_U3C_important_CreateAtU3Ek__BackingField_3() const { return ___U3C_important_CreateAtU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3C_important_CreateAtU3Ek__BackingField_3() { return &___U3C_important_CreateAtU3Ek__BackingField_3; }
	inline void set_U3C_important_CreateAtU3Ek__BackingField_3(String_t* value)
	{
		___U3C_important_CreateAtU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_important_CreateAtU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
