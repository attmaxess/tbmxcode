﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<GetMobilityItemsReactionItemsList>
struct List_1_t1949604308;
// System.Collections.Generic.List`1<GetMobilityEncountList>
struct List_1_t414479431;
// System.Collections.Generic.List`1<GetMobilityRegisterdMotionList>
struct List_1_t3508513714;
// System.Collections.Generic.List`1<GetMobilityRecordedMotionList>
struct List_1_t1538208813;
// System.Collections.Generic.List`1<GetMobilityChildPartsListModel>
struct List_1_t2060957510;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetMobilityModel
struct  GetMobilityModel_t1064189486  : public Model_t873752437
{
public:
	// System.Int32 GetMobilityModel::<mobility_Mobility_Id>k__BackingField
	int32_t ___U3Cmobility_Mobility_IdU3Ek__BackingField_0;
	// System.String GetMobilityModel::<mobility_date>k__BackingField
	String_t* ___U3Cmobility_dateU3Ek__BackingField_1;
	// System.String GetMobilityModel::<mobility_name>k__BackingField
	String_t* ___U3Cmobility_nameU3Ek__BackingField_2;
	// System.String GetMobilityModel::<mobility_situation>k__BackingField
	String_t* ___U3Cmobility_situationU3Ek__BackingField_3;
	// System.String GetMobilityModel::<mobility_description>k__BackingField
	String_t* ___U3Cmobility_descriptionU3Ek__BackingField_4;
	// System.Int32 GetMobilityModel::<mobility_parent_Id>k__BackingField
	int32_t ___U3Cmobility_parent_IdU3Ek__BackingField_5;
	// System.Int32 GetMobilityModel::<mobility_coreParts_Id>k__BackingField
	int32_t ___U3Cmobility_coreParts_IdU3Ek__BackingField_6;
	// System.Int32 GetMobilityModel::<mobility_coreParts_Color>k__BackingField
	int32_t ___U3Cmobility_coreParts_ColorU3Ek__BackingField_7;
	// System.Single GetMobilityModel::<mobility_corePartsScale_X>k__BackingField
	float ___U3Cmobility_corePartsScale_XU3Ek__BackingField_8;
	// System.Single GetMobilityModel::<mobility_corePartsScale_Y>k__BackingField
	float ___U3Cmobility_corePartsScale_YU3Ek__BackingField_9;
	// System.Single GetMobilityModel::<mobility_corePartsScale_Z>k__BackingField
	float ___U3Cmobility_corePartsScale_ZU3Ek__BackingField_10;
	// System.String GetMobilityModel::<mobility_coreParts_Name>k__BackingField
	String_t* ___U3Cmobility_coreParts_NameU3Ek__BackingField_11;
	// System.Int32 GetMobilityModel::<mobility_createdUser_Id>k__BackingField
	int32_t ___U3Cmobility_createdUser_IdU3Ek__BackingField_12;
	// System.String GetMobilityModel::<mobility_createdUser_Name>k__BackingField
	String_t* ___U3Cmobility_createdUser_NameU3Ek__BackingField_13;
	// System.String GetMobilityModel::<mobility_createdUser_Rank>k__BackingField
	String_t* ___U3Cmobility_createdUser_RankU3Ek__BackingField_14;
	// System.Int32 GetMobilityModel::<mobility_reaction_allcount>k__BackingField
	int32_t ___U3Cmobility_reaction_allcountU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<System.Object> GetMobilityModel::mobility_prized_list
	List_1_t2058570427 * ___mobility_prized_list_16;
	// System.Collections.Generic.List`1<GetMobilityItemsReactionItemsList> GetMobilityModel::<_mobility_reaction_items_list>k__BackingField
	List_1_t1949604308 * ___U3C_mobility_reaction_items_listU3Ek__BackingField_17;
	// System.Collections.Generic.List`1<GetMobilityEncountList> GetMobilityModel::<_mobility_encount_list>k__BackingField
	List_1_t414479431 * ___U3C_mobility_encount_listU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<GetMobilityRegisterdMotionList> GetMobilityModel::<_mobility_registerMotion_list>k__BackingField
	List_1_t3508513714 * ___U3C_mobility_registerMotion_listU3Ek__BackingField_19;
	// System.Collections.Generic.List`1<GetMobilityRecordedMotionList> GetMobilityModel::<_mobility_recordedMotion_list>k__BackingField
	List_1_t1538208813 * ___U3C_mobility_recordedMotion_listU3Ek__BackingField_20;
	// System.Collections.Generic.List`1<GetMobilityChildPartsListModel> GetMobilityModel::<_mobility_childParts_list>k__BackingField
	List_1_t2060957510 * ___U3C_mobility_childParts_listU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3Cmobility_Mobility_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_Mobility_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cmobility_Mobility_IdU3Ek__BackingField_0() const { return ___U3Cmobility_Mobility_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cmobility_Mobility_IdU3Ek__BackingField_0() { return &___U3Cmobility_Mobility_IdU3Ek__BackingField_0; }
	inline void set_U3Cmobility_Mobility_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cmobility_Mobility_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_dateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_dateU3Ek__BackingField_1)); }
	inline String_t* get_U3Cmobility_dateU3Ek__BackingField_1() const { return ___U3Cmobility_dateU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cmobility_dateU3Ek__BackingField_1() { return &___U3Cmobility_dateU3Ek__BackingField_1; }
	inline void set_U3Cmobility_dateU3Ek__BackingField_1(String_t* value)
	{
		___U3Cmobility_dateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_dateU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_nameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_nameU3Ek__BackingField_2)); }
	inline String_t* get_U3Cmobility_nameU3Ek__BackingField_2() const { return ___U3Cmobility_nameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cmobility_nameU3Ek__BackingField_2() { return &___U3Cmobility_nameU3Ek__BackingField_2; }
	inline void set_U3Cmobility_nameU3Ek__BackingField_2(String_t* value)
	{
		___U3Cmobility_nameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_nameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_situationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_situationU3Ek__BackingField_3)); }
	inline String_t* get_U3Cmobility_situationU3Ek__BackingField_3() const { return ___U3Cmobility_situationU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cmobility_situationU3Ek__BackingField_3() { return &___U3Cmobility_situationU3Ek__BackingField_3; }
	inline void set_U3Cmobility_situationU3Ek__BackingField_3(String_t* value)
	{
		___U3Cmobility_situationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_situationU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_descriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_descriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3Cmobility_descriptionU3Ek__BackingField_4() const { return ___U3Cmobility_descriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cmobility_descriptionU3Ek__BackingField_4() { return &___U3Cmobility_descriptionU3Ek__BackingField_4; }
	inline void set_U3Cmobility_descriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3Cmobility_descriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_descriptionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_parent_IdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_parent_IdU3Ek__BackingField_5)); }
	inline int32_t get_U3Cmobility_parent_IdU3Ek__BackingField_5() const { return ___U3Cmobility_parent_IdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3Cmobility_parent_IdU3Ek__BackingField_5() { return &___U3Cmobility_parent_IdU3Ek__BackingField_5; }
	inline void set_U3Cmobility_parent_IdU3Ek__BackingField_5(int32_t value)
	{
		___U3Cmobility_parent_IdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_coreParts_IdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_coreParts_IdU3Ek__BackingField_6)); }
	inline int32_t get_U3Cmobility_coreParts_IdU3Ek__BackingField_6() const { return ___U3Cmobility_coreParts_IdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3Cmobility_coreParts_IdU3Ek__BackingField_6() { return &___U3Cmobility_coreParts_IdU3Ek__BackingField_6; }
	inline void set_U3Cmobility_coreParts_IdU3Ek__BackingField_6(int32_t value)
	{
		___U3Cmobility_coreParts_IdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_coreParts_ColorU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_coreParts_ColorU3Ek__BackingField_7)); }
	inline int32_t get_U3Cmobility_coreParts_ColorU3Ek__BackingField_7() const { return ___U3Cmobility_coreParts_ColorU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3Cmobility_coreParts_ColorU3Ek__BackingField_7() { return &___U3Cmobility_coreParts_ColorU3Ek__BackingField_7; }
	inline void set_U3Cmobility_coreParts_ColorU3Ek__BackingField_7(int32_t value)
	{
		___U3Cmobility_coreParts_ColorU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_corePartsScale_XU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_corePartsScale_XU3Ek__BackingField_8)); }
	inline float get_U3Cmobility_corePartsScale_XU3Ek__BackingField_8() const { return ___U3Cmobility_corePartsScale_XU3Ek__BackingField_8; }
	inline float* get_address_of_U3Cmobility_corePartsScale_XU3Ek__BackingField_8() { return &___U3Cmobility_corePartsScale_XU3Ek__BackingField_8; }
	inline void set_U3Cmobility_corePartsScale_XU3Ek__BackingField_8(float value)
	{
		___U3Cmobility_corePartsScale_XU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_corePartsScale_YU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_corePartsScale_YU3Ek__BackingField_9)); }
	inline float get_U3Cmobility_corePartsScale_YU3Ek__BackingField_9() const { return ___U3Cmobility_corePartsScale_YU3Ek__BackingField_9; }
	inline float* get_address_of_U3Cmobility_corePartsScale_YU3Ek__BackingField_9() { return &___U3Cmobility_corePartsScale_YU3Ek__BackingField_9; }
	inline void set_U3Cmobility_corePartsScale_YU3Ek__BackingField_9(float value)
	{
		___U3Cmobility_corePartsScale_YU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_corePartsScale_ZU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_corePartsScale_ZU3Ek__BackingField_10)); }
	inline float get_U3Cmobility_corePartsScale_ZU3Ek__BackingField_10() const { return ___U3Cmobility_corePartsScale_ZU3Ek__BackingField_10; }
	inline float* get_address_of_U3Cmobility_corePartsScale_ZU3Ek__BackingField_10() { return &___U3Cmobility_corePartsScale_ZU3Ek__BackingField_10; }
	inline void set_U3Cmobility_corePartsScale_ZU3Ek__BackingField_10(float value)
	{
		___U3Cmobility_corePartsScale_ZU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_coreParts_NameU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_coreParts_NameU3Ek__BackingField_11)); }
	inline String_t* get_U3Cmobility_coreParts_NameU3Ek__BackingField_11() const { return ___U3Cmobility_coreParts_NameU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3Cmobility_coreParts_NameU3Ek__BackingField_11() { return &___U3Cmobility_coreParts_NameU3Ek__BackingField_11; }
	inline void set_U3Cmobility_coreParts_NameU3Ek__BackingField_11(String_t* value)
	{
		___U3Cmobility_coreParts_NameU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_coreParts_NameU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_createdUser_IdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_createdUser_IdU3Ek__BackingField_12)); }
	inline int32_t get_U3Cmobility_createdUser_IdU3Ek__BackingField_12() const { return ___U3Cmobility_createdUser_IdU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3Cmobility_createdUser_IdU3Ek__BackingField_12() { return &___U3Cmobility_createdUser_IdU3Ek__BackingField_12; }
	inline void set_U3Cmobility_createdUser_IdU3Ek__BackingField_12(int32_t value)
	{
		___U3Cmobility_createdUser_IdU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_createdUser_NameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_createdUser_NameU3Ek__BackingField_13)); }
	inline String_t* get_U3Cmobility_createdUser_NameU3Ek__BackingField_13() const { return ___U3Cmobility_createdUser_NameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3Cmobility_createdUser_NameU3Ek__BackingField_13() { return &___U3Cmobility_createdUser_NameU3Ek__BackingField_13; }
	inline void set_U3Cmobility_createdUser_NameU3Ek__BackingField_13(String_t* value)
	{
		___U3Cmobility_createdUser_NameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_createdUser_NameU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_createdUser_RankU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_createdUser_RankU3Ek__BackingField_14)); }
	inline String_t* get_U3Cmobility_createdUser_RankU3Ek__BackingField_14() const { return ___U3Cmobility_createdUser_RankU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3Cmobility_createdUser_RankU3Ek__BackingField_14() { return &___U3Cmobility_createdUser_RankU3Ek__BackingField_14; }
	inline void set_U3Cmobility_createdUser_RankU3Ek__BackingField_14(String_t* value)
	{
		___U3Cmobility_createdUser_RankU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_createdUser_RankU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_reaction_allcountU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3Cmobility_reaction_allcountU3Ek__BackingField_15)); }
	inline int32_t get_U3Cmobility_reaction_allcountU3Ek__BackingField_15() const { return ___U3Cmobility_reaction_allcountU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3Cmobility_reaction_allcountU3Ek__BackingField_15() { return &___U3Cmobility_reaction_allcountU3Ek__BackingField_15; }
	inline void set_U3Cmobility_reaction_allcountU3Ek__BackingField_15(int32_t value)
	{
		___U3Cmobility_reaction_allcountU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_mobility_prized_list_16() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___mobility_prized_list_16)); }
	inline List_1_t2058570427 * get_mobility_prized_list_16() const { return ___mobility_prized_list_16; }
	inline List_1_t2058570427 ** get_address_of_mobility_prized_list_16() { return &___mobility_prized_list_16; }
	inline void set_mobility_prized_list_16(List_1_t2058570427 * value)
	{
		___mobility_prized_list_16 = value;
		Il2CppCodeGenWriteBarrier(&___mobility_prized_list_16, value);
	}

	inline static int32_t get_offset_of_U3C_mobility_reaction_items_listU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3C_mobility_reaction_items_listU3Ek__BackingField_17)); }
	inline List_1_t1949604308 * get_U3C_mobility_reaction_items_listU3Ek__BackingField_17() const { return ___U3C_mobility_reaction_items_listU3Ek__BackingField_17; }
	inline List_1_t1949604308 ** get_address_of_U3C_mobility_reaction_items_listU3Ek__BackingField_17() { return &___U3C_mobility_reaction_items_listU3Ek__BackingField_17; }
	inline void set_U3C_mobility_reaction_items_listU3Ek__BackingField_17(List_1_t1949604308 * value)
	{
		___U3C_mobility_reaction_items_listU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_mobility_reaction_items_listU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3C_mobility_encount_listU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3C_mobility_encount_listU3Ek__BackingField_18)); }
	inline List_1_t414479431 * get_U3C_mobility_encount_listU3Ek__BackingField_18() const { return ___U3C_mobility_encount_listU3Ek__BackingField_18; }
	inline List_1_t414479431 ** get_address_of_U3C_mobility_encount_listU3Ek__BackingField_18() { return &___U3C_mobility_encount_listU3Ek__BackingField_18; }
	inline void set_U3C_mobility_encount_listU3Ek__BackingField_18(List_1_t414479431 * value)
	{
		___U3C_mobility_encount_listU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_mobility_encount_listU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3C_mobility_registerMotion_listU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3C_mobility_registerMotion_listU3Ek__BackingField_19)); }
	inline List_1_t3508513714 * get_U3C_mobility_registerMotion_listU3Ek__BackingField_19() const { return ___U3C_mobility_registerMotion_listU3Ek__BackingField_19; }
	inline List_1_t3508513714 ** get_address_of_U3C_mobility_registerMotion_listU3Ek__BackingField_19() { return &___U3C_mobility_registerMotion_listU3Ek__BackingField_19; }
	inline void set_U3C_mobility_registerMotion_listU3Ek__BackingField_19(List_1_t3508513714 * value)
	{
		___U3C_mobility_registerMotion_listU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_mobility_registerMotion_listU3Ek__BackingField_19, value);
	}

	inline static int32_t get_offset_of_U3C_mobility_recordedMotion_listU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3C_mobility_recordedMotion_listU3Ek__BackingField_20)); }
	inline List_1_t1538208813 * get_U3C_mobility_recordedMotion_listU3Ek__BackingField_20() const { return ___U3C_mobility_recordedMotion_listU3Ek__BackingField_20; }
	inline List_1_t1538208813 ** get_address_of_U3C_mobility_recordedMotion_listU3Ek__BackingField_20() { return &___U3C_mobility_recordedMotion_listU3Ek__BackingField_20; }
	inline void set_U3C_mobility_recordedMotion_listU3Ek__BackingField_20(List_1_t1538208813 * value)
	{
		___U3C_mobility_recordedMotion_listU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_mobility_recordedMotion_listU3Ek__BackingField_20, value);
	}

	inline static int32_t get_offset_of_U3C_mobility_childParts_listU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(GetMobilityModel_t1064189486, ___U3C_mobility_childParts_listU3Ek__BackingField_21)); }
	inline List_1_t2060957510 * get_U3C_mobility_childParts_listU3Ek__BackingField_21() const { return ___U3C_mobility_childParts_listU3Ek__BackingField_21; }
	inline List_1_t2060957510 ** get_address_of_U3C_mobility_childParts_listU3Ek__BackingField_21() { return &___U3C_mobility_childParts_listU3Ek__BackingField_21; }
	inline void set_U3C_mobility_childParts_listU3Ek__BackingField_21(List_1_t2060957510 * value)
	{
		___U3C_mobility_childParts_listU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_mobility_childParts_listU3Ek__BackingField_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
