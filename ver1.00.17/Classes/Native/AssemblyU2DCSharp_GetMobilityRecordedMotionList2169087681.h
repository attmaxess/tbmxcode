﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GetMobilityRecordedMotionListMotions>
struct List_1_t3640412062;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetMobilityRecordedMotionList
struct  GetMobilityRecordedMotionList_t2169087681  : public Model_t873752437
{
public:
	// System.Int32 GetMobilityRecordedMotionList::<mobility_recordedMotionList_recorded_Id>k__BackingField
	int32_t ___U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0;
	// System.String GetMobilityRecordedMotionList::<mobility_recordedMotionList_recorded_Data>k__BackingField
	String_t* ___U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1;
	// System.Single GetMobilityRecordedMotionList::<mobility_recordedMotionList_startPoint_X>k__BackingField
	float ___U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2;
	// System.Single GetMobilityRecordedMotionList::<mobility_recordedMotionList_startPoint_Y>k__BackingField
	float ___U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3;
	// System.Single GetMobilityRecordedMotionList::<mobility_recordedMotionList_startPoint_Z>k__BackingField
	float ___U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<GetMobilityRecordedMotionListMotions> GetMobilityRecordedMotionList::<_mobility_recordedMotionList_motionsData>k__BackingField
	List_1_t3640412062 * ___U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetMobilityRecordedMotionList_t2169087681, ___U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0() const { return ___U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0() { return &___U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0; }
	inline void set_U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cmobility_recordedMotionList_recorded_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetMobilityRecordedMotionList_t2169087681, ___U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1)); }
	inline String_t* get_U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1() const { return ___U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1() { return &___U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1; }
	inline void set_U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1(String_t* value)
	{
		___U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmobility_recordedMotionList_recorded_DataU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetMobilityRecordedMotionList_t2169087681, ___U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2)); }
	inline float get_U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2() const { return ___U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2; }
	inline float* get_address_of_U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2() { return &___U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2; }
	inline void set_U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2(float value)
	{
		___U3Cmobility_recordedMotionList_startPoint_XU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetMobilityRecordedMotionList_t2169087681, ___U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3)); }
	inline float get_U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3() const { return ___U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3; }
	inline float* get_address_of_U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3() { return &___U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3; }
	inline void set_U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3(float value)
	{
		___U3Cmobility_recordedMotionList_startPoint_YU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetMobilityRecordedMotionList_t2169087681, ___U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4)); }
	inline float get_U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4() const { return ___U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4; }
	inline float* get_address_of_U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4() { return &___U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4; }
	inline void set_U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4(float value)
	{
		___U3Cmobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetMobilityRecordedMotionList_t2169087681, ___U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5)); }
	inline List_1_t3640412062 * get_U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5() const { return ___U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5; }
	inline List_1_t3640412062 ** get_address_of_U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5() { return &___U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5; }
	inline void set_U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5(List_1_t3640412062 * value)
	{
		___U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
