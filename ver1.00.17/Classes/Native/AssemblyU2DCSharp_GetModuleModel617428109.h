﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GetModuleChildPartsListModel>
struct List_1_t2928902997;
// GetModuleRegisterdMotionList
struct GetModuleRegisterdMotionList_t852868807;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetModuleModel
struct  GetModuleModel_t617428109  : public Model_t873752437
{
public:
	// System.Int32 GetModuleModel::<module_Id>k__BackingField
	int32_t ___U3Cmodule_IdU3Ek__BackingField_0;
	// System.Int32 GetModuleModel::<module_parent_Id>k__BackingField
	int32_t ___U3Cmodule_parent_IdU3Ek__BackingField_1;
	// System.Int32 GetModuleModel::<module_coreParts_Id>k__BackingField
	int32_t ___U3Cmodule_coreParts_IdU3Ek__BackingField_2;
	// System.Single GetModuleModel::<module_Rotation_X>k__BackingField
	float ___U3Cmodule_Rotation_XU3Ek__BackingField_3;
	// System.Single GetModuleModel::<module_Rotation_Y>k__BackingField
	float ___U3Cmodule_Rotation_YU3Ek__BackingField_4;
	// System.Single GetModuleModel::<module_Rotation_Z>k__BackingField
	float ___U3Cmodule_Rotation_ZU3Ek__BackingField_5;
	// System.Single GetModuleModel::<module_Scale_X>k__BackingField
	float ___U3Cmodule_Scale_XU3Ek__BackingField_6;
	// System.Single GetModuleModel::<module_Scale_Y>k__BackingField
	float ___U3Cmodule_Scale_YU3Ek__BackingField_7;
	// System.Single GetModuleModel::<module_Scale_Z>k__BackingField
	float ___U3Cmodule_Scale_ZU3Ek__BackingField_8;
	// System.String GetModuleModel::<module_coreParts_Name>k__BackingField
	String_t* ___U3Cmodule_coreParts_NameU3Ek__BackingField_9;
	// System.Int32 GetModuleModel::<module_coreParts_ColorId>k__BackingField
	int32_t ___U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10;
	// System.Int32 GetModuleModel::<module_createUserId>k__BackingField
	int32_t ___U3Cmodule_createUserIdU3Ek__BackingField_11;
	// System.Collections.Generic.List`1<GetModuleChildPartsListModel> GetModuleModel::<_module_childParts_list>k__BackingField
	List_1_t2928902997 * ___U3C_module_childParts_listU3Ek__BackingField_12;
	// GetModuleRegisterdMotionList GetModuleModel::<_module_registerMotion_Data>k__BackingField
	GetModuleRegisterdMotionList_t852868807 * ___U3C_module_registerMotion_DataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3Cmodule_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cmodule_IdU3Ek__BackingField_0() const { return ___U3Cmodule_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cmodule_IdU3Ek__BackingField_0() { return &___U3Cmodule_IdU3Ek__BackingField_0; }
	inline void set_U3Cmodule_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cmodule_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_parent_IdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_parent_IdU3Ek__BackingField_1)); }
	inline int32_t get_U3Cmodule_parent_IdU3Ek__BackingField_1() const { return ___U3Cmodule_parent_IdU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cmodule_parent_IdU3Ek__BackingField_1() { return &___U3Cmodule_parent_IdU3Ek__BackingField_1; }
	inline void set_U3Cmodule_parent_IdU3Ek__BackingField_1(int32_t value)
	{
		___U3Cmodule_parent_IdU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_coreParts_IdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_coreParts_IdU3Ek__BackingField_2)); }
	inline int32_t get_U3Cmodule_coreParts_IdU3Ek__BackingField_2() const { return ___U3Cmodule_coreParts_IdU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3Cmodule_coreParts_IdU3Ek__BackingField_2() { return &___U3Cmodule_coreParts_IdU3Ek__BackingField_2; }
	inline void set_U3Cmodule_coreParts_IdU3Ek__BackingField_2(int32_t value)
	{
		___U3Cmodule_coreParts_IdU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_Rotation_XU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_Rotation_XU3Ek__BackingField_3)); }
	inline float get_U3Cmodule_Rotation_XU3Ek__BackingField_3() const { return ___U3Cmodule_Rotation_XU3Ek__BackingField_3; }
	inline float* get_address_of_U3Cmodule_Rotation_XU3Ek__BackingField_3() { return &___U3Cmodule_Rotation_XU3Ek__BackingField_3; }
	inline void set_U3Cmodule_Rotation_XU3Ek__BackingField_3(float value)
	{
		___U3Cmodule_Rotation_XU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_Rotation_YU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_Rotation_YU3Ek__BackingField_4)); }
	inline float get_U3Cmodule_Rotation_YU3Ek__BackingField_4() const { return ___U3Cmodule_Rotation_YU3Ek__BackingField_4; }
	inline float* get_address_of_U3Cmodule_Rotation_YU3Ek__BackingField_4() { return &___U3Cmodule_Rotation_YU3Ek__BackingField_4; }
	inline void set_U3Cmodule_Rotation_YU3Ek__BackingField_4(float value)
	{
		___U3Cmodule_Rotation_YU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_Rotation_ZU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_Rotation_ZU3Ek__BackingField_5)); }
	inline float get_U3Cmodule_Rotation_ZU3Ek__BackingField_5() const { return ___U3Cmodule_Rotation_ZU3Ek__BackingField_5; }
	inline float* get_address_of_U3Cmodule_Rotation_ZU3Ek__BackingField_5() { return &___U3Cmodule_Rotation_ZU3Ek__BackingField_5; }
	inline void set_U3Cmodule_Rotation_ZU3Ek__BackingField_5(float value)
	{
		___U3Cmodule_Rotation_ZU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_Scale_XU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_Scale_XU3Ek__BackingField_6)); }
	inline float get_U3Cmodule_Scale_XU3Ek__BackingField_6() const { return ___U3Cmodule_Scale_XU3Ek__BackingField_6; }
	inline float* get_address_of_U3Cmodule_Scale_XU3Ek__BackingField_6() { return &___U3Cmodule_Scale_XU3Ek__BackingField_6; }
	inline void set_U3Cmodule_Scale_XU3Ek__BackingField_6(float value)
	{
		___U3Cmodule_Scale_XU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_Scale_YU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_Scale_YU3Ek__BackingField_7)); }
	inline float get_U3Cmodule_Scale_YU3Ek__BackingField_7() const { return ___U3Cmodule_Scale_YU3Ek__BackingField_7; }
	inline float* get_address_of_U3Cmodule_Scale_YU3Ek__BackingField_7() { return &___U3Cmodule_Scale_YU3Ek__BackingField_7; }
	inline void set_U3Cmodule_Scale_YU3Ek__BackingField_7(float value)
	{
		___U3Cmodule_Scale_YU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_Scale_ZU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_Scale_ZU3Ek__BackingField_8)); }
	inline float get_U3Cmodule_Scale_ZU3Ek__BackingField_8() const { return ___U3Cmodule_Scale_ZU3Ek__BackingField_8; }
	inline float* get_address_of_U3Cmodule_Scale_ZU3Ek__BackingField_8() { return &___U3Cmodule_Scale_ZU3Ek__BackingField_8; }
	inline void set_U3Cmodule_Scale_ZU3Ek__BackingField_8(float value)
	{
		___U3Cmodule_Scale_ZU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_coreParts_NameU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_coreParts_NameU3Ek__BackingField_9)); }
	inline String_t* get_U3Cmodule_coreParts_NameU3Ek__BackingField_9() const { return ___U3Cmodule_coreParts_NameU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3Cmodule_coreParts_NameU3Ek__BackingField_9() { return &___U3Cmodule_coreParts_NameU3Ek__BackingField_9; }
	inline void set_U3Cmodule_coreParts_NameU3Ek__BackingField_9(String_t* value)
	{
		___U3Cmodule_coreParts_NameU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmodule_coreParts_NameU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10)); }
	inline int32_t get_U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10() const { return ___U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10() { return &___U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10; }
	inline void set_U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10(int32_t value)
	{
		___U3Cmodule_coreParts_ColorIdU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_createUserIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3Cmodule_createUserIdU3Ek__BackingField_11)); }
	inline int32_t get_U3Cmodule_createUserIdU3Ek__BackingField_11() const { return ___U3Cmodule_createUserIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3Cmodule_createUserIdU3Ek__BackingField_11() { return &___U3Cmodule_createUserIdU3Ek__BackingField_11; }
	inline void set_U3Cmodule_createUserIdU3Ek__BackingField_11(int32_t value)
	{
		___U3Cmodule_createUserIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3C_module_childParts_listU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3C_module_childParts_listU3Ek__BackingField_12)); }
	inline List_1_t2928902997 * get_U3C_module_childParts_listU3Ek__BackingField_12() const { return ___U3C_module_childParts_listU3Ek__BackingField_12; }
	inline List_1_t2928902997 ** get_address_of_U3C_module_childParts_listU3Ek__BackingField_12() { return &___U3C_module_childParts_listU3Ek__BackingField_12; }
	inline void set_U3C_module_childParts_listU3Ek__BackingField_12(List_1_t2928902997 * value)
	{
		___U3C_module_childParts_listU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_module_childParts_listU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3C_module_registerMotion_DataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetModuleModel_t617428109, ___U3C_module_registerMotion_DataU3Ek__BackingField_13)); }
	inline GetModuleRegisterdMotionList_t852868807 * get_U3C_module_registerMotion_DataU3Ek__BackingField_13() const { return ___U3C_module_registerMotion_DataU3Ek__BackingField_13; }
	inline GetModuleRegisterdMotionList_t852868807 ** get_address_of_U3C_module_registerMotion_DataU3Ek__BackingField_13() { return &___U3C_module_registerMotion_DataU3Ek__BackingField_13; }
	inline void set_U3C_module_registerMotion_DataU3Ek__BackingField_13(GetModuleRegisterdMotionList_t852868807 * value)
	{
		___U3C_module_registerMotion_DataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_module_registerMotion_DataU3Ek__BackingField_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
