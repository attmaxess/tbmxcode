﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetModuleRegisterMotionListPoseData
struct  GetModuleRegisterMotionListPoseData_t154016370  : public Model_t873752437
{
public:
	// System.Single GetModuleRegisterMotionListPoseData::<module_registerdMotionList_motionData_poseData_poseKeyTime>k__BackingField
	float ___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0;
	// System.Single GetModuleRegisterMotionListPoseData::<module_registerdMotionList_motionData_poseData_poseKeyValue>k__BackingField
	float ___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetModuleRegisterMotionListPoseData_t154016370, ___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0)); }
	inline float get_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0() const { return ___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0() { return &___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0; }
	inline void set_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0(float value)
	{
		___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetModuleRegisterMotionListPoseData_t154016370, ___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1)); }
	inline float get_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1() const { return ___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1; }
	inline float* get_address_of_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1() { return &___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1; }
	inline void set_U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1(float value)
	{
		___U3Cmodule_registerdMotionList_motionData_poseData_poseKeyValueU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
