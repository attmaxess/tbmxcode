﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<GetModuleRegisterMotionListMotionData>
struct List_1_t1678879873;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetModuleRegisterdMotionList
struct  GetModuleRegisterdMotionList_t852868807  : public Model_t873752437
{
public:
	// System.Int32 GetModuleRegisterdMotionList::<module_registerdMotionList_motion_Id>k__BackingField
	int32_t ___U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0;
	// System.Int32 GetModuleRegisterdMotionList::<module_registerdMotionList_action_Id>k__BackingField
	int32_t ___U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<GetModuleRegisterMotionListMotionData> GetModuleRegisterdMotionList::<_module_registerMotionList_motionData>k__BackingField
	List_1_t1678879873 * ___U3C_module_registerMotionList_motionDataU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetModuleRegisterdMotionList_t852868807, ___U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0() const { return ___U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0() { return &___U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0; }
	inline void set_U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cmodule_registerdMotionList_motion_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetModuleRegisterdMotionList_t852868807, ___U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1)); }
	inline int32_t get_U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1() const { return ___U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1() { return &___U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1; }
	inline void set_U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1(int32_t value)
	{
		___U3Cmodule_registerdMotionList_action_IdU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3C_module_registerMotionList_motionDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetModuleRegisterdMotionList_t852868807, ___U3C_module_registerMotionList_motionDataU3Ek__BackingField_2)); }
	inline List_1_t1678879873 * get_U3C_module_registerMotionList_motionDataU3Ek__BackingField_2() const { return ___U3C_module_registerMotionList_motionDataU3Ek__BackingField_2; }
	inline List_1_t1678879873 ** get_address_of_U3C_module_registerMotionList_motionDataU3Ek__BackingField_2() { return &___U3C_module_registerMotionList_motionDataU3Ek__BackingField_2; }
	inline void set_U3C_module_registerMotionList_motionDataU3Ek__BackingField_2(List_1_t1678879873 * value)
	{
		___U3C_module_registerMotionList_motionDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_module_registerMotionList_motionDataU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
