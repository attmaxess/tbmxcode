﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<GetMotionModel>
struct List_1_t1527099497;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetMotionBase
struct  GetMotionBase_t403349095  : public Model_t873752437
{
public:
	// System.Int32 GetMotionBase::<mobility_Id>k__BackingField
	int32_t ___U3Cmobility_IdU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<GetMotionModel> GetMotionBase::<motionList>k__BackingField
	List_1_t1527099497 * ___U3CmotionListU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cmobility_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetMotionBase_t403349095, ___U3Cmobility_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cmobility_IdU3Ek__BackingField_0() const { return ___U3Cmobility_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cmobility_IdU3Ek__BackingField_0() { return &___U3Cmobility_IdU3Ek__BackingField_0; }
	inline void set_U3Cmobility_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cmobility_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CmotionListU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetMotionBase_t403349095, ___U3CmotionListU3Ek__BackingField_1)); }
	inline List_1_t1527099497 * get_U3CmotionListU3Ek__BackingField_1() const { return ___U3CmotionListU3Ek__BackingField_1; }
	inline List_1_t1527099497 ** get_address_of_U3CmotionListU3Ek__BackingField_1() { return &___U3CmotionListU3Ek__BackingField_1; }
	inline void set_U3CmotionListU3Ek__BackingField_1(List_1_t1527099497 * value)
	{
		___U3CmotionListU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmotionListU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
