﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GetPoseDataModel>
struct List_1_t2493094650;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetMotionDataModel
struct  GetMotionDataModel_t3220649645  : public Model_t873752437
{
public:
	// System.Int32 GetMotionDataModel::<motionData_pose_Id>k__BackingField
	int32_t ___U3CmotionData_pose_IdU3Ek__BackingField_0;
	// System.String GetMotionDataModel::<motionData_pose_Path>k__BackingField
	String_t* ___U3CmotionData_pose_PathU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<GetPoseDataModel> GetMotionDataModel::<motionData_poseData>k__BackingField
	List_1_t2493094650 * ___U3CmotionData_poseDataU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmotionData_pose_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetMotionDataModel_t3220649645, ___U3CmotionData_pose_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3CmotionData_pose_IdU3Ek__BackingField_0() const { return ___U3CmotionData_pose_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CmotionData_pose_IdU3Ek__BackingField_0() { return &___U3CmotionData_pose_IdU3Ek__BackingField_0; }
	inline void set_U3CmotionData_pose_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3CmotionData_pose_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CmotionData_pose_PathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetMotionDataModel_t3220649645, ___U3CmotionData_pose_PathU3Ek__BackingField_1)); }
	inline String_t* get_U3CmotionData_pose_PathU3Ek__BackingField_1() const { return ___U3CmotionData_pose_PathU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmotionData_pose_PathU3Ek__BackingField_1() { return &___U3CmotionData_pose_PathU3Ek__BackingField_1; }
	inline void set_U3CmotionData_pose_PathU3Ek__BackingField_1(String_t* value)
	{
		___U3CmotionData_pose_PathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmotionData_pose_PathU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CmotionData_poseDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetMotionDataModel_t3220649645, ___U3CmotionData_poseDataU3Ek__BackingField_2)); }
	inline List_1_t2493094650 * get_U3CmotionData_poseDataU3Ek__BackingField_2() const { return ___U3CmotionData_poseDataU3Ek__BackingField_2; }
	inline List_1_t2493094650 ** get_address_of_U3CmotionData_poseDataU3Ek__BackingField_2() { return &___U3CmotionData_poseDataU3Ek__BackingField_2; }
	inline void set_U3CmotionData_poseDataU3Ek__BackingField_2(List_1_t2493094650 * value)
	{
		___U3CmotionData_poseDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmotionData_poseDataU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
