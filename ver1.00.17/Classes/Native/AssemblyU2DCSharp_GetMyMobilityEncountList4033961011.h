﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetMyMobilityEncountList
struct  GetMyMobilityEncountList_t4033961011  : public Model_t873752437
{
public:
	// System.Int32 GetMyMobilityEncountList::<myMobility_encountList_encountMobility_Id>k__BackingField
	int32_t ___U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0;
	// System.Int32 GetMyMobilityEncountList::<myMobility_encountList_point>k__BackingField
	int32_t ___U3CmyMobility_encountList_pointU3Ek__BackingField_1;
	// System.String GetMyMobilityEncountList::<myMobility_encountList_encount_time>k__BackingField
	String_t* ___U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetMyMobilityEncountList_t4033961011, ___U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0() const { return ___U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0() { return &___U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0; }
	inline void set_U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3CmyMobility_encountList_encountMobility_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_encountList_pointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetMyMobilityEncountList_t4033961011, ___U3CmyMobility_encountList_pointU3Ek__BackingField_1)); }
	inline int32_t get_U3CmyMobility_encountList_pointU3Ek__BackingField_1() const { return ___U3CmyMobility_encountList_pointU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CmyMobility_encountList_pointU3Ek__BackingField_1() { return &___U3CmyMobility_encountList_pointU3Ek__BackingField_1; }
	inline void set_U3CmyMobility_encountList_pointU3Ek__BackingField_1(int32_t value)
	{
		___U3CmyMobility_encountList_pointU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetMyMobilityEncountList_t4033961011, ___U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2)); }
	inline String_t* get_U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2() const { return ___U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2() { return &___U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2; }
	inline void set_U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2(String_t* value)
	{
		___U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_encountList_encount_timeU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
