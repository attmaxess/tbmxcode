﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GetNearmobRegisterMotionListPoseData>
struct List_1_t981755702;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetNearmobRegisterMotionListMotionData
struct  GetNearmobRegisterMotionListMotionData_t1259926239  : public Model_t873752437
{
public:
	// System.Int32 GetNearmobRegisterMotionListMotionData::<nearmob_registerdMotionList_motionData_pose_Id>k__BackingField
	int32_t ___U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0;
	// System.String GetNearmobRegisterMotionListMotionData::<nearmob_registerdMotionList_motionData_pose_Path>k__BackingField
	String_t* ___U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<GetNearmobRegisterMotionListPoseData> GetNearmobRegisterMotionListMotionData::<_nearmob_registerMotionList_poseData>k__BackingField
	List_1_t981755702 * ___U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetNearmobRegisterMotionListMotionData_t1259926239, ___U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0() const { return ___U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0() { return &___U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0; }
	inline void set_U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cnearmob_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetNearmobRegisterMotionListMotionData_t1259926239, ___U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1)); }
	inline String_t* get_U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1() const { return ___U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1() { return &___U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1; }
	inline void set_U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1(String_t* value)
	{
		___U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_registerdMotionList_motionData_pose_PathU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetNearmobRegisterMotionListMotionData_t1259926239, ___U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2)); }
	inline List_1_t981755702 * get_U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2() const { return ___U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2; }
	inline List_1_t981755702 ** get_address_of_U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2() { return &___U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2; }
	inline void set_U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2(List_1_t981755702 * value)
	{
		___U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_nearmob_registerMotionList_poseDataU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
