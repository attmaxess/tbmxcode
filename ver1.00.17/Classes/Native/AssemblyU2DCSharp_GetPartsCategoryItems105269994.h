﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetPartsCategoryItems
struct  GetPartsCategoryItems_t105269994  : public Model_t873752437
{
public:
	// System.Int32 GetPartsCategoryItems::<partsCategory_partsCat_Id>k__BackingField
	int32_t ___U3CpartsCategory_partsCat_IdU3Ek__BackingField_0;
	// System.String GetPartsCategoryItems::<partsCategory_partsCat_name>k__BackingField
	String_t* ___U3CpartsCategory_partsCat_nameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CpartsCategory_partsCat_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetPartsCategoryItems_t105269994, ___U3CpartsCategory_partsCat_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3CpartsCategory_partsCat_IdU3Ek__BackingField_0() const { return ___U3CpartsCategory_partsCat_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CpartsCategory_partsCat_IdU3Ek__BackingField_0() { return &___U3CpartsCategory_partsCat_IdU3Ek__BackingField_0; }
	inline void set_U3CpartsCategory_partsCat_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3CpartsCategory_partsCat_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CpartsCategory_partsCat_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetPartsCategoryItems_t105269994, ___U3CpartsCategory_partsCat_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3CpartsCategory_partsCat_nameU3Ek__BackingField_1() const { return ___U3CpartsCategory_partsCat_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CpartsCategory_partsCat_nameU3Ek__BackingField_1() { return &___U3CpartsCategory_partsCat_nameU3Ek__BackingField_1; }
	inline void set_U3CpartsCategory_partsCat_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3CpartsCategory_partsCat_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsCategory_partsCat_nameU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
