﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<GetPartsCategoryItems>
struct List_1_t3769358422;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetPartsCategoryModel
struct  GetPartsCategoryModel_t595760411  : public Model_t873752437
{
public:
	// System.Collections.Generic.List`1<GetPartsCategoryItems> GetPartsCategoryModel::<partsCategory_items_list>k__BackingField
	List_1_t3769358422 * ___U3CpartsCategory_items_listU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpartsCategory_items_listU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetPartsCategoryModel_t595760411, ___U3CpartsCategory_items_listU3Ek__BackingField_0)); }
	inline List_1_t3769358422 * get_U3CpartsCategory_items_listU3Ek__BackingField_0() const { return ___U3CpartsCategory_items_listU3Ek__BackingField_0; }
	inline List_1_t3769358422 ** get_address_of_U3CpartsCategory_items_listU3Ek__BackingField_0() { return &___U3CpartsCategory_items_listU3Ek__BackingField_0; }
	inline void set_U3CpartsCategory_items_listU3Ek__BackingField_0(List_1_t3769358422 * value)
	{
		___U3CpartsCategory_items_listU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsCategory_items_listU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
