﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetPartsList
struct  GetPartsList_t1367059906  : public Model_t873752437
{
public:
	// System.Int32 GetPartsList::<parts_Id>k__BackingField
	int32_t ___U3Cparts_IdU3Ek__BackingField_0;
	// System.String GetPartsList::<parts_name>k__BackingField
	String_t* ___U3Cparts_nameU3Ek__BackingField_1;
	// System.String GetPartsList::<parts_description>k__BackingField
	String_t* ___U3Cparts_descriptionU3Ek__BackingField_2;
	// System.String GetPartsList::<parts_category>k__BackingField
	String_t* ___U3Cparts_categoryU3Ek__BackingField_3;
	// System.Single GetPartsList::<parts_parameter_mass>k__BackingField
	float ___U3Cparts_parameter_massU3Ek__BackingField_4;
	// System.Single GetPartsList::<parts_parameter_force>k__BackingField
	float ___U3Cparts_parameter_forceU3Ek__BackingField_5;
	// System.Single GetPartsList::<parts_parameter_bounce>k__BackingField
	float ___U3Cparts_parameter_bounceU3Ek__BackingField_6;
	// System.Single GetPartsList::<parts_parameter_floatForce>k__BackingField
	float ___U3Cparts_parameter_floatForceU3Ek__BackingField_7;
	// System.Single GetPartsList::<parts_parameter_floatBounce>k__BackingField
	float ___U3Cparts_parameter_floatBounceU3Ek__BackingField_8;
	// System.Single GetPartsList::<parts_parameter_floatFriction>k__BackingField
	float ___U3Cparts_parameter_floatFrictionU3Ek__BackingField_9;
	// System.Single GetPartsList::<parts_parameter_friction>k__BackingField
	float ___U3Cparts_parameter_frictionU3Ek__BackingField_10;
	// System.Single GetPartsList::<parts_parameter_elasticitySpeed>k__BackingField
	float ___U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11;
	// System.Single GetPartsList::<parts_parameter_rotationSpeed>k__BackingField
	float ___U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3Cparts_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cparts_IdU3Ek__BackingField_0() const { return ___U3Cparts_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cparts_IdU3Ek__BackingField_0() { return &___U3Cparts_IdU3Ek__BackingField_0; }
	inline void set_U3Cparts_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cparts_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3Cparts_nameU3Ek__BackingField_1() const { return ___U3Cparts_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cparts_nameU3Ek__BackingField_1() { return &___U3Cparts_nameU3Ek__BackingField_1; }
	inline void set_U3Cparts_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3Cparts_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cparts_nameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3Cparts_descriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_descriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3Cparts_descriptionU3Ek__BackingField_2() const { return ___U3Cparts_descriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cparts_descriptionU3Ek__BackingField_2() { return &___U3Cparts_descriptionU3Ek__BackingField_2; }
	inline void set_U3Cparts_descriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3Cparts_descriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cparts_descriptionU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3Cparts_categoryU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_categoryU3Ek__BackingField_3)); }
	inline String_t* get_U3Cparts_categoryU3Ek__BackingField_3() const { return ___U3Cparts_categoryU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cparts_categoryU3Ek__BackingField_3() { return &___U3Cparts_categoryU3Ek__BackingField_3; }
	inline void set_U3Cparts_categoryU3Ek__BackingField_3(String_t* value)
	{
		___U3Cparts_categoryU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cparts_categoryU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_massU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_massU3Ek__BackingField_4)); }
	inline float get_U3Cparts_parameter_massU3Ek__BackingField_4() const { return ___U3Cparts_parameter_massU3Ek__BackingField_4; }
	inline float* get_address_of_U3Cparts_parameter_massU3Ek__BackingField_4() { return &___U3Cparts_parameter_massU3Ek__BackingField_4; }
	inline void set_U3Cparts_parameter_massU3Ek__BackingField_4(float value)
	{
		___U3Cparts_parameter_massU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_forceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_forceU3Ek__BackingField_5)); }
	inline float get_U3Cparts_parameter_forceU3Ek__BackingField_5() const { return ___U3Cparts_parameter_forceU3Ek__BackingField_5; }
	inline float* get_address_of_U3Cparts_parameter_forceU3Ek__BackingField_5() { return &___U3Cparts_parameter_forceU3Ek__BackingField_5; }
	inline void set_U3Cparts_parameter_forceU3Ek__BackingField_5(float value)
	{
		___U3Cparts_parameter_forceU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_bounceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_bounceU3Ek__BackingField_6)); }
	inline float get_U3Cparts_parameter_bounceU3Ek__BackingField_6() const { return ___U3Cparts_parameter_bounceU3Ek__BackingField_6; }
	inline float* get_address_of_U3Cparts_parameter_bounceU3Ek__BackingField_6() { return &___U3Cparts_parameter_bounceU3Ek__BackingField_6; }
	inline void set_U3Cparts_parameter_bounceU3Ek__BackingField_6(float value)
	{
		___U3Cparts_parameter_bounceU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_floatForceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_floatForceU3Ek__BackingField_7)); }
	inline float get_U3Cparts_parameter_floatForceU3Ek__BackingField_7() const { return ___U3Cparts_parameter_floatForceU3Ek__BackingField_7; }
	inline float* get_address_of_U3Cparts_parameter_floatForceU3Ek__BackingField_7() { return &___U3Cparts_parameter_floatForceU3Ek__BackingField_7; }
	inline void set_U3Cparts_parameter_floatForceU3Ek__BackingField_7(float value)
	{
		___U3Cparts_parameter_floatForceU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_floatBounceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_floatBounceU3Ek__BackingField_8)); }
	inline float get_U3Cparts_parameter_floatBounceU3Ek__BackingField_8() const { return ___U3Cparts_parameter_floatBounceU3Ek__BackingField_8; }
	inline float* get_address_of_U3Cparts_parameter_floatBounceU3Ek__BackingField_8() { return &___U3Cparts_parameter_floatBounceU3Ek__BackingField_8; }
	inline void set_U3Cparts_parameter_floatBounceU3Ek__BackingField_8(float value)
	{
		___U3Cparts_parameter_floatBounceU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_floatFrictionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_floatFrictionU3Ek__BackingField_9)); }
	inline float get_U3Cparts_parameter_floatFrictionU3Ek__BackingField_9() const { return ___U3Cparts_parameter_floatFrictionU3Ek__BackingField_9; }
	inline float* get_address_of_U3Cparts_parameter_floatFrictionU3Ek__BackingField_9() { return &___U3Cparts_parameter_floatFrictionU3Ek__BackingField_9; }
	inline void set_U3Cparts_parameter_floatFrictionU3Ek__BackingField_9(float value)
	{
		___U3Cparts_parameter_floatFrictionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_frictionU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_frictionU3Ek__BackingField_10)); }
	inline float get_U3Cparts_parameter_frictionU3Ek__BackingField_10() const { return ___U3Cparts_parameter_frictionU3Ek__BackingField_10; }
	inline float* get_address_of_U3Cparts_parameter_frictionU3Ek__BackingField_10() { return &___U3Cparts_parameter_frictionU3Ek__BackingField_10; }
	inline void set_U3Cparts_parameter_frictionU3Ek__BackingField_10(float value)
	{
		___U3Cparts_parameter_frictionU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11)); }
	inline float get_U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11() const { return ___U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11; }
	inline float* get_address_of_U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11() { return &___U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11; }
	inline void set_U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11(float value)
	{
		___U3Cparts_parameter_elasticitySpeedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetPartsList_t1367059906, ___U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12)); }
	inline float get_U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12() const { return ___U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12; }
	inline float* get_address_of_U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12() { return &___U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12; }
	inline void set_U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12(float value)
	{
		___U3Cparts_parameter_rotationSpeedU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
