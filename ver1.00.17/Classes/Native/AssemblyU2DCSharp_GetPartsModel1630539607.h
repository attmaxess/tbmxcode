﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.Collections.Generic.List`1<GetPartsList>
struct List_1_t736181038;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetPartsModel
struct  GetPartsModel_t1630539607  : public Model_t873752437
{
public:
	// System.Collections.Generic.List`1<GetPartsList> GetPartsModel::<_parts__list>k__BackingField
	List_1_t736181038 * ___U3C_parts__listU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3C_parts__listU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetPartsModel_t1630539607, ___U3C_parts__listU3Ek__BackingField_0)); }
	inline List_1_t736181038 * get_U3C_parts__listU3Ek__BackingField_0() const { return ___U3C_parts__listU3Ek__BackingField_0; }
	inline List_1_t736181038 ** get_address_of_U3C_parts__listU3Ek__BackingField_0() { return &___U3C_parts__listU3Ek__BackingField_0; }
	inline void set_U3C_parts__listU3Ek__BackingField_0(List_1_t736181038 * value)
	{
		___U3C_parts__listU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_parts__listU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
