﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetReactionModel
struct  GetReactionModel_t3327617864  : public Model_t873752437
{
public:
	// System.Int32 GetReactionModel::<_feed_Reaction_AllCount>k__BackingField
	int32_t ___U3C_feed_Reaction_AllCountU3Ek__BackingField_0;
	// System.String GetReactionModel::<_feed_Funny>k__BackingField
	String_t* ___U3C_feed_FunnyU3Ek__BackingField_1;
	// System.String GetReactionModel::<_feed_Amazing>k__BackingField
	String_t* ___U3C_feed_AmazingU3Ek__BackingField_2;
	// System.String GetReactionModel::<_feed_Cute>k__BackingField
	String_t* ___U3C_feed_CuteU3Ek__BackingField_3;
	// System.String GetReactionModel::<_feed_Cool>k__BackingField
	String_t* ___U3C_feed_CoolU3Ek__BackingField_4;
	// System.String GetReactionModel::<_feed_Like>k__BackingField
	String_t* ___U3C_feed_LikeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3C_feed_Reaction_AllCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetReactionModel_t3327617864, ___U3C_feed_Reaction_AllCountU3Ek__BackingField_0)); }
	inline int32_t get_U3C_feed_Reaction_AllCountU3Ek__BackingField_0() const { return ___U3C_feed_Reaction_AllCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3C_feed_Reaction_AllCountU3Ek__BackingField_0() { return &___U3C_feed_Reaction_AllCountU3Ek__BackingField_0; }
	inline void set_U3C_feed_Reaction_AllCountU3Ek__BackingField_0(int32_t value)
	{
		___U3C_feed_Reaction_AllCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3C_feed_FunnyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetReactionModel_t3327617864, ___U3C_feed_FunnyU3Ek__BackingField_1)); }
	inline String_t* get_U3C_feed_FunnyU3Ek__BackingField_1() const { return ___U3C_feed_FunnyU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3C_feed_FunnyU3Ek__BackingField_1() { return &___U3C_feed_FunnyU3Ek__BackingField_1; }
	inline void set_U3C_feed_FunnyU3Ek__BackingField_1(String_t* value)
	{
		___U3C_feed_FunnyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_FunnyU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3C_feed_AmazingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetReactionModel_t3327617864, ___U3C_feed_AmazingU3Ek__BackingField_2)); }
	inline String_t* get_U3C_feed_AmazingU3Ek__BackingField_2() const { return ___U3C_feed_AmazingU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3C_feed_AmazingU3Ek__BackingField_2() { return &___U3C_feed_AmazingU3Ek__BackingField_2; }
	inline void set_U3C_feed_AmazingU3Ek__BackingField_2(String_t* value)
	{
		___U3C_feed_AmazingU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_AmazingU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3C_feed_CuteU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetReactionModel_t3327617864, ___U3C_feed_CuteU3Ek__BackingField_3)); }
	inline String_t* get_U3C_feed_CuteU3Ek__BackingField_3() const { return ___U3C_feed_CuteU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3C_feed_CuteU3Ek__BackingField_3() { return &___U3C_feed_CuteU3Ek__BackingField_3; }
	inline void set_U3C_feed_CuteU3Ek__BackingField_3(String_t* value)
	{
		___U3C_feed_CuteU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_CuteU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3C_feed_CoolU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetReactionModel_t3327617864, ___U3C_feed_CoolU3Ek__BackingField_4)); }
	inline String_t* get_U3C_feed_CoolU3Ek__BackingField_4() const { return ___U3C_feed_CoolU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3C_feed_CoolU3Ek__BackingField_4() { return &___U3C_feed_CoolU3Ek__BackingField_4; }
	inline void set_U3C_feed_CoolU3Ek__BackingField_4(String_t* value)
	{
		___U3C_feed_CoolU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_CoolU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3C_feed_LikeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetReactionModel_t3327617864, ___U3C_feed_LikeU3Ek__BackingField_5)); }
	inline String_t* get_U3C_feed_LikeU3Ek__BackingField_5() const { return ___U3C_feed_LikeU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3C_feed_LikeU3Ek__BackingField_5() { return &___U3C_feed_LikeU3Ek__BackingField_5; }
	inline void set_U3C_feed_LikeU3Ek__BackingField_5(String_t* value)
	{
		___U3C_feed_LikeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_feed_LikeU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
