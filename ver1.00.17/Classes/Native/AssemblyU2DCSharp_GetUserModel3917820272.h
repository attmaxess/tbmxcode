﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetUserModel
struct  GetUserModel_t3917820272  : public Model_t873752437
{
public:
	// System.Int32 GetUserModel::<user_Id>k__BackingField
	int32_t ___U3Cuser_IdU3Ek__BackingField_0;
	// System.String GetUserModel::<user_name>k__BackingField
	String_t* ___U3Cuser_nameU3Ek__BackingField_1;
	// System.Int32 GetUserModel::<user_country_No>k__BackingField
	int32_t ___U3Cuser_country_NoU3Ek__BackingField_2;
	// System.Int32 GetUserModel::<user_language_No>k__BackingField
	int32_t ___U3Cuser_language_NoU3Ek__BackingField_3;
	// System.String GetUserModel::<user_rank>k__BackingField
	String_t* ___U3Cuser_rankU3Ek__BackingField_4;
	// System.Int32 GetUserModel::<user_rankup_point>k__BackingField
	int32_t ___U3Cuser_rankup_pointU3Ek__BackingField_5;
	// System.Int32 GetUserModel::<user_point>k__BackingField
	int32_t ___U3Cuser_pointU3Ek__BackingField_6;
	// System.Int32 GetUserModel::<user_mobility_count>k__BackingField
	int32_t ___U3Cuser_mobility_countU3Ek__BackingField_7;
	// System.Int32 GetUserModel::<user_copied_count>k__BackingField
	int32_t ___U3Cuser_copied_countU3Ek__BackingField_8;
	// System.Int32 GetUserModel::<user_main_mobId>k__BackingField
	int32_t ___U3Cuser_main_mobIdU3Ek__BackingField_9;
	// System.Single GetUserModel::<user_startPosition_X>k__BackingField
	float ___U3Cuser_startPosition_XU3Ek__BackingField_10;
	// System.Single GetUserModel::<user_startPosition_Y>k__BackingField
	float ___U3Cuser_startPosition_YU3Ek__BackingField_11;
	// System.Single GetUserModel::<user_startPosition_Z>k__BackingField
	float ___U3Cuser_startPosition_ZU3Ek__BackingField_12;
	// System.Single GetUserModel::<user_startRotation_X>k__BackingField
	float ___U3Cuser_startRotation_XU3Ek__BackingField_13;
	// System.Single GetUserModel::<user_startRotation_Y>k__BackingField
	float ___U3Cuser_startRotation_YU3Ek__BackingField_14;
	// System.Single GetUserModel::<user_startRotation_Z>k__BackingField
	float ___U3Cuser_startRotation_ZU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3Cuser_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cuser_IdU3Ek__BackingField_0() const { return ___U3Cuser_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cuser_IdU3Ek__BackingField_0() { return &___U3Cuser_IdU3Ek__BackingField_0; }
	inline void set_U3Cuser_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cuser_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3Cuser_nameU3Ek__BackingField_1() const { return ___U3Cuser_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cuser_nameU3Ek__BackingField_1() { return &___U3Cuser_nameU3Ek__BackingField_1; }
	inline void set_U3Cuser_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3Cuser_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cuser_nameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3Cuser_country_NoU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_country_NoU3Ek__BackingField_2)); }
	inline int32_t get_U3Cuser_country_NoU3Ek__BackingField_2() const { return ___U3Cuser_country_NoU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3Cuser_country_NoU3Ek__BackingField_2() { return &___U3Cuser_country_NoU3Ek__BackingField_2; }
	inline void set_U3Cuser_country_NoU3Ek__BackingField_2(int32_t value)
	{
		___U3Cuser_country_NoU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_language_NoU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_language_NoU3Ek__BackingField_3)); }
	inline int32_t get_U3Cuser_language_NoU3Ek__BackingField_3() const { return ___U3Cuser_language_NoU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3Cuser_language_NoU3Ek__BackingField_3() { return &___U3Cuser_language_NoU3Ek__BackingField_3; }
	inline void set_U3Cuser_language_NoU3Ek__BackingField_3(int32_t value)
	{
		___U3Cuser_language_NoU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_rankU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_rankU3Ek__BackingField_4)); }
	inline String_t* get_U3Cuser_rankU3Ek__BackingField_4() const { return ___U3Cuser_rankU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cuser_rankU3Ek__BackingField_4() { return &___U3Cuser_rankU3Ek__BackingField_4; }
	inline void set_U3Cuser_rankU3Ek__BackingField_4(String_t* value)
	{
		___U3Cuser_rankU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cuser_rankU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3Cuser_rankup_pointU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_rankup_pointU3Ek__BackingField_5)); }
	inline int32_t get_U3Cuser_rankup_pointU3Ek__BackingField_5() const { return ___U3Cuser_rankup_pointU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3Cuser_rankup_pointU3Ek__BackingField_5() { return &___U3Cuser_rankup_pointU3Ek__BackingField_5; }
	inline void set_U3Cuser_rankup_pointU3Ek__BackingField_5(int32_t value)
	{
		___U3Cuser_rankup_pointU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_pointU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_pointU3Ek__BackingField_6)); }
	inline int32_t get_U3Cuser_pointU3Ek__BackingField_6() const { return ___U3Cuser_pointU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3Cuser_pointU3Ek__BackingField_6() { return &___U3Cuser_pointU3Ek__BackingField_6; }
	inline void set_U3Cuser_pointU3Ek__BackingField_6(int32_t value)
	{
		___U3Cuser_pointU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_mobility_countU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_mobility_countU3Ek__BackingField_7)); }
	inline int32_t get_U3Cuser_mobility_countU3Ek__BackingField_7() const { return ___U3Cuser_mobility_countU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3Cuser_mobility_countU3Ek__BackingField_7() { return &___U3Cuser_mobility_countU3Ek__BackingField_7; }
	inline void set_U3Cuser_mobility_countU3Ek__BackingField_7(int32_t value)
	{
		___U3Cuser_mobility_countU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_copied_countU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_copied_countU3Ek__BackingField_8)); }
	inline int32_t get_U3Cuser_copied_countU3Ek__BackingField_8() const { return ___U3Cuser_copied_countU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3Cuser_copied_countU3Ek__BackingField_8() { return &___U3Cuser_copied_countU3Ek__BackingField_8; }
	inline void set_U3Cuser_copied_countU3Ek__BackingField_8(int32_t value)
	{
		___U3Cuser_copied_countU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_main_mobIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_main_mobIdU3Ek__BackingField_9)); }
	inline int32_t get_U3Cuser_main_mobIdU3Ek__BackingField_9() const { return ___U3Cuser_main_mobIdU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3Cuser_main_mobIdU3Ek__BackingField_9() { return &___U3Cuser_main_mobIdU3Ek__BackingField_9; }
	inline void set_U3Cuser_main_mobIdU3Ek__BackingField_9(int32_t value)
	{
		___U3Cuser_main_mobIdU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_startPosition_XU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_startPosition_XU3Ek__BackingField_10)); }
	inline float get_U3Cuser_startPosition_XU3Ek__BackingField_10() const { return ___U3Cuser_startPosition_XU3Ek__BackingField_10; }
	inline float* get_address_of_U3Cuser_startPosition_XU3Ek__BackingField_10() { return &___U3Cuser_startPosition_XU3Ek__BackingField_10; }
	inline void set_U3Cuser_startPosition_XU3Ek__BackingField_10(float value)
	{
		___U3Cuser_startPosition_XU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_startPosition_YU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_startPosition_YU3Ek__BackingField_11)); }
	inline float get_U3Cuser_startPosition_YU3Ek__BackingField_11() const { return ___U3Cuser_startPosition_YU3Ek__BackingField_11; }
	inline float* get_address_of_U3Cuser_startPosition_YU3Ek__BackingField_11() { return &___U3Cuser_startPosition_YU3Ek__BackingField_11; }
	inline void set_U3Cuser_startPosition_YU3Ek__BackingField_11(float value)
	{
		___U3Cuser_startPosition_YU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_startPosition_ZU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_startPosition_ZU3Ek__BackingField_12)); }
	inline float get_U3Cuser_startPosition_ZU3Ek__BackingField_12() const { return ___U3Cuser_startPosition_ZU3Ek__BackingField_12; }
	inline float* get_address_of_U3Cuser_startPosition_ZU3Ek__BackingField_12() { return &___U3Cuser_startPosition_ZU3Ek__BackingField_12; }
	inline void set_U3Cuser_startPosition_ZU3Ek__BackingField_12(float value)
	{
		___U3Cuser_startPosition_ZU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_startRotation_XU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_startRotation_XU3Ek__BackingField_13)); }
	inline float get_U3Cuser_startRotation_XU3Ek__BackingField_13() const { return ___U3Cuser_startRotation_XU3Ek__BackingField_13; }
	inline float* get_address_of_U3Cuser_startRotation_XU3Ek__BackingField_13() { return &___U3Cuser_startRotation_XU3Ek__BackingField_13; }
	inline void set_U3Cuser_startRotation_XU3Ek__BackingField_13(float value)
	{
		___U3Cuser_startRotation_XU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_startRotation_YU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_startRotation_YU3Ek__BackingField_14)); }
	inline float get_U3Cuser_startRotation_YU3Ek__BackingField_14() const { return ___U3Cuser_startRotation_YU3Ek__BackingField_14; }
	inline float* get_address_of_U3Cuser_startRotation_YU3Ek__BackingField_14() { return &___U3Cuser_startRotation_YU3Ek__BackingField_14; }
	inline void set_U3Cuser_startRotation_YU3Ek__BackingField_14(float value)
	{
		___U3Cuser_startRotation_YU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3Cuser_startRotation_ZU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GetUserModel_t3917820272, ___U3Cuser_startRotation_ZU3Ek__BackingField_15)); }
	inline float get_U3Cuser_startRotation_ZU3Ek__BackingField_15() const { return ___U3Cuser_startRotation_ZU3Ek__BackingField_15; }
	inline float* get_address_of_U3Cuser_startRotation_ZU3Ek__BackingField_15() { return &___U3Cuser_startRotation_ZU3Ek__BackingField_15; }
	inline void set_U3Cuser_startRotation_ZU3Ek__BackingField_15(float value)
	{
		___U3Cuser_startRotation_ZU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
