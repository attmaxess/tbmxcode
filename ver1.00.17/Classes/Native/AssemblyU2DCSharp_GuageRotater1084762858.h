﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuageRotater
struct  GuageRotater_t1084762858  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform GuageRotater::Outside
	RectTransform_t3349966182 * ___Outside_2;
	// UnityEngine.RectTransform GuageRotater::Inside
	RectTransform_t3349966182 * ___Inside_3;
	// System.Single GuageRotater::OutsideSpeed
	float ___OutsideSpeed_4;
	// System.Single GuageRotater::InsideSpeed
	float ___InsideSpeed_5;
	// UnityEngine.Vector3 GuageRotater::m_vRotOutside
	Vector3_t2243707580  ___m_vRotOutside_6;
	// UnityEngine.Vector3 GuageRotater::m_vRotInside
	Vector3_t2243707580  ___m_vRotInside_7;

public:
	inline static int32_t get_offset_of_Outside_2() { return static_cast<int32_t>(offsetof(GuageRotater_t1084762858, ___Outside_2)); }
	inline RectTransform_t3349966182 * get_Outside_2() const { return ___Outside_2; }
	inline RectTransform_t3349966182 ** get_address_of_Outside_2() { return &___Outside_2; }
	inline void set_Outside_2(RectTransform_t3349966182 * value)
	{
		___Outside_2 = value;
		Il2CppCodeGenWriteBarrier(&___Outside_2, value);
	}

	inline static int32_t get_offset_of_Inside_3() { return static_cast<int32_t>(offsetof(GuageRotater_t1084762858, ___Inside_3)); }
	inline RectTransform_t3349966182 * get_Inside_3() const { return ___Inside_3; }
	inline RectTransform_t3349966182 ** get_address_of_Inside_3() { return &___Inside_3; }
	inline void set_Inside_3(RectTransform_t3349966182 * value)
	{
		___Inside_3 = value;
		Il2CppCodeGenWriteBarrier(&___Inside_3, value);
	}

	inline static int32_t get_offset_of_OutsideSpeed_4() { return static_cast<int32_t>(offsetof(GuageRotater_t1084762858, ___OutsideSpeed_4)); }
	inline float get_OutsideSpeed_4() const { return ___OutsideSpeed_4; }
	inline float* get_address_of_OutsideSpeed_4() { return &___OutsideSpeed_4; }
	inline void set_OutsideSpeed_4(float value)
	{
		___OutsideSpeed_4 = value;
	}

	inline static int32_t get_offset_of_InsideSpeed_5() { return static_cast<int32_t>(offsetof(GuageRotater_t1084762858, ___InsideSpeed_5)); }
	inline float get_InsideSpeed_5() const { return ___InsideSpeed_5; }
	inline float* get_address_of_InsideSpeed_5() { return &___InsideSpeed_5; }
	inline void set_InsideSpeed_5(float value)
	{
		___InsideSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_vRotOutside_6() { return static_cast<int32_t>(offsetof(GuageRotater_t1084762858, ___m_vRotOutside_6)); }
	inline Vector3_t2243707580  get_m_vRotOutside_6() const { return ___m_vRotOutside_6; }
	inline Vector3_t2243707580 * get_address_of_m_vRotOutside_6() { return &___m_vRotOutside_6; }
	inline void set_m_vRotOutside_6(Vector3_t2243707580  value)
	{
		___m_vRotOutside_6 = value;
	}

	inline static int32_t get_offset_of_m_vRotInside_7() { return static_cast<int32_t>(offsetof(GuageRotater_t1084762858, ___m_vRotInside_7)); }
	inline Vector3_t2243707580  get_m_vRotInside_7() const { return ___m_vRotInside_7; }
	inline Vector3_t2243707580 * get_address_of_m_vRotInside_7() { return &___m_vRotInside_7; }
	inline void set_m_vRotInside_7(Vector3_t2243707580  value)
	{
		___m_vRotInside_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
