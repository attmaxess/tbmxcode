﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3948421699;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelloWorldPanel
struct  HelloWorldPanel_t280472022  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text HelloWorldPanel::HelloWorldText
	Text_t356221433 * ___HelloWorldText_2;
	// UnityEngine.RectTransform[] HelloWorldPanel::Frames
	RectTransformU5BU5D_t3948421699* ___Frames_3;
	// System.String HelloWorldPanel::LogoText
	String_t* ___LogoText_4;
	// System.Single HelloWorldPanel::TextSpeed
	float ___TextSpeed_5;
	// System.Boolean HelloWorldPanel::IsComplete
	bool ___IsComplete_6;

public:
	inline static int32_t get_offset_of_HelloWorldText_2() { return static_cast<int32_t>(offsetof(HelloWorldPanel_t280472022, ___HelloWorldText_2)); }
	inline Text_t356221433 * get_HelloWorldText_2() const { return ___HelloWorldText_2; }
	inline Text_t356221433 ** get_address_of_HelloWorldText_2() { return &___HelloWorldText_2; }
	inline void set_HelloWorldText_2(Text_t356221433 * value)
	{
		___HelloWorldText_2 = value;
		Il2CppCodeGenWriteBarrier(&___HelloWorldText_2, value);
	}

	inline static int32_t get_offset_of_Frames_3() { return static_cast<int32_t>(offsetof(HelloWorldPanel_t280472022, ___Frames_3)); }
	inline RectTransformU5BU5D_t3948421699* get_Frames_3() const { return ___Frames_3; }
	inline RectTransformU5BU5D_t3948421699** get_address_of_Frames_3() { return &___Frames_3; }
	inline void set_Frames_3(RectTransformU5BU5D_t3948421699* value)
	{
		___Frames_3 = value;
		Il2CppCodeGenWriteBarrier(&___Frames_3, value);
	}

	inline static int32_t get_offset_of_LogoText_4() { return static_cast<int32_t>(offsetof(HelloWorldPanel_t280472022, ___LogoText_4)); }
	inline String_t* get_LogoText_4() const { return ___LogoText_4; }
	inline String_t** get_address_of_LogoText_4() { return &___LogoText_4; }
	inline void set_LogoText_4(String_t* value)
	{
		___LogoText_4 = value;
		Il2CppCodeGenWriteBarrier(&___LogoText_4, value);
	}

	inline static int32_t get_offset_of_TextSpeed_5() { return static_cast<int32_t>(offsetof(HelloWorldPanel_t280472022, ___TextSpeed_5)); }
	inline float get_TextSpeed_5() const { return ___TextSpeed_5; }
	inline float* get_address_of_TextSpeed_5() { return &___TextSpeed_5; }
	inline void set_TextSpeed_5(float value)
	{
		___TextSpeed_5 = value;
	}

	inline static int32_t get_offset_of_IsComplete_6() { return static_cast<int32_t>(offsetof(HelloWorldPanel_t280472022, ___IsComplete_6)); }
	inline bool get_IsComplete_6() const { return ___IsComplete_6; }
	inline bool* get_address_of_IsComplete_6() { return &___IsComplete_6; }
	inline void set_IsComplete_6(bool value)
	{
		___IsComplete_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
