﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// HelloWorldPanel
struct HelloWorldPanel_t280472022;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelloWorldPanel/<ShowHelloWorld>c__AnonStorey0
struct  U3CShowHelloWorldU3Ec__AnonStorey0_t2519494053  : public Il2CppObject
{
public:
	// System.Int32 HelloWorldPanel/<ShowHelloWorld>c__AnonStorey0::sChar
	int32_t ___sChar_0;
	// HelloWorldPanel HelloWorldPanel/<ShowHelloWorld>c__AnonStorey0::$this
	HelloWorldPanel_t280472022 * ___U24this_1;

public:
	inline static int32_t get_offset_of_sChar_0() { return static_cast<int32_t>(offsetof(U3CShowHelloWorldU3Ec__AnonStorey0_t2519494053, ___sChar_0)); }
	inline int32_t get_sChar_0() const { return ___sChar_0; }
	inline int32_t* get_address_of_sChar_0() { return &___sChar_0; }
	inline void set_sChar_0(int32_t value)
	{
		___sChar_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowHelloWorldU3Ec__AnonStorey0_t2519494053, ___U24this_1)); }
	inline HelloWorldPanel_t280472022 * get_U24this_1() const { return ___U24this_1; }
	inline HelloWorldPanel_t280472022 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HelloWorldPanel_t280472022 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
