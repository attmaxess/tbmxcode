﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.Material[][]
struct MaterialU5BU5DU5BU5D_t889506739;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// Section
struct Section_t3430619939;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HorizontalClippingSection
struct  HorizontalClippingSection_t3861440939  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GUISkin HorizontalClippingSection::skin1
	GUISkin_t1436893342 * ___skin1_2;
	// UnityEngine.Material[][] HorizontalClippingSection::normalMaterials
	MaterialU5BU5DU5BU5D_t889506739* ___normalMaterials_3;
	// UnityEngine.Material[][] HorizontalClippingSection::clippingMaterials
	MaterialU5BU5DU5BU5D_t889506739* ___clippingMaterials_4;
	// UnityEngine.Renderer[] HorizontalClippingSection::renderers
	RendererU5BU5D_t2810717544* ___renderers_5;
	// UnityEngine.Vector3 HorizontalClippingSection::boundsCentre
	Vector3_t2243707580  ___boundsCentre_6;
	// UnityEngine.Vector4 HorizontalClippingSection::sectionplane
	Vector4_t2243707581  ___sectionplane_7;
	// Section HorizontalClippingSection::sectVars
	Section_t3430619939 * ___sectVars_8;
	// System.Single HorizontalClippingSection::zeroLevelOffset
	float ___zeroLevelOffset_9;
	// System.Single HorizontalClippingSection::sliderRange
	float ___sliderRange_10;
	// UnityEngine.Rect HorizontalClippingSection::labelRect
	Rect_t3681755626  ___labelRect_11;
	// UnityEngine.Rect HorizontalClippingSection::slidersRect
	Rect_t3681755626  ___slidersRect_12;
	// UnityEngine.Rect HorizontalClippingSection::topSliderRect
	Rect_t3681755626  ___topSliderRect_13;
	// UnityEngine.Rect HorizontalClippingSection::midSliderRect
	Rect_t3681755626  ___midSliderRect_14;
	// UnityEngine.Rect HorizontalClippingSection::bottomSliderRect
	Rect_t3681755626  ___bottomSliderRect_15;
	// System.Boolean HorizontalClippingSection::mouseOverGui
	bool ___mouseOverGui_16;

public:
	inline static int32_t get_offset_of_skin1_2() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___skin1_2)); }
	inline GUISkin_t1436893342 * get_skin1_2() const { return ___skin1_2; }
	inline GUISkin_t1436893342 ** get_address_of_skin1_2() { return &___skin1_2; }
	inline void set_skin1_2(GUISkin_t1436893342 * value)
	{
		___skin1_2 = value;
		Il2CppCodeGenWriteBarrier(&___skin1_2, value);
	}

	inline static int32_t get_offset_of_normalMaterials_3() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___normalMaterials_3)); }
	inline MaterialU5BU5DU5BU5D_t889506739* get_normalMaterials_3() const { return ___normalMaterials_3; }
	inline MaterialU5BU5DU5BU5D_t889506739** get_address_of_normalMaterials_3() { return &___normalMaterials_3; }
	inline void set_normalMaterials_3(MaterialU5BU5DU5BU5D_t889506739* value)
	{
		___normalMaterials_3 = value;
		Il2CppCodeGenWriteBarrier(&___normalMaterials_3, value);
	}

	inline static int32_t get_offset_of_clippingMaterials_4() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___clippingMaterials_4)); }
	inline MaterialU5BU5DU5BU5D_t889506739* get_clippingMaterials_4() const { return ___clippingMaterials_4; }
	inline MaterialU5BU5DU5BU5D_t889506739** get_address_of_clippingMaterials_4() { return &___clippingMaterials_4; }
	inline void set_clippingMaterials_4(MaterialU5BU5DU5BU5D_t889506739* value)
	{
		___clippingMaterials_4 = value;
		Il2CppCodeGenWriteBarrier(&___clippingMaterials_4, value);
	}

	inline static int32_t get_offset_of_renderers_5() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___renderers_5)); }
	inline RendererU5BU5D_t2810717544* get_renderers_5() const { return ___renderers_5; }
	inline RendererU5BU5D_t2810717544** get_address_of_renderers_5() { return &___renderers_5; }
	inline void set_renderers_5(RendererU5BU5D_t2810717544* value)
	{
		___renderers_5 = value;
		Il2CppCodeGenWriteBarrier(&___renderers_5, value);
	}

	inline static int32_t get_offset_of_boundsCentre_6() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___boundsCentre_6)); }
	inline Vector3_t2243707580  get_boundsCentre_6() const { return ___boundsCentre_6; }
	inline Vector3_t2243707580 * get_address_of_boundsCentre_6() { return &___boundsCentre_6; }
	inline void set_boundsCentre_6(Vector3_t2243707580  value)
	{
		___boundsCentre_6 = value;
	}

	inline static int32_t get_offset_of_sectionplane_7() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___sectionplane_7)); }
	inline Vector4_t2243707581  get_sectionplane_7() const { return ___sectionplane_7; }
	inline Vector4_t2243707581 * get_address_of_sectionplane_7() { return &___sectionplane_7; }
	inline void set_sectionplane_7(Vector4_t2243707581  value)
	{
		___sectionplane_7 = value;
	}

	inline static int32_t get_offset_of_sectVars_8() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___sectVars_8)); }
	inline Section_t3430619939 * get_sectVars_8() const { return ___sectVars_8; }
	inline Section_t3430619939 ** get_address_of_sectVars_8() { return &___sectVars_8; }
	inline void set_sectVars_8(Section_t3430619939 * value)
	{
		___sectVars_8 = value;
		Il2CppCodeGenWriteBarrier(&___sectVars_8, value);
	}

	inline static int32_t get_offset_of_zeroLevelOffset_9() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___zeroLevelOffset_9)); }
	inline float get_zeroLevelOffset_9() const { return ___zeroLevelOffset_9; }
	inline float* get_address_of_zeroLevelOffset_9() { return &___zeroLevelOffset_9; }
	inline void set_zeroLevelOffset_9(float value)
	{
		___zeroLevelOffset_9 = value;
	}

	inline static int32_t get_offset_of_sliderRange_10() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___sliderRange_10)); }
	inline float get_sliderRange_10() const { return ___sliderRange_10; }
	inline float* get_address_of_sliderRange_10() { return &___sliderRange_10; }
	inline void set_sliderRange_10(float value)
	{
		___sliderRange_10 = value;
	}

	inline static int32_t get_offset_of_labelRect_11() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___labelRect_11)); }
	inline Rect_t3681755626  get_labelRect_11() const { return ___labelRect_11; }
	inline Rect_t3681755626 * get_address_of_labelRect_11() { return &___labelRect_11; }
	inline void set_labelRect_11(Rect_t3681755626  value)
	{
		___labelRect_11 = value;
	}

	inline static int32_t get_offset_of_slidersRect_12() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___slidersRect_12)); }
	inline Rect_t3681755626  get_slidersRect_12() const { return ___slidersRect_12; }
	inline Rect_t3681755626 * get_address_of_slidersRect_12() { return &___slidersRect_12; }
	inline void set_slidersRect_12(Rect_t3681755626  value)
	{
		___slidersRect_12 = value;
	}

	inline static int32_t get_offset_of_topSliderRect_13() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___topSliderRect_13)); }
	inline Rect_t3681755626  get_topSliderRect_13() const { return ___topSliderRect_13; }
	inline Rect_t3681755626 * get_address_of_topSliderRect_13() { return &___topSliderRect_13; }
	inline void set_topSliderRect_13(Rect_t3681755626  value)
	{
		___topSliderRect_13 = value;
	}

	inline static int32_t get_offset_of_midSliderRect_14() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___midSliderRect_14)); }
	inline Rect_t3681755626  get_midSliderRect_14() const { return ___midSliderRect_14; }
	inline Rect_t3681755626 * get_address_of_midSliderRect_14() { return &___midSliderRect_14; }
	inline void set_midSliderRect_14(Rect_t3681755626  value)
	{
		___midSliderRect_14 = value;
	}

	inline static int32_t get_offset_of_bottomSliderRect_15() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___bottomSliderRect_15)); }
	inline Rect_t3681755626  get_bottomSliderRect_15() const { return ___bottomSliderRect_15; }
	inline Rect_t3681755626 * get_address_of_bottomSliderRect_15() { return &___bottomSliderRect_15; }
	inline void set_bottomSliderRect_15(Rect_t3681755626  value)
	{
		___bottomSliderRect_15 = value;
	}

	inline static int32_t get_offset_of_mouseOverGui_16() { return static_cast<int32_t>(offsetof(HorizontalClippingSection_t3861440939, ___mouseOverGui_16)); }
	inline bool get_mouseOverGui_16() const { return ___mouseOverGui_16; }
	inline bool* get_address_of_mouseOverGui_16() { return &___mouseOverGui_16; }
	inline void set_mouseOverGui_16(bool value)
	{
		___mouseOverGui_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
