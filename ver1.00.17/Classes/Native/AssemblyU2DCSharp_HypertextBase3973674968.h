﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"

// UnityEngine.Canvas
struct Canvas_t209405766;
// System.Collections.Generic.List`1<HypertextBase/ClickableEntry>
struct List_1_t3400148091;
// ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t2193656061;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HypertextBase
struct  HypertextBase_t3973674968  : public Text_t356221433
{
public:
	// UnityEngine.Canvas HypertextBase::_rootCanvas
	Canvas_t209405766 * ____rootCanvas_35;
	// System.Collections.Generic.List`1<HypertextBase/ClickableEntry> HypertextBase::_entries
	List_1_t3400148091 * ____entries_37;

public:
	inline static int32_t get_offset_of__rootCanvas_35() { return static_cast<int32_t>(offsetof(HypertextBase_t3973674968, ____rootCanvas_35)); }
	inline Canvas_t209405766 * get__rootCanvas_35() const { return ____rootCanvas_35; }
	inline Canvas_t209405766 ** get_address_of__rootCanvas_35() { return &____rootCanvas_35; }
	inline void set__rootCanvas_35(Canvas_t209405766 * value)
	{
		____rootCanvas_35 = value;
		Il2CppCodeGenWriteBarrier(&____rootCanvas_35, value);
	}

	inline static int32_t get_offset_of__entries_37() { return static_cast<int32_t>(offsetof(HypertextBase_t3973674968, ____entries_37)); }
	inline List_1_t3400148091 * get__entries_37() const { return ____entries_37; }
	inline List_1_t3400148091 ** get_address_of__entries_37() { return &____entries_37; }
	inline void set__entries_37(List_1_t3400148091 * value)
	{
		____entries_37 = value;
		Il2CppCodeGenWriteBarrier(&____entries_37, value);
	}
};

struct HypertextBase_t3973674968_StaticFields
{
public:
	// ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>> HypertextBase::_verticesPool
	ObjectPool_1_t2193656061 * ____verticesPool_38;

public:
	inline static int32_t get_offset_of__verticesPool_38() { return static_cast<int32_t>(offsetof(HypertextBase_t3973674968_StaticFields, ____verticesPool_38)); }
	inline ObjectPool_1_t2193656061 * get__verticesPool_38() const { return ____verticesPool_38; }
	inline ObjectPool_1_t2193656061 ** get_address_of__verticesPool_38() { return &____verticesPool_38; }
	inline void set__verticesPool_38(ObjectPool_1_t2193656061 * value)
	{
		____verticesPool_38 = value;
		Il2CppCodeGenWriteBarrier(&____verticesPool_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
