﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Collections.Generic.List`1<UnityEngine.Rect>
struct List_1_t3050876758;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HypertextBase/ClickableEntry
struct  ClickableEntry_t4031026959 
{
public:
	// System.String HypertextBase/ClickableEntry::Word
	String_t* ___Word_0;
	// System.Int32 HypertextBase/ClickableEntry::StartIndex
	int32_t ___StartIndex_1;
	// UnityEngine.Color HypertextBase/ClickableEntry::Color
	Color_t2020392075  ___Color_2;
	// System.Action`1<System.String> HypertextBase/ClickableEntry::OnClick
	Action_1_t1831019615 * ___OnClick_3;
	// System.Collections.Generic.List`1<UnityEngine.Rect> HypertextBase/ClickableEntry::Rects
	List_1_t3050876758 * ___Rects_4;

public:
	inline static int32_t get_offset_of_Word_0() { return static_cast<int32_t>(offsetof(ClickableEntry_t4031026959, ___Word_0)); }
	inline String_t* get_Word_0() const { return ___Word_0; }
	inline String_t** get_address_of_Word_0() { return &___Word_0; }
	inline void set_Word_0(String_t* value)
	{
		___Word_0 = value;
		Il2CppCodeGenWriteBarrier(&___Word_0, value);
	}

	inline static int32_t get_offset_of_StartIndex_1() { return static_cast<int32_t>(offsetof(ClickableEntry_t4031026959, ___StartIndex_1)); }
	inline int32_t get_StartIndex_1() const { return ___StartIndex_1; }
	inline int32_t* get_address_of_StartIndex_1() { return &___StartIndex_1; }
	inline void set_StartIndex_1(int32_t value)
	{
		___StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_Color_2() { return static_cast<int32_t>(offsetof(ClickableEntry_t4031026959, ___Color_2)); }
	inline Color_t2020392075  get_Color_2() const { return ___Color_2; }
	inline Color_t2020392075 * get_address_of_Color_2() { return &___Color_2; }
	inline void set_Color_2(Color_t2020392075  value)
	{
		___Color_2 = value;
	}

	inline static int32_t get_offset_of_OnClick_3() { return static_cast<int32_t>(offsetof(ClickableEntry_t4031026959, ___OnClick_3)); }
	inline Action_1_t1831019615 * get_OnClick_3() const { return ___OnClick_3; }
	inline Action_1_t1831019615 ** get_address_of_OnClick_3() { return &___OnClick_3; }
	inline void set_OnClick_3(Action_1_t1831019615 * value)
	{
		___OnClick_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnClick_3, value);
	}

	inline static int32_t get_offset_of_Rects_4() { return static_cast<int32_t>(offsetof(ClickableEntry_t4031026959, ___Rects_4)); }
	inline List_1_t3050876758 * get_Rects_4() const { return ___Rects_4; }
	inline List_1_t3050876758 ** get_address_of_Rects_4() { return &___Rects_4; }
	inline void set_Rects_4(List_1_t3050876758 * value)
	{
		___Rects_4 = value;
		Il2CppCodeGenWriteBarrier(&___Rects_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HypertextBase/ClickableEntry
struct ClickableEntry_t4031026959_marshaled_pinvoke
{
	char* ___Word_0;
	int32_t ___StartIndex_1;
	Color_t2020392075  ___Color_2;
	Il2CppMethodPointer ___OnClick_3;
	List_1_t3050876758 * ___Rects_4;
};
// Native definition for COM marshalling of HypertextBase/ClickableEntry
struct ClickableEntry_t4031026959_marshaled_com
{
	Il2CppChar* ___Word_0;
	int32_t ___StartIndex_1;
	Color_t2020392075  ___Color_2;
	Il2CppMethodPointer ___OnClick_3;
	List_1_t3050876758 * ___Rects_4;
};
