﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3777177449;
// System.Action`1<System.Object>
struct Action_1_t2491248677;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HypertextHelper.ObjectPool`1<System.Object>
struct  ObjectPool_1_t2355888186  : public Il2CppObject
{
public:
	// System.Collections.Generic.Stack`1<T> HypertextHelper.ObjectPool`1::_stack
	Stack_1_t3777177449 * ____stack_0;
	// System.Action`1<T> HypertextHelper.ObjectPool`1::_onGet
	Action_1_t2491248677 * ____onGet_1;
	// System.Action`1<T> HypertextHelper.ObjectPool`1::_onRelease
	Action_1_t2491248677 * ____onRelease_2;
	// System.Int32 HypertextHelper.ObjectPool`1::<CountAll>k__BackingField
	int32_t ___U3CCountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2355888186, ____stack_0)); }
	inline Stack_1_t3777177449 * get__stack_0() const { return ____stack_0; }
	inline Stack_1_t3777177449 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_1_t3777177449 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier(&____stack_0, value);
	}

	inline static int32_t get_offset_of__onGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2355888186, ____onGet_1)); }
	inline Action_1_t2491248677 * get__onGet_1() const { return ____onGet_1; }
	inline Action_1_t2491248677 ** get_address_of__onGet_1() { return &____onGet_1; }
	inline void set__onGet_1(Action_1_t2491248677 * value)
	{
		____onGet_1 = value;
		Il2CppCodeGenWriteBarrier(&____onGet_1, value);
	}

	inline static int32_t get_offset_of__onRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2355888186, ____onRelease_2)); }
	inline Action_1_t2491248677 * get__onRelease_2() const { return ____onRelease_2; }
	inline Action_1_t2491248677 ** get_address_of__onRelease_2() { return &____onRelease_2; }
	inline void set__onRelease_2(Action_1_t2491248677 * value)
	{
		____onRelease_2 = value;
		Il2CppCodeGenWriteBarrier(&____onRelease_2, value);
	}

	inline static int32_t get_offset_of_U3CCountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2355888186, ___U3CCountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CCountAllU3Ek__BackingField_3() const { return ___U3CCountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCountAllU3Ek__BackingField_3() { return &___U3CCountAllU3Ek__BackingField_3; }
	inline void set_U3CCountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CCountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
