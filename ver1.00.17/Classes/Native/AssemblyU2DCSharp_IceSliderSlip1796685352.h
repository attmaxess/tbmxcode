﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IceSliderSlip
struct  IceSliderSlip_t1796685352  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean IceSliderSlip::PlayerMoblimoCol
	bool ___PlayerMoblimoCol_2;
	// UnityEngine.Transform IceSliderSlip::SliderTarget
	Transform_t3275118058 * ___SliderTarget_3;

public:
	inline static int32_t get_offset_of_PlayerMoblimoCol_2() { return static_cast<int32_t>(offsetof(IceSliderSlip_t1796685352, ___PlayerMoblimoCol_2)); }
	inline bool get_PlayerMoblimoCol_2() const { return ___PlayerMoblimoCol_2; }
	inline bool* get_address_of_PlayerMoblimoCol_2() { return &___PlayerMoblimoCol_2; }
	inline void set_PlayerMoblimoCol_2(bool value)
	{
		___PlayerMoblimoCol_2 = value;
	}

	inline static int32_t get_offset_of_SliderTarget_3() { return static_cast<int32_t>(offsetof(IceSliderSlip_t1796685352, ___SliderTarget_3)); }
	inline Transform_t3275118058 * get_SliderTarget_3() const { return ___SliderTarget_3; }
	inline Transform_t3275118058 ** get_address_of_SliderTarget_3() { return &___SliderTarget_3; }
	inline void set_SliderTarget_3(Transform_t3275118058 * value)
	{
		___SliderTarget_3 = value;
		Il2CppCodeGenWriteBarrier(&___SliderTarget_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
