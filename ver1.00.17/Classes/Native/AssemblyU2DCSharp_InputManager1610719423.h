﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen307395758.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputManager
struct  InputManager_t1610719423  : public SingletonMonoBehaviour_1_t307395758
{
public:
	// System.Single InputManager::m_StartDis
	float ___m_StartDis_3;
	// System.Single InputManager::m_fPinch
	float ___m_fPinch_4;
	// System.Single InputManager::m_vSwipeX
	float ___m_vSwipeX_5;
	// System.Single InputManager::m_vSwipeY
	float ___m_vSwipeY_6;
	// UnityEngine.Vector2 InputManager::m_vPrevClickPos
	Vector2_t2243707579  ___m_vPrevClickPos_7;
	// System.Single InputManager::m_vFlickX
	float ___m_vFlickX_8;
	// System.Single InputManager::m_vFlickY
	float ___m_vFlickY_9;
	// UnityEngine.Vector2 InputManager::m_vPrevMousePos
	Vector2_t2243707579  ___m_vPrevMousePos_10;
	// System.Boolean InputManager::m_bIsDoubleTap
	bool ___m_bIsDoubleTap_11;
	// System.Single InputManager::m_fDoubleTime
	float ___m_fDoubleTime_12;

public:
	inline static int32_t get_offset_of_m_StartDis_3() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_StartDis_3)); }
	inline float get_m_StartDis_3() const { return ___m_StartDis_3; }
	inline float* get_address_of_m_StartDis_3() { return &___m_StartDis_3; }
	inline void set_m_StartDis_3(float value)
	{
		___m_StartDis_3 = value;
	}

	inline static int32_t get_offset_of_m_fPinch_4() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_fPinch_4)); }
	inline float get_m_fPinch_4() const { return ___m_fPinch_4; }
	inline float* get_address_of_m_fPinch_4() { return &___m_fPinch_4; }
	inline void set_m_fPinch_4(float value)
	{
		___m_fPinch_4 = value;
	}

	inline static int32_t get_offset_of_m_vSwipeX_5() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_vSwipeX_5)); }
	inline float get_m_vSwipeX_5() const { return ___m_vSwipeX_5; }
	inline float* get_address_of_m_vSwipeX_5() { return &___m_vSwipeX_5; }
	inline void set_m_vSwipeX_5(float value)
	{
		___m_vSwipeX_5 = value;
	}

	inline static int32_t get_offset_of_m_vSwipeY_6() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_vSwipeY_6)); }
	inline float get_m_vSwipeY_6() const { return ___m_vSwipeY_6; }
	inline float* get_address_of_m_vSwipeY_6() { return &___m_vSwipeY_6; }
	inline void set_m_vSwipeY_6(float value)
	{
		___m_vSwipeY_6 = value;
	}

	inline static int32_t get_offset_of_m_vPrevClickPos_7() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_vPrevClickPos_7)); }
	inline Vector2_t2243707579  get_m_vPrevClickPos_7() const { return ___m_vPrevClickPos_7; }
	inline Vector2_t2243707579 * get_address_of_m_vPrevClickPos_7() { return &___m_vPrevClickPos_7; }
	inline void set_m_vPrevClickPos_7(Vector2_t2243707579  value)
	{
		___m_vPrevClickPos_7 = value;
	}

	inline static int32_t get_offset_of_m_vFlickX_8() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_vFlickX_8)); }
	inline float get_m_vFlickX_8() const { return ___m_vFlickX_8; }
	inline float* get_address_of_m_vFlickX_8() { return &___m_vFlickX_8; }
	inline void set_m_vFlickX_8(float value)
	{
		___m_vFlickX_8 = value;
	}

	inline static int32_t get_offset_of_m_vFlickY_9() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_vFlickY_9)); }
	inline float get_m_vFlickY_9() const { return ___m_vFlickY_9; }
	inline float* get_address_of_m_vFlickY_9() { return &___m_vFlickY_9; }
	inline void set_m_vFlickY_9(float value)
	{
		___m_vFlickY_9 = value;
	}

	inline static int32_t get_offset_of_m_vPrevMousePos_10() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_vPrevMousePos_10)); }
	inline Vector2_t2243707579  get_m_vPrevMousePos_10() const { return ___m_vPrevMousePos_10; }
	inline Vector2_t2243707579 * get_address_of_m_vPrevMousePos_10() { return &___m_vPrevMousePos_10; }
	inline void set_m_vPrevMousePos_10(Vector2_t2243707579  value)
	{
		___m_vPrevMousePos_10 = value;
	}

	inline static int32_t get_offset_of_m_bIsDoubleTap_11() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_bIsDoubleTap_11)); }
	inline bool get_m_bIsDoubleTap_11() const { return ___m_bIsDoubleTap_11; }
	inline bool* get_address_of_m_bIsDoubleTap_11() { return &___m_bIsDoubleTap_11; }
	inline void set_m_bIsDoubleTap_11(bool value)
	{
		___m_bIsDoubleTap_11 = value;
	}

	inline static int32_t get_offset_of_m_fDoubleTime_12() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___m_fDoubleTime_12)); }
	inline float get_m_fDoubleTime_12() const { return ___m_fDoubleTime_12; }
	inline float* get_address_of_m_fDoubleTime_12() { return &___m_fDoubleTime_12; }
	inline void set_m_fDoubleTime_12(float value)
	{
		___m_fDoubleTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
