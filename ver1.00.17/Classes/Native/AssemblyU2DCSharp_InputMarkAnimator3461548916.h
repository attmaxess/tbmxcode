﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// NamePanelItem
struct NamePanelItem_t3250109188;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputMarkAnimator
struct  InputMarkAnimator_t3461548916  : public MonoBehaviour_t1158329972
{
public:
	// NamePanelItem InputMarkAnimator::m_Input
	NamePanelItem_t3250109188 * ___m_Input_2;
	// UnityEngine.UI.RawImage InputMarkAnimator::m_RawImage
	RawImage_t2749640213 * ___m_RawImage_3;
	// UnityEngine.RectTransform InputMarkAnimator::m_RectTrans
	RectTransform_t3349966182 * ___m_RectTrans_4;
	// UnityEngine.Coroutine InputMarkAnimator::m_SwingCoroutine
	Coroutine_t2299508840 * ___m_SwingCoroutine_5;

public:
	inline static int32_t get_offset_of_m_Input_2() { return static_cast<int32_t>(offsetof(InputMarkAnimator_t3461548916, ___m_Input_2)); }
	inline NamePanelItem_t3250109188 * get_m_Input_2() const { return ___m_Input_2; }
	inline NamePanelItem_t3250109188 ** get_address_of_m_Input_2() { return &___m_Input_2; }
	inline void set_m_Input_2(NamePanelItem_t3250109188 * value)
	{
		___m_Input_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Input_2, value);
	}

	inline static int32_t get_offset_of_m_RawImage_3() { return static_cast<int32_t>(offsetof(InputMarkAnimator_t3461548916, ___m_RawImage_3)); }
	inline RawImage_t2749640213 * get_m_RawImage_3() const { return ___m_RawImage_3; }
	inline RawImage_t2749640213 ** get_address_of_m_RawImage_3() { return &___m_RawImage_3; }
	inline void set_m_RawImage_3(RawImage_t2749640213 * value)
	{
		___m_RawImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_RawImage_3, value);
	}

	inline static int32_t get_offset_of_m_RectTrans_4() { return static_cast<int32_t>(offsetof(InputMarkAnimator_t3461548916, ___m_RectTrans_4)); }
	inline RectTransform_t3349966182 * get_m_RectTrans_4() const { return ___m_RectTrans_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTrans_4() { return &___m_RectTrans_4; }
	inline void set_m_RectTrans_4(RectTransform_t3349966182 * value)
	{
		___m_RectTrans_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTrans_4, value);
	}

	inline static int32_t get_offset_of_m_SwingCoroutine_5() { return static_cast<int32_t>(offsetof(InputMarkAnimator_t3461548916, ___m_SwingCoroutine_5)); }
	inline Coroutine_t2299508840 * get_m_SwingCoroutine_5() const { return ___m_SwingCoroutine_5; }
	inline Coroutine_t2299508840 ** get_address_of_m_SwingCoroutine_5() { return &___m_SwingCoroutine_5; }
	inline void set_m_SwingCoroutine_5(Coroutine_t2299508840 * value)
	{
		___m_SwingCoroutine_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_SwingCoroutine_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
