﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// InstantateAsyncManager
struct InstantateAsyncManager_t2235219776;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantateAsyncManager
struct  InstantateAsyncManager_t2235219776  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct InstantateAsyncManager_t2235219776_StaticFields
{
public:
	// InstantateAsyncManager InstantateAsyncManager::Instance
	InstantateAsyncManager_t2235219776 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstantateAsyncManager_t2235219776_StaticFields, ___Instance_2)); }
	inline InstantateAsyncManager_t2235219776 * get_Instance_2() const { return ___Instance_2; }
	inline InstantateAsyncManager_t2235219776 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(InstantateAsyncManager_t2235219776 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
