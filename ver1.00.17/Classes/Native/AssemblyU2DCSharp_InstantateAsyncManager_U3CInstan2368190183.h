﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantateAsyncManager/<InstanceObjects>c__Iterator0
struct  U3CInstanceObjectsU3Ec__Iterator0_t2368190183  : public Il2CppObject
{
public:
	// UnityEngine.Transform InstantateAsyncManager/<InstanceObjects>c__Iterator0::self
	Transform_t3275118058 * ___self_0;
	// UnityEngine.GameObject[] InstantateAsyncManager/<InstanceObjects>c__Iterator0::objects
	GameObjectU5BU5D_t3057952154* ___objects_1;
	// UnityEngine.GameObject[] InstantateAsyncManager/<InstanceObjects>c__Iterator0::$locvar0
	GameObjectU5BU5D_t3057952154* ___U24locvar0_2;
	// System.Int32 InstantateAsyncManager/<InstanceObjects>c__Iterator0::$locvar1
	int32_t ___U24locvar1_3;
	// UnityEngine.GameObject InstantateAsyncManager/<InstanceObjects>c__Iterator0::<obj>__1
	GameObject_t1756533147 * ___U3CobjU3E__1_4;
	// UnityEngine.GameObject InstantateAsyncManager/<InstanceObjects>c__Iterator0::<item>__2
	GameObject_t1756533147 * ___U3CitemU3E__2_5;
	// System.Object InstantateAsyncManager/<InstanceObjects>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean InstantateAsyncManager/<InstanceObjects>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 InstantateAsyncManager/<InstanceObjects>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_self_0() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___self_0)); }
	inline Transform_t3275118058 * get_self_0() const { return ___self_0; }
	inline Transform_t3275118058 ** get_address_of_self_0() { return &___self_0; }
	inline void set_self_0(Transform_t3275118058 * value)
	{
		___self_0 = value;
		Il2CppCodeGenWriteBarrier(&___self_0, value);
	}

	inline static int32_t get_offset_of_objects_1() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___objects_1)); }
	inline GameObjectU5BU5D_t3057952154* get_objects_1() const { return ___objects_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_objects_1() { return &___objects_1; }
	inline void set_objects_1(GameObjectU5BU5D_t3057952154* value)
	{
		___objects_1 = value;
		Il2CppCodeGenWriteBarrier(&___objects_1, value);
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___U24locvar0_2)); }
	inline GameObjectU5BU5D_t3057952154* get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(GameObjectU5BU5D_t3057952154* value)
	{
		___U24locvar0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_2, value);
	}

	inline static int32_t get_offset_of_U24locvar1_3() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___U24locvar1_3)); }
	inline int32_t get_U24locvar1_3() const { return ___U24locvar1_3; }
	inline int32_t* get_address_of_U24locvar1_3() { return &___U24locvar1_3; }
	inline void set_U24locvar1_3(int32_t value)
	{
		___U24locvar1_3 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__1_4() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___U3CobjU3E__1_4)); }
	inline GameObject_t1756533147 * get_U3CobjU3E__1_4() const { return ___U3CobjU3E__1_4; }
	inline GameObject_t1756533147 ** get_address_of_U3CobjU3E__1_4() { return &___U3CobjU3E__1_4; }
	inline void set_U3CobjU3E__1_4(GameObject_t1756533147 * value)
	{
		___U3CobjU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__2_5() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___U3CitemU3E__2_5)); }
	inline GameObject_t1756533147 * get_U3CitemU3E__2_5() const { return ___U3CitemU3E__2_5; }
	inline GameObject_t1756533147 ** get_address_of_U3CitemU3E__2_5() { return &___U3CitemU3E__2_5; }
	inline void set_U3CitemU3E__2_5(GameObject_t1756533147 * value)
	{
		___U3CitemU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3E__2_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CInstanceObjectsU3Ec__Iterator0_t2368190183, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
