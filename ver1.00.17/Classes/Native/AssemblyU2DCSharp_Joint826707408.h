﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// Parts
struct Parts_t3804168686;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joint
struct  Joint_t826707408  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Joint::JointNum
	int32_t ___JointNum_2;
	// UnityEngine.Transform Joint::m_ParentPartsObj
	Transform_t3275118058 * ___m_ParentPartsObj_3;
	// System.Boolean Joint::m_bIsSelect
	bool ___m_bIsSelect_4;
	// System.Boolean Joint::m_bIsConnect
	bool ___m_bIsConnect_5;
	// Parts Joint::m_ParentParts
	Parts_t3804168686 * ___m_ParentParts_6;
	// UnityEngine.Transform Joint::m_SpriteObj
	Transform_t3275118058 * ___m_SpriteObj_7;
	// UnityEngine.Vector3 Joint::m_vOriginalScale
	Vector3_t2243707580  ___m_vOriginalScale_8;
	// UnityEngine.SpriteRenderer Joint::m_Sprite
	SpriteRenderer_t1209076198 * ___m_Sprite_9;
	// UnityEngine.Color Joint::m_DefaultColor
	Color_t2020392075  ___m_DefaultColor_10;

public:
	inline static int32_t get_offset_of_JointNum_2() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___JointNum_2)); }
	inline int32_t get_JointNum_2() const { return ___JointNum_2; }
	inline int32_t* get_address_of_JointNum_2() { return &___JointNum_2; }
	inline void set_JointNum_2(int32_t value)
	{
		___JointNum_2 = value;
	}

	inline static int32_t get_offset_of_m_ParentPartsObj_3() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_ParentPartsObj_3)); }
	inline Transform_t3275118058 * get_m_ParentPartsObj_3() const { return ___m_ParentPartsObj_3; }
	inline Transform_t3275118058 ** get_address_of_m_ParentPartsObj_3() { return &___m_ParentPartsObj_3; }
	inline void set_m_ParentPartsObj_3(Transform_t3275118058 * value)
	{
		___m_ParentPartsObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_ParentPartsObj_3, value);
	}

	inline static int32_t get_offset_of_m_bIsSelect_4() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_bIsSelect_4)); }
	inline bool get_m_bIsSelect_4() const { return ___m_bIsSelect_4; }
	inline bool* get_address_of_m_bIsSelect_4() { return &___m_bIsSelect_4; }
	inline void set_m_bIsSelect_4(bool value)
	{
		___m_bIsSelect_4 = value;
	}

	inline static int32_t get_offset_of_m_bIsConnect_5() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_bIsConnect_5)); }
	inline bool get_m_bIsConnect_5() const { return ___m_bIsConnect_5; }
	inline bool* get_address_of_m_bIsConnect_5() { return &___m_bIsConnect_5; }
	inline void set_m_bIsConnect_5(bool value)
	{
		___m_bIsConnect_5 = value;
	}

	inline static int32_t get_offset_of_m_ParentParts_6() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_ParentParts_6)); }
	inline Parts_t3804168686 * get_m_ParentParts_6() const { return ___m_ParentParts_6; }
	inline Parts_t3804168686 ** get_address_of_m_ParentParts_6() { return &___m_ParentParts_6; }
	inline void set_m_ParentParts_6(Parts_t3804168686 * value)
	{
		___m_ParentParts_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_ParentParts_6, value);
	}

	inline static int32_t get_offset_of_m_SpriteObj_7() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_SpriteObj_7)); }
	inline Transform_t3275118058 * get_m_SpriteObj_7() const { return ___m_SpriteObj_7; }
	inline Transform_t3275118058 ** get_address_of_m_SpriteObj_7() { return &___m_SpriteObj_7; }
	inline void set_m_SpriteObj_7(Transform_t3275118058 * value)
	{
		___m_SpriteObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_SpriteObj_7, value);
	}

	inline static int32_t get_offset_of_m_vOriginalScale_8() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_vOriginalScale_8)); }
	inline Vector3_t2243707580  get_m_vOriginalScale_8() const { return ___m_vOriginalScale_8; }
	inline Vector3_t2243707580 * get_address_of_m_vOriginalScale_8() { return &___m_vOriginalScale_8; }
	inline void set_m_vOriginalScale_8(Vector3_t2243707580  value)
	{
		___m_vOriginalScale_8 = value;
	}

	inline static int32_t get_offset_of_m_Sprite_9() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_Sprite_9)); }
	inline SpriteRenderer_t1209076198 * get_m_Sprite_9() const { return ___m_Sprite_9; }
	inline SpriteRenderer_t1209076198 ** get_address_of_m_Sprite_9() { return &___m_Sprite_9; }
	inline void set_m_Sprite_9(SpriteRenderer_t1209076198 * value)
	{
		___m_Sprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_Sprite_9, value);
	}

	inline static int32_t get_offset_of_m_DefaultColor_10() { return static_cast<int32_t>(offsetof(Joint_t826707408, ___m_DefaultColor_10)); }
	inline Color_t2020392075  get_m_DefaultColor_10() const { return ___m_DefaultColor_10; }
	inline Color_t2020392075 * get_address_of_m_DefaultColor_10() { return &___m_DefaultColor_10; }
	inline void set_m_DefaultColor_10(Color_t2020392075  value)
	{
		___m_DefaultColor_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
