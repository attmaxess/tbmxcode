﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageData
struct  LanguageData_t336865618  : public Il2CppObject
{
public:
	// System.String LanguageData::key
	String_t* ___key_0;
	// System.String[] LanguageData::setupjp
	StringU5BU5D_t1642385972* ___setupjp_1;
	// System.String LanguageData::jp
	String_t* ___jp_2;
	// System.String[] LanguageData::setupus
	StringU5BU5D_t1642385972* ___setupus_3;
	// System.String LanguageData::us
	String_t* ___us_4;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(LanguageData_t336865618, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_setupjp_1() { return static_cast<int32_t>(offsetof(LanguageData_t336865618, ___setupjp_1)); }
	inline StringU5BU5D_t1642385972* get_setupjp_1() const { return ___setupjp_1; }
	inline StringU5BU5D_t1642385972** get_address_of_setupjp_1() { return &___setupjp_1; }
	inline void set_setupjp_1(StringU5BU5D_t1642385972* value)
	{
		___setupjp_1 = value;
		Il2CppCodeGenWriteBarrier(&___setupjp_1, value);
	}

	inline static int32_t get_offset_of_jp_2() { return static_cast<int32_t>(offsetof(LanguageData_t336865618, ___jp_2)); }
	inline String_t* get_jp_2() const { return ___jp_2; }
	inline String_t** get_address_of_jp_2() { return &___jp_2; }
	inline void set_jp_2(String_t* value)
	{
		___jp_2 = value;
		Il2CppCodeGenWriteBarrier(&___jp_2, value);
	}

	inline static int32_t get_offset_of_setupus_3() { return static_cast<int32_t>(offsetof(LanguageData_t336865618, ___setupus_3)); }
	inline StringU5BU5D_t1642385972* get_setupus_3() const { return ___setupus_3; }
	inline StringU5BU5D_t1642385972** get_address_of_setupus_3() { return &___setupus_3; }
	inline void set_setupus_3(StringU5BU5D_t1642385972* value)
	{
		___setupus_3 = value;
		Il2CppCodeGenWriteBarrier(&___setupus_3, value);
	}

	inline static int32_t get_offset_of_us_4() { return static_cast<int32_t>(offsetof(LanguageData_t336865618, ___us_4)); }
	inline String_t* get_us_4() const { return ___us_4; }
	inline String_t** get_address_of_us_4() { return &___us_4; }
	inline void set_us_4(String_t* value)
	{
		___us_4 = value;
		Il2CppCodeGenWriteBarrier(&___us_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
