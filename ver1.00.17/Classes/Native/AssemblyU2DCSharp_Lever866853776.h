﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Slider
struct Slider_t297367283;
// ActionLever
struct ActionLever_t131660742;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lever
struct  Lever_t866853776  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Lever::LeverNum
	int32_t ___LeverNum_2;
	// UnityEngine.UI.Slider Lever::m_Lever
	Slider_t297367283 * ___m_Lever_3;
	// ActionLever Lever::m_ActionLever
	ActionLever_t131660742 * ___m_ActionLever_4;
	// System.Single Lever::m_fValue
	float ___m_fValue_5;
	// System.Single Lever::pre_value
	float ___pre_value_6;

public:
	inline static int32_t get_offset_of_LeverNum_2() { return static_cast<int32_t>(offsetof(Lever_t866853776, ___LeverNum_2)); }
	inline int32_t get_LeverNum_2() const { return ___LeverNum_2; }
	inline int32_t* get_address_of_LeverNum_2() { return &___LeverNum_2; }
	inline void set_LeverNum_2(int32_t value)
	{
		___LeverNum_2 = value;
	}

	inline static int32_t get_offset_of_m_Lever_3() { return static_cast<int32_t>(offsetof(Lever_t866853776, ___m_Lever_3)); }
	inline Slider_t297367283 * get_m_Lever_3() const { return ___m_Lever_3; }
	inline Slider_t297367283 ** get_address_of_m_Lever_3() { return &___m_Lever_3; }
	inline void set_m_Lever_3(Slider_t297367283 * value)
	{
		___m_Lever_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Lever_3, value);
	}

	inline static int32_t get_offset_of_m_ActionLever_4() { return static_cast<int32_t>(offsetof(Lever_t866853776, ___m_ActionLever_4)); }
	inline ActionLever_t131660742 * get_m_ActionLever_4() const { return ___m_ActionLever_4; }
	inline ActionLever_t131660742 ** get_address_of_m_ActionLever_4() { return &___m_ActionLever_4; }
	inline void set_m_ActionLever_4(ActionLever_t131660742 * value)
	{
		___m_ActionLever_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionLever_4, value);
	}

	inline static int32_t get_offset_of_m_fValue_5() { return static_cast<int32_t>(offsetof(Lever_t866853776, ___m_fValue_5)); }
	inline float get_m_fValue_5() const { return ___m_fValue_5; }
	inline float* get_address_of_m_fValue_5() { return &___m_fValue_5; }
	inline void set_m_fValue_5(float value)
	{
		___m_fValue_5 = value;
	}

	inline static int32_t get_offset_of_pre_value_6() { return static_cast<int32_t>(offsetof(Lever_t866853776, ___pre_value_6)); }
	inline float get_pre_value_6() const { return ___pre_value_6; }
	inline float* get_address_of_pre_value_6() { return &___pre_value_6; }
	inline void set_pre_value_6(float value)
	{
		___pre_value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
