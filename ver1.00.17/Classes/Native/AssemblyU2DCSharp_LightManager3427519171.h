﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2124195506.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// lightObj[]
struct lightObjU5BU5D_t219586334;
// System.Collections.Generic.Dictionary`2<eLIGHTTYPE,UnityEngine.Light>
struct Dictionary_2_t242885326;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightManager
struct  LightManager_t3427519171  : public SingletonMonoBehaviour_1_t2124195506
{
public:
	// lightObj[] LightManager::LightList
	lightObjU5BU5D_t219586334* ___LightList_3;
	// System.Collections.Generic.Dictionary`2<eLIGHTTYPE,UnityEngine.Light> LightManager::lightDic
	Dictionary_2_t242885326 * ___lightDic_4;
	// UnityEngine.Vector3 LightManager::defaultPosition
	Vector3_t2243707580  ___defaultPosition_5;
	// UnityEngine.Vector3 LightManager::defaultLightPosition
	Vector3_t2243707580  ___defaultLightPosition_6;
	// UnityEngine.Quaternion LightManager::defaultLightRot
	Quaternion_t4030073918  ___defaultLightRot_7;

public:
	inline static int32_t get_offset_of_LightList_3() { return static_cast<int32_t>(offsetof(LightManager_t3427519171, ___LightList_3)); }
	inline lightObjU5BU5D_t219586334* get_LightList_3() const { return ___LightList_3; }
	inline lightObjU5BU5D_t219586334** get_address_of_LightList_3() { return &___LightList_3; }
	inline void set_LightList_3(lightObjU5BU5D_t219586334* value)
	{
		___LightList_3 = value;
		Il2CppCodeGenWriteBarrier(&___LightList_3, value);
	}

	inline static int32_t get_offset_of_lightDic_4() { return static_cast<int32_t>(offsetof(LightManager_t3427519171, ___lightDic_4)); }
	inline Dictionary_2_t242885326 * get_lightDic_4() const { return ___lightDic_4; }
	inline Dictionary_2_t242885326 ** get_address_of_lightDic_4() { return &___lightDic_4; }
	inline void set_lightDic_4(Dictionary_2_t242885326 * value)
	{
		___lightDic_4 = value;
		Il2CppCodeGenWriteBarrier(&___lightDic_4, value);
	}

	inline static int32_t get_offset_of_defaultPosition_5() { return static_cast<int32_t>(offsetof(LightManager_t3427519171, ___defaultPosition_5)); }
	inline Vector3_t2243707580  get_defaultPosition_5() const { return ___defaultPosition_5; }
	inline Vector3_t2243707580 * get_address_of_defaultPosition_5() { return &___defaultPosition_5; }
	inline void set_defaultPosition_5(Vector3_t2243707580  value)
	{
		___defaultPosition_5 = value;
	}

	inline static int32_t get_offset_of_defaultLightPosition_6() { return static_cast<int32_t>(offsetof(LightManager_t3427519171, ___defaultLightPosition_6)); }
	inline Vector3_t2243707580  get_defaultLightPosition_6() const { return ___defaultLightPosition_6; }
	inline Vector3_t2243707580 * get_address_of_defaultLightPosition_6() { return &___defaultLightPosition_6; }
	inline void set_defaultLightPosition_6(Vector3_t2243707580  value)
	{
		___defaultLightPosition_6 = value;
	}

	inline static int32_t get_offset_of_defaultLightRot_7() { return static_cast<int32_t>(offsetof(LightManager_t3427519171, ___defaultLightRot_7)); }
	inline Quaternion_t4030073918  get_defaultLightRot_7() const { return ___defaultLightRot_7; }
	inline Quaternion_t4030073918 * get_address_of_defaultLightRot_7() { return &___defaultLightRot_7; }
	inline void set_defaultLightRot_7(Quaternion_t4030073918  value)
	{
		___defaultLightRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
