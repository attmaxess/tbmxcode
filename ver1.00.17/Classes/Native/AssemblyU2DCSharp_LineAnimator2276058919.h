﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineAnimator
struct  LineAnimator_t2276058919  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean LineAnimator::Active
	bool ___Active_2;
	// UnityEngine.Vector2 LineAnimator::StartUV
	Vector2_t2243707579  ___StartUV_3;
	// UnityEngine.Vector2 LineAnimator::MoveUV
	Vector2_t2243707579  ___MoveUV_4;
	// UnityEngine.UI.RawImage LineAnimator::m_RawImage
	RawImage_t2749640213 * ___m_RawImage_5;
	// UnityEngine.Vector2 LineAnimator::m_vUV
	Vector2_t2243707579  ___m_vUV_6;
	// UnityEngine.Coroutine LineAnimator::m_FlowGlossCoroutine
	Coroutine_t2299508840 * ___m_FlowGlossCoroutine_7;

public:
	inline static int32_t get_offset_of_Active_2() { return static_cast<int32_t>(offsetof(LineAnimator_t2276058919, ___Active_2)); }
	inline bool get_Active_2() const { return ___Active_2; }
	inline bool* get_address_of_Active_2() { return &___Active_2; }
	inline void set_Active_2(bool value)
	{
		___Active_2 = value;
	}

	inline static int32_t get_offset_of_StartUV_3() { return static_cast<int32_t>(offsetof(LineAnimator_t2276058919, ___StartUV_3)); }
	inline Vector2_t2243707579  get_StartUV_3() const { return ___StartUV_3; }
	inline Vector2_t2243707579 * get_address_of_StartUV_3() { return &___StartUV_3; }
	inline void set_StartUV_3(Vector2_t2243707579  value)
	{
		___StartUV_3 = value;
	}

	inline static int32_t get_offset_of_MoveUV_4() { return static_cast<int32_t>(offsetof(LineAnimator_t2276058919, ___MoveUV_4)); }
	inline Vector2_t2243707579  get_MoveUV_4() const { return ___MoveUV_4; }
	inline Vector2_t2243707579 * get_address_of_MoveUV_4() { return &___MoveUV_4; }
	inline void set_MoveUV_4(Vector2_t2243707579  value)
	{
		___MoveUV_4 = value;
	}

	inline static int32_t get_offset_of_m_RawImage_5() { return static_cast<int32_t>(offsetof(LineAnimator_t2276058919, ___m_RawImage_5)); }
	inline RawImage_t2749640213 * get_m_RawImage_5() const { return ___m_RawImage_5; }
	inline RawImage_t2749640213 ** get_address_of_m_RawImage_5() { return &___m_RawImage_5; }
	inline void set_m_RawImage_5(RawImage_t2749640213 * value)
	{
		___m_RawImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_RawImage_5, value);
	}

	inline static int32_t get_offset_of_m_vUV_6() { return static_cast<int32_t>(offsetof(LineAnimator_t2276058919, ___m_vUV_6)); }
	inline Vector2_t2243707579  get_m_vUV_6() const { return ___m_vUV_6; }
	inline Vector2_t2243707579 * get_address_of_m_vUV_6() { return &___m_vUV_6; }
	inline void set_m_vUV_6(Vector2_t2243707579  value)
	{
		___m_vUV_6 = value;
	}

	inline static int32_t get_offset_of_m_FlowGlossCoroutine_7() { return static_cast<int32_t>(offsetof(LineAnimator_t2276058919, ___m_FlowGlossCoroutine_7)); }
	inline Coroutine_t2299508840 * get_m_FlowGlossCoroutine_7() const { return ___m_FlowGlossCoroutine_7; }
	inline Coroutine_t2299508840 ** get_address_of_m_FlowGlossCoroutine_7() { return &___m_FlowGlossCoroutine_7; }
	inline void set_m_FlowGlossCoroutine_7(Coroutine_t2299508840 * value)
	{
		___m_FlowGlossCoroutine_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_FlowGlossCoroutine_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
