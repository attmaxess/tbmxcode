﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// LineAnimator
struct LineAnimator_t2276058919;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineAnimator/<_Gloss>c__AnonStorey1
struct  U3C_GlossU3Ec__AnonStorey1_t167591579  : public Il2CppObject
{
public:
	// System.Single LineAnimator/<_Gloss>c__AnonStorey1::fGlow
	float ___fGlow_0;
	// LineAnimator LineAnimator/<_Gloss>c__AnonStorey1::$this
	LineAnimator_t2276058919 * ___U24this_1;

public:
	inline static int32_t get_offset_of_fGlow_0() { return static_cast<int32_t>(offsetof(U3C_GlossU3Ec__AnonStorey1_t167591579, ___fGlow_0)); }
	inline float get_fGlow_0() const { return ___fGlow_0; }
	inline float* get_address_of_fGlow_0() { return &___fGlow_0; }
	inline void set_fGlow_0(float value)
	{
		___fGlow_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3C_GlossU3Ec__AnonStorey1_t167591579, ___U24this_1)); }
	inline LineAnimator_t2276058919 * get_U24this_1() const { return ___U24this_1; }
	inline LineAnimator_t2276058919 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LineAnimator_t2276058919 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
