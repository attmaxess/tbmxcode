﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3370644843.h"

// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadAssetBundle
struct  LoadAssetBundle_t379001212  : public SingletonMonoBehaviour_1_t3370644843
{
public:
	// System.String LoadAssetBundle::url
	String_t* ___url_3;
	// System.Int32 LoadAssetBundle::version
	int32_t ___version_4;
	// System.String LoadAssetBundle::manifestName
	String_t* ___manifestName_5;
	// UnityEngine.AudioClip LoadAssetBundle::loadedAudioClip
	AudioClip_t1932558630 * ___loadedAudioClip_6;
	// UnityEngine.WWW LoadAssetBundle::www
	WWW_t2919945039 * ___www_7;
	// UnityEngine.AssetBundle LoadAssetBundle::mab
	AssetBundle_t2054978754 * ___mab_8;

public:
	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(LoadAssetBundle_t379001212, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier(&___url_3, value);
	}

	inline static int32_t get_offset_of_version_4() { return static_cast<int32_t>(offsetof(LoadAssetBundle_t379001212, ___version_4)); }
	inline int32_t get_version_4() const { return ___version_4; }
	inline int32_t* get_address_of_version_4() { return &___version_4; }
	inline void set_version_4(int32_t value)
	{
		___version_4 = value;
	}

	inline static int32_t get_offset_of_manifestName_5() { return static_cast<int32_t>(offsetof(LoadAssetBundle_t379001212, ___manifestName_5)); }
	inline String_t* get_manifestName_5() const { return ___manifestName_5; }
	inline String_t** get_address_of_manifestName_5() { return &___manifestName_5; }
	inline void set_manifestName_5(String_t* value)
	{
		___manifestName_5 = value;
		Il2CppCodeGenWriteBarrier(&___manifestName_5, value);
	}

	inline static int32_t get_offset_of_loadedAudioClip_6() { return static_cast<int32_t>(offsetof(LoadAssetBundle_t379001212, ___loadedAudioClip_6)); }
	inline AudioClip_t1932558630 * get_loadedAudioClip_6() const { return ___loadedAudioClip_6; }
	inline AudioClip_t1932558630 ** get_address_of_loadedAudioClip_6() { return &___loadedAudioClip_6; }
	inline void set_loadedAudioClip_6(AudioClip_t1932558630 * value)
	{
		___loadedAudioClip_6 = value;
		Il2CppCodeGenWriteBarrier(&___loadedAudioClip_6, value);
	}

	inline static int32_t get_offset_of_www_7() { return static_cast<int32_t>(offsetof(LoadAssetBundle_t379001212, ___www_7)); }
	inline WWW_t2919945039 * get_www_7() const { return ___www_7; }
	inline WWW_t2919945039 ** get_address_of_www_7() { return &___www_7; }
	inline void set_www_7(WWW_t2919945039 * value)
	{
		___www_7 = value;
		Il2CppCodeGenWriteBarrier(&___www_7, value);
	}

	inline static int32_t get_offset_of_mab_8() { return static_cast<int32_t>(offsetof(LoadAssetBundle_t379001212, ___mab_8)); }
	inline AssetBundle_t2054978754 * get_mab_8() const { return ___mab_8; }
	inline AssetBundle_t2054978754 ** get_address_of_mab_8() { return &___mab_8; }
	inline void set_mab_8(AssetBundle_t2054978754 * value)
	{
		___mab_8 = value;
		Il2CppCodeGenWriteBarrier(&___mab_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
