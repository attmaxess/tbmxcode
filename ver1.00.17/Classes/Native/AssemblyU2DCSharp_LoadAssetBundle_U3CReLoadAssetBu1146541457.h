﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_eAUDIOBGM4173725093.h"

// System.String
struct String_t;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// LoadAssetBundle
struct LoadAssetBundle_t379001212;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2
struct  U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457  : public Il2CppObject
{
public:
	// System.String LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::assetBundleName
	String_t* ___assetBundleName_0;
	// UnityEngine.AssetBundle LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::<ab>__0
	AssetBundle_t2054978754 * ___U3CabU3E__0_1;
	// System.String LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::assetName
	String_t* ___assetName_2;
	// eAUDIOBGM LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::bgm
	int32_t ___bgm_3;
	// LoadAssetBundle LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::$this
	LoadAssetBundle_t379001212 * ___U24this_4;
	// System.Object LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 LoadAssetBundle/<ReLoadAssetBundleAsBgm>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_assetBundleName_0() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___assetBundleName_0)); }
	inline String_t* get_assetBundleName_0() const { return ___assetBundleName_0; }
	inline String_t** get_address_of_assetBundleName_0() { return &___assetBundleName_0; }
	inline void set_assetBundleName_0(String_t* value)
	{
		___assetBundleName_0 = value;
		Il2CppCodeGenWriteBarrier(&___assetBundleName_0, value);
	}

	inline static int32_t get_offset_of_U3CabU3E__0_1() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___U3CabU3E__0_1)); }
	inline AssetBundle_t2054978754 * get_U3CabU3E__0_1() const { return ___U3CabU3E__0_1; }
	inline AssetBundle_t2054978754 ** get_address_of_U3CabU3E__0_1() { return &___U3CabU3E__0_1; }
	inline void set_U3CabU3E__0_1(AssetBundle_t2054978754 * value)
	{
		___U3CabU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CabU3E__0_1, value);
	}

	inline static int32_t get_offset_of_assetName_2() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___assetName_2)); }
	inline String_t* get_assetName_2() const { return ___assetName_2; }
	inline String_t** get_address_of_assetName_2() { return &___assetName_2; }
	inline void set_assetName_2(String_t* value)
	{
		___assetName_2 = value;
		Il2CppCodeGenWriteBarrier(&___assetName_2, value);
	}

	inline static int32_t get_offset_of_bgm_3() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___bgm_3)); }
	inline int32_t get_bgm_3() const { return ___bgm_3; }
	inline int32_t* get_address_of_bgm_3() { return &___bgm_3; }
	inline void set_bgm_3(int32_t value)
	{
		___bgm_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___U24this_4)); }
	inline LoadAssetBundle_t379001212 * get_U24this_4() const { return ___U24this_4; }
	inline LoadAssetBundle_t379001212 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(LoadAssetBundle_t379001212 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
