﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_eAREATYPE1739175762.h"

// System.String
struct String_t;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1038783543;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2674559435;
// LoadAssetBundle
struct LoadAssetBundle_t379001212;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3
struct  U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451  : public Il2CppObject
{
public:
	// eAREATYPE LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::areaType
	int32_t ___areaType_0;
	// System.String LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::assetName
	String_t* ___assetName_1;
	// System.String LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::assetBundleName
	String_t* ___assetBundleName_2;
	// UnityEngine.AssetBundleCreateRequest LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::<bundleLoadRequest>__0
	AssetBundleCreateRequest_t1038783543 * ___U3CbundleLoadRequestU3E__0_3;
	// UnityEngine.AssetBundle LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::<myLoadedAssetbundle>__0
	AssetBundle_t2054978754 * ___U3CmyLoadedAssetbundleU3E__0_4;
	// UnityEngine.AssetBundleRequest LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::<assetLoadRequest>__1
	AssetBundleRequest_t2674559435 * ___U3CassetLoadRequestU3E__1_5;
	// LoadAssetBundle LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::$this
	LoadAssetBundle_t379001212 * ___U24this_6;
	// System.Object LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::$disposing
	bool ___U24disposing_8;
	// System.Int32 LoadAssetBundle/<ReLoadAssetBundleAsAreaBgm>c__Iterator3::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_areaType_0() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___areaType_0)); }
	inline int32_t get_areaType_0() const { return ___areaType_0; }
	inline int32_t* get_address_of_areaType_0() { return &___areaType_0; }
	inline void set_areaType_0(int32_t value)
	{
		___areaType_0 = value;
	}

	inline static int32_t get_offset_of_assetName_1() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___assetName_1)); }
	inline String_t* get_assetName_1() const { return ___assetName_1; }
	inline String_t** get_address_of_assetName_1() { return &___assetName_1; }
	inline void set_assetName_1(String_t* value)
	{
		___assetName_1 = value;
		Il2CppCodeGenWriteBarrier(&___assetName_1, value);
	}

	inline static int32_t get_offset_of_assetBundleName_2() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___assetBundleName_2)); }
	inline String_t* get_assetBundleName_2() const { return ___assetBundleName_2; }
	inline String_t** get_address_of_assetBundleName_2() { return &___assetBundleName_2; }
	inline void set_assetBundleName_2(String_t* value)
	{
		___assetBundleName_2 = value;
		Il2CppCodeGenWriteBarrier(&___assetBundleName_2, value);
	}

	inline static int32_t get_offset_of_U3CbundleLoadRequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___U3CbundleLoadRequestU3E__0_3)); }
	inline AssetBundleCreateRequest_t1038783543 * get_U3CbundleLoadRequestU3E__0_3() const { return ___U3CbundleLoadRequestU3E__0_3; }
	inline AssetBundleCreateRequest_t1038783543 ** get_address_of_U3CbundleLoadRequestU3E__0_3() { return &___U3CbundleLoadRequestU3E__0_3; }
	inline void set_U3CbundleLoadRequestU3E__0_3(AssetBundleCreateRequest_t1038783543 * value)
	{
		___U3CbundleLoadRequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbundleLoadRequestU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CmyLoadedAssetbundleU3E__0_4() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___U3CmyLoadedAssetbundleU3E__0_4)); }
	inline AssetBundle_t2054978754 * get_U3CmyLoadedAssetbundleU3E__0_4() const { return ___U3CmyLoadedAssetbundleU3E__0_4; }
	inline AssetBundle_t2054978754 ** get_address_of_U3CmyLoadedAssetbundleU3E__0_4() { return &___U3CmyLoadedAssetbundleU3E__0_4; }
	inline void set_U3CmyLoadedAssetbundleU3E__0_4(AssetBundle_t2054978754 * value)
	{
		___U3CmyLoadedAssetbundleU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyLoadedAssetbundleU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U3CassetLoadRequestU3E__1_5() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___U3CassetLoadRequestU3E__1_5)); }
	inline AssetBundleRequest_t2674559435 * get_U3CassetLoadRequestU3E__1_5() const { return ___U3CassetLoadRequestU3E__1_5; }
	inline AssetBundleRequest_t2674559435 ** get_address_of_U3CassetLoadRequestU3E__1_5() { return &___U3CassetLoadRequestU3E__1_5; }
	inline void set_U3CassetLoadRequestU3E__1_5(AssetBundleRequest_t2674559435 * value)
	{
		___U3CassetLoadRequestU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CassetLoadRequestU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___U24this_6)); }
	inline LoadAssetBundle_t379001212 * get_U24this_6() const { return ___U24this_6; }
	inline LoadAssetBundle_t379001212 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(LoadAssetBundle_t379001212 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
