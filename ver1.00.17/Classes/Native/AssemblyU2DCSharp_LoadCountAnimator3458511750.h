﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadCountAnimator
struct  LoadCountAnimator_t3458511750  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text LoadCountAnimator::Text
	Text_t356221433 * ___Text_2;
	// System.Boolean LoadCountAnimator::IsCompleteToCountUp
	bool ___IsCompleteToCountUp_3;
	// System.Boolean LoadCountAnimator::IsCompleteToBlink
	bool ___IsCompleteToBlink_4;
	// System.Int32 LoadCountAnimator::sCountNum
	int32_t ___sCountNum_5;

public:
	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(LoadCountAnimator_t3458511750, ___Text_2)); }
	inline Text_t356221433 * get_Text_2() const { return ___Text_2; }
	inline Text_t356221433 ** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(Text_t356221433 * value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}

	inline static int32_t get_offset_of_IsCompleteToCountUp_3() { return static_cast<int32_t>(offsetof(LoadCountAnimator_t3458511750, ___IsCompleteToCountUp_3)); }
	inline bool get_IsCompleteToCountUp_3() const { return ___IsCompleteToCountUp_3; }
	inline bool* get_address_of_IsCompleteToCountUp_3() { return &___IsCompleteToCountUp_3; }
	inline void set_IsCompleteToCountUp_3(bool value)
	{
		___IsCompleteToCountUp_3 = value;
	}

	inline static int32_t get_offset_of_IsCompleteToBlink_4() { return static_cast<int32_t>(offsetof(LoadCountAnimator_t3458511750, ___IsCompleteToBlink_4)); }
	inline bool get_IsCompleteToBlink_4() const { return ___IsCompleteToBlink_4; }
	inline bool* get_address_of_IsCompleteToBlink_4() { return &___IsCompleteToBlink_4; }
	inline void set_IsCompleteToBlink_4(bool value)
	{
		___IsCompleteToBlink_4 = value;
	}

	inline static int32_t get_offset_of_sCountNum_5() { return static_cast<int32_t>(offsetof(LoadCountAnimator_t3458511750, ___sCountNum_5)); }
	inline int32_t get_sCountNum_5() const { return ___sCountNum_5; }
	inline int32_t* get_address_of_sCountNum_5() { return &___sCountNum_5; }
	inline void set_sCountNum_5(int32_t value)
	{
		___sCountNum_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
