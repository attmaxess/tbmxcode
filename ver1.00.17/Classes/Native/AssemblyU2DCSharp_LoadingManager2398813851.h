﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1095490186.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingManager
struct  LoadingManager_t2398813851  : public SingletonMonoBehaviour_1_t1095490186
{
public:
	// UnityEngine.Transform LoadingManager::Navit
	Transform_t3275118058 * ___Navit_3;
	// System.Single LoadingManager::NavitSpeed
	float ___NavitSpeed_4;
	// UnityEngine.Vector3 LoadingManager::m_vRotNavit
	Vector3_t2243707580  ___m_vRotNavit_5;

public:
	inline static int32_t get_offset_of_Navit_3() { return static_cast<int32_t>(offsetof(LoadingManager_t2398813851, ___Navit_3)); }
	inline Transform_t3275118058 * get_Navit_3() const { return ___Navit_3; }
	inline Transform_t3275118058 ** get_address_of_Navit_3() { return &___Navit_3; }
	inline void set_Navit_3(Transform_t3275118058 * value)
	{
		___Navit_3 = value;
		Il2CppCodeGenWriteBarrier(&___Navit_3, value);
	}

	inline static int32_t get_offset_of_NavitSpeed_4() { return static_cast<int32_t>(offsetof(LoadingManager_t2398813851, ___NavitSpeed_4)); }
	inline float get_NavitSpeed_4() const { return ___NavitSpeed_4; }
	inline float* get_address_of_NavitSpeed_4() { return &___NavitSpeed_4; }
	inline void set_NavitSpeed_4(float value)
	{
		___NavitSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_vRotNavit_5() { return static_cast<int32_t>(offsetof(LoadingManager_t2398813851, ___m_vRotNavit_5)); }
	inline Vector3_t2243707580  get_m_vRotNavit_5() const { return ___m_vRotNavit_5; }
	inline Vector3_t2243707580 * get_address_of_m_vRotNavit_5() { return &___m_vRotNavit_5; }
	inline void set_m_vRotNavit_5(Vector3_t2243707580  value)
	{
		___m_vRotNavit_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
