﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Localization_CountryCodeKey3757133144.h"

// System.Collections.Generic.Dictionary`2<Localization.LocalizeKey,LocalizeTextContents>
struct Dictionary_2_t540054039;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizeItem
struct  LocalizeItem_t998829008  : public Il2CppObject
{
public:
	// Localization.CountryCodeKey LocalizeItem::Key
	int32_t ___Key_0;
	// System.Collections.Generic.Dictionary`2<Localization.LocalizeKey,LocalizeTextContents> LocalizeItem::Item
	Dictionary_2_t540054039 * ___Item_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(LocalizeItem_t998829008, ___Key_0)); }
	inline int32_t get_Key_0() const { return ___Key_0; }
	inline int32_t* get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(int32_t value)
	{
		___Key_0 = value;
	}

	inline static int32_t get_offset_of_Item_1() { return static_cast<int32_t>(offsetof(LocalizeItem_t998829008, ___Item_1)); }
	inline Dictionary_2_t540054039 * get_Item_1() const { return ___Item_1; }
	inline Dictionary_2_t540054039 ** get_address_of_Item_1() { return &___Item_1; }
	inline void set_Item_1(Dictionary_2_t540054039 * value)
	{
		___Item_1 = value;
		Il2CppCodeGenWriteBarrier(&___Item_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
