﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen4256331373.h"
#include "AssemblyU2DCSharp_Localization_CountryCodeKey3757133144.h"

// DefaultCode
struct DefaultCode_t3729787656;
// CountryCode
struct CountryCode_t2172753331;
// Language
struct Language_t4035666274;
// UnityEngine.Font[]
struct FontU5BU5D_t4130692050;
// System.Collections.Generic.List`1<DefaultCodeData>
struct List_1_t1657382276;
// System.Collections.Generic.List`1<CountryCodeData>
struct List_1_t1134158735;
// System.Collections.Generic.List`1<LanguageData>
struct List_1_t4000954046;
// System.Collections.Generic.List`1<LocalizeItem>
struct List_1_t367950140;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizeManager
struct  LocalizeManager_t1264687742  : public SingletonMonoBehaviour_1_t4256331373
{
public:
	// DefaultCode LocalizeManager::DefaultSheet
	DefaultCode_t3729787656 * ___DefaultSheet_3;
	// CountryCode LocalizeManager::CountrySheet
	CountryCode_t2172753331 * ___CountrySheet_4;
	// Language LocalizeManager::LanguageSheet
	Language_t4035666274 * ___LanguageSheet_5;
	// UnityEngine.Font[] LocalizeManager::Fonts
	FontU5BU5D_t4130692050* ___Fonts_6;
	// System.Int32 LocalizeManager::UnusedKernig
	int32_t ___UnusedKernig_7;
	// System.Int32 LocalizeManager::LanguageNo
	int32_t ___LanguageNo_8;
	// Localization.CountryCodeKey LocalizeManager::m_DefaultCountryCode
	int32_t ___m_DefaultCountryCode_9;
	// Localization.CountryCodeKey LocalizeManager::m_CurrentCountryKey
	int32_t ___m_CurrentCountryKey_10;
	// System.Collections.Generic.List`1<DefaultCodeData> LocalizeManager::m_DefaultCodeSheetData
	List_1_t1657382276 * ___m_DefaultCodeSheetData_11;
	// System.Collections.Generic.List`1<CountryCodeData> LocalizeManager::m_CountryCodeSheetData
	List_1_t1134158735 * ___m_CountryCodeSheetData_12;
	// System.Collections.Generic.List`1<LanguageData> LocalizeManager::m_LanguageSheetData
	List_1_t4000954046 * ___m_LanguageSheetData_13;
	// System.Collections.Generic.List`1<LocalizeItem> LocalizeManager::m_LocalizeTextList
	List_1_t367950140 * ___m_LocalizeTextList_14;

public:
	inline static int32_t get_offset_of_DefaultSheet_3() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___DefaultSheet_3)); }
	inline DefaultCode_t3729787656 * get_DefaultSheet_3() const { return ___DefaultSheet_3; }
	inline DefaultCode_t3729787656 ** get_address_of_DefaultSheet_3() { return &___DefaultSheet_3; }
	inline void set_DefaultSheet_3(DefaultCode_t3729787656 * value)
	{
		___DefaultSheet_3 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultSheet_3, value);
	}

	inline static int32_t get_offset_of_CountrySheet_4() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___CountrySheet_4)); }
	inline CountryCode_t2172753331 * get_CountrySheet_4() const { return ___CountrySheet_4; }
	inline CountryCode_t2172753331 ** get_address_of_CountrySheet_4() { return &___CountrySheet_4; }
	inline void set_CountrySheet_4(CountryCode_t2172753331 * value)
	{
		___CountrySheet_4 = value;
		Il2CppCodeGenWriteBarrier(&___CountrySheet_4, value);
	}

	inline static int32_t get_offset_of_LanguageSheet_5() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___LanguageSheet_5)); }
	inline Language_t4035666274 * get_LanguageSheet_5() const { return ___LanguageSheet_5; }
	inline Language_t4035666274 ** get_address_of_LanguageSheet_5() { return &___LanguageSheet_5; }
	inline void set_LanguageSheet_5(Language_t4035666274 * value)
	{
		___LanguageSheet_5 = value;
		Il2CppCodeGenWriteBarrier(&___LanguageSheet_5, value);
	}

	inline static int32_t get_offset_of_Fonts_6() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___Fonts_6)); }
	inline FontU5BU5D_t4130692050* get_Fonts_6() const { return ___Fonts_6; }
	inline FontU5BU5D_t4130692050** get_address_of_Fonts_6() { return &___Fonts_6; }
	inline void set_Fonts_6(FontU5BU5D_t4130692050* value)
	{
		___Fonts_6 = value;
		Il2CppCodeGenWriteBarrier(&___Fonts_6, value);
	}

	inline static int32_t get_offset_of_UnusedKernig_7() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___UnusedKernig_7)); }
	inline int32_t get_UnusedKernig_7() const { return ___UnusedKernig_7; }
	inline int32_t* get_address_of_UnusedKernig_7() { return &___UnusedKernig_7; }
	inline void set_UnusedKernig_7(int32_t value)
	{
		___UnusedKernig_7 = value;
	}

	inline static int32_t get_offset_of_LanguageNo_8() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___LanguageNo_8)); }
	inline int32_t get_LanguageNo_8() const { return ___LanguageNo_8; }
	inline int32_t* get_address_of_LanguageNo_8() { return &___LanguageNo_8; }
	inline void set_LanguageNo_8(int32_t value)
	{
		___LanguageNo_8 = value;
	}

	inline static int32_t get_offset_of_m_DefaultCountryCode_9() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___m_DefaultCountryCode_9)); }
	inline int32_t get_m_DefaultCountryCode_9() const { return ___m_DefaultCountryCode_9; }
	inline int32_t* get_address_of_m_DefaultCountryCode_9() { return &___m_DefaultCountryCode_9; }
	inline void set_m_DefaultCountryCode_9(int32_t value)
	{
		___m_DefaultCountryCode_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentCountryKey_10() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___m_CurrentCountryKey_10)); }
	inline int32_t get_m_CurrentCountryKey_10() const { return ___m_CurrentCountryKey_10; }
	inline int32_t* get_address_of_m_CurrentCountryKey_10() { return &___m_CurrentCountryKey_10; }
	inline void set_m_CurrentCountryKey_10(int32_t value)
	{
		___m_CurrentCountryKey_10 = value;
	}

	inline static int32_t get_offset_of_m_DefaultCodeSheetData_11() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___m_DefaultCodeSheetData_11)); }
	inline List_1_t1657382276 * get_m_DefaultCodeSheetData_11() const { return ___m_DefaultCodeSheetData_11; }
	inline List_1_t1657382276 ** get_address_of_m_DefaultCodeSheetData_11() { return &___m_DefaultCodeSheetData_11; }
	inline void set_m_DefaultCodeSheetData_11(List_1_t1657382276 * value)
	{
		___m_DefaultCodeSheetData_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_DefaultCodeSheetData_11, value);
	}

	inline static int32_t get_offset_of_m_CountryCodeSheetData_12() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___m_CountryCodeSheetData_12)); }
	inline List_1_t1134158735 * get_m_CountryCodeSheetData_12() const { return ___m_CountryCodeSheetData_12; }
	inline List_1_t1134158735 ** get_address_of_m_CountryCodeSheetData_12() { return &___m_CountryCodeSheetData_12; }
	inline void set_m_CountryCodeSheetData_12(List_1_t1134158735 * value)
	{
		___m_CountryCodeSheetData_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_CountryCodeSheetData_12, value);
	}

	inline static int32_t get_offset_of_m_LanguageSheetData_13() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___m_LanguageSheetData_13)); }
	inline List_1_t4000954046 * get_m_LanguageSheetData_13() const { return ___m_LanguageSheetData_13; }
	inline List_1_t4000954046 ** get_address_of_m_LanguageSheetData_13() { return &___m_LanguageSheetData_13; }
	inline void set_m_LanguageSheetData_13(List_1_t4000954046 * value)
	{
		___m_LanguageSheetData_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_LanguageSheetData_13, value);
	}

	inline static int32_t get_offset_of_m_LocalizeTextList_14() { return static_cast<int32_t>(offsetof(LocalizeManager_t1264687742, ___m_LocalizeTextList_14)); }
	inline List_1_t367950140 * get_m_LocalizeTextList_14() const { return ___m_LocalizeTextList_14; }
	inline List_1_t367950140 ** get_address_of_m_LocalizeTextList_14() { return &___m_LocalizeTextList_14; }
	inline void set_m_LocalizeTextList_14(List_1_t367950140 * value)
	{
		___m_LocalizeTextList_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_LocalizeTextList_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
