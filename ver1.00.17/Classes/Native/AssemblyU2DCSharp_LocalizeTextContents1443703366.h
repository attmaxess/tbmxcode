﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizeTextContents
struct  LocalizeTextContents_t1443703366  : public Il2CppObject
{
public:
	// System.String LocalizeTextContents::Font
	String_t* ___Font_0;
	// System.Int32 LocalizeTextContents::Size
	int32_t ___Size_1;
	// System.Single LocalizeTextContents::Kerning
	float ___Kerning_2;
	// System.Single LocalizeTextContents::Linespacing
	float ___Linespacing_3;
	// System.String LocalizeTextContents::Content
	String_t* ___Content_4;

public:
	inline static int32_t get_offset_of_Font_0() { return static_cast<int32_t>(offsetof(LocalizeTextContents_t1443703366, ___Font_0)); }
	inline String_t* get_Font_0() const { return ___Font_0; }
	inline String_t** get_address_of_Font_0() { return &___Font_0; }
	inline void set_Font_0(String_t* value)
	{
		___Font_0 = value;
		Il2CppCodeGenWriteBarrier(&___Font_0, value);
	}

	inline static int32_t get_offset_of_Size_1() { return static_cast<int32_t>(offsetof(LocalizeTextContents_t1443703366, ___Size_1)); }
	inline int32_t get_Size_1() const { return ___Size_1; }
	inline int32_t* get_address_of_Size_1() { return &___Size_1; }
	inline void set_Size_1(int32_t value)
	{
		___Size_1 = value;
	}

	inline static int32_t get_offset_of_Kerning_2() { return static_cast<int32_t>(offsetof(LocalizeTextContents_t1443703366, ___Kerning_2)); }
	inline float get_Kerning_2() const { return ___Kerning_2; }
	inline float* get_address_of_Kerning_2() { return &___Kerning_2; }
	inline void set_Kerning_2(float value)
	{
		___Kerning_2 = value;
	}

	inline static int32_t get_offset_of_Linespacing_3() { return static_cast<int32_t>(offsetof(LocalizeTextContents_t1443703366, ___Linespacing_3)); }
	inline float get_Linespacing_3() const { return ___Linespacing_3; }
	inline float* get_address_of_Linespacing_3() { return &___Linespacing_3; }
	inline void set_Linespacing_3(float value)
	{
		___Linespacing_3 = value;
	}

	inline static int32_t get_offset_of_Content_4() { return static_cast<int32_t>(offsetof(LocalizeTextContents_t1443703366, ___Content_4)); }
	inline String_t* get_Content_4() const { return ___Content_4; }
	inline String_t** get_address_of_Content_4() { return &___Content_4; }
	inline void set_Content_4(String_t* value)
	{
		___Content_4 = value;
		Il2CppCodeGenWriteBarrier(&___Content_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
