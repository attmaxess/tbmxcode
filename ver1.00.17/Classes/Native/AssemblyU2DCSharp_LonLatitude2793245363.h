﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_eAREATYPE1739175762.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LonLatitude
struct  LonLatitude_t2793245363  : public Il2CppObject
{
public:
	// eAREATYPE LonLatitude::areaType
	int32_t ___areaType_0;
	// UnityEngine.Vector2 LonLatitude::min
	Vector2_t2243707579  ___min_1;
	// UnityEngine.Vector2 LonLatitude::max
	Vector2_t2243707579  ___max_2;

public:
	inline static int32_t get_offset_of_areaType_0() { return static_cast<int32_t>(offsetof(LonLatitude_t2793245363, ___areaType_0)); }
	inline int32_t get_areaType_0() const { return ___areaType_0; }
	inline int32_t* get_address_of_areaType_0() { return &___areaType_0; }
	inline void set_areaType_0(int32_t value)
	{
		___areaType_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(LonLatitude_t2793245363, ___min_1)); }
	inline Vector2_t2243707579  get_min_1() const { return ___min_1; }
	inline Vector2_t2243707579 * get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(Vector2_t2243707579  value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(LonLatitude_t2793245363, ___max_2)); }
	inline Vector2_t2243707579  get_max_2() const { return ___max_2; }
	inline Vector2_t2243707579 * get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(Vector2_t2243707579  value)
	{
		___max_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
