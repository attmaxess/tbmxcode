﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Mesh
struct Mesh_t1356156583;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshCombineUtilityAFS/MeshInstance
struct  MeshInstance_t2175729061 
{
public:
	// UnityEngine.Mesh MeshCombineUtilityAFS/MeshInstance::mesh
	Mesh_t1356156583 * ___mesh_0;
	// System.Int32 MeshCombineUtilityAFS/MeshInstance::subMeshIndex
	int32_t ___subMeshIndex_1;
	// UnityEngine.Matrix4x4 MeshCombineUtilityAFS/MeshInstance::transform
	Matrix4x4_t2933234003  ___transform_2;
	// UnityEngine.Vector3 MeshCombineUtilityAFS/MeshInstance::groundNormal
	Vector3_t2243707580  ___groundNormal_3;
	// System.Single MeshCombineUtilityAFS/MeshInstance::scale
	float ___scale_4;
	// UnityEngine.Vector3 MeshCombineUtilityAFS/MeshInstance::pivot
	Vector3_t2243707580  ___pivot_5;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(MeshInstance_t2175729061, ___mesh_0)); }
	inline Mesh_t1356156583 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t1356156583 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t1356156583 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_0, value);
	}

	inline static int32_t get_offset_of_subMeshIndex_1() { return static_cast<int32_t>(offsetof(MeshInstance_t2175729061, ___subMeshIndex_1)); }
	inline int32_t get_subMeshIndex_1() const { return ___subMeshIndex_1; }
	inline int32_t* get_address_of_subMeshIndex_1() { return &___subMeshIndex_1; }
	inline void set_subMeshIndex_1(int32_t value)
	{
		___subMeshIndex_1 = value;
	}

	inline static int32_t get_offset_of_transform_2() { return static_cast<int32_t>(offsetof(MeshInstance_t2175729061, ___transform_2)); }
	inline Matrix4x4_t2933234003  get_transform_2() const { return ___transform_2; }
	inline Matrix4x4_t2933234003 * get_address_of_transform_2() { return &___transform_2; }
	inline void set_transform_2(Matrix4x4_t2933234003  value)
	{
		___transform_2 = value;
	}

	inline static int32_t get_offset_of_groundNormal_3() { return static_cast<int32_t>(offsetof(MeshInstance_t2175729061, ___groundNormal_3)); }
	inline Vector3_t2243707580  get_groundNormal_3() const { return ___groundNormal_3; }
	inline Vector3_t2243707580 * get_address_of_groundNormal_3() { return &___groundNormal_3; }
	inline void set_groundNormal_3(Vector3_t2243707580  value)
	{
		___groundNormal_3 = value;
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(MeshInstance_t2175729061, ___scale_4)); }
	inline float get_scale_4() const { return ___scale_4; }
	inline float* get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(float value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_pivot_5() { return static_cast<int32_t>(offsetof(MeshInstance_t2175729061, ___pivot_5)); }
	inline Vector3_t2243707580  get_pivot_5() const { return ___pivot_5; }
	inline Vector3_t2243707580 * get_address_of_pivot_5() { return &___pivot_5; }
	inline void set_pivot_5(Vector3_t2243707580  value)
	{
		___pivot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MeshCombineUtilityAFS/MeshInstance
struct MeshInstance_t2175729061_marshaled_pinvoke
{
	Mesh_t1356156583 * ___mesh_0;
	int32_t ___subMeshIndex_1;
	Matrix4x4_t2933234003  ___transform_2;
	Vector3_t2243707580  ___groundNormal_3;
	float ___scale_4;
	Vector3_t2243707580  ___pivot_5;
};
// Native definition for COM marshalling of MeshCombineUtilityAFS/MeshInstance
struct MeshInstance_t2175729061_marshaled_com
{
	Mesh_t1356156583 * ___mesh_0;
	int32_t ___subMeshIndex_1;
	Matrix4x4_t2933234003  ___transform_2;
	Vector3_t2243707580  ___groundNormal_3;
	float ___scale_4;
	Vector3_t2243707580  ___pivot_5;
};
