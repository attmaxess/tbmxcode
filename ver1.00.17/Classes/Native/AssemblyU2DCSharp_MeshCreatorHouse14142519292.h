﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshCreatorHouse1
struct  MeshCreatorHouse1_t4142519292  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material MeshCreatorHouse1::material
	Material_t193706927 * ___material_2;
	// UnityEngine.GameObject MeshCreatorHouse1::prefab
	GameObject_t1756533147 * ___prefab_3;
	// UnityEngine.Transform MeshCreatorHouse1::parent
	Transform_t3275118058 * ___parent_4;
	// UnityEngine.Camera MeshCreatorHouse1::camera
	Camera_t189460977 * ___camera_5;
	// UnityEngine.GameObject MeshCreatorHouse1::scrollbar
	GameObject_t1756533147 * ___scrollbar_6;
	// UnityEngine.GameObject MeshCreatorHouse1::SCT
	GameObject_t1756533147 * ___SCT_7;
	// UnityEngine.GameObject MeshCreatorHouse1::ST
	GameObject_t1756533147 * ___ST_8;
	// UnityEngine.GameObject MeshCreatorHouse1::ET
	GameObject_t1756533147 * ___ET_9;
	// UnityEngine.GameObject MeshCreatorHouse1::ERT
	GameObject_t1756533147 * ___ERT_10;
	// System.Single MeshCreatorHouse1::time
	float ___time_14;
	// System.Single MeshCreatorHouse1::times
	float ___times_15;
	// System.Single MeshCreatorHouse1::timee
	float ___timee_16;
	// System.Single MeshCreatorHouse1::timeStart
	float ___timeStart_17;
	// System.Single MeshCreatorHouse1::timeEnd
	float ___timeEnd_18;
	// System.Int32 MeshCreatorHouse1::p
	int32_t ___p_19;
	// System.Int32 MeshCreatorHouse1::n
	int32_t ___n_20;
	// System.Boolean MeshCreatorHouse1::isStarted
	bool ___isStarted_21;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___material_2)); }
	inline Material_t193706927 * get_material_2() const { return ___material_2; }
	inline Material_t193706927 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t193706927 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier(&___material_2, value);
	}

	inline static int32_t get_offset_of_prefab_3() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___prefab_3)); }
	inline GameObject_t1756533147 * get_prefab_3() const { return ___prefab_3; }
	inline GameObject_t1756533147 ** get_address_of_prefab_3() { return &___prefab_3; }
	inline void set_prefab_3(GameObject_t1756533147 * value)
	{
		___prefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_3, value);
	}

	inline static int32_t get_offset_of_parent_4() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___parent_4)); }
	inline Transform_t3275118058 * get_parent_4() const { return ___parent_4; }
	inline Transform_t3275118058 ** get_address_of_parent_4() { return &___parent_4; }
	inline void set_parent_4(Transform_t3275118058 * value)
	{
		___parent_4 = value;
		Il2CppCodeGenWriteBarrier(&___parent_4, value);
	}

	inline static int32_t get_offset_of_camera_5() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___camera_5)); }
	inline Camera_t189460977 * get_camera_5() const { return ___camera_5; }
	inline Camera_t189460977 ** get_address_of_camera_5() { return &___camera_5; }
	inline void set_camera_5(Camera_t189460977 * value)
	{
		___camera_5 = value;
		Il2CppCodeGenWriteBarrier(&___camera_5, value);
	}

	inline static int32_t get_offset_of_scrollbar_6() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___scrollbar_6)); }
	inline GameObject_t1756533147 * get_scrollbar_6() const { return ___scrollbar_6; }
	inline GameObject_t1756533147 ** get_address_of_scrollbar_6() { return &___scrollbar_6; }
	inline void set_scrollbar_6(GameObject_t1756533147 * value)
	{
		___scrollbar_6 = value;
		Il2CppCodeGenWriteBarrier(&___scrollbar_6, value);
	}

	inline static int32_t get_offset_of_SCT_7() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___SCT_7)); }
	inline GameObject_t1756533147 * get_SCT_7() const { return ___SCT_7; }
	inline GameObject_t1756533147 ** get_address_of_SCT_7() { return &___SCT_7; }
	inline void set_SCT_7(GameObject_t1756533147 * value)
	{
		___SCT_7 = value;
		Il2CppCodeGenWriteBarrier(&___SCT_7, value);
	}

	inline static int32_t get_offset_of_ST_8() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___ST_8)); }
	inline GameObject_t1756533147 * get_ST_8() const { return ___ST_8; }
	inline GameObject_t1756533147 ** get_address_of_ST_8() { return &___ST_8; }
	inline void set_ST_8(GameObject_t1756533147 * value)
	{
		___ST_8 = value;
		Il2CppCodeGenWriteBarrier(&___ST_8, value);
	}

	inline static int32_t get_offset_of_ET_9() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___ET_9)); }
	inline GameObject_t1756533147 * get_ET_9() const { return ___ET_9; }
	inline GameObject_t1756533147 ** get_address_of_ET_9() { return &___ET_9; }
	inline void set_ET_9(GameObject_t1756533147 * value)
	{
		___ET_9 = value;
		Il2CppCodeGenWriteBarrier(&___ET_9, value);
	}

	inline static int32_t get_offset_of_ERT_10() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___ERT_10)); }
	inline GameObject_t1756533147 * get_ERT_10() const { return ___ERT_10; }
	inline GameObject_t1756533147 ** get_address_of_ERT_10() { return &___ERT_10; }
	inline void set_ERT_10(GameObject_t1756533147 * value)
	{
		___ERT_10 = value;
		Il2CppCodeGenWriteBarrier(&___ERT_10, value);
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_times_15() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___times_15)); }
	inline float get_times_15() const { return ___times_15; }
	inline float* get_address_of_times_15() { return &___times_15; }
	inline void set_times_15(float value)
	{
		___times_15 = value;
	}

	inline static int32_t get_offset_of_timee_16() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___timee_16)); }
	inline float get_timee_16() const { return ___timee_16; }
	inline float* get_address_of_timee_16() { return &___timee_16; }
	inline void set_timee_16(float value)
	{
		___timee_16 = value;
	}

	inline static int32_t get_offset_of_timeStart_17() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___timeStart_17)); }
	inline float get_timeStart_17() const { return ___timeStart_17; }
	inline float* get_address_of_timeStart_17() { return &___timeStart_17; }
	inline void set_timeStart_17(float value)
	{
		___timeStart_17 = value;
	}

	inline static int32_t get_offset_of_timeEnd_18() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___timeEnd_18)); }
	inline float get_timeEnd_18() const { return ___timeEnd_18; }
	inline float* get_address_of_timeEnd_18() { return &___timeEnd_18; }
	inline void set_timeEnd_18(float value)
	{
		___timeEnd_18 = value;
	}

	inline static int32_t get_offset_of_p_19() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___p_19)); }
	inline int32_t get_p_19() const { return ___p_19; }
	inline int32_t* get_address_of_p_19() { return &___p_19; }
	inline void set_p_19(int32_t value)
	{
		___p_19 = value;
	}

	inline static int32_t get_offset_of_n_20() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___n_20)); }
	inline int32_t get_n_20() const { return ___n_20; }
	inline int32_t* get_address_of_n_20() { return &___n_20; }
	inline void set_n_20(int32_t value)
	{
		___n_20 = value;
	}

	inline static int32_t get_offset_of_isStarted_21() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292, ___isStarted_21)); }
	inline bool get_isStarted_21() const { return ___isStarted_21; }
	inline bool* get_address_of_isStarted_21() { return &___isStarted_21; }
	inline void set_isStarted_21(bool value)
	{
		___isStarted_21 = value;
	}
};

struct MeshCreatorHouse1_t4142519292_StaticFields
{
public:
	// System.Int32 MeshCreatorHouse1::readNum
	int32_t ___readNum_11;
	// System.Int32 MeshCreatorHouse1::nowNum
	int32_t ___nowNum_12;
	// System.Single MeshCreatorHouse1::timeR
	float ___timeR_13;

public:
	inline static int32_t get_offset_of_readNum_11() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292_StaticFields, ___readNum_11)); }
	inline int32_t get_readNum_11() const { return ___readNum_11; }
	inline int32_t* get_address_of_readNum_11() { return &___readNum_11; }
	inline void set_readNum_11(int32_t value)
	{
		___readNum_11 = value;
	}

	inline static int32_t get_offset_of_nowNum_12() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292_StaticFields, ___nowNum_12)); }
	inline int32_t get_nowNum_12() const { return ___nowNum_12; }
	inline int32_t* get_address_of_nowNum_12() { return &___nowNum_12; }
	inline void set_nowNum_12(int32_t value)
	{
		___nowNum_12 = value;
	}

	inline static int32_t get_offset_of_timeR_13() { return static_cast<int32_t>(offsetof(MeshCreatorHouse1_t4142519292_StaticFields, ___timeR_13)); }
	inline float get_timeR_13() const { return ___timeR_13; }
	inline float* get_address_of_timeR_13() { return &___timeR_13; }
	inline void set_timeR_13(float value)
	{
		___timeR_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
