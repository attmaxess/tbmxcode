﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshCreatorHouse1/MeshData
struct  MeshData_t1378722312  : public Il2CppObject
{
public:
	// UnityEngine.Vector3[] MeshCreatorHouse1/MeshData::vertices
	Vector3U5BU5D_t1172311765* ___vertices_0;
	// System.Int32[] MeshCreatorHouse1/MeshData::triangles
	Int32U5BU5D_t3030399641* ___triangles_1;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(MeshData_t1378722312, ___vertices_0)); }
	inline Vector3U5BU5D_t1172311765* get_vertices_0() const { return ___vertices_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(Vector3U5BU5D_t1172311765* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_0, value);
	}

	inline static int32_t get_offset_of_triangles_1() { return static_cast<int32_t>(offsetof(MeshData_t1378722312, ___triangles_1)); }
	inline Int32U5BU5D_t3030399641* get_triangles_1() const { return ___triangles_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_triangles_1() { return &___triangles_1; }
	inline void set_triangles_1(Int32U5BU5D_t3030399641* value)
	{
		___triangles_1 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
