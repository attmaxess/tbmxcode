﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<MeshCreatorHouse2/MeshData>[]
struct List_1U5BU5D_t3489311246;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshCreatorHouse2
struct  MeshCreatorHouse2_t4142519291  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material MeshCreatorHouse2::material
	Material_t193706927 * ___material_2;
	// UnityEngine.GameObject MeshCreatorHouse2::prefab
	GameObject_t1756533147 * ___prefab_3;
	// UnityEngine.Transform MeshCreatorHouse2::parent
	Transform_t3275118058 * ___parent_4;
	// System.Collections.Generic.List`1<MeshCreatorHouse2/MeshData>[] MeshCreatorHouse2::meshsp
	List_1U5BU5D_t3489311246* ___meshsp_5;
	// System.Int32 MeshCreatorHouse2::p
	int32_t ___p_6;
	// System.Single MeshCreatorHouse2::timew
	float ___timew_7;
	// System.Single MeshCreatorHouse2::time
	float ___time_8;
	// System.Int32 MeshCreatorHouse2::k
	int32_t ___k_9;
	// System.Int32 MeshCreatorHouse2::iw
	int32_t ___iw_10;
	// System.Boolean MeshCreatorHouse2::isStarted
	bool ___isStarted_11;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___material_2)); }
	inline Material_t193706927 * get_material_2() const { return ___material_2; }
	inline Material_t193706927 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t193706927 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier(&___material_2, value);
	}

	inline static int32_t get_offset_of_prefab_3() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___prefab_3)); }
	inline GameObject_t1756533147 * get_prefab_3() const { return ___prefab_3; }
	inline GameObject_t1756533147 ** get_address_of_prefab_3() { return &___prefab_3; }
	inline void set_prefab_3(GameObject_t1756533147 * value)
	{
		___prefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_3, value);
	}

	inline static int32_t get_offset_of_parent_4() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___parent_4)); }
	inline Transform_t3275118058 * get_parent_4() const { return ___parent_4; }
	inline Transform_t3275118058 ** get_address_of_parent_4() { return &___parent_4; }
	inline void set_parent_4(Transform_t3275118058 * value)
	{
		___parent_4 = value;
		Il2CppCodeGenWriteBarrier(&___parent_4, value);
	}

	inline static int32_t get_offset_of_meshsp_5() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___meshsp_5)); }
	inline List_1U5BU5D_t3489311246* get_meshsp_5() const { return ___meshsp_5; }
	inline List_1U5BU5D_t3489311246** get_address_of_meshsp_5() { return &___meshsp_5; }
	inline void set_meshsp_5(List_1U5BU5D_t3489311246* value)
	{
		___meshsp_5 = value;
		Il2CppCodeGenWriteBarrier(&___meshsp_5, value);
	}

	inline static int32_t get_offset_of_p_6() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___p_6)); }
	inline int32_t get_p_6() const { return ___p_6; }
	inline int32_t* get_address_of_p_6() { return &___p_6; }
	inline void set_p_6(int32_t value)
	{
		___p_6 = value;
	}

	inline static int32_t get_offset_of_timew_7() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___timew_7)); }
	inline float get_timew_7() const { return ___timew_7; }
	inline float* get_address_of_timew_7() { return &___timew_7; }
	inline void set_timew_7(float value)
	{
		___timew_7 = value;
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_k_9() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___k_9)); }
	inline int32_t get_k_9() const { return ___k_9; }
	inline int32_t* get_address_of_k_9() { return &___k_9; }
	inline void set_k_9(int32_t value)
	{
		___k_9 = value;
	}

	inline static int32_t get_offset_of_iw_10() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___iw_10)); }
	inline int32_t get_iw_10() const { return ___iw_10; }
	inline int32_t* get_address_of_iw_10() { return &___iw_10; }
	inline void set_iw_10(int32_t value)
	{
		___iw_10 = value;
	}

	inline static int32_t get_offset_of_isStarted_11() { return static_cast<int32_t>(offsetof(MeshCreatorHouse2_t4142519291, ___isStarted_11)); }
	inline bool get_isStarted_11() const { return ___isStarted_11; }
	inline bool* get_address_of_isStarted_11() { return &___isStarted_11; }
	inline void set_isStarted_11(bool value)
	{
		___isStarted_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
