﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<MeshCreatorHouse1/MeshData>[]
struct List_1U5BU5D_t1295581245;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.IO.BinaryReader
struct BinaryReader_t2491843768;
// System.Collections.Generic.List`1<MeshCreatorHouse1/MeshData>
struct List_1_t747843444;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshReadHouse1
struct  MeshReadHouse1_t3589368014  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MeshReadHouse1::time
	float ___time_3;
	// System.Int32 MeshReadHouse1::a
	int32_t ___a_5;
	// System.Int32 MeshReadHouse1::a1
	int32_t ___a1_6;
	// System.Int32 MeshReadHouse1::b
	int32_t ___b_7;
	// System.Int32 MeshReadHouse1::b1
	int32_t ___b1_8;
	// System.Int32 MeshReadHouse1::c
	int32_t ___c_9;
	// System.Int32 MeshReadHouse1::c1
	int32_t ___c1_10;
	// System.Int32 MeshReadHouse1::ds
	int32_t ___ds_11;
	// System.Int32 MeshReadHouse1::es
	int32_t ___es_12;
	// System.Int32 MeshReadHouse1::numa
	int32_t ___numa_13;
	// System.Int32 MeshReadHouse1::numOfVertex
	int32_t ___numOfVertex_14;
	// System.Int32[] MeshReadHouse1::triangles
	Int32U5BU5D_t3030399641* ___triangles_15;
	// UnityEngine.Vector3[] MeshReadHouse1::vertices
	Vector3U5BU5D_t1172311765* ___vertices_16;
	// System.IO.BinaryReader MeshReadHouse1::reader
	BinaryReader_t2491843768 * ___reader_17;
	// System.Int32 MeshReadHouse1::numOfTriangles
	int32_t ___numOfTriangles_18;
	// System.Collections.Generic.List`1<MeshCreatorHouse1/MeshData> MeshReadHouse1::meshs
	List_1_t747843444 * ___meshs_19;
	// System.Single MeshReadHouse1::r1
	float ___r1_20;
	// System.Single MeshReadHouse1::r2
	float ___r2_21;
	// System.Single MeshReadHouse1::re
	float ___re_22;
	// System.Int32 MeshReadHouse1::v
	int32_t ___v_23;
	// System.Single MeshReadHouse1::x
	float ___x_24;
	// System.Single MeshReadHouse1::y
	float ___y_25;
	// System.Single MeshReadHouse1::z
	float ___z_26;

public:
	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___time_3)); }
	inline float get_time_3() const { return ___time_3; }
	inline float* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(float value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___a_5)); }
	inline int32_t get_a_5() const { return ___a_5; }
	inline int32_t* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(int32_t value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_a1_6() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___a1_6)); }
	inline int32_t get_a1_6() const { return ___a1_6; }
	inline int32_t* get_address_of_a1_6() { return &___a1_6; }
	inline void set_a1_6(int32_t value)
	{
		___a1_6 = value;
	}

	inline static int32_t get_offset_of_b_7() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___b_7)); }
	inline int32_t get_b_7() const { return ___b_7; }
	inline int32_t* get_address_of_b_7() { return &___b_7; }
	inline void set_b_7(int32_t value)
	{
		___b_7 = value;
	}

	inline static int32_t get_offset_of_b1_8() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___b1_8)); }
	inline int32_t get_b1_8() const { return ___b1_8; }
	inline int32_t* get_address_of_b1_8() { return &___b1_8; }
	inline void set_b1_8(int32_t value)
	{
		___b1_8 = value;
	}

	inline static int32_t get_offset_of_c_9() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___c_9)); }
	inline int32_t get_c_9() const { return ___c_9; }
	inline int32_t* get_address_of_c_9() { return &___c_9; }
	inline void set_c_9(int32_t value)
	{
		___c_9 = value;
	}

	inline static int32_t get_offset_of_c1_10() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___c1_10)); }
	inline int32_t get_c1_10() const { return ___c1_10; }
	inline int32_t* get_address_of_c1_10() { return &___c1_10; }
	inline void set_c1_10(int32_t value)
	{
		___c1_10 = value;
	}

	inline static int32_t get_offset_of_ds_11() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___ds_11)); }
	inline int32_t get_ds_11() const { return ___ds_11; }
	inline int32_t* get_address_of_ds_11() { return &___ds_11; }
	inline void set_ds_11(int32_t value)
	{
		___ds_11 = value;
	}

	inline static int32_t get_offset_of_es_12() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___es_12)); }
	inline int32_t get_es_12() const { return ___es_12; }
	inline int32_t* get_address_of_es_12() { return &___es_12; }
	inline void set_es_12(int32_t value)
	{
		___es_12 = value;
	}

	inline static int32_t get_offset_of_numa_13() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___numa_13)); }
	inline int32_t get_numa_13() const { return ___numa_13; }
	inline int32_t* get_address_of_numa_13() { return &___numa_13; }
	inline void set_numa_13(int32_t value)
	{
		___numa_13 = value;
	}

	inline static int32_t get_offset_of_numOfVertex_14() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___numOfVertex_14)); }
	inline int32_t get_numOfVertex_14() const { return ___numOfVertex_14; }
	inline int32_t* get_address_of_numOfVertex_14() { return &___numOfVertex_14; }
	inline void set_numOfVertex_14(int32_t value)
	{
		___numOfVertex_14 = value;
	}

	inline static int32_t get_offset_of_triangles_15() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___triangles_15)); }
	inline Int32U5BU5D_t3030399641* get_triangles_15() const { return ___triangles_15; }
	inline Int32U5BU5D_t3030399641** get_address_of_triangles_15() { return &___triangles_15; }
	inline void set_triangles_15(Int32U5BU5D_t3030399641* value)
	{
		___triangles_15 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_15, value);
	}

	inline static int32_t get_offset_of_vertices_16() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___vertices_16)); }
	inline Vector3U5BU5D_t1172311765* get_vertices_16() const { return ___vertices_16; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vertices_16() { return &___vertices_16; }
	inline void set_vertices_16(Vector3U5BU5D_t1172311765* value)
	{
		___vertices_16 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_16, value);
	}

	inline static int32_t get_offset_of_reader_17() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___reader_17)); }
	inline BinaryReader_t2491843768 * get_reader_17() const { return ___reader_17; }
	inline BinaryReader_t2491843768 ** get_address_of_reader_17() { return &___reader_17; }
	inline void set_reader_17(BinaryReader_t2491843768 * value)
	{
		___reader_17 = value;
		Il2CppCodeGenWriteBarrier(&___reader_17, value);
	}

	inline static int32_t get_offset_of_numOfTriangles_18() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___numOfTriangles_18)); }
	inline int32_t get_numOfTriangles_18() const { return ___numOfTriangles_18; }
	inline int32_t* get_address_of_numOfTriangles_18() { return &___numOfTriangles_18; }
	inline void set_numOfTriangles_18(int32_t value)
	{
		___numOfTriangles_18 = value;
	}

	inline static int32_t get_offset_of_meshs_19() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___meshs_19)); }
	inline List_1_t747843444 * get_meshs_19() const { return ___meshs_19; }
	inline List_1_t747843444 ** get_address_of_meshs_19() { return &___meshs_19; }
	inline void set_meshs_19(List_1_t747843444 * value)
	{
		___meshs_19 = value;
		Il2CppCodeGenWriteBarrier(&___meshs_19, value);
	}

	inline static int32_t get_offset_of_r1_20() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___r1_20)); }
	inline float get_r1_20() const { return ___r1_20; }
	inline float* get_address_of_r1_20() { return &___r1_20; }
	inline void set_r1_20(float value)
	{
		___r1_20 = value;
	}

	inline static int32_t get_offset_of_r2_21() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___r2_21)); }
	inline float get_r2_21() const { return ___r2_21; }
	inline float* get_address_of_r2_21() { return &___r2_21; }
	inline void set_r2_21(float value)
	{
		___r2_21 = value;
	}

	inline static int32_t get_offset_of_re_22() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___re_22)); }
	inline float get_re_22() const { return ___re_22; }
	inline float* get_address_of_re_22() { return &___re_22; }
	inline void set_re_22(float value)
	{
		___re_22 = value;
	}

	inline static int32_t get_offset_of_v_23() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___v_23)); }
	inline int32_t get_v_23() const { return ___v_23; }
	inline int32_t* get_address_of_v_23() { return &___v_23; }
	inline void set_v_23(int32_t value)
	{
		___v_23 = value;
	}

	inline static int32_t get_offset_of_x_24() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___x_24)); }
	inline float get_x_24() const { return ___x_24; }
	inline float* get_address_of_x_24() { return &___x_24; }
	inline void set_x_24(float value)
	{
		___x_24 = value;
	}

	inline static int32_t get_offset_of_y_25() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___y_25)); }
	inline float get_y_25() const { return ___y_25; }
	inline float* get_address_of_y_25() { return &___y_25; }
	inline void set_y_25(float value)
	{
		___y_25 = value;
	}

	inline static int32_t get_offset_of_z_26() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014, ___z_26)); }
	inline float get_z_26() const { return ___z_26; }
	inline float* get_address_of_z_26() { return &___z_26; }
	inline void set_z_26(float value)
	{
		___z_26 = value;
	}
};

struct MeshReadHouse1_t3589368014_StaticFields
{
public:
	// System.Collections.Generic.List`1<MeshCreatorHouse1/MeshData>[] MeshReadHouse1::meshsp
	List_1U5BU5D_t1295581245* ___meshsp_2;
	// System.Int32 MeshReadHouse1::k
	int32_t ___k_4;

public:
	inline static int32_t get_offset_of_meshsp_2() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014_StaticFields, ___meshsp_2)); }
	inline List_1U5BU5D_t1295581245* get_meshsp_2() const { return ___meshsp_2; }
	inline List_1U5BU5D_t1295581245** get_address_of_meshsp_2() { return &___meshsp_2; }
	inline void set_meshsp_2(List_1U5BU5D_t1295581245* value)
	{
		___meshsp_2 = value;
		Il2CppCodeGenWriteBarrier(&___meshsp_2, value);
	}

	inline static int32_t get_offset_of_k_4() { return static_cast<int32_t>(offsetof(MeshReadHouse1_t3589368014_StaticFields, ___k_4)); }
	inline int32_t get_k_4() const { return ___k_4; }
	inline int32_t* get_address_of_k_4() { return &___k_4; }
	inline void set_k_4(int32_t value)
	{
		___k_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
