﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.InputField
struct InputField_t1631627530;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MigrateGetCodePanel
struct  MigrateGetCodePanel_t3530957368  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MigrateGetCodePanel::InputArea
	GameObject_t1756533147 * ___InputArea_2;
	// UnityEngine.GameObject MigrateGetCodePanel::IntructionArea
	GameObject_t1756533147 * ___IntructionArea_3;
	// UnityEngine.GameObject MigrateGetCodePanel::buttonClear
	GameObject_t1756533147 * ___buttonClear_4;
	// UnityEngine.UI.InputField MigrateGetCodePanel::IDUserInput
	InputField_t1631627530 * ___IDUserInput_5;
	// UnityEngine.UI.InputField MigrateGetCodePanel::passwordInput
	InputField_t1631627530 * ___passwordInput_6;

public:
	inline static int32_t get_offset_of_InputArea_2() { return static_cast<int32_t>(offsetof(MigrateGetCodePanel_t3530957368, ___InputArea_2)); }
	inline GameObject_t1756533147 * get_InputArea_2() const { return ___InputArea_2; }
	inline GameObject_t1756533147 ** get_address_of_InputArea_2() { return &___InputArea_2; }
	inline void set_InputArea_2(GameObject_t1756533147 * value)
	{
		___InputArea_2 = value;
		Il2CppCodeGenWriteBarrier(&___InputArea_2, value);
	}

	inline static int32_t get_offset_of_IntructionArea_3() { return static_cast<int32_t>(offsetof(MigrateGetCodePanel_t3530957368, ___IntructionArea_3)); }
	inline GameObject_t1756533147 * get_IntructionArea_3() const { return ___IntructionArea_3; }
	inline GameObject_t1756533147 ** get_address_of_IntructionArea_3() { return &___IntructionArea_3; }
	inline void set_IntructionArea_3(GameObject_t1756533147 * value)
	{
		___IntructionArea_3 = value;
		Il2CppCodeGenWriteBarrier(&___IntructionArea_3, value);
	}

	inline static int32_t get_offset_of_buttonClear_4() { return static_cast<int32_t>(offsetof(MigrateGetCodePanel_t3530957368, ___buttonClear_4)); }
	inline GameObject_t1756533147 * get_buttonClear_4() const { return ___buttonClear_4; }
	inline GameObject_t1756533147 ** get_address_of_buttonClear_4() { return &___buttonClear_4; }
	inline void set_buttonClear_4(GameObject_t1756533147 * value)
	{
		___buttonClear_4 = value;
		Il2CppCodeGenWriteBarrier(&___buttonClear_4, value);
	}

	inline static int32_t get_offset_of_IDUserInput_5() { return static_cast<int32_t>(offsetof(MigrateGetCodePanel_t3530957368, ___IDUserInput_5)); }
	inline InputField_t1631627530 * get_IDUserInput_5() const { return ___IDUserInput_5; }
	inline InputField_t1631627530 ** get_address_of_IDUserInput_5() { return &___IDUserInput_5; }
	inline void set_IDUserInput_5(InputField_t1631627530 * value)
	{
		___IDUserInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___IDUserInput_5, value);
	}

	inline static int32_t get_offset_of_passwordInput_6() { return static_cast<int32_t>(offsetof(MigrateGetCodePanel_t3530957368, ___passwordInput_6)); }
	inline InputField_t1631627530 * get_passwordInput_6() const { return ___passwordInput_6; }
	inline InputField_t1631627530 ** get_address_of_passwordInput_6() { return &___passwordInput_6; }
	inline void set_passwordInput_6(InputField_t1631627530 * value)
	{
		___passwordInput_6 = value;
		Il2CppCodeGenWriteBarrier(&___passwordInput_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
