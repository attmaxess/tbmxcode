﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.InputField
struct InputField_t1631627530;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MigratePostCodePanel
struct  MigratePostCodePanel_t3492203860  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MigratePostCodePanel::inputArea
	GameObject_t1756533147 * ___inputArea_2;
	// UnityEngine.GameObject MigratePostCodePanel::successArea
	GameObject_t1756533147 * ___successArea_3;
	// UnityEngine.UI.InputField MigratePostCodePanel::inputFieldID
	InputField_t1631627530 * ___inputFieldID_4;
	// UnityEngine.UI.InputField MigratePostCodePanel::inputFieldPassword
	InputField_t1631627530 * ___inputFieldPassword_5;

public:
	inline static int32_t get_offset_of_inputArea_2() { return static_cast<int32_t>(offsetof(MigratePostCodePanel_t3492203860, ___inputArea_2)); }
	inline GameObject_t1756533147 * get_inputArea_2() const { return ___inputArea_2; }
	inline GameObject_t1756533147 ** get_address_of_inputArea_2() { return &___inputArea_2; }
	inline void set_inputArea_2(GameObject_t1756533147 * value)
	{
		___inputArea_2 = value;
		Il2CppCodeGenWriteBarrier(&___inputArea_2, value);
	}

	inline static int32_t get_offset_of_successArea_3() { return static_cast<int32_t>(offsetof(MigratePostCodePanel_t3492203860, ___successArea_3)); }
	inline GameObject_t1756533147 * get_successArea_3() const { return ___successArea_3; }
	inline GameObject_t1756533147 ** get_address_of_successArea_3() { return &___successArea_3; }
	inline void set_successArea_3(GameObject_t1756533147 * value)
	{
		___successArea_3 = value;
		Il2CppCodeGenWriteBarrier(&___successArea_3, value);
	}

	inline static int32_t get_offset_of_inputFieldID_4() { return static_cast<int32_t>(offsetof(MigratePostCodePanel_t3492203860, ___inputFieldID_4)); }
	inline InputField_t1631627530 * get_inputFieldID_4() const { return ___inputFieldID_4; }
	inline InputField_t1631627530 ** get_address_of_inputFieldID_4() { return &___inputFieldID_4; }
	inline void set_inputFieldID_4(InputField_t1631627530 * value)
	{
		___inputFieldID_4 = value;
		Il2CppCodeGenWriteBarrier(&___inputFieldID_4, value);
	}

	inline static int32_t get_offset_of_inputFieldPassword_5() { return static_cast<int32_t>(offsetof(MigratePostCodePanel_t3492203860, ___inputFieldPassword_5)); }
	inline InputField_t1631627530 * get_inputFieldPassword_5() const { return ___inputFieldPassword_5; }
	inline InputField_t1631627530 ** get_address_of_inputFieldPassword_5() { return &___inputFieldPassword_5; }
	inline void set_inputFieldPassword_5(InputField_t1631627530 * value)
	{
		___inputFieldPassword_5 = value;
		Il2CppCodeGenWriteBarrier(&___inputFieldPassword_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
