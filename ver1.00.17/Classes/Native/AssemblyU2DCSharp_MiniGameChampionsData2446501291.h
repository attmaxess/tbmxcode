﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGameChampionsData
struct  MiniGameChampionsData_t2446501291  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MiniGameChampionsData::rankNum
	Text_t356221433 * ___rankNum_2;
	// UnityEngine.UI.Text MiniGameChampionsData::time
	Text_t356221433 * ___time_3;
	// UnityEngine.UI.Text MiniGameChampionsData::mobilityName
	Text_t356221433 * ___mobilityName_4;
	// UnityEngine.UI.Text MiniGameChampionsData::userName
	Text_t356221433 * ___userName_5;
	// UnityEngine.UI.Image MiniGameChampionsData::prizedImg
	Image_t2042527209 * ___prizedImg_6;
	// System.Int32 MiniGameChampionsData::mobilityId
	int32_t ___mobilityId_7;
	// System.String MiniGameChampionsData::userRank
	String_t* ___userRank_8;

public:
	inline static int32_t get_offset_of_rankNum_2() { return static_cast<int32_t>(offsetof(MiniGameChampionsData_t2446501291, ___rankNum_2)); }
	inline Text_t356221433 * get_rankNum_2() const { return ___rankNum_2; }
	inline Text_t356221433 ** get_address_of_rankNum_2() { return &___rankNum_2; }
	inline void set_rankNum_2(Text_t356221433 * value)
	{
		___rankNum_2 = value;
		Il2CppCodeGenWriteBarrier(&___rankNum_2, value);
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(MiniGameChampionsData_t2446501291, ___time_3)); }
	inline Text_t356221433 * get_time_3() const { return ___time_3; }
	inline Text_t356221433 ** get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(Text_t356221433 * value)
	{
		___time_3 = value;
		Il2CppCodeGenWriteBarrier(&___time_3, value);
	}

	inline static int32_t get_offset_of_mobilityName_4() { return static_cast<int32_t>(offsetof(MiniGameChampionsData_t2446501291, ___mobilityName_4)); }
	inline Text_t356221433 * get_mobilityName_4() const { return ___mobilityName_4; }
	inline Text_t356221433 ** get_address_of_mobilityName_4() { return &___mobilityName_4; }
	inline void set_mobilityName_4(Text_t356221433 * value)
	{
		___mobilityName_4 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityName_4, value);
	}

	inline static int32_t get_offset_of_userName_5() { return static_cast<int32_t>(offsetof(MiniGameChampionsData_t2446501291, ___userName_5)); }
	inline Text_t356221433 * get_userName_5() const { return ___userName_5; }
	inline Text_t356221433 ** get_address_of_userName_5() { return &___userName_5; }
	inline void set_userName_5(Text_t356221433 * value)
	{
		___userName_5 = value;
		Il2CppCodeGenWriteBarrier(&___userName_5, value);
	}

	inline static int32_t get_offset_of_prizedImg_6() { return static_cast<int32_t>(offsetof(MiniGameChampionsData_t2446501291, ___prizedImg_6)); }
	inline Image_t2042527209 * get_prizedImg_6() const { return ___prizedImg_6; }
	inline Image_t2042527209 ** get_address_of_prizedImg_6() { return &___prizedImg_6; }
	inline void set_prizedImg_6(Image_t2042527209 * value)
	{
		___prizedImg_6 = value;
		Il2CppCodeGenWriteBarrier(&___prizedImg_6, value);
	}

	inline static int32_t get_offset_of_mobilityId_7() { return static_cast<int32_t>(offsetof(MiniGameChampionsData_t2446501291, ___mobilityId_7)); }
	inline int32_t get_mobilityId_7() const { return ___mobilityId_7; }
	inline int32_t* get_address_of_mobilityId_7() { return &___mobilityId_7; }
	inline void set_mobilityId_7(int32_t value)
	{
		___mobilityId_7 = value;
	}

	inline static int32_t get_offset_of_userRank_8() { return static_cast<int32_t>(offsetof(MiniGameChampionsData_t2446501291, ___userRank_8)); }
	inline String_t* get_userRank_8() const { return ___userRank_8; }
	inline String_t** get_address_of_userRank_8() { return &___userRank_8; }
	inline void set_userRank_8(String_t* value)
	{
		___userRank_8 = value;
		Il2CppCodeGenWriteBarrier(&___userRank_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
