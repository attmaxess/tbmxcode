﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MINIGAME3871825659.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// MiniGameChara/LocalizeString
struct LocalizeString_t2519063711;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGameChara
struct  MiniGameChara_t2588887264  : public MonoBehaviour_t1158329972
{
public:
	// MINIGAME MiniGameChara::MiniGameType
	int32_t ___MiniGameType_2;
	// UnityEngine.Vector3 MiniGameChara::GameStartPos
	Vector3_t2243707580  ___GameStartPos_3;
	// UnityEngine.Transform MiniGameChara::GameStartobj
	Transform_t3275118058 * ___GameStartobj_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> MiniGameChara::tagetPos
	List_1_t1612828712 * ___tagetPos_5;
	// MiniGameChara/LocalizeString MiniGameChara::CaptionKey
	LocalizeString_t2519063711 * ___CaptionKey_6;
	// MiniGameChara/LocalizeString MiniGameChara::LeadKey
	LocalizeString_t2519063711 * ___LeadKey_7;

public:
	inline static int32_t get_offset_of_MiniGameType_2() { return static_cast<int32_t>(offsetof(MiniGameChara_t2588887264, ___MiniGameType_2)); }
	inline int32_t get_MiniGameType_2() const { return ___MiniGameType_2; }
	inline int32_t* get_address_of_MiniGameType_2() { return &___MiniGameType_2; }
	inline void set_MiniGameType_2(int32_t value)
	{
		___MiniGameType_2 = value;
	}

	inline static int32_t get_offset_of_GameStartPos_3() { return static_cast<int32_t>(offsetof(MiniGameChara_t2588887264, ___GameStartPos_3)); }
	inline Vector3_t2243707580  get_GameStartPos_3() const { return ___GameStartPos_3; }
	inline Vector3_t2243707580 * get_address_of_GameStartPos_3() { return &___GameStartPos_3; }
	inline void set_GameStartPos_3(Vector3_t2243707580  value)
	{
		___GameStartPos_3 = value;
	}

	inline static int32_t get_offset_of_GameStartobj_4() { return static_cast<int32_t>(offsetof(MiniGameChara_t2588887264, ___GameStartobj_4)); }
	inline Transform_t3275118058 * get_GameStartobj_4() const { return ___GameStartobj_4; }
	inline Transform_t3275118058 ** get_address_of_GameStartobj_4() { return &___GameStartobj_4; }
	inline void set_GameStartobj_4(Transform_t3275118058 * value)
	{
		___GameStartobj_4 = value;
		Il2CppCodeGenWriteBarrier(&___GameStartobj_4, value);
	}

	inline static int32_t get_offset_of_tagetPos_5() { return static_cast<int32_t>(offsetof(MiniGameChara_t2588887264, ___tagetPos_5)); }
	inline List_1_t1612828712 * get_tagetPos_5() const { return ___tagetPos_5; }
	inline List_1_t1612828712 ** get_address_of_tagetPos_5() { return &___tagetPos_5; }
	inline void set_tagetPos_5(List_1_t1612828712 * value)
	{
		___tagetPos_5 = value;
		Il2CppCodeGenWriteBarrier(&___tagetPos_5, value);
	}

	inline static int32_t get_offset_of_CaptionKey_6() { return static_cast<int32_t>(offsetof(MiniGameChara_t2588887264, ___CaptionKey_6)); }
	inline LocalizeString_t2519063711 * get_CaptionKey_6() const { return ___CaptionKey_6; }
	inline LocalizeString_t2519063711 ** get_address_of_CaptionKey_6() { return &___CaptionKey_6; }
	inline void set_CaptionKey_6(LocalizeString_t2519063711 * value)
	{
		___CaptionKey_6 = value;
		Il2CppCodeGenWriteBarrier(&___CaptionKey_6, value);
	}

	inline static int32_t get_offset_of_LeadKey_7() { return static_cast<int32_t>(offsetof(MiniGameChara_t2588887264, ___LeadKey_7)); }
	inline LocalizeString_t2519063711 * get_LeadKey_7() const { return ___LeadKey_7; }
	inline LocalizeString_t2519063711 ** get_address_of_LeadKey_7() { return &___LeadKey_7; }
	inline void set_LeadKey_7(LocalizeString_t2519063711 * value)
	{
		___LeadKey_7 = value;
		Il2CppCodeGenWriteBarrier(&___LeadKey_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
