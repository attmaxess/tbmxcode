﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3841508583.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// MiniGamePlayContents
struct MiniGamePlayContents_t3012008261;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Single[]
struct SingleU5BU5D_t577127397;
// MiniGameChara
struct MiniGameChara_t2588887264;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGameManager
struct  MiniGameManager_t849864952  : public SingletonMonoBehaviour_1_t3841508583
{
public:
	// UnityEngine.UI.Text MiniGameManager::Text_GameTitle
	Text_t356221433 * ___Text_GameTitle_3;
	// UnityEngine.UI.Text MiniGameManager::Text_GameDescription
	Text_t356221433 * ___Text_GameDescription_4;
	// MiniGamePlayContents MiniGameManager::playContents
	MiniGamePlayContents_t3012008261 * ___playContents_5;
	// UnityEngine.GameObject MiniGameManager::ClearPannel
	GameObject_t1756533147 * ___ClearPannel_6;
	// UnityEngine.GameObject MiniGameManager::GameOverPannel
	GameObject_t1756533147 * ___GameOverPannel_7;
	// UnityEngine.UI.Text MiniGameManager::ResultChallengerValue
	Text_t356221433 * ___ResultChallengerValue_8;
	// UnityEngine.UI.Text MiniGameManager::ResultChampionValue
	Text_t356221433 * ___ResultChampionValue_9;
	// UnityEngine.UI.Text MiniGameManager::GameOverText
	Text_t356221433 * ___GameOverText_10;
	// System.Boolean MiniGameManager::m_bIsGamePlay
	bool ___m_bIsGamePlay_11;
	// System.Boolean MiniGameManager::IsGameFinish
	bool ___IsGameFinish_12;
	// UnityEngine.GameObject[] MiniGameManager::RankingObj
	GameObjectU5BU5D_t3057952154* ___RankingObj_13;
	// System.String[] MiniGameManager::MobilityName
	StringU5BU5D_t1642385972* ___MobilityName_14;
	// System.String[] MiniGameManager::UserName
	StringU5BU5D_t1642385972* ___UserName_15;
	// System.Single[] MiniGameManager::RankerTime
	SingleU5BU5D_t577127397* ___RankerTime_16;
	// System.String[] MiniGameManager::PrizedType
	StringU5BU5D_t1642385972* ___PrizedType_17;
	// MiniGameChara MiniGameManager::gameChara
	MiniGameChara_t2588887264 * ___gameChara_18;
	// System.Boolean MiniGameManager::isCanTap
	bool ___isCanTap_19;

public:
	inline static int32_t get_offset_of_Text_GameTitle_3() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___Text_GameTitle_3)); }
	inline Text_t356221433 * get_Text_GameTitle_3() const { return ___Text_GameTitle_3; }
	inline Text_t356221433 ** get_address_of_Text_GameTitle_3() { return &___Text_GameTitle_3; }
	inline void set_Text_GameTitle_3(Text_t356221433 * value)
	{
		___Text_GameTitle_3 = value;
		Il2CppCodeGenWriteBarrier(&___Text_GameTitle_3, value);
	}

	inline static int32_t get_offset_of_Text_GameDescription_4() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___Text_GameDescription_4)); }
	inline Text_t356221433 * get_Text_GameDescription_4() const { return ___Text_GameDescription_4; }
	inline Text_t356221433 ** get_address_of_Text_GameDescription_4() { return &___Text_GameDescription_4; }
	inline void set_Text_GameDescription_4(Text_t356221433 * value)
	{
		___Text_GameDescription_4 = value;
		Il2CppCodeGenWriteBarrier(&___Text_GameDescription_4, value);
	}

	inline static int32_t get_offset_of_playContents_5() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___playContents_5)); }
	inline MiniGamePlayContents_t3012008261 * get_playContents_5() const { return ___playContents_5; }
	inline MiniGamePlayContents_t3012008261 ** get_address_of_playContents_5() { return &___playContents_5; }
	inline void set_playContents_5(MiniGamePlayContents_t3012008261 * value)
	{
		___playContents_5 = value;
		Il2CppCodeGenWriteBarrier(&___playContents_5, value);
	}

	inline static int32_t get_offset_of_ClearPannel_6() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___ClearPannel_6)); }
	inline GameObject_t1756533147 * get_ClearPannel_6() const { return ___ClearPannel_6; }
	inline GameObject_t1756533147 ** get_address_of_ClearPannel_6() { return &___ClearPannel_6; }
	inline void set_ClearPannel_6(GameObject_t1756533147 * value)
	{
		___ClearPannel_6 = value;
		Il2CppCodeGenWriteBarrier(&___ClearPannel_6, value);
	}

	inline static int32_t get_offset_of_GameOverPannel_7() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___GameOverPannel_7)); }
	inline GameObject_t1756533147 * get_GameOverPannel_7() const { return ___GameOverPannel_7; }
	inline GameObject_t1756533147 ** get_address_of_GameOverPannel_7() { return &___GameOverPannel_7; }
	inline void set_GameOverPannel_7(GameObject_t1756533147 * value)
	{
		___GameOverPannel_7 = value;
		Il2CppCodeGenWriteBarrier(&___GameOverPannel_7, value);
	}

	inline static int32_t get_offset_of_ResultChallengerValue_8() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___ResultChallengerValue_8)); }
	inline Text_t356221433 * get_ResultChallengerValue_8() const { return ___ResultChallengerValue_8; }
	inline Text_t356221433 ** get_address_of_ResultChallengerValue_8() { return &___ResultChallengerValue_8; }
	inline void set_ResultChallengerValue_8(Text_t356221433 * value)
	{
		___ResultChallengerValue_8 = value;
		Il2CppCodeGenWriteBarrier(&___ResultChallengerValue_8, value);
	}

	inline static int32_t get_offset_of_ResultChampionValue_9() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___ResultChampionValue_9)); }
	inline Text_t356221433 * get_ResultChampionValue_9() const { return ___ResultChampionValue_9; }
	inline Text_t356221433 ** get_address_of_ResultChampionValue_9() { return &___ResultChampionValue_9; }
	inline void set_ResultChampionValue_9(Text_t356221433 * value)
	{
		___ResultChampionValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___ResultChampionValue_9, value);
	}

	inline static int32_t get_offset_of_GameOverText_10() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___GameOverText_10)); }
	inline Text_t356221433 * get_GameOverText_10() const { return ___GameOverText_10; }
	inline Text_t356221433 ** get_address_of_GameOverText_10() { return &___GameOverText_10; }
	inline void set_GameOverText_10(Text_t356221433 * value)
	{
		___GameOverText_10 = value;
		Il2CppCodeGenWriteBarrier(&___GameOverText_10, value);
	}

	inline static int32_t get_offset_of_m_bIsGamePlay_11() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___m_bIsGamePlay_11)); }
	inline bool get_m_bIsGamePlay_11() const { return ___m_bIsGamePlay_11; }
	inline bool* get_address_of_m_bIsGamePlay_11() { return &___m_bIsGamePlay_11; }
	inline void set_m_bIsGamePlay_11(bool value)
	{
		___m_bIsGamePlay_11 = value;
	}

	inline static int32_t get_offset_of_IsGameFinish_12() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___IsGameFinish_12)); }
	inline bool get_IsGameFinish_12() const { return ___IsGameFinish_12; }
	inline bool* get_address_of_IsGameFinish_12() { return &___IsGameFinish_12; }
	inline void set_IsGameFinish_12(bool value)
	{
		___IsGameFinish_12 = value;
	}

	inline static int32_t get_offset_of_RankingObj_13() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___RankingObj_13)); }
	inline GameObjectU5BU5D_t3057952154* get_RankingObj_13() const { return ___RankingObj_13; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_RankingObj_13() { return &___RankingObj_13; }
	inline void set_RankingObj_13(GameObjectU5BU5D_t3057952154* value)
	{
		___RankingObj_13 = value;
		Il2CppCodeGenWriteBarrier(&___RankingObj_13, value);
	}

	inline static int32_t get_offset_of_MobilityName_14() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___MobilityName_14)); }
	inline StringU5BU5D_t1642385972* get_MobilityName_14() const { return ___MobilityName_14; }
	inline StringU5BU5D_t1642385972** get_address_of_MobilityName_14() { return &___MobilityName_14; }
	inline void set_MobilityName_14(StringU5BU5D_t1642385972* value)
	{
		___MobilityName_14 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityName_14, value);
	}

	inline static int32_t get_offset_of_UserName_15() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___UserName_15)); }
	inline StringU5BU5D_t1642385972* get_UserName_15() const { return ___UserName_15; }
	inline StringU5BU5D_t1642385972** get_address_of_UserName_15() { return &___UserName_15; }
	inline void set_UserName_15(StringU5BU5D_t1642385972* value)
	{
		___UserName_15 = value;
		Il2CppCodeGenWriteBarrier(&___UserName_15, value);
	}

	inline static int32_t get_offset_of_RankerTime_16() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___RankerTime_16)); }
	inline SingleU5BU5D_t577127397* get_RankerTime_16() const { return ___RankerTime_16; }
	inline SingleU5BU5D_t577127397** get_address_of_RankerTime_16() { return &___RankerTime_16; }
	inline void set_RankerTime_16(SingleU5BU5D_t577127397* value)
	{
		___RankerTime_16 = value;
		Il2CppCodeGenWriteBarrier(&___RankerTime_16, value);
	}

	inline static int32_t get_offset_of_PrizedType_17() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___PrizedType_17)); }
	inline StringU5BU5D_t1642385972* get_PrizedType_17() const { return ___PrizedType_17; }
	inline StringU5BU5D_t1642385972** get_address_of_PrizedType_17() { return &___PrizedType_17; }
	inline void set_PrizedType_17(StringU5BU5D_t1642385972* value)
	{
		___PrizedType_17 = value;
		Il2CppCodeGenWriteBarrier(&___PrizedType_17, value);
	}

	inline static int32_t get_offset_of_gameChara_18() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___gameChara_18)); }
	inline MiniGameChara_t2588887264 * get_gameChara_18() const { return ___gameChara_18; }
	inline MiniGameChara_t2588887264 ** get_address_of_gameChara_18() { return &___gameChara_18; }
	inline void set_gameChara_18(MiniGameChara_t2588887264 * value)
	{
		___gameChara_18 = value;
		Il2CppCodeGenWriteBarrier(&___gameChara_18, value);
	}

	inline static int32_t get_offset_of_isCanTap_19() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952, ___isCanTap_19)); }
	inline bool get_isCanTap_19() const { return ___isCanTap_19; }
	inline bool* get_address_of_isCanTap_19() { return &___isCanTap_19; }
	inline void set_isCanTap_19(bool value)
	{
		___isCanTap_19 = value;
	}
};

struct MiniGameManager_t849864952_StaticFields
{
public:
	// System.Action MiniGameManager::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_20() { return static_cast<int32_t>(offsetof(MiniGameManager_t849864952_StaticFields, ___U3CU3Ef__amU24cache0_20)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_20() const { return ___U3CU3Ef__amU24cache0_20; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_20() { return &___U3CU3Ef__amU24cache0_20; }
	inline void set_U3CU3Ef__amU24cache0_20(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
