﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// MiniGameManager
struct MiniGameManager_t849864952;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGameManager/<MiniGameOverDialog>c__AnonStorey0
struct  U3CMiniGameOverDialogU3Ec__AnonStorey0_t214630277  : public Il2CppObject
{
public:
	// System.String MiniGameManager/<MiniGameOverDialog>c__AnonStorey0::ReasonText
	String_t* ___ReasonText_0;
	// MiniGameManager MiniGameManager/<MiniGameOverDialog>c__AnonStorey0::$this
	MiniGameManager_t849864952 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ReasonText_0() { return static_cast<int32_t>(offsetof(U3CMiniGameOverDialogU3Ec__AnonStorey0_t214630277, ___ReasonText_0)); }
	inline String_t* get_ReasonText_0() const { return ___ReasonText_0; }
	inline String_t** get_address_of_ReasonText_0() { return &___ReasonText_0; }
	inline void set_ReasonText_0(String_t* value)
	{
		___ReasonText_0 = value;
		Il2CppCodeGenWriteBarrier(&___ReasonText_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CMiniGameOverDialogU3Ec__AnonStorey0_t214630277, ___U24this_1)); }
	inline MiniGameManager_t849864952 * get_U24this_1() const { return ___U24this_1; }
	inline MiniGameManager_t849864952 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MiniGameManager_t849864952 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
