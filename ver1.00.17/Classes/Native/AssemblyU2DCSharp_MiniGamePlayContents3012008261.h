﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGamePlayContents
struct  MiniGamePlayContents_t3012008261  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MiniGamePlayContents::Time
	GameObject_t1756533147 * ___Time_2;
	// UnityEngine.UI.Text MiniGamePlayContents::TimeSecondsText
	Text_t356221433 * ___TimeSecondsText_3;
	// UnityEngine.UI.Text MiniGamePlayContents::TimeMilliSecondsText
	Text_t356221433 * ___TimeMilliSecondsText_4;
	// UnityEngine.GameObject MiniGamePlayContents::Score
	GameObject_t1756533147 * ___Score_5;
	// UnityEngine.UI.Text MiniGamePlayContents::ScoreText
	Text_t356221433 * ___ScoreText_6;

public:
	inline static int32_t get_offset_of_Time_2() { return static_cast<int32_t>(offsetof(MiniGamePlayContents_t3012008261, ___Time_2)); }
	inline GameObject_t1756533147 * get_Time_2() const { return ___Time_2; }
	inline GameObject_t1756533147 ** get_address_of_Time_2() { return &___Time_2; }
	inline void set_Time_2(GameObject_t1756533147 * value)
	{
		___Time_2 = value;
		Il2CppCodeGenWriteBarrier(&___Time_2, value);
	}

	inline static int32_t get_offset_of_TimeSecondsText_3() { return static_cast<int32_t>(offsetof(MiniGamePlayContents_t3012008261, ___TimeSecondsText_3)); }
	inline Text_t356221433 * get_TimeSecondsText_3() const { return ___TimeSecondsText_3; }
	inline Text_t356221433 ** get_address_of_TimeSecondsText_3() { return &___TimeSecondsText_3; }
	inline void set_TimeSecondsText_3(Text_t356221433 * value)
	{
		___TimeSecondsText_3 = value;
		Il2CppCodeGenWriteBarrier(&___TimeSecondsText_3, value);
	}

	inline static int32_t get_offset_of_TimeMilliSecondsText_4() { return static_cast<int32_t>(offsetof(MiniGamePlayContents_t3012008261, ___TimeMilliSecondsText_4)); }
	inline Text_t356221433 * get_TimeMilliSecondsText_4() const { return ___TimeMilliSecondsText_4; }
	inline Text_t356221433 ** get_address_of_TimeMilliSecondsText_4() { return &___TimeMilliSecondsText_4; }
	inline void set_TimeMilliSecondsText_4(Text_t356221433 * value)
	{
		___TimeMilliSecondsText_4 = value;
		Il2CppCodeGenWriteBarrier(&___TimeMilliSecondsText_4, value);
	}

	inline static int32_t get_offset_of_Score_5() { return static_cast<int32_t>(offsetof(MiniGamePlayContents_t3012008261, ___Score_5)); }
	inline GameObject_t1756533147 * get_Score_5() const { return ___Score_5; }
	inline GameObject_t1756533147 ** get_address_of_Score_5() { return &___Score_5; }
	inline void set_Score_5(GameObject_t1756533147 * value)
	{
		___Score_5 = value;
		Il2CppCodeGenWriteBarrier(&___Score_5, value);
	}

	inline static int32_t get_offset_of_ScoreText_6() { return static_cast<int32_t>(offsetof(MiniGamePlayContents_t3012008261, ___ScoreText_6)); }
	inline Text_t356221433 * get_ScoreText_6() const { return ___ScoreText_6; }
	inline Text_t356221433 ** get_address_of_ScoreText_6() { return &___ScoreText_6; }
	inline void set_ScoreText_6(Text_t356221433 * value)
	{
		___ScoreText_6 = value;
		Il2CppCodeGenWriteBarrier(&___ScoreText_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
