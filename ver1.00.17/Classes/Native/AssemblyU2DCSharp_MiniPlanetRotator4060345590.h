﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniPlanetRotator
struct  MiniPlanetRotator_t4060345590  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MiniPlanetRotator::MiniPlanet
	GameObject_t1756533147 * ___MiniPlanet_2;
	// System.Boolean MiniPlanetRotator::Active
	bool ___Active_3;
	// UnityEngine.Vector3 MiniPlanetRotator::RotateSpeed
	Vector3_t2243707580  ___RotateSpeed_4;

public:
	inline static int32_t get_offset_of_MiniPlanet_2() { return static_cast<int32_t>(offsetof(MiniPlanetRotator_t4060345590, ___MiniPlanet_2)); }
	inline GameObject_t1756533147 * get_MiniPlanet_2() const { return ___MiniPlanet_2; }
	inline GameObject_t1756533147 ** get_address_of_MiniPlanet_2() { return &___MiniPlanet_2; }
	inline void set_MiniPlanet_2(GameObject_t1756533147 * value)
	{
		___MiniPlanet_2 = value;
		Il2CppCodeGenWriteBarrier(&___MiniPlanet_2, value);
	}

	inline static int32_t get_offset_of_Active_3() { return static_cast<int32_t>(offsetof(MiniPlanetRotator_t4060345590, ___Active_3)); }
	inline bool get_Active_3() const { return ___Active_3; }
	inline bool* get_address_of_Active_3() { return &___Active_3; }
	inline void set_Active_3(bool value)
	{
		___Active_3 = value;
	}

	inline static int32_t get_offset_of_RotateSpeed_4() { return static_cast<int32_t>(offsetof(MiniPlanetRotator_t4060345590, ___RotateSpeed_4)); }
	inline Vector3_t2243707580  get_RotateSpeed_4() const { return ___RotateSpeed_4; }
	inline Vector3_t2243707580 * get_address_of_RotateSpeed_4() { return &___RotateSpeed_4; }
	inline void set_RotateSpeed_4(Vector3_t2243707580  value)
	{
		___RotateSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
