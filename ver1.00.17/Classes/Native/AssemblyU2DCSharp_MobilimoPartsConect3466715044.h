﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// MobilmoPartsConectContents
struct MobilmoPartsConectContents_t1795215227;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// PartsViewPanel
struct PartsViewPanel_t2613620109;
// PartsScroll
struct PartsScroll_t2740354319;
// PartsPanelSliders
struct PartsPanelSliders_t4209932670;
// ModelingConfigPanel
struct ModelingConfigPanel_t2566912473;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilimoPartsConect
struct  MobilimoPartsConect_t3466715044  : public MonoBehaviour_t1158329972
{
public:
	// MobilmoPartsConectContents MobilimoPartsConect::Contents
	MobilmoPartsConectContents_t1795215227 * ___Contents_2;
	// UnityEngine.GameObject[] MobilimoPartsConect::Sequences
	GameObjectU5BU5D_t3057952154* ___Sequences_3;
	// PartsViewPanel MobilimoPartsConect::PartsViewPanel
	PartsViewPanel_t2613620109 * ___PartsViewPanel_4;
	// PartsScroll MobilimoPartsConect::PartsBar
	PartsScroll_t2740354319 * ___PartsBar_5;
	// PartsPanelSliders MobilimoPartsConect::Sliders
	PartsPanelSliders_t4209932670 * ___Sliders_6;
	// ModelingConfigPanel MobilimoPartsConect::ConfigPanel
	ModelingConfigPanel_t2566912473 * ___ConfigPanel_7;
	// System.Int32 MobilimoPartsConect::TutorialStep
	int32_t ___TutorialStep_8;
	// System.Int32 MobilimoPartsConect::CreateTutorialStep
	int32_t ___CreateTutorialStep_9;

public:
	inline static int32_t get_offset_of_Contents_2() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___Contents_2)); }
	inline MobilmoPartsConectContents_t1795215227 * get_Contents_2() const { return ___Contents_2; }
	inline MobilmoPartsConectContents_t1795215227 ** get_address_of_Contents_2() { return &___Contents_2; }
	inline void set_Contents_2(MobilmoPartsConectContents_t1795215227 * value)
	{
		___Contents_2 = value;
		Il2CppCodeGenWriteBarrier(&___Contents_2, value);
	}

	inline static int32_t get_offset_of_Sequences_3() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___Sequences_3)); }
	inline GameObjectU5BU5D_t3057952154* get_Sequences_3() const { return ___Sequences_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Sequences_3() { return &___Sequences_3; }
	inline void set_Sequences_3(GameObjectU5BU5D_t3057952154* value)
	{
		___Sequences_3 = value;
		Il2CppCodeGenWriteBarrier(&___Sequences_3, value);
	}

	inline static int32_t get_offset_of_PartsViewPanel_4() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___PartsViewPanel_4)); }
	inline PartsViewPanel_t2613620109 * get_PartsViewPanel_4() const { return ___PartsViewPanel_4; }
	inline PartsViewPanel_t2613620109 ** get_address_of_PartsViewPanel_4() { return &___PartsViewPanel_4; }
	inline void set_PartsViewPanel_4(PartsViewPanel_t2613620109 * value)
	{
		___PartsViewPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___PartsViewPanel_4, value);
	}

	inline static int32_t get_offset_of_PartsBar_5() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___PartsBar_5)); }
	inline PartsScroll_t2740354319 * get_PartsBar_5() const { return ___PartsBar_5; }
	inline PartsScroll_t2740354319 ** get_address_of_PartsBar_5() { return &___PartsBar_5; }
	inline void set_PartsBar_5(PartsScroll_t2740354319 * value)
	{
		___PartsBar_5 = value;
		Il2CppCodeGenWriteBarrier(&___PartsBar_5, value);
	}

	inline static int32_t get_offset_of_Sliders_6() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___Sliders_6)); }
	inline PartsPanelSliders_t4209932670 * get_Sliders_6() const { return ___Sliders_6; }
	inline PartsPanelSliders_t4209932670 ** get_address_of_Sliders_6() { return &___Sliders_6; }
	inline void set_Sliders_6(PartsPanelSliders_t4209932670 * value)
	{
		___Sliders_6 = value;
		Il2CppCodeGenWriteBarrier(&___Sliders_6, value);
	}

	inline static int32_t get_offset_of_ConfigPanel_7() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___ConfigPanel_7)); }
	inline ModelingConfigPanel_t2566912473 * get_ConfigPanel_7() const { return ___ConfigPanel_7; }
	inline ModelingConfigPanel_t2566912473 ** get_address_of_ConfigPanel_7() { return &___ConfigPanel_7; }
	inline void set_ConfigPanel_7(ModelingConfigPanel_t2566912473 * value)
	{
		___ConfigPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigPanel_7, value);
	}

	inline static int32_t get_offset_of_TutorialStep_8() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___TutorialStep_8)); }
	inline int32_t get_TutorialStep_8() const { return ___TutorialStep_8; }
	inline int32_t* get_address_of_TutorialStep_8() { return &___TutorialStep_8; }
	inline void set_TutorialStep_8(int32_t value)
	{
		___TutorialStep_8 = value;
	}

	inline static int32_t get_offset_of_CreateTutorialStep_9() { return static_cast<int32_t>(offsetof(MobilimoPartsConect_t3466715044, ___CreateTutorialStep_9)); }
	inline int32_t get_CreateTutorialStep_9() const { return ___CreateTutorialStep_9; }
	inline int32_t* get_address_of_CreateTutorialStep_9() { return &___CreateTutorialStep_9; }
	inline void set_CreateTutorialStep_9(int32_t value)
	{
		___CreateTutorialStep_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
