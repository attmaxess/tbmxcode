﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilityDeletePanel
struct  MobilityDeletePanel_t3457116160  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MobilityDeletePanel::_mobilityObject
	GameObject_t1756533147 * ____mobilityObject_2;
	// UnityEngine.UI.Text MobilityDeletePanel::deleteMobilityName
	Text_t356221433 * ___deleteMobilityName_3;
	// UnityEngine.UI.Button MobilityDeletePanel::DelOKButton
	Button_t2872111280 * ___DelOKButton_4;

public:
	inline static int32_t get_offset_of__mobilityObject_2() { return static_cast<int32_t>(offsetof(MobilityDeletePanel_t3457116160, ____mobilityObject_2)); }
	inline GameObject_t1756533147 * get__mobilityObject_2() const { return ____mobilityObject_2; }
	inline GameObject_t1756533147 ** get_address_of__mobilityObject_2() { return &____mobilityObject_2; }
	inline void set__mobilityObject_2(GameObject_t1756533147 * value)
	{
		____mobilityObject_2 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityObject_2, value);
	}

	inline static int32_t get_offset_of_deleteMobilityName_3() { return static_cast<int32_t>(offsetof(MobilityDeletePanel_t3457116160, ___deleteMobilityName_3)); }
	inline Text_t356221433 * get_deleteMobilityName_3() const { return ___deleteMobilityName_3; }
	inline Text_t356221433 ** get_address_of_deleteMobilityName_3() { return &___deleteMobilityName_3; }
	inline void set_deleteMobilityName_3(Text_t356221433 * value)
	{
		___deleteMobilityName_3 = value;
		Il2CppCodeGenWriteBarrier(&___deleteMobilityName_3, value);
	}

	inline static int32_t get_offset_of_DelOKButton_4() { return static_cast<int32_t>(offsetof(MobilityDeletePanel_t3457116160, ___DelOKButton_4)); }
	inline Button_t2872111280 * get_DelOKButton_4() const { return ___DelOKButton_4; }
	inline Button_t2872111280 ** get_address_of_DelOKButton_4() { return &___DelOKButton_4; }
	inline void set_DelOKButton_4(Button_t2872111280 * value)
	{
		___DelOKButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___DelOKButton_4, value);
	}
};

struct MobilityDeletePanel_t3457116160_StaticFields
{
public:
	// System.Action MobilityDeletePanel::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(MobilityDeletePanel_t3457116160_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
