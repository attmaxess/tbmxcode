﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilityItemGo
struct  MobilityItemGo_t3232108924  : public MonoBehaviour_t1158329972
{
public:
	// System.String MobilityItemGo::test_Mobility_Json
	String_t* ___test_Mobility_Json_2;
	// System.String[] MobilityItemGo::childPartsType
	StringU5BU5D_t1642385972* ___childPartsType_3;
	// System.String[] MobilityItemGo::corePartsType
	StringU5BU5D_t1642385972* ___corePartsType_4;
	// UnityEngine.UI.Text MobilityItemGo::mobilityName
	Text_t356221433 * ___mobilityName_5;
	// System.Single MobilityItemGo::childDrillerPositionX
	float ___childDrillerPositionX_6;
	// System.Single MobilityItemGo::childDrillerPositionY
	float ___childDrillerPositionY_7;
	// System.Single MobilityItemGo::childDrillerPositionZ
	float ___childDrillerPositionZ_8;
	// System.Single MobilityItemGo::childCenterRotationX
	float ___childCenterRotationX_9;
	// System.Single MobilityItemGo::childCenterRotationY
	float ___childCenterRotationY_10;
	// System.Single MobilityItemGo::childCenterRotationZ
	float ___childCenterRotationZ_11;
	// System.Single MobilityItemGo::childPartsPositionX
	float ___childPartsPositionX_12;
	// System.Single MobilityItemGo::childPartsPositionY
	float ___childPartsPositionY_13;
	// System.Single MobilityItemGo::childPartsPositionZ
	float ___childPartsPositionZ_14;
	// System.Single MobilityItemGo::childPartsRotationX
	float ___childPartsRotationX_15;
	// System.Single MobilityItemGo::childPartsRotationY
	float ___childPartsRotationY_16;
	// System.Single MobilityItemGo::childPartsRotationZ
	float ___childPartsRotationZ_17;
	// System.Single MobilityItemGo::childPartsScaleX
	float ___childPartsScaleX_18;
	// System.Single MobilityItemGo::childPartsScaleY
	float ___childPartsScaleY_19;
	// System.Single MobilityItemGo::childPartsScaleZ
	float ___childPartsScaleZ_20;

public:
	inline static int32_t get_offset_of_test_Mobility_Json_2() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___test_Mobility_Json_2)); }
	inline String_t* get_test_Mobility_Json_2() const { return ___test_Mobility_Json_2; }
	inline String_t** get_address_of_test_Mobility_Json_2() { return &___test_Mobility_Json_2; }
	inline void set_test_Mobility_Json_2(String_t* value)
	{
		___test_Mobility_Json_2 = value;
		Il2CppCodeGenWriteBarrier(&___test_Mobility_Json_2, value);
	}

	inline static int32_t get_offset_of_childPartsType_3() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsType_3)); }
	inline StringU5BU5D_t1642385972* get_childPartsType_3() const { return ___childPartsType_3; }
	inline StringU5BU5D_t1642385972** get_address_of_childPartsType_3() { return &___childPartsType_3; }
	inline void set_childPartsType_3(StringU5BU5D_t1642385972* value)
	{
		___childPartsType_3 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsType_3, value);
	}

	inline static int32_t get_offset_of_corePartsType_4() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___corePartsType_4)); }
	inline StringU5BU5D_t1642385972* get_corePartsType_4() const { return ___corePartsType_4; }
	inline StringU5BU5D_t1642385972** get_address_of_corePartsType_4() { return &___corePartsType_4; }
	inline void set_corePartsType_4(StringU5BU5D_t1642385972* value)
	{
		___corePartsType_4 = value;
		Il2CppCodeGenWriteBarrier(&___corePartsType_4, value);
	}

	inline static int32_t get_offset_of_mobilityName_5() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___mobilityName_5)); }
	inline Text_t356221433 * get_mobilityName_5() const { return ___mobilityName_5; }
	inline Text_t356221433 ** get_address_of_mobilityName_5() { return &___mobilityName_5; }
	inline void set_mobilityName_5(Text_t356221433 * value)
	{
		___mobilityName_5 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityName_5, value);
	}

	inline static int32_t get_offset_of_childDrillerPositionX_6() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childDrillerPositionX_6)); }
	inline float get_childDrillerPositionX_6() const { return ___childDrillerPositionX_6; }
	inline float* get_address_of_childDrillerPositionX_6() { return &___childDrillerPositionX_6; }
	inline void set_childDrillerPositionX_6(float value)
	{
		___childDrillerPositionX_6 = value;
	}

	inline static int32_t get_offset_of_childDrillerPositionY_7() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childDrillerPositionY_7)); }
	inline float get_childDrillerPositionY_7() const { return ___childDrillerPositionY_7; }
	inline float* get_address_of_childDrillerPositionY_7() { return &___childDrillerPositionY_7; }
	inline void set_childDrillerPositionY_7(float value)
	{
		___childDrillerPositionY_7 = value;
	}

	inline static int32_t get_offset_of_childDrillerPositionZ_8() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childDrillerPositionZ_8)); }
	inline float get_childDrillerPositionZ_8() const { return ___childDrillerPositionZ_8; }
	inline float* get_address_of_childDrillerPositionZ_8() { return &___childDrillerPositionZ_8; }
	inline void set_childDrillerPositionZ_8(float value)
	{
		___childDrillerPositionZ_8 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationX_9() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childCenterRotationX_9)); }
	inline float get_childCenterRotationX_9() const { return ___childCenterRotationX_9; }
	inline float* get_address_of_childCenterRotationX_9() { return &___childCenterRotationX_9; }
	inline void set_childCenterRotationX_9(float value)
	{
		___childCenterRotationX_9 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationY_10() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childCenterRotationY_10)); }
	inline float get_childCenterRotationY_10() const { return ___childCenterRotationY_10; }
	inline float* get_address_of_childCenterRotationY_10() { return &___childCenterRotationY_10; }
	inline void set_childCenterRotationY_10(float value)
	{
		___childCenterRotationY_10 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationZ_11() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childCenterRotationZ_11)); }
	inline float get_childCenterRotationZ_11() const { return ___childCenterRotationZ_11; }
	inline float* get_address_of_childCenterRotationZ_11() { return &___childCenterRotationZ_11; }
	inline void set_childCenterRotationZ_11(float value)
	{
		___childCenterRotationZ_11 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionX_12() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsPositionX_12)); }
	inline float get_childPartsPositionX_12() const { return ___childPartsPositionX_12; }
	inline float* get_address_of_childPartsPositionX_12() { return &___childPartsPositionX_12; }
	inline void set_childPartsPositionX_12(float value)
	{
		___childPartsPositionX_12 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionY_13() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsPositionY_13)); }
	inline float get_childPartsPositionY_13() const { return ___childPartsPositionY_13; }
	inline float* get_address_of_childPartsPositionY_13() { return &___childPartsPositionY_13; }
	inline void set_childPartsPositionY_13(float value)
	{
		___childPartsPositionY_13 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionZ_14() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsPositionZ_14)); }
	inline float get_childPartsPositionZ_14() const { return ___childPartsPositionZ_14; }
	inline float* get_address_of_childPartsPositionZ_14() { return &___childPartsPositionZ_14; }
	inline void set_childPartsPositionZ_14(float value)
	{
		___childPartsPositionZ_14 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationX_15() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsRotationX_15)); }
	inline float get_childPartsRotationX_15() const { return ___childPartsRotationX_15; }
	inline float* get_address_of_childPartsRotationX_15() { return &___childPartsRotationX_15; }
	inline void set_childPartsRotationX_15(float value)
	{
		___childPartsRotationX_15 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationY_16() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsRotationY_16)); }
	inline float get_childPartsRotationY_16() const { return ___childPartsRotationY_16; }
	inline float* get_address_of_childPartsRotationY_16() { return &___childPartsRotationY_16; }
	inline void set_childPartsRotationY_16(float value)
	{
		___childPartsRotationY_16 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationZ_17() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsRotationZ_17)); }
	inline float get_childPartsRotationZ_17() const { return ___childPartsRotationZ_17; }
	inline float* get_address_of_childPartsRotationZ_17() { return &___childPartsRotationZ_17; }
	inline void set_childPartsRotationZ_17(float value)
	{
		___childPartsRotationZ_17 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleX_18() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsScaleX_18)); }
	inline float get_childPartsScaleX_18() const { return ___childPartsScaleX_18; }
	inline float* get_address_of_childPartsScaleX_18() { return &___childPartsScaleX_18; }
	inline void set_childPartsScaleX_18(float value)
	{
		___childPartsScaleX_18 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleY_19() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsScaleY_19)); }
	inline float get_childPartsScaleY_19() const { return ___childPartsScaleY_19; }
	inline float* get_address_of_childPartsScaleY_19() { return &___childPartsScaleY_19; }
	inline void set_childPartsScaleY_19(float value)
	{
		___childPartsScaleY_19 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleZ_20() { return static_cast<int32_t>(offsetof(MobilityItemGo_t3232108924, ___childPartsScaleZ_20)); }
	inline float get_childPartsScaleZ_20() const { return ___childPartsScaleZ_20; }
	inline float* get_address_of_childPartsScaleZ_20() { return &___childPartsScaleZ_20; }
	inline void set_childPartsScaleZ_20(float value)
	{
		___childPartsScaleZ_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
