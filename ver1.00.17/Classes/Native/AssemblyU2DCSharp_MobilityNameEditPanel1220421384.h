﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilityNameEditPanel
struct  MobilityNameEditPanel_t1220421384  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField MobilityNameEditPanel::nameInputField
	InputField_t1631627530 * ___nameInputField_2;
	// UnityEngine.UI.Button MobilityNameEditPanel::CreateButton
	Button_t2872111280 * ___CreateButton_3;
	// System.Int32 MobilityNameEditPanel::cnt
	int32_t ___cnt_4;

public:
	inline static int32_t get_offset_of_nameInputField_2() { return static_cast<int32_t>(offsetof(MobilityNameEditPanel_t1220421384, ___nameInputField_2)); }
	inline InputField_t1631627530 * get_nameInputField_2() const { return ___nameInputField_2; }
	inline InputField_t1631627530 ** get_address_of_nameInputField_2() { return &___nameInputField_2; }
	inline void set_nameInputField_2(InputField_t1631627530 * value)
	{
		___nameInputField_2 = value;
		Il2CppCodeGenWriteBarrier(&___nameInputField_2, value);
	}

	inline static int32_t get_offset_of_CreateButton_3() { return static_cast<int32_t>(offsetof(MobilityNameEditPanel_t1220421384, ___CreateButton_3)); }
	inline Button_t2872111280 * get_CreateButton_3() const { return ___CreateButton_3; }
	inline Button_t2872111280 ** get_address_of_CreateButton_3() { return &___CreateButton_3; }
	inline void set_CreateButton_3(Button_t2872111280 * value)
	{
		___CreateButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___CreateButton_3, value);
	}

	inline static int32_t get_offset_of_cnt_4() { return static_cast<int32_t>(offsetof(MobilityNameEditPanel_t1220421384, ___cnt_4)); }
	inline int32_t get_cnt_4() const { return ___cnt_4; }
	inline int32_t* get_address_of_cnt_4() { return &___cnt_4; }
	inline void set_cnt_4(int32_t value)
	{
		___cnt_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
