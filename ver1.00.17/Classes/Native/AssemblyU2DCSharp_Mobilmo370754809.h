﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.Dictionary`2<System.Int32,Module>
struct Dictionary_2_t2148260463;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<RecordingManager/recordingMotionData>
struct List_1_t1073334888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mobilmo
struct  Mobilmo_t370754809  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 Mobilmo::startPosition
	Vector3_t2243707580  ___startPosition_2;
	// UnityEngine.Vector3 Mobilmo::startRotation
	Vector3_t2243707580  ___startRotation_3;
	// System.Int32 Mobilmo::mobilmoId
	int32_t ___mobilmoId_4;
	// System.String Mobilmo::mobilityName
	String_t* ___mobilityName_5;
	// System.Int32 Mobilmo::m_nearMobUserId
	int32_t ___m_nearMobUserId_6;
	// System.Int32 Mobilmo::prizedIdListCnt
	int32_t ___prizedIdListCnt_7;
	// System.Int32 Mobilmo::createdUserId
	int32_t ___createdUserId_8;
	// System.String Mobilmo::m_userName
	String_t* ___m_userName_9;
	// System.String Mobilmo::m_userRank
	String_t* ___m_userRank_10;
	// System.Collections.Generic.List`1<System.Int32> Mobilmo::PrizedMasterIdList
	List_1_t1440998580 * ___PrizedMasterIdList_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,Module> Mobilmo::ActionA
	Dictionary_2_t2148260463 * ___ActionA_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,Module> Mobilmo::ActionB
	Dictionary_2_t2148260463 * ___ActionB_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,Module> Mobilmo::ActionX
	Dictionary_2_t2148260463 * ___ActionX_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,Module> Mobilmo::ActionY
	Dictionary_2_t2148260463 * ___ActionY_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,Module> Mobilmo::moduleList
	Dictionary_2_t2148260463 * ___moduleList_16;
	// UnityEngine.GameObject Mobilmo::m_CoreObj
	GameObject_t1756533147 * ___m_CoreObj_17;
	// System.Boolean Mobilmo::m_bRecMobility
	bool ___m_bRecMobility_18;
	// System.Boolean Mobilmo::m_bCoreNone
	bool ___m_bCoreNone_19;
	// System.Boolean Mobilmo::m_bTutorialCloneMobilmo
	bool ___m_bTutorialCloneMobilmo_20;
	// System.Single Mobilmo::m_fDefaultMoveTime
	float ___m_fDefaultMoveTime_21;
	// System.Int32 Mobilmo::m_sMotionNum
	int32_t ___m_sMotionNum_22;
	// System.Collections.Generic.List`1<RecordingManager/recordingMotionData> Mobilmo::recordingMotionDataList
	List_1_t1073334888 * ___recordingMotionDataList_23;

public:
	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___startPosition_2)); }
	inline Vector3_t2243707580  get_startPosition_2() const { return ___startPosition_2; }
	inline Vector3_t2243707580 * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(Vector3_t2243707580  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_startRotation_3() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___startRotation_3)); }
	inline Vector3_t2243707580  get_startRotation_3() const { return ___startRotation_3; }
	inline Vector3_t2243707580 * get_address_of_startRotation_3() { return &___startRotation_3; }
	inline void set_startRotation_3(Vector3_t2243707580  value)
	{
		___startRotation_3 = value;
	}

	inline static int32_t get_offset_of_mobilmoId_4() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___mobilmoId_4)); }
	inline int32_t get_mobilmoId_4() const { return ___mobilmoId_4; }
	inline int32_t* get_address_of_mobilmoId_4() { return &___mobilmoId_4; }
	inline void set_mobilmoId_4(int32_t value)
	{
		___mobilmoId_4 = value;
	}

	inline static int32_t get_offset_of_mobilityName_5() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___mobilityName_5)); }
	inline String_t* get_mobilityName_5() const { return ___mobilityName_5; }
	inline String_t** get_address_of_mobilityName_5() { return &___mobilityName_5; }
	inline void set_mobilityName_5(String_t* value)
	{
		___mobilityName_5 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityName_5, value);
	}

	inline static int32_t get_offset_of_m_nearMobUserId_6() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_nearMobUserId_6)); }
	inline int32_t get_m_nearMobUserId_6() const { return ___m_nearMobUserId_6; }
	inline int32_t* get_address_of_m_nearMobUserId_6() { return &___m_nearMobUserId_6; }
	inline void set_m_nearMobUserId_6(int32_t value)
	{
		___m_nearMobUserId_6 = value;
	}

	inline static int32_t get_offset_of_prizedIdListCnt_7() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___prizedIdListCnt_7)); }
	inline int32_t get_prizedIdListCnt_7() const { return ___prizedIdListCnt_7; }
	inline int32_t* get_address_of_prizedIdListCnt_7() { return &___prizedIdListCnt_7; }
	inline void set_prizedIdListCnt_7(int32_t value)
	{
		___prizedIdListCnt_7 = value;
	}

	inline static int32_t get_offset_of_createdUserId_8() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___createdUserId_8)); }
	inline int32_t get_createdUserId_8() const { return ___createdUserId_8; }
	inline int32_t* get_address_of_createdUserId_8() { return &___createdUserId_8; }
	inline void set_createdUserId_8(int32_t value)
	{
		___createdUserId_8 = value;
	}

	inline static int32_t get_offset_of_m_userName_9() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_userName_9)); }
	inline String_t* get_m_userName_9() const { return ___m_userName_9; }
	inline String_t** get_address_of_m_userName_9() { return &___m_userName_9; }
	inline void set_m_userName_9(String_t* value)
	{
		___m_userName_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_userName_9, value);
	}

	inline static int32_t get_offset_of_m_userRank_10() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_userRank_10)); }
	inline String_t* get_m_userRank_10() const { return ___m_userRank_10; }
	inline String_t** get_address_of_m_userRank_10() { return &___m_userRank_10; }
	inline void set_m_userRank_10(String_t* value)
	{
		___m_userRank_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_userRank_10, value);
	}

	inline static int32_t get_offset_of_PrizedMasterIdList_11() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___PrizedMasterIdList_11)); }
	inline List_1_t1440998580 * get_PrizedMasterIdList_11() const { return ___PrizedMasterIdList_11; }
	inline List_1_t1440998580 ** get_address_of_PrizedMasterIdList_11() { return &___PrizedMasterIdList_11; }
	inline void set_PrizedMasterIdList_11(List_1_t1440998580 * value)
	{
		___PrizedMasterIdList_11 = value;
		Il2CppCodeGenWriteBarrier(&___PrizedMasterIdList_11, value);
	}

	inline static int32_t get_offset_of_ActionA_12() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___ActionA_12)); }
	inline Dictionary_2_t2148260463 * get_ActionA_12() const { return ___ActionA_12; }
	inline Dictionary_2_t2148260463 ** get_address_of_ActionA_12() { return &___ActionA_12; }
	inline void set_ActionA_12(Dictionary_2_t2148260463 * value)
	{
		___ActionA_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionA_12, value);
	}

	inline static int32_t get_offset_of_ActionB_13() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___ActionB_13)); }
	inline Dictionary_2_t2148260463 * get_ActionB_13() const { return ___ActionB_13; }
	inline Dictionary_2_t2148260463 ** get_address_of_ActionB_13() { return &___ActionB_13; }
	inline void set_ActionB_13(Dictionary_2_t2148260463 * value)
	{
		___ActionB_13 = value;
		Il2CppCodeGenWriteBarrier(&___ActionB_13, value);
	}

	inline static int32_t get_offset_of_ActionX_14() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___ActionX_14)); }
	inline Dictionary_2_t2148260463 * get_ActionX_14() const { return ___ActionX_14; }
	inline Dictionary_2_t2148260463 ** get_address_of_ActionX_14() { return &___ActionX_14; }
	inline void set_ActionX_14(Dictionary_2_t2148260463 * value)
	{
		___ActionX_14 = value;
		Il2CppCodeGenWriteBarrier(&___ActionX_14, value);
	}

	inline static int32_t get_offset_of_ActionY_15() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___ActionY_15)); }
	inline Dictionary_2_t2148260463 * get_ActionY_15() const { return ___ActionY_15; }
	inline Dictionary_2_t2148260463 ** get_address_of_ActionY_15() { return &___ActionY_15; }
	inline void set_ActionY_15(Dictionary_2_t2148260463 * value)
	{
		___ActionY_15 = value;
		Il2CppCodeGenWriteBarrier(&___ActionY_15, value);
	}

	inline static int32_t get_offset_of_moduleList_16() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___moduleList_16)); }
	inline Dictionary_2_t2148260463 * get_moduleList_16() const { return ___moduleList_16; }
	inline Dictionary_2_t2148260463 ** get_address_of_moduleList_16() { return &___moduleList_16; }
	inline void set_moduleList_16(Dictionary_2_t2148260463 * value)
	{
		___moduleList_16 = value;
		Il2CppCodeGenWriteBarrier(&___moduleList_16, value);
	}

	inline static int32_t get_offset_of_m_CoreObj_17() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_CoreObj_17)); }
	inline GameObject_t1756533147 * get_m_CoreObj_17() const { return ___m_CoreObj_17; }
	inline GameObject_t1756533147 ** get_address_of_m_CoreObj_17() { return &___m_CoreObj_17; }
	inline void set_m_CoreObj_17(GameObject_t1756533147 * value)
	{
		___m_CoreObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_CoreObj_17, value);
	}

	inline static int32_t get_offset_of_m_bRecMobility_18() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_bRecMobility_18)); }
	inline bool get_m_bRecMobility_18() const { return ___m_bRecMobility_18; }
	inline bool* get_address_of_m_bRecMobility_18() { return &___m_bRecMobility_18; }
	inline void set_m_bRecMobility_18(bool value)
	{
		___m_bRecMobility_18 = value;
	}

	inline static int32_t get_offset_of_m_bCoreNone_19() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_bCoreNone_19)); }
	inline bool get_m_bCoreNone_19() const { return ___m_bCoreNone_19; }
	inline bool* get_address_of_m_bCoreNone_19() { return &___m_bCoreNone_19; }
	inline void set_m_bCoreNone_19(bool value)
	{
		___m_bCoreNone_19 = value;
	}

	inline static int32_t get_offset_of_m_bTutorialCloneMobilmo_20() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_bTutorialCloneMobilmo_20)); }
	inline bool get_m_bTutorialCloneMobilmo_20() const { return ___m_bTutorialCloneMobilmo_20; }
	inline bool* get_address_of_m_bTutorialCloneMobilmo_20() { return &___m_bTutorialCloneMobilmo_20; }
	inline void set_m_bTutorialCloneMobilmo_20(bool value)
	{
		___m_bTutorialCloneMobilmo_20 = value;
	}

	inline static int32_t get_offset_of_m_fDefaultMoveTime_21() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_fDefaultMoveTime_21)); }
	inline float get_m_fDefaultMoveTime_21() const { return ___m_fDefaultMoveTime_21; }
	inline float* get_address_of_m_fDefaultMoveTime_21() { return &___m_fDefaultMoveTime_21; }
	inline void set_m_fDefaultMoveTime_21(float value)
	{
		___m_fDefaultMoveTime_21 = value;
	}

	inline static int32_t get_offset_of_m_sMotionNum_22() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___m_sMotionNum_22)); }
	inline int32_t get_m_sMotionNum_22() const { return ___m_sMotionNum_22; }
	inline int32_t* get_address_of_m_sMotionNum_22() { return &___m_sMotionNum_22; }
	inline void set_m_sMotionNum_22(int32_t value)
	{
		___m_sMotionNum_22 = value;
	}

	inline static int32_t get_offset_of_recordingMotionDataList_23() { return static_cast<int32_t>(offsetof(Mobilmo_t370754809, ___recordingMotionDataList_23)); }
	inline List_1_t1073334888 * get_recordingMotionDataList_23() const { return ___recordingMotionDataList_23; }
	inline List_1_t1073334888 ** get_address_of_recordingMotionDataList_23() { return &___recordingMotionDataList_23; }
	inline void set_recordingMotionDataList_23(List_1_t1073334888 * value)
	{
		___recordingMotionDataList_23 = value;
		Il2CppCodeGenWriteBarrier(&___recordingMotionDataList_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
