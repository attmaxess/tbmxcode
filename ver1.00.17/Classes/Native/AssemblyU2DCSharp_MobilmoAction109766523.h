﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Canvas
struct Canvas_t209405766;
// MoveSet
struct MoveSet_t2777185973;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Mobilmo
struct Mobilmo_t370754809;
// System.Collections.Generic.Dictionary`2<System.Int32,Module>
struct Dictionary_2_t2148260463;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t764358782;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoAction
struct  MobilmoAction_t109766523  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Canvas MobilmoAction::MobilityCanvas
	Canvas_t209405766 * ___MobilityCanvas_2;
	// MoveSet MobilmoAction::MoveSet
	MoveSet_t2777185973 * ___MoveSet_3;
	// UnityEngine.UI.Image MobilmoAction::Background
	Image_t2042527209 * ___Background_4;
	// UnityEngine.GameObject MobilmoAction::m_CopyMobilmoObj
	GameObject_t1756533147 * ___m_CopyMobilmoObj_5;
	// Mobilmo MobilmoAction::m_CopyMobilmo
	Mobilmo_t370754809 * ___m_CopyMobilmo_6;
	// Mobilmo MobilmoAction::m_OriginMobilmo
	Mobilmo_t370754809 * ___m_OriginMobilmo_7;
	// System.Boolean MobilmoAction::m_bFromMyPage
	bool ___m_bFromMyPage_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,Module> MobilmoAction::m_pSelectMap
	Dictionary_2_t2148260463 * ___m_pSelectMap_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> MobilmoAction::m_pActionObjMap
	Dictionary_2_t764358782 * ___m_pActionObjMap_10;
	// UnityEngine.RectTransform MobilmoAction::m_CanvasTrans
	RectTransform_t3349966182 * ___m_CanvasTrans_11;
	// UnityEngine.Transform MobilmoAction::actionModuleArea
	Transform_t3275118058 * ___actionModuleArea_12;
	// UnityEngine.GameObject MobilmoAction::actionObj
	GameObject_t1756533147 * ___actionObj_13;

public:
	inline static int32_t get_offset_of_MobilityCanvas_2() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___MobilityCanvas_2)); }
	inline Canvas_t209405766 * get_MobilityCanvas_2() const { return ___MobilityCanvas_2; }
	inline Canvas_t209405766 ** get_address_of_MobilityCanvas_2() { return &___MobilityCanvas_2; }
	inline void set_MobilityCanvas_2(Canvas_t209405766 * value)
	{
		___MobilityCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityCanvas_2, value);
	}

	inline static int32_t get_offset_of_MoveSet_3() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___MoveSet_3)); }
	inline MoveSet_t2777185973 * get_MoveSet_3() const { return ___MoveSet_3; }
	inline MoveSet_t2777185973 ** get_address_of_MoveSet_3() { return &___MoveSet_3; }
	inline void set_MoveSet_3(MoveSet_t2777185973 * value)
	{
		___MoveSet_3 = value;
		Il2CppCodeGenWriteBarrier(&___MoveSet_3, value);
	}

	inline static int32_t get_offset_of_Background_4() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___Background_4)); }
	inline Image_t2042527209 * get_Background_4() const { return ___Background_4; }
	inline Image_t2042527209 ** get_address_of_Background_4() { return &___Background_4; }
	inline void set_Background_4(Image_t2042527209 * value)
	{
		___Background_4 = value;
		Il2CppCodeGenWriteBarrier(&___Background_4, value);
	}

	inline static int32_t get_offset_of_m_CopyMobilmoObj_5() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___m_CopyMobilmoObj_5)); }
	inline GameObject_t1756533147 * get_m_CopyMobilmoObj_5() const { return ___m_CopyMobilmoObj_5; }
	inline GameObject_t1756533147 ** get_address_of_m_CopyMobilmoObj_5() { return &___m_CopyMobilmoObj_5; }
	inline void set_m_CopyMobilmoObj_5(GameObject_t1756533147 * value)
	{
		___m_CopyMobilmoObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_CopyMobilmoObj_5, value);
	}

	inline static int32_t get_offset_of_m_CopyMobilmo_6() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___m_CopyMobilmo_6)); }
	inline Mobilmo_t370754809 * get_m_CopyMobilmo_6() const { return ___m_CopyMobilmo_6; }
	inline Mobilmo_t370754809 ** get_address_of_m_CopyMobilmo_6() { return &___m_CopyMobilmo_6; }
	inline void set_m_CopyMobilmo_6(Mobilmo_t370754809 * value)
	{
		___m_CopyMobilmo_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_CopyMobilmo_6, value);
	}

	inline static int32_t get_offset_of_m_OriginMobilmo_7() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___m_OriginMobilmo_7)); }
	inline Mobilmo_t370754809 * get_m_OriginMobilmo_7() const { return ___m_OriginMobilmo_7; }
	inline Mobilmo_t370754809 ** get_address_of_m_OriginMobilmo_7() { return &___m_OriginMobilmo_7; }
	inline void set_m_OriginMobilmo_7(Mobilmo_t370754809 * value)
	{
		___m_OriginMobilmo_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_OriginMobilmo_7, value);
	}

	inline static int32_t get_offset_of_m_bFromMyPage_8() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___m_bFromMyPage_8)); }
	inline bool get_m_bFromMyPage_8() const { return ___m_bFromMyPage_8; }
	inline bool* get_address_of_m_bFromMyPage_8() { return &___m_bFromMyPage_8; }
	inline void set_m_bFromMyPage_8(bool value)
	{
		___m_bFromMyPage_8 = value;
	}

	inline static int32_t get_offset_of_m_pSelectMap_9() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___m_pSelectMap_9)); }
	inline Dictionary_2_t2148260463 * get_m_pSelectMap_9() const { return ___m_pSelectMap_9; }
	inline Dictionary_2_t2148260463 ** get_address_of_m_pSelectMap_9() { return &___m_pSelectMap_9; }
	inline void set_m_pSelectMap_9(Dictionary_2_t2148260463 * value)
	{
		___m_pSelectMap_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_pSelectMap_9, value);
	}

	inline static int32_t get_offset_of_m_pActionObjMap_10() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___m_pActionObjMap_10)); }
	inline Dictionary_2_t764358782 * get_m_pActionObjMap_10() const { return ___m_pActionObjMap_10; }
	inline Dictionary_2_t764358782 ** get_address_of_m_pActionObjMap_10() { return &___m_pActionObjMap_10; }
	inline void set_m_pActionObjMap_10(Dictionary_2_t764358782 * value)
	{
		___m_pActionObjMap_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_pActionObjMap_10, value);
	}

	inline static int32_t get_offset_of_m_CanvasTrans_11() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___m_CanvasTrans_11)); }
	inline RectTransform_t3349966182 * get_m_CanvasTrans_11() const { return ___m_CanvasTrans_11; }
	inline RectTransform_t3349966182 ** get_address_of_m_CanvasTrans_11() { return &___m_CanvasTrans_11; }
	inline void set_m_CanvasTrans_11(RectTransform_t3349966182 * value)
	{
		___m_CanvasTrans_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_CanvasTrans_11, value);
	}

	inline static int32_t get_offset_of_actionModuleArea_12() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___actionModuleArea_12)); }
	inline Transform_t3275118058 * get_actionModuleArea_12() const { return ___actionModuleArea_12; }
	inline Transform_t3275118058 ** get_address_of_actionModuleArea_12() { return &___actionModuleArea_12; }
	inline void set_actionModuleArea_12(Transform_t3275118058 * value)
	{
		___actionModuleArea_12 = value;
		Il2CppCodeGenWriteBarrier(&___actionModuleArea_12, value);
	}

	inline static int32_t get_offset_of_actionObj_13() { return static_cast<int32_t>(offsetof(MobilmoAction_t109766523, ___actionObj_13)); }
	inline GameObject_t1756533147 * get_actionObj_13() const { return ___actionObj_13; }
	inline GameObject_t1756533147 ** get_address_of_actionObj_13() { return &___actionObj_13; }
	inline void set_actionObj_13(GameObject_t1756533147 * value)
	{
		___actionObj_13 = value;
		Il2CppCodeGenWriteBarrier(&___actionObj_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
