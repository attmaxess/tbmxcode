﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Module[]
struct ModuleU5BU5D_t2077132997;
// Parts[]
struct PartsU5BU5D_t3272295067;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t3881993182;
// MobilmoAction
struct MobilmoAction_t109766523;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoAction/<SetActionMobilmo>c__Iterator0
struct  U3CSetActionMobilmoU3Ec__Iterator0_t2346939560  : public Il2CppObject
{
public:
	// Module[] MobilmoAction/<SetActionMobilmo>c__Iterator0::<modulist>__0
	ModuleU5BU5D_t2077132997* ___U3CmodulistU3E__0_0;
	// Module[] MobilmoAction/<SetActionMobilmo>c__Iterator0::$locvar0
	ModuleU5BU5D_t2077132997* ___U24locvar0_1;
	// System.Int32 MobilmoAction/<SetActionMobilmo>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// Parts[] MobilmoAction/<SetActionMobilmo>c__Iterator0::$locvar2
	PartsU5BU5D_t3272295067* ___U24locvar2_3;
	// System.Int32 MobilmoAction/<SetActionMobilmo>c__Iterator0::$locvar3
	int32_t ___U24locvar3_4;
	// UnityEngine.Rigidbody[] MobilmoAction/<SetActionMobilmo>c__Iterator0::$locvar4
	RigidbodyU5BU5D_t3881993182* ___U24locvar4_5;
	// System.Int32 MobilmoAction/<SetActionMobilmo>c__Iterator0::$locvar5
	int32_t ___U24locvar5_6;
	// MobilmoAction MobilmoAction/<SetActionMobilmo>c__Iterator0::$this
	MobilmoAction_t109766523 * ___U24this_7;
	// System.Object MobilmoAction/<SetActionMobilmo>c__Iterator0::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean MobilmoAction/<SetActionMobilmo>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 MobilmoAction/<SetActionMobilmo>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CmodulistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U3CmodulistU3E__0_0)); }
	inline ModuleU5BU5D_t2077132997* get_U3CmodulistU3E__0_0() const { return ___U3CmodulistU3E__0_0; }
	inline ModuleU5BU5D_t2077132997** get_address_of_U3CmodulistU3E__0_0() { return &___U3CmodulistU3E__0_0; }
	inline void set_U3CmodulistU3E__0_0(ModuleU5BU5D_t2077132997* value)
	{
		___U3CmodulistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmodulistU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24locvar0_1)); }
	inline ModuleU5BU5D_t2077132997* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ModuleU5BU5D_t2077132997** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ModuleU5BU5D_t2077132997* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_3() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24locvar2_3)); }
	inline PartsU5BU5D_t3272295067* get_U24locvar2_3() const { return ___U24locvar2_3; }
	inline PartsU5BU5D_t3272295067** get_address_of_U24locvar2_3() { return &___U24locvar2_3; }
	inline void set_U24locvar2_3(PartsU5BU5D_t3272295067* value)
	{
		___U24locvar2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar2_3, value);
	}

	inline static int32_t get_offset_of_U24locvar3_4() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24locvar3_4)); }
	inline int32_t get_U24locvar3_4() const { return ___U24locvar3_4; }
	inline int32_t* get_address_of_U24locvar3_4() { return &___U24locvar3_4; }
	inline void set_U24locvar3_4(int32_t value)
	{
		___U24locvar3_4 = value;
	}

	inline static int32_t get_offset_of_U24locvar4_5() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24locvar4_5)); }
	inline RigidbodyU5BU5D_t3881993182* get_U24locvar4_5() const { return ___U24locvar4_5; }
	inline RigidbodyU5BU5D_t3881993182** get_address_of_U24locvar4_5() { return &___U24locvar4_5; }
	inline void set_U24locvar4_5(RigidbodyU5BU5D_t3881993182* value)
	{
		___U24locvar4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar4_5, value);
	}

	inline static int32_t get_offset_of_U24locvar5_6() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24locvar5_6)); }
	inline int32_t get_U24locvar5_6() const { return ___U24locvar5_6; }
	inline int32_t* get_address_of_U24locvar5_6() { return &___U24locvar5_6; }
	inline void set_U24locvar5_6(int32_t value)
	{
		___U24locvar5_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24this_7)); }
	inline MobilmoAction_t109766523 * get_U24this_7() const { return ___U24this_7; }
	inline MobilmoAction_t109766523 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(MobilmoAction_t109766523 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CSetActionMobilmoU3Ec__Iterator0_t2346939560, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
