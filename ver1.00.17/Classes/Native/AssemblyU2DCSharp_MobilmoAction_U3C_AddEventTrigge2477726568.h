﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// MobilmoAction
struct MobilmoAction_t109766523;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoAction/<_AddEventTriggerActionObj>c__AnonStorey2
struct  U3C_AddEventTriggerActionObjU3Ec__AnonStorey2_t2477726568  : public Il2CppObject
{
public:
	// System.Int32 MobilmoAction/<_AddEventTriggerActionObj>c__AnonStorey2::id
	int32_t ___id_0;
	// UnityEngine.GameObject MobilmoAction/<_AddEventTriggerActionObj>c__AnonStorey2::obj
	GameObject_t1756533147 * ___obj_1;
	// MobilmoAction MobilmoAction/<_AddEventTriggerActionObj>c__AnonStorey2::$this
	MobilmoAction_t109766523 * ___U24this_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3C_AddEventTriggerActionObjU3Ec__AnonStorey2_t2477726568, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_obj_1() { return static_cast<int32_t>(offsetof(U3C_AddEventTriggerActionObjU3Ec__AnonStorey2_t2477726568, ___obj_1)); }
	inline GameObject_t1756533147 * get_obj_1() const { return ___obj_1; }
	inline GameObject_t1756533147 ** get_address_of_obj_1() { return &___obj_1; }
	inline void set_obj_1(GameObject_t1756533147 * value)
	{
		___obj_1 = value;
		Il2CppCodeGenWriteBarrier(&___obj_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_AddEventTriggerActionObjU3Ec__AnonStorey2_t2477726568, ___U24this_2)); }
	inline MobilmoAction_t109766523 * get_U24this_2() const { return ___U24this_2; }
	inline MobilmoAction_t109766523 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MobilmoAction_t109766523 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
