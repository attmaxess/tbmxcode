﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// MobilmoAction
struct MobilmoAction_t109766523;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoAction/<_CreateActionObj>c__AnonStorey1
struct  U3C_CreateActionObjU3Ec__AnonStorey1_t1482489480  : public Il2CppObject
{
public:
	// System.Int32 MobilmoAction/<_CreateActionObj>c__AnonStorey1::id
	int32_t ___id_0;
	// MobilmoAction MobilmoAction/<_CreateActionObj>c__AnonStorey1::$this
	MobilmoAction_t109766523 * ___U24this_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3C_CreateActionObjU3Ec__AnonStorey1_t1482489480, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3C_CreateActionObjU3Ec__AnonStorey1_t1482489480, ___U24this_1)); }
	inline MobilmoAction_t109766523 * get_U24this_1() const { return ___U24this_1; }
	inline MobilmoAction_t109766523 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MobilmoAction_t109766523 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
