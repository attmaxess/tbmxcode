﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.String
struct String_t;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoDnaController
struct  MobilmoDnaController_t1006271200  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 MobilmoDnaController::m_moduleCount
	int32_t ___m_moduleCount_2;
	// UnityEngine.Transform MobilmoDnaController::dnaMobilmoArea
	Transform_t3275118058 * ___dnaMobilmoArea_3;
	// UnityEngine.Transform MobilmoDnaController::dnaOriginalArea
	Transform_t3275118058 * ___dnaOriginalArea_4;
	// UnityEngine.Transform MobilmoDnaController::dnaCobilmoArea
	Transform_t3275118058 * ___dnaCobilmoArea_5;
	// UnityEngine.Transform MobilmoDnaController::dnaEmblemArea
	Transform_t3275118058 * ___dnaEmblemArea_6;
	// UnityEngine.GameObject MobilmoDnaController::areaObj
	GameObject_t1756533147 * ___areaObj_7;
	// UnityEngine.GameObject MobilmoDnaController::mobilmoAreaObj
	GameObject_t1756533147 * ___mobilmoAreaObj_8;
	// UnityEngine.GameObject MobilmoDnaController::emblemAreaObj
	GameObject_t1756533147 * ___emblemAreaObj_9;
	// UnityEngine.Vector3 MobilmoDnaController::moduleScale
	Vector3_t2243707580  ___moduleScale_10;
	// UnityEngine.GameObject MobilmoDnaController::raderMobilmoObject
	GameObject_t1756533147 * ___raderMobilmoObject_11;
	// UnityEngine.Vector3 MobilmoDnaController::originMobilmoAreaPos
	Vector3_t2243707580  ___originMobilmoAreaPos_12;
	// System.Boolean MobilmoDnaController::isClicked
	bool ___isClicked_13;
	// UnityEngine.UI.Image MobilmoDnaController::emblemImage
	Image_t2042527209 * ___emblemImage_14;
	// UnityEngine.UI.Text MobilmoDnaController::mobilmoNameText
	Text_t356221433 * ___mobilmoNameText_15;
	// UnityEngine.UI.Text MobilmoDnaController::userNameText
	Text_t356221433 * ___userNameText_16;
	// UnityEngine.UI.Text MobilmoDnaController::reactionCntText
	Text_t356221433 * ___reactionCntText_17;
	// UnityEngine.UI.Text MobilmoDnaController::prizedCntText
	Text_t356221433 * ___prizedCntText_18;
	// UnityEngine.GameObject MobilmoDnaController::reactionPanel
	GameObject_t1756533147 * ___reactionPanel_19;
	// UnityEngine.GameObject MobilmoDnaController::TutorialGetModulePanel
	GameObject_t1756533147 * ___TutorialGetModulePanel_20;
	// System.Int32 MobilmoDnaController::selectedModId
	int32_t ___selectedModId_21;
	// System.Int32 MobilmoDnaController::createUserId
	int32_t ___createUserId_22;
	// System.Collections.Generic.List`1<System.Int32> MobilmoDnaController::m_liEmblemList
	List_1_t1440998580 * ___m_liEmblemList_23;
	// System.String MobilmoDnaController::previousModuleId
	String_t* ___previousModuleId_24;
	// UnityEngine.GameObject MobilmoDnaController::mobilmoDnaPanelScroll
	GameObject_t1756533147 * ___mobilmoDnaPanelScroll_25;
	// UnityEngine.RectTransform MobilmoDnaController::mobilmoDnaPanelTrans
	RectTransform_t3349966182 * ___mobilmoDnaPanelTrans_26;
	// System.Single MobilmoDnaController::originalMobilmoDnaPanelPosY
	float ___originalMobilmoDnaPanelPosY_27;
	// System.Single MobilmoDnaController::originalMobilmoDnaPanelSizeDeltaY
	float ___originalMobilmoDnaPanelSizeDeltaY_28;
	// System.Int32 MobilmoDnaController::mobilmoCnt
	int32_t ___mobilmoCnt_29;
	// System.Single MobilmoDnaController::dnaScollPosY
	float ___dnaScollPosY_30;
	// System.Int32 MobilmoDnaController::scollCnt
	int32_t ___scollCnt_31;
	// System.Single MobilmoDnaController::mobilmoRows
	float ___mobilmoRows_32;
	// UnityEngine.GameObject MobilmoDnaController::mobilmo
	GameObject_t1756533147 * ___mobilmo_33;
	// UnityEngine.GameObject MobilmoDnaController::childPartsObj
	GameObject_t1756533147 * ___childPartsObj_34;
	// UnityEngine.GameObject MobilmoDnaController::ChildCenter
	GameObject_t1756533147 * ___ChildCenter_35;
	// UnityEngine.GameObject MobilmoDnaController::ChildLeap
	GameObject_t1756533147 * ___ChildLeap_36;
	// UnityEngine.GameObject MobilmoDnaController::dnaModuleObj
	GameObject_t1756533147 * ___dnaModuleObj_37;
	// UnityEngine.GameObject MobilmoDnaController::ModuleCenter
	GameObject_t1756533147 * ___ModuleCenter_38;
	// System.Int32 MobilmoDnaController::cnt
	int32_t ___cnt_39;

public:
	inline static int32_t get_offset_of_m_moduleCount_2() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___m_moduleCount_2)); }
	inline int32_t get_m_moduleCount_2() const { return ___m_moduleCount_2; }
	inline int32_t* get_address_of_m_moduleCount_2() { return &___m_moduleCount_2; }
	inline void set_m_moduleCount_2(int32_t value)
	{
		___m_moduleCount_2 = value;
	}

	inline static int32_t get_offset_of_dnaMobilmoArea_3() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___dnaMobilmoArea_3)); }
	inline Transform_t3275118058 * get_dnaMobilmoArea_3() const { return ___dnaMobilmoArea_3; }
	inline Transform_t3275118058 ** get_address_of_dnaMobilmoArea_3() { return &___dnaMobilmoArea_3; }
	inline void set_dnaMobilmoArea_3(Transform_t3275118058 * value)
	{
		___dnaMobilmoArea_3 = value;
		Il2CppCodeGenWriteBarrier(&___dnaMobilmoArea_3, value);
	}

	inline static int32_t get_offset_of_dnaOriginalArea_4() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___dnaOriginalArea_4)); }
	inline Transform_t3275118058 * get_dnaOriginalArea_4() const { return ___dnaOriginalArea_4; }
	inline Transform_t3275118058 ** get_address_of_dnaOriginalArea_4() { return &___dnaOriginalArea_4; }
	inline void set_dnaOriginalArea_4(Transform_t3275118058 * value)
	{
		___dnaOriginalArea_4 = value;
		Il2CppCodeGenWriteBarrier(&___dnaOriginalArea_4, value);
	}

	inline static int32_t get_offset_of_dnaCobilmoArea_5() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___dnaCobilmoArea_5)); }
	inline Transform_t3275118058 * get_dnaCobilmoArea_5() const { return ___dnaCobilmoArea_5; }
	inline Transform_t3275118058 ** get_address_of_dnaCobilmoArea_5() { return &___dnaCobilmoArea_5; }
	inline void set_dnaCobilmoArea_5(Transform_t3275118058 * value)
	{
		___dnaCobilmoArea_5 = value;
		Il2CppCodeGenWriteBarrier(&___dnaCobilmoArea_5, value);
	}

	inline static int32_t get_offset_of_dnaEmblemArea_6() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___dnaEmblemArea_6)); }
	inline Transform_t3275118058 * get_dnaEmblemArea_6() const { return ___dnaEmblemArea_6; }
	inline Transform_t3275118058 ** get_address_of_dnaEmblemArea_6() { return &___dnaEmblemArea_6; }
	inline void set_dnaEmblemArea_6(Transform_t3275118058 * value)
	{
		___dnaEmblemArea_6 = value;
		Il2CppCodeGenWriteBarrier(&___dnaEmblemArea_6, value);
	}

	inline static int32_t get_offset_of_areaObj_7() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___areaObj_7)); }
	inline GameObject_t1756533147 * get_areaObj_7() const { return ___areaObj_7; }
	inline GameObject_t1756533147 ** get_address_of_areaObj_7() { return &___areaObj_7; }
	inline void set_areaObj_7(GameObject_t1756533147 * value)
	{
		___areaObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___areaObj_7, value);
	}

	inline static int32_t get_offset_of_mobilmoAreaObj_8() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___mobilmoAreaObj_8)); }
	inline GameObject_t1756533147 * get_mobilmoAreaObj_8() const { return ___mobilmoAreaObj_8; }
	inline GameObject_t1756533147 ** get_address_of_mobilmoAreaObj_8() { return &___mobilmoAreaObj_8; }
	inline void set_mobilmoAreaObj_8(GameObject_t1756533147 * value)
	{
		___mobilmoAreaObj_8 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoAreaObj_8, value);
	}

	inline static int32_t get_offset_of_emblemAreaObj_9() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___emblemAreaObj_9)); }
	inline GameObject_t1756533147 * get_emblemAreaObj_9() const { return ___emblemAreaObj_9; }
	inline GameObject_t1756533147 ** get_address_of_emblemAreaObj_9() { return &___emblemAreaObj_9; }
	inline void set_emblemAreaObj_9(GameObject_t1756533147 * value)
	{
		___emblemAreaObj_9 = value;
		Il2CppCodeGenWriteBarrier(&___emblemAreaObj_9, value);
	}

	inline static int32_t get_offset_of_moduleScale_10() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___moduleScale_10)); }
	inline Vector3_t2243707580  get_moduleScale_10() const { return ___moduleScale_10; }
	inline Vector3_t2243707580 * get_address_of_moduleScale_10() { return &___moduleScale_10; }
	inline void set_moduleScale_10(Vector3_t2243707580  value)
	{
		___moduleScale_10 = value;
	}

	inline static int32_t get_offset_of_raderMobilmoObject_11() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___raderMobilmoObject_11)); }
	inline GameObject_t1756533147 * get_raderMobilmoObject_11() const { return ___raderMobilmoObject_11; }
	inline GameObject_t1756533147 ** get_address_of_raderMobilmoObject_11() { return &___raderMobilmoObject_11; }
	inline void set_raderMobilmoObject_11(GameObject_t1756533147 * value)
	{
		___raderMobilmoObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___raderMobilmoObject_11, value);
	}

	inline static int32_t get_offset_of_originMobilmoAreaPos_12() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___originMobilmoAreaPos_12)); }
	inline Vector3_t2243707580  get_originMobilmoAreaPos_12() const { return ___originMobilmoAreaPos_12; }
	inline Vector3_t2243707580 * get_address_of_originMobilmoAreaPos_12() { return &___originMobilmoAreaPos_12; }
	inline void set_originMobilmoAreaPos_12(Vector3_t2243707580  value)
	{
		___originMobilmoAreaPos_12 = value;
	}

	inline static int32_t get_offset_of_isClicked_13() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___isClicked_13)); }
	inline bool get_isClicked_13() const { return ___isClicked_13; }
	inline bool* get_address_of_isClicked_13() { return &___isClicked_13; }
	inline void set_isClicked_13(bool value)
	{
		___isClicked_13 = value;
	}

	inline static int32_t get_offset_of_emblemImage_14() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___emblemImage_14)); }
	inline Image_t2042527209 * get_emblemImage_14() const { return ___emblemImage_14; }
	inline Image_t2042527209 ** get_address_of_emblemImage_14() { return &___emblemImage_14; }
	inline void set_emblemImage_14(Image_t2042527209 * value)
	{
		___emblemImage_14 = value;
		Il2CppCodeGenWriteBarrier(&___emblemImage_14, value);
	}

	inline static int32_t get_offset_of_mobilmoNameText_15() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___mobilmoNameText_15)); }
	inline Text_t356221433 * get_mobilmoNameText_15() const { return ___mobilmoNameText_15; }
	inline Text_t356221433 ** get_address_of_mobilmoNameText_15() { return &___mobilmoNameText_15; }
	inline void set_mobilmoNameText_15(Text_t356221433 * value)
	{
		___mobilmoNameText_15 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoNameText_15, value);
	}

	inline static int32_t get_offset_of_userNameText_16() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___userNameText_16)); }
	inline Text_t356221433 * get_userNameText_16() const { return ___userNameText_16; }
	inline Text_t356221433 ** get_address_of_userNameText_16() { return &___userNameText_16; }
	inline void set_userNameText_16(Text_t356221433 * value)
	{
		___userNameText_16 = value;
		Il2CppCodeGenWriteBarrier(&___userNameText_16, value);
	}

	inline static int32_t get_offset_of_reactionCntText_17() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___reactionCntText_17)); }
	inline Text_t356221433 * get_reactionCntText_17() const { return ___reactionCntText_17; }
	inline Text_t356221433 ** get_address_of_reactionCntText_17() { return &___reactionCntText_17; }
	inline void set_reactionCntText_17(Text_t356221433 * value)
	{
		___reactionCntText_17 = value;
		Il2CppCodeGenWriteBarrier(&___reactionCntText_17, value);
	}

	inline static int32_t get_offset_of_prizedCntText_18() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___prizedCntText_18)); }
	inline Text_t356221433 * get_prizedCntText_18() const { return ___prizedCntText_18; }
	inline Text_t356221433 ** get_address_of_prizedCntText_18() { return &___prizedCntText_18; }
	inline void set_prizedCntText_18(Text_t356221433 * value)
	{
		___prizedCntText_18 = value;
		Il2CppCodeGenWriteBarrier(&___prizedCntText_18, value);
	}

	inline static int32_t get_offset_of_reactionPanel_19() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___reactionPanel_19)); }
	inline GameObject_t1756533147 * get_reactionPanel_19() const { return ___reactionPanel_19; }
	inline GameObject_t1756533147 ** get_address_of_reactionPanel_19() { return &___reactionPanel_19; }
	inline void set_reactionPanel_19(GameObject_t1756533147 * value)
	{
		___reactionPanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___reactionPanel_19, value);
	}

	inline static int32_t get_offset_of_TutorialGetModulePanel_20() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___TutorialGetModulePanel_20)); }
	inline GameObject_t1756533147 * get_TutorialGetModulePanel_20() const { return ___TutorialGetModulePanel_20; }
	inline GameObject_t1756533147 ** get_address_of_TutorialGetModulePanel_20() { return &___TutorialGetModulePanel_20; }
	inline void set_TutorialGetModulePanel_20(GameObject_t1756533147 * value)
	{
		___TutorialGetModulePanel_20 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialGetModulePanel_20, value);
	}

	inline static int32_t get_offset_of_selectedModId_21() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___selectedModId_21)); }
	inline int32_t get_selectedModId_21() const { return ___selectedModId_21; }
	inline int32_t* get_address_of_selectedModId_21() { return &___selectedModId_21; }
	inline void set_selectedModId_21(int32_t value)
	{
		___selectedModId_21 = value;
	}

	inline static int32_t get_offset_of_createUserId_22() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___createUserId_22)); }
	inline int32_t get_createUserId_22() const { return ___createUserId_22; }
	inline int32_t* get_address_of_createUserId_22() { return &___createUserId_22; }
	inline void set_createUserId_22(int32_t value)
	{
		___createUserId_22 = value;
	}

	inline static int32_t get_offset_of_m_liEmblemList_23() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___m_liEmblemList_23)); }
	inline List_1_t1440998580 * get_m_liEmblemList_23() const { return ___m_liEmblemList_23; }
	inline List_1_t1440998580 ** get_address_of_m_liEmblemList_23() { return &___m_liEmblemList_23; }
	inline void set_m_liEmblemList_23(List_1_t1440998580 * value)
	{
		___m_liEmblemList_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_liEmblemList_23, value);
	}

	inline static int32_t get_offset_of_previousModuleId_24() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___previousModuleId_24)); }
	inline String_t* get_previousModuleId_24() const { return ___previousModuleId_24; }
	inline String_t** get_address_of_previousModuleId_24() { return &___previousModuleId_24; }
	inline void set_previousModuleId_24(String_t* value)
	{
		___previousModuleId_24 = value;
		Il2CppCodeGenWriteBarrier(&___previousModuleId_24, value);
	}

	inline static int32_t get_offset_of_mobilmoDnaPanelScroll_25() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___mobilmoDnaPanelScroll_25)); }
	inline GameObject_t1756533147 * get_mobilmoDnaPanelScroll_25() const { return ___mobilmoDnaPanelScroll_25; }
	inline GameObject_t1756533147 ** get_address_of_mobilmoDnaPanelScroll_25() { return &___mobilmoDnaPanelScroll_25; }
	inline void set_mobilmoDnaPanelScroll_25(GameObject_t1756533147 * value)
	{
		___mobilmoDnaPanelScroll_25 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoDnaPanelScroll_25, value);
	}

	inline static int32_t get_offset_of_mobilmoDnaPanelTrans_26() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___mobilmoDnaPanelTrans_26)); }
	inline RectTransform_t3349966182 * get_mobilmoDnaPanelTrans_26() const { return ___mobilmoDnaPanelTrans_26; }
	inline RectTransform_t3349966182 ** get_address_of_mobilmoDnaPanelTrans_26() { return &___mobilmoDnaPanelTrans_26; }
	inline void set_mobilmoDnaPanelTrans_26(RectTransform_t3349966182 * value)
	{
		___mobilmoDnaPanelTrans_26 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoDnaPanelTrans_26, value);
	}

	inline static int32_t get_offset_of_originalMobilmoDnaPanelPosY_27() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___originalMobilmoDnaPanelPosY_27)); }
	inline float get_originalMobilmoDnaPanelPosY_27() const { return ___originalMobilmoDnaPanelPosY_27; }
	inline float* get_address_of_originalMobilmoDnaPanelPosY_27() { return &___originalMobilmoDnaPanelPosY_27; }
	inline void set_originalMobilmoDnaPanelPosY_27(float value)
	{
		___originalMobilmoDnaPanelPosY_27 = value;
	}

	inline static int32_t get_offset_of_originalMobilmoDnaPanelSizeDeltaY_28() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___originalMobilmoDnaPanelSizeDeltaY_28)); }
	inline float get_originalMobilmoDnaPanelSizeDeltaY_28() const { return ___originalMobilmoDnaPanelSizeDeltaY_28; }
	inline float* get_address_of_originalMobilmoDnaPanelSizeDeltaY_28() { return &___originalMobilmoDnaPanelSizeDeltaY_28; }
	inline void set_originalMobilmoDnaPanelSizeDeltaY_28(float value)
	{
		___originalMobilmoDnaPanelSizeDeltaY_28 = value;
	}

	inline static int32_t get_offset_of_mobilmoCnt_29() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___mobilmoCnt_29)); }
	inline int32_t get_mobilmoCnt_29() const { return ___mobilmoCnt_29; }
	inline int32_t* get_address_of_mobilmoCnt_29() { return &___mobilmoCnt_29; }
	inline void set_mobilmoCnt_29(int32_t value)
	{
		___mobilmoCnt_29 = value;
	}

	inline static int32_t get_offset_of_dnaScollPosY_30() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___dnaScollPosY_30)); }
	inline float get_dnaScollPosY_30() const { return ___dnaScollPosY_30; }
	inline float* get_address_of_dnaScollPosY_30() { return &___dnaScollPosY_30; }
	inline void set_dnaScollPosY_30(float value)
	{
		___dnaScollPosY_30 = value;
	}

	inline static int32_t get_offset_of_scollCnt_31() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___scollCnt_31)); }
	inline int32_t get_scollCnt_31() const { return ___scollCnt_31; }
	inline int32_t* get_address_of_scollCnt_31() { return &___scollCnt_31; }
	inline void set_scollCnt_31(int32_t value)
	{
		___scollCnt_31 = value;
	}

	inline static int32_t get_offset_of_mobilmoRows_32() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___mobilmoRows_32)); }
	inline float get_mobilmoRows_32() const { return ___mobilmoRows_32; }
	inline float* get_address_of_mobilmoRows_32() { return &___mobilmoRows_32; }
	inline void set_mobilmoRows_32(float value)
	{
		___mobilmoRows_32 = value;
	}

	inline static int32_t get_offset_of_mobilmo_33() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___mobilmo_33)); }
	inline GameObject_t1756533147 * get_mobilmo_33() const { return ___mobilmo_33; }
	inline GameObject_t1756533147 ** get_address_of_mobilmo_33() { return &___mobilmo_33; }
	inline void set_mobilmo_33(GameObject_t1756533147 * value)
	{
		___mobilmo_33 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmo_33, value);
	}

	inline static int32_t get_offset_of_childPartsObj_34() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___childPartsObj_34)); }
	inline GameObject_t1756533147 * get_childPartsObj_34() const { return ___childPartsObj_34; }
	inline GameObject_t1756533147 ** get_address_of_childPartsObj_34() { return &___childPartsObj_34; }
	inline void set_childPartsObj_34(GameObject_t1756533147 * value)
	{
		___childPartsObj_34 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsObj_34, value);
	}

	inline static int32_t get_offset_of_ChildCenter_35() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___ChildCenter_35)); }
	inline GameObject_t1756533147 * get_ChildCenter_35() const { return ___ChildCenter_35; }
	inline GameObject_t1756533147 ** get_address_of_ChildCenter_35() { return &___ChildCenter_35; }
	inline void set_ChildCenter_35(GameObject_t1756533147 * value)
	{
		___ChildCenter_35 = value;
		Il2CppCodeGenWriteBarrier(&___ChildCenter_35, value);
	}

	inline static int32_t get_offset_of_ChildLeap_36() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___ChildLeap_36)); }
	inline GameObject_t1756533147 * get_ChildLeap_36() const { return ___ChildLeap_36; }
	inline GameObject_t1756533147 ** get_address_of_ChildLeap_36() { return &___ChildLeap_36; }
	inline void set_ChildLeap_36(GameObject_t1756533147 * value)
	{
		___ChildLeap_36 = value;
		Il2CppCodeGenWriteBarrier(&___ChildLeap_36, value);
	}

	inline static int32_t get_offset_of_dnaModuleObj_37() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___dnaModuleObj_37)); }
	inline GameObject_t1756533147 * get_dnaModuleObj_37() const { return ___dnaModuleObj_37; }
	inline GameObject_t1756533147 ** get_address_of_dnaModuleObj_37() { return &___dnaModuleObj_37; }
	inline void set_dnaModuleObj_37(GameObject_t1756533147 * value)
	{
		___dnaModuleObj_37 = value;
		Il2CppCodeGenWriteBarrier(&___dnaModuleObj_37, value);
	}

	inline static int32_t get_offset_of_ModuleCenter_38() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___ModuleCenter_38)); }
	inline GameObject_t1756533147 * get_ModuleCenter_38() const { return ___ModuleCenter_38; }
	inline GameObject_t1756533147 ** get_address_of_ModuleCenter_38() { return &___ModuleCenter_38; }
	inline void set_ModuleCenter_38(GameObject_t1756533147 * value)
	{
		___ModuleCenter_38 = value;
		Il2CppCodeGenWriteBarrier(&___ModuleCenter_38, value);
	}

	inline static int32_t get_offset_of_cnt_39() { return static_cast<int32_t>(offsetof(MobilmoDnaController_t1006271200, ___cnt_39)); }
	inline int32_t get_cnt_39() const { return ___cnt_39; }
	inline int32_t* get_address_of_cnt_39() { return &___cnt_39; }
	inline void set_cnt_39(int32_t value)
	{
		___cnt_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
