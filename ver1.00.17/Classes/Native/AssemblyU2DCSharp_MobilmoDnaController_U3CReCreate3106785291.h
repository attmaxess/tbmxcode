﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"

// System.String
struct String_t;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t3881993182;
// Module[]
struct ModuleU5BU5D_t2077132997;
// MobilmoDnaController
struct MobilmoDnaController_t1006271200;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0
struct  U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291  : public Il2CppObject
{
public:
	// System.String MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::mobilmoId
	String_t* ___mobilmoId_0;
	// UnityEngine.Rigidbody[] MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$locvar0
	RigidbodyU5BU5D_t3881993182* ___U24locvar0_1;
	// System.Int32 MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.Collections.Generic.List`1/Enumerator<System.Int32> MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$locvar2
	Enumerator_t975728254  ___U24locvar2_3;
	// System.String MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::<resEmblemSt>__0
	String_t* ___U3CresEmblemStU3E__0_4;
	// Module[] MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::<modules>__0
	ModuleU5BU5D_t2077132997* ___U3CmodulesU3E__0_5;
	// Module[] MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$locvar3
	ModuleU5BU5D_t2077132997* ___U24locvar3_6;
	// System.Int32 MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$locvar4
	int32_t ___U24locvar4_7;
	// Module[] MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$locvar5
	ModuleU5BU5D_t2077132997* ___U24locvar5_8;
	// System.Int32 MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$locvar6
	int32_t ___U24locvar6_9;
	// MobilmoDnaController MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$this
	MobilmoDnaController_t1006271200 * ___U24this_10;
	// System.Object MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$current
	Il2CppObject * ___U24current_11;
	// System.Boolean MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$disposing
	bool ___U24disposing_12;
	// System.Int32 MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_mobilmoId_0() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___mobilmoId_0)); }
	inline String_t* get_mobilmoId_0() const { return ___mobilmoId_0; }
	inline String_t** get_address_of_mobilmoId_0() { return &___mobilmoId_0; }
	inline void set_mobilmoId_0(String_t* value)
	{
		___mobilmoId_0 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoId_0, value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24locvar0_1)); }
	inline RigidbodyU5BU5D_t3881993182* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RigidbodyU5BU5D_t3881993182** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RigidbodyU5BU5D_t3881993182* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_3() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24locvar2_3)); }
	inline Enumerator_t975728254  get_U24locvar2_3() const { return ___U24locvar2_3; }
	inline Enumerator_t975728254 * get_address_of_U24locvar2_3() { return &___U24locvar2_3; }
	inline void set_U24locvar2_3(Enumerator_t975728254  value)
	{
		___U24locvar2_3 = value;
	}

	inline static int32_t get_offset_of_U3CresEmblemStU3E__0_4() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U3CresEmblemStU3E__0_4)); }
	inline String_t* get_U3CresEmblemStU3E__0_4() const { return ___U3CresEmblemStU3E__0_4; }
	inline String_t** get_address_of_U3CresEmblemStU3E__0_4() { return &___U3CresEmblemStU3E__0_4; }
	inline void set_U3CresEmblemStU3E__0_4(String_t* value)
	{
		___U3CresEmblemStU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresEmblemStU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U3CmodulesU3E__0_5() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U3CmodulesU3E__0_5)); }
	inline ModuleU5BU5D_t2077132997* get_U3CmodulesU3E__0_5() const { return ___U3CmodulesU3E__0_5; }
	inline ModuleU5BU5D_t2077132997** get_address_of_U3CmodulesU3E__0_5() { return &___U3CmodulesU3E__0_5; }
	inline void set_U3CmodulesU3E__0_5(ModuleU5BU5D_t2077132997* value)
	{
		___U3CmodulesU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmodulesU3E__0_5, value);
	}

	inline static int32_t get_offset_of_U24locvar3_6() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24locvar3_6)); }
	inline ModuleU5BU5D_t2077132997* get_U24locvar3_6() const { return ___U24locvar3_6; }
	inline ModuleU5BU5D_t2077132997** get_address_of_U24locvar3_6() { return &___U24locvar3_6; }
	inline void set_U24locvar3_6(ModuleU5BU5D_t2077132997* value)
	{
		___U24locvar3_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar3_6, value);
	}

	inline static int32_t get_offset_of_U24locvar4_7() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24locvar4_7)); }
	inline int32_t get_U24locvar4_7() const { return ___U24locvar4_7; }
	inline int32_t* get_address_of_U24locvar4_7() { return &___U24locvar4_7; }
	inline void set_U24locvar4_7(int32_t value)
	{
		___U24locvar4_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar5_8() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24locvar5_8)); }
	inline ModuleU5BU5D_t2077132997* get_U24locvar5_8() const { return ___U24locvar5_8; }
	inline ModuleU5BU5D_t2077132997** get_address_of_U24locvar5_8() { return &___U24locvar5_8; }
	inline void set_U24locvar5_8(ModuleU5BU5D_t2077132997* value)
	{
		___U24locvar5_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar5_8, value);
	}

	inline static int32_t get_offset_of_U24locvar6_9() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24locvar6_9)); }
	inline int32_t get_U24locvar6_9() const { return ___U24locvar6_9; }
	inline int32_t* get_address_of_U24locvar6_9() { return &___U24locvar6_9; }
	inline void set_U24locvar6_9(int32_t value)
	{
		___U24locvar6_9 = value;
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24this_10)); }
	inline MobilmoDnaController_t1006271200 * get_U24this_10() const { return ___U24this_10; }
	inline MobilmoDnaController_t1006271200 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(MobilmoDnaController_t1006271200 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_10, value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
