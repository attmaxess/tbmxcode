﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// MobilmoDnaController
struct MobilmoDnaController_t1006271200;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoDnaController/<ReCreateMobilmoInRader>c__AnonStorey2
struct  U3CReCreateMobilmoInRaderU3Ec__AnonStorey2_t3664355197  : public Il2CppObject
{
public:
	// UnityEngine.GameObject MobilmoDnaController/<ReCreateMobilmoInRader>c__AnonStorey2::modObj
	GameObject_t1756533147 * ___modObj_0;
	// MobilmoDnaController MobilmoDnaController/<ReCreateMobilmoInRader>c__AnonStorey2::$this
	MobilmoDnaController_t1006271200 * ___U24this_1;

public:
	inline static int32_t get_offset_of_modObj_0() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInRaderU3Ec__AnonStorey2_t3664355197, ___modObj_0)); }
	inline GameObject_t1756533147 * get_modObj_0() const { return ___modObj_0; }
	inline GameObject_t1756533147 ** get_address_of_modObj_0() { return &___modObj_0; }
	inline void set_modObj_0(GameObject_t1756533147 * value)
	{
		___modObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___modObj_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInRaderU3Ec__AnonStorey2_t3664355197, ___U24this_1)); }
	inline MobilmoDnaController_t1006271200 * get_U24this_1() const { return ___U24this_1; }
	inline MobilmoDnaController_t1006271200 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MobilmoDnaController_t1006271200 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
