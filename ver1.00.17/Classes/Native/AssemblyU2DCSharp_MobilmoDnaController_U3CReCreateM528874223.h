﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0
struct U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0/<ReCreateMobilmoInWorld>c__AnonStorey3
struct  U3CReCreateMobilmoInWorldU3Ec__AnonStorey3_t528874223  : public Il2CppObject
{
public:
	// UnityEngine.GameObject MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0/<ReCreateMobilmoInWorld>c__AnonStorey3::modObj
	GameObject_t1756533147 * ___modObj_0;
	// MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0 MobilmoDnaController/<ReCreateMobilmoInWorld>c__Iterator0/<ReCreateMobilmoInWorld>c__AnonStorey3::<>f__ref$0
	U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_modObj_0() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__AnonStorey3_t528874223, ___modObj_0)); }
	inline GameObject_t1756533147 * get_modObj_0() const { return ___modObj_0; }
	inline GameObject_t1756533147 ** get_address_of_modObj_0() { return &___modObj_0; }
	inline void set_modObj_0(GameObject_t1756533147 * value)
	{
		___modObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___modObj_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CReCreateMobilmoInWorldU3Ec__AnonStorey3_t528874223, ___U3CU3Ef__refU240_1)); }
	inline U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CReCreateMobilmoInWorldU3Ec__Iterator0_t3106785291 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
