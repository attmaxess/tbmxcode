﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3599699288.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1984491874.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetMyMobilityModel
struct GetMyMobilityModel_t2067367768;
// MyMobilityItemsModel
struct MyMobilityItemsModel_t400881186;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// GetMyMobilityChildPartsListModel
struct GetMyMobilityChildPartsListModel_t3080641068;
// ModuleManager
struct ModuleManager_t1065445307;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// Parts[]
struct PartsU5BU5D_t3272295067;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// Joint[]
struct JointU5BU5D_t171503857;
// MobilmoDnaController
struct MobilmoDnaController_t1006271200;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1
struct  U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884  : public Il2CppObject
{
public:
	// System.String MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::json
	String_t* ___json_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_1;
	// GetMyMobilityModel MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<_getMyMobilityModel>__0
	GetMyMobilityModel_t2067367768 * ___U3C_getMyMobilityModelU3E__0_2;
	// System.Collections.Generic.List`1/Enumerator<MyMobilityItemsModel> MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$locvar0
	Enumerator_t3599699288  ___U24locvar0_3;
	// MyMobilityItemsModel MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<itemModel>__1
	MyMobilityItemsModel_t400881186 * ___U3CitemModelU3E__1_4;
	// UnityEngine.GameObject MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<mobilmo>__2
	GameObject_t1756533147 * ___U3CmobilmoU3E__2_5;
	// UnityEngine.GameObject MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<ChildObj>__2
	GameObject_t1756533147 * ___U3CChildObjU3E__2_6;
	// UnityEngine.GameObject MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<m_CreObj>__2
	GameObject_t1756533147 * ___U3Cm_CreObjU3E__2_7;
	// UnityEngine.Material MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<coreMat>__2
	Material_t193706927 * ___U3CcoreMatU3E__2_8;
	// System.Collections.Generic.List`1/Enumerator<GetMyMobilityChildPartsListModel> MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$locvar1
	Enumerator_t1984491874  ___U24locvar1_9;
	// GetMyMobilityChildPartsListModel MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<child>__3
	GetMyMobilityChildPartsListModel_t3080641068 * ___U3CchildU3E__3_10;
	// UnityEngine.GameObject MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<ModuleLeap>__4
	GameObject_t1756533147 * ___U3CModuleLeapU3E__4_11;
	// ModuleManager MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<moduleMgr>__4
	ModuleManager_t1065445307 * ___U3CmoduleMgrU3E__4_12;
	// System.String MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<moduleJson>__4
	String_t* ___U3CmoduleJsonU3E__4_13;
	// System.Int32 MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<parentPartsJointNo>__4
	int32_t ___U3CparentPartsJointNoU3E__4_14;
	// System.Int32 MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<modulePartsCnt>__4
	int32_t ___U3CmodulePartsCntU3E__4_15;
	// System.String[] MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<partsIdIndex>__4
	StringU5BU5D_t1642385972* ___U3CpartsIdIndexU3E__4_16;
	// System.Collections.Generic.List`1<System.String> MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<partsIdList>__4
	List_1_t1398341365 * ___U3CpartsIdListU3E__4_17;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<modulePartsList>__4
	List_1_t1125654279 * ___U3CmodulePartsListU3E__4_18;
	// Parts[] MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$locvar6
	PartsU5BU5D_t3272295067* ___U24locvar6_19;
	// System.Int32 MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$locvar7
	int32_t ___U24locvar7_20;
	// UnityEngine.Transform[] MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$locvar8
	TransformU5BU5D_t3764228911* ___U24locvar8_21;
	// System.Int32 MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$locvar9
	int32_t ___U24locvar9_22;
	// Joint[] MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<jointParents>__2
	JointU5BU5D_t171503857* ___U3CjointParentsU3E__2_23;
	// MobilmoDnaController MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$this
	MobilmoDnaController_t1006271200 * ___U24this_24;
	// System.Object MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$current
	Il2CppObject * ___U24current_25;
	// System.Boolean MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$disposing
	bool ___U24disposing_26;
	// System.Int32 MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::$PC
	int32_t ___U24PC_27;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___json_0)); }
	inline String_t* get_json_0() const { return ___json_0; }
	inline String_t** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(String_t* value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier(&___json_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CwwwDataU3E__0_1)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_1() const { return ___U3CwwwDataU3E__0_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_1() { return &___U3CwwwDataU3E__0_1; }
	inline void set_U3CwwwDataU3E__0_1(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3C_getMyMobilityModelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3C_getMyMobilityModelU3E__0_2)); }
	inline GetMyMobilityModel_t2067367768 * get_U3C_getMyMobilityModelU3E__0_2() const { return ___U3C_getMyMobilityModelU3E__0_2; }
	inline GetMyMobilityModel_t2067367768 ** get_address_of_U3C_getMyMobilityModelU3E__0_2() { return &___U3C_getMyMobilityModelU3E__0_2; }
	inline void set_U3C_getMyMobilityModelU3E__0_2(GetMyMobilityModel_t2067367768 * value)
	{
		___U3C_getMyMobilityModelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getMyMobilityModelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24locvar0_3)); }
	inline Enumerator_t3599699288  get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline Enumerator_t3599699288 * get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(Enumerator_t3599699288  value)
	{
		___U24locvar0_3 = value;
	}

	inline static int32_t get_offset_of_U3CitemModelU3E__1_4() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CitemModelU3E__1_4)); }
	inline MyMobilityItemsModel_t400881186 * get_U3CitemModelU3E__1_4() const { return ___U3CitemModelU3E__1_4; }
	inline MyMobilityItemsModel_t400881186 ** get_address_of_U3CitemModelU3E__1_4() { return &___U3CitemModelU3E__1_4; }
	inline void set_U3CitemModelU3E__1_4(MyMobilityItemsModel_t400881186 * value)
	{
		___U3CitemModelU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemModelU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U3CmobilmoU3E__2_5() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CmobilmoU3E__2_5)); }
	inline GameObject_t1756533147 * get_U3CmobilmoU3E__2_5() const { return ___U3CmobilmoU3E__2_5; }
	inline GameObject_t1756533147 ** get_address_of_U3CmobilmoU3E__2_5() { return &___U3CmobilmoU3E__2_5; }
	inline void set_U3CmobilmoU3E__2_5(GameObject_t1756533147 * value)
	{
		___U3CmobilmoU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilmoU3E__2_5, value);
	}

	inline static int32_t get_offset_of_U3CChildObjU3E__2_6() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CChildObjU3E__2_6)); }
	inline GameObject_t1756533147 * get_U3CChildObjU3E__2_6() const { return ___U3CChildObjU3E__2_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CChildObjU3E__2_6() { return &___U3CChildObjU3E__2_6; }
	inline void set_U3CChildObjU3E__2_6(GameObject_t1756533147 * value)
	{
		___U3CChildObjU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChildObjU3E__2_6, value);
	}

	inline static int32_t get_offset_of_U3Cm_CreObjU3E__2_7() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3Cm_CreObjU3E__2_7)); }
	inline GameObject_t1756533147 * get_U3Cm_CreObjU3E__2_7() const { return ___U3Cm_CreObjU3E__2_7; }
	inline GameObject_t1756533147 ** get_address_of_U3Cm_CreObjU3E__2_7() { return &___U3Cm_CreObjU3E__2_7; }
	inline void set_U3Cm_CreObjU3E__2_7(GameObject_t1756533147 * value)
	{
		___U3Cm_CreObjU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_CreObjU3E__2_7, value);
	}

	inline static int32_t get_offset_of_U3CcoreMatU3E__2_8() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CcoreMatU3E__2_8)); }
	inline Material_t193706927 * get_U3CcoreMatU3E__2_8() const { return ___U3CcoreMatU3E__2_8; }
	inline Material_t193706927 ** get_address_of_U3CcoreMatU3E__2_8() { return &___U3CcoreMatU3E__2_8; }
	inline void set_U3CcoreMatU3E__2_8(Material_t193706927 * value)
	{
		___U3CcoreMatU3E__2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreMatU3E__2_8, value);
	}

	inline static int32_t get_offset_of_U24locvar1_9() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24locvar1_9)); }
	inline Enumerator_t1984491874  get_U24locvar1_9() const { return ___U24locvar1_9; }
	inline Enumerator_t1984491874 * get_address_of_U24locvar1_9() { return &___U24locvar1_9; }
	inline void set_U24locvar1_9(Enumerator_t1984491874  value)
	{
		___U24locvar1_9 = value;
	}

	inline static int32_t get_offset_of_U3CchildU3E__3_10() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CchildU3E__3_10)); }
	inline GetMyMobilityChildPartsListModel_t3080641068 * get_U3CchildU3E__3_10() const { return ___U3CchildU3E__3_10; }
	inline GetMyMobilityChildPartsListModel_t3080641068 ** get_address_of_U3CchildU3E__3_10() { return &___U3CchildU3E__3_10; }
	inline void set_U3CchildU3E__3_10(GetMyMobilityChildPartsListModel_t3080641068 * value)
	{
		___U3CchildU3E__3_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildU3E__3_10, value);
	}

	inline static int32_t get_offset_of_U3CModuleLeapU3E__4_11() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CModuleLeapU3E__4_11)); }
	inline GameObject_t1756533147 * get_U3CModuleLeapU3E__4_11() const { return ___U3CModuleLeapU3E__4_11; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleLeapU3E__4_11() { return &___U3CModuleLeapU3E__4_11; }
	inline void set_U3CModuleLeapU3E__4_11(GameObject_t1756533147 * value)
	{
		___U3CModuleLeapU3E__4_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleLeapU3E__4_11, value);
	}

	inline static int32_t get_offset_of_U3CmoduleMgrU3E__4_12() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CmoduleMgrU3E__4_12)); }
	inline ModuleManager_t1065445307 * get_U3CmoduleMgrU3E__4_12() const { return ___U3CmoduleMgrU3E__4_12; }
	inline ModuleManager_t1065445307 ** get_address_of_U3CmoduleMgrU3E__4_12() { return &___U3CmoduleMgrU3E__4_12; }
	inline void set_U3CmoduleMgrU3E__4_12(ModuleManager_t1065445307 * value)
	{
		___U3CmoduleMgrU3E__4_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleMgrU3E__4_12, value);
	}

	inline static int32_t get_offset_of_U3CmoduleJsonU3E__4_13() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CmoduleJsonU3E__4_13)); }
	inline String_t* get_U3CmoduleJsonU3E__4_13() const { return ___U3CmoduleJsonU3E__4_13; }
	inline String_t** get_address_of_U3CmoduleJsonU3E__4_13() { return &___U3CmoduleJsonU3E__4_13; }
	inline void set_U3CmoduleJsonU3E__4_13(String_t* value)
	{
		___U3CmoduleJsonU3E__4_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleJsonU3E__4_13, value);
	}

	inline static int32_t get_offset_of_U3CparentPartsJointNoU3E__4_14() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CparentPartsJointNoU3E__4_14)); }
	inline int32_t get_U3CparentPartsJointNoU3E__4_14() const { return ___U3CparentPartsJointNoU3E__4_14; }
	inline int32_t* get_address_of_U3CparentPartsJointNoU3E__4_14() { return &___U3CparentPartsJointNoU3E__4_14; }
	inline void set_U3CparentPartsJointNoU3E__4_14(int32_t value)
	{
		___U3CparentPartsJointNoU3E__4_14 = value;
	}

	inline static int32_t get_offset_of_U3CmodulePartsCntU3E__4_15() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CmodulePartsCntU3E__4_15)); }
	inline int32_t get_U3CmodulePartsCntU3E__4_15() const { return ___U3CmodulePartsCntU3E__4_15; }
	inline int32_t* get_address_of_U3CmodulePartsCntU3E__4_15() { return &___U3CmodulePartsCntU3E__4_15; }
	inline void set_U3CmodulePartsCntU3E__4_15(int32_t value)
	{
		___U3CmodulePartsCntU3E__4_15 = value;
	}

	inline static int32_t get_offset_of_U3CpartsIdIndexU3E__4_16() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CpartsIdIndexU3E__4_16)); }
	inline StringU5BU5D_t1642385972* get_U3CpartsIdIndexU3E__4_16() const { return ___U3CpartsIdIndexU3E__4_16; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CpartsIdIndexU3E__4_16() { return &___U3CpartsIdIndexU3E__4_16; }
	inline void set_U3CpartsIdIndexU3E__4_16(StringU5BU5D_t1642385972* value)
	{
		___U3CpartsIdIndexU3E__4_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdIndexU3E__4_16, value);
	}

	inline static int32_t get_offset_of_U3CpartsIdListU3E__4_17() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CpartsIdListU3E__4_17)); }
	inline List_1_t1398341365 * get_U3CpartsIdListU3E__4_17() const { return ___U3CpartsIdListU3E__4_17; }
	inline List_1_t1398341365 ** get_address_of_U3CpartsIdListU3E__4_17() { return &___U3CpartsIdListU3E__4_17; }
	inline void set_U3CpartsIdListU3E__4_17(List_1_t1398341365 * value)
	{
		___U3CpartsIdListU3E__4_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdListU3E__4_17, value);
	}

	inline static int32_t get_offset_of_U3CmodulePartsListU3E__4_18() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CmodulePartsListU3E__4_18)); }
	inline List_1_t1125654279 * get_U3CmodulePartsListU3E__4_18() const { return ___U3CmodulePartsListU3E__4_18; }
	inline List_1_t1125654279 ** get_address_of_U3CmodulePartsListU3E__4_18() { return &___U3CmodulePartsListU3E__4_18; }
	inline void set_U3CmodulePartsListU3E__4_18(List_1_t1125654279 * value)
	{
		___U3CmodulePartsListU3E__4_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmodulePartsListU3E__4_18, value);
	}

	inline static int32_t get_offset_of_U24locvar6_19() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24locvar6_19)); }
	inline PartsU5BU5D_t3272295067* get_U24locvar6_19() const { return ___U24locvar6_19; }
	inline PartsU5BU5D_t3272295067** get_address_of_U24locvar6_19() { return &___U24locvar6_19; }
	inline void set_U24locvar6_19(PartsU5BU5D_t3272295067* value)
	{
		___U24locvar6_19 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar6_19, value);
	}

	inline static int32_t get_offset_of_U24locvar7_20() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24locvar7_20)); }
	inline int32_t get_U24locvar7_20() const { return ___U24locvar7_20; }
	inline int32_t* get_address_of_U24locvar7_20() { return &___U24locvar7_20; }
	inline void set_U24locvar7_20(int32_t value)
	{
		___U24locvar7_20 = value;
	}

	inline static int32_t get_offset_of_U24locvar8_21() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24locvar8_21)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar8_21() const { return ___U24locvar8_21; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar8_21() { return &___U24locvar8_21; }
	inline void set_U24locvar8_21(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar8_21 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar8_21, value);
	}

	inline static int32_t get_offset_of_U24locvar9_22() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24locvar9_22)); }
	inline int32_t get_U24locvar9_22() const { return ___U24locvar9_22; }
	inline int32_t* get_address_of_U24locvar9_22() { return &___U24locvar9_22; }
	inline void set_U24locvar9_22(int32_t value)
	{
		___U24locvar9_22 = value;
	}

	inline static int32_t get_offset_of_U3CjointParentsU3E__2_23() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U3CjointParentsU3E__2_23)); }
	inline JointU5BU5D_t171503857* get_U3CjointParentsU3E__2_23() const { return ___U3CjointParentsU3E__2_23; }
	inline JointU5BU5D_t171503857** get_address_of_U3CjointParentsU3E__2_23() { return &___U3CjointParentsU3E__2_23; }
	inline void set_U3CjointParentsU3E__2_23(JointU5BU5D_t171503857* value)
	{
		___U3CjointParentsU3E__2_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjointParentsU3E__2_23, value);
	}

	inline static int32_t get_offset_of_U24this_24() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24this_24)); }
	inline MobilmoDnaController_t1006271200 * get_U24this_24() const { return ___U24this_24; }
	inline MobilmoDnaController_t1006271200 ** get_address_of_U24this_24() { return &___U24this_24; }
	inline void set_U24this_24(MobilmoDnaController_t1006271200 * value)
	{
		___U24this_24 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_24, value);
	}

	inline static int32_t get_offset_of_U24current_25() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24current_25)); }
	inline Il2CppObject * get_U24current_25() const { return ___U24current_25; }
	inline Il2CppObject ** get_address_of_U24current_25() { return &___U24current_25; }
	inline void set_U24current_25(Il2CppObject * value)
	{
		___U24current_25 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_25, value);
	}

	inline static int32_t get_offset_of_U24disposing_26() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24disposing_26)); }
	inline bool get_U24disposing_26() const { return ___U24disposing_26; }
	inline bool* get_address_of_U24disposing_26() { return &___U24disposing_26; }
	inline void set_U24disposing_26(bool value)
	{
		___U24disposing_26 = value;
	}

	inline static int32_t get_offset_of_U24PC_27() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884, ___U24PC_27)); }
	inline int32_t get_U24PC_27() const { return ___U24PC_27; }
	inline int32_t* get_address_of_U24PC_27() { return &___U24PC_27; }
	inline void set_U24PC_27(int32_t value)
	{
		___U24PC_27 = value;
	}
};

struct U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884_StaticFields
{
public:
	// System.Func`1<System.Boolean> MobilmoDnaController/<reCreateDnaMobilmo>c__Iterator1::<>f__am$cache0
	Func_1_t1485000104 * ___U3CU3Ef__amU24cache0_28;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_28() { return static_cast<int32_t>(offsetof(U3CreCreateDnaMobilmoU3Ec__Iterator1_t294057884_StaticFields, ___U3CU3Ef__amU24cache0_28)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__amU24cache0_28() const { return ___U3CU3Ef__amU24cache0_28; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__amU24cache0_28() { return &___U3CU3Ef__amU24cache0_28; }
	inline void set_U3CU3Ef__amU24cache0_28(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__amU24cache0_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
