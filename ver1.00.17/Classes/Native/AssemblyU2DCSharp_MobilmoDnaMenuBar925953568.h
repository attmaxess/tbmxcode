﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoDnaMenuBar
struct  MobilmoDnaMenuBar_t925953568  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MobilmoDnaMenuBar::infoListPlayer
	GameObject_t1756533147 * ___infoListPlayer_2;
	// UnityEngine.GameObject MobilmoDnaMenuBar::infoReactionPrefab
	GameObject_t1756533147 * ___infoReactionPrefab_3;
	// UnityEngine.GameObject MobilmoDnaMenuBar::contentHolder
	GameObject_t1756533147 * ___contentHolder_4;
	// UnityEngine.Sprite[] MobilmoDnaMenuBar::reactionSprites
	SpriteU5BU5D_t3359083662* ___reactionSprites_5;
	// UnityEngine.Sprite[] MobilmoDnaMenuBar::rankSprites
	SpriteU5BU5D_t3359083662* ___rankSprites_6;
	// System.String MobilmoDnaMenuBar::userID
	String_t* ___userID_7;

public:
	inline static int32_t get_offset_of_infoListPlayer_2() { return static_cast<int32_t>(offsetof(MobilmoDnaMenuBar_t925953568, ___infoListPlayer_2)); }
	inline GameObject_t1756533147 * get_infoListPlayer_2() const { return ___infoListPlayer_2; }
	inline GameObject_t1756533147 ** get_address_of_infoListPlayer_2() { return &___infoListPlayer_2; }
	inline void set_infoListPlayer_2(GameObject_t1756533147 * value)
	{
		___infoListPlayer_2 = value;
		Il2CppCodeGenWriteBarrier(&___infoListPlayer_2, value);
	}

	inline static int32_t get_offset_of_infoReactionPrefab_3() { return static_cast<int32_t>(offsetof(MobilmoDnaMenuBar_t925953568, ___infoReactionPrefab_3)); }
	inline GameObject_t1756533147 * get_infoReactionPrefab_3() const { return ___infoReactionPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_infoReactionPrefab_3() { return &___infoReactionPrefab_3; }
	inline void set_infoReactionPrefab_3(GameObject_t1756533147 * value)
	{
		___infoReactionPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___infoReactionPrefab_3, value);
	}

	inline static int32_t get_offset_of_contentHolder_4() { return static_cast<int32_t>(offsetof(MobilmoDnaMenuBar_t925953568, ___contentHolder_4)); }
	inline GameObject_t1756533147 * get_contentHolder_4() const { return ___contentHolder_4; }
	inline GameObject_t1756533147 ** get_address_of_contentHolder_4() { return &___contentHolder_4; }
	inline void set_contentHolder_4(GameObject_t1756533147 * value)
	{
		___contentHolder_4 = value;
		Il2CppCodeGenWriteBarrier(&___contentHolder_4, value);
	}

	inline static int32_t get_offset_of_reactionSprites_5() { return static_cast<int32_t>(offsetof(MobilmoDnaMenuBar_t925953568, ___reactionSprites_5)); }
	inline SpriteU5BU5D_t3359083662* get_reactionSprites_5() const { return ___reactionSprites_5; }
	inline SpriteU5BU5D_t3359083662** get_address_of_reactionSprites_5() { return &___reactionSprites_5; }
	inline void set_reactionSprites_5(SpriteU5BU5D_t3359083662* value)
	{
		___reactionSprites_5 = value;
		Il2CppCodeGenWriteBarrier(&___reactionSprites_5, value);
	}

	inline static int32_t get_offset_of_rankSprites_6() { return static_cast<int32_t>(offsetof(MobilmoDnaMenuBar_t925953568, ___rankSprites_6)); }
	inline SpriteU5BU5D_t3359083662* get_rankSprites_6() const { return ___rankSprites_6; }
	inline SpriteU5BU5D_t3359083662** get_address_of_rankSprites_6() { return &___rankSprites_6; }
	inline void set_rankSprites_6(SpriteU5BU5D_t3359083662* value)
	{
		___rankSprites_6 = value;
		Il2CppCodeGenWriteBarrier(&___rankSprites_6, value);
	}

	inline static int32_t get_offset_of_userID_7() { return static_cast<int32_t>(offsetof(MobilmoDnaMenuBar_t925953568, ___userID_7)); }
	inline String_t* get_userID_7() const { return ___userID_7; }
	inline String_t** get_address_of_userID_7() { return &___userID_7; }
	inline void set_userID_7(String_t* value)
	{
		___userID_7 = value;
		Il2CppCodeGenWriteBarrier(&___userID_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
