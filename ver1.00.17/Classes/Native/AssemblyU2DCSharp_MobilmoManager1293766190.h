﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen4285409821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// Mobilmo
struct Mobilmo_t370754809;
// EditMobilmo
struct EditMobilmo_t2535788613;
// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoManager
struct  MobilmoManager_t1293766190  : public SingletonMonoBehaviour_1_t4285409821
{
public:
	// System.String MobilmoManager::mobilmoJson
	String_t* ___mobilmoJson_3;
	// System.String MobilmoManager::myMobilmoJson
	String_t* ___myMobilmoJson_4;
	// UnityEngine.GameObject MobilmoManager::recreatedModuleObj
	GameObject_t1756533147 * ___recreatedModuleObj_5;
	// UnityEngine.GameObject MobilmoManager::recoredModuleObj
	GameObject_t1756533147 * ___recoredModuleObj_6;
	// UnityEngine.GameObject MobilmoManager::worldMobilmoObj
	GameObject_t1756533147 * ___worldMobilmoObj_7;
	// System.String MobilmoManager::testJson
	String_t* ___testJson_8;
	// System.Int32 MobilmoManager::recMobilityId
	int32_t ___recMobilityId_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MobilmoManager::nearmobObjList
	List_1_t1125654279 * ___nearmobObjList_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MobilmoManager::deleteModuleList
	List_1_t1125654279 * ___deleteModuleList_11;
	// Mobilmo MobilmoManager::recMobilmo
	Mobilmo_t370754809 * ___recMobilmo_12;
	// System.Int32 MobilmoManager::m_moduleCount
	int32_t ___m_moduleCount_13;
	// System.Int32 MobilmoManager::m_recModuleCount
	int32_t ___m_recModuleCount_14;
	// UnityEngine.Vector3 MobilmoManager::moduleScale
	Vector3_t2243707580  ___moduleScale_15;
	// System.Single MobilmoManager::reRecoredTimer
	float ___reRecoredTimer_16;
	// System.Int32 MobilmoManager::m_iPartsCount
	int32_t ___m_iPartsCount_17;
	// System.Boolean MobilmoManager::isTouchRecoredObject
	bool ___isTouchRecoredObject_18;
	// EditMobilmo MobilmoManager::m_EditMobilmo
	EditMobilmo_t2535788613 * ___m_EditMobilmo_19;
	// System.Int32 MobilmoManager::reModCnt
	int32_t ___reModCnt_20;
	// System.Int32 MobilmoManager::mobilityCnt
	int32_t ___mobilityCnt_21;
	// UnityEngine.GameObject MobilmoManager::mobilmo
	GameObject_t1756533147 * ___mobilmo_22;
	// UnityEngine.GameObject MobilmoManager::childPartsObj
	GameObject_t1756533147 * ___childPartsObj_23;
	// UnityEngine.GameObject MobilmoManager::ChildCenter
	GameObject_t1756533147 * ___ChildCenter_24;
	// UnityEngine.GameObject MobilmoManager::ChildLeap
	GameObject_t1756533147 * ___ChildLeap_25;
	// UnityEngine.GameObject MobilmoManager::ChildRoll
	GameObject_t1756533147 * ___ChildRoll_26;
	// UnityEngine.GameObject MobilmoManager::ModuleCenter
	GameObject_t1756533147 * ___ModuleCenter_27;
	// System.Collections.ArrayList MobilmoManager::partsNameAL
	ArrayList_t4252133567 * ___partsNameAL_28;
	// System.Int32 MobilmoManager::m_mobilmoCount
	int32_t ___m_mobilmoCount_29;
	// UnityEngine.GameObject MobilmoManager::myMobilmoArea
	GameObject_t1756533147 * ___myMobilmoArea_30;
	// System.Int32 MobilmoManager::recordingPoseDataCount
	int32_t ___recordingPoseDataCount_31;
	// UnityEngine.GameObject MobilmoManager::recordedChildPartsObj
	GameObject_t1756533147 * ___recordedChildPartsObj_32;
	// System.Boolean MobilmoManager::isRecreateRecoredObjEnd
	bool ___isRecreateRecoredObjEnd_33;

public:
	inline static int32_t get_offset_of_mobilmoJson_3() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___mobilmoJson_3)); }
	inline String_t* get_mobilmoJson_3() const { return ___mobilmoJson_3; }
	inline String_t** get_address_of_mobilmoJson_3() { return &___mobilmoJson_3; }
	inline void set_mobilmoJson_3(String_t* value)
	{
		___mobilmoJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoJson_3, value);
	}

	inline static int32_t get_offset_of_myMobilmoJson_4() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___myMobilmoJson_4)); }
	inline String_t* get_myMobilmoJson_4() const { return ___myMobilmoJson_4; }
	inline String_t** get_address_of_myMobilmoJson_4() { return &___myMobilmoJson_4; }
	inline void set_myMobilmoJson_4(String_t* value)
	{
		___myMobilmoJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___myMobilmoJson_4, value);
	}

	inline static int32_t get_offset_of_recreatedModuleObj_5() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___recreatedModuleObj_5)); }
	inline GameObject_t1756533147 * get_recreatedModuleObj_5() const { return ___recreatedModuleObj_5; }
	inline GameObject_t1756533147 ** get_address_of_recreatedModuleObj_5() { return &___recreatedModuleObj_5; }
	inline void set_recreatedModuleObj_5(GameObject_t1756533147 * value)
	{
		___recreatedModuleObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___recreatedModuleObj_5, value);
	}

	inline static int32_t get_offset_of_recoredModuleObj_6() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___recoredModuleObj_6)); }
	inline GameObject_t1756533147 * get_recoredModuleObj_6() const { return ___recoredModuleObj_6; }
	inline GameObject_t1756533147 ** get_address_of_recoredModuleObj_6() { return &___recoredModuleObj_6; }
	inline void set_recoredModuleObj_6(GameObject_t1756533147 * value)
	{
		___recoredModuleObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___recoredModuleObj_6, value);
	}

	inline static int32_t get_offset_of_worldMobilmoObj_7() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___worldMobilmoObj_7)); }
	inline GameObject_t1756533147 * get_worldMobilmoObj_7() const { return ___worldMobilmoObj_7; }
	inline GameObject_t1756533147 ** get_address_of_worldMobilmoObj_7() { return &___worldMobilmoObj_7; }
	inline void set_worldMobilmoObj_7(GameObject_t1756533147 * value)
	{
		___worldMobilmoObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___worldMobilmoObj_7, value);
	}

	inline static int32_t get_offset_of_testJson_8() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___testJson_8)); }
	inline String_t* get_testJson_8() const { return ___testJson_8; }
	inline String_t** get_address_of_testJson_8() { return &___testJson_8; }
	inline void set_testJson_8(String_t* value)
	{
		___testJson_8 = value;
		Il2CppCodeGenWriteBarrier(&___testJson_8, value);
	}

	inline static int32_t get_offset_of_recMobilityId_9() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___recMobilityId_9)); }
	inline int32_t get_recMobilityId_9() const { return ___recMobilityId_9; }
	inline int32_t* get_address_of_recMobilityId_9() { return &___recMobilityId_9; }
	inline void set_recMobilityId_9(int32_t value)
	{
		___recMobilityId_9 = value;
	}

	inline static int32_t get_offset_of_nearmobObjList_10() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___nearmobObjList_10)); }
	inline List_1_t1125654279 * get_nearmobObjList_10() const { return ___nearmobObjList_10; }
	inline List_1_t1125654279 ** get_address_of_nearmobObjList_10() { return &___nearmobObjList_10; }
	inline void set_nearmobObjList_10(List_1_t1125654279 * value)
	{
		___nearmobObjList_10 = value;
		Il2CppCodeGenWriteBarrier(&___nearmobObjList_10, value);
	}

	inline static int32_t get_offset_of_deleteModuleList_11() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___deleteModuleList_11)); }
	inline List_1_t1125654279 * get_deleteModuleList_11() const { return ___deleteModuleList_11; }
	inline List_1_t1125654279 ** get_address_of_deleteModuleList_11() { return &___deleteModuleList_11; }
	inline void set_deleteModuleList_11(List_1_t1125654279 * value)
	{
		___deleteModuleList_11 = value;
		Il2CppCodeGenWriteBarrier(&___deleteModuleList_11, value);
	}

	inline static int32_t get_offset_of_recMobilmo_12() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___recMobilmo_12)); }
	inline Mobilmo_t370754809 * get_recMobilmo_12() const { return ___recMobilmo_12; }
	inline Mobilmo_t370754809 ** get_address_of_recMobilmo_12() { return &___recMobilmo_12; }
	inline void set_recMobilmo_12(Mobilmo_t370754809 * value)
	{
		___recMobilmo_12 = value;
		Il2CppCodeGenWriteBarrier(&___recMobilmo_12, value);
	}

	inline static int32_t get_offset_of_m_moduleCount_13() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___m_moduleCount_13)); }
	inline int32_t get_m_moduleCount_13() const { return ___m_moduleCount_13; }
	inline int32_t* get_address_of_m_moduleCount_13() { return &___m_moduleCount_13; }
	inline void set_m_moduleCount_13(int32_t value)
	{
		___m_moduleCount_13 = value;
	}

	inline static int32_t get_offset_of_m_recModuleCount_14() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___m_recModuleCount_14)); }
	inline int32_t get_m_recModuleCount_14() const { return ___m_recModuleCount_14; }
	inline int32_t* get_address_of_m_recModuleCount_14() { return &___m_recModuleCount_14; }
	inline void set_m_recModuleCount_14(int32_t value)
	{
		___m_recModuleCount_14 = value;
	}

	inline static int32_t get_offset_of_moduleScale_15() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___moduleScale_15)); }
	inline Vector3_t2243707580  get_moduleScale_15() const { return ___moduleScale_15; }
	inline Vector3_t2243707580 * get_address_of_moduleScale_15() { return &___moduleScale_15; }
	inline void set_moduleScale_15(Vector3_t2243707580  value)
	{
		___moduleScale_15 = value;
	}

	inline static int32_t get_offset_of_reRecoredTimer_16() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___reRecoredTimer_16)); }
	inline float get_reRecoredTimer_16() const { return ___reRecoredTimer_16; }
	inline float* get_address_of_reRecoredTimer_16() { return &___reRecoredTimer_16; }
	inline void set_reRecoredTimer_16(float value)
	{
		___reRecoredTimer_16 = value;
	}

	inline static int32_t get_offset_of_m_iPartsCount_17() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___m_iPartsCount_17)); }
	inline int32_t get_m_iPartsCount_17() const { return ___m_iPartsCount_17; }
	inline int32_t* get_address_of_m_iPartsCount_17() { return &___m_iPartsCount_17; }
	inline void set_m_iPartsCount_17(int32_t value)
	{
		___m_iPartsCount_17 = value;
	}

	inline static int32_t get_offset_of_isTouchRecoredObject_18() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___isTouchRecoredObject_18)); }
	inline bool get_isTouchRecoredObject_18() const { return ___isTouchRecoredObject_18; }
	inline bool* get_address_of_isTouchRecoredObject_18() { return &___isTouchRecoredObject_18; }
	inline void set_isTouchRecoredObject_18(bool value)
	{
		___isTouchRecoredObject_18 = value;
	}

	inline static int32_t get_offset_of_m_EditMobilmo_19() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___m_EditMobilmo_19)); }
	inline EditMobilmo_t2535788613 * get_m_EditMobilmo_19() const { return ___m_EditMobilmo_19; }
	inline EditMobilmo_t2535788613 ** get_address_of_m_EditMobilmo_19() { return &___m_EditMobilmo_19; }
	inline void set_m_EditMobilmo_19(EditMobilmo_t2535788613 * value)
	{
		___m_EditMobilmo_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_EditMobilmo_19, value);
	}

	inline static int32_t get_offset_of_reModCnt_20() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___reModCnt_20)); }
	inline int32_t get_reModCnt_20() const { return ___reModCnt_20; }
	inline int32_t* get_address_of_reModCnt_20() { return &___reModCnt_20; }
	inline void set_reModCnt_20(int32_t value)
	{
		___reModCnt_20 = value;
	}

	inline static int32_t get_offset_of_mobilityCnt_21() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___mobilityCnt_21)); }
	inline int32_t get_mobilityCnt_21() const { return ___mobilityCnt_21; }
	inline int32_t* get_address_of_mobilityCnt_21() { return &___mobilityCnt_21; }
	inline void set_mobilityCnt_21(int32_t value)
	{
		___mobilityCnt_21 = value;
	}

	inline static int32_t get_offset_of_mobilmo_22() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___mobilmo_22)); }
	inline GameObject_t1756533147 * get_mobilmo_22() const { return ___mobilmo_22; }
	inline GameObject_t1756533147 ** get_address_of_mobilmo_22() { return &___mobilmo_22; }
	inline void set_mobilmo_22(GameObject_t1756533147 * value)
	{
		___mobilmo_22 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmo_22, value);
	}

	inline static int32_t get_offset_of_childPartsObj_23() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___childPartsObj_23)); }
	inline GameObject_t1756533147 * get_childPartsObj_23() const { return ___childPartsObj_23; }
	inline GameObject_t1756533147 ** get_address_of_childPartsObj_23() { return &___childPartsObj_23; }
	inline void set_childPartsObj_23(GameObject_t1756533147 * value)
	{
		___childPartsObj_23 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsObj_23, value);
	}

	inline static int32_t get_offset_of_ChildCenter_24() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___ChildCenter_24)); }
	inline GameObject_t1756533147 * get_ChildCenter_24() const { return ___ChildCenter_24; }
	inline GameObject_t1756533147 ** get_address_of_ChildCenter_24() { return &___ChildCenter_24; }
	inline void set_ChildCenter_24(GameObject_t1756533147 * value)
	{
		___ChildCenter_24 = value;
		Il2CppCodeGenWriteBarrier(&___ChildCenter_24, value);
	}

	inline static int32_t get_offset_of_ChildLeap_25() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___ChildLeap_25)); }
	inline GameObject_t1756533147 * get_ChildLeap_25() const { return ___ChildLeap_25; }
	inline GameObject_t1756533147 ** get_address_of_ChildLeap_25() { return &___ChildLeap_25; }
	inline void set_ChildLeap_25(GameObject_t1756533147 * value)
	{
		___ChildLeap_25 = value;
		Il2CppCodeGenWriteBarrier(&___ChildLeap_25, value);
	}

	inline static int32_t get_offset_of_ChildRoll_26() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___ChildRoll_26)); }
	inline GameObject_t1756533147 * get_ChildRoll_26() const { return ___ChildRoll_26; }
	inline GameObject_t1756533147 ** get_address_of_ChildRoll_26() { return &___ChildRoll_26; }
	inline void set_ChildRoll_26(GameObject_t1756533147 * value)
	{
		___ChildRoll_26 = value;
		Il2CppCodeGenWriteBarrier(&___ChildRoll_26, value);
	}

	inline static int32_t get_offset_of_ModuleCenter_27() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___ModuleCenter_27)); }
	inline GameObject_t1756533147 * get_ModuleCenter_27() const { return ___ModuleCenter_27; }
	inline GameObject_t1756533147 ** get_address_of_ModuleCenter_27() { return &___ModuleCenter_27; }
	inline void set_ModuleCenter_27(GameObject_t1756533147 * value)
	{
		___ModuleCenter_27 = value;
		Il2CppCodeGenWriteBarrier(&___ModuleCenter_27, value);
	}

	inline static int32_t get_offset_of_partsNameAL_28() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___partsNameAL_28)); }
	inline ArrayList_t4252133567 * get_partsNameAL_28() const { return ___partsNameAL_28; }
	inline ArrayList_t4252133567 ** get_address_of_partsNameAL_28() { return &___partsNameAL_28; }
	inline void set_partsNameAL_28(ArrayList_t4252133567 * value)
	{
		___partsNameAL_28 = value;
		Il2CppCodeGenWriteBarrier(&___partsNameAL_28, value);
	}

	inline static int32_t get_offset_of_m_mobilmoCount_29() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___m_mobilmoCount_29)); }
	inline int32_t get_m_mobilmoCount_29() const { return ___m_mobilmoCount_29; }
	inline int32_t* get_address_of_m_mobilmoCount_29() { return &___m_mobilmoCount_29; }
	inline void set_m_mobilmoCount_29(int32_t value)
	{
		___m_mobilmoCount_29 = value;
	}

	inline static int32_t get_offset_of_myMobilmoArea_30() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___myMobilmoArea_30)); }
	inline GameObject_t1756533147 * get_myMobilmoArea_30() const { return ___myMobilmoArea_30; }
	inline GameObject_t1756533147 ** get_address_of_myMobilmoArea_30() { return &___myMobilmoArea_30; }
	inline void set_myMobilmoArea_30(GameObject_t1756533147 * value)
	{
		___myMobilmoArea_30 = value;
		Il2CppCodeGenWriteBarrier(&___myMobilmoArea_30, value);
	}

	inline static int32_t get_offset_of_recordingPoseDataCount_31() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___recordingPoseDataCount_31)); }
	inline int32_t get_recordingPoseDataCount_31() const { return ___recordingPoseDataCount_31; }
	inline int32_t* get_address_of_recordingPoseDataCount_31() { return &___recordingPoseDataCount_31; }
	inline void set_recordingPoseDataCount_31(int32_t value)
	{
		___recordingPoseDataCount_31 = value;
	}

	inline static int32_t get_offset_of_recordedChildPartsObj_32() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___recordedChildPartsObj_32)); }
	inline GameObject_t1756533147 * get_recordedChildPartsObj_32() const { return ___recordedChildPartsObj_32; }
	inline GameObject_t1756533147 ** get_address_of_recordedChildPartsObj_32() { return &___recordedChildPartsObj_32; }
	inline void set_recordedChildPartsObj_32(GameObject_t1756533147 * value)
	{
		___recordedChildPartsObj_32 = value;
		Il2CppCodeGenWriteBarrier(&___recordedChildPartsObj_32, value);
	}

	inline static int32_t get_offset_of_isRecreateRecoredObjEnd_33() { return static_cast<int32_t>(offsetof(MobilmoManager_t1293766190, ___isRecreateRecoredObjEnd_33)); }
	inline bool get_isRecreateRecoredObjEnd_33() const { return ___isRecreateRecoredObjEnd_33; }
	inline bool* get_address_of_isRecreateRecoredObjEnd_33() { return &___isRecreateRecoredObjEnd_33; }
	inline void set_isRecreateRecoredObjEnd_33(bool value)
	{
		___isRecreateRecoredObjEnd_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
