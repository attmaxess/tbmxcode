﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1595687184.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetMobilityModel
struct GetMobilityModel_t1064189486;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// Mobilmo
struct Mobilmo_t370754809;
// UnityEngine.FixedJoint
struct FixedJoint_t3848069458;
// UnityEngine.Material
struct Material_t193706927;
// GetMobilityChildPartsListModel
struct GetMobilityChildPartsListModel_t2691836378;
// ModuleManager
struct ModuleManager_t1065445307;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// Joint[]
struct JointU5BU5D_t171503857;
// MobilmoManager
struct MobilmoManager_t1293766190;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3
struct  U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177  : public Il2CppObject
{
public:
	// System.String MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::json
	String_t* ___json_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_1;
	// GetMobilityModel MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<_getMobilityModel>__0
	GetMobilityModel_t1064189486 * ___U3C_getMobilityModelU3E__0_2;
	// UnityEngine.GameObject MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<mobilmo>__0
	GameObject_t1756533147 * ___U3CmobilmoU3E__0_3;
	// UnityEngine.Rigidbody MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<mobilmoRigid>__0
	Rigidbody_t4233889191 * ___U3CmobilmoRigidU3E__0_4;
	// Mobilmo MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<m_mobilimo>__0
	Mobilmo_t370754809 * ___U3Cm_mobilimoU3E__0_5;
	// UnityEngine.FixedJoint MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<mobilFixed>__0
	FixedJoint_t3848069458 * ___U3CmobilFixedU3E__0_6;
	// System.Collections.Generic.List`1/Enumerator<System.Object> MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar0
	Enumerator_t1593300101  ___U24locvar0_7;
	// UnityEngine.GameObject MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<coreChildObj>__0
	GameObject_t1756533147 * ___U3CcoreChildObjU3E__0_8;
	// UnityEngine.GameObject MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<m_CreObj>__0
	GameObject_t1756533147 * ___U3Cm_CreObjU3E__0_9;
	// UnityEngine.Material MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<coreMat>__0
	Material_t193706927 * ___U3CcoreMatU3E__0_10;
	// System.Collections.Generic.List`1/Enumerator<GetMobilityChildPartsListModel> MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar1
	Enumerator_t1595687184  ___U24locvar1_11;
	// GetMobilityChildPartsListModel MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<child>__1
	GetMobilityChildPartsListModel_t2691836378 * ___U3CchildU3E__1_12;
	// UnityEngine.GameObject MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<ModuleCenter>__2
	GameObject_t1756533147 * ___U3CModuleCenterU3E__2_13;
	// UnityEngine.GameObject MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<ModuleLeap>__2
	GameObject_t1756533147 * ___U3CModuleLeapU3E__2_14;
	// UnityEngine.GameObject MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<ModuleRoll>__2
	GameObject_t1756533147 * ___U3CModuleRollU3E__2_15;
	// ModuleManager MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<moduleMgr>__2
	ModuleManager_t1065445307 * ___U3CmoduleMgrU3E__2_16;
	// System.Int32 MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<parentPartsJointNo>__2
	int32_t ___U3CparentPartsJointNoU3E__2_17;
	// System.String MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<moduleJson>__2
	String_t* ___U3CmoduleJsonU3E__2_18;
	// System.Int32 MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<modulePartsCnt>__2
	int32_t ___U3CmodulePartsCntU3E__2_19;
	// System.String[] MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<partsIdIndex>__2
	StringU5BU5D_t1642385972* ___U3CpartsIdIndexU3E__2_20;
	// System.Collections.Generic.List`1<System.String> MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<partsIdList>__2
	List_1_t1398341365 * ___U3CpartsIdListU3E__2_21;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<modulePartsList>__2
	List_1_t1125654279 * ___U3CmodulePartsListU3E__2_22;
	// UnityEngine.Transform[] MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar6
	TransformU5BU5D_t3764228911* ___U24locvar6_23;
	// System.Int32 MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar7
	int32_t ___U24locvar7_24;
	// UnityEngine.Transform[] MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar8
	TransformU5BU5D_t3764228911* ___U24locvar8_25;
	// System.Int32 MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar9
	int32_t ___U24locvar9_26;
	// Joint[] MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<jointParents>__0
	JointU5BU5D_t171503857* ___U3CjointParentsU3E__0_27;
	// UnityEngine.Transform[] MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar12
	TransformU5BU5D_t3764228911* ___U24locvar12_28;
	// System.Int32 MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$locvar13
	int32_t ___U24locvar13_29;
	// MobilmoManager MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$this
	MobilmoManager_t1293766190 * ___U24this_30;
	// System.Object MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$current
	Il2CppObject * ___U24current_31;
	// System.Boolean MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$disposing
	bool ___U24disposing_32;
	// System.Int32 MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::$PC
	int32_t ___U24PC_33;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___json_0)); }
	inline String_t* get_json_0() const { return ___json_0; }
	inline String_t** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(String_t* value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier(&___json_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CwwwDataU3E__0_1)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_1() const { return ___U3CwwwDataU3E__0_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_1() { return &___U3CwwwDataU3E__0_1; }
	inline void set_U3CwwwDataU3E__0_1(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3C_getMobilityModelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3C_getMobilityModelU3E__0_2)); }
	inline GetMobilityModel_t1064189486 * get_U3C_getMobilityModelU3E__0_2() const { return ___U3C_getMobilityModelU3E__0_2; }
	inline GetMobilityModel_t1064189486 ** get_address_of_U3C_getMobilityModelU3E__0_2() { return &___U3C_getMobilityModelU3E__0_2; }
	inline void set_U3C_getMobilityModelU3E__0_2(GetMobilityModel_t1064189486 * value)
	{
		___U3C_getMobilityModelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getMobilityModelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CmobilmoU3E__0_3() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CmobilmoU3E__0_3)); }
	inline GameObject_t1756533147 * get_U3CmobilmoU3E__0_3() const { return ___U3CmobilmoU3E__0_3; }
	inline GameObject_t1756533147 ** get_address_of_U3CmobilmoU3E__0_3() { return &___U3CmobilmoU3E__0_3; }
	inline void set_U3CmobilmoU3E__0_3(GameObject_t1756533147 * value)
	{
		___U3CmobilmoU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilmoU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CmobilmoRigidU3E__0_4() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CmobilmoRigidU3E__0_4)); }
	inline Rigidbody_t4233889191 * get_U3CmobilmoRigidU3E__0_4() const { return ___U3CmobilmoRigidU3E__0_4; }
	inline Rigidbody_t4233889191 ** get_address_of_U3CmobilmoRigidU3E__0_4() { return &___U3CmobilmoRigidU3E__0_4; }
	inline void set_U3CmobilmoRigidU3E__0_4(Rigidbody_t4233889191 * value)
	{
		___U3CmobilmoRigidU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilmoRigidU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U3Cm_mobilimoU3E__0_5() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3Cm_mobilimoU3E__0_5)); }
	inline Mobilmo_t370754809 * get_U3Cm_mobilimoU3E__0_5() const { return ___U3Cm_mobilimoU3E__0_5; }
	inline Mobilmo_t370754809 ** get_address_of_U3Cm_mobilimoU3E__0_5() { return &___U3Cm_mobilimoU3E__0_5; }
	inline void set_U3Cm_mobilimoU3E__0_5(Mobilmo_t370754809 * value)
	{
		___U3Cm_mobilimoU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_mobilimoU3E__0_5, value);
	}

	inline static int32_t get_offset_of_U3CmobilFixedU3E__0_6() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CmobilFixedU3E__0_6)); }
	inline FixedJoint_t3848069458 * get_U3CmobilFixedU3E__0_6() const { return ___U3CmobilFixedU3E__0_6; }
	inline FixedJoint_t3848069458 ** get_address_of_U3CmobilFixedU3E__0_6() { return &___U3CmobilFixedU3E__0_6; }
	inline void set_U3CmobilFixedU3E__0_6(FixedJoint_t3848069458 * value)
	{
		___U3CmobilFixedU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilFixedU3E__0_6, value);
	}

	inline static int32_t get_offset_of_U24locvar0_7() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar0_7)); }
	inline Enumerator_t1593300101  get_U24locvar0_7() const { return ___U24locvar0_7; }
	inline Enumerator_t1593300101 * get_address_of_U24locvar0_7() { return &___U24locvar0_7; }
	inline void set_U24locvar0_7(Enumerator_t1593300101  value)
	{
		___U24locvar0_7 = value;
	}

	inline static int32_t get_offset_of_U3CcoreChildObjU3E__0_8() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CcoreChildObjU3E__0_8)); }
	inline GameObject_t1756533147 * get_U3CcoreChildObjU3E__0_8() const { return ___U3CcoreChildObjU3E__0_8; }
	inline GameObject_t1756533147 ** get_address_of_U3CcoreChildObjU3E__0_8() { return &___U3CcoreChildObjU3E__0_8; }
	inline void set_U3CcoreChildObjU3E__0_8(GameObject_t1756533147 * value)
	{
		___U3CcoreChildObjU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreChildObjU3E__0_8, value);
	}

	inline static int32_t get_offset_of_U3Cm_CreObjU3E__0_9() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3Cm_CreObjU3E__0_9)); }
	inline GameObject_t1756533147 * get_U3Cm_CreObjU3E__0_9() const { return ___U3Cm_CreObjU3E__0_9; }
	inline GameObject_t1756533147 ** get_address_of_U3Cm_CreObjU3E__0_9() { return &___U3Cm_CreObjU3E__0_9; }
	inline void set_U3Cm_CreObjU3E__0_9(GameObject_t1756533147 * value)
	{
		___U3Cm_CreObjU3E__0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_CreObjU3E__0_9, value);
	}

	inline static int32_t get_offset_of_U3CcoreMatU3E__0_10() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CcoreMatU3E__0_10)); }
	inline Material_t193706927 * get_U3CcoreMatU3E__0_10() const { return ___U3CcoreMatU3E__0_10; }
	inline Material_t193706927 ** get_address_of_U3CcoreMatU3E__0_10() { return &___U3CcoreMatU3E__0_10; }
	inline void set_U3CcoreMatU3E__0_10(Material_t193706927 * value)
	{
		___U3CcoreMatU3E__0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreMatU3E__0_10, value);
	}

	inline static int32_t get_offset_of_U24locvar1_11() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar1_11)); }
	inline Enumerator_t1595687184  get_U24locvar1_11() const { return ___U24locvar1_11; }
	inline Enumerator_t1595687184 * get_address_of_U24locvar1_11() { return &___U24locvar1_11; }
	inline void set_U24locvar1_11(Enumerator_t1595687184  value)
	{
		___U24locvar1_11 = value;
	}

	inline static int32_t get_offset_of_U3CchildU3E__1_12() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CchildU3E__1_12)); }
	inline GetMobilityChildPartsListModel_t2691836378 * get_U3CchildU3E__1_12() const { return ___U3CchildU3E__1_12; }
	inline GetMobilityChildPartsListModel_t2691836378 ** get_address_of_U3CchildU3E__1_12() { return &___U3CchildU3E__1_12; }
	inline void set_U3CchildU3E__1_12(GetMobilityChildPartsListModel_t2691836378 * value)
	{
		___U3CchildU3E__1_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildU3E__1_12, value);
	}

	inline static int32_t get_offset_of_U3CModuleCenterU3E__2_13() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CModuleCenterU3E__2_13)); }
	inline GameObject_t1756533147 * get_U3CModuleCenterU3E__2_13() const { return ___U3CModuleCenterU3E__2_13; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleCenterU3E__2_13() { return &___U3CModuleCenterU3E__2_13; }
	inline void set_U3CModuleCenterU3E__2_13(GameObject_t1756533147 * value)
	{
		___U3CModuleCenterU3E__2_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleCenterU3E__2_13, value);
	}

	inline static int32_t get_offset_of_U3CModuleLeapU3E__2_14() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CModuleLeapU3E__2_14)); }
	inline GameObject_t1756533147 * get_U3CModuleLeapU3E__2_14() const { return ___U3CModuleLeapU3E__2_14; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleLeapU3E__2_14() { return &___U3CModuleLeapU3E__2_14; }
	inline void set_U3CModuleLeapU3E__2_14(GameObject_t1756533147 * value)
	{
		___U3CModuleLeapU3E__2_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleLeapU3E__2_14, value);
	}

	inline static int32_t get_offset_of_U3CModuleRollU3E__2_15() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CModuleRollU3E__2_15)); }
	inline GameObject_t1756533147 * get_U3CModuleRollU3E__2_15() const { return ___U3CModuleRollU3E__2_15; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleRollU3E__2_15() { return &___U3CModuleRollU3E__2_15; }
	inline void set_U3CModuleRollU3E__2_15(GameObject_t1756533147 * value)
	{
		___U3CModuleRollU3E__2_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleRollU3E__2_15, value);
	}

	inline static int32_t get_offset_of_U3CmoduleMgrU3E__2_16() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CmoduleMgrU3E__2_16)); }
	inline ModuleManager_t1065445307 * get_U3CmoduleMgrU3E__2_16() const { return ___U3CmoduleMgrU3E__2_16; }
	inline ModuleManager_t1065445307 ** get_address_of_U3CmoduleMgrU3E__2_16() { return &___U3CmoduleMgrU3E__2_16; }
	inline void set_U3CmoduleMgrU3E__2_16(ModuleManager_t1065445307 * value)
	{
		___U3CmoduleMgrU3E__2_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleMgrU3E__2_16, value);
	}

	inline static int32_t get_offset_of_U3CparentPartsJointNoU3E__2_17() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CparentPartsJointNoU3E__2_17)); }
	inline int32_t get_U3CparentPartsJointNoU3E__2_17() const { return ___U3CparentPartsJointNoU3E__2_17; }
	inline int32_t* get_address_of_U3CparentPartsJointNoU3E__2_17() { return &___U3CparentPartsJointNoU3E__2_17; }
	inline void set_U3CparentPartsJointNoU3E__2_17(int32_t value)
	{
		___U3CparentPartsJointNoU3E__2_17 = value;
	}

	inline static int32_t get_offset_of_U3CmoduleJsonU3E__2_18() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CmoduleJsonU3E__2_18)); }
	inline String_t* get_U3CmoduleJsonU3E__2_18() const { return ___U3CmoduleJsonU3E__2_18; }
	inline String_t** get_address_of_U3CmoduleJsonU3E__2_18() { return &___U3CmoduleJsonU3E__2_18; }
	inline void set_U3CmoduleJsonU3E__2_18(String_t* value)
	{
		___U3CmoduleJsonU3E__2_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleJsonU3E__2_18, value);
	}

	inline static int32_t get_offset_of_U3CmodulePartsCntU3E__2_19() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CmodulePartsCntU3E__2_19)); }
	inline int32_t get_U3CmodulePartsCntU3E__2_19() const { return ___U3CmodulePartsCntU3E__2_19; }
	inline int32_t* get_address_of_U3CmodulePartsCntU3E__2_19() { return &___U3CmodulePartsCntU3E__2_19; }
	inline void set_U3CmodulePartsCntU3E__2_19(int32_t value)
	{
		___U3CmodulePartsCntU3E__2_19 = value;
	}

	inline static int32_t get_offset_of_U3CpartsIdIndexU3E__2_20() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CpartsIdIndexU3E__2_20)); }
	inline StringU5BU5D_t1642385972* get_U3CpartsIdIndexU3E__2_20() const { return ___U3CpartsIdIndexU3E__2_20; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CpartsIdIndexU3E__2_20() { return &___U3CpartsIdIndexU3E__2_20; }
	inline void set_U3CpartsIdIndexU3E__2_20(StringU5BU5D_t1642385972* value)
	{
		___U3CpartsIdIndexU3E__2_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdIndexU3E__2_20, value);
	}

	inline static int32_t get_offset_of_U3CpartsIdListU3E__2_21() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CpartsIdListU3E__2_21)); }
	inline List_1_t1398341365 * get_U3CpartsIdListU3E__2_21() const { return ___U3CpartsIdListU3E__2_21; }
	inline List_1_t1398341365 ** get_address_of_U3CpartsIdListU3E__2_21() { return &___U3CpartsIdListU3E__2_21; }
	inline void set_U3CpartsIdListU3E__2_21(List_1_t1398341365 * value)
	{
		___U3CpartsIdListU3E__2_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsIdListU3E__2_21, value);
	}

	inline static int32_t get_offset_of_U3CmodulePartsListU3E__2_22() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CmodulePartsListU3E__2_22)); }
	inline List_1_t1125654279 * get_U3CmodulePartsListU3E__2_22() const { return ___U3CmodulePartsListU3E__2_22; }
	inline List_1_t1125654279 ** get_address_of_U3CmodulePartsListU3E__2_22() { return &___U3CmodulePartsListU3E__2_22; }
	inline void set_U3CmodulePartsListU3E__2_22(List_1_t1125654279 * value)
	{
		___U3CmodulePartsListU3E__2_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmodulePartsListU3E__2_22, value);
	}

	inline static int32_t get_offset_of_U24locvar6_23() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar6_23)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar6_23() const { return ___U24locvar6_23; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar6_23() { return &___U24locvar6_23; }
	inline void set_U24locvar6_23(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar6_23 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar6_23, value);
	}

	inline static int32_t get_offset_of_U24locvar7_24() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar7_24)); }
	inline int32_t get_U24locvar7_24() const { return ___U24locvar7_24; }
	inline int32_t* get_address_of_U24locvar7_24() { return &___U24locvar7_24; }
	inline void set_U24locvar7_24(int32_t value)
	{
		___U24locvar7_24 = value;
	}

	inline static int32_t get_offset_of_U24locvar8_25() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar8_25)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar8_25() const { return ___U24locvar8_25; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar8_25() { return &___U24locvar8_25; }
	inline void set_U24locvar8_25(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar8_25 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar8_25, value);
	}

	inline static int32_t get_offset_of_U24locvar9_26() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar9_26)); }
	inline int32_t get_U24locvar9_26() const { return ___U24locvar9_26; }
	inline int32_t* get_address_of_U24locvar9_26() { return &___U24locvar9_26; }
	inline void set_U24locvar9_26(int32_t value)
	{
		___U24locvar9_26 = value;
	}

	inline static int32_t get_offset_of_U3CjointParentsU3E__0_27() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U3CjointParentsU3E__0_27)); }
	inline JointU5BU5D_t171503857* get_U3CjointParentsU3E__0_27() const { return ___U3CjointParentsU3E__0_27; }
	inline JointU5BU5D_t171503857** get_address_of_U3CjointParentsU3E__0_27() { return &___U3CjointParentsU3E__0_27; }
	inline void set_U3CjointParentsU3E__0_27(JointU5BU5D_t171503857* value)
	{
		___U3CjointParentsU3E__0_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjointParentsU3E__0_27, value);
	}

	inline static int32_t get_offset_of_U24locvar12_28() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar12_28)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar12_28() const { return ___U24locvar12_28; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar12_28() { return &___U24locvar12_28; }
	inline void set_U24locvar12_28(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar12_28 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar12_28, value);
	}

	inline static int32_t get_offset_of_U24locvar13_29() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24locvar13_29)); }
	inline int32_t get_U24locvar13_29() const { return ___U24locvar13_29; }
	inline int32_t* get_address_of_U24locvar13_29() { return &___U24locvar13_29; }
	inline void set_U24locvar13_29(int32_t value)
	{
		___U24locvar13_29 = value;
	}

	inline static int32_t get_offset_of_U24this_30() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24this_30)); }
	inline MobilmoManager_t1293766190 * get_U24this_30() const { return ___U24this_30; }
	inline MobilmoManager_t1293766190 ** get_address_of_U24this_30() { return &___U24this_30; }
	inline void set_U24this_30(MobilmoManager_t1293766190 * value)
	{
		___U24this_30 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_30, value);
	}

	inline static int32_t get_offset_of_U24current_31() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24current_31)); }
	inline Il2CppObject * get_U24current_31() const { return ___U24current_31; }
	inline Il2CppObject ** get_address_of_U24current_31() { return &___U24current_31; }
	inline void set_U24current_31(Il2CppObject * value)
	{
		___U24current_31 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_31, value);
	}

	inline static int32_t get_offset_of_U24disposing_32() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24disposing_32)); }
	inline bool get_U24disposing_32() const { return ___U24disposing_32; }
	inline bool* get_address_of_U24disposing_32() { return &___U24disposing_32; }
	inline void set_U24disposing_32(bool value)
	{
		___U24disposing_32 = value;
	}

	inline static int32_t get_offset_of_U24PC_33() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177, ___U24PC_33)); }
	inline int32_t get_U24PC_33() const { return ___U24PC_33; }
	inline int32_t* get_address_of_U24PC_33() { return &___U24PC_33; }
	inline void set_U24PC_33(int32_t value)
	{
		___U24PC_33 = value;
	}
};

struct U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177_StaticFields
{
public:
	// System.Func`1<System.Boolean> MobilmoManager/<reCreateActionSetingMobilmo>c__Iterator3::<>f__am$cache0
	Func_1_t1485000104 * ___U3CU3Ef__amU24cache0_34;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_34() { return static_cast<int32_t>(offsetof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177_StaticFields, ___U3CU3Ef__amU24cache0_34)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__amU24cache0_34() const { return ___U3CU3Ef__amU24cache0_34; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__amU24cache0_34() { return &___U3CU3Ef__amU24cache0_34; }
	inline void set_U3CU3Ef__amU24cache0_34(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__amU24cache0_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
