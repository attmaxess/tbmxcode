﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat545388769.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2566733593.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3170814519.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetNearmobModel
struct GetNearmobModel_t3189293895;
// NearmobItemsModel
struct NearmobItemsModel_t1641537963;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// Mobilmo
struct Mobilmo_t370754809;
// UnityEngine.FixedJoint
struct FixedJoint_t3848069458;
// UnityEngine.Material
struct Material_t193706927;
// GetNearmobChildPartsListModel
struct GetNearmobChildPartsListModel_t3662882787;
// ModuleManager
struct ModuleManager_t1065445307;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// Parts[]
struct PartsU5BU5D_t3272295067;
// Joint[]
struct JointU5BU5D_t171503857;
// MobilmoManager
struct MobilmoManager_t1293766190;
// System.Object
struct Il2CppObject;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoManager/<recordingRecreateMobility>c__Iterator2
struct  U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739  : public Il2CppObject
{
public:
	// System.String MobilmoManager/<recordingRecreateMobility>c__Iterator2::json
	String_t* ___json_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> MobilmoManager/<recordingRecreateMobility>c__Iterator2::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_1;
	// GetNearmobModel MobilmoManager/<recordingRecreateMobility>c__Iterator2::<_getNearModel>__0
	GetNearmobModel_t3189293895 * ___U3C_getNearModelU3E__0_2;
	// System.Collections.Generic.List`1/Enumerator<NearmobItemsModel> MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar0
	Enumerator_t545388769  ___U24locvar0_3;
	// NearmobItemsModel MobilmoManager/<recordingRecreateMobility>c__Iterator2::<itemModel>__1
	NearmobItemsModel_t1641537963 * ___U3CitemModelU3E__1_4;
	// System.String MobilmoManager/<recordingRecreateMobility>c__Iterator2::<createUserId>__2
	String_t* ___U3CcreateUserIdU3E__2_5;
	// UnityEngine.GameObject MobilmoManager/<recordingRecreateMobility>c__Iterator2::<mobilmo>__2
	GameObject_t1756533147 * ___U3CmobilmoU3E__2_6;
	// UnityEngine.Rigidbody MobilmoManager/<recordingRecreateMobility>c__Iterator2::<mobilmoRigid>__2
	Rigidbody_t4233889191 * ___U3CmobilmoRigidU3E__2_7;
	// Mobilmo MobilmoManager/<recordingRecreateMobility>c__Iterator2::<m_mobilimo>__2
	Mobilmo_t370754809 * ___U3Cm_mobilimoU3E__2_8;
	// UnityEngine.FixedJoint MobilmoManager/<recordingRecreateMobility>c__Iterator2::<mobilFixed>__2
	FixedJoint_t3848069458 * ___U3CmobilFixedU3E__2_9;
	// UnityEngine.GameObject MobilmoManager/<recordingRecreateMobility>c__Iterator2::<coreChildObj>__2
	GameObject_t1756533147 * ___U3CcoreChildObjU3E__2_10;
	// UnityEngine.GameObject MobilmoManager/<recordingRecreateMobility>c__Iterator2::<m_CreObj>__2
	GameObject_t1756533147 * ___U3Cm_CreObjU3E__2_11;
	// UnityEngine.Material MobilmoManager/<recordingRecreateMobility>c__Iterator2::<coreMat>__2
	Material_t193706927 * ___U3CcoreMatU3E__2_12;
	// System.Collections.Generic.List`1/Enumerator<GetNearmobChildPartsListModel> MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar1
	Enumerator_t2566733593  ___U24locvar1_13;
	// GetNearmobChildPartsListModel MobilmoManager/<recordingRecreateMobility>c__Iterator2::<child>__3
	GetNearmobChildPartsListModel_t3662882787 * ___U3CchildU3E__3_14;
	// UnityEngine.GameObject MobilmoManager/<recordingRecreateMobility>c__Iterator2::<reModuleCenter>__4
	GameObject_t1756533147 * ___U3CreModuleCenterU3E__4_15;
	// System.String MobilmoManager/<recordingRecreateMobility>c__Iterator2::<moduleJson>__4
	String_t* ___U3CmoduleJsonU3E__4_16;
	// UnityEngine.GameObject MobilmoManager/<recordingRecreateMobility>c__Iterator2::<ModuleLeap>__4
	GameObject_t1756533147 * ___U3CModuleLeapU3E__4_17;
	// UnityEngine.GameObject MobilmoManager/<recordingRecreateMobility>c__Iterator2::<ModuleRoll>__4
	GameObject_t1756533147 * ___U3CModuleRollU3E__4_18;
	// ModuleManager MobilmoManager/<recordingRecreateMobility>c__Iterator2::<moduleMgr>__4
	ModuleManager_t1065445307 * ___U3CmoduleMgrU3E__4_19;
	// System.Int32 MobilmoManager/<recordingRecreateMobility>c__Iterator2::<parentPartsJointNo>__4
	int32_t ___U3CparentPartsJointNoU3E__4_20;
	// UnityEngine.Transform[] MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar8
	TransformU5BU5D_t3764228911* ___U24locvar8_21;
	// System.Int32 MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar9
	int32_t ___U24locvar9_22;
	// Parts[] MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar12
	PartsU5BU5D_t3272295067* ___U24locvar12_23;
	// System.Int32 MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar13
	int32_t ___U24locvar13_24;
	// UnityEngine.Transform[] MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar14
	TransformU5BU5D_t3764228911* ___U24locvar14_25;
	// System.Int32 MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar15
	int32_t ___U24locvar15_26;
	// UnityEngine.Transform[] MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar16
	TransformU5BU5D_t3764228911* ___U24locvar16_27;
	// System.Int32 MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar17
	int32_t ___U24locvar17_28;
	// Mobilmo MobilmoManager/<recordingRecreateMobility>c__Iterator2::<recMobilmoStatus>__2
	Mobilmo_t370754809 * ___U3CrecMobilmoStatusU3E__2_29;
	// Joint[] MobilmoManager/<recordingRecreateMobility>c__Iterator2::<jointParents>__2
	JointU5BU5D_t171503857* ___U3CjointParentsU3E__2_30;
	// UnityEngine.Transform[] MobilmoManager/<recordingRecreateMobility>c__Iterator2::<partsTra>__2
	TransformU5BU5D_t3764228911* ___U3CpartsTraU3E__2_31;
	// UnityEngine.Transform[] MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar18
	TransformU5BU5D_t3764228911* ___U24locvar18_32;
	// System.Int32 MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar19
	int32_t ___U24locvar19_33;
	// System.Collections.Generic.List`1/Enumerator<GetNearmobRecordedMotionListMotions> MobilmoManager/<recordingRecreateMobility>c__Iterator2::$locvar1A
	Enumerator_t3170814519  ___U24locvar1A_34;
	// MobilmoManager MobilmoManager/<recordingRecreateMobility>c__Iterator2::$this
	MobilmoManager_t1293766190 * ___U24this_35;
	// System.Object MobilmoManager/<recordingRecreateMobility>c__Iterator2::$current
	Il2CppObject * ___U24current_36;
	// System.Boolean MobilmoManager/<recordingRecreateMobility>c__Iterator2::$disposing
	bool ___U24disposing_37;
	// System.Int32 MobilmoManager/<recordingRecreateMobility>c__Iterator2::$PC
	int32_t ___U24PC_38;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___json_0)); }
	inline String_t* get_json_0() const { return ___json_0; }
	inline String_t** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(String_t* value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier(&___json_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CwwwDataU3E__0_1)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_1() const { return ___U3CwwwDataU3E__0_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_1() { return &___U3CwwwDataU3E__0_1; }
	inline void set_U3CwwwDataU3E__0_1(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3C_getNearModelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3C_getNearModelU3E__0_2)); }
	inline GetNearmobModel_t3189293895 * get_U3C_getNearModelU3E__0_2() const { return ___U3C_getNearModelU3E__0_2; }
	inline GetNearmobModel_t3189293895 ** get_address_of_U3C_getNearModelU3E__0_2() { return &___U3C_getNearModelU3E__0_2; }
	inline void set_U3C_getNearModelU3E__0_2(GetNearmobModel_t3189293895 * value)
	{
		___U3C_getNearModelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_getNearModelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar0_3)); }
	inline Enumerator_t545388769  get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline Enumerator_t545388769 * get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(Enumerator_t545388769  value)
	{
		___U24locvar0_3 = value;
	}

	inline static int32_t get_offset_of_U3CitemModelU3E__1_4() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CitemModelU3E__1_4)); }
	inline NearmobItemsModel_t1641537963 * get_U3CitemModelU3E__1_4() const { return ___U3CitemModelU3E__1_4; }
	inline NearmobItemsModel_t1641537963 ** get_address_of_U3CitemModelU3E__1_4() { return &___U3CitemModelU3E__1_4; }
	inline void set_U3CitemModelU3E__1_4(NearmobItemsModel_t1641537963 * value)
	{
		___U3CitemModelU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemModelU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U3CcreateUserIdU3E__2_5() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CcreateUserIdU3E__2_5)); }
	inline String_t* get_U3CcreateUserIdU3E__2_5() const { return ___U3CcreateUserIdU3E__2_5; }
	inline String_t** get_address_of_U3CcreateUserIdU3E__2_5() { return &___U3CcreateUserIdU3E__2_5; }
	inline void set_U3CcreateUserIdU3E__2_5(String_t* value)
	{
		___U3CcreateUserIdU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcreateUserIdU3E__2_5, value);
	}

	inline static int32_t get_offset_of_U3CmobilmoU3E__2_6() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CmobilmoU3E__2_6)); }
	inline GameObject_t1756533147 * get_U3CmobilmoU3E__2_6() const { return ___U3CmobilmoU3E__2_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CmobilmoU3E__2_6() { return &___U3CmobilmoU3E__2_6; }
	inline void set_U3CmobilmoU3E__2_6(GameObject_t1756533147 * value)
	{
		___U3CmobilmoU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilmoU3E__2_6, value);
	}

	inline static int32_t get_offset_of_U3CmobilmoRigidU3E__2_7() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CmobilmoRigidU3E__2_7)); }
	inline Rigidbody_t4233889191 * get_U3CmobilmoRigidU3E__2_7() const { return ___U3CmobilmoRigidU3E__2_7; }
	inline Rigidbody_t4233889191 ** get_address_of_U3CmobilmoRigidU3E__2_7() { return &___U3CmobilmoRigidU3E__2_7; }
	inline void set_U3CmobilmoRigidU3E__2_7(Rigidbody_t4233889191 * value)
	{
		___U3CmobilmoRigidU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilmoRigidU3E__2_7, value);
	}

	inline static int32_t get_offset_of_U3Cm_mobilimoU3E__2_8() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3Cm_mobilimoU3E__2_8)); }
	inline Mobilmo_t370754809 * get_U3Cm_mobilimoU3E__2_8() const { return ___U3Cm_mobilimoU3E__2_8; }
	inline Mobilmo_t370754809 ** get_address_of_U3Cm_mobilimoU3E__2_8() { return &___U3Cm_mobilimoU3E__2_8; }
	inline void set_U3Cm_mobilimoU3E__2_8(Mobilmo_t370754809 * value)
	{
		___U3Cm_mobilimoU3E__2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_mobilimoU3E__2_8, value);
	}

	inline static int32_t get_offset_of_U3CmobilFixedU3E__2_9() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CmobilFixedU3E__2_9)); }
	inline FixedJoint_t3848069458 * get_U3CmobilFixedU3E__2_9() const { return ___U3CmobilFixedU3E__2_9; }
	inline FixedJoint_t3848069458 ** get_address_of_U3CmobilFixedU3E__2_9() { return &___U3CmobilFixedU3E__2_9; }
	inline void set_U3CmobilFixedU3E__2_9(FixedJoint_t3848069458 * value)
	{
		___U3CmobilFixedU3E__2_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmobilFixedU3E__2_9, value);
	}

	inline static int32_t get_offset_of_U3CcoreChildObjU3E__2_10() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CcoreChildObjU3E__2_10)); }
	inline GameObject_t1756533147 * get_U3CcoreChildObjU3E__2_10() const { return ___U3CcoreChildObjU3E__2_10; }
	inline GameObject_t1756533147 ** get_address_of_U3CcoreChildObjU3E__2_10() { return &___U3CcoreChildObjU3E__2_10; }
	inline void set_U3CcoreChildObjU3E__2_10(GameObject_t1756533147 * value)
	{
		___U3CcoreChildObjU3E__2_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreChildObjU3E__2_10, value);
	}

	inline static int32_t get_offset_of_U3Cm_CreObjU3E__2_11() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3Cm_CreObjU3E__2_11)); }
	inline GameObject_t1756533147 * get_U3Cm_CreObjU3E__2_11() const { return ___U3Cm_CreObjU3E__2_11; }
	inline GameObject_t1756533147 ** get_address_of_U3Cm_CreObjU3E__2_11() { return &___U3Cm_CreObjU3E__2_11; }
	inline void set_U3Cm_CreObjU3E__2_11(GameObject_t1756533147 * value)
	{
		___U3Cm_CreObjU3E__2_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cm_CreObjU3E__2_11, value);
	}

	inline static int32_t get_offset_of_U3CcoreMatU3E__2_12() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CcoreMatU3E__2_12)); }
	inline Material_t193706927 * get_U3CcoreMatU3E__2_12() const { return ___U3CcoreMatU3E__2_12; }
	inline Material_t193706927 ** get_address_of_U3CcoreMatU3E__2_12() { return &___U3CcoreMatU3E__2_12; }
	inline void set_U3CcoreMatU3E__2_12(Material_t193706927 * value)
	{
		___U3CcoreMatU3E__2_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreMatU3E__2_12, value);
	}

	inline static int32_t get_offset_of_U24locvar1_13() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar1_13)); }
	inline Enumerator_t2566733593  get_U24locvar1_13() const { return ___U24locvar1_13; }
	inline Enumerator_t2566733593 * get_address_of_U24locvar1_13() { return &___U24locvar1_13; }
	inline void set_U24locvar1_13(Enumerator_t2566733593  value)
	{
		___U24locvar1_13 = value;
	}

	inline static int32_t get_offset_of_U3CchildU3E__3_14() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CchildU3E__3_14)); }
	inline GetNearmobChildPartsListModel_t3662882787 * get_U3CchildU3E__3_14() const { return ___U3CchildU3E__3_14; }
	inline GetNearmobChildPartsListModel_t3662882787 ** get_address_of_U3CchildU3E__3_14() { return &___U3CchildU3E__3_14; }
	inline void set_U3CchildU3E__3_14(GetNearmobChildPartsListModel_t3662882787 * value)
	{
		___U3CchildU3E__3_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildU3E__3_14, value);
	}

	inline static int32_t get_offset_of_U3CreModuleCenterU3E__4_15() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CreModuleCenterU3E__4_15)); }
	inline GameObject_t1756533147 * get_U3CreModuleCenterU3E__4_15() const { return ___U3CreModuleCenterU3E__4_15; }
	inline GameObject_t1756533147 ** get_address_of_U3CreModuleCenterU3E__4_15() { return &___U3CreModuleCenterU3E__4_15; }
	inline void set_U3CreModuleCenterU3E__4_15(GameObject_t1756533147 * value)
	{
		___U3CreModuleCenterU3E__4_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreModuleCenterU3E__4_15, value);
	}

	inline static int32_t get_offset_of_U3CmoduleJsonU3E__4_16() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CmoduleJsonU3E__4_16)); }
	inline String_t* get_U3CmoduleJsonU3E__4_16() const { return ___U3CmoduleJsonU3E__4_16; }
	inline String_t** get_address_of_U3CmoduleJsonU3E__4_16() { return &___U3CmoduleJsonU3E__4_16; }
	inline void set_U3CmoduleJsonU3E__4_16(String_t* value)
	{
		___U3CmoduleJsonU3E__4_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleJsonU3E__4_16, value);
	}

	inline static int32_t get_offset_of_U3CModuleLeapU3E__4_17() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CModuleLeapU3E__4_17)); }
	inline GameObject_t1756533147 * get_U3CModuleLeapU3E__4_17() const { return ___U3CModuleLeapU3E__4_17; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleLeapU3E__4_17() { return &___U3CModuleLeapU3E__4_17; }
	inline void set_U3CModuleLeapU3E__4_17(GameObject_t1756533147 * value)
	{
		___U3CModuleLeapU3E__4_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleLeapU3E__4_17, value);
	}

	inline static int32_t get_offset_of_U3CModuleRollU3E__4_18() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CModuleRollU3E__4_18)); }
	inline GameObject_t1756533147 * get_U3CModuleRollU3E__4_18() const { return ___U3CModuleRollU3E__4_18; }
	inline GameObject_t1756533147 ** get_address_of_U3CModuleRollU3E__4_18() { return &___U3CModuleRollU3E__4_18; }
	inline void set_U3CModuleRollU3E__4_18(GameObject_t1756533147 * value)
	{
		___U3CModuleRollU3E__4_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CModuleRollU3E__4_18, value);
	}

	inline static int32_t get_offset_of_U3CmoduleMgrU3E__4_19() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CmoduleMgrU3E__4_19)); }
	inline ModuleManager_t1065445307 * get_U3CmoduleMgrU3E__4_19() const { return ___U3CmoduleMgrU3E__4_19; }
	inline ModuleManager_t1065445307 ** get_address_of_U3CmoduleMgrU3E__4_19() { return &___U3CmoduleMgrU3E__4_19; }
	inline void set_U3CmoduleMgrU3E__4_19(ModuleManager_t1065445307 * value)
	{
		___U3CmoduleMgrU3E__4_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleMgrU3E__4_19, value);
	}

	inline static int32_t get_offset_of_U3CparentPartsJointNoU3E__4_20() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CparentPartsJointNoU3E__4_20)); }
	inline int32_t get_U3CparentPartsJointNoU3E__4_20() const { return ___U3CparentPartsJointNoU3E__4_20; }
	inline int32_t* get_address_of_U3CparentPartsJointNoU3E__4_20() { return &___U3CparentPartsJointNoU3E__4_20; }
	inline void set_U3CparentPartsJointNoU3E__4_20(int32_t value)
	{
		___U3CparentPartsJointNoU3E__4_20 = value;
	}

	inline static int32_t get_offset_of_U24locvar8_21() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar8_21)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar8_21() const { return ___U24locvar8_21; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar8_21() { return &___U24locvar8_21; }
	inline void set_U24locvar8_21(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar8_21 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar8_21, value);
	}

	inline static int32_t get_offset_of_U24locvar9_22() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar9_22)); }
	inline int32_t get_U24locvar9_22() const { return ___U24locvar9_22; }
	inline int32_t* get_address_of_U24locvar9_22() { return &___U24locvar9_22; }
	inline void set_U24locvar9_22(int32_t value)
	{
		___U24locvar9_22 = value;
	}

	inline static int32_t get_offset_of_U24locvar12_23() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar12_23)); }
	inline PartsU5BU5D_t3272295067* get_U24locvar12_23() const { return ___U24locvar12_23; }
	inline PartsU5BU5D_t3272295067** get_address_of_U24locvar12_23() { return &___U24locvar12_23; }
	inline void set_U24locvar12_23(PartsU5BU5D_t3272295067* value)
	{
		___U24locvar12_23 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar12_23, value);
	}

	inline static int32_t get_offset_of_U24locvar13_24() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar13_24)); }
	inline int32_t get_U24locvar13_24() const { return ___U24locvar13_24; }
	inline int32_t* get_address_of_U24locvar13_24() { return &___U24locvar13_24; }
	inline void set_U24locvar13_24(int32_t value)
	{
		___U24locvar13_24 = value;
	}

	inline static int32_t get_offset_of_U24locvar14_25() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar14_25)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar14_25() const { return ___U24locvar14_25; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar14_25() { return &___U24locvar14_25; }
	inline void set_U24locvar14_25(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar14_25 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar14_25, value);
	}

	inline static int32_t get_offset_of_U24locvar15_26() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar15_26)); }
	inline int32_t get_U24locvar15_26() const { return ___U24locvar15_26; }
	inline int32_t* get_address_of_U24locvar15_26() { return &___U24locvar15_26; }
	inline void set_U24locvar15_26(int32_t value)
	{
		___U24locvar15_26 = value;
	}

	inline static int32_t get_offset_of_U24locvar16_27() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar16_27)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar16_27() const { return ___U24locvar16_27; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar16_27() { return &___U24locvar16_27; }
	inline void set_U24locvar16_27(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar16_27 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar16_27, value);
	}

	inline static int32_t get_offset_of_U24locvar17_28() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar17_28)); }
	inline int32_t get_U24locvar17_28() const { return ___U24locvar17_28; }
	inline int32_t* get_address_of_U24locvar17_28() { return &___U24locvar17_28; }
	inline void set_U24locvar17_28(int32_t value)
	{
		___U24locvar17_28 = value;
	}

	inline static int32_t get_offset_of_U3CrecMobilmoStatusU3E__2_29() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CrecMobilmoStatusU3E__2_29)); }
	inline Mobilmo_t370754809 * get_U3CrecMobilmoStatusU3E__2_29() const { return ___U3CrecMobilmoStatusU3E__2_29; }
	inline Mobilmo_t370754809 ** get_address_of_U3CrecMobilmoStatusU3E__2_29() { return &___U3CrecMobilmoStatusU3E__2_29; }
	inline void set_U3CrecMobilmoStatusU3E__2_29(Mobilmo_t370754809 * value)
	{
		___U3CrecMobilmoStatusU3E__2_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrecMobilmoStatusU3E__2_29, value);
	}

	inline static int32_t get_offset_of_U3CjointParentsU3E__2_30() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CjointParentsU3E__2_30)); }
	inline JointU5BU5D_t171503857* get_U3CjointParentsU3E__2_30() const { return ___U3CjointParentsU3E__2_30; }
	inline JointU5BU5D_t171503857** get_address_of_U3CjointParentsU3E__2_30() { return &___U3CjointParentsU3E__2_30; }
	inline void set_U3CjointParentsU3E__2_30(JointU5BU5D_t171503857* value)
	{
		___U3CjointParentsU3E__2_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjointParentsU3E__2_30, value);
	}

	inline static int32_t get_offset_of_U3CpartsTraU3E__2_31() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U3CpartsTraU3E__2_31)); }
	inline TransformU5BU5D_t3764228911* get_U3CpartsTraU3E__2_31() const { return ___U3CpartsTraU3E__2_31; }
	inline TransformU5BU5D_t3764228911** get_address_of_U3CpartsTraU3E__2_31() { return &___U3CpartsTraU3E__2_31; }
	inline void set_U3CpartsTraU3E__2_31(TransformU5BU5D_t3764228911* value)
	{
		___U3CpartsTraU3E__2_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsTraU3E__2_31, value);
	}

	inline static int32_t get_offset_of_U24locvar18_32() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar18_32)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar18_32() const { return ___U24locvar18_32; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar18_32() { return &___U24locvar18_32; }
	inline void set_U24locvar18_32(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar18_32 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar18_32, value);
	}

	inline static int32_t get_offset_of_U24locvar19_33() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar19_33)); }
	inline int32_t get_U24locvar19_33() const { return ___U24locvar19_33; }
	inline int32_t* get_address_of_U24locvar19_33() { return &___U24locvar19_33; }
	inline void set_U24locvar19_33(int32_t value)
	{
		___U24locvar19_33 = value;
	}

	inline static int32_t get_offset_of_U24locvar1A_34() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24locvar1A_34)); }
	inline Enumerator_t3170814519  get_U24locvar1A_34() const { return ___U24locvar1A_34; }
	inline Enumerator_t3170814519 * get_address_of_U24locvar1A_34() { return &___U24locvar1A_34; }
	inline void set_U24locvar1A_34(Enumerator_t3170814519  value)
	{
		___U24locvar1A_34 = value;
	}

	inline static int32_t get_offset_of_U24this_35() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24this_35)); }
	inline MobilmoManager_t1293766190 * get_U24this_35() const { return ___U24this_35; }
	inline MobilmoManager_t1293766190 ** get_address_of_U24this_35() { return &___U24this_35; }
	inline void set_U24this_35(MobilmoManager_t1293766190 * value)
	{
		___U24this_35 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_35, value);
	}

	inline static int32_t get_offset_of_U24current_36() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24current_36)); }
	inline Il2CppObject * get_U24current_36() const { return ___U24current_36; }
	inline Il2CppObject ** get_address_of_U24current_36() { return &___U24current_36; }
	inline void set_U24current_36(Il2CppObject * value)
	{
		___U24current_36 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_36, value);
	}

	inline static int32_t get_offset_of_U24disposing_37() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24disposing_37)); }
	inline bool get_U24disposing_37() const { return ___U24disposing_37; }
	inline bool* get_address_of_U24disposing_37() { return &___U24disposing_37; }
	inline void set_U24disposing_37(bool value)
	{
		___U24disposing_37 = value;
	}

	inline static int32_t get_offset_of_U24PC_38() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739, ___U24PC_38)); }
	inline int32_t get_U24PC_38() const { return ___U24PC_38; }
	inline int32_t* get_address_of_U24PC_38() { return &___U24PC_38; }
	inline void set_U24PC_38(int32_t value)
	{
		___U24PC_38 = value;
	}
};

struct U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739_StaticFields
{
public:
	// System.Func`1<System.Boolean> MobilmoManager/<recordingRecreateMobility>c__Iterator2::<>f__am$cache0
	Func_1_t1485000104 * ___U3CU3Ef__amU24cache0_39;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_39() { return static_cast<int32_t>(offsetof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739_StaticFields, ___U3CU3Ef__amU24cache0_39)); }
	inline Func_1_t1485000104 * get_U3CU3Ef__amU24cache0_39() const { return ___U3CU3Ef__amU24cache0_39; }
	inline Func_1_t1485000104 ** get_address_of_U3CU3Ef__amU24cache0_39() { return &___U3CU3Ef__amU24cache0_39; }
	inline void set_U3CU3Ef__amU24cache0_39(Func_1_t1485000104 * value)
	{
		___U3CU3Ef__amU24cache0_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
