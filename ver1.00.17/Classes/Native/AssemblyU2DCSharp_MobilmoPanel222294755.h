﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MobilmoPanel
struct  MobilmoPanel_t222294755  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MobilmoPanel::CoreSelect
	GameObject_t1756533147 * ___CoreSelect_2;
	// UnityEngine.GameObject MobilmoPanel::PartsConect
	GameObject_t1756533147 * ___PartsConect_3;
	// UnityEngine.GameObject[] MobilmoPanel::CompleteButton
	GameObjectU5BU5D_t3057952154* ___CompleteButton_4;

public:
	inline static int32_t get_offset_of_CoreSelect_2() { return static_cast<int32_t>(offsetof(MobilmoPanel_t222294755, ___CoreSelect_2)); }
	inline GameObject_t1756533147 * get_CoreSelect_2() const { return ___CoreSelect_2; }
	inline GameObject_t1756533147 ** get_address_of_CoreSelect_2() { return &___CoreSelect_2; }
	inline void set_CoreSelect_2(GameObject_t1756533147 * value)
	{
		___CoreSelect_2 = value;
		Il2CppCodeGenWriteBarrier(&___CoreSelect_2, value);
	}

	inline static int32_t get_offset_of_PartsConect_3() { return static_cast<int32_t>(offsetof(MobilmoPanel_t222294755, ___PartsConect_3)); }
	inline GameObject_t1756533147 * get_PartsConect_3() const { return ___PartsConect_3; }
	inline GameObject_t1756533147 ** get_address_of_PartsConect_3() { return &___PartsConect_3; }
	inline void set_PartsConect_3(GameObject_t1756533147 * value)
	{
		___PartsConect_3 = value;
		Il2CppCodeGenWriteBarrier(&___PartsConect_3, value);
	}

	inline static int32_t get_offset_of_CompleteButton_4() { return static_cast<int32_t>(offsetof(MobilmoPanel_t222294755, ___CompleteButton_4)); }
	inline GameObjectU5BU5D_t3057952154* get_CompleteButton_4() const { return ___CompleteButton_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_CompleteButton_4() { return &___CompleteButton_4; }
	inline void set_CompleteButton_4(GameObjectU5BU5D_t3057952154* value)
	{
		___CompleteButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___CompleteButton_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
