﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// SlideInOut
struct SlideInOut_t958296806;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// SliderControl[]
struct SliderControlU5BU5D_t1084111215;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelingConfigPanel
struct  ModelingConfigPanel_t2566912473  : public MonoBehaviour_t1158329972
{
public:
	// SlideInOut ModelingConfigPanel::Slider
	SlideInOut_t958296806 * ___Slider_2;
	// UnityEngine.UI.Slider ModelingConfigPanel::SliderEnagy
	Slider_t297367283 * ___SliderEnagy_3;
	// UnityEngine.UI.Slider ModelingConfigPanel::SliderLight
	Slider_t297367283 * ___SliderLight_4;
	// SliderControl[] ModelingConfigPanel::PositionSliders
	SliderControlU5BU5D_t1084111215* ___PositionSliders_5;
	// SliderControl[] ModelingConfigPanel::StatusSliders
	SliderControlU5BU5D_t1084111215* ___StatusSliders_6;

public:
	inline static int32_t get_offset_of_Slider_2() { return static_cast<int32_t>(offsetof(ModelingConfigPanel_t2566912473, ___Slider_2)); }
	inline SlideInOut_t958296806 * get_Slider_2() const { return ___Slider_2; }
	inline SlideInOut_t958296806 ** get_address_of_Slider_2() { return &___Slider_2; }
	inline void set_Slider_2(SlideInOut_t958296806 * value)
	{
		___Slider_2 = value;
		Il2CppCodeGenWriteBarrier(&___Slider_2, value);
	}

	inline static int32_t get_offset_of_SliderEnagy_3() { return static_cast<int32_t>(offsetof(ModelingConfigPanel_t2566912473, ___SliderEnagy_3)); }
	inline Slider_t297367283 * get_SliderEnagy_3() const { return ___SliderEnagy_3; }
	inline Slider_t297367283 ** get_address_of_SliderEnagy_3() { return &___SliderEnagy_3; }
	inline void set_SliderEnagy_3(Slider_t297367283 * value)
	{
		___SliderEnagy_3 = value;
		Il2CppCodeGenWriteBarrier(&___SliderEnagy_3, value);
	}

	inline static int32_t get_offset_of_SliderLight_4() { return static_cast<int32_t>(offsetof(ModelingConfigPanel_t2566912473, ___SliderLight_4)); }
	inline Slider_t297367283 * get_SliderLight_4() const { return ___SliderLight_4; }
	inline Slider_t297367283 ** get_address_of_SliderLight_4() { return &___SliderLight_4; }
	inline void set_SliderLight_4(Slider_t297367283 * value)
	{
		___SliderLight_4 = value;
		Il2CppCodeGenWriteBarrier(&___SliderLight_4, value);
	}

	inline static int32_t get_offset_of_PositionSliders_5() { return static_cast<int32_t>(offsetof(ModelingConfigPanel_t2566912473, ___PositionSliders_5)); }
	inline SliderControlU5BU5D_t1084111215* get_PositionSliders_5() const { return ___PositionSliders_5; }
	inline SliderControlU5BU5D_t1084111215** get_address_of_PositionSliders_5() { return &___PositionSliders_5; }
	inline void set_PositionSliders_5(SliderControlU5BU5D_t1084111215* value)
	{
		___PositionSliders_5 = value;
		Il2CppCodeGenWriteBarrier(&___PositionSliders_5, value);
	}

	inline static int32_t get_offset_of_StatusSliders_6() { return static_cast<int32_t>(offsetof(ModelingConfigPanel_t2566912473, ___StatusSliders_6)); }
	inline SliderControlU5BU5D_t1084111215* get_StatusSliders_6() const { return ___StatusSliders_6; }
	inline SliderControlU5BU5D_t1084111215** get_address_of_StatusSliders_6() { return &___StatusSliders_6; }
	inline void set_StatusSliders_6(SliderControlU5BU5D_t1084111215* value)
	{
		___StatusSliders_6 = value;
		Il2CppCodeGenWriteBarrier(&___StatusSliders_6, value);
	}
};

struct ModelingConfigPanel_t2566912473_StaticFields
{
public:
	// DG.Tweening.TweenCallback ModelingConfigPanel::<>f__am$cache0
	TweenCallback_t3697142134 * ___U3CU3Ef__amU24cache0_7;
	// DG.Tweening.TweenCallback ModelingConfigPanel::<>f__am$cache1
	TweenCallback_t3697142134 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(ModelingConfigPanel_t2566912473_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline TweenCallback_t3697142134 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline TweenCallback_t3697142134 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(TweenCallback_t3697142134 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(ModelingConfigPanel_t2566912473_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline TweenCallback_t3697142134 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline TweenCallback_t3697142134 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(TweenCallback_t3697142134 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
