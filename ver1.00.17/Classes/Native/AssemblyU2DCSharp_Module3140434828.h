﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Parts
struct Parts_t3804168686;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve>
struct Dictionary_2_t926353117;
// System.String
struct String_t;
// Mobilmo
struct Mobilmo_t370754809;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Module
struct  Module_t3140434828  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Module::m_ModuleObj
	GameObject_t1756533147 * ___m_ModuleObj_2;
	// UnityEngine.GameObject Module::m_CoreObj
	GameObject_t1756533147 * ___m_CoreObj_3;
	// Parts Module::ParentParts
	Parts_t3804168686 * ___ParentParts_4;
	// UnityEngine.Animation Module::m_Anim
	Animation_t2068071072 * ___m_Anim_5;
	// UnityEngine.AnimationClip Module::m_ModuleAnim
	AnimationClip_t3510324950 * ___m_ModuleAnim_6;
	// UnityEngine.AnimationClip Module::m_ModuleReverseAnim
	AnimationClip_t3510324950 * ___m_ModuleReverseAnim_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> Module::m_AnimCurveDic
	Dictionary_2_t926353117 * ___m_AnimCurveDic_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> Module::m_ReverseCurveDic
	Dictionary_2_t926353117 * ___m_ReverseCurveDic_9;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> Module::m_pDefaultCurve
	Dictionary_2_t926353117 * ___m_pDefaultCurve_10;
	// UnityEngine.AnimationClip Module::m_DefaultClip
	AnimationClip_t3510324950 * ___m_DefaultClip_11;
	// System.String Module::iconKey
	String_t* ___iconKey_12;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> Module::m_pEndPoseCurve
	Dictionary_2_t926353117 * ___m_pEndPoseCurve_13;
	// UnityEngine.AnimationClip Module::m_EndPoseClip
	AnimationClip_t3510324950 * ___m_EndPoseClip_14;
	// System.Int32 Module::PlayModeNum
	int32_t ___PlayModeNum_15;
	// System.Boolean Module::m_bIsActionPlay
	bool ___m_bIsActionPlay_16;
	// System.Boolean Module::m_bisTestAnim
	bool ___m_bisTestAnim_17;
	// System.Boolean Module::m_bIsJointConnect
	bool ___m_bIsJointConnect_18;
	// System.String Module::moduleJson
	String_t* ___moduleJson_19;
	// System.String Module::m_moduleId
	String_t* ___m_moduleId_20;
	// System.String Module::m_moduleAnimId
	String_t* ___m_moduleAnimId_21;
	// System.Int32 Module::m_parentModuleNo
	int32_t ___m_parentModuleNo_22;
	// System.Int32 Module::m_ModuleNo
	int32_t ___m_ModuleNo_23;
	// System.String Module::childPartsName
	String_t* ___childPartsName_24;
	// System.Boolean Module::recModule
	bool ___recModule_25;
	// Mobilmo Module::ParentMobilmo
	Mobilmo_t370754809 * ___ParentMobilmo_26;
	// System.Int32 Module::parentMobilmoId
	int32_t ___parentMobilmoId_27;
	// UnityEngine.Transform Module::JointChildParts
	Transform_t3275118058 * ___JointChildParts_28;
	// System.Int32 Module::moduleColorId
	int32_t ___moduleColorId_29;
	// System.Int32 Module::cloneModuleCreateUserId
	int32_t ___cloneModuleCreateUserId_30;

public:
	inline static int32_t get_offset_of_m_ModuleObj_2() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_ModuleObj_2)); }
	inline GameObject_t1756533147 * get_m_ModuleObj_2() const { return ___m_ModuleObj_2; }
	inline GameObject_t1756533147 ** get_address_of_m_ModuleObj_2() { return &___m_ModuleObj_2; }
	inline void set_m_ModuleObj_2(GameObject_t1756533147 * value)
	{
		___m_ModuleObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ModuleObj_2, value);
	}

	inline static int32_t get_offset_of_m_CoreObj_3() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_CoreObj_3)); }
	inline GameObject_t1756533147 * get_m_CoreObj_3() const { return ___m_CoreObj_3; }
	inline GameObject_t1756533147 ** get_address_of_m_CoreObj_3() { return &___m_CoreObj_3; }
	inline void set_m_CoreObj_3(GameObject_t1756533147 * value)
	{
		___m_CoreObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_CoreObj_3, value);
	}

	inline static int32_t get_offset_of_ParentParts_4() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___ParentParts_4)); }
	inline Parts_t3804168686 * get_ParentParts_4() const { return ___ParentParts_4; }
	inline Parts_t3804168686 ** get_address_of_ParentParts_4() { return &___ParentParts_4; }
	inline void set_ParentParts_4(Parts_t3804168686 * value)
	{
		___ParentParts_4 = value;
		Il2CppCodeGenWriteBarrier(&___ParentParts_4, value);
	}

	inline static int32_t get_offset_of_m_Anim_5() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_Anim_5)); }
	inline Animation_t2068071072 * get_m_Anim_5() const { return ___m_Anim_5; }
	inline Animation_t2068071072 ** get_address_of_m_Anim_5() { return &___m_Anim_5; }
	inline void set_m_Anim_5(Animation_t2068071072 * value)
	{
		___m_Anim_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Anim_5, value);
	}

	inline static int32_t get_offset_of_m_ModuleAnim_6() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_ModuleAnim_6)); }
	inline AnimationClip_t3510324950 * get_m_ModuleAnim_6() const { return ___m_ModuleAnim_6; }
	inline AnimationClip_t3510324950 ** get_address_of_m_ModuleAnim_6() { return &___m_ModuleAnim_6; }
	inline void set_m_ModuleAnim_6(AnimationClip_t3510324950 * value)
	{
		___m_ModuleAnim_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_ModuleAnim_6, value);
	}

	inline static int32_t get_offset_of_m_ModuleReverseAnim_7() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_ModuleReverseAnim_7)); }
	inline AnimationClip_t3510324950 * get_m_ModuleReverseAnim_7() const { return ___m_ModuleReverseAnim_7; }
	inline AnimationClip_t3510324950 ** get_address_of_m_ModuleReverseAnim_7() { return &___m_ModuleReverseAnim_7; }
	inline void set_m_ModuleReverseAnim_7(AnimationClip_t3510324950 * value)
	{
		___m_ModuleReverseAnim_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_ModuleReverseAnim_7, value);
	}

	inline static int32_t get_offset_of_m_AnimCurveDic_8() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_AnimCurveDic_8)); }
	inline Dictionary_2_t926353117 * get_m_AnimCurveDic_8() const { return ___m_AnimCurveDic_8; }
	inline Dictionary_2_t926353117 ** get_address_of_m_AnimCurveDic_8() { return &___m_AnimCurveDic_8; }
	inline void set_m_AnimCurveDic_8(Dictionary_2_t926353117 * value)
	{
		___m_AnimCurveDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_AnimCurveDic_8, value);
	}

	inline static int32_t get_offset_of_m_ReverseCurveDic_9() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_ReverseCurveDic_9)); }
	inline Dictionary_2_t926353117 * get_m_ReverseCurveDic_9() const { return ___m_ReverseCurveDic_9; }
	inline Dictionary_2_t926353117 ** get_address_of_m_ReverseCurveDic_9() { return &___m_ReverseCurveDic_9; }
	inline void set_m_ReverseCurveDic_9(Dictionary_2_t926353117 * value)
	{
		___m_ReverseCurveDic_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReverseCurveDic_9, value);
	}

	inline static int32_t get_offset_of_m_pDefaultCurve_10() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_pDefaultCurve_10)); }
	inline Dictionary_2_t926353117 * get_m_pDefaultCurve_10() const { return ___m_pDefaultCurve_10; }
	inline Dictionary_2_t926353117 ** get_address_of_m_pDefaultCurve_10() { return &___m_pDefaultCurve_10; }
	inline void set_m_pDefaultCurve_10(Dictionary_2_t926353117 * value)
	{
		___m_pDefaultCurve_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_pDefaultCurve_10, value);
	}

	inline static int32_t get_offset_of_m_DefaultClip_11() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_DefaultClip_11)); }
	inline AnimationClip_t3510324950 * get_m_DefaultClip_11() const { return ___m_DefaultClip_11; }
	inline AnimationClip_t3510324950 ** get_address_of_m_DefaultClip_11() { return &___m_DefaultClip_11; }
	inline void set_m_DefaultClip_11(AnimationClip_t3510324950 * value)
	{
		___m_DefaultClip_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_DefaultClip_11, value);
	}

	inline static int32_t get_offset_of_iconKey_12() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___iconKey_12)); }
	inline String_t* get_iconKey_12() const { return ___iconKey_12; }
	inline String_t** get_address_of_iconKey_12() { return &___iconKey_12; }
	inline void set_iconKey_12(String_t* value)
	{
		___iconKey_12 = value;
		Il2CppCodeGenWriteBarrier(&___iconKey_12, value);
	}

	inline static int32_t get_offset_of_m_pEndPoseCurve_13() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_pEndPoseCurve_13)); }
	inline Dictionary_2_t926353117 * get_m_pEndPoseCurve_13() const { return ___m_pEndPoseCurve_13; }
	inline Dictionary_2_t926353117 ** get_address_of_m_pEndPoseCurve_13() { return &___m_pEndPoseCurve_13; }
	inline void set_m_pEndPoseCurve_13(Dictionary_2_t926353117 * value)
	{
		___m_pEndPoseCurve_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_pEndPoseCurve_13, value);
	}

	inline static int32_t get_offset_of_m_EndPoseClip_14() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_EndPoseClip_14)); }
	inline AnimationClip_t3510324950 * get_m_EndPoseClip_14() const { return ___m_EndPoseClip_14; }
	inline AnimationClip_t3510324950 ** get_address_of_m_EndPoseClip_14() { return &___m_EndPoseClip_14; }
	inline void set_m_EndPoseClip_14(AnimationClip_t3510324950 * value)
	{
		___m_EndPoseClip_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_EndPoseClip_14, value);
	}

	inline static int32_t get_offset_of_PlayModeNum_15() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___PlayModeNum_15)); }
	inline int32_t get_PlayModeNum_15() const { return ___PlayModeNum_15; }
	inline int32_t* get_address_of_PlayModeNum_15() { return &___PlayModeNum_15; }
	inline void set_PlayModeNum_15(int32_t value)
	{
		___PlayModeNum_15 = value;
	}

	inline static int32_t get_offset_of_m_bIsActionPlay_16() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_bIsActionPlay_16)); }
	inline bool get_m_bIsActionPlay_16() const { return ___m_bIsActionPlay_16; }
	inline bool* get_address_of_m_bIsActionPlay_16() { return &___m_bIsActionPlay_16; }
	inline void set_m_bIsActionPlay_16(bool value)
	{
		___m_bIsActionPlay_16 = value;
	}

	inline static int32_t get_offset_of_m_bisTestAnim_17() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_bisTestAnim_17)); }
	inline bool get_m_bisTestAnim_17() const { return ___m_bisTestAnim_17; }
	inline bool* get_address_of_m_bisTestAnim_17() { return &___m_bisTestAnim_17; }
	inline void set_m_bisTestAnim_17(bool value)
	{
		___m_bisTestAnim_17 = value;
	}

	inline static int32_t get_offset_of_m_bIsJointConnect_18() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_bIsJointConnect_18)); }
	inline bool get_m_bIsJointConnect_18() const { return ___m_bIsJointConnect_18; }
	inline bool* get_address_of_m_bIsJointConnect_18() { return &___m_bIsJointConnect_18; }
	inline void set_m_bIsJointConnect_18(bool value)
	{
		___m_bIsJointConnect_18 = value;
	}

	inline static int32_t get_offset_of_moduleJson_19() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___moduleJson_19)); }
	inline String_t* get_moduleJson_19() const { return ___moduleJson_19; }
	inline String_t** get_address_of_moduleJson_19() { return &___moduleJson_19; }
	inline void set_moduleJson_19(String_t* value)
	{
		___moduleJson_19 = value;
		Il2CppCodeGenWriteBarrier(&___moduleJson_19, value);
	}

	inline static int32_t get_offset_of_m_moduleId_20() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_moduleId_20)); }
	inline String_t* get_m_moduleId_20() const { return ___m_moduleId_20; }
	inline String_t** get_address_of_m_moduleId_20() { return &___m_moduleId_20; }
	inline void set_m_moduleId_20(String_t* value)
	{
		___m_moduleId_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_moduleId_20, value);
	}

	inline static int32_t get_offset_of_m_moduleAnimId_21() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_moduleAnimId_21)); }
	inline String_t* get_m_moduleAnimId_21() const { return ___m_moduleAnimId_21; }
	inline String_t** get_address_of_m_moduleAnimId_21() { return &___m_moduleAnimId_21; }
	inline void set_m_moduleAnimId_21(String_t* value)
	{
		___m_moduleAnimId_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_moduleAnimId_21, value);
	}

	inline static int32_t get_offset_of_m_parentModuleNo_22() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_parentModuleNo_22)); }
	inline int32_t get_m_parentModuleNo_22() const { return ___m_parentModuleNo_22; }
	inline int32_t* get_address_of_m_parentModuleNo_22() { return &___m_parentModuleNo_22; }
	inline void set_m_parentModuleNo_22(int32_t value)
	{
		___m_parentModuleNo_22 = value;
	}

	inline static int32_t get_offset_of_m_ModuleNo_23() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___m_ModuleNo_23)); }
	inline int32_t get_m_ModuleNo_23() const { return ___m_ModuleNo_23; }
	inline int32_t* get_address_of_m_ModuleNo_23() { return &___m_ModuleNo_23; }
	inline void set_m_ModuleNo_23(int32_t value)
	{
		___m_ModuleNo_23 = value;
	}

	inline static int32_t get_offset_of_childPartsName_24() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___childPartsName_24)); }
	inline String_t* get_childPartsName_24() const { return ___childPartsName_24; }
	inline String_t** get_address_of_childPartsName_24() { return &___childPartsName_24; }
	inline void set_childPartsName_24(String_t* value)
	{
		___childPartsName_24 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsName_24, value);
	}

	inline static int32_t get_offset_of_recModule_25() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___recModule_25)); }
	inline bool get_recModule_25() const { return ___recModule_25; }
	inline bool* get_address_of_recModule_25() { return &___recModule_25; }
	inline void set_recModule_25(bool value)
	{
		___recModule_25 = value;
	}

	inline static int32_t get_offset_of_ParentMobilmo_26() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___ParentMobilmo_26)); }
	inline Mobilmo_t370754809 * get_ParentMobilmo_26() const { return ___ParentMobilmo_26; }
	inline Mobilmo_t370754809 ** get_address_of_ParentMobilmo_26() { return &___ParentMobilmo_26; }
	inline void set_ParentMobilmo_26(Mobilmo_t370754809 * value)
	{
		___ParentMobilmo_26 = value;
		Il2CppCodeGenWriteBarrier(&___ParentMobilmo_26, value);
	}

	inline static int32_t get_offset_of_parentMobilmoId_27() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___parentMobilmoId_27)); }
	inline int32_t get_parentMobilmoId_27() const { return ___parentMobilmoId_27; }
	inline int32_t* get_address_of_parentMobilmoId_27() { return &___parentMobilmoId_27; }
	inline void set_parentMobilmoId_27(int32_t value)
	{
		___parentMobilmoId_27 = value;
	}

	inline static int32_t get_offset_of_JointChildParts_28() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___JointChildParts_28)); }
	inline Transform_t3275118058 * get_JointChildParts_28() const { return ___JointChildParts_28; }
	inline Transform_t3275118058 ** get_address_of_JointChildParts_28() { return &___JointChildParts_28; }
	inline void set_JointChildParts_28(Transform_t3275118058 * value)
	{
		___JointChildParts_28 = value;
		Il2CppCodeGenWriteBarrier(&___JointChildParts_28, value);
	}

	inline static int32_t get_offset_of_moduleColorId_29() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___moduleColorId_29)); }
	inline int32_t get_moduleColorId_29() const { return ___moduleColorId_29; }
	inline int32_t* get_address_of_moduleColorId_29() { return &___moduleColorId_29; }
	inline void set_moduleColorId_29(int32_t value)
	{
		___moduleColorId_29 = value;
	}

	inline static int32_t get_offset_of_cloneModuleCreateUserId_30() { return static_cast<int32_t>(offsetof(Module_t3140434828, ___cloneModuleCreateUserId_30)); }
	inline int32_t get_cloneModuleCreateUserId_30() const { return ___cloneModuleCreateUserId_30; }
	inline int32_t* get_address_of_cloneModuleCreateUserId_30() { return &___cloneModuleCreateUserId_30; }
	inline void set_cloneModuleCreateUserId_30(int32_t value)
	{
		___cloneModuleCreateUserId_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
