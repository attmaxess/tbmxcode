﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen4057088938.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// ActionPartsPanelController
struct ActionPartsPanelController_t3710630626;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager
struct  ModuleManager_t1065445307  : public SingletonMonoBehaviour_1_t4057088938
{
public:
	// UnityEngine.GameObject ModuleManager::moduleObj
	GameObject_t1756533147 * ___moduleObj_3;
	// UnityEngine.GameObject ModuleManager::corePartsObj
	GameObject_t1756533147 * ___corePartsObj_4;
	// UnityEngine.GameObject ModuleManager::_moduleObject
	GameObject_t1756533147 * ____moduleObject_5;
	// UnityEngine.GameObject ModuleManager::m_getModuleObject
	GameObject_t1756533147 * ___m_getModuleObject_6;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> ModuleManager::moduleDic
	Dictionary_2_t3671312409 * ___moduleDic_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> ModuleManager::defaultModuleDic
	Dictionary_2_t3671312409 * ___defaultModuleDic_8;
	// System.String ModuleManager::json
	String_t* ___json_9;
	// UnityEngine.GameObject ModuleManager::myModuleArea
	GameObject_t1756533147 * ___myModuleArea_10;
	// UnityEngine.GameObject ModuleManager::actionPartsPanel
	GameObject_t1756533147 * ___actionPartsPanel_11;
	// UnityEngine.GameObject ModuleManager::selectModuleArea
	GameObject_t1756533147 * ___selectModuleArea_12;
	// UnityEngine.GameObject ModuleManager::selectActionPartsPanel
	GameObject_t1756533147 * ___selectActionPartsPanel_13;
	// UnityEngine.GameObject ModuleManager::dnaActionPartsArea
	GameObject_t1756533147 * ___dnaActionPartsArea_14;
	// UnityEngine.Transform ModuleManager::dnaActionPartsContent
	Transform_t3275118058 * ___dnaActionPartsContent_15;
	// System.Int32 ModuleManager::modulePoseDataCount
	int32_t ___modulePoseDataCount_16;
	// UnityEngine.TextAsset ModuleManager::defJsonText
	TextAsset_t3973159845 * ___defJsonText_17;
	// System.String[] ModuleManager::defualtModuleJsons
	StringU5BU5D_t1642385972* ___defualtModuleJsons_18;
	// UnityEngine.GameObject ModuleManager::editActionPartsArea
	GameObject_t1756533147 * ___editActionPartsArea_19;
	// UnityEngine.GameObject ModuleManager::defualtModuleObj
	GameObject_t1756533147 * ___defualtModuleObj_20;
	// UnityEngine.GameObject ModuleManager::createdModuleObj
	GameObject_t1756533147 * ___createdModuleObj_21;
	// UnityEngine.GameObject ModuleManager::editModuleObj
	GameObject_t1756533147 * ___editModuleObj_22;
	// UnityEngine.GameObject ModuleManager::moduleCoreParts
	GameObject_t1756533147 * ___moduleCoreParts_23;
	// System.Boolean ModuleManager::isCreatingNewModule
	bool ___isCreatingNewModule_24;
	// System.Boolean ModuleManager::isPlayingInActionPanel
	bool ___isPlayingInActionPanel_25;
	// System.Boolean ModuleManager::isPlayingInCreatePanel
	bool ___isPlayingInCreatePanel_26;
	// System.Int32 ModuleManager::selectModuleId
	int32_t ___selectModuleId_27;
	// UnityEngine.GameObject ModuleManager::getModuleArea
	GameObject_t1756533147 * ___getModuleArea_28;
	// UnityEngine.Transform ModuleManager::getModulePanelContent
	Transform_t3275118058 * ___getModulePanelContent_29;
	// System.Int32 ModuleManager::getModuleCnt
	int32_t ___getModuleCnt_30;
	// System.Collections.Generic.List`1<System.String> ModuleManager::moduleJsonList
	List_1_t1398341365 * ___moduleJsonList_31;
	// System.Boolean ModuleManager::isClickedActionParts
	bool ___isClickedActionParts_32;
	// ActionPartsPanelController ModuleManager::m_actionPartsPanelController
	ActionPartsPanelController_t3710630626 * ___m_actionPartsPanelController_33;
	// System.Boolean ModuleManager::isDeletedModule
	bool ___isDeletedModule_34;
	// UnityEngine.Vector3 ModuleManager::moduleScale
	Vector3_t2243707580  ___moduleScale_35;
	// UnityEngine.GameObject[] ModuleManager::actionPartsListMenuButtons
	GameObjectU5BU5D_t3057952154* ___actionPartsListMenuButtons_36;
	// System.Int32 ModuleManager::cnt
	int32_t ___cnt_37;
	// System.Int32 ModuleManager::partsCount
	int32_t ___partsCount_38;
	// UnityEngine.UI.Image ModuleManager::m_oldAreaImage
	Image_t2042527209 * ___m_oldAreaImage_39;

public:
	inline static int32_t get_offset_of_moduleObj_3() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___moduleObj_3)); }
	inline GameObject_t1756533147 * get_moduleObj_3() const { return ___moduleObj_3; }
	inline GameObject_t1756533147 ** get_address_of_moduleObj_3() { return &___moduleObj_3; }
	inline void set_moduleObj_3(GameObject_t1756533147 * value)
	{
		___moduleObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___moduleObj_3, value);
	}

	inline static int32_t get_offset_of_corePartsObj_4() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___corePartsObj_4)); }
	inline GameObject_t1756533147 * get_corePartsObj_4() const { return ___corePartsObj_4; }
	inline GameObject_t1756533147 ** get_address_of_corePartsObj_4() { return &___corePartsObj_4; }
	inline void set_corePartsObj_4(GameObject_t1756533147 * value)
	{
		___corePartsObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___corePartsObj_4, value);
	}

	inline static int32_t get_offset_of__moduleObject_5() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ____moduleObject_5)); }
	inline GameObject_t1756533147 * get__moduleObject_5() const { return ____moduleObject_5; }
	inline GameObject_t1756533147 ** get_address_of__moduleObject_5() { return &____moduleObject_5; }
	inline void set__moduleObject_5(GameObject_t1756533147 * value)
	{
		____moduleObject_5 = value;
		Il2CppCodeGenWriteBarrier(&____moduleObject_5, value);
	}

	inline static int32_t get_offset_of_m_getModuleObject_6() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___m_getModuleObject_6)); }
	inline GameObject_t1756533147 * get_m_getModuleObject_6() const { return ___m_getModuleObject_6; }
	inline GameObject_t1756533147 ** get_address_of_m_getModuleObject_6() { return &___m_getModuleObject_6; }
	inline void set_m_getModuleObject_6(GameObject_t1756533147 * value)
	{
		___m_getModuleObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_getModuleObject_6, value);
	}

	inline static int32_t get_offset_of_moduleDic_7() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___moduleDic_7)); }
	inline Dictionary_2_t3671312409 * get_moduleDic_7() const { return ___moduleDic_7; }
	inline Dictionary_2_t3671312409 ** get_address_of_moduleDic_7() { return &___moduleDic_7; }
	inline void set_moduleDic_7(Dictionary_2_t3671312409 * value)
	{
		___moduleDic_7 = value;
		Il2CppCodeGenWriteBarrier(&___moduleDic_7, value);
	}

	inline static int32_t get_offset_of_defaultModuleDic_8() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___defaultModuleDic_8)); }
	inline Dictionary_2_t3671312409 * get_defaultModuleDic_8() const { return ___defaultModuleDic_8; }
	inline Dictionary_2_t3671312409 ** get_address_of_defaultModuleDic_8() { return &___defaultModuleDic_8; }
	inline void set_defaultModuleDic_8(Dictionary_2_t3671312409 * value)
	{
		___defaultModuleDic_8 = value;
		Il2CppCodeGenWriteBarrier(&___defaultModuleDic_8, value);
	}

	inline static int32_t get_offset_of_json_9() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___json_9)); }
	inline String_t* get_json_9() const { return ___json_9; }
	inline String_t** get_address_of_json_9() { return &___json_9; }
	inline void set_json_9(String_t* value)
	{
		___json_9 = value;
		Il2CppCodeGenWriteBarrier(&___json_9, value);
	}

	inline static int32_t get_offset_of_myModuleArea_10() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___myModuleArea_10)); }
	inline GameObject_t1756533147 * get_myModuleArea_10() const { return ___myModuleArea_10; }
	inline GameObject_t1756533147 ** get_address_of_myModuleArea_10() { return &___myModuleArea_10; }
	inline void set_myModuleArea_10(GameObject_t1756533147 * value)
	{
		___myModuleArea_10 = value;
		Il2CppCodeGenWriteBarrier(&___myModuleArea_10, value);
	}

	inline static int32_t get_offset_of_actionPartsPanel_11() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___actionPartsPanel_11)); }
	inline GameObject_t1756533147 * get_actionPartsPanel_11() const { return ___actionPartsPanel_11; }
	inline GameObject_t1756533147 ** get_address_of_actionPartsPanel_11() { return &___actionPartsPanel_11; }
	inline void set_actionPartsPanel_11(GameObject_t1756533147 * value)
	{
		___actionPartsPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___actionPartsPanel_11, value);
	}

	inline static int32_t get_offset_of_selectModuleArea_12() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___selectModuleArea_12)); }
	inline GameObject_t1756533147 * get_selectModuleArea_12() const { return ___selectModuleArea_12; }
	inline GameObject_t1756533147 ** get_address_of_selectModuleArea_12() { return &___selectModuleArea_12; }
	inline void set_selectModuleArea_12(GameObject_t1756533147 * value)
	{
		___selectModuleArea_12 = value;
		Il2CppCodeGenWriteBarrier(&___selectModuleArea_12, value);
	}

	inline static int32_t get_offset_of_selectActionPartsPanel_13() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___selectActionPartsPanel_13)); }
	inline GameObject_t1756533147 * get_selectActionPartsPanel_13() const { return ___selectActionPartsPanel_13; }
	inline GameObject_t1756533147 ** get_address_of_selectActionPartsPanel_13() { return &___selectActionPartsPanel_13; }
	inline void set_selectActionPartsPanel_13(GameObject_t1756533147 * value)
	{
		___selectActionPartsPanel_13 = value;
		Il2CppCodeGenWriteBarrier(&___selectActionPartsPanel_13, value);
	}

	inline static int32_t get_offset_of_dnaActionPartsArea_14() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___dnaActionPartsArea_14)); }
	inline GameObject_t1756533147 * get_dnaActionPartsArea_14() const { return ___dnaActionPartsArea_14; }
	inline GameObject_t1756533147 ** get_address_of_dnaActionPartsArea_14() { return &___dnaActionPartsArea_14; }
	inline void set_dnaActionPartsArea_14(GameObject_t1756533147 * value)
	{
		___dnaActionPartsArea_14 = value;
		Il2CppCodeGenWriteBarrier(&___dnaActionPartsArea_14, value);
	}

	inline static int32_t get_offset_of_dnaActionPartsContent_15() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___dnaActionPartsContent_15)); }
	inline Transform_t3275118058 * get_dnaActionPartsContent_15() const { return ___dnaActionPartsContent_15; }
	inline Transform_t3275118058 ** get_address_of_dnaActionPartsContent_15() { return &___dnaActionPartsContent_15; }
	inline void set_dnaActionPartsContent_15(Transform_t3275118058 * value)
	{
		___dnaActionPartsContent_15 = value;
		Il2CppCodeGenWriteBarrier(&___dnaActionPartsContent_15, value);
	}

	inline static int32_t get_offset_of_modulePoseDataCount_16() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___modulePoseDataCount_16)); }
	inline int32_t get_modulePoseDataCount_16() const { return ___modulePoseDataCount_16; }
	inline int32_t* get_address_of_modulePoseDataCount_16() { return &___modulePoseDataCount_16; }
	inline void set_modulePoseDataCount_16(int32_t value)
	{
		___modulePoseDataCount_16 = value;
	}

	inline static int32_t get_offset_of_defJsonText_17() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___defJsonText_17)); }
	inline TextAsset_t3973159845 * get_defJsonText_17() const { return ___defJsonText_17; }
	inline TextAsset_t3973159845 ** get_address_of_defJsonText_17() { return &___defJsonText_17; }
	inline void set_defJsonText_17(TextAsset_t3973159845 * value)
	{
		___defJsonText_17 = value;
		Il2CppCodeGenWriteBarrier(&___defJsonText_17, value);
	}

	inline static int32_t get_offset_of_defualtModuleJsons_18() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___defualtModuleJsons_18)); }
	inline StringU5BU5D_t1642385972* get_defualtModuleJsons_18() const { return ___defualtModuleJsons_18; }
	inline StringU5BU5D_t1642385972** get_address_of_defualtModuleJsons_18() { return &___defualtModuleJsons_18; }
	inline void set_defualtModuleJsons_18(StringU5BU5D_t1642385972* value)
	{
		___defualtModuleJsons_18 = value;
		Il2CppCodeGenWriteBarrier(&___defualtModuleJsons_18, value);
	}

	inline static int32_t get_offset_of_editActionPartsArea_19() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___editActionPartsArea_19)); }
	inline GameObject_t1756533147 * get_editActionPartsArea_19() const { return ___editActionPartsArea_19; }
	inline GameObject_t1756533147 ** get_address_of_editActionPartsArea_19() { return &___editActionPartsArea_19; }
	inline void set_editActionPartsArea_19(GameObject_t1756533147 * value)
	{
		___editActionPartsArea_19 = value;
		Il2CppCodeGenWriteBarrier(&___editActionPartsArea_19, value);
	}

	inline static int32_t get_offset_of_defualtModuleObj_20() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___defualtModuleObj_20)); }
	inline GameObject_t1756533147 * get_defualtModuleObj_20() const { return ___defualtModuleObj_20; }
	inline GameObject_t1756533147 ** get_address_of_defualtModuleObj_20() { return &___defualtModuleObj_20; }
	inline void set_defualtModuleObj_20(GameObject_t1756533147 * value)
	{
		___defualtModuleObj_20 = value;
		Il2CppCodeGenWriteBarrier(&___defualtModuleObj_20, value);
	}

	inline static int32_t get_offset_of_createdModuleObj_21() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___createdModuleObj_21)); }
	inline GameObject_t1756533147 * get_createdModuleObj_21() const { return ___createdModuleObj_21; }
	inline GameObject_t1756533147 ** get_address_of_createdModuleObj_21() { return &___createdModuleObj_21; }
	inline void set_createdModuleObj_21(GameObject_t1756533147 * value)
	{
		___createdModuleObj_21 = value;
		Il2CppCodeGenWriteBarrier(&___createdModuleObj_21, value);
	}

	inline static int32_t get_offset_of_editModuleObj_22() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___editModuleObj_22)); }
	inline GameObject_t1756533147 * get_editModuleObj_22() const { return ___editModuleObj_22; }
	inline GameObject_t1756533147 ** get_address_of_editModuleObj_22() { return &___editModuleObj_22; }
	inline void set_editModuleObj_22(GameObject_t1756533147 * value)
	{
		___editModuleObj_22 = value;
		Il2CppCodeGenWriteBarrier(&___editModuleObj_22, value);
	}

	inline static int32_t get_offset_of_moduleCoreParts_23() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___moduleCoreParts_23)); }
	inline GameObject_t1756533147 * get_moduleCoreParts_23() const { return ___moduleCoreParts_23; }
	inline GameObject_t1756533147 ** get_address_of_moduleCoreParts_23() { return &___moduleCoreParts_23; }
	inline void set_moduleCoreParts_23(GameObject_t1756533147 * value)
	{
		___moduleCoreParts_23 = value;
		Il2CppCodeGenWriteBarrier(&___moduleCoreParts_23, value);
	}

	inline static int32_t get_offset_of_isCreatingNewModule_24() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___isCreatingNewModule_24)); }
	inline bool get_isCreatingNewModule_24() const { return ___isCreatingNewModule_24; }
	inline bool* get_address_of_isCreatingNewModule_24() { return &___isCreatingNewModule_24; }
	inline void set_isCreatingNewModule_24(bool value)
	{
		___isCreatingNewModule_24 = value;
	}

	inline static int32_t get_offset_of_isPlayingInActionPanel_25() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___isPlayingInActionPanel_25)); }
	inline bool get_isPlayingInActionPanel_25() const { return ___isPlayingInActionPanel_25; }
	inline bool* get_address_of_isPlayingInActionPanel_25() { return &___isPlayingInActionPanel_25; }
	inline void set_isPlayingInActionPanel_25(bool value)
	{
		___isPlayingInActionPanel_25 = value;
	}

	inline static int32_t get_offset_of_isPlayingInCreatePanel_26() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___isPlayingInCreatePanel_26)); }
	inline bool get_isPlayingInCreatePanel_26() const { return ___isPlayingInCreatePanel_26; }
	inline bool* get_address_of_isPlayingInCreatePanel_26() { return &___isPlayingInCreatePanel_26; }
	inline void set_isPlayingInCreatePanel_26(bool value)
	{
		___isPlayingInCreatePanel_26 = value;
	}

	inline static int32_t get_offset_of_selectModuleId_27() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___selectModuleId_27)); }
	inline int32_t get_selectModuleId_27() const { return ___selectModuleId_27; }
	inline int32_t* get_address_of_selectModuleId_27() { return &___selectModuleId_27; }
	inline void set_selectModuleId_27(int32_t value)
	{
		___selectModuleId_27 = value;
	}

	inline static int32_t get_offset_of_getModuleArea_28() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___getModuleArea_28)); }
	inline GameObject_t1756533147 * get_getModuleArea_28() const { return ___getModuleArea_28; }
	inline GameObject_t1756533147 ** get_address_of_getModuleArea_28() { return &___getModuleArea_28; }
	inline void set_getModuleArea_28(GameObject_t1756533147 * value)
	{
		___getModuleArea_28 = value;
		Il2CppCodeGenWriteBarrier(&___getModuleArea_28, value);
	}

	inline static int32_t get_offset_of_getModulePanelContent_29() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___getModulePanelContent_29)); }
	inline Transform_t3275118058 * get_getModulePanelContent_29() const { return ___getModulePanelContent_29; }
	inline Transform_t3275118058 ** get_address_of_getModulePanelContent_29() { return &___getModulePanelContent_29; }
	inline void set_getModulePanelContent_29(Transform_t3275118058 * value)
	{
		___getModulePanelContent_29 = value;
		Il2CppCodeGenWriteBarrier(&___getModulePanelContent_29, value);
	}

	inline static int32_t get_offset_of_getModuleCnt_30() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___getModuleCnt_30)); }
	inline int32_t get_getModuleCnt_30() const { return ___getModuleCnt_30; }
	inline int32_t* get_address_of_getModuleCnt_30() { return &___getModuleCnt_30; }
	inline void set_getModuleCnt_30(int32_t value)
	{
		___getModuleCnt_30 = value;
	}

	inline static int32_t get_offset_of_moduleJsonList_31() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___moduleJsonList_31)); }
	inline List_1_t1398341365 * get_moduleJsonList_31() const { return ___moduleJsonList_31; }
	inline List_1_t1398341365 ** get_address_of_moduleJsonList_31() { return &___moduleJsonList_31; }
	inline void set_moduleJsonList_31(List_1_t1398341365 * value)
	{
		___moduleJsonList_31 = value;
		Il2CppCodeGenWriteBarrier(&___moduleJsonList_31, value);
	}

	inline static int32_t get_offset_of_isClickedActionParts_32() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___isClickedActionParts_32)); }
	inline bool get_isClickedActionParts_32() const { return ___isClickedActionParts_32; }
	inline bool* get_address_of_isClickedActionParts_32() { return &___isClickedActionParts_32; }
	inline void set_isClickedActionParts_32(bool value)
	{
		___isClickedActionParts_32 = value;
	}

	inline static int32_t get_offset_of_m_actionPartsPanelController_33() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___m_actionPartsPanelController_33)); }
	inline ActionPartsPanelController_t3710630626 * get_m_actionPartsPanelController_33() const { return ___m_actionPartsPanelController_33; }
	inline ActionPartsPanelController_t3710630626 ** get_address_of_m_actionPartsPanelController_33() { return &___m_actionPartsPanelController_33; }
	inline void set_m_actionPartsPanelController_33(ActionPartsPanelController_t3710630626 * value)
	{
		___m_actionPartsPanelController_33 = value;
		Il2CppCodeGenWriteBarrier(&___m_actionPartsPanelController_33, value);
	}

	inline static int32_t get_offset_of_isDeletedModule_34() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___isDeletedModule_34)); }
	inline bool get_isDeletedModule_34() const { return ___isDeletedModule_34; }
	inline bool* get_address_of_isDeletedModule_34() { return &___isDeletedModule_34; }
	inline void set_isDeletedModule_34(bool value)
	{
		___isDeletedModule_34 = value;
	}

	inline static int32_t get_offset_of_moduleScale_35() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___moduleScale_35)); }
	inline Vector3_t2243707580  get_moduleScale_35() const { return ___moduleScale_35; }
	inline Vector3_t2243707580 * get_address_of_moduleScale_35() { return &___moduleScale_35; }
	inline void set_moduleScale_35(Vector3_t2243707580  value)
	{
		___moduleScale_35 = value;
	}

	inline static int32_t get_offset_of_actionPartsListMenuButtons_36() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___actionPartsListMenuButtons_36)); }
	inline GameObjectU5BU5D_t3057952154* get_actionPartsListMenuButtons_36() const { return ___actionPartsListMenuButtons_36; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_actionPartsListMenuButtons_36() { return &___actionPartsListMenuButtons_36; }
	inline void set_actionPartsListMenuButtons_36(GameObjectU5BU5D_t3057952154* value)
	{
		___actionPartsListMenuButtons_36 = value;
		Il2CppCodeGenWriteBarrier(&___actionPartsListMenuButtons_36, value);
	}

	inline static int32_t get_offset_of_cnt_37() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___cnt_37)); }
	inline int32_t get_cnt_37() const { return ___cnt_37; }
	inline int32_t* get_address_of_cnt_37() { return &___cnt_37; }
	inline void set_cnt_37(int32_t value)
	{
		___cnt_37 = value;
	}

	inline static int32_t get_offset_of_partsCount_38() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___partsCount_38)); }
	inline int32_t get_partsCount_38() const { return ___partsCount_38; }
	inline int32_t* get_address_of_partsCount_38() { return &___partsCount_38; }
	inline void set_partsCount_38(int32_t value)
	{
		___partsCount_38 = value;
	}

	inline static int32_t get_offset_of_m_oldAreaImage_39() { return static_cast<int32_t>(offsetof(ModuleManager_t1065445307, ___m_oldAreaImage_39)); }
	inline Image_t2042527209 * get_m_oldAreaImage_39() const { return ___m_oldAreaImage_39; }
	inline Image_t2042527209 ** get_address_of_m_oldAreaImage_39() { return &___m_oldAreaImage_39; }
	inline void set_m_oldAreaImage_39(Image_t2042527209 * value)
	{
		___m_oldAreaImage_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_oldAreaImage_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
