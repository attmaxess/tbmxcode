﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// ModuleManager
struct ModuleManager_t1065445307;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager/<WaitCreateDefaultModule>c__Iterator0
struct  U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157  : public Il2CppObject
{
public:
	// System.String[] ModuleManager/<WaitCreateDefaultModule>c__Iterator0::$locvar0
	StringU5BU5D_t1642385972* ___U24locvar0_0;
	// System.Int32 ModuleManager/<WaitCreateDefaultModule>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.String ModuleManager/<WaitCreateDefaultModule>c__Iterator0::<json>__1
	String_t* ___U3CjsonU3E__1_2;
	// ModuleManager ModuleManager/<WaitCreateDefaultModule>c__Iterator0::$this
	ModuleManager_t1065445307 * ___U24this_3;
	// System.Object ModuleManager/<WaitCreateDefaultModule>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean ModuleManager/<WaitCreateDefaultModule>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 ModuleManager/<WaitCreateDefaultModule>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157, ___U24locvar0_0)); }
	inline StringU5BU5D_t1642385972* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(StringU5BU5D_t1642385972* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_0, value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_2() { return static_cast<int32_t>(offsetof(U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157, ___U3CjsonU3E__1_2)); }
	inline String_t* get_U3CjsonU3E__1_2() const { return ___U3CjsonU3E__1_2; }
	inline String_t** get_address_of_U3CjsonU3E__1_2() { return &___U3CjsonU3E__1_2; }
	inline void set_U3CjsonU3E__1_2(String_t* value)
	{
		___U3CjsonU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157, ___U24this_3)); }
	inline ModuleManager_t1065445307 * get_U24this_3() const { return ___U24this_3; }
	inline ModuleManager_t1065445307 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ModuleManager_t1065445307 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
