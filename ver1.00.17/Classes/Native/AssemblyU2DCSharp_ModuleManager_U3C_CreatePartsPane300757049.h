﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// ModuleManager
struct ModuleManager_t1065445307;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager/<_CreatePartsPanelScrollObject>c__AnonStorey9
struct  U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049  : public Il2CppObject
{
public:
	// UnityEngine.GameObject ModuleManager/<_CreatePartsPanelScrollObject>c__AnonStorey9::actionPartsObj
	GameObject_t1756533147 * ___actionPartsObj_0;
	// UnityEngine.GameObject ModuleManager/<_CreatePartsPanelScrollObject>c__AnonStorey9::obj
	GameObject_t1756533147 * ___obj_1;
	// ModuleManager ModuleManager/<_CreatePartsPanelScrollObject>c__AnonStorey9::$this
	ModuleManager_t1065445307 * ___U24this_2;

public:
	inline static int32_t get_offset_of_actionPartsObj_0() { return static_cast<int32_t>(offsetof(U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049, ___actionPartsObj_0)); }
	inline GameObject_t1756533147 * get_actionPartsObj_0() const { return ___actionPartsObj_0; }
	inline GameObject_t1756533147 ** get_address_of_actionPartsObj_0() { return &___actionPartsObj_0; }
	inline void set_actionPartsObj_0(GameObject_t1756533147 * value)
	{
		___actionPartsObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___actionPartsObj_0, value);
	}

	inline static int32_t get_offset_of_obj_1() { return static_cast<int32_t>(offsetof(U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049, ___obj_1)); }
	inline GameObject_t1756533147 * get_obj_1() const { return ___obj_1; }
	inline GameObject_t1756533147 ** get_address_of_obj_1() { return &___obj_1; }
	inline void set_obj_1(GameObject_t1756533147 * value)
	{
		___obj_1 = value;
		Il2CppCodeGenWriteBarrier(&___obj_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049, ___U24this_2)); }
	inline ModuleManager_t1065445307 * get_U24this_2() const { return ___U24this_2; }
	inline ModuleManager_t1065445307 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ModuleManager_t1065445307 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
