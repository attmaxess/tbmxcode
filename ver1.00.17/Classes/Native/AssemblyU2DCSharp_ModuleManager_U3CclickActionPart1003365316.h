﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;
// ModuleManager
struct ModuleManager_t1065445307;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager/<clickActionPartsInMyPage>c__Iterator5
struct  U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316  : public Il2CppObject
{
public:
	// UnityEngine.GameObject ModuleManager/<clickActionPartsInMyPage>c__Iterator5::obj
	GameObject_t1756533147 * ___obj_0;
	// UnityEngine.Transform[] ModuleManager/<clickActionPartsInMyPage>c__Iterator5::$locvar0
	TransformU5BU5D_t3764228911* ___U24locvar0_1;
	// System.Int32 ModuleManager/<clickActionPartsInMyPage>c__Iterator5::$locvar1
	int32_t ___U24locvar1_2;
	// UnityEngine.AnimationClip ModuleManager/<clickActionPartsInMyPage>c__Iterator5::<TestAnim>__0
	AnimationClip_t3510324950 * ___U3CTestAnimU3E__0_3;
	// ModuleManager ModuleManager/<clickActionPartsInMyPage>c__Iterator5::$this
	ModuleManager_t1065445307 * ___U24this_4;
	// System.Object ModuleManager/<clickActionPartsInMyPage>c__Iterator5::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean ModuleManager/<clickActionPartsInMyPage>c__Iterator5::$disposing
	bool ___U24disposing_6;
	// System.Int32 ModuleManager/<clickActionPartsInMyPage>c__Iterator5::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___obj_0)); }
	inline GameObject_t1756533147 * get_obj_0() const { return ___obj_0; }
	inline GameObject_t1756533147 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_t1756533147 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___U24locvar0_1)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CTestAnimU3E__0_3() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___U3CTestAnimU3E__0_3)); }
	inline AnimationClip_t3510324950 * get_U3CTestAnimU3E__0_3() const { return ___U3CTestAnimU3E__0_3; }
	inline AnimationClip_t3510324950 ** get_address_of_U3CTestAnimU3E__0_3() { return &___U3CTestAnimU3E__0_3; }
	inline void set_U3CTestAnimU3E__0_3(AnimationClip_t3510324950 * value)
	{
		___U3CTestAnimU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTestAnimU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___U24this_4)); }
	inline ModuleManager_t1065445307 * get_U24this_4() const { return ___U24this_4; }
	inline ModuleManager_t1065445307 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ModuleManager_t1065445307 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
