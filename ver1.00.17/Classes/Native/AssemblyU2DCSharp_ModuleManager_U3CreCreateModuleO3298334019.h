﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2463632671.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1213609547.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetModuleModel
struct GetModuleModel_t617428109;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// ModuleManager
struct ModuleManager_t1065445307;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7
struct  U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019  : public Il2CppObject
{
public:
	// System.String ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::json
	String_t* ___json_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_1;
	// GetModuleModel ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::<_moduleModel>__0
	GetModuleModel_t617428109 * ___U3C_moduleModelU3E__0_2;
	// UnityEngine.GameObject ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::<moduleObj>__1
	GameObject_t1756533147 * ___U3CmoduleObjU3E__1_3;
	// UnityEngine.GameObject ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::<childCoreObj>__1
	GameObject_t1756533147 * ___U3CchildCoreObjU3E__1_4;
	// UnityEngine.GameObject ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::<corePartsObj>__1
	GameObject_t1756533147 * ___U3CcorePartsObjU3E__1_5;
	// UnityEngine.Material ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::<coreMat>__1
	Material_t193706927 * ___U3CcoreMatU3E__1_6;
	// System.Boolean ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::isCreatingMobInMyPage
	bool ___isCreatingMobInMyPage_7;
	// System.Collections.Generic.List`1/Enumerator<GetModuleChildPartsListModel> ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$locvar0
	Enumerator_t2463632671  ___U24locvar0_8;
	// System.Boolean ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::isRecorded
	bool ___isRecorded_9;
	// UnityEngine.Transform[] ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::<partsTra>__1
	TransformU5BU5D_t3764228911* ___U3CpartsTraU3E__1_10;
	// UnityEngine.Transform[] ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$locvar3
	TransformU5BU5D_t3764228911* ___U24locvar3_11;
	// System.Int32 ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$locvar4
	int32_t ___U24locvar4_12;
	// System.Collections.Generic.List`1/Enumerator<GetModuleRegisterMotionListMotionData> ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$locvar5
	Enumerator_t1213609547  ___U24locvar5_13;
	// ModuleManager ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$this
	ModuleManager_t1065445307 * ___U24this_14;
	// System.Object ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$current
	Il2CppObject * ___U24current_15;
	// System.Boolean ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$disposing
	bool ___U24disposing_16;
	// System.Int32 ModuleManager/<reCreateModuleObjectInTitle>c__Iterator7::$PC
	int32_t ___U24PC_17;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___json_0)); }
	inline String_t* get_json_0() const { return ___json_0; }
	inline String_t** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(String_t* value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier(&___json_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U3CwwwDataU3E__0_1)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_1() const { return ___U3CwwwDataU3E__0_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_1() { return &___U3CwwwDataU3E__0_1; }
	inline void set_U3CwwwDataU3E__0_1(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3C_moduleModelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U3C_moduleModelU3E__0_2)); }
	inline GetModuleModel_t617428109 * get_U3C_moduleModelU3E__0_2() const { return ___U3C_moduleModelU3E__0_2; }
	inline GetModuleModel_t617428109 ** get_address_of_U3C_moduleModelU3E__0_2() { return &___U3C_moduleModelU3E__0_2; }
	inline void set_U3C_moduleModelU3E__0_2(GetModuleModel_t617428109 * value)
	{
		___U3C_moduleModelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_moduleModelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CmoduleObjU3E__1_3() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U3CmoduleObjU3E__1_3)); }
	inline GameObject_t1756533147 * get_U3CmoduleObjU3E__1_3() const { return ___U3CmoduleObjU3E__1_3; }
	inline GameObject_t1756533147 ** get_address_of_U3CmoduleObjU3E__1_3() { return &___U3CmoduleObjU3E__1_3; }
	inline void set_U3CmoduleObjU3E__1_3(GameObject_t1756533147 * value)
	{
		___U3CmoduleObjU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoduleObjU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CchildCoreObjU3E__1_4() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U3CchildCoreObjU3E__1_4)); }
	inline GameObject_t1756533147 * get_U3CchildCoreObjU3E__1_4() const { return ___U3CchildCoreObjU3E__1_4; }
	inline GameObject_t1756533147 ** get_address_of_U3CchildCoreObjU3E__1_4() { return &___U3CchildCoreObjU3E__1_4; }
	inline void set_U3CchildCoreObjU3E__1_4(GameObject_t1756533147 * value)
	{
		___U3CchildCoreObjU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildCoreObjU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U3CcorePartsObjU3E__1_5() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U3CcorePartsObjU3E__1_5)); }
	inline GameObject_t1756533147 * get_U3CcorePartsObjU3E__1_5() const { return ___U3CcorePartsObjU3E__1_5; }
	inline GameObject_t1756533147 ** get_address_of_U3CcorePartsObjU3E__1_5() { return &___U3CcorePartsObjU3E__1_5; }
	inline void set_U3CcorePartsObjU3E__1_5(GameObject_t1756533147 * value)
	{
		___U3CcorePartsObjU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcorePartsObjU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U3CcoreMatU3E__1_6() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U3CcoreMatU3E__1_6)); }
	inline Material_t193706927 * get_U3CcoreMatU3E__1_6() const { return ___U3CcoreMatU3E__1_6; }
	inline Material_t193706927 ** get_address_of_U3CcoreMatU3E__1_6() { return &___U3CcoreMatU3E__1_6; }
	inline void set_U3CcoreMatU3E__1_6(Material_t193706927 * value)
	{
		___U3CcoreMatU3E__1_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreMatU3E__1_6, value);
	}

	inline static int32_t get_offset_of_isCreatingMobInMyPage_7() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___isCreatingMobInMyPage_7)); }
	inline bool get_isCreatingMobInMyPage_7() const { return ___isCreatingMobInMyPage_7; }
	inline bool* get_address_of_isCreatingMobInMyPage_7() { return &___isCreatingMobInMyPage_7; }
	inline void set_isCreatingMobInMyPage_7(bool value)
	{
		___isCreatingMobInMyPage_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24locvar0_8)); }
	inline Enumerator_t2463632671  get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline Enumerator_t2463632671 * get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(Enumerator_t2463632671  value)
	{
		___U24locvar0_8 = value;
	}

	inline static int32_t get_offset_of_isRecorded_9() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___isRecorded_9)); }
	inline bool get_isRecorded_9() const { return ___isRecorded_9; }
	inline bool* get_address_of_isRecorded_9() { return &___isRecorded_9; }
	inline void set_isRecorded_9(bool value)
	{
		___isRecorded_9 = value;
	}

	inline static int32_t get_offset_of_U3CpartsTraU3E__1_10() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U3CpartsTraU3E__1_10)); }
	inline TransformU5BU5D_t3764228911* get_U3CpartsTraU3E__1_10() const { return ___U3CpartsTraU3E__1_10; }
	inline TransformU5BU5D_t3764228911** get_address_of_U3CpartsTraU3E__1_10() { return &___U3CpartsTraU3E__1_10; }
	inline void set_U3CpartsTraU3E__1_10(TransformU5BU5D_t3764228911* value)
	{
		___U3CpartsTraU3E__1_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsTraU3E__1_10, value);
	}

	inline static int32_t get_offset_of_U24locvar3_11() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24locvar3_11)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar3_11() const { return ___U24locvar3_11; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar3_11() { return &___U24locvar3_11; }
	inline void set_U24locvar3_11(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar3_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar3_11, value);
	}

	inline static int32_t get_offset_of_U24locvar4_12() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24locvar4_12)); }
	inline int32_t get_U24locvar4_12() const { return ___U24locvar4_12; }
	inline int32_t* get_address_of_U24locvar4_12() { return &___U24locvar4_12; }
	inline void set_U24locvar4_12(int32_t value)
	{
		___U24locvar4_12 = value;
	}

	inline static int32_t get_offset_of_U24locvar5_13() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24locvar5_13)); }
	inline Enumerator_t1213609547  get_U24locvar5_13() const { return ___U24locvar5_13; }
	inline Enumerator_t1213609547 * get_address_of_U24locvar5_13() { return &___U24locvar5_13; }
	inline void set_U24locvar5_13(Enumerator_t1213609547  value)
	{
		___U24locvar5_13 = value;
	}

	inline static int32_t get_offset_of_U24this_14() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24this_14)); }
	inline ModuleManager_t1065445307 * get_U24this_14() const { return ___U24this_14; }
	inline ModuleManager_t1065445307 ** get_address_of_U24this_14() { return &___U24this_14; }
	inline void set_U24this_14(ModuleManager_t1065445307 * value)
	{
		___U24this_14 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_14, value);
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24current_15)); }
	inline Il2CppObject * get_U24current_15() const { return ___U24current_15; }
	inline Il2CppObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(Il2CppObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_15, value);
	}

	inline static int32_t get_offset_of_U24disposing_16() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24disposing_16)); }
	inline bool get_U24disposing_16() const { return ___U24disposing_16; }
	inline bool* get_address_of_U24disposing_16() { return &___U24disposing_16; }
	inline void set_U24disposing_16(bool value)
	{
		___U24disposing_16 = value;
	}

	inline static int32_t get_offset_of_U24PC_17() { return static_cast<int32_t>(offsetof(U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019, ___U24PC_17)); }
	inline int32_t get_U24PC_17() const { return ___U24PC_17; }
	inline int32_t* get_address_of_U24PC_17() { return &___U24PC_17; }
	inline void set_U24PC_17(int32_t value)
	{
		___U24PC_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
