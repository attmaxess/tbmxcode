﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// ModuleManager
struct ModuleManager_t1065445307;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager/<reCreateModule>c__Iterator2
struct  U3CreCreateModuleU3Ec__Iterator2_t2731074232  : public Il2CppObject
{
public:
	// System.String ModuleManager/<reCreateModule>c__Iterator2::key
	String_t* ___key_0;
	// ModuleManager ModuleManager/<reCreateModule>c__Iterator2::$this
	ModuleManager_t1065445307 * ___U24this_1;
	// System.Object ModuleManager/<reCreateModule>c__Iterator2::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean ModuleManager/<reCreateModule>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 ModuleManager/<reCreateModule>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CreCreateModuleU3Ec__Iterator2_t2731074232, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CreCreateModuleU3Ec__Iterator2_t2731074232, ___U24this_1)); }
	inline ModuleManager_t1065445307 * get_U24this_1() const { return ___U24this_1; }
	inline ModuleManager_t1065445307 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ModuleManager_t1065445307 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CreCreateModuleU3Ec__Iterator2_t2731074232, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CreCreateModuleU3Ec__Iterator2_t2731074232, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CreCreateModuleU3Ec__Iterator2_t2731074232, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
