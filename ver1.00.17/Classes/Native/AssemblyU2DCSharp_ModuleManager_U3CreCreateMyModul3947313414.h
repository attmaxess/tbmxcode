﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En696369815.h"

// ModuleManager
struct ModuleManager_t1065445307;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager/<reCreateMyModuleInMyPage>c__Iterator4
struct  U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GameObject> ModuleManager/<reCreateMyModuleInMyPage>c__Iterator4::$locvar0
	Enumerator_t696369815  ___U24locvar0_0;
	// ModuleManager ModuleManager/<reCreateMyModuleInMyPage>c__Iterator4::$this
	ModuleManager_t1065445307 * ___U24this_1;
	// System.Object ModuleManager/<reCreateMyModuleInMyPage>c__Iterator4::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean ModuleManager/<reCreateMyModuleInMyPage>c__Iterator4::$disposing
	bool ___U24disposing_3;
	// System.Int32 ModuleManager/<reCreateMyModuleInMyPage>c__Iterator4::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414, ___U24locvar0_0)); }
	inline Enumerator_t696369815  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t696369815 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t696369815  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414, ___U24this_1)); }
	inline ModuleManager_t1065445307 * get_U24this_1() const { return ___U24this_1; }
	inline ModuleManager_t1065445307 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ModuleManager_t1065445307 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
