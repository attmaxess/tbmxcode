﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2463632671.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1213609547.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// GetModuleModel
struct GetModuleModel_t617428109;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// ModuleManager
struct ModuleManager_t1065445307;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleManager/<waitCreateModuleObject>c__Iterator3
struct  U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358  : public Il2CppObject
{
public:
	// System.String ModuleManager/<waitCreateModuleObject>c__Iterator3::json
	String_t* ___json_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> ModuleManager/<waitCreateModuleObject>c__Iterator3::<wwwData>__0
	Dictionary_2_t309261261 * ___U3CwwwDataU3E__0_1;
	// GetModuleModel ModuleManager/<waitCreateModuleObject>c__Iterator3::<_moduleModel>__0
	GetModuleModel_t617428109 * ___U3C_moduleModelU3E__0_2;
	// UnityEngine.GameObject ModuleManager/<waitCreateModuleObject>c__Iterator3::<childCoreObj>__1
	GameObject_t1756533147 * ___U3CchildCoreObjU3E__1_3;
	// UnityEngine.Material ModuleManager/<waitCreateModuleObject>c__Iterator3::<coreMat>__1
	Material_t193706927 * ___U3CcoreMatU3E__1_4;
	// System.Boolean ModuleManager/<waitCreateModuleObject>c__Iterator3::isCreatingMobInMyPage
	bool ___isCreatingMobInMyPage_5;
	// System.Collections.Generic.List`1/Enumerator<GetModuleChildPartsListModel> ModuleManager/<waitCreateModuleObject>c__Iterator3::$locvar0
	Enumerator_t2463632671  ___U24locvar0_6;
	// System.Boolean ModuleManager/<waitCreateModuleObject>c__Iterator3::isRecorded
	bool ___isRecorded_7;
	// UnityEngine.Transform[] ModuleManager/<waitCreateModuleObject>c__Iterator3::<partsTra>__1
	TransformU5BU5D_t3764228911* ___U3CpartsTraU3E__1_8;
	// UnityEngine.Transform[] ModuleManager/<waitCreateModuleObject>c__Iterator3::$locvar3
	TransformU5BU5D_t3764228911* ___U24locvar3_9;
	// System.Int32 ModuleManager/<waitCreateModuleObject>c__Iterator3::$locvar4
	int32_t ___U24locvar4_10;
	// System.Collections.Generic.List`1/Enumerator<GetModuleRegisterMotionListMotionData> ModuleManager/<waitCreateModuleObject>c__Iterator3::$locvar5
	Enumerator_t1213609547  ___U24locvar5_11;
	// ModuleManager ModuleManager/<waitCreateModuleObject>c__Iterator3::$this
	ModuleManager_t1065445307 * ___U24this_12;
	// System.Object ModuleManager/<waitCreateModuleObject>c__Iterator3::$current
	Il2CppObject * ___U24current_13;
	// System.Boolean ModuleManager/<waitCreateModuleObject>c__Iterator3::$disposing
	bool ___U24disposing_14;
	// System.Int32 ModuleManager/<waitCreateModuleObject>c__Iterator3::$PC
	int32_t ___U24PC_15;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___json_0)); }
	inline String_t* get_json_0() const { return ___json_0; }
	inline String_t** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(String_t* value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier(&___json_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U3CwwwDataU3E__0_1)); }
	inline Dictionary_2_t309261261 * get_U3CwwwDataU3E__0_1() const { return ___U3CwwwDataU3E__0_1; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CwwwDataU3E__0_1() { return &___U3CwwwDataU3E__0_1; }
	inline void set_U3CwwwDataU3E__0_1(Dictionary_2_t309261261 * value)
	{
		___U3CwwwDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwDataU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3C_moduleModelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U3C_moduleModelU3E__0_2)); }
	inline GetModuleModel_t617428109 * get_U3C_moduleModelU3E__0_2() const { return ___U3C_moduleModelU3E__0_2; }
	inline GetModuleModel_t617428109 ** get_address_of_U3C_moduleModelU3E__0_2() { return &___U3C_moduleModelU3E__0_2; }
	inline void set_U3C_moduleModelU3E__0_2(GetModuleModel_t617428109 * value)
	{
		___U3C_moduleModelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_moduleModelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CchildCoreObjU3E__1_3() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U3CchildCoreObjU3E__1_3)); }
	inline GameObject_t1756533147 * get_U3CchildCoreObjU3E__1_3() const { return ___U3CchildCoreObjU3E__1_3; }
	inline GameObject_t1756533147 ** get_address_of_U3CchildCoreObjU3E__1_3() { return &___U3CchildCoreObjU3E__1_3; }
	inline void set_U3CchildCoreObjU3E__1_3(GameObject_t1756533147 * value)
	{
		___U3CchildCoreObjU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildCoreObjU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CcoreMatU3E__1_4() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U3CcoreMatU3E__1_4)); }
	inline Material_t193706927 * get_U3CcoreMatU3E__1_4() const { return ___U3CcoreMatU3E__1_4; }
	inline Material_t193706927 ** get_address_of_U3CcoreMatU3E__1_4() { return &___U3CcoreMatU3E__1_4; }
	inline void set_U3CcoreMatU3E__1_4(Material_t193706927 * value)
	{
		___U3CcoreMatU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcoreMatU3E__1_4, value);
	}

	inline static int32_t get_offset_of_isCreatingMobInMyPage_5() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___isCreatingMobInMyPage_5)); }
	inline bool get_isCreatingMobInMyPage_5() const { return ___isCreatingMobInMyPage_5; }
	inline bool* get_address_of_isCreatingMobInMyPage_5() { return &___isCreatingMobInMyPage_5; }
	inline void set_isCreatingMobInMyPage_5(bool value)
	{
		___isCreatingMobInMyPage_5 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_6() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24locvar0_6)); }
	inline Enumerator_t2463632671  get_U24locvar0_6() const { return ___U24locvar0_6; }
	inline Enumerator_t2463632671 * get_address_of_U24locvar0_6() { return &___U24locvar0_6; }
	inline void set_U24locvar0_6(Enumerator_t2463632671  value)
	{
		___U24locvar0_6 = value;
	}

	inline static int32_t get_offset_of_isRecorded_7() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___isRecorded_7)); }
	inline bool get_isRecorded_7() const { return ___isRecorded_7; }
	inline bool* get_address_of_isRecorded_7() { return &___isRecorded_7; }
	inline void set_isRecorded_7(bool value)
	{
		___isRecorded_7 = value;
	}

	inline static int32_t get_offset_of_U3CpartsTraU3E__1_8() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U3CpartsTraU3E__1_8)); }
	inline TransformU5BU5D_t3764228911* get_U3CpartsTraU3E__1_8() const { return ___U3CpartsTraU3E__1_8; }
	inline TransformU5BU5D_t3764228911** get_address_of_U3CpartsTraU3E__1_8() { return &___U3CpartsTraU3E__1_8; }
	inline void set_U3CpartsTraU3E__1_8(TransformU5BU5D_t3764228911* value)
	{
		___U3CpartsTraU3E__1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpartsTraU3E__1_8, value);
	}

	inline static int32_t get_offset_of_U24locvar3_9() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24locvar3_9)); }
	inline TransformU5BU5D_t3764228911* get_U24locvar3_9() const { return ___U24locvar3_9; }
	inline TransformU5BU5D_t3764228911** get_address_of_U24locvar3_9() { return &___U24locvar3_9; }
	inline void set_U24locvar3_9(TransformU5BU5D_t3764228911* value)
	{
		___U24locvar3_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar3_9, value);
	}

	inline static int32_t get_offset_of_U24locvar4_10() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24locvar4_10)); }
	inline int32_t get_U24locvar4_10() const { return ___U24locvar4_10; }
	inline int32_t* get_address_of_U24locvar4_10() { return &___U24locvar4_10; }
	inline void set_U24locvar4_10(int32_t value)
	{
		___U24locvar4_10 = value;
	}

	inline static int32_t get_offset_of_U24locvar5_11() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24locvar5_11)); }
	inline Enumerator_t1213609547  get_U24locvar5_11() const { return ___U24locvar5_11; }
	inline Enumerator_t1213609547 * get_address_of_U24locvar5_11() { return &___U24locvar5_11; }
	inline void set_U24locvar5_11(Enumerator_t1213609547  value)
	{
		___U24locvar5_11 = value;
	}

	inline static int32_t get_offset_of_U24this_12() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24this_12)); }
	inline ModuleManager_t1065445307 * get_U24this_12() const { return ___U24this_12; }
	inline ModuleManager_t1065445307 ** get_address_of_U24this_12() { return &___U24this_12; }
	inline void set_U24this_12(ModuleManager_t1065445307 * value)
	{
		___U24this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_12, value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24current_13)); }
	inline Il2CppObject * get_U24current_13() const { return ___U24current_13; }
	inline Il2CppObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Il2CppObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
