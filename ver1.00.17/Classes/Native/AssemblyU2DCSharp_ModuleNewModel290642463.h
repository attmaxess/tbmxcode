﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModuleNewModel
struct  ModuleNewModel_t290642463  : public Model_t873752437
{
public:
	// System.Int32 ModuleNewModel::<_module_new_id>k__BackingField
	int32_t ___U3C_module_new_idU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3C_module_new_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ModuleNewModel_t290642463, ___U3C_module_new_idU3Ek__BackingField_0)); }
	inline int32_t get_U3C_module_new_idU3Ek__BackingField_0() const { return ___U3C_module_new_idU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3C_module_new_idU3Ek__BackingField_0() { return &___U3C_module_new_idU3Ek__BackingField_0; }
	inline void set_U3C_module_new_idU3Ek__BackingField_0(int32_t value)
	{
		___U3C_module_new_idU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
