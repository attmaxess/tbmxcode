﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModulePanel
struct  ModulePanel_t1015180434  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ModulePanel::ModuleSelect
	GameObject_t1756533147 * ___ModuleSelect_2;
	// UnityEngine.GameObject ModulePanel::PartsConect
	GameObject_t1756533147 * ___PartsConect_3;

public:
	inline static int32_t get_offset_of_ModuleSelect_2() { return static_cast<int32_t>(offsetof(ModulePanel_t1015180434, ___ModuleSelect_2)); }
	inline GameObject_t1756533147 * get_ModuleSelect_2() const { return ___ModuleSelect_2; }
	inline GameObject_t1756533147 ** get_address_of_ModuleSelect_2() { return &___ModuleSelect_2; }
	inline void set_ModuleSelect_2(GameObject_t1756533147 * value)
	{
		___ModuleSelect_2 = value;
		Il2CppCodeGenWriteBarrier(&___ModuleSelect_2, value);
	}

	inline static int32_t get_offset_of_PartsConect_3() { return static_cast<int32_t>(offsetof(ModulePanel_t1015180434, ___PartsConect_3)); }
	inline GameObject_t1756533147 * get_PartsConect_3() const { return ___PartsConect_3; }
	inline GameObject_t1756533147 ** get_address_of_PartsConect_3() { return &___PartsConect_3; }
	inline void set_PartsConect_3(GameObject_t1756533147 * value)
	{
		___PartsConect_3 = value;
		Il2CppCodeGenWriteBarrier(&___PartsConect_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
