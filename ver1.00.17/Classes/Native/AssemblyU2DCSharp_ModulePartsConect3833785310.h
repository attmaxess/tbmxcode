﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ModulePartsConectContents
struct ModulePartsConectContents_t560460772;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// PartsViewPanel
struct PartsViewPanel_t2613620109;
// PartsPanelSliders
struct PartsPanelSliders_t4209932670;
// ModelingConfigPanel
struct ModelingConfigPanel_t2566912473;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t10059812;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModulePartsConect
struct  ModulePartsConect_t3833785310  : public MonoBehaviour_t1158329972
{
public:
	// ModulePartsConectContents ModulePartsConect::Contents
	ModulePartsConectContents_t560460772 * ___Contents_2;
	// UnityEngine.GameObject[] ModulePartsConect::Sequences
	GameObjectU5BU5D_t3057952154* ___Sequences_3;
	// PartsViewPanel ModulePartsConect::PartsViewPanel
	PartsViewPanel_t2613620109 * ___PartsViewPanel_4;
	// PartsPanelSliders ModulePartsConect::Sliders
	PartsPanelSliders_t4209932670 * ___Sliders_5;
	// ModelingConfigPanel ModulePartsConect::ConfigPanel
	ModelingConfigPanel_t2566912473 * ___ConfigPanel_6;
	// UnityEngine.GameObject ModulePartsConect::MovieObj
	GameObject_t1756533147 * ___MovieObj_7;
	// UnityEngine.Video.VideoPlayer ModulePartsConect::TutorialVideoPlayer
	VideoPlayer_t10059812 * ___TutorialVideoPlayer_8;

public:
	inline static int32_t get_offset_of_Contents_2() { return static_cast<int32_t>(offsetof(ModulePartsConect_t3833785310, ___Contents_2)); }
	inline ModulePartsConectContents_t560460772 * get_Contents_2() const { return ___Contents_2; }
	inline ModulePartsConectContents_t560460772 ** get_address_of_Contents_2() { return &___Contents_2; }
	inline void set_Contents_2(ModulePartsConectContents_t560460772 * value)
	{
		___Contents_2 = value;
		Il2CppCodeGenWriteBarrier(&___Contents_2, value);
	}

	inline static int32_t get_offset_of_Sequences_3() { return static_cast<int32_t>(offsetof(ModulePartsConect_t3833785310, ___Sequences_3)); }
	inline GameObjectU5BU5D_t3057952154* get_Sequences_3() const { return ___Sequences_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Sequences_3() { return &___Sequences_3; }
	inline void set_Sequences_3(GameObjectU5BU5D_t3057952154* value)
	{
		___Sequences_3 = value;
		Il2CppCodeGenWriteBarrier(&___Sequences_3, value);
	}

	inline static int32_t get_offset_of_PartsViewPanel_4() { return static_cast<int32_t>(offsetof(ModulePartsConect_t3833785310, ___PartsViewPanel_4)); }
	inline PartsViewPanel_t2613620109 * get_PartsViewPanel_4() const { return ___PartsViewPanel_4; }
	inline PartsViewPanel_t2613620109 ** get_address_of_PartsViewPanel_4() { return &___PartsViewPanel_4; }
	inline void set_PartsViewPanel_4(PartsViewPanel_t2613620109 * value)
	{
		___PartsViewPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___PartsViewPanel_4, value);
	}

	inline static int32_t get_offset_of_Sliders_5() { return static_cast<int32_t>(offsetof(ModulePartsConect_t3833785310, ___Sliders_5)); }
	inline PartsPanelSliders_t4209932670 * get_Sliders_5() const { return ___Sliders_5; }
	inline PartsPanelSliders_t4209932670 ** get_address_of_Sliders_5() { return &___Sliders_5; }
	inline void set_Sliders_5(PartsPanelSliders_t4209932670 * value)
	{
		___Sliders_5 = value;
		Il2CppCodeGenWriteBarrier(&___Sliders_5, value);
	}

	inline static int32_t get_offset_of_ConfigPanel_6() { return static_cast<int32_t>(offsetof(ModulePartsConect_t3833785310, ___ConfigPanel_6)); }
	inline ModelingConfigPanel_t2566912473 * get_ConfigPanel_6() const { return ___ConfigPanel_6; }
	inline ModelingConfigPanel_t2566912473 ** get_address_of_ConfigPanel_6() { return &___ConfigPanel_6; }
	inline void set_ConfigPanel_6(ModelingConfigPanel_t2566912473 * value)
	{
		___ConfigPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigPanel_6, value);
	}

	inline static int32_t get_offset_of_MovieObj_7() { return static_cast<int32_t>(offsetof(ModulePartsConect_t3833785310, ___MovieObj_7)); }
	inline GameObject_t1756533147 * get_MovieObj_7() const { return ___MovieObj_7; }
	inline GameObject_t1756533147 ** get_address_of_MovieObj_7() { return &___MovieObj_7; }
	inline void set_MovieObj_7(GameObject_t1756533147 * value)
	{
		___MovieObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___MovieObj_7, value);
	}

	inline static int32_t get_offset_of_TutorialVideoPlayer_8() { return static_cast<int32_t>(offsetof(ModulePartsConect_t3833785310, ___TutorialVideoPlayer_8)); }
	inline VideoPlayer_t10059812 * get_TutorialVideoPlayer_8() const { return ___TutorialVideoPlayer_8; }
	inline VideoPlayer_t10059812 ** get_address_of_TutorialVideoPlayer_8() { return &___TutorialVideoPlayer_8; }
	inline void set_TutorialVideoPlayer_8(VideoPlayer_t10059812 * value)
	{
		___TutorialVideoPlayer_8 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialVideoPlayer_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
