﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModulePartsConectContents
struct  ModulePartsConectContents_t560460772  : public Il2CppObject
{
public:
	// UnityEngine.UI.Button ModulePartsConectContents::CameraResetButton
	Button_t2872111280 * ___CameraResetButton_0;
	// UnityEngine.UI.Button ModulePartsConectContents::PartsDeleteButton
	Button_t2872111280 * ___PartsDeleteButton_1;
	// UnityEngine.CanvasGroup ModulePartsConectContents::ResetPanel
	CanvasGroup_t3296560743 * ___ResetPanel_2;
	// UnityEngine.RectTransform ModulePartsConectContents::ResetPanelRectTrans
	RectTransform_t3349966182 * ___ResetPanelRectTrans_3;
	// UnityEngine.GameObject ModulePartsConectContents::PosisionPanel
	GameObject_t1756533147 * ___PosisionPanel_4;
	// UnityEngine.GameObject ModulePartsConectContents::StatusPanel
	GameObject_t1756533147 * ___StatusPanel_5;

public:
	inline static int32_t get_offset_of_CameraResetButton_0() { return static_cast<int32_t>(offsetof(ModulePartsConectContents_t560460772, ___CameraResetButton_0)); }
	inline Button_t2872111280 * get_CameraResetButton_0() const { return ___CameraResetButton_0; }
	inline Button_t2872111280 ** get_address_of_CameraResetButton_0() { return &___CameraResetButton_0; }
	inline void set_CameraResetButton_0(Button_t2872111280 * value)
	{
		___CameraResetButton_0 = value;
		Il2CppCodeGenWriteBarrier(&___CameraResetButton_0, value);
	}

	inline static int32_t get_offset_of_PartsDeleteButton_1() { return static_cast<int32_t>(offsetof(ModulePartsConectContents_t560460772, ___PartsDeleteButton_1)); }
	inline Button_t2872111280 * get_PartsDeleteButton_1() const { return ___PartsDeleteButton_1; }
	inline Button_t2872111280 ** get_address_of_PartsDeleteButton_1() { return &___PartsDeleteButton_1; }
	inline void set_PartsDeleteButton_1(Button_t2872111280 * value)
	{
		___PartsDeleteButton_1 = value;
		Il2CppCodeGenWriteBarrier(&___PartsDeleteButton_1, value);
	}

	inline static int32_t get_offset_of_ResetPanel_2() { return static_cast<int32_t>(offsetof(ModulePartsConectContents_t560460772, ___ResetPanel_2)); }
	inline CanvasGroup_t3296560743 * get_ResetPanel_2() const { return ___ResetPanel_2; }
	inline CanvasGroup_t3296560743 ** get_address_of_ResetPanel_2() { return &___ResetPanel_2; }
	inline void set_ResetPanel_2(CanvasGroup_t3296560743 * value)
	{
		___ResetPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___ResetPanel_2, value);
	}

	inline static int32_t get_offset_of_ResetPanelRectTrans_3() { return static_cast<int32_t>(offsetof(ModulePartsConectContents_t560460772, ___ResetPanelRectTrans_3)); }
	inline RectTransform_t3349966182 * get_ResetPanelRectTrans_3() const { return ___ResetPanelRectTrans_3; }
	inline RectTransform_t3349966182 ** get_address_of_ResetPanelRectTrans_3() { return &___ResetPanelRectTrans_3; }
	inline void set_ResetPanelRectTrans_3(RectTransform_t3349966182 * value)
	{
		___ResetPanelRectTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___ResetPanelRectTrans_3, value);
	}

	inline static int32_t get_offset_of_PosisionPanel_4() { return static_cast<int32_t>(offsetof(ModulePartsConectContents_t560460772, ___PosisionPanel_4)); }
	inline GameObject_t1756533147 * get_PosisionPanel_4() const { return ___PosisionPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_PosisionPanel_4() { return &___PosisionPanel_4; }
	inline void set_PosisionPanel_4(GameObject_t1756533147 * value)
	{
		___PosisionPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___PosisionPanel_4, value);
	}

	inline static int32_t get_offset_of_StatusPanel_5() { return static_cast<int32_t>(offsetof(ModulePartsConectContents_t560460772, ___StatusPanel_5)); }
	inline GameObject_t1756533147 * get_StatusPanel_5() const { return ___StatusPanel_5; }
	inline GameObject_t1756533147 ** get_address_of_StatusPanel_5() { return &___StatusPanel_5; }
	inline void set_StatusPanel_5(GameObject_t1756533147 * value)
	{
		___StatusPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___StatusPanel_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
