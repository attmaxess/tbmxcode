﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2552670118.h"

// System.String
struct String_t;
// Mobilmo
struct Mobilmo_t370754809;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve>
struct Dictionary_2_t926353117;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t3936083219;
// UnityEngine.Animation
struct Animation_t2068071072;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MotionManager
struct  MotionManager_t3855993783  : public SingletonMonoBehaviour_1_t2552670118
{
public:
	// System.Single MotionManager::actionChangeTimer
	float ___actionChangeTimer_3;
	// System.String MotionManager::motionJson
	String_t* ___motionJson_4;
	// System.String MotionManager::motionNewJson
	String_t* ___motionNewJson_5;
	// System.Int32 MotionManager::m_MobId
	int32_t ___m_MobId_6;
	// Mobilmo MotionManager::m_Mobilmo
	Mobilmo_t370754809 * ___m_Mobilmo_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> MotionManager::m_pCurveDic1
	Dictionary_2_t926353117 * ___m_pCurveDic1_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> MotionManager::m_pCurveDic2
	Dictionary_2_t926353117 * ___m_pCurveDic2_9;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> MotionManager::m_pCurveDic3
	Dictionary_2_t926353117 * ___m_pCurveDic3_10;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AnimationCurve> MotionManager::m_pCurveDic4
	Dictionary_2_t926353117 * ___m_pCurveDic4_11;
	// UnityEngine.AnimationCurve MotionManager::curve
	AnimationCurve_t3306541151 * ___curve_12;
	// UnityEngine.Keyframe[] MotionManager::ks
	KeyframeU5BU5D_t449065829* ___ks_13;
	// System.String MotionManager::path
	String_t* ___path_14;
	// System.String MotionManager::property
	String_t* ___property_15;
	// UnityEngine.AnimationClip[] MotionManager::animClip
	AnimationClipU5BU5D_t3936083219* ___animClip_16;
	// UnityEngine.AnimationClip[] MotionManager::recordingAnimClip
	AnimationClipU5BU5D_t3936083219* ___recordingAnimClip_17;
	// UnityEngine.Animation MotionManager::anim
	Animation_t2068071072 * ___anim_18;
	// System.Boolean MotionManager::isPlaying
	bool ___isPlaying_19;
	// System.Boolean MotionManager::isSuccess
	bool ___isSuccess_20;
	// System.Int32 MotionManager::motionNum
	int32_t ___motionNum_21;
	// System.Boolean MotionManager::ischangeClip
	bool ___ischangeClip_22;
	// Mobilmo MotionManager::m_recMobilmo
	Mobilmo_t370754809 * ___m_recMobilmo_23;

public:
	inline static int32_t get_offset_of_actionChangeTimer_3() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___actionChangeTimer_3)); }
	inline float get_actionChangeTimer_3() const { return ___actionChangeTimer_3; }
	inline float* get_address_of_actionChangeTimer_3() { return &___actionChangeTimer_3; }
	inline void set_actionChangeTimer_3(float value)
	{
		___actionChangeTimer_3 = value;
	}

	inline static int32_t get_offset_of_motionJson_4() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___motionJson_4)); }
	inline String_t* get_motionJson_4() const { return ___motionJson_4; }
	inline String_t** get_address_of_motionJson_4() { return &___motionJson_4; }
	inline void set_motionJson_4(String_t* value)
	{
		___motionJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___motionJson_4, value);
	}

	inline static int32_t get_offset_of_motionNewJson_5() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___motionNewJson_5)); }
	inline String_t* get_motionNewJson_5() const { return ___motionNewJson_5; }
	inline String_t** get_address_of_motionNewJson_5() { return &___motionNewJson_5; }
	inline void set_motionNewJson_5(String_t* value)
	{
		___motionNewJson_5 = value;
		Il2CppCodeGenWriteBarrier(&___motionNewJson_5, value);
	}

	inline static int32_t get_offset_of_m_MobId_6() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___m_MobId_6)); }
	inline int32_t get_m_MobId_6() const { return ___m_MobId_6; }
	inline int32_t* get_address_of_m_MobId_6() { return &___m_MobId_6; }
	inline void set_m_MobId_6(int32_t value)
	{
		___m_MobId_6 = value;
	}

	inline static int32_t get_offset_of_m_Mobilmo_7() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___m_Mobilmo_7)); }
	inline Mobilmo_t370754809 * get_m_Mobilmo_7() const { return ___m_Mobilmo_7; }
	inline Mobilmo_t370754809 ** get_address_of_m_Mobilmo_7() { return &___m_Mobilmo_7; }
	inline void set_m_Mobilmo_7(Mobilmo_t370754809 * value)
	{
		___m_Mobilmo_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_Mobilmo_7, value);
	}

	inline static int32_t get_offset_of_m_pCurveDic1_8() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___m_pCurveDic1_8)); }
	inline Dictionary_2_t926353117 * get_m_pCurveDic1_8() const { return ___m_pCurveDic1_8; }
	inline Dictionary_2_t926353117 ** get_address_of_m_pCurveDic1_8() { return &___m_pCurveDic1_8; }
	inline void set_m_pCurveDic1_8(Dictionary_2_t926353117 * value)
	{
		___m_pCurveDic1_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_pCurveDic1_8, value);
	}

	inline static int32_t get_offset_of_m_pCurveDic2_9() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___m_pCurveDic2_9)); }
	inline Dictionary_2_t926353117 * get_m_pCurveDic2_9() const { return ___m_pCurveDic2_9; }
	inline Dictionary_2_t926353117 ** get_address_of_m_pCurveDic2_9() { return &___m_pCurveDic2_9; }
	inline void set_m_pCurveDic2_9(Dictionary_2_t926353117 * value)
	{
		___m_pCurveDic2_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_pCurveDic2_9, value);
	}

	inline static int32_t get_offset_of_m_pCurveDic3_10() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___m_pCurveDic3_10)); }
	inline Dictionary_2_t926353117 * get_m_pCurveDic3_10() const { return ___m_pCurveDic3_10; }
	inline Dictionary_2_t926353117 ** get_address_of_m_pCurveDic3_10() { return &___m_pCurveDic3_10; }
	inline void set_m_pCurveDic3_10(Dictionary_2_t926353117 * value)
	{
		___m_pCurveDic3_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_pCurveDic3_10, value);
	}

	inline static int32_t get_offset_of_m_pCurveDic4_11() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___m_pCurveDic4_11)); }
	inline Dictionary_2_t926353117 * get_m_pCurveDic4_11() const { return ___m_pCurveDic4_11; }
	inline Dictionary_2_t926353117 ** get_address_of_m_pCurveDic4_11() { return &___m_pCurveDic4_11; }
	inline void set_m_pCurveDic4_11(Dictionary_2_t926353117 * value)
	{
		___m_pCurveDic4_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_pCurveDic4_11, value);
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___curve_12)); }
	inline AnimationCurve_t3306541151 * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_t3306541151 ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_t3306541151 * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier(&___curve_12, value);
	}

	inline static int32_t get_offset_of_ks_13() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___ks_13)); }
	inline KeyframeU5BU5D_t449065829* get_ks_13() const { return ___ks_13; }
	inline KeyframeU5BU5D_t449065829** get_address_of_ks_13() { return &___ks_13; }
	inline void set_ks_13(KeyframeU5BU5D_t449065829* value)
	{
		___ks_13 = value;
		Il2CppCodeGenWriteBarrier(&___ks_13, value);
	}

	inline static int32_t get_offset_of_path_14() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___path_14)); }
	inline String_t* get_path_14() const { return ___path_14; }
	inline String_t** get_address_of_path_14() { return &___path_14; }
	inline void set_path_14(String_t* value)
	{
		___path_14 = value;
		Il2CppCodeGenWriteBarrier(&___path_14, value);
	}

	inline static int32_t get_offset_of_property_15() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___property_15)); }
	inline String_t* get_property_15() const { return ___property_15; }
	inline String_t** get_address_of_property_15() { return &___property_15; }
	inline void set_property_15(String_t* value)
	{
		___property_15 = value;
		Il2CppCodeGenWriteBarrier(&___property_15, value);
	}

	inline static int32_t get_offset_of_animClip_16() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___animClip_16)); }
	inline AnimationClipU5BU5D_t3936083219* get_animClip_16() const { return ___animClip_16; }
	inline AnimationClipU5BU5D_t3936083219** get_address_of_animClip_16() { return &___animClip_16; }
	inline void set_animClip_16(AnimationClipU5BU5D_t3936083219* value)
	{
		___animClip_16 = value;
		Il2CppCodeGenWriteBarrier(&___animClip_16, value);
	}

	inline static int32_t get_offset_of_recordingAnimClip_17() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___recordingAnimClip_17)); }
	inline AnimationClipU5BU5D_t3936083219* get_recordingAnimClip_17() const { return ___recordingAnimClip_17; }
	inline AnimationClipU5BU5D_t3936083219** get_address_of_recordingAnimClip_17() { return &___recordingAnimClip_17; }
	inline void set_recordingAnimClip_17(AnimationClipU5BU5D_t3936083219* value)
	{
		___recordingAnimClip_17 = value;
		Il2CppCodeGenWriteBarrier(&___recordingAnimClip_17, value);
	}

	inline static int32_t get_offset_of_anim_18() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___anim_18)); }
	inline Animation_t2068071072 * get_anim_18() const { return ___anim_18; }
	inline Animation_t2068071072 ** get_address_of_anim_18() { return &___anim_18; }
	inline void set_anim_18(Animation_t2068071072 * value)
	{
		___anim_18 = value;
		Il2CppCodeGenWriteBarrier(&___anim_18, value);
	}

	inline static int32_t get_offset_of_isPlaying_19() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___isPlaying_19)); }
	inline bool get_isPlaying_19() const { return ___isPlaying_19; }
	inline bool* get_address_of_isPlaying_19() { return &___isPlaying_19; }
	inline void set_isPlaying_19(bool value)
	{
		___isPlaying_19 = value;
	}

	inline static int32_t get_offset_of_isSuccess_20() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___isSuccess_20)); }
	inline bool get_isSuccess_20() const { return ___isSuccess_20; }
	inline bool* get_address_of_isSuccess_20() { return &___isSuccess_20; }
	inline void set_isSuccess_20(bool value)
	{
		___isSuccess_20 = value;
	}

	inline static int32_t get_offset_of_motionNum_21() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___motionNum_21)); }
	inline int32_t get_motionNum_21() const { return ___motionNum_21; }
	inline int32_t* get_address_of_motionNum_21() { return &___motionNum_21; }
	inline void set_motionNum_21(int32_t value)
	{
		___motionNum_21 = value;
	}

	inline static int32_t get_offset_of_ischangeClip_22() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___ischangeClip_22)); }
	inline bool get_ischangeClip_22() const { return ___ischangeClip_22; }
	inline bool* get_address_of_ischangeClip_22() { return &___ischangeClip_22; }
	inline void set_ischangeClip_22(bool value)
	{
		___ischangeClip_22 = value;
	}

	inline static int32_t get_offset_of_m_recMobilmo_23() { return static_cast<int32_t>(offsetof(MotionManager_t3855993783, ___m_recMobilmo_23)); }
	inline Mobilmo_t370754809 * get_m_recMobilmo_23() const { return ___m_recMobilmo_23; }
	inline Mobilmo_t370754809 ** get_address_of_m_recMobilmo_23() { return &___m_recMobilmo_23; }
	inline void set_m_recMobilmo_23(Mobilmo_t370754809 * value)
	{
		___m_recMobilmo_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_recMobilmo_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
