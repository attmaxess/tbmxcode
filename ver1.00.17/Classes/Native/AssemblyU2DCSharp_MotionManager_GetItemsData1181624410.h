﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<MotionManager/GetMotionsData>
struct List_1_t1787044743;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MotionManager/GetItemsData
struct  GetItemsData_t1181624410  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<MotionManager/GetMotionsData> MotionManager/GetItemsData::motionData
	List_1_t1787044743 * ___motionData_0;

public:
	inline static int32_t get_offset_of_motionData_0() { return static_cast<int32_t>(offsetof(GetItemsData_t1181624410, ___motionData_0)); }
	inline List_1_t1787044743 * get_motionData_0() const { return ___motionData_0; }
	inline List_1_t1787044743 ** get_address_of_motionData_0() { return &___motionData_0; }
	inline void set_motionData_0(List_1_t1787044743 * value)
	{
		___motionData_0 = value;
		Il2CppCodeGenWriteBarrier(&___motionData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
