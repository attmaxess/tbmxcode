﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<MotionManager/GetMotionsDataInWorldActionSetting>
struct List_1_t1441732976;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MotionManager/GetItemsDataInWorldActionSetting
struct  GetItemsDataInWorldActionSetting_t1654137477  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<MotionManager/GetMotionsDataInWorldActionSetting> MotionManager/GetItemsDataInWorldActionSetting::motionData
	List_1_t1441732976 * ___motionData_0;

public:
	inline static int32_t get_offset_of_motionData_0() { return static_cast<int32_t>(offsetof(GetItemsDataInWorldActionSetting_t1654137477, ___motionData_0)); }
	inline List_1_t1441732976 * get_motionData_0() const { return ___motionData_0; }
	inline List_1_t1441732976 ** get_address_of_motionData_0() { return &___motionData_0; }
	inline void set_motionData_0(List_1_t1441732976 * value)
	{
		___motionData_0 = value;
		Il2CppCodeGenWriteBarrier(&___motionData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
