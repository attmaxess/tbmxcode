﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MotionManager/GetPoseData
struct  GetPoseData_t2785975099  : public Il2CppObject
{
public:
	// System.Single MotionManager/GetPoseData::poseKeyTime
	float ___poseKeyTime_0;
	// System.Single MotionManager/GetPoseData::poseKeyValue
	float ___poseKeyValue_1;

public:
	inline static int32_t get_offset_of_poseKeyTime_0() { return static_cast<int32_t>(offsetof(GetPoseData_t2785975099, ___poseKeyTime_0)); }
	inline float get_poseKeyTime_0() const { return ___poseKeyTime_0; }
	inline float* get_address_of_poseKeyTime_0() { return &___poseKeyTime_0; }
	inline void set_poseKeyTime_0(float value)
	{
		___poseKeyTime_0 = value;
	}

	inline static int32_t get_offset_of_poseKeyValue_1() { return static_cast<int32_t>(offsetof(GetPoseData_t2785975099, ___poseKeyValue_1)); }
	inline float get_poseKeyValue_1() const { return ___poseKeyValue_1; }
	inline float* get_address_of_poseKeyValue_1() { return &___poseKeyValue_1; }
	inline void set_poseKeyValue_1(float value)
	{
		___poseKeyValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
