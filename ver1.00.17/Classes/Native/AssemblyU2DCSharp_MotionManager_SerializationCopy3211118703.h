﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MotionManager/SerializationCopy
struct  SerializationCopy_t3211118703  : public Il2CppObject
{
public:
	// System.Int32 MotionManager/SerializationCopy::mobilityId
	int32_t ___mobilityId_0;
	// System.String MotionManager/SerializationCopy::items
	String_t* ___items_1;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(SerializationCopy_t3211118703, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(SerializationCopy_t3211118703, ___items_1)); }
	inline String_t* get_items_1() const { return ___items_1; }
	inline String_t** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(String_t* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier(&___items_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
