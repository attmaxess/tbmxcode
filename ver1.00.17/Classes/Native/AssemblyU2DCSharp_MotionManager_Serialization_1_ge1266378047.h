﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MotionManager/Serialization`1<System.Object>
struct  Serialization_1_t1266378047  : public Il2CppObject
{
public:
	// System.Int32 MotionManager/Serialization`1::mobilityId
	int32_t ___mobilityId_0;
	// System.Collections.Generic.List`1<T> MotionManager/Serialization`1::items
	List_1_t2058570427 * ___items_1;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(Serialization_1_t1266378047, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(Serialization_1_t1266378047, ___items_1)); }
	inline List_1_t2058570427 * get_items_1() const { return ___items_1; }
	inline List_1_t2058570427 ** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(List_1_t2058570427 * value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier(&___items_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
