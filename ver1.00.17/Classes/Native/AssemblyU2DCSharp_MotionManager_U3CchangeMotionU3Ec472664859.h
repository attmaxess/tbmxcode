﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;
// System.String
struct String_t;
// UnityEngine.Animation
struct Animation_t2068071072;
// MotionManager
struct MotionManager_t3855993783;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MotionManager/<changeMotion>c__Iterator0
struct  U3CchangeMotionU3Ec__Iterator0_t472664859  : public Il2CppObject
{
public:
	// UnityEngine.AnimationClip MotionManager/<changeMotion>c__Iterator0::<clip>__0
	AnimationClip_t3510324950 * ___U3CclipU3E__0_0;
	// System.String MotionManager/<changeMotion>c__Iterator0::<clipName>__0
	String_t* ___U3CclipNameU3E__0_1;
	// System.Boolean MotionManager/<changeMotion>c__Iterator0::ischange
	bool ___ischange_2;
	// System.Int32 MotionManager/<changeMotion>c__Iterator0::motionNum
	int32_t ___motionNum_3;
	// UnityEngine.Animation MotionManager/<changeMotion>c__Iterator0::coreAnim
	Animation_t2068071072 * ___coreAnim_4;
	// System.Single MotionManager/<changeMotion>c__Iterator0::actionChangeTimer
	float ___actionChangeTimer_5;
	// MotionManager MotionManager/<changeMotion>c__Iterator0::$this
	MotionManager_t3855993783 * ___U24this_6;
	// System.Object MotionManager/<changeMotion>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean MotionManager/<changeMotion>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 MotionManager/<changeMotion>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CclipU3E__0_0() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___U3CclipU3E__0_0)); }
	inline AnimationClip_t3510324950 * get_U3CclipU3E__0_0() const { return ___U3CclipU3E__0_0; }
	inline AnimationClip_t3510324950 ** get_address_of_U3CclipU3E__0_0() { return &___U3CclipU3E__0_0; }
	inline void set_U3CclipU3E__0_0(AnimationClip_t3510324950 * value)
	{
		___U3CclipU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CclipU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CclipNameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___U3CclipNameU3E__0_1)); }
	inline String_t* get_U3CclipNameU3E__0_1() const { return ___U3CclipNameU3E__0_1; }
	inline String_t** get_address_of_U3CclipNameU3E__0_1() { return &___U3CclipNameU3E__0_1; }
	inline void set_U3CclipNameU3E__0_1(String_t* value)
	{
		___U3CclipNameU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CclipNameU3E__0_1, value);
	}

	inline static int32_t get_offset_of_ischange_2() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___ischange_2)); }
	inline bool get_ischange_2() const { return ___ischange_2; }
	inline bool* get_address_of_ischange_2() { return &___ischange_2; }
	inline void set_ischange_2(bool value)
	{
		___ischange_2 = value;
	}

	inline static int32_t get_offset_of_motionNum_3() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___motionNum_3)); }
	inline int32_t get_motionNum_3() const { return ___motionNum_3; }
	inline int32_t* get_address_of_motionNum_3() { return &___motionNum_3; }
	inline void set_motionNum_3(int32_t value)
	{
		___motionNum_3 = value;
	}

	inline static int32_t get_offset_of_coreAnim_4() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___coreAnim_4)); }
	inline Animation_t2068071072 * get_coreAnim_4() const { return ___coreAnim_4; }
	inline Animation_t2068071072 ** get_address_of_coreAnim_4() { return &___coreAnim_4; }
	inline void set_coreAnim_4(Animation_t2068071072 * value)
	{
		___coreAnim_4 = value;
		Il2CppCodeGenWriteBarrier(&___coreAnim_4, value);
	}

	inline static int32_t get_offset_of_actionChangeTimer_5() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___actionChangeTimer_5)); }
	inline float get_actionChangeTimer_5() const { return ___actionChangeTimer_5; }
	inline float* get_address_of_actionChangeTimer_5() { return &___actionChangeTimer_5; }
	inline void set_actionChangeTimer_5(float value)
	{
		___actionChangeTimer_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___U24this_6)); }
	inline MotionManager_t3855993783 * get_U24this_6() const { return ___U24this_6; }
	inline MotionManager_t3855993783 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(MotionManager_t3855993783 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CchangeMotionU3Ec__Iterator0_t472664859, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
