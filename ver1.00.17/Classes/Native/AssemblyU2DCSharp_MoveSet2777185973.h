﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// MobilmoAction
struct MobilmoAction_t109766523;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// ActionContents
struct ActionContents_t3118354192;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3948421699;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveSet
struct  MoveSet_t2777185973  : public MonoBehaviour_t1158329972
{
public:
	// MobilmoAction MoveSet::mobilmoAction
	MobilmoAction_t109766523 * ___mobilmoAction_2;
	// UnityEngine.GameObject[] MoveSet::ActionArea
	GameObjectU5BU5D_t3057952154* ___ActionArea_3;
	// ActionContents MoveSet::ActionContents
	ActionContents_t3118354192 * ___ActionContents_4;
	// UnityEngine.UI.Image[] MoveSet::ActionButtons
	ImageU5BU5D_t590162004* ___ActionButtons_5;
	// UnityEngine.RectTransform[] MoveSet::ActionButtonTrans
	RectTransformU5BU5D_t3948421699* ___ActionButtonTrans_6;
	// System.Boolean MoveSet::m_bTutorialNext
	bool ___m_bTutorialNext_7;

public:
	inline static int32_t get_offset_of_mobilmoAction_2() { return static_cast<int32_t>(offsetof(MoveSet_t2777185973, ___mobilmoAction_2)); }
	inline MobilmoAction_t109766523 * get_mobilmoAction_2() const { return ___mobilmoAction_2; }
	inline MobilmoAction_t109766523 ** get_address_of_mobilmoAction_2() { return &___mobilmoAction_2; }
	inline void set_mobilmoAction_2(MobilmoAction_t109766523 * value)
	{
		___mobilmoAction_2 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoAction_2, value);
	}

	inline static int32_t get_offset_of_ActionArea_3() { return static_cast<int32_t>(offsetof(MoveSet_t2777185973, ___ActionArea_3)); }
	inline GameObjectU5BU5D_t3057952154* get_ActionArea_3() const { return ___ActionArea_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ActionArea_3() { return &___ActionArea_3; }
	inline void set_ActionArea_3(GameObjectU5BU5D_t3057952154* value)
	{
		___ActionArea_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActionArea_3, value);
	}

	inline static int32_t get_offset_of_ActionContents_4() { return static_cast<int32_t>(offsetof(MoveSet_t2777185973, ___ActionContents_4)); }
	inline ActionContents_t3118354192 * get_ActionContents_4() const { return ___ActionContents_4; }
	inline ActionContents_t3118354192 ** get_address_of_ActionContents_4() { return &___ActionContents_4; }
	inline void set_ActionContents_4(ActionContents_t3118354192 * value)
	{
		___ActionContents_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionContents_4, value);
	}

	inline static int32_t get_offset_of_ActionButtons_5() { return static_cast<int32_t>(offsetof(MoveSet_t2777185973, ___ActionButtons_5)); }
	inline ImageU5BU5D_t590162004* get_ActionButtons_5() const { return ___ActionButtons_5; }
	inline ImageU5BU5D_t590162004** get_address_of_ActionButtons_5() { return &___ActionButtons_5; }
	inline void set_ActionButtons_5(ImageU5BU5D_t590162004* value)
	{
		___ActionButtons_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButtons_5, value);
	}

	inline static int32_t get_offset_of_ActionButtonTrans_6() { return static_cast<int32_t>(offsetof(MoveSet_t2777185973, ___ActionButtonTrans_6)); }
	inline RectTransformU5BU5D_t3948421699* get_ActionButtonTrans_6() const { return ___ActionButtonTrans_6; }
	inline RectTransformU5BU5D_t3948421699** get_address_of_ActionButtonTrans_6() { return &___ActionButtonTrans_6; }
	inline void set_ActionButtonTrans_6(RectTransformU5BU5D_t3948421699* value)
	{
		___ActionButtonTrans_6 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButtonTrans_6, value);
	}

	inline static int32_t get_offset_of_m_bTutorialNext_7() { return static_cast<int32_t>(offsetof(MoveSet_t2777185973, ___m_bTutorialNext_7)); }
	inline bool get_m_bTutorialNext_7() const { return ___m_bTutorialNext_7; }
	inline bool* get_address_of_m_bTutorialNext_7() { return &___m_bTutorialNext_7; }
	inline void set_m_bTutorialNext_7(bool value)
	{
		___m_bTutorialNext_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
