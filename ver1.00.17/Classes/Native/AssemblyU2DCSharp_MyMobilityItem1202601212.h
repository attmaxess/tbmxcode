﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyMobilityItem
struct  MyMobilityItem_t1202601212  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MyMobilityItem::_mobilityDetailNameText
	Text_t356221433 * ____mobilityDetailNameText_2;
	// UnityEngine.UI.Text MyMobilityItem::_mobilityDescriptionText
	Text_t356221433 * ____mobilityDescriptionText_3;
	// UnityEngine.UI.Text MyMobilityItem::_mobilityidText
	Text_t356221433 * ____mobilityidText_4;
	// System.Int32 MyMobilityItem::myPageMobilityId
	int32_t ___myPageMobilityId_5;
	// System.Int32 MyMobilityItem::myPageFloorNo
	int32_t ___myPageFloorNo_6;

public:
	inline static int32_t get_offset_of__mobilityDetailNameText_2() { return static_cast<int32_t>(offsetof(MyMobilityItem_t1202601212, ____mobilityDetailNameText_2)); }
	inline Text_t356221433 * get__mobilityDetailNameText_2() const { return ____mobilityDetailNameText_2; }
	inline Text_t356221433 ** get_address_of__mobilityDetailNameText_2() { return &____mobilityDetailNameText_2; }
	inline void set__mobilityDetailNameText_2(Text_t356221433 * value)
	{
		____mobilityDetailNameText_2 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityDetailNameText_2, value);
	}

	inline static int32_t get_offset_of__mobilityDescriptionText_3() { return static_cast<int32_t>(offsetof(MyMobilityItem_t1202601212, ____mobilityDescriptionText_3)); }
	inline Text_t356221433 * get__mobilityDescriptionText_3() const { return ____mobilityDescriptionText_3; }
	inline Text_t356221433 ** get_address_of__mobilityDescriptionText_3() { return &____mobilityDescriptionText_3; }
	inline void set__mobilityDescriptionText_3(Text_t356221433 * value)
	{
		____mobilityDescriptionText_3 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityDescriptionText_3, value);
	}

	inline static int32_t get_offset_of__mobilityidText_4() { return static_cast<int32_t>(offsetof(MyMobilityItem_t1202601212, ____mobilityidText_4)); }
	inline Text_t356221433 * get__mobilityidText_4() const { return ____mobilityidText_4; }
	inline Text_t356221433 ** get_address_of__mobilityidText_4() { return &____mobilityidText_4; }
	inline void set__mobilityidText_4(Text_t356221433 * value)
	{
		____mobilityidText_4 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityidText_4, value);
	}

	inline static int32_t get_offset_of_myPageMobilityId_5() { return static_cast<int32_t>(offsetof(MyMobilityItem_t1202601212, ___myPageMobilityId_5)); }
	inline int32_t get_myPageMobilityId_5() const { return ___myPageMobilityId_5; }
	inline int32_t* get_address_of_myPageMobilityId_5() { return &___myPageMobilityId_5; }
	inline void set_myPageMobilityId_5(int32_t value)
	{
		___myPageMobilityId_5 = value;
	}

	inline static int32_t get_offset_of_myPageFloorNo_6() { return static_cast<int32_t>(offsetof(MyMobilityItem_t1202601212, ___myPageFloorNo_6)); }
	inline int32_t get_myPageFloorNo_6() const { return ___myPageFloorNo_6; }
	inline int32_t* get_address_of_myPageFloorNo_6() { return &___myPageFloorNo_6; }
	inline void set_myPageFloorNo_6(int32_t value)
	{
		___myPageFloorNo_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
