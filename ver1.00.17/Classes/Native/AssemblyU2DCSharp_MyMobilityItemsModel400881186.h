﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<GetMyMobilityItemsReactionItemsList>
struct List_1_t3821816970;
// System.Collections.Generic.List`1<GetMyMobilityEncountList>
struct List_1_t3403082143;
// System.Collections.Generic.List`1<GetMyMobilityRegisterdMotionList>
struct List_1_t308780408;
// System.Collections.Generic.List`1<GetMyMobilityRecordedMotionList>
struct List_1_t3731186501;
// System.Collections.Generic.List`1<GetMyMobilityChildPartsListModel>
struct List_1_t2449762200;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyMobilityItemsModel
struct  MyMobilityItemsModel_t400881186  : public Model_t873752437
{
public:
	// System.Int32 MyMobilityItemsModel::<myMobility_items_Mobility_Id>k__BackingField
	int32_t ___U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0;
	// System.String MyMobilityItemsModel::<myMobility_items_date>k__BackingField
	String_t* ___U3CmyMobility_items_dateU3Ek__BackingField_1;
	// System.String MyMobilityItemsModel::<myMobility_items_name>k__BackingField
	String_t* ___U3CmyMobility_items_nameU3Ek__BackingField_2;
	// System.String MyMobilityItemsModel::<myMobility_items_situation>k__BackingField
	String_t* ___U3CmyMobility_items_situationU3Ek__BackingField_3;
	// System.String MyMobilityItemsModel::<myMobility_items_description>k__BackingField
	String_t* ___U3CmyMobility_items_descriptionU3Ek__BackingField_4;
	// System.Int32 MyMobilityItemsModel::<myMobility_items_parentId>k__BackingField
	int32_t ___U3CmyMobility_items_parentIdU3Ek__BackingField_5;
	// System.Int32 MyMobilityItemsModel::<myMobility_items_corePartsId>k__BackingField
	int32_t ___U3CmyMobility_items_corePartsIdU3Ek__BackingField_6;
	// System.String MyMobilityItemsModel::<myMobility_items_corePartsName>k__BackingField
	String_t* ___U3CmyMobility_items_corePartsNameU3Ek__BackingField_7;
	// System.Int32 MyMobilityItemsModel::<myMobility_items_createdUser_Id>k__BackingField
	int32_t ___U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8;
	// System.String MyMobilityItemsModel::<myMobility_items_createdUser_Name>k__BackingField
	String_t* ___U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9;
	// System.String MyMobilityItemsModel::<myMobility_items_createdUser_Rank>k__BackingField
	String_t* ___U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10;
	// System.Int32 MyMobilityItemsModel::<myMobility_items_coreParts_Color>k__BackingField
	int32_t ___U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11;
	// System.Single MyMobilityItemsModel::<myMobility_items_coreParts_Scale_X>k__BackingField
	float ___U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12;
	// System.Single MyMobilityItemsModel::<myMobility_items_coreParts_Scale_Y>k__BackingField
	float ___U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13;
	// System.Single MyMobilityItemsModel::<myMobility_items_coreParts_Scale_Z>k__BackingField
	float ___U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14;
	// System.Int32 MyMobilityItemsModel::<myMobility_reaction_allcount>k__BackingField
	int32_t ___U3CmyMobility_reaction_allcountU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<System.Object> MyMobilityItemsModel::myMobility_prized_list
	List_1_t2058570427 * ___myMobility_prized_list_16;
	// System.Collections.Generic.List`1<GetMyMobilityItemsReactionItemsList> MyMobilityItemsModel::<_myMobility_reaction_items_list>k__BackingField
	List_1_t3821816970 * ___U3C_myMobility_reaction_items_listU3Ek__BackingField_17;
	// System.Collections.Generic.List`1<GetMyMobilityEncountList> MyMobilityItemsModel::<_myMobility_encount_list>k__BackingField
	List_1_t3403082143 * ___U3C_myMobility_encount_listU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<GetMyMobilityRegisterdMotionList> MyMobilityItemsModel::<_myMobility_registerMotion_list>k__BackingField
	List_1_t308780408 * ___U3C_myMobility_registerMotion_listU3Ek__BackingField_19;
	// System.Collections.Generic.List`1<GetMyMobilityRecordedMotionList> MyMobilityItemsModel::<_myMobility_recordedMotion_list>k__BackingField
	List_1_t3731186501 * ___U3C_myMobility_recordedMotion_listU3Ek__BackingField_20;
	// System.Collections.Generic.List`1<GetMyMobilityChildPartsListModel> MyMobilityItemsModel::<_myMobility_childParts_list>k__BackingField
	List_1_t2449762200 * ___U3C_myMobility_childParts_listU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0() const { return ___U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0() { return &___U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0; }
	inline void set_U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3CmyMobility_items_Mobility_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_dateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_dateU3Ek__BackingField_1)); }
	inline String_t* get_U3CmyMobility_items_dateU3Ek__BackingField_1() const { return ___U3CmyMobility_items_dateU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmyMobility_items_dateU3Ek__BackingField_1() { return &___U3CmyMobility_items_dateU3Ek__BackingField_1; }
	inline void set_U3CmyMobility_items_dateU3Ek__BackingField_1(String_t* value)
	{
		___U3CmyMobility_items_dateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_items_dateU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_nameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_nameU3Ek__BackingField_2)); }
	inline String_t* get_U3CmyMobility_items_nameU3Ek__BackingField_2() const { return ___U3CmyMobility_items_nameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CmyMobility_items_nameU3Ek__BackingField_2() { return &___U3CmyMobility_items_nameU3Ek__BackingField_2; }
	inline void set_U3CmyMobility_items_nameU3Ek__BackingField_2(String_t* value)
	{
		___U3CmyMobility_items_nameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_items_nameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_situationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_situationU3Ek__BackingField_3)); }
	inline String_t* get_U3CmyMobility_items_situationU3Ek__BackingField_3() const { return ___U3CmyMobility_items_situationU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CmyMobility_items_situationU3Ek__BackingField_3() { return &___U3CmyMobility_items_situationU3Ek__BackingField_3; }
	inline void set_U3CmyMobility_items_situationU3Ek__BackingField_3(String_t* value)
	{
		___U3CmyMobility_items_situationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_items_situationU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_descriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_descriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CmyMobility_items_descriptionU3Ek__BackingField_4() const { return ___U3CmyMobility_items_descriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CmyMobility_items_descriptionU3Ek__BackingField_4() { return &___U3CmyMobility_items_descriptionU3Ek__BackingField_4; }
	inline void set_U3CmyMobility_items_descriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CmyMobility_items_descriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_items_descriptionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_parentIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_parentIdU3Ek__BackingField_5)); }
	inline int32_t get_U3CmyMobility_items_parentIdU3Ek__BackingField_5() const { return ___U3CmyMobility_items_parentIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CmyMobility_items_parentIdU3Ek__BackingField_5() { return &___U3CmyMobility_items_parentIdU3Ek__BackingField_5; }
	inline void set_U3CmyMobility_items_parentIdU3Ek__BackingField_5(int32_t value)
	{
		___U3CmyMobility_items_parentIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_corePartsIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_corePartsIdU3Ek__BackingField_6)); }
	inline int32_t get_U3CmyMobility_items_corePartsIdU3Ek__BackingField_6() const { return ___U3CmyMobility_items_corePartsIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CmyMobility_items_corePartsIdU3Ek__BackingField_6() { return &___U3CmyMobility_items_corePartsIdU3Ek__BackingField_6; }
	inline void set_U3CmyMobility_items_corePartsIdU3Ek__BackingField_6(int32_t value)
	{
		___U3CmyMobility_items_corePartsIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_corePartsNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_corePartsNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CmyMobility_items_corePartsNameU3Ek__BackingField_7() const { return ___U3CmyMobility_items_corePartsNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CmyMobility_items_corePartsNameU3Ek__BackingField_7() { return &___U3CmyMobility_items_corePartsNameU3Ek__BackingField_7; }
	inline void set_U3CmyMobility_items_corePartsNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CmyMobility_items_corePartsNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_items_corePartsNameU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8)); }
	inline int32_t get_U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8() const { return ___U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8() { return &___U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8; }
	inline void set_U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8(int32_t value)
	{
		___U3CmyMobility_items_createdUser_IdU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9)); }
	inline String_t* get_U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9() const { return ___U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9() { return &___U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9; }
	inline void set_U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9(String_t* value)
	{
		___U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_items_createdUser_NameU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10)); }
	inline String_t* get_U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10() const { return ___U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10() { return &___U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10; }
	inline void set_U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10(String_t* value)
	{
		___U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyMobility_items_createdUser_RankU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11)); }
	inline int32_t get_U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11() const { return ___U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11() { return &___U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11; }
	inline void set_U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11(int32_t value)
	{
		___U3CmyMobility_items_coreParts_ColorU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12)); }
	inline float get_U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12() const { return ___U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12; }
	inline float* get_address_of_U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12() { return &___U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12; }
	inline void set_U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12(float value)
	{
		___U3CmyMobility_items_coreParts_Scale_XU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13)); }
	inline float get_U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13() const { return ___U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13; }
	inline float* get_address_of_U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13() { return &___U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13; }
	inline void set_U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13(float value)
	{
		___U3CmyMobility_items_coreParts_Scale_YU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14)); }
	inline float get_U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14() const { return ___U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14; }
	inline float* get_address_of_U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14() { return &___U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14; }
	inline void set_U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14(float value)
	{
		___U3CmyMobility_items_coreParts_Scale_ZU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CmyMobility_reaction_allcountU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3CmyMobility_reaction_allcountU3Ek__BackingField_15)); }
	inline int32_t get_U3CmyMobility_reaction_allcountU3Ek__BackingField_15() const { return ___U3CmyMobility_reaction_allcountU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CmyMobility_reaction_allcountU3Ek__BackingField_15() { return &___U3CmyMobility_reaction_allcountU3Ek__BackingField_15; }
	inline void set_U3CmyMobility_reaction_allcountU3Ek__BackingField_15(int32_t value)
	{
		___U3CmyMobility_reaction_allcountU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_myMobility_prized_list_16() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___myMobility_prized_list_16)); }
	inline List_1_t2058570427 * get_myMobility_prized_list_16() const { return ___myMobility_prized_list_16; }
	inline List_1_t2058570427 ** get_address_of_myMobility_prized_list_16() { return &___myMobility_prized_list_16; }
	inline void set_myMobility_prized_list_16(List_1_t2058570427 * value)
	{
		___myMobility_prized_list_16 = value;
		Il2CppCodeGenWriteBarrier(&___myMobility_prized_list_16, value);
	}

	inline static int32_t get_offset_of_U3C_myMobility_reaction_items_listU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3C_myMobility_reaction_items_listU3Ek__BackingField_17)); }
	inline List_1_t3821816970 * get_U3C_myMobility_reaction_items_listU3Ek__BackingField_17() const { return ___U3C_myMobility_reaction_items_listU3Ek__BackingField_17; }
	inline List_1_t3821816970 ** get_address_of_U3C_myMobility_reaction_items_listU3Ek__BackingField_17() { return &___U3C_myMobility_reaction_items_listU3Ek__BackingField_17; }
	inline void set_U3C_myMobility_reaction_items_listU3Ek__BackingField_17(List_1_t3821816970 * value)
	{
		___U3C_myMobility_reaction_items_listU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_myMobility_reaction_items_listU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3C_myMobility_encount_listU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3C_myMobility_encount_listU3Ek__BackingField_18)); }
	inline List_1_t3403082143 * get_U3C_myMobility_encount_listU3Ek__BackingField_18() const { return ___U3C_myMobility_encount_listU3Ek__BackingField_18; }
	inline List_1_t3403082143 ** get_address_of_U3C_myMobility_encount_listU3Ek__BackingField_18() { return &___U3C_myMobility_encount_listU3Ek__BackingField_18; }
	inline void set_U3C_myMobility_encount_listU3Ek__BackingField_18(List_1_t3403082143 * value)
	{
		___U3C_myMobility_encount_listU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_myMobility_encount_listU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3C_myMobility_registerMotion_listU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3C_myMobility_registerMotion_listU3Ek__BackingField_19)); }
	inline List_1_t308780408 * get_U3C_myMobility_registerMotion_listU3Ek__BackingField_19() const { return ___U3C_myMobility_registerMotion_listU3Ek__BackingField_19; }
	inline List_1_t308780408 ** get_address_of_U3C_myMobility_registerMotion_listU3Ek__BackingField_19() { return &___U3C_myMobility_registerMotion_listU3Ek__BackingField_19; }
	inline void set_U3C_myMobility_registerMotion_listU3Ek__BackingField_19(List_1_t308780408 * value)
	{
		___U3C_myMobility_registerMotion_listU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_myMobility_registerMotion_listU3Ek__BackingField_19, value);
	}

	inline static int32_t get_offset_of_U3C_myMobility_recordedMotion_listU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3C_myMobility_recordedMotion_listU3Ek__BackingField_20)); }
	inline List_1_t3731186501 * get_U3C_myMobility_recordedMotion_listU3Ek__BackingField_20() const { return ___U3C_myMobility_recordedMotion_listU3Ek__BackingField_20; }
	inline List_1_t3731186501 ** get_address_of_U3C_myMobility_recordedMotion_listU3Ek__BackingField_20() { return &___U3C_myMobility_recordedMotion_listU3Ek__BackingField_20; }
	inline void set_U3C_myMobility_recordedMotion_listU3Ek__BackingField_20(List_1_t3731186501 * value)
	{
		___U3C_myMobility_recordedMotion_listU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_myMobility_recordedMotion_listU3Ek__BackingField_20, value);
	}

	inline static int32_t get_offset_of_U3C_myMobility_childParts_listU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(MyMobilityItemsModel_t400881186, ___U3C_myMobility_childParts_listU3Ek__BackingField_21)); }
	inline List_1_t2449762200 * get_U3C_myMobility_childParts_listU3Ek__BackingField_21() const { return ___U3C_myMobility_childParts_listU3Ek__BackingField_21; }
	inline List_1_t2449762200 ** get_address_of_U3C_myMobility_childParts_listU3Ek__BackingField_21() { return &___U3C_myMobility_childParts_listU3Ek__BackingField_21; }
	inline void set_U3C_myMobility_childParts_listU3Ek__BackingField_21(List_1_t2449762200 * value)
	{
		___U3C_myMobility_childParts_listU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_myMobility_childParts_listU3Ek__BackingField_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
