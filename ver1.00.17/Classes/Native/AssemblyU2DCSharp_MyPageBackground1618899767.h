﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// MyPageBilmoNamePanel
struct MyPageBilmoNamePanel_t4154252521;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// MyPageFloor[]
struct MyPageFloorU5BU5D_t1754832696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageBackground
struct  MyPageBackground_t1618899767  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.CanvasGroup MyPageBackground::BilmoInfos
	CanvasGroup_t3296560743 * ___BilmoInfos_2;
	// UnityEngine.CanvasGroup MyPageBackground::SwitchButtons
	CanvasGroup_t3296560743 * ___SwitchButtons_3;
	// MyPageBilmoNamePanel MyPageBackground::BilmoPanel
	MyPageBilmoNamePanel_t4154252521 * ___BilmoPanel_4;
	// UnityEngine.GameObject[] MyPageBackground::FloorObjs
	GameObjectU5BU5D_t3057952154* ___FloorObjs_5;
	// UnityEngine.GameObject[] MyPageBackground::LoopObjs
	GameObjectU5BU5D_t3057952154* ___LoopObjs_6;
	// MyPageFloor[] MyPageBackground::Floors
	MyPageFloorU5BU5D_t1754832696* ___Floors_7;

public:
	inline static int32_t get_offset_of_BilmoInfos_2() { return static_cast<int32_t>(offsetof(MyPageBackground_t1618899767, ___BilmoInfos_2)); }
	inline CanvasGroup_t3296560743 * get_BilmoInfos_2() const { return ___BilmoInfos_2; }
	inline CanvasGroup_t3296560743 ** get_address_of_BilmoInfos_2() { return &___BilmoInfos_2; }
	inline void set_BilmoInfos_2(CanvasGroup_t3296560743 * value)
	{
		___BilmoInfos_2 = value;
		Il2CppCodeGenWriteBarrier(&___BilmoInfos_2, value);
	}

	inline static int32_t get_offset_of_SwitchButtons_3() { return static_cast<int32_t>(offsetof(MyPageBackground_t1618899767, ___SwitchButtons_3)); }
	inline CanvasGroup_t3296560743 * get_SwitchButtons_3() const { return ___SwitchButtons_3; }
	inline CanvasGroup_t3296560743 ** get_address_of_SwitchButtons_3() { return &___SwitchButtons_3; }
	inline void set_SwitchButtons_3(CanvasGroup_t3296560743 * value)
	{
		___SwitchButtons_3 = value;
		Il2CppCodeGenWriteBarrier(&___SwitchButtons_3, value);
	}

	inline static int32_t get_offset_of_BilmoPanel_4() { return static_cast<int32_t>(offsetof(MyPageBackground_t1618899767, ___BilmoPanel_4)); }
	inline MyPageBilmoNamePanel_t4154252521 * get_BilmoPanel_4() const { return ___BilmoPanel_4; }
	inline MyPageBilmoNamePanel_t4154252521 ** get_address_of_BilmoPanel_4() { return &___BilmoPanel_4; }
	inline void set_BilmoPanel_4(MyPageBilmoNamePanel_t4154252521 * value)
	{
		___BilmoPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___BilmoPanel_4, value);
	}

	inline static int32_t get_offset_of_FloorObjs_5() { return static_cast<int32_t>(offsetof(MyPageBackground_t1618899767, ___FloorObjs_5)); }
	inline GameObjectU5BU5D_t3057952154* get_FloorObjs_5() const { return ___FloorObjs_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_FloorObjs_5() { return &___FloorObjs_5; }
	inline void set_FloorObjs_5(GameObjectU5BU5D_t3057952154* value)
	{
		___FloorObjs_5 = value;
		Il2CppCodeGenWriteBarrier(&___FloorObjs_5, value);
	}

	inline static int32_t get_offset_of_LoopObjs_6() { return static_cast<int32_t>(offsetof(MyPageBackground_t1618899767, ___LoopObjs_6)); }
	inline GameObjectU5BU5D_t3057952154* get_LoopObjs_6() const { return ___LoopObjs_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_LoopObjs_6() { return &___LoopObjs_6; }
	inline void set_LoopObjs_6(GameObjectU5BU5D_t3057952154* value)
	{
		___LoopObjs_6 = value;
		Il2CppCodeGenWriteBarrier(&___LoopObjs_6, value);
	}

	inline static int32_t get_offset_of_Floors_7() { return static_cast<int32_t>(offsetof(MyPageBackground_t1618899767, ___Floors_7)); }
	inline MyPageFloorU5BU5D_t1754832696* get_Floors_7() const { return ___Floors_7; }
	inline MyPageFloorU5BU5D_t1754832696** get_address_of_Floors_7() { return &___Floors_7; }
	inline void set_Floors_7(MyPageFloorU5BU5D_t1754832696* value)
	{
		___Floors_7 = value;
		Il2CppCodeGenWriteBarrier(&___Floors_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
