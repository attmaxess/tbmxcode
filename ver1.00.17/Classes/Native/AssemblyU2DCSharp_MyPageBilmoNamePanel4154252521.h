﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageBilmoNamePanel
struct  MyPageBilmoNamePanel_t4154252521  : public Il2CppObject
{
public:
	// UnityEngine.UI.Text MyPageBilmoNamePanel::Number
	Text_t356221433 * ___Number_0;
	// UnityEngine.UI.Text MyPageBilmoNamePanel::Name
	Text_t356221433 * ___Name_1;

public:
	inline static int32_t get_offset_of_Number_0() { return static_cast<int32_t>(offsetof(MyPageBilmoNamePanel_t4154252521, ___Number_0)); }
	inline Text_t356221433 * get_Number_0() const { return ___Number_0; }
	inline Text_t356221433 ** get_address_of_Number_0() { return &___Number_0; }
	inline void set_Number_0(Text_t356221433 * value)
	{
		___Number_0 = value;
		Il2CppCodeGenWriteBarrier(&___Number_0, value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(MyPageBilmoNamePanel_t4154252521, ___Name_1)); }
	inline Text_t356221433 * get_Name_1() const { return ___Name_1; }
	inline Text_t356221433 ** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(Text_t356221433 * value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier(&___Name_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
