﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1967201810;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// MypageMenuItem
struct MypageMenuItem_t3200819439;
// MyPageCounters
struct MyPageCounters_t1952450212;
// MyPageMenuBar
struct MyPageMenuBar_t3059127011;
// MyPageBackground
struct MyPageBackground_t1618899767;
// MobilmoAction
struct MobilmoAction_t109766523;
// MobilmoDnaController
struct MobilmoDnaController_t1006271200;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageContents
struct  MyPageContents_t4122237443  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MyPageContents::MobilmoObj
	GameObject_t1756533147 * ___MobilmoObj_2;
	// UnityEngine.UI.Button MyPageContents::WorldButton
	Button_t2872111280 * ___WorldButton_3;
	// UnityEngine.EventSystems.EventTrigger MyPageContents::WorldButtonEvent
	EventTrigger_t1967201810 * ___WorldButtonEvent_4;
	// UnityEngine.UI.Button MyPageContents::HeartButton
	Button_t2872111280 * ___HeartButton_5;
	// UnityEngine.CanvasGroup MyPageContents::ReactionPanel
	CanvasGroup_t3296560743 * ___ReactionPanel_6;
	// UnityEngine.UI.Button[] MyPageContents::TopMenus
	ButtonU5BU5D_t3071100561* ___TopMenus_7;
	// MypageMenuItem MyPageContents::PartslistItem
	MypageMenuItem_t3200819439 * ___PartslistItem_8;
	// MyPageCounters MyPageContents::Counters
	MyPageCounters_t1952450212 * ___Counters_9;
	// MyPageMenuBar MyPageContents::MenuBar
	MyPageMenuBar_t3059127011 * ___MenuBar_10;
	// MyPageBackground MyPageContents::Background
	MyPageBackground_t1618899767 * ___Background_11;
	// MobilmoAction MyPageContents::mobilmoAction
	MobilmoAction_t109766523 * ___mobilmoAction_12;
	// MobilmoDnaController MyPageContents::m_mobilmoDnaController
	MobilmoDnaController_t1006271200 * ___m_mobilmoDnaController_13;
	// UnityEngine.GameObject MyPageContents::mypageDnaCloseButton
	GameObject_t1756533147 * ___mypageDnaCloseButton_14;
	// System.Boolean MyPageContents::m_bActive
	bool ___m_bActive_15;
	// UnityEngine.UI.Text MyPageContents::mobilmoCntText
	Text_t356221433 * ___mobilmoCntText_16;

public:
	inline static int32_t get_offset_of_MobilmoObj_2() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___MobilmoObj_2)); }
	inline GameObject_t1756533147 * get_MobilmoObj_2() const { return ___MobilmoObj_2; }
	inline GameObject_t1756533147 ** get_address_of_MobilmoObj_2() { return &___MobilmoObj_2; }
	inline void set_MobilmoObj_2(GameObject_t1756533147 * value)
	{
		___MobilmoObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___MobilmoObj_2, value);
	}

	inline static int32_t get_offset_of_WorldButton_3() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___WorldButton_3)); }
	inline Button_t2872111280 * get_WorldButton_3() const { return ___WorldButton_3; }
	inline Button_t2872111280 ** get_address_of_WorldButton_3() { return &___WorldButton_3; }
	inline void set_WorldButton_3(Button_t2872111280 * value)
	{
		___WorldButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___WorldButton_3, value);
	}

	inline static int32_t get_offset_of_WorldButtonEvent_4() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___WorldButtonEvent_4)); }
	inline EventTrigger_t1967201810 * get_WorldButtonEvent_4() const { return ___WorldButtonEvent_4; }
	inline EventTrigger_t1967201810 ** get_address_of_WorldButtonEvent_4() { return &___WorldButtonEvent_4; }
	inline void set_WorldButtonEvent_4(EventTrigger_t1967201810 * value)
	{
		___WorldButtonEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___WorldButtonEvent_4, value);
	}

	inline static int32_t get_offset_of_HeartButton_5() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___HeartButton_5)); }
	inline Button_t2872111280 * get_HeartButton_5() const { return ___HeartButton_5; }
	inline Button_t2872111280 ** get_address_of_HeartButton_5() { return &___HeartButton_5; }
	inline void set_HeartButton_5(Button_t2872111280 * value)
	{
		___HeartButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___HeartButton_5, value);
	}

	inline static int32_t get_offset_of_ReactionPanel_6() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___ReactionPanel_6)); }
	inline CanvasGroup_t3296560743 * get_ReactionPanel_6() const { return ___ReactionPanel_6; }
	inline CanvasGroup_t3296560743 ** get_address_of_ReactionPanel_6() { return &___ReactionPanel_6; }
	inline void set_ReactionPanel_6(CanvasGroup_t3296560743 * value)
	{
		___ReactionPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___ReactionPanel_6, value);
	}

	inline static int32_t get_offset_of_TopMenus_7() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___TopMenus_7)); }
	inline ButtonU5BU5D_t3071100561* get_TopMenus_7() const { return ___TopMenus_7; }
	inline ButtonU5BU5D_t3071100561** get_address_of_TopMenus_7() { return &___TopMenus_7; }
	inline void set_TopMenus_7(ButtonU5BU5D_t3071100561* value)
	{
		___TopMenus_7 = value;
		Il2CppCodeGenWriteBarrier(&___TopMenus_7, value);
	}

	inline static int32_t get_offset_of_PartslistItem_8() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___PartslistItem_8)); }
	inline MypageMenuItem_t3200819439 * get_PartslistItem_8() const { return ___PartslistItem_8; }
	inline MypageMenuItem_t3200819439 ** get_address_of_PartslistItem_8() { return &___PartslistItem_8; }
	inline void set_PartslistItem_8(MypageMenuItem_t3200819439 * value)
	{
		___PartslistItem_8 = value;
		Il2CppCodeGenWriteBarrier(&___PartslistItem_8, value);
	}

	inline static int32_t get_offset_of_Counters_9() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___Counters_9)); }
	inline MyPageCounters_t1952450212 * get_Counters_9() const { return ___Counters_9; }
	inline MyPageCounters_t1952450212 ** get_address_of_Counters_9() { return &___Counters_9; }
	inline void set_Counters_9(MyPageCounters_t1952450212 * value)
	{
		___Counters_9 = value;
		Il2CppCodeGenWriteBarrier(&___Counters_9, value);
	}

	inline static int32_t get_offset_of_MenuBar_10() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___MenuBar_10)); }
	inline MyPageMenuBar_t3059127011 * get_MenuBar_10() const { return ___MenuBar_10; }
	inline MyPageMenuBar_t3059127011 ** get_address_of_MenuBar_10() { return &___MenuBar_10; }
	inline void set_MenuBar_10(MyPageMenuBar_t3059127011 * value)
	{
		___MenuBar_10 = value;
		Il2CppCodeGenWriteBarrier(&___MenuBar_10, value);
	}

	inline static int32_t get_offset_of_Background_11() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___Background_11)); }
	inline MyPageBackground_t1618899767 * get_Background_11() const { return ___Background_11; }
	inline MyPageBackground_t1618899767 ** get_address_of_Background_11() { return &___Background_11; }
	inline void set_Background_11(MyPageBackground_t1618899767 * value)
	{
		___Background_11 = value;
		Il2CppCodeGenWriteBarrier(&___Background_11, value);
	}

	inline static int32_t get_offset_of_mobilmoAction_12() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___mobilmoAction_12)); }
	inline MobilmoAction_t109766523 * get_mobilmoAction_12() const { return ___mobilmoAction_12; }
	inline MobilmoAction_t109766523 ** get_address_of_mobilmoAction_12() { return &___mobilmoAction_12; }
	inline void set_mobilmoAction_12(MobilmoAction_t109766523 * value)
	{
		___mobilmoAction_12 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoAction_12, value);
	}

	inline static int32_t get_offset_of_m_mobilmoDnaController_13() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___m_mobilmoDnaController_13)); }
	inline MobilmoDnaController_t1006271200 * get_m_mobilmoDnaController_13() const { return ___m_mobilmoDnaController_13; }
	inline MobilmoDnaController_t1006271200 ** get_address_of_m_mobilmoDnaController_13() { return &___m_mobilmoDnaController_13; }
	inline void set_m_mobilmoDnaController_13(MobilmoDnaController_t1006271200 * value)
	{
		___m_mobilmoDnaController_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_mobilmoDnaController_13, value);
	}

	inline static int32_t get_offset_of_mypageDnaCloseButton_14() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___mypageDnaCloseButton_14)); }
	inline GameObject_t1756533147 * get_mypageDnaCloseButton_14() const { return ___mypageDnaCloseButton_14; }
	inline GameObject_t1756533147 ** get_address_of_mypageDnaCloseButton_14() { return &___mypageDnaCloseButton_14; }
	inline void set_mypageDnaCloseButton_14(GameObject_t1756533147 * value)
	{
		___mypageDnaCloseButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___mypageDnaCloseButton_14, value);
	}

	inline static int32_t get_offset_of_m_bActive_15() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___m_bActive_15)); }
	inline bool get_m_bActive_15() const { return ___m_bActive_15; }
	inline bool* get_address_of_m_bActive_15() { return &___m_bActive_15; }
	inline void set_m_bActive_15(bool value)
	{
		___m_bActive_15 = value;
	}

	inline static int32_t get_offset_of_mobilmoCntText_16() { return static_cast<int32_t>(offsetof(MyPageContents_t4122237443, ___mobilmoCntText_16)); }
	inline Text_t356221433 * get_mobilmoCntText_16() const { return ___mobilmoCntText_16; }
	inline Text_t356221433 ** get_address_of_mobilmoCntText_16() { return &___mobilmoCntText_16; }
	inline void set_mobilmoCntText_16(Text_t356221433 * value)
	{
		___mobilmoCntText_16 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoCntText_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
