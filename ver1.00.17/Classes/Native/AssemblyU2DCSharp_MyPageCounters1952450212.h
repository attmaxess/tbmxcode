﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageCounters
struct  MyPageCounters_t1952450212  : public Il2CppObject
{
public:
	// UnityEngine.UI.Text MyPageCounters::Heart
	Text_t356221433 * ___Heart_0;
	// UnityEngine.UI.Text MyPageCounters::Medal
	Text_t356221433 * ___Medal_1;
	// UnityEngine.UI.Text[] MyPageCounters::Reactions
	TextU5BU5D_t4216439300* ___Reactions_2;

public:
	inline static int32_t get_offset_of_Heart_0() { return static_cast<int32_t>(offsetof(MyPageCounters_t1952450212, ___Heart_0)); }
	inline Text_t356221433 * get_Heart_0() const { return ___Heart_0; }
	inline Text_t356221433 ** get_address_of_Heart_0() { return &___Heart_0; }
	inline void set_Heart_0(Text_t356221433 * value)
	{
		___Heart_0 = value;
		Il2CppCodeGenWriteBarrier(&___Heart_0, value);
	}

	inline static int32_t get_offset_of_Medal_1() { return static_cast<int32_t>(offsetof(MyPageCounters_t1952450212, ___Medal_1)); }
	inline Text_t356221433 * get_Medal_1() const { return ___Medal_1; }
	inline Text_t356221433 ** get_address_of_Medal_1() { return &___Medal_1; }
	inline void set_Medal_1(Text_t356221433 * value)
	{
		___Medal_1 = value;
		Il2CppCodeGenWriteBarrier(&___Medal_1, value);
	}

	inline static int32_t get_offset_of_Reactions_2() { return static_cast<int32_t>(offsetof(MyPageCounters_t1952450212, ___Reactions_2)); }
	inline TextU5BU5D_t4216439300* get_Reactions_2() const { return ___Reactions_2; }
	inline TextU5BU5D_t4216439300** get_address_of_Reactions_2() { return &___Reactions_2; }
	inline void set_Reactions_2(TextU5BU5D_t4216439300* value)
	{
		___Reactions_2 = value;
		Il2CppCodeGenWriteBarrier(&___Reactions_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
