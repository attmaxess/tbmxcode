﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageCreationLocation
struct  MyPageCreationLocation_t2722685817  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform MyPageCreationLocation::prevPrevTrans
	Transform_t3275118058 * ___prevPrevTrans_2;
	// UnityEngine.Transform MyPageCreationLocation::prevTrans
	Transform_t3275118058 * ___prevTrans_3;
	// UnityEngine.Transform MyPageCreationLocation::nowTrans
	Transform_t3275118058 * ___nowTrans_4;
	// UnityEngine.Transform MyPageCreationLocation::nextTrans
	Transform_t3275118058 * ___nextTrans_5;
	// UnityEngine.Transform MyPageCreationLocation::nextNextTrans
	Transform_t3275118058 * ___nextNextTrans_6;

public:
	inline static int32_t get_offset_of_prevPrevTrans_2() { return static_cast<int32_t>(offsetof(MyPageCreationLocation_t2722685817, ___prevPrevTrans_2)); }
	inline Transform_t3275118058 * get_prevPrevTrans_2() const { return ___prevPrevTrans_2; }
	inline Transform_t3275118058 ** get_address_of_prevPrevTrans_2() { return &___prevPrevTrans_2; }
	inline void set_prevPrevTrans_2(Transform_t3275118058 * value)
	{
		___prevPrevTrans_2 = value;
		Il2CppCodeGenWriteBarrier(&___prevPrevTrans_2, value);
	}

	inline static int32_t get_offset_of_prevTrans_3() { return static_cast<int32_t>(offsetof(MyPageCreationLocation_t2722685817, ___prevTrans_3)); }
	inline Transform_t3275118058 * get_prevTrans_3() const { return ___prevTrans_3; }
	inline Transform_t3275118058 ** get_address_of_prevTrans_3() { return &___prevTrans_3; }
	inline void set_prevTrans_3(Transform_t3275118058 * value)
	{
		___prevTrans_3 = value;
		Il2CppCodeGenWriteBarrier(&___prevTrans_3, value);
	}

	inline static int32_t get_offset_of_nowTrans_4() { return static_cast<int32_t>(offsetof(MyPageCreationLocation_t2722685817, ___nowTrans_4)); }
	inline Transform_t3275118058 * get_nowTrans_4() const { return ___nowTrans_4; }
	inline Transform_t3275118058 ** get_address_of_nowTrans_4() { return &___nowTrans_4; }
	inline void set_nowTrans_4(Transform_t3275118058 * value)
	{
		___nowTrans_4 = value;
		Il2CppCodeGenWriteBarrier(&___nowTrans_4, value);
	}

	inline static int32_t get_offset_of_nextTrans_5() { return static_cast<int32_t>(offsetof(MyPageCreationLocation_t2722685817, ___nextTrans_5)); }
	inline Transform_t3275118058 * get_nextTrans_5() const { return ___nextTrans_5; }
	inline Transform_t3275118058 ** get_address_of_nextTrans_5() { return &___nextTrans_5; }
	inline void set_nextTrans_5(Transform_t3275118058 * value)
	{
		___nextTrans_5 = value;
		Il2CppCodeGenWriteBarrier(&___nextTrans_5, value);
	}

	inline static int32_t get_offset_of_nextNextTrans_6() { return static_cast<int32_t>(offsetof(MyPageCreationLocation_t2722685817, ___nextNextTrans_6)); }
	inline Transform_t3275118058 * get_nextNextTrans_6() const { return ___nextNextTrans_6; }
	inline Transform_t3275118058 ** get_address_of_nextNextTrans_6() { return &___nextNextTrans_6; }
	inline void set_nextNextTrans_6(Transform_t3275118058 * value)
	{
		___nextNextTrans_6 = value;
		Il2CppCodeGenWriteBarrier(&___nextNextTrans_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
