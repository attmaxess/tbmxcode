﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageFloor
struct  MyPageFloor_t1585319381  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image MyPageFloor::On
	Image_t2042527209 * ___On_2;
	// UnityEngine.UI.Image MyPageFloor::Off
	Image_t2042527209 * ___Off_3;

public:
	inline static int32_t get_offset_of_On_2() { return static_cast<int32_t>(offsetof(MyPageFloor_t1585319381, ___On_2)); }
	inline Image_t2042527209 * get_On_2() const { return ___On_2; }
	inline Image_t2042527209 ** get_address_of_On_2() { return &___On_2; }
	inline void set_On_2(Image_t2042527209 * value)
	{
		___On_2 = value;
		Il2CppCodeGenWriteBarrier(&___On_2, value);
	}

	inline static int32_t get_offset_of_Off_3() { return static_cast<int32_t>(offsetof(MyPageFloor_t1585319381, ___Off_3)); }
	inline Image_t2042527209 * get_Off_3() const { return ___Off_3; }
	inline Image_t2042527209 ** get_address_of_Off_3() { return &___Off_3; }
	inline void set_Off_3(Image_t2042527209 * value)
	{
		___Off_3 = value;
		Il2CppCodeGenWriteBarrier(&___Off_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
