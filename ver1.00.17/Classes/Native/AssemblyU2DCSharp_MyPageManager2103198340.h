﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen799874675.h"

// MyPageContents
struct MyPageContents_t4122237443;
// MyPageCreationLocation
struct MyPageCreationLocation_t2722685817;
// System.Collections.Generic.List`1<MyPageMobilmoItem>
struct List_1_t4010263515;
// ContactSetting
struct ContactSetting_t3049199920;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageManager
struct  MyPageManager_t2103198340  : public SingletonMonoBehaviour_1_t799874675
{
public:
	// MyPageContents MyPageManager::Contents
	MyPageContents_t4122237443 * ___Contents_3;
	// MyPageCreationLocation MyPageManager::Location
	MyPageCreationLocation_t2722685817 * ___Location_4;
	// System.Collections.Generic.List`1<MyPageMobilmoItem> MyPageManager::MyMobilmoItemList
	List_1_t4010263515 * ___MyMobilmoItemList_5;
	// System.Int32 MyPageManager::SelectMoblityId
	int32_t ___SelectMoblityId_6;
	// ContactSetting MyPageManager::Contact
	ContactSetting_t3049199920 * ___Contact_7;
	// UnityEngine.GameObject[] MyPageManager::createPanel
	GameObjectU5BU5D_t3057952154* ___createPanel_8;
	// UnityEngine.GameObject MyPageManager::CurrentMobility
	GameObject_t1756533147 * ___CurrentMobility_9;
	// System.Int32 MyPageManager::MobilityCount
	int32_t ___MobilityCount_10;
	// System.Int32 MyPageManager::CurrentIndex
	int32_t ___CurrentIndex_11;
	// UnityEngine.GameObject MyPageManager::userSettingPanel
	GameObject_t1756533147 * ___userSettingPanel_12;
	// System.Int32 MyPageManager::pressMyPageCount
	int32_t ___pressMyPageCount_13;
	// DG.Tweening.Tweener MyPageManager::m_CurrentTween
	Tweener_t760404022 * ___m_CurrentTween_14;
	// DG.Tweening.Tweener MyPageManager::m_MovingTween
	Tweener_t760404022 * ___m_MovingTween_15;
	// System.Boolean MyPageManager::isCreatedMobility
	bool ___isCreatedMobility_16;
	// System.Int32 MyPageManager::pressCnt
	int32_t ___pressCnt_17;
	// UnityEngine.Vector3[] MyPageManager::LinePos
	Vector3U5BU5D_t1172311765* ___LinePos_18;
	// UnityEngine.Vector3[] MyPageManager::FloorPos
	Vector3U5BU5D_t1172311765* ___FloorPos_19;
	// System.Int32 MyPageManager::LineIndex
	int32_t ___LineIndex_20;
	// System.Int32 MyPageManager::FloorIndex
	int32_t ___FloorIndex_21;
	// System.Boolean MyPageManager::m_bSceneMoved
	bool ___m_bSceneMoved_22;
	// System.Boolean MyPageManager::isChangingMypage
	bool ___isChangingMypage_23;

public:
	inline static int32_t get_offset_of_Contents_3() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___Contents_3)); }
	inline MyPageContents_t4122237443 * get_Contents_3() const { return ___Contents_3; }
	inline MyPageContents_t4122237443 ** get_address_of_Contents_3() { return &___Contents_3; }
	inline void set_Contents_3(MyPageContents_t4122237443 * value)
	{
		___Contents_3 = value;
		Il2CppCodeGenWriteBarrier(&___Contents_3, value);
	}

	inline static int32_t get_offset_of_Location_4() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___Location_4)); }
	inline MyPageCreationLocation_t2722685817 * get_Location_4() const { return ___Location_4; }
	inline MyPageCreationLocation_t2722685817 ** get_address_of_Location_4() { return &___Location_4; }
	inline void set_Location_4(MyPageCreationLocation_t2722685817 * value)
	{
		___Location_4 = value;
		Il2CppCodeGenWriteBarrier(&___Location_4, value);
	}

	inline static int32_t get_offset_of_MyMobilmoItemList_5() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___MyMobilmoItemList_5)); }
	inline List_1_t4010263515 * get_MyMobilmoItemList_5() const { return ___MyMobilmoItemList_5; }
	inline List_1_t4010263515 ** get_address_of_MyMobilmoItemList_5() { return &___MyMobilmoItemList_5; }
	inline void set_MyMobilmoItemList_5(List_1_t4010263515 * value)
	{
		___MyMobilmoItemList_5 = value;
		Il2CppCodeGenWriteBarrier(&___MyMobilmoItemList_5, value);
	}

	inline static int32_t get_offset_of_SelectMoblityId_6() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___SelectMoblityId_6)); }
	inline int32_t get_SelectMoblityId_6() const { return ___SelectMoblityId_6; }
	inline int32_t* get_address_of_SelectMoblityId_6() { return &___SelectMoblityId_6; }
	inline void set_SelectMoblityId_6(int32_t value)
	{
		___SelectMoblityId_6 = value;
	}

	inline static int32_t get_offset_of_Contact_7() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___Contact_7)); }
	inline ContactSetting_t3049199920 * get_Contact_7() const { return ___Contact_7; }
	inline ContactSetting_t3049199920 ** get_address_of_Contact_7() { return &___Contact_7; }
	inline void set_Contact_7(ContactSetting_t3049199920 * value)
	{
		___Contact_7 = value;
		Il2CppCodeGenWriteBarrier(&___Contact_7, value);
	}

	inline static int32_t get_offset_of_createPanel_8() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___createPanel_8)); }
	inline GameObjectU5BU5D_t3057952154* get_createPanel_8() const { return ___createPanel_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_createPanel_8() { return &___createPanel_8; }
	inline void set_createPanel_8(GameObjectU5BU5D_t3057952154* value)
	{
		___createPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___createPanel_8, value);
	}

	inline static int32_t get_offset_of_CurrentMobility_9() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___CurrentMobility_9)); }
	inline GameObject_t1756533147 * get_CurrentMobility_9() const { return ___CurrentMobility_9; }
	inline GameObject_t1756533147 ** get_address_of_CurrentMobility_9() { return &___CurrentMobility_9; }
	inline void set_CurrentMobility_9(GameObject_t1756533147 * value)
	{
		___CurrentMobility_9 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentMobility_9, value);
	}

	inline static int32_t get_offset_of_MobilityCount_10() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___MobilityCount_10)); }
	inline int32_t get_MobilityCount_10() const { return ___MobilityCount_10; }
	inline int32_t* get_address_of_MobilityCount_10() { return &___MobilityCount_10; }
	inline void set_MobilityCount_10(int32_t value)
	{
		___MobilityCount_10 = value;
	}

	inline static int32_t get_offset_of_CurrentIndex_11() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___CurrentIndex_11)); }
	inline int32_t get_CurrentIndex_11() const { return ___CurrentIndex_11; }
	inline int32_t* get_address_of_CurrentIndex_11() { return &___CurrentIndex_11; }
	inline void set_CurrentIndex_11(int32_t value)
	{
		___CurrentIndex_11 = value;
	}

	inline static int32_t get_offset_of_userSettingPanel_12() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___userSettingPanel_12)); }
	inline GameObject_t1756533147 * get_userSettingPanel_12() const { return ___userSettingPanel_12; }
	inline GameObject_t1756533147 ** get_address_of_userSettingPanel_12() { return &___userSettingPanel_12; }
	inline void set_userSettingPanel_12(GameObject_t1756533147 * value)
	{
		___userSettingPanel_12 = value;
		Il2CppCodeGenWriteBarrier(&___userSettingPanel_12, value);
	}

	inline static int32_t get_offset_of_pressMyPageCount_13() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___pressMyPageCount_13)); }
	inline int32_t get_pressMyPageCount_13() const { return ___pressMyPageCount_13; }
	inline int32_t* get_address_of_pressMyPageCount_13() { return &___pressMyPageCount_13; }
	inline void set_pressMyPageCount_13(int32_t value)
	{
		___pressMyPageCount_13 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTween_14() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___m_CurrentTween_14)); }
	inline Tweener_t760404022 * get_m_CurrentTween_14() const { return ___m_CurrentTween_14; }
	inline Tweener_t760404022 ** get_address_of_m_CurrentTween_14() { return &___m_CurrentTween_14; }
	inline void set_m_CurrentTween_14(Tweener_t760404022 * value)
	{
		___m_CurrentTween_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentTween_14, value);
	}

	inline static int32_t get_offset_of_m_MovingTween_15() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___m_MovingTween_15)); }
	inline Tweener_t760404022 * get_m_MovingTween_15() const { return ___m_MovingTween_15; }
	inline Tweener_t760404022 ** get_address_of_m_MovingTween_15() { return &___m_MovingTween_15; }
	inline void set_m_MovingTween_15(Tweener_t760404022 * value)
	{
		___m_MovingTween_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_MovingTween_15, value);
	}

	inline static int32_t get_offset_of_isCreatedMobility_16() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___isCreatedMobility_16)); }
	inline bool get_isCreatedMobility_16() const { return ___isCreatedMobility_16; }
	inline bool* get_address_of_isCreatedMobility_16() { return &___isCreatedMobility_16; }
	inline void set_isCreatedMobility_16(bool value)
	{
		___isCreatedMobility_16 = value;
	}

	inline static int32_t get_offset_of_pressCnt_17() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___pressCnt_17)); }
	inline int32_t get_pressCnt_17() const { return ___pressCnt_17; }
	inline int32_t* get_address_of_pressCnt_17() { return &___pressCnt_17; }
	inline void set_pressCnt_17(int32_t value)
	{
		___pressCnt_17 = value;
	}

	inline static int32_t get_offset_of_LinePos_18() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___LinePos_18)); }
	inline Vector3U5BU5D_t1172311765* get_LinePos_18() const { return ___LinePos_18; }
	inline Vector3U5BU5D_t1172311765** get_address_of_LinePos_18() { return &___LinePos_18; }
	inline void set_LinePos_18(Vector3U5BU5D_t1172311765* value)
	{
		___LinePos_18 = value;
		Il2CppCodeGenWriteBarrier(&___LinePos_18, value);
	}

	inline static int32_t get_offset_of_FloorPos_19() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___FloorPos_19)); }
	inline Vector3U5BU5D_t1172311765* get_FloorPos_19() const { return ___FloorPos_19; }
	inline Vector3U5BU5D_t1172311765** get_address_of_FloorPos_19() { return &___FloorPos_19; }
	inline void set_FloorPos_19(Vector3U5BU5D_t1172311765* value)
	{
		___FloorPos_19 = value;
		Il2CppCodeGenWriteBarrier(&___FloorPos_19, value);
	}

	inline static int32_t get_offset_of_LineIndex_20() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___LineIndex_20)); }
	inline int32_t get_LineIndex_20() const { return ___LineIndex_20; }
	inline int32_t* get_address_of_LineIndex_20() { return &___LineIndex_20; }
	inline void set_LineIndex_20(int32_t value)
	{
		___LineIndex_20 = value;
	}

	inline static int32_t get_offset_of_FloorIndex_21() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___FloorIndex_21)); }
	inline int32_t get_FloorIndex_21() const { return ___FloorIndex_21; }
	inline int32_t* get_address_of_FloorIndex_21() { return &___FloorIndex_21; }
	inline void set_FloorIndex_21(int32_t value)
	{
		___FloorIndex_21 = value;
	}

	inline static int32_t get_offset_of_m_bSceneMoved_22() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___m_bSceneMoved_22)); }
	inline bool get_m_bSceneMoved_22() const { return ___m_bSceneMoved_22; }
	inline bool* get_address_of_m_bSceneMoved_22() { return &___m_bSceneMoved_22; }
	inline void set_m_bSceneMoved_22(bool value)
	{
		___m_bSceneMoved_22 = value;
	}

	inline static int32_t get_offset_of_isChangingMypage_23() { return static_cast<int32_t>(offsetof(MyPageManager_t2103198340, ___isChangingMypage_23)); }
	inline bool get_isChangingMypage_23() const { return ___isChangingMypage_23; }
	inline bool* get_address_of_isChangingMypage_23() { return &___isChangingMypage_23; }
	inline void set_isChangingMypage_23(bool value)
	{
		___isChangingMypage_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
