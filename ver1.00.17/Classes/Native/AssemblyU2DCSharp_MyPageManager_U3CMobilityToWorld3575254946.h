﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageManager/<MobilityToWorld>c__Iterator0
struct  U3CMobilityToWorldU3Ec__Iterator0_t3575254946  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 MyPageManager/<MobilityToWorld>c__Iterator0::<vPlanetPos>__0
	Vector3_t2243707580  ___U3CvPlanetPosU3E__0_0;
	// UnityEngine.Transform MyPageManager/<MobilityToWorld>c__Iterator0::<mainCameraTrans>__0
	Transform_t3275118058 * ___U3CmainCameraTransU3E__0_1;
	// System.Object MyPageManager/<MobilityToWorld>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean MyPageManager/<MobilityToWorld>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MyPageManager/<MobilityToWorld>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CvPlanetPosU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMobilityToWorldU3Ec__Iterator0_t3575254946, ___U3CvPlanetPosU3E__0_0)); }
	inline Vector3_t2243707580  get_U3CvPlanetPosU3E__0_0() const { return ___U3CvPlanetPosU3E__0_0; }
	inline Vector3_t2243707580 * get_address_of_U3CvPlanetPosU3E__0_0() { return &___U3CvPlanetPosU3E__0_0; }
	inline void set_U3CvPlanetPosU3E__0_0(Vector3_t2243707580  value)
	{
		___U3CvPlanetPosU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraTransU3E__0_1() { return static_cast<int32_t>(offsetof(U3CMobilityToWorldU3Ec__Iterator0_t3575254946, ___U3CmainCameraTransU3E__0_1)); }
	inline Transform_t3275118058 * get_U3CmainCameraTransU3E__0_1() const { return ___U3CmainCameraTransU3E__0_1; }
	inline Transform_t3275118058 ** get_address_of_U3CmainCameraTransU3E__0_1() { return &___U3CmainCameraTransU3E__0_1; }
	inline void set_U3CmainCameraTransU3E__0_1(Transform_t3275118058 * value)
	{
		___U3CmainCameraTransU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmainCameraTransU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CMobilityToWorldU3Ec__Iterator0_t3575254946, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CMobilityToWorldU3Ec__Iterator0_t3575254946, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CMobilityToWorldU3Ec__Iterator0_t3575254946, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
