﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// MyPageManager
struct MyPageManager_t2103198340;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageManager/<_LoopNextFloor>c__AnonStorey1
struct  U3C_LoopNextFloorU3Ec__AnonStorey1_t2772912709  : public Il2CppObject
{
public:
	// System.Int32 MyPageManager/<_LoopNextFloor>c__AnonStorey1::j
	int32_t ___j_0;
	// MyPageManager MyPageManager/<_LoopNextFloor>c__AnonStorey1::$this
	MyPageManager_t2103198340 * ___U24this_1;

public:
	inline static int32_t get_offset_of_j_0() { return static_cast<int32_t>(offsetof(U3C_LoopNextFloorU3Ec__AnonStorey1_t2772912709, ___j_0)); }
	inline int32_t get_j_0() const { return ___j_0; }
	inline int32_t* get_address_of_j_0() { return &___j_0; }
	inline void set_j_0(int32_t value)
	{
		___j_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3C_LoopNextFloorU3Ec__AnonStorey1_t2772912709, ___U24this_1)); }
	inline MyPageManager_t2103198340 * get_U24this_1() const { return ___U24this_1; }
	inline MyPageManager_t2103198340 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MyPageManager_t2103198340 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
