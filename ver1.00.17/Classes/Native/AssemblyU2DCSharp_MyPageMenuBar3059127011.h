﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// MypageMenuItem[]
struct MypageMenuItemU5BU5D_t2027874934;
// UnityEngine.UI.Text
struct Text_t356221433;
// MyPageRank
struct MyPageRank_t2203892935;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageMenuBar
struct  MyPageMenuBar_t3059127011  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button MyPageMenuBar::CreateButton
	Button_t2872111280 * ___CreateButton_2;
	// MypageMenuItem[] MyPageMenuBar::MenuItems
	MypageMenuItemU5BU5D_t2027874934* ___MenuItems_3;
	// UnityEngine.UI.Text MyPageMenuBar::UserName
	Text_t356221433 * ___UserName_4;
	// MyPageRank MyPageMenuBar::Rank
	MyPageRank_t2203892935 * ___Rank_5;

public:
	inline static int32_t get_offset_of_CreateButton_2() { return static_cast<int32_t>(offsetof(MyPageMenuBar_t3059127011, ___CreateButton_2)); }
	inline Button_t2872111280 * get_CreateButton_2() const { return ___CreateButton_2; }
	inline Button_t2872111280 ** get_address_of_CreateButton_2() { return &___CreateButton_2; }
	inline void set_CreateButton_2(Button_t2872111280 * value)
	{
		___CreateButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CreateButton_2, value);
	}

	inline static int32_t get_offset_of_MenuItems_3() { return static_cast<int32_t>(offsetof(MyPageMenuBar_t3059127011, ___MenuItems_3)); }
	inline MypageMenuItemU5BU5D_t2027874934* get_MenuItems_3() const { return ___MenuItems_3; }
	inline MypageMenuItemU5BU5D_t2027874934** get_address_of_MenuItems_3() { return &___MenuItems_3; }
	inline void set_MenuItems_3(MypageMenuItemU5BU5D_t2027874934* value)
	{
		___MenuItems_3 = value;
		Il2CppCodeGenWriteBarrier(&___MenuItems_3, value);
	}

	inline static int32_t get_offset_of_UserName_4() { return static_cast<int32_t>(offsetof(MyPageMenuBar_t3059127011, ___UserName_4)); }
	inline Text_t356221433 * get_UserName_4() const { return ___UserName_4; }
	inline Text_t356221433 ** get_address_of_UserName_4() { return &___UserName_4; }
	inline void set_UserName_4(Text_t356221433 * value)
	{
		___UserName_4 = value;
		Il2CppCodeGenWriteBarrier(&___UserName_4, value);
	}

	inline static int32_t get_offset_of_Rank_5() { return static_cast<int32_t>(offsetof(MyPageMenuBar_t3059127011, ___Rank_5)); }
	inline MyPageRank_t2203892935 * get_Rank_5() const { return ___Rank_5; }
	inline MyPageRank_t2203892935 ** get_address_of_Rank_5() { return &___Rank_5; }
	inline void set_Rank_5(MyPageRank_t2203892935 * value)
	{
		___Rank_5 = value;
		Il2CppCodeGenWriteBarrier(&___Rank_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
