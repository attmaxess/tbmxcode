﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<ReactionList>
struct List_1_t423989893;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageMobilmoItem
struct  MyPageMobilmoItem_t346175087  : public Il2CppObject
{
public:
	// System.String MyPageMobilmoItem::Name
	String_t* ___Name_0;
	// System.String MyPageMobilmoItem::Description
	String_t* ___Description_1;
	// UnityEngine.GameObject MyPageMobilmoItem::Obj
	GameObject_t1756533147 * ___Obj_2;
	// System.Int32 MyPageMobilmoItem::Id
	int32_t ___Id_3;
	// System.Int32 MyPageMobilmoItem::SelectNo
	int32_t ___SelectNo_4;
	// System.Int32 MyPageMobilmoItem::ReactionAll
	int32_t ___ReactionAll_5;
	// System.Int32 MyPageMobilmoItem::Prized
	int32_t ___Prized_6;
	// System.Int32 MyPageMobilmoItem::UsedPartsCount
	int32_t ___UsedPartsCount_7;
	// System.Collections.Generic.List`1<ReactionList> MyPageMobilmoItem::ReactionList
	List_1_t423989893 * ___ReactionList_8;
	// System.Collections.Generic.List`1<System.Int32> MyPageMobilmoItem::PrizedMasterIdList
	List_1_t1440998580 * ___PrizedMasterIdList_9;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier(&___Name_0, value);
	}

	inline static int32_t get_offset_of_Description_1() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___Description_1)); }
	inline String_t* get_Description_1() const { return ___Description_1; }
	inline String_t** get_address_of_Description_1() { return &___Description_1; }
	inline void set_Description_1(String_t* value)
	{
		___Description_1 = value;
		Il2CppCodeGenWriteBarrier(&___Description_1, value);
	}

	inline static int32_t get_offset_of_Obj_2() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___Obj_2)); }
	inline GameObject_t1756533147 * get_Obj_2() const { return ___Obj_2; }
	inline GameObject_t1756533147 ** get_address_of_Obj_2() { return &___Obj_2; }
	inline void set_Obj_2(GameObject_t1756533147 * value)
	{
		___Obj_2 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_2, value);
	}

	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___Id_3)); }
	inline int32_t get_Id_3() const { return ___Id_3; }
	inline int32_t* get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(int32_t value)
	{
		___Id_3 = value;
	}

	inline static int32_t get_offset_of_SelectNo_4() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___SelectNo_4)); }
	inline int32_t get_SelectNo_4() const { return ___SelectNo_4; }
	inline int32_t* get_address_of_SelectNo_4() { return &___SelectNo_4; }
	inline void set_SelectNo_4(int32_t value)
	{
		___SelectNo_4 = value;
	}

	inline static int32_t get_offset_of_ReactionAll_5() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___ReactionAll_5)); }
	inline int32_t get_ReactionAll_5() const { return ___ReactionAll_5; }
	inline int32_t* get_address_of_ReactionAll_5() { return &___ReactionAll_5; }
	inline void set_ReactionAll_5(int32_t value)
	{
		___ReactionAll_5 = value;
	}

	inline static int32_t get_offset_of_Prized_6() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___Prized_6)); }
	inline int32_t get_Prized_6() const { return ___Prized_6; }
	inline int32_t* get_address_of_Prized_6() { return &___Prized_6; }
	inline void set_Prized_6(int32_t value)
	{
		___Prized_6 = value;
	}

	inline static int32_t get_offset_of_UsedPartsCount_7() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___UsedPartsCount_7)); }
	inline int32_t get_UsedPartsCount_7() const { return ___UsedPartsCount_7; }
	inline int32_t* get_address_of_UsedPartsCount_7() { return &___UsedPartsCount_7; }
	inline void set_UsedPartsCount_7(int32_t value)
	{
		___UsedPartsCount_7 = value;
	}

	inline static int32_t get_offset_of_ReactionList_8() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___ReactionList_8)); }
	inline List_1_t423989893 * get_ReactionList_8() const { return ___ReactionList_8; }
	inline List_1_t423989893 ** get_address_of_ReactionList_8() { return &___ReactionList_8; }
	inline void set_ReactionList_8(List_1_t423989893 * value)
	{
		___ReactionList_8 = value;
		Il2CppCodeGenWriteBarrier(&___ReactionList_8, value);
	}

	inline static int32_t get_offset_of_PrizedMasterIdList_9() { return static_cast<int32_t>(offsetof(MyPageMobilmoItem_t346175087, ___PrizedMasterIdList_9)); }
	inline List_1_t1440998580 * get_PrizedMasterIdList_9() const { return ___PrizedMasterIdList_9; }
	inline List_1_t1440998580 ** get_address_of_PrizedMasterIdList_9() { return &___PrizedMasterIdList_9; }
	inline void set_PrizedMasterIdList_9(List_1_t1440998580 * value)
	{
		___PrizedMasterIdList_9 = value;
		Il2CppCodeGenWriteBarrier(&___PrizedMasterIdList_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
