﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPageRank
struct  MyPageRank_t2203892935  : public Il2CppObject
{
public:
	// UnityEngine.UI.Image MyPageRank::Guage
	Image_t2042527209 * ___Guage_0;
	// UnityEngine.UI.Text MyPageRank::Point
	Text_t356221433 * ___Point_1;

public:
	inline static int32_t get_offset_of_Guage_0() { return static_cast<int32_t>(offsetof(MyPageRank_t2203892935, ___Guage_0)); }
	inline Image_t2042527209 * get_Guage_0() const { return ___Guage_0; }
	inline Image_t2042527209 ** get_address_of_Guage_0() { return &___Guage_0; }
	inline void set_Guage_0(Image_t2042527209 * value)
	{
		___Guage_0 = value;
		Il2CppCodeGenWriteBarrier(&___Guage_0, value);
	}

	inline static int32_t get_offset_of_Point_1() { return static_cast<int32_t>(offsetof(MyPageRank_t2203892935, ___Point_1)); }
	inline Text_t356221433 * get_Point_1() const { return ___Point_1; }
	inline Text_t356221433 ** get_address_of_Point_1() { return &___Point_1; }
	inline void set_Point_1(Text_t356221433 * value)
	{
		___Point_1 = value;
		Il2CppCodeGenWriteBarrier(&___Point_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
