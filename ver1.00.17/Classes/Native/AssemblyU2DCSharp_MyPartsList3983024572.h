﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// System.Collections.Generic.List`1<PremitivePartContent>
struct List_1_t3065337161;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPartsList
struct  MyPartsList_t3983024572  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MyPartsList::PremitiveContentSample
	GameObject_t1756533147 * ___PremitiveContentSample_2;
	// UnityEngine.Transform[] MyPartsList::PartsSelectList
	TransformU5BU5D_t3764228911* ___PartsSelectList_3;
	// System.Collections.Generic.List`1<PremitivePartContent> MyPartsList::m_PartsList
	List_1_t3065337161 * ___m_PartsList_4;

public:
	inline static int32_t get_offset_of_PremitiveContentSample_2() { return static_cast<int32_t>(offsetof(MyPartsList_t3983024572, ___PremitiveContentSample_2)); }
	inline GameObject_t1756533147 * get_PremitiveContentSample_2() const { return ___PremitiveContentSample_2; }
	inline GameObject_t1756533147 ** get_address_of_PremitiveContentSample_2() { return &___PremitiveContentSample_2; }
	inline void set_PremitiveContentSample_2(GameObject_t1756533147 * value)
	{
		___PremitiveContentSample_2 = value;
		Il2CppCodeGenWriteBarrier(&___PremitiveContentSample_2, value);
	}

	inline static int32_t get_offset_of_PartsSelectList_3() { return static_cast<int32_t>(offsetof(MyPartsList_t3983024572, ___PartsSelectList_3)); }
	inline TransformU5BU5D_t3764228911* get_PartsSelectList_3() const { return ___PartsSelectList_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_PartsSelectList_3() { return &___PartsSelectList_3; }
	inline void set_PartsSelectList_3(TransformU5BU5D_t3764228911* value)
	{
		___PartsSelectList_3 = value;
		Il2CppCodeGenWriteBarrier(&___PartsSelectList_3, value);
	}

	inline static int32_t get_offset_of_m_PartsList_4() { return static_cast<int32_t>(offsetof(MyPartsList_t3983024572, ___m_PartsList_4)); }
	inline List_1_t3065337161 * get_m_PartsList_4() const { return ___m_PartsList_4; }
	inline List_1_t3065337161 ** get_address_of_m_PartsList_4() { return &___m_PartsList_4; }
	inline void set_m_PartsList_4(List_1_t3065337161 * value)
	{
		___m_PartsList_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_PartsList_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
