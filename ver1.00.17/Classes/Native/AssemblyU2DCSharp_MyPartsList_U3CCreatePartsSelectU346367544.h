﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MyPartsList
struct MyPartsList_t3983024572;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyPartsList/<CreatePartsSelect>c__Iterator0
struct  U3CCreatePartsSelectU3Ec__Iterator0_t46367544  : public Il2CppObject
{
public:
	// System.Int32 MyPartsList/<CreatePartsSelect>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// System.String MyPartsList/<CreatePartsSelect>c__Iterator0::<_partsName>__2
	String_t* ___U3C_partsNameU3E__2_1;
	// UnityEngine.GameObject MyPartsList/<CreatePartsSelect>c__Iterator0::<obj>__2
	GameObject_t1756533147 * ___U3CobjU3E__2_2;
	// MyPartsList MyPartsList/<CreatePartsSelect>c__Iterator0::$this
	MyPartsList_t3983024572 * ___U24this_3;
	// System.Object MyPartsList/<CreatePartsSelect>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean MyPartsList/<CreatePartsSelect>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 MyPartsList/<CreatePartsSelect>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t46367544, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3C_partsNameU3E__2_1() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t46367544, ___U3C_partsNameU3E__2_1)); }
	inline String_t* get_U3C_partsNameU3E__2_1() const { return ___U3C_partsNameU3E__2_1; }
	inline String_t** get_address_of_U3C_partsNameU3E__2_1() { return &___U3C_partsNameU3E__2_1; }
	inline void set_U3C_partsNameU3E__2_1(String_t* value)
	{
		___U3C_partsNameU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_partsNameU3E__2_1, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_2() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t46367544, ___U3CobjU3E__2_2)); }
	inline GameObject_t1756533147 * get_U3CobjU3E__2_2() const { return ___U3CobjU3E__2_2; }
	inline GameObject_t1756533147 ** get_address_of_U3CobjU3E__2_2() { return &___U3CobjU3E__2_2; }
	inline void set_U3CobjU3E__2_2(GameObject_t1756533147 * value)
	{
		___U3CobjU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t46367544, ___U24this_3)); }
	inline MyPartsList_t3983024572 * get_U24this_3() const { return ___U24this_3; }
	inline MyPartsList_t3983024572 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MyPartsList_t3983024572 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t46367544, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t46367544, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t46367544, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
