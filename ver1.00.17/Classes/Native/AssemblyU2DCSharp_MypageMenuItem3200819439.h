﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MypageMenuItem
struct  MypageMenuItem_t3200819439  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image[] MypageMenuItem::Icons
	ImageU5BU5D_t590162004* ___Icons_2;

public:
	inline static int32_t get_offset_of_Icons_2() { return static_cast<int32_t>(offsetof(MypageMenuItem_t3200819439, ___Icons_2)); }
	inline ImageU5BU5D_t590162004* get_Icons_2() const { return ___Icons_2; }
	inline ImageU5BU5D_t590162004** get_address_of_Icons_2() { return &___Icons_2; }
	inline void set_Icons_2(ImageU5BU5D_t590162004* value)
	{
		___Icons_2 = value;
		Il2CppCodeGenWriteBarrier(&___Icons_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
