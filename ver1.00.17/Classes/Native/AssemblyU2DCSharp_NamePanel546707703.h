﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// NamePanelItem[]
struct NamePanelItemU5BU5D_t2974319725;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NamePanel
struct  NamePanel_t546707703  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject NamePanel::YourName
	GameObject_t1756533147 * ___YourName_2;
	// UnityEngine.GameObject NamePanel::Input
	GameObject_t1756533147 * ___Input_3;
	// UnityEngine.GameObject NamePanel::Form
	GameObject_t1756533147 * ___Form_4;
	// UnityEngine.GameObject NamePanel::Buttons
	GameObject_t1756533147 * ___Buttons_5;
	// NamePanelItem[] NamePanel::Items
	NamePanelItemU5BU5D_t2974319725* ___Items_6;
	// System.Boolean NamePanel::Hide
	bool ___Hide_7;

public:
	inline static int32_t get_offset_of_YourName_2() { return static_cast<int32_t>(offsetof(NamePanel_t546707703, ___YourName_2)); }
	inline GameObject_t1756533147 * get_YourName_2() const { return ___YourName_2; }
	inline GameObject_t1756533147 ** get_address_of_YourName_2() { return &___YourName_2; }
	inline void set_YourName_2(GameObject_t1756533147 * value)
	{
		___YourName_2 = value;
		Il2CppCodeGenWriteBarrier(&___YourName_2, value);
	}

	inline static int32_t get_offset_of_Input_3() { return static_cast<int32_t>(offsetof(NamePanel_t546707703, ___Input_3)); }
	inline GameObject_t1756533147 * get_Input_3() const { return ___Input_3; }
	inline GameObject_t1756533147 ** get_address_of_Input_3() { return &___Input_3; }
	inline void set_Input_3(GameObject_t1756533147 * value)
	{
		___Input_3 = value;
		Il2CppCodeGenWriteBarrier(&___Input_3, value);
	}

	inline static int32_t get_offset_of_Form_4() { return static_cast<int32_t>(offsetof(NamePanel_t546707703, ___Form_4)); }
	inline GameObject_t1756533147 * get_Form_4() const { return ___Form_4; }
	inline GameObject_t1756533147 ** get_address_of_Form_4() { return &___Form_4; }
	inline void set_Form_4(GameObject_t1756533147 * value)
	{
		___Form_4 = value;
		Il2CppCodeGenWriteBarrier(&___Form_4, value);
	}

	inline static int32_t get_offset_of_Buttons_5() { return static_cast<int32_t>(offsetof(NamePanel_t546707703, ___Buttons_5)); }
	inline GameObject_t1756533147 * get_Buttons_5() const { return ___Buttons_5; }
	inline GameObject_t1756533147 ** get_address_of_Buttons_5() { return &___Buttons_5; }
	inline void set_Buttons_5(GameObject_t1756533147 * value)
	{
		___Buttons_5 = value;
		Il2CppCodeGenWriteBarrier(&___Buttons_5, value);
	}

	inline static int32_t get_offset_of_Items_6() { return static_cast<int32_t>(offsetof(NamePanel_t546707703, ___Items_6)); }
	inline NamePanelItemU5BU5D_t2974319725* get_Items_6() const { return ___Items_6; }
	inline NamePanelItemU5BU5D_t2974319725** get_address_of_Items_6() { return &___Items_6; }
	inline void set_Items_6(NamePanelItemU5BU5D_t2974319725* value)
	{
		___Items_6 = value;
		Il2CppCodeGenWriteBarrier(&___Items_6, value);
	}

	inline static int32_t get_offset_of_Hide_7() { return static_cast<int32_t>(offsetof(NamePanel_t546707703, ___Hide_7)); }
	inline bool get_Hide_7() const { return ___Hide_7; }
	inline bool* get_address_of_Hide_7() { return &___Hide_7; }
	inline void set_Hide_7(bool value)
	{
		___Hide_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
