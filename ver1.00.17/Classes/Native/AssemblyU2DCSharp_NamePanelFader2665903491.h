﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NamePanelFader
struct  NamePanelFader_t2665903491  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.CanvasGroup NamePanelFader::m_CanvasGroup
	CanvasGroup_t3296560743 * ___m_CanvasGroup_2;

public:
	inline static int32_t get_offset_of_m_CanvasGroup_2() { return static_cast<int32_t>(offsetof(NamePanelFader_t2665903491, ___m_CanvasGroup_2)); }
	inline CanvasGroup_t3296560743 * get_m_CanvasGroup_2() const { return ___m_CanvasGroup_2; }
	inline CanvasGroup_t3296560743 ** get_address_of_m_CanvasGroup_2() { return &___m_CanvasGroup_2; }
	inline void set_m_CanvasGroup_2(CanvasGroup_t3296560743 * value)
	{
		___m_CanvasGroup_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_CanvasGroup_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
