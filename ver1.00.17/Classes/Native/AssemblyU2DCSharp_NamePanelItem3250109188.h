﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NamePanelItem
struct  NamePanelItem_t3250109188  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform NamePanelItem::RectTrans
	RectTransform_t3349966182 * ___RectTrans_2;
	// UnityEngine.CanvasGroup NamePanelItem::CanvasGroup
	CanvasGroup_t3296560743 * ___CanvasGroup_3;

public:
	inline static int32_t get_offset_of_RectTrans_2() { return static_cast<int32_t>(offsetof(NamePanelItem_t3250109188, ___RectTrans_2)); }
	inline RectTransform_t3349966182 * get_RectTrans_2() const { return ___RectTrans_2; }
	inline RectTransform_t3349966182 ** get_address_of_RectTrans_2() { return &___RectTrans_2; }
	inline void set_RectTrans_2(RectTransform_t3349966182 * value)
	{
		___RectTrans_2 = value;
		Il2CppCodeGenWriteBarrier(&___RectTrans_2, value);
	}

	inline static int32_t get_offset_of_CanvasGroup_3() { return static_cast<int32_t>(offsetof(NamePanelItem_t3250109188, ___CanvasGroup_3)); }
	inline CanvasGroup_t3296560743 * get_CanvasGroup_3() const { return ___CanvasGroup_3; }
	inline CanvasGroup_t3296560743 ** get_address_of_CanvasGroup_3() { return &___CanvasGroup_3; }
	inline void set_CanvasGroup_3(CanvasGroup_t3296560743 * value)
	{
		___CanvasGroup_3 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasGroup_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
