﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UserNameCreator
struct UserNameCreator_t3044989708;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NameRegister
struct  NameRegister_t1336585606  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean NameRegister::APIActive
	bool ___APIActive_2;
	// UserNameCreator NameRegister::UserNameCreator
	UserNameCreator_t3044989708 * ___UserNameCreator_3;

public:
	inline static int32_t get_offset_of_APIActive_2() { return static_cast<int32_t>(offsetof(NameRegister_t1336585606, ___APIActive_2)); }
	inline bool get_APIActive_2() const { return ___APIActive_2; }
	inline bool* get_address_of_APIActive_2() { return &___APIActive_2; }
	inline void set_APIActive_2(bool value)
	{
		___APIActive_2 = value;
	}

	inline static int32_t get_offset_of_UserNameCreator_3() { return static_cast<int32_t>(offsetof(NameRegister_t1336585606, ___UserNameCreator_3)); }
	inline UserNameCreator_t3044989708 * get_UserNameCreator_3() const { return ___UserNameCreator_3; }
	inline UserNameCreator_t3044989708 ** get_address_of_UserNameCreator_3() { return &___UserNameCreator_3; }
	inline void set_UserNameCreator_3(UserNameCreator_t3044989708 * value)
	{
		___UserNameCreator_3 = value;
		Il2CppCodeGenWriteBarrier(&___UserNameCreator_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
