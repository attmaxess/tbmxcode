﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<GetNearmobRecordedMotionListMotions>
struct List_1_t3636084845;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<GetNearmobItemsReactionItemsList>
struct List_1_t4250470571;
// System.Collections.Generic.List`1<GetNearmobEncountList>
struct List_1_t1887225754;
// System.Collections.Generic.List`1<GetNearmobRegisterdMotionList>
struct List_1_t2826338337;
// System.Collections.Generic.List`1<GetNearmobRecordedMotionList>
struct List_1_t3728958674;
// System.Collections.Generic.List`1<GetNearmobChildPartsListModel>
struct List_1_t3032003919;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NearmobItemsModel
struct  NearmobItemsModel_t1641537963  : public Model_t873752437
{
public:
	// System.Int32 NearmobItemsModel::<nearmob_items_Mobility_Id>k__BackingField
	int32_t ___U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0;
	// System.String NearmobItemsModel::<nearmob_items_date>k__BackingField
	String_t* ___U3Cnearmob_items_dateU3Ek__BackingField_1;
	// System.String NearmobItemsModel::<nearmob_items_name>k__BackingField
	String_t* ___U3Cnearmob_items_nameU3Ek__BackingField_2;
	// System.String NearmobItemsModel::<nearmob_items_situation>k__BackingField
	String_t* ___U3Cnearmob_items_situationU3Ek__BackingField_3;
	// System.String NearmobItemsModel::<nearmob_items_description>k__BackingField
	String_t* ___U3Cnearmob_items_descriptionU3Ek__BackingField_4;
	// System.Int32 NearmobItemsModel::<nearmob_items_parentId>k__BackingField
	int32_t ___U3Cnearmob_items_parentIdU3Ek__BackingField_5;
	// System.Int32 NearmobItemsModel::<nearmob_items_corePartsId>k__BackingField
	int32_t ___U3Cnearmob_items_corePartsIdU3Ek__BackingField_6;
	// System.String NearmobItemsModel::<nearmob_items_corePartsName>k__BackingField
	String_t* ___U3Cnearmob_items_corePartsNameU3Ek__BackingField_7;
	// System.Int32 NearmobItemsModel::<nearmob_items_createdUser_Id>k__BackingField
	int32_t ___U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8;
	// System.String NearmobItemsModel::<nearmob_items_createdUserName>k__BackingField
	String_t* ___U3Cnearmob_items_createdUserNameU3Ek__BackingField_9;
	// System.Int32 NearmobItemsModel::<nearmob_items_coreParts_Color>k__BackingField
	int32_t ___U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10;
	// System.Single NearmobItemsModel::<nearmob_items_coreParts_Scale_X>k__BackingField
	float ___U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11;
	// System.Single NearmobItemsModel::<nearmob_items_coreParts_Scale_Y>k__BackingField
	float ___U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12;
	// System.Single NearmobItemsModel::<nearmob_items_coreParts_Scale_Z>k__BackingField
	float ___U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13;
	// System.Int32 NearmobItemsModel::<nearmob_reaction_allcount>k__BackingField
	int32_t ___U3Cnearmob_reaction_allcountU3Ek__BackingField_14;
	// System.Int32 NearmobItemsModel::<nearmob_recordedMotionList_recorded_Id>k__BackingField
	int32_t ___U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15;
	// System.String NearmobItemsModel::<nearmob_recordedMotionList_recorded_Data>k__BackingField
	String_t* ___U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16;
	// System.Single NearmobItemsModel::<nearmob_recordedMotionList_startPoint_X>k__BackingField
	float ___U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17;
	// System.Single NearmobItemsModel::<nearmob_recordedMotionList_startPoint_Y>k__BackingField
	float ___U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18;
	// System.Single NearmobItemsModel::<nearmob_recordedMotionList_startPoint_Z>k__BackingField
	float ___U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19;
	// System.Single NearmobItemsModel::<nearmob_recordedMotionList_startRotation_X>k__BackingField
	float ___U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20;
	// System.Single NearmobItemsModel::<nearmob_recordedMotionList_startRotation_Y>k__BackingField
	float ___U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21;
	// System.Single NearmobItemsModel::<nearmob_recordedMotionList_startRotation_Z>k__BackingField
	float ___U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22;
	// System.Collections.Generic.List`1<GetNearmobRecordedMotionListMotions> NearmobItemsModel::<_nearmob_recordedMotionList_motionsData>k__BackingField
	List_1_t3636084845 * ___U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23;
	// System.Collections.Generic.List`1<System.Object> NearmobItemsModel::nearmob_prized_list
	List_1_t2058570427 * ___nearmob_prized_list_24;
	// System.Collections.Generic.List`1<GetNearmobItemsReactionItemsList> NearmobItemsModel::<_nearmob_reaction_items_list>k__BackingField
	List_1_t4250470571 * ___U3C_nearmob_reaction_items_listU3Ek__BackingField_25;
	// System.Collections.Generic.List`1<GetNearmobEncountList> NearmobItemsModel::<_nearmob_encount_list>k__BackingField
	List_1_t1887225754 * ___U3C_nearmob_encount_listU3Ek__BackingField_26;
	// System.Collections.Generic.List`1<GetNearmobRegisterdMotionList> NearmobItemsModel::<_nearmob_registerMotion_list>k__BackingField
	List_1_t2826338337 * ___U3C_nearmob_registerMotion_listU3Ek__BackingField_27;
	// System.Collections.Generic.List`1<GetNearmobRecordedMotionList> NearmobItemsModel::<_nearmob_recordedMotion>k__BackingField
	List_1_t3728958674 * ___U3C_nearmob_recordedMotionU3Ek__BackingField_28;
	// System.Collections.Generic.List`1<GetNearmobChildPartsListModel> NearmobItemsModel::<_nearmob_childParts_list>k__BackingField
	List_1_t3032003919 * ___U3C_nearmob_childParts_listU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0)); }
	inline int32_t get_U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0() const { return ___U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0() { return &___U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0; }
	inline void set_U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0(int32_t value)
	{
		___U3Cnearmob_items_Mobility_IdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_dateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_dateU3Ek__BackingField_1)); }
	inline String_t* get_U3Cnearmob_items_dateU3Ek__BackingField_1() const { return ___U3Cnearmob_items_dateU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cnearmob_items_dateU3Ek__BackingField_1() { return &___U3Cnearmob_items_dateU3Ek__BackingField_1; }
	inline void set_U3Cnearmob_items_dateU3Ek__BackingField_1(String_t* value)
	{
		___U3Cnearmob_items_dateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_items_dateU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_nameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_nameU3Ek__BackingField_2)); }
	inline String_t* get_U3Cnearmob_items_nameU3Ek__BackingField_2() const { return ___U3Cnearmob_items_nameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cnearmob_items_nameU3Ek__BackingField_2() { return &___U3Cnearmob_items_nameU3Ek__BackingField_2; }
	inline void set_U3Cnearmob_items_nameU3Ek__BackingField_2(String_t* value)
	{
		___U3Cnearmob_items_nameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_items_nameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_situationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_situationU3Ek__BackingField_3)); }
	inline String_t* get_U3Cnearmob_items_situationU3Ek__BackingField_3() const { return ___U3Cnearmob_items_situationU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cnearmob_items_situationU3Ek__BackingField_3() { return &___U3Cnearmob_items_situationU3Ek__BackingField_3; }
	inline void set_U3Cnearmob_items_situationU3Ek__BackingField_3(String_t* value)
	{
		___U3Cnearmob_items_situationU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_items_situationU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_descriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_descriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3Cnearmob_items_descriptionU3Ek__BackingField_4() const { return ___U3Cnearmob_items_descriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cnearmob_items_descriptionU3Ek__BackingField_4() { return &___U3Cnearmob_items_descriptionU3Ek__BackingField_4; }
	inline void set_U3Cnearmob_items_descriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3Cnearmob_items_descriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_items_descriptionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_parentIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_parentIdU3Ek__BackingField_5)); }
	inline int32_t get_U3Cnearmob_items_parentIdU3Ek__BackingField_5() const { return ___U3Cnearmob_items_parentIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3Cnearmob_items_parentIdU3Ek__BackingField_5() { return &___U3Cnearmob_items_parentIdU3Ek__BackingField_5; }
	inline void set_U3Cnearmob_items_parentIdU3Ek__BackingField_5(int32_t value)
	{
		___U3Cnearmob_items_parentIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_corePartsIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_corePartsIdU3Ek__BackingField_6)); }
	inline int32_t get_U3Cnearmob_items_corePartsIdU3Ek__BackingField_6() const { return ___U3Cnearmob_items_corePartsIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3Cnearmob_items_corePartsIdU3Ek__BackingField_6() { return &___U3Cnearmob_items_corePartsIdU3Ek__BackingField_6; }
	inline void set_U3Cnearmob_items_corePartsIdU3Ek__BackingField_6(int32_t value)
	{
		___U3Cnearmob_items_corePartsIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_corePartsNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_corePartsNameU3Ek__BackingField_7)); }
	inline String_t* get_U3Cnearmob_items_corePartsNameU3Ek__BackingField_7() const { return ___U3Cnearmob_items_corePartsNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3Cnearmob_items_corePartsNameU3Ek__BackingField_7() { return &___U3Cnearmob_items_corePartsNameU3Ek__BackingField_7; }
	inline void set_U3Cnearmob_items_corePartsNameU3Ek__BackingField_7(String_t* value)
	{
		___U3Cnearmob_items_corePartsNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_items_corePartsNameU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8)); }
	inline int32_t get_U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8() const { return ___U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8() { return &___U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8; }
	inline void set_U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8(int32_t value)
	{
		___U3Cnearmob_items_createdUser_IdU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_createdUserNameU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_createdUserNameU3Ek__BackingField_9)); }
	inline String_t* get_U3Cnearmob_items_createdUserNameU3Ek__BackingField_9() const { return ___U3Cnearmob_items_createdUserNameU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3Cnearmob_items_createdUserNameU3Ek__BackingField_9() { return &___U3Cnearmob_items_createdUserNameU3Ek__BackingField_9; }
	inline void set_U3Cnearmob_items_createdUserNameU3Ek__BackingField_9(String_t* value)
	{
		___U3Cnearmob_items_createdUserNameU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_items_createdUserNameU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10)); }
	inline int32_t get_U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10() const { return ___U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10() { return &___U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10; }
	inline void set_U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10(int32_t value)
	{
		___U3Cnearmob_items_coreParts_ColorU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11)); }
	inline float get_U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11() const { return ___U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11; }
	inline float* get_address_of_U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11() { return &___U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11; }
	inline void set_U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11(float value)
	{
		___U3Cnearmob_items_coreParts_Scale_XU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12)); }
	inline float get_U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12() const { return ___U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12; }
	inline float* get_address_of_U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12() { return &___U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12; }
	inline void set_U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12(float value)
	{
		___U3Cnearmob_items_coreParts_Scale_YU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13)); }
	inline float get_U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13() const { return ___U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13; }
	inline float* get_address_of_U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13() { return &___U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13; }
	inline void set_U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13(float value)
	{
		___U3Cnearmob_items_coreParts_Scale_ZU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_reaction_allcountU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_reaction_allcountU3Ek__BackingField_14)); }
	inline int32_t get_U3Cnearmob_reaction_allcountU3Ek__BackingField_14() const { return ___U3Cnearmob_reaction_allcountU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3Cnearmob_reaction_allcountU3Ek__BackingField_14() { return &___U3Cnearmob_reaction_allcountU3Ek__BackingField_14; }
	inline void set_U3Cnearmob_reaction_allcountU3Ek__BackingField_14(int32_t value)
	{
		___U3Cnearmob_reaction_allcountU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15)); }
	inline int32_t get_U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15() const { return ___U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15() { return &___U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15; }
	inline void set_U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15(int32_t value)
	{
		___U3Cnearmob_recordedMotionList_recorded_IdU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16)); }
	inline String_t* get_U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16() const { return ___U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16() { return &___U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16; }
	inline void set_U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16(String_t* value)
	{
		___U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cnearmob_recordedMotionList_recorded_DataU3Ek__BackingField_16, value);
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17)); }
	inline float get_U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17() const { return ___U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17; }
	inline float* get_address_of_U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17() { return &___U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17; }
	inline void set_U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17(float value)
	{
		___U3Cnearmob_recordedMotionList_startPoint_XU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18)); }
	inline float get_U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18() const { return ___U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18; }
	inline float* get_address_of_U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18() { return &___U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18; }
	inline void set_U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18(float value)
	{
		___U3Cnearmob_recordedMotionList_startPoint_YU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19)); }
	inline float get_U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19() const { return ___U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19; }
	inline float* get_address_of_U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19() { return &___U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19; }
	inline void set_U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19(float value)
	{
		___U3Cnearmob_recordedMotionList_startPoint_ZU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20)); }
	inline float get_U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20() const { return ___U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20; }
	inline float* get_address_of_U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20() { return &___U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20; }
	inline void set_U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20(float value)
	{
		___U3Cnearmob_recordedMotionList_startRotation_XU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21)); }
	inline float get_U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21() const { return ___U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21; }
	inline float* get_address_of_U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21() { return &___U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21; }
	inline void set_U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21(float value)
	{
		___U3Cnearmob_recordedMotionList_startRotation_YU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22)); }
	inline float get_U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22() const { return ___U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22; }
	inline float* get_address_of_U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22() { return &___U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22; }
	inline void set_U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22(float value)
	{
		___U3Cnearmob_recordedMotionList_startRotation_ZU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23)); }
	inline List_1_t3636084845 * get_U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23() const { return ___U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23; }
	inline List_1_t3636084845 ** get_address_of_U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23() { return &___U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23; }
	inline void set_U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23(List_1_t3636084845 * value)
	{
		___U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_nearmob_recordedMotionList_motionsDataU3Ek__BackingField_23, value);
	}

	inline static int32_t get_offset_of_nearmob_prized_list_24() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___nearmob_prized_list_24)); }
	inline List_1_t2058570427 * get_nearmob_prized_list_24() const { return ___nearmob_prized_list_24; }
	inline List_1_t2058570427 ** get_address_of_nearmob_prized_list_24() { return &___nearmob_prized_list_24; }
	inline void set_nearmob_prized_list_24(List_1_t2058570427 * value)
	{
		___nearmob_prized_list_24 = value;
		Il2CppCodeGenWriteBarrier(&___nearmob_prized_list_24, value);
	}

	inline static int32_t get_offset_of_U3C_nearmob_reaction_items_listU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3C_nearmob_reaction_items_listU3Ek__BackingField_25)); }
	inline List_1_t4250470571 * get_U3C_nearmob_reaction_items_listU3Ek__BackingField_25() const { return ___U3C_nearmob_reaction_items_listU3Ek__BackingField_25; }
	inline List_1_t4250470571 ** get_address_of_U3C_nearmob_reaction_items_listU3Ek__BackingField_25() { return &___U3C_nearmob_reaction_items_listU3Ek__BackingField_25; }
	inline void set_U3C_nearmob_reaction_items_listU3Ek__BackingField_25(List_1_t4250470571 * value)
	{
		___U3C_nearmob_reaction_items_listU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_nearmob_reaction_items_listU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_U3C_nearmob_encount_listU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3C_nearmob_encount_listU3Ek__BackingField_26)); }
	inline List_1_t1887225754 * get_U3C_nearmob_encount_listU3Ek__BackingField_26() const { return ___U3C_nearmob_encount_listU3Ek__BackingField_26; }
	inline List_1_t1887225754 ** get_address_of_U3C_nearmob_encount_listU3Ek__BackingField_26() { return &___U3C_nearmob_encount_listU3Ek__BackingField_26; }
	inline void set_U3C_nearmob_encount_listU3Ek__BackingField_26(List_1_t1887225754 * value)
	{
		___U3C_nearmob_encount_listU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_nearmob_encount_listU3Ek__BackingField_26, value);
	}

	inline static int32_t get_offset_of_U3C_nearmob_registerMotion_listU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3C_nearmob_registerMotion_listU3Ek__BackingField_27)); }
	inline List_1_t2826338337 * get_U3C_nearmob_registerMotion_listU3Ek__BackingField_27() const { return ___U3C_nearmob_registerMotion_listU3Ek__BackingField_27; }
	inline List_1_t2826338337 ** get_address_of_U3C_nearmob_registerMotion_listU3Ek__BackingField_27() { return &___U3C_nearmob_registerMotion_listU3Ek__BackingField_27; }
	inline void set_U3C_nearmob_registerMotion_listU3Ek__BackingField_27(List_1_t2826338337 * value)
	{
		___U3C_nearmob_registerMotion_listU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_nearmob_registerMotion_listU3Ek__BackingField_27, value);
	}

	inline static int32_t get_offset_of_U3C_nearmob_recordedMotionU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3C_nearmob_recordedMotionU3Ek__BackingField_28)); }
	inline List_1_t3728958674 * get_U3C_nearmob_recordedMotionU3Ek__BackingField_28() const { return ___U3C_nearmob_recordedMotionU3Ek__BackingField_28; }
	inline List_1_t3728958674 ** get_address_of_U3C_nearmob_recordedMotionU3Ek__BackingField_28() { return &___U3C_nearmob_recordedMotionU3Ek__BackingField_28; }
	inline void set_U3C_nearmob_recordedMotionU3Ek__BackingField_28(List_1_t3728958674 * value)
	{
		___U3C_nearmob_recordedMotionU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_nearmob_recordedMotionU3Ek__BackingField_28, value);
	}

	inline static int32_t get_offset_of_U3C_nearmob_childParts_listU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(NearmobItemsModel_t1641537963, ___U3C_nearmob_childParts_listU3Ek__BackingField_29)); }
	inline List_1_t3032003919 * get_U3C_nearmob_childParts_listU3Ek__BackingField_29() const { return ___U3C_nearmob_childParts_listU3Ek__BackingField_29; }
	inline List_1_t3032003919 ** get_address_of_U3C_nearmob_childParts_listU3Ek__BackingField_29() { return &___U3C_nearmob_childParts_listU3Ek__BackingField_29; }
	inline void set_U3C_nearmob_childParts_listU3Ek__BackingField_29(List_1_t3032003919 * value)
	{
		___U3C_nearmob_childParts_listU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3C_nearmob_childParts_listU3Ek__BackingField_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
