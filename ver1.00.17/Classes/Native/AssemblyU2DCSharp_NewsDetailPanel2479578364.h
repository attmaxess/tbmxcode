﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UIPanel1795085332.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewsDetailPanel
struct  NewsDetailPanel_t2479578364  : public UIPanel_t1795085332
{
public:
	// UnityEngine.UI.Text NewsDetailPanel::titleText
	Text_t356221433 * ___titleText_2;
	// UnityEngine.UI.Text NewsDetailPanel::dateText
	Text_t356221433 * ___dateText_3;
	// UnityEngine.UI.Text NewsDetailPanel::contentText
	Text_t356221433 * ___contentText_4;

public:
	inline static int32_t get_offset_of_titleText_2() { return static_cast<int32_t>(offsetof(NewsDetailPanel_t2479578364, ___titleText_2)); }
	inline Text_t356221433 * get_titleText_2() const { return ___titleText_2; }
	inline Text_t356221433 ** get_address_of_titleText_2() { return &___titleText_2; }
	inline void set_titleText_2(Text_t356221433 * value)
	{
		___titleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_2, value);
	}

	inline static int32_t get_offset_of_dateText_3() { return static_cast<int32_t>(offsetof(NewsDetailPanel_t2479578364, ___dateText_3)); }
	inline Text_t356221433 * get_dateText_3() const { return ___dateText_3; }
	inline Text_t356221433 ** get_address_of_dateText_3() { return &___dateText_3; }
	inline void set_dateText_3(Text_t356221433 * value)
	{
		___dateText_3 = value;
		Il2CppCodeGenWriteBarrier(&___dateText_3, value);
	}

	inline static int32_t get_offset_of_contentText_4() { return static_cast<int32_t>(offsetof(NewsDetailPanel_t2479578364, ___contentText_4)); }
	inline Text_t356221433 * get_contentText_4() const { return ___contentText_4; }
	inline Text_t356221433 ** get_address_of_contentText_4() { return &___contentText_4; }
	inline void set_contentText_4(Text_t356221433 * value)
	{
		___contentText_4 = value;
		Il2CppCodeGenWriteBarrier(&___contentText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
