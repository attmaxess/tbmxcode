﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// NoticeListModel
struct NoticeListModel_t1317032645;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewsItem
struct  NewsItem_t154948812  : public EnhancedScrollerCellView_t1104668249
{
public:
	// UnityEngine.UI.Text NewsItem::dateText
	Text_t356221433 * ___dateText_6;
	// UnityEngine.UI.Text NewsItem::contentText
	Text_t356221433 * ___contentText_7;
	// NoticeListModel NewsItem::_noticeModel
	NoticeListModel_t1317032645 * ____noticeModel_8;

public:
	inline static int32_t get_offset_of_dateText_6() { return static_cast<int32_t>(offsetof(NewsItem_t154948812, ___dateText_6)); }
	inline Text_t356221433 * get_dateText_6() const { return ___dateText_6; }
	inline Text_t356221433 ** get_address_of_dateText_6() { return &___dateText_6; }
	inline void set_dateText_6(Text_t356221433 * value)
	{
		___dateText_6 = value;
		Il2CppCodeGenWriteBarrier(&___dateText_6, value);
	}

	inline static int32_t get_offset_of_contentText_7() { return static_cast<int32_t>(offsetof(NewsItem_t154948812, ___contentText_7)); }
	inline Text_t356221433 * get_contentText_7() const { return ___contentText_7; }
	inline Text_t356221433 ** get_address_of_contentText_7() { return &___contentText_7; }
	inline void set_contentText_7(Text_t356221433 * value)
	{
		___contentText_7 = value;
		Il2CppCodeGenWriteBarrier(&___contentText_7, value);
	}

	inline static int32_t get_offset_of__noticeModel_8() { return static_cast<int32_t>(offsetof(NewsItem_t154948812, ____noticeModel_8)); }
	inline NoticeListModel_t1317032645 * get__noticeModel_8() const { return ____noticeModel_8; }
	inline NoticeListModel_t1317032645 ** get_address_of__noticeModel_8() { return &____noticeModel_8; }
	inline void set__noticeModel_8(NoticeListModel_t1317032645 * value)
	{
		____noticeModel_8 = value;
		Il2CppCodeGenWriteBarrier(&____noticeModel_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
