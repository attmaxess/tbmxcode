﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_UIPanel1795085332.h"

// EnhancedUI.EnhancedScroller.EnhancedScroller
struct EnhancedScroller_t2375706558;
// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView
struct EnhancedScrollerCellView_t1104668249;
// System.Collections.Generic.List`1<NoticeListModel>
struct List_1_t686153777;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewsPanel
struct  NewsPanel_t117762891  : public UIPanel_t1795085332
{
public:
	// EnhancedUI.EnhancedScroller.EnhancedScroller NewsPanel::scroller
	EnhancedScroller_t2375706558 * ___scroller_2;
	// EnhancedUI.EnhancedScroller.EnhancedScrollerCellView NewsPanel::cellViewPrefab
	EnhancedScrollerCellView_t1104668249 * ___cellViewPrefab_3;
	// System.Collections.Generic.List`1<NoticeListModel> NewsPanel::noticeDataList
	List_1_t686153777 * ___noticeDataList_4;

public:
	inline static int32_t get_offset_of_scroller_2() { return static_cast<int32_t>(offsetof(NewsPanel_t117762891, ___scroller_2)); }
	inline EnhancedScroller_t2375706558 * get_scroller_2() const { return ___scroller_2; }
	inline EnhancedScroller_t2375706558 ** get_address_of_scroller_2() { return &___scroller_2; }
	inline void set_scroller_2(EnhancedScroller_t2375706558 * value)
	{
		___scroller_2 = value;
		Il2CppCodeGenWriteBarrier(&___scroller_2, value);
	}

	inline static int32_t get_offset_of_cellViewPrefab_3() { return static_cast<int32_t>(offsetof(NewsPanel_t117762891, ___cellViewPrefab_3)); }
	inline EnhancedScrollerCellView_t1104668249 * get_cellViewPrefab_3() const { return ___cellViewPrefab_3; }
	inline EnhancedScrollerCellView_t1104668249 ** get_address_of_cellViewPrefab_3() { return &___cellViewPrefab_3; }
	inline void set_cellViewPrefab_3(EnhancedScrollerCellView_t1104668249 * value)
	{
		___cellViewPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___cellViewPrefab_3, value);
	}

	inline static int32_t get_offset_of_noticeDataList_4() { return static_cast<int32_t>(offsetof(NewsPanel_t117762891, ___noticeDataList_4)); }
	inline List_1_t686153777 * get_noticeDataList_4() const { return ___noticeDataList_4; }
	inline List_1_t686153777 ** get_address_of_noticeDataList_4() { return &___noticeDataList_4; }
	inline void set_noticeDataList_4(List_1_t686153777 * value)
	{
		___noticeDataList_4 = value;
		Il2CppCodeGenWriteBarrier(&___noticeDataList_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
