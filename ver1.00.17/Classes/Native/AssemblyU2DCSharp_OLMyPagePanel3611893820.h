﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// GetMyMobilityModel
struct GetMyMobilityModel_t2067367768;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OLMyPagePanel
struct  OLMyPagePanel_t3611893820  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text OLMyPagePanel::_allCountText
	Text_t356221433 * ____allCountText_2;
	// UnityEngine.UI.Text OLMyPagePanel::_prizedIdListText
	Text_t356221433 * ____prizedIdListText_3;
	// UnityEngine.UI.Text OLMyPagePanel::_mobilityDetailNameText
	Text_t356221433 * ____mobilityDetailNameText_4;
	// UnityEngine.UI.Text OLMyPagePanel::_mobilityDescriptionText
	Text_t356221433 * ____mobilityDescriptionText_5;
	// UnityEngine.UI.Text OLMyPagePanel::_userNameText
	Text_t356221433 * ____userNameText_6;
	// UnityEngine.UI.Text OLMyPagePanel::_userRankText
	Text_t356221433 * ____userRankText_7;
	// UnityEngine.UI.Text OLMyPagePanel::_userPointText
	Text_t356221433 * ____userPointText_8;
	// System.Int32 OLMyPagePanel::_mMobiliteIndexCurrent
	int32_t ____mMobiliteIndexCurrent_9;
	// GetMyMobilityModel OLMyPagePanel::_myMobilityModel
	GetMyMobilityModel_t2067367768 * ____myMobilityModel_10;

public:
	inline static int32_t get_offset_of__allCountText_2() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____allCountText_2)); }
	inline Text_t356221433 * get__allCountText_2() const { return ____allCountText_2; }
	inline Text_t356221433 ** get_address_of__allCountText_2() { return &____allCountText_2; }
	inline void set__allCountText_2(Text_t356221433 * value)
	{
		____allCountText_2 = value;
		Il2CppCodeGenWriteBarrier(&____allCountText_2, value);
	}

	inline static int32_t get_offset_of__prizedIdListText_3() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____prizedIdListText_3)); }
	inline Text_t356221433 * get__prizedIdListText_3() const { return ____prizedIdListText_3; }
	inline Text_t356221433 ** get_address_of__prizedIdListText_3() { return &____prizedIdListText_3; }
	inline void set__prizedIdListText_3(Text_t356221433 * value)
	{
		____prizedIdListText_3 = value;
		Il2CppCodeGenWriteBarrier(&____prizedIdListText_3, value);
	}

	inline static int32_t get_offset_of__mobilityDetailNameText_4() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____mobilityDetailNameText_4)); }
	inline Text_t356221433 * get__mobilityDetailNameText_4() const { return ____mobilityDetailNameText_4; }
	inline Text_t356221433 ** get_address_of__mobilityDetailNameText_4() { return &____mobilityDetailNameText_4; }
	inline void set__mobilityDetailNameText_4(Text_t356221433 * value)
	{
		____mobilityDetailNameText_4 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityDetailNameText_4, value);
	}

	inline static int32_t get_offset_of__mobilityDescriptionText_5() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____mobilityDescriptionText_5)); }
	inline Text_t356221433 * get__mobilityDescriptionText_5() const { return ____mobilityDescriptionText_5; }
	inline Text_t356221433 ** get_address_of__mobilityDescriptionText_5() { return &____mobilityDescriptionText_5; }
	inline void set__mobilityDescriptionText_5(Text_t356221433 * value)
	{
		____mobilityDescriptionText_5 = value;
		Il2CppCodeGenWriteBarrier(&____mobilityDescriptionText_5, value);
	}

	inline static int32_t get_offset_of__userNameText_6() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____userNameText_6)); }
	inline Text_t356221433 * get__userNameText_6() const { return ____userNameText_6; }
	inline Text_t356221433 ** get_address_of__userNameText_6() { return &____userNameText_6; }
	inline void set__userNameText_6(Text_t356221433 * value)
	{
		____userNameText_6 = value;
		Il2CppCodeGenWriteBarrier(&____userNameText_6, value);
	}

	inline static int32_t get_offset_of__userRankText_7() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____userRankText_7)); }
	inline Text_t356221433 * get__userRankText_7() const { return ____userRankText_7; }
	inline Text_t356221433 ** get_address_of__userRankText_7() { return &____userRankText_7; }
	inline void set__userRankText_7(Text_t356221433 * value)
	{
		____userRankText_7 = value;
		Il2CppCodeGenWriteBarrier(&____userRankText_7, value);
	}

	inline static int32_t get_offset_of__userPointText_8() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____userPointText_8)); }
	inline Text_t356221433 * get__userPointText_8() const { return ____userPointText_8; }
	inline Text_t356221433 ** get_address_of__userPointText_8() { return &____userPointText_8; }
	inline void set__userPointText_8(Text_t356221433 * value)
	{
		____userPointText_8 = value;
		Il2CppCodeGenWriteBarrier(&____userPointText_8, value);
	}

	inline static int32_t get_offset_of__mMobiliteIndexCurrent_9() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____mMobiliteIndexCurrent_9)); }
	inline int32_t get__mMobiliteIndexCurrent_9() const { return ____mMobiliteIndexCurrent_9; }
	inline int32_t* get_address_of__mMobiliteIndexCurrent_9() { return &____mMobiliteIndexCurrent_9; }
	inline void set__mMobiliteIndexCurrent_9(int32_t value)
	{
		____mMobiliteIndexCurrent_9 = value;
	}

	inline static int32_t get_offset_of__myMobilityModel_10() { return static_cast<int32_t>(offsetof(OLMyPagePanel_t3611893820, ____myMobilityModel_10)); }
	inline GetMyMobilityModel_t2067367768 * get__myMobilityModel_10() const { return ____myMobilityModel_10; }
	inline GetMyMobilityModel_t2067367768 ** get_address_of__myMobilityModel_10() { return &____myMobilityModel_10; }
	inline void set__myMobilityModel_10(GetMyMobilityModel_t2067367768 * value)
	{
		____myMobilityModel_10 = value;
		Il2CppCodeGenWriteBarrier(&____myMobilityModel_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
