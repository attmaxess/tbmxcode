﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerStringB1343648701.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.DerGraphicString
struct  DerGraphicString_t3982270410  : public DerStringBase_t1343648701
{
public:
	// System.Byte[] Org.BouncyCastle.Asn1.DerGraphicString::mString
	ByteU5BU5D_t3397334013* ___mString_2;

public:
	inline static int32_t get_offset_of_mString_2() { return static_cast<int32_t>(offsetof(DerGraphicString_t3982270410, ___mString_2)); }
	inline ByteU5BU5D_t3397334013* get_mString_2() const { return ___mString_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_mString_2() { return &___mString_2; }
	inline void set_mString_2(ByteU5BU5D_t3397334013* value)
	{
		___mString_2 = value;
		Il2CppCodeGenWriteBarrier(&___mString_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
