﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t3495876513;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers
struct  MiscObjectIdentifiers_t808030413  : public Il2CppObject
{
public:

public:
};

struct MiscObjectIdentifiers_t808030413_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Netscape
	DerObjectIdentifier_t3495876513 * ___Netscape_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCertType
	DerObjectIdentifier_t3495876513 * ___NetscapeCertType_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeBaseUrl
	DerObjectIdentifier_t3495876513 * ___NetscapeBaseUrl_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeRevocationUrl
	DerObjectIdentifier_t3495876513 * ___NetscapeRevocationUrl_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCARevocationUrl
	DerObjectIdentifier_t3495876513 * ___NetscapeCARevocationUrl_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeRenewalUrl
	DerObjectIdentifier_t3495876513 * ___NetscapeRenewalUrl_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCAPolicyUrl
	DerObjectIdentifier_t3495876513 * ___NetscapeCAPolicyUrl_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeSslServerName
	DerObjectIdentifier_t3495876513 * ___NetscapeSslServerName_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCertComment
	DerObjectIdentifier_t3495876513 * ___NetscapeCertComment_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Verisign
	DerObjectIdentifier_t3495876513 * ___Verisign_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignCzagExtension
	DerObjectIdentifier_t3495876513 * ___VerisignCzagExtension_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignPrivate_6_9
	DerObjectIdentifier_t3495876513 * ___VerisignPrivate_6_9_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignOnSiteJurisdictionHash
	DerObjectIdentifier_t3495876513 * ___VerisignOnSiteJurisdictionHash_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignBitString_6_13
	DerObjectIdentifier_t3495876513 * ___VerisignBitString_6_13_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignDnbDunsNumber
	DerObjectIdentifier_t3495876513 * ___VerisignDnbDunsNumber_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignIssStrongCrypto
	DerObjectIdentifier_t3495876513 * ___VerisignIssStrongCrypto_15;
	// System.String Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Novell
	String_t* ___Novell_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NovellSecurityAttribs
	DerObjectIdentifier_t3495876513 * ___NovellSecurityAttribs_17;
	// System.String Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Entrust
	String_t* ___Entrust_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::EntrustVersionExtension
	DerObjectIdentifier_t3495876513 * ___EntrustVersionExtension_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::as_sys_sec_alg_ideaCBC
	DerObjectIdentifier_t3495876513 * ___as_sys_sec_alg_ideaCBC_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib
	DerObjectIdentifier_t3495876513 * ___cryptlib_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm
	DerObjectIdentifier_t3495876513 * ___cryptlib_algorithm_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_ECB
	DerObjectIdentifier_t3495876513 * ___cryptlib_algorithm_blowfish_ECB_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_CBC
	DerObjectIdentifier_t3495876513 * ___cryptlib_algorithm_blowfish_CBC_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_CFB
	DerObjectIdentifier_t3495876513 * ___cryptlib_algorithm_blowfish_CFB_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_OFB
	DerObjectIdentifier_t3495876513 * ___cryptlib_algorithm_blowfish_OFB_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::blake2
	DerObjectIdentifier_t3495876513 * ___blake2_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b160
	DerObjectIdentifier_t3495876513 * ___id_blake2b160_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b256
	DerObjectIdentifier_t3495876513 * ___id_blake2b256_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b384
	DerObjectIdentifier_t3495876513 * ___id_blake2b384_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b512
	DerObjectIdentifier_t3495876513 * ___id_blake2b512_31;

public:
	inline static int32_t get_offset_of_Netscape_0() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___Netscape_0)); }
	inline DerObjectIdentifier_t3495876513 * get_Netscape_0() const { return ___Netscape_0; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_Netscape_0() { return &___Netscape_0; }
	inline void set_Netscape_0(DerObjectIdentifier_t3495876513 * value)
	{
		___Netscape_0 = value;
		Il2CppCodeGenWriteBarrier(&___Netscape_0, value);
	}

	inline static int32_t get_offset_of_NetscapeCertType_1() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeCertType_1)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeCertType_1() const { return ___NetscapeCertType_1; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeCertType_1() { return &___NetscapeCertType_1; }
	inline void set_NetscapeCertType_1(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeCertType_1 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeCertType_1, value);
	}

	inline static int32_t get_offset_of_NetscapeBaseUrl_2() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeBaseUrl_2)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeBaseUrl_2() const { return ___NetscapeBaseUrl_2; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeBaseUrl_2() { return &___NetscapeBaseUrl_2; }
	inline void set_NetscapeBaseUrl_2(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeBaseUrl_2 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeBaseUrl_2, value);
	}

	inline static int32_t get_offset_of_NetscapeRevocationUrl_3() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeRevocationUrl_3)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeRevocationUrl_3() const { return ___NetscapeRevocationUrl_3; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeRevocationUrl_3() { return &___NetscapeRevocationUrl_3; }
	inline void set_NetscapeRevocationUrl_3(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeRevocationUrl_3 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeRevocationUrl_3, value);
	}

	inline static int32_t get_offset_of_NetscapeCARevocationUrl_4() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeCARevocationUrl_4)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeCARevocationUrl_4() const { return ___NetscapeCARevocationUrl_4; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeCARevocationUrl_4() { return &___NetscapeCARevocationUrl_4; }
	inline void set_NetscapeCARevocationUrl_4(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeCARevocationUrl_4 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeCARevocationUrl_4, value);
	}

	inline static int32_t get_offset_of_NetscapeRenewalUrl_5() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeRenewalUrl_5)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeRenewalUrl_5() const { return ___NetscapeRenewalUrl_5; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeRenewalUrl_5() { return &___NetscapeRenewalUrl_5; }
	inline void set_NetscapeRenewalUrl_5(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeRenewalUrl_5 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeRenewalUrl_5, value);
	}

	inline static int32_t get_offset_of_NetscapeCAPolicyUrl_6() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeCAPolicyUrl_6)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeCAPolicyUrl_6() const { return ___NetscapeCAPolicyUrl_6; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeCAPolicyUrl_6() { return &___NetscapeCAPolicyUrl_6; }
	inline void set_NetscapeCAPolicyUrl_6(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeCAPolicyUrl_6 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeCAPolicyUrl_6, value);
	}

	inline static int32_t get_offset_of_NetscapeSslServerName_7() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeSslServerName_7)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeSslServerName_7() const { return ___NetscapeSslServerName_7; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeSslServerName_7() { return &___NetscapeSslServerName_7; }
	inline void set_NetscapeSslServerName_7(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeSslServerName_7 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeSslServerName_7, value);
	}

	inline static int32_t get_offset_of_NetscapeCertComment_8() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NetscapeCertComment_8)); }
	inline DerObjectIdentifier_t3495876513 * get_NetscapeCertComment_8() const { return ___NetscapeCertComment_8; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NetscapeCertComment_8() { return &___NetscapeCertComment_8; }
	inline void set_NetscapeCertComment_8(DerObjectIdentifier_t3495876513 * value)
	{
		___NetscapeCertComment_8 = value;
		Il2CppCodeGenWriteBarrier(&___NetscapeCertComment_8, value);
	}

	inline static int32_t get_offset_of_Verisign_9() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___Verisign_9)); }
	inline DerObjectIdentifier_t3495876513 * get_Verisign_9() const { return ___Verisign_9; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_Verisign_9() { return &___Verisign_9; }
	inline void set_Verisign_9(DerObjectIdentifier_t3495876513 * value)
	{
		___Verisign_9 = value;
		Il2CppCodeGenWriteBarrier(&___Verisign_9, value);
	}

	inline static int32_t get_offset_of_VerisignCzagExtension_10() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___VerisignCzagExtension_10)); }
	inline DerObjectIdentifier_t3495876513 * get_VerisignCzagExtension_10() const { return ___VerisignCzagExtension_10; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_VerisignCzagExtension_10() { return &___VerisignCzagExtension_10; }
	inline void set_VerisignCzagExtension_10(DerObjectIdentifier_t3495876513 * value)
	{
		___VerisignCzagExtension_10 = value;
		Il2CppCodeGenWriteBarrier(&___VerisignCzagExtension_10, value);
	}

	inline static int32_t get_offset_of_VerisignPrivate_6_9_11() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___VerisignPrivate_6_9_11)); }
	inline DerObjectIdentifier_t3495876513 * get_VerisignPrivate_6_9_11() const { return ___VerisignPrivate_6_9_11; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_VerisignPrivate_6_9_11() { return &___VerisignPrivate_6_9_11; }
	inline void set_VerisignPrivate_6_9_11(DerObjectIdentifier_t3495876513 * value)
	{
		___VerisignPrivate_6_9_11 = value;
		Il2CppCodeGenWriteBarrier(&___VerisignPrivate_6_9_11, value);
	}

	inline static int32_t get_offset_of_VerisignOnSiteJurisdictionHash_12() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___VerisignOnSiteJurisdictionHash_12)); }
	inline DerObjectIdentifier_t3495876513 * get_VerisignOnSiteJurisdictionHash_12() const { return ___VerisignOnSiteJurisdictionHash_12; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_VerisignOnSiteJurisdictionHash_12() { return &___VerisignOnSiteJurisdictionHash_12; }
	inline void set_VerisignOnSiteJurisdictionHash_12(DerObjectIdentifier_t3495876513 * value)
	{
		___VerisignOnSiteJurisdictionHash_12 = value;
		Il2CppCodeGenWriteBarrier(&___VerisignOnSiteJurisdictionHash_12, value);
	}

	inline static int32_t get_offset_of_VerisignBitString_6_13_13() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___VerisignBitString_6_13_13)); }
	inline DerObjectIdentifier_t3495876513 * get_VerisignBitString_6_13_13() const { return ___VerisignBitString_6_13_13; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_VerisignBitString_6_13_13() { return &___VerisignBitString_6_13_13; }
	inline void set_VerisignBitString_6_13_13(DerObjectIdentifier_t3495876513 * value)
	{
		___VerisignBitString_6_13_13 = value;
		Il2CppCodeGenWriteBarrier(&___VerisignBitString_6_13_13, value);
	}

	inline static int32_t get_offset_of_VerisignDnbDunsNumber_14() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___VerisignDnbDunsNumber_14)); }
	inline DerObjectIdentifier_t3495876513 * get_VerisignDnbDunsNumber_14() const { return ___VerisignDnbDunsNumber_14; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_VerisignDnbDunsNumber_14() { return &___VerisignDnbDunsNumber_14; }
	inline void set_VerisignDnbDunsNumber_14(DerObjectIdentifier_t3495876513 * value)
	{
		___VerisignDnbDunsNumber_14 = value;
		Il2CppCodeGenWriteBarrier(&___VerisignDnbDunsNumber_14, value);
	}

	inline static int32_t get_offset_of_VerisignIssStrongCrypto_15() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___VerisignIssStrongCrypto_15)); }
	inline DerObjectIdentifier_t3495876513 * get_VerisignIssStrongCrypto_15() const { return ___VerisignIssStrongCrypto_15; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_VerisignIssStrongCrypto_15() { return &___VerisignIssStrongCrypto_15; }
	inline void set_VerisignIssStrongCrypto_15(DerObjectIdentifier_t3495876513 * value)
	{
		___VerisignIssStrongCrypto_15 = value;
		Il2CppCodeGenWriteBarrier(&___VerisignIssStrongCrypto_15, value);
	}

	inline static int32_t get_offset_of_Novell_16() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___Novell_16)); }
	inline String_t* get_Novell_16() const { return ___Novell_16; }
	inline String_t** get_address_of_Novell_16() { return &___Novell_16; }
	inline void set_Novell_16(String_t* value)
	{
		___Novell_16 = value;
		Il2CppCodeGenWriteBarrier(&___Novell_16, value);
	}

	inline static int32_t get_offset_of_NovellSecurityAttribs_17() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___NovellSecurityAttribs_17)); }
	inline DerObjectIdentifier_t3495876513 * get_NovellSecurityAttribs_17() const { return ___NovellSecurityAttribs_17; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NovellSecurityAttribs_17() { return &___NovellSecurityAttribs_17; }
	inline void set_NovellSecurityAttribs_17(DerObjectIdentifier_t3495876513 * value)
	{
		___NovellSecurityAttribs_17 = value;
		Il2CppCodeGenWriteBarrier(&___NovellSecurityAttribs_17, value);
	}

	inline static int32_t get_offset_of_Entrust_18() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___Entrust_18)); }
	inline String_t* get_Entrust_18() const { return ___Entrust_18; }
	inline String_t** get_address_of_Entrust_18() { return &___Entrust_18; }
	inline void set_Entrust_18(String_t* value)
	{
		___Entrust_18 = value;
		Il2CppCodeGenWriteBarrier(&___Entrust_18, value);
	}

	inline static int32_t get_offset_of_EntrustVersionExtension_19() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___EntrustVersionExtension_19)); }
	inline DerObjectIdentifier_t3495876513 * get_EntrustVersionExtension_19() const { return ___EntrustVersionExtension_19; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_EntrustVersionExtension_19() { return &___EntrustVersionExtension_19; }
	inline void set_EntrustVersionExtension_19(DerObjectIdentifier_t3495876513 * value)
	{
		___EntrustVersionExtension_19 = value;
		Il2CppCodeGenWriteBarrier(&___EntrustVersionExtension_19, value);
	}

	inline static int32_t get_offset_of_as_sys_sec_alg_ideaCBC_20() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___as_sys_sec_alg_ideaCBC_20)); }
	inline DerObjectIdentifier_t3495876513 * get_as_sys_sec_alg_ideaCBC_20() const { return ___as_sys_sec_alg_ideaCBC_20; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_as_sys_sec_alg_ideaCBC_20() { return &___as_sys_sec_alg_ideaCBC_20; }
	inline void set_as_sys_sec_alg_ideaCBC_20(DerObjectIdentifier_t3495876513 * value)
	{
		___as_sys_sec_alg_ideaCBC_20 = value;
		Il2CppCodeGenWriteBarrier(&___as_sys_sec_alg_ideaCBC_20, value);
	}

	inline static int32_t get_offset_of_cryptlib_21() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___cryptlib_21)); }
	inline DerObjectIdentifier_t3495876513 * get_cryptlib_21() const { return ___cryptlib_21; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_cryptlib_21() { return &___cryptlib_21; }
	inline void set_cryptlib_21(DerObjectIdentifier_t3495876513 * value)
	{
		___cryptlib_21 = value;
		Il2CppCodeGenWriteBarrier(&___cryptlib_21, value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_22() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___cryptlib_algorithm_22)); }
	inline DerObjectIdentifier_t3495876513 * get_cryptlib_algorithm_22() const { return ___cryptlib_algorithm_22; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_cryptlib_algorithm_22() { return &___cryptlib_algorithm_22; }
	inline void set_cryptlib_algorithm_22(DerObjectIdentifier_t3495876513 * value)
	{
		___cryptlib_algorithm_22 = value;
		Il2CppCodeGenWriteBarrier(&___cryptlib_algorithm_22, value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_ECB_23() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___cryptlib_algorithm_blowfish_ECB_23)); }
	inline DerObjectIdentifier_t3495876513 * get_cryptlib_algorithm_blowfish_ECB_23() const { return ___cryptlib_algorithm_blowfish_ECB_23; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_cryptlib_algorithm_blowfish_ECB_23() { return &___cryptlib_algorithm_blowfish_ECB_23; }
	inline void set_cryptlib_algorithm_blowfish_ECB_23(DerObjectIdentifier_t3495876513 * value)
	{
		___cryptlib_algorithm_blowfish_ECB_23 = value;
		Il2CppCodeGenWriteBarrier(&___cryptlib_algorithm_blowfish_ECB_23, value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_CBC_24() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___cryptlib_algorithm_blowfish_CBC_24)); }
	inline DerObjectIdentifier_t3495876513 * get_cryptlib_algorithm_blowfish_CBC_24() const { return ___cryptlib_algorithm_blowfish_CBC_24; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_cryptlib_algorithm_blowfish_CBC_24() { return &___cryptlib_algorithm_blowfish_CBC_24; }
	inline void set_cryptlib_algorithm_blowfish_CBC_24(DerObjectIdentifier_t3495876513 * value)
	{
		___cryptlib_algorithm_blowfish_CBC_24 = value;
		Il2CppCodeGenWriteBarrier(&___cryptlib_algorithm_blowfish_CBC_24, value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_CFB_25() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___cryptlib_algorithm_blowfish_CFB_25)); }
	inline DerObjectIdentifier_t3495876513 * get_cryptlib_algorithm_blowfish_CFB_25() const { return ___cryptlib_algorithm_blowfish_CFB_25; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_cryptlib_algorithm_blowfish_CFB_25() { return &___cryptlib_algorithm_blowfish_CFB_25; }
	inline void set_cryptlib_algorithm_blowfish_CFB_25(DerObjectIdentifier_t3495876513 * value)
	{
		___cryptlib_algorithm_blowfish_CFB_25 = value;
		Il2CppCodeGenWriteBarrier(&___cryptlib_algorithm_blowfish_CFB_25, value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_OFB_26() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___cryptlib_algorithm_blowfish_OFB_26)); }
	inline DerObjectIdentifier_t3495876513 * get_cryptlib_algorithm_blowfish_OFB_26() const { return ___cryptlib_algorithm_blowfish_OFB_26; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_cryptlib_algorithm_blowfish_OFB_26() { return &___cryptlib_algorithm_blowfish_OFB_26; }
	inline void set_cryptlib_algorithm_blowfish_OFB_26(DerObjectIdentifier_t3495876513 * value)
	{
		___cryptlib_algorithm_blowfish_OFB_26 = value;
		Il2CppCodeGenWriteBarrier(&___cryptlib_algorithm_blowfish_OFB_26, value);
	}

	inline static int32_t get_offset_of_blake2_27() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___blake2_27)); }
	inline DerObjectIdentifier_t3495876513 * get_blake2_27() const { return ___blake2_27; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_blake2_27() { return &___blake2_27; }
	inline void set_blake2_27(DerObjectIdentifier_t3495876513 * value)
	{
		___blake2_27 = value;
		Il2CppCodeGenWriteBarrier(&___blake2_27, value);
	}

	inline static int32_t get_offset_of_id_blake2b160_28() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___id_blake2b160_28)); }
	inline DerObjectIdentifier_t3495876513 * get_id_blake2b160_28() const { return ___id_blake2b160_28; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_id_blake2b160_28() { return &___id_blake2b160_28; }
	inline void set_id_blake2b160_28(DerObjectIdentifier_t3495876513 * value)
	{
		___id_blake2b160_28 = value;
		Il2CppCodeGenWriteBarrier(&___id_blake2b160_28, value);
	}

	inline static int32_t get_offset_of_id_blake2b256_29() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___id_blake2b256_29)); }
	inline DerObjectIdentifier_t3495876513 * get_id_blake2b256_29() const { return ___id_blake2b256_29; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_id_blake2b256_29() { return &___id_blake2b256_29; }
	inline void set_id_blake2b256_29(DerObjectIdentifier_t3495876513 * value)
	{
		___id_blake2b256_29 = value;
		Il2CppCodeGenWriteBarrier(&___id_blake2b256_29, value);
	}

	inline static int32_t get_offset_of_id_blake2b384_30() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___id_blake2b384_30)); }
	inline DerObjectIdentifier_t3495876513 * get_id_blake2b384_30() const { return ___id_blake2b384_30; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_id_blake2b384_30() { return &___id_blake2b384_30; }
	inline void set_id_blake2b384_30(DerObjectIdentifier_t3495876513 * value)
	{
		___id_blake2b384_30 = value;
		Il2CppCodeGenWriteBarrier(&___id_blake2b384_30, value);
	}

	inline static int32_t get_offset_of_id_blake2b512_31() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t808030413_StaticFields, ___id_blake2b512_31)); }
	inline DerObjectIdentifier_t3495876513 * get_id_blake2b512_31() const { return ___id_blake2b512_31; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_id_blake2b512_31() { return &___id_blake2b512_31; }
	inline void set_id_blake2b512_31(DerObjectIdentifier_t3495876513 * value)
	{
		___id_blake2b512_31 = value;
		Il2CppCodeGenWriteBarrier(&___id_blake2b512_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
