﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t3495876513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers
struct  NistObjectIdentifiers_t1742042649  : public Il2CppObject
{
public:

public:
};

struct NistObjectIdentifiers_t1742042649_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::NistAlgorithm
	DerObjectIdentifier_t3495876513 * ___NistAlgorithm_0;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::HashAlgs
	DerObjectIdentifier_t3495876513 * ___HashAlgs_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha256
	DerObjectIdentifier_t3495876513 * ___IdSha256_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha384
	DerObjectIdentifier_t3495876513 * ___IdSha384_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512
	DerObjectIdentifier_t3495876513 * ___IdSha512_4;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha224
	DerObjectIdentifier_t3495876513 * ___IdSha224_5;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512_224
	DerObjectIdentifier_t3495876513 * ___IdSha512_224_6;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512_256
	DerObjectIdentifier_t3495876513 * ___IdSha512_256_7;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_224
	DerObjectIdentifier_t3495876513 * ___IdSha3_224_8;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_256
	DerObjectIdentifier_t3495876513 * ___IdSha3_256_9;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_384
	DerObjectIdentifier_t3495876513 * ___IdSha3_384_10;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_512
	DerObjectIdentifier_t3495876513 * ___IdSha3_512_11;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdShake128
	DerObjectIdentifier_t3495876513 * ___IdShake128_12;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdShake256
	DerObjectIdentifier_t3495876513 * ___IdShake256_13;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::Aes
	DerObjectIdentifier_t3495876513 * ___Aes_14;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ecb
	DerObjectIdentifier_t3495876513 * ___IdAes128Ecb_15;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Cbc
	DerObjectIdentifier_t3495876513 * ___IdAes128Cbc_16;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ofb
	DerObjectIdentifier_t3495876513 * ___IdAes128Ofb_17;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Cfb
	DerObjectIdentifier_t3495876513 * ___IdAes128Cfb_18;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Wrap
	DerObjectIdentifier_t3495876513 * ___IdAes128Wrap_19;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Gcm
	DerObjectIdentifier_t3495876513 * ___IdAes128Gcm_20;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ccm
	DerObjectIdentifier_t3495876513 * ___IdAes128Ccm_21;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ecb
	DerObjectIdentifier_t3495876513 * ___IdAes192Ecb_22;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Cbc
	DerObjectIdentifier_t3495876513 * ___IdAes192Cbc_23;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ofb
	DerObjectIdentifier_t3495876513 * ___IdAes192Ofb_24;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Cfb
	DerObjectIdentifier_t3495876513 * ___IdAes192Cfb_25;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Wrap
	DerObjectIdentifier_t3495876513 * ___IdAes192Wrap_26;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Gcm
	DerObjectIdentifier_t3495876513 * ___IdAes192Gcm_27;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ccm
	DerObjectIdentifier_t3495876513 * ___IdAes192Ccm_28;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ecb
	DerObjectIdentifier_t3495876513 * ___IdAes256Ecb_29;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Cbc
	DerObjectIdentifier_t3495876513 * ___IdAes256Cbc_30;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ofb
	DerObjectIdentifier_t3495876513 * ___IdAes256Ofb_31;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Cfb
	DerObjectIdentifier_t3495876513 * ___IdAes256Cfb_32;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Wrap
	DerObjectIdentifier_t3495876513 * ___IdAes256Wrap_33;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Gcm
	DerObjectIdentifier_t3495876513 * ___IdAes256Gcm_34;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ccm
	DerObjectIdentifier_t3495876513 * ___IdAes256Ccm_35;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdDsaWithSha2
	DerObjectIdentifier_t3495876513 * ___IdDsaWithSha2_36;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha224
	DerObjectIdentifier_t3495876513 * ___DsaWithSha224_37;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha256
	DerObjectIdentifier_t3495876513 * ___DsaWithSha256_38;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha384
	DerObjectIdentifier_t3495876513 * ___DsaWithSha384_39;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha512
	DerObjectIdentifier_t3495876513 * ___DsaWithSha512_40;

public:
	inline static int32_t get_offset_of_NistAlgorithm_0() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___NistAlgorithm_0)); }
	inline DerObjectIdentifier_t3495876513 * get_NistAlgorithm_0() const { return ___NistAlgorithm_0; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_NistAlgorithm_0() { return &___NistAlgorithm_0; }
	inline void set_NistAlgorithm_0(DerObjectIdentifier_t3495876513 * value)
	{
		___NistAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier(&___NistAlgorithm_0, value);
	}

	inline static int32_t get_offset_of_HashAlgs_1() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___HashAlgs_1)); }
	inline DerObjectIdentifier_t3495876513 * get_HashAlgs_1() const { return ___HashAlgs_1; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_HashAlgs_1() { return &___HashAlgs_1; }
	inline void set_HashAlgs_1(DerObjectIdentifier_t3495876513 * value)
	{
		___HashAlgs_1 = value;
		Il2CppCodeGenWriteBarrier(&___HashAlgs_1, value);
	}

	inline static int32_t get_offset_of_IdSha256_2() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha256_2)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha256_2() const { return ___IdSha256_2; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha256_2() { return &___IdSha256_2; }
	inline void set_IdSha256_2(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha256_2 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha256_2, value);
	}

	inline static int32_t get_offset_of_IdSha384_3() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha384_3)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha384_3() const { return ___IdSha384_3; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha384_3() { return &___IdSha384_3; }
	inline void set_IdSha384_3(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha384_3 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha384_3, value);
	}

	inline static int32_t get_offset_of_IdSha512_4() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha512_4)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha512_4() const { return ___IdSha512_4; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha512_4() { return &___IdSha512_4; }
	inline void set_IdSha512_4(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha512_4 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha512_4, value);
	}

	inline static int32_t get_offset_of_IdSha224_5() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha224_5)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha224_5() const { return ___IdSha224_5; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha224_5() { return &___IdSha224_5; }
	inline void set_IdSha224_5(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha224_5 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha224_5, value);
	}

	inline static int32_t get_offset_of_IdSha512_224_6() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha512_224_6)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha512_224_6() const { return ___IdSha512_224_6; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha512_224_6() { return &___IdSha512_224_6; }
	inline void set_IdSha512_224_6(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha512_224_6 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha512_224_6, value);
	}

	inline static int32_t get_offset_of_IdSha512_256_7() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha512_256_7)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha512_256_7() const { return ___IdSha512_256_7; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha512_256_7() { return &___IdSha512_256_7; }
	inline void set_IdSha512_256_7(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha512_256_7 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha512_256_7, value);
	}

	inline static int32_t get_offset_of_IdSha3_224_8() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha3_224_8)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha3_224_8() const { return ___IdSha3_224_8; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha3_224_8() { return &___IdSha3_224_8; }
	inline void set_IdSha3_224_8(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha3_224_8 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha3_224_8, value);
	}

	inline static int32_t get_offset_of_IdSha3_256_9() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha3_256_9)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha3_256_9() const { return ___IdSha3_256_9; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha3_256_9() { return &___IdSha3_256_9; }
	inline void set_IdSha3_256_9(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha3_256_9 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha3_256_9, value);
	}

	inline static int32_t get_offset_of_IdSha3_384_10() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha3_384_10)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha3_384_10() const { return ___IdSha3_384_10; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha3_384_10() { return &___IdSha3_384_10; }
	inline void set_IdSha3_384_10(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha3_384_10 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha3_384_10, value);
	}

	inline static int32_t get_offset_of_IdSha3_512_11() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdSha3_512_11)); }
	inline DerObjectIdentifier_t3495876513 * get_IdSha3_512_11() const { return ___IdSha3_512_11; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdSha3_512_11() { return &___IdSha3_512_11; }
	inline void set_IdSha3_512_11(DerObjectIdentifier_t3495876513 * value)
	{
		___IdSha3_512_11 = value;
		Il2CppCodeGenWriteBarrier(&___IdSha3_512_11, value);
	}

	inline static int32_t get_offset_of_IdShake128_12() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdShake128_12)); }
	inline DerObjectIdentifier_t3495876513 * get_IdShake128_12() const { return ___IdShake128_12; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdShake128_12() { return &___IdShake128_12; }
	inline void set_IdShake128_12(DerObjectIdentifier_t3495876513 * value)
	{
		___IdShake128_12 = value;
		Il2CppCodeGenWriteBarrier(&___IdShake128_12, value);
	}

	inline static int32_t get_offset_of_IdShake256_13() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdShake256_13)); }
	inline DerObjectIdentifier_t3495876513 * get_IdShake256_13() const { return ___IdShake256_13; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdShake256_13() { return &___IdShake256_13; }
	inline void set_IdShake256_13(DerObjectIdentifier_t3495876513 * value)
	{
		___IdShake256_13 = value;
		Il2CppCodeGenWriteBarrier(&___IdShake256_13, value);
	}

	inline static int32_t get_offset_of_Aes_14() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___Aes_14)); }
	inline DerObjectIdentifier_t3495876513 * get_Aes_14() const { return ___Aes_14; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_Aes_14() { return &___Aes_14; }
	inline void set_Aes_14(DerObjectIdentifier_t3495876513 * value)
	{
		___Aes_14 = value;
		Il2CppCodeGenWriteBarrier(&___Aes_14, value);
	}

	inline static int32_t get_offset_of_IdAes128Ecb_15() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes128Ecb_15)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes128Ecb_15() const { return ___IdAes128Ecb_15; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes128Ecb_15() { return &___IdAes128Ecb_15; }
	inline void set_IdAes128Ecb_15(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes128Ecb_15 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes128Ecb_15, value);
	}

	inline static int32_t get_offset_of_IdAes128Cbc_16() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes128Cbc_16)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes128Cbc_16() const { return ___IdAes128Cbc_16; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes128Cbc_16() { return &___IdAes128Cbc_16; }
	inline void set_IdAes128Cbc_16(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes128Cbc_16 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes128Cbc_16, value);
	}

	inline static int32_t get_offset_of_IdAes128Ofb_17() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes128Ofb_17)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes128Ofb_17() const { return ___IdAes128Ofb_17; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes128Ofb_17() { return &___IdAes128Ofb_17; }
	inline void set_IdAes128Ofb_17(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes128Ofb_17 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes128Ofb_17, value);
	}

	inline static int32_t get_offset_of_IdAes128Cfb_18() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes128Cfb_18)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes128Cfb_18() const { return ___IdAes128Cfb_18; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes128Cfb_18() { return &___IdAes128Cfb_18; }
	inline void set_IdAes128Cfb_18(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes128Cfb_18 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes128Cfb_18, value);
	}

	inline static int32_t get_offset_of_IdAes128Wrap_19() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes128Wrap_19)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes128Wrap_19() const { return ___IdAes128Wrap_19; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes128Wrap_19() { return &___IdAes128Wrap_19; }
	inline void set_IdAes128Wrap_19(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes128Wrap_19 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes128Wrap_19, value);
	}

	inline static int32_t get_offset_of_IdAes128Gcm_20() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes128Gcm_20)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes128Gcm_20() const { return ___IdAes128Gcm_20; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes128Gcm_20() { return &___IdAes128Gcm_20; }
	inline void set_IdAes128Gcm_20(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes128Gcm_20 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes128Gcm_20, value);
	}

	inline static int32_t get_offset_of_IdAes128Ccm_21() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes128Ccm_21)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes128Ccm_21() const { return ___IdAes128Ccm_21; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes128Ccm_21() { return &___IdAes128Ccm_21; }
	inline void set_IdAes128Ccm_21(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes128Ccm_21 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes128Ccm_21, value);
	}

	inline static int32_t get_offset_of_IdAes192Ecb_22() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes192Ecb_22)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes192Ecb_22() const { return ___IdAes192Ecb_22; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes192Ecb_22() { return &___IdAes192Ecb_22; }
	inline void set_IdAes192Ecb_22(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes192Ecb_22 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes192Ecb_22, value);
	}

	inline static int32_t get_offset_of_IdAes192Cbc_23() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes192Cbc_23)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes192Cbc_23() const { return ___IdAes192Cbc_23; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes192Cbc_23() { return &___IdAes192Cbc_23; }
	inline void set_IdAes192Cbc_23(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes192Cbc_23 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes192Cbc_23, value);
	}

	inline static int32_t get_offset_of_IdAes192Ofb_24() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes192Ofb_24)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes192Ofb_24() const { return ___IdAes192Ofb_24; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes192Ofb_24() { return &___IdAes192Ofb_24; }
	inline void set_IdAes192Ofb_24(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes192Ofb_24 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes192Ofb_24, value);
	}

	inline static int32_t get_offset_of_IdAes192Cfb_25() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes192Cfb_25)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes192Cfb_25() const { return ___IdAes192Cfb_25; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes192Cfb_25() { return &___IdAes192Cfb_25; }
	inline void set_IdAes192Cfb_25(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes192Cfb_25 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes192Cfb_25, value);
	}

	inline static int32_t get_offset_of_IdAes192Wrap_26() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes192Wrap_26)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes192Wrap_26() const { return ___IdAes192Wrap_26; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes192Wrap_26() { return &___IdAes192Wrap_26; }
	inline void set_IdAes192Wrap_26(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes192Wrap_26 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes192Wrap_26, value);
	}

	inline static int32_t get_offset_of_IdAes192Gcm_27() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes192Gcm_27)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes192Gcm_27() const { return ___IdAes192Gcm_27; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes192Gcm_27() { return &___IdAes192Gcm_27; }
	inline void set_IdAes192Gcm_27(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes192Gcm_27 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes192Gcm_27, value);
	}

	inline static int32_t get_offset_of_IdAes192Ccm_28() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes192Ccm_28)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes192Ccm_28() const { return ___IdAes192Ccm_28; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes192Ccm_28() { return &___IdAes192Ccm_28; }
	inline void set_IdAes192Ccm_28(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes192Ccm_28 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes192Ccm_28, value);
	}

	inline static int32_t get_offset_of_IdAes256Ecb_29() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes256Ecb_29)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes256Ecb_29() const { return ___IdAes256Ecb_29; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes256Ecb_29() { return &___IdAes256Ecb_29; }
	inline void set_IdAes256Ecb_29(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes256Ecb_29 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes256Ecb_29, value);
	}

	inline static int32_t get_offset_of_IdAes256Cbc_30() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes256Cbc_30)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes256Cbc_30() const { return ___IdAes256Cbc_30; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes256Cbc_30() { return &___IdAes256Cbc_30; }
	inline void set_IdAes256Cbc_30(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes256Cbc_30 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes256Cbc_30, value);
	}

	inline static int32_t get_offset_of_IdAes256Ofb_31() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes256Ofb_31)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes256Ofb_31() const { return ___IdAes256Ofb_31; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes256Ofb_31() { return &___IdAes256Ofb_31; }
	inline void set_IdAes256Ofb_31(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes256Ofb_31 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes256Ofb_31, value);
	}

	inline static int32_t get_offset_of_IdAes256Cfb_32() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes256Cfb_32)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes256Cfb_32() const { return ___IdAes256Cfb_32; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes256Cfb_32() { return &___IdAes256Cfb_32; }
	inline void set_IdAes256Cfb_32(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes256Cfb_32 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes256Cfb_32, value);
	}

	inline static int32_t get_offset_of_IdAes256Wrap_33() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes256Wrap_33)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes256Wrap_33() const { return ___IdAes256Wrap_33; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes256Wrap_33() { return &___IdAes256Wrap_33; }
	inline void set_IdAes256Wrap_33(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes256Wrap_33 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes256Wrap_33, value);
	}

	inline static int32_t get_offset_of_IdAes256Gcm_34() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes256Gcm_34)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes256Gcm_34() const { return ___IdAes256Gcm_34; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes256Gcm_34() { return &___IdAes256Gcm_34; }
	inline void set_IdAes256Gcm_34(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes256Gcm_34 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes256Gcm_34, value);
	}

	inline static int32_t get_offset_of_IdAes256Ccm_35() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdAes256Ccm_35)); }
	inline DerObjectIdentifier_t3495876513 * get_IdAes256Ccm_35() const { return ___IdAes256Ccm_35; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdAes256Ccm_35() { return &___IdAes256Ccm_35; }
	inline void set_IdAes256Ccm_35(DerObjectIdentifier_t3495876513 * value)
	{
		___IdAes256Ccm_35 = value;
		Il2CppCodeGenWriteBarrier(&___IdAes256Ccm_35, value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha2_36() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___IdDsaWithSha2_36)); }
	inline DerObjectIdentifier_t3495876513 * get_IdDsaWithSha2_36() const { return ___IdDsaWithSha2_36; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_IdDsaWithSha2_36() { return &___IdDsaWithSha2_36; }
	inline void set_IdDsaWithSha2_36(DerObjectIdentifier_t3495876513 * value)
	{
		___IdDsaWithSha2_36 = value;
		Il2CppCodeGenWriteBarrier(&___IdDsaWithSha2_36, value);
	}

	inline static int32_t get_offset_of_DsaWithSha224_37() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___DsaWithSha224_37)); }
	inline DerObjectIdentifier_t3495876513 * get_DsaWithSha224_37() const { return ___DsaWithSha224_37; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_DsaWithSha224_37() { return &___DsaWithSha224_37; }
	inline void set_DsaWithSha224_37(DerObjectIdentifier_t3495876513 * value)
	{
		___DsaWithSha224_37 = value;
		Il2CppCodeGenWriteBarrier(&___DsaWithSha224_37, value);
	}

	inline static int32_t get_offset_of_DsaWithSha256_38() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___DsaWithSha256_38)); }
	inline DerObjectIdentifier_t3495876513 * get_DsaWithSha256_38() const { return ___DsaWithSha256_38; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_DsaWithSha256_38() { return &___DsaWithSha256_38; }
	inline void set_DsaWithSha256_38(DerObjectIdentifier_t3495876513 * value)
	{
		___DsaWithSha256_38 = value;
		Il2CppCodeGenWriteBarrier(&___DsaWithSha256_38, value);
	}

	inline static int32_t get_offset_of_DsaWithSha384_39() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___DsaWithSha384_39)); }
	inline DerObjectIdentifier_t3495876513 * get_DsaWithSha384_39() const { return ___DsaWithSha384_39; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_DsaWithSha384_39() { return &___DsaWithSha384_39; }
	inline void set_DsaWithSha384_39(DerObjectIdentifier_t3495876513 * value)
	{
		___DsaWithSha384_39 = value;
		Il2CppCodeGenWriteBarrier(&___DsaWithSha384_39, value);
	}

	inline static int32_t get_offset_of_DsaWithSha512_40() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t1742042649_StaticFields, ___DsaWithSha512_40)); }
	inline DerObjectIdentifier_t3495876513 * get_DsaWithSha512_40() const { return ___DsaWithSha512_40; }
	inline DerObjectIdentifier_t3495876513 ** get_address_of_DsaWithSha512_40() { return &___DsaWithSha512_40; }
	inline void set_DsaWithSha512_40(DerObjectIdentifier_t3495876513 * value)
	{
		___DsaWithSha512_40 = value;
		Il2CppCodeGenWriteBarrier(&___DsaWithSha512_40, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
