﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Encoda3447851422.h"

// Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t1486532927;
// Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t140895757;
// Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_t626351532;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Asn1.X9.X9ECPoint
struct  X9ECPoint_t3031020555  : public Asn1Encodable_t3447851422
{
public:
	// Org.BouncyCastle.Asn1.Asn1OctetString Org.BouncyCastle.Asn1.X9.X9ECPoint::encoding
	Asn1OctetString_t1486532927 * ___encoding_2;
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Asn1.X9.X9ECPoint::c
	ECCurve_t140895757 * ___c_3;
	// Org.BouncyCastle.Math.EC.ECPoint Org.BouncyCastle.Asn1.X9.X9ECPoint::p
	ECPoint_t626351532 * ___p_4;

public:
	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(X9ECPoint_t3031020555, ___encoding_2)); }
	inline Asn1OctetString_t1486532927 * get_encoding_2() const { return ___encoding_2; }
	inline Asn1OctetString_t1486532927 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Asn1OctetString_t1486532927 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_2, value);
	}

	inline static int32_t get_offset_of_c_3() { return static_cast<int32_t>(offsetof(X9ECPoint_t3031020555, ___c_3)); }
	inline ECCurve_t140895757 * get_c_3() const { return ___c_3; }
	inline ECCurve_t140895757 ** get_address_of_c_3() { return &___c_3; }
	inline void set_c_3(ECCurve_t140895757 * value)
	{
		___c_3 = value;
		Il2CppCodeGenWriteBarrier(&___c_3, value);
	}

	inline static int32_t get_offset_of_p_4() { return static_cast<int32_t>(offsetof(X9ECPoint_t3031020555, ___p_4)); }
	inline ECPoint_t626351532 * get_p_4() const { return ___p_4; }
	inline ECPoint_t626351532 ** get_address_of_p_4() { return &___p_4; }
	inline void set_p_4(ECPoint_t626351532 * value)
	{
		___p_4 = value;
		Il2CppCodeGenWriteBarrier(&___p_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
