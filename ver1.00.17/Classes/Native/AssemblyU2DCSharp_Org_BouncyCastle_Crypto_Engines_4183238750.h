﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.SerpentEngineBase
struct  SerpentEngineBase_t4183238750  : public Il2CppObject
{
public:
	// System.Boolean Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::encrypting
	bool ___encrypting_3;
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::wKey
	Int32U5BU5D_t3030399641* ___wKey_4;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X0
	int32_t ___X0_5;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X1
	int32_t ___X1_6;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X2
	int32_t ___X2_7;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X3
	int32_t ___X3_8;

public:
	inline static int32_t get_offset_of_encrypting_3() { return static_cast<int32_t>(offsetof(SerpentEngineBase_t4183238750, ___encrypting_3)); }
	inline bool get_encrypting_3() const { return ___encrypting_3; }
	inline bool* get_address_of_encrypting_3() { return &___encrypting_3; }
	inline void set_encrypting_3(bool value)
	{
		___encrypting_3 = value;
	}

	inline static int32_t get_offset_of_wKey_4() { return static_cast<int32_t>(offsetof(SerpentEngineBase_t4183238750, ___wKey_4)); }
	inline Int32U5BU5D_t3030399641* get_wKey_4() const { return ___wKey_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_wKey_4() { return &___wKey_4; }
	inline void set_wKey_4(Int32U5BU5D_t3030399641* value)
	{
		___wKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___wKey_4, value);
	}

	inline static int32_t get_offset_of_X0_5() { return static_cast<int32_t>(offsetof(SerpentEngineBase_t4183238750, ___X0_5)); }
	inline int32_t get_X0_5() const { return ___X0_5; }
	inline int32_t* get_address_of_X0_5() { return &___X0_5; }
	inline void set_X0_5(int32_t value)
	{
		___X0_5 = value;
	}

	inline static int32_t get_offset_of_X1_6() { return static_cast<int32_t>(offsetof(SerpentEngineBase_t4183238750, ___X1_6)); }
	inline int32_t get_X1_6() const { return ___X1_6; }
	inline int32_t* get_address_of_X1_6() { return &___X1_6; }
	inline void set_X1_6(int32_t value)
	{
		___X1_6 = value;
	}

	inline static int32_t get_offset_of_X2_7() { return static_cast<int32_t>(offsetof(SerpentEngineBase_t4183238750, ___X2_7)); }
	inline int32_t get_X2_7() const { return ___X2_7; }
	inline int32_t* get_address_of_X2_7() { return &___X2_7; }
	inline void set_X2_7(int32_t value)
	{
		___X2_7 = value;
	}

	inline static int32_t get_offset_of_X3_8() { return static_cast<int32_t>(offsetof(SerpentEngineBase_t4183238750, ___X3_8)); }
	inline int32_t get_X3_8() const { return ___X3_8; }
	inline int32_t* get_address_of_X3_8() { return &___X3_8; }
	inline void set_X3_8(int32_t value)
	{
		___X3_8 = value;
	}
};

struct SerpentEngineBase_t4183238750_StaticFields
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::BlockSize
	int32_t ___BlockSize_0;

public:
	inline static int32_t get_offset_of_BlockSize_0() { return static_cast<int32_t>(offsetof(SerpentEngineBase_t4183238750_StaticFields, ___BlockSize_0)); }
	inline int32_t get_BlockSize_0() const { return ___BlockSize_0; }
	inline int32_t* get_address_of_BlockSize_0() { return &___BlockSize_0; }
	inline void set_BlockSize_0(int32_t value)
	{
		___BlockSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
