﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Asn1.Asn1Null
struct Asn1Null_t813671072;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// Org.BouncyCastle.Utilities.Collections.ISet
struct ISet_t4031211071;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Operators.X509Utilities
struct  X509Utilities_t1210696522  : public Il2CppObject
{
public:

public:
};

struct X509Utilities_t1210696522_StaticFields
{
public:
	// Org.BouncyCastle.Asn1.Asn1Null Org.BouncyCastle.Crypto.Operators.X509Utilities::derNull
	Asn1Null_t813671072 * ___derNull_0;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Operators.X509Utilities::algorithms
	Il2CppObject * ___algorithms_1;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Operators.X509Utilities::exParams
	Il2CppObject * ___exParams_2;
	// Org.BouncyCastle.Utilities.Collections.ISet Org.BouncyCastle.Crypto.Operators.X509Utilities::noParams
	Il2CppObject * ___noParams_3;

public:
	inline static int32_t get_offset_of_derNull_0() { return static_cast<int32_t>(offsetof(X509Utilities_t1210696522_StaticFields, ___derNull_0)); }
	inline Asn1Null_t813671072 * get_derNull_0() const { return ___derNull_0; }
	inline Asn1Null_t813671072 ** get_address_of_derNull_0() { return &___derNull_0; }
	inline void set_derNull_0(Asn1Null_t813671072 * value)
	{
		___derNull_0 = value;
		Il2CppCodeGenWriteBarrier(&___derNull_0, value);
	}

	inline static int32_t get_offset_of_algorithms_1() { return static_cast<int32_t>(offsetof(X509Utilities_t1210696522_StaticFields, ___algorithms_1)); }
	inline Il2CppObject * get_algorithms_1() const { return ___algorithms_1; }
	inline Il2CppObject ** get_address_of_algorithms_1() { return &___algorithms_1; }
	inline void set_algorithms_1(Il2CppObject * value)
	{
		___algorithms_1 = value;
		Il2CppCodeGenWriteBarrier(&___algorithms_1, value);
	}

	inline static int32_t get_offset_of_exParams_2() { return static_cast<int32_t>(offsetof(X509Utilities_t1210696522_StaticFields, ___exParams_2)); }
	inline Il2CppObject * get_exParams_2() const { return ___exParams_2; }
	inline Il2CppObject ** get_address_of_exParams_2() { return &___exParams_2; }
	inline void set_exParams_2(Il2CppObject * value)
	{
		___exParams_2 = value;
		Il2CppCodeGenWriteBarrier(&___exParams_2, value);
	}

	inline static int32_t get_offset_of_noParams_3() { return static_cast<int32_t>(offsetof(X509Utilities_t1210696522_StaticFields, ___noParams_3)); }
	inline Il2CppObject * get_noParams_3() const { return ___noParams_3; }
	inline Il2CppObject ** get_address_of_noParams_3() { return &___noParams_3; }
	inline void set_noParams_3(Il2CppObject * value)
	{
		___noParams_3 = value;
		Il2CppCodeGenWriteBarrier(&___noParams_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
