﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_t2670781410;
// System.String
struct String_t;
// Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t1663727050;
// Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t3117234712;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory
struct  Asn1SignatureFactory_t2866294513  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::algID
	AlgorithmIdentifier_t2670781410 * ___algID_0;
	// System.String Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::algorithm
	String_t* ___algorithm_1;
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::privateKey
	AsymmetricKeyParameter_t1663727050 * ___privateKey_2;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::random
	SecureRandom_t3117234712 * ___random_3;

public:
	inline static int32_t get_offset_of_algID_0() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t2866294513, ___algID_0)); }
	inline AlgorithmIdentifier_t2670781410 * get_algID_0() const { return ___algID_0; }
	inline AlgorithmIdentifier_t2670781410 ** get_address_of_algID_0() { return &___algID_0; }
	inline void set_algID_0(AlgorithmIdentifier_t2670781410 * value)
	{
		___algID_0 = value;
		Il2CppCodeGenWriteBarrier(&___algID_0, value);
	}

	inline static int32_t get_offset_of_algorithm_1() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t2866294513, ___algorithm_1)); }
	inline String_t* get_algorithm_1() const { return ___algorithm_1; }
	inline String_t** get_address_of_algorithm_1() { return &___algorithm_1; }
	inline void set_algorithm_1(String_t* value)
	{
		___algorithm_1 = value;
		Il2CppCodeGenWriteBarrier(&___algorithm_1, value);
	}

	inline static int32_t get_offset_of_privateKey_2() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t2866294513, ___privateKey_2)); }
	inline AsymmetricKeyParameter_t1663727050 * get_privateKey_2() const { return ___privateKey_2; }
	inline AsymmetricKeyParameter_t1663727050 ** get_address_of_privateKey_2() { return &___privateKey_2; }
	inline void set_privateKey_2(AsymmetricKeyParameter_t1663727050 * value)
	{
		___privateKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___privateKey_2, value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t2866294513, ___random_3)); }
	inline SecureRandom_t3117234712 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t3117234712 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t3117234712 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier(&___random_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
