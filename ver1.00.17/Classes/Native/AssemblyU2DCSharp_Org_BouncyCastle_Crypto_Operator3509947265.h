﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Crypto.ISigner
struct ISigner_t3640387509;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Operators.VerifierResult
struct  VerifierResult_t3509947265  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Crypto.ISigner Org.BouncyCastle.Crypto.Operators.VerifierResult::sig
	Il2CppObject * ___sig_0;

public:
	inline static int32_t get_offset_of_sig_0() { return static_cast<int32_t>(offsetof(VerifierResult_t3509947265, ___sig_0)); }
	inline Il2CppObject * get_sig_0() const { return ___sig_0; }
	inline Il2CppObject ** get_address_of_sig_0() { return &___sig_0; }
	inline void set_sig_0(Il2CppObject * value)
	{
		___sig_0 = value;
		Il2CppCodeGenWriteBarrier(&___sig_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
