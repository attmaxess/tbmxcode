﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// Org.BouncyCastle.Crypto.ISigner
struct ISigner_t3640387509;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Operators.SignerBucket
struct  SignerBucket_t3562239414  : public Stream_t3255436806
{
public:
	// Org.BouncyCastle.Crypto.ISigner Org.BouncyCastle.Crypto.Operators.SignerBucket::signer
	Il2CppObject * ___signer_1;

public:
	inline static int32_t get_offset_of_signer_1() { return static_cast<int32_t>(offsetof(SignerBucket_t3562239414, ___signer_1)); }
	inline Il2CppObject * get_signer_1() const { return ___signer_1; }
	inline Il2CppObject ** get_address_of_signer_1() { return &___signer_1; }
	inline void set_signer_1(Il2CppObject * value)
	{
		___signer_1 = value;
		Il2CppCodeGenWriteBarrier(&___signer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
