﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t1663727050;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactoryProvider
struct  Asn1VerifierFactoryProvider_t3967385528  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactoryProvider::publicKey
	AsymmetricKeyParameter_t1663727050 * ___publicKey_0;

public:
	inline static int32_t get_offset_of_publicKey_0() { return static_cast<int32_t>(offsetof(Asn1VerifierFactoryProvider_t3967385528, ___publicKey_0)); }
	inline AsymmetricKeyParameter_t1663727050 * get_publicKey_0() const { return ___publicKey_0; }
	inline AsymmetricKeyParameter_t1663727050 ** get_address_of_publicKey_0() { return &___publicKey_0; }
	inline void set_publicKey_0(AsymmetricKeyParameter_t1663727050 * value)
	{
		___publicKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___publicKey_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
