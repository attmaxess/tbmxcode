﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_t2670781410;
// Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t1663727050;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactory
struct  Asn1VerifierFactory_t549551221  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactory::algID
	AlgorithmIdentifier_t2670781410 * ___algID_0;
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactory::publicKey
	AsymmetricKeyParameter_t1663727050 * ___publicKey_1;

public:
	inline static int32_t get_offset_of_algID_0() { return static_cast<int32_t>(offsetof(Asn1VerifierFactory_t549551221, ___algID_0)); }
	inline AlgorithmIdentifier_t2670781410 * get_algID_0() const { return ___algID_0; }
	inline AlgorithmIdentifier_t2670781410 ** get_address_of_algID_0() { return &___algID_0; }
	inline void set_algID_0(AlgorithmIdentifier_t2670781410 * value)
	{
		___algID_0 = value;
		Il2CppCodeGenWriteBarrier(&___algID_0, value);
	}

	inline static int32_t get_offset_of_publicKey_1() { return static_cast<int32_t>(offsetof(Asn1VerifierFactory_t549551221, ___publicKey_1)); }
	inline AsymmetricKeyParameter_t1663727050 * get_publicKey_1() const { return ___publicKey_1; }
	inline AsymmetricKeyParameter_t1663727050 ** get_address_of_publicKey_1() { return &___publicKey_1; }
	inline void set_publicKey_1(AsymmetricKeyParameter_t1663727050 * value)
	{
		___publicKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___publicKey_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
