﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.IDictionary
struct IDictionary_t596158605;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.IsoTrailers
struct  IsoTrailers_t2065783217  : public Il2CppObject
{
public:

public:
};

struct IsoTrailers_t2065783217_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Signers.IsoTrailers::trailerMap
	Il2CppObject * ___trailerMap_11;

public:
	inline static int32_t get_offset_of_trailerMap_11() { return static_cast<int32_t>(offsetof(IsoTrailers_t2065783217_StaticFields, ___trailerMap_11)); }
	inline Il2CppObject * get_trailerMap_11() const { return ___trailerMap_11; }
	inline Il2CppObject ** get_address_of_trailerMap_11() { return &___trailerMap_11; }
	inline void set_trailerMap_11(Il2CppObject * value)
	{
		___trailerMap_11 = value;
		Il2CppCodeGenWriteBarrier(&___trailerMap_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
