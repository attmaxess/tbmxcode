﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Crypto.IDigest
struct IDigest_t1344033143;
// Org.BouncyCastle.Crypto.IAsymmetricBlockCipher
struct IAsymmetricBlockCipher_t1189117395;
// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct RsaKeyParameters_t3425534311;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.X931Signer
struct  X931Signer_t244497657  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Signers.X931Signer::digest
	Il2CppObject * ___digest_9;
	// Org.BouncyCastle.Crypto.IAsymmetricBlockCipher Org.BouncyCastle.Crypto.Signers.X931Signer::cipher
	Il2CppObject * ___cipher_10;
	// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters Org.BouncyCastle.Crypto.Signers.X931Signer::kParam
	RsaKeyParameters_t3425534311 * ___kParam_11;
	// System.Int32 Org.BouncyCastle.Crypto.Signers.X931Signer::trailer
	int32_t ___trailer_12;
	// System.Int32 Org.BouncyCastle.Crypto.Signers.X931Signer::keyBits
	int32_t ___keyBits_13;
	// System.Byte[] Org.BouncyCastle.Crypto.Signers.X931Signer::block
	ByteU5BU5D_t3397334013* ___block_14;

public:
	inline static int32_t get_offset_of_digest_9() { return static_cast<int32_t>(offsetof(X931Signer_t244497657, ___digest_9)); }
	inline Il2CppObject * get_digest_9() const { return ___digest_9; }
	inline Il2CppObject ** get_address_of_digest_9() { return &___digest_9; }
	inline void set_digest_9(Il2CppObject * value)
	{
		___digest_9 = value;
		Il2CppCodeGenWriteBarrier(&___digest_9, value);
	}

	inline static int32_t get_offset_of_cipher_10() { return static_cast<int32_t>(offsetof(X931Signer_t244497657, ___cipher_10)); }
	inline Il2CppObject * get_cipher_10() const { return ___cipher_10; }
	inline Il2CppObject ** get_address_of_cipher_10() { return &___cipher_10; }
	inline void set_cipher_10(Il2CppObject * value)
	{
		___cipher_10 = value;
		Il2CppCodeGenWriteBarrier(&___cipher_10, value);
	}

	inline static int32_t get_offset_of_kParam_11() { return static_cast<int32_t>(offsetof(X931Signer_t244497657, ___kParam_11)); }
	inline RsaKeyParameters_t3425534311 * get_kParam_11() const { return ___kParam_11; }
	inline RsaKeyParameters_t3425534311 ** get_address_of_kParam_11() { return &___kParam_11; }
	inline void set_kParam_11(RsaKeyParameters_t3425534311 * value)
	{
		___kParam_11 = value;
		Il2CppCodeGenWriteBarrier(&___kParam_11, value);
	}

	inline static int32_t get_offset_of_trailer_12() { return static_cast<int32_t>(offsetof(X931Signer_t244497657, ___trailer_12)); }
	inline int32_t get_trailer_12() const { return ___trailer_12; }
	inline int32_t* get_address_of_trailer_12() { return &___trailer_12; }
	inline void set_trailer_12(int32_t value)
	{
		___trailer_12 = value;
	}

	inline static int32_t get_offset_of_keyBits_13() { return static_cast<int32_t>(offsetof(X931Signer_t244497657, ___keyBits_13)); }
	inline int32_t get_keyBits_13() const { return ___keyBits_13; }
	inline int32_t* get_address_of_keyBits_13() { return &___keyBits_13; }
	inline void set_keyBits_13(int32_t value)
	{
		___keyBits_13 = value;
	}

	inline static int32_t get_offset_of_block_14() { return static_cast<int32_t>(offsetof(X931Signer_t244497657, ___block_14)); }
	inline ByteU5BU5D_t3397334013* get_block_14() const { return ___block_14; }
	inline ByteU5BU5D_t3397334013** get_address_of_block_14() { return &___block_14; }
	inline void set_block_14(ByteU5BU5D_t3397334013* value)
	{
		___block_14 = value;
		Il2CppCodeGenWriteBarrier(&___block_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
