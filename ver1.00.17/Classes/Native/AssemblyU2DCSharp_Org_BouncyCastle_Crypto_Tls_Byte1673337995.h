﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream3255436806.h"

// Org.BouncyCastle.Crypto.Tls.ByteQueue
struct ByteQueue_t1600245655;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ByteQueueStream
struct  ByteQueueStream_t1673337995  : public Stream_t3255436806
{
public:
	// Org.BouncyCastle.Crypto.Tls.ByteQueue Org.BouncyCastle.Crypto.Tls.ByteQueueStream::buffer
	ByteQueue_t1600245655 * ___buffer_1;

public:
	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(ByteQueueStream_t1673337995, ___buffer_1)); }
	inline ByteQueue_t1600245655 * get_buffer_1() const { return ___buffer_1; }
	inline ByteQueue_t1600245655 ** get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(ByteQueue_t1600245655 * value)
	{
		___buffer_1 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
