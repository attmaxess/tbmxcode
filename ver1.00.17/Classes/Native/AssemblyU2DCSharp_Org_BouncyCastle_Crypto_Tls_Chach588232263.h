﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Org.BouncyCastle.Crypto.Tls.TlsContext
struct TlsContext_t4077776538;
// Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine
struct ChaCha7539Engine_t118168352;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305
struct  Chacha20Poly1305_t588232263  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::context
	Il2CppObject * ___context_1;
	// Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::encryptCipher
	ChaCha7539Engine_t118168352 * ___encryptCipher_2;
	// Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::decryptCipher
	ChaCha7539Engine_t118168352 * ___decryptCipher_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::encryptIV
	ByteU5BU5D_t3397334013* ___encryptIV_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::decryptIV
	ByteU5BU5D_t3397334013* ___decryptIV_5;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t588232263, ___context_1)); }
	inline Il2CppObject * get_context_1() const { return ___context_1; }
	inline Il2CppObject ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(Il2CppObject * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier(&___context_1, value);
	}

	inline static int32_t get_offset_of_encryptCipher_2() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t588232263, ___encryptCipher_2)); }
	inline ChaCha7539Engine_t118168352 * get_encryptCipher_2() const { return ___encryptCipher_2; }
	inline ChaCha7539Engine_t118168352 ** get_address_of_encryptCipher_2() { return &___encryptCipher_2; }
	inline void set_encryptCipher_2(ChaCha7539Engine_t118168352 * value)
	{
		___encryptCipher_2 = value;
		Il2CppCodeGenWriteBarrier(&___encryptCipher_2, value);
	}

	inline static int32_t get_offset_of_decryptCipher_3() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t588232263, ___decryptCipher_3)); }
	inline ChaCha7539Engine_t118168352 * get_decryptCipher_3() const { return ___decryptCipher_3; }
	inline ChaCha7539Engine_t118168352 ** get_address_of_decryptCipher_3() { return &___decryptCipher_3; }
	inline void set_decryptCipher_3(ChaCha7539Engine_t118168352 * value)
	{
		___decryptCipher_3 = value;
		Il2CppCodeGenWriteBarrier(&___decryptCipher_3, value);
	}

	inline static int32_t get_offset_of_encryptIV_4() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t588232263, ___encryptIV_4)); }
	inline ByteU5BU5D_t3397334013* get_encryptIV_4() const { return ___encryptIV_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_encryptIV_4() { return &___encryptIV_4; }
	inline void set_encryptIV_4(ByteU5BU5D_t3397334013* value)
	{
		___encryptIV_4 = value;
		Il2CppCodeGenWriteBarrier(&___encryptIV_4, value);
	}

	inline static int32_t get_offset_of_decryptIV_5() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t588232263, ___decryptIV_5)); }
	inline ByteU5BU5D_t3397334013* get_decryptIV_5() const { return ___decryptIV_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_decryptIV_5() { return &___decryptIV_5; }
	inline void set_decryptIV_5(ByteU5BU5D_t3397334013* value)
	{
		___decryptIV_5 = value;
		Il2CppCodeGenWriteBarrier(&___decryptIV_5, value);
	}
};

struct Chacha20Poly1305_t588232263_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::Zeroes
	ByteU5BU5D_t3397334013* ___Zeroes_0;

public:
	inline static int32_t get_offset_of_Zeroes_0() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t588232263_StaticFields, ___Zeroes_0)); }
	inline ByteU5BU5D_t3397334013* get_Zeroes_0() const { return ___Zeroes_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_Zeroes_0() { return &___Zeroes_0; }
	inline void set_Zeroes_0(ByteU5BU5D_t3397334013* value)
	{
		___Zeroes_0 = value;
		Il2CppCodeGenWriteBarrier(&___Zeroes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
