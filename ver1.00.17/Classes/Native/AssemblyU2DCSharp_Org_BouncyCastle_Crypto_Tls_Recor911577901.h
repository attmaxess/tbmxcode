﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct TlsProtocol_t2348540693;
// System.IO.Stream
struct Stream_t3255436806;
// Org.BouncyCastle.Crypto.Tls.TlsCompression
struct TlsCompression_t3250792153;
// Org.BouncyCastle.Crypto.Tls.TlsCipher
struct TlsCipher_t927308168;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash
struct TlsHandshakeHash_t1728544356;
// Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct ProtocolVersion_t3273908466;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.RecordStream
struct  RecordStream_t911577901  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsProtocol Org.BouncyCastle.Crypto.Tls.RecordStream::mHandler
	TlsProtocol_t2348540693 * ___mHandler_5;
	// System.IO.Stream Org.BouncyCastle.Crypto.Tls.RecordStream::mInput
	Stream_t3255436806 * ___mInput_6;
	// System.IO.Stream Org.BouncyCastle.Crypto.Tls.RecordStream::mOutput
	Stream_t3255436806 * ___mOutput_7;
	// Org.BouncyCastle.Crypto.Tls.TlsCompression Org.BouncyCastle.Crypto.Tls.RecordStream::mPendingCompression
	Il2CppObject * ___mPendingCompression_8;
	// Org.BouncyCastle.Crypto.Tls.TlsCompression Org.BouncyCastle.Crypto.Tls.RecordStream::mReadCompression
	Il2CppObject * ___mReadCompression_9;
	// Org.BouncyCastle.Crypto.Tls.TlsCompression Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteCompression
	Il2CppObject * ___mWriteCompression_10;
	// Org.BouncyCastle.Crypto.Tls.TlsCipher Org.BouncyCastle.Crypto.Tls.RecordStream::mPendingCipher
	Il2CppObject * ___mPendingCipher_11;
	// Org.BouncyCastle.Crypto.Tls.TlsCipher Org.BouncyCastle.Crypto.Tls.RecordStream::mReadCipher
	Il2CppObject * ___mReadCipher_12;
	// Org.BouncyCastle.Crypto.Tls.TlsCipher Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteCipher
	Il2CppObject * ___mWriteCipher_13;
	// System.Int64 Org.BouncyCastle.Crypto.Tls.RecordStream::mReadSeqNo
	int64_t ___mReadSeqNo_14;
	// System.Int64 Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteSeqNo
	int64_t ___mWriteSeqNo_15;
	// System.IO.MemoryStream Org.BouncyCastle.Crypto.Tls.RecordStream::mBuffer
	MemoryStream_t743994179 * ___mBuffer_16;
	// Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash Org.BouncyCastle.Crypto.Tls.RecordStream::mHandshakeHash
	Il2CppObject * ___mHandshakeHash_17;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.RecordStream::mReadVersion
	ProtocolVersion_t3273908466 * ___mReadVersion_18;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteVersion
	ProtocolVersion_t3273908466 * ___mWriteVersion_19;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.RecordStream::mRestrictReadVersion
	bool ___mRestrictReadVersion_20;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.RecordStream::mPlaintextLimit
	int32_t ___mPlaintextLimit_21;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.RecordStream::mCompressedLimit
	int32_t ___mCompressedLimit_22;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.RecordStream::mCiphertextLimit
	int32_t ___mCiphertextLimit_23;

public:
	inline static int32_t get_offset_of_mHandler_5() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mHandler_5)); }
	inline TlsProtocol_t2348540693 * get_mHandler_5() const { return ___mHandler_5; }
	inline TlsProtocol_t2348540693 ** get_address_of_mHandler_5() { return &___mHandler_5; }
	inline void set_mHandler_5(TlsProtocol_t2348540693 * value)
	{
		___mHandler_5 = value;
		Il2CppCodeGenWriteBarrier(&___mHandler_5, value);
	}

	inline static int32_t get_offset_of_mInput_6() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mInput_6)); }
	inline Stream_t3255436806 * get_mInput_6() const { return ___mInput_6; }
	inline Stream_t3255436806 ** get_address_of_mInput_6() { return &___mInput_6; }
	inline void set_mInput_6(Stream_t3255436806 * value)
	{
		___mInput_6 = value;
		Il2CppCodeGenWriteBarrier(&___mInput_6, value);
	}

	inline static int32_t get_offset_of_mOutput_7() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mOutput_7)); }
	inline Stream_t3255436806 * get_mOutput_7() const { return ___mOutput_7; }
	inline Stream_t3255436806 ** get_address_of_mOutput_7() { return &___mOutput_7; }
	inline void set_mOutput_7(Stream_t3255436806 * value)
	{
		___mOutput_7 = value;
		Il2CppCodeGenWriteBarrier(&___mOutput_7, value);
	}

	inline static int32_t get_offset_of_mPendingCompression_8() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mPendingCompression_8)); }
	inline Il2CppObject * get_mPendingCompression_8() const { return ___mPendingCompression_8; }
	inline Il2CppObject ** get_address_of_mPendingCompression_8() { return &___mPendingCompression_8; }
	inline void set_mPendingCompression_8(Il2CppObject * value)
	{
		___mPendingCompression_8 = value;
		Il2CppCodeGenWriteBarrier(&___mPendingCompression_8, value);
	}

	inline static int32_t get_offset_of_mReadCompression_9() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mReadCompression_9)); }
	inline Il2CppObject * get_mReadCompression_9() const { return ___mReadCompression_9; }
	inline Il2CppObject ** get_address_of_mReadCompression_9() { return &___mReadCompression_9; }
	inline void set_mReadCompression_9(Il2CppObject * value)
	{
		___mReadCompression_9 = value;
		Il2CppCodeGenWriteBarrier(&___mReadCompression_9, value);
	}

	inline static int32_t get_offset_of_mWriteCompression_10() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mWriteCompression_10)); }
	inline Il2CppObject * get_mWriteCompression_10() const { return ___mWriteCompression_10; }
	inline Il2CppObject ** get_address_of_mWriteCompression_10() { return &___mWriteCompression_10; }
	inline void set_mWriteCompression_10(Il2CppObject * value)
	{
		___mWriteCompression_10 = value;
		Il2CppCodeGenWriteBarrier(&___mWriteCompression_10, value);
	}

	inline static int32_t get_offset_of_mPendingCipher_11() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mPendingCipher_11)); }
	inline Il2CppObject * get_mPendingCipher_11() const { return ___mPendingCipher_11; }
	inline Il2CppObject ** get_address_of_mPendingCipher_11() { return &___mPendingCipher_11; }
	inline void set_mPendingCipher_11(Il2CppObject * value)
	{
		___mPendingCipher_11 = value;
		Il2CppCodeGenWriteBarrier(&___mPendingCipher_11, value);
	}

	inline static int32_t get_offset_of_mReadCipher_12() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mReadCipher_12)); }
	inline Il2CppObject * get_mReadCipher_12() const { return ___mReadCipher_12; }
	inline Il2CppObject ** get_address_of_mReadCipher_12() { return &___mReadCipher_12; }
	inline void set_mReadCipher_12(Il2CppObject * value)
	{
		___mReadCipher_12 = value;
		Il2CppCodeGenWriteBarrier(&___mReadCipher_12, value);
	}

	inline static int32_t get_offset_of_mWriteCipher_13() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mWriteCipher_13)); }
	inline Il2CppObject * get_mWriteCipher_13() const { return ___mWriteCipher_13; }
	inline Il2CppObject ** get_address_of_mWriteCipher_13() { return &___mWriteCipher_13; }
	inline void set_mWriteCipher_13(Il2CppObject * value)
	{
		___mWriteCipher_13 = value;
		Il2CppCodeGenWriteBarrier(&___mWriteCipher_13, value);
	}

	inline static int32_t get_offset_of_mReadSeqNo_14() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mReadSeqNo_14)); }
	inline int64_t get_mReadSeqNo_14() const { return ___mReadSeqNo_14; }
	inline int64_t* get_address_of_mReadSeqNo_14() { return &___mReadSeqNo_14; }
	inline void set_mReadSeqNo_14(int64_t value)
	{
		___mReadSeqNo_14 = value;
	}

	inline static int32_t get_offset_of_mWriteSeqNo_15() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mWriteSeqNo_15)); }
	inline int64_t get_mWriteSeqNo_15() const { return ___mWriteSeqNo_15; }
	inline int64_t* get_address_of_mWriteSeqNo_15() { return &___mWriteSeqNo_15; }
	inline void set_mWriteSeqNo_15(int64_t value)
	{
		___mWriteSeqNo_15 = value;
	}

	inline static int32_t get_offset_of_mBuffer_16() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mBuffer_16)); }
	inline MemoryStream_t743994179 * get_mBuffer_16() const { return ___mBuffer_16; }
	inline MemoryStream_t743994179 ** get_address_of_mBuffer_16() { return &___mBuffer_16; }
	inline void set_mBuffer_16(MemoryStream_t743994179 * value)
	{
		___mBuffer_16 = value;
		Il2CppCodeGenWriteBarrier(&___mBuffer_16, value);
	}

	inline static int32_t get_offset_of_mHandshakeHash_17() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mHandshakeHash_17)); }
	inline Il2CppObject * get_mHandshakeHash_17() const { return ___mHandshakeHash_17; }
	inline Il2CppObject ** get_address_of_mHandshakeHash_17() { return &___mHandshakeHash_17; }
	inline void set_mHandshakeHash_17(Il2CppObject * value)
	{
		___mHandshakeHash_17 = value;
		Il2CppCodeGenWriteBarrier(&___mHandshakeHash_17, value);
	}

	inline static int32_t get_offset_of_mReadVersion_18() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mReadVersion_18)); }
	inline ProtocolVersion_t3273908466 * get_mReadVersion_18() const { return ___mReadVersion_18; }
	inline ProtocolVersion_t3273908466 ** get_address_of_mReadVersion_18() { return &___mReadVersion_18; }
	inline void set_mReadVersion_18(ProtocolVersion_t3273908466 * value)
	{
		___mReadVersion_18 = value;
		Il2CppCodeGenWriteBarrier(&___mReadVersion_18, value);
	}

	inline static int32_t get_offset_of_mWriteVersion_19() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mWriteVersion_19)); }
	inline ProtocolVersion_t3273908466 * get_mWriteVersion_19() const { return ___mWriteVersion_19; }
	inline ProtocolVersion_t3273908466 ** get_address_of_mWriteVersion_19() { return &___mWriteVersion_19; }
	inline void set_mWriteVersion_19(ProtocolVersion_t3273908466 * value)
	{
		___mWriteVersion_19 = value;
		Il2CppCodeGenWriteBarrier(&___mWriteVersion_19, value);
	}

	inline static int32_t get_offset_of_mRestrictReadVersion_20() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mRestrictReadVersion_20)); }
	inline bool get_mRestrictReadVersion_20() const { return ___mRestrictReadVersion_20; }
	inline bool* get_address_of_mRestrictReadVersion_20() { return &___mRestrictReadVersion_20; }
	inline void set_mRestrictReadVersion_20(bool value)
	{
		___mRestrictReadVersion_20 = value;
	}

	inline static int32_t get_offset_of_mPlaintextLimit_21() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mPlaintextLimit_21)); }
	inline int32_t get_mPlaintextLimit_21() const { return ___mPlaintextLimit_21; }
	inline int32_t* get_address_of_mPlaintextLimit_21() { return &___mPlaintextLimit_21; }
	inline void set_mPlaintextLimit_21(int32_t value)
	{
		___mPlaintextLimit_21 = value;
	}

	inline static int32_t get_offset_of_mCompressedLimit_22() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mCompressedLimit_22)); }
	inline int32_t get_mCompressedLimit_22() const { return ___mCompressedLimit_22; }
	inline int32_t* get_address_of_mCompressedLimit_22() { return &___mCompressedLimit_22; }
	inline void set_mCompressedLimit_22(int32_t value)
	{
		___mCompressedLimit_22 = value;
	}

	inline static int32_t get_offset_of_mCiphertextLimit_23() { return static_cast<int32_t>(offsetof(RecordStream_t911577901, ___mCiphertextLimit_23)); }
	inline int32_t get_mCiphertextLimit_23() const { return ___mCiphertextLimit_23; }
	inline int32_t* get_address_of_mCiphertextLimit_23() { return &___mCiphertextLimit_23; }
	inline void set_mCiphertextLimit_23(int32_t value)
	{
		___mCiphertextLimit_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
