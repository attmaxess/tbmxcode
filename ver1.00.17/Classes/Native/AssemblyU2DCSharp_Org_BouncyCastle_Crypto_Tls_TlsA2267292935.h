﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Crypto.Tls.TlsContext
struct TlsContext_t4077776538;
// Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher
struct IAeadBlockCipher_t2629578910;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsAeadCipher
struct  TlsAeadCipher_t2267292935  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::context
	Il2CppObject * ___context_2;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::macSize
	int32_t ___macSize_3;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::record_iv_length
	int32_t ___record_iv_length_4;
	// Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::encryptCipher
	Il2CppObject * ___encryptCipher_5;
	// Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::decryptCipher
	Il2CppObject * ___decryptCipher_6;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::encryptImplicitNonce
	ByteU5BU5D_t3397334013* ___encryptImplicitNonce_7;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::decryptImplicitNonce
	ByteU5BU5D_t3397334013* ___decryptImplicitNonce_8;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::nonceMode
	int32_t ___nonceMode_9;

public:
	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___context_2)); }
	inline Il2CppObject * get_context_2() const { return ___context_2; }
	inline Il2CppObject ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(Il2CppObject * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier(&___context_2, value);
	}

	inline static int32_t get_offset_of_macSize_3() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___macSize_3)); }
	inline int32_t get_macSize_3() const { return ___macSize_3; }
	inline int32_t* get_address_of_macSize_3() { return &___macSize_3; }
	inline void set_macSize_3(int32_t value)
	{
		___macSize_3 = value;
	}

	inline static int32_t get_offset_of_record_iv_length_4() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___record_iv_length_4)); }
	inline int32_t get_record_iv_length_4() const { return ___record_iv_length_4; }
	inline int32_t* get_address_of_record_iv_length_4() { return &___record_iv_length_4; }
	inline void set_record_iv_length_4(int32_t value)
	{
		___record_iv_length_4 = value;
	}

	inline static int32_t get_offset_of_encryptCipher_5() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___encryptCipher_5)); }
	inline Il2CppObject * get_encryptCipher_5() const { return ___encryptCipher_5; }
	inline Il2CppObject ** get_address_of_encryptCipher_5() { return &___encryptCipher_5; }
	inline void set_encryptCipher_5(Il2CppObject * value)
	{
		___encryptCipher_5 = value;
		Il2CppCodeGenWriteBarrier(&___encryptCipher_5, value);
	}

	inline static int32_t get_offset_of_decryptCipher_6() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___decryptCipher_6)); }
	inline Il2CppObject * get_decryptCipher_6() const { return ___decryptCipher_6; }
	inline Il2CppObject ** get_address_of_decryptCipher_6() { return &___decryptCipher_6; }
	inline void set_decryptCipher_6(Il2CppObject * value)
	{
		___decryptCipher_6 = value;
		Il2CppCodeGenWriteBarrier(&___decryptCipher_6, value);
	}

	inline static int32_t get_offset_of_encryptImplicitNonce_7() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___encryptImplicitNonce_7)); }
	inline ByteU5BU5D_t3397334013* get_encryptImplicitNonce_7() const { return ___encryptImplicitNonce_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_encryptImplicitNonce_7() { return &___encryptImplicitNonce_7; }
	inline void set_encryptImplicitNonce_7(ByteU5BU5D_t3397334013* value)
	{
		___encryptImplicitNonce_7 = value;
		Il2CppCodeGenWriteBarrier(&___encryptImplicitNonce_7, value);
	}

	inline static int32_t get_offset_of_decryptImplicitNonce_8() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___decryptImplicitNonce_8)); }
	inline ByteU5BU5D_t3397334013* get_decryptImplicitNonce_8() const { return ___decryptImplicitNonce_8; }
	inline ByteU5BU5D_t3397334013** get_address_of_decryptImplicitNonce_8() { return &___decryptImplicitNonce_8; }
	inline void set_decryptImplicitNonce_8(ByteU5BU5D_t3397334013* value)
	{
		___decryptImplicitNonce_8 = value;
		Il2CppCodeGenWriteBarrier(&___decryptImplicitNonce_8, value);
	}

	inline static int32_t get_offset_of_nonceMode_9() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2267292935, ___nonceMode_9)); }
	inline int32_t get_nonceMode_9() const { return ___nonceMode_9; }
	inline int32_t* get_address_of_nonceMode_9() { return &___nonceMode_9; }
	inline void set_nonceMode_9(int32_t value)
	{
		___nonceMode_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
