﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsP2348540693.h"

// Org.BouncyCastle.Crypto.Tls.TlsClient
struct TlsClient_t1962488424;
// Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl
struct TlsClientContextImpl_t2876508357;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Org.BouncyCastle.Crypto.Tls.TlsKeyExchange
struct TlsKeyExchange_t520409047;
// Org.BouncyCastle.Crypto.Tls.TlsAuthentication
struct TlsAuthentication_t69036015;
// Org.BouncyCastle.Crypto.Tls.CertificateStatus
struct CertificateStatus_t1829945713;
// Org.BouncyCastle.Crypto.Tls.CertificateRequest
struct CertificateRequest_t4188827490;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsClientProtocol
struct  TlsClientProtocol_t400461164  : public TlsProtocol_t2348540693
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsClient Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClient
	Il2CppObject * ___mTlsClient_50;
	// Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClientContext
	TlsClientContextImpl_t2876508357 * ___mTlsClientContext_51;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mSelectedSessionID
	ByteU5BU5D_t3397334013* ___mSelectedSessionID_52;
	// Org.BouncyCastle.Crypto.Tls.TlsKeyExchange Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mKeyExchange
	Il2CppObject * ___mKeyExchange_53;
	// Org.BouncyCastle.Crypto.Tls.TlsAuthentication Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mAuthentication
	Il2CppObject * ___mAuthentication_54;
	// Org.BouncyCastle.Crypto.Tls.CertificateStatus Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateStatus
	CertificateStatus_t1829945713 * ___mCertificateStatus_55;
	// Org.BouncyCastle.Crypto.Tls.CertificateRequest Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateRequest
	CertificateRequest_t4188827490 * ___mCertificateRequest_56;

public:
	inline static int32_t get_offset_of_mTlsClient_50() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t400461164, ___mTlsClient_50)); }
	inline Il2CppObject * get_mTlsClient_50() const { return ___mTlsClient_50; }
	inline Il2CppObject ** get_address_of_mTlsClient_50() { return &___mTlsClient_50; }
	inline void set_mTlsClient_50(Il2CppObject * value)
	{
		___mTlsClient_50 = value;
		Il2CppCodeGenWriteBarrier(&___mTlsClient_50, value);
	}

	inline static int32_t get_offset_of_mTlsClientContext_51() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t400461164, ___mTlsClientContext_51)); }
	inline TlsClientContextImpl_t2876508357 * get_mTlsClientContext_51() const { return ___mTlsClientContext_51; }
	inline TlsClientContextImpl_t2876508357 ** get_address_of_mTlsClientContext_51() { return &___mTlsClientContext_51; }
	inline void set_mTlsClientContext_51(TlsClientContextImpl_t2876508357 * value)
	{
		___mTlsClientContext_51 = value;
		Il2CppCodeGenWriteBarrier(&___mTlsClientContext_51, value);
	}

	inline static int32_t get_offset_of_mSelectedSessionID_52() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t400461164, ___mSelectedSessionID_52)); }
	inline ByteU5BU5D_t3397334013* get_mSelectedSessionID_52() const { return ___mSelectedSessionID_52; }
	inline ByteU5BU5D_t3397334013** get_address_of_mSelectedSessionID_52() { return &___mSelectedSessionID_52; }
	inline void set_mSelectedSessionID_52(ByteU5BU5D_t3397334013* value)
	{
		___mSelectedSessionID_52 = value;
		Il2CppCodeGenWriteBarrier(&___mSelectedSessionID_52, value);
	}

	inline static int32_t get_offset_of_mKeyExchange_53() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t400461164, ___mKeyExchange_53)); }
	inline Il2CppObject * get_mKeyExchange_53() const { return ___mKeyExchange_53; }
	inline Il2CppObject ** get_address_of_mKeyExchange_53() { return &___mKeyExchange_53; }
	inline void set_mKeyExchange_53(Il2CppObject * value)
	{
		___mKeyExchange_53 = value;
		Il2CppCodeGenWriteBarrier(&___mKeyExchange_53, value);
	}

	inline static int32_t get_offset_of_mAuthentication_54() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t400461164, ___mAuthentication_54)); }
	inline Il2CppObject * get_mAuthentication_54() const { return ___mAuthentication_54; }
	inline Il2CppObject ** get_address_of_mAuthentication_54() { return &___mAuthentication_54; }
	inline void set_mAuthentication_54(Il2CppObject * value)
	{
		___mAuthentication_54 = value;
		Il2CppCodeGenWriteBarrier(&___mAuthentication_54, value);
	}

	inline static int32_t get_offset_of_mCertificateStatus_55() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t400461164, ___mCertificateStatus_55)); }
	inline CertificateStatus_t1829945713 * get_mCertificateStatus_55() const { return ___mCertificateStatus_55; }
	inline CertificateStatus_t1829945713 ** get_address_of_mCertificateStatus_55() { return &___mCertificateStatus_55; }
	inline void set_mCertificateStatus_55(CertificateStatus_t1829945713 * value)
	{
		___mCertificateStatus_55 = value;
		Il2CppCodeGenWriteBarrier(&___mCertificateStatus_55, value);
	}

	inline static int32_t get_offset_of_mCertificateRequest_56() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t400461164, ___mCertificateRequest_56)); }
	inline CertificateRequest_t4188827490 * get_mCertificateRequest_56() const { return ___mCertificateRequest_56; }
	inline CertificateRequest_t4188827490 ** get_address_of_mCertificateRequest_56() { return &___mCertificateRequest_56; }
	inline void set_mCertificateRequest_56(CertificateRequest_t4188827490 * value)
	{
		___mCertificateRequest_56 = value;
		Il2CppCodeGenWriteBarrier(&___mCertificateRequest_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
