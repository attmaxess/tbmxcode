﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int322071877448.h"

// System.String
struct String_t;
// Org.BouncyCastle.Crypto.Tls.ByteQueue
struct ByteQueue_t1600245655;
// Org.BouncyCastle.Crypto.Tls.RecordStream
struct RecordStream_t911577901;
// Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t3117234712;
// Org.BouncyCastle.Crypto.Tls.TlsStream
struct TlsStream_t477156699;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Org.BouncyCastle.Crypto.Tls.TlsSession
struct TlsSession_t3695793821;
// Org.BouncyCastle.Crypto.Tls.SessionParameters
struct SessionParameters_t833892266;
// Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct SecurityParameters_t3985528004;
// Org.BouncyCastle.Crypto.Tls.Certificate
struct Certificate_t2775016569;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// Org.BouncyCastle.Crypto.Tls.ByteQueueStream
struct ByteQueueStream_t1673337995;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct  TlsProtocol_t2348540693  : public Il2CppObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.ByteQueue Org.BouncyCastle.Crypto.Tls.TlsProtocol::mApplicationDataQueue
	ByteQueue_t1600245655 * ___mApplicationDataQueue_21;
	// Org.BouncyCastle.Crypto.Tls.ByteQueue Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAlertQueue
	ByteQueue_t1600245655 * ___mAlertQueue_22;
	// Org.BouncyCastle.Crypto.Tls.ByteQueue Org.BouncyCastle.Crypto.Tls.TlsProtocol::mHandshakeQueue
	ByteQueue_t1600245655 * ___mHandshakeQueue_23;
	// Org.BouncyCastle.Crypto.Tls.RecordStream Org.BouncyCastle.Crypto.Tls.TlsProtocol::mRecordStream
	RecordStream_t911577901 * ___mRecordStream_24;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRandom
	SecureRandom_t3117234712 * ___mSecureRandom_25;
	// Org.BouncyCastle.Crypto.Tls.TlsStream Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsStream
	TlsStream_t477156699 * ___mTlsStream_26;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClosed
	bool ___mClosed_27;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mFailedWithError
	bool ___mFailedWithError_28;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataReady
	bool ___mAppDataReady_29;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataSplitEnabled
	bool ___mAppDataSplitEnabled_30;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataSplitMode
	int32_t ___mAppDataSplitMode_31;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectedVerifyData
	ByteU5BU5D_t3397334013* ___mExpectedVerifyData_32;
	// Org.BouncyCastle.Crypto.Tls.TlsSession Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsSession
	Il2CppObject * ___mTlsSession_33;
	// Org.BouncyCastle.Crypto.Tls.SessionParameters Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSessionParameters
	SessionParameters_t833892266 * ___mSessionParameters_34;
	// Org.BouncyCastle.Crypto.Tls.SecurityParameters Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecurityParameters
	SecurityParameters_t3985528004 * ___mSecurityParameters_35;
	// Org.BouncyCastle.Crypto.Tls.Certificate Org.BouncyCastle.Crypto.Tls.TlsProtocol::mPeerCertificate
	Certificate_t2775016569 * ___mPeerCertificate_36;
	// System.Int32[] Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCipherSuites
	Int32U5BU5D_t3030399641* ___mOfferedCipherSuites_37;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCompressionMethods
	ByteU5BU5D_t3397334013* ___mOfferedCompressionMethods_38;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClientExtensions
	Il2CppObject * ___mClientExtensions_39;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Tls.TlsProtocol::mServerExtensions
	Il2CppObject * ___mServerExtensions_40;
	// System.Int16 Org.BouncyCastle.Crypto.Tls.TlsProtocol::mConnectionState
	int16_t ___mConnectionState_41;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mResumedSession
	bool ___mResumedSession_42;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mReceivedChangeCipherSpec
	bool ___mReceivedChangeCipherSpec_43;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRenegotiation
	bool ___mSecureRenegotiation_44;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAllowCertificateStatus
	bool ___mAllowCertificateStatus_45;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectSessionTicket
	bool ___mExpectSessionTicket_46;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mBlocking
	bool ___mBlocking_47;
	// Org.BouncyCastle.Crypto.Tls.ByteQueueStream Org.BouncyCastle.Crypto.Tls.TlsProtocol::mInputBuffers
	ByteQueueStream_t1673337995 * ___mInputBuffers_48;
	// Org.BouncyCastle.Crypto.Tls.ByteQueueStream Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOutputBuffer
	ByteQueueStream_t1673337995 * ___mOutputBuffer_49;

public:
	inline static int32_t get_offset_of_mApplicationDataQueue_21() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mApplicationDataQueue_21)); }
	inline ByteQueue_t1600245655 * get_mApplicationDataQueue_21() const { return ___mApplicationDataQueue_21; }
	inline ByteQueue_t1600245655 ** get_address_of_mApplicationDataQueue_21() { return &___mApplicationDataQueue_21; }
	inline void set_mApplicationDataQueue_21(ByteQueue_t1600245655 * value)
	{
		___mApplicationDataQueue_21 = value;
		Il2CppCodeGenWriteBarrier(&___mApplicationDataQueue_21, value);
	}

	inline static int32_t get_offset_of_mAlertQueue_22() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mAlertQueue_22)); }
	inline ByteQueue_t1600245655 * get_mAlertQueue_22() const { return ___mAlertQueue_22; }
	inline ByteQueue_t1600245655 ** get_address_of_mAlertQueue_22() { return &___mAlertQueue_22; }
	inline void set_mAlertQueue_22(ByteQueue_t1600245655 * value)
	{
		___mAlertQueue_22 = value;
		Il2CppCodeGenWriteBarrier(&___mAlertQueue_22, value);
	}

	inline static int32_t get_offset_of_mHandshakeQueue_23() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mHandshakeQueue_23)); }
	inline ByteQueue_t1600245655 * get_mHandshakeQueue_23() const { return ___mHandshakeQueue_23; }
	inline ByteQueue_t1600245655 ** get_address_of_mHandshakeQueue_23() { return &___mHandshakeQueue_23; }
	inline void set_mHandshakeQueue_23(ByteQueue_t1600245655 * value)
	{
		___mHandshakeQueue_23 = value;
		Il2CppCodeGenWriteBarrier(&___mHandshakeQueue_23, value);
	}

	inline static int32_t get_offset_of_mRecordStream_24() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mRecordStream_24)); }
	inline RecordStream_t911577901 * get_mRecordStream_24() const { return ___mRecordStream_24; }
	inline RecordStream_t911577901 ** get_address_of_mRecordStream_24() { return &___mRecordStream_24; }
	inline void set_mRecordStream_24(RecordStream_t911577901 * value)
	{
		___mRecordStream_24 = value;
		Il2CppCodeGenWriteBarrier(&___mRecordStream_24, value);
	}

	inline static int32_t get_offset_of_mSecureRandom_25() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mSecureRandom_25)); }
	inline SecureRandom_t3117234712 * get_mSecureRandom_25() const { return ___mSecureRandom_25; }
	inline SecureRandom_t3117234712 ** get_address_of_mSecureRandom_25() { return &___mSecureRandom_25; }
	inline void set_mSecureRandom_25(SecureRandom_t3117234712 * value)
	{
		___mSecureRandom_25 = value;
		Il2CppCodeGenWriteBarrier(&___mSecureRandom_25, value);
	}

	inline static int32_t get_offset_of_mTlsStream_26() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mTlsStream_26)); }
	inline TlsStream_t477156699 * get_mTlsStream_26() const { return ___mTlsStream_26; }
	inline TlsStream_t477156699 ** get_address_of_mTlsStream_26() { return &___mTlsStream_26; }
	inline void set_mTlsStream_26(TlsStream_t477156699 * value)
	{
		___mTlsStream_26 = value;
		Il2CppCodeGenWriteBarrier(&___mTlsStream_26, value);
	}

	inline static int32_t get_offset_of_mClosed_27() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mClosed_27)); }
	inline bool get_mClosed_27() const { return ___mClosed_27; }
	inline bool* get_address_of_mClosed_27() { return &___mClosed_27; }
	inline void set_mClosed_27(bool value)
	{
		___mClosed_27 = value;
	}

	inline static int32_t get_offset_of_mFailedWithError_28() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mFailedWithError_28)); }
	inline bool get_mFailedWithError_28() const { return ___mFailedWithError_28; }
	inline bool* get_address_of_mFailedWithError_28() { return &___mFailedWithError_28; }
	inline void set_mFailedWithError_28(bool value)
	{
		___mFailedWithError_28 = value;
	}

	inline static int32_t get_offset_of_mAppDataReady_29() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mAppDataReady_29)); }
	inline bool get_mAppDataReady_29() const { return ___mAppDataReady_29; }
	inline bool* get_address_of_mAppDataReady_29() { return &___mAppDataReady_29; }
	inline void set_mAppDataReady_29(bool value)
	{
		___mAppDataReady_29 = value;
	}

	inline static int32_t get_offset_of_mAppDataSplitEnabled_30() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mAppDataSplitEnabled_30)); }
	inline bool get_mAppDataSplitEnabled_30() const { return ___mAppDataSplitEnabled_30; }
	inline bool* get_address_of_mAppDataSplitEnabled_30() { return &___mAppDataSplitEnabled_30; }
	inline void set_mAppDataSplitEnabled_30(bool value)
	{
		___mAppDataSplitEnabled_30 = value;
	}

	inline static int32_t get_offset_of_mAppDataSplitMode_31() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mAppDataSplitMode_31)); }
	inline int32_t get_mAppDataSplitMode_31() const { return ___mAppDataSplitMode_31; }
	inline int32_t* get_address_of_mAppDataSplitMode_31() { return &___mAppDataSplitMode_31; }
	inline void set_mAppDataSplitMode_31(int32_t value)
	{
		___mAppDataSplitMode_31 = value;
	}

	inline static int32_t get_offset_of_mExpectedVerifyData_32() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mExpectedVerifyData_32)); }
	inline ByteU5BU5D_t3397334013* get_mExpectedVerifyData_32() const { return ___mExpectedVerifyData_32; }
	inline ByteU5BU5D_t3397334013** get_address_of_mExpectedVerifyData_32() { return &___mExpectedVerifyData_32; }
	inline void set_mExpectedVerifyData_32(ByteU5BU5D_t3397334013* value)
	{
		___mExpectedVerifyData_32 = value;
		Il2CppCodeGenWriteBarrier(&___mExpectedVerifyData_32, value);
	}

	inline static int32_t get_offset_of_mTlsSession_33() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mTlsSession_33)); }
	inline Il2CppObject * get_mTlsSession_33() const { return ___mTlsSession_33; }
	inline Il2CppObject ** get_address_of_mTlsSession_33() { return &___mTlsSession_33; }
	inline void set_mTlsSession_33(Il2CppObject * value)
	{
		___mTlsSession_33 = value;
		Il2CppCodeGenWriteBarrier(&___mTlsSession_33, value);
	}

	inline static int32_t get_offset_of_mSessionParameters_34() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mSessionParameters_34)); }
	inline SessionParameters_t833892266 * get_mSessionParameters_34() const { return ___mSessionParameters_34; }
	inline SessionParameters_t833892266 ** get_address_of_mSessionParameters_34() { return &___mSessionParameters_34; }
	inline void set_mSessionParameters_34(SessionParameters_t833892266 * value)
	{
		___mSessionParameters_34 = value;
		Il2CppCodeGenWriteBarrier(&___mSessionParameters_34, value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_35() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mSecurityParameters_35)); }
	inline SecurityParameters_t3985528004 * get_mSecurityParameters_35() const { return ___mSecurityParameters_35; }
	inline SecurityParameters_t3985528004 ** get_address_of_mSecurityParameters_35() { return &___mSecurityParameters_35; }
	inline void set_mSecurityParameters_35(SecurityParameters_t3985528004 * value)
	{
		___mSecurityParameters_35 = value;
		Il2CppCodeGenWriteBarrier(&___mSecurityParameters_35, value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_36() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mPeerCertificate_36)); }
	inline Certificate_t2775016569 * get_mPeerCertificate_36() const { return ___mPeerCertificate_36; }
	inline Certificate_t2775016569 ** get_address_of_mPeerCertificate_36() { return &___mPeerCertificate_36; }
	inline void set_mPeerCertificate_36(Certificate_t2775016569 * value)
	{
		___mPeerCertificate_36 = value;
		Il2CppCodeGenWriteBarrier(&___mPeerCertificate_36, value);
	}

	inline static int32_t get_offset_of_mOfferedCipherSuites_37() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mOfferedCipherSuites_37)); }
	inline Int32U5BU5D_t3030399641* get_mOfferedCipherSuites_37() const { return ___mOfferedCipherSuites_37; }
	inline Int32U5BU5D_t3030399641** get_address_of_mOfferedCipherSuites_37() { return &___mOfferedCipherSuites_37; }
	inline void set_mOfferedCipherSuites_37(Int32U5BU5D_t3030399641* value)
	{
		___mOfferedCipherSuites_37 = value;
		Il2CppCodeGenWriteBarrier(&___mOfferedCipherSuites_37, value);
	}

	inline static int32_t get_offset_of_mOfferedCompressionMethods_38() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mOfferedCompressionMethods_38)); }
	inline ByteU5BU5D_t3397334013* get_mOfferedCompressionMethods_38() const { return ___mOfferedCompressionMethods_38; }
	inline ByteU5BU5D_t3397334013** get_address_of_mOfferedCompressionMethods_38() { return &___mOfferedCompressionMethods_38; }
	inline void set_mOfferedCompressionMethods_38(ByteU5BU5D_t3397334013* value)
	{
		___mOfferedCompressionMethods_38 = value;
		Il2CppCodeGenWriteBarrier(&___mOfferedCompressionMethods_38, value);
	}

	inline static int32_t get_offset_of_mClientExtensions_39() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mClientExtensions_39)); }
	inline Il2CppObject * get_mClientExtensions_39() const { return ___mClientExtensions_39; }
	inline Il2CppObject ** get_address_of_mClientExtensions_39() { return &___mClientExtensions_39; }
	inline void set_mClientExtensions_39(Il2CppObject * value)
	{
		___mClientExtensions_39 = value;
		Il2CppCodeGenWriteBarrier(&___mClientExtensions_39, value);
	}

	inline static int32_t get_offset_of_mServerExtensions_40() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mServerExtensions_40)); }
	inline Il2CppObject * get_mServerExtensions_40() const { return ___mServerExtensions_40; }
	inline Il2CppObject ** get_address_of_mServerExtensions_40() { return &___mServerExtensions_40; }
	inline void set_mServerExtensions_40(Il2CppObject * value)
	{
		___mServerExtensions_40 = value;
		Il2CppCodeGenWriteBarrier(&___mServerExtensions_40, value);
	}

	inline static int32_t get_offset_of_mConnectionState_41() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mConnectionState_41)); }
	inline int16_t get_mConnectionState_41() const { return ___mConnectionState_41; }
	inline int16_t* get_address_of_mConnectionState_41() { return &___mConnectionState_41; }
	inline void set_mConnectionState_41(int16_t value)
	{
		___mConnectionState_41 = value;
	}

	inline static int32_t get_offset_of_mResumedSession_42() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mResumedSession_42)); }
	inline bool get_mResumedSession_42() const { return ___mResumedSession_42; }
	inline bool* get_address_of_mResumedSession_42() { return &___mResumedSession_42; }
	inline void set_mResumedSession_42(bool value)
	{
		___mResumedSession_42 = value;
	}

	inline static int32_t get_offset_of_mReceivedChangeCipherSpec_43() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mReceivedChangeCipherSpec_43)); }
	inline bool get_mReceivedChangeCipherSpec_43() const { return ___mReceivedChangeCipherSpec_43; }
	inline bool* get_address_of_mReceivedChangeCipherSpec_43() { return &___mReceivedChangeCipherSpec_43; }
	inline void set_mReceivedChangeCipherSpec_43(bool value)
	{
		___mReceivedChangeCipherSpec_43 = value;
	}

	inline static int32_t get_offset_of_mSecureRenegotiation_44() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mSecureRenegotiation_44)); }
	inline bool get_mSecureRenegotiation_44() const { return ___mSecureRenegotiation_44; }
	inline bool* get_address_of_mSecureRenegotiation_44() { return &___mSecureRenegotiation_44; }
	inline void set_mSecureRenegotiation_44(bool value)
	{
		___mSecureRenegotiation_44 = value;
	}

	inline static int32_t get_offset_of_mAllowCertificateStatus_45() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mAllowCertificateStatus_45)); }
	inline bool get_mAllowCertificateStatus_45() const { return ___mAllowCertificateStatus_45; }
	inline bool* get_address_of_mAllowCertificateStatus_45() { return &___mAllowCertificateStatus_45; }
	inline void set_mAllowCertificateStatus_45(bool value)
	{
		___mAllowCertificateStatus_45 = value;
	}

	inline static int32_t get_offset_of_mExpectSessionTicket_46() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mExpectSessionTicket_46)); }
	inline bool get_mExpectSessionTicket_46() const { return ___mExpectSessionTicket_46; }
	inline bool* get_address_of_mExpectSessionTicket_46() { return &___mExpectSessionTicket_46; }
	inline void set_mExpectSessionTicket_46(bool value)
	{
		___mExpectSessionTicket_46 = value;
	}

	inline static int32_t get_offset_of_mBlocking_47() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mBlocking_47)); }
	inline bool get_mBlocking_47() const { return ___mBlocking_47; }
	inline bool* get_address_of_mBlocking_47() { return &___mBlocking_47; }
	inline void set_mBlocking_47(bool value)
	{
		___mBlocking_47 = value;
	}

	inline static int32_t get_offset_of_mInputBuffers_48() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mInputBuffers_48)); }
	inline ByteQueueStream_t1673337995 * get_mInputBuffers_48() const { return ___mInputBuffers_48; }
	inline ByteQueueStream_t1673337995 ** get_address_of_mInputBuffers_48() { return &___mInputBuffers_48; }
	inline void set_mInputBuffers_48(ByteQueueStream_t1673337995 * value)
	{
		___mInputBuffers_48 = value;
		Il2CppCodeGenWriteBarrier(&___mInputBuffers_48, value);
	}

	inline static int32_t get_offset_of_mOutputBuffer_49() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693, ___mOutputBuffer_49)); }
	inline ByteQueueStream_t1673337995 * get_mOutputBuffer_49() const { return ___mOutputBuffer_49; }
	inline ByteQueueStream_t1673337995 ** get_address_of_mOutputBuffer_49() { return &___mOutputBuffer_49; }
	inline void set_mOutputBuffer_49(ByteQueueStream_t1673337995 * value)
	{
		___mOutputBuffer_49 = value;
		Il2CppCodeGenWriteBarrier(&___mOutputBuffer_49, value);
	}
};

struct TlsProtocol_t2348540693_StaticFields
{
public:
	// System.String Org.BouncyCastle.Crypto.Tls.TlsProtocol::TLS_ERROR_MESSAGE
	String_t* ___TLS_ERROR_MESSAGE_0;

public:
	inline static int32_t get_offset_of_TLS_ERROR_MESSAGE_0() { return static_cast<int32_t>(offsetof(TlsProtocol_t2348540693_StaticFields, ___TLS_ERROR_MESSAGE_0)); }
	inline String_t* get_TLS_ERROR_MESSAGE_0() const { return ___TLS_ERROR_MESSAGE_0; }
	inline String_t** get_address_of_TLS_ERROR_MESSAGE_0() { return &___TLS_ERROR_MESSAGE_0; }
	inline void set_TLS_ERROR_MESSAGE_0(String_t* value)
	{
		___TLS_ERROR_MESSAGE_0 = value;
		Il2CppCodeGenWriteBarrier(&___TLS_ERROR_MESSAGE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
