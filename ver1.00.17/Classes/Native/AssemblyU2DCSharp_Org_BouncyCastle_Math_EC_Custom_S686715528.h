﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.UInt64[]
struct UInt64U5BU5D_t1668688775;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecT571Field
struct  SecT571Field_t686715528  : public Il2CppObject
{
public:

public:
};

struct SecT571Field_t686715528_StaticFields
{
public:
	// System.UInt64[] Org.BouncyCastle.Math.EC.Custom.Sec.SecT571Field::ROOT_Z
	UInt64U5BU5D_t1668688775* ___ROOT_Z_2;

public:
	inline static int32_t get_offset_of_ROOT_Z_2() { return static_cast<int32_t>(offsetof(SecT571Field_t686715528_StaticFields, ___ROOT_Z_2)); }
	inline UInt64U5BU5D_t1668688775* get_ROOT_Z_2() const { return ___ROOT_Z_2; }
	inline UInt64U5BU5D_t1668688775** get_address_of_ROOT_Z_2() { return &___ROOT_Z_2; }
	inline void set_ROOT_Z_2(UInt64U5BU5D_t1668688775* value)
	{
		___ROOT_Z_2 = value;
		Il2CppCodeGenWriteBarrier(&___ROOT_Z_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
