﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t3117234712;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Mod
struct  Mod_t3464319674  : public Il2CppObject
{
public:

public:
};

struct Mod_t3464319674_StaticFields
{
public:
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Math.Raw.Mod::RandomSource
	SecureRandom_t3117234712 * ___RandomSource_0;

public:
	inline static int32_t get_offset_of_RandomSource_0() { return static_cast<int32_t>(offsetof(Mod_t3464319674_StaticFields, ___RandomSource_0)); }
	inline SecureRandom_t3117234712 * get_RandomSource_0() const { return ___RandomSource_0; }
	inline SecureRandom_t3117234712 ** get_address_of_RandomSource_0() { return &___RandomSource_0; }
	inline void set_RandomSource_0(SecureRandom_t3117234712 * value)
	{
		___RandomSource_0 = value;
		Il2CppCodeGenWriteBarrier(&___RandomSource_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
