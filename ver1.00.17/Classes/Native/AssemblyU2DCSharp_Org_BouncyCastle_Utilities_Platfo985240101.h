﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Globalization.CompareInfo
struct CompareInfo_t2310920157;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Platform
struct  Platform_t985240101  : public Il2CppObject
{
public:

public:
};

struct Platform_t985240101_StaticFields
{
public:
	// System.Globalization.CompareInfo Org.BouncyCastle.Utilities.Platform::InvariantCompareInfo
	CompareInfo_t2310920157 * ___InvariantCompareInfo_0;
	// System.String Org.BouncyCastle.Utilities.Platform::NewLine
	String_t* ___NewLine_1;

public:
	inline static int32_t get_offset_of_InvariantCompareInfo_0() { return static_cast<int32_t>(offsetof(Platform_t985240101_StaticFields, ___InvariantCompareInfo_0)); }
	inline CompareInfo_t2310920157 * get_InvariantCompareInfo_0() const { return ___InvariantCompareInfo_0; }
	inline CompareInfo_t2310920157 ** get_address_of_InvariantCompareInfo_0() { return &___InvariantCompareInfo_0; }
	inline void set_InvariantCompareInfo_0(CompareInfo_t2310920157 * value)
	{
		___InvariantCompareInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___InvariantCompareInfo_0, value);
	}

	inline static int32_t get_offset_of_NewLine_1() { return static_cast<int32_t>(offsetof(Platform_t985240101_StaticFields, ___NewLine_1)); }
	inline String_t* get_NewLine_1() const { return ___NewLine_1; }
	inline String_t** get_address_of_NewLine_1() { return &___NewLine_1; }
	inline void set_NewLine_1(String_t* value)
	{
		___NewLine_1 = value;
		Il2CppCodeGenWriteBarrier(&___NewLine_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
