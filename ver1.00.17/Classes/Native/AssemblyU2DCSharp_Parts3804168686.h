﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// Parts/SaveMobilityJsonData
struct SaveMobilityJsonData_t2430892781;
// Parts/SaveModuleChildPartsData
struct SaveModuleChildPartsData_t3597262878;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Joint[]
struct JointU5BU5D_t171503857;
// UnityEngine.Light
struct Light_t494725636;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Parts
struct  Parts_t3804168686  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Parts::parentJoint
	Transform_t3275118058 * ___parentJoint_2;
	// UnityEngine.Transform Parts::actionPartsParentJoint
	Transform_t3275118058 * ___actionPartsParentJoint_3;
	// System.String Parts::mobilityName
	String_t* ___mobilityName_4;
	// System.Int32 Parts::PartsId
	int32_t ___PartsId_5;
	// System.Int32 Parts::ColorId
	int32_t ___ColorId_6;
	// System.String Parts::PartsName
	String_t* ___PartsName_7;
	// System.Int32 Parts::parentNo
	int32_t ___parentNo_8;
	// System.Int32 Parts::rollId
	int32_t ___rollId_9;
	// System.Int32 Parts::mobilityId
	int32_t ___mobilityId_10;
	// System.Int32 Parts::createdUserId
	int32_t ___createdUserId_11;
	// System.String Parts::nearmobObjectName
	String_t* ___nearmobObjectName_12;
	// System.Boolean Parts::nearMobool
	bool ___nearMobool_13;
	// Parts/SaveMobilityJsonData Parts::m_SaveMobilityJsonData
	SaveMobilityJsonData_t2430892781 * ___m_SaveMobilityJsonData_14;
	// Parts/SaveModuleChildPartsData Parts::m_SaveModuleChildPartsData
	SaveModuleChildPartsData_t3597262878 * ___m_SaveModuleChildPartsData_15;
	// System.Single[] Parts::additionalRotate
	SingleU5BU5D_t577127397* ___additionalRotate_16;
	// System.Single Parts::addRotateParameter
	float ___addRotateParameter_17;
	// System.Single Parts::pose1AddRotate
	float ___pose1AddRotate_18;
	// System.Single Parts::pose2AddRotate
	float ___pose2AddRotate_19;
	// System.Single Parts::pose3AddRotate
	float ___pose3AddRotate_20;
	// UnityEngine.ParticleSystem Parts::EnagyParticle
	ParticleSystem_t3394631041 * ___EnagyParticle_21;
	// UnityEngine.GameObject Parts::ChildObj
	GameObject_t1756533147 * ___ChildObj_22;
	// UnityEngine.Transform Parts::m_RollTrans
	Transform_t3275118058 * ___m_RollTrans_23;
	// Joint[] Parts::m_PartsJointList
	JointU5BU5D_t171503857* ___m_PartsJointList_24;
	// System.Int32 Parts::m_sParentConectJointNum
	int32_t ___m_sParentConectJointNum_25;
	// System.Single Parts::mobilityLevel
	float ___mobilityLevel_26;
	// System.Single Parts::EnagyValue
	float ___EnagyValue_27;
	// System.String Parts::mobilityCountryText
	String_t* ___mobilityCountryText_28;
	// System.String Parts::mobilityReactionText
	String_t* ___mobilityReactionText_29;
	// System.Boolean Parts::m_bIsConnect
	bool ___m_bIsConnect_30;
	// UnityEngine.Light Parts::m_PartsLight
	Light_t494725636 * ___m_PartsLight_31;
	// System.Boolean Parts::m_bLeapFixed
	bool ___m_bLeapFixed_32;
	// System.Single Parts::startRotate
	float ___startRotate_33;
	// System.Single Parts::endRotate
	float ___endRotate_34;
	// System.Int32 Parts::m_parentMobilmoId
	int32_t ___m_parentMobilmoId_35;

public:
	inline static int32_t get_offset_of_parentJoint_2() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___parentJoint_2)); }
	inline Transform_t3275118058 * get_parentJoint_2() const { return ___parentJoint_2; }
	inline Transform_t3275118058 ** get_address_of_parentJoint_2() { return &___parentJoint_2; }
	inline void set_parentJoint_2(Transform_t3275118058 * value)
	{
		___parentJoint_2 = value;
		Il2CppCodeGenWriteBarrier(&___parentJoint_2, value);
	}

	inline static int32_t get_offset_of_actionPartsParentJoint_3() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___actionPartsParentJoint_3)); }
	inline Transform_t3275118058 * get_actionPartsParentJoint_3() const { return ___actionPartsParentJoint_3; }
	inline Transform_t3275118058 ** get_address_of_actionPartsParentJoint_3() { return &___actionPartsParentJoint_3; }
	inline void set_actionPartsParentJoint_3(Transform_t3275118058 * value)
	{
		___actionPartsParentJoint_3 = value;
		Il2CppCodeGenWriteBarrier(&___actionPartsParentJoint_3, value);
	}

	inline static int32_t get_offset_of_mobilityName_4() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___mobilityName_4)); }
	inline String_t* get_mobilityName_4() const { return ___mobilityName_4; }
	inline String_t** get_address_of_mobilityName_4() { return &___mobilityName_4; }
	inline void set_mobilityName_4(String_t* value)
	{
		___mobilityName_4 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityName_4, value);
	}

	inline static int32_t get_offset_of_PartsId_5() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___PartsId_5)); }
	inline int32_t get_PartsId_5() const { return ___PartsId_5; }
	inline int32_t* get_address_of_PartsId_5() { return &___PartsId_5; }
	inline void set_PartsId_5(int32_t value)
	{
		___PartsId_5 = value;
	}

	inline static int32_t get_offset_of_ColorId_6() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___ColorId_6)); }
	inline int32_t get_ColorId_6() const { return ___ColorId_6; }
	inline int32_t* get_address_of_ColorId_6() { return &___ColorId_6; }
	inline void set_ColorId_6(int32_t value)
	{
		___ColorId_6 = value;
	}

	inline static int32_t get_offset_of_PartsName_7() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___PartsName_7)); }
	inline String_t* get_PartsName_7() const { return ___PartsName_7; }
	inline String_t** get_address_of_PartsName_7() { return &___PartsName_7; }
	inline void set_PartsName_7(String_t* value)
	{
		___PartsName_7 = value;
		Il2CppCodeGenWriteBarrier(&___PartsName_7, value);
	}

	inline static int32_t get_offset_of_parentNo_8() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___parentNo_8)); }
	inline int32_t get_parentNo_8() const { return ___parentNo_8; }
	inline int32_t* get_address_of_parentNo_8() { return &___parentNo_8; }
	inline void set_parentNo_8(int32_t value)
	{
		___parentNo_8 = value;
	}

	inline static int32_t get_offset_of_rollId_9() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___rollId_9)); }
	inline int32_t get_rollId_9() const { return ___rollId_9; }
	inline int32_t* get_address_of_rollId_9() { return &___rollId_9; }
	inline void set_rollId_9(int32_t value)
	{
		___rollId_9 = value;
	}

	inline static int32_t get_offset_of_mobilityId_10() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___mobilityId_10)); }
	inline int32_t get_mobilityId_10() const { return ___mobilityId_10; }
	inline int32_t* get_address_of_mobilityId_10() { return &___mobilityId_10; }
	inline void set_mobilityId_10(int32_t value)
	{
		___mobilityId_10 = value;
	}

	inline static int32_t get_offset_of_createdUserId_11() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___createdUserId_11)); }
	inline int32_t get_createdUserId_11() const { return ___createdUserId_11; }
	inline int32_t* get_address_of_createdUserId_11() { return &___createdUserId_11; }
	inline void set_createdUserId_11(int32_t value)
	{
		___createdUserId_11 = value;
	}

	inline static int32_t get_offset_of_nearmobObjectName_12() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___nearmobObjectName_12)); }
	inline String_t* get_nearmobObjectName_12() const { return ___nearmobObjectName_12; }
	inline String_t** get_address_of_nearmobObjectName_12() { return &___nearmobObjectName_12; }
	inline void set_nearmobObjectName_12(String_t* value)
	{
		___nearmobObjectName_12 = value;
		Il2CppCodeGenWriteBarrier(&___nearmobObjectName_12, value);
	}

	inline static int32_t get_offset_of_nearMobool_13() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___nearMobool_13)); }
	inline bool get_nearMobool_13() const { return ___nearMobool_13; }
	inline bool* get_address_of_nearMobool_13() { return &___nearMobool_13; }
	inline void set_nearMobool_13(bool value)
	{
		___nearMobool_13 = value;
	}

	inline static int32_t get_offset_of_m_SaveMobilityJsonData_14() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_SaveMobilityJsonData_14)); }
	inline SaveMobilityJsonData_t2430892781 * get_m_SaveMobilityJsonData_14() const { return ___m_SaveMobilityJsonData_14; }
	inline SaveMobilityJsonData_t2430892781 ** get_address_of_m_SaveMobilityJsonData_14() { return &___m_SaveMobilityJsonData_14; }
	inline void set_m_SaveMobilityJsonData_14(SaveMobilityJsonData_t2430892781 * value)
	{
		___m_SaveMobilityJsonData_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_SaveMobilityJsonData_14, value);
	}

	inline static int32_t get_offset_of_m_SaveModuleChildPartsData_15() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_SaveModuleChildPartsData_15)); }
	inline SaveModuleChildPartsData_t3597262878 * get_m_SaveModuleChildPartsData_15() const { return ___m_SaveModuleChildPartsData_15; }
	inline SaveModuleChildPartsData_t3597262878 ** get_address_of_m_SaveModuleChildPartsData_15() { return &___m_SaveModuleChildPartsData_15; }
	inline void set_m_SaveModuleChildPartsData_15(SaveModuleChildPartsData_t3597262878 * value)
	{
		___m_SaveModuleChildPartsData_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_SaveModuleChildPartsData_15, value);
	}

	inline static int32_t get_offset_of_additionalRotate_16() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___additionalRotate_16)); }
	inline SingleU5BU5D_t577127397* get_additionalRotate_16() const { return ___additionalRotate_16; }
	inline SingleU5BU5D_t577127397** get_address_of_additionalRotate_16() { return &___additionalRotate_16; }
	inline void set_additionalRotate_16(SingleU5BU5D_t577127397* value)
	{
		___additionalRotate_16 = value;
		Il2CppCodeGenWriteBarrier(&___additionalRotate_16, value);
	}

	inline static int32_t get_offset_of_addRotateParameter_17() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___addRotateParameter_17)); }
	inline float get_addRotateParameter_17() const { return ___addRotateParameter_17; }
	inline float* get_address_of_addRotateParameter_17() { return &___addRotateParameter_17; }
	inline void set_addRotateParameter_17(float value)
	{
		___addRotateParameter_17 = value;
	}

	inline static int32_t get_offset_of_pose1AddRotate_18() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___pose1AddRotate_18)); }
	inline float get_pose1AddRotate_18() const { return ___pose1AddRotate_18; }
	inline float* get_address_of_pose1AddRotate_18() { return &___pose1AddRotate_18; }
	inline void set_pose1AddRotate_18(float value)
	{
		___pose1AddRotate_18 = value;
	}

	inline static int32_t get_offset_of_pose2AddRotate_19() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___pose2AddRotate_19)); }
	inline float get_pose2AddRotate_19() const { return ___pose2AddRotate_19; }
	inline float* get_address_of_pose2AddRotate_19() { return &___pose2AddRotate_19; }
	inline void set_pose2AddRotate_19(float value)
	{
		___pose2AddRotate_19 = value;
	}

	inline static int32_t get_offset_of_pose3AddRotate_20() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___pose3AddRotate_20)); }
	inline float get_pose3AddRotate_20() const { return ___pose3AddRotate_20; }
	inline float* get_address_of_pose3AddRotate_20() { return &___pose3AddRotate_20; }
	inline void set_pose3AddRotate_20(float value)
	{
		___pose3AddRotate_20 = value;
	}

	inline static int32_t get_offset_of_EnagyParticle_21() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___EnagyParticle_21)); }
	inline ParticleSystem_t3394631041 * get_EnagyParticle_21() const { return ___EnagyParticle_21; }
	inline ParticleSystem_t3394631041 ** get_address_of_EnagyParticle_21() { return &___EnagyParticle_21; }
	inline void set_EnagyParticle_21(ParticleSystem_t3394631041 * value)
	{
		___EnagyParticle_21 = value;
		Il2CppCodeGenWriteBarrier(&___EnagyParticle_21, value);
	}

	inline static int32_t get_offset_of_ChildObj_22() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___ChildObj_22)); }
	inline GameObject_t1756533147 * get_ChildObj_22() const { return ___ChildObj_22; }
	inline GameObject_t1756533147 ** get_address_of_ChildObj_22() { return &___ChildObj_22; }
	inline void set_ChildObj_22(GameObject_t1756533147 * value)
	{
		___ChildObj_22 = value;
		Il2CppCodeGenWriteBarrier(&___ChildObj_22, value);
	}

	inline static int32_t get_offset_of_m_RollTrans_23() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_RollTrans_23)); }
	inline Transform_t3275118058 * get_m_RollTrans_23() const { return ___m_RollTrans_23; }
	inline Transform_t3275118058 ** get_address_of_m_RollTrans_23() { return &___m_RollTrans_23; }
	inline void set_m_RollTrans_23(Transform_t3275118058 * value)
	{
		___m_RollTrans_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_RollTrans_23, value);
	}

	inline static int32_t get_offset_of_m_PartsJointList_24() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_PartsJointList_24)); }
	inline JointU5BU5D_t171503857* get_m_PartsJointList_24() const { return ___m_PartsJointList_24; }
	inline JointU5BU5D_t171503857** get_address_of_m_PartsJointList_24() { return &___m_PartsJointList_24; }
	inline void set_m_PartsJointList_24(JointU5BU5D_t171503857* value)
	{
		___m_PartsJointList_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_PartsJointList_24, value);
	}

	inline static int32_t get_offset_of_m_sParentConectJointNum_25() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_sParentConectJointNum_25)); }
	inline int32_t get_m_sParentConectJointNum_25() const { return ___m_sParentConectJointNum_25; }
	inline int32_t* get_address_of_m_sParentConectJointNum_25() { return &___m_sParentConectJointNum_25; }
	inline void set_m_sParentConectJointNum_25(int32_t value)
	{
		___m_sParentConectJointNum_25 = value;
	}

	inline static int32_t get_offset_of_mobilityLevel_26() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___mobilityLevel_26)); }
	inline float get_mobilityLevel_26() const { return ___mobilityLevel_26; }
	inline float* get_address_of_mobilityLevel_26() { return &___mobilityLevel_26; }
	inline void set_mobilityLevel_26(float value)
	{
		___mobilityLevel_26 = value;
	}

	inline static int32_t get_offset_of_EnagyValue_27() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___EnagyValue_27)); }
	inline float get_EnagyValue_27() const { return ___EnagyValue_27; }
	inline float* get_address_of_EnagyValue_27() { return &___EnagyValue_27; }
	inline void set_EnagyValue_27(float value)
	{
		___EnagyValue_27 = value;
	}

	inline static int32_t get_offset_of_mobilityCountryText_28() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___mobilityCountryText_28)); }
	inline String_t* get_mobilityCountryText_28() const { return ___mobilityCountryText_28; }
	inline String_t** get_address_of_mobilityCountryText_28() { return &___mobilityCountryText_28; }
	inline void set_mobilityCountryText_28(String_t* value)
	{
		___mobilityCountryText_28 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityCountryText_28, value);
	}

	inline static int32_t get_offset_of_mobilityReactionText_29() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___mobilityReactionText_29)); }
	inline String_t* get_mobilityReactionText_29() const { return ___mobilityReactionText_29; }
	inline String_t** get_address_of_mobilityReactionText_29() { return &___mobilityReactionText_29; }
	inline void set_mobilityReactionText_29(String_t* value)
	{
		___mobilityReactionText_29 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityReactionText_29, value);
	}

	inline static int32_t get_offset_of_m_bIsConnect_30() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_bIsConnect_30)); }
	inline bool get_m_bIsConnect_30() const { return ___m_bIsConnect_30; }
	inline bool* get_address_of_m_bIsConnect_30() { return &___m_bIsConnect_30; }
	inline void set_m_bIsConnect_30(bool value)
	{
		___m_bIsConnect_30 = value;
	}

	inline static int32_t get_offset_of_m_PartsLight_31() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_PartsLight_31)); }
	inline Light_t494725636 * get_m_PartsLight_31() const { return ___m_PartsLight_31; }
	inline Light_t494725636 ** get_address_of_m_PartsLight_31() { return &___m_PartsLight_31; }
	inline void set_m_PartsLight_31(Light_t494725636 * value)
	{
		___m_PartsLight_31 = value;
		Il2CppCodeGenWriteBarrier(&___m_PartsLight_31, value);
	}

	inline static int32_t get_offset_of_m_bLeapFixed_32() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_bLeapFixed_32)); }
	inline bool get_m_bLeapFixed_32() const { return ___m_bLeapFixed_32; }
	inline bool* get_address_of_m_bLeapFixed_32() { return &___m_bLeapFixed_32; }
	inline void set_m_bLeapFixed_32(bool value)
	{
		___m_bLeapFixed_32 = value;
	}

	inline static int32_t get_offset_of_startRotate_33() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___startRotate_33)); }
	inline float get_startRotate_33() const { return ___startRotate_33; }
	inline float* get_address_of_startRotate_33() { return &___startRotate_33; }
	inline void set_startRotate_33(float value)
	{
		___startRotate_33 = value;
	}

	inline static int32_t get_offset_of_endRotate_34() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___endRotate_34)); }
	inline float get_endRotate_34() const { return ___endRotate_34; }
	inline float* get_address_of_endRotate_34() { return &___endRotate_34; }
	inline void set_endRotate_34(float value)
	{
		___endRotate_34 = value;
	}

	inline static int32_t get_offset_of_m_parentMobilmoId_35() { return static_cast<int32_t>(offsetof(Parts_t3804168686, ___m_parentMobilmoId_35)); }
	inline int32_t get_m_parentMobilmoId_35() const { return ___m_parentMobilmoId_35; }
	inline int32_t* get_address_of_m_parentMobilmoId_35() { return &___m_parentMobilmoId_35; }
	inline void set_m_parentMobilmoId_35(int32_t value)
	{
		___m_parentMobilmoId_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
