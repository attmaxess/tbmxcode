﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsCapture
struct  PartsCapture_t2562628060  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera PartsCapture::CaptureCamera
	Camera_t189460977 * ___CaptureCamera_2;
	// System.String PartsCapture::Name
	String_t* ___Name_3;
	// System.Int32 PartsCapture::Width
	int32_t ___Width_4;
	// System.Int32 PartsCapture::Height
	int32_t ___Height_5;
	// UnityEngine.GameObject PartsCapture::Captures
	GameObject_t1756533147 * ___Captures_6;

public:
	inline static int32_t get_offset_of_CaptureCamera_2() { return static_cast<int32_t>(offsetof(PartsCapture_t2562628060, ___CaptureCamera_2)); }
	inline Camera_t189460977 * get_CaptureCamera_2() const { return ___CaptureCamera_2; }
	inline Camera_t189460977 ** get_address_of_CaptureCamera_2() { return &___CaptureCamera_2; }
	inline void set_CaptureCamera_2(Camera_t189460977 * value)
	{
		___CaptureCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___CaptureCamera_2, value);
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(PartsCapture_t2562628060, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier(&___Name_3, value);
	}

	inline static int32_t get_offset_of_Width_4() { return static_cast<int32_t>(offsetof(PartsCapture_t2562628060, ___Width_4)); }
	inline int32_t get_Width_4() const { return ___Width_4; }
	inline int32_t* get_address_of_Width_4() { return &___Width_4; }
	inline void set_Width_4(int32_t value)
	{
		___Width_4 = value;
	}

	inline static int32_t get_offset_of_Height_5() { return static_cast<int32_t>(offsetof(PartsCapture_t2562628060, ___Height_5)); }
	inline int32_t get_Height_5() const { return ___Height_5; }
	inline int32_t* get_address_of_Height_5() { return &___Height_5; }
	inline void set_Height_5(int32_t value)
	{
		___Height_5 = value;
	}

	inline static int32_t get_offset_of_Captures_6() { return static_cast<int32_t>(offsetof(PartsCapture_t2562628060, ___Captures_6)); }
	inline GameObject_t1756533147 * get_Captures_6() const { return ___Captures_6; }
	inline GameObject_t1756533147 ** get_address_of_Captures_6() { return &___Captures_6; }
	inline void set_Captures_6(GameObject_t1756533147 * value)
	{
		___Captures_6 = value;
		Il2CppCodeGenWriteBarrier(&___Captures_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
