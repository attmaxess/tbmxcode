﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// PartsCapture
struct PartsCapture_t2562628060;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsCapture/<Capture>c__Iterator1
struct  U3CCaptureU3Ec__Iterator1_t3002243375  : public Il2CppObject
{
public:
	// UnityEngine.RenderTexture PartsCapture/<Capture>c__Iterator1::<rt>__0
	RenderTexture_t2666733923 * ___U3CrtU3E__0_0;
	// UnityEngine.Texture2D PartsCapture/<Capture>c__Iterator1::<tex>__0
	Texture2D_t3542995729 * ___U3CtexU3E__0_1;
	// System.Byte[] PartsCapture/<Capture>c__Iterator1::<bytes>__0
	ByteU5BU5D_t3397334013* ___U3CbytesU3E__0_2;
	// System.String PartsCapture/<Capture>c__Iterator1::<dirPath>__0
	String_t* ___U3CdirPathU3E__0_3;
	// System.String PartsCapture/<Capture>c__Iterator1::name
	String_t* ___name_4;
	// System.String PartsCapture/<Capture>c__Iterator1::<savePath>__0
	String_t* ___U3CsavePathU3E__0_5;
	// PartsCapture PartsCapture/<Capture>c__Iterator1::$this
	PartsCapture_t2562628060 * ___U24this_6;
	// System.Object PartsCapture/<Capture>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean PartsCapture/<Capture>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 PartsCapture/<Capture>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CrtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U3CrtU3E__0_0)); }
	inline RenderTexture_t2666733923 * get_U3CrtU3E__0_0() const { return ___U3CrtU3E__0_0; }
	inline RenderTexture_t2666733923 ** get_address_of_U3CrtU3E__0_0() { return &___U3CrtU3E__0_0; }
	inline void set_U3CrtU3E__0_0(RenderTexture_t2666733923 * value)
	{
		___U3CrtU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrtU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U3CtexU3E__0_1)); }
	inline Texture2D_t3542995729 * get_U3CtexU3E__0_1() const { return ___U3CtexU3E__0_1; }
	inline Texture2D_t3542995729 ** get_address_of_U3CtexU3E__0_1() { return &___U3CtexU3E__0_1; }
	inline void set_U3CtexU3E__0_1(Texture2D_t3542995729 * value)
	{
		___U3CtexU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CbytesU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U3CbytesU3E__0_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CbytesU3E__0_2() const { return ___U3CbytesU3E__0_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CbytesU3E__0_2() { return &___U3CbytesU3E__0_2; }
	inline void set_U3CbytesU3E__0_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CbytesU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CdirPathU3E__0_3() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U3CdirPathU3E__0_3)); }
	inline String_t* get_U3CdirPathU3E__0_3() const { return ___U3CdirPathU3E__0_3; }
	inline String_t** get_address_of_U3CdirPathU3E__0_3() { return &___U3CdirPathU3E__0_3; }
	inline void set_U3CdirPathU3E__0_3(String_t* value)
	{
		___U3CdirPathU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdirPathU3E__0_3, value);
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier(&___name_4, value);
	}

	inline static int32_t get_offset_of_U3CsavePathU3E__0_5() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U3CsavePathU3E__0_5)); }
	inline String_t* get_U3CsavePathU3E__0_5() const { return ___U3CsavePathU3E__0_5; }
	inline String_t** get_address_of_U3CsavePathU3E__0_5() { return &___U3CsavePathU3E__0_5; }
	inline void set_U3CsavePathU3E__0_5(String_t* value)
	{
		___U3CsavePathU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsavePathU3E__0_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U24this_6)); }
	inline PartsCapture_t2562628060 * get_U24this_6() const { return ___U24this_6; }
	inline PartsCapture_t2562628060 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(PartsCapture_t2562628060 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CCaptureU3Ec__Iterator1_t3002243375, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
