﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,UnityEngine.GameObject>
struct Dictionary_2_t791911295;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsConfig
struct  PartsConfig_t872073448  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PartsConfig::ParentToPartsConfigList
	GameObject_t1756533147 * ___ParentToPartsConfigList_2;
	// UnityEngine.GameObject PartsConfig::ConfigPartsPrefab
	GameObject_t1756533147 * ___ConfigPartsPrefab_3;
	// System.Boolean PartsConfig::m_bOffsetStart
	bool ___m_bOffsetStart_4;
	// System.Int32 PartsConfig::m_sItemOffset
	int32_t ___m_sItemOffset_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,UnityEngine.GameObject> PartsConfig::m_pConfigPartsMap
	Dictionary_2_t791911295 * ___m_pConfigPartsMap_6;
	// UnityEngine.CanvasGroup PartsConfig::m_CanvasGroup
	CanvasGroup_t3296560743 * ___m_CanvasGroup_7;

public:
	inline static int32_t get_offset_of_ParentToPartsConfigList_2() { return static_cast<int32_t>(offsetof(PartsConfig_t872073448, ___ParentToPartsConfigList_2)); }
	inline GameObject_t1756533147 * get_ParentToPartsConfigList_2() const { return ___ParentToPartsConfigList_2; }
	inline GameObject_t1756533147 ** get_address_of_ParentToPartsConfigList_2() { return &___ParentToPartsConfigList_2; }
	inline void set_ParentToPartsConfigList_2(GameObject_t1756533147 * value)
	{
		___ParentToPartsConfigList_2 = value;
		Il2CppCodeGenWriteBarrier(&___ParentToPartsConfigList_2, value);
	}

	inline static int32_t get_offset_of_ConfigPartsPrefab_3() { return static_cast<int32_t>(offsetof(PartsConfig_t872073448, ___ConfigPartsPrefab_3)); }
	inline GameObject_t1756533147 * get_ConfigPartsPrefab_3() const { return ___ConfigPartsPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_ConfigPartsPrefab_3() { return &___ConfigPartsPrefab_3; }
	inline void set_ConfigPartsPrefab_3(GameObject_t1756533147 * value)
	{
		___ConfigPartsPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigPartsPrefab_3, value);
	}

	inline static int32_t get_offset_of_m_bOffsetStart_4() { return static_cast<int32_t>(offsetof(PartsConfig_t872073448, ___m_bOffsetStart_4)); }
	inline bool get_m_bOffsetStart_4() const { return ___m_bOffsetStart_4; }
	inline bool* get_address_of_m_bOffsetStart_4() { return &___m_bOffsetStart_4; }
	inline void set_m_bOffsetStart_4(bool value)
	{
		___m_bOffsetStart_4 = value;
	}

	inline static int32_t get_offset_of_m_sItemOffset_5() { return static_cast<int32_t>(offsetof(PartsConfig_t872073448, ___m_sItemOffset_5)); }
	inline int32_t get_m_sItemOffset_5() const { return ___m_sItemOffset_5; }
	inline int32_t* get_address_of_m_sItemOffset_5() { return &___m_sItemOffset_5; }
	inline void set_m_sItemOffset_5(int32_t value)
	{
		___m_sItemOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_pConfigPartsMap_6() { return static_cast<int32_t>(offsetof(PartsConfig_t872073448, ___m_pConfigPartsMap_6)); }
	inline Dictionary_2_t791911295 * get_m_pConfigPartsMap_6() const { return ___m_pConfigPartsMap_6; }
	inline Dictionary_2_t791911295 ** get_address_of_m_pConfigPartsMap_6() { return &___m_pConfigPartsMap_6; }
	inline void set_m_pConfigPartsMap_6(Dictionary_2_t791911295 * value)
	{
		___m_pConfigPartsMap_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_pConfigPartsMap_6, value);
	}

	inline static int32_t get_offset_of_m_CanvasGroup_7() { return static_cast<int32_t>(offsetof(PartsConfig_t872073448, ___m_CanvasGroup_7)); }
	inline CanvasGroup_t3296560743 * get_m_CanvasGroup_7() const { return ___m_CanvasGroup_7; }
	inline CanvasGroup_t3296560743 ** get_address_of_m_CanvasGroup_7() { return &___m_CanvasGroup_7; }
	inline void set_m_CanvasGroup_7(CanvasGroup_t3296560743 * value)
	{
		___m_CanvasGroup_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_CanvasGroup_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
