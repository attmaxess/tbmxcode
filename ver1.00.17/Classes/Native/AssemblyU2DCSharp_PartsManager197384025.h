﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3189027656.h"

// System.Collections.Generic.List`1<PremitivePartContent>
struct List_1_t3065337161;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsManager
struct  PartsManager_t197384025  : public SingletonMonoBehaviour_1_t3189027656
{
public:
	// System.Collections.Generic.List`1<PremitivePartContent> PartsManager::partsList
	List_1_t3065337161 * ___partsList_3;

public:
	inline static int32_t get_offset_of_partsList_3() { return static_cast<int32_t>(offsetof(PartsManager_t197384025, ___partsList_3)); }
	inline List_1_t3065337161 * get_partsList_3() const { return ___partsList_3; }
	inline List_1_t3065337161 ** get_address_of_partsList_3() { return &___partsList_3; }
	inline void set_partsList_3(List_1_t3065337161 * value)
	{
		___partsList_3 = value;
		Il2CppCodeGenWriteBarrier(&___partsList_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
