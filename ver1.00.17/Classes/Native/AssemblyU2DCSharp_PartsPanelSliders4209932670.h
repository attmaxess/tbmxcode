﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// SlideInOut[]
struct SlideInOutU5BU5D_t2120079043;
// SlideInOut
struct SlideInOut_t958296806;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsPanelSliders
struct  PartsPanelSliders_t4209932670  : public MonoBehaviour_t1158329972
{
public:
	// SlideInOut[] PartsPanelSliders::PartsConfigs
	SlideInOutU5BU5D_t2120079043* ___PartsConfigs_2;
	// SlideInOut[] PartsPanelSliders::PartsPanel
	SlideInOutU5BU5D_t2120079043* ___PartsPanel_3;
	// SlideInOut PartsPanelSliders::ActionPartsPanel
	SlideInOut_t958296806 * ___ActionPartsPanel_4;
	// UnityEngine.GameObject[] PartsPanelSliders::PartsBarButton
	GameObjectU5BU5D_t3057952154* ___PartsBarButton_5;
	// UnityEngine.GameObject[] PartsPanelSliders::ConfigButton
	GameObjectU5BU5D_t3057952154* ___ConfigButton_6;
	// SlideInOut[] PartsPanelSliders::ConfigPanel
	SlideInOutU5BU5D_t2120079043* ___ConfigPanel_7;
	// UnityEngine.GameObject[] PartsPanelSliders::BackCoreSelectButton
	GameObjectU5BU5D_t3057952154* ___BackCoreSelectButton_8;
	// UnityEngine.GameObject[] PartsPanelSliders::BackButton
	GameObjectU5BU5D_t3057952154* ___BackButton_9;
	// UnityEngine.GameObject[] PartsPanelSliders::CompleteButton
	GameObjectU5BU5D_t3057952154* ___CompleteButton_10;
	// SlideInOut PartsPanelSliders::PartsViewPanel
	SlideInOut_t958296806 * ___PartsViewPanel_11;
	// System.Boolean PartsPanelSliders::IsSubWindow
	bool ___IsSubWindow_12;

public:
	inline static int32_t get_offset_of_PartsConfigs_2() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___PartsConfigs_2)); }
	inline SlideInOutU5BU5D_t2120079043* get_PartsConfigs_2() const { return ___PartsConfigs_2; }
	inline SlideInOutU5BU5D_t2120079043** get_address_of_PartsConfigs_2() { return &___PartsConfigs_2; }
	inline void set_PartsConfigs_2(SlideInOutU5BU5D_t2120079043* value)
	{
		___PartsConfigs_2 = value;
		Il2CppCodeGenWriteBarrier(&___PartsConfigs_2, value);
	}

	inline static int32_t get_offset_of_PartsPanel_3() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___PartsPanel_3)); }
	inline SlideInOutU5BU5D_t2120079043* get_PartsPanel_3() const { return ___PartsPanel_3; }
	inline SlideInOutU5BU5D_t2120079043** get_address_of_PartsPanel_3() { return &___PartsPanel_3; }
	inline void set_PartsPanel_3(SlideInOutU5BU5D_t2120079043* value)
	{
		___PartsPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___PartsPanel_3, value);
	}

	inline static int32_t get_offset_of_ActionPartsPanel_4() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___ActionPartsPanel_4)); }
	inline SlideInOut_t958296806 * get_ActionPartsPanel_4() const { return ___ActionPartsPanel_4; }
	inline SlideInOut_t958296806 ** get_address_of_ActionPartsPanel_4() { return &___ActionPartsPanel_4; }
	inline void set_ActionPartsPanel_4(SlideInOut_t958296806 * value)
	{
		___ActionPartsPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPartsPanel_4, value);
	}

	inline static int32_t get_offset_of_PartsBarButton_5() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___PartsBarButton_5)); }
	inline GameObjectU5BU5D_t3057952154* get_PartsBarButton_5() const { return ___PartsBarButton_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PartsBarButton_5() { return &___PartsBarButton_5; }
	inline void set_PartsBarButton_5(GameObjectU5BU5D_t3057952154* value)
	{
		___PartsBarButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___PartsBarButton_5, value);
	}

	inline static int32_t get_offset_of_ConfigButton_6() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___ConfigButton_6)); }
	inline GameObjectU5BU5D_t3057952154* get_ConfigButton_6() const { return ___ConfigButton_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ConfigButton_6() { return &___ConfigButton_6; }
	inline void set_ConfigButton_6(GameObjectU5BU5D_t3057952154* value)
	{
		___ConfigButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigButton_6, value);
	}

	inline static int32_t get_offset_of_ConfigPanel_7() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___ConfigPanel_7)); }
	inline SlideInOutU5BU5D_t2120079043* get_ConfigPanel_7() const { return ___ConfigPanel_7; }
	inline SlideInOutU5BU5D_t2120079043** get_address_of_ConfigPanel_7() { return &___ConfigPanel_7; }
	inline void set_ConfigPanel_7(SlideInOutU5BU5D_t2120079043* value)
	{
		___ConfigPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigPanel_7, value);
	}

	inline static int32_t get_offset_of_BackCoreSelectButton_8() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___BackCoreSelectButton_8)); }
	inline GameObjectU5BU5D_t3057952154* get_BackCoreSelectButton_8() const { return ___BackCoreSelectButton_8; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_BackCoreSelectButton_8() { return &___BackCoreSelectButton_8; }
	inline void set_BackCoreSelectButton_8(GameObjectU5BU5D_t3057952154* value)
	{
		___BackCoreSelectButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___BackCoreSelectButton_8, value);
	}

	inline static int32_t get_offset_of_BackButton_9() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___BackButton_9)); }
	inline GameObjectU5BU5D_t3057952154* get_BackButton_9() const { return ___BackButton_9; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_BackButton_9() { return &___BackButton_9; }
	inline void set_BackButton_9(GameObjectU5BU5D_t3057952154* value)
	{
		___BackButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___BackButton_9, value);
	}

	inline static int32_t get_offset_of_CompleteButton_10() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___CompleteButton_10)); }
	inline GameObjectU5BU5D_t3057952154* get_CompleteButton_10() const { return ___CompleteButton_10; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_CompleteButton_10() { return &___CompleteButton_10; }
	inline void set_CompleteButton_10(GameObjectU5BU5D_t3057952154* value)
	{
		___CompleteButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___CompleteButton_10, value);
	}

	inline static int32_t get_offset_of_PartsViewPanel_11() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___PartsViewPanel_11)); }
	inline SlideInOut_t958296806 * get_PartsViewPanel_11() const { return ___PartsViewPanel_11; }
	inline SlideInOut_t958296806 ** get_address_of_PartsViewPanel_11() { return &___PartsViewPanel_11; }
	inline void set_PartsViewPanel_11(SlideInOut_t958296806 * value)
	{
		___PartsViewPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___PartsViewPanel_11, value);
	}

	inline static int32_t get_offset_of_IsSubWindow_12() { return static_cast<int32_t>(offsetof(PartsPanelSliders_t4209932670, ___IsSubWindow_12)); }
	inline bool get_IsSubWindow_12() const { return ___IsSubWindow_12; }
	inline bool* get_address_of_IsSubWindow_12() { return &___IsSubWindow_12; }
	inline void set_IsSubWindow_12(bool value)
	{
		___IsSubWindow_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
