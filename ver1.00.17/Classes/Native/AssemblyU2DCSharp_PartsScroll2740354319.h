﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// System.Collections.Generic.List`1<PremitivePartContent>
struct List_1_t3065337161;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsScroll
struct  PartsScroll_t2740354319  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PartsScroll::PartsSelectSample
	GameObject_t1756533147 * ___PartsSelectSample_2;
	// UnityEngine.Transform[] PartsScroll::PartsSelectList
	TransformU5BU5D_t3764228911* ___PartsSelectList_3;
	// System.Int32 PartsScroll::m_sCurrent
	int32_t ___m_sCurrent_4;
	// System.Collections.Generic.List`1<PremitivePartContent> PartsScroll::m_PartsList
	List_1_t3065337161 * ___m_PartsList_5;

public:
	inline static int32_t get_offset_of_PartsSelectSample_2() { return static_cast<int32_t>(offsetof(PartsScroll_t2740354319, ___PartsSelectSample_2)); }
	inline GameObject_t1756533147 * get_PartsSelectSample_2() const { return ___PartsSelectSample_2; }
	inline GameObject_t1756533147 ** get_address_of_PartsSelectSample_2() { return &___PartsSelectSample_2; }
	inline void set_PartsSelectSample_2(GameObject_t1756533147 * value)
	{
		___PartsSelectSample_2 = value;
		Il2CppCodeGenWriteBarrier(&___PartsSelectSample_2, value);
	}

	inline static int32_t get_offset_of_PartsSelectList_3() { return static_cast<int32_t>(offsetof(PartsScroll_t2740354319, ___PartsSelectList_3)); }
	inline TransformU5BU5D_t3764228911* get_PartsSelectList_3() const { return ___PartsSelectList_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_PartsSelectList_3() { return &___PartsSelectList_3; }
	inline void set_PartsSelectList_3(TransformU5BU5D_t3764228911* value)
	{
		___PartsSelectList_3 = value;
		Il2CppCodeGenWriteBarrier(&___PartsSelectList_3, value);
	}

	inline static int32_t get_offset_of_m_sCurrent_4() { return static_cast<int32_t>(offsetof(PartsScroll_t2740354319, ___m_sCurrent_4)); }
	inline int32_t get_m_sCurrent_4() const { return ___m_sCurrent_4; }
	inline int32_t* get_address_of_m_sCurrent_4() { return &___m_sCurrent_4; }
	inline void set_m_sCurrent_4(int32_t value)
	{
		___m_sCurrent_4 = value;
	}

	inline static int32_t get_offset_of_m_PartsList_5() { return static_cast<int32_t>(offsetof(PartsScroll_t2740354319, ___m_PartsList_5)); }
	inline List_1_t3065337161 * get_m_PartsList_5() const { return ___m_PartsList_5; }
	inline List_1_t3065337161 ** get_address_of_m_PartsList_5() { return &___m_PartsList_5; }
	inline void set_m_PartsList_5(List_1_t3065337161 * value)
	{
		___m_PartsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_PartsList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
