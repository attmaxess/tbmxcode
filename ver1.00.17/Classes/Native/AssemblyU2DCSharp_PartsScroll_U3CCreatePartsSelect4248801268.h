﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// PartsScroll/<CreatePartsSelect>c__Iterator0
struct U3CCreatePartsSelectU3Ec__Iterator0_t176489707;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsScroll/<CreatePartsSelect>c__Iterator0/<CreatePartsSelect>c__AnonStorey1
struct  U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268  : public Il2CppObject
{
public:
	// System.String PartsScroll/<CreatePartsSelect>c__Iterator0/<CreatePartsSelect>c__AnonStorey1::_partsName
	String_t* ____partsName_0;
	// PartsScroll/<CreatePartsSelect>c__Iterator0 PartsScroll/<CreatePartsSelect>c__Iterator0/<CreatePartsSelect>c__AnonStorey1::<>f__ref$0
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of__partsName_0() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268, ____partsName_0)); }
	inline String_t* get__partsName_0() const { return ____partsName_0; }
	inline String_t** get_address_of__partsName_0() { return &____partsName_0; }
	inline void set__partsName_0(String_t* value)
	{
		____partsName_0 = value;
		Il2CppCodeGenWriteBarrier(&____partsName_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268, ___U3CU3Ef__refU240_1)); }
	inline U3CCreatePartsSelectU3Ec__Iterator0_t176489707 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CCreatePartsSelectU3Ec__Iterator0_t176489707 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CCreatePartsSelectU3Ec__Iterator0_t176489707 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
