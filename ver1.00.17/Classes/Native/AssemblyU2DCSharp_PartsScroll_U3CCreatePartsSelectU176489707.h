﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PartsScroll
struct PartsScroll_t2740354319;
// System.Object
struct Il2CppObject;
// PartsScroll/<CreatePartsSelect>c__Iterator0/<CreatePartsSelect>c__AnonStorey1
struct U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsScroll/<CreatePartsSelect>c__Iterator0
struct  U3CCreatePartsSelectU3Ec__Iterator0_t176489707  : public Il2CppObject
{
public:
	// System.Int32 PartsScroll/<CreatePartsSelect>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// UnityEngine.GameObject PartsScroll/<CreatePartsSelect>c__Iterator0::<obj>__2
	GameObject_t1756533147 * ___U3CobjU3E__2_1;
	// PartsScroll PartsScroll/<CreatePartsSelect>c__Iterator0::$this
	PartsScroll_t2740354319 * ___U24this_2;
	// System.Object PartsScroll/<CreatePartsSelect>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean PartsScroll/<CreatePartsSelect>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PartsScroll/<CreatePartsSelect>c__Iterator0::$PC
	int32_t ___U24PC_5;
	// PartsScroll/<CreatePartsSelect>c__Iterator0/<CreatePartsSelect>c__AnonStorey1 PartsScroll/<CreatePartsSelect>c__Iterator0::$locvar0
	U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268 * ___U24locvar0_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t176489707, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E__2_1() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t176489707, ___U3CobjU3E__2_1)); }
	inline GameObject_t1756533147 * get_U3CobjU3E__2_1() const { return ___U3CobjU3E__2_1; }
	inline GameObject_t1756533147 ** get_address_of_U3CobjU3E__2_1() { return &___U3CobjU3E__2_1; }
	inline void set_U3CobjU3E__2_1(GameObject_t1756533147 * value)
	{
		___U3CobjU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__2_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t176489707, ___U24this_2)); }
	inline PartsScroll_t2740354319 * get_U24this_2() const { return ___U24this_2; }
	inline PartsScroll_t2740354319 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PartsScroll_t2740354319 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t176489707, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t176489707, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t176489707, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_6() { return static_cast<int32_t>(offsetof(U3CCreatePartsSelectU3Ec__Iterator0_t176489707, ___U24locvar0_6)); }
	inline U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268 * get_U24locvar0_6() const { return ___U24locvar0_6; }
	inline U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268 ** get_address_of_U24locvar0_6() { return &___U24locvar0_6; }
	inline void set_U24locvar0_6(U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268 * value)
	{
		___U24locvar0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
