﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsSelector
struct  PartsSelector_t3170319883  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PartsSelector::m_coreParts
	GameObject_t1756533147 * ___m_coreParts_2;
	// UnityEngine.GameObject PartsSelector::SelectParts
	GameObject_t1756533147 * ___SelectParts_3;

public:
	inline static int32_t get_offset_of_m_coreParts_2() { return static_cast<int32_t>(offsetof(PartsSelector_t3170319883, ___m_coreParts_2)); }
	inline GameObject_t1756533147 * get_m_coreParts_2() const { return ___m_coreParts_2; }
	inline GameObject_t1756533147 ** get_address_of_m_coreParts_2() { return &___m_coreParts_2; }
	inline void set_m_coreParts_2(GameObject_t1756533147 * value)
	{
		___m_coreParts_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_coreParts_2, value);
	}

	inline static int32_t get_offset_of_SelectParts_3() { return static_cast<int32_t>(offsetof(PartsSelector_t3170319883, ___SelectParts_3)); }
	inline GameObject_t1756533147 * get_SelectParts_3() const { return ___SelectParts_3; }
	inline GameObject_t1756533147 ** get_address_of_SelectParts_3() { return &___SelectParts_3; }
	inline void set_SelectParts_3(GameObject_t1756533147 * value)
	{
		___SelectParts_3 = value;
		Il2CppCodeGenWriteBarrier(&___SelectParts_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
