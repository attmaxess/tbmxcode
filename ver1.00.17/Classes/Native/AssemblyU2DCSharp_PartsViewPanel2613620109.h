﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// DoubleTapPartsScaling
struct DoubleTapPartsScaling_t180404427;
// SlideInOut
struct SlideInOut_t958296806;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PartsViewPanel
struct  PartsViewPanel_t2613620109  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PartsViewPanel::ActionPartsPanel
	GameObject_t1756533147 * ___ActionPartsPanel_2;
	// UnityEngine.UI.Button PartsViewPanel::BackButton
	Button_t2872111280 * ___BackButton_3;
	// UnityEngine.UI.Button PartsViewPanel::CancelButton
	Button_t2872111280 * ___CancelButton_4;
	// UnityEngine.UI.Button[] PartsViewPanel::ScaleButtons
	ButtonU5BU5D_t3071100561* ___ScaleButtons_5;
	// UnityEngine.UI.Button PartsViewPanel::CompleteButton
	Button_t2872111280 * ___CompleteButton_6;
	// DoubleTapPartsScaling PartsViewPanel::m_Scaler
	DoubleTapPartsScaling_t180404427 * ___m_Scaler_7;
	// SlideInOut PartsViewPanel::m_Slider
	SlideInOut_t958296806 * ___m_Slider_8;

public:
	inline static int32_t get_offset_of_ActionPartsPanel_2() { return static_cast<int32_t>(offsetof(PartsViewPanel_t2613620109, ___ActionPartsPanel_2)); }
	inline GameObject_t1756533147 * get_ActionPartsPanel_2() const { return ___ActionPartsPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_ActionPartsPanel_2() { return &___ActionPartsPanel_2; }
	inline void set_ActionPartsPanel_2(GameObject_t1756533147 * value)
	{
		___ActionPartsPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPartsPanel_2, value);
	}

	inline static int32_t get_offset_of_BackButton_3() { return static_cast<int32_t>(offsetof(PartsViewPanel_t2613620109, ___BackButton_3)); }
	inline Button_t2872111280 * get_BackButton_3() const { return ___BackButton_3; }
	inline Button_t2872111280 ** get_address_of_BackButton_3() { return &___BackButton_3; }
	inline void set_BackButton_3(Button_t2872111280 * value)
	{
		___BackButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___BackButton_3, value);
	}

	inline static int32_t get_offset_of_CancelButton_4() { return static_cast<int32_t>(offsetof(PartsViewPanel_t2613620109, ___CancelButton_4)); }
	inline Button_t2872111280 * get_CancelButton_4() const { return ___CancelButton_4; }
	inline Button_t2872111280 ** get_address_of_CancelButton_4() { return &___CancelButton_4; }
	inline void set_CancelButton_4(Button_t2872111280 * value)
	{
		___CancelButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___CancelButton_4, value);
	}

	inline static int32_t get_offset_of_ScaleButtons_5() { return static_cast<int32_t>(offsetof(PartsViewPanel_t2613620109, ___ScaleButtons_5)); }
	inline ButtonU5BU5D_t3071100561* get_ScaleButtons_5() const { return ___ScaleButtons_5; }
	inline ButtonU5BU5D_t3071100561** get_address_of_ScaleButtons_5() { return &___ScaleButtons_5; }
	inline void set_ScaleButtons_5(ButtonU5BU5D_t3071100561* value)
	{
		___ScaleButtons_5 = value;
		Il2CppCodeGenWriteBarrier(&___ScaleButtons_5, value);
	}

	inline static int32_t get_offset_of_CompleteButton_6() { return static_cast<int32_t>(offsetof(PartsViewPanel_t2613620109, ___CompleteButton_6)); }
	inline Button_t2872111280 * get_CompleteButton_6() const { return ___CompleteButton_6; }
	inline Button_t2872111280 ** get_address_of_CompleteButton_6() { return &___CompleteButton_6; }
	inline void set_CompleteButton_6(Button_t2872111280 * value)
	{
		___CompleteButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___CompleteButton_6, value);
	}

	inline static int32_t get_offset_of_m_Scaler_7() { return static_cast<int32_t>(offsetof(PartsViewPanel_t2613620109, ___m_Scaler_7)); }
	inline DoubleTapPartsScaling_t180404427 * get_m_Scaler_7() const { return ___m_Scaler_7; }
	inline DoubleTapPartsScaling_t180404427 ** get_address_of_m_Scaler_7() { return &___m_Scaler_7; }
	inline void set_m_Scaler_7(DoubleTapPartsScaling_t180404427 * value)
	{
		___m_Scaler_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_Scaler_7, value);
	}

	inline static int32_t get_offset_of_m_Slider_8() { return static_cast<int32_t>(offsetof(PartsViewPanel_t2613620109, ___m_Slider_8)); }
	inline SlideInOut_t958296806 * get_m_Slider_8() const { return ___m_Slider_8; }
	inline SlideInOut_t958296806 ** get_address_of_m_Slider_8() { return &___m_Slider_8; }
	inline void set_m_Slider_8(SlideInOut_t958296806 * value)
	{
		___m_Slider_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_Slider_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
