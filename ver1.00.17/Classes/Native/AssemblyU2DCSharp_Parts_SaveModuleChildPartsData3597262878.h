﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Parts/SaveModuleChildPartsData
struct  SaveModuleChildPartsData_t3597262878  : public Il2CppObject
{
public:
	// System.Int32 Parts/SaveModuleChildPartsData::parentPartsJointNum
	int32_t ___parentPartsJointNum_0;
	// System.Int32 Parts/SaveModuleChildPartsData::childPartsJointNum
	int32_t ___childPartsJointNum_1;
	// System.Single Parts/SaveModuleChildPartsData::childObjPositionX
	float ___childObjPositionX_2;
	// System.Single Parts/SaveModuleChildPartsData::childObjPositionY
	float ___childObjPositionY_3;
	// System.Single Parts/SaveModuleChildPartsData::childObjPositionZ
	float ___childObjPositionZ_4;
	// System.Int32 Parts/SaveModuleChildPartsData::childPartsId
	int32_t ___childPartsId_5;
	// System.String Parts/SaveModuleChildPartsData::childPartsName
	String_t* ___childPartsName_6;
	// System.Int32 Parts/SaveModuleChildPartsData::childPartsColor
	int32_t ___childPartsColor_7;
	// System.String Parts/SaveModuleChildPartsData::parentPartsName
	String_t* ___parentPartsName_8;
	// System.Int32 Parts/SaveModuleChildPartsData::parentPartsId
	int32_t ___parentPartsId_9;
	// System.Single Parts/SaveModuleChildPartsData::childPartsPositionX
	float ___childPartsPositionX_10;
	// System.Single Parts/SaveModuleChildPartsData::childPartsPositionY
	float ___childPartsPositionY_11;
	// System.Single Parts/SaveModuleChildPartsData::childPartsPositionZ
	float ___childPartsPositionZ_12;
	// System.Single Parts/SaveModuleChildPartsData::childPartsRotationX
	float ___childPartsRotationX_13;
	// System.Single Parts/SaveModuleChildPartsData::childPartsRotationY
	float ___childPartsRotationY_14;
	// System.Single Parts/SaveModuleChildPartsData::childPartsRotationZ
	float ___childPartsRotationZ_15;
	// System.Single Parts/SaveModuleChildPartsData::childPartsScaleX
	float ___childPartsScaleX_16;
	// System.Single Parts/SaveModuleChildPartsData::childPartsScaleY
	float ___childPartsScaleY_17;
	// System.Single Parts/SaveModuleChildPartsData::childPartsScaleZ
	float ___childPartsScaleZ_18;
	// System.Single Parts/SaveModuleChildPartsData::childLeapRotationX
	float ___childLeapRotationX_19;
	// System.Single Parts/SaveModuleChildPartsData::childLeapRotationY
	float ___childLeapRotationY_20;
	// System.Single Parts/SaveModuleChildPartsData::childLeapRotationZ
	float ___childLeapRotationZ_21;
	// System.Single Parts/SaveModuleChildPartsData::childRollRotationX
	float ___childRollRotationX_22;
	// System.Single Parts/SaveModuleChildPartsData::childRollRotationY
	float ___childRollRotationY_23;
	// System.Single Parts/SaveModuleChildPartsData::childRollRotationZ
	float ___childRollRotationZ_24;
	// System.Int32 Parts/SaveModuleChildPartsData::leapFixed
	int32_t ___leapFixed_25;

public:
	inline static int32_t get_offset_of_parentPartsJointNum_0() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___parentPartsJointNum_0)); }
	inline int32_t get_parentPartsJointNum_0() const { return ___parentPartsJointNum_0; }
	inline int32_t* get_address_of_parentPartsJointNum_0() { return &___parentPartsJointNum_0; }
	inline void set_parentPartsJointNum_0(int32_t value)
	{
		___parentPartsJointNum_0 = value;
	}

	inline static int32_t get_offset_of_childPartsJointNum_1() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsJointNum_1)); }
	inline int32_t get_childPartsJointNum_1() const { return ___childPartsJointNum_1; }
	inline int32_t* get_address_of_childPartsJointNum_1() { return &___childPartsJointNum_1; }
	inline void set_childPartsJointNum_1(int32_t value)
	{
		___childPartsJointNum_1 = value;
	}

	inline static int32_t get_offset_of_childObjPositionX_2() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childObjPositionX_2)); }
	inline float get_childObjPositionX_2() const { return ___childObjPositionX_2; }
	inline float* get_address_of_childObjPositionX_2() { return &___childObjPositionX_2; }
	inline void set_childObjPositionX_2(float value)
	{
		___childObjPositionX_2 = value;
	}

	inline static int32_t get_offset_of_childObjPositionY_3() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childObjPositionY_3)); }
	inline float get_childObjPositionY_3() const { return ___childObjPositionY_3; }
	inline float* get_address_of_childObjPositionY_3() { return &___childObjPositionY_3; }
	inline void set_childObjPositionY_3(float value)
	{
		___childObjPositionY_3 = value;
	}

	inline static int32_t get_offset_of_childObjPositionZ_4() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childObjPositionZ_4)); }
	inline float get_childObjPositionZ_4() const { return ___childObjPositionZ_4; }
	inline float* get_address_of_childObjPositionZ_4() { return &___childObjPositionZ_4; }
	inline void set_childObjPositionZ_4(float value)
	{
		___childObjPositionZ_4 = value;
	}

	inline static int32_t get_offset_of_childPartsId_5() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsId_5)); }
	inline int32_t get_childPartsId_5() const { return ___childPartsId_5; }
	inline int32_t* get_address_of_childPartsId_5() { return &___childPartsId_5; }
	inline void set_childPartsId_5(int32_t value)
	{
		___childPartsId_5 = value;
	}

	inline static int32_t get_offset_of_childPartsName_6() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsName_6)); }
	inline String_t* get_childPartsName_6() const { return ___childPartsName_6; }
	inline String_t** get_address_of_childPartsName_6() { return &___childPartsName_6; }
	inline void set_childPartsName_6(String_t* value)
	{
		___childPartsName_6 = value;
		Il2CppCodeGenWriteBarrier(&___childPartsName_6, value);
	}

	inline static int32_t get_offset_of_childPartsColor_7() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsColor_7)); }
	inline int32_t get_childPartsColor_7() const { return ___childPartsColor_7; }
	inline int32_t* get_address_of_childPartsColor_7() { return &___childPartsColor_7; }
	inline void set_childPartsColor_7(int32_t value)
	{
		___childPartsColor_7 = value;
	}

	inline static int32_t get_offset_of_parentPartsName_8() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___parentPartsName_8)); }
	inline String_t* get_parentPartsName_8() const { return ___parentPartsName_8; }
	inline String_t** get_address_of_parentPartsName_8() { return &___parentPartsName_8; }
	inline void set_parentPartsName_8(String_t* value)
	{
		___parentPartsName_8 = value;
		Il2CppCodeGenWriteBarrier(&___parentPartsName_8, value);
	}

	inline static int32_t get_offset_of_parentPartsId_9() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___parentPartsId_9)); }
	inline int32_t get_parentPartsId_9() const { return ___parentPartsId_9; }
	inline int32_t* get_address_of_parentPartsId_9() { return &___parentPartsId_9; }
	inline void set_parentPartsId_9(int32_t value)
	{
		___parentPartsId_9 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionX_10() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsPositionX_10)); }
	inline float get_childPartsPositionX_10() const { return ___childPartsPositionX_10; }
	inline float* get_address_of_childPartsPositionX_10() { return &___childPartsPositionX_10; }
	inline void set_childPartsPositionX_10(float value)
	{
		___childPartsPositionX_10 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionY_11() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsPositionY_11)); }
	inline float get_childPartsPositionY_11() const { return ___childPartsPositionY_11; }
	inline float* get_address_of_childPartsPositionY_11() { return &___childPartsPositionY_11; }
	inline void set_childPartsPositionY_11(float value)
	{
		___childPartsPositionY_11 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionZ_12() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsPositionZ_12)); }
	inline float get_childPartsPositionZ_12() const { return ___childPartsPositionZ_12; }
	inline float* get_address_of_childPartsPositionZ_12() { return &___childPartsPositionZ_12; }
	inline void set_childPartsPositionZ_12(float value)
	{
		___childPartsPositionZ_12 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationX_13() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsRotationX_13)); }
	inline float get_childPartsRotationX_13() const { return ___childPartsRotationX_13; }
	inline float* get_address_of_childPartsRotationX_13() { return &___childPartsRotationX_13; }
	inline void set_childPartsRotationX_13(float value)
	{
		___childPartsRotationX_13 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationY_14() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsRotationY_14)); }
	inline float get_childPartsRotationY_14() const { return ___childPartsRotationY_14; }
	inline float* get_address_of_childPartsRotationY_14() { return &___childPartsRotationY_14; }
	inline void set_childPartsRotationY_14(float value)
	{
		___childPartsRotationY_14 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationZ_15() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsRotationZ_15)); }
	inline float get_childPartsRotationZ_15() const { return ___childPartsRotationZ_15; }
	inline float* get_address_of_childPartsRotationZ_15() { return &___childPartsRotationZ_15; }
	inline void set_childPartsRotationZ_15(float value)
	{
		___childPartsRotationZ_15 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleX_16() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsScaleX_16)); }
	inline float get_childPartsScaleX_16() const { return ___childPartsScaleX_16; }
	inline float* get_address_of_childPartsScaleX_16() { return &___childPartsScaleX_16; }
	inline void set_childPartsScaleX_16(float value)
	{
		___childPartsScaleX_16 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleY_17() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsScaleY_17)); }
	inline float get_childPartsScaleY_17() const { return ___childPartsScaleY_17; }
	inline float* get_address_of_childPartsScaleY_17() { return &___childPartsScaleY_17; }
	inline void set_childPartsScaleY_17(float value)
	{
		___childPartsScaleY_17 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleZ_18() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childPartsScaleZ_18)); }
	inline float get_childPartsScaleZ_18() const { return ___childPartsScaleZ_18; }
	inline float* get_address_of_childPartsScaleZ_18() { return &___childPartsScaleZ_18; }
	inline void set_childPartsScaleZ_18(float value)
	{
		___childPartsScaleZ_18 = value;
	}

	inline static int32_t get_offset_of_childLeapRotationX_19() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childLeapRotationX_19)); }
	inline float get_childLeapRotationX_19() const { return ___childLeapRotationX_19; }
	inline float* get_address_of_childLeapRotationX_19() { return &___childLeapRotationX_19; }
	inline void set_childLeapRotationX_19(float value)
	{
		___childLeapRotationX_19 = value;
	}

	inline static int32_t get_offset_of_childLeapRotationY_20() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childLeapRotationY_20)); }
	inline float get_childLeapRotationY_20() const { return ___childLeapRotationY_20; }
	inline float* get_address_of_childLeapRotationY_20() { return &___childLeapRotationY_20; }
	inline void set_childLeapRotationY_20(float value)
	{
		___childLeapRotationY_20 = value;
	}

	inline static int32_t get_offset_of_childLeapRotationZ_21() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childLeapRotationZ_21)); }
	inline float get_childLeapRotationZ_21() const { return ___childLeapRotationZ_21; }
	inline float* get_address_of_childLeapRotationZ_21() { return &___childLeapRotationZ_21; }
	inline void set_childLeapRotationZ_21(float value)
	{
		___childLeapRotationZ_21 = value;
	}

	inline static int32_t get_offset_of_childRollRotationX_22() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childRollRotationX_22)); }
	inline float get_childRollRotationX_22() const { return ___childRollRotationX_22; }
	inline float* get_address_of_childRollRotationX_22() { return &___childRollRotationX_22; }
	inline void set_childRollRotationX_22(float value)
	{
		___childRollRotationX_22 = value;
	}

	inline static int32_t get_offset_of_childRollRotationY_23() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childRollRotationY_23)); }
	inline float get_childRollRotationY_23() const { return ___childRollRotationY_23; }
	inline float* get_address_of_childRollRotationY_23() { return &___childRollRotationY_23; }
	inline void set_childRollRotationY_23(float value)
	{
		___childRollRotationY_23 = value;
	}

	inline static int32_t get_offset_of_childRollRotationZ_24() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___childRollRotationZ_24)); }
	inline float get_childRollRotationZ_24() const { return ___childRollRotationZ_24; }
	inline float* get_address_of_childRollRotationZ_24() { return &___childRollRotationZ_24; }
	inline void set_childRollRotationZ_24(float value)
	{
		___childRollRotationZ_24 = value;
	}

	inline static int32_t get_offset_of_leapFixed_25() { return static_cast<int32_t>(offsetof(SaveModuleChildPartsData_t3597262878, ___leapFixed_25)); }
	inline int32_t get_leapFixed_25() const { return ___leapFixed_25; }
	inline int32_t* get_address_of_leapFixed_25() { return &___leapFixed_25; }
	inline void set_leapFixed_25(int32_t value)
	{
		___leapFixed_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
