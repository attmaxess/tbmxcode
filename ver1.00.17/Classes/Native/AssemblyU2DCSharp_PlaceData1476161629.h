﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceData
struct  PlaceData_t1476161629  : public Il2CppObject
{
public:
	// System.Int32 PlaceData::level
	int32_t ___level_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PlaceData::ballPosition
	List_1_t1612828712 * ___ballPosition_1;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(PlaceData_t1476161629, ___level_0)); }
	inline int32_t get_level_0() const { return ___level_0; }
	inline int32_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(int32_t value)
	{
		___level_0 = value;
	}

	inline static int32_t get_offset_of_ballPosition_1() { return static_cast<int32_t>(offsetof(PlaceData_t1476161629, ___ballPosition_1)); }
	inline List_1_t1612828712 * get_ballPosition_1() const { return ___ballPosition_1; }
	inline List_1_t1612828712 ** get_address_of_ballPosition_1() { return &___ballPosition_1; }
	inline void set_ballPosition_1(List_1_t1612828712 * value)
	{
		___ballPosition_1 = value;
		Il2CppCodeGenWriteBarrier(&___ballPosition_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
