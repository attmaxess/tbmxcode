﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlanetGravity
struct  PlanetGravity_t2098207264  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PlanetGravity::planet
	GameObject_t1756533147 * ___planet_2;
	// System.Single PlanetGravity::m_fGravity
	float ___m_fGravity_3;
	// System.Single PlanetGravity::gravityLevel
	float ___gravityLevel_4;
	// System.Single PlanetGravity::accelerationScale
	float ___accelerationScale_5;
	// System.Single PlanetGravity::windScale
	float ___windScale_6;
	// System.Boolean PlanetGravity::inWater
	bool ___inWater_7;
	// UnityEngine.Vector3 PlanetGravity::direction
	Vector3_t2243707580  ___direction_8;
	// System.Single PlanetGravity::waterSoundTimer
	float ___waterSoundTimer_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlanetGravity::colList
	List_1_t1125654279 * ___colList_10;

public:
	inline static int32_t get_offset_of_planet_2() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___planet_2)); }
	inline GameObject_t1756533147 * get_planet_2() const { return ___planet_2; }
	inline GameObject_t1756533147 ** get_address_of_planet_2() { return &___planet_2; }
	inline void set_planet_2(GameObject_t1756533147 * value)
	{
		___planet_2 = value;
		Il2CppCodeGenWriteBarrier(&___planet_2, value);
	}

	inline static int32_t get_offset_of_m_fGravity_3() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___m_fGravity_3)); }
	inline float get_m_fGravity_3() const { return ___m_fGravity_3; }
	inline float* get_address_of_m_fGravity_3() { return &___m_fGravity_3; }
	inline void set_m_fGravity_3(float value)
	{
		___m_fGravity_3 = value;
	}

	inline static int32_t get_offset_of_gravityLevel_4() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___gravityLevel_4)); }
	inline float get_gravityLevel_4() const { return ___gravityLevel_4; }
	inline float* get_address_of_gravityLevel_4() { return &___gravityLevel_4; }
	inline void set_gravityLevel_4(float value)
	{
		___gravityLevel_4 = value;
	}

	inline static int32_t get_offset_of_accelerationScale_5() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___accelerationScale_5)); }
	inline float get_accelerationScale_5() const { return ___accelerationScale_5; }
	inline float* get_address_of_accelerationScale_5() { return &___accelerationScale_5; }
	inline void set_accelerationScale_5(float value)
	{
		___accelerationScale_5 = value;
	}

	inline static int32_t get_offset_of_windScale_6() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___windScale_6)); }
	inline float get_windScale_6() const { return ___windScale_6; }
	inline float* get_address_of_windScale_6() { return &___windScale_6; }
	inline void set_windScale_6(float value)
	{
		___windScale_6 = value;
	}

	inline static int32_t get_offset_of_inWater_7() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___inWater_7)); }
	inline bool get_inWater_7() const { return ___inWater_7; }
	inline bool* get_address_of_inWater_7() { return &___inWater_7; }
	inline void set_inWater_7(bool value)
	{
		___inWater_7 = value;
	}

	inline static int32_t get_offset_of_direction_8() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___direction_8)); }
	inline Vector3_t2243707580  get_direction_8() const { return ___direction_8; }
	inline Vector3_t2243707580 * get_address_of_direction_8() { return &___direction_8; }
	inline void set_direction_8(Vector3_t2243707580  value)
	{
		___direction_8 = value;
	}

	inline static int32_t get_offset_of_waterSoundTimer_9() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___waterSoundTimer_9)); }
	inline float get_waterSoundTimer_9() const { return ___waterSoundTimer_9; }
	inline float* get_address_of_waterSoundTimer_9() { return &___waterSoundTimer_9; }
	inline void set_waterSoundTimer_9(float value)
	{
		___waterSoundTimer_9 = value;
	}

	inline static int32_t get_offset_of_colList_10() { return static_cast<int32_t>(offsetof(PlanetGravity_t2098207264, ___colList_10)); }
	inline List_1_t1125654279 * get_colList_10() const { return ___colList_10; }
	inline List_1_t1125654279 ** get_address_of_colList_10() { return &___colList_10; }
	inline void set_colList_10(List_1_t1125654279 * value)
	{
		___colList_10 = value;
		Il2CppCodeGenWriteBarrier(&___colList_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
