﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler
struct NotifyCollectionChangedEventHandler_t2314871951;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>
struct  ObservableDictionary_2_t1611561244  : public Il2CppObject
{
public:
	// System.Collections.Generic.IDictionary`2<TKey,TValue> PlatformSupport.Collections.ObjectModel.ObservableDictionary`2::_Dictionary
	Il2CppObject* ____Dictionary_4;
	// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler PlatformSupport.Collections.ObjectModel.ObservableDictionary`2::CollectionChanged
	NotifyCollectionChangedEventHandler_t2314871951 * ___CollectionChanged_5;
	// System.ComponentModel.PropertyChangedEventHandler PlatformSupport.Collections.ObjectModel.ObservableDictionary`2::PropertyChanged
	PropertyChangedEventHandler_t3042952059 * ___PropertyChanged_6;

public:
	inline static int32_t get_offset_of__Dictionary_4() { return static_cast<int32_t>(offsetof(ObservableDictionary_2_t1611561244, ____Dictionary_4)); }
	inline Il2CppObject* get__Dictionary_4() const { return ____Dictionary_4; }
	inline Il2CppObject** get_address_of__Dictionary_4() { return &____Dictionary_4; }
	inline void set__Dictionary_4(Il2CppObject* value)
	{
		____Dictionary_4 = value;
		Il2CppCodeGenWriteBarrier(&____Dictionary_4, value);
	}

	inline static int32_t get_offset_of_CollectionChanged_5() { return static_cast<int32_t>(offsetof(ObservableDictionary_2_t1611561244, ___CollectionChanged_5)); }
	inline NotifyCollectionChangedEventHandler_t2314871951 * get_CollectionChanged_5() const { return ___CollectionChanged_5; }
	inline NotifyCollectionChangedEventHandler_t2314871951 ** get_address_of_CollectionChanged_5() { return &___CollectionChanged_5; }
	inline void set_CollectionChanged_5(NotifyCollectionChangedEventHandler_t2314871951 * value)
	{
		___CollectionChanged_5 = value;
		Il2CppCodeGenWriteBarrier(&___CollectionChanged_5, value);
	}

	inline static int32_t get_offset_of_PropertyChanged_6() { return static_cast<int32_t>(offsetof(ObservableDictionary_2_t1611561244, ___PropertyChanged_6)); }
	inline PropertyChangedEventHandler_t3042952059 * get_PropertyChanged_6() const { return ___PropertyChanged_6; }
	inline PropertyChangedEventHandler_t3042952059 ** get_address_of_PropertyChanged_6() { return &___PropertyChanged_6; }
	inline void set_PropertyChanged_6(PropertyChangedEventHandler_t3042952059 * value)
	{
		___PropertyChanged_6 = value;
		Il2CppCodeGenWriteBarrier(&___PropertyChanged_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
