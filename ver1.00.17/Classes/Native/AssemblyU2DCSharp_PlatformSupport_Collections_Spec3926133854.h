﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_EventArgs3289624707.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2640700633.h"

// System.Collections.IList
struct IList_t3321498491;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs
struct  NotifyCollectionChangedEventArgs_t3926133854  : public EventArgs_t3289624707
{
public:
	// PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_action
	int32_t ____action_1;
	// System.Collections.IList PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_newItems
	Il2CppObject * ____newItems_2;
	// System.Collections.IList PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_oldItems
	Il2CppObject * ____oldItems_3;
	// System.Int32 PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_newStartingIndex
	int32_t ____newStartingIndex_4;
	// System.Int32 PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_oldStartingIndex
	int32_t ____oldStartingIndex_5;

public:
	inline static int32_t get_offset_of__action_1() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t3926133854, ____action_1)); }
	inline int32_t get__action_1() const { return ____action_1; }
	inline int32_t* get_address_of__action_1() { return &____action_1; }
	inline void set__action_1(int32_t value)
	{
		____action_1 = value;
	}

	inline static int32_t get_offset_of__newItems_2() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t3926133854, ____newItems_2)); }
	inline Il2CppObject * get__newItems_2() const { return ____newItems_2; }
	inline Il2CppObject ** get_address_of__newItems_2() { return &____newItems_2; }
	inline void set__newItems_2(Il2CppObject * value)
	{
		____newItems_2 = value;
		Il2CppCodeGenWriteBarrier(&____newItems_2, value);
	}

	inline static int32_t get_offset_of__oldItems_3() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t3926133854, ____oldItems_3)); }
	inline Il2CppObject * get__oldItems_3() const { return ____oldItems_3; }
	inline Il2CppObject ** get_address_of__oldItems_3() { return &____oldItems_3; }
	inline void set__oldItems_3(Il2CppObject * value)
	{
		____oldItems_3 = value;
		Il2CppCodeGenWriteBarrier(&____oldItems_3, value);
	}

	inline static int32_t get_offset_of__newStartingIndex_4() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t3926133854, ____newStartingIndex_4)); }
	inline int32_t get__newStartingIndex_4() const { return ____newStartingIndex_4; }
	inline int32_t* get_address_of__newStartingIndex_4() { return &____newStartingIndex_4; }
	inline void set__newStartingIndex_4(int32_t value)
	{
		____newStartingIndex_4 = value;
	}

	inline static int32_t get_offset_of__oldStartingIndex_5() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_t3926133854, ____oldStartingIndex_5)); }
	inline int32_t get__oldStartingIndex_5() const { return ____oldStartingIndex_5; }
	inline int32_t* get_address_of__oldStartingIndex_5() { return &____oldStartingIndex_5; }
	inline void set__oldStartingIndex_5(int32_t value)
	{
		____oldStartingIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
