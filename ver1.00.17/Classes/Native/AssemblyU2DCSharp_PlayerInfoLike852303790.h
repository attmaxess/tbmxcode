﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerInfoLike
struct  PlayerInfoLike_t852303790  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image PlayerInfoLike::iconEmotion
	Image_t2042527209 * ___iconEmotion_2;
	// UnityEngine.UI.Image PlayerInfoLike::iconRank
	Image_t2042527209 * ___iconRank_3;
	// UnityEngine.UI.Text PlayerInfoLike::nameUsertxt
	Text_t356221433 * ___nameUsertxt_4;

public:
	inline static int32_t get_offset_of_iconEmotion_2() { return static_cast<int32_t>(offsetof(PlayerInfoLike_t852303790, ___iconEmotion_2)); }
	inline Image_t2042527209 * get_iconEmotion_2() const { return ___iconEmotion_2; }
	inline Image_t2042527209 ** get_address_of_iconEmotion_2() { return &___iconEmotion_2; }
	inline void set_iconEmotion_2(Image_t2042527209 * value)
	{
		___iconEmotion_2 = value;
		Il2CppCodeGenWriteBarrier(&___iconEmotion_2, value);
	}

	inline static int32_t get_offset_of_iconRank_3() { return static_cast<int32_t>(offsetof(PlayerInfoLike_t852303790, ___iconRank_3)); }
	inline Image_t2042527209 * get_iconRank_3() const { return ___iconRank_3; }
	inline Image_t2042527209 ** get_address_of_iconRank_3() { return &___iconRank_3; }
	inline void set_iconRank_3(Image_t2042527209 * value)
	{
		___iconRank_3 = value;
		Il2CppCodeGenWriteBarrier(&___iconRank_3, value);
	}

	inline static int32_t get_offset_of_nameUsertxt_4() { return static_cast<int32_t>(offsetof(PlayerInfoLike_t852303790, ___nameUsertxt_4)); }
	inline Text_t356221433 * get_nameUsertxt_4() const { return ___nameUsertxt_4; }
	inline Text_t356221433 ** get_address_of_nameUsertxt_4() { return &___nameUsertxt_4; }
	inline void set_nameUsertxt_4(Text_t356221433 * value)
	{
		___nameUsertxt_4 = value;
		Il2CppCodeGenWriteBarrier(&___nameUsertxt_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
