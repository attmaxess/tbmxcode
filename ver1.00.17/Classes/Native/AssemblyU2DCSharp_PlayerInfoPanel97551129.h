﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerInfoPanel
struct  PlayerInfoPanel_t97551129  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text PlayerInfoPanel::textName
	Text_t356221433 * ___textName_2;
	// UnityEngine.UI.Image PlayerInfoPanel::iconRank
	Image_t2042527209 * ___iconRank_3;
	// UnityEngine.UI.Button PlayerInfoPanel::detailButton
	Button_t2872111280 * ___detailButton_4;
	// System.Int32 PlayerInfoPanel::searchedUserId
	int32_t ___searchedUserId_5;

public:
	inline static int32_t get_offset_of_textName_2() { return static_cast<int32_t>(offsetof(PlayerInfoPanel_t97551129, ___textName_2)); }
	inline Text_t356221433 * get_textName_2() const { return ___textName_2; }
	inline Text_t356221433 ** get_address_of_textName_2() { return &___textName_2; }
	inline void set_textName_2(Text_t356221433 * value)
	{
		___textName_2 = value;
		Il2CppCodeGenWriteBarrier(&___textName_2, value);
	}

	inline static int32_t get_offset_of_iconRank_3() { return static_cast<int32_t>(offsetof(PlayerInfoPanel_t97551129, ___iconRank_3)); }
	inline Image_t2042527209 * get_iconRank_3() const { return ___iconRank_3; }
	inline Image_t2042527209 ** get_address_of_iconRank_3() { return &___iconRank_3; }
	inline void set_iconRank_3(Image_t2042527209 * value)
	{
		___iconRank_3 = value;
		Il2CppCodeGenWriteBarrier(&___iconRank_3, value);
	}

	inline static int32_t get_offset_of_detailButton_4() { return static_cast<int32_t>(offsetof(PlayerInfoPanel_t97551129, ___detailButton_4)); }
	inline Button_t2872111280 * get_detailButton_4() const { return ___detailButton_4; }
	inline Button_t2872111280 ** get_address_of_detailButton_4() { return &___detailButton_4; }
	inline void set_detailButton_4(Button_t2872111280 * value)
	{
		___detailButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___detailButton_4, value);
	}

	inline static int32_t get_offset_of_searchedUserId_5() { return static_cast<int32_t>(offsetof(PlayerInfoPanel_t97551129, ___searchedUserId_5)); }
	inline int32_t get_searchedUserId_5() const { return ___searchedUserId_5; }
	inline int32_t* get_address_of_searchedUserId_5() { return &___searchedUserId_5; }
	inline void set_searchedUserId_5(int32_t value)
	{
		___searchedUserId_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
