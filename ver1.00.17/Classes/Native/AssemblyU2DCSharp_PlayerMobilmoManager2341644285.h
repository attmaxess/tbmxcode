﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1038320620.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// DemoWorld
struct DemoWorld_t457704051;
// Mobilmo
struct Mobilmo_t370754809;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.UI.Text
struct Text_t356221433;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMobilmoManager
struct  PlayerMobilmoManager_t2341644285  : public SingletonMonoBehaviour_1_t1038320620
{
public:
	// UnityEngine.UI.Image[] PlayerMobilmoManager::ActionUI
	ImageU5BU5D_t590162004* ___ActionUI_3;
	// DemoWorld PlayerMobilmoManager::demoWold
	DemoWorld_t457704051 * ___demoWold_4;
	// Mobilmo PlayerMobilmoManager::playerMobilmo
	Mobilmo_t370754809 * ___playerMobilmo_5;
	// UnityEngine.GameObject PlayerMobilmoManager::m_MobilmoObj
	GameObject_t1756533147 * ___m_MobilmoObj_6;
	// UnityEngine.GameObject PlayerMobilmoManager::m_CoreObj
	GameObject_t1756533147 * ___m_CoreObj_7;
	// UnityEngine.Vector3 PlayerMobilmoManager::m_vSavePos
	Vector3_t2243707580  ___m_vSavePos_8;
	// System.Boolean PlayerMobilmoManager::m_bIsMobilityBreak
	bool ___m_bIsMobilityBreak_9;
	// System.Single[] PlayerMobilmoManager::m_fLeverValue
	SingleU5BU5D_t577127397* ___m_fLeverValue_10;
	// System.Boolean PlayerMobilmoManager::m_bActionLever
	bool ___m_bActionLever_11;
	// System.Single PlayerMobilmoManager::ResetPosCoolTime
	float ___ResetPosCoolTime_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlayerMobilmoManager::nearmobObjList
	List_1_t1125654279 * ___nearmobObjList_13;
	// UnityEngine.GameObject PlayerMobilmoManager::m_RecordingCreObj
	GameObject_t1756533147 * ___m_RecordingCreObj_14;
	// UnityEngine.GameObject PlayerMobilmoManager::m_MyPageCreObj
	GameObject_t1756533147 * ___m_MyPageCreObj_15;
	// UnityEngine.Transform[] PlayerMobilmoManager::corePastsChildTra
	TransformU5BU5D_t3764228911* ___corePastsChildTra_16;
	// System.Single PlayerMobilmoManager::childDrillerPositionX
	float ___childDrillerPositionX_17;
	// System.Single PlayerMobilmoManager::childDrillerPositionY
	float ___childDrillerPositionY_18;
	// System.Single PlayerMobilmoManager::childDrillerPositionZ
	float ___childDrillerPositionZ_19;
	// System.Single PlayerMobilmoManager::childCenterRotationX
	float ___childCenterRotationX_20;
	// System.Single PlayerMobilmoManager::childCenterRotationY
	float ___childCenterRotationY_21;
	// System.Single PlayerMobilmoManager::childCenterRotationZ
	float ___childCenterRotationZ_22;
	// System.Single PlayerMobilmoManager::childPartsPositionX
	float ___childPartsPositionX_23;
	// System.Single PlayerMobilmoManager::childPartsPositionY
	float ___childPartsPositionY_24;
	// System.Single PlayerMobilmoManager::childPartsPositionZ
	float ___childPartsPositionZ_25;
	// System.Single PlayerMobilmoManager::childPartsRotationX
	float ___childPartsRotationX_26;
	// System.Single PlayerMobilmoManager::childPartsRotationY
	float ___childPartsRotationY_27;
	// System.Single PlayerMobilmoManager::childPartsRotationZ
	float ___childPartsRotationZ_28;
	// System.Single PlayerMobilmoManager::childPartsScaleX
	float ___childPartsScaleX_29;
	// System.Single PlayerMobilmoManager::childPartsScaleY
	float ___childPartsScaleY_30;
	// System.Single PlayerMobilmoManager::childPartsScaleZ
	float ___childPartsScaleZ_31;
	// System.Int32 PlayerMobilmoManager::parentPartsJointNo
	int32_t ___parentPartsJointNo_32;
	// System.Int32 PlayerMobilmoManager::childPartsJointNo
	int32_t ___childPartsJointNo_33;
	// UnityEngine.GameObject PlayerMobilmoManager::mobilityObject
	GameObject_t1756533147 * ___mobilityObject_34;
	// UnityEngine.GameObject PlayerMobilmoManager::selectingcopyObj
	GameObject_t1756533147 * ___selectingcopyObj_35;
	// System.Boolean PlayerMobilmoManager::m_bPlayAnim
	bool ___m_bPlayAnim_36;
	// System.Boolean PlayerMobilmoManager::m_bTutorialLever
	bool ___m_bTutorialLever_37;
	// System.Single PlayerMobilmoManager::m_fLeverPermission
	float ___m_fLeverPermission_38;
	// UnityEngine.GameObject PlayerMobilmoManager::recreatedModuleObj
	GameObject_t1756533147 * ___recreatedModuleObj_39;
	// UnityEngine.UI.Text PlayerMobilmoManager::MobilityNameText
	Text_t356221433 * ___MobilityNameText_40;
	// UnityEngine.UI.Text PlayerMobilmoManager::MobilityReactionText
	Text_t356221433 * ___MobilityReactionText_41;
	// UnityEngine.GameObject PlayerMobilmoManager::BreakedTextObj
	GameObject_t1756533147 * ___BreakedTextObj_42;

public:
	inline static int32_t get_offset_of_ActionUI_3() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___ActionUI_3)); }
	inline ImageU5BU5D_t590162004* get_ActionUI_3() const { return ___ActionUI_3; }
	inline ImageU5BU5D_t590162004** get_address_of_ActionUI_3() { return &___ActionUI_3; }
	inline void set_ActionUI_3(ImageU5BU5D_t590162004* value)
	{
		___ActionUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActionUI_3, value);
	}

	inline static int32_t get_offset_of_demoWold_4() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___demoWold_4)); }
	inline DemoWorld_t457704051 * get_demoWold_4() const { return ___demoWold_4; }
	inline DemoWorld_t457704051 ** get_address_of_demoWold_4() { return &___demoWold_4; }
	inline void set_demoWold_4(DemoWorld_t457704051 * value)
	{
		___demoWold_4 = value;
		Il2CppCodeGenWriteBarrier(&___demoWold_4, value);
	}

	inline static int32_t get_offset_of_playerMobilmo_5() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___playerMobilmo_5)); }
	inline Mobilmo_t370754809 * get_playerMobilmo_5() const { return ___playerMobilmo_5; }
	inline Mobilmo_t370754809 ** get_address_of_playerMobilmo_5() { return &___playerMobilmo_5; }
	inline void set_playerMobilmo_5(Mobilmo_t370754809 * value)
	{
		___playerMobilmo_5 = value;
		Il2CppCodeGenWriteBarrier(&___playerMobilmo_5, value);
	}

	inline static int32_t get_offset_of_m_MobilmoObj_6() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_MobilmoObj_6)); }
	inline GameObject_t1756533147 * get_m_MobilmoObj_6() const { return ___m_MobilmoObj_6; }
	inline GameObject_t1756533147 ** get_address_of_m_MobilmoObj_6() { return &___m_MobilmoObj_6; }
	inline void set_m_MobilmoObj_6(GameObject_t1756533147 * value)
	{
		___m_MobilmoObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_MobilmoObj_6, value);
	}

	inline static int32_t get_offset_of_m_CoreObj_7() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_CoreObj_7)); }
	inline GameObject_t1756533147 * get_m_CoreObj_7() const { return ___m_CoreObj_7; }
	inline GameObject_t1756533147 ** get_address_of_m_CoreObj_7() { return &___m_CoreObj_7; }
	inline void set_m_CoreObj_7(GameObject_t1756533147 * value)
	{
		___m_CoreObj_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_CoreObj_7, value);
	}

	inline static int32_t get_offset_of_m_vSavePos_8() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_vSavePos_8)); }
	inline Vector3_t2243707580  get_m_vSavePos_8() const { return ___m_vSavePos_8; }
	inline Vector3_t2243707580 * get_address_of_m_vSavePos_8() { return &___m_vSavePos_8; }
	inline void set_m_vSavePos_8(Vector3_t2243707580  value)
	{
		___m_vSavePos_8 = value;
	}

	inline static int32_t get_offset_of_m_bIsMobilityBreak_9() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_bIsMobilityBreak_9)); }
	inline bool get_m_bIsMobilityBreak_9() const { return ___m_bIsMobilityBreak_9; }
	inline bool* get_address_of_m_bIsMobilityBreak_9() { return &___m_bIsMobilityBreak_9; }
	inline void set_m_bIsMobilityBreak_9(bool value)
	{
		___m_bIsMobilityBreak_9 = value;
	}

	inline static int32_t get_offset_of_m_fLeverValue_10() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_fLeverValue_10)); }
	inline SingleU5BU5D_t577127397* get_m_fLeverValue_10() const { return ___m_fLeverValue_10; }
	inline SingleU5BU5D_t577127397** get_address_of_m_fLeverValue_10() { return &___m_fLeverValue_10; }
	inline void set_m_fLeverValue_10(SingleU5BU5D_t577127397* value)
	{
		___m_fLeverValue_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_fLeverValue_10, value);
	}

	inline static int32_t get_offset_of_m_bActionLever_11() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_bActionLever_11)); }
	inline bool get_m_bActionLever_11() const { return ___m_bActionLever_11; }
	inline bool* get_address_of_m_bActionLever_11() { return &___m_bActionLever_11; }
	inline void set_m_bActionLever_11(bool value)
	{
		___m_bActionLever_11 = value;
	}

	inline static int32_t get_offset_of_ResetPosCoolTime_12() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___ResetPosCoolTime_12)); }
	inline float get_ResetPosCoolTime_12() const { return ___ResetPosCoolTime_12; }
	inline float* get_address_of_ResetPosCoolTime_12() { return &___ResetPosCoolTime_12; }
	inline void set_ResetPosCoolTime_12(float value)
	{
		___ResetPosCoolTime_12 = value;
	}

	inline static int32_t get_offset_of_nearmobObjList_13() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___nearmobObjList_13)); }
	inline List_1_t1125654279 * get_nearmobObjList_13() const { return ___nearmobObjList_13; }
	inline List_1_t1125654279 ** get_address_of_nearmobObjList_13() { return &___nearmobObjList_13; }
	inline void set_nearmobObjList_13(List_1_t1125654279 * value)
	{
		___nearmobObjList_13 = value;
		Il2CppCodeGenWriteBarrier(&___nearmobObjList_13, value);
	}

	inline static int32_t get_offset_of_m_RecordingCreObj_14() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_RecordingCreObj_14)); }
	inline GameObject_t1756533147 * get_m_RecordingCreObj_14() const { return ___m_RecordingCreObj_14; }
	inline GameObject_t1756533147 ** get_address_of_m_RecordingCreObj_14() { return &___m_RecordingCreObj_14; }
	inline void set_m_RecordingCreObj_14(GameObject_t1756533147 * value)
	{
		___m_RecordingCreObj_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_RecordingCreObj_14, value);
	}

	inline static int32_t get_offset_of_m_MyPageCreObj_15() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_MyPageCreObj_15)); }
	inline GameObject_t1756533147 * get_m_MyPageCreObj_15() const { return ___m_MyPageCreObj_15; }
	inline GameObject_t1756533147 ** get_address_of_m_MyPageCreObj_15() { return &___m_MyPageCreObj_15; }
	inline void set_m_MyPageCreObj_15(GameObject_t1756533147 * value)
	{
		___m_MyPageCreObj_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_MyPageCreObj_15, value);
	}

	inline static int32_t get_offset_of_corePastsChildTra_16() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___corePastsChildTra_16)); }
	inline TransformU5BU5D_t3764228911* get_corePastsChildTra_16() const { return ___corePastsChildTra_16; }
	inline TransformU5BU5D_t3764228911** get_address_of_corePastsChildTra_16() { return &___corePastsChildTra_16; }
	inline void set_corePastsChildTra_16(TransformU5BU5D_t3764228911* value)
	{
		___corePastsChildTra_16 = value;
		Il2CppCodeGenWriteBarrier(&___corePastsChildTra_16, value);
	}

	inline static int32_t get_offset_of_childDrillerPositionX_17() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childDrillerPositionX_17)); }
	inline float get_childDrillerPositionX_17() const { return ___childDrillerPositionX_17; }
	inline float* get_address_of_childDrillerPositionX_17() { return &___childDrillerPositionX_17; }
	inline void set_childDrillerPositionX_17(float value)
	{
		___childDrillerPositionX_17 = value;
	}

	inline static int32_t get_offset_of_childDrillerPositionY_18() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childDrillerPositionY_18)); }
	inline float get_childDrillerPositionY_18() const { return ___childDrillerPositionY_18; }
	inline float* get_address_of_childDrillerPositionY_18() { return &___childDrillerPositionY_18; }
	inline void set_childDrillerPositionY_18(float value)
	{
		___childDrillerPositionY_18 = value;
	}

	inline static int32_t get_offset_of_childDrillerPositionZ_19() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childDrillerPositionZ_19)); }
	inline float get_childDrillerPositionZ_19() const { return ___childDrillerPositionZ_19; }
	inline float* get_address_of_childDrillerPositionZ_19() { return &___childDrillerPositionZ_19; }
	inline void set_childDrillerPositionZ_19(float value)
	{
		___childDrillerPositionZ_19 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationX_20() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childCenterRotationX_20)); }
	inline float get_childCenterRotationX_20() const { return ___childCenterRotationX_20; }
	inline float* get_address_of_childCenterRotationX_20() { return &___childCenterRotationX_20; }
	inline void set_childCenterRotationX_20(float value)
	{
		___childCenterRotationX_20 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationY_21() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childCenterRotationY_21)); }
	inline float get_childCenterRotationY_21() const { return ___childCenterRotationY_21; }
	inline float* get_address_of_childCenterRotationY_21() { return &___childCenterRotationY_21; }
	inline void set_childCenterRotationY_21(float value)
	{
		___childCenterRotationY_21 = value;
	}

	inline static int32_t get_offset_of_childCenterRotationZ_22() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childCenterRotationZ_22)); }
	inline float get_childCenterRotationZ_22() const { return ___childCenterRotationZ_22; }
	inline float* get_address_of_childCenterRotationZ_22() { return &___childCenterRotationZ_22; }
	inline void set_childCenterRotationZ_22(float value)
	{
		___childCenterRotationZ_22 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionX_23() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsPositionX_23)); }
	inline float get_childPartsPositionX_23() const { return ___childPartsPositionX_23; }
	inline float* get_address_of_childPartsPositionX_23() { return &___childPartsPositionX_23; }
	inline void set_childPartsPositionX_23(float value)
	{
		___childPartsPositionX_23 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionY_24() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsPositionY_24)); }
	inline float get_childPartsPositionY_24() const { return ___childPartsPositionY_24; }
	inline float* get_address_of_childPartsPositionY_24() { return &___childPartsPositionY_24; }
	inline void set_childPartsPositionY_24(float value)
	{
		___childPartsPositionY_24 = value;
	}

	inline static int32_t get_offset_of_childPartsPositionZ_25() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsPositionZ_25)); }
	inline float get_childPartsPositionZ_25() const { return ___childPartsPositionZ_25; }
	inline float* get_address_of_childPartsPositionZ_25() { return &___childPartsPositionZ_25; }
	inline void set_childPartsPositionZ_25(float value)
	{
		___childPartsPositionZ_25 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationX_26() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsRotationX_26)); }
	inline float get_childPartsRotationX_26() const { return ___childPartsRotationX_26; }
	inline float* get_address_of_childPartsRotationX_26() { return &___childPartsRotationX_26; }
	inline void set_childPartsRotationX_26(float value)
	{
		___childPartsRotationX_26 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationY_27() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsRotationY_27)); }
	inline float get_childPartsRotationY_27() const { return ___childPartsRotationY_27; }
	inline float* get_address_of_childPartsRotationY_27() { return &___childPartsRotationY_27; }
	inline void set_childPartsRotationY_27(float value)
	{
		___childPartsRotationY_27 = value;
	}

	inline static int32_t get_offset_of_childPartsRotationZ_28() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsRotationZ_28)); }
	inline float get_childPartsRotationZ_28() const { return ___childPartsRotationZ_28; }
	inline float* get_address_of_childPartsRotationZ_28() { return &___childPartsRotationZ_28; }
	inline void set_childPartsRotationZ_28(float value)
	{
		___childPartsRotationZ_28 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleX_29() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsScaleX_29)); }
	inline float get_childPartsScaleX_29() const { return ___childPartsScaleX_29; }
	inline float* get_address_of_childPartsScaleX_29() { return &___childPartsScaleX_29; }
	inline void set_childPartsScaleX_29(float value)
	{
		___childPartsScaleX_29 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleY_30() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsScaleY_30)); }
	inline float get_childPartsScaleY_30() const { return ___childPartsScaleY_30; }
	inline float* get_address_of_childPartsScaleY_30() { return &___childPartsScaleY_30; }
	inline void set_childPartsScaleY_30(float value)
	{
		___childPartsScaleY_30 = value;
	}

	inline static int32_t get_offset_of_childPartsScaleZ_31() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsScaleZ_31)); }
	inline float get_childPartsScaleZ_31() const { return ___childPartsScaleZ_31; }
	inline float* get_address_of_childPartsScaleZ_31() { return &___childPartsScaleZ_31; }
	inline void set_childPartsScaleZ_31(float value)
	{
		___childPartsScaleZ_31 = value;
	}

	inline static int32_t get_offset_of_parentPartsJointNo_32() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___parentPartsJointNo_32)); }
	inline int32_t get_parentPartsJointNo_32() const { return ___parentPartsJointNo_32; }
	inline int32_t* get_address_of_parentPartsJointNo_32() { return &___parentPartsJointNo_32; }
	inline void set_parentPartsJointNo_32(int32_t value)
	{
		___parentPartsJointNo_32 = value;
	}

	inline static int32_t get_offset_of_childPartsJointNo_33() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___childPartsJointNo_33)); }
	inline int32_t get_childPartsJointNo_33() const { return ___childPartsJointNo_33; }
	inline int32_t* get_address_of_childPartsJointNo_33() { return &___childPartsJointNo_33; }
	inline void set_childPartsJointNo_33(int32_t value)
	{
		___childPartsJointNo_33 = value;
	}

	inline static int32_t get_offset_of_mobilityObject_34() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___mobilityObject_34)); }
	inline GameObject_t1756533147 * get_mobilityObject_34() const { return ___mobilityObject_34; }
	inline GameObject_t1756533147 ** get_address_of_mobilityObject_34() { return &___mobilityObject_34; }
	inline void set_mobilityObject_34(GameObject_t1756533147 * value)
	{
		___mobilityObject_34 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityObject_34, value);
	}

	inline static int32_t get_offset_of_selectingcopyObj_35() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___selectingcopyObj_35)); }
	inline GameObject_t1756533147 * get_selectingcopyObj_35() const { return ___selectingcopyObj_35; }
	inline GameObject_t1756533147 ** get_address_of_selectingcopyObj_35() { return &___selectingcopyObj_35; }
	inline void set_selectingcopyObj_35(GameObject_t1756533147 * value)
	{
		___selectingcopyObj_35 = value;
		Il2CppCodeGenWriteBarrier(&___selectingcopyObj_35, value);
	}

	inline static int32_t get_offset_of_m_bPlayAnim_36() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_bPlayAnim_36)); }
	inline bool get_m_bPlayAnim_36() const { return ___m_bPlayAnim_36; }
	inline bool* get_address_of_m_bPlayAnim_36() { return &___m_bPlayAnim_36; }
	inline void set_m_bPlayAnim_36(bool value)
	{
		___m_bPlayAnim_36 = value;
	}

	inline static int32_t get_offset_of_m_bTutorialLever_37() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_bTutorialLever_37)); }
	inline bool get_m_bTutorialLever_37() const { return ___m_bTutorialLever_37; }
	inline bool* get_address_of_m_bTutorialLever_37() { return &___m_bTutorialLever_37; }
	inline void set_m_bTutorialLever_37(bool value)
	{
		___m_bTutorialLever_37 = value;
	}

	inline static int32_t get_offset_of_m_fLeverPermission_38() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___m_fLeverPermission_38)); }
	inline float get_m_fLeverPermission_38() const { return ___m_fLeverPermission_38; }
	inline float* get_address_of_m_fLeverPermission_38() { return &___m_fLeverPermission_38; }
	inline void set_m_fLeverPermission_38(float value)
	{
		___m_fLeverPermission_38 = value;
	}

	inline static int32_t get_offset_of_recreatedModuleObj_39() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___recreatedModuleObj_39)); }
	inline GameObject_t1756533147 * get_recreatedModuleObj_39() const { return ___recreatedModuleObj_39; }
	inline GameObject_t1756533147 ** get_address_of_recreatedModuleObj_39() { return &___recreatedModuleObj_39; }
	inline void set_recreatedModuleObj_39(GameObject_t1756533147 * value)
	{
		___recreatedModuleObj_39 = value;
		Il2CppCodeGenWriteBarrier(&___recreatedModuleObj_39, value);
	}

	inline static int32_t get_offset_of_MobilityNameText_40() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___MobilityNameText_40)); }
	inline Text_t356221433 * get_MobilityNameText_40() const { return ___MobilityNameText_40; }
	inline Text_t356221433 ** get_address_of_MobilityNameText_40() { return &___MobilityNameText_40; }
	inline void set_MobilityNameText_40(Text_t356221433 * value)
	{
		___MobilityNameText_40 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityNameText_40, value);
	}

	inline static int32_t get_offset_of_MobilityReactionText_41() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___MobilityReactionText_41)); }
	inline Text_t356221433 * get_MobilityReactionText_41() const { return ___MobilityReactionText_41; }
	inline Text_t356221433 ** get_address_of_MobilityReactionText_41() { return &___MobilityReactionText_41; }
	inline void set_MobilityReactionText_41(Text_t356221433 * value)
	{
		___MobilityReactionText_41 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityReactionText_41, value);
	}

	inline static int32_t get_offset_of_BreakedTextObj_42() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285, ___BreakedTextObj_42)); }
	inline GameObject_t1756533147 * get_BreakedTextObj_42() const { return ___BreakedTextObj_42; }
	inline GameObject_t1756533147 ** get_address_of_BreakedTextObj_42() { return &___BreakedTextObj_42; }
	inline void set_BreakedTextObj_42(GameObject_t1756533147 * value)
	{
		___BreakedTextObj_42 = value;
		Il2CppCodeGenWriteBarrier(&___BreakedTextObj_42, value);
	}
};

struct PlayerMobilmoManager_t2341644285_StaticFields
{
public:
	// DG.Tweening.TweenCallback PlayerMobilmoManager::<>f__am$cache0
	TweenCallback_t3697142134 * ___U3CU3Ef__amU24cache0_43;
	// DG.Tweening.TweenCallback PlayerMobilmoManager::<>f__am$cache1
	TweenCallback_t3697142134 * ___U3CU3Ef__amU24cache1_44;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_43() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285_StaticFields, ___U3CU3Ef__amU24cache0_43)); }
	inline TweenCallback_t3697142134 * get_U3CU3Ef__amU24cache0_43() const { return ___U3CU3Ef__amU24cache0_43; }
	inline TweenCallback_t3697142134 ** get_address_of_U3CU3Ef__amU24cache0_43() { return &___U3CU3Ef__amU24cache0_43; }
	inline void set_U3CU3Ef__amU24cache0_43(TweenCallback_t3697142134 * value)
	{
		___U3CU3Ef__amU24cache0_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_43, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_44() { return static_cast<int32_t>(offsetof(PlayerMobilmoManager_t2341644285_StaticFields, ___U3CU3Ef__amU24cache1_44)); }
	inline TweenCallback_t3697142134 * get_U3CU3Ef__amU24cache1_44() const { return ___U3CU3Ef__amU24cache1_44; }
	inline TweenCallback_t3697142134 ** get_address_of_U3CU3Ef__amU24cache1_44() { return &___U3CU3Ef__amU24cache1_44; }
	inline void set_U3CU3Ef__amU24cache1_44(TweenCallback_t3697142134 * value)
	{
		___U3CU3Ef__amU24cache1_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_44, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
