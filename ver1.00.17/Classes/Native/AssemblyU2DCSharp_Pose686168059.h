﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pose
struct  Pose_t686168059  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button Pose::Button
	Button_t2872111280 * ___Button_2;
	// UnityEngine.UI.Image Pose::Frame
	Image_t2042527209 * ___Frame_3;
	// UnityEngine.UI.RawImage Pose::Image
	RawImage_t2749640213 * ___Image_4;
	// UnityEngine.GameObject Pose::Delete
	GameObject_t1756533147 * ___Delete_5;
	// UnityEngine.UI.Button Pose::Plus
	Button_t2872111280 * ___Plus_6;
	// UnityEngine.UI.Image Pose::PlusImage
	Image_t2042527209 * ___PlusImage_7;

public:
	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(Pose_t686168059, ___Button_2)); }
	inline Button_t2872111280 * get_Button_2() const { return ___Button_2; }
	inline Button_t2872111280 ** get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(Button_t2872111280 * value)
	{
		___Button_2 = value;
		Il2CppCodeGenWriteBarrier(&___Button_2, value);
	}

	inline static int32_t get_offset_of_Frame_3() { return static_cast<int32_t>(offsetof(Pose_t686168059, ___Frame_3)); }
	inline Image_t2042527209 * get_Frame_3() const { return ___Frame_3; }
	inline Image_t2042527209 ** get_address_of_Frame_3() { return &___Frame_3; }
	inline void set_Frame_3(Image_t2042527209 * value)
	{
		___Frame_3 = value;
		Il2CppCodeGenWriteBarrier(&___Frame_3, value);
	}

	inline static int32_t get_offset_of_Image_4() { return static_cast<int32_t>(offsetof(Pose_t686168059, ___Image_4)); }
	inline RawImage_t2749640213 * get_Image_4() const { return ___Image_4; }
	inline RawImage_t2749640213 ** get_address_of_Image_4() { return &___Image_4; }
	inline void set_Image_4(RawImage_t2749640213 * value)
	{
		___Image_4 = value;
		Il2CppCodeGenWriteBarrier(&___Image_4, value);
	}

	inline static int32_t get_offset_of_Delete_5() { return static_cast<int32_t>(offsetof(Pose_t686168059, ___Delete_5)); }
	inline GameObject_t1756533147 * get_Delete_5() const { return ___Delete_5; }
	inline GameObject_t1756533147 ** get_address_of_Delete_5() { return &___Delete_5; }
	inline void set_Delete_5(GameObject_t1756533147 * value)
	{
		___Delete_5 = value;
		Il2CppCodeGenWriteBarrier(&___Delete_5, value);
	}

	inline static int32_t get_offset_of_Plus_6() { return static_cast<int32_t>(offsetof(Pose_t686168059, ___Plus_6)); }
	inline Button_t2872111280 * get_Plus_6() const { return ___Plus_6; }
	inline Button_t2872111280 ** get_address_of_Plus_6() { return &___Plus_6; }
	inline void set_Plus_6(Button_t2872111280 * value)
	{
		___Plus_6 = value;
		Il2CppCodeGenWriteBarrier(&___Plus_6, value);
	}

	inline static int32_t get_offset_of_PlusImage_7() { return static_cast<int32_t>(offsetof(Pose_t686168059, ___PlusImage_7)); }
	inline Image_t2042527209 * get_PlusImage_7() const { return ___PlusImage_7; }
	inline Image_t2042527209 ** get_address_of_PlusImage_7() { return &___PlusImage_7; }
	inline void set_PlusImage_7(Image_t2042527209 * value)
	{
		___PlusImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___PlusImage_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
