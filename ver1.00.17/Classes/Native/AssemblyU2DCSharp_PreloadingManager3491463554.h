﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// DownLoadAssetbundle
struct DownLoadAssetbundle_t3666029200;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PreloadingManager
struct  PreloadingManager_t3491463554  : public MonoBehaviour_t1158329972
{
public:
	// System.String PreloadingManager::urlPath
	String_t* ___urlPath_2;
	// UnityEngine.Transform PreloadingManager::Navit
	Transform_t3275118058 * ___Navit_3;
	// UnityEngine.UI.Image PreloadingManager::Guage
	Image_t2042527209 * ___Guage_4;
	// UnityEngine.UI.Text PreloadingManager::Raito
	Text_t356221433 * ___Raito_5;
	// UnityEngine.RectTransform PreloadingManager::PercentRectTrans
	RectTransform_t3349966182 * ___PercentRectTrans_6;
	// System.Single PreloadingManager::NavitSpeed
	float ___NavitSpeed_7;
	// UnityEngine.GameObject PreloadingManager::CompletePanel
	GameObject_t1756533147 * ___CompletePanel_8;
	// UnityEngine.Vector3 PreloadingManager::m_vRotNavit
	Vector3_t2243707580  ___m_vRotNavit_9;
	// UnityEngine.AsyncOperation PreloadingManager::m_Async
	AsyncOperation_t3814632279 * ___m_Async_10;
	// DownLoadAssetbundle PreloadingManager::m_DownLoadAssetbundle
	DownLoadAssetbundle_t3666029200 * ___m_DownLoadAssetbundle_11;
	// UnityEngine.TextAsset PreloadingManager::verText
	TextAsset_t3973159845 * ___verText_12;

public:
	inline static int32_t get_offset_of_urlPath_2() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___urlPath_2)); }
	inline String_t* get_urlPath_2() const { return ___urlPath_2; }
	inline String_t** get_address_of_urlPath_2() { return &___urlPath_2; }
	inline void set_urlPath_2(String_t* value)
	{
		___urlPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___urlPath_2, value);
	}

	inline static int32_t get_offset_of_Navit_3() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___Navit_3)); }
	inline Transform_t3275118058 * get_Navit_3() const { return ___Navit_3; }
	inline Transform_t3275118058 ** get_address_of_Navit_3() { return &___Navit_3; }
	inline void set_Navit_3(Transform_t3275118058 * value)
	{
		___Navit_3 = value;
		Il2CppCodeGenWriteBarrier(&___Navit_3, value);
	}

	inline static int32_t get_offset_of_Guage_4() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___Guage_4)); }
	inline Image_t2042527209 * get_Guage_4() const { return ___Guage_4; }
	inline Image_t2042527209 ** get_address_of_Guage_4() { return &___Guage_4; }
	inline void set_Guage_4(Image_t2042527209 * value)
	{
		___Guage_4 = value;
		Il2CppCodeGenWriteBarrier(&___Guage_4, value);
	}

	inline static int32_t get_offset_of_Raito_5() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___Raito_5)); }
	inline Text_t356221433 * get_Raito_5() const { return ___Raito_5; }
	inline Text_t356221433 ** get_address_of_Raito_5() { return &___Raito_5; }
	inline void set_Raito_5(Text_t356221433 * value)
	{
		___Raito_5 = value;
		Il2CppCodeGenWriteBarrier(&___Raito_5, value);
	}

	inline static int32_t get_offset_of_PercentRectTrans_6() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___PercentRectTrans_6)); }
	inline RectTransform_t3349966182 * get_PercentRectTrans_6() const { return ___PercentRectTrans_6; }
	inline RectTransform_t3349966182 ** get_address_of_PercentRectTrans_6() { return &___PercentRectTrans_6; }
	inline void set_PercentRectTrans_6(RectTransform_t3349966182 * value)
	{
		___PercentRectTrans_6 = value;
		Il2CppCodeGenWriteBarrier(&___PercentRectTrans_6, value);
	}

	inline static int32_t get_offset_of_NavitSpeed_7() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___NavitSpeed_7)); }
	inline float get_NavitSpeed_7() const { return ___NavitSpeed_7; }
	inline float* get_address_of_NavitSpeed_7() { return &___NavitSpeed_7; }
	inline void set_NavitSpeed_7(float value)
	{
		___NavitSpeed_7 = value;
	}

	inline static int32_t get_offset_of_CompletePanel_8() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___CompletePanel_8)); }
	inline GameObject_t1756533147 * get_CompletePanel_8() const { return ___CompletePanel_8; }
	inline GameObject_t1756533147 ** get_address_of_CompletePanel_8() { return &___CompletePanel_8; }
	inline void set_CompletePanel_8(GameObject_t1756533147 * value)
	{
		___CompletePanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___CompletePanel_8, value);
	}

	inline static int32_t get_offset_of_m_vRotNavit_9() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___m_vRotNavit_9)); }
	inline Vector3_t2243707580  get_m_vRotNavit_9() const { return ___m_vRotNavit_9; }
	inline Vector3_t2243707580 * get_address_of_m_vRotNavit_9() { return &___m_vRotNavit_9; }
	inline void set_m_vRotNavit_9(Vector3_t2243707580  value)
	{
		___m_vRotNavit_9 = value;
	}

	inline static int32_t get_offset_of_m_Async_10() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___m_Async_10)); }
	inline AsyncOperation_t3814632279 * get_m_Async_10() const { return ___m_Async_10; }
	inline AsyncOperation_t3814632279 ** get_address_of_m_Async_10() { return &___m_Async_10; }
	inline void set_m_Async_10(AsyncOperation_t3814632279 * value)
	{
		___m_Async_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Async_10, value);
	}

	inline static int32_t get_offset_of_m_DownLoadAssetbundle_11() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___m_DownLoadAssetbundle_11)); }
	inline DownLoadAssetbundle_t3666029200 * get_m_DownLoadAssetbundle_11() const { return ___m_DownLoadAssetbundle_11; }
	inline DownLoadAssetbundle_t3666029200 ** get_address_of_m_DownLoadAssetbundle_11() { return &___m_DownLoadAssetbundle_11; }
	inline void set_m_DownLoadAssetbundle_11(DownLoadAssetbundle_t3666029200 * value)
	{
		___m_DownLoadAssetbundle_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_DownLoadAssetbundle_11, value);
	}

	inline static int32_t get_offset_of_verText_12() { return static_cast<int32_t>(offsetof(PreloadingManager_t3491463554, ___verText_12)); }
	inline TextAsset_t3973159845 * get_verText_12() const { return ___verText_12; }
	inline TextAsset_t3973159845 ** get_address_of_verText_12() { return &___verText_12; }
	inline void set_verText_12(TextAsset_t3973159845 * value)
	{
		___verText_12 = value;
		Il2CppCodeGenWriteBarrier(&___verText_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
