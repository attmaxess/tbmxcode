﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PremitivePartContent
struct  PremitivePartContent_t3696216029  : public Il2CppObject
{
public:
	// System.String PremitivePartContent::name
	String_t* ___name_0;
	// UnityEngine.Sprite PremitivePartContent::image
	Sprite_t309593783 * ___image_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(PremitivePartContent_t3696216029, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_image_1() { return static_cast<int32_t>(offsetof(PremitivePartContent_t3696216029, ___image_1)); }
	inline Sprite_t309593783 * get_image_1() const { return ___image_1; }
	inline Sprite_t309593783 ** get_address_of_image_1() { return &___image_1; }
	inline void set_image_1(Sprite_t309593783 * value)
	{
		___image_1 = value;
		Il2CppCodeGenWriteBarrier(&___image_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
