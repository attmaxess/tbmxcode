﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrivacyPanel
struct  PrivacyPanel_t1464948090  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform PrivacyPanel::Label
	RectTransform_t3349966182 * ___Label_2;
	// UnityEngine.RectTransform PrivacyPanel::ScrollTextPanel
	RectTransform_t3349966182 * ___ScrollTextPanel_3;
	// System.Boolean PrivacyPanel::IsScrolled
	bool ___IsScrolled_4;

public:
	inline static int32_t get_offset_of_Label_2() { return static_cast<int32_t>(offsetof(PrivacyPanel_t1464948090, ___Label_2)); }
	inline RectTransform_t3349966182 * get_Label_2() const { return ___Label_2; }
	inline RectTransform_t3349966182 ** get_address_of_Label_2() { return &___Label_2; }
	inline void set_Label_2(RectTransform_t3349966182 * value)
	{
		___Label_2 = value;
		Il2CppCodeGenWriteBarrier(&___Label_2, value);
	}

	inline static int32_t get_offset_of_ScrollTextPanel_3() { return static_cast<int32_t>(offsetof(PrivacyPanel_t1464948090, ___ScrollTextPanel_3)); }
	inline RectTransform_t3349966182 * get_ScrollTextPanel_3() const { return ___ScrollTextPanel_3; }
	inline RectTransform_t3349966182 ** get_address_of_ScrollTextPanel_3() { return &___ScrollTextPanel_3; }
	inline void set_ScrollTextPanel_3(RectTransform_t3349966182 * value)
	{
		___ScrollTextPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___ScrollTextPanel_3, value);
	}

	inline static int32_t get_offset_of_IsScrolled_4() { return static_cast<int32_t>(offsetof(PrivacyPanel_t1464948090, ___IsScrolled_4)); }
	inline bool get_IsScrolled_4() const { return ___IsScrolled_4; }
	inline bool* get_address_of_IsScrolled_4() { return &___IsScrolled_4; }
	inline void set_IsScrolled_4(bool value)
	{
		___IsScrolled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
