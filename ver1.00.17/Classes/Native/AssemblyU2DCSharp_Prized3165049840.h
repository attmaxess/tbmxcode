﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prized
struct  Prized_t3165049840  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image Prized::PrizeImage
	Image_t2042527209 * ___PrizeImage_2;
	// UnityEngine.UI.Text Prized::PrizedText
	Text_t356221433 * ___PrizedText_3;
	// UnityEngine.UI.Image Prized::Background
	Image_t2042527209 * ___Background_4;
	// System.Single Prized::RotateSpeed
	float ___RotateSpeed_5;
	// System.Single Prized::FadeTime
	float ___FadeTime_6;
	// System.Int32 Prized::prizedIdcol
	int32_t ___prizedIdcol_7;
	// System.Boolean Prized::IsDisp
	bool ___IsDisp_8;

public:
	inline static int32_t get_offset_of_PrizeImage_2() { return static_cast<int32_t>(offsetof(Prized_t3165049840, ___PrizeImage_2)); }
	inline Image_t2042527209 * get_PrizeImage_2() const { return ___PrizeImage_2; }
	inline Image_t2042527209 ** get_address_of_PrizeImage_2() { return &___PrizeImage_2; }
	inline void set_PrizeImage_2(Image_t2042527209 * value)
	{
		___PrizeImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___PrizeImage_2, value);
	}

	inline static int32_t get_offset_of_PrizedText_3() { return static_cast<int32_t>(offsetof(Prized_t3165049840, ___PrizedText_3)); }
	inline Text_t356221433 * get_PrizedText_3() const { return ___PrizedText_3; }
	inline Text_t356221433 ** get_address_of_PrizedText_3() { return &___PrizedText_3; }
	inline void set_PrizedText_3(Text_t356221433 * value)
	{
		___PrizedText_3 = value;
		Il2CppCodeGenWriteBarrier(&___PrizedText_3, value);
	}

	inline static int32_t get_offset_of_Background_4() { return static_cast<int32_t>(offsetof(Prized_t3165049840, ___Background_4)); }
	inline Image_t2042527209 * get_Background_4() const { return ___Background_4; }
	inline Image_t2042527209 ** get_address_of_Background_4() { return &___Background_4; }
	inline void set_Background_4(Image_t2042527209 * value)
	{
		___Background_4 = value;
		Il2CppCodeGenWriteBarrier(&___Background_4, value);
	}

	inline static int32_t get_offset_of_RotateSpeed_5() { return static_cast<int32_t>(offsetof(Prized_t3165049840, ___RotateSpeed_5)); }
	inline float get_RotateSpeed_5() const { return ___RotateSpeed_5; }
	inline float* get_address_of_RotateSpeed_5() { return &___RotateSpeed_5; }
	inline void set_RotateSpeed_5(float value)
	{
		___RotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_FadeTime_6() { return static_cast<int32_t>(offsetof(Prized_t3165049840, ___FadeTime_6)); }
	inline float get_FadeTime_6() const { return ___FadeTime_6; }
	inline float* get_address_of_FadeTime_6() { return &___FadeTime_6; }
	inline void set_FadeTime_6(float value)
	{
		___FadeTime_6 = value;
	}

	inline static int32_t get_offset_of_prizedIdcol_7() { return static_cast<int32_t>(offsetof(Prized_t3165049840, ___prizedIdcol_7)); }
	inline int32_t get_prizedIdcol_7() const { return ___prizedIdcol_7; }
	inline int32_t* get_address_of_prizedIdcol_7() { return &___prizedIdcol_7; }
	inline void set_prizedIdcol_7(int32_t value)
	{
		___prizedIdcol_7 = value;
	}

	inline static int32_t get_offset_of_IsDisp_8() { return static_cast<int32_t>(offsetof(Prized_t3165049840, ___IsDisp_8)); }
	inline bool get_IsDisp_8() const { return ___IsDisp_8; }
	inline bool* get_address_of_IsDisp_8() { return &___IsDisp_8; }
	inline void set_IsDisp_8(bool value)
	{
		___IsDisp_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
