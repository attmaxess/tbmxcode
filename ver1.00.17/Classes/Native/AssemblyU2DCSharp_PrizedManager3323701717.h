﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2020378052.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrizedManager
struct  PrizedManager_t3323701717  : public SingletonMonoBehaviour_1_t2020378052
{
public:
	// System.Collections.Generic.List`1<System.Int32> PrizedManager::_prizedList
	List_1_t1440998580 * ____prizedList_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PrizedManager::_mobilmoDic
	Dictionary_2_t3986656710 * ____mobilmoDic_4;
	// System.Collections.Generic.List`1<System.Int32> PrizedManager::PrizedChargeList
	List_1_t1440998580 * ___PrizedChargeList_5;

public:
	inline static int32_t get_offset_of__prizedList_3() { return static_cast<int32_t>(offsetof(PrizedManager_t3323701717, ____prizedList_3)); }
	inline List_1_t1440998580 * get__prizedList_3() const { return ____prizedList_3; }
	inline List_1_t1440998580 ** get_address_of__prizedList_3() { return &____prizedList_3; }
	inline void set__prizedList_3(List_1_t1440998580 * value)
	{
		____prizedList_3 = value;
		Il2CppCodeGenWriteBarrier(&____prizedList_3, value);
	}

	inline static int32_t get_offset_of__mobilmoDic_4() { return static_cast<int32_t>(offsetof(PrizedManager_t3323701717, ____mobilmoDic_4)); }
	inline Dictionary_2_t3986656710 * get__mobilmoDic_4() const { return ____mobilmoDic_4; }
	inline Dictionary_2_t3986656710 ** get_address_of__mobilmoDic_4() { return &____mobilmoDic_4; }
	inline void set__mobilmoDic_4(Dictionary_2_t3986656710 * value)
	{
		____mobilmoDic_4 = value;
		Il2CppCodeGenWriteBarrier(&____mobilmoDic_4, value);
	}

	inline static int32_t get_offset_of_PrizedChargeList_5() { return static_cast<int32_t>(offsetof(PrizedManager_t3323701717, ___PrizedChargeList_5)); }
	inline List_1_t1440998580 * get_PrizedChargeList_5() const { return ___PrizedChargeList_5; }
	inline List_1_t1440998580 ** get_address_of_PrizedChargeList_5() { return &___PrizedChargeList_5; }
	inline void set_PrizedChargeList_5(List_1_t1440998580 * value)
	{
		___PrizedChargeList_5 = value;
		Il2CppCodeGenWriteBarrier(&___PrizedChargeList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
