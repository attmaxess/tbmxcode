﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectorMatrix
struct  ProjectorMatrix_t3999868417  : public MonoBehaviour_t1158329972
{
public:
	// System.String[] ProjectorMatrix::GlobalMatrixNames
	StringU5BU5D_t1642385972* ___GlobalMatrixNames_2;
	// UnityEngine.Transform[] ProjectorMatrix::ProjectiveTranforms
	TransformU5BU5D_t3764228911* ___ProjectiveTranforms_3;
	// System.Boolean ProjectorMatrix::UpdateOnStart
	bool ___UpdateOnStart_4;
	// System.Boolean ProjectorMatrix::CanUpdate
	bool ___CanUpdate_5;
	// UnityEngine.Transform ProjectorMatrix::t
	Transform_t3275118058 * ___t_6;

public:
	inline static int32_t get_offset_of_GlobalMatrixNames_2() { return static_cast<int32_t>(offsetof(ProjectorMatrix_t3999868417, ___GlobalMatrixNames_2)); }
	inline StringU5BU5D_t1642385972* get_GlobalMatrixNames_2() const { return ___GlobalMatrixNames_2; }
	inline StringU5BU5D_t1642385972** get_address_of_GlobalMatrixNames_2() { return &___GlobalMatrixNames_2; }
	inline void set_GlobalMatrixNames_2(StringU5BU5D_t1642385972* value)
	{
		___GlobalMatrixNames_2 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalMatrixNames_2, value);
	}

	inline static int32_t get_offset_of_ProjectiveTranforms_3() { return static_cast<int32_t>(offsetof(ProjectorMatrix_t3999868417, ___ProjectiveTranforms_3)); }
	inline TransformU5BU5D_t3764228911* get_ProjectiveTranforms_3() const { return ___ProjectiveTranforms_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_ProjectiveTranforms_3() { return &___ProjectiveTranforms_3; }
	inline void set_ProjectiveTranforms_3(TransformU5BU5D_t3764228911* value)
	{
		___ProjectiveTranforms_3 = value;
		Il2CppCodeGenWriteBarrier(&___ProjectiveTranforms_3, value);
	}

	inline static int32_t get_offset_of_UpdateOnStart_4() { return static_cast<int32_t>(offsetof(ProjectorMatrix_t3999868417, ___UpdateOnStart_4)); }
	inline bool get_UpdateOnStart_4() const { return ___UpdateOnStart_4; }
	inline bool* get_address_of_UpdateOnStart_4() { return &___UpdateOnStart_4; }
	inline void set_UpdateOnStart_4(bool value)
	{
		___UpdateOnStart_4 = value;
	}

	inline static int32_t get_offset_of_CanUpdate_5() { return static_cast<int32_t>(offsetof(ProjectorMatrix_t3999868417, ___CanUpdate_5)); }
	inline bool get_CanUpdate_5() const { return ___CanUpdate_5; }
	inline bool* get_address_of_CanUpdate_5() { return &___CanUpdate_5; }
	inline void set_CanUpdate_5(bool value)
	{
		___CanUpdate_5 = value;
	}

	inline static int32_t get_offset_of_t_6() { return static_cast<int32_t>(offsetof(ProjectorMatrix_t3999868417, ___t_6)); }
	inline Transform_t3275118058 * get_t_6() const { return ___t_6; }
	inline Transform_t3275118058 ** get_address_of_t_6() { return &___t_6; }
	inline void set_t_6(Transform_t3275118058 * value)
	{
		___t_6 = value;
		Il2CppCodeGenWriteBarrier(&___t_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
