﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaderController
struct  RaderController_t1714548872  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite[] RaderController::buttonSprite
	SpriteU5BU5D_t3359083662* ___buttonSprite_2;
	// UnityEngine.UI.Button RaderController::newBilmoButton
	Button_t2872111280 * ___newBilmoButton_3;
	// UnityEngine.UI.Button RaderController::likeRankingButton
	Button_t2872111280 * ___likeRankingButton_4;
	// UnityEngine.GameObject[] RaderController::mobimoDnaPanel
	GameObjectU5BU5D_t3057952154* ___mobimoDnaPanel_5;
	// UnityEngine.GameObject[] RaderController::raderUI
	GameObjectU5BU5D_t3057952154* ___raderUI_6;
	// System.Boolean RaderController::isMobilmoDna
	bool ___isMobilmoDna_7;
	// UnityEngine.Vector3 RaderController::dragedPos
	Vector3_t2243707580  ___dragedPos_8;
	// UnityEngine.Vector3 RaderController::originalLocalPointerPosition
	Vector3_t2243707580  ___originalLocalPointerPosition_9;
	// UnityEngine.Vector3 RaderController::originalPanelLocalPosition
	Vector3_t2243707580  ___originalPanelLocalPosition_10;
	// UnityEngine.RectTransform RaderController::panelRectTransform
	RectTransform_t3349966182 * ___panelRectTransform_11;
	// System.Boolean RaderController::isMove
	bool ___isMove_12;

public:
	inline static int32_t get_offset_of_buttonSprite_2() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___buttonSprite_2)); }
	inline SpriteU5BU5D_t3359083662* get_buttonSprite_2() const { return ___buttonSprite_2; }
	inline SpriteU5BU5D_t3359083662** get_address_of_buttonSprite_2() { return &___buttonSprite_2; }
	inline void set_buttonSprite_2(SpriteU5BU5D_t3359083662* value)
	{
		___buttonSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_2, value);
	}

	inline static int32_t get_offset_of_newBilmoButton_3() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___newBilmoButton_3)); }
	inline Button_t2872111280 * get_newBilmoButton_3() const { return ___newBilmoButton_3; }
	inline Button_t2872111280 ** get_address_of_newBilmoButton_3() { return &___newBilmoButton_3; }
	inline void set_newBilmoButton_3(Button_t2872111280 * value)
	{
		___newBilmoButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___newBilmoButton_3, value);
	}

	inline static int32_t get_offset_of_likeRankingButton_4() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___likeRankingButton_4)); }
	inline Button_t2872111280 * get_likeRankingButton_4() const { return ___likeRankingButton_4; }
	inline Button_t2872111280 ** get_address_of_likeRankingButton_4() { return &___likeRankingButton_4; }
	inline void set_likeRankingButton_4(Button_t2872111280 * value)
	{
		___likeRankingButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___likeRankingButton_4, value);
	}

	inline static int32_t get_offset_of_mobimoDnaPanel_5() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___mobimoDnaPanel_5)); }
	inline GameObjectU5BU5D_t3057952154* get_mobimoDnaPanel_5() const { return ___mobimoDnaPanel_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_mobimoDnaPanel_5() { return &___mobimoDnaPanel_5; }
	inline void set_mobimoDnaPanel_5(GameObjectU5BU5D_t3057952154* value)
	{
		___mobimoDnaPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___mobimoDnaPanel_5, value);
	}

	inline static int32_t get_offset_of_raderUI_6() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___raderUI_6)); }
	inline GameObjectU5BU5D_t3057952154* get_raderUI_6() const { return ___raderUI_6; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_raderUI_6() { return &___raderUI_6; }
	inline void set_raderUI_6(GameObjectU5BU5D_t3057952154* value)
	{
		___raderUI_6 = value;
		Il2CppCodeGenWriteBarrier(&___raderUI_6, value);
	}

	inline static int32_t get_offset_of_isMobilmoDna_7() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___isMobilmoDna_7)); }
	inline bool get_isMobilmoDna_7() const { return ___isMobilmoDna_7; }
	inline bool* get_address_of_isMobilmoDna_7() { return &___isMobilmoDna_7; }
	inline void set_isMobilmoDna_7(bool value)
	{
		___isMobilmoDna_7 = value;
	}

	inline static int32_t get_offset_of_dragedPos_8() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___dragedPos_8)); }
	inline Vector3_t2243707580  get_dragedPos_8() const { return ___dragedPos_8; }
	inline Vector3_t2243707580 * get_address_of_dragedPos_8() { return &___dragedPos_8; }
	inline void set_dragedPos_8(Vector3_t2243707580  value)
	{
		___dragedPos_8 = value;
	}

	inline static int32_t get_offset_of_originalLocalPointerPosition_9() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___originalLocalPointerPosition_9)); }
	inline Vector3_t2243707580  get_originalLocalPointerPosition_9() const { return ___originalLocalPointerPosition_9; }
	inline Vector3_t2243707580 * get_address_of_originalLocalPointerPosition_9() { return &___originalLocalPointerPosition_9; }
	inline void set_originalLocalPointerPosition_9(Vector3_t2243707580  value)
	{
		___originalLocalPointerPosition_9 = value;
	}

	inline static int32_t get_offset_of_originalPanelLocalPosition_10() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___originalPanelLocalPosition_10)); }
	inline Vector3_t2243707580  get_originalPanelLocalPosition_10() const { return ___originalPanelLocalPosition_10; }
	inline Vector3_t2243707580 * get_address_of_originalPanelLocalPosition_10() { return &___originalPanelLocalPosition_10; }
	inline void set_originalPanelLocalPosition_10(Vector3_t2243707580  value)
	{
		___originalPanelLocalPosition_10 = value;
	}

	inline static int32_t get_offset_of_panelRectTransform_11() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___panelRectTransform_11)); }
	inline RectTransform_t3349966182 * get_panelRectTransform_11() const { return ___panelRectTransform_11; }
	inline RectTransform_t3349966182 ** get_address_of_panelRectTransform_11() { return &___panelRectTransform_11; }
	inline void set_panelRectTransform_11(RectTransform_t3349966182 * value)
	{
		___panelRectTransform_11 = value;
		Il2CppCodeGenWriteBarrier(&___panelRectTransform_11, value);
	}

	inline static int32_t get_offset_of_isMove_12() { return static_cast<int32_t>(offsetof(RaderController_t1714548872, ___isMove_12)); }
	inline bool get_isMove_12() const { return ___isMove_12; }
	inline bool* get_address_of_isMove_12() { return &___isMove_12; }
	inline void set_isMove_12(bool value)
	{
		___isMove_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
