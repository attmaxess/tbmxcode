﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2309347870.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_PrimitiveType2454390065.h"
#include "AssemblyU2DCSharp_RayManager_OPERATION_TYPE1833855786.h"

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RayManager
struct  RayManager_t3612671535  : public SingletonMonoBehaviour_1_t2309347870
{
public:
	// UnityEngine.RaycastHit RayManager::m_RayCast
	RaycastHit_t87180320  ___m_RayCast_3;
	// UnityEngine.Camera RayManager::m_mainCamera
	Camera_t189460977 * ___m_mainCamera_4;
	// UnityEngine.GameObject RayManager::m_CreObj
	GameObject_t1756533147 * ___m_CreObj_5;
	// UnityEngine.Vector3 RayManager::m_vMousePos
	Vector3_t2243707580  ___m_vMousePos_6;
	// UnityEngine.PrimitiveType RayManager::m_PriType
	int32_t ___m_PriType_7;
	// RayManager/OPERATION_TYPE RayManager::m_eOpeType
	int32_t ___m_eOpeType_8;

public:
	inline static int32_t get_offset_of_m_RayCast_3() { return static_cast<int32_t>(offsetof(RayManager_t3612671535, ___m_RayCast_3)); }
	inline RaycastHit_t87180320  get_m_RayCast_3() const { return ___m_RayCast_3; }
	inline RaycastHit_t87180320 * get_address_of_m_RayCast_3() { return &___m_RayCast_3; }
	inline void set_m_RayCast_3(RaycastHit_t87180320  value)
	{
		___m_RayCast_3 = value;
	}

	inline static int32_t get_offset_of_m_mainCamera_4() { return static_cast<int32_t>(offsetof(RayManager_t3612671535, ___m_mainCamera_4)); }
	inline Camera_t189460977 * get_m_mainCamera_4() const { return ___m_mainCamera_4; }
	inline Camera_t189460977 ** get_address_of_m_mainCamera_4() { return &___m_mainCamera_4; }
	inline void set_m_mainCamera_4(Camera_t189460977 * value)
	{
		___m_mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_mainCamera_4, value);
	}

	inline static int32_t get_offset_of_m_CreObj_5() { return static_cast<int32_t>(offsetof(RayManager_t3612671535, ___m_CreObj_5)); }
	inline GameObject_t1756533147 * get_m_CreObj_5() const { return ___m_CreObj_5; }
	inline GameObject_t1756533147 ** get_address_of_m_CreObj_5() { return &___m_CreObj_5; }
	inline void set_m_CreObj_5(GameObject_t1756533147 * value)
	{
		___m_CreObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_CreObj_5, value);
	}

	inline static int32_t get_offset_of_m_vMousePos_6() { return static_cast<int32_t>(offsetof(RayManager_t3612671535, ___m_vMousePos_6)); }
	inline Vector3_t2243707580  get_m_vMousePos_6() const { return ___m_vMousePos_6; }
	inline Vector3_t2243707580 * get_address_of_m_vMousePos_6() { return &___m_vMousePos_6; }
	inline void set_m_vMousePos_6(Vector3_t2243707580  value)
	{
		___m_vMousePos_6 = value;
	}

	inline static int32_t get_offset_of_m_PriType_7() { return static_cast<int32_t>(offsetof(RayManager_t3612671535, ___m_PriType_7)); }
	inline int32_t get_m_PriType_7() const { return ___m_PriType_7; }
	inline int32_t* get_address_of_m_PriType_7() { return &___m_PriType_7; }
	inline void set_m_PriType_7(int32_t value)
	{
		___m_PriType_7 = value;
	}

	inline static int32_t get_offset_of_m_eOpeType_8() { return static_cast<int32_t>(offsetof(RayManager_t3612671535, ___m_eOpeType_8)); }
	inline int32_t get_m_eOpeType_8() const { return ___m_eOpeType_8; }
	inline int32_t* get_address_of_m_eOpeType_8() { return &___m_eOpeType_8; }
	inline void set_m_eOpeType_8(int32_t value)
	{
		___m_eOpeType_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
