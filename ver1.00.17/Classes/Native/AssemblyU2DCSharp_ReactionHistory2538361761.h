﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1235038096.h"

// UnityEngine.Events.UnityAction`3<System.String,System.String,System.String>
struct UnityAction_3_t1327975858;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactionHistory
struct  ReactionHistory_t2538361761  : public SingletonMonoBehaviour_1_t1235038096
{
public:

public:
};

struct ReactionHistory_t2538361761_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<System.String,System.String,System.String> ReactionHistory::OnGetDataUserLikeEvent
	UnityAction_3_t1327975858 * ___OnGetDataUserLikeEvent_3;
	// UnityEngine.Events.UnityAction ReactionHistory::OnAnalizeDataEvent
	UnityAction_t4025899511 * ___OnAnalizeDataEvent_4;

public:
	inline static int32_t get_offset_of_OnGetDataUserLikeEvent_3() { return static_cast<int32_t>(offsetof(ReactionHistory_t2538361761_StaticFields, ___OnGetDataUserLikeEvent_3)); }
	inline UnityAction_3_t1327975858 * get_OnGetDataUserLikeEvent_3() const { return ___OnGetDataUserLikeEvent_3; }
	inline UnityAction_3_t1327975858 ** get_address_of_OnGetDataUserLikeEvent_3() { return &___OnGetDataUserLikeEvent_3; }
	inline void set_OnGetDataUserLikeEvent_3(UnityAction_3_t1327975858 * value)
	{
		___OnGetDataUserLikeEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnGetDataUserLikeEvent_3, value);
	}

	inline static int32_t get_offset_of_OnAnalizeDataEvent_4() { return static_cast<int32_t>(offsetof(ReactionHistory_t2538361761_StaticFields, ___OnAnalizeDataEvent_4)); }
	inline UnityAction_t4025899511 * get_OnAnalizeDataEvent_4() const { return ___OnAnalizeDataEvent_4; }
	inline UnityAction_t4025899511 ** get_address_of_OnAnalizeDataEvent_4() { return &___OnAnalizeDataEvent_4; }
	inline void set_OnAnalizeDataEvent_4(UnityAction_t4025899511 * value)
	{
		___OnAnalizeDataEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnAnalizeDataEvent_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
