﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactionList
struct  ReactionList_t1054868761  : public Il2CppObject
{
public:
	// System.String ReactionList::ReactionTypeList
	String_t* ___ReactionTypeList_0;
	// System.Int32 ReactionList::ReactionCount
	int32_t ___ReactionCount_1;

public:
	inline static int32_t get_offset_of_ReactionTypeList_0() { return static_cast<int32_t>(offsetof(ReactionList_t1054868761, ___ReactionTypeList_0)); }
	inline String_t* get_ReactionTypeList_0() const { return ___ReactionTypeList_0; }
	inline String_t** get_address_of_ReactionTypeList_0() { return &___ReactionTypeList_0; }
	inline void set_ReactionTypeList_0(String_t* value)
	{
		___ReactionTypeList_0 = value;
		Il2CppCodeGenWriteBarrier(&___ReactionTypeList_0, value);
	}

	inline static int32_t get_offset_of_ReactionCount_1() { return static_cast<int32_t>(offsetof(ReactionList_t1054868761, ___ReactionCount_1)); }
	inline int32_t get_ReactionCount_1() const { return ___ReactionCount_1; }
	inline int32_t* get_address_of_ReactionCount_1() { return &___ReactionCount_1; }
	inline void set_ReactionCount_1(int32_t value)
	{
		___ReactionCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
