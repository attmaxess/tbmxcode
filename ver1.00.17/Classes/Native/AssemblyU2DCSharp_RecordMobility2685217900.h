﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// MobilmoDnaController
struct MobilmoDnaController_t1006271200;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordMobility
struct  RecordMobility_t2685217900  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform RecordMobility::m_MobilityArea
	Transform_t3275118058 * ___m_MobilityArea_2;
	// UnityEngine.GameObject RecordMobility::Controller
	GameObject_t1756533147 * ___Controller_3;
	// System.String RecordMobility::MobilityJson
	String_t* ___MobilityJson_4;
	// UnityEngine.GameObject[] RecordMobility::FakeMob
	GameObjectU5BU5D_t3057952154* ___FakeMob_5;
	// UnityEngine.GameObject RecordMobility::m_CopyObj
	GameObject_t1756533147 * ___m_CopyObj_6;
	// UnityEngine.Transform[] RecordMobility::corePastsChildTra
	TransformU5BU5D_t3764228911* ___corePastsChildTra_7;
	// MobilmoDnaController RecordMobility::m_mobilmoDnaController
	MobilmoDnaController_t1006271200 * ___m_mobilmoDnaController_8;
	// UnityEngine.UI.Text RecordMobility::Rank
	Text_t356221433 * ___Rank_9;
	// UnityEngine.UI.Text RecordMobility::MobilityName
	Text_t356221433 * ___MobilityName_10;
	// UnityEngine.UI.Text RecordMobility::UserName
	Text_t356221433 * ___UserName_11;
	// UnityEngine.UI.Text RecordMobility::Reaction
	Text_t356221433 * ___Reaction_12;

public:
	inline static int32_t get_offset_of_m_MobilityArea_2() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___m_MobilityArea_2)); }
	inline Transform_t3275118058 * get_m_MobilityArea_2() const { return ___m_MobilityArea_2; }
	inline Transform_t3275118058 ** get_address_of_m_MobilityArea_2() { return &___m_MobilityArea_2; }
	inline void set_m_MobilityArea_2(Transform_t3275118058 * value)
	{
		___m_MobilityArea_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_MobilityArea_2, value);
	}

	inline static int32_t get_offset_of_Controller_3() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___Controller_3)); }
	inline GameObject_t1756533147 * get_Controller_3() const { return ___Controller_3; }
	inline GameObject_t1756533147 ** get_address_of_Controller_3() { return &___Controller_3; }
	inline void set_Controller_3(GameObject_t1756533147 * value)
	{
		___Controller_3 = value;
		Il2CppCodeGenWriteBarrier(&___Controller_3, value);
	}

	inline static int32_t get_offset_of_MobilityJson_4() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___MobilityJson_4)); }
	inline String_t* get_MobilityJson_4() const { return ___MobilityJson_4; }
	inline String_t** get_address_of_MobilityJson_4() { return &___MobilityJson_4; }
	inline void set_MobilityJson_4(String_t* value)
	{
		___MobilityJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityJson_4, value);
	}

	inline static int32_t get_offset_of_FakeMob_5() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___FakeMob_5)); }
	inline GameObjectU5BU5D_t3057952154* get_FakeMob_5() const { return ___FakeMob_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_FakeMob_5() { return &___FakeMob_5; }
	inline void set_FakeMob_5(GameObjectU5BU5D_t3057952154* value)
	{
		___FakeMob_5 = value;
		Il2CppCodeGenWriteBarrier(&___FakeMob_5, value);
	}

	inline static int32_t get_offset_of_m_CopyObj_6() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___m_CopyObj_6)); }
	inline GameObject_t1756533147 * get_m_CopyObj_6() const { return ___m_CopyObj_6; }
	inline GameObject_t1756533147 ** get_address_of_m_CopyObj_6() { return &___m_CopyObj_6; }
	inline void set_m_CopyObj_6(GameObject_t1756533147 * value)
	{
		___m_CopyObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_CopyObj_6, value);
	}

	inline static int32_t get_offset_of_corePastsChildTra_7() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___corePastsChildTra_7)); }
	inline TransformU5BU5D_t3764228911* get_corePastsChildTra_7() const { return ___corePastsChildTra_7; }
	inline TransformU5BU5D_t3764228911** get_address_of_corePastsChildTra_7() { return &___corePastsChildTra_7; }
	inline void set_corePastsChildTra_7(TransformU5BU5D_t3764228911* value)
	{
		___corePastsChildTra_7 = value;
		Il2CppCodeGenWriteBarrier(&___corePastsChildTra_7, value);
	}

	inline static int32_t get_offset_of_m_mobilmoDnaController_8() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___m_mobilmoDnaController_8)); }
	inline MobilmoDnaController_t1006271200 * get_m_mobilmoDnaController_8() const { return ___m_mobilmoDnaController_8; }
	inline MobilmoDnaController_t1006271200 ** get_address_of_m_mobilmoDnaController_8() { return &___m_mobilmoDnaController_8; }
	inline void set_m_mobilmoDnaController_8(MobilmoDnaController_t1006271200 * value)
	{
		___m_mobilmoDnaController_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_mobilmoDnaController_8, value);
	}

	inline static int32_t get_offset_of_Rank_9() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___Rank_9)); }
	inline Text_t356221433 * get_Rank_9() const { return ___Rank_9; }
	inline Text_t356221433 ** get_address_of_Rank_9() { return &___Rank_9; }
	inline void set_Rank_9(Text_t356221433 * value)
	{
		___Rank_9 = value;
		Il2CppCodeGenWriteBarrier(&___Rank_9, value);
	}

	inline static int32_t get_offset_of_MobilityName_10() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___MobilityName_10)); }
	inline Text_t356221433 * get_MobilityName_10() const { return ___MobilityName_10; }
	inline Text_t356221433 ** get_address_of_MobilityName_10() { return &___MobilityName_10; }
	inline void set_MobilityName_10(Text_t356221433 * value)
	{
		___MobilityName_10 = value;
		Il2CppCodeGenWriteBarrier(&___MobilityName_10, value);
	}

	inline static int32_t get_offset_of_UserName_11() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___UserName_11)); }
	inline Text_t356221433 * get_UserName_11() const { return ___UserName_11; }
	inline Text_t356221433 ** get_address_of_UserName_11() { return &___UserName_11; }
	inline void set_UserName_11(Text_t356221433 * value)
	{
		___UserName_11 = value;
		Il2CppCodeGenWriteBarrier(&___UserName_11, value);
	}

	inline static int32_t get_offset_of_Reaction_12() { return static_cast<int32_t>(offsetof(RecordMobility_t2685217900, ___Reaction_12)); }
	inline Text_t356221433 * get_Reaction_12() const { return ___Reaction_12; }
	inline Text_t356221433 ** get_address_of_Reaction_12() { return &___Reaction_12; }
	inline void set_Reaction_12(Text_t356221433 * value)
	{
		___Reaction_12 = value;
		Il2CppCodeGenWriteBarrier(&___Reaction_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
