﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordPanel
struct  RecordPanel_t2270010953  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button RecordPanel::BackButton
	Button_t2872111280 * ___BackButton_2;

public:
	inline static int32_t get_offset_of_BackButton_2() { return static_cast<int32_t>(offsetof(RecordPanel_t2270010953, ___BackButton_2)); }
	inline Button_t2872111280 * get_BackButton_2() const { return ___BackButton_2; }
	inline Button_t2872111280 ** get_address_of_BackButton_2() { return &___BackButton_2; }
	inline void set_BackButton_2(Button_t2872111280 * value)
	{
		___BackButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___BackButton_2, value);
	}
};

struct RecordPanel_t2270010953_StaticFields
{
public:
	// UnityEngine.Events.UnityAction RecordPanel::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(RecordPanel_t2270010953_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
