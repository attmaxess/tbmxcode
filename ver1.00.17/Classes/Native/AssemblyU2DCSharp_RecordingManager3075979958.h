﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1772656293.h"
#include "mscorlib_System_DateTime693205669.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// System.Collections.Generic.List`1<RecordingManager/GetRecordingNewMotions>
struct List_1_t3439182517;
// System.Collections.Generic.List`1<RecordingManager/recordingMotionData>
struct List_1_t1073334888;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// RecordingManager/GetItemsData
struct GetItemsData_t174665839;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Mobilmo
struct Mobilmo_t370754809;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// DG.Tweening.Tweener
struct Tweener_t760404022;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordingManager
struct  RecordingManager_t3075979958  : public SingletonMonoBehaviour_1_t1772656293
{
public:
	// System.Int32 RecordingManager::recordedId
	int32_t ___recordedId_3;
	// System.Single RecordingManager::startPointX
	float ___startPointX_4;
	// System.Single RecordingManager::startPointY
	float ___startPointY_5;
	// System.Single RecordingManager::startPointZ
	float ___startPointZ_6;
	// System.Single RecordingManager::startRotateX
	float ___startRotateX_7;
	// System.Single RecordingManager::startRotateY
	float ___startRotateY_8;
	// System.Single RecordingManager::startRotateZ
	float ___startRotateZ_9;
	// System.String RecordingManager::motionStartTime
	String_t* ___motionStartTime_10;
	// System.Int32 RecordingManager::actionNo
	int32_t ___actionNo_11;
	// System.DateTime RecordingManager::startRecordingTime
	DateTime_t693205669  ___startRecordingTime_12;
	// System.Single RecordingManager::timer
	float ___timer_13;
	// System.Boolean RecordingManager::isRecordingStart
	bool ___isRecordingStart_14;
	// System.Boolean RecordingManager::isActionButtonClick
	bool ___isActionButtonClick_15;
	// System.Single RecordingManager::corePartsStartPointX
	float ___corePartsStartPointX_16;
	// System.Single RecordingManager::corePartsStartPointY
	float ___corePartsStartPointY_17;
	// System.Single RecordingManager::corePartsStartPointZ
	float ___corePartsStartPointZ_18;
	// System.Single RecordingManager::corePartsStartRotateX
	float ___corePartsStartRotateX_19;
	// System.Single RecordingManager::corePartsStartRotateY
	float ___corePartsStartRotateY_20;
	// System.Single RecordingManager::corePartsStartRotateZ
	float ___corePartsStartRotateZ_21;
	// System.String[] RecordingManager::mobilityJsonArray
	StringU5BU5D_t1642385972* ___mobilityJsonArray_22;
	// System.Boolean RecordingManager::NPCMobGet
	bool ___NPCMobGet_23;
	// System.Collections.Generic.List`1<UnityEngine.Transform> RecordingManager::pTranses
	List_1_t2644239190 * ___pTranses_24;
	// System.Collections.Generic.List`1<UnityEngine.Transform> RecordingManager::pChildParts
	List_1_t2644239190 * ___pChildParts_25;
	// UnityEngine.AnimationCurve RecordingManager::anim
	AnimationCurve_t3306541151 * ___anim_26;
	// System.Collections.Generic.List`1<RecordingManager/GetRecordingNewMotions> RecordingManager::newRecording
	List_1_t3439182517 * ___newRecording_27;
	// System.Collections.Generic.List`1<RecordingManager/recordingMotionData> RecordingManager::recordingMotionDataList
	List_1_t1073334888 * ___recordingMotionDataList_28;
	// System.Collections.Generic.List`1<System.Int32> RecordingManager::getModuleIdList
	List_1_t1440998580 * ___getModuleIdList_29;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> RecordingManager::getOtherModuleIdList
	Dictionary_2_t1079703083 * ___getOtherModuleIdList_30;
	// RecordingManager/GetItemsData RecordingManager::newItems
	GetItemsData_t174665839 * ___newItems_31;
	// UnityEngine.GameObject RecordingManager::plantObj
	GameObject_t1756533147 * ___plantObj_32;
	// System.String RecordingManager::moduleCopyJson
	String_t* ___moduleCopyJson_33;
	// System.Int32 RecordingManager::copyedMobilityId
	int32_t ___copyedMobilityId_34;
	// System.String RecordingManager::recordingJson
	String_t* ___recordingJson_35;
	// Mobilmo RecordingManager::mobilmo
	Mobilmo_t370754809 * ___mobilmo_36;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> RecordingManager::RecMobilityList
	List_1_t1125654279 * ___RecMobilityList_37;
	// System.Boolean RecordingManager::isGettingRecordedmodule
	bool ___isGettingRecordedmodule_38;
	// UnityEngine.GameObject RecordingManager::Controller
	GameObject_t1756533147 * ___Controller_39;
	// System.Int32 RecordingManager::copyedModuleId
	int32_t ___copyedModuleId_40;
	// UnityEngine.UI.Image RecordingManager::RecordGuage
	Image_t2042527209 * ___RecordGuage_41;
	// UnityEngine.CanvasGroup RecordingManager::Recording
	CanvasGroup_t3296560743 * ___Recording_42;
	// DG.Tweening.Tweener RecordingManager::m_risingTween
	Tweener_t760404022 * ___m_risingTween_43;
	// DG.Tweening.Tweener RecordingManager::m_blinkTween
	Tweener_t760404022 * ___m_blinkTween_44;
	// System.Int32 RecordingManager::pressRecoredCnt
	int32_t ___pressRecoredCnt_45;

public:
	inline static int32_t get_offset_of_recordedId_3() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___recordedId_3)); }
	inline int32_t get_recordedId_3() const { return ___recordedId_3; }
	inline int32_t* get_address_of_recordedId_3() { return &___recordedId_3; }
	inline void set_recordedId_3(int32_t value)
	{
		___recordedId_3 = value;
	}

	inline static int32_t get_offset_of_startPointX_4() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___startPointX_4)); }
	inline float get_startPointX_4() const { return ___startPointX_4; }
	inline float* get_address_of_startPointX_4() { return &___startPointX_4; }
	inline void set_startPointX_4(float value)
	{
		___startPointX_4 = value;
	}

	inline static int32_t get_offset_of_startPointY_5() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___startPointY_5)); }
	inline float get_startPointY_5() const { return ___startPointY_5; }
	inline float* get_address_of_startPointY_5() { return &___startPointY_5; }
	inline void set_startPointY_5(float value)
	{
		___startPointY_5 = value;
	}

	inline static int32_t get_offset_of_startPointZ_6() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___startPointZ_6)); }
	inline float get_startPointZ_6() const { return ___startPointZ_6; }
	inline float* get_address_of_startPointZ_6() { return &___startPointZ_6; }
	inline void set_startPointZ_6(float value)
	{
		___startPointZ_6 = value;
	}

	inline static int32_t get_offset_of_startRotateX_7() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___startRotateX_7)); }
	inline float get_startRotateX_7() const { return ___startRotateX_7; }
	inline float* get_address_of_startRotateX_7() { return &___startRotateX_7; }
	inline void set_startRotateX_7(float value)
	{
		___startRotateX_7 = value;
	}

	inline static int32_t get_offset_of_startRotateY_8() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___startRotateY_8)); }
	inline float get_startRotateY_8() const { return ___startRotateY_8; }
	inline float* get_address_of_startRotateY_8() { return &___startRotateY_8; }
	inline void set_startRotateY_8(float value)
	{
		___startRotateY_8 = value;
	}

	inline static int32_t get_offset_of_startRotateZ_9() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___startRotateZ_9)); }
	inline float get_startRotateZ_9() const { return ___startRotateZ_9; }
	inline float* get_address_of_startRotateZ_9() { return &___startRotateZ_9; }
	inline void set_startRotateZ_9(float value)
	{
		___startRotateZ_9 = value;
	}

	inline static int32_t get_offset_of_motionStartTime_10() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___motionStartTime_10)); }
	inline String_t* get_motionStartTime_10() const { return ___motionStartTime_10; }
	inline String_t** get_address_of_motionStartTime_10() { return &___motionStartTime_10; }
	inline void set_motionStartTime_10(String_t* value)
	{
		___motionStartTime_10 = value;
		Il2CppCodeGenWriteBarrier(&___motionStartTime_10, value);
	}

	inline static int32_t get_offset_of_actionNo_11() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___actionNo_11)); }
	inline int32_t get_actionNo_11() const { return ___actionNo_11; }
	inline int32_t* get_address_of_actionNo_11() { return &___actionNo_11; }
	inline void set_actionNo_11(int32_t value)
	{
		___actionNo_11 = value;
	}

	inline static int32_t get_offset_of_startRecordingTime_12() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___startRecordingTime_12)); }
	inline DateTime_t693205669  get_startRecordingTime_12() const { return ___startRecordingTime_12; }
	inline DateTime_t693205669 * get_address_of_startRecordingTime_12() { return &___startRecordingTime_12; }
	inline void set_startRecordingTime_12(DateTime_t693205669  value)
	{
		___startRecordingTime_12 = value;
	}

	inline static int32_t get_offset_of_timer_13() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___timer_13)); }
	inline float get_timer_13() const { return ___timer_13; }
	inline float* get_address_of_timer_13() { return &___timer_13; }
	inline void set_timer_13(float value)
	{
		___timer_13 = value;
	}

	inline static int32_t get_offset_of_isRecordingStart_14() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___isRecordingStart_14)); }
	inline bool get_isRecordingStart_14() const { return ___isRecordingStart_14; }
	inline bool* get_address_of_isRecordingStart_14() { return &___isRecordingStart_14; }
	inline void set_isRecordingStart_14(bool value)
	{
		___isRecordingStart_14 = value;
	}

	inline static int32_t get_offset_of_isActionButtonClick_15() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___isActionButtonClick_15)); }
	inline bool get_isActionButtonClick_15() const { return ___isActionButtonClick_15; }
	inline bool* get_address_of_isActionButtonClick_15() { return &___isActionButtonClick_15; }
	inline void set_isActionButtonClick_15(bool value)
	{
		___isActionButtonClick_15 = value;
	}

	inline static int32_t get_offset_of_corePartsStartPointX_16() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___corePartsStartPointX_16)); }
	inline float get_corePartsStartPointX_16() const { return ___corePartsStartPointX_16; }
	inline float* get_address_of_corePartsStartPointX_16() { return &___corePartsStartPointX_16; }
	inline void set_corePartsStartPointX_16(float value)
	{
		___corePartsStartPointX_16 = value;
	}

	inline static int32_t get_offset_of_corePartsStartPointY_17() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___corePartsStartPointY_17)); }
	inline float get_corePartsStartPointY_17() const { return ___corePartsStartPointY_17; }
	inline float* get_address_of_corePartsStartPointY_17() { return &___corePartsStartPointY_17; }
	inline void set_corePartsStartPointY_17(float value)
	{
		___corePartsStartPointY_17 = value;
	}

	inline static int32_t get_offset_of_corePartsStartPointZ_18() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___corePartsStartPointZ_18)); }
	inline float get_corePartsStartPointZ_18() const { return ___corePartsStartPointZ_18; }
	inline float* get_address_of_corePartsStartPointZ_18() { return &___corePartsStartPointZ_18; }
	inline void set_corePartsStartPointZ_18(float value)
	{
		___corePartsStartPointZ_18 = value;
	}

	inline static int32_t get_offset_of_corePartsStartRotateX_19() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___corePartsStartRotateX_19)); }
	inline float get_corePartsStartRotateX_19() const { return ___corePartsStartRotateX_19; }
	inline float* get_address_of_corePartsStartRotateX_19() { return &___corePartsStartRotateX_19; }
	inline void set_corePartsStartRotateX_19(float value)
	{
		___corePartsStartRotateX_19 = value;
	}

	inline static int32_t get_offset_of_corePartsStartRotateY_20() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___corePartsStartRotateY_20)); }
	inline float get_corePartsStartRotateY_20() const { return ___corePartsStartRotateY_20; }
	inline float* get_address_of_corePartsStartRotateY_20() { return &___corePartsStartRotateY_20; }
	inline void set_corePartsStartRotateY_20(float value)
	{
		___corePartsStartRotateY_20 = value;
	}

	inline static int32_t get_offset_of_corePartsStartRotateZ_21() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___corePartsStartRotateZ_21)); }
	inline float get_corePartsStartRotateZ_21() const { return ___corePartsStartRotateZ_21; }
	inline float* get_address_of_corePartsStartRotateZ_21() { return &___corePartsStartRotateZ_21; }
	inline void set_corePartsStartRotateZ_21(float value)
	{
		___corePartsStartRotateZ_21 = value;
	}

	inline static int32_t get_offset_of_mobilityJsonArray_22() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___mobilityJsonArray_22)); }
	inline StringU5BU5D_t1642385972* get_mobilityJsonArray_22() const { return ___mobilityJsonArray_22; }
	inline StringU5BU5D_t1642385972** get_address_of_mobilityJsonArray_22() { return &___mobilityJsonArray_22; }
	inline void set_mobilityJsonArray_22(StringU5BU5D_t1642385972* value)
	{
		___mobilityJsonArray_22 = value;
		Il2CppCodeGenWriteBarrier(&___mobilityJsonArray_22, value);
	}

	inline static int32_t get_offset_of_NPCMobGet_23() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___NPCMobGet_23)); }
	inline bool get_NPCMobGet_23() const { return ___NPCMobGet_23; }
	inline bool* get_address_of_NPCMobGet_23() { return &___NPCMobGet_23; }
	inline void set_NPCMobGet_23(bool value)
	{
		___NPCMobGet_23 = value;
	}

	inline static int32_t get_offset_of_pTranses_24() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___pTranses_24)); }
	inline List_1_t2644239190 * get_pTranses_24() const { return ___pTranses_24; }
	inline List_1_t2644239190 ** get_address_of_pTranses_24() { return &___pTranses_24; }
	inline void set_pTranses_24(List_1_t2644239190 * value)
	{
		___pTranses_24 = value;
		Il2CppCodeGenWriteBarrier(&___pTranses_24, value);
	}

	inline static int32_t get_offset_of_pChildParts_25() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___pChildParts_25)); }
	inline List_1_t2644239190 * get_pChildParts_25() const { return ___pChildParts_25; }
	inline List_1_t2644239190 ** get_address_of_pChildParts_25() { return &___pChildParts_25; }
	inline void set_pChildParts_25(List_1_t2644239190 * value)
	{
		___pChildParts_25 = value;
		Il2CppCodeGenWriteBarrier(&___pChildParts_25, value);
	}

	inline static int32_t get_offset_of_anim_26() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___anim_26)); }
	inline AnimationCurve_t3306541151 * get_anim_26() const { return ___anim_26; }
	inline AnimationCurve_t3306541151 ** get_address_of_anim_26() { return &___anim_26; }
	inline void set_anim_26(AnimationCurve_t3306541151 * value)
	{
		___anim_26 = value;
		Il2CppCodeGenWriteBarrier(&___anim_26, value);
	}

	inline static int32_t get_offset_of_newRecording_27() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___newRecording_27)); }
	inline List_1_t3439182517 * get_newRecording_27() const { return ___newRecording_27; }
	inline List_1_t3439182517 ** get_address_of_newRecording_27() { return &___newRecording_27; }
	inline void set_newRecording_27(List_1_t3439182517 * value)
	{
		___newRecording_27 = value;
		Il2CppCodeGenWriteBarrier(&___newRecording_27, value);
	}

	inline static int32_t get_offset_of_recordingMotionDataList_28() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___recordingMotionDataList_28)); }
	inline List_1_t1073334888 * get_recordingMotionDataList_28() const { return ___recordingMotionDataList_28; }
	inline List_1_t1073334888 ** get_address_of_recordingMotionDataList_28() { return &___recordingMotionDataList_28; }
	inline void set_recordingMotionDataList_28(List_1_t1073334888 * value)
	{
		___recordingMotionDataList_28 = value;
		Il2CppCodeGenWriteBarrier(&___recordingMotionDataList_28, value);
	}

	inline static int32_t get_offset_of_getModuleIdList_29() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___getModuleIdList_29)); }
	inline List_1_t1440998580 * get_getModuleIdList_29() const { return ___getModuleIdList_29; }
	inline List_1_t1440998580 ** get_address_of_getModuleIdList_29() { return &___getModuleIdList_29; }
	inline void set_getModuleIdList_29(List_1_t1440998580 * value)
	{
		___getModuleIdList_29 = value;
		Il2CppCodeGenWriteBarrier(&___getModuleIdList_29, value);
	}

	inline static int32_t get_offset_of_getOtherModuleIdList_30() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___getOtherModuleIdList_30)); }
	inline Dictionary_2_t1079703083 * get_getOtherModuleIdList_30() const { return ___getOtherModuleIdList_30; }
	inline Dictionary_2_t1079703083 ** get_address_of_getOtherModuleIdList_30() { return &___getOtherModuleIdList_30; }
	inline void set_getOtherModuleIdList_30(Dictionary_2_t1079703083 * value)
	{
		___getOtherModuleIdList_30 = value;
		Il2CppCodeGenWriteBarrier(&___getOtherModuleIdList_30, value);
	}

	inline static int32_t get_offset_of_newItems_31() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___newItems_31)); }
	inline GetItemsData_t174665839 * get_newItems_31() const { return ___newItems_31; }
	inline GetItemsData_t174665839 ** get_address_of_newItems_31() { return &___newItems_31; }
	inline void set_newItems_31(GetItemsData_t174665839 * value)
	{
		___newItems_31 = value;
		Il2CppCodeGenWriteBarrier(&___newItems_31, value);
	}

	inline static int32_t get_offset_of_plantObj_32() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___plantObj_32)); }
	inline GameObject_t1756533147 * get_plantObj_32() const { return ___plantObj_32; }
	inline GameObject_t1756533147 ** get_address_of_plantObj_32() { return &___plantObj_32; }
	inline void set_plantObj_32(GameObject_t1756533147 * value)
	{
		___plantObj_32 = value;
		Il2CppCodeGenWriteBarrier(&___plantObj_32, value);
	}

	inline static int32_t get_offset_of_moduleCopyJson_33() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___moduleCopyJson_33)); }
	inline String_t* get_moduleCopyJson_33() const { return ___moduleCopyJson_33; }
	inline String_t** get_address_of_moduleCopyJson_33() { return &___moduleCopyJson_33; }
	inline void set_moduleCopyJson_33(String_t* value)
	{
		___moduleCopyJson_33 = value;
		Il2CppCodeGenWriteBarrier(&___moduleCopyJson_33, value);
	}

	inline static int32_t get_offset_of_copyedMobilityId_34() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___copyedMobilityId_34)); }
	inline int32_t get_copyedMobilityId_34() const { return ___copyedMobilityId_34; }
	inline int32_t* get_address_of_copyedMobilityId_34() { return &___copyedMobilityId_34; }
	inline void set_copyedMobilityId_34(int32_t value)
	{
		___copyedMobilityId_34 = value;
	}

	inline static int32_t get_offset_of_recordingJson_35() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___recordingJson_35)); }
	inline String_t* get_recordingJson_35() const { return ___recordingJson_35; }
	inline String_t** get_address_of_recordingJson_35() { return &___recordingJson_35; }
	inline void set_recordingJson_35(String_t* value)
	{
		___recordingJson_35 = value;
		Il2CppCodeGenWriteBarrier(&___recordingJson_35, value);
	}

	inline static int32_t get_offset_of_mobilmo_36() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___mobilmo_36)); }
	inline Mobilmo_t370754809 * get_mobilmo_36() const { return ___mobilmo_36; }
	inline Mobilmo_t370754809 ** get_address_of_mobilmo_36() { return &___mobilmo_36; }
	inline void set_mobilmo_36(Mobilmo_t370754809 * value)
	{
		___mobilmo_36 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmo_36, value);
	}

	inline static int32_t get_offset_of_RecMobilityList_37() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___RecMobilityList_37)); }
	inline List_1_t1125654279 * get_RecMobilityList_37() const { return ___RecMobilityList_37; }
	inline List_1_t1125654279 ** get_address_of_RecMobilityList_37() { return &___RecMobilityList_37; }
	inline void set_RecMobilityList_37(List_1_t1125654279 * value)
	{
		___RecMobilityList_37 = value;
		Il2CppCodeGenWriteBarrier(&___RecMobilityList_37, value);
	}

	inline static int32_t get_offset_of_isGettingRecordedmodule_38() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___isGettingRecordedmodule_38)); }
	inline bool get_isGettingRecordedmodule_38() const { return ___isGettingRecordedmodule_38; }
	inline bool* get_address_of_isGettingRecordedmodule_38() { return &___isGettingRecordedmodule_38; }
	inline void set_isGettingRecordedmodule_38(bool value)
	{
		___isGettingRecordedmodule_38 = value;
	}

	inline static int32_t get_offset_of_Controller_39() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___Controller_39)); }
	inline GameObject_t1756533147 * get_Controller_39() const { return ___Controller_39; }
	inline GameObject_t1756533147 ** get_address_of_Controller_39() { return &___Controller_39; }
	inline void set_Controller_39(GameObject_t1756533147 * value)
	{
		___Controller_39 = value;
		Il2CppCodeGenWriteBarrier(&___Controller_39, value);
	}

	inline static int32_t get_offset_of_copyedModuleId_40() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___copyedModuleId_40)); }
	inline int32_t get_copyedModuleId_40() const { return ___copyedModuleId_40; }
	inline int32_t* get_address_of_copyedModuleId_40() { return &___copyedModuleId_40; }
	inline void set_copyedModuleId_40(int32_t value)
	{
		___copyedModuleId_40 = value;
	}

	inline static int32_t get_offset_of_RecordGuage_41() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___RecordGuage_41)); }
	inline Image_t2042527209 * get_RecordGuage_41() const { return ___RecordGuage_41; }
	inline Image_t2042527209 ** get_address_of_RecordGuage_41() { return &___RecordGuage_41; }
	inline void set_RecordGuage_41(Image_t2042527209 * value)
	{
		___RecordGuage_41 = value;
		Il2CppCodeGenWriteBarrier(&___RecordGuage_41, value);
	}

	inline static int32_t get_offset_of_Recording_42() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___Recording_42)); }
	inline CanvasGroup_t3296560743 * get_Recording_42() const { return ___Recording_42; }
	inline CanvasGroup_t3296560743 ** get_address_of_Recording_42() { return &___Recording_42; }
	inline void set_Recording_42(CanvasGroup_t3296560743 * value)
	{
		___Recording_42 = value;
		Il2CppCodeGenWriteBarrier(&___Recording_42, value);
	}

	inline static int32_t get_offset_of_m_risingTween_43() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___m_risingTween_43)); }
	inline Tweener_t760404022 * get_m_risingTween_43() const { return ___m_risingTween_43; }
	inline Tweener_t760404022 ** get_address_of_m_risingTween_43() { return &___m_risingTween_43; }
	inline void set_m_risingTween_43(Tweener_t760404022 * value)
	{
		___m_risingTween_43 = value;
		Il2CppCodeGenWriteBarrier(&___m_risingTween_43, value);
	}

	inline static int32_t get_offset_of_m_blinkTween_44() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___m_blinkTween_44)); }
	inline Tweener_t760404022 * get_m_blinkTween_44() const { return ___m_blinkTween_44; }
	inline Tweener_t760404022 ** get_address_of_m_blinkTween_44() { return &___m_blinkTween_44; }
	inline void set_m_blinkTween_44(Tweener_t760404022 * value)
	{
		___m_blinkTween_44 = value;
		Il2CppCodeGenWriteBarrier(&___m_blinkTween_44, value);
	}

	inline static int32_t get_offset_of_pressRecoredCnt_45() { return static_cast<int32_t>(offsetof(RecordingManager_t3075979958, ___pressRecoredCnt_45)); }
	inline int32_t get_pressRecoredCnt_45() const { return ___pressRecoredCnt_45; }
	inline int32_t* get_address_of_pressRecoredCnt_45() { return &___pressRecoredCnt_45; }
	inline void set_pressRecoredCnt_45(int32_t value)
	{
		___pressRecoredCnt_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
