﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordingManager/CopyModuleData
struct  CopyModuleData_t4206646110  : public Il2CppObject
{
public:
	// System.Int32 RecordingManager/CopyModuleData::userId
	int32_t ___userId_0;
	// System.Int32 RecordingManager/CopyModuleData::mobilityId
	int32_t ___mobilityId_1;
	// System.Int32 RecordingManager/CopyModuleData::moduleId
	int32_t ___moduleId_2;

public:
	inline static int32_t get_offset_of_userId_0() { return static_cast<int32_t>(offsetof(CopyModuleData_t4206646110, ___userId_0)); }
	inline int32_t get_userId_0() const { return ___userId_0; }
	inline int32_t* get_address_of_userId_0() { return &___userId_0; }
	inline void set_userId_0(int32_t value)
	{
		___userId_0 = value;
	}

	inline static int32_t get_offset_of_mobilityId_1() { return static_cast<int32_t>(offsetof(CopyModuleData_t4206646110, ___mobilityId_1)); }
	inline int32_t get_mobilityId_1() const { return ___mobilityId_1; }
	inline int32_t* get_address_of_mobilityId_1() { return &___mobilityId_1; }
	inline void set_mobilityId_1(int32_t value)
	{
		___mobilityId_1 = value;
	}

	inline static int32_t get_offset_of_moduleId_2() { return static_cast<int32_t>(offsetof(CopyModuleData_t4206646110, ___moduleId_2)); }
	inline int32_t get_moduleId_2() const { return ___moduleId_2; }
	inline int32_t* get_address_of_moduleId_2() { return &___moduleId_2; }
	inline void set_moduleId_2(int32_t value)
	{
		___moduleId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
