﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordingManager/CopyRecModuleDataInWorld
struct  CopyRecModuleDataInWorld_t1848073189  : public Il2CppObject
{
public:
	// System.Int32 RecordingManager/CopyRecModuleDataInWorld::userId
	int32_t ___userId_0;
	// System.Int32 RecordingManager/CopyRecModuleDataInWorld::moduleId
	int32_t ___moduleId_1;

public:
	inline static int32_t get_offset_of_userId_0() { return static_cast<int32_t>(offsetof(CopyRecModuleDataInWorld_t1848073189, ___userId_0)); }
	inline int32_t get_userId_0() const { return ___userId_0; }
	inline int32_t* get_address_of_userId_0() { return &___userId_0; }
	inline void set_userId_0(int32_t value)
	{
		___userId_0 = value;
	}

	inline static int32_t get_offset_of_moduleId_1() { return static_cast<int32_t>(offsetof(CopyRecModuleDataInWorld_t1848073189, ___moduleId_1)); }
	inline int32_t get_moduleId_1() const { return ___moduleId_1; }
	inline int32_t* get_address_of_moduleId_1() { return &___moduleId_1; }
	inline void set_moduleId_1(int32_t value)
	{
		___moduleId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
