﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<RecordingManager/GetRecordingNewMotions>
struct List_1_t3439182517;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordingManager/GetItemsData
struct  GetItemsData_t174665839  : public Il2CppObject
{
public:
	// System.String RecordingManager/GetItemsData::recordedDate
	String_t* ___recordedDate_0;
	// System.Single RecordingManager/GetItemsData::startPointX
	float ___startPointX_1;
	// System.Single RecordingManager/GetItemsData::startPointY
	float ___startPointY_2;
	// System.Single RecordingManager/GetItemsData::startPointZ
	float ___startPointZ_3;
	// System.Single RecordingManager/GetItemsData::startRotateX
	float ___startRotateX_4;
	// System.Single RecordingManager/GetItemsData::startRotateY
	float ___startRotateY_5;
	// System.Single RecordingManager/GetItemsData::startRotateZ
	float ___startRotateZ_6;
	// System.Collections.Generic.List`1<RecordingManager/GetRecordingNewMotions> RecordingManager/GetItemsData::motions
	List_1_t3439182517 * ___motions_7;

public:
	inline static int32_t get_offset_of_recordedDate_0() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___recordedDate_0)); }
	inline String_t* get_recordedDate_0() const { return ___recordedDate_0; }
	inline String_t** get_address_of_recordedDate_0() { return &___recordedDate_0; }
	inline void set_recordedDate_0(String_t* value)
	{
		___recordedDate_0 = value;
		Il2CppCodeGenWriteBarrier(&___recordedDate_0, value);
	}

	inline static int32_t get_offset_of_startPointX_1() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___startPointX_1)); }
	inline float get_startPointX_1() const { return ___startPointX_1; }
	inline float* get_address_of_startPointX_1() { return &___startPointX_1; }
	inline void set_startPointX_1(float value)
	{
		___startPointX_1 = value;
	}

	inline static int32_t get_offset_of_startPointY_2() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___startPointY_2)); }
	inline float get_startPointY_2() const { return ___startPointY_2; }
	inline float* get_address_of_startPointY_2() { return &___startPointY_2; }
	inline void set_startPointY_2(float value)
	{
		___startPointY_2 = value;
	}

	inline static int32_t get_offset_of_startPointZ_3() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___startPointZ_3)); }
	inline float get_startPointZ_3() const { return ___startPointZ_3; }
	inline float* get_address_of_startPointZ_3() { return &___startPointZ_3; }
	inline void set_startPointZ_3(float value)
	{
		___startPointZ_3 = value;
	}

	inline static int32_t get_offset_of_startRotateX_4() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___startRotateX_4)); }
	inline float get_startRotateX_4() const { return ___startRotateX_4; }
	inline float* get_address_of_startRotateX_4() { return &___startRotateX_4; }
	inline void set_startRotateX_4(float value)
	{
		___startRotateX_4 = value;
	}

	inline static int32_t get_offset_of_startRotateY_5() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___startRotateY_5)); }
	inline float get_startRotateY_5() const { return ___startRotateY_5; }
	inline float* get_address_of_startRotateY_5() { return &___startRotateY_5; }
	inline void set_startRotateY_5(float value)
	{
		___startRotateY_5 = value;
	}

	inline static int32_t get_offset_of_startRotateZ_6() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___startRotateZ_6)); }
	inline float get_startRotateZ_6() const { return ___startRotateZ_6; }
	inline float* get_address_of_startRotateZ_6() { return &___startRotateZ_6; }
	inline void set_startRotateZ_6(float value)
	{
		___startRotateZ_6 = value;
	}

	inline static int32_t get_offset_of_motions_7() { return static_cast<int32_t>(offsetof(GetItemsData_t174665839, ___motions_7)); }
	inline List_1_t3439182517 * get_motions_7() const { return ___motions_7; }
	inline List_1_t3439182517 ** get_address_of_motions_7() { return &___motions_7; }
	inline void set_motions_7(List_1_t3439182517 * value)
	{
		___motions_7 = value;
		Il2CppCodeGenWriteBarrier(&___motions_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
