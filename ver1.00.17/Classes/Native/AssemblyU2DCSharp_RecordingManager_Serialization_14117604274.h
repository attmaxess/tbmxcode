﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// RecordingManager/GetItemsData
struct GetItemsData_t174665839;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordingManager/Serialization`1<RecordingManager/GetItemsData>
struct  Serialization_1_t4117604274  : public Il2CppObject
{
public:
	// System.Int32 RecordingManager/Serialization`1::mobilityId
	int32_t ___mobilityId_0;
	// System.Int32 RecordingManager/Serialization`1::userId
	int32_t ___userId_1;
	// T RecordingManager/Serialization`1::item
	GetItemsData_t174665839 * ___item_2;

public:
	inline static int32_t get_offset_of_mobilityId_0() { return static_cast<int32_t>(offsetof(Serialization_1_t4117604274, ___mobilityId_0)); }
	inline int32_t get_mobilityId_0() const { return ___mobilityId_0; }
	inline int32_t* get_address_of_mobilityId_0() { return &___mobilityId_0; }
	inline void set_mobilityId_0(int32_t value)
	{
		___mobilityId_0 = value;
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(Serialization_1_t4117604274, ___userId_1)); }
	inline int32_t get_userId_1() const { return ___userId_1; }
	inline int32_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(int32_t value)
	{
		___userId_1 = value;
	}

	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(Serialization_1_t4117604274, ___item_2)); }
	inline GetItemsData_t174665839 * get_item_2() const { return ___item_2; }
	inline GetItemsData_t174665839 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(GetItemsData_t174665839 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier(&___item_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
