﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Mobilmo
struct Mobilmo_t370754809;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecordingManager/recordingMotionData
struct  recordingMotionData_t1704213756  : public Il2CppObject
{
public:
	// System.Single RecordingManager/recordingMotionData::motionStartTime
	float ___motionStartTime_0;
	// System.Int32 RecordingManager/recordingMotionData::motionNum
	int32_t ___motionNum_1;
	// Mobilmo RecordingManager/recordingMotionData::mobilmo
	Mobilmo_t370754809 * ___mobilmo_2;

public:
	inline static int32_t get_offset_of_motionStartTime_0() { return static_cast<int32_t>(offsetof(recordingMotionData_t1704213756, ___motionStartTime_0)); }
	inline float get_motionStartTime_0() const { return ___motionStartTime_0; }
	inline float* get_address_of_motionStartTime_0() { return &___motionStartTime_0; }
	inline void set_motionStartTime_0(float value)
	{
		___motionStartTime_0 = value;
	}

	inline static int32_t get_offset_of_motionNum_1() { return static_cast<int32_t>(offsetof(recordingMotionData_t1704213756, ___motionNum_1)); }
	inline int32_t get_motionNum_1() const { return ___motionNum_1; }
	inline int32_t* get_address_of_motionNum_1() { return &___motionNum_1; }
	inline void set_motionNum_1(int32_t value)
	{
		___motionNum_1 = value;
	}

	inline static int32_t get_offset_of_mobilmo_2() { return static_cast<int32_t>(offsetof(recordingMotionData_t1704213756, ___mobilmo_2)); }
	inline Mobilmo_t370754809 * get_mobilmo_2() const { return ___mobilmo_2; }
	inline Mobilmo_t370754809 ** get_address_of_mobilmo_2() { return &___mobilmo_2; }
	inline void set_mobilmo_2(Mobilmo_t370754809 * value)
	{
		___mobilmo_2 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmo_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
