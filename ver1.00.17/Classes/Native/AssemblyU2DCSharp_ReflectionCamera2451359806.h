﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_FilterMode10814199.h"
#include "UnityEngine_UnityEngine_RenderingPath1538007819.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReflectionCamera
struct  ReflectionCamera_t2451359806  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ReflectionCamera::m_ClipPlaneOffset
	float ___m_ClipPlaneOffset_2;
	// UnityEngine.LayerMask ReflectionCamera::CullingMask
	LayerMask_t3188175821  ___CullingMask_3;
	// System.Boolean ReflectionCamera::HDR
	bool ___HDR_4;
	// System.Boolean ReflectionCamera::OcclusionCulling
	bool ___OcclusionCulling_5;
	// System.Single ReflectionCamera::TextureScale
	float ___TextureScale_6;
	// UnityEngine.RenderTextureFormat ReflectionCamera::RenderTextureFormat
	int32_t ___RenderTextureFormat_7;
	// UnityEngine.FilterMode ReflectionCamera::FilterMode
	int32_t ___FilterMode_8;
	// UnityEngine.RenderingPath ReflectionCamera::RenderingPath
	int32_t ___RenderingPath_9;
	// System.Boolean ReflectionCamera::UseRealtimeUpdate
	bool ___UseRealtimeUpdate_10;
	// System.Int32 ReflectionCamera::FPSWhenMoveCamera
	int32_t ___FPSWhenMoveCamera_11;
	// System.Int32 ReflectionCamera::FPSWhenStaticCamera
	int32_t ___FPSWhenStaticCamera_12;
	// UnityEngine.RenderTexture ReflectionCamera::renderTexture
	RenderTexture_t2666733923 * ___renderTexture_13;
	// UnityEngine.GameObject ReflectionCamera::go
	GameObject_t1756533147 * ___go_14;
	// UnityEngine.Camera ReflectionCamera::reflectionCamera
	Camera_t189460977 * ___reflectionCamera_15;
	// UnityEngine.Vector3 ReflectionCamera::oldPosition
	Vector3_t2243707580  ___oldPosition_16;
	// UnityEngine.Quaternion ReflectionCamera::oldRotation
	Quaternion_t4030073918  ___oldRotation_17;
	// UnityEngine.Transform ReflectionCamera::instanceCameraTransform
	Transform_t3275118058 * ___instanceCameraTransform_18;
	// System.Int32 ReflectionCamera::frameCountWhenCameraIsStatic
	int32_t ___frameCountWhenCameraIsStatic_19;
	// System.Boolean ReflectionCamera::canUpdateCamera
	bool ___canUpdateCamera_20;
	// System.Boolean ReflectionCamera::isStaticUpdate
	bool ___isStaticUpdate_21;
	// UnityEngine.WaitForSeconds ReflectionCamera::fpsMove
	WaitForSeconds_t3839502067 * ___fpsMove_22;
	// UnityEngine.WaitForSeconds ReflectionCamera::fpsStatic
	WaitForSeconds_t3839502067 * ___fpsStatic_23;
	// UnityEngine.Camera ReflectionCamera::currentCamera
	Camera_t189460977 * ___currentCamera_25;

public:
	inline static int32_t get_offset_of_m_ClipPlaneOffset_2() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___m_ClipPlaneOffset_2)); }
	inline float get_m_ClipPlaneOffset_2() const { return ___m_ClipPlaneOffset_2; }
	inline float* get_address_of_m_ClipPlaneOffset_2() { return &___m_ClipPlaneOffset_2; }
	inline void set_m_ClipPlaneOffset_2(float value)
	{
		___m_ClipPlaneOffset_2 = value;
	}

	inline static int32_t get_offset_of_CullingMask_3() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___CullingMask_3)); }
	inline LayerMask_t3188175821  get_CullingMask_3() const { return ___CullingMask_3; }
	inline LayerMask_t3188175821 * get_address_of_CullingMask_3() { return &___CullingMask_3; }
	inline void set_CullingMask_3(LayerMask_t3188175821  value)
	{
		___CullingMask_3 = value;
	}

	inline static int32_t get_offset_of_HDR_4() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___HDR_4)); }
	inline bool get_HDR_4() const { return ___HDR_4; }
	inline bool* get_address_of_HDR_4() { return &___HDR_4; }
	inline void set_HDR_4(bool value)
	{
		___HDR_4 = value;
	}

	inline static int32_t get_offset_of_OcclusionCulling_5() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___OcclusionCulling_5)); }
	inline bool get_OcclusionCulling_5() const { return ___OcclusionCulling_5; }
	inline bool* get_address_of_OcclusionCulling_5() { return &___OcclusionCulling_5; }
	inline void set_OcclusionCulling_5(bool value)
	{
		___OcclusionCulling_5 = value;
	}

	inline static int32_t get_offset_of_TextureScale_6() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___TextureScale_6)); }
	inline float get_TextureScale_6() const { return ___TextureScale_6; }
	inline float* get_address_of_TextureScale_6() { return &___TextureScale_6; }
	inline void set_TextureScale_6(float value)
	{
		___TextureScale_6 = value;
	}

	inline static int32_t get_offset_of_RenderTextureFormat_7() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___RenderTextureFormat_7)); }
	inline int32_t get_RenderTextureFormat_7() const { return ___RenderTextureFormat_7; }
	inline int32_t* get_address_of_RenderTextureFormat_7() { return &___RenderTextureFormat_7; }
	inline void set_RenderTextureFormat_7(int32_t value)
	{
		___RenderTextureFormat_7 = value;
	}

	inline static int32_t get_offset_of_FilterMode_8() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___FilterMode_8)); }
	inline int32_t get_FilterMode_8() const { return ___FilterMode_8; }
	inline int32_t* get_address_of_FilterMode_8() { return &___FilterMode_8; }
	inline void set_FilterMode_8(int32_t value)
	{
		___FilterMode_8 = value;
	}

	inline static int32_t get_offset_of_RenderingPath_9() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___RenderingPath_9)); }
	inline int32_t get_RenderingPath_9() const { return ___RenderingPath_9; }
	inline int32_t* get_address_of_RenderingPath_9() { return &___RenderingPath_9; }
	inline void set_RenderingPath_9(int32_t value)
	{
		___RenderingPath_9 = value;
	}

	inline static int32_t get_offset_of_UseRealtimeUpdate_10() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___UseRealtimeUpdate_10)); }
	inline bool get_UseRealtimeUpdate_10() const { return ___UseRealtimeUpdate_10; }
	inline bool* get_address_of_UseRealtimeUpdate_10() { return &___UseRealtimeUpdate_10; }
	inline void set_UseRealtimeUpdate_10(bool value)
	{
		___UseRealtimeUpdate_10 = value;
	}

	inline static int32_t get_offset_of_FPSWhenMoveCamera_11() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___FPSWhenMoveCamera_11)); }
	inline int32_t get_FPSWhenMoveCamera_11() const { return ___FPSWhenMoveCamera_11; }
	inline int32_t* get_address_of_FPSWhenMoveCamera_11() { return &___FPSWhenMoveCamera_11; }
	inline void set_FPSWhenMoveCamera_11(int32_t value)
	{
		___FPSWhenMoveCamera_11 = value;
	}

	inline static int32_t get_offset_of_FPSWhenStaticCamera_12() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___FPSWhenStaticCamera_12)); }
	inline int32_t get_FPSWhenStaticCamera_12() const { return ___FPSWhenStaticCamera_12; }
	inline int32_t* get_address_of_FPSWhenStaticCamera_12() { return &___FPSWhenStaticCamera_12; }
	inline void set_FPSWhenStaticCamera_12(int32_t value)
	{
		___FPSWhenStaticCamera_12 = value;
	}

	inline static int32_t get_offset_of_renderTexture_13() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___renderTexture_13)); }
	inline RenderTexture_t2666733923 * get_renderTexture_13() const { return ___renderTexture_13; }
	inline RenderTexture_t2666733923 ** get_address_of_renderTexture_13() { return &___renderTexture_13; }
	inline void set_renderTexture_13(RenderTexture_t2666733923 * value)
	{
		___renderTexture_13 = value;
		Il2CppCodeGenWriteBarrier(&___renderTexture_13, value);
	}

	inline static int32_t get_offset_of_go_14() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___go_14)); }
	inline GameObject_t1756533147 * get_go_14() const { return ___go_14; }
	inline GameObject_t1756533147 ** get_address_of_go_14() { return &___go_14; }
	inline void set_go_14(GameObject_t1756533147 * value)
	{
		___go_14 = value;
		Il2CppCodeGenWriteBarrier(&___go_14, value);
	}

	inline static int32_t get_offset_of_reflectionCamera_15() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___reflectionCamera_15)); }
	inline Camera_t189460977 * get_reflectionCamera_15() const { return ___reflectionCamera_15; }
	inline Camera_t189460977 ** get_address_of_reflectionCamera_15() { return &___reflectionCamera_15; }
	inline void set_reflectionCamera_15(Camera_t189460977 * value)
	{
		___reflectionCamera_15 = value;
		Il2CppCodeGenWriteBarrier(&___reflectionCamera_15, value);
	}

	inline static int32_t get_offset_of_oldPosition_16() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___oldPosition_16)); }
	inline Vector3_t2243707580  get_oldPosition_16() const { return ___oldPosition_16; }
	inline Vector3_t2243707580 * get_address_of_oldPosition_16() { return &___oldPosition_16; }
	inline void set_oldPosition_16(Vector3_t2243707580  value)
	{
		___oldPosition_16 = value;
	}

	inline static int32_t get_offset_of_oldRotation_17() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___oldRotation_17)); }
	inline Quaternion_t4030073918  get_oldRotation_17() const { return ___oldRotation_17; }
	inline Quaternion_t4030073918 * get_address_of_oldRotation_17() { return &___oldRotation_17; }
	inline void set_oldRotation_17(Quaternion_t4030073918  value)
	{
		___oldRotation_17 = value;
	}

	inline static int32_t get_offset_of_instanceCameraTransform_18() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___instanceCameraTransform_18)); }
	inline Transform_t3275118058 * get_instanceCameraTransform_18() const { return ___instanceCameraTransform_18; }
	inline Transform_t3275118058 ** get_address_of_instanceCameraTransform_18() { return &___instanceCameraTransform_18; }
	inline void set_instanceCameraTransform_18(Transform_t3275118058 * value)
	{
		___instanceCameraTransform_18 = value;
		Il2CppCodeGenWriteBarrier(&___instanceCameraTransform_18, value);
	}

	inline static int32_t get_offset_of_frameCountWhenCameraIsStatic_19() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___frameCountWhenCameraIsStatic_19)); }
	inline int32_t get_frameCountWhenCameraIsStatic_19() const { return ___frameCountWhenCameraIsStatic_19; }
	inline int32_t* get_address_of_frameCountWhenCameraIsStatic_19() { return &___frameCountWhenCameraIsStatic_19; }
	inline void set_frameCountWhenCameraIsStatic_19(int32_t value)
	{
		___frameCountWhenCameraIsStatic_19 = value;
	}

	inline static int32_t get_offset_of_canUpdateCamera_20() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___canUpdateCamera_20)); }
	inline bool get_canUpdateCamera_20() const { return ___canUpdateCamera_20; }
	inline bool* get_address_of_canUpdateCamera_20() { return &___canUpdateCamera_20; }
	inline void set_canUpdateCamera_20(bool value)
	{
		___canUpdateCamera_20 = value;
	}

	inline static int32_t get_offset_of_isStaticUpdate_21() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___isStaticUpdate_21)); }
	inline bool get_isStaticUpdate_21() const { return ___isStaticUpdate_21; }
	inline bool* get_address_of_isStaticUpdate_21() { return &___isStaticUpdate_21; }
	inline void set_isStaticUpdate_21(bool value)
	{
		___isStaticUpdate_21 = value;
	}

	inline static int32_t get_offset_of_fpsMove_22() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___fpsMove_22)); }
	inline WaitForSeconds_t3839502067 * get_fpsMove_22() const { return ___fpsMove_22; }
	inline WaitForSeconds_t3839502067 ** get_address_of_fpsMove_22() { return &___fpsMove_22; }
	inline void set_fpsMove_22(WaitForSeconds_t3839502067 * value)
	{
		___fpsMove_22 = value;
		Il2CppCodeGenWriteBarrier(&___fpsMove_22, value);
	}

	inline static int32_t get_offset_of_fpsStatic_23() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___fpsStatic_23)); }
	inline WaitForSeconds_t3839502067 * get_fpsStatic_23() const { return ___fpsStatic_23; }
	inline WaitForSeconds_t3839502067 ** get_address_of_fpsStatic_23() { return &___fpsStatic_23; }
	inline void set_fpsStatic_23(WaitForSeconds_t3839502067 * value)
	{
		___fpsStatic_23 = value;
		Il2CppCodeGenWriteBarrier(&___fpsStatic_23, value);
	}

	inline static int32_t get_offset_of_currentCamera_25() { return static_cast<int32_t>(offsetof(ReflectionCamera_t2451359806, ___currentCamera_25)); }
	inline Camera_t189460977 * get_currentCamera_25() const { return ___currentCamera_25; }
	inline Camera_t189460977 ** get_address_of_currentCamera_25() { return &___currentCamera_25; }
	inline void set_currentCamera_25(Camera_t189460977 * value)
	{
		___currentCamera_25 = value;
		Il2CppCodeGenWriteBarrier(&___currentCamera_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
