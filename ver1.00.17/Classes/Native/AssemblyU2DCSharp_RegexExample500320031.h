﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// RegexHypertext
struct RegexHypertext_t2320066936;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegexExample
struct  RegexExample_t500320031  : public MonoBehaviour_t1158329972
{
public:
	// RegexHypertext RegexExample::_text
	RegexHypertext_t2320066936 * ____text_2;

public:
	inline static int32_t get_offset_of__text_2() { return static_cast<int32_t>(offsetof(RegexExample_t500320031, ____text_2)); }
	inline RegexHypertext_t2320066936 * get__text_2() const { return ____text_2; }
	inline RegexHypertext_t2320066936 ** get_address_of__text_2() { return &____text_2; }
	inline void set__text_2(RegexHypertext_t2320066936 * value)
	{
		____text_2 = value;
		Il2CppCodeGenWriteBarrier(&____text_2, value);
	}
};

struct RegexExample_t500320031_StaticFields
{
public:
	// System.Action`1<System.String> RegexExample::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_5;
	// System.Action`1<System.String> RegexExample::<>f__am$cache1
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(RegexExample_t500320031_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(RegexExample_t500320031_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
