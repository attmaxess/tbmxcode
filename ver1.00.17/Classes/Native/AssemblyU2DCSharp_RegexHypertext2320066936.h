﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HypertextBase3973674968.h"

// System.Collections.Generic.Dictionary`2<System.String,RegexHypertext/Entry>
struct Dictionary_2_t3441603169;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegexHypertext
struct  RegexHypertext_t2320066936  : public HypertextBase_t3973674968
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,RegexHypertext/Entry> RegexHypertext::_entryTable
	Dictionary_2_t3441603169 * ____entryTable_39;

public:
	inline static int32_t get_offset_of__entryTable_39() { return static_cast<int32_t>(offsetof(RegexHypertext_t2320066936, ____entryTable_39)); }
	inline Dictionary_2_t3441603169 * get__entryTable_39() const { return ____entryTable_39; }
	inline Dictionary_2_t3441603169 ** get_address_of__entryTable_39() { return &____entryTable_39; }
	inline void set__entryTable_39(Dictionary_2_t3441603169 * value)
	{
		____entryTable_39 = value;
		Il2CppCodeGenWriteBarrier(&____entryTable_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
