﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// RegexHypertext
struct RegexHypertext_t2320066936;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegexHypertextLinker
struct  RegexHypertextLinker_t2463221871  : public MonoBehaviour_t1158329972
{
public:
	// RegexHypertext RegexHypertextLinker::Text
	RegexHypertext_t2320066936 * ___Text_2;

public:
	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(RegexHypertextLinker_t2463221871, ___Text_2)); }
	inline RegexHypertext_t2320066936 * get_Text_2() const { return ___Text_2; }
	inline RegexHypertext_t2320066936 ** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(RegexHypertext_t2320066936 * value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
