﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegexHypertext/Entry
struct  Entry_t1526823907 
{
public:
	// System.String RegexHypertext/Entry::RegexPattern
	String_t* ___RegexPattern_0;
	// UnityEngine.Color RegexHypertext/Entry::Color
	Color_t2020392075  ___Color_1;
	// System.Action`1<System.String> RegexHypertext/Entry::OnClick
	Action_1_t1831019615 * ___OnClick_2;

public:
	inline static int32_t get_offset_of_RegexPattern_0() { return static_cast<int32_t>(offsetof(Entry_t1526823907, ___RegexPattern_0)); }
	inline String_t* get_RegexPattern_0() const { return ___RegexPattern_0; }
	inline String_t** get_address_of_RegexPattern_0() { return &___RegexPattern_0; }
	inline void set_RegexPattern_0(String_t* value)
	{
		___RegexPattern_0 = value;
		Il2CppCodeGenWriteBarrier(&___RegexPattern_0, value);
	}

	inline static int32_t get_offset_of_Color_1() { return static_cast<int32_t>(offsetof(Entry_t1526823907, ___Color_1)); }
	inline Color_t2020392075  get_Color_1() const { return ___Color_1; }
	inline Color_t2020392075 * get_address_of_Color_1() { return &___Color_1; }
	inline void set_Color_1(Color_t2020392075  value)
	{
		___Color_1 = value;
	}

	inline static int32_t get_offset_of_OnClick_2() { return static_cast<int32_t>(offsetof(Entry_t1526823907, ___OnClick_2)); }
	inline Action_1_t1831019615 * get_OnClick_2() const { return ___OnClick_2; }
	inline Action_1_t1831019615 ** get_address_of_OnClick_2() { return &___OnClick_2; }
	inline void set_OnClick_2(Action_1_t1831019615 * value)
	{
		___OnClick_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnClick_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of RegexHypertext/Entry
struct Entry_t1526823907_marshaled_pinvoke
{
	char* ___RegexPattern_0;
	Color_t2020392075  ___Color_1;
	Il2CppMethodPointer ___OnClick_2;
};
// Native definition for COM marshalling of RegexHypertext/Entry
struct Entry_t1526823907_marshaled_com
{
	Il2CppChar* ___RegexPattern_0;
	Color_t2020392075  ___Color_1;
	Il2CppMethodPointer ___OnClick_2;
};
