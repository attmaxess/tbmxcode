﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1967201810;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegisterButtonCrossFader
struct  RegisterButtonCrossFader_t205138403  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventTrigger RegisterButtonCrossFader::RegiterButtonEvent
	EventTrigger_t1967201810 * ___RegiterButtonEvent_2;
	// UnityEngine.UI.Image RegisterButtonCrossFader::NonActive
	Image_t2042527209 * ___NonActive_3;
	// UnityEngine.UI.Image RegisterButtonCrossFader::Register
	Image_t2042527209 * ___Register_4;
	// System.Single RegisterButtonCrossFader::CrossFadeTime
	float ___CrossFadeTime_5;
	// System.Boolean RegisterButtonCrossFader::m_bCrossFade
	bool ___m_bCrossFade_6;

public:
	inline static int32_t get_offset_of_RegiterButtonEvent_2() { return static_cast<int32_t>(offsetof(RegisterButtonCrossFader_t205138403, ___RegiterButtonEvent_2)); }
	inline EventTrigger_t1967201810 * get_RegiterButtonEvent_2() const { return ___RegiterButtonEvent_2; }
	inline EventTrigger_t1967201810 ** get_address_of_RegiterButtonEvent_2() { return &___RegiterButtonEvent_2; }
	inline void set_RegiterButtonEvent_2(EventTrigger_t1967201810 * value)
	{
		___RegiterButtonEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___RegiterButtonEvent_2, value);
	}

	inline static int32_t get_offset_of_NonActive_3() { return static_cast<int32_t>(offsetof(RegisterButtonCrossFader_t205138403, ___NonActive_3)); }
	inline Image_t2042527209 * get_NonActive_3() const { return ___NonActive_3; }
	inline Image_t2042527209 ** get_address_of_NonActive_3() { return &___NonActive_3; }
	inline void set_NonActive_3(Image_t2042527209 * value)
	{
		___NonActive_3 = value;
		Il2CppCodeGenWriteBarrier(&___NonActive_3, value);
	}

	inline static int32_t get_offset_of_Register_4() { return static_cast<int32_t>(offsetof(RegisterButtonCrossFader_t205138403, ___Register_4)); }
	inline Image_t2042527209 * get_Register_4() const { return ___Register_4; }
	inline Image_t2042527209 ** get_address_of_Register_4() { return &___Register_4; }
	inline void set_Register_4(Image_t2042527209 * value)
	{
		___Register_4 = value;
		Il2CppCodeGenWriteBarrier(&___Register_4, value);
	}

	inline static int32_t get_offset_of_CrossFadeTime_5() { return static_cast<int32_t>(offsetof(RegisterButtonCrossFader_t205138403, ___CrossFadeTime_5)); }
	inline float get_CrossFadeTime_5() const { return ___CrossFadeTime_5; }
	inline float* get_address_of_CrossFadeTime_5() { return &___CrossFadeTime_5; }
	inline void set_CrossFadeTime_5(float value)
	{
		___CrossFadeTime_5 = value;
	}

	inline static int32_t get_offset_of_m_bCrossFade_6() { return static_cast<int32_t>(offsetof(RegisterButtonCrossFader_t205138403, ___m_bCrossFade_6)); }
	inline bool get_m_bCrossFade_6() const { return ___m_bCrossFade_6; }
	inline bool* get_address_of_m_bCrossFade_6() { return &___m_bCrossFade_6; }
	inline void set_m_bCrossFade_6(bool value)
	{
		___m_bCrossFade_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
