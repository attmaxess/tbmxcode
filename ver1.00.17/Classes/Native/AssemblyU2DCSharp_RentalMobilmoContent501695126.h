﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RentalMobilmoContent
struct  RentalMobilmoContent_t501695126  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text RentalMobilmoContent::Name
	Text_t356221433 * ___Name_2;
	// UnityEngine.UI.Image RentalMobilmoContent::Image
	Image_t2042527209 * ___Image_3;
	// UnityEngine.UI.Text RentalMobilmoContent::Info
	Text_t356221433 * ___Info_4;
	// UnityEngine.UI.Button RentalMobilmoContent::SelectButton
	Button_t2872111280 * ___SelectButton_5;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(RentalMobilmoContent_t501695126, ___Name_2)); }
	inline Text_t356221433 * get_Name_2() const { return ___Name_2; }
	inline Text_t356221433 ** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(Text_t356221433 * value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier(&___Name_2, value);
	}

	inline static int32_t get_offset_of_Image_3() { return static_cast<int32_t>(offsetof(RentalMobilmoContent_t501695126, ___Image_3)); }
	inline Image_t2042527209 * get_Image_3() const { return ___Image_3; }
	inline Image_t2042527209 ** get_address_of_Image_3() { return &___Image_3; }
	inline void set_Image_3(Image_t2042527209 * value)
	{
		___Image_3 = value;
		Il2CppCodeGenWriteBarrier(&___Image_3, value);
	}

	inline static int32_t get_offset_of_Info_4() { return static_cast<int32_t>(offsetof(RentalMobilmoContent_t501695126, ___Info_4)); }
	inline Text_t356221433 * get_Info_4() const { return ___Info_4; }
	inline Text_t356221433 ** get_address_of_Info_4() { return &___Info_4; }
	inline void set_Info_4(Text_t356221433 * value)
	{
		___Info_4 = value;
		Il2CppCodeGenWriteBarrier(&___Info_4, value);
	}

	inline static int32_t get_offset_of_SelectButton_5() { return static_cast<int32_t>(offsetof(RentalMobilmoContent_t501695126, ___SelectButton_5)); }
	inline Button_t2872111280 * get_SelectButton_5() const { return ___SelectButton_5; }
	inline Button_t2872111280 ** get_address_of_SelectButton_5() { return &___SelectButton_5; }
	inline void set_SelectButton_5(Button_t2872111280 * value)
	{
		___SelectButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___SelectButton_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
