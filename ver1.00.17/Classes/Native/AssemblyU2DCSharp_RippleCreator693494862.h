﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// WaterRipples
struct WaterRipples_t656902750;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.Queue`1<RippleCreator/ReversedRipple>
struct Queue_1_t2164466792;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RippleCreator
struct  RippleCreator_t693494862  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean RippleCreator::IsReversedRipple
	bool ___IsReversedRipple_2;
	// System.Single RippleCreator::RippleStrenght
	float ___RippleStrenght_3;
	// System.Single RippleCreator::MaxVelocity
	float ___MaxVelocity_4;
	// System.Single RippleCreator::RandomRipplesInterval
	float ___RandomRipplesInterval_5;
	// System.Single RippleCreator::reversedRippleDelay
	float ___reversedRippleDelay_6;
	// UnityEngine.GameObject RippleCreator::SplashEffect
	GameObject_t1756533147 * ___SplashEffect_7;
	// UnityEngine.GameObject RippleCreator::SplashEffectMoved
	GameObject_t1756533147 * ___SplashEffectMoved_8;
	// UnityEngine.AudioSource RippleCreator::SplashAudioSource
	AudioSource_t1135106623 * ___SplashAudioSource_9;
	// System.Int32 RippleCreator::fadeInVelocityLimit
	int32_t ___fadeInVelocityLimit_10;
	// System.Int32 RippleCreator::fadeInVelocity
	int32_t ___fadeInVelocity_11;
	// WaterRipples RippleCreator::waterRipples
	WaterRipples_t656902750 * ___waterRipples_12;
	// UnityEngine.Vector3 RippleCreator::oldPos
	Vector3_t2243707580  ___oldPos_13;
	// System.Single RippleCreator::currentVelocity
	float ___currentVelocity_14;
	// UnityEngine.Transform RippleCreator::t
	Transform_t3275118058 * ___t_15;
	// System.Collections.Generic.Queue`1<RippleCreator/ReversedRipple> RippleCreator::reversedVelocityQueue
	Queue_1_t2164466792 * ___reversedVelocityQueue_16;
	// System.Single RippleCreator::triggeredTime
	float ___triggeredTime_17;
	// System.Boolean RippleCreator::canUpdate
	bool ___canUpdate_18;
	// System.Single RippleCreator::randomRipplesCurrentTime
	float ___randomRipplesCurrentTime_19;
	// System.Boolean RippleCreator::canInstantiateRandomRipple
	bool ___canInstantiateRandomRipple_20;
	// UnityEngine.GameObject RippleCreator::splashMovedInstance
	GameObject_t1756533147 * ___splashMovedInstance_21;
	// UnityEngine.ParticleSystem RippleCreator::splashParticleSystem
	ParticleSystem_t3394631041 * ___splashParticleSystem_22;
	// System.Single RippleCreator::splashSizeMultiplier
	float ___splashSizeMultiplier_23;

public:
	inline static int32_t get_offset_of_IsReversedRipple_2() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___IsReversedRipple_2)); }
	inline bool get_IsReversedRipple_2() const { return ___IsReversedRipple_2; }
	inline bool* get_address_of_IsReversedRipple_2() { return &___IsReversedRipple_2; }
	inline void set_IsReversedRipple_2(bool value)
	{
		___IsReversedRipple_2 = value;
	}

	inline static int32_t get_offset_of_RippleStrenght_3() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___RippleStrenght_3)); }
	inline float get_RippleStrenght_3() const { return ___RippleStrenght_3; }
	inline float* get_address_of_RippleStrenght_3() { return &___RippleStrenght_3; }
	inline void set_RippleStrenght_3(float value)
	{
		___RippleStrenght_3 = value;
	}

	inline static int32_t get_offset_of_MaxVelocity_4() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___MaxVelocity_4)); }
	inline float get_MaxVelocity_4() const { return ___MaxVelocity_4; }
	inline float* get_address_of_MaxVelocity_4() { return &___MaxVelocity_4; }
	inline void set_MaxVelocity_4(float value)
	{
		___MaxVelocity_4 = value;
	}

	inline static int32_t get_offset_of_RandomRipplesInterval_5() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___RandomRipplesInterval_5)); }
	inline float get_RandomRipplesInterval_5() const { return ___RandomRipplesInterval_5; }
	inline float* get_address_of_RandomRipplesInterval_5() { return &___RandomRipplesInterval_5; }
	inline void set_RandomRipplesInterval_5(float value)
	{
		___RandomRipplesInterval_5 = value;
	}

	inline static int32_t get_offset_of_reversedRippleDelay_6() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___reversedRippleDelay_6)); }
	inline float get_reversedRippleDelay_6() const { return ___reversedRippleDelay_6; }
	inline float* get_address_of_reversedRippleDelay_6() { return &___reversedRippleDelay_6; }
	inline void set_reversedRippleDelay_6(float value)
	{
		___reversedRippleDelay_6 = value;
	}

	inline static int32_t get_offset_of_SplashEffect_7() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___SplashEffect_7)); }
	inline GameObject_t1756533147 * get_SplashEffect_7() const { return ___SplashEffect_7; }
	inline GameObject_t1756533147 ** get_address_of_SplashEffect_7() { return &___SplashEffect_7; }
	inline void set_SplashEffect_7(GameObject_t1756533147 * value)
	{
		___SplashEffect_7 = value;
		Il2CppCodeGenWriteBarrier(&___SplashEffect_7, value);
	}

	inline static int32_t get_offset_of_SplashEffectMoved_8() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___SplashEffectMoved_8)); }
	inline GameObject_t1756533147 * get_SplashEffectMoved_8() const { return ___SplashEffectMoved_8; }
	inline GameObject_t1756533147 ** get_address_of_SplashEffectMoved_8() { return &___SplashEffectMoved_8; }
	inline void set_SplashEffectMoved_8(GameObject_t1756533147 * value)
	{
		___SplashEffectMoved_8 = value;
		Il2CppCodeGenWriteBarrier(&___SplashEffectMoved_8, value);
	}

	inline static int32_t get_offset_of_SplashAudioSource_9() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___SplashAudioSource_9)); }
	inline AudioSource_t1135106623 * get_SplashAudioSource_9() const { return ___SplashAudioSource_9; }
	inline AudioSource_t1135106623 ** get_address_of_SplashAudioSource_9() { return &___SplashAudioSource_9; }
	inline void set_SplashAudioSource_9(AudioSource_t1135106623 * value)
	{
		___SplashAudioSource_9 = value;
		Il2CppCodeGenWriteBarrier(&___SplashAudioSource_9, value);
	}

	inline static int32_t get_offset_of_fadeInVelocityLimit_10() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___fadeInVelocityLimit_10)); }
	inline int32_t get_fadeInVelocityLimit_10() const { return ___fadeInVelocityLimit_10; }
	inline int32_t* get_address_of_fadeInVelocityLimit_10() { return &___fadeInVelocityLimit_10; }
	inline void set_fadeInVelocityLimit_10(int32_t value)
	{
		___fadeInVelocityLimit_10 = value;
	}

	inline static int32_t get_offset_of_fadeInVelocity_11() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___fadeInVelocity_11)); }
	inline int32_t get_fadeInVelocity_11() const { return ___fadeInVelocity_11; }
	inline int32_t* get_address_of_fadeInVelocity_11() { return &___fadeInVelocity_11; }
	inline void set_fadeInVelocity_11(int32_t value)
	{
		___fadeInVelocity_11 = value;
	}

	inline static int32_t get_offset_of_waterRipples_12() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___waterRipples_12)); }
	inline WaterRipples_t656902750 * get_waterRipples_12() const { return ___waterRipples_12; }
	inline WaterRipples_t656902750 ** get_address_of_waterRipples_12() { return &___waterRipples_12; }
	inline void set_waterRipples_12(WaterRipples_t656902750 * value)
	{
		___waterRipples_12 = value;
		Il2CppCodeGenWriteBarrier(&___waterRipples_12, value);
	}

	inline static int32_t get_offset_of_oldPos_13() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___oldPos_13)); }
	inline Vector3_t2243707580  get_oldPos_13() const { return ___oldPos_13; }
	inline Vector3_t2243707580 * get_address_of_oldPos_13() { return &___oldPos_13; }
	inline void set_oldPos_13(Vector3_t2243707580  value)
	{
		___oldPos_13 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_14() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___currentVelocity_14)); }
	inline float get_currentVelocity_14() const { return ___currentVelocity_14; }
	inline float* get_address_of_currentVelocity_14() { return &___currentVelocity_14; }
	inline void set_currentVelocity_14(float value)
	{
		___currentVelocity_14 = value;
	}

	inline static int32_t get_offset_of_t_15() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___t_15)); }
	inline Transform_t3275118058 * get_t_15() const { return ___t_15; }
	inline Transform_t3275118058 ** get_address_of_t_15() { return &___t_15; }
	inline void set_t_15(Transform_t3275118058 * value)
	{
		___t_15 = value;
		Il2CppCodeGenWriteBarrier(&___t_15, value);
	}

	inline static int32_t get_offset_of_reversedVelocityQueue_16() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___reversedVelocityQueue_16)); }
	inline Queue_1_t2164466792 * get_reversedVelocityQueue_16() const { return ___reversedVelocityQueue_16; }
	inline Queue_1_t2164466792 ** get_address_of_reversedVelocityQueue_16() { return &___reversedVelocityQueue_16; }
	inline void set_reversedVelocityQueue_16(Queue_1_t2164466792 * value)
	{
		___reversedVelocityQueue_16 = value;
		Il2CppCodeGenWriteBarrier(&___reversedVelocityQueue_16, value);
	}

	inline static int32_t get_offset_of_triggeredTime_17() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___triggeredTime_17)); }
	inline float get_triggeredTime_17() const { return ___triggeredTime_17; }
	inline float* get_address_of_triggeredTime_17() { return &___triggeredTime_17; }
	inline void set_triggeredTime_17(float value)
	{
		___triggeredTime_17 = value;
	}

	inline static int32_t get_offset_of_canUpdate_18() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___canUpdate_18)); }
	inline bool get_canUpdate_18() const { return ___canUpdate_18; }
	inline bool* get_address_of_canUpdate_18() { return &___canUpdate_18; }
	inline void set_canUpdate_18(bool value)
	{
		___canUpdate_18 = value;
	}

	inline static int32_t get_offset_of_randomRipplesCurrentTime_19() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___randomRipplesCurrentTime_19)); }
	inline float get_randomRipplesCurrentTime_19() const { return ___randomRipplesCurrentTime_19; }
	inline float* get_address_of_randomRipplesCurrentTime_19() { return &___randomRipplesCurrentTime_19; }
	inline void set_randomRipplesCurrentTime_19(float value)
	{
		___randomRipplesCurrentTime_19 = value;
	}

	inline static int32_t get_offset_of_canInstantiateRandomRipple_20() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___canInstantiateRandomRipple_20)); }
	inline bool get_canInstantiateRandomRipple_20() const { return ___canInstantiateRandomRipple_20; }
	inline bool* get_address_of_canInstantiateRandomRipple_20() { return &___canInstantiateRandomRipple_20; }
	inline void set_canInstantiateRandomRipple_20(bool value)
	{
		___canInstantiateRandomRipple_20 = value;
	}

	inline static int32_t get_offset_of_splashMovedInstance_21() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___splashMovedInstance_21)); }
	inline GameObject_t1756533147 * get_splashMovedInstance_21() const { return ___splashMovedInstance_21; }
	inline GameObject_t1756533147 ** get_address_of_splashMovedInstance_21() { return &___splashMovedInstance_21; }
	inline void set_splashMovedInstance_21(GameObject_t1756533147 * value)
	{
		___splashMovedInstance_21 = value;
		Il2CppCodeGenWriteBarrier(&___splashMovedInstance_21, value);
	}

	inline static int32_t get_offset_of_splashParticleSystem_22() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___splashParticleSystem_22)); }
	inline ParticleSystem_t3394631041 * get_splashParticleSystem_22() const { return ___splashParticleSystem_22; }
	inline ParticleSystem_t3394631041 ** get_address_of_splashParticleSystem_22() { return &___splashParticleSystem_22; }
	inline void set_splashParticleSystem_22(ParticleSystem_t3394631041 * value)
	{
		___splashParticleSystem_22 = value;
		Il2CppCodeGenWriteBarrier(&___splashParticleSystem_22, value);
	}

	inline static int32_t get_offset_of_splashSizeMultiplier_23() { return static_cast<int32_t>(offsetof(RippleCreator_t693494862, ___splashSizeMultiplier_23)); }
	inline float get_splashSizeMultiplier_23() const { return ___splashSizeMultiplier_23; }
	inline float* get_address_of_splashSizeMultiplier_23() { return &___splashSizeMultiplier_23; }
	inline void set_splashSizeMultiplier_23(float value)
	{
		___splashSizeMultiplier_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
