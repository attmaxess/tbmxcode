﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RippleCreator/ReversedRipple
struct  ReversedRipple_t2344809957  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 RippleCreator/ReversedRipple::Position
	Vector3_t2243707580  ___Position_0;
	// System.Single RippleCreator/ReversedRipple::Velocity
	float ___Velocity_1;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(ReversedRipple_t2344809957, ___Position_0)); }
	inline Vector3_t2243707580  get_Position_0() const { return ___Position_0; }
	inline Vector3_t2243707580 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t2243707580  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Velocity_1() { return static_cast<int32_t>(offsetof(ReversedRipple_t2344809957, ___Velocity_1)); }
	inline float get_Velocity_1() const { return ___Velocity_1; }
	inline float* get_address_of_Velocity_1() { return &___Velocity_1; }
	inline void set_Velocity_1(float value)
	{
		___Velocity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
