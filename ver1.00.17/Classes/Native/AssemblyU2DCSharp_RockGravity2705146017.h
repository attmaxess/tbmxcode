﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RockGravity
struct  RockGravity_t2705146017  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RockGravity::m_fAccelerationScale
	float ___m_fAccelerationScale_2;
	// UnityEngine.GameObject RockGravity::m_gPlanet
	GameObject_t1756533147 * ___m_gPlanet_3;
	// System.Boolean RockGravity::rockDrop
	bool ___rockDrop_4;

public:
	inline static int32_t get_offset_of_m_fAccelerationScale_2() { return static_cast<int32_t>(offsetof(RockGravity_t2705146017, ___m_fAccelerationScale_2)); }
	inline float get_m_fAccelerationScale_2() const { return ___m_fAccelerationScale_2; }
	inline float* get_address_of_m_fAccelerationScale_2() { return &___m_fAccelerationScale_2; }
	inline void set_m_fAccelerationScale_2(float value)
	{
		___m_fAccelerationScale_2 = value;
	}

	inline static int32_t get_offset_of_m_gPlanet_3() { return static_cast<int32_t>(offsetof(RockGravity_t2705146017, ___m_gPlanet_3)); }
	inline GameObject_t1756533147 * get_m_gPlanet_3() const { return ___m_gPlanet_3; }
	inline GameObject_t1756533147 ** get_address_of_m_gPlanet_3() { return &___m_gPlanet_3; }
	inline void set_m_gPlanet_3(GameObject_t1756533147 * value)
	{
		___m_gPlanet_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_gPlanet_3, value);
	}

	inline static int32_t get_offset_of_rockDrop_4() { return static_cast<int32_t>(offsetof(RockGravity_t2705146017, ___rockDrop_4)); }
	inline bool get_rockDrop_4() const { return ___rockDrop_4; }
	inline bool* get_address_of_rockDrop_4() { return &___rockDrop_4; }
	inline void set_rockDrop_4(bool value)
	{
		___rockDrop_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
