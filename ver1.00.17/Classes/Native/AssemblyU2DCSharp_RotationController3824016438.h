﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2520692773.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotationController
struct  RotationController_t3824016438  : public SingletonMonoBehaviour_1_t2520692773
{
public:
	// System.Single RotationController::OffsetSpeedX
	float ___OffsetSpeedX_3;
	// System.Single RotationController::OffsetSpeedY
	float ___OffsetSpeedY_4;
	// System.Boolean RotationController::TouchObject
	bool ___TouchObject_5;
	// UnityEngine.Transform RotationController::selectObject
	Transform_t3275118058 * ___selectObject_6;
	// UnityEngine.Transform RotationController::m_partsObject
	Transform_t3275118058 * ___m_partsObject_7;
	// UnityEngine.Transform RotationController::m_slidePartsObject
	Transform_t3275118058 * ___m_slidePartsObject_8;
	// UnityEngine.Transform RotationController::m_poleDecisionObject
	Transform_t3275118058 * ___m_poleDecisionObject_9;
	// System.Single RotationController::m_fXangle
	float ___m_fXangle_10;
	// System.Single RotationController::m_fZangle
	float ___m_fZangle_11;
	// System.Single RotationController::m_fClickStartPos
	float ___m_fClickStartPos_12;
	// System.Boolean RotationController::m_bPoleShift
	bool ___m_bPoleShift_13;
	// UnityEngine.Vector3 RotationController::mousePos
	Vector3_t2243707580  ___mousePos_14;
	// System.Boolean RotationController::m_bMouseDirect
	bool ___m_bMouseDirect_15;
	// UnityEngine.Vector3 RotationController::startPos
	Vector3_t2243707580  ___startPos_16;
	// UnityEngine.Vector3 RotationController::endPos
	Vector3_t2243707580  ___endPos_17;
	// System.Single RotationController::SEtimer
	float ___SEtimer_18;
	// System.Single RotationController::swipeDistX
	float ___swipeDistX_19;
	// System.Single RotationController::swipeDistY
	float ___swipeDistY_20;
	// System.Single RotationController::SignValueX
	float ___SignValueX_21;
	// System.Single RotationController::SignValueY
	float ___SignValueY_22;
	// System.Single RotationController::minSwipeDistX
	float ___minSwipeDistX_23;
	// System.Single RotationController::minSwipeDistY
	float ___minSwipeDistY_24;

public:
	inline static int32_t get_offset_of_OffsetSpeedX_3() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___OffsetSpeedX_3)); }
	inline float get_OffsetSpeedX_3() const { return ___OffsetSpeedX_3; }
	inline float* get_address_of_OffsetSpeedX_3() { return &___OffsetSpeedX_3; }
	inline void set_OffsetSpeedX_3(float value)
	{
		___OffsetSpeedX_3 = value;
	}

	inline static int32_t get_offset_of_OffsetSpeedY_4() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___OffsetSpeedY_4)); }
	inline float get_OffsetSpeedY_4() const { return ___OffsetSpeedY_4; }
	inline float* get_address_of_OffsetSpeedY_4() { return &___OffsetSpeedY_4; }
	inline void set_OffsetSpeedY_4(float value)
	{
		___OffsetSpeedY_4 = value;
	}

	inline static int32_t get_offset_of_TouchObject_5() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___TouchObject_5)); }
	inline bool get_TouchObject_5() const { return ___TouchObject_5; }
	inline bool* get_address_of_TouchObject_5() { return &___TouchObject_5; }
	inline void set_TouchObject_5(bool value)
	{
		___TouchObject_5 = value;
	}

	inline static int32_t get_offset_of_selectObject_6() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___selectObject_6)); }
	inline Transform_t3275118058 * get_selectObject_6() const { return ___selectObject_6; }
	inline Transform_t3275118058 ** get_address_of_selectObject_6() { return &___selectObject_6; }
	inline void set_selectObject_6(Transform_t3275118058 * value)
	{
		___selectObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___selectObject_6, value);
	}

	inline static int32_t get_offset_of_m_partsObject_7() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_partsObject_7)); }
	inline Transform_t3275118058 * get_m_partsObject_7() const { return ___m_partsObject_7; }
	inline Transform_t3275118058 ** get_address_of_m_partsObject_7() { return &___m_partsObject_7; }
	inline void set_m_partsObject_7(Transform_t3275118058 * value)
	{
		___m_partsObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_partsObject_7, value);
	}

	inline static int32_t get_offset_of_m_slidePartsObject_8() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_slidePartsObject_8)); }
	inline Transform_t3275118058 * get_m_slidePartsObject_8() const { return ___m_slidePartsObject_8; }
	inline Transform_t3275118058 ** get_address_of_m_slidePartsObject_8() { return &___m_slidePartsObject_8; }
	inline void set_m_slidePartsObject_8(Transform_t3275118058 * value)
	{
		___m_slidePartsObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_slidePartsObject_8, value);
	}

	inline static int32_t get_offset_of_m_poleDecisionObject_9() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_poleDecisionObject_9)); }
	inline Transform_t3275118058 * get_m_poleDecisionObject_9() const { return ___m_poleDecisionObject_9; }
	inline Transform_t3275118058 ** get_address_of_m_poleDecisionObject_9() { return &___m_poleDecisionObject_9; }
	inline void set_m_poleDecisionObject_9(Transform_t3275118058 * value)
	{
		___m_poleDecisionObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_poleDecisionObject_9, value);
	}

	inline static int32_t get_offset_of_m_fXangle_10() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_fXangle_10)); }
	inline float get_m_fXangle_10() const { return ___m_fXangle_10; }
	inline float* get_address_of_m_fXangle_10() { return &___m_fXangle_10; }
	inline void set_m_fXangle_10(float value)
	{
		___m_fXangle_10 = value;
	}

	inline static int32_t get_offset_of_m_fZangle_11() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_fZangle_11)); }
	inline float get_m_fZangle_11() const { return ___m_fZangle_11; }
	inline float* get_address_of_m_fZangle_11() { return &___m_fZangle_11; }
	inline void set_m_fZangle_11(float value)
	{
		___m_fZangle_11 = value;
	}

	inline static int32_t get_offset_of_m_fClickStartPos_12() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_fClickStartPos_12)); }
	inline float get_m_fClickStartPos_12() const { return ___m_fClickStartPos_12; }
	inline float* get_address_of_m_fClickStartPos_12() { return &___m_fClickStartPos_12; }
	inline void set_m_fClickStartPos_12(float value)
	{
		___m_fClickStartPos_12 = value;
	}

	inline static int32_t get_offset_of_m_bPoleShift_13() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_bPoleShift_13)); }
	inline bool get_m_bPoleShift_13() const { return ___m_bPoleShift_13; }
	inline bool* get_address_of_m_bPoleShift_13() { return &___m_bPoleShift_13; }
	inline void set_m_bPoleShift_13(bool value)
	{
		___m_bPoleShift_13 = value;
	}

	inline static int32_t get_offset_of_mousePos_14() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___mousePos_14)); }
	inline Vector3_t2243707580  get_mousePos_14() const { return ___mousePos_14; }
	inline Vector3_t2243707580 * get_address_of_mousePos_14() { return &___mousePos_14; }
	inline void set_mousePos_14(Vector3_t2243707580  value)
	{
		___mousePos_14 = value;
	}

	inline static int32_t get_offset_of_m_bMouseDirect_15() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___m_bMouseDirect_15)); }
	inline bool get_m_bMouseDirect_15() const { return ___m_bMouseDirect_15; }
	inline bool* get_address_of_m_bMouseDirect_15() { return &___m_bMouseDirect_15; }
	inline void set_m_bMouseDirect_15(bool value)
	{
		___m_bMouseDirect_15 = value;
	}

	inline static int32_t get_offset_of_startPos_16() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___startPos_16)); }
	inline Vector3_t2243707580  get_startPos_16() const { return ___startPos_16; }
	inline Vector3_t2243707580 * get_address_of_startPos_16() { return &___startPos_16; }
	inline void set_startPos_16(Vector3_t2243707580  value)
	{
		___startPos_16 = value;
	}

	inline static int32_t get_offset_of_endPos_17() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___endPos_17)); }
	inline Vector3_t2243707580  get_endPos_17() const { return ___endPos_17; }
	inline Vector3_t2243707580 * get_address_of_endPos_17() { return &___endPos_17; }
	inline void set_endPos_17(Vector3_t2243707580  value)
	{
		___endPos_17 = value;
	}

	inline static int32_t get_offset_of_SEtimer_18() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___SEtimer_18)); }
	inline float get_SEtimer_18() const { return ___SEtimer_18; }
	inline float* get_address_of_SEtimer_18() { return &___SEtimer_18; }
	inline void set_SEtimer_18(float value)
	{
		___SEtimer_18 = value;
	}

	inline static int32_t get_offset_of_swipeDistX_19() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___swipeDistX_19)); }
	inline float get_swipeDistX_19() const { return ___swipeDistX_19; }
	inline float* get_address_of_swipeDistX_19() { return &___swipeDistX_19; }
	inline void set_swipeDistX_19(float value)
	{
		___swipeDistX_19 = value;
	}

	inline static int32_t get_offset_of_swipeDistY_20() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___swipeDistY_20)); }
	inline float get_swipeDistY_20() const { return ___swipeDistY_20; }
	inline float* get_address_of_swipeDistY_20() { return &___swipeDistY_20; }
	inline void set_swipeDistY_20(float value)
	{
		___swipeDistY_20 = value;
	}

	inline static int32_t get_offset_of_SignValueX_21() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___SignValueX_21)); }
	inline float get_SignValueX_21() const { return ___SignValueX_21; }
	inline float* get_address_of_SignValueX_21() { return &___SignValueX_21; }
	inline void set_SignValueX_21(float value)
	{
		___SignValueX_21 = value;
	}

	inline static int32_t get_offset_of_SignValueY_22() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___SignValueY_22)); }
	inline float get_SignValueY_22() const { return ___SignValueY_22; }
	inline float* get_address_of_SignValueY_22() { return &___SignValueY_22; }
	inline void set_SignValueY_22(float value)
	{
		___SignValueY_22 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistX_23() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___minSwipeDistX_23)); }
	inline float get_minSwipeDistX_23() const { return ___minSwipeDistX_23; }
	inline float* get_address_of_minSwipeDistX_23() { return &___minSwipeDistX_23; }
	inline void set_minSwipeDistX_23(float value)
	{
		___minSwipeDistX_23 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistY_24() { return static_cast<int32_t>(offsetof(RotationController_t3824016438, ___minSwipeDistY_24)); }
	inline float get_minSwipeDistY_24() const { return ___minSwipeDistY_24; }
	inline float* get_address_of_minSwipeDistY_24() { return &___minSwipeDistY_24; }
	inline void set_minSwipeDistY_24(float value)
	{
		___minSwipeDistY_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
