﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_t725277715;
// System.String[]
struct StringU5BU5D_t1642385972;
// RuntimeStlLoader
struct RuntimeStlLoader_t3120181086;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RuntimeStlLoader
struct  RuntimeStlLoader_t3120181086  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 RuntimeStlLoader::m_vetex_limit
	int32_t ___m_vetex_limit_2;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> RuntimeStlLoader::m_tmp_meshes
	List_1_t725277715 * ___m_tmp_meshes_3;
	// System.Boolean RuntimeStlLoader::m_is_read
	bool ___m_is_read_4;
	// System.String[] RuntimeStlLoader::m_text_regex_ptn
	StringU5BU5D_t1642385972* ___m_text_regex_ptn_5;

public:
	inline static int32_t get_offset_of_m_vetex_limit_2() { return static_cast<int32_t>(offsetof(RuntimeStlLoader_t3120181086, ___m_vetex_limit_2)); }
	inline int32_t get_m_vetex_limit_2() const { return ___m_vetex_limit_2; }
	inline int32_t* get_address_of_m_vetex_limit_2() { return &___m_vetex_limit_2; }
	inline void set_m_vetex_limit_2(int32_t value)
	{
		___m_vetex_limit_2 = value;
	}

	inline static int32_t get_offset_of_m_tmp_meshes_3() { return static_cast<int32_t>(offsetof(RuntimeStlLoader_t3120181086, ___m_tmp_meshes_3)); }
	inline List_1_t725277715 * get_m_tmp_meshes_3() const { return ___m_tmp_meshes_3; }
	inline List_1_t725277715 ** get_address_of_m_tmp_meshes_3() { return &___m_tmp_meshes_3; }
	inline void set_m_tmp_meshes_3(List_1_t725277715 * value)
	{
		___m_tmp_meshes_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tmp_meshes_3, value);
	}

	inline static int32_t get_offset_of_m_is_read_4() { return static_cast<int32_t>(offsetof(RuntimeStlLoader_t3120181086, ___m_is_read_4)); }
	inline bool get_m_is_read_4() const { return ___m_is_read_4; }
	inline bool* get_address_of_m_is_read_4() { return &___m_is_read_4; }
	inline void set_m_is_read_4(bool value)
	{
		___m_is_read_4 = value;
	}

	inline static int32_t get_offset_of_m_text_regex_ptn_5() { return static_cast<int32_t>(offsetof(RuntimeStlLoader_t3120181086, ___m_text_regex_ptn_5)); }
	inline StringU5BU5D_t1642385972* get_m_text_regex_ptn_5() const { return ___m_text_regex_ptn_5; }
	inline StringU5BU5D_t1642385972** get_address_of_m_text_regex_ptn_5() { return &___m_text_regex_ptn_5; }
	inline void set_m_text_regex_ptn_5(StringU5BU5D_t1642385972* value)
	{
		___m_text_regex_ptn_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_text_regex_ptn_5, value);
	}
};

struct RuntimeStlLoader_t3120181086_StaticFields
{
public:
	// RuntimeStlLoader RuntimeStlLoader::m_instance
	RuntimeStlLoader_t3120181086 * ___m_instance_6;

public:
	inline static int32_t get_offset_of_m_instance_6() { return static_cast<int32_t>(offsetof(RuntimeStlLoader_t3120181086_StaticFields, ___m_instance_6)); }
	inline RuntimeStlLoader_t3120181086 * get_m_instance_6() const { return ___m_instance_6; }
	inline RuntimeStlLoader_t3120181086 ** get_address_of_m_instance_6() { return &___m_instance_6; }
	inline void set_m_instance_6(RuntimeStlLoader_t3120181086 * value)
	{
		___m_instance_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_instance_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
