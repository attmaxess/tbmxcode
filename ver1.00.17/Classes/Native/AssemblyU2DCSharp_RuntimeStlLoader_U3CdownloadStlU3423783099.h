﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_RuntimeStlLoader_EN_STL_FILE_FORM140015201.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_t725277715;
// RuntimeStlLoader/OnCompleteLoadUrl
struct OnCompleteLoadUrl_t162185122;
// RuntimeStlLoader
struct RuntimeStlLoader_t3120181086;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RuntimeStlLoader/<downloadStl>c__Iterator0
struct  U3CdownloadStlU3Ec__Iterator0_t3423783099  : public Il2CppObject
{
public:
	// UnityEngine.GameObject RuntimeStlLoader/<downloadStl>c__Iterator0::prefab
	GameObject_t1756533147 * ___prefab_0;
	// System.String RuntimeStlLoader/<downloadStl>c__Iterator0::url
	String_t* ___url_1;
	// UnityEngine.WWW RuntimeStlLoader/<downloadStl>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_2;
	// UnityEngine.GameObject RuntimeStlLoader/<downloadStl>c__Iterator0::parent
	GameObject_t1756533147 * ___parent_3;
	// RuntimeStlLoader/EN_STL_FILE_FORMAT RuntimeStlLoader/<downloadStl>c__Iterator0::<format>__0
	int32_t ___U3CformatU3E__0_4;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> RuntimeStlLoader/<downloadStl>c__Iterator0::<mesh_list>__0
	List_1_t725277715 * ___U3Cmesh_listU3E__0_5;
	// RuntimeStlLoader/OnCompleteLoadUrl RuntimeStlLoader/<downloadStl>c__Iterator0::func
	OnCompleteLoadUrl_t162185122 * ___func_6;
	// RuntimeStlLoader RuntimeStlLoader/<downloadStl>c__Iterator0::$this
	RuntimeStlLoader_t3120181086 * ___U24this_7;
	// System.Object RuntimeStlLoader/<downloadStl>c__Iterator0::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean RuntimeStlLoader/<downloadStl>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 RuntimeStlLoader/<downloadStl>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___prefab_0)); }
	inline GameObject_t1756533147 * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_t1756533147 ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_t1756533147 * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_0, value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier(&___url_1, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___U3CwwwU3E__0_2)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_2, value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___parent_3)); }
	inline GameObject_t1756533147 * get_parent_3() const { return ___parent_3; }
	inline GameObject_t1756533147 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(GameObject_t1756533147 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parent_3, value);
	}

	inline static int32_t get_offset_of_U3CformatU3E__0_4() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___U3CformatU3E__0_4)); }
	inline int32_t get_U3CformatU3E__0_4() const { return ___U3CformatU3E__0_4; }
	inline int32_t* get_address_of_U3CformatU3E__0_4() { return &___U3CformatU3E__0_4; }
	inline void set_U3CformatU3E__0_4(int32_t value)
	{
		___U3CformatU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3Cmesh_listU3E__0_5() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___U3Cmesh_listU3E__0_5)); }
	inline List_1_t725277715 * get_U3Cmesh_listU3E__0_5() const { return ___U3Cmesh_listU3E__0_5; }
	inline List_1_t725277715 ** get_address_of_U3Cmesh_listU3E__0_5() { return &___U3Cmesh_listU3E__0_5; }
	inline void set_U3Cmesh_listU3E__0_5(List_1_t725277715 * value)
	{
		___U3Cmesh_listU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cmesh_listU3E__0_5, value);
	}

	inline static int32_t get_offset_of_func_6() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___func_6)); }
	inline OnCompleteLoadUrl_t162185122 * get_func_6() const { return ___func_6; }
	inline OnCompleteLoadUrl_t162185122 ** get_address_of_func_6() { return &___func_6; }
	inline void set_func_6(OnCompleteLoadUrl_t162185122 * value)
	{
		___func_6 = value;
		Il2CppCodeGenWriteBarrier(&___func_6, value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___U24this_7)); }
	inline RuntimeStlLoader_t3120181086 * get_U24this_7() const { return ___U24this_7; }
	inline RuntimeStlLoader_t3120181086 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(RuntimeStlLoader_t3120181086 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CdownloadStlU3Ec__Iterator0_t3423783099, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
