﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen425619164.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String
struct String_t;
// SceneFadeManager/OnComplete
struct OnComplete_t2805533292;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneFadeManager
struct  SceneFadeManager_t1728942829  : public SingletonMonoBehaviour_1_t425619164
{
public:
	// System.Boolean SceneFadeManager::Active
	bool ___Active_3;
	// System.Boolean SceneFadeManager::IsFading
	bool ___IsFading_4;
	// UnityEngine.Texture2D SceneFadeManager::m_Texture
	Texture2D_t3542995729 * ___m_Texture_5;
	// System.String SceneFadeManager::m_Sequence
	String_t* ___m_Sequence_6;
	// UnityEngine.Color SceneFadeManager::m_FromColor
	Color_t2020392075  ___m_FromColor_7;
	// UnityEngine.Color SceneFadeManager::m_ToColor
	Color_t2020392075  ___m_ToColor_8;
	// UnityEngine.Color SceneFadeManager::m_NowColor
	Color_t2020392075  ___m_NowColor_9;
	// System.Single SceneFadeManager::m_fTime
	float ___m_fTime_10;
	// SceneFadeManager/OnComplete SceneFadeManager::m_CompleteAction
	OnComplete_t2805533292 * ___m_CompleteAction_11;

public:
	inline static int32_t get_offset_of_Active_3() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___Active_3)); }
	inline bool get_Active_3() const { return ___Active_3; }
	inline bool* get_address_of_Active_3() { return &___Active_3; }
	inline void set_Active_3(bool value)
	{
		___Active_3 = value;
	}

	inline static int32_t get_offset_of_IsFading_4() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___IsFading_4)); }
	inline bool get_IsFading_4() const { return ___IsFading_4; }
	inline bool* get_address_of_IsFading_4() { return &___IsFading_4; }
	inline void set_IsFading_4(bool value)
	{
		___IsFading_4 = value;
	}

	inline static int32_t get_offset_of_m_Texture_5() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___m_Texture_5)); }
	inline Texture2D_t3542995729 * get_m_Texture_5() const { return ___m_Texture_5; }
	inline Texture2D_t3542995729 ** get_address_of_m_Texture_5() { return &___m_Texture_5; }
	inline void set_m_Texture_5(Texture2D_t3542995729 * value)
	{
		___m_Texture_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_Texture_5, value);
	}

	inline static int32_t get_offset_of_m_Sequence_6() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___m_Sequence_6)); }
	inline String_t* get_m_Sequence_6() const { return ___m_Sequence_6; }
	inline String_t** get_address_of_m_Sequence_6() { return &___m_Sequence_6; }
	inline void set_m_Sequence_6(String_t* value)
	{
		___m_Sequence_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_Sequence_6, value);
	}

	inline static int32_t get_offset_of_m_FromColor_7() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___m_FromColor_7)); }
	inline Color_t2020392075  get_m_FromColor_7() const { return ___m_FromColor_7; }
	inline Color_t2020392075 * get_address_of_m_FromColor_7() { return &___m_FromColor_7; }
	inline void set_m_FromColor_7(Color_t2020392075  value)
	{
		___m_FromColor_7 = value;
	}

	inline static int32_t get_offset_of_m_ToColor_8() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___m_ToColor_8)); }
	inline Color_t2020392075  get_m_ToColor_8() const { return ___m_ToColor_8; }
	inline Color_t2020392075 * get_address_of_m_ToColor_8() { return &___m_ToColor_8; }
	inline void set_m_ToColor_8(Color_t2020392075  value)
	{
		___m_ToColor_8 = value;
	}

	inline static int32_t get_offset_of_m_NowColor_9() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___m_NowColor_9)); }
	inline Color_t2020392075  get_m_NowColor_9() const { return ___m_NowColor_9; }
	inline Color_t2020392075 * get_address_of_m_NowColor_9() { return &___m_NowColor_9; }
	inline void set_m_NowColor_9(Color_t2020392075  value)
	{
		___m_NowColor_9 = value;
	}

	inline static int32_t get_offset_of_m_fTime_10() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___m_fTime_10)); }
	inline float get_m_fTime_10() const { return ___m_fTime_10; }
	inline float* get_address_of_m_fTime_10() { return &___m_fTime_10; }
	inline void set_m_fTime_10(float value)
	{
		___m_fTime_10 = value;
	}

	inline static int32_t get_offset_of_m_CompleteAction_11() { return static_cast<int32_t>(offsetof(SceneFadeManager_t1728942829, ___m_CompleteAction_11)); }
	inline OnComplete_t2805533292 * get_m_CompleteAction_11() const { return ___m_CompleteAction_11; }
	inline OnComplete_t2805533292 ** get_address_of_m_CompleteAction_11() { return &___m_CompleteAction_11; }
	inline void set_m_CompleteAction_11(OnComplete_t2805533292 * value)
	{
		___m_CompleteAction_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_CompleteAction_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
