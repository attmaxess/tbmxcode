﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen521648034.h"
#include "AssemblyU2DCSharp_eSCENETYPE4122992453.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneManager
struct  SceneManager_t1824971699  : public SingletonMonoBehaviour_1_t521648034
{
public:
	// eSCENETYPE SceneManager::m_SceneType
	int32_t ___m_SceneType_3;
	// UnityEngine.UI.Text SceneManager::fpsText
	Text_t356221433 * ___fpsText_4;
	// System.Single SceneManager::fps
	float ___fps_5;
	// eSCENETYPE SceneManager::previousSceneType
	int32_t ___previousSceneType_6;

public:
	inline static int32_t get_offset_of_m_SceneType_3() { return static_cast<int32_t>(offsetof(SceneManager_t1824971699, ___m_SceneType_3)); }
	inline int32_t get_m_SceneType_3() const { return ___m_SceneType_3; }
	inline int32_t* get_address_of_m_SceneType_3() { return &___m_SceneType_3; }
	inline void set_m_SceneType_3(int32_t value)
	{
		___m_SceneType_3 = value;
	}

	inline static int32_t get_offset_of_fpsText_4() { return static_cast<int32_t>(offsetof(SceneManager_t1824971699, ___fpsText_4)); }
	inline Text_t356221433 * get_fpsText_4() const { return ___fpsText_4; }
	inline Text_t356221433 ** get_address_of_fpsText_4() { return &___fpsText_4; }
	inline void set_fpsText_4(Text_t356221433 * value)
	{
		___fpsText_4 = value;
		Il2CppCodeGenWriteBarrier(&___fpsText_4, value);
	}

	inline static int32_t get_offset_of_fps_5() { return static_cast<int32_t>(offsetof(SceneManager_t1824971699, ___fps_5)); }
	inline float get_fps_5() const { return ___fps_5; }
	inline float* get_address_of_fps_5() { return &___fps_5; }
	inline void set_fps_5(float value)
	{
		___fps_5 = value;
	}

	inline static int32_t get_offset_of_previousSceneType_6() { return static_cast<int32_t>(offsetof(SceneManager_t1824971699, ___previousSceneType_6)); }
	inline int32_t get_previousSceneType_6() const { return ___previousSceneType_6; }
	inline int32_t* get_address_of_previousSceneType_6() { return &___previousSceneType_6; }
	inline void set_previousSceneType_6(int32_t value)
	{
		___previousSceneType_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
