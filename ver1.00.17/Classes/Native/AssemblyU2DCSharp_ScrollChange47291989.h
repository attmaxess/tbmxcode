﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollChange
struct  ScrollChange_t47291989  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform ScrollChange::rectran
	RectTransform_t3349966182 * ___rectran_2;
	// UnityEngine.RectTransform ScrollChange::recver
	RectTransform_t3349966182 * ___recver_3;
	// UnityEngine.Vector3 ScrollChange::mousePos
	Vector3_t2243707580  ___mousePos_4;
	// System.Boolean ScrollChange::scrollON
	bool ___scrollON_5;
	// System.Single ScrollChange::m_fClickStartPos
	float ___m_fClickStartPos_6;

public:
	inline static int32_t get_offset_of_rectran_2() { return static_cast<int32_t>(offsetof(ScrollChange_t47291989, ___rectran_2)); }
	inline RectTransform_t3349966182 * get_rectran_2() const { return ___rectran_2; }
	inline RectTransform_t3349966182 ** get_address_of_rectran_2() { return &___rectran_2; }
	inline void set_rectran_2(RectTransform_t3349966182 * value)
	{
		___rectran_2 = value;
		Il2CppCodeGenWriteBarrier(&___rectran_2, value);
	}

	inline static int32_t get_offset_of_recver_3() { return static_cast<int32_t>(offsetof(ScrollChange_t47291989, ___recver_3)); }
	inline RectTransform_t3349966182 * get_recver_3() const { return ___recver_3; }
	inline RectTransform_t3349966182 ** get_address_of_recver_3() { return &___recver_3; }
	inline void set_recver_3(RectTransform_t3349966182 * value)
	{
		___recver_3 = value;
		Il2CppCodeGenWriteBarrier(&___recver_3, value);
	}

	inline static int32_t get_offset_of_mousePos_4() { return static_cast<int32_t>(offsetof(ScrollChange_t47291989, ___mousePos_4)); }
	inline Vector3_t2243707580  get_mousePos_4() const { return ___mousePos_4; }
	inline Vector3_t2243707580 * get_address_of_mousePos_4() { return &___mousePos_4; }
	inline void set_mousePos_4(Vector3_t2243707580  value)
	{
		___mousePos_4 = value;
	}

	inline static int32_t get_offset_of_scrollON_5() { return static_cast<int32_t>(offsetof(ScrollChange_t47291989, ___scrollON_5)); }
	inline bool get_scrollON_5() const { return ___scrollON_5; }
	inline bool* get_address_of_scrollON_5() { return &___scrollON_5; }
	inline void set_scrollON_5(bool value)
	{
		___scrollON_5 = value;
	}

	inline static int32_t get_offset_of_m_fClickStartPos_6() { return static_cast<int32_t>(offsetof(ScrollChange_t47291989, ___m_fClickStartPos_6)); }
	inline float get_m_fClickStartPos_6() const { return ___m_fClickStartPos_6; }
	inline float* get_address_of_m_fClickStartPos_6() { return &___m_fClickStartPos_6; }
	inline void set_m_fClickStartPos_6(float value)
	{
		___m_fClickStartPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
