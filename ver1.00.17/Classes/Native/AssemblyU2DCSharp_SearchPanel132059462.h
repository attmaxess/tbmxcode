﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchPanel
struct  SearchPanel_t132059462  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField SearchPanel::inputFieldName
	InputField_t1631627530 * ___inputFieldName_2;
	// UnityEngine.UI.Button SearchPanel::searchButton
	Button_t2872111280 * ___searchButton_3;
	// UnityEngine.GameObject SearchPanel::contentHolder
	GameObject_t1756533147 * ___contentHolder_4;
	// UnityEngine.GameObject SearchPanel::itemPlayerPrefab
	GameObject_t1756533147 * ___itemPlayerPrefab_5;
	// UnityEngine.GameObject SearchPanel::popUpSearchWarning
	GameObject_t1756533147 * ___popUpSearchWarning_6;
	// UnityEngine.GameObject SearchPanel::historyText
	GameObject_t1756533147 * ___historyText_7;
	// UnityEngine.Sprite[] SearchPanel::rankSprites
	SpriteU5BU5D_t3359083662* ___rankSprites_8;
	// System.Single SearchPanel::timeStart
	float ___timeStart_9;
	// System.Single SearchPanel::timeSinceStart
	float ___timeSinceStart_10;

public:
	inline static int32_t get_offset_of_inputFieldName_2() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___inputFieldName_2)); }
	inline InputField_t1631627530 * get_inputFieldName_2() const { return ___inputFieldName_2; }
	inline InputField_t1631627530 ** get_address_of_inputFieldName_2() { return &___inputFieldName_2; }
	inline void set_inputFieldName_2(InputField_t1631627530 * value)
	{
		___inputFieldName_2 = value;
		Il2CppCodeGenWriteBarrier(&___inputFieldName_2, value);
	}

	inline static int32_t get_offset_of_searchButton_3() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___searchButton_3)); }
	inline Button_t2872111280 * get_searchButton_3() const { return ___searchButton_3; }
	inline Button_t2872111280 ** get_address_of_searchButton_3() { return &___searchButton_3; }
	inline void set_searchButton_3(Button_t2872111280 * value)
	{
		___searchButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___searchButton_3, value);
	}

	inline static int32_t get_offset_of_contentHolder_4() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___contentHolder_4)); }
	inline GameObject_t1756533147 * get_contentHolder_4() const { return ___contentHolder_4; }
	inline GameObject_t1756533147 ** get_address_of_contentHolder_4() { return &___contentHolder_4; }
	inline void set_contentHolder_4(GameObject_t1756533147 * value)
	{
		___contentHolder_4 = value;
		Il2CppCodeGenWriteBarrier(&___contentHolder_4, value);
	}

	inline static int32_t get_offset_of_itemPlayerPrefab_5() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___itemPlayerPrefab_5)); }
	inline GameObject_t1756533147 * get_itemPlayerPrefab_5() const { return ___itemPlayerPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_itemPlayerPrefab_5() { return &___itemPlayerPrefab_5; }
	inline void set_itemPlayerPrefab_5(GameObject_t1756533147 * value)
	{
		___itemPlayerPrefab_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemPlayerPrefab_5, value);
	}

	inline static int32_t get_offset_of_popUpSearchWarning_6() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___popUpSearchWarning_6)); }
	inline GameObject_t1756533147 * get_popUpSearchWarning_6() const { return ___popUpSearchWarning_6; }
	inline GameObject_t1756533147 ** get_address_of_popUpSearchWarning_6() { return &___popUpSearchWarning_6; }
	inline void set_popUpSearchWarning_6(GameObject_t1756533147 * value)
	{
		___popUpSearchWarning_6 = value;
		Il2CppCodeGenWriteBarrier(&___popUpSearchWarning_6, value);
	}

	inline static int32_t get_offset_of_historyText_7() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___historyText_7)); }
	inline GameObject_t1756533147 * get_historyText_7() const { return ___historyText_7; }
	inline GameObject_t1756533147 ** get_address_of_historyText_7() { return &___historyText_7; }
	inline void set_historyText_7(GameObject_t1756533147 * value)
	{
		___historyText_7 = value;
		Il2CppCodeGenWriteBarrier(&___historyText_7, value);
	}

	inline static int32_t get_offset_of_rankSprites_8() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___rankSprites_8)); }
	inline SpriteU5BU5D_t3359083662* get_rankSprites_8() const { return ___rankSprites_8; }
	inline SpriteU5BU5D_t3359083662** get_address_of_rankSprites_8() { return &___rankSprites_8; }
	inline void set_rankSprites_8(SpriteU5BU5D_t3359083662* value)
	{
		___rankSprites_8 = value;
		Il2CppCodeGenWriteBarrier(&___rankSprites_8, value);
	}

	inline static int32_t get_offset_of_timeStart_9() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___timeStart_9)); }
	inline float get_timeStart_9() const { return ___timeStart_9; }
	inline float* get_address_of_timeStart_9() { return &___timeStart_9; }
	inline void set_timeStart_9(float value)
	{
		___timeStart_9 = value;
	}

	inline static int32_t get_offset_of_timeSinceStart_10() { return static_cast<int32_t>(offsetof(SearchPanel_t132059462, ___timeSinceStart_10)); }
	inline float get_timeSinceStart_10() const { return ___timeSinceStart_10; }
	inline float* get_address_of_timeSinceStart_10() { return &___timeSinceStart_10; }
	inline void set_timeSinceStart_10(float value)
	{
		___timeSinceStart_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
