﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Section
struct  Section_t3430619939  : public Il2CppObject
{
public:
	// System.Single Section::rot_x
	float ___rot_x_0;
	// System.Single Section::rot_z
	float ___rot_z_1;
	// System.Single Section::offset
	float ___offset_2;
	// System.Single Section::offsetRange
	float ___offsetRange_3;

public:
	inline static int32_t get_offset_of_rot_x_0() { return static_cast<int32_t>(offsetof(Section_t3430619939, ___rot_x_0)); }
	inline float get_rot_x_0() const { return ___rot_x_0; }
	inline float* get_address_of_rot_x_0() { return &___rot_x_0; }
	inline void set_rot_x_0(float value)
	{
		___rot_x_0 = value;
	}

	inline static int32_t get_offset_of_rot_z_1() { return static_cast<int32_t>(offsetof(Section_t3430619939, ___rot_z_1)); }
	inline float get_rot_z_1() const { return ___rot_z_1; }
	inline float* get_address_of_rot_z_1() { return &___rot_z_1; }
	inline void set_rot_z_1(float value)
	{
		___rot_z_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(Section_t3430619939, ___offset_2)); }
	inline float get_offset_2() const { return ___offset_2; }
	inline float* get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(float value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_offsetRange_3() { return static_cast<int32_t>(offsetof(Section_t3430619939, ___offsetRange_3)); }
	inline float get_offsetRange_3() const { return ___offsetRange_3; }
	inline float* get_address_of_offsetRange_3() { return &___offsetRange_3; }
	inline void set_offsetRange_3(float value)
	{
		___offsetRange_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
