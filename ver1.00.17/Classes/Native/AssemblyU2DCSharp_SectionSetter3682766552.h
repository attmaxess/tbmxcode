﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Shader[]
struct ShaderU5BU5D_t1916779366;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// Section
struct Section_t3430619939;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SectionSetter
struct  SectionSetter_t3682766552  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SectionSetter::toBeSectioned
	GameObject_t1756533147 * ___toBeSectioned_2;
	// UnityEngine.Shader[] SectionSetter::crossSectionShaders
	ShaderU5BU5D_t1916779366* ___crossSectionShaders_3;
	// UnityEngine.GUISkin SectionSetter::skin1
	GUISkin_t1436893342 * ___skin1_4;
	// UnityEngine.MonoBehaviour SectionSetter::orbitScript
	MonoBehaviour_t1158329972 * ___orbitScript_5;
	// Section SectionSetter::section
	Section_t3430619939 * ___section_6;

public:
	inline static int32_t get_offset_of_toBeSectioned_2() { return static_cast<int32_t>(offsetof(SectionSetter_t3682766552, ___toBeSectioned_2)); }
	inline GameObject_t1756533147 * get_toBeSectioned_2() const { return ___toBeSectioned_2; }
	inline GameObject_t1756533147 ** get_address_of_toBeSectioned_2() { return &___toBeSectioned_2; }
	inline void set_toBeSectioned_2(GameObject_t1756533147 * value)
	{
		___toBeSectioned_2 = value;
		Il2CppCodeGenWriteBarrier(&___toBeSectioned_2, value);
	}

	inline static int32_t get_offset_of_crossSectionShaders_3() { return static_cast<int32_t>(offsetof(SectionSetter_t3682766552, ___crossSectionShaders_3)); }
	inline ShaderU5BU5D_t1916779366* get_crossSectionShaders_3() const { return ___crossSectionShaders_3; }
	inline ShaderU5BU5D_t1916779366** get_address_of_crossSectionShaders_3() { return &___crossSectionShaders_3; }
	inline void set_crossSectionShaders_3(ShaderU5BU5D_t1916779366* value)
	{
		___crossSectionShaders_3 = value;
		Il2CppCodeGenWriteBarrier(&___crossSectionShaders_3, value);
	}

	inline static int32_t get_offset_of_skin1_4() { return static_cast<int32_t>(offsetof(SectionSetter_t3682766552, ___skin1_4)); }
	inline GUISkin_t1436893342 * get_skin1_4() const { return ___skin1_4; }
	inline GUISkin_t1436893342 ** get_address_of_skin1_4() { return &___skin1_4; }
	inline void set_skin1_4(GUISkin_t1436893342 * value)
	{
		___skin1_4 = value;
		Il2CppCodeGenWriteBarrier(&___skin1_4, value);
	}

	inline static int32_t get_offset_of_orbitScript_5() { return static_cast<int32_t>(offsetof(SectionSetter_t3682766552, ___orbitScript_5)); }
	inline MonoBehaviour_t1158329972 * get_orbitScript_5() const { return ___orbitScript_5; }
	inline MonoBehaviour_t1158329972 ** get_address_of_orbitScript_5() { return &___orbitScript_5; }
	inline void set_orbitScript_5(MonoBehaviour_t1158329972 * value)
	{
		___orbitScript_5 = value;
		Il2CppCodeGenWriteBarrier(&___orbitScript_5, value);
	}

	inline static int32_t get_offset_of_section_6() { return static_cast<int32_t>(offsetof(SectionSetter_t3682766552, ___section_6)); }
	inline Section_t3430619939 * get_section_6() const { return ___section_6; }
	inline Section_t3430619939 ** get_address_of_section_6() { return &___section_6; }
	inline void set_section_6(Section_t3430619939 * value)
	{
		___section_6 = value;
		Il2CppCodeGenWriteBarrier(&___section_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
