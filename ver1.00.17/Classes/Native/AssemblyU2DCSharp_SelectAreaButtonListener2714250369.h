﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1967201810;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.EventSystems.EventTrigger[]
struct EventTriggerU5BU5D_t3645906279;
// WorldMapPanel
struct WorldMapPanel_t4256308156;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectAreaButtonListener
struct  SelectAreaButtonListener_t2714250369  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button SelectAreaButtonListener::GoButton
	Button_t2872111280 * ___GoButton_2;
	// UnityEngine.EventSystems.EventTrigger SelectAreaButtonListener::GoButtonTriggers
	EventTrigger_t1967201810 * ___GoButtonTriggers_3;
	// UnityEngine.UI.Button[] SelectAreaButtonListener::AreaButtons
	ButtonU5BU5D_t3071100561* ___AreaButtons_4;
	// UnityEngine.EventSystems.EventTrigger[] SelectAreaButtonListener::AreaButtonTriggers
	EventTriggerU5BU5D_t3645906279* ___AreaButtonTriggers_5;
	// WorldMapPanel SelectAreaButtonListener::Panel
	WorldMapPanel_t4256308156 * ___Panel_6;

public:
	inline static int32_t get_offset_of_GoButton_2() { return static_cast<int32_t>(offsetof(SelectAreaButtonListener_t2714250369, ___GoButton_2)); }
	inline Button_t2872111280 * get_GoButton_2() const { return ___GoButton_2; }
	inline Button_t2872111280 ** get_address_of_GoButton_2() { return &___GoButton_2; }
	inline void set_GoButton_2(Button_t2872111280 * value)
	{
		___GoButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___GoButton_2, value);
	}

	inline static int32_t get_offset_of_GoButtonTriggers_3() { return static_cast<int32_t>(offsetof(SelectAreaButtonListener_t2714250369, ___GoButtonTriggers_3)); }
	inline EventTrigger_t1967201810 * get_GoButtonTriggers_3() const { return ___GoButtonTriggers_3; }
	inline EventTrigger_t1967201810 ** get_address_of_GoButtonTriggers_3() { return &___GoButtonTriggers_3; }
	inline void set_GoButtonTriggers_3(EventTrigger_t1967201810 * value)
	{
		___GoButtonTriggers_3 = value;
		Il2CppCodeGenWriteBarrier(&___GoButtonTriggers_3, value);
	}

	inline static int32_t get_offset_of_AreaButtons_4() { return static_cast<int32_t>(offsetof(SelectAreaButtonListener_t2714250369, ___AreaButtons_4)); }
	inline ButtonU5BU5D_t3071100561* get_AreaButtons_4() const { return ___AreaButtons_4; }
	inline ButtonU5BU5D_t3071100561** get_address_of_AreaButtons_4() { return &___AreaButtons_4; }
	inline void set_AreaButtons_4(ButtonU5BU5D_t3071100561* value)
	{
		___AreaButtons_4 = value;
		Il2CppCodeGenWriteBarrier(&___AreaButtons_4, value);
	}

	inline static int32_t get_offset_of_AreaButtonTriggers_5() { return static_cast<int32_t>(offsetof(SelectAreaButtonListener_t2714250369, ___AreaButtonTriggers_5)); }
	inline EventTriggerU5BU5D_t3645906279* get_AreaButtonTriggers_5() const { return ___AreaButtonTriggers_5; }
	inline EventTriggerU5BU5D_t3645906279** get_address_of_AreaButtonTriggers_5() { return &___AreaButtonTriggers_5; }
	inline void set_AreaButtonTriggers_5(EventTriggerU5BU5D_t3645906279* value)
	{
		___AreaButtonTriggers_5 = value;
		Il2CppCodeGenWriteBarrier(&___AreaButtonTriggers_5, value);
	}

	inline static int32_t get_offset_of_Panel_6() { return static_cast<int32_t>(offsetof(SelectAreaButtonListener_t2714250369, ___Panel_6)); }
	inline WorldMapPanel_t4256308156 * get_Panel_6() const { return ___Panel_6; }
	inline WorldMapPanel_t4256308156 ** get_address_of_Panel_6() { return &___Panel_6; }
	inline void set_Panel_6(WorldMapPanel_t4256308156 * value)
	{
		___Panel_6 = value;
		Il2CppCodeGenWriteBarrier(&___Panel_6, value);
	}
};

struct SelectAreaButtonListener_t2714250369_StaticFields
{
public:
	// System.Action SelectAreaButtonListener::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(SelectAreaButtonListener_t2714250369_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
