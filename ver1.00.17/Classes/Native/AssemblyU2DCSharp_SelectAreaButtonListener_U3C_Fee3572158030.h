﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SelectAreaButtonListener
struct SelectAreaButtonListener_t2714250369;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectAreaButtonListener/<_FeedbackEffect>c__AnonStorey4
struct  U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030  : public Il2CppObject
{
public:
	// System.Single SelectAreaButtonListener/<_FeedbackEffect>c__AnonStorey4::scale
	float ___scale_0;
	// System.Int32 SelectAreaButtonListener/<_FeedbackEffect>c__AnonStorey4::index
	int32_t ___index_1;
	// SelectAreaButtonListener SelectAreaButtonListener/<_FeedbackEffect>c__AnonStorey4::$this
	SelectAreaButtonListener_t2714250369 * ___U24this_2;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030, ___scale_0)); }
	inline float get_scale_0() const { return ___scale_0; }
	inline float* get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(float value)
	{
		___scale_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030, ___U24this_2)); }
	inline SelectAreaButtonListener_t2714250369 * get_U24this_2() const { return ___U24this_2; }
	inline SelectAreaButtonListener_t2714250369 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SelectAreaButtonListener_t2714250369 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
