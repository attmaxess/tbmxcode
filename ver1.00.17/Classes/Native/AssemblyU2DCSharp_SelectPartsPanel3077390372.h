﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PartsScroll
struct PartsScroll_t2740354319;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectPartsPanel
struct  SelectPartsPanel_t3077390372  : public MonoBehaviour_t1158329972
{
public:
	// PartsScroll SelectPartsPanel::PartsBarSwitch
	PartsScroll_t2740354319 * ___PartsBarSwitch_2;
	// UnityEngine.GameObject[] SelectPartsPanel::Scrolls
	GameObjectU5BU5D_t3057952154* ___Scrolls_3;
	// UnityEngine.GameObject[] SelectPartsPanel::Buttons
	GameObjectU5BU5D_t3057952154* ___Buttons_4;
	// UnityEngine.UI.Button[] SelectPartsPanel::SelectButons
	ButtonU5BU5D_t3071100561* ___SelectButons_5;

public:
	inline static int32_t get_offset_of_PartsBarSwitch_2() { return static_cast<int32_t>(offsetof(SelectPartsPanel_t3077390372, ___PartsBarSwitch_2)); }
	inline PartsScroll_t2740354319 * get_PartsBarSwitch_2() const { return ___PartsBarSwitch_2; }
	inline PartsScroll_t2740354319 ** get_address_of_PartsBarSwitch_2() { return &___PartsBarSwitch_2; }
	inline void set_PartsBarSwitch_2(PartsScroll_t2740354319 * value)
	{
		___PartsBarSwitch_2 = value;
		Il2CppCodeGenWriteBarrier(&___PartsBarSwitch_2, value);
	}

	inline static int32_t get_offset_of_Scrolls_3() { return static_cast<int32_t>(offsetof(SelectPartsPanel_t3077390372, ___Scrolls_3)); }
	inline GameObjectU5BU5D_t3057952154* get_Scrolls_3() const { return ___Scrolls_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Scrolls_3() { return &___Scrolls_3; }
	inline void set_Scrolls_3(GameObjectU5BU5D_t3057952154* value)
	{
		___Scrolls_3 = value;
		Il2CppCodeGenWriteBarrier(&___Scrolls_3, value);
	}

	inline static int32_t get_offset_of_Buttons_4() { return static_cast<int32_t>(offsetof(SelectPartsPanel_t3077390372, ___Buttons_4)); }
	inline GameObjectU5BU5D_t3057952154* get_Buttons_4() const { return ___Buttons_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Buttons_4() { return &___Buttons_4; }
	inline void set_Buttons_4(GameObjectU5BU5D_t3057952154* value)
	{
		___Buttons_4 = value;
		Il2CppCodeGenWriteBarrier(&___Buttons_4, value);
	}

	inline static int32_t get_offset_of_SelectButons_5() { return static_cast<int32_t>(offsetof(SelectPartsPanel_t3077390372, ___SelectButons_5)); }
	inline ButtonU5BU5D_t3071100561* get_SelectButons_5() const { return ___SelectButons_5; }
	inline ButtonU5BU5D_t3071100561** get_address_of_SelectButons_5() { return &___SelectButons_5; }
	inline void set_SelectButons_5(ButtonU5BU5D_t3071100561* value)
	{
		___SelectButons_5 = value;
		Il2CppCodeGenWriteBarrier(&___SelectButons_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
