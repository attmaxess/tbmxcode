﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Light
struct Light_t494725636;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Terrain
struct Terrain_t59182933;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Terrain[]
struct TerrainU5BU5D_t3242190776;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1658499504;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetupAdvancedFoliageShader
struct  SetupAdvancedFoliageShader_t182079117  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SetupAdvancedFoliageShader::isLinear
	bool ___isLinear_2;
	// System.Boolean SetupAdvancedFoliageShader::AmbientLightingSH
	bool ___AmbientLightingSH_3;
	// UnityEngine.GameObject SetupAdvancedFoliageShader::TerrianLight0
	GameObject_t1756533147 * ___TerrianLight0_4;
	// UnityEngine.GameObject SetupAdvancedFoliageShader::TerrianLight1
	GameObject_t1756533147 * ___TerrianLight1_5;
	// UnityEngine.GameObject SetupAdvancedFoliageShader::DirectionalLightReference
	GameObject_t1756533147 * ___DirectionalLightReference_6;
	// UnityEngine.Vector3 SetupAdvancedFoliageShader::DirectlightDir
	Vector3_t2243707580  ___DirectlightDir_7;
	// UnityEngine.Light SetupAdvancedFoliageShader::Sunlight
	Light_t494725636 * ___Sunlight_8;
	// UnityEngine.Vector3 SetupAdvancedFoliageShader::SunLightCol
	Vector3_t2243707580  ___SunLightCol_9;
	// System.Single SetupAdvancedFoliageShader::SunLuminance
	float ___SunLuminance_10;
	// System.Boolean SetupAdvancedFoliageShader::GrassApproxTrans
	bool ___GrassApproxTrans_11;
	// System.Int32 SetupAdvancedFoliageShader::AFSFog_Mode
	int32_t ___AFSFog_Mode_12;
	// System.String SetupAdvancedFoliageShader::AFSShader_Folder
	String_t* ___AFSShader_Folder_13;
	// System.Boolean SetupAdvancedFoliageShader::disableFoginShader
	bool ___disableFoginShader_14;
	// UnityEngine.Vector4 SetupAdvancedFoliageShader::Wind
	Vector4_t2243707581  ___Wind_15;
	// System.Single SetupAdvancedFoliageShader::WindFrequency
	float ___WindFrequency_16;
	// System.Single SetupAdvancedFoliageShader::WaveSizeFoliageShader
	float ___WaveSizeFoliageShader_17;
	// System.Single SetupAdvancedFoliageShader::LeafTurbulenceFoliageShader
	float ___LeafTurbulenceFoliageShader_18;
	// System.Single SetupAdvancedFoliageShader::WindMultiplierForGrassshader
	float ___WindMultiplierForGrassshader_19;
	// System.Single SetupAdvancedFoliageShader::WaveSizeForGrassshader
	float ___WaveSizeForGrassshader_20;
	// System.Single SetupAdvancedFoliageShader::WindJitterFrequencyForGrassshader
	float ___WindJitterFrequencyForGrassshader_21;
	// System.Single SetupAdvancedFoliageShader::WindJitterScaleForGrassshader
	float ___WindJitterScaleForGrassshader_22;
	// System.Boolean SetupAdvancedFoliageShader::SyncWindDir
	bool ___SyncWindDir_23;
	// System.Single SetupAdvancedFoliageShader::WindMuliplierForTreeShaderPrimary
	float ___WindMuliplierForTreeShaderPrimary_24;
	// System.Single SetupAdvancedFoliageShader::WindMuliplierForTreeShaderSecondary
	float ___WindMuliplierForTreeShaderSecondary_25;
	// UnityEngine.Vector4 SetupAdvancedFoliageShader::WindMuliplierForTreeShader
	Vector4_t2243707581  ___WindMuliplierForTreeShader_26;
	// System.Single SetupAdvancedFoliageShader::temp_WindFrequency
	float ___temp_WindFrequency_27;
	// System.Single SetupAdvancedFoliageShader::temp_WindJitterFrequency
	float ___temp_WindJitterFrequency_28;
	// System.Single SetupAdvancedFoliageShader::freqSpeed
	float ___freqSpeed_30;
	// System.Single SetupAdvancedFoliageShader::domainTime_Wind
	float ___domainTime_Wind_31;
	// System.Single SetupAdvancedFoliageShader::domainTime_2ndBending
	float ___domainTime_2ndBending_32;
	// System.Single SetupAdvancedFoliageShader::domainTime_Grass
	float ___domainTime_Grass_33;
	// System.Single SetupAdvancedFoliageShader::RainAmount
	float ___RainAmount_34;
	// System.Single SetupAdvancedFoliageShader::VertexLitAlphaCutOff
	float ___VertexLitAlphaCutOff_35;
	// UnityEngine.Color SetupAdvancedFoliageShader::VertexLitTranslucencyColor
	Color_t2020392075  ___VertexLitTranslucencyColor_36;
	// System.Single SetupAdvancedFoliageShader::VertexLitTranslucencyViewDependency
	float ___VertexLitTranslucencyViewDependency_37;
	// System.Single SetupAdvancedFoliageShader::VertexLitShadowStrength
	float ___VertexLitShadowStrength_38;
	// UnityEngine.Color SetupAdvancedFoliageShader::VertexLitSpecularReflectivity
	Color_t2020392075  ___VertexLitSpecularReflectivity_39;
	// UnityEngine.Vector2 SetupAdvancedFoliageShader::AfsSpecFade
	Vector2_t2243707579  ___AfsSpecFade_40;
	// UnityEngine.Texture SetupAdvancedFoliageShader::TerrainFoliageNrmSpecMap
	Texture_t2243626319 * ___TerrainFoliageNrmSpecMap_41;
	// System.Boolean SetupAdvancedFoliageShader::AutoSyncToTerrain
	bool ___AutoSyncToTerrain_42;
	// UnityEngine.Terrain SetupAdvancedFoliageShader::SyncedTerrain
	Terrain_t59182933 * ___SyncedTerrain_43;
	// System.Boolean SetupAdvancedFoliageShader::AutoSyncInPlaymode
	bool ___AutoSyncInPlaymode_44;
	// System.Single SetupAdvancedFoliageShader::DetailDistanceForGrassShader
	float ___DetailDistanceForGrassShader_45;
	// System.Single SetupAdvancedFoliageShader::BillboardStart
	float ___BillboardStart_46;
	// System.Single SetupAdvancedFoliageShader::BillboardFadeLenght
	float ___BillboardFadeLenght_47;
	// System.Boolean SetupAdvancedFoliageShader::GrassAnimateNormal
	bool ___GrassAnimateNormal_48;
	// UnityEngine.Color SetupAdvancedFoliageShader::GrassWavingTint
	Color_t2020392075  ___GrassWavingTint_49;
	// UnityEngine.Color SetupAdvancedFoliageShader::AFSTreeColor
	Color_t2020392075  ___AFSTreeColor_50;
	// UnityEngine.Camera SetupAdvancedFoliageShader::MainCam
	Camera_t189460977 * ___MainCam_51;
	// System.Boolean SetupAdvancedFoliageShader::TreeBillboardShadows
	bool ___TreeBillboardShadows_52;
	// System.Single SetupAdvancedFoliageShader::BillboardFadeOutLength
	float ___BillboardFadeOutLength_53;
	// System.Boolean SetupAdvancedFoliageShader::BillboardAdjustToCamera
	bool ___BillboardAdjustToCamera_54;
	// System.Single SetupAdvancedFoliageShader::BillboardAngleLimit
	float ___BillboardAngleLimit_55;
	// UnityEngine.Color SetupAdvancedFoliageShader::BillboardShadowColor
	Color_t2020392075  ___BillboardShadowColor_56;
	// System.Single SetupAdvancedFoliageShader::BillboardAmbientLightFactor
	float ___BillboardAmbientLightFactor_57;
	// System.Single SetupAdvancedFoliageShader::BillboardAmbientLightDesaturationFactor
	float ___BillboardAmbientLightDesaturationFactor_58;
	// System.Boolean SetupAdvancedFoliageShader::AutosyncShadowColor
	bool ___AutosyncShadowColor_59;
	// System.Boolean SetupAdvancedFoliageShader::EnableCameraLayerCulling
	bool ___EnableCameraLayerCulling_60;
	// System.Int32 SetupAdvancedFoliageShader::SmallDetailsDistance
	int32_t ___SmallDetailsDistance_61;
	// System.Int32 SetupAdvancedFoliageShader::MediumDetailsDistance
	int32_t ___MediumDetailsDistance_62;
	// System.Boolean SetupAdvancedFoliageShader::AllGrassObjectsCombined
	bool ___AllGrassObjectsCombined_63;
	// UnityEngine.Vector4 SetupAdvancedFoliageShader::TempWind
	Vector4_t2243707581  ___TempWind_64;
	// System.Single SetupAdvancedFoliageShader::GrassWindSpeed
	float ___GrassWindSpeed_65;
	// System.Single SetupAdvancedFoliageShader::SinusWave
	float ___SinusWave_66;
	// UnityEngine.Vector4 SetupAdvancedFoliageShader::TriangleWaves
	Vector4_t2243707581  ___TriangleWaves_67;
	// System.Single SetupAdvancedFoliageShader::Oscillation
	float ___Oscillation_68;
	// UnityEngine.Vector3 SetupAdvancedFoliageShader::CameraForward
	Vector3_t2243707580  ___CameraForward_69;
	// UnityEngine.Vector3 SetupAdvancedFoliageShader::ShadowCameraForward
	Vector3_t2243707580  ___ShadowCameraForward_70;
	// UnityEngine.Vector3 SetupAdvancedFoliageShader::CameraForwardVec
	Vector3_t2243707580  ___CameraForwardVec_71;
	// System.Single SetupAdvancedFoliageShader::rollingX
	float ___rollingX_72;
	// System.Single SetupAdvancedFoliageShader::rollingXShadow
	float ___rollingXShadow_73;
	// UnityEngine.Vector3 SetupAdvancedFoliageShader::lightDir
	Vector3_t2243707580  ___lightDir_74;
	// UnityEngine.Vector3 SetupAdvancedFoliageShader::templightDir
	Vector3_t2243707580  ___templightDir_75;
	// System.Single SetupAdvancedFoliageShader::CameraAngle
	float ___CameraAngle_76;
	// UnityEngine.Terrain[] SetupAdvancedFoliageShader::allTerrains
	TerrainU5BU5D_t3242190776* ___allTerrains_77;
	// UnityEngine.Vector3[] SetupAdvancedFoliageShader::fLight
	Vector3U5BU5D_t1172311765* ___fLight_78;
	// UnityEngine.Vector4[] SetupAdvancedFoliageShader::vCoeff
	Vector4U5BU5D_t1658499504* ___vCoeff_79;

public:
	inline static int32_t get_offset_of_isLinear_2() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___isLinear_2)); }
	inline bool get_isLinear_2() const { return ___isLinear_2; }
	inline bool* get_address_of_isLinear_2() { return &___isLinear_2; }
	inline void set_isLinear_2(bool value)
	{
		___isLinear_2 = value;
	}

	inline static int32_t get_offset_of_AmbientLightingSH_3() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AmbientLightingSH_3)); }
	inline bool get_AmbientLightingSH_3() const { return ___AmbientLightingSH_3; }
	inline bool* get_address_of_AmbientLightingSH_3() { return &___AmbientLightingSH_3; }
	inline void set_AmbientLightingSH_3(bool value)
	{
		___AmbientLightingSH_3 = value;
	}

	inline static int32_t get_offset_of_TerrianLight0_4() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___TerrianLight0_4)); }
	inline GameObject_t1756533147 * get_TerrianLight0_4() const { return ___TerrianLight0_4; }
	inline GameObject_t1756533147 ** get_address_of_TerrianLight0_4() { return &___TerrianLight0_4; }
	inline void set_TerrianLight0_4(GameObject_t1756533147 * value)
	{
		___TerrianLight0_4 = value;
		Il2CppCodeGenWriteBarrier(&___TerrianLight0_4, value);
	}

	inline static int32_t get_offset_of_TerrianLight1_5() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___TerrianLight1_5)); }
	inline GameObject_t1756533147 * get_TerrianLight1_5() const { return ___TerrianLight1_5; }
	inline GameObject_t1756533147 ** get_address_of_TerrianLight1_5() { return &___TerrianLight1_5; }
	inline void set_TerrianLight1_5(GameObject_t1756533147 * value)
	{
		___TerrianLight1_5 = value;
		Il2CppCodeGenWriteBarrier(&___TerrianLight1_5, value);
	}

	inline static int32_t get_offset_of_DirectionalLightReference_6() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___DirectionalLightReference_6)); }
	inline GameObject_t1756533147 * get_DirectionalLightReference_6() const { return ___DirectionalLightReference_6; }
	inline GameObject_t1756533147 ** get_address_of_DirectionalLightReference_6() { return &___DirectionalLightReference_6; }
	inline void set_DirectionalLightReference_6(GameObject_t1756533147 * value)
	{
		___DirectionalLightReference_6 = value;
		Il2CppCodeGenWriteBarrier(&___DirectionalLightReference_6, value);
	}

	inline static int32_t get_offset_of_DirectlightDir_7() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___DirectlightDir_7)); }
	inline Vector3_t2243707580  get_DirectlightDir_7() const { return ___DirectlightDir_7; }
	inline Vector3_t2243707580 * get_address_of_DirectlightDir_7() { return &___DirectlightDir_7; }
	inline void set_DirectlightDir_7(Vector3_t2243707580  value)
	{
		___DirectlightDir_7 = value;
	}

	inline static int32_t get_offset_of_Sunlight_8() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___Sunlight_8)); }
	inline Light_t494725636 * get_Sunlight_8() const { return ___Sunlight_8; }
	inline Light_t494725636 ** get_address_of_Sunlight_8() { return &___Sunlight_8; }
	inline void set_Sunlight_8(Light_t494725636 * value)
	{
		___Sunlight_8 = value;
		Il2CppCodeGenWriteBarrier(&___Sunlight_8, value);
	}

	inline static int32_t get_offset_of_SunLightCol_9() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___SunLightCol_9)); }
	inline Vector3_t2243707580  get_SunLightCol_9() const { return ___SunLightCol_9; }
	inline Vector3_t2243707580 * get_address_of_SunLightCol_9() { return &___SunLightCol_9; }
	inline void set_SunLightCol_9(Vector3_t2243707580  value)
	{
		___SunLightCol_9 = value;
	}

	inline static int32_t get_offset_of_SunLuminance_10() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___SunLuminance_10)); }
	inline float get_SunLuminance_10() const { return ___SunLuminance_10; }
	inline float* get_address_of_SunLuminance_10() { return &___SunLuminance_10; }
	inline void set_SunLuminance_10(float value)
	{
		___SunLuminance_10 = value;
	}

	inline static int32_t get_offset_of_GrassApproxTrans_11() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___GrassApproxTrans_11)); }
	inline bool get_GrassApproxTrans_11() const { return ___GrassApproxTrans_11; }
	inline bool* get_address_of_GrassApproxTrans_11() { return &___GrassApproxTrans_11; }
	inline void set_GrassApproxTrans_11(bool value)
	{
		___GrassApproxTrans_11 = value;
	}

	inline static int32_t get_offset_of_AFSFog_Mode_12() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AFSFog_Mode_12)); }
	inline int32_t get_AFSFog_Mode_12() const { return ___AFSFog_Mode_12; }
	inline int32_t* get_address_of_AFSFog_Mode_12() { return &___AFSFog_Mode_12; }
	inline void set_AFSFog_Mode_12(int32_t value)
	{
		___AFSFog_Mode_12 = value;
	}

	inline static int32_t get_offset_of_AFSShader_Folder_13() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AFSShader_Folder_13)); }
	inline String_t* get_AFSShader_Folder_13() const { return ___AFSShader_Folder_13; }
	inline String_t** get_address_of_AFSShader_Folder_13() { return &___AFSShader_Folder_13; }
	inline void set_AFSShader_Folder_13(String_t* value)
	{
		___AFSShader_Folder_13 = value;
		Il2CppCodeGenWriteBarrier(&___AFSShader_Folder_13, value);
	}

	inline static int32_t get_offset_of_disableFoginShader_14() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___disableFoginShader_14)); }
	inline bool get_disableFoginShader_14() const { return ___disableFoginShader_14; }
	inline bool* get_address_of_disableFoginShader_14() { return &___disableFoginShader_14; }
	inline void set_disableFoginShader_14(bool value)
	{
		___disableFoginShader_14 = value;
	}

	inline static int32_t get_offset_of_Wind_15() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___Wind_15)); }
	inline Vector4_t2243707581  get_Wind_15() const { return ___Wind_15; }
	inline Vector4_t2243707581 * get_address_of_Wind_15() { return &___Wind_15; }
	inline void set_Wind_15(Vector4_t2243707581  value)
	{
		___Wind_15 = value;
	}

	inline static int32_t get_offset_of_WindFrequency_16() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WindFrequency_16)); }
	inline float get_WindFrequency_16() const { return ___WindFrequency_16; }
	inline float* get_address_of_WindFrequency_16() { return &___WindFrequency_16; }
	inline void set_WindFrequency_16(float value)
	{
		___WindFrequency_16 = value;
	}

	inline static int32_t get_offset_of_WaveSizeFoliageShader_17() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WaveSizeFoliageShader_17)); }
	inline float get_WaveSizeFoliageShader_17() const { return ___WaveSizeFoliageShader_17; }
	inline float* get_address_of_WaveSizeFoliageShader_17() { return &___WaveSizeFoliageShader_17; }
	inline void set_WaveSizeFoliageShader_17(float value)
	{
		___WaveSizeFoliageShader_17 = value;
	}

	inline static int32_t get_offset_of_LeafTurbulenceFoliageShader_18() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___LeafTurbulenceFoliageShader_18)); }
	inline float get_LeafTurbulenceFoliageShader_18() const { return ___LeafTurbulenceFoliageShader_18; }
	inline float* get_address_of_LeafTurbulenceFoliageShader_18() { return &___LeafTurbulenceFoliageShader_18; }
	inline void set_LeafTurbulenceFoliageShader_18(float value)
	{
		___LeafTurbulenceFoliageShader_18 = value;
	}

	inline static int32_t get_offset_of_WindMultiplierForGrassshader_19() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WindMultiplierForGrassshader_19)); }
	inline float get_WindMultiplierForGrassshader_19() const { return ___WindMultiplierForGrassshader_19; }
	inline float* get_address_of_WindMultiplierForGrassshader_19() { return &___WindMultiplierForGrassshader_19; }
	inline void set_WindMultiplierForGrassshader_19(float value)
	{
		___WindMultiplierForGrassshader_19 = value;
	}

	inline static int32_t get_offset_of_WaveSizeForGrassshader_20() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WaveSizeForGrassshader_20)); }
	inline float get_WaveSizeForGrassshader_20() const { return ___WaveSizeForGrassshader_20; }
	inline float* get_address_of_WaveSizeForGrassshader_20() { return &___WaveSizeForGrassshader_20; }
	inline void set_WaveSizeForGrassshader_20(float value)
	{
		___WaveSizeForGrassshader_20 = value;
	}

	inline static int32_t get_offset_of_WindJitterFrequencyForGrassshader_21() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WindJitterFrequencyForGrassshader_21)); }
	inline float get_WindJitterFrequencyForGrassshader_21() const { return ___WindJitterFrequencyForGrassshader_21; }
	inline float* get_address_of_WindJitterFrequencyForGrassshader_21() { return &___WindJitterFrequencyForGrassshader_21; }
	inline void set_WindJitterFrequencyForGrassshader_21(float value)
	{
		___WindJitterFrequencyForGrassshader_21 = value;
	}

	inline static int32_t get_offset_of_WindJitterScaleForGrassshader_22() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WindJitterScaleForGrassshader_22)); }
	inline float get_WindJitterScaleForGrassshader_22() const { return ___WindJitterScaleForGrassshader_22; }
	inline float* get_address_of_WindJitterScaleForGrassshader_22() { return &___WindJitterScaleForGrassshader_22; }
	inline void set_WindJitterScaleForGrassshader_22(float value)
	{
		___WindJitterScaleForGrassshader_22 = value;
	}

	inline static int32_t get_offset_of_SyncWindDir_23() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___SyncWindDir_23)); }
	inline bool get_SyncWindDir_23() const { return ___SyncWindDir_23; }
	inline bool* get_address_of_SyncWindDir_23() { return &___SyncWindDir_23; }
	inline void set_SyncWindDir_23(bool value)
	{
		___SyncWindDir_23 = value;
	}

	inline static int32_t get_offset_of_WindMuliplierForTreeShaderPrimary_24() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WindMuliplierForTreeShaderPrimary_24)); }
	inline float get_WindMuliplierForTreeShaderPrimary_24() const { return ___WindMuliplierForTreeShaderPrimary_24; }
	inline float* get_address_of_WindMuliplierForTreeShaderPrimary_24() { return &___WindMuliplierForTreeShaderPrimary_24; }
	inline void set_WindMuliplierForTreeShaderPrimary_24(float value)
	{
		___WindMuliplierForTreeShaderPrimary_24 = value;
	}

	inline static int32_t get_offset_of_WindMuliplierForTreeShaderSecondary_25() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WindMuliplierForTreeShaderSecondary_25)); }
	inline float get_WindMuliplierForTreeShaderSecondary_25() const { return ___WindMuliplierForTreeShaderSecondary_25; }
	inline float* get_address_of_WindMuliplierForTreeShaderSecondary_25() { return &___WindMuliplierForTreeShaderSecondary_25; }
	inline void set_WindMuliplierForTreeShaderSecondary_25(float value)
	{
		___WindMuliplierForTreeShaderSecondary_25 = value;
	}

	inline static int32_t get_offset_of_WindMuliplierForTreeShader_26() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___WindMuliplierForTreeShader_26)); }
	inline Vector4_t2243707581  get_WindMuliplierForTreeShader_26() const { return ___WindMuliplierForTreeShader_26; }
	inline Vector4_t2243707581 * get_address_of_WindMuliplierForTreeShader_26() { return &___WindMuliplierForTreeShader_26; }
	inline void set_WindMuliplierForTreeShader_26(Vector4_t2243707581  value)
	{
		___WindMuliplierForTreeShader_26 = value;
	}

	inline static int32_t get_offset_of_temp_WindFrequency_27() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___temp_WindFrequency_27)); }
	inline float get_temp_WindFrequency_27() const { return ___temp_WindFrequency_27; }
	inline float* get_address_of_temp_WindFrequency_27() { return &___temp_WindFrequency_27; }
	inline void set_temp_WindFrequency_27(float value)
	{
		___temp_WindFrequency_27 = value;
	}

	inline static int32_t get_offset_of_temp_WindJitterFrequency_28() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___temp_WindJitterFrequency_28)); }
	inline float get_temp_WindJitterFrequency_28() const { return ___temp_WindJitterFrequency_28; }
	inline float* get_address_of_temp_WindJitterFrequency_28() { return &___temp_WindJitterFrequency_28; }
	inline void set_temp_WindJitterFrequency_28(float value)
	{
		___temp_WindJitterFrequency_28 = value;
	}

	inline static int32_t get_offset_of_freqSpeed_30() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___freqSpeed_30)); }
	inline float get_freqSpeed_30() const { return ___freqSpeed_30; }
	inline float* get_address_of_freqSpeed_30() { return &___freqSpeed_30; }
	inline void set_freqSpeed_30(float value)
	{
		___freqSpeed_30 = value;
	}

	inline static int32_t get_offset_of_domainTime_Wind_31() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___domainTime_Wind_31)); }
	inline float get_domainTime_Wind_31() const { return ___domainTime_Wind_31; }
	inline float* get_address_of_domainTime_Wind_31() { return &___domainTime_Wind_31; }
	inline void set_domainTime_Wind_31(float value)
	{
		___domainTime_Wind_31 = value;
	}

	inline static int32_t get_offset_of_domainTime_2ndBending_32() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___domainTime_2ndBending_32)); }
	inline float get_domainTime_2ndBending_32() const { return ___domainTime_2ndBending_32; }
	inline float* get_address_of_domainTime_2ndBending_32() { return &___domainTime_2ndBending_32; }
	inline void set_domainTime_2ndBending_32(float value)
	{
		___domainTime_2ndBending_32 = value;
	}

	inline static int32_t get_offset_of_domainTime_Grass_33() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___domainTime_Grass_33)); }
	inline float get_domainTime_Grass_33() const { return ___domainTime_Grass_33; }
	inline float* get_address_of_domainTime_Grass_33() { return &___domainTime_Grass_33; }
	inline void set_domainTime_Grass_33(float value)
	{
		___domainTime_Grass_33 = value;
	}

	inline static int32_t get_offset_of_RainAmount_34() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___RainAmount_34)); }
	inline float get_RainAmount_34() const { return ___RainAmount_34; }
	inline float* get_address_of_RainAmount_34() { return &___RainAmount_34; }
	inline void set_RainAmount_34(float value)
	{
		___RainAmount_34 = value;
	}

	inline static int32_t get_offset_of_VertexLitAlphaCutOff_35() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___VertexLitAlphaCutOff_35)); }
	inline float get_VertexLitAlphaCutOff_35() const { return ___VertexLitAlphaCutOff_35; }
	inline float* get_address_of_VertexLitAlphaCutOff_35() { return &___VertexLitAlphaCutOff_35; }
	inline void set_VertexLitAlphaCutOff_35(float value)
	{
		___VertexLitAlphaCutOff_35 = value;
	}

	inline static int32_t get_offset_of_VertexLitTranslucencyColor_36() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___VertexLitTranslucencyColor_36)); }
	inline Color_t2020392075  get_VertexLitTranslucencyColor_36() const { return ___VertexLitTranslucencyColor_36; }
	inline Color_t2020392075 * get_address_of_VertexLitTranslucencyColor_36() { return &___VertexLitTranslucencyColor_36; }
	inline void set_VertexLitTranslucencyColor_36(Color_t2020392075  value)
	{
		___VertexLitTranslucencyColor_36 = value;
	}

	inline static int32_t get_offset_of_VertexLitTranslucencyViewDependency_37() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___VertexLitTranslucencyViewDependency_37)); }
	inline float get_VertexLitTranslucencyViewDependency_37() const { return ___VertexLitTranslucencyViewDependency_37; }
	inline float* get_address_of_VertexLitTranslucencyViewDependency_37() { return &___VertexLitTranslucencyViewDependency_37; }
	inline void set_VertexLitTranslucencyViewDependency_37(float value)
	{
		___VertexLitTranslucencyViewDependency_37 = value;
	}

	inline static int32_t get_offset_of_VertexLitShadowStrength_38() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___VertexLitShadowStrength_38)); }
	inline float get_VertexLitShadowStrength_38() const { return ___VertexLitShadowStrength_38; }
	inline float* get_address_of_VertexLitShadowStrength_38() { return &___VertexLitShadowStrength_38; }
	inline void set_VertexLitShadowStrength_38(float value)
	{
		___VertexLitShadowStrength_38 = value;
	}

	inline static int32_t get_offset_of_VertexLitSpecularReflectivity_39() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___VertexLitSpecularReflectivity_39)); }
	inline Color_t2020392075  get_VertexLitSpecularReflectivity_39() const { return ___VertexLitSpecularReflectivity_39; }
	inline Color_t2020392075 * get_address_of_VertexLitSpecularReflectivity_39() { return &___VertexLitSpecularReflectivity_39; }
	inline void set_VertexLitSpecularReflectivity_39(Color_t2020392075  value)
	{
		___VertexLitSpecularReflectivity_39 = value;
	}

	inline static int32_t get_offset_of_AfsSpecFade_40() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AfsSpecFade_40)); }
	inline Vector2_t2243707579  get_AfsSpecFade_40() const { return ___AfsSpecFade_40; }
	inline Vector2_t2243707579 * get_address_of_AfsSpecFade_40() { return &___AfsSpecFade_40; }
	inline void set_AfsSpecFade_40(Vector2_t2243707579  value)
	{
		___AfsSpecFade_40 = value;
	}

	inline static int32_t get_offset_of_TerrainFoliageNrmSpecMap_41() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___TerrainFoliageNrmSpecMap_41)); }
	inline Texture_t2243626319 * get_TerrainFoliageNrmSpecMap_41() const { return ___TerrainFoliageNrmSpecMap_41; }
	inline Texture_t2243626319 ** get_address_of_TerrainFoliageNrmSpecMap_41() { return &___TerrainFoliageNrmSpecMap_41; }
	inline void set_TerrainFoliageNrmSpecMap_41(Texture_t2243626319 * value)
	{
		___TerrainFoliageNrmSpecMap_41 = value;
		Il2CppCodeGenWriteBarrier(&___TerrainFoliageNrmSpecMap_41, value);
	}

	inline static int32_t get_offset_of_AutoSyncToTerrain_42() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AutoSyncToTerrain_42)); }
	inline bool get_AutoSyncToTerrain_42() const { return ___AutoSyncToTerrain_42; }
	inline bool* get_address_of_AutoSyncToTerrain_42() { return &___AutoSyncToTerrain_42; }
	inline void set_AutoSyncToTerrain_42(bool value)
	{
		___AutoSyncToTerrain_42 = value;
	}

	inline static int32_t get_offset_of_SyncedTerrain_43() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___SyncedTerrain_43)); }
	inline Terrain_t59182933 * get_SyncedTerrain_43() const { return ___SyncedTerrain_43; }
	inline Terrain_t59182933 ** get_address_of_SyncedTerrain_43() { return &___SyncedTerrain_43; }
	inline void set_SyncedTerrain_43(Terrain_t59182933 * value)
	{
		___SyncedTerrain_43 = value;
		Il2CppCodeGenWriteBarrier(&___SyncedTerrain_43, value);
	}

	inline static int32_t get_offset_of_AutoSyncInPlaymode_44() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AutoSyncInPlaymode_44)); }
	inline bool get_AutoSyncInPlaymode_44() const { return ___AutoSyncInPlaymode_44; }
	inline bool* get_address_of_AutoSyncInPlaymode_44() { return &___AutoSyncInPlaymode_44; }
	inline void set_AutoSyncInPlaymode_44(bool value)
	{
		___AutoSyncInPlaymode_44 = value;
	}

	inline static int32_t get_offset_of_DetailDistanceForGrassShader_45() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___DetailDistanceForGrassShader_45)); }
	inline float get_DetailDistanceForGrassShader_45() const { return ___DetailDistanceForGrassShader_45; }
	inline float* get_address_of_DetailDistanceForGrassShader_45() { return &___DetailDistanceForGrassShader_45; }
	inline void set_DetailDistanceForGrassShader_45(float value)
	{
		___DetailDistanceForGrassShader_45 = value;
	}

	inline static int32_t get_offset_of_BillboardStart_46() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardStart_46)); }
	inline float get_BillboardStart_46() const { return ___BillboardStart_46; }
	inline float* get_address_of_BillboardStart_46() { return &___BillboardStart_46; }
	inline void set_BillboardStart_46(float value)
	{
		___BillboardStart_46 = value;
	}

	inline static int32_t get_offset_of_BillboardFadeLenght_47() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardFadeLenght_47)); }
	inline float get_BillboardFadeLenght_47() const { return ___BillboardFadeLenght_47; }
	inline float* get_address_of_BillboardFadeLenght_47() { return &___BillboardFadeLenght_47; }
	inline void set_BillboardFadeLenght_47(float value)
	{
		___BillboardFadeLenght_47 = value;
	}

	inline static int32_t get_offset_of_GrassAnimateNormal_48() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___GrassAnimateNormal_48)); }
	inline bool get_GrassAnimateNormal_48() const { return ___GrassAnimateNormal_48; }
	inline bool* get_address_of_GrassAnimateNormal_48() { return &___GrassAnimateNormal_48; }
	inline void set_GrassAnimateNormal_48(bool value)
	{
		___GrassAnimateNormal_48 = value;
	}

	inline static int32_t get_offset_of_GrassWavingTint_49() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___GrassWavingTint_49)); }
	inline Color_t2020392075  get_GrassWavingTint_49() const { return ___GrassWavingTint_49; }
	inline Color_t2020392075 * get_address_of_GrassWavingTint_49() { return &___GrassWavingTint_49; }
	inline void set_GrassWavingTint_49(Color_t2020392075  value)
	{
		___GrassWavingTint_49 = value;
	}

	inline static int32_t get_offset_of_AFSTreeColor_50() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AFSTreeColor_50)); }
	inline Color_t2020392075  get_AFSTreeColor_50() const { return ___AFSTreeColor_50; }
	inline Color_t2020392075 * get_address_of_AFSTreeColor_50() { return &___AFSTreeColor_50; }
	inline void set_AFSTreeColor_50(Color_t2020392075  value)
	{
		___AFSTreeColor_50 = value;
	}

	inline static int32_t get_offset_of_MainCam_51() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___MainCam_51)); }
	inline Camera_t189460977 * get_MainCam_51() const { return ___MainCam_51; }
	inline Camera_t189460977 ** get_address_of_MainCam_51() { return &___MainCam_51; }
	inline void set_MainCam_51(Camera_t189460977 * value)
	{
		___MainCam_51 = value;
		Il2CppCodeGenWriteBarrier(&___MainCam_51, value);
	}

	inline static int32_t get_offset_of_TreeBillboardShadows_52() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___TreeBillboardShadows_52)); }
	inline bool get_TreeBillboardShadows_52() const { return ___TreeBillboardShadows_52; }
	inline bool* get_address_of_TreeBillboardShadows_52() { return &___TreeBillboardShadows_52; }
	inline void set_TreeBillboardShadows_52(bool value)
	{
		___TreeBillboardShadows_52 = value;
	}

	inline static int32_t get_offset_of_BillboardFadeOutLength_53() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardFadeOutLength_53)); }
	inline float get_BillboardFadeOutLength_53() const { return ___BillboardFadeOutLength_53; }
	inline float* get_address_of_BillboardFadeOutLength_53() { return &___BillboardFadeOutLength_53; }
	inline void set_BillboardFadeOutLength_53(float value)
	{
		___BillboardFadeOutLength_53 = value;
	}

	inline static int32_t get_offset_of_BillboardAdjustToCamera_54() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardAdjustToCamera_54)); }
	inline bool get_BillboardAdjustToCamera_54() const { return ___BillboardAdjustToCamera_54; }
	inline bool* get_address_of_BillboardAdjustToCamera_54() { return &___BillboardAdjustToCamera_54; }
	inline void set_BillboardAdjustToCamera_54(bool value)
	{
		___BillboardAdjustToCamera_54 = value;
	}

	inline static int32_t get_offset_of_BillboardAngleLimit_55() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardAngleLimit_55)); }
	inline float get_BillboardAngleLimit_55() const { return ___BillboardAngleLimit_55; }
	inline float* get_address_of_BillboardAngleLimit_55() { return &___BillboardAngleLimit_55; }
	inline void set_BillboardAngleLimit_55(float value)
	{
		___BillboardAngleLimit_55 = value;
	}

	inline static int32_t get_offset_of_BillboardShadowColor_56() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardShadowColor_56)); }
	inline Color_t2020392075  get_BillboardShadowColor_56() const { return ___BillboardShadowColor_56; }
	inline Color_t2020392075 * get_address_of_BillboardShadowColor_56() { return &___BillboardShadowColor_56; }
	inline void set_BillboardShadowColor_56(Color_t2020392075  value)
	{
		___BillboardShadowColor_56 = value;
	}

	inline static int32_t get_offset_of_BillboardAmbientLightFactor_57() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardAmbientLightFactor_57)); }
	inline float get_BillboardAmbientLightFactor_57() const { return ___BillboardAmbientLightFactor_57; }
	inline float* get_address_of_BillboardAmbientLightFactor_57() { return &___BillboardAmbientLightFactor_57; }
	inline void set_BillboardAmbientLightFactor_57(float value)
	{
		___BillboardAmbientLightFactor_57 = value;
	}

	inline static int32_t get_offset_of_BillboardAmbientLightDesaturationFactor_58() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___BillboardAmbientLightDesaturationFactor_58)); }
	inline float get_BillboardAmbientLightDesaturationFactor_58() const { return ___BillboardAmbientLightDesaturationFactor_58; }
	inline float* get_address_of_BillboardAmbientLightDesaturationFactor_58() { return &___BillboardAmbientLightDesaturationFactor_58; }
	inline void set_BillboardAmbientLightDesaturationFactor_58(float value)
	{
		___BillboardAmbientLightDesaturationFactor_58 = value;
	}

	inline static int32_t get_offset_of_AutosyncShadowColor_59() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AutosyncShadowColor_59)); }
	inline bool get_AutosyncShadowColor_59() const { return ___AutosyncShadowColor_59; }
	inline bool* get_address_of_AutosyncShadowColor_59() { return &___AutosyncShadowColor_59; }
	inline void set_AutosyncShadowColor_59(bool value)
	{
		___AutosyncShadowColor_59 = value;
	}

	inline static int32_t get_offset_of_EnableCameraLayerCulling_60() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___EnableCameraLayerCulling_60)); }
	inline bool get_EnableCameraLayerCulling_60() const { return ___EnableCameraLayerCulling_60; }
	inline bool* get_address_of_EnableCameraLayerCulling_60() { return &___EnableCameraLayerCulling_60; }
	inline void set_EnableCameraLayerCulling_60(bool value)
	{
		___EnableCameraLayerCulling_60 = value;
	}

	inline static int32_t get_offset_of_SmallDetailsDistance_61() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___SmallDetailsDistance_61)); }
	inline int32_t get_SmallDetailsDistance_61() const { return ___SmallDetailsDistance_61; }
	inline int32_t* get_address_of_SmallDetailsDistance_61() { return &___SmallDetailsDistance_61; }
	inline void set_SmallDetailsDistance_61(int32_t value)
	{
		___SmallDetailsDistance_61 = value;
	}

	inline static int32_t get_offset_of_MediumDetailsDistance_62() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___MediumDetailsDistance_62)); }
	inline int32_t get_MediumDetailsDistance_62() const { return ___MediumDetailsDistance_62; }
	inline int32_t* get_address_of_MediumDetailsDistance_62() { return &___MediumDetailsDistance_62; }
	inline void set_MediumDetailsDistance_62(int32_t value)
	{
		___MediumDetailsDistance_62 = value;
	}

	inline static int32_t get_offset_of_AllGrassObjectsCombined_63() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___AllGrassObjectsCombined_63)); }
	inline bool get_AllGrassObjectsCombined_63() const { return ___AllGrassObjectsCombined_63; }
	inline bool* get_address_of_AllGrassObjectsCombined_63() { return &___AllGrassObjectsCombined_63; }
	inline void set_AllGrassObjectsCombined_63(bool value)
	{
		___AllGrassObjectsCombined_63 = value;
	}

	inline static int32_t get_offset_of_TempWind_64() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___TempWind_64)); }
	inline Vector4_t2243707581  get_TempWind_64() const { return ___TempWind_64; }
	inline Vector4_t2243707581 * get_address_of_TempWind_64() { return &___TempWind_64; }
	inline void set_TempWind_64(Vector4_t2243707581  value)
	{
		___TempWind_64 = value;
	}

	inline static int32_t get_offset_of_GrassWindSpeed_65() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___GrassWindSpeed_65)); }
	inline float get_GrassWindSpeed_65() const { return ___GrassWindSpeed_65; }
	inline float* get_address_of_GrassWindSpeed_65() { return &___GrassWindSpeed_65; }
	inline void set_GrassWindSpeed_65(float value)
	{
		___GrassWindSpeed_65 = value;
	}

	inline static int32_t get_offset_of_SinusWave_66() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___SinusWave_66)); }
	inline float get_SinusWave_66() const { return ___SinusWave_66; }
	inline float* get_address_of_SinusWave_66() { return &___SinusWave_66; }
	inline void set_SinusWave_66(float value)
	{
		___SinusWave_66 = value;
	}

	inline static int32_t get_offset_of_TriangleWaves_67() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___TriangleWaves_67)); }
	inline Vector4_t2243707581  get_TriangleWaves_67() const { return ___TriangleWaves_67; }
	inline Vector4_t2243707581 * get_address_of_TriangleWaves_67() { return &___TriangleWaves_67; }
	inline void set_TriangleWaves_67(Vector4_t2243707581  value)
	{
		___TriangleWaves_67 = value;
	}

	inline static int32_t get_offset_of_Oscillation_68() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___Oscillation_68)); }
	inline float get_Oscillation_68() const { return ___Oscillation_68; }
	inline float* get_address_of_Oscillation_68() { return &___Oscillation_68; }
	inline void set_Oscillation_68(float value)
	{
		___Oscillation_68 = value;
	}

	inline static int32_t get_offset_of_CameraForward_69() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___CameraForward_69)); }
	inline Vector3_t2243707580  get_CameraForward_69() const { return ___CameraForward_69; }
	inline Vector3_t2243707580 * get_address_of_CameraForward_69() { return &___CameraForward_69; }
	inline void set_CameraForward_69(Vector3_t2243707580  value)
	{
		___CameraForward_69 = value;
	}

	inline static int32_t get_offset_of_ShadowCameraForward_70() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___ShadowCameraForward_70)); }
	inline Vector3_t2243707580  get_ShadowCameraForward_70() const { return ___ShadowCameraForward_70; }
	inline Vector3_t2243707580 * get_address_of_ShadowCameraForward_70() { return &___ShadowCameraForward_70; }
	inline void set_ShadowCameraForward_70(Vector3_t2243707580  value)
	{
		___ShadowCameraForward_70 = value;
	}

	inline static int32_t get_offset_of_CameraForwardVec_71() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___CameraForwardVec_71)); }
	inline Vector3_t2243707580  get_CameraForwardVec_71() const { return ___CameraForwardVec_71; }
	inline Vector3_t2243707580 * get_address_of_CameraForwardVec_71() { return &___CameraForwardVec_71; }
	inline void set_CameraForwardVec_71(Vector3_t2243707580  value)
	{
		___CameraForwardVec_71 = value;
	}

	inline static int32_t get_offset_of_rollingX_72() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___rollingX_72)); }
	inline float get_rollingX_72() const { return ___rollingX_72; }
	inline float* get_address_of_rollingX_72() { return &___rollingX_72; }
	inline void set_rollingX_72(float value)
	{
		___rollingX_72 = value;
	}

	inline static int32_t get_offset_of_rollingXShadow_73() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___rollingXShadow_73)); }
	inline float get_rollingXShadow_73() const { return ___rollingXShadow_73; }
	inline float* get_address_of_rollingXShadow_73() { return &___rollingXShadow_73; }
	inline void set_rollingXShadow_73(float value)
	{
		___rollingXShadow_73 = value;
	}

	inline static int32_t get_offset_of_lightDir_74() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___lightDir_74)); }
	inline Vector3_t2243707580  get_lightDir_74() const { return ___lightDir_74; }
	inline Vector3_t2243707580 * get_address_of_lightDir_74() { return &___lightDir_74; }
	inline void set_lightDir_74(Vector3_t2243707580  value)
	{
		___lightDir_74 = value;
	}

	inline static int32_t get_offset_of_templightDir_75() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___templightDir_75)); }
	inline Vector3_t2243707580  get_templightDir_75() const { return ___templightDir_75; }
	inline Vector3_t2243707580 * get_address_of_templightDir_75() { return &___templightDir_75; }
	inline void set_templightDir_75(Vector3_t2243707580  value)
	{
		___templightDir_75 = value;
	}

	inline static int32_t get_offset_of_CameraAngle_76() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___CameraAngle_76)); }
	inline float get_CameraAngle_76() const { return ___CameraAngle_76; }
	inline float* get_address_of_CameraAngle_76() { return &___CameraAngle_76; }
	inline void set_CameraAngle_76(float value)
	{
		___CameraAngle_76 = value;
	}

	inline static int32_t get_offset_of_allTerrains_77() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___allTerrains_77)); }
	inline TerrainU5BU5D_t3242190776* get_allTerrains_77() const { return ___allTerrains_77; }
	inline TerrainU5BU5D_t3242190776** get_address_of_allTerrains_77() { return &___allTerrains_77; }
	inline void set_allTerrains_77(TerrainU5BU5D_t3242190776* value)
	{
		___allTerrains_77 = value;
		Il2CppCodeGenWriteBarrier(&___allTerrains_77, value);
	}

	inline static int32_t get_offset_of_fLight_78() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___fLight_78)); }
	inline Vector3U5BU5D_t1172311765* get_fLight_78() const { return ___fLight_78; }
	inline Vector3U5BU5D_t1172311765** get_address_of_fLight_78() { return &___fLight_78; }
	inline void set_fLight_78(Vector3U5BU5D_t1172311765* value)
	{
		___fLight_78 = value;
		Il2CppCodeGenWriteBarrier(&___fLight_78, value);
	}

	inline static int32_t get_offset_of_vCoeff_79() { return static_cast<int32_t>(offsetof(SetupAdvancedFoliageShader_t182079117, ___vCoeff_79)); }
	inline Vector4U5BU5D_t1658499504* get_vCoeff_79() const { return ___vCoeff_79; }
	inline Vector4U5BU5D_t1658499504** get_address_of_vCoeff_79() { return &___vCoeff_79; }
	inline void set_vCoeff_79(Vector4U5BU5D_t1658499504* value)
	{
		___vCoeff_79 = value;
		Il2CppCodeGenWriteBarrier(&___vCoeff_79, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
