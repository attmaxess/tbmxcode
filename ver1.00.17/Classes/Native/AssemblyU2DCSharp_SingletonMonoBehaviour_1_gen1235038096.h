﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ReactionHistory
struct ReactionHistory_t2538361761;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<ReactionHistory>
struct  SingletonMonoBehaviour_1_t1235038096  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct SingletonMonoBehaviour_1_t1235038096_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	ReactionHistory_t2538361761 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t1235038096_StaticFields, ___instance_2)); }
	inline ReactionHistory_t2538361761 * get_instance_2() const { return ___instance_2; }
	inline ReactionHistory_t2538361761 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ReactionHistory_t2538361761 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
