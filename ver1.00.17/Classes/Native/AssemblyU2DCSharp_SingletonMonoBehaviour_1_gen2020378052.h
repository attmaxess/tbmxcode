﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PrizedManager
struct PrizedManager_t3323701717;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<PrizedManager>
struct  SingletonMonoBehaviour_1_t2020378052  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct SingletonMonoBehaviour_1_t2020378052_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	PrizedManager_t3323701717 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t2020378052_StaticFields, ___instance_2)); }
	inline PrizedManager_t3323701717 * get_instance_2() const { return ___instance_2; }
	inline PrizedManager_t3323701717 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PrizedManager_t3323701717 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
