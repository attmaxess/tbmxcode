﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// MiniGameManager
struct MiniGameManager_t849864952;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<MiniGameManager>
struct  SingletonMonoBehaviour_1_t3841508583  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct SingletonMonoBehaviour_1_t3841508583_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	MiniGameManager_t849864952 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t3841508583_StaticFields, ___instance_2)); }
	inline MiniGameManager_t849864952 * get_instance_2() const { return ___instance_2; }
	inline MiniGameManager_t849864952 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(MiniGameManager_t849864952 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
