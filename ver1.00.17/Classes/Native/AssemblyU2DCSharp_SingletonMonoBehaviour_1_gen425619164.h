﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// SceneFadeManager
struct SceneFadeManager_t1728942829;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<SceneFadeManager>
struct  SingletonMonoBehaviour_1_t425619164  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct SingletonMonoBehaviour_1_t425619164_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	SceneFadeManager_t1728942829 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t425619164_StaticFields, ___instance_2)); }
	inline SceneFadeManager_t1728942829 * get_instance_2() const { return ___instance_2; }
	inline SceneFadeManager_t1728942829 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SceneFadeManager_t1728942829 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
