﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlideInOut
struct  SlideInOut_t958296806  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean SlideInOut::Enable
	bool ___Enable_2;
	// UnityEngine.Vector2 SlideInOut::ActivePos
	Vector2_t2243707579  ___ActivePos_3;
	// UnityEngine.Vector2 SlideInOut::NonActive
	Vector2_t2243707579  ___NonActive_4;
	// System.Single SlideInOut::Speed
	float ___Speed_5;
	// System.Single SlideInOut::Raito
	float ___Raito_6;
	// DG.Tweening.Ease SlideInOut::EaseType
	int32_t ___EaseType_7;
	// UnityEngine.RectTransform SlideInOut::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_8;

public:
	inline static int32_t get_offset_of_Enable_2() { return static_cast<int32_t>(offsetof(SlideInOut_t958296806, ___Enable_2)); }
	inline bool get_Enable_2() const { return ___Enable_2; }
	inline bool* get_address_of_Enable_2() { return &___Enable_2; }
	inline void set_Enable_2(bool value)
	{
		___Enable_2 = value;
	}

	inline static int32_t get_offset_of_ActivePos_3() { return static_cast<int32_t>(offsetof(SlideInOut_t958296806, ___ActivePos_3)); }
	inline Vector2_t2243707579  get_ActivePos_3() const { return ___ActivePos_3; }
	inline Vector2_t2243707579 * get_address_of_ActivePos_3() { return &___ActivePos_3; }
	inline void set_ActivePos_3(Vector2_t2243707579  value)
	{
		___ActivePos_3 = value;
	}

	inline static int32_t get_offset_of_NonActive_4() { return static_cast<int32_t>(offsetof(SlideInOut_t958296806, ___NonActive_4)); }
	inline Vector2_t2243707579  get_NonActive_4() const { return ___NonActive_4; }
	inline Vector2_t2243707579 * get_address_of_NonActive_4() { return &___NonActive_4; }
	inline void set_NonActive_4(Vector2_t2243707579  value)
	{
		___NonActive_4 = value;
	}

	inline static int32_t get_offset_of_Speed_5() { return static_cast<int32_t>(offsetof(SlideInOut_t958296806, ___Speed_5)); }
	inline float get_Speed_5() const { return ___Speed_5; }
	inline float* get_address_of_Speed_5() { return &___Speed_5; }
	inline void set_Speed_5(float value)
	{
		___Speed_5 = value;
	}

	inline static int32_t get_offset_of_Raito_6() { return static_cast<int32_t>(offsetof(SlideInOut_t958296806, ___Raito_6)); }
	inline float get_Raito_6() const { return ___Raito_6; }
	inline float* get_address_of_Raito_6() { return &___Raito_6; }
	inline void set_Raito_6(float value)
	{
		___Raito_6 = value;
	}

	inline static int32_t get_offset_of_EaseType_7() { return static_cast<int32_t>(offsetof(SlideInOut_t958296806, ___EaseType_7)); }
	inline int32_t get_EaseType_7() const { return ___EaseType_7; }
	inline int32_t* get_address_of_EaseType_7() { return &___EaseType_7; }
	inline void set_EaseType_7(int32_t value)
	{
		___EaseType_7 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_8() { return static_cast<int32_t>(offsetof(SlideInOut_t958296806, ___m_RectTransform_8)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_8() const { return ___m_RectTransform_8; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_8() { return &___m_RectTransform_8; }
	inline void set_m_RectTransform_8(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTransform_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
