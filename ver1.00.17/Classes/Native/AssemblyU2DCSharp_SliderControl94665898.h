﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// ModelingConfigPanel
struct ModelingConfigPanel_t2566912473;
// UnityEngine.UI.Slider
struct Slider_t297367283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SliderControl
struct  SliderControl_t94665898  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SliderControl::sliderVol
	Text_t356221433 * ___sliderVol_2;
	// System.Boolean SliderControl::RotetoVal
	bool ___RotetoVal_3;
	// System.Int32 SliderControl::Pole
	int32_t ___Pole_4;
	// ModelingConfigPanel SliderControl::m_modelingConfigPanel
	ModelingConfigPanel_t2566912473 * ___m_modelingConfigPanel_5;
	// UnityEngine.UI.Slider SliderControl::slideber
	Slider_t297367283 * ___slideber_6;
	// System.Single SliderControl::prevValue
	float ___prevValue_7;

public:
	inline static int32_t get_offset_of_sliderVol_2() { return static_cast<int32_t>(offsetof(SliderControl_t94665898, ___sliderVol_2)); }
	inline Text_t356221433 * get_sliderVol_2() const { return ___sliderVol_2; }
	inline Text_t356221433 ** get_address_of_sliderVol_2() { return &___sliderVol_2; }
	inline void set_sliderVol_2(Text_t356221433 * value)
	{
		___sliderVol_2 = value;
		Il2CppCodeGenWriteBarrier(&___sliderVol_2, value);
	}

	inline static int32_t get_offset_of_RotetoVal_3() { return static_cast<int32_t>(offsetof(SliderControl_t94665898, ___RotetoVal_3)); }
	inline bool get_RotetoVal_3() const { return ___RotetoVal_3; }
	inline bool* get_address_of_RotetoVal_3() { return &___RotetoVal_3; }
	inline void set_RotetoVal_3(bool value)
	{
		___RotetoVal_3 = value;
	}

	inline static int32_t get_offset_of_Pole_4() { return static_cast<int32_t>(offsetof(SliderControl_t94665898, ___Pole_4)); }
	inline int32_t get_Pole_4() const { return ___Pole_4; }
	inline int32_t* get_address_of_Pole_4() { return &___Pole_4; }
	inline void set_Pole_4(int32_t value)
	{
		___Pole_4 = value;
	}

	inline static int32_t get_offset_of_m_modelingConfigPanel_5() { return static_cast<int32_t>(offsetof(SliderControl_t94665898, ___m_modelingConfigPanel_5)); }
	inline ModelingConfigPanel_t2566912473 * get_m_modelingConfigPanel_5() const { return ___m_modelingConfigPanel_5; }
	inline ModelingConfigPanel_t2566912473 ** get_address_of_m_modelingConfigPanel_5() { return &___m_modelingConfigPanel_5; }
	inline void set_m_modelingConfigPanel_5(ModelingConfigPanel_t2566912473 * value)
	{
		___m_modelingConfigPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_modelingConfigPanel_5, value);
	}

	inline static int32_t get_offset_of_slideber_6() { return static_cast<int32_t>(offsetof(SliderControl_t94665898, ___slideber_6)); }
	inline Slider_t297367283 * get_slideber_6() const { return ___slideber_6; }
	inline Slider_t297367283 ** get_address_of_slideber_6() { return &___slideber_6; }
	inline void set_slideber_6(Slider_t297367283 * value)
	{
		___slideber_6 = value;
		Il2CppCodeGenWriteBarrier(&___slideber_6, value);
	}

	inline static int32_t get_offset_of_prevValue_7() { return static_cast<int32_t>(offsetof(SliderControl_t94665898, ___prevValue_7)); }
	inline float get_prevValue_7() const { return ___prevValue_7; }
	inline float* get_address_of_prevValue_7() { return &___prevValue_7; }
	inline void set_prevValue_7(float value)
	{
		___prevValue_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
