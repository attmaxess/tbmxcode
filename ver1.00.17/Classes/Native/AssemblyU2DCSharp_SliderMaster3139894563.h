﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1836570898.h"

// ConfigPanel[]
struct ConfigPanelU5BU5D_t759859513;
// UnityEngine.UI.Slider
struct Slider_t297367283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SliderMaster
struct  SliderMaster_t3139894563  : public SingletonMonoBehaviour_1_t1836570898
{
public:
	// ConfigPanel[] SliderMaster::ConfigPanels
	ConfigPanelU5BU5D_t759859513* ___ConfigPanels_3;
	// UnityEngine.UI.Slider SliderMaster::EnagySlider
	Slider_t297367283 * ___EnagySlider_4;
	// UnityEngine.UI.Slider SliderMaster::ActionEnagySlider
	Slider_t297367283 * ___ActionEnagySlider_5;
	// System.Boolean SliderMaster::PanelOn
	bool ___PanelOn_6;

public:
	inline static int32_t get_offset_of_ConfigPanels_3() { return static_cast<int32_t>(offsetof(SliderMaster_t3139894563, ___ConfigPanels_3)); }
	inline ConfigPanelU5BU5D_t759859513* get_ConfigPanels_3() const { return ___ConfigPanels_3; }
	inline ConfigPanelU5BU5D_t759859513** get_address_of_ConfigPanels_3() { return &___ConfigPanels_3; }
	inline void set_ConfigPanels_3(ConfigPanelU5BU5D_t759859513* value)
	{
		___ConfigPanels_3 = value;
		Il2CppCodeGenWriteBarrier(&___ConfigPanels_3, value);
	}

	inline static int32_t get_offset_of_EnagySlider_4() { return static_cast<int32_t>(offsetof(SliderMaster_t3139894563, ___EnagySlider_4)); }
	inline Slider_t297367283 * get_EnagySlider_4() const { return ___EnagySlider_4; }
	inline Slider_t297367283 ** get_address_of_EnagySlider_4() { return &___EnagySlider_4; }
	inline void set_EnagySlider_4(Slider_t297367283 * value)
	{
		___EnagySlider_4 = value;
		Il2CppCodeGenWriteBarrier(&___EnagySlider_4, value);
	}

	inline static int32_t get_offset_of_ActionEnagySlider_5() { return static_cast<int32_t>(offsetof(SliderMaster_t3139894563, ___ActionEnagySlider_5)); }
	inline Slider_t297367283 * get_ActionEnagySlider_5() const { return ___ActionEnagySlider_5; }
	inline Slider_t297367283 ** get_address_of_ActionEnagySlider_5() { return &___ActionEnagySlider_5; }
	inline void set_ActionEnagySlider_5(Slider_t297367283 * value)
	{
		___ActionEnagySlider_5 = value;
		Il2CppCodeGenWriteBarrier(&___ActionEnagySlider_5, value);
	}

	inline static int32_t get_offset_of_PanelOn_6() { return static_cast<int32_t>(offsetof(SliderMaster_t3139894563, ___PanelOn_6)); }
	inline bool get_PanelOn_6() const { return ___PanelOn_6; }
	inline bool* get_address_of_PanelOn_6() { return &___PanelOn_6; }
	inline void set_PanelOn_6(bool value)
	{
		___PanelOn_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
