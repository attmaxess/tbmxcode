﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PartsPanelSliders
struct PartsPanelSliders_t4209932670;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubCameraHandler
struct  SubCameraHandler_t2753085375  : public MonoBehaviour_t1158329972
{
public:
	// PartsPanelSliders SubCameraHandler::PartsPanelSliders
	PartsPanelSliders_t4209932670 * ___PartsPanelSliders_2;
	// System.Single SubCameraHandler::Speed
	float ___Speed_3;

public:
	inline static int32_t get_offset_of_PartsPanelSliders_2() { return static_cast<int32_t>(offsetof(SubCameraHandler_t2753085375, ___PartsPanelSliders_2)); }
	inline PartsPanelSliders_t4209932670 * get_PartsPanelSliders_2() const { return ___PartsPanelSliders_2; }
	inline PartsPanelSliders_t4209932670 ** get_address_of_PartsPanelSliders_2() { return &___PartsPanelSliders_2; }
	inline void set_PartsPanelSliders_2(PartsPanelSliders_t4209932670 * value)
	{
		___PartsPanelSliders_2 = value;
		Il2CppCodeGenWriteBarrier(&___PartsPanelSliders_2, value);
	}

	inline static int32_t get_offset_of_Speed_3() { return static_cast<int32_t>(offsetof(SubCameraHandler_t2753085375, ___Speed_3)); }
	inline float get_Speed_3() const { return ___Speed_3; }
	inline float* get_address_of_Speed_3() { return &___Speed_3; }
	inline void set_Speed_3(float value)
	{
		___Speed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
