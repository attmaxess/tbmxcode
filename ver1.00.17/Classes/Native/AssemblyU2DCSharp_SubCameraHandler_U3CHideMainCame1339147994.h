﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SubCameraHandler
struct SubCameraHandler_t2753085375;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubCameraHandler/<HideMainCamera>c__AnonStorey3
struct  U3CHideMainCameraU3Ec__AnonStorey3_t1339147994  : public Il2CppObject
{
public:
	// System.Single SubCameraHandler/<HideMainCamera>c__AnonStorey3::vCam
	float ___vCam_0;
	// SubCameraHandler SubCameraHandler/<HideMainCamera>c__AnonStorey3::$this
	SubCameraHandler_t2753085375 * ___U24this_1;

public:
	inline static int32_t get_offset_of_vCam_0() { return static_cast<int32_t>(offsetof(U3CHideMainCameraU3Ec__AnonStorey3_t1339147994, ___vCam_0)); }
	inline float get_vCam_0() const { return ___vCam_0; }
	inline float* get_address_of_vCam_0() { return &___vCam_0; }
	inline void set_vCam_0(float value)
	{
		___vCam_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CHideMainCameraU3Ec__AnonStorey3_t1339147994, ___U24this_1)); }
	inline SubCameraHandler_t2753085375 * get_U24this_1() const { return ___U24this_1; }
	inline SubCameraHandler_t2753085375 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SubCameraHandler_t2753085375 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
