﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// BackgroundFader
struct BackgroundFader_t837055620;
// SuccessPanelFader
struct SuccessPanelFader_t1974355695;
// SuccessfulCircles
struct SuccessfulCircles_t3791497435;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuccessfulAnimator
struct  SuccessfulAnimator_t1087388389  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SuccessfulAnimator::Circles
	GameObject_t1756533147 * ___Circles_2;
	// BackgroundFader SuccessfulAnimator::Background
	BackgroundFader_t837055620 * ___Background_3;
	// SuccessPanelFader SuccessfulAnimator::SuccessPanel
	SuccessPanelFader_t1974355695 * ___SuccessPanel_4;
	// System.Single SuccessfulAnimator::BackgroundTime
	float ___BackgroundTime_5;
	// System.Single SuccessfulAnimator::SuccessTime
	float ___SuccessTime_6;
	// SuccessfulCircles SuccessfulAnimator::m_SuccessfulCircles
	SuccessfulCircles_t3791497435 * ___m_SuccessfulCircles_7;
	// System.Boolean SuccessfulAnimator::m_bHideSuccess
	bool ___m_bHideSuccess_8;

public:
	inline static int32_t get_offset_of_Circles_2() { return static_cast<int32_t>(offsetof(SuccessfulAnimator_t1087388389, ___Circles_2)); }
	inline GameObject_t1756533147 * get_Circles_2() const { return ___Circles_2; }
	inline GameObject_t1756533147 ** get_address_of_Circles_2() { return &___Circles_2; }
	inline void set_Circles_2(GameObject_t1756533147 * value)
	{
		___Circles_2 = value;
		Il2CppCodeGenWriteBarrier(&___Circles_2, value);
	}

	inline static int32_t get_offset_of_Background_3() { return static_cast<int32_t>(offsetof(SuccessfulAnimator_t1087388389, ___Background_3)); }
	inline BackgroundFader_t837055620 * get_Background_3() const { return ___Background_3; }
	inline BackgroundFader_t837055620 ** get_address_of_Background_3() { return &___Background_3; }
	inline void set_Background_3(BackgroundFader_t837055620 * value)
	{
		___Background_3 = value;
		Il2CppCodeGenWriteBarrier(&___Background_3, value);
	}

	inline static int32_t get_offset_of_SuccessPanel_4() { return static_cast<int32_t>(offsetof(SuccessfulAnimator_t1087388389, ___SuccessPanel_4)); }
	inline SuccessPanelFader_t1974355695 * get_SuccessPanel_4() const { return ___SuccessPanel_4; }
	inline SuccessPanelFader_t1974355695 ** get_address_of_SuccessPanel_4() { return &___SuccessPanel_4; }
	inline void set_SuccessPanel_4(SuccessPanelFader_t1974355695 * value)
	{
		___SuccessPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___SuccessPanel_4, value);
	}

	inline static int32_t get_offset_of_BackgroundTime_5() { return static_cast<int32_t>(offsetof(SuccessfulAnimator_t1087388389, ___BackgroundTime_5)); }
	inline float get_BackgroundTime_5() const { return ___BackgroundTime_5; }
	inline float* get_address_of_BackgroundTime_5() { return &___BackgroundTime_5; }
	inline void set_BackgroundTime_5(float value)
	{
		___BackgroundTime_5 = value;
	}

	inline static int32_t get_offset_of_SuccessTime_6() { return static_cast<int32_t>(offsetof(SuccessfulAnimator_t1087388389, ___SuccessTime_6)); }
	inline float get_SuccessTime_6() const { return ___SuccessTime_6; }
	inline float* get_address_of_SuccessTime_6() { return &___SuccessTime_6; }
	inline void set_SuccessTime_6(float value)
	{
		___SuccessTime_6 = value;
	}

	inline static int32_t get_offset_of_m_SuccessfulCircles_7() { return static_cast<int32_t>(offsetof(SuccessfulAnimator_t1087388389, ___m_SuccessfulCircles_7)); }
	inline SuccessfulCircles_t3791497435 * get_m_SuccessfulCircles_7() const { return ___m_SuccessfulCircles_7; }
	inline SuccessfulCircles_t3791497435 ** get_address_of_m_SuccessfulCircles_7() { return &___m_SuccessfulCircles_7; }
	inline void set_m_SuccessfulCircles_7(SuccessfulCircles_t3791497435 * value)
	{
		___m_SuccessfulCircles_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_SuccessfulCircles_7, value);
	}

	inline static int32_t get_offset_of_m_bHideSuccess_8() { return static_cast<int32_t>(offsetof(SuccessfulAnimator_t1087388389, ___m_bHideSuccess_8)); }
	inline bool get_m_bHideSuccess_8() const { return ___m_bHideSuccess_8; }
	inline bool* get_address_of_m_bHideSuccess_8() { return &___m_bHideSuccess_8; }
	inline void set_m_bHideSuccess_8(bool value)
	{
		___m_bHideSuccess_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
