﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SurfaceTension
struct  SurfaceTension_t3962841259  : public MonoBehaviour_t1158329972
{
public:
	// System.Single SurfaceTension::Tension
	float ___Tension_2;
	// System.Single SurfaceTension::defaultTension
	float ___defaultTension_3;
	// System.Boolean SurfaceTension::off
	bool ___off_4;

public:
	inline static int32_t get_offset_of_Tension_2() { return static_cast<int32_t>(offsetof(SurfaceTension_t3962841259, ___Tension_2)); }
	inline float get_Tension_2() const { return ___Tension_2; }
	inline float* get_address_of_Tension_2() { return &___Tension_2; }
	inline void set_Tension_2(float value)
	{
		___Tension_2 = value;
	}

	inline static int32_t get_offset_of_defaultTension_3() { return static_cast<int32_t>(offsetof(SurfaceTension_t3962841259, ___defaultTension_3)); }
	inline float get_defaultTension_3() const { return ___defaultTension_3; }
	inline float* get_address_of_defaultTension_3() { return &___defaultTension_3; }
	inline void set_defaultTension_3(float value)
	{
		___defaultTension_3 = value;
	}

	inline static int32_t get_offset_of_off_4() { return static_cast<int32_t>(offsetof(SurfaceTension_t3962841259, ___off_4)); }
	inline bool get_off_4() const { return ___off_4; }
	inline bool* get_address_of_off_4() { return &___off_4; }
	inline void set_off_4(bool value)
	{
		___off_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
