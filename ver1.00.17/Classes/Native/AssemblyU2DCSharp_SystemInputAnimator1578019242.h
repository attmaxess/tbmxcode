﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// InputPanelItem[]
struct InputPanelItemU5BU5D_t3786550914;
// LoadCountAnimator
struct LoadCountAnimator_t3458511750;
// SystemInputCircles
struct SystemInputCircles_t2127672316;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SystemInputAnimator
struct  SystemInputAnimator_t1578019242  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SystemInputAnimator::Successful
	GameObject_t1756533147 * ___Successful_2;
	// UnityEngine.GameObject SystemInputAnimator::Circles
	GameObject_t1756533147 * ___Circles_3;
	// UnityEngine.UI.Text SystemInputAnimator::YourNameText
	Text_t356221433 * ___YourNameText_4;
	// UnityEngine.UI.Text SystemInputAnimator::NameText
	Text_t356221433 * ___NameText_5;
	// UnityEngine.RectTransform SystemInputAnimator::SuccessPanelRectTrans
	RectTransform_t3349966182 * ___SuccessPanelRectTrans_6;
	// InputPanelItem[] SystemInputAnimator::Items
	InputPanelItemU5BU5D_t3786550914* ___Items_7;
	// LoadCountAnimator SystemInputAnimator::LoadCountAnimator
	LoadCountAnimator_t3458511750 * ___LoadCountAnimator_8;
	// System.Single SystemInputAnimator::ShowYourNameTime
	float ___ShowYourNameTime_9;
	// System.Single SystemInputAnimator::ShowNameTime
	float ___ShowNameTime_10;
	// System.Single SystemInputAnimator::ShowInputPanelTime
	float ___ShowInputPanelTime_11;
	// System.Single SystemInputAnimator::HideYourNameTime
	float ___HideYourNameTime_12;
	// System.Single SystemInputAnimator::HideNameTime
	float ___HideNameTime_13;
	// System.Single SystemInputAnimator::HideInputPanelTime
	float ___HideInputPanelTime_14;
	// SystemInputCircles SystemInputAnimator::m_Circles
	SystemInputCircles_t2127672316 * ___m_Circles_15;
	// System.Boolean SystemInputAnimator::m_bShow
	bool ___m_bShow_16;
	// System.Boolean SystemInputAnimator::m_bHide
	bool ___m_bHide_17;
	// System.Boolean SystemInputAnimator::m_bLoading
	bool ___m_bLoading_18;
	// System.Boolean SystemInputAnimator::m_bConter
	bool ___m_bConter_19;
	// System.Boolean SystemInputAnimator::m_bHideName
	bool ___m_bHideName_20;
	// System.Boolean SystemInputAnimator::m_bFlash
	bool ___m_bFlash_21;

public:
	inline static int32_t get_offset_of_Successful_2() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___Successful_2)); }
	inline GameObject_t1756533147 * get_Successful_2() const { return ___Successful_2; }
	inline GameObject_t1756533147 ** get_address_of_Successful_2() { return &___Successful_2; }
	inline void set_Successful_2(GameObject_t1756533147 * value)
	{
		___Successful_2 = value;
		Il2CppCodeGenWriteBarrier(&___Successful_2, value);
	}

	inline static int32_t get_offset_of_Circles_3() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___Circles_3)); }
	inline GameObject_t1756533147 * get_Circles_3() const { return ___Circles_3; }
	inline GameObject_t1756533147 ** get_address_of_Circles_3() { return &___Circles_3; }
	inline void set_Circles_3(GameObject_t1756533147 * value)
	{
		___Circles_3 = value;
		Il2CppCodeGenWriteBarrier(&___Circles_3, value);
	}

	inline static int32_t get_offset_of_YourNameText_4() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___YourNameText_4)); }
	inline Text_t356221433 * get_YourNameText_4() const { return ___YourNameText_4; }
	inline Text_t356221433 ** get_address_of_YourNameText_4() { return &___YourNameText_4; }
	inline void set_YourNameText_4(Text_t356221433 * value)
	{
		___YourNameText_4 = value;
		Il2CppCodeGenWriteBarrier(&___YourNameText_4, value);
	}

	inline static int32_t get_offset_of_NameText_5() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___NameText_5)); }
	inline Text_t356221433 * get_NameText_5() const { return ___NameText_5; }
	inline Text_t356221433 ** get_address_of_NameText_5() { return &___NameText_5; }
	inline void set_NameText_5(Text_t356221433 * value)
	{
		___NameText_5 = value;
		Il2CppCodeGenWriteBarrier(&___NameText_5, value);
	}

	inline static int32_t get_offset_of_SuccessPanelRectTrans_6() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___SuccessPanelRectTrans_6)); }
	inline RectTransform_t3349966182 * get_SuccessPanelRectTrans_6() const { return ___SuccessPanelRectTrans_6; }
	inline RectTransform_t3349966182 ** get_address_of_SuccessPanelRectTrans_6() { return &___SuccessPanelRectTrans_6; }
	inline void set_SuccessPanelRectTrans_6(RectTransform_t3349966182 * value)
	{
		___SuccessPanelRectTrans_6 = value;
		Il2CppCodeGenWriteBarrier(&___SuccessPanelRectTrans_6, value);
	}

	inline static int32_t get_offset_of_Items_7() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___Items_7)); }
	inline InputPanelItemU5BU5D_t3786550914* get_Items_7() const { return ___Items_7; }
	inline InputPanelItemU5BU5D_t3786550914** get_address_of_Items_7() { return &___Items_7; }
	inline void set_Items_7(InputPanelItemU5BU5D_t3786550914* value)
	{
		___Items_7 = value;
		Il2CppCodeGenWriteBarrier(&___Items_7, value);
	}

	inline static int32_t get_offset_of_LoadCountAnimator_8() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___LoadCountAnimator_8)); }
	inline LoadCountAnimator_t3458511750 * get_LoadCountAnimator_8() const { return ___LoadCountAnimator_8; }
	inline LoadCountAnimator_t3458511750 ** get_address_of_LoadCountAnimator_8() { return &___LoadCountAnimator_8; }
	inline void set_LoadCountAnimator_8(LoadCountAnimator_t3458511750 * value)
	{
		___LoadCountAnimator_8 = value;
		Il2CppCodeGenWriteBarrier(&___LoadCountAnimator_8, value);
	}

	inline static int32_t get_offset_of_ShowYourNameTime_9() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___ShowYourNameTime_9)); }
	inline float get_ShowYourNameTime_9() const { return ___ShowYourNameTime_9; }
	inline float* get_address_of_ShowYourNameTime_9() { return &___ShowYourNameTime_9; }
	inline void set_ShowYourNameTime_9(float value)
	{
		___ShowYourNameTime_9 = value;
	}

	inline static int32_t get_offset_of_ShowNameTime_10() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___ShowNameTime_10)); }
	inline float get_ShowNameTime_10() const { return ___ShowNameTime_10; }
	inline float* get_address_of_ShowNameTime_10() { return &___ShowNameTime_10; }
	inline void set_ShowNameTime_10(float value)
	{
		___ShowNameTime_10 = value;
	}

	inline static int32_t get_offset_of_ShowInputPanelTime_11() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___ShowInputPanelTime_11)); }
	inline float get_ShowInputPanelTime_11() const { return ___ShowInputPanelTime_11; }
	inline float* get_address_of_ShowInputPanelTime_11() { return &___ShowInputPanelTime_11; }
	inline void set_ShowInputPanelTime_11(float value)
	{
		___ShowInputPanelTime_11 = value;
	}

	inline static int32_t get_offset_of_HideYourNameTime_12() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___HideYourNameTime_12)); }
	inline float get_HideYourNameTime_12() const { return ___HideYourNameTime_12; }
	inline float* get_address_of_HideYourNameTime_12() { return &___HideYourNameTime_12; }
	inline void set_HideYourNameTime_12(float value)
	{
		___HideYourNameTime_12 = value;
	}

	inline static int32_t get_offset_of_HideNameTime_13() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___HideNameTime_13)); }
	inline float get_HideNameTime_13() const { return ___HideNameTime_13; }
	inline float* get_address_of_HideNameTime_13() { return &___HideNameTime_13; }
	inline void set_HideNameTime_13(float value)
	{
		___HideNameTime_13 = value;
	}

	inline static int32_t get_offset_of_HideInputPanelTime_14() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___HideInputPanelTime_14)); }
	inline float get_HideInputPanelTime_14() const { return ___HideInputPanelTime_14; }
	inline float* get_address_of_HideInputPanelTime_14() { return &___HideInputPanelTime_14; }
	inline void set_HideInputPanelTime_14(float value)
	{
		___HideInputPanelTime_14 = value;
	}

	inline static int32_t get_offset_of_m_Circles_15() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___m_Circles_15)); }
	inline SystemInputCircles_t2127672316 * get_m_Circles_15() const { return ___m_Circles_15; }
	inline SystemInputCircles_t2127672316 ** get_address_of_m_Circles_15() { return &___m_Circles_15; }
	inline void set_m_Circles_15(SystemInputCircles_t2127672316 * value)
	{
		___m_Circles_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_Circles_15, value);
	}

	inline static int32_t get_offset_of_m_bShow_16() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___m_bShow_16)); }
	inline bool get_m_bShow_16() const { return ___m_bShow_16; }
	inline bool* get_address_of_m_bShow_16() { return &___m_bShow_16; }
	inline void set_m_bShow_16(bool value)
	{
		___m_bShow_16 = value;
	}

	inline static int32_t get_offset_of_m_bHide_17() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___m_bHide_17)); }
	inline bool get_m_bHide_17() const { return ___m_bHide_17; }
	inline bool* get_address_of_m_bHide_17() { return &___m_bHide_17; }
	inline void set_m_bHide_17(bool value)
	{
		___m_bHide_17 = value;
	}

	inline static int32_t get_offset_of_m_bLoading_18() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___m_bLoading_18)); }
	inline bool get_m_bLoading_18() const { return ___m_bLoading_18; }
	inline bool* get_address_of_m_bLoading_18() { return &___m_bLoading_18; }
	inline void set_m_bLoading_18(bool value)
	{
		___m_bLoading_18 = value;
	}

	inline static int32_t get_offset_of_m_bConter_19() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___m_bConter_19)); }
	inline bool get_m_bConter_19() const { return ___m_bConter_19; }
	inline bool* get_address_of_m_bConter_19() { return &___m_bConter_19; }
	inline void set_m_bConter_19(bool value)
	{
		___m_bConter_19 = value;
	}

	inline static int32_t get_offset_of_m_bHideName_20() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___m_bHideName_20)); }
	inline bool get_m_bHideName_20() const { return ___m_bHideName_20; }
	inline bool* get_address_of_m_bHideName_20() { return &___m_bHideName_20; }
	inline void set_m_bHideName_20(bool value)
	{
		___m_bHideName_20 = value;
	}

	inline static int32_t get_offset_of_m_bFlash_21() { return static_cast<int32_t>(offsetof(SystemInputAnimator_t1578019242, ___m_bFlash_21)); }
	inline bool get_m_bFlash_21() const { return ___m_bFlash_21; }
	inline bool* get_address_of_m_bFlash_21() { return &___m_bFlash_21; }
	inline void set_m_bFlash_21(bool value)
	{
		___m_bFlash_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
