﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Single[]
struct SingleU5BU5D_t577127397;
// CircleItem[]
struct CircleItemU5BU5D_t2008956368;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SystemInputCircles
struct  SystemInputCircles_t2127672316  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] SystemInputCircles::CircleObjs
	GameObjectU5BU5D_t3057952154* ___CircleObjs_2;
	// System.Single[] SystemInputCircles::CircleSpeed
	SingleU5BU5D_t577127397* ___CircleSpeed_3;
	// CircleItem[] SystemInputCircles::m_Circles
	CircleItemU5BU5D_t2008956368* ___m_Circles_4;
	// System.Boolean SystemInputCircles::m_bRotate
	bool ___m_bRotate_5;
	// System.Boolean SystemInputCircles::m_bFadeInFinish
	bool ___m_bFadeInFinish_6;

public:
	inline static int32_t get_offset_of_CircleObjs_2() { return static_cast<int32_t>(offsetof(SystemInputCircles_t2127672316, ___CircleObjs_2)); }
	inline GameObjectU5BU5D_t3057952154* get_CircleObjs_2() const { return ___CircleObjs_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_CircleObjs_2() { return &___CircleObjs_2; }
	inline void set_CircleObjs_2(GameObjectU5BU5D_t3057952154* value)
	{
		___CircleObjs_2 = value;
		Il2CppCodeGenWriteBarrier(&___CircleObjs_2, value);
	}

	inline static int32_t get_offset_of_CircleSpeed_3() { return static_cast<int32_t>(offsetof(SystemInputCircles_t2127672316, ___CircleSpeed_3)); }
	inline SingleU5BU5D_t577127397* get_CircleSpeed_3() const { return ___CircleSpeed_3; }
	inline SingleU5BU5D_t577127397** get_address_of_CircleSpeed_3() { return &___CircleSpeed_3; }
	inline void set_CircleSpeed_3(SingleU5BU5D_t577127397* value)
	{
		___CircleSpeed_3 = value;
		Il2CppCodeGenWriteBarrier(&___CircleSpeed_3, value);
	}

	inline static int32_t get_offset_of_m_Circles_4() { return static_cast<int32_t>(offsetof(SystemInputCircles_t2127672316, ___m_Circles_4)); }
	inline CircleItemU5BU5D_t2008956368* get_m_Circles_4() const { return ___m_Circles_4; }
	inline CircleItemU5BU5D_t2008956368** get_address_of_m_Circles_4() { return &___m_Circles_4; }
	inline void set_m_Circles_4(CircleItemU5BU5D_t2008956368* value)
	{
		___m_Circles_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Circles_4, value);
	}

	inline static int32_t get_offset_of_m_bRotate_5() { return static_cast<int32_t>(offsetof(SystemInputCircles_t2127672316, ___m_bRotate_5)); }
	inline bool get_m_bRotate_5() const { return ___m_bRotate_5; }
	inline bool* get_address_of_m_bRotate_5() { return &___m_bRotate_5; }
	inline void set_m_bRotate_5(bool value)
	{
		___m_bRotate_5 = value;
	}

	inline static int32_t get_offset_of_m_bFadeInFinish_6() { return static_cast<int32_t>(offsetof(SystemInputCircles_t2127672316, ___m_bFadeInFinish_6)); }
	inline bool get_m_bFadeInFinish_6() const { return ___m_bFadeInFinish_6; }
	inline bool* get_address_of_m_bFadeInFinish_6() { return &___m_bFadeInFinish_6; }
	inline void set_m_bFadeInFinish_6(bool value)
	{
		___m_bFadeInFinish_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
