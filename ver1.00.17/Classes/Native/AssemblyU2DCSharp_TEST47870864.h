﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TEST
struct  TEST_t47870864  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] TEST::ttt
	GameObjectU5BU5D_t3057952154* ___ttt_2;
	// UnityEngine.GameObject TEST::obj
	GameObject_t1756533147 * ___obj_3;
	// System.Single TEST::Nts
	float ___Nts_4;
	// System.Single TEST::Ntmm
	float ___Ntmm_5;
	// System.Single TEST::Ets
	float ___Ets_6;
	// System.Single TEST::Etmm
	float ___Etmm_7;
	// System.Int32 TEST::Ntm
	int32_t ___Ntm_8;
	// System.Int32 TEST::Etm
	int32_t ___Etm_9;
	// System.Int32 TEST::NtM
	int32_t ___NtM_10;
	// System.Int32 TEST::EtM
	int32_t ___EtM_11;
	// System.Int32 TEST::ner
	int32_t ___ner_12;
	// System.Single TEST::time
	float ___time_13;
	// System.Single TEST::times
	float ___times_14;
	// System.Single TEST::timee
	float ___timee_15;
	// System.Single TEST::timeStart
	float ___timeStart_16;
	// System.Single TEST::timeStartt
	float ___timeStartt_17;
	// System.Single TEST::timeEnd
	float ___timeEnd_18;
	// System.Int32 TEST::number
	int32_t ___number_19;

public:
	inline static int32_t get_offset_of_ttt_2() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___ttt_2)); }
	inline GameObjectU5BU5D_t3057952154* get_ttt_2() const { return ___ttt_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ttt_2() { return &___ttt_2; }
	inline void set_ttt_2(GameObjectU5BU5D_t3057952154* value)
	{
		___ttt_2 = value;
		Il2CppCodeGenWriteBarrier(&___ttt_2, value);
	}

	inline static int32_t get_offset_of_obj_3() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___obj_3)); }
	inline GameObject_t1756533147 * get_obj_3() const { return ___obj_3; }
	inline GameObject_t1756533147 ** get_address_of_obj_3() { return &___obj_3; }
	inline void set_obj_3(GameObject_t1756533147 * value)
	{
		___obj_3 = value;
		Il2CppCodeGenWriteBarrier(&___obj_3, value);
	}

	inline static int32_t get_offset_of_Nts_4() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___Nts_4)); }
	inline float get_Nts_4() const { return ___Nts_4; }
	inline float* get_address_of_Nts_4() { return &___Nts_4; }
	inline void set_Nts_4(float value)
	{
		___Nts_4 = value;
	}

	inline static int32_t get_offset_of_Ntmm_5() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___Ntmm_5)); }
	inline float get_Ntmm_5() const { return ___Ntmm_5; }
	inline float* get_address_of_Ntmm_5() { return &___Ntmm_5; }
	inline void set_Ntmm_5(float value)
	{
		___Ntmm_5 = value;
	}

	inline static int32_t get_offset_of_Ets_6() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___Ets_6)); }
	inline float get_Ets_6() const { return ___Ets_6; }
	inline float* get_address_of_Ets_6() { return &___Ets_6; }
	inline void set_Ets_6(float value)
	{
		___Ets_6 = value;
	}

	inline static int32_t get_offset_of_Etmm_7() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___Etmm_7)); }
	inline float get_Etmm_7() const { return ___Etmm_7; }
	inline float* get_address_of_Etmm_7() { return &___Etmm_7; }
	inline void set_Etmm_7(float value)
	{
		___Etmm_7 = value;
	}

	inline static int32_t get_offset_of_Ntm_8() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___Ntm_8)); }
	inline int32_t get_Ntm_8() const { return ___Ntm_8; }
	inline int32_t* get_address_of_Ntm_8() { return &___Ntm_8; }
	inline void set_Ntm_8(int32_t value)
	{
		___Ntm_8 = value;
	}

	inline static int32_t get_offset_of_Etm_9() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___Etm_9)); }
	inline int32_t get_Etm_9() const { return ___Etm_9; }
	inline int32_t* get_address_of_Etm_9() { return &___Etm_9; }
	inline void set_Etm_9(int32_t value)
	{
		___Etm_9 = value;
	}

	inline static int32_t get_offset_of_NtM_10() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___NtM_10)); }
	inline int32_t get_NtM_10() const { return ___NtM_10; }
	inline int32_t* get_address_of_NtM_10() { return &___NtM_10; }
	inline void set_NtM_10(int32_t value)
	{
		___NtM_10 = value;
	}

	inline static int32_t get_offset_of_EtM_11() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___EtM_11)); }
	inline int32_t get_EtM_11() const { return ___EtM_11; }
	inline int32_t* get_address_of_EtM_11() { return &___EtM_11; }
	inline void set_EtM_11(int32_t value)
	{
		___EtM_11 = value;
	}

	inline static int32_t get_offset_of_ner_12() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___ner_12)); }
	inline int32_t get_ner_12() const { return ___ner_12; }
	inline int32_t* get_address_of_ner_12() { return &___ner_12; }
	inline void set_ner_12(int32_t value)
	{
		___ner_12 = value;
	}

	inline static int32_t get_offset_of_time_13() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___time_13)); }
	inline float get_time_13() const { return ___time_13; }
	inline float* get_address_of_time_13() { return &___time_13; }
	inline void set_time_13(float value)
	{
		___time_13 = value;
	}

	inline static int32_t get_offset_of_times_14() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___times_14)); }
	inline float get_times_14() const { return ___times_14; }
	inline float* get_address_of_times_14() { return &___times_14; }
	inline void set_times_14(float value)
	{
		___times_14 = value;
	}

	inline static int32_t get_offset_of_timee_15() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___timee_15)); }
	inline float get_timee_15() const { return ___timee_15; }
	inline float* get_address_of_timee_15() { return &___timee_15; }
	inline void set_timee_15(float value)
	{
		___timee_15 = value;
	}

	inline static int32_t get_offset_of_timeStart_16() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___timeStart_16)); }
	inline float get_timeStart_16() const { return ___timeStart_16; }
	inline float* get_address_of_timeStart_16() { return &___timeStart_16; }
	inline void set_timeStart_16(float value)
	{
		___timeStart_16 = value;
	}

	inline static int32_t get_offset_of_timeStartt_17() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___timeStartt_17)); }
	inline float get_timeStartt_17() const { return ___timeStartt_17; }
	inline float* get_address_of_timeStartt_17() { return &___timeStartt_17; }
	inline void set_timeStartt_17(float value)
	{
		___timeStartt_17 = value;
	}

	inline static int32_t get_offset_of_timeEnd_18() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___timeEnd_18)); }
	inline float get_timeEnd_18() const { return ___timeEnd_18; }
	inline float* get_address_of_timeEnd_18() { return &___timeEnd_18; }
	inline void set_timeEnd_18(float value)
	{
		___timeEnd_18 = value;
	}

	inline static int32_t get_offset_of_number_19() { return static_cast<int32_t>(offsetof(TEST_t47870864, ___number_19)); }
	inline int32_t get_number_19() const { return ___number_19; }
	inline int32_t* get_address_of_number_19() { return &___number_19; }
	inline void set_number_19(int32_t value)
	{
		___number_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
