﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkPanel
struct  TalkPanel_t2596865392  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TalkPanel::Navit
	GameObject_t1756533147 * ___Navit_2;
	// UnityEngine.GameObject TalkPanel::TalkField
	GameObject_t1756533147 * ___TalkField_3;
	// UnityEngine.UI.Text TalkPanel::NavitName
	Text_t356221433 * ___NavitName_4;
	// UnityEngine.UI.Text TalkPanel::TalkText
	Text_t356221433 * ___TalkText_5;
	// UnityEngine.CanvasGroup TalkPanel::CanvasGroup
	CanvasGroup_t3296560743 * ___CanvasGroup_6;

public:
	inline static int32_t get_offset_of_Navit_2() { return static_cast<int32_t>(offsetof(TalkPanel_t2596865392, ___Navit_2)); }
	inline GameObject_t1756533147 * get_Navit_2() const { return ___Navit_2; }
	inline GameObject_t1756533147 ** get_address_of_Navit_2() { return &___Navit_2; }
	inline void set_Navit_2(GameObject_t1756533147 * value)
	{
		___Navit_2 = value;
		Il2CppCodeGenWriteBarrier(&___Navit_2, value);
	}

	inline static int32_t get_offset_of_TalkField_3() { return static_cast<int32_t>(offsetof(TalkPanel_t2596865392, ___TalkField_3)); }
	inline GameObject_t1756533147 * get_TalkField_3() const { return ___TalkField_3; }
	inline GameObject_t1756533147 ** get_address_of_TalkField_3() { return &___TalkField_3; }
	inline void set_TalkField_3(GameObject_t1756533147 * value)
	{
		___TalkField_3 = value;
		Il2CppCodeGenWriteBarrier(&___TalkField_3, value);
	}

	inline static int32_t get_offset_of_NavitName_4() { return static_cast<int32_t>(offsetof(TalkPanel_t2596865392, ___NavitName_4)); }
	inline Text_t356221433 * get_NavitName_4() const { return ___NavitName_4; }
	inline Text_t356221433 ** get_address_of_NavitName_4() { return &___NavitName_4; }
	inline void set_NavitName_4(Text_t356221433 * value)
	{
		___NavitName_4 = value;
		Il2CppCodeGenWriteBarrier(&___NavitName_4, value);
	}

	inline static int32_t get_offset_of_TalkText_5() { return static_cast<int32_t>(offsetof(TalkPanel_t2596865392, ___TalkText_5)); }
	inline Text_t356221433 * get_TalkText_5() const { return ___TalkText_5; }
	inline Text_t356221433 ** get_address_of_TalkText_5() { return &___TalkText_5; }
	inline void set_TalkText_5(Text_t356221433 * value)
	{
		___TalkText_5 = value;
		Il2CppCodeGenWriteBarrier(&___TalkText_5, value);
	}

	inline static int32_t get_offset_of_CanvasGroup_6() { return static_cast<int32_t>(offsetof(TalkPanel_t2596865392, ___CanvasGroup_6)); }
	inline CanvasGroup_t3296560743 * get_CanvasGroup_6() const { return ___CanvasGroup_6; }
	inline CanvasGroup_t3296560743 ** get_address_of_CanvasGroup_6() { return &___CanvasGroup_6; }
	inline void set_CanvasGroup_6(CanvasGroup_t3296560743 * value)
	{
		___CanvasGroup_6 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasGroup_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
