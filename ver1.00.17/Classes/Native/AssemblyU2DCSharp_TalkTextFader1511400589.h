﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TalkTextFader
struct  TalkTextFader_t1511400589  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TalkTextFader::IsComplete
	bool ___IsComplete_2;
	// System.Boolean TalkTextFader::m_bIsFade
	bool ___m_bIsFade_3;
	// UnityEngine.UI.Text TalkTextFader::m_Text
	Text_t356221433 * ___m_Text_4;

public:
	inline static int32_t get_offset_of_IsComplete_2() { return static_cast<int32_t>(offsetof(TalkTextFader_t1511400589, ___IsComplete_2)); }
	inline bool get_IsComplete_2() const { return ___IsComplete_2; }
	inline bool* get_address_of_IsComplete_2() { return &___IsComplete_2; }
	inline void set_IsComplete_2(bool value)
	{
		___IsComplete_2 = value;
	}

	inline static int32_t get_offset_of_m_bIsFade_3() { return static_cast<int32_t>(offsetof(TalkTextFader_t1511400589, ___m_bIsFade_3)); }
	inline bool get_m_bIsFade_3() const { return ___m_bIsFade_3; }
	inline bool* get_address_of_m_bIsFade_3() { return &___m_bIsFade_3; }
	inline void set_m_bIsFade_3(bool value)
	{
		___m_bIsFade_3 = value;
	}

	inline static int32_t get_offset_of_m_Text_4() { return static_cast<int32_t>(offsetof(TalkTextFader_t1511400589, ___m_Text_4)); }
	inline Text_t356221433 * get_m_Text_4() const { return ___m_Text_4; }
	inline Text_t356221433 ** get_address_of_m_Text_4() { return &___m_Text_4; }
	inline void set_m_Text_4(Text_t356221433 * value)
	{
		___m_Text_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_Text_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
