﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2915070953.h"

// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeleportManager
struct  TeleportManager_t4218394618  : public SingletonMonoBehaviour_1_t2915070953
{
public:
	// UnityEngine.Transform[] TeleportManager::Teleporters
	TransformU5BU5D_t3764228911* ___Teleporters_3;
	// System.Boolean TeleportManager::IsMove
	bool ___IsMove_4;

public:
	inline static int32_t get_offset_of_Teleporters_3() { return static_cast<int32_t>(offsetof(TeleportManager_t4218394618, ___Teleporters_3)); }
	inline TransformU5BU5D_t3764228911* get_Teleporters_3() const { return ___Teleporters_3; }
	inline TransformU5BU5D_t3764228911** get_address_of_Teleporters_3() { return &___Teleporters_3; }
	inline void set_Teleporters_3(TransformU5BU5D_t3764228911* value)
	{
		___Teleporters_3 = value;
		Il2CppCodeGenWriteBarrier(&___Teleporters_3, value);
	}

	inline static int32_t get_offset_of_IsMove_4() { return static_cast<int32_t>(offsetof(TeleportManager_t4218394618, ___IsMove_4)); }
	inline bool get_IsMove_4() const { return ___IsMove_4; }
	inline bool* get_address_of_IsMove_4() { return &___IsMove_4; }
	inline void set_IsMove_4(bool value)
	{
		___IsMove_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
