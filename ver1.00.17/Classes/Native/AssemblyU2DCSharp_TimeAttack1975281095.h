﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.Generic.Dictionary`2<TimeAttackPoint,UnityEngine.GameObject>
struct Dictionary_2_t2136108779;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeAttack
struct  TimeAttack_t1975281095  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> TimeAttack::CheckPointPos
	List_1_t1612828712 * ___CheckPointPos_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> TimeAttack::CheckPointList
	List_1_t1125654279 * ___CheckPointList_3;
	// UnityEngine.GameObject TimeAttack::CheckObjParent
	GameObject_t1756533147 * ___CheckObjParent_4;
	// UnityEngine.GameObject TimeAttack::PointObj
	GameObject_t1756533147 * ___PointObj_5;
	// UnityEngine.Sprite TimeAttack::PointImage
	Sprite_t309593783 * ___PointImage_6;
	// UnityEngine.Sprite TimeAttack::CheckUIOn
	Sprite_t309593783 * ___CheckUIOn_7;
	// UnityEngine.Sprite TimeAttack::CheckObjOn
	Sprite_t309593783 * ___CheckObjOn_8;
	// System.Collections.Generic.Dictionary`2<TimeAttackPoint,UnityEngine.GameObject> TimeAttack::PointUI
	Dictionary_2_t2136108779 * ___PointUI_9;
	// System.Single TimeAttack::m_sTime
	float ___m_sTime_10;
	// System.Int32 TimeAttack::m_mobilityId
	int32_t ___m_mobilityId_11;

public:
	inline static int32_t get_offset_of_CheckPointPos_2() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___CheckPointPos_2)); }
	inline List_1_t1612828712 * get_CheckPointPos_2() const { return ___CheckPointPos_2; }
	inline List_1_t1612828712 ** get_address_of_CheckPointPos_2() { return &___CheckPointPos_2; }
	inline void set_CheckPointPos_2(List_1_t1612828712 * value)
	{
		___CheckPointPos_2 = value;
		Il2CppCodeGenWriteBarrier(&___CheckPointPos_2, value);
	}

	inline static int32_t get_offset_of_CheckPointList_3() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___CheckPointList_3)); }
	inline List_1_t1125654279 * get_CheckPointList_3() const { return ___CheckPointList_3; }
	inline List_1_t1125654279 ** get_address_of_CheckPointList_3() { return &___CheckPointList_3; }
	inline void set_CheckPointList_3(List_1_t1125654279 * value)
	{
		___CheckPointList_3 = value;
		Il2CppCodeGenWriteBarrier(&___CheckPointList_3, value);
	}

	inline static int32_t get_offset_of_CheckObjParent_4() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___CheckObjParent_4)); }
	inline GameObject_t1756533147 * get_CheckObjParent_4() const { return ___CheckObjParent_4; }
	inline GameObject_t1756533147 ** get_address_of_CheckObjParent_4() { return &___CheckObjParent_4; }
	inline void set_CheckObjParent_4(GameObject_t1756533147 * value)
	{
		___CheckObjParent_4 = value;
		Il2CppCodeGenWriteBarrier(&___CheckObjParent_4, value);
	}

	inline static int32_t get_offset_of_PointObj_5() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___PointObj_5)); }
	inline GameObject_t1756533147 * get_PointObj_5() const { return ___PointObj_5; }
	inline GameObject_t1756533147 ** get_address_of_PointObj_5() { return &___PointObj_5; }
	inline void set_PointObj_5(GameObject_t1756533147 * value)
	{
		___PointObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___PointObj_5, value);
	}

	inline static int32_t get_offset_of_PointImage_6() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___PointImage_6)); }
	inline Sprite_t309593783 * get_PointImage_6() const { return ___PointImage_6; }
	inline Sprite_t309593783 ** get_address_of_PointImage_6() { return &___PointImage_6; }
	inline void set_PointImage_6(Sprite_t309593783 * value)
	{
		___PointImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___PointImage_6, value);
	}

	inline static int32_t get_offset_of_CheckUIOn_7() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___CheckUIOn_7)); }
	inline Sprite_t309593783 * get_CheckUIOn_7() const { return ___CheckUIOn_7; }
	inline Sprite_t309593783 ** get_address_of_CheckUIOn_7() { return &___CheckUIOn_7; }
	inline void set_CheckUIOn_7(Sprite_t309593783 * value)
	{
		___CheckUIOn_7 = value;
		Il2CppCodeGenWriteBarrier(&___CheckUIOn_7, value);
	}

	inline static int32_t get_offset_of_CheckObjOn_8() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___CheckObjOn_8)); }
	inline Sprite_t309593783 * get_CheckObjOn_8() const { return ___CheckObjOn_8; }
	inline Sprite_t309593783 ** get_address_of_CheckObjOn_8() { return &___CheckObjOn_8; }
	inline void set_CheckObjOn_8(Sprite_t309593783 * value)
	{
		___CheckObjOn_8 = value;
		Il2CppCodeGenWriteBarrier(&___CheckObjOn_8, value);
	}

	inline static int32_t get_offset_of_PointUI_9() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___PointUI_9)); }
	inline Dictionary_2_t2136108779 * get_PointUI_9() const { return ___PointUI_9; }
	inline Dictionary_2_t2136108779 ** get_address_of_PointUI_9() { return &___PointUI_9; }
	inline void set_PointUI_9(Dictionary_2_t2136108779 * value)
	{
		___PointUI_9 = value;
		Il2CppCodeGenWriteBarrier(&___PointUI_9, value);
	}

	inline static int32_t get_offset_of_m_sTime_10() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___m_sTime_10)); }
	inline float get_m_sTime_10() const { return ___m_sTime_10; }
	inline float* get_address_of_m_sTime_10() { return &___m_sTime_10; }
	inline void set_m_sTime_10(float value)
	{
		___m_sTime_10 = value;
	}

	inline static int32_t get_offset_of_m_mobilityId_11() { return static_cast<int32_t>(offsetof(TimeAttack_t1975281095, ___m_mobilityId_11)); }
	inline int32_t get_m_mobilityId_11() const { return ___m_mobilityId_11; }
	inline int32_t* get_address_of_m_mobilityId_11() { return &___m_mobilityId_11; }
	inline void set_m_mobilityId_11(int32_t value)
	{
		___m_mobilityId_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
