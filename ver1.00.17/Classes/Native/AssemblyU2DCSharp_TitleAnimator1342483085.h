﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TitlePanelItem[]
struct TitlePanelItemU5BU5D_t3108109364;
// LineAnimator
struct LineAnimator_t2276058919;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1967201810;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleAnimator
struct  TitleAnimator_t1342483085  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TitleAnimator::Enable
	bool ___Enable_2;
	// UnityEngine.GameObject TitleAnimator::TitleLogo
	GameObject_t1756533147 * ___TitleLogo_3;
	// TitlePanelItem[] TitleAnimator::Items
	TitlePanelItemU5BU5D_t3108109364* ___Items_4;
	// LineAnimator TitleAnimator::LineAnimator
	LineAnimator_t2276058919 * ___LineAnimator_5;
	// UnityEngine.EventSystems.EventTrigger TitleAnimator::StartButtonEvent
	EventTrigger_t1967201810 * ___StartButtonEvent_6;
	// UnityEngine.EventSystems.EventTrigger TitleAnimator::TermsOfLinkEvent
	EventTrigger_t1967201810 * ___TermsOfLinkEvent_7;
	// System.Single TitleAnimator::PlanetsTime
	float ___PlanetsTime_8;
	// System.Single TitleAnimator::TitleLogoTime
	float ___TitleLogoTime_9;
	// System.Single TitleAnimator::SubPanelTime
	float ___SubPanelTime_10;
	// System.Boolean TitleAnimator::IsComplete
	bool ___IsComplete_11;
	// UnityEngine.RectTransform TitleAnimator::m_TitleLogoRectTrans
	RectTransform_t3349966182 * ___m_TitleLogoRectTrans_12;
	// UnityEngine.UI.Image TitleAnimator::m_TitleLogo
	Image_t2042527209 * ___m_TitleLogo_13;
	// System.Boolean TitleAnimator::m_bSkip
	bool ___m_bSkip_14;

public:
	inline static int32_t get_offset_of_Enable_2() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___Enable_2)); }
	inline bool get_Enable_2() const { return ___Enable_2; }
	inline bool* get_address_of_Enable_2() { return &___Enable_2; }
	inline void set_Enable_2(bool value)
	{
		___Enable_2 = value;
	}

	inline static int32_t get_offset_of_TitleLogo_3() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___TitleLogo_3)); }
	inline GameObject_t1756533147 * get_TitleLogo_3() const { return ___TitleLogo_3; }
	inline GameObject_t1756533147 ** get_address_of_TitleLogo_3() { return &___TitleLogo_3; }
	inline void set_TitleLogo_3(GameObject_t1756533147 * value)
	{
		___TitleLogo_3 = value;
		Il2CppCodeGenWriteBarrier(&___TitleLogo_3, value);
	}

	inline static int32_t get_offset_of_Items_4() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___Items_4)); }
	inline TitlePanelItemU5BU5D_t3108109364* get_Items_4() const { return ___Items_4; }
	inline TitlePanelItemU5BU5D_t3108109364** get_address_of_Items_4() { return &___Items_4; }
	inline void set_Items_4(TitlePanelItemU5BU5D_t3108109364* value)
	{
		___Items_4 = value;
		Il2CppCodeGenWriteBarrier(&___Items_4, value);
	}

	inline static int32_t get_offset_of_LineAnimator_5() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___LineAnimator_5)); }
	inline LineAnimator_t2276058919 * get_LineAnimator_5() const { return ___LineAnimator_5; }
	inline LineAnimator_t2276058919 ** get_address_of_LineAnimator_5() { return &___LineAnimator_5; }
	inline void set_LineAnimator_5(LineAnimator_t2276058919 * value)
	{
		___LineAnimator_5 = value;
		Il2CppCodeGenWriteBarrier(&___LineAnimator_5, value);
	}

	inline static int32_t get_offset_of_StartButtonEvent_6() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___StartButtonEvent_6)); }
	inline EventTrigger_t1967201810 * get_StartButtonEvent_6() const { return ___StartButtonEvent_6; }
	inline EventTrigger_t1967201810 ** get_address_of_StartButtonEvent_6() { return &___StartButtonEvent_6; }
	inline void set_StartButtonEvent_6(EventTrigger_t1967201810 * value)
	{
		___StartButtonEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___StartButtonEvent_6, value);
	}

	inline static int32_t get_offset_of_TermsOfLinkEvent_7() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___TermsOfLinkEvent_7)); }
	inline EventTrigger_t1967201810 * get_TermsOfLinkEvent_7() const { return ___TermsOfLinkEvent_7; }
	inline EventTrigger_t1967201810 ** get_address_of_TermsOfLinkEvent_7() { return &___TermsOfLinkEvent_7; }
	inline void set_TermsOfLinkEvent_7(EventTrigger_t1967201810 * value)
	{
		___TermsOfLinkEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___TermsOfLinkEvent_7, value);
	}

	inline static int32_t get_offset_of_PlanetsTime_8() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___PlanetsTime_8)); }
	inline float get_PlanetsTime_8() const { return ___PlanetsTime_8; }
	inline float* get_address_of_PlanetsTime_8() { return &___PlanetsTime_8; }
	inline void set_PlanetsTime_8(float value)
	{
		___PlanetsTime_8 = value;
	}

	inline static int32_t get_offset_of_TitleLogoTime_9() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___TitleLogoTime_9)); }
	inline float get_TitleLogoTime_9() const { return ___TitleLogoTime_9; }
	inline float* get_address_of_TitleLogoTime_9() { return &___TitleLogoTime_9; }
	inline void set_TitleLogoTime_9(float value)
	{
		___TitleLogoTime_9 = value;
	}

	inline static int32_t get_offset_of_SubPanelTime_10() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___SubPanelTime_10)); }
	inline float get_SubPanelTime_10() const { return ___SubPanelTime_10; }
	inline float* get_address_of_SubPanelTime_10() { return &___SubPanelTime_10; }
	inline void set_SubPanelTime_10(float value)
	{
		___SubPanelTime_10 = value;
	}

	inline static int32_t get_offset_of_IsComplete_11() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___IsComplete_11)); }
	inline bool get_IsComplete_11() const { return ___IsComplete_11; }
	inline bool* get_address_of_IsComplete_11() { return &___IsComplete_11; }
	inline void set_IsComplete_11(bool value)
	{
		___IsComplete_11 = value;
	}

	inline static int32_t get_offset_of_m_TitleLogoRectTrans_12() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___m_TitleLogoRectTrans_12)); }
	inline RectTransform_t3349966182 * get_m_TitleLogoRectTrans_12() const { return ___m_TitleLogoRectTrans_12; }
	inline RectTransform_t3349966182 ** get_address_of_m_TitleLogoRectTrans_12() { return &___m_TitleLogoRectTrans_12; }
	inline void set_m_TitleLogoRectTrans_12(RectTransform_t3349966182 * value)
	{
		___m_TitleLogoRectTrans_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_TitleLogoRectTrans_12, value);
	}

	inline static int32_t get_offset_of_m_TitleLogo_13() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___m_TitleLogo_13)); }
	inline Image_t2042527209 * get_m_TitleLogo_13() const { return ___m_TitleLogo_13; }
	inline Image_t2042527209 ** get_address_of_m_TitleLogo_13() { return &___m_TitleLogo_13; }
	inline void set_m_TitleLogo_13(Image_t2042527209 * value)
	{
		___m_TitleLogo_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_TitleLogo_13, value);
	}

	inline static int32_t get_offset_of_m_bSkip_14() { return static_cast<int32_t>(offsetof(TitleAnimator_t1342483085, ___m_bSkip_14)); }
	inline bool get_m_bSkip_14() const { return ___m_bSkip_14; }
	inline bool* get_address_of_m_bSkip_14() { return &___m_bSkip_14; }
	inline void set_m_bSkip_14(bool value)
	{
		___m_bSkip_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
