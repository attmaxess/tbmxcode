﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// TitleAnimator
struct TitleAnimator_t1342483085;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleLogin
struct  TitleLogin_t2728597065  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TitleLogin::IsLoginCheck
	bool ___IsLoginCheck_2;
	// TitleAnimator TitleLogin::m_TitleAnimator
	TitleAnimator_t1342483085 * ___m_TitleAnimator_3;

public:
	inline static int32_t get_offset_of_IsLoginCheck_2() { return static_cast<int32_t>(offsetof(TitleLogin_t2728597065, ___IsLoginCheck_2)); }
	inline bool get_IsLoginCheck_2() const { return ___IsLoginCheck_2; }
	inline bool* get_address_of_IsLoginCheck_2() { return &___IsLoginCheck_2; }
	inline void set_IsLoginCheck_2(bool value)
	{
		___IsLoginCheck_2 = value;
	}

	inline static int32_t get_offset_of_m_TitleAnimator_3() { return static_cast<int32_t>(offsetof(TitleLogin_t2728597065, ___m_TitleAnimator_3)); }
	inline TitleAnimator_t1342483085 * get_m_TitleAnimator_3() const { return ___m_TitleAnimator_3; }
	inline TitleAnimator_t1342483085 ** get_address_of_m_TitleAnimator_3() { return &___m_TitleAnimator_3; }
	inline void set_m_TitleAnimator_3(TitleAnimator_t1342483085 * value)
	{
		___m_TitleAnimator_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_TitleAnimator_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
