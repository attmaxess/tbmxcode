﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitlePanelItem
struct  TitlePanelItem_t1000476553  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TitlePanelItem::IsComplete
	bool ___IsComplete_2;
	// UnityEngine.CanvasGroup TitlePanelItem::m_CanvasGroup
	CanvasGroup_t3296560743 * ___m_CanvasGroup_3;
	// UnityEngine.RectTransform TitlePanelItem::m_RectTrans
	RectTransform_t3349966182 * ___m_RectTrans_4;

public:
	inline static int32_t get_offset_of_IsComplete_2() { return static_cast<int32_t>(offsetof(TitlePanelItem_t1000476553, ___IsComplete_2)); }
	inline bool get_IsComplete_2() const { return ___IsComplete_2; }
	inline bool* get_address_of_IsComplete_2() { return &___IsComplete_2; }
	inline void set_IsComplete_2(bool value)
	{
		___IsComplete_2 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroup_3() { return static_cast<int32_t>(offsetof(TitlePanelItem_t1000476553, ___m_CanvasGroup_3)); }
	inline CanvasGroup_t3296560743 * get_m_CanvasGroup_3() const { return ___m_CanvasGroup_3; }
	inline CanvasGroup_t3296560743 ** get_address_of_m_CanvasGroup_3() { return &___m_CanvasGroup_3; }
	inline void set_m_CanvasGroup_3(CanvasGroup_t3296560743 * value)
	{
		___m_CanvasGroup_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_CanvasGroup_3, value);
	}

	inline static int32_t get_offset_of_m_RectTrans_4() { return static_cast<int32_t>(offsetof(TitlePanelItem_t1000476553, ___m_RectTrans_4)); }
	inline RectTransform_t3349966182 * get_m_RectTrans_4() const { return ___m_RectTrans_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTrans_4() { return &___m_RectTrans_4; }
	inline void set_m_RectTrans_4(RectTransform_t3349966182 * value)
	{
		___m_RectTrans_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTrans_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
