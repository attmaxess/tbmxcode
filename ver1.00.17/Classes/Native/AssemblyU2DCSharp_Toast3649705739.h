﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Toast
struct  Toast_t3649705739  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Toast::ShowSecond
	float ___ShowSecond_2;
	// System.String Toast::Message
	String_t* ___Message_3;
	// System.Single Toast::m_fTimeElapsed
	float ___m_fTimeElapsed_4;

public:
	inline static int32_t get_offset_of_ShowSecond_2() { return static_cast<int32_t>(offsetof(Toast_t3649705739, ___ShowSecond_2)); }
	inline float get_ShowSecond_2() const { return ___ShowSecond_2; }
	inline float* get_address_of_ShowSecond_2() { return &___ShowSecond_2; }
	inline void set_ShowSecond_2(float value)
	{
		___ShowSecond_2 = value;
	}

	inline static int32_t get_offset_of_Message_3() { return static_cast<int32_t>(offsetof(Toast_t3649705739, ___Message_3)); }
	inline String_t* get_Message_3() const { return ___Message_3; }
	inline String_t** get_address_of_Message_3() { return &___Message_3; }
	inline void set_Message_3(String_t* value)
	{
		___Message_3 = value;
		Il2CppCodeGenWriteBarrier(&___Message_3, value);
	}

	inline static int32_t get_offset_of_m_fTimeElapsed_4() { return static_cast<int32_t>(offsetof(Toast_t3649705739, ___m_fTimeElapsed_4)); }
	inline float get_m_fTimeElapsed_4() const { return ___m_fTimeElapsed_4; }
	inline float* get_address_of_m_fTimeElapsed_4() { return &___m_fTimeElapsed_4; }
	inline void set_m_fTimeElapsed_4(float value)
	{
		___m_fTimeElapsed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
