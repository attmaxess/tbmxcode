﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityStandardAssets.CrossPlatformInput.Joystick
struct Joystick_t2144252492;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnStick
struct  TurnStick_t3528168681  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TurnStick::TurnUI
	Transform_t3275118058 * ___TurnUI_2;
	// UnityEngine.Vector2 TurnStick::m_vStartPos
	Vector2_t2243707579  ___m_vStartPos_3;
	// UnityStandardAssets.CrossPlatformInput.Joystick TurnStick::m_TurnStick
	Joystick_t2144252492 * ___m_TurnStick_4;
	// System.Boolean TurnStick::m_bTurnButton
	bool ___m_bTurnButton_5;

public:
	inline static int32_t get_offset_of_TurnUI_2() { return static_cast<int32_t>(offsetof(TurnStick_t3528168681, ___TurnUI_2)); }
	inline Transform_t3275118058 * get_TurnUI_2() const { return ___TurnUI_2; }
	inline Transform_t3275118058 ** get_address_of_TurnUI_2() { return &___TurnUI_2; }
	inline void set_TurnUI_2(Transform_t3275118058 * value)
	{
		___TurnUI_2 = value;
		Il2CppCodeGenWriteBarrier(&___TurnUI_2, value);
	}

	inline static int32_t get_offset_of_m_vStartPos_3() { return static_cast<int32_t>(offsetof(TurnStick_t3528168681, ___m_vStartPos_3)); }
	inline Vector2_t2243707579  get_m_vStartPos_3() const { return ___m_vStartPos_3; }
	inline Vector2_t2243707579 * get_address_of_m_vStartPos_3() { return &___m_vStartPos_3; }
	inline void set_m_vStartPos_3(Vector2_t2243707579  value)
	{
		___m_vStartPos_3 = value;
	}

	inline static int32_t get_offset_of_m_TurnStick_4() { return static_cast<int32_t>(offsetof(TurnStick_t3528168681, ___m_TurnStick_4)); }
	inline Joystick_t2144252492 * get_m_TurnStick_4() const { return ___m_TurnStick_4; }
	inline Joystick_t2144252492 ** get_address_of_m_TurnStick_4() { return &___m_TurnStick_4; }
	inline void set_m_TurnStick_4(Joystick_t2144252492 * value)
	{
		___m_TurnStick_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_TurnStick_4, value);
	}

	inline static int32_t get_offset_of_m_bTurnButton_5() { return static_cast<int32_t>(offsetof(TurnStick_t3528168681, ___m_bTurnButton_5)); }
	inline bool get_m_bTurnButton_5() const { return ___m_bTurnButton_5; }
	inline bool* get_address_of_m_bTurnButton_5() { return &___m_bTurnButton_5; }
	inline void set_m_bTurnButton_5(bool value)
	{
		___m_bTurnButton_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
