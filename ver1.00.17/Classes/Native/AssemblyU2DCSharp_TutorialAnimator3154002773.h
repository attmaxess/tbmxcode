﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// HelloWorldPanel
struct HelloWorldPanel_t280472022;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialAnimator
struct  TutorialAnimator_t3154002773  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TutorialAnimator::Circles
	GameObject_t1756533147 * ___Circles_2;
	// UnityEngine.GameObject TutorialAnimator::HelloWorldPanel
	GameObject_t1756533147 * ___HelloWorldPanel_3;
	// UnityEngine.GameObject TutorialAnimator::TalkPanel
	GameObject_t1756533147 * ___TalkPanel_4;
	// System.Single TutorialAnimator::FrameTime
	float ___FrameTime_5;
	// HelloWorldPanel TutorialAnimator::m_HelloWorldPanel
	HelloWorldPanel_t280472022 * ___m_HelloWorldPanel_6;
	// System.Boolean TutorialAnimator::m_bStart
	bool ___m_bStart_7;
	// System.Boolean TutorialAnimator::m_bIsAnimation
	bool ___m_bIsAnimation_8;

public:
	inline static int32_t get_offset_of_Circles_2() { return static_cast<int32_t>(offsetof(TutorialAnimator_t3154002773, ___Circles_2)); }
	inline GameObject_t1756533147 * get_Circles_2() const { return ___Circles_2; }
	inline GameObject_t1756533147 ** get_address_of_Circles_2() { return &___Circles_2; }
	inline void set_Circles_2(GameObject_t1756533147 * value)
	{
		___Circles_2 = value;
		Il2CppCodeGenWriteBarrier(&___Circles_2, value);
	}

	inline static int32_t get_offset_of_HelloWorldPanel_3() { return static_cast<int32_t>(offsetof(TutorialAnimator_t3154002773, ___HelloWorldPanel_3)); }
	inline GameObject_t1756533147 * get_HelloWorldPanel_3() const { return ___HelloWorldPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_HelloWorldPanel_3() { return &___HelloWorldPanel_3; }
	inline void set_HelloWorldPanel_3(GameObject_t1756533147 * value)
	{
		___HelloWorldPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___HelloWorldPanel_3, value);
	}

	inline static int32_t get_offset_of_TalkPanel_4() { return static_cast<int32_t>(offsetof(TutorialAnimator_t3154002773, ___TalkPanel_4)); }
	inline GameObject_t1756533147 * get_TalkPanel_4() const { return ___TalkPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_TalkPanel_4() { return &___TalkPanel_4; }
	inline void set_TalkPanel_4(GameObject_t1756533147 * value)
	{
		___TalkPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___TalkPanel_4, value);
	}

	inline static int32_t get_offset_of_FrameTime_5() { return static_cast<int32_t>(offsetof(TutorialAnimator_t3154002773, ___FrameTime_5)); }
	inline float get_FrameTime_5() const { return ___FrameTime_5; }
	inline float* get_address_of_FrameTime_5() { return &___FrameTime_5; }
	inline void set_FrameTime_5(float value)
	{
		___FrameTime_5 = value;
	}

	inline static int32_t get_offset_of_m_HelloWorldPanel_6() { return static_cast<int32_t>(offsetof(TutorialAnimator_t3154002773, ___m_HelloWorldPanel_6)); }
	inline HelloWorldPanel_t280472022 * get_m_HelloWorldPanel_6() const { return ___m_HelloWorldPanel_6; }
	inline HelloWorldPanel_t280472022 ** get_address_of_m_HelloWorldPanel_6() { return &___m_HelloWorldPanel_6; }
	inline void set_m_HelloWorldPanel_6(HelloWorldPanel_t280472022 * value)
	{
		___m_HelloWorldPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_HelloWorldPanel_6, value);
	}

	inline static int32_t get_offset_of_m_bStart_7() { return static_cast<int32_t>(offsetof(TutorialAnimator_t3154002773, ___m_bStart_7)); }
	inline bool get_m_bStart_7() const { return ___m_bStart_7; }
	inline bool* get_address_of_m_bStart_7() { return &___m_bStart_7; }
	inline void set_m_bStart_7(bool value)
	{
		___m_bStart_7 = value;
	}

	inline static int32_t get_offset_of_m_bIsAnimation_8() { return static_cast<int32_t>(offsetof(TutorialAnimator_t3154002773, ___m_bIsAnimation_8)); }
	inline bool get_m_bIsAnimation_8() const { return ___m_bIsAnimation_8; }
	inline bool* get_address_of_m_bIsAnimation_8() { return &___m_bIsAnimation_8; }
	inline void set_m_bIsAnimation_8(bool value)
	{
		___m_bIsAnimation_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
