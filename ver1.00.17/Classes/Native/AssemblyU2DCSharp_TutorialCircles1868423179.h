﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCircles
struct  TutorialCircles_t1868423179  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] TutorialCircles::CircleObjs
	GameObjectU5BU5D_t3057952154* ___CircleObjs_2;
	// System.Single[] TutorialCircles::CircleSpeed
	SingleU5BU5D_t577127397* ___CircleSpeed_3;

public:
	inline static int32_t get_offset_of_CircleObjs_2() { return static_cast<int32_t>(offsetof(TutorialCircles_t1868423179, ___CircleObjs_2)); }
	inline GameObjectU5BU5D_t3057952154* get_CircleObjs_2() const { return ___CircleObjs_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_CircleObjs_2() { return &___CircleObjs_2; }
	inline void set_CircleObjs_2(GameObjectU5BU5D_t3057952154* value)
	{
		___CircleObjs_2 = value;
		Il2CppCodeGenWriteBarrier(&___CircleObjs_2, value);
	}

	inline static int32_t get_offset_of_CircleSpeed_3() { return static_cast<int32_t>(offsetof(TutorialCircles_t1868423179, ___CircleSpeed_3)); }
	inline SingleU5BU5D_t577127397* get_CircleSpeed_3() const { return ___CircleSpeed_3; }
	inline SingleU5BU5D_t577127397** get_address_of_CircleSpeed_3() { return &___CircleSpeed_3; }
	inline void set_CircleSpeed_3(SingleU5BU5D_t577127397* value)
	{
		___CircleSpeed_3 = value;
		Il2CppCodeGenWriteBarrier(&___CircleSpeed_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
