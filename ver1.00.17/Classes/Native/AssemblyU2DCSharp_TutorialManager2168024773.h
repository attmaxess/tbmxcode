﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen864701108.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TalkPanel
struct TalkPanel_t2596865392;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// DG.Tweening.Tween
struct Tween_t278478013;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t410733016;
// CoreSelect
struct CoreSelect_t1755339513;
// MobilimoPartsConect
struct MobilimoPartsConect_t3466715044;
// MoveSet
struct MoveSet_t2777185973;
// WorldContents
struct WorldContents_t112941966;
// RecordMobility
struct RecordMobility_t2685217900;
// MobilmoAction
struct MobilmoAction_t109766523;
// DemoWorld
struct DemoWorld_t457704051;
// TutorialMobilmoSelect
struct TutorialMobilmoSelect_t578122297;
// PartsScroll
struct PartsScroll_t2740354319;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// MobilmoDnaController
struct MobilmoDnaController_t1006271200;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action[]
struct ActionU5BU5D_t87223449;
// TalkTextFader
struct TalkTextFader_t1511400589;
// System.String
struct String_t;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager
struct  TutorialManager_t2168024773  : public SingletonMonoBehaviour_1_t864701108
{
public:
	// System.Boolean TutorialManager::m_bIsTutorial
	bool ___m_bIsTutorial_3;
	// System.Boolean TutorialManager::m_bIsCreateTutorial
	bool ___m_bIsCreateTutorial_4;
	// System.Boolean TutorialManager::m_bIsSkip
	bool ___m_bIsSkip_5;
	// System.Boolean TutorialManager::m_bEndTutorial
	bool ___m_bEndTutorial_6;
	// System.Boolean TutorialManager::m_bEndCreateTutorial
	bool ___m_bEndCreateTutorial_7;
	// System.Boolean TutorialManager::m_bEndTutorialAction
	bool ___m_bEndTutorialAction_8;
	// System.Int32 TutorialManager::m_sTutorialNum
	int32_t ___m_sTutorialNum_9;
	// UnityEngine.GameObject TutorialManager::TalkPanelObj
	GameObject_t1756533147 * ___TalkPanelObj_10;
	// TalkPanel TutorialManager::m_TalkPanel
	TalkPanel_t2596865392 * ___m_TalkPanel_11;
	// UnityEngine.UI.Button TutorialManager::talkButton
	Button_t2872111280 * ___talkButton_12;
	// UnityEngine.UI.Text TutorialManager::TalkText
	Text_t356221433 * ___TalkText_13;
	// UnityEngine.GameObject TutorialManager::talkNextButton
	GameObject_t1756533147 * ___talkNextButton_14;
	// System.Boolean TutorialManager::DebugTutorialOFF
	bool ___DebugTutorialOFF_15;
	// System.Boolean TutorialManager::myRecordOn
	bool ___myRecordOn_16;
	// UnityEngine.TextAsset TutorialManager::DummyStartRecord
	TextAsset_t3973159845 * ___DummyStartRecord_17;
	// UnityEngine.TextAsset TutorialManager::DummyRecord
	TextAsset_t3973159845 * ___DummyRecord_18;
	// UnityEngine.RectTransform TutorialManager::CircleRectTran
	RectTransform_t3349966182 * ___CircleRectTran_19;
	// DG.Tweening.Tween TutorialManager::circleTw
	Tween_t278478013 * ___circleTw_20;
	// System.Collections.IEnumerator TutorialManager::circleCoroutine
	Il2CppObject * ___circleCoroutine_21;
	// UnityEngine.RectTransform TutorialManager::FingerRectTran
	RectTransform_t3349966182 * ___FingerRectTran_22;
	// DG.Tweening.Tween TutorialManager::fingerTw
	Tween_t278478013 * ___fingerTw_23;
	// UnityEngine.GameObject TutorialManager::MobilmoArea
	GameObject_t1756533147 * ___MobilmoArea_24;
	// UnityEngine.UI.GraphicRaycaster TutorialManager::t_CanvasRayCaster
	GraphicRaycaster_t410733016 * ___t_CanvasRayCaster_25;
	// CoreSelect TutorialManager::t_CoreSelect
	CoreSelect_t1755339513 * ___t_CoreSelect_26;
	// MobilimoPartsConect TutorialManager::t_PartsConect
	MobilimoPartsConect_t3466715044 * ___t_PartsConect_27;
	// MoveSet TutorialManager::t_MoveSet
	MoveSet_t2777185973 * ___t_MoveSet_28;
	// WorldContents TutorialManager::t_WorldContents
	WorldContents_t112941966 * ___t_WorldContents_29;
	// RecordMobility TutorialManager::t_RecodeMobility
	RecordMobility_t2685217900 * ___t_RecodeMobility_30;
	// MobilmoAction TutorialManager::t_MobilmoAction
	MobilmoAction_t109766523 * ___t_MobilmoAction_31;
	// DemoWorld TutorialManager::t_DemoWorld
	DemoWorld_t457704051 * ___t_DemoWorld_32;
	// TutorialMobilmoSelect TutorialManager::t_MobiSele
	TutorialMobilmoSelect_t578122297 * ___t_MobiSele_33;
	// PartsScroll TutorialManager::t_PartsScroll
	PartsScroll_t2740354319 * ___t_PartsScroll_34;
	// UnityEngine.UI.ScrollRect TutorialManager::PartsListScroll
	ScrollRect_t1199013257 * ___PartsListScroll_35;
	// UnityEngine.RectTransform TutorialManager::partsListButtonTran
	RectTransform_t3349966182 * ___partsListButtonTran_36;
	// UnityEngine.RectTransform TutorialManager::PartsConfigButton
	RectTransform_t3349966182 * ___PartsConfigButton_37;
	// UnityEngine.RectTransform TutorialManager::modelingCompleteButton
	RectTransform_t3349966182 * ___modelingCompleteButton_38;
	// UnityEngine.RectTransform TutorialManager::createButton
	RectTransform_t3349966182 * ___createButton_39;
	// UnityEngine.RectTransform TutorialManager::WorldButton
	RectTransform_t3349966182 * ___WorldButton_40;
	// UnityEngine.RectTransform TutorialManager::LeverX
	RectTransform_t3349966182 * ___LeverX_41;
	// UnityEngine.RectTransform TutorialManager::RollControl
	RectTransform_t3349966182 * ___RollControl_42;
	// UnityEngine.RectTransform TutorialManager::RecButton
	RectTransform_t3349966182 * ___RecButton_43;
	// UnityEngine.GameObject TutorialManager::SkipButton
	GameObject_t1756533147 * ___SkipButton_44;
	// UnityEngine.RectTransform TutorialManager::NomalTabButton
	RectTransform_t3349966182 * ___NomalTabButton_45;
	// MobilmoDnaController TutorialManager::mobilmoDnaCon
	MobilmoDnaController_t1006271200 * ___mobilmoDnaCon_46;
	// System.Int32 TutorialManager::m_sWardsNum
	int32_t ___m_sWardsNum_47;
	// System.String[] TutorialManager::m_pTalkWards
	StringU5BU5D_t1642385972* ___m_pTalkWards_48;
	// System.Action[] TutorialManager::TutrialAction
	ActionU5BU5D_t87223449* ___TutrialAction_49;
	// TalkTextFader TutorialManager::m_TalkFader
	TalkTextFader_t1511400589 * ___m_TalkFader_50;
	// System.String[] TutorialManager::m_pSerifs
	StringU5BU5D_t1642385972* ___m_pSerifs_51;
	// System.String TutorialManager::m_pSerifsMobilmo
	String_t* ___m_pSerifsMobilmo_52;
	// System.String TutorialManager::m_pSerifsAction
	String_t* ___m_pSerifsAction_53;
	// System.Int32 TutorialManager::VoiceNum
	int32_t ___VoiceNum_54;
	// System.Single TutorialManager::MoveDis
	float ___MoveDis_55;

public:
	inline static int32_t get_offset_of_m_bIsTutorial_3() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_bIsTutorial_3)); }
	inline bool get_m_bIsTutorial_3() const { return ___m_bIsTutorial_3; }
	inline bool* get_address_of_m_bIsTutorial_3() { return &___m_bIsTutorial_3; }
	inline void set_m_bIsTutorial_3(bool value)
	{
		___m_bIsTutorial_3 = value;
	}

	inline static int32_t get_offset_of_m_bIsCreateTutorial_4() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_bIsCreateTutorial_4)); }
	inline bool get_m_bIsCreateTutorial_4() const { return ___m_bIsCreateTutorial_4; }
	inline bool* get_address_of_m_bIsCreateTutorial_4() { return &___m_bIsCreateTutorial_4; }
	inline void set_m_bIsCreateTutorial_4(bool value)
	{
		___m_bIsCreateTutorial_4 = value;
	}

	inline static int32_t get_offset_of_m_bIsSkip_5() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_bIsSkip_5)); }
	inline bool get_m_bIsSkip_5() const { return ___m_bIsSkip_5; }
	inline bool* get_address_of_m_bIsSkip_5() { return &___m_bIsSkip_5; }
	inline void set_m_bIsSkip_5(bool value)
	{
		___m_bIsSkip_5 = value;
	}

	inline static int32_t get_offset_of_m_bEndTutorial_6() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_bEndTutorial_6)); }
	inline bool get_m_bEndTutorial_6() const { return ___m_bEndTutorial_6; }
	inline bool* get_address_of_m_bEndTutorial_6() { return &___m_bEndTutorial_6; }
	inline void set_m_bEndTutorial_6(bool value)
	{
		___m_bEndTutorial_6 = value;
	}

	inline static int32_t get_offset_of_m_bEndCreateTutorial_7() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_bEndCreateTutorial_7)); }
	inline bool get_m_bEndCreateTutorial_7() const { return ___m_bEndCreateTutorial_7; }
	inline bool* get_address_of_m_bEndCreateTutorial_7() { return &___m_bEndCreateTutorial_7; }
	inline void set_m_bEndCreateTutorial_7(bool value)
	{
		___m_bEndCreateTutorial_7 = value;
	}

	inline static int32_t get_offset_of_m_bEndTutorialAction_8() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_bEndTutorialAction_8)); }
	inline bool get_m_bEndTutorialAction_8() const { return ___m_bEndTutorialAction_8; }
	inline bool* get_address_of_m_bEndTutorialAction_8() { return &___m_bEndTutorialAction_8; }
	inline void set_m_bEndTutorialAction_8(bool value)
	{
		___m_bEndTutorialAction_8 = value;
	}

	inline static int32_t get_offset_of_m_sTutorialNum_9() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_sTutorialNum_9)); }
	inline int32_t get_m_sTutorialNum_9() const { return ___m_sTutorialNum_9; }
	inline int32_t* get_address_of_m_sTutorialNum_9() { return &___m_sTutorialNum_9; }
	inline void set_m_sTutorialNum_9(int32_t value)
	{
		___m_sTutorialNum_9 = value;
	}

	inline static int32_t get_offset_of_TalkPanelObj_10() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___TalkPanelObj_10)); }
	inline GameObject_t1756533147 * get_TalkPanelObj_10() const { return ___TalkPanelObj_10; }
	inline GameObject_t1756533147 ** get_address_of_TalkPanelObj_10() { return &___TalkPanelObj_10; }
	inline void set_TalkPanelObj_10(GameObject_t1756533147 * value)
	{
		___TalkPanelObj_10 = value;
		Il2CppCodeGenWriteBarrier(&___TalkPanelObj_10, value);
	}

	inline static int32_t get_offset_of_m_TalkPanel_11() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_TalkPanel_11)); }
	inline TalkPanel_t2596865392 * get_m_TalkPanel_11() const { return ___m_TalkPanel_11; }
	inline TalkPanel_t2596865392 ** get_address_of_m_TalkPanel_11() { return &___m_TalkPanel_11; }
	inline void set_m_TalkPanel_11(TalkPanel_t2596865392 * value)
	{
		___m_TalkPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_TalkPanel_11, value);
	}

	inline static int32_t get_offset_of_talkButton_12() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___talkButton_12)); }
	inline Button_t2872111280 * get_talkButton_12() const { return ___talkButton_12; }
	inline Button_t2872111280 ** get_address_of_talkButton_12() { return &___talkButton_12; }
	inline void set_talkButton_12(Button_t2872111280 * value)
	{
		___talkButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___talkButton_12, value);
	}

	inline static int32_t get_offset_of_TalkText_13() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___TalkText_13)); }
	inline Text_t356221433 * get_TalkText_13() const { return ___TalkText_13; }
	inline Text_t356221433 ** get_address_of_TalkText_13() { return &___TalkText_13; }
	inline void set_TalkText_13(Text_t356221433 * value)
	{
		___TalkText_13 = value;
		Il2CppCodeGenWriteBarrier(&___TalkText_13, value);
	}

	inline static int32_t get_offset_of_talkNextButton_14() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___talkNextButton_14)); }
	inline GameObject_t1756533147 * get_talkNextButton_14() const { return ___talkNextButton_14; }
	inline GameObject_t1756533147 ** get_address_of_talkNextButton_14() { return &___talkNextButton_14; }
	inline void set_talkNextButton_14(GameObject_t1756533147 * value)
	{
		___talkNextButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___talkNextButton_14, value);
	}

	inline static int32_t get_offset_of_DebugTutorialOFF_15() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___DebugTutorialOFF_15)); }
	inline bool get_DebugTutorialOFF_15() const { return ___DebugTutorialOFF_15; }
	inline bool* get_address_of_DebugTutorialOFF_15() { return &___DebugTutorialOFF_15; }
	inline void set_DebugTutorialOFF_15(bool value)
	{
		___DebugTutorialOFF_15 = value;
	}

	inline static int32_t get_offset_of_myRecordOn_16() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___myRecordOn_16)); }
	inline bool get_myRecordOn_16() const { return ___myRecordOn_16; }
	inline bool* get_address_of_myRecordOn_16() { return &___myRecordOn_16; }
	inline void set_myRecordOn_16(bool value)
	{
		___myRecordOn_16 = value;
	}

	inline static int32_t get_offset_of_DummyStartRecord_17() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___DummyStartRecord_17)); }
	inline TextAsset_t3973159845 * get_DummyStartRecord_17() const { return ___DummyStartRecord_17; }
	inline TextAsset_t3973159845 ** get_address_of_DummyStartRecord_17() { return &___DummyStartRecord_17; }
	inline void set_DummyStartRecord_17(TextAsset_t3973159845 * value)
	{
		___DummyStartRecord_17 = value;
		Il2CppCodeGenWriteBarrier(&___DummyStartRecord_17, value);
	}

	inline static int32_t get_offset_of_DummyRecord_18() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___DummyRecord_18)); }
	inline TextAsset_t3973159845 * get_DummyRecord_18() const { return ___DummyRecord_18; }
	inline TextAsset_t3973159845 ** get_address_of_DummyRecord_18() { return &___DummyRecord_18; }
	inline void set_DummyRecord_18(TextAsset_t3973159845 * value)
	{
		___DummyRecord_18 = value;
		Il2CppCodeGenWriteBarrier(&___DummyRecord_18, value);
	}

	inline static int32_t get_offset_of_CircleRectTran_19() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___CircleRectTran_19)); }
	inline RectTransform_t3349966182 * get_CircleRectTran_19() const { return ___CircleRectTran_19; }
	inline RectTransform_t3349966182 ** get_address_of_CircleRectTran_19() { return &___CircleRectTran_19; }
	inline void set_CircleRectTran_19(RectTransform_t3349966182 * value)
	{
		___CircleRectTran_19 = value;
		Il2CppCodeGenWriteBarrier(&___CircleRectTran_19, value);
	}

	inline static int32_t get_offset_of_circleTw_20() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___circleTw_20)); }
	inline Tween_t278478013 * get_circleTw_20() const { return ___circleTw_20; }
	inline Tween_t278478013 ** get_address_of_circleTw_20() { return &___circleTw_20; }
	inline void set_circleTw_20(Tween_t278478013 * value)
	{
		___circleTw_20 = value;
		Il2CppCodeGenWriteBarrier(&___circleTw_20, value);
	}

	inline static int32_t get_offset_of_circleCoroutine_21() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___circleCoroutine_21)); }
	inline Il2CppObject * get_circleCoroutine_21() const { return ___circleCoroutine_21; }
	inline Il2CppObject ** get_address_of_circleCoroutine_21() { return &___circleCoroutine_21; }
	inline void set_circleCoroutine_21(Il2CppObject * value)
	{
		___circleCoroutine_21 = value;
		Il2CppCodeGenWriteBarrier(&___circleCoroutine_21, value);
	}

	inline static int32_t get_offset_of_FingerRectTran_22() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___FingerRectTran_22)); }
	inline RectTransform_t3349966182 * get_FingerRectTran_22() const { return ___FingerRectTran_22; }
	inline RectTransform_t3349966182 ** get_address_of_FingerRectTran_22() { return &___FingerRectTran_22; }
	inline void set_FingerRectTran_22(RectTransform_t3349966182 * value)
	{
		___FingerRectTran_22 = value;
		Il2CppCodeGenWriteBarrier(&___FingerRectTran_22, value);
	}

	inline static int32_t get_offset_of_fingerTw_23() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___fingerTw_23)); }
	inline Tween_t278478013 * get_fingerTw_23() const { return ___fingerTw_23; }
	inline Tween_t278478013 ** get_address_of_fingerTw_23() { return &___fingerTw_23; }
	inline void set_fingerTw_23(Tween_t278478013 * value)
	{
		___fingerTw_23 = value;
		Il2CppCodeGenWriteBarrier(&___fingerTw_23, value);
	}

	inline static int32_t get_offset_of_MobilmoArea_24() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___MobilmoArea_24)); }
	inline GameObject_t1756533147 * get_MobilmoArea_24() const { return ___MobilmoArea_24; }
	inline GameObject_t1756533147 ** get_address_of_MobilmoArea_24() { return &___MobilmoArea_24; }
	inline void set_MobilmoArea_24(GameObject_t1756533147 * value)
	{
		___MobilmoArea_24 = value;
		Il2CppCodeGenWriteBarrier(&___MobilmoArea_24, value);
	}

	inline static int32_t get_offset_of_t_CanvasRayCaster_25() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_CanvasRayCaster_25)); }
	inline GraphicRaycaster_t410733016 * get_t_CanvasRayCaster_25() const { return ___t_CanvasRayCaster_25; }
	inline GraphicRaycaster_t410733016 ** get_address_of_t_CanvasRayCaster_25() { return &___t_CanvasRayCaster_25; }
	inline void set_t_CanvasRayCaster_25(GraphicRaycaster_t410733016 * value)
	{
		___t_CanvasRayCaster_25 = value;
		Il2CppCodeGenWriteBarrier(&___t_CanvasRayCaster_25, value);
	}

	inline static int32_t get_offset_of_t_CoreSelect_26() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_CoreSelect_26)); }
	inline CoreSelect_t1755339513 * get_t_CoreSelect_26() const { return ___t_CoreSelect_26; }
	inline CoreSelect_t1755339513 ** get_address_of_t_CoreSelect_26() { return &___t_CoreSelect_26; }
	inline void set_t_CoreSelect_26(CoreSelect_t1755339513 * value)
	{
		___t_CoreSelect_26 = value;
		Il2CppCodeGenWriteBarrier(&___t_CoreSelect_26, value);
	}

	inline static int32_t get_offset_of_t_PartsConect_27() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_PartsConect_27)); }
	inline MobilimoPartsConect_t3466715044 * get_t_PartsConect_27() const { return ___t_PartsConect_27; }
	inline MobilimoPartsConect_t3466715044 ** get_address_of_t_PartsConect_27() { return &___t_PartsConect_27; }
	inline void set_t_PartsConect_27(MobilimoPartsConect_t3466715044 * value)
	{
		___t_PartsConect_27 = value;
		Il2CppCodeGenWriteBarrier(&___t_PartsConect_27, value);
	}

	inline static int32_t get_offset_of_t_MoveSet_28() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_MoveSet_28)); }
	inline MoveSet_t2777185973 * get_t_MoveSet_28() const { return ___t_MoveSet_28; }
	inline MoveSet_t2777185973 ** get_address_of_t_MoveSet_28() { return &___t_MoveSet_28; }
	inline void set_t_MoveSet_28(MoveSet_t2777185973 * value)
	{
		___t_MoveSet_28 = value;
		Il2CppCodeGenWriteBarrier(&___t_MoveSet_28, value);
	}

	inline static int32_t get_offset_of_t_WorldContents_29() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_WorldContents_29)); }
	inline WorldContents_t112941966 * get_t_WorldContents_29() const { return ___t_WorldContents_29; }
	inline WorldContents_t112941966 ** get_address_of_t_WorldContents_29() { return &___t_WorldContents_29; }
	inline void set_t_WorldContents_29(WorldContents_t112941966 * value)
	{
		___t_WorldContents_29 = value;
		Il2CppCodeGenWriteBarrier(&___t_WorldContents_29, value);
	}

	inline static int32_t get_offset_of_t_RecodeMobility_30() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_RecodeMobility_30)); }
	inline RecordMobility_t2685217900 * get_t_RecodeMobility_30() const { return ___t_RecodeMobility_30; }
	inline RecordMobility_t2685217900 ** get_address_of_t_RecodeMobility_30() { return &___t_RecodeMobility_30; }
	inline void set_t_RecodeMobility_30(RecordMobility_t2685217900 * value)
	{
		___t_RecodeMobility_30 = value;
		Il2CppCodeGenWriteBarrier(&___t_RecodeMobility_30, value);
	}

	inline static int32_t get_offset_of_t_MobilmoAction_31() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_MobilmoAction_31)); }
	inline MobilmoAction_t109766523 * get_t_MobilmoAction_31() const { return ___t_MobilmoAction_31; }
	inline MobilmoAction_t109766523 ** get_address_of_t_MobilmoAction_31() { return &___t_MobilmoAction_31; }
	inline void set_t_MobilmoAction_31(MobilmoAction_t109766523 * value)
	{
		___t_MobilmoAction_31 = value;
		Il2CppCodeGenWriteBarrier(&___t_MobilmoAction_31, value);
	}

	inline static int32_t get_offset_of_t_DemoWorld_32() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_DemoWorld_32)); }
	inline DemoWorld_t457704051 * get_t_DemoWorld_32() const { return ___t_DemoWorld_32; }
	inline DemoWorld_t457704051 ** get_address_of_t_DemoWorld_32() { return &___t_DemoWorld_32; }
	inline void set_t_DemoWorld_32(DemoWorld_t457704051 * value)
	{
		___t_DemoWorld_32 = value;
		Il2CppCodeGenWriteBarrier(&___t_DemoWorld_32, value);
	}

	inline static int32_t get_offset_of_t_MobiSele_33() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_MobiSele_33)); }
	inline TutorialMobilmoSelect_t578122297 * get_t_MobiSele_33() const { return ___t_MobiSele_33; }
	inline TutorialMobilmoSelect_t578122297 ** get_address_of_t_MobiSele_33() { return &___t_MobiSele_33; }
	inline void set_t_MobiSele_33(TutorialMobilmoSelect_t578122297 * value)
	{
		___t_MobiSele_33 = value;
		Il2CppCodeGenWriteBarrier(&___t_MobiSele_33, value);
	}

	inline static int32_t get_offset_of_t_PartsScroll_34() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___t_PartsScroll_34)); }
	inline PartsScroll_t2740354319 * get_t_PartsScroll_34() const { return ___t_PartsScroll_34; }
	inline PartsScroll_t2740354319 ** get_address_of_t_PartsScroll_34() { return &___t_PartsScroll_34; }
	inline void set_t_PartsScroll_34(PartsScroll_t2740354319 * value)
	{
		___t_PartsScroll_34 = value;
		Il2CppCodeGenWriteBarrier(&___t_PartsScroll_34, value);
	}

	inline static int32_t get_offset_of_PartsListScroll_35() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___PartsListScroll_35)); }
	inline ScrollRect_t1199013257 * get_PartsListScroll_35() const { return ___PartsListScroll_35; }
	inline ScrollRect_t1199013257 ** get_address_of_PartsListScroll_35() { return &___PartsListScroll_35; }
	inline void set_PartsListScroll_35(ScrollRect_t1199013257 * value)
	{
		___PartsListScroll_35 = value;
		Il2CppCodeGenWriteBarrier(&___PartsListScroll_35, value);
	}

	inline static int32_t get_offset_of_partsListButtonTran_36() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___partsListButtonTran_36)); }
	inline RectTransform_t3349966182 * get_partsListButtonTran_36() const { return ___partsListButtonTran_36; }
	inline RectTransform_t3349966182 ** get_address_of_partsListButtonTran_36() { return &___partsListButtonTran_36; }
	inline void set_partsListButtonTran_36(RectTransform_t3349966182 * value)
	{
		___partsListButtonTran_36 = value;
		Il2CppCodeGenWriteBarrier(&___partsListButtonTran_36, value);
	}

	inline static int32_t get_offset_of_PartsConfigButton_37() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___PartsConfigButton_37)); }
	inline RectTransform_t3349966182 * get_PartsConfigButton_37() const { return ___PartsConfigButton_37; }
	inline RectTransform_t3349966182 ** get_address_of_PartsConfigButton_37() { return &___PartsConfigButton_37; }
	inline void set_PartsConfigButton_37(RectTransform_t3349966182 * value)
	{
		___PartsConfigButton_37 = value;
		Il2CppCodeGenWriteBarrier(&___PartsConfigButton_37, value);
	}

	inline static int32_t get_offset_of_modelingCompleteButton_38() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___modelingCompleteButton_38)); }
	inline RectTransform_t3349966182 * get_modelingCompleteButton_38() const { return ___modelingCompleteButton_38; }
	inline RectTransform_t3349966182 ** get_address_of_modelingCompleteButton_38() { return &___modelingCompleteButton_38; }
	inline void set_modelingCompleteButton_38(RectTransform_t3349966182 * value)
	{
		___modelingCompleteButton_38 = value;
		Il2CppCodeGenWriteBarrier(&___modelingCompleteButton_38, value);
	}

	inline static int32_t get_offset_of_createButton_39() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___createButton_39)); }
	inline RectTransform_t3349966182 * get_createButton_39() const { return ___createButton_39; }
	inline RectTransform_t3349966182 ** get_address_of_createButton_39() { return &___createButton_39; }
	inline void set_createButton_39(RectTransform_t3349966182 * value)
	{
		___createButton_39 = value;
		Il2CppCodeGenWriteBarrier(&___createButton_39, value);
	}

	inline static int32_t get_offset_of_WorldButton_40() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___WorldButton_40)); }
	inline RectTransform_t3349966182 * get_WorldButton_40() const { return ___WorldButton_40; }
	inline RectTransform_t3349966182 ** get_address_of_WorldButton_40() { return &___WorldButton_40; }
	inline void set_WorldButton_40(RectTransform_t3349966182 * value)
	{
		___WorldButton_40 = value;
		Il2CppCodeGenWriteBarrier(&___WorldButton_40, value);
	}

	inline static int32_t get_offset_of_LeverX_41() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___LeverX_41)); }
	inline RectTransform_t3349966182 * get_LeverX_41() const { return ___LeverX_41; }
	inline RectTransform_t3349966182 ** get_address_of_LeverX_41() { return &___LeverX_41; }
	inline void set_LeverX_41(RectTransform_t3349966182 * value)
	{
		___LeverX_41 = value;
		Il2CppCodeGenWriteBarrier(&___LeverX_41, value);
	}

	inline static int32_t get_offset_of_RollControl_42() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___RollControl_42)); }
	inline RectTransform_t3349966182 * get_RollControl_42() const { return ___RollControl_42; }
	inline RectTransform_t3349966182 ** get_address_of_RollControl_42() { return &___RollControl_42; }
	inline void set_RollControl_42(RectTransform_t3349966182 * value)
	{
		___RollControl_42 = value;
		Il2CppCodeGenWriteBarrier(&___RollControl_42, value);
	}

	inline static int32_t get_offset_of_RecButton_43() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___RecButton_43)); }
	inline RectTransform_t3349966182 * get_RecButton_43() const { return ___RecButton_43; }
	inline RectTransform_t3349966182 ** get_address_of_RecButton_43() { return &___RecButton_43; }
	inline void set_RecButton_43(RectTransform_t3349966182 * value)
	{
		___RecButton_43 = value;
		Il2CppCodeGenWriteBarrier(&___RecButton_43, value);
	}

	inline static int32_t get_offset_of_SkipButton_44() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___SkipButton_44)); }
	inline GameObject_t1756533147 * get_SkipButton_44() const { return ___SkipButton_44; }
	inline GameObject_t1756533147 ** get_address_of_SkipButton_44() { return &___SkipButton_44; }
	inline void set_SkipButton_44(GameObject_t1756533147 * value)
	{
		___SkipButton_44 = value;
		Il2CppCodeGenWriteBarrier(&___SkipButton_44, value);
	}

	inline static int32_t get_offset_of_NomalTabButton_45() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___NomalTabButton_45)); }
	inline RectTransform_t3349966182 * get_NomalTabButton_45() const { return ___NomalTabButton_45; }
	inline RectTransform_t3349966182 ** get_address_of_NomalTabButton_45() { return &___NomalTabButton_45; }
	inline void set_NomalTabButton_45(RectTransform_t3349966182 * value)
	{
		___NomalTabButton_45 = value;
		Il2CppCodeGenWriteBarrier(&___NomalTabButton_45, value);
	}

	inline static int32_t get_offset_of_mobilmoDnaCon_46() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___mobilmoDnaCon_46)); }
	inline MobilmoDnaController_t1006271200 * get_mobilmoDnaCon_46() const { return ___mobilmoDnaCon_46; }
	inline MobilmoDnaController_t1006271200 ** get_address_of_mobilmoDnaCon_46() { return &___mobilmoDnaCon_46; }
	inline void set_mobilmoDnaCon_46(MobilmoDnaController_t1006271200 * value)
	{
		___mobilmoDnaCon_46 = value;
		Il2CppCodeGenWriteBarrier(&___mobilmoDnaCon_46, value);
	}

	inline static int32_t get_offset_of_m_sWardsNum_47() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_sWardsNum_47)); }
	inline int32_t get_m_sWardsNum_47() const { return ___m_sWardsNum_47; }
	inline int32_t* get_address_of_m_sWardsNum_47() { return &___m_sWardsNum_47; }
	inline void set_m_sWardsNum_47(int32_t value)
	{
		___m_sWardsNum_47 = value;
	}

	inline static int32_t get_offset_of_m_pTalkWards_48() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_pTalkWards_48)); }
	inline StringU5BU5D_t1642385972* get_m_pTalkWards_48() const { return ___m_pTalkWards_48; }
	inline StringU5BU5D_t1642385972** get_address_of_m_pTalkWards_48() { return &___m_pTalkWards_48; }
	inline void set_m_pTalkWards_48(StringU5BU5D_t1642385972* value)
	{
		___m_pTalkWards_48 = value;
		Il2CppCodeGenWriteBarrier(&___m_pTalkWards_48, value);
	}

	inline static int32_t get_offset_of_TutrialAction_49() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___TutrialAction_49)); }
	inline ActionU5BU5D_t87223449* get_TutrialAction_49() const { return ___TutrialAction_49; }
	inline ActionU5BU5D_t87223449** get_address_of_TutrialAction_49() { return &___TutrialAction_49; }
	inline void set_TutrialAction_49(ActionU5BU5D_t87223449* value)
	{
		___TutrialAction_49 = value;
		Il2CppCodeGenWriteBarrier(&___TutrialAction_49, value);
	}

	inline static int32_t get_offset_of_m_TalkFader_50() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_TalkFader_50)); }
	inline TalkTextFader_t1511400589 * get_m_TalkFader_50() const { return ___m_TalkFader_50; }
	inline TalkTextFader_t1511400589 ** get_address_of_m_TalkFader_50() { return &___m_TalkFader_50; }
	inline void set_m_TalkFader_50(TalkTextFader_t1511400589 * value)
	{
		___m_TalkFader_50 = value;
		Il2CppCodeGenWriteBarrier(&___m_TalkFader_50, value);
	}

	inline static int32_t get_offset_of_m_pSerifs_51() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_pSerifs_51)); }
	inline StringU5BU5D_t1642385972* get_m_pSerifs_51() const { return ___m_pSerifs_51; }
	inline StringU5BU5D_t1642385972** get_address_of_m_pSerifs_51() { return &___m_pSerifs_51; }
	inline void set_m_pSerifs_51(StringU5BU5D_t1642385972* value)
	{
		___m_pSerifs_51 = value;
		Il2CppCodeGenWriteBarrier(&___m_pSerifs_51, value);
	}

	inline static int32_t get_offset_of_m_pSerifsMobilmo_52() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_pSerifsMobilmo_52)); }
	inline String_t* get_m_pSerifsMobilmo_52() const { return ___m_pSerifsMobilmo_52; }
	inline String_t** get_address_of_m_pSerifsMobilmo_52() { return &___m_pSerifsMobilmo_52; }
	inline void set_m_pSerifsMobilmo_52(String_t* value)
	{
		___m_pSerifsMobilmo_52 = value;
		Il2CppCodeGenWriteBarrier(&___m_pSerifsMobilmo_52, value);
	}

	inline static int32_t get_offset_of_m_pSerifsAction_53() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___m_pSerifsAction_53)); }
	inline String_t* get_m_pSerifsAction_53() const { return ___m_pSerifsAction_53; }
	inline String_t** get_address_of_m_pSerifsAction_53() { return &___m_pSerifsAction_53; }
	inline void set_m_pSerifsAction_53(String_t* value)
	{
		___m_pSerifsAction_53 = value;
		Il2CppCodeGenWriteBarrier(&___m_pSerifsAction_53, value);
	}

	inline static int32_t get_offset_of_VoiceNum_54() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___VoiceNum_54)); }
	inline int32_t get_VoiceNum_54() const { return ___VoiceNum_54; }
	inline int32_t* get_address_of_VoiceNum_54() { return &___VoiceNum_54; }
	inline void set_VoiceNum_54(int32_t value)
	{
		___VoiceNum_54 = value;
	}

	inline static int32_t get_offset_of_MoveDis_55() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773, ___MoveDis_55)); }
	inline float get_MoveDis_55() const { return ___MoveDis_55; }
	inline float* get_address_of_MoveDis_55() { return &___MoveDis_55; }
	inline void set_MoveDis_55(float value)
	{
		___MoveDis_55 = value;
	}
};

struct TutorialManager_t2168024773_StaticFields
{
public:
	// DG.Tweening.TweenCallback TutorialManager::<>f__am$cache0
	TweenCallback_t3697142134 * ___U3CU3Ef__amU24cache0_56;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_56() { return static_cast<int32_t>(offsetof(TutorialManager_t2168024773_StaticFields, ___U3CU3Ef__amU24cache0_56)); }
	inline TweenCallback_t3697142134 * get_U3CU3Ef__amU24cache0_56() const { return ___U3CU3Ef__amU24cache0_56; }
	inline TweenCallback_t3697142134 ** get_address_of_U3CU3Ef__amU24cache0_56() { return &___U3CU3Ef__amU24cache0_56; }
	inline void set_U3CU3Ef__amU24cache0_56(TweenCallback_t3697142134 * value)
	{
		___U3CU3Ef__amU24cache0_56 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
