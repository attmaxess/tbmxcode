﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Camera
struct Camera_t189460977;
// TutorialManager
struct TutorialManager_t2168024773;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<CircleSetLoop>c__Iterator1
struct  U3CCircleSetLoopU3Ec__Iterator1_t3353285022  : public Il2CppObject
{
public:
	// UnityEngine.Transform TutorialManager/<CircleSetLoop>c__Iterator1::target
	Transform_t3275118058 * ___target_0;
	// UnityEngine.Camera TutorialManager/<CircleSetLoop>c__Iterator1::cam
	Camera_t189460977 * ___cam_1;
	// TutorialManager TutorialManager/<CircleSetLoop>c__Iterator1::$this
	TutorialManager_t2168024773 * ___U24this_2;
	// System.Object TutorialManager/<CircleSetLoop>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean TutorialManager/<CircleSetLoop>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 TutorialManager/<CircleSetLoop>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CCircleSetLoopU3Ec__Iterator1_t3353285022, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}

	inline static int32_t get_offset_of_cam_1() { return static_cast<int32_t>(offsetof(U3CCircleSetLoopU3Ec__Iterator1_t3353285022, ___cam_1)); }
	inline Camera_t189460977 * get_cam_1() const { return ___cam_1; }
	inline Camera_t189460977 ** get_address_of_cam_1() { return &___cam_1; }
	inline void set_cam_1(Camera_t189460977 * value)
	{
		___cam_1 = value;
		Il2CppCodeGenWriteBarrier(&___cam_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCircleSetLoopU3Ec__Iterator1_t3353285022, ___U24this_2)); }
	inline TutorialManager_t2168024773 * get_U24this_2() const { return ___U24this_2; }
	inline TutorialManager_t2168024773 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TutorialManager_t2168024773 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCircleSetLoopU3Ec__Iterator1_t3353285022, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCircleSetLoopU3Ec__Iterator1_t3353285022, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCircleSetLoopU3Ec__Iterator1_t3353285022, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
