﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Transform
struct Transform_t3275118058;
// TutorialManager
struct TutorialManager_t2168024773;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<CircleSet>c__AnonStorey4
struct  U3CCircleSetU3Ec__AnonStorey4_t2608721195  : public Il2CppObject
{
public:
	// UnityEngine.Camera TutorialManager/<CircleSet>c__AnonStorey4::cam
	Camera_t189460977 * ___cam_0;
	// UnityEngine.Transform TutorialManager/<CircleSet>c__AnonStorey4::target
	Transform_t3275118058 * ___target_1;
	// TutorialManager TutorialManager/<CircleSet>c__AnonStorey4::$this
	TutorialManager_t2168024773 * ___U24this_2;

public:
	inline static int32_t get_offset_of_cam_0() { return static_cast<int32_t>(offsetof(U3CCircleSetU3Ec__AnonStorey4_t2608721195, ___cam_0)); }
	inline Camera_t189460977 * get_cam_0() const { return ___cam_0; }
	inline Camera_t189460977 ** get_address_of_cam_0() { return &___cam_0; }
	inline void set_cam_0(Camera_t189460977 * value)
	{
		___cam_0 = value;
		Il2CppCodeGenWriteBarrier(&___cam_0, value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CCircleSetU3Ec__AnonStorey4_t2608721195, ___target_1)); }
	inline Transform_t3275118058 * get_target_1() const { return ___target_1; }
	inline Transform_t3275118058 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3275118058 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier(&___target_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCircleSetU3Ec__AnonStorey4_t2608721195, ___U24this_2)); }
	inline TutorialManager_t2168024773 * get_U24this_2() const { return ___U24this_2; }
	inline TutorialManager_t2168024773 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TutorialManager_t2168024773 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
