﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// TutorialManager
struct TutorialManager_t2168024773;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<CircleSet>c__AnonStorey5
struct  U3CCircleSetU3Ec__AnonStorey5_t2608721196  : public Il2CppObject
{
public:
	// UnityEngine.RectTransform TutorialManager/<CircleSet>c__AnonStorey5::tran
	RectTransform_t3349966182 * ___tran_0;
	// TutorialManager TutorialManager/<CircleSet>c__AnonStorey5::$this
	TutorialManager_t2168024773 * ___U24this_1;

public:
	inline static int32_t get_offset_of_tran_0() { return static_cast<int32_t>(offsetof(U3CCircleSetU3Ec__AnonStorey5_t2608721196, ___tran_0)); }
	inline RectTransform_t3349966182 * get_tran_0() const { return ___tran_0; }
	inline RectTransform_t3349966182 ** get_address_of_tran_0() { return &___tran_0; }
	inline void set_tran_0(RectTransform_t3349966182 * value)
	{
		___tran_0 = value;
		Il2CppCodeGenWriteBarrier(&___tran_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCircleSetU3Ec__AnonStorey5_t2608721196, ___U24this_1)); }
	inline TutorialManager_t2168024773 * get_U24this_1() const { return ___U24this_1; }
	inline TutorialManager_t2168024773 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TutorialManager_t2168024773 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
