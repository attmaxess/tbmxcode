﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// System.Action
struct Action_t3226471752;
// TutorialManager
struct TutorialManager_t2168024773;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<DistanceMeasure>c__Iterator3
struct  U3CDistanceMeasureU3Ec__Iterator3_t2793382657  : public Il2CppObject
{
public:
	// UnityEngine.Transform TutorialManager/<DistanceMeasure>c__Iterator3::taget
	Transform_t3275118058 * ___taget_0;
	// UnityEngine.Vector3 TutorialManager/<DistanceMeasure>c__Iterator3::<prevpos>__0
	Vector3_t2243707580  ___U3CprevposU3E__0_1;
	// UnityEngine.Vector3 TutorialManager/<DistanceMeasure>c__Iterator3::<NowPos>__0
	Vector3_t2243707580  ___U3CNowPosU3E__0_2;
	// System.Single TutorialManager/<DistanceMeasure>c__Iterator3::maxDis
	float ___maxDis_3;
	// System.Action TutorialManager/<DistanceMeasure>c__Iterator3::EndAction
	Action_t3226471752 * ___EndAction_4;
	// TutorialManager TutorialManager/<DistanceMeasure>c__Iterator3::$this
	TutorialManager_t2168024773 * ___U24this_5;
	// System.Object TutorialManager/<DistanceMeasure>c__Iterator3::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean TutorialManager/<DistanceMeasure>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 TutorialManager/<DistanceMeasure>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_taget_0() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___taget_0)); }
	inline Transform_t3275118058 * get_taget_0() const { return ___taget_0; }
	inline Transform_t3275118058 ** get_address_of_taget_0() { return &___taget_0; }
	inline void set_taget_0(Transform_t3275118058 * value)
	{
		___taget_0 = value;
		Il2CppCodeGenWriteBarrier(&___taget_0, value);
	}

	inline static int32_t get_offset_of_U3CprevposU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___U3CprevposU3E__0_1)); }
	inline Vector3_t2243707580  get_U3CprevposU3E__0_1() const { return ___U3CprevposU3E__0_1; }
	inline Vector3_t2243707580 * get_address_of_U3CprevposU3E__0_1() { return &___U3CprevposU3E__0_1; }
	inline void set_U3CprevposU3E__0_1(Vector3_t2243707580  value)
	{
		___U3CprevposU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CNowPosU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___U3CNowPosU3E__0_2)); }
	inline Vector3_t2243707580  get_U3CNowPosU3E__0_2() const { return ___U3CNowPosU3E__0_2; }
	inline Vector3_t2243707580 * get_address_of_U3CNowPosU3E__0_2() { return &___U3CNowPosU3E__0_2; }
	inline void set_U3CNowPosU3E__0_2(Vector3_t2243707580  value)
	{
		___U3CNowPosU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_maxDis_3() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___maxDis_3)); }
	inline float get_maxDis_3() const { return ___maxDis_3; }
	inline float* get_address_of_maxDis_3() { return &___maxDis_3; }
	inline void set_maxDis_3(float value)
	{
		___maxDis_3 = value;
	}

	inline static int32_t get_offset_of_EndAction_4() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___EndAction_4)); }
	inline Action_t3226471752 * get_EndAction_4() const { return ___EndAction_4; }
	inline Action_t3226471752 ** get_address_of_EndAction_4() { return &___EndAction_4; }
	inline void set_EndAction_4(Action_t3226471752 * value)
	{
		___EndAction_4 = value;
		Il2CppCodeGenWriteBarrier(&___EndAction_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___U24this_5)); }
	inline TutorialManager_t2168024773 * get_U24this_5() const { return ___U24this_5; }
	inline TutorialManager_t2168024773 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TutorialManager_t2168024773 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDistanceMeasureU3Ec__Iterator3_t2793382657, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
