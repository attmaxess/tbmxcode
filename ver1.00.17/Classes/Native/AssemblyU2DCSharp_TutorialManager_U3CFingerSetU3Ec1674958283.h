﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// TutorialManager
struct TutorialManager_t2168024773;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<FingerSet>c__AnonStorey7
struct  U3CFingerSetU3Ec__AnonStorey7_t1674958283  : public Il2CppObject
{
public:
	// UnityEngine.RectTransform TutorialManager/<FingerSet>c__AnonStorey7::tran
	RectTransform_t3349966182 * ___tran_0;
	// UnityEngine.Vector3 TutorialManager/<FingerSet>c__AnonStorey7::pos
	Vector3_t2243707580  ___pos_1;
	// TutorialManager TutorialManager/<FingerSet>c__AnonStorey7::$this
	TutorialManager_t2168024773 * ___U24this_2;

public:
	inline static int32_t get_offset_of_tran_0() { return static_cast<int32_t>(offsetof(U3CFingerSetU3Ec__AnonStorey7_t1674958283, ___tran_0)); }
	inline RectTransform_t3349966182 * get_tran_0() const { return ___tran_0; }
	inline RectTransform_t3349966182 ** get_address_of_tran_0() { return &___tran_0; }
	inline void set_tran_0(RectTransform_t3349966182 * value)
	{
		___tran_0 = value;
		Il2CppCodeGenWriteBarrier(&___tran_0, value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(U3CFingerSetU3Ec__AnonStorey7_t1674958283, ___pos_1)); }
	inline Vector3_t2243707580  get_pos_1() const { return ___pos_1; }
	inline Vector3_t2243707580 * get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(Vector3_t2243707580  value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CFingerSetU3Ec__AnonStorey7_t1674958283, ___U24this_2)); }
	inline TutorialManager_t2168024773 * get_U24this_2() const { return ___U24this_2; }
	inline TutorialManager_t2168024773 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TutorialManager_t2168024773 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
