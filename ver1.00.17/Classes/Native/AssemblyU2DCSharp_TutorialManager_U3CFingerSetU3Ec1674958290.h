﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// TutorialManager
struct TutorialManager_t2168024773;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager/<FingerSet>c__AnonStorey8
struct  U3CFingerSetU3Ec__AnonStorey8_t1674958290  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 TutorialManager/<FingerSet>c__AnonStorey8::startpos
	Vector2_t2243707579  ___startpos_0;
	// UnityEngine.Vector2 TutorialManager/<FingerSet>c__AnonStorey8::endPos
	Vector2_t2243707579  ___endPos_1;
	// TutorialManager TutorialManager/<FingerSet>c__AnonStorey8::$this
	TutorialManager_t2168024773 * ___U24this_2;

public:
	inline static int32_t get_offset_of_startpos_0() { return static_cast<int32_t>(offsetof(U3CFingerSetU3Ec__AnonStorey8_t1674958290, ___startpos_0)); }
	inline Vector2_t2243707579  get_startpos_0() const { return ___startpos_0; }
	inline Vector2_t2243707579 * get_address_of_startpos_0() { return &___startpos_0; }
	inline void set_startpos_0(Vector2_t2243707579  value)
	{
		___startpos_0 = value;
	}

	inline static int32_t get_offset_of_endPos_1() { return static_cast<int32_t>(offsetof(U3CFingerSetU3Ec__AnonStorey8_t1674958290, ___endPos_1)); }
	inline Vector2_t2243707579  get_endPos_1() const { return ___endPos_1; }
	inline Vector2_t2243707579 * get_address_of_endPos_1() { return &___endPos_1; }
	inline void set_endPos_1(Vector2_t2243707579  value)
	{
		___endPos_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CFingerSetU3Ec__AnonStorey8_t1674958290, ___U24this_2)); }
	inline TutorialManager_t2168024773 * get_U24this_2() const { return ___U24this_2; }
	inline TutorialManager_t2168024773 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TutorialManager_t2168024773 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
