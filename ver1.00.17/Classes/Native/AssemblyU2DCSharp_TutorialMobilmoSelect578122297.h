﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Transform
struct Transform_t3275118058;
// TutorialMobilmoSelect/RentalMobilmo[]
struct RentalMobilmoU5BU5D_t796232736;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialMobilmoSelect
struct  TutorialMobilmoSelect_t578122297  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TutorialMobilmoSelect::Info01
	GameObject_t1756533147 * ___Info01_2;
	// UnityEngine.GameObject TutorialMobilmoSelect::Info02
	GameObject_t1756533147 * ___Info02_3;
	// UnityEngine.GameObject TutorialMobilmoSelect::RentalMobilmoSelect
	GameObject_t1756533147 * ___RentalMobilmoSelect_4;
	// UnityEngine.GameObject TutorialMobilmoSelect::SelectMobilmoPanel
	GameObject_t1756533147 * ___SelectMobilmoPanel_5;
	// UnityEngine.UI.Text TutorialMobilmoSelect::SelectedNameText
	Text_t356221433 * ___SelectedNameText_6;
	// UnityEngine.UI.Image TutorialMobilmoSelect::SelectedImage
	Image_t2042527209 * ___SelectedImage_7;
	// UnityEngine.Transform TutorialMobilmoSelect::RentalMobilmoListParent
	Transform_t3275118058 * ___RentalMobilmoListParent_8;
	// TutorialMobilmoSelect/RentalMobilmo[] TutorialMobilmoSelect::RentalMobi
	RentalMobilmoU5BU5D_t796232736* ___RentalMobi_9;
	// System.Int32 TutorialMobilmoSelect::SelectMobilmoNum
	int32_t ___SelectMobilmoNum_10;
	// UnityEngine.GameObject TutorialMobilmoSelect::RentalContentSample
	GameObject_t1756533147 * ___RentalContentSample_11;
	// UnityEngine.CanvasGroup TutorialMobilmoSelect::m_Canvas
	CanvasGroup_t3296560743 * ___m_Canvas_12;

public:
	inline static int32_t get_offset_of_Info01_2() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___Info01_2)); }
	inline GameObject_t1756533147 * get_Info01_2() const { return ___Info01_2; }
	inline GameObject_t1756533147 ** get_address_of_Info01_2() { return &___Info01_2; }
	inline void set_Info01_2(GameObject_t1756533147 * value)
	{
		___Info01_2 = value;
		Il2CppCodeGenWriteBarrier(&___Info01_2, value);
	}

	inline static int32_t get_offset_of_Info02_3() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___Info02_3)); }
	inline GameObject_t1756533147 * get_Info02_3() const { return ___Info02_3; }
	inline GameObject_t1756533147 ** get_address_of_Info02_3() { return &___Info02_3; }
	inline void set_Info02_3(GameObject_t1756533147 * value)
	{
		___Info02_3 = value;
		Il2CppCodeGenWriteBarrier(&___Info02_3, value);
	}

	inline static int32_t get_offset_of_RentalMobilmoSelect_4() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___RentalMobilmoSelect_4)); }
	inline GameObject_t1756533147 * get_RentalMobilmoSelect_4() const { return ___RentalMobilmoSelect_4; }
	inline GameObject_t1756533147 ** get_address_of_RentalMobilmoSelect_4() { return &___RentalMobilmoSelect_4; }
	inline void set_RentalMobilmoSelect_4(GameObject_t1756533147 * value)
	{
		___RentalMobilmoSelect_4 = value;
		Il2CppCodeGenWriteBarrier(&___RentalMobilmoSelect_4, value);
	}

	inline static int32_t get_offset_of_SelectMobilmoPanel_5() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___SelectMobilmoPanel_5)); }
	inline GameObject_t1756533147 * get_SelectMobilmoPanel_5() const { return ___SelectMobilmoPanel_5; }
	inline GameObject_t1756533147 ** get_address_of_SelectMobilmoPanel_5() { return &___SelectMobilmoPanel_5; }
	inline void set_SelectMobilmoPanel_5(GameObject_t1756533147 * value)
	{
		___SelectMobilmoPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___SelectMobilmoPanel_5, value);
	}

	inline static int32_t get_offset_of_SelectedNameText_6() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___SelectedNameText_6)); }
	inline Text_t356221433 * get_SelectedNameText_6() const { return ___SelectedNameText_6; }
	inline Text_t356221433 ** get_address_of_SelectedNameText_6() { return &___SelectedNameText_6; }
	inline void set_SelectedNameText_6(Text_t356221433 * value)
	{
		___SelectedNameText_6 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedNameText_6, value);
	}

	inline static int32_t get_offset_of_SelectedImage_7() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___SelectedImage_7)); }
	inline Image_t2042527209 * get_SelectedImage_7() const { return ___SelectedImage_7; }
	inline Image_t2042527209 ** get_address_of_SelectedImage_7() { return &___SelectedImage_7; }
	inline void set_SelectedImage_7(Image_t2042527209 * value)
	{
		___SelectedImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedImage_7, value);
	}

	inline static int32_t get_offset_of_RentalMobilmoListParent_8() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___RentalMobilmoListParent_8)); }
	inline Transform_t3275118058 * get_RentalMobilmoListParent_8() const { return ___RentalMobilmoListParent_8; }
	inline Transform_t3275118058 ** get_address_of_RentalMobilmoListParent_8() { return &___RentalMobilmoListParent_8; }
	inline void set_RentalMobilmoListParent_8(Transform_t3275118058 * value)
	{
		___RentalMobilmoListParent_8 = value;
		Il2CppCodeGenWriteBarrier(&___RentalMobilmoListParent_8, value);
	}

	inline static int32_t get_offset_of_RentalMobi_9() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___RentalMobi_9)); }
	inline RentalMobilmoU5BU5D_t796232736* get_RentalMobi_9() const { return ___RentalMobi_9; }
	inline RentalMobilmoU5BU5D_t796232736** get_address_of_RentalMobi_9() { return &___RentalMobi_9; }
	inline void set_RentalMobi_9(RentalMobilmoU5BU5D_t796232736* value)
	{
		___RentalMobi_9 = value;
		Il2CppCodeGenWriteBarrier(&___RentalMobi_9, value);
	}

	inline static int32_t get_offset_of_SelectMobilmoNum_10() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___SelectMobilmoNum_10)); }
	inline int32_t get_SelectMobilmoNum_10() const { return ___SelectMobilmoNum_10; }
	inline int32_t* get_address_of_SelectMobilmoNum_10() { return &___SelectMobilmoNum_10; }
	inline void set_SelectMobilmoNum_10(int32_t value)
	{
		___SelectMobilmoNum_10 = value;
	}

	inline static int32_t get_offset_of_RentalContentSample_11() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___RentalContentSample_11)); }
	inline GameObject_t1756533147 * get_RentalContentSample_11() const { return ___RentalContentSample_11; }
	inline GameObject_t1756533147 ** get_address_of_RentalContentSample_11() { return &___RentalContentSample_11; }
	inline void set_RentalContentSample_11(GameObject_t1756533147 * value)
	{
		___RentalContentSample_11 = value;
		Il2CppCodeGenWriteBarrier(&___RentalContentSample_11, value);
	}

	inline static int32_t get_offset_of_m_Canvas_12() { return static_cast<int32_t>(offsetof(TutorialMobilmoSelect_t578122297, ___m_Canvas_12)); }
	inline CanvasGroup_t3296560743 * get_m_Canvas_12() const { return ___m_Canvas_12; }
	inline CanvasGroup_t3296560743 ** get_address_of_m_Canvas_12() { return &___m_Canvas_12; }
	inline void set_m_Canvas_12(CanvasGroup_t3296560743 * value)
	{
		___m_Canvas_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_Canvas_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
