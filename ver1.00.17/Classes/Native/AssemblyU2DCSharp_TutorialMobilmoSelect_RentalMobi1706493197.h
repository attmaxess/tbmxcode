﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// RentalMobilmoContent
struct RentalMobilmoContent_t501695126;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialMobilmoSelect/RentalMobilmo
struct  RentalMobilmo_t1706493197 
{
public:
	// System.String TutorialMobilmoSelect/RentalMobilmo::Name
	String_t* ___Name_0;
	// UnityEngine.Sprite TutorialMobilmoSelect/RentalMobilmo::Image
	Sprite_t309593783 * ___Image_1;
	// UnityEngine.TextAsset TutorialMobilmoSelect/RentalMobilmo::MobilmoJson
	TextAsset_t3973159845 * ___MobilmoJson_2;
	// UnityEngine.TextAsset TutorialMobilmoSelect/RentalMobilmo::RecodingJson
	TextAsset_t3973159845 * ___RecodingJson_3;
	// UnityEngine.TextAsset TutorialMobilmoSelect/RentalMobilmo::CreateMobilmoJson
	TextAsset_t3973159845 * ___CreateMobilmoJson_4;
	// UnityEngine.TextAsset TutorialMobilmoSelect/RentalMobilmo::CreateMotionJson
	TextAsset_t3973159845 * ___CreateMotionJson_5;
	// System.Int32 TutorialMobilmoSelect/RentalMobilmo::CustomJointParentNum
	int32_t ___CustomJointParentNum_6;
	// System.Int32 TutorialMobilmoSelect/RentalMobilmo::CustomJointChildNum
	int32_t ___CustomJointChildNum_7;
	// System.String TutorialMobilmoSelect/RentalMobilmo::Info
	String_t* ___Info_8;
	// RentalMobilmoContent TutorialMobilmoSelect/RentalMobilmo::content
	RentalMobilmoContent_t501695126 * ___content_9;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier(&___Name_0, value);
	}

	inline static int32_t get_offset_of_Image_1() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___Image_1)); }
	inline Sprite_t309593783 * get_Image_1() const { return ___Image_1; }
	inline Sprite_t309593783 ** get_address_of_Image_1() { return &___Image_1; }
	inline void set_Image_1(Sprite_t309593783 * value)
	{
		___Image_1 = value;
		Il2CppCodeGenWriteBarrier(&___Image_1, value);
	}

	inline static int32_t get_offset_of_MobilmoJson_2() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___MobilmoJson_2)); }
	inline TextAsset_t3973159845 * get_MobilmoJson_2() const { return ___MobilmoJson_2; }
	inline TextAsset_t3973159845 ** get_address_of_MobilmoJson_2() { return &___MobilmoJson_2; }
	inline void set_MobilmoJson_2(TextAsset_t3973159845 * value)
	{
		___MobilmoJson_2 = value;
		Il2CppCodeGenWriteBarrier(&___MobilmoJson_2, value);
	}

	inline static int32_t get_offset_of_RecodingJson_3() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___RecodingJson_3)); }
	inline TextAsset_t3973159845 * get_RecodingJson_3() const { return ___RecodingJson_3; }
	inline TextAsset_t3973159845 ** get_address_of_RecodingJson_3() { return &___RecodingJson_3; }
	inline void set_RecodingJson_3(TextAsset_t3973159845 * value)
	{
		___RecodingJson_3 = value;
		Il2CppCodeGenWriteBarrier(&___RecodingJson_3, value);
	}

	inline static int32_t get_offset_of_CreateMobilmoJson_4() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___CreateMobilmoJson_4)); }
	inline TextAsset_t3973159845 * get_CreateMobilmoJson_4() const { return ___CreateMobilmoJson_4; }
	inline TextAsset_t3973159845 ** get_address_of_CreateMobilmoJson_4() { return &___CreateMobilmoJson_4; }
	inline void set_CreateMobilmoJson_4(TextAsset_t3973159845 * value)
	{
		___CreateMobilmoJson_4 = value;
		Il2CppCodeGenWriteBarrier(&___CreateMobilmoJson_4, value);
	}

	inline static int32_t get_offset_of_CreateMotionJson_5() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___CreateMotionJson_5)); }
	inline TextAsset_t3973159845 * get_CreateMotionJson_5() const { return ___CreateMotionJson_5; }
	inline TextAsset_t3973159845 ** get_address_of_CreateMotionJson_5() { return &___CreateMotionJson_5; }
	inline void set_CreateMotionJson_5(TextAsset_t3973159845 * value)
	{
		___CreateMotionJson_5 = value;
		Il2CppCodeGenWriteBarrier(&___CreateMotionJson_5, value);
	}

	inline static int32_t get_offset_of_CustomJointParentNum_6() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___CustomJointParentNum_6)); }
	inline int32_t get_CustomJointParentNum_6() const { return ___CustomJointParentNum_6; }
	inline int32_t* get_address_of_CustomJointParentNum_6() { return &___CustomJointParentNum_6; }
	inline void set_CustomJointParentNum_6(int32_t value)
	{
		___CustomJointParentNum_6 = value;
	}

	inline static int32_t get_offset_of_CustomJointChildNum_7() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___CustomJointChildNum_7)); }
	inline int32_t get_CustomJointChildNum_7() const { return ___CustomJointChildNum_7; }
	inline int32_t* get_address_of_CustomJointChildNum_7() { return &___CustomJointChildNum_7; }
	inline void set_CustomJointChildNum_7(int32_t value)
	{
		___CustomJointChildNum_7 = value;
	}

	inline static int32_t get_offset_of_Info_8() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___Info_8)); }
	inline String_t* get_Info_8() const { return ___Info_8; }
	inline String_t** get_address_of_Info_8() { return &___Info_8; }
	inline void set_Info_8(String_t* value)
	{
		___Info_8 = value;
		Il2CppCodeGenWriteBarrier(&___Info_8, value);
	}

	inline static int32_t get_offset_of_content_9() { return static_cast<int32_t>(offsetof(RentalMobilmo_t1706493197, ___content_9)); }
	inline RentalMobilmoContent_t501695126 * get_content_9() const { return ___content_9; }
	inline RentalMobilmoContent_t501695126 ** get_address_of_content_9() { return &___content_9; }
	inline void set_content_9(RentalMobilmoContent_t501695126 * value)
	{
		___content_9 = value;
		Il2CppCodeGenWriteBarrier(&___content_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TutorialMobilmoSelect/RentalMobilmo
struct RentalMobilmo_t1706493197_marshaled_pinvoke
{
	char* ___Name_0;
	Sprite_t309593783 * ___Image_1;
	TextAsset_t3973159845 * ___MobilmoJson_2;
	TextAsset_t3973159845 * ___RecodingJson_3;
	TextAsset_t3973159845 * ___CreateMobilmoJson_4;
	TextAsset_t3973159845 * ___CreateMotionJson_5;
	int32_t ___CustomJointParentNum_6;
	int32_t ___CustomJointChildNum_7;
	char* ___Info_8;
	RentalMobilmoContent_t501695126 * ___content_9;
};
// Native definition for COM marshalling of TutorialMobilmoSelect/RentalMobilmo
struct RentalMobilmo_t1706493197_marshaled_com
{
	Il2CppChar* ___Name_0;
	Sprite_t309593783 * ___Image_1;
	TextAsset_t3973159845 * ___MobilmoJson_2;
	TextAsset_t3973159845 * ___RecodingJson_3;
	TextAsset_t3973159845 * ___CreateMobilmoJson_4;
	TextAsset_t3973159845 * ___CreateMotionJson_5;
	int32_t ___CustomJointParentNum_6;
	int32_t ___CustomJointChildNum_7;
	Il2CppChar* ___Info_8;
	RentalMobilmoContent_t501695126 * ___content_9;
};
