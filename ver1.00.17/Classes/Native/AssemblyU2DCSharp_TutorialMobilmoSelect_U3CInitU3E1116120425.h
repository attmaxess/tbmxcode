﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// TutorialMobilmoSelect
struct TutorialMobilmoSelect_t578122297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialMobilmoSelect/<Init>c__AnonStorey0
struct  U3CInitU3Ec__AnonStorey0_t1116120425  : public Il2CppObject
{
public:
	// System.Int32 TutorialMobilmoSelect/<Init>c__AnonStorey0::n
	int32_t ___n_0;
	// TutorialMobilmoSelect TutorialMobilmoSelect/<Init>c__AnonStorey0::$this
	TutorialMobilmoSelect_t578122297 * ___U24this_1;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t1116120425, ___n_0)); }
	inline int32_t get_n_0() const { return ___n_0; }
	inline int32_t* get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(int32_t value)
	{
		___n_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey0_t1116120425, ___U24this_1)); }
	inline TutorialMobilmoSelect_t578122297 * get_U24this_1() const { return ___U24this_1; }
	inline TutorialMobilmoSelect_t578122297 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TutorialMobilmoSelect_t578122297 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
