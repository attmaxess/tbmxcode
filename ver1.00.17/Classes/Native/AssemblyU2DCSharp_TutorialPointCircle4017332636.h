﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPointCircle
struct  TutorialPointCircle_t4017332636  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image TutorialPointCircle::Circle
	Image_t2042527209 * ___Circle_2;

public:
	inline static int32_t get_offset_of_Circle_2() { return static_cast<int32_t>(offsetof(TutorialPointCircle_t4017332636, ___Circle_2)); }
	inline Image_t2042527209 * get_Circle_2() const { return ___Circle_2; }
	inline Image_t2042527209 ** get_address_of_Circle_2() { return &___Circle_2; }
	inline void set_Circle_2(Image_t2042527209 * value)
	{
		___Circle_2 = value;
		Il2CppCodeGenWriteBarrier(&___Circle_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
