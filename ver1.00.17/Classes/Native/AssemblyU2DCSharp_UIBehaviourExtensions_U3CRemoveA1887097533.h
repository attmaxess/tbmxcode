﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBehaviourExtensions/<RemoveAllListeners>c__AnonStorey0
struct  U3CRemoveAllListenersU3Ec__AnonStorey0_t1887097533  : public Il2CppObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UIBehaviourExtensions/<RemoveAllListeners>c__AnonStorey0::eventID
	int32_t ___eventID_0;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(U3CRemoveAllListenersU3Ec__AnonStorey0_t1887097533, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
