﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UILocalizer/LocalizeString
struct LocalizeString_t1691720564;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.LetterSpacing
struct LetterSpacing_t3080148137;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UILocalizer
struct  UILocalizer_t2245030347  : public MonoBehaviour_t1158329972
{
public:
	// UILocalizer/LocalizeString UILocalizer::m_Key
	LocalizeString_t1691720564 * ___m_Key_2;
	// UnityEngine.UI.Text UILocalizer::Text
	Text_t356221433 * ___Text_3;
	// UnityEngine.UI.LetterSpacing UILocalizer::LetterSpacing
	LetterSpacing_t3080148137 * ___LetterSpacing_4;
	// System.String UILocalizer::Font
	String_t* ___Font_5;
	// System.Int32 UILocalizer::Size
	int32_t ___Size_6;
	// System.Single UILocalizer::Kerning
	float ___Kerning_7;
	// System.Single UILocalizer::Linespacing
	float ___Linespacing_8;
	// System.String UILocalizer::Content
	String_t* ___Content_9;
	// System.Boolean UILocalizer::IsNotTextUpdate
	bool ___IsNotTextUpdate_10;
	// System.Boolean UILocalizer::IsUsedRegexHypertext
	bool ___IsUsedRegexHypertext_11;

public:
	inline static int32_t get_offset_of_m_Key_2() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___m_Key_2)); }
	inline LocalizeString_t1691720564 * get_m_Key_2() const { return ___m_Key_2; }
	inline LocalizeString_t1691720564 ** get_address_of_m_Key_2() { return &___m_Key_2; }
	inline void set_m_Key_2(LocalizeString_t1691720564 * value)
	{
		___m_Key_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Key_2, value);
	}

	inline static int32_t get_offset_of_Text_3() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___Text_3)); }
	inline Text_t356221433 * get_Text_3() const { return ___Text_3; }
	inline Text_t356221433 ** get_address_of_Text_3() { return &___Text_3; }
	inline void set_Text_3(Text_t356221433 * value)
	{
		___Text_3 = value;
		Il2CppCodeGenWriteBarrier(&___Text_3, value);
	}

	inline static int32_t get_offset_of_LetterSpacing_4() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___LetterSpacing_4)); }
	inline LetterSpacing_t3080148137 * get_LetterSpacing_4() const { return ___LetterSpacing_4; }
	inline LetterSpacing_t3080148137 ** get_address_of_LetterSpacing_4() { return &___LetterSpacing_4; }
	inline void set_LetterSpacing_4(LetterSpacing_t3080148137 * value)
	{
		___LetterSpacing_4 = value;
		Il2CppCodeGenWriteBarrier(&___LetterSpacing_4, value);
	}

	inline static int32_t get_offset_of_Font_5() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___Font_5)); }
	inline String_t* get_Font_5() const { return ___Font_5; }
	inline String_t** get_address_of_Font_5() { return &___Font_5; }
	inline void set_Font_5(String_t* value)
	{
		___Font_5 = value;
		Il2CppCodeGenWriteBarrier(&___Font_5, value);
	}

	inline static int32_t get_offset_of_Size_6() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___Size_6)); }
	inline int32_t get_Size_6() const { return ___Size_6; }
	inline int32_t* get_address_of_Size_6() { return &___Size_6; }
	inline void set_Size_6(int32_t value)
	{
		___Size_6 = value;
	}

	inline static int32_t get_offset_of_Kerning_7() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___Kerning_7)); }
	inline float get_Kerning_7() const { return ___Kerning_7; }
	inline float* get_address_of_Kerning_7() { return &___Kerning_7; }
	inline void set_Kerning_7(float value)
	{
		___Kerning_7 = value;
	}

	inline static int32_t get_offset_of_Linespacing_8() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___Linespacing_8)); }
	inline float get_Linespacing_8() const { return ___Linespacing_8; }
	inline float* get_address_of_Linespacing_8() { return &___Linespacing_8; }
	inline void set_Linespacing_8(float value)
	{
		___Linespacing_8 = value;
	}

	inline static int32_t get_offset_of_Content_9() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___Content_9)); }
	inline String_t* get_Content_9() const { return ___Content_9; }
	inline String_t** get_address_of_Content_9() { return &___Content_9; }
	inline void set_Content_9(String_t* value)
	{
		___Content_9 = value;
		Il2CppCodeGenWriteBarrier(&___Content_9, value);
	}

	inline static int32_t get_offset_of_IsNotTextUpdate_10() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___IsNotTextUpdate_10)); }
	inline bool get_IsNotTextUpdate_10() const { return ___IsNotTextUpdate_10; }
	inline bool* get_address_of_IsNotTextUpdate_10() { return &___IsNotTextUpdate_10; }
	inline void set_IsNotTextUpdate_10(bool value)
	{
		___IsNotTextUpdate_10 = value;
	}

	inline static int32_t get_offset_of_IsUsedRegexHypertext_11() { return static_cast<int32_t>(offsetof(UILocalizer_t2245030347, ___IsUsedRegexHypertext_11)); }
	inline bool get_IsUsedRegexHypertext_11() const { return ___IsUsedRegexHypertext_11; }
	inline bool* get_address_of_IsUsedRegexHypertext_11() { return &___IsUsedRegexHypertext_11; }
	inline void set_IsUsedRegexHypertext_11(bool value)
	{
		___IsUsedRegexHypertext_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
