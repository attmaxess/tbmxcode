﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1215859820.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<UIObject>
struct List_1_t3648280775;
// System.Collections.Generic.List`1<UIPanel>
struct List_1_t1164206464;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// WorldMapPanel
struct WorldMapPanel_t4256308156;
// ActionPartsPanelController
struct ActionPartsPanelController_t3710630626;
// CreateModeSelect
struct CreateModeSelect_t1995266573;
// MobilimoPartsConect
struct MobilimoPartsConect_t3466715044;
// ModulePartsConect
struct ModulePartsConect_t3833785310;
// MoveSet
struct MoveSet_t2777185973;
// ActionSet
struct ActionSet_t264428022;
// SlideInOut
struct SlideInOut_t958296806;
// RaderController
struct RaderController_t1714548872;
// DemoWorldContents
struct DemoWorldContents_t2362154501;
// MyPageContents
struct MyPageContents_t4122237443;
// Dialog
struct Dialog_t1378192732;
// Prized
struct Prized_t3165049840;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1854387467;
// System.Predicate`1<UIObject>
struct Predicate_1_t2722129758;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_t2519183485  : public SingletonMonoBehaviour_1_t1215859820
{
public:
	// System.Int32 UIManager::m_sUINum
	int32_t ___m_sUINum_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UIManager::UImodules
	List_1_t2644239190 * ___UImodules_4;
	// System.Collections.Generic.List`1<UIObject> UIManager::m_UIList
	List_1_t3648280775 * ___m_UIList_5;
	// System.Collections.Generic.List`1<UIPanel> UIManager::panels
	List_1_t1164206464 * ___panels_6;
	// UnityEngine.GameObject UIManager::ControlUI
	GameObject_t1756533147 * ___ControlUI_7;
	// UnityEngine.GameObject UIManager::ActionButton
	GameObject_t1756533147 * ___ActionButton_8;
	// UnityEngine.GameObject UIManager::Rollstick
	GameObject_t1756533147 * ___Rollstick_9;
	// UnityEngine.GameObject UIManager::LoadingUI
	GameObject_t1756533147 * ___LoadingUI_10;
	// WorldMapPanel UIManager::m_WorldMapUI
	WorldMapPanel_t4256308156 * ___m_WorldMapUI_11;
	// ActionPartsPanelController UIManager::ActionPartsPanelCntr
	ActionPartsPanelController_t3710630626 * ___ActionPartsPanelCntr_12;
	// CreateModeSelect UIManager::m_CreateModeSelect
	CreateModeSelect_t1995266573 * ___m_CreateModeSelect_13;
	// UnityEngine.GameObject UIManager::m_CoreSelectUIGO
	GameObject_t1756533147 * ___m_CoreSelectUIGO_14;
	// UnityEngine.GameObject UIManager::m_CoreSelectModuleUIGO
	GameObject_t1756533147 * ___m_CoreSelectModuleUIGO_15;
	// MobilimoPartsConect UIManager::m_MobilmoPartsConect
	MobilimoPartsConect_t3466715044 * ___m_MobilmoPartsConect_16;
	// ModulePartsConect UIManager::m_ModulePartsConect
	ModulePartsConect_t3833785310 * ___m_ModulePartsConect_17;
	// UnityEngine.GameObject UIManager::m_gBackCoreSelectButton
	GameObject_t1756533147 * ___m_gBackCoreSelectButton_18;
	// UnityEngine.GameObject UIManager::m_gBackSelectButton
	GameObject_t1756533147 * ___m_gBackSelectButton_19;
	// UnityEngine.GameObject UIManager::m_gCompleteButton
	GameObject_t1756533147 * ___m_gCompleteButton_20;
	// UnityEngine.GameObject UIManager::m_gBackCoreSelectModuleButton
	GameObject_t1756533147 * ___m_gBackCoreSelectModuleButton_21;
	// UnityEngine.GameObject UIManager::m_gBackSelectModuleButton
	GameObject_t1756533147 * ___m_gBackSelectModuleButton_22;
	// UnityEngine.GameObject UIManager::m_gCompleteModuleButton
	GameObject_t1756533147 * ___m_gCompleteModuleButton_23;
	// MoveSet UIManager::m_MoveSet
	MoveSet_t2777185973 * ___m_MoveSet_24;
	// ActionSet UIManager::m_ActionSet
	ActionSet_t264428022 * ___m_ActionSet_25;
	// SlideInOut UIManager::m_ActionSlidePanel
	SlideInOut_t958296806 * ___m_ActionSlidePanel_26;
	// UnityEngine.GameObject UIManager::m_ActionNamePanel
	GameObject_t1756533147 * ___m_ActionNamePanel_27;
	// UnityEngine.GameObject UIManager::m_ActionTitle
	GameObject_t1756533147 * ___m_ActionTitle_28;
	// UnityEngine.GameObject UIManager::m_PoseTitle
	GameObject_t1756533147 * ___m_PoseTitle_29;
	// RaderController UIManager::m_RaderCtrl
	RaderController_t1714548872 * ___m_RaderCtrl_30;
	// DemoWorldContents UIManager::m_DemoWorldCOntents
	DemoWorldContents_t2362154501 * ___m_DemoWorldCOntents_31;
	// MyPageContents UIManager::m_MyPageContents
	MyPageContents_t4122237443 * ___m_MyPageContents_32;
	// System.Boolean UIManager::m_bUIPoint
	bool ___m_bUIPoint_33;
	// System.Boolean UIManager::m_bUIClick
	bool ___m_bUIClick_34;
	// Dialog UIManager::dialog
	Dialog_t1378192732 * ___dialog_35;
	// Prized UIManager::prized
	Prized_t3165049840 * ___prized_36;
	// UnityEngine.Vector2 UIManager::BackgroundScrollValue
	Vector2_t2243707579  ___BackgroundScrollValue_37;
	// UnityEngine.Vector2 UIManager::WorldMapBackgroundScrollValue
	Vector2_t2243707579  ___WorldMapBackgroundScrollValue_38;
	// UnityEngine.Quaternion[] UIManager::RotCircles
	QuaternionU5BU5D_t1854387467* ___RotCircles_39;
	// System.Boolean UIManager::IsCirclesSave
	bool ___IsCirclesSave_40;
	// System.Int32 UIManager::test
	int32_t ___test_41;

public:
	inline static int32_t get_offset_of_m_sUINum_3() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_sUINum_3)); }
	inline int32_t get_m_sUINum_3() const { return ___m_sUINum_3; }
	inline int32_t* get_address_of_m_sUINum_3() { return &___m_sUINum_3; }
	inline void set_m_sUINum_3(int32_t value)
	{
		___m_sUINum_3 = value;
	}

	inline static int32_t get_offset_of_UImodules_4() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___UImodules_4)); }
	inline List_1_t2644239190 * get_UImodules_4() const { return ___UImodules_4; }
	inline List_1_t2644239190 ** get_address_of_UImodules_4() { return &___UImodules_4; }
	inline void set_UImodules_4(List_1_t2644239190 * value)
	{
		___UImodules_4 = value;
		Il2CppCodeGenWriteBarrier(&___UImodules_4, value);
	}

	inline static int32_t get_offset_of_m_UIList_5() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_UIList_5)); }
	inline List_1_t3648280775 * get_m_UIList_5() const { return ___m_UIList_5; }
	inline List_1_t3648280775 ** get_address_of_m_UIList_5() { return &___m_UIList_5; }
	inline void set_m_UIList_5(List_1_t3648280775 * value)
	{
		___m_UIList_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_UIList_5, value);
	}

	inline static int32_t get_offset_of_panels_6() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___panels_6)); }
	inline List_1_t1164206464 * get_panels_6() const { return ___panels_6; }
	inline List_1_t1164206464 ** get_address_of_panels_6() { return &___panels_6; }
	inline void set_panels_6(List_1_t1164206464 * value)
	{
		___panels_6 = value;
		Il2CppCodeGenWriteBarrier(&___panels_6, value);
	}

	inline static int32_t get_offset_of_ControlUI_7() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___ControlUI_7)); }
	inline GameObject_t1756533147 * get_ControlUI_7() const { return ___ControlUI_7; }
	inline GameObject_t1756533147 ** get_address_of_ControlUI_7() { return &___ControlUI_7; }
	inline void set_ControlUI_7(GameObject_t1756533147 * value)
	{
		___ControlUI_7 = value;
		Il2CppCodeGenWriteBarrier(&___ControlUI_7, value);
	}

	inline static int32_t get_offset_of_ActionButton_8() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___ActionButton_8)); }
	inline GameObject_t1756533147 * get_ActionButton_8() const { return ___ActionButton_8; }
	inline GameObject_t1756533147 ** get_address_of_ActionButton_8() { return &___ActionButton_8; }
	inline void set_ActionButton_8(GameObject_t1756533147 * value)
	{
		___ActionButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___ActionButton_8, value);
	}

	inline static int32_t get_offset_of_Rollstick_9() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___Rollstick_9)); }
	inline GameObject_t1756533147 * get_Rollstick_9() const { return ___Rollstick_9; }
	inline GameObject_t1756533147 ** get_address_of_Rollstick_9() { return &___Rollstick_9; }
	inline void set_Rollstick_9(GameObject_t1756533147 * value)
	{
		___Rollstick_9 = value;
		Il2CppCodeGenWriteBarrier(&___Rollstick_9, value);
	}

	inline static int32_t get_offset_of_LoadingUI_10() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___LoadingUI_10)); }
	inline GameObject_t1756533147 * get_LoadingUI_10() const { return ___LoadingUI_10; }
	inline GameObject_t1756533147 ** get_address_of_LoadingUI_10() { return &___LoadingUI_10; }
	inline void set_LoadingUI_10(GameObject_t1756533147 * value)
	{
		___LoadingUI_10 = value;
		Il2CppCodeGenWriteBarrier(&___LoadingUI_10, value);
	}

	inline static int32_t get_offset_of_m_WorldMapUI_11() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_WorldMapUI_11)); }
	inline WorldMapPanel_t4256308156 * get_m_WorldMapUI_11() const { return ___m_WorldMapUI_11; }
	inline WorldMapPanel_t4256308156 ** get_address_of_m_WorldMapUI_11() { return &___m_WorldMapUI_11; }
	inline void set_m_WorldMapUI_11(WorldMapPanel_t4256308156 * value)
	{
		___m_WorldMapUI_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_WorldMapUI_11, value);
	}

	inline static int32_t get_offset_of_ActionPartsPanelCntr_12() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___ActionPartsPanelCntr_12)); }
	inline ActionPartsPanelController_t3710630626 * get_ActionPartsPanelCntr_12() const { return ___ActionPartsPanelCntr_12; }
	inline ActionPartsPanelController_t3710630626 ** get_address_of_ActionPartsPanelCntr_12() { return &___ActionPartsPanelCntr_12; }
	inline void set_ActionPartsPanelCntr_12(ActionPartsPanelController_t3710630626 * value)
	{
		___ActionPartsPanelCntr_12 = value;
		Il2CppCodeGenWriteBarrier(&___ActionPartsPanelCntr_12, value);
	}

	inline static int32_t get_offset_of_m_CreateModeSelect_13() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_CreateModeSelect_13)); }
	inline CreateModeSelect_t1995266573 * get_m_CreateModeSelect_13() const { return ___m_CreateModeSelect_13; }
	inline CreateModeSelect_t1995266573 ** get_address_of_m_CreateModeSelect_13() { return &___m_CreateModeSelect_13; }
	inline void set_m_CreateModeSelect_13(CreateModeSelect_t1995266573 * value)
	{
		___m_CreateModeSelect_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_CreateModeSelect_13, value);
	}

	inline static int32_t get_offset_of_m_CoreSelectUIGO_14() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_CoreSelectUIGO_14)); }
	inline GameObject_t1756533147 * get_m_CoreSelectUIGO_14() const { return ___m_CoreSelectUIGO_14; }
	inline GameObject_t1756533147 ** get_address_of_m_CoreSelectUIGO_14() { return &___m_CoreSelectUIGO_14; }
	inline void set_m_CoreSelectUIGO_14(GameObject_t1756533147 * value)
	{
		___m_CoreSelectUIGO_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_CoreSelectUIGO_14, value);
	}

	inline static int32_t get_offset_of_m_CoreSelectModuleUIGO_15() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_CoreSelectModuleUIGO_15)); }
	inline GameObject_t1756533147 * get_m_CoreSelectModuleUIGO_15() const { return ___m_CoreSelectModuleUIGO_15; }
	inline GameObject_t1756533147 ** get_address_of_m_CoreSelectModuleUIGO_15() { return &___m_CoreSelectModuleUIGO_15; }
	inline void set_m_CoreSelectModuleUIGO_15(GameObject_t1756533147 * value)
	{
		___m_CoreSelectModuleUIGO_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_CoreSelectModuleUIGO_15, value);
	}

	inline static int32_t get_offset_of_m_MobilmoPartsConect_16() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_MobilmoPartsConect_16)); }
	inline MobilimoPartsConect_t3466715044 * get_m_MobilmoPartsConect_16() const { return ___m_MobilmoPartsConect_16; }
	inline MobilimoPartsConect_t3466715044 ** get_address_of_m_MobilmoPartsConect_16() { return &___m_MobilmoPartsConect_16; }
	inline void set_m_MobilmoPartsConect_16(MobilimoPartsConect_t3466715044 * value)
	{
		___m_MobilmoPartsConect_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_MobilmoPartsConect_16, value);
	}

	inline static int32_t get_offset_of_m_ModulePartsConect_17() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_ModulePartsConect_17)); }
	inline ModulePartsConect_t3833785310 * get_m_ModulePartsConect_17() const { return ___m_ModulePartsConect_17; }
	inline ModulePartsConect_t3833785310 ** get_address_of_m_ModulePartsConect_17() { return &___m_ModulePartsConect_17; }
	inline void set_m_ModulePartsConect_17(ModulePartsConect_t3833785310 * value)
	{
		___m_ModulePartsConect_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_ModulePartsConect_17, value);
	}

	inline static int32_t get_offset_of_m_gBackCoreSelectButton_18() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_gBackCoreSelectButton_18)); }
	inline GameObject_t1756533147 * get_m_gBackCoreSelectButton_18() const { return ___m_gBackCoreSelectButton_18; }
	inline GameObject_t1756533147 ** get_address_of_m_gBackCoreSelectButton_18() { return &___m_gBackCoreSelectButton_18; }
	inline void set_m_gBackCoreSelectButton_18(GameObject_t1756533147 * value)
	{
		___m_gBackCoreSelectButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_gBackCoreSelectButton_18, value);
	}

	inline static int32_t get_offset_of_m_gBackSelectButton_19() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_gBackSelectButton_19)); }
	inline GameObject_t1756533147 * get_m_gBackSelectButton_19() const { return ___m_gBackSelectButton_19; }
	inline GameObject_t1756533147 ** get_address_of_m_gBackSelectButton_19() { return &___m_gBackSelectButton_19; }
	inline void set_m_gBackSelectButton_19(GameObject_t1756533147 * value)
	{
		___m_gBackSelectButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_gBackSelectButton_19, value);
	}

	inline static int32_t get_offset_of_m_gCompleteButton_20() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_gCompleteButton_20)); }
	inline GameObject_t1756533147 * get_m_gCompleteButton_20() const { return ___m_gCompleteButton_20; }
	inline GameObject_t1756533147 ** get_address_of_m_gCompleteButton_20() { return &___m_gCompleteButton_20; }
	inline void set_m_gCompleteButton_20(GameObject_t1756533147 * value)
	{
		___m_gCompleteButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_gCompleteButton_20, value);
	}

	inline static int32_t get_offset_of_m_gBackCoreSelectModuleButton_21() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_gBackCoreSelectModuleButton_21)); }
	inline GameObject_t1756533147 * get_m_gBackCoreSelectModuleButton_21() const { return ___m_gBackCoreSelectModuleButton_21; }
	inline GameObject_t1756533147 ** get_address_of_m_gBackCoreSelectModuleButton_21() { return &___m_gBackCoreSelectModuleButton_21; }
	inline void set_m_gBackCoreSelectModuleButton_21(GameObject_t1756533147 * value)
	{
		___m_gBackCoreSelectModuleButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_gBackCoreSelectModuleButton_21, value);
	}

	inline static int32_t get_offset_of_m_gBackSelectModuleButton_22() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_gBackSelectModuleButton_22)); }
	inline GameObject_t1756533147 * get_m_gBackSelectModuleButton_22() const { return ___m_gBackSelectModuleButton_22; }
	inline GameObject_t1756533147 ** get_address_of_m_gBackSelectModuleButton_22() { return &___m_gBackSelectModuleButton_22; }
	inline void set_m_gBackSelectModuleButton_22(GameObject_t1756533147 * value)
	{
		___m_gBackSelectModuleButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_gBackSelectModuleButton_22, value);
	}

	inline static int32_t get_offset_of_m_gCompleteModuleButton_23() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_gCompleteModuleButton_23)); }
	inline GameObject_t1756533147 * get_m_gCompleteModuleButton_23() const { return ___m_gCompleteModuleButton_23; }
	inline GameObject_t1756533147 ** get_address_of_m_gCompleteModuleButton_23() { return &___m_gCompleteModuleButton_23; }
	inline void set_m_gCompleteModuleButton_23(GameObject_t1756533147 * value)
	{
		___m_gCompleteModuleButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_gCompleteModuleButton_23, value);
	}

	inline static int32_t get_offset_of_m_MoveSet_24() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_MoveSet_24)); }
	inline MoveSet_t2777185973 * get_m_MoveSet_24() const { return ___m_MoveSet_24; }
	inline MoveSet_t2777185973 ** get_address_of_m_MoveSet_24() { return &___m_MoveSet_24; }
	inline void set_m_MoveSet_24(MoveSet_t2777185973 * value)
	{
		___m_MoveSet_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_MoveSet_24, value);
	}

	inline static int32_t get_offset_of_m_ActionSet_25() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_ActionSet_25)); }
	inline ActionSet_t264428022 * get_m_ActionSet_25() const { return ___m_ActionSet_25; }
	inline ActionSet_t264428022 ** get_address_of_m_ActionSet_25() { return &___m_ActionSet_25; }
	inline void set_m_ActionSet_25(ActionSet_t264428022 * value)
	{
		___m_ActionSet_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionSet_25, value);
	}

	inline static int32_t get_offset_of_m_ActionSlidePanel_26() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_ActionSlidePanel_26)); }
	inline SlideInOut_t958296806 * get_m_ActionSlidePanel_26() const { return ___m_ActionSlidePanel_26; }
	inline SlideInOut_t958296806 ** get_address_of_m_ActionSlidePanel_26() { return &___m_ActionSlidePanel_26; }
	inline void set_m_ActionSlidePanel_26(SlideInOut_t958296806 * value)
	{
		___m_ActionSlidePanel_26 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionSlidePanel_26, value);
	}

	inline static int32_t get_offset_of_m_ActionNamePanel_27() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_ActionNamePanel_27)); }
	inline GameObject_t1756533147 * get_m_ActionNamePanel_27() const { return ___m_ActionNamePanel_27; }
	inline GameObject_t1756533147 ** get_address_of_m_ActionNamePanel_27() { return &___m_ActionNamePanel_27; }
	inline void set_m_ActionNamePanel_27(GameObject_t1756533147 * value)
	{
		___m_ActionNamePanel_27 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionNamePanel_27, value);
	}

	inline static int32_t get_offset_of_m_ActionTitle_28() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_ActionTitle_28)); }
	inline GameObject_t1756533147 * get_m_ActionTitle_28() const { return ___m_ActionTitle_28; }
	inline GameObject_t1756533147 ** get_address_of_m_ActionTitle_28() { return &___m_ActionTitle_28; }
	inline void set_m_ActionTitle_28(GameObject_t1756533147 * value)
	{
		___m_ActionTitle_28 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActionTitle_28, value);
	}

	inline static int32_t get_offset_of_m_PoseTitle_29() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_PoseTitle_29)); }
	inline GameObject_t1756533147 * get_m_PoseTitle_29() const { return ___m_PoseTitle_29; }
	inline GameObject_t1756533147 ** get_address_of_m_PoseTitle_29() { return &___m_PoseTitle_29; }
	inline void set_m_PoseTitle_29(GameObject_t1756533147 * value)
	{
		___m_PoseTitle_29 = value;
		Il2CppCodeGenWriteBarrier(&___m_PoseTitle_29, value);
	}

	inline static int32_t get_offset_of_m_RaderCtrl_30() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_RaderCtrl_30)); }
	inline RaderController_t1714548872 * get_m_RaderCtrl_30() const { return ___m_RaderCtrl_30; }
	inline RaderController_t1714548872 ** get_address_of_m_RaderCtrl_30() { return &___m_RaderCtrl_30; }
	inline void set_m_RaderCtrl_30(RaderController_t1714548872 * value)
	{
		___m_RaderCtrl_30 = value;
		Il2CppCodeGenWriteBarrier(&___m_RaderCtrl_30, value);
	}

	inline static int32_t get_offset_of_m_DemoWorldCOntents_31() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_DemoWorldCOntents_31)); }
	inline DemoWorldContents_t2362154501 * get_m_DemoWorldCOntents_31() const { return ___m_DemoWorldCOntents_31; }
	inline DemoWorldContents_t2362154501 ** get_address_of_m_DemoWorldCOntents_31() { return &___m_DemoWorldCOntents_31; }
	inline void set_m_DemoWorldCOntents_31(DemoWorldContents_t2362154501 * value)
	{
		___m_DemoWorldCOntents_31 = value;
		Il2CppCodeGenWriteBarrier(&___m_DemoWorldCOntents_31, value);
	}

	inline static int32_t get_offset_of_m_MyPageContents_32() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_MyPageContents_32)); }
	inline MyPageContents_t4122237443 * get_m_MyPageContents_32() const { return ___m_MyPageContents_32; }
	inline MyPageContents_t4122237443 ** get_address_of_m_MyPageContents_32() { return &___m_MyPageContents_32; }
	inline void set_m_MyPageContents_32(MyPageContents_t4122237443 * value)
	{
		___m_MyPageContents_32 = value;
		Il2CppCodeGenWriteBarrier(&___m_MyPageContents_32, value);
	}

	inline static int32_t get_offset_of_m_bUIPoint_33() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_bUIPoint_33)); }
	inline bool get_m_bUIPoint_33() const { return ___m_bUIPoint_33; }
	inline bool* get_address_of_m_bUIPoint_33() { return &___m_bUIPoint_33; }
	inline void set_m_bUIPoint_33(bool value)
	{
		___m_bUIPoint_33 = value;
	}

	inline static int32_t get_offset_of_m_bUIClick_34() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___m_bUIClick_34)); }
	inline bool get_m_bUIClick_34() const { return ___m_bUIClick_34; }
	inline bool* get_address_of_m_bUIClick_34() { return &___m_bUIClick_34; }
	inline void set_m_bUIClick_34(bool value)
	{
		___m_bUIClick_34 = value;
	}

	inline static int32_t get_offset_of_dialog_35() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___dialog_35)); }
	inline Dialog_t1378192732 * get_dialog_35() const { return ___dialog_35; }
	inline Dialog_t1378192732 ** get_address_of_dialog_35() { return &___dialog_35; }
	inline void set_dialog_35(Dialog_t1378192732 * value)
	{
		___dialog_35 = value;
		Il2CppCodeGenWriteBarrier(&___dialog_35, value);
	}

	inline static int32_t get_offset_of_prized_36() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___prized_36)); }
	inline Prized_t3165049840 * get_prized_36() const { return ___prized_36; }
	inline Prized_t3165049840 ** get_address_of_prized_36() { return &___prized_36; }
	inline void set_prized_36(Prized_t3165049840 * value)
	{
		___prized_36 = value;
		Il2CppCodeGenWriteBarrier(&___prized_36, value);
	}

	inline static int32_t get_offset_of_BackgroundScrollValue_37() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___BackgroundScrollValue_37)); }
	inline Vector2_t2243707579  get_BackgroundScrollValue_37() const { return ___BackgroundScrollValue_37; }
	inline Vector2_t2243707579 * get_address_of_BackgroundScrollValue_37() { return &___BackgroundScrollValue_37; }
	inline void set_BackgroundScrollValue_37(Vector2_t2243707579  value)
	{
		___BackgroundScrollValue_37 = value;
	}

	inline static int32_t get_offset_of_WorldMapBackgroundScrollValue_38() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___WorldMapBackgroundScrollValue_38)); }
	inline Vector2_t2243707579  get_WorldMapBackgroundScrollValue_38() const { return ___WorldMapBackgroundScrollValue_38; }
	inline Vector2_t2243707579 * get_address_of_WorldMapBackgroundScrollValue_38() { return &___WorldMapBackgroundScrollValue_38; }
	inline void set_WorldMapBackgroundScrollValue_38(Vector2_t2243707579  value)
	{
		___WorldMapBackgroundScrollValue_38 = value;
	}

	inline static int32_t get_offset_of_RotCircles_39() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___RotCircles_39)); }
	inline QuaternionU5BU5D_t1854387467* get_RotCircles_39() const { return ___RotCircles_39; }
	inline QuaternionU5BU5D_t1854387467** get_address_of_RotCircles_39() { return &___RotCircles_39; }
	inline void set_RotCircles_39(QuaternionU5BU5D_t1854387467* value)
	{
		___RotCircles_39 = value;
		Il2CppCodeGenWriteBarrier(&___RotCircles_39, value);
	}

	inline static int32_t get_offset_of_IsCirclesSave_40() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___IsCirclesSave_40)); }
	inline bool get_IsCirclesSave_40() const { return ___IsCirclesSave_40; }
	inline bool* get_address_of_IsCirclesSave_40() { return &___IsCirclesSave_40; }
	inline void set_IsCirclesSave_40(bool value)
	{
		___IsCirclesSave_40 = value;
	}

	inline static int32_t get_offset_of_test_41() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___test_41)); }
	inline int32_t get_test_41() const { return ___test_41; }
	inline int32_t* get_address_of_test_41() { return &___test_41; }
	inline void set_test_41(int32_t value)
	{
		___test_41 = value;
	}
};

struct UIManager_t2519183485_StaticFields
{
public:
	// System.Predicate`1<UIObject> UIManager::<>f__am$cache0
	Predicate_1_t2722129758 * ___U3CU3Ef__amU24cache0_42;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_42() { return static_cast<int32_t>(offsetof(UIManager_t2519183485_StaticFields, ___U3CU3Ef__amU24cache0_42)); }
	inline Predicate_1_t2722129758 * get_U3CU3Ef__amU24cache0_42() const { return ___U3CU3Ef__amU24cache0_42; }
	inline Predicate_1_t2722129758 ** get_address_of_U3CU3Ef__amU24cache0_42() { return &___U3CU3Ef__amU24cache0_42; }
	inline void set_U3CU3Ef__amU24cache0_42(Predicate_1_t2722129758 * value)
	{
		___U3CU3Ef__amU24cache0_42 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_42, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
