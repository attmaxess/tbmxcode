﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_eSCENETYPE4122992453.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager/<UIChange>c__AnonStorey1
struct  U3CUIChangeU3Ec__AnonStorey1_t3981264818  : public Il2CppObject
{
public:
	// eSCENETYPE UIManager/<UIChange>c__AnonStorey1::uiType
	int32_t ___uiType_0;

public:
	inline static int32_t get_offset_of_uiType_0() { return static_cast<int32_t>(offsetof(U3CUIChangeU3Ec__AnonStorey1_t3981264818, ___uiType_0)); }
	inline int32_t get_uiType_0() const { return ___uiType_0; }
	inline int32_t* get_address_of_uiType_0() { return &___uiType_0; }
	inline void set_uiType_0(int32_t value)
	{
		___uiType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
