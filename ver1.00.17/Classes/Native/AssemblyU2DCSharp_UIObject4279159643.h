﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_eSCENETYPE4122992453.h"

// System.Collections.Generic.List`1<UIPanel>
struct List_1_t1164206464;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIObject
struct  UIObject_t4279159643  : public Il2CppObject
{
public:
	// eSCENETYPE UIObject::SceneType
	int32_t ___SceneType_0;
	// System.Collections.Generic.List`1<UIPanel> UIObject::objs
	List_1_t1164206464 * ___objs_1;
	// System.Collections.Generic.List`1<UIPanel> UIObject::objs_ignore
	List_1_t1164206464 * ___objs_ignore_2;
	// System.Boolean UIObject::isDefault
	bool ___isDefault_3;

public:
	inline static int32_t get_offset_of_SceneType_0() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___SceneType_0)); }
	inline int32_t get_SceneType_0() const { return ___SceneType_0; }
	inline int32_t* get_address_of_SceneType_0() { return &___SceneType_0; }
	inline void set_SceneType_0(int32_t value)
	{
		___SceneType_0 = value;
	}

	inline static int32_t get_offset_of_objs_1() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___objs_1)); }
	inline List_1_t1164206464 * get_objs_1() const { return ___objs_1; }
	inline List_1_t1164206464 ** get_address_of_objs_1() { return &___objs_1; }
	inline void set_objs_1(List_1_t1164206464 * value)
	{
		___objs_1 = value;
		Il2CppCodeGenWriteBarrier(&___objs_1, value);
	}

	inline static int32_t get_offset_of_objs_ignore_2() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___objs_ignore_2)); }
	inline List_1_t1164206464 * get_objs_ignore_2() const { return ___objs_ignore_2; }
	inline List_1_t1164206464 ** get_address_of_objs_ignore_2() { return &___objs_ignore_2; }
	inline void set_objs_ignore_2(List_1_t1164206464 * value)
	{
		___objs_ignore_2 = value;
		Il2CppCodeGenWriteBarrier(&___objs_ignore_2, value);
	}

	inline static int32_t get_offset_of_isDefault_3() { return static_cast<int32_t>(offsetof(UIObject_t4279159643, ___isDefault_3)); }
	inline bool get_isDefault_3() const { return ___isDefault_3; }
	inline bool* get_address_of_isDefault_3() { return &___isDefault_3; }
	inline void set_isDefault_3(bool value)
	{
		___isDefault_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
