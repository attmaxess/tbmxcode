﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_FogMode2386547659.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Underwater
struct  Underwater_t1731910439  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Underwater::UnderwaterLevel
	float ___UnderwaterLevel_2;
	// System.Single Underwater::UnderwaterLevelx
	float ___UnderwaterLevelx_3;
	// UnityEngine.Color Underwater::FogColor
	Color_t2020392075  ___FogColor_4;
	// System.Single Underwater::FogDensity
	float ___FogDensity_5;
	// UnityEngine.FogMode Underwater::FogMode
	int32_t ___FogMode_6;
	// System.Boolean Underwater::defaultFog
	bool ___defaultFog_7;
	// UnityEngine.Color Underwater::defaultFogColor
	Color_t2020392075  ___defaultFogColor_8;
	// System.Single Underwater::defaultFogDensity
	float ___defaultFogDensity_9;
	// UnityEngine.FogMode Underwater::defaultFogMod
	int32_t ___defaultFogMod_10;
	// UnityEngine.Material Underwater::defaultSkybox
	Material_t193706927 * ___defaultSkybox_11;

public:
	inline static int32_t get_offset_of_UnderwaterLevel_2() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___UnderwaterLevel_2)); }
	inline float get_UnderwaterLevel_2() const { return ___UnderwaterLevel_2; }
	inline float* get_address_of_UnderwaterLevel_2() { return &___UnderwaterLevel_2; }
	inline void set_UnderwaterLevel_2(float value)
	{
		___UnderwaterLevel_2 = value;
	}

	inline static int32_t get_offset_of_UnderwaterLevelx_3() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___UnderwaterLevelx_3)); }
	inline float get_UnderwaterLevelx_3() const { return ___UnderwaterLevelx_3; }
	inline float* get_address_of_UnderwaterLevelx_3() { return &___UnderwaterLevelx_3; }
	inline void set_UnderwaterLevelx_3(float value)
	{
		___UnderwaterLevelx_3 = value;
	}

	inline static int32_t get_offset_of_FogColor_4() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___FogColor_4)); }
	inline Color_t2020392075  get_FogColor_4() const { return ___FogColor_4; }
	inline Color_t2020392075 * get_address_of_FogColor_4() { return &___FogColor_4; }
	inline void set_FogColor_4(Color_t2020392075  value)
	{
		___FogColor_4 = value;
	}

	inline static int32_t get_offset_of_FogDensity_5() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___FogDensity_5)); }
	inline float get_FogDensity_5() const { return ___FogDensity_5; }
	inline float* get_address_of_FogDensity_5() { return &___FogDensity_5; }
	inline void set_FogDensity_5(float value)
	{
		___FogDensity_5 = value;
	}

	inline static int32_t get_offset_of_FogMode_6() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___FogMode_6)); }
	inline int32_t get_FogMode_6() const { return ___FogMode_6; }
	inline int32_t* get_address_of_FogMode_6() { return &___FogMode_6; }
	inline void set_FogMode_6(int32_t value)
	{
		___FogMode_6 = value;
	}

	inline static int32_t get_offset_of_defaultFog_7() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___defaultFog_7)); }
	inline bool get_defaultFog_7() const { return ___defaultFog_7; }
	inline bool* get_address_of_defaultFog_7() { return &___defaultFog_7; }
	inline void set_defaultFog_7(bool value)
	{
		___defaultFog_7 = value;
	}

	inline static int32_t get_offset_of_defaultFogColor_8() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___defaultFogColor_8)); }
	inline Color_t2020392075  get_defaultFogColor_8() const { return ___defaultFogColor_8; }
	inline Color_t2020392075 * get_address_of_defaultFogColor_8() { return &___defaultFogColor_8; }
	inline void set_defaultFogColor_8(Color_t2020392075  value)
	{
		___defaultFogColor_8 = value;
	}

	inline static int32_t get_offset_of_defaultFogDensity_9() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___defaultFogDensity_9)); }
	inline float get_defaultFogDensity_9() const { return ___defaultFogDensity_9; }
	inline float* get_address_of_defaultFogDensity_9() { return &___defaultFogDensity_9; }
	inline void set_defaultFogDensity_9(float value)
	{
		___defaultFogDensity_9 = value;
	}

	inline static int32_t get_offset_of_defaultFogMod_10() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___defaultFogMod_10)); }
	inline int32_t get_defaultFogMod_10() const { return ___defaultFogMod_10; }
	inline int32_t* get_address_of_defaultFogMod_10() { return &___defaultFogMod_10; }
	inline void set_defaultFogMod_10(int32_t value)
	{
		___defaultFogMod_10 = value;
	}

	inline static int32_t get_offset_of_defaultSkybox_11() { return static_cast<int32_t>(offsetof(Underwater_t1731910439, ___defaultSkybox_11)); }
	inline Material_t193706927 * get_defaultSkybox_11() const { return ___defaultSkybox_11; }
	inline Material_t193706927 ** get_address_of_defaultSkybox_11() { return &___defaultSkybox_11; }
	inline void set_defaultSkybox_11(Material_t193706927 * value)
	{
		___defaultSkybox_11 = value;
		Il2CppCodeGenWriteBarrier(&___defaultSkybox_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
