﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ShaftsScreenBlen616022271.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Camera
struct Camera_t189460977;
// SunShafts
struct SunShafts_t482045181;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnderwaterPostEffects
struct  UnderwaterPostEffects_t3806762907  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color UnderwaterPostEffects::FogColor
	Color_t2020392075  ___FogColor_2;
	// System.Single UnderwaterPostEffects::FogDensity
	float ___FogDensity_3;
	// System.Boolean UnderwaterPostEffects::UseSunShafts
	bool ___UseSunShafts_4;
	// System.Single UnderwaterPostEffects::SunShuftsIntensity
	float ___SunShuftsIntensity_5;
	// ShaftsScreenBlendMode UnderwaterPostEffects::SunShuftsScreenBlendMode
	int32_t ___SunShuftsScreenBlendMode_6;
	// UnityEngine.Vector3 UnderwaterPostEffects::SunShaftTargetPosition
	Vector3_t2243707580  ___SunShaftTargetPosition_7;
	// UnityEngine.Camera UnderwaterPostEffects::cam
	Camera_t189460977 * ___cam_8;
	// SunShafts UnderwaterPostEffects::SunShafts
	SunShafts_t482045181 * ___SunShafts_9;

public:
	inline static int32_t get_offset_of_FogColor_2() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___FogColor_2)); }
	inline Color_t2020392075  get_FogColor_2() const { return ___FogColor_2; }
	inline Color_t2020392075 * get_address_of_FogColor_2() { return &___FogColor_2; }
	inline void set_FogColor_2(Color_t2020392075  value)
	{
		___FogColor_2 = value;
	}

	inline static int32_t get_offset_of_FogDensity_3() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___FogDensity_3)); }
	inline float get_FogDensity_3() const { return ___FogDensity_3; }
	inline float* get_address_of_FogDensity_3() { return &___FogDensity_3; }
	inline void set_FogDensity_3(float value)
	{
		___FogDensity_3 = value;
	}

	inline static int32_t get_offset_of_UseSunShafts_4() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___UseSunShafts_4)); }
	inline bool get_UseSunShafts_4() const { return ___UseSunShafts_4; }
	inline bool* get_address_of_UseSunShafts_4() { return &___UseSunShafts_4; }
	inline void set_UseSunShafts_4(bool value)
	{
		___UseSunShafts_4 = value;
	}

	inline static int32_t get_offset_of_SunShuftsIntensity_5() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___SunShuftsIntensity_5)); }
	inline float get_SunShuftsIntensity_5() const { return ___SunShuftsIntensity_5; }
	inline float* get_address_of_SunShuftsIntensity_5() { return &___SunShuftsIntensity_5; }
	inline void set_SunShuftsIntensity_5(float value)
	{
		___SunShuftsIntensity_5 = value;
	}

	inline static int32_t get_offset_of_SunShuftsScreenBlendMode_6() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___SunShuftsScreenBlendMode_6)); }
	inline int32_t get_SunShuftsScreenBlendMode_6() const { return ___SunShuftsScreenBlendMode_6; }
	inline int32_t* get_address_of_SunShuftsScreenBlendMode_6() { return &___SunShuftsScreenBlendMode_6; }
	inline void set_SunShuftsScreenBlendMode_6(int32_t value)
	{
		___SunShuftsScreenBlendMode_6 = value;
	}

	inline static int32_t get_offset_of_SunShaftTargetPosition_7() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___SunShaftTargetPosition_7)); }
	inline Vector3_t2243707580  get_SunShaftTargetPosition_7() const { return ___SunShaftTargetPosition_7; }
	inline Vector3_t2243707580 * get_address_of_SunShaftTargetPosition_7() { return &___SunShaftTargetPosition_7; }
	inline void set_SunShaftTargetPosition_7(Vector3_t2243707580  value)
	{
		___SunShaftTargetPosition_7 = value;
	}

	inline static int32_t get_offset_of_cam_8() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___cam_8)); }
	inline Camera_t189460977 * get_cam_8() const { return ___cam_8; }
	inline Camera_t189460977 ** get_address_of_cam_8() { return &___cam_8; }
	inline void set_cam_8(Camera_t189460977 * value)
	{
		___cam_8 = value;
		Il2CppCodeGenWriteBarrier(&___cam_8, value);
	}

	inline static int32_t get_offset_of_SunShafts_9() { return static_cast<int32_t>(offsetof(UnderwaterPostEffects_t3806762907, ___SunShafts_9)); }
	inline SunShafts_t482045181 * get_SunShafts_9() const { return ___SunShafts_9; }
	inline SunShafts_t482045181 ** get_address_of_SunShafts_9() { return &___SunShafts_9; }
	inline void set_SunShafts_9(SunShafts_t482045181 * value)
	{
		___SunShafts_9 = value;
		Il2CppCodeGenWriteBarrier(&___SunShafts_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
