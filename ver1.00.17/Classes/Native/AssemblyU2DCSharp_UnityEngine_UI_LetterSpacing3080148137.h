﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LetterSpacing
struct  LetterSpacing_t3080148137  : public BaseMeshEffect_t1728560551
{
public:
	// System.Boolean UnityEngine.UI.LetterSpacing::useRichText
	bool ___useRichText_4;
	// System.Single UnityEngine.UI.LetterSpacing::m_spacing
	float ___m_spacing_5;

public:
	inline static int32_t get_offset_of_useRichText_4() { return static_cast<int32_t>(offsetof(LetterSpacing_t3080148137, ___useRichText_4)); }
	inline bool get_useRichText_4() const { return ___useRichText_4; }
	inline bool* get_address_of_useRichText_4() { return &___useRichText_4; }
	inline void set_useRichText_4(bool value)
	{
		___useRichText_4 = value;
	}

	inline static int32_t get_offset_of_m_spacing_5() { return static_cast<int32_t>(offsetof(LetterSpacing_t3080148137, ___m_spacing_5)); }
	inline float get_m_spacing_5() const { return ___m_spacing_5; }
	inline float* get_address_of_m_spacing_5() { return &___m_spacing_5; }
	inline void set_m_spacing_5(float value)
	{
		___m_spacing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
