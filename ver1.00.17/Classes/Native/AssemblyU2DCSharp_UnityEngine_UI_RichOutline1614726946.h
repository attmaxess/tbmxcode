﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RichOutline
struct  RichOutline_t1614726946  : public Outline_t1417504278
{
public:
	// System.Int32 UnityEngine.UI.RichOutline::copyCount
	int32_t ___copyCount_7;

public:
	inline static int32_t get_offset_of_copyCount_7() { return static_cast<int32_t>(offsetof(RichOutline_t1614726946, ___copyCount_7)); }
	inline int32_t get_copyCount_7() const { return ___copyCount_7; }
	inline int32_t* get_address_of_copyCount_7() { return &___copyCount_7; }
	inline void set_copyCount_7(int32_t value)
	{
		___copyCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
