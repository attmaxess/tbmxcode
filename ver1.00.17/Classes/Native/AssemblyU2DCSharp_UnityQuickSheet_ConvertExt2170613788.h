﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`2<System.Char,System.Boolean>
struct Func_2_t1675079469;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2212564818;
// System.Func`2<System.Object,System.Double>
struct Func_2_t4214070567;
// System.Func`2<System.Object,System.Int16>
struct Func_2_t4177300800;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2207932334;
// System.Func`2<System.Object,System.Int64>
struct Func_2_t1045132923;
// System.Func`2<System.Object,System.String>
struct Func_2_t2165275119;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityQuickSheet.ConvertExt
struct  ConvertExt_t2170613788  : public Il2CppObject
{
public:

public:
};

struct ConvertExt_t2170613788_StaticFields
{
public:
	// System.Func`2<System.Char,System.Boolean> UnityQuickSheet.ConvertExt::<>f__am$cache0
	Func_2_t1675079469 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Object,System.Object> UnityQuickSheet.ConvertExt::<>f__am$cache1
	Func_2_t2825504181 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<System.Object,System.Single> UnityQuickSheet.ConvertExt::<>f__am$cache2
	Func_2_t2212564818 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<System.Object,System.Object> UnityQuickSheet.ConvertExt::<>f__am$cache3
	Func_2_t2825504181 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<System.Object,System.Double> UnityQuickSheet.ConvertExt::<>f__am$cache4
	Func_2_t4214070567 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<System.Object,System.Object> UnityQuickSheet.ConvertExt::<>f__am$cache5
	Func_2_t2825504181 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<System.Object,System.Int16> UnityQuickSheet.ConvertExt::<>f__am$cache6
	Func_2_t4177300800 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<System.Object,System.Object> UnityQuickSheet.ConvertExt::<>f__am$cache7
	Func_2_t2825504181 * ___U3CU3Ef__amU24cache7_7;
	// System.Func`2<System.Object,System.Int32> UnityQuickSheet.ConvertExt::<>f__am$cache8
	Func_2_t2207932334 * ___U3CU3Ef__amU24cache8_8;
	// System.Func`2<System.Object,System.Object> UnityQuickSheet.ConvertExt::<>f__am$cache9
	Func_2_t2825504181 * ___U3CU3Ef__amU24cache9_9;
	// System.Func`2<System.Object,System.Int64> UnityQuickSheet.ConvertExt::<>f__am$cacheA
	Func_2_t1045132923 * ___U3CU3Ef__amU24cacheA_10;
	// System.Func`2<System.Object,System.Object> UnityQuickSheet.ConvertExt::<>f__am$cacheB
	Func_2_t2825504181 * ___U3CU3Ef__amU24cacheB_11;
	// System.Func`2<System.Object,System.String> UnityQuickSheet.ConvertExt::<>f__am$cacheC
	Func_2_t2165275119 * ___U3CU3Ef__amU24cacheC_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1675079469 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1675079469 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1675079469 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t2212564818 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t2212564818 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t2212564818 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t4214070567 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t4214070567 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t4214070567 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t4177300800 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t4177300800 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t4177300800 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Func_2_t2207932334 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Func_2_t2207932334 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Func_2_t2207932334 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline Func_2_t1045132923 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline Func_2_t1045132923 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(Func_2_t1045132923 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(ConvertExt_t2170613788_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline Func_2_t2165275119 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline Func_2_t2165275119 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(Func_2_t2165275119 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
