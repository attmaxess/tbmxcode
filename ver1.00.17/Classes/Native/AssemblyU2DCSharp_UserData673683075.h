﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Model873752437.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserData
struct  UserData_t673683075  : public Model_t873752437
{
public:
	// System.Int32 UserData::<userId>k__BackingField
	int32_t ___U3CuserIdU3Ek__BackingField_0;
	// System.String UserData::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String UserData::<rank>k__BackingField
	String_t* ___U3CrankU3Ek__BackingField_2;
	// System.String UserData::<reactionType>k__BackingField
	String_t* ___U3CreactionTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CuserIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserData_t673683075, ___U3CuserIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CuserIdU3Ek__BackingField_0() const { return ___U3CuserIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CuserIdU3Ek__BackingField_0() { return &___U3CuserIdU3Ek__BackingField_0; }
	inline void set_U3CuserIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CuserIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserData_t673683075, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CrankU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UserData_t673683075, ___U3CrankU3Ek__BackingField_2)); }
	inline String_t* get_U3CrankU3Ek__BackingField_2() const { return ___U3CrankU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CrankU3Ek__BackingField_2() { return &___U3CrankU3Ek__BackingField_2; }
	inline void set_U3CrankU3Ek__BackingField_2(String_t* value)
	{
		___U3CrankU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrankU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CreactionTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UserData_t673683075, ___U3CreactionTypeU3Ek__BackingField_3)); }
	inline String_t* get_U3CreactionTypeU3Ek__BackingField_3() const { return ___U3CreactionTypeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CreactionTypeU3Ek__BackingField_3() { return &___U3CreactionTypeU3Ek__BackingField_3; }
	inline void set_U3CreactionTypeU3Ek__BackingField_3(String_t* value)
	{
		___U3CreactionTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreactionTypeU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
