﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_eNAMEANIMATE2125136975.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// BackgroundFader
struct BackgroundFader_t837055620;
// NamePanel
struct NamePanel_t546707703;
// InputMarkAnimator
struct InputMarkAnimator_t3461548916;
// RegisterButtonCrossFader
struct RegisterButtonCrossFader_t205138403;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserNameCreator
struct  UserNameCreator_t3044989708  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UserNameCreator::Background
	GameObject_t1756533147 * ___Background_2;
	// UnityEngine.GameObject[] UserNameCreator::LineObjs
	GameObjectU5BU5D_t3057952154* ___LineObjs_3;
	// UnityEngine.RectTransform UserNameCreator::PanelTrans
	RectTransform_t3349966182 * ___PanelTrans_4;
	// UnityEngine.RectTransform UserNameCreator::CanvasTrans
	RectTransform_t3349966182 * ___CanvasTrans_5;
	// UnityEngine.UI.InputField UserNameCreator::InputField
	InputField_t1631627530 * ___InputField_6;
	// UnityEngine.UI.Text UserNameCreator::ExplainText
	Text_t356221433 * ___ExplainText_7;
	// UnityEngine.UI.Text UserNameCreator::PermitText
	Text_t356221433 * ___PermitText_8;
	// UnityEngine.GameObject UserNameCreator::NamePanel
	GameObject_t1756533147 * ___NamePanel_9;
	// UnityEngine.GameObject UserNameCreator::SystemInput
	GameObject_t1756533147 * ___SystemInput_10;
	// System.Single UserNameCreator::PermitTime
	float ___PermitTime_11;
	// System.Single UserNameCreator::m_fWidth
	float ___m_fWidth_12;
	// System.Single UserNameCreator::m_fHeight
	float ___m_fHeight_13;
	// UnityEngine.Vector3 UserNameCreator::m_vPanelPos
	Vector3_t2243707580  ___m_vPanelPos_14;
	// UnityEngine.Vector3 UserNameCreator::m_vLocalCanvasScale
	Vector3_t2243707580  ___m_vLocalCanvasScale_15;
	// UnityEngine.Vector3[] UserNameCreator::LinePos
	Vector3U5BU5D_t1172311765* ___LinePos_16;
	// eNAMEANIMATE UserNameCreator::m_NameAnimate
	int32_t ___m_NameAnimate_17;
	// System.Boolean UserNameCreator::m_bFadeIn
	bool ___m_bFadeIn_18;
	// System.Boolean UserNameCreator::m_bFadeOut
	bool ___m_bFadeOut_19;
	// BackgroundFader UserNameCreator::m_BackgroundFader
	BackgroundFader_t837055620 * ___m_BackgroundFader_20;
	// System.Boolean UserNameCreator::m_bOnLine
	bool ___m_bOnLine_21;
	// NamePanel UserNameCreator::m_NamePanel
	NamePanel_t546707703 * ___m_NamePanel_22;
	// InputMarkAnimator UserNameCreator::m_InputMarkAnimator
	InputMarkAnimator_t3461548916 * ___m_InputMarkAnimator_23;
	// RegisterButtonCrossFader UserNameCreator::m_Buttons
	RegisterButtonCrossFader_t205138403 * ___m_Buttons_24;
	// System.Boolean[] UserNameCreator::m_bFlowLines
	BooleanU5BU5D_t3568034315* ___m_bFlowLines_25;

public:
	inline static int32_t get_offset_of_Background_2() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___Background_2)); }
	inline GameObject_t1756533147 * get_Background_2() const { return ___Background_2; }
	inline GameObject_t1756533147 ** get_address_of_Background_2() { return &___Background_2; }
	inline void set_Background_2(GameObject_t1756533147 * value)
	{
		___Background_2 = value;
		Il2CppCodeGenWriteBarrier(&___Background_2, value);
	}

	inline static int32_t get_offset_of_LineObjs_3() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___LineObjs_3)); }
	inline GameObjectU5BU5D_t3057952154* get_LineObjs_3() const { return ___LineObjs_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_LineObjs_3() { return &___LineObjs_3; }
	inline void set_LineObjs_3(GameObjectU5BU5D_t3057952154* value)
	{
		___LineObjs_3 = value;
		Il2CppCodeGenWriteBarrier(&___LineObjs_3, value);
	}

	inline static int32_t get_offset_of_PanelTrans_4() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___PanelTrans_4)); }
	inline RectTransform_t3349966182 * get_PanelTrans_4() const { return ___PanelTrans_4; }
	inline RectTransform_t3349966182 ** get_address_of_PanelTrans_4() { return &___PanelTrans_4; }
	inline void set_PanelTrans_4(RectTransform_t3349966182 * value)
	{
		___PanelTrans_4 = value;
		Il2CppCodeGenWriteBarrier(&___PanelTrans_4, value);
	}

	inline static int32_t get_offset_of_CanvasTrans_5() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___CanvasTrans_5)); }
	inline RectTransform_t3349966182 * get_CanvasTrans_5() const { return ___CanvasTrans_5; }
	inline RectTransform_t3349966182 ** get_address_of_CanvasTrans_5() { return &___CanvasTrans_5; }
	inline void set_CanvasTrans_5(RectTransform_t3349966182 * value)
	{
		___CanvasTrans_5 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasTrans_5, value);
	}

	inline static int32_t get_offset_of_InputField_6() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___InputField_6)); }
	inline InputField_t1631627530 * get_InputField_6() const { return ___InputField_6; }
	inline InputField_t1631627530 ** get_address_of_InputField_6() { return &___InputField_6; }
	inline void set_InputField_6(InputField_t1631627530 * value)
	{
		___InputField_6 = value;
		Il2CppCodeGenWriteBarrier(&___InputField_6, value);
	}

	inline static int32_t get_offset_of_ExplainText_7() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___ExplainText_7)); }
	inline Text_t356221433 * get_ExplainText_7() const { return ___ExplainText_7; }
	inline Text_t356221433 ** get_address_of_ExplainText_7() { return &___ExplainText_7; }
	inline void set_ExplainText_7(Text_t356221433 * value)
	{
		___ExplainText_7 = value;
		Il2CppCodeGenWriteBarrier(&___ExplainText_7, value);
	}

	inline static int32_t get_offset_of_PermitText_8() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___PermitText_8)); }
	inline Text_t356221433 * get_PermitText_8() const { return ___PermitText_8; }
	inline Text_t356221433 ** get_address_of_PermitText_8() { return &___PermitText_8; }
	inline void set_PermitText_8(Text_t356221433 * value)
	{
		___PermitText_8 = value;
		Il2CppCodeGenWriteBarrier(&___PermitText_8, value);
	}

	inline static int32_t get_offset_of_NamePanel_9() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___NamePanel_9)); }
	inline GameObject_t1756533147 * get_NamePanel_9() const { return ___NamePanel_9; }
	inline GameObject_t1756533147 ** get_address_of_NamePanel_9() { return &___NamePanel_9; }
	inline void set_NamePanel_9(GameObject_t1756533147 * value)
	{
		___NamePanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___NamePanel_9, value);
	}

	inline static int32_t get_offset_of_SystemInput_10() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___SystemInput_10)); }
	inline GameObject_t1756533147 * get_SystemInput_10() const { return ___SystemInput_10; }
	inline GameObject_t1756533147 ** get_address_of_SystemInput_10() { return &___SystemInput_10; }
	inline void set_SystemInput_10(GameObject_t1756533147 * value)
	{
		___SystemInput_10 = value;
		Il2CppCodeGenWriteBarrier(&___SystemInput_10, value);
	}

	inline static int32_t get_offset_of_PermitTime_11() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___PermitTime_11)); }
	inline float get_PermitTime_11() const { return ___PermitTime_11; }
	inline float* get_address_of_PermitTime_11() { return &___PermitTime_11; }
	inline void set_PermitTime_11(float value)
	{
		___PermitTime_11 = value;
	}

	inline static int32_t get_offset_of_m_fWidth_12() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_fWidth_12)); }
	inline float get_m_fWidth_12() const { return ___m_fWidth_12; }
	inline float* get_address_of_m_fWidth_12() { return &___m_fWidth_12; }
	inline void set_m_fWidth_12(float value)
	{
		___m_fWidth_12 = value;
	}

	inline static int32_t get_offset_of_m_fHeight_13() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_fHeight_13)); }
	inline float get_m_fHeight_13() const { return ___m_fHeight_13; }
	inline float* get_address_of_m_fHeight_13() { return &___m_fHeight_13; }
	inline void set_m_fHeight_13(float value)
	{
		___m_fHeight_13 = value;
	}

	inline static int32_t get_offset_of_m_vPanelPos_14() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_vPanelPos_14)); }
	inline Vector3_t2243707580  get_m_vPanelPos_14() const { return ___m_vPanelPos_14; }
	inline Vector3_t2243707580 * get_address_of_m_vPanelPos_14() { return &___m_vPanelPos_14; }
	inline void set_m_vPanelPos_14(Vector3_t2243707580  value)
	{
		___m_vPanelPos_14 = value;
	}

	inline static int32_t get_offset_of_m_vLocalCanvasScale_15() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_vLocalCanvasScale_15)); }
	inline Vector3_t2243707580  get_m_vLocalCanvasScale_15() const { return ___m_vLocalCanvasScale_15; }
	inline Vector3_t2243707580 * get_address_of_m_vLocalCanvasScale_15() { return &___m_vLocalCanvasScale_15; }
	inline void set_m_vLocalCanvasScale_15(Vector3_t2243707580  value)
	{
		___m_vLocalCanvasScale_15 = value;
	}

	inline static int32_t get_offset_of_LinePos_16() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___LinePos_16)); }
	inline Vector3U5BU5D_t1172311765* get_LinePos_16() const { return ___LinePos_16; }
	inline Vector3U5BU5D_t1172311765** get_address_of_LinePos_16() { return &___LinePos_16; }
	inline void set_LinePos_16(Vector3U5BU5D_t1172311765* value)
	{
		___LinePos_16 = value;
		Il2CppCodeGenWriteBarrier(&___LinePos_16, value);
	}

	inline static int32_t get_offset_of_m_NameAnimate_17() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_NameAnimate_17)); }
	inline int32_t get_m_NameAnimate_17() const { return ___m_NameAnimate_17; }
	inline int32_t* get_address_of_m_NameAnimate_17() { return &___m_NameAnimate_17; }
	inline void set_m_NameAnimate_17(int32_t value)
	{
		___m_NameAnimate_17 = value;
	}

	inline static int32_t get_offset_of_m_bFadeIn_18() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_bFadeIn_18)); }
	inline bool get_m_bFadeIn_18() const { return ___m_bFadeIn_18; }
	inline bool* get_address_of_m_bFadeIn_18() { return &___m_bFadeIn_18; }
	inline void set_m_bFadeIn_18(bool value)
	{
		___m_bFadeIn_18 = value;
	}

	inline static int32_t get_offset_of_m_bFadeOut_19() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_bFadeOut_19)); }
	inline bool get_m_bFadeOut_19() const { return ___m_bFadeOut_19; }
	inline bool* get_address_of_m_bFadeOut_19() { return &___m_bFadeOut_19; }
	inline void set_m_bFadeOut_19(bool value)
	{
		___m_bFadeOut_19 = value;
	}

	inline static int32_t get_offset_of_m_BackgroundFader_20() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_BackgroundFader_20)); }
	inline BackgroundFader_t837055620 * get_m_BackgroundFader_20() const { return ___m_BackgroundFader_20; }
	inline BackgroundFader_t837055620 ** get_address_of_m_BackgroundFader_20() { return &___m_BackgroundFader_20; }
	inline void set_m_BackgroundFader_20(BackgroundFader_t837055620 * value)
	{
		___m_BackgroundFader_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_BackgroundFader_20, value);
	}

	inline static int32_t get_offset_of_m_bOnLine_21() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_bOnLine_21)); }
	inline bool get_m_bOnLine_21() const { return ___m_bOnLine_21; }
	inline bool* get_address_of_m_bOnLine_21() { return &___m_bOnLine_21; }
	inline void set_m_bOnLine_21(bool value)
	{
		___m_bOnLine_21 = value;
	}

	inline static int32_t get_offset_of_m_NamePanel_22() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_NamePanel_22)); }
	inline NamePanel_t546707703 * get_m_NamePanel_22() const { return ___m_NamePanel_22; }
	inline NamePanel_t546707703 ** get_address_of_m_NamePanel_22() { return &___m_NamePanel_22; }
	inline void set_m_NamePanel_22(NamePanel_t546707703 * value)
	{
		___m_NamePanel_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_NamePanel_22, value);
	}

	inline static int32_t get_offset_of_m_InputMarkAnimator_23() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_InputMarkAnimator_23)); }
	inline InputMarkAnimator_t3461548916 * get_m_InputMarkAnimator_23() const { return ___m_InputMarkAnimator_23; }
	inline InputMarkAnimator_t3461548916 ** get_address_of_m_InputMarkAnimator_23() { return &___m_InputMarkAnimator_23; }
	inline void set_m_InputMarkAnimator_23(InputMarkAnimator_t3461548916 * value)
	{
		___m_InputMarkAnimator_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_InputMarkAnimator_23, value);
	}

	inline static int32_t get_offset_of_m_Buttons_24() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_Buttons_24)); }
	inline RegisterButtonCrossFader_t205138403 * get_m_Buttons_24() const { return ___m_Buttons_24; }
	inline RegisterButtonCrossFader_t205138403 ** get_address_of_m_Buttons_24() { return &___m_Buttons_24; }
	inline void set_m_Buttons_24(RegisterButtonCrossFader_t205138403 * value)
	{
		___m_Buttons_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_Buttons_24, value);
	}

	inline static int32_t get_offset_of_m_bFlowLines_25() { return static_cast<int32_t>(offsetof(UserNameCreator_t3044989708, ___m_bFlowLines_25)); }
	inline BooleanU5BU5D_t3568034315* get_m_bFlowLines_25() const { return ___m_bFlowLines_25; }
	inline BooleanU5BU5D_t3568034315** get_address_of_m_bFlowLines_25() { return &___m_bFlowLines_25; }
	inline void set_m_bFlowLines_25(BooleanU5BU5D_t3568034315* value)
	{
		___m_bFlowLines_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_bFlowLines_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
