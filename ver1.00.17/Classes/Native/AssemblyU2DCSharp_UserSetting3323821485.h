﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserSetting
struct  UserSetting_t3323821485  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField UserSetting::nickNameInputField
	InputField_t1631627530 * ___nickNameInputField_2;
	// System.Int32 UserSetting::m_coutryNo
	int32_t ___m_coutryNo_3;
	// UnityEngine.UI.Text UserSetting::nickName
	Text_t356221433 * ___nickName_4;
	// UnityEngine.UI.Dropdown UserSetting::dropDown
	Dropdown_t1985816271 * ___dropDown_5;

public:
	inline static int32_t get_offset_of_nickNameInputField_2() { return static_cast<int32_t>(offsetof(UserSetting_t3323821485, ___nickNameInputField_2)); }
	inline InputField_t1631627530 * get_nickNameInputField_2() const { return ___nickNameInputField_2; }
	inline InputField_t1631627530 ** get_address_of_nickNameInputField_2() { return &___nickNameInputField_2; }
	inline void set_nickNameInputField_2(InputField_t1631627530 * value)
	{
		___nickNameInputField_2 = value;
		Il2CppCodeGenWriteBarrier(&___nickNameInputField_2, value);
	}

	inline static int32_t get_offset_of_m_coutryNo_3() { return static_cast<int32_t>(offsetof(UserSetting_t3323821485, ___m_coutryNo_3)); }
	inline int32_t get_m_coutryNo_3() const { return ___m_coutryNo_3; }
	inline int32_t* get_address_of_m_coutryNo_3() { return &___m_coutryNo_3; }
	inline void set_m_coutryNo_3(int32_t value)
	{
		___m_coutryNo_3 = value;
	}

	inline static int32_t get_offset_of_nickName_4() { return static_cast<int32_t>(offsetof(UserSetting_t3323821485, ___nickName_4)); }
	inline Text_t356221433 * get_nickName_4() const { return ___nickName_4; }
	inline Text_t356221433 ** get_address_of_nickName_4() { return &___nickName_4; }
	inline void set_nickName_4(Text_t356221433 * value)
	{
		___nickName_4 = value;
		Il2CppCodeGenWriteBarrier(&___nickName_4, value);
	}

	inline static int32_t get_offset_of_dropDown_5() { return static_cast<int32_t>(offsetof(UserSetting_t3323821485, ___dropDown_5)); }
	inline Dropdown_t1985816271 * get_dropDown_5() const { return ___dropDown_5; }
	inline Dropdown_t1985816271 ** get_address_of_dropDown_5() { return &___dropDown_5; }
	inline void set_dropDown_5(Dropdown_t1985816271 * value)
	{
		___dropDown_5 = value;
		Il2CppCodeGenWriteBarrier(&___dropDown_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
