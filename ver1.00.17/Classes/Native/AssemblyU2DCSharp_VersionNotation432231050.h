﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionNotation
struct  VersionNotation_t432231050  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextAsset VersionNotation::m_VersionTextAsset
	TextAsset_t3973159845 * ___m_VersionTextAsset_2;
	// UnityEngine.UI.Text VersionNotation::m_Version
	Text_t356221433 * ___m_Version_3;

public:
	inline static int32_t get_offset_of_m_VersionTextAsset_2() { return static_cast<int32_t>(offsetof(VersionNotation_t432231050, ___m_VersionTextAsset_2)); }
	inline TextAsset_t3973159845 * get_m_VersionTextAsset_2() const { return ___m_VersionTextAsset_2; }
	inline TextAsset_t3973159845 ** get_address_of_m_VersionTextAsset_2() { return &___m_VersionTextAsset_2; }
	inline void set_m_VersionTextAsset_2(TextAsset_t3973159845 * value)
	{
		___m_VersionTextAsset_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_VersionTextAsset_2, value);
	}

	inline static int32_t get_offset_of_m_Version_3() { return static_cast<int32_t>(offsetof(VersionNotation_t432231050, ___m_Version_3)); }
	inline Text_t356221433 * get_m_Version_3() const { return ___m_Version_3; }
	inline Text_t356221433 ** get_address_of_m_Version_3() { return &___m_Version_3; }
	inline void set_m_Version_3(Text_t356221433 * value)
	{
		___m_Version_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Version_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
