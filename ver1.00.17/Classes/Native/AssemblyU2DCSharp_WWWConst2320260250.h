﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WWWConst
struct  WWWConst_t2320260250  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct WWWConst_t2320260250_StaticFields
{
public:
	// System.String WWWConst::WWW_DOMAIN
	String_t* ___WWW_DOMAIN_2;
	// System.String[] WWWConst::WWW_API
	StringU5BU5D_t1642385972* ___WWW_API_3;

public:
	inline static int32_t get_offset_of_WWW_DOMAIN_2() { return static_cast<int32_t>(offsetof(WWWConst_t2320260250_StaticFields, ___WWW_DOMAIN_2)); }
	inline String_t* get_WWW_DOMAIN_2() const { return ___WWW_DOMAIN_2; }
	inline String_t** get_address_of_WWW_DOMAIN_2() { return &___WWW_DOMAIN_2; }
	inline void set_WWW_DOMAIN_2(String_t* value)
	{
		___WWW_DOMAIN_2 = value;
		Il2CppCodeGenWriteBarrier(&___WWW_DOMAIN_2, value);
	}

	inline static int32_t get_offset_of_WWW_API_3() { return static_cast<int32_t>(offsetof(WWWConst_t2320260250_StaticFields, ___WWW_API_3)); }
	inline StringU5BU5D_t1642385972* get_WWW_API_3() const { return ___WWW_API_3; }
	inline StringU5BU5D_t1642385972** get_address_of_WWW_API_3() { return &___WWW_API_3; }
	inline void set_WWW_API_3(StringU5BU5D_t1642385972* value)
	{
		___WWW_API_3 = value;
		Il2CppCodeGenWriteBarrier(&___WWW_API_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
