﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen398463373.h"
#include "AssemblyU2DCSharp_WWWConst_WWWLIST792320822.h"

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// WWWManager
struct WWWManager_t1701787038;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WWWManager
struct  WWWManager_t1701787038  : public SingletonMonoBehaviour_1_t398463373
{
public:
	// System.String WWWManager::sKy
	String_t* ___sKy_6;
	// System.String WWWManager::sIV
	String_t* ___sIV_7;

public:
	inline static int32_t get_offset_of_sKy_6() { return static_cast<int32_t>(offsetof(WWWManager_t1701787038, ___sKy_6)); }
	inline String_t* get_sKy_6() const { return ___sKy_6; }
	inline String_t** get_address_of_sKy_6() { return &___sKy_6; }
	inline void set_sKy_6(String_t* value)
	{
		___sKy_6 = value;
		Il2CppCodeGenWriteBarrier(&___sKy_6, value);
	}

	inline static int32_t get_offset_of_sIV_7() { return static_cast<int32_t>(offsetof(WWWManager_t1701787038, ___sIV_7)); }
	inline String_t* get_sIV_7() const { return ___sIV_7; }
	inline String_t** get_address_of_sIV_7() { return &___sIV_7; }
	inline void set_sIV_7(String_t* value)
	{
		___sIV_7 = value;
		Il2CppCodeGenWriteBarrier(&___sIV_7, value);
	}
};

struct WWWManager_t1701787038_StaticFields
{
public:
	// WWWConst/WWWLIST WWWManager::lastApi
	int32_t ___lastApi_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WWWManager::list
	List_1_t1125654279 * ___list_4;
	// WWWManager WWWManager::instance
	WWWManager_t1701787038 * ___instance_5;

public:
	inline static int32_t get_offset_of_lastApi_3() { return static_cast<int32_t>(offsetof(WWWManager_t1701787038_StaticFields, ___lastApi_3)); }
	inline int32_t get_lastApi_3() const { return ___lastApi_3; }
	inline int32_t* get_address_of_lastApi_3() { return &___lastApi_3; }
	inline void set_lastApi_3(int32_t value)
	{
		___lastApi_3 = value;
	}

	inline static int32_t get_offset_of_list_4() { return static_cast<int32_t>(offsetof(WWWManager_t1701787038_StaticFields, ___list_4)); }
	inline List_1_t1125654279 * get_list_4() const { return ___list_4; }
	inline List_1_t1125654279 ** get_address_of_list_4() { return &___list_4; }
	inline void set_list_4(List_1_t1125654279 * value)
	{
		___list_4 = value;
		Il2CppCodeGenWriteBarrier(&___list_4, value);
	}

	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(WWWManager_t1701787038_StaticFields, ___instance_5)); }
	inline WWWManager_t1701787038 * get_instance_5() const { return ___instance_5; }
	inline WWWManager_t1701787038 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(WWWManager_t1701787038 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier(&___instance_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
