﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_WWWConst_WWWCONNECTTYPE2173880684.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// WWWManager/WWWSuccess
struct WWWSuccess_t4269981261;
// WWWManager/WWWError
struct WWWError_t409848164;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WWWManager/<Connect>c__AnonStorey1
struct  U3CConnectU3Ec__AnonStorey1_t2162990091  : public Il2CppObject
{
public:
	// System.String WWWManager/<Connect>c__AnonStorey1::url
	String_t* ___url_0;
	// WWWConst/WWWCONNECTTYPE WWWManager/<Connect>c__AnonStorey1::connectType
	int32_t ___connectType_1;
	// System.Byte[] WWWManager/<Connect>c__AnonStorey1::postJsonData
	ByteU5BU5D_t3397334013* ___postJsonData_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> WWWManager/<Connect>c__AnonStorey1::query
	Dictionary_2_t3943999495 * ___query_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> WWWManager/<Connect>c__AnonStorey1::header
	Dictionary_2_t3943999495 * ___header_4;
	// WWWManager/WWWSuccess WWWManager/<Connect>c__AnonStorey1::successAction
	WWWSuccess_t4269981261 * ___successAction_5;
	// WWWManager/WWWError WWWManager/<Connect>c__AnonStorey1::errorAction
	WWWError_t409848164 * ___errorAction_6;
	// System.Boolean WWWManager/<Connect>c__AnonStorey1::checkCache
	bool ___checkCache_7;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_connectType_1() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___connectType_1)); }
	inline int32_t get_connectType_1() const { return ___connectType_1; }
	inline int32_t* get_address_of_connectType_1() { return &___connectType_1; }
	inline void set_connectType_1(int32_t value)
	{
		___connectType_1 = value;
	}

	inline static int32_t get_offset_of_postJsonData_2() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___postJsonData_2)); }
	inline ByteU5BU5D_t3397334013* get_postJsonData_2() const { return ___postJsonData_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_postJsonData_2() { return &___postJsonData_2; }
	inline void set_postJsonData_2(ByteU5BU5D_t3397334013* value)
	{
		___postJsonData_2 = value;
		Il2CppCodeGenWriteBarrier(&___postJsonData_2, value);
	}

	inline static int32_t get_offset_of_query_3() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___query_3)); }
	inline Dictionary_2_t3943999495 * get_query_3() const { return ___query_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_query_3() { return &___query_3; }
	inline void set_query_3(Dictionary_2_t3943999495 * value)
	{
		___query_3 = value;
		Il2CppCodeGenWriteBarrier(&___query_3, value);
	}

	inline static int32_t get_offset_of_header_4() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___header_4)); }
	inline Dictionary_2_t3943999495 * get_header_4() const { return ___header_4; }
	inline Dictionary_2_t3943999495 ** get_address_of_header_4() { return &___header_4; }
	inline void set_header_4(Dictionary_2_t3943999495 * value)
	{
		___header_4 = value;
		Il2CppCodeGenWriteBarrier(&___header_4, value);
	}

	inline static int32_t get_offset_of_successAction_5() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___successAction_5)); }
	inline WWWSuccess_t4269981261 * get_successAction_5() const { return ___successAction_5; }
	inline WWWSuccess_t4269981261 ** get_address_of_successAction_5() { return &___successAction_5; }
	inline void set_successAction_5(WWWSuccess_t4269981261 * value)
	{
		___successAction_5 = value;
		Il2CppCodeGenWriteBarrier(&___successAction_5, value);
	}

	inline static int32_t get_offset_of_errorAction_6() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___errorAction_6)); }
	inline WWWError_t409848164 * get_errorAction_6() const { return ___errorAction_6; }
	inline WWWError_t409848164 ** get_address_of_errorAction_6() { return &___errorAction_6; }
	inline void set_errorAction_6(WWWError_t409848164 * value)
	{
		___errorAction_6 = value;
		Il2CppCodeGenWriteBarrier(&___errorAction_6, value);
	}

	inline static int32_t get_offset_of_checkCache_7() { return static_cast<int32_t>(offsetof(U3CConnectU3Ec__AnonStorey1_t2162990091, ___checkCache_7)); }
	inline bool get_checkCache_7() const { return ___checkCache_7; }
	inline bool* get_address_of_checkCache_7() { return &___checkCache_7; }
	inline void set_checkCache_7(bool value)
	{
		___checkCache_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
