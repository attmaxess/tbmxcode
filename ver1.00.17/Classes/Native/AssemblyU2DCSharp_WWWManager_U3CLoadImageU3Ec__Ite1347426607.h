﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WWWManager/<LoadImage>c__Iterator0
struct  U3CLoadImageU3Ec__Iterator0_t1347426607  : public Il2CppObject
{
public:
	// System.String WWWManager/<LoadImage>c__Iterator0::imagePath
	String_t* ___imagePath_0;
	// System.String WWWManager/<LoadImage>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_1;
	// UnityEngine.WWW WWWManager/<LoadImage>c__Iterator0::<imageWWW>__0
	WWW_t2919945039 * ___U3CimageWWWU3E__0_2;
	// UnityEngine.Rect WWWManager/<LoadImage>c__Iterator0::rect
	Rect_t3681755626  ___rect_3;
	// UnityEngine.Vector2 WWWManager/<LoadImage>c__Iterator0::pivot
	Vector2_t2243707579  ___pivot_4;
	// UnityEngine.GameObject WWWManager/<LoadImage>c__Iterator0::go
	GameObject_t1756533147 * ___go_5;
	// System.Object WWWManager/<LoadImage>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean WWWManager/<LoadImage>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 WWWManager/<LoadImage>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_imagePath_0() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___imagePath_0)); }
	inline String_t* get_imagePath_0() const { return ___imagePath_0; }
	inline String_t** get_address_of_imagePath_0() { return &___imagePath_0; }
	inline void set_imagePath_0(String_t* value)
	{
		___imagePath_0 = value;
		Il2CppCodeGenWriteBarrier(&___imagePath_0, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___U3CurlU3E__0_1)); }
	inline String_t* get_U3CurlU3E__0_1() const { return ___U3CurlU3E__0_1; }
	inline String_t** get_address_of_U3CurlU3E__0_1() { return &___U3CurlU3E__0_1; }
	inline void set_U3CurlU3E__0_1(String_t* value)
	{
		___U3CurlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CimageWWWU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___U3CimageWWWU3E__0_2)); }
	inline WWW_t2919945039 * get_U3CimageWWWU3E__0_2() const { return ___U3CimageWWWU3E__0_2; }
	inline WWW_t2919945039 ** get_address_of_U3CimageWWWU3E__0_2() { return &___U3CimageWWWU3E__0_2; }
	inline void set_U3CimageWWWU3E__0_2(WWW_t2919945039 * value)
	{
		___U3CimageWWWU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimageWWWU3E__0_2, value);
	}

	inline static int32_t get_offset_of_rect_3() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___rect_3)); }
	inline Rect_t3681755626  get_rect_3() const { return ___rect_3; }
	inline Rect_t3681755626 * get_address_of_rect_3() { return &___rect_3; }
	inline void set_rect_3(Rect_t3681755626  value)
	{
		___rect_3 = value;
	}

	inline static int32_t get_offset_of_pivot_4() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___pivot_4)); }
	inline Vector2_t2243707579  get_pivot_4() const { return ___pivot_4; }
	inline Vector2_t2243707579 * get_address_of_pivot_4() { return &___pivot_4; }
	inline void set_pivot_4(Vector2_t2243707579  value)
	{
		___pivot_4 = value;
	}

	inline static int32_t get_offset_of_go_5() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___go_5)); }
	inline GameObject_t1756533147 * get_go_5() const { return ___go_5; }
	inline GameObject_t1756533147 ** get_address_of_go_5() { return &___go_5; }
	inline void set_go_5(GameObject_t1756533147 * value)
	{
		___go_5 = value;
		Il2CppCodeGenWriteBarrier(&___go_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ec__Iterator0_t1347426607, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
