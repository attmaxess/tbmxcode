﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.WWW
struct WWW_t2919945039;
// System.String
struct String_t;
// WWWManager/WWWSuccess
struct WWWSuccess_t4269981261;
// WWWManager/WWWError
struct WWWError_t409848164;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WWWRequest
struct  WWWRequest_t2978566704  : public Il2CppObject
{
public:
	// UnityEngine.WWW WWWRequest::www
	WWW_t2919945039 * ___www_0;
	// System.Boolean WWWRequest::isDone
	bool ___isDone_1;
	// System.String WWWRequest::url
	String_t* ___url_2;
	// WWWManager/WWWSuccess WWWRequest::successAction
	WWWSuccess_t4269981261 * ___successAction_3;
	// WWWManager/WWWError WWWRequest::errorAction
	WWWError_t409848164 * ___errorAction_4;
	// UnityEngine.WWWForm WWWRequest::form
	WWWForm_t3950226929 * ___form_5;
	// System.Boolean WWWRequest::isReconnect
	bool ___isReconnect_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> WWWRequest::postData
	Dictionary_2_t3943999495 * ___postData_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> WWWRequest::getData
	Dictionary_2_t3943999495 * ___getData_8;
	// System.Byte[] WWWRequest::jsonData
	ByteU5BU5D_t3397334013* ___jsonData_9;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___www_0)); }
	inline WWW_t2919945039 * get_www_0() const { return ___www_0; }
	inline WWW_t2919945039 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t2919945039 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier(&___www_0, value);
	}

	inline static int32_t get_offset_of_isDone_1() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___isDone_1)); }
	inline bool get_isDone_1() const { return ___isDone_1; }
	inline bool* get_address_of_isDone_1() { return &___isDone_1; }
	inline void set_isDone_1(bool value)
	{
		___isDone_1 = value;
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier(&___url_2, value);
	}

	inline static int32_t get_offset_of_successAction_3() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___successAction_3)); }
	inline WWWSuccess_t4269981261 * get_successAction_3() const { return ___successAction_3; }
	inline WWWSuccess_t4269981261 ** get_address_of_successAction_3() { return &___successAction_3; }
	inline void set_successAction_3(WWWSuccess_t4269981261 * value)
	{
		___successAction_3 = value;
		Il2CppCodeGenWriteBarrier(&___successAction_3, value);
	}

	inline static int32_t get_offset_of_errorAction_4() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___errorAction_4)); }
	inline WWWError_t409848164 * get_errorAction_4() const { return ___errorAction_4; }
	inline WWWError_t409848164 ** get_address_of_errorAction_4() { return &___errorAction_4; }
	inline void set_errorAction_4(WWWError_t409848164 * value)
	{
		___errorAction_4 = value;
		Il2CppCodeGenWriteBarrier(&___errorAction_4, value);
	}

	inline static int32_t get_offset_of_form_5() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___form_5)); }
	inline WWWForm_t3950226929 * get_form_5() const { return ___form_5; }
	inline WWWForm_t3950226929 ** get_address_of_form_5() { return &___form_5; }
	inline void set_form_5(WWWForm_t3950226929 * value)
	{
		___form_5 = value;
		Il2CppCodeGenWriteBarrier(&___form_5, value);
	}

	inline static int32_t get_offset_of_isReconnect_6() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___isReconnect_6)); }
	inline bool get_isReconnect_6() const { return ___isReconnect_6; }
	inline bool* get_address_of_isReconnect_6() { return &___isReconnect_6; }
	inline void set_isReconnect_6(bool value)
	{
		___isReconnect_6 = value;
	}

	inline static int32_t get_offset_of_postData_7() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___postData_7)); }
	inline Dictionary_2_t3943999495 * get_postData_7() const { return ___postData_7; }
	inline Dictionary_2_t3943999495 ** get_address_of_postData_7() { return &___postData_7; }
	inline void set_postData_7(Dictionary_2_t3943999495 * value)
	{
		___postData_7 = value;
		Il2CppCodeGenWriteBarrier(&___postData_7, value);
	}

	inline static int32_t get_offset_of_getData_8() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___getData_8)); }
	inline Dictionary_2_t3943999495 * get_getData_8() const { return ___getData_8; }
	inline Dictionary_2_t3943999495 ** get_address_of_getData_8() { return &___getData_8; }
	inline void set_getData_8(Dictionary_2_t3943999495 * value)
	{
		___getData_8 = value;
		Il2CppCodeGenWriteBarrier(&___getData_8, value);
	}

	inline static int32_t get_offset_of_jsonData_9() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704, ___jsonData_9)); }
	inline ByteU5BU5D_t3397334013* get_jsonData_9() const { return ___jsonData_9; }
	inline ByteU5BU5D_t3397334013** get_address_of_jsonData_9() { return &___jsonData_9; }
	inline void set_jsonData_9(ByteU5BU5D_t3397334013* value)
	{
		___jsonData_9 = value;
		Il2CppCodeGenWriteBarrier(&___jsonData_9, value);
	}
};

struct WWWRequest_t2978566704_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WWWRequest::list
	List_1_t1125654279 * ___list_10;

public:
	inline static int32_t get_offset_of_list_10() { return static_cast<int32_t>(offsetof(WWWRequest_t2978566704_StaticFields, ___list_10)); }
	inline List_1_t1125654279 * get_list_10() const { return ___list_10; }
	inline List_1_t1125654279 ** get_address_of_list_10() { return &___list_10; }
	inline void set_list_10(List_1_t1125654279 * value)
	{
		___list_10 = value;
		Il2CppCodeGenWriteBarrier(&___list_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
