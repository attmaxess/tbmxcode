﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_WaterCamera_eCAMERAFOG194227.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaterCamera
struct  WaterCamera_t1875834828  : public MonoBehaviour_t1158329972
{
public:
	// WaterCamera/eCAMERAFOG WaterCamera::nowCameraFog
	int32_t ___nowCameraFog_2;
	// WaterCamera/eCAMERAFOG WaterCamera::nextCameraFog
	int32_t ___nextCameraFog_3;
	// UnityEngine.GameObject WaterCamera::WaterFillter
	GameObject_t1756533147 * ___WaterFillter_4;
	// UnityEngine.Vector3 WaterCamera::direction
	Vector3_t2243707580  ___direction_5;
	// System.Single WaterCamera::dist
	float ___dist_6;
	// System.Boolean WaterCamera::inWater
	bool ___inWater_7;
	// UnityEngine.GameObject WaterCamera::planet
	GameObject_t1756533147 * ___planet_8;
	// UnityEngine.Material WaterCamera::outWater
	Material_t193706927 * ___outWater_9;
	// UnityEngine.Material WaterCamera::underWater
	Material_t193706927 * ___underWater_10;

public:
	inline static int32_t get_offset_of_nowCameraFog_2() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___nowCameraFog_2)); }
	inline int32_t get_nowCameraFog_2() const { return ___nowCameraFog_2; }
	inline int32_t* get_address_of_nowCameraFog_2() { return &___nowCameraFog_2; }
	inline void set_nowCameraFog_2(int32_t value)
	{
		___nowCameraFog_2 = value;
	}

	inline static int32_t get_offset_of_nextCameraFog_3() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___nextCameraFog_3)); }
	inline int32_t get_nextCameraFog_3() const { return ___nextCameraFog_3; }
	inline int32_t* get_address_of_nextCameraFog_3() { return &___nextCameraFog_3; }
	inline void set_nextCameraFog_3(int32_t value)
	{
		___nextCameraFog_3 = value;
	}

	inline static int32_t get_offset_of_WaterFillter_4() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___WaterFillter_4)); }
	inline GameObject_t1756533147 * get_WaterFillter_4() const { return ___WaterFillter_4; }
	inline GameObject_t1756533147 ** get_address_of_WaterFillter_4() { return &___WaterFillter_4; }
	inline void set_WaterFillter_4(GameObject_t1756533147 * value)
	{
		___WaterFillter_4 = value;
		Il2CppCodeGenWriteBarrier(&___WaterFillter_4, value);
	}

	inline static int32_t get_offset_of_direction_5() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___direction_5)); }
	inline Vector3_t2243707580  get_direction_5() const { return ___direction_5; }
	inline Vector3_t2243707580 * get_address_of_direction_5() { return &___direction_5; }
	inline void set_direction_5(Vector3_t2243707580  value)
	{
		___direction_5 = value;
	}

	inline static int32_t get_offset_of_dist_6() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___dist_6)); }
	inline float get_dist_6() const { return ___dist_6; }
	inline float* get_address_of_dist_6() { return &___dist_6; }
	inline void set_dist_6(float value)
	{
		___dist_6 = value;
	}

	inline static int32_t get_offset_of_inWater_7() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___inWater_7)); }
	inline bool get_inWater_7() const { return ___inWater_7; }
	inline bool* get_address_of_inWater_7() { return &___inWater_7; }
	inline void set_inWater_7(bool value)
	{
		___inWater_7 = value;
	}

	inline static int32_t get_offset_of_planet_8() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___planet_8)); }
	inline GameObject_t1756533147 * get_planet_8() const { return ___planet_8; }
	inline GameObject_t1756533147 ** get_address_of_planet_8() { return &___planet_8; }
	inline void set_planet_8(GameObject_t1756533147 * value)
	{
		___planet_8 = value;
		Il2CppCodeGenWriteBarrier(&___planet_8, value);
	}

	inline static int32_t get_offset_of_outWater_9() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___outWater_9)); }
	inline Material_t193706927 * get_outWater_9() const { return ___outWater_9; }
	inline Material_t193706927 ** get_address_of_outWater_9() { return &___outWater_9; }
	inline void set_outWater_9(Material_t193706927 * value)
	{
		___outWater_9 = value;
		Il2CppCodeGenWriteBarrier(&___outWater_9, value);
	}

	inline static int32_t get_offset_of_underWater_10() { return static_cast<int32_t>(offsetof(WaterCamera_t1875834828, ___underWater_10)); }
	inline Material_t193706927 * get_underWater_10() const { return ___underWater_10; }
	inline Material_t193706927 ** get_address_of_underWater_10() { return &___underWater_10; }
	inline void set_underWater_10(Material_t193706927 * value)
	{
		___underWater_10 = value;
		Il2CppCodeGenWriteBarrier(&___underWater_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
