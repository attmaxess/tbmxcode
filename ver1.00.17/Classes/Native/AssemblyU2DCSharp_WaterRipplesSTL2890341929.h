﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Vector2[0...,0...]
struct Vector2U5BU2CU5D_t686124027;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Threading.Thread
struct Thread_t241561612;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaterRipplesSTL
struct  WaterRipplesSTL_t2890341929  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 WaterRipplesSTL::UpdateFPS
	int32_t ___UpdateFPS_2;
	// System.Boolean WaterRipplesSTL::Multithreading
	bool ___Multithreading_3;
	// System.Int32 WaterRipplesSTL::DisplacementResolution
	int32_t ___DisplacementResolution_4;
	// System.Single WaterRipplesSTL::Damping
	float ___Damping_5;
	// System.Single WaterRipplesSTL::Speed
	float ___Speed_6;
	// System.Boolean WaterRipplesSTL::UseSmoothWaves
	bool ___UseSmoothWaves_7;
	// System.Boolean WaterRipplesSTL::UseProjectedWaves
	bool ___UseProjectedWaves_8;
	// UnityEngine.Texture2D WaterRipplesSTL::CutOutTexture
	Texture2D_t3542995729 * ___CutOutTexture_9;
	// UnityEngine.Transform WaterRipplesSTL::t
	Transform_t3275118058 * ___t_10;
	// System.Single WaterRipplesSTL::textureColorMultiplier
	float ___textureColorMultiplier_11;
	// UnityEngine.Texture2D WaterRipplesSTL::displacementTexture
	Texture2D_t3542995729 * ___displacementTexture_12;
	// UnityEngine.Vector2[0...,0...] WaterRipplesSTL::waveAcceleration
	Vector2U5BU2CU5D_t686124027* ___waveAcceleration_13;
	// UnityEngine.Color[] WaterRipplesSTL::col
	ColorU5BU5D_t672350442* ___col_14;
	// UnityEngine.Vector3[] WaterRipplesSTL::wavePoints
	Vector3U5BU5D_t1172311765* ___wavePoints_15;
	// UnityEngine.Vector3 WaterRipplesSTL::scaleBounds
	Vector3_t2243707580  ___scaleBounds_16;
	// System.Single WaterRipplesSTL::inversedDamping
	float ___inversedDamping_17;
	// System.Single[] WaterRipplesSTL::cutOutTextureGray
	SingleU5BU5D_t577127397* ___cutOutTextureGray_18;
	// System.Boolean WaterRipplesSTL::cutOutTextureInitialized
	bool ___cutOutTextureInitialized_19;
	// System.Threading.Thread WaterRipplesSTL::thread
	Thread_t241561612 * ___thread_20;
	// System.Boolean WaterRipplesSTL::canUpdate
	bool ___canUpdate_21;
	// System.Double WaterRipplesSTL::threadDeltaTime
	double ___threadDeltaTime_22;
	// System.DateTime WaterRipplesSTL::oldDateTime
	DateTime_t693205669  ___oldDateTime_23;
	// UnityEngine.Vector2 WaterRipplesSTL::movedObjPos
	Vector2_t2243707579  ___movedObjPos_24;
	// UnityEngine.Vector2 WaterRipplesSTL::projectorPosition
	Vector2_t2243707579  ___projectorPosition_25;
	// UnityEngine.Vector4 WaterRipplesSTL::_GAmplitude
	Vector4_t2243707581  ____GAmplitude_26;
	// UnityEngine.Vector4 WaterRipplesSTL::_GFrequency
	Vector4_t2243707581  ____GFrequency_27;
	// UnityEngine.Vector4 WaterRipplesSTL::_GSteepness
	Vector4_t2243707581  ____GSteepness_28;
	// UnityEngine.Vector4 WaterRipplesSTL::_GSpeed
	Vector4_t2243707581  ____GSpeed_29;
	// UnityEngine.Vector4 WaterRipplesSTL::_GDirectionAB
	Vector4_t2243707581  ____GDirectionAB_30;
	// UnityEngine.Vector4 WaterRipplesSTL::_GDirectionCD
	Vector4_t2243707581  ____GDirectionCD_31;
	// System.Single WaterRipplesSTL::time
	float ___time_32;
	// System.Int32 WaterRipplesSTL::k
	int32_t ___k_33;
	// System.Boolean WaterRipplesSTL::isStarted
	bool ___isStarted_34;

public:
	inline static int32_t get_offset_of_UpdateFPS_2() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___UpdateFPS_2)); }
	inline int32_t get_UpdateFPS_2() const { return ___UpdateFPS_2; }
	inline int32_t* get_address_of_UpdateFPS_2() { return &___UpdateFPS_2; }
	inline void set_UpdateFPS_2(int32_t value)
	{
		___UpdateFPS_2 = value;
	}

	inline static int32_t get_offset_of_Multithreading_3() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___Multithreading_3)); }
	inline bool get_Multithreading_3() const { return ___Multithreading_3; }
	inline bool* get_address_of_Multithreading_3() { return &___Multithreading_3; }
	inline void set_Multithreading_3(bool value)
	{
		___Multithreading_3 = value;
	}

	inline static int32_t get_offset_of_DisplacementResolution_4() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___DisplacementResolution_4)); }
	inline int32_t get_DisplacementResolution_4() const { return ___DisplacementResolution_4; }
	inline int32_t* get_address_of_DisplacementResolution_4() { return &___DisplacementResolution_4; }
	inline void set_DisplacementResolution_4(int32_t value)
	{
		___DisplacementResolution_4 = value;
	}

	inline static int32_t get_offset_of_Damping_5() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___Damping_5)); }
	inline float get_Damping_5() const { return ___Damping_5; }
	inline float* get_address_of_Damping_5() { return &___Damping_5; }
	inline void set_Damping_5(float value)
	{
		___Damping_5 = value;
	}

	inline static int32_t get_offset_of_Speed_6() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___Speed_6)); }
	inline float get_Speed_6() const { return ___Speed_6; }
	inline float* get_address_of_Speed_6() { return &___Speed_6; }
	inline void set_Speed_6(float value)
	{
		___Speed_6 = value;
	}

	inline static int32_t get_offset_of_UseSmoothWaves_7() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___UseSmoothWaves_7)); }
	inline bool get_UseSmoothWaves_7() const { return ___UseSmoothWaves_7; }
	inline bool* get_address_of_UseSmoothWaves_7() { return &___UseSmoothWaves_7; }
	inline void set_UseSmoothWaves_7(bool value)
	{
		___UseSmoothWaves_7 = value;
	}

	inline static int32_t get_offset_of_UseProjectedWaves_8() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___UseProjectedWaves_8)); }
	inline bool get_UseProjectedWaves_8() const { return ___UseProjectedWaves_8; }
	inline bool* get_address_of_UseProjectedWaves_8() { return &___UseProjectedWaves_8; }
	inline void set_UseProjectedWaves_8(bool value)
	{
		___UseProjectedWaves_8 = value;
	}

	inline static int32_t get_offset_of_CutOutTexture_9() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___CutOutTexture_9)); }
	inline Texture2D_t3542995729 * get_CutOutTexture_9() const { return ___CutOutTexture_9; }
	inline Texture2D_t3542995729 ** get_address_of_CutOutTexture_9() { return &___CutOutTexture_9; }
	inline void set_CutOutTexture_9(Texture2D_t3542995729 * value)
	{
		___CutOutTexture_9 = value;
		Il2CppCodeGenWriteBarrier(&___CutOutTexture_9, value);
	}

	inline static int32_t get_offset_of_t_10() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___t_10)); }
	inline Transform_t3275118058 * get_t_10() const { return ___t_10; }
	inline Transform_t3275118058 ** get_address_of_t_10() { return &___t_10; }
	inline void set_t_10(Transform_t3275118058 * value)
	{
		___t_10 = value;
		Il2CppCodeGenWriteBarrier(&___t_10, value);
	}

	inline static int32_t get_offset_of_textureColorMultiplier_11() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___textureColorMultiplier_11)); }
	inline float get_textureColorMultiplier_11() const { return ___textureColorMultiplier_11; }
	inline float* get_address_of_textureColorMultiplier_11() { return &___textureColorMultiplier_11; }
	inline void set_textureColorMultiplier_11(float value)
	{
		___textureColorMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_displacementTexture_12() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___displacementTexture_12)); }
	inline Texture2D_t3542995729 * get_displacementTexture_12() const { return ___displacementTexture_12; }
	inline Texture2D_t3542995729 ** get_address_of_displacementTexture_12() { return &___displacementTexture_12; }
	inline void set_displacementTexture_12(Texture2D_t3542995729 * value)
	{
		___displacementTexture_12 = value;
		Il2CppCodeGenWriteBarrier(&___displacementTexture_12, value);
	}

	inline static int32_t get_offset_of_waveAcceleration_13() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___waveAcceleration_13)); }
	inline Vector2U5BU2CU5D_t686124027* get_waveAcceleration_13() const { return ___waveAcceleration_13; }
	inline Vector2U5BU2CU5D_t686124027** get_address_of_waveAcceleration_13() { return &___waveAcceleration_13; }
	inline void set_waveAcceleration_13(Vector2U5BU2CU5D_t686124027* value)
	{
		___waveAcceleration_13 = value;
		Il2CppCodeGenWriteBarrier(&___waveAcceleration_13, value);
	}

	inline static int32_t get_offset_of_col_14() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___col_14)); }
	inline ColorU5BU5D_t672350442* get_col_14() const { return ___col_14; }
	inline ColorU5BU5D_t672350442** get_address_of_col_14() { return &___col_14; }
	inline void set_col_14(ColorU5BU5D_t672350442* value)
	{
		___col_14 = value;
		Il2CppCodeGenWriteBarrier(&___col_14, value);
	}

	inline static int32_t get_offset_of_wavePoints_15() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___wavePoints_15)); }
	inline Vector3U5BU5D_t1172311765* get_wavePoints_15() const { return ___wavePoints_15; }
	inline Vector3U5BU5D_t1172311765** get_address_of_wavePoints_15() { return &___wavePoints_15; }
	inline void set_wavePoints_15(Vector3U5BU5D_t1172311765* value)
	{
		___wavePoints_15 = value;
		Il2CppCodeGenWriteBarrier(&___wavePoints_15, value);
	}

	inline static int32_t get_offset_of_scaleBounds_16() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___scaleBounds_16)); }
	inline Vector3_t2243707580  get_scaleBounds_16() const { return ___scaleBounds_16; }
	inline Vector3_t2243707580 * get_address_of_scaleBounds_16() { return &___scaleBounds_16; }
	inline void set_scaleBounds_16(Vector3_t2243707580  value)
	{
		___scaleBounds_16 = value;
	}

	inline static int32_t get_offset_of_inversedDamping_17() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___inversedDamping_17)); }
	inline float get_inversedDamping_17() const { return ___inversedDamping_17; }
	inline float* get_address_of_inversedDamping_17() { return &___inversedDamping_17; }
	inline void set_inversedDamping_17(float value)
	{
		___inversedDamping_17 = value;
	}

	inline static int32_t get_offset_of_cutOutTextureGray_18() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___cutOutTextureGray_18)); }
	inline SingleU5BU5D_t577127397* get_cutOutTextureGray_18() const { return ___cutOutTextureGray_18; }
	inline SingleU5BU5D_t577127397** get_address_of_cutOutTextureGray_18() { return &___cutOutTextureGray_18; }
	inline void set_cutOutTextureGray_18(SingleU5BU5D_t577127397* value)
	{
		___cutOutTextureGray_18 = value;
		Il2CppCodeGenWriteBarrier(&___cutOutTextureGray_18, value);
	}

	inline static int32_t get_offset_of_cutOutTextureInitialized_19() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___cutOutTextureInitialized_19)); }
	inline bool get_cutOutTextureInitialized_19() const { return ___cutOutTextureInitialized_19; }
	inline bool* get_address_of_cutOutTextureInitialized_19() { return &___cutOutTextureInitialized_19; }
	inline void set_cutOutTextureInitialized_19(bool value)
	{
		___cutOutTextureInitialized_19 = value;
	}

	inline static int32_t get_offset_of_thread_20() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___thread_20)); }
	inline Thread_t241561612 * get_thread_20() const { return ___thread_20; }
	inline Thread_t241561612 ** get_address_of_thread_20() { return &___thread_20; }
	inline void set_thread_20(Thread_t241561612 * value)
	{
		___thread_20 = value;
		Il2CppCodeGenWriteBarrier(&___thread_20, value);
	}

	inline static int32_t get_offset_of_canUpdate_21() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___canUpdate_21)); }
	inline bool get_canUpdate_21() const { return ___canUpdate_21; }
	inline bool* get_address_of_canUpdate_21() { return &___canUpdate_21; }
	inline void set_canUpdate_21(bool value)
	{
		___canUpdate_21 = value;
	}

	inline static int32_t get_offset_of_threadDeltaTime_22() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___threadDeltaTime_22)); }
	inline double get_threadDeltaTime_22() const { return ___threadDeltaTime_22; }
	inline double* get_address_of_threadDeltaTime_22() { return &___threadDeltaTime_22; }
	inline void set_threadDeltaTime_22(double value)
	{
		___threadDeltaTime_22 = value;
	}

	inline static int32_t get_offset_of_oldDateTime_23() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___oldDateTime_23)); }
	inline DateTime_t693205669  get_oldDateTime_23() const { return ___oldDateTime_23; }
	inline DateTime_t693205669 * get_address_of_oldDateTime_23() { return &___oldDateTime_23; }
	inline void set_oldDateTime_23(DateTime_t693205669  value)
	{
		___oldDateTime_23 = value;
	}

	inline static int32_t get_offset_of_movedObjPos_24() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___movedObjPos_24)); }
	inline Vector2_t2243707579  get_movedObjPos_24() const { return ___movedObjPos_24; }
	inline Vector2_t2243707579 * get_address_of_movedObjPos_24() { return &___movedObjPos_24; }
	inline void set_movedObjPos_24(Vector2_t2243707579  value)
	{
		___movedObjPos_24 = value;
	}

	inline static int32_t get_offset_of_projectorPosition_25() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___projectorPosition_25)); }
	inline Vector2_t2243707579  get_projectorPosition_25() const { return ___projectorPosition_25; }
	inline Vector2_t2243707579 * get_address_of_projectorPosition_25() { return &___projectorPosition_25; }
	inline void set_projectorPosition_25(Vector2_t2243707579  value)
	{
		___projectorPosition_25 = value;
	}

	inline static int32_t get_offset_of__GAmplitude_26() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ____GAmplitude_26)); }
	inline Vector4_t2243707581  get__GAmplitude_26() const { return ____GAmplitude_26; }
	inline Vector4_t2243707581 * get_address_of__GAmplitude_26() { return &____GAmplitude_26; }
	inline void set__GAmplitude_26(Vector4_t2243707581  value)
	{
		____GAmplitude_26 = value;
	}

	inline static int32_t get_offset_of__GFrequency_27() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ____GFrequency_27)); }
	inline Vector4_t2243707581  get__GFrequency_27() const { return ____GFrequency_27; }
	inline Vector4_t2243707581 * get_address_of__GFrequency_27() { return &____GFrequency_27; }
	inline void set__GFrequency_27(Vector4_t2243707581  value)
	{
		____GFrequency_27 = value;
	}

	inline static int32_t get_offset_of__GSteepness_28() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ____GSteepness_28)); }
	inline Vector4_t2243707581  get__GSteepness_28() const { return ____GSteepness_28; }
	inline Vector4_t2243707581 * get_address_of__GSteepness_28() { return &____GSteepness_28; }
	inline void set__GSteepness_28(Vector4_t2243707581  value)
	{
		____GSteepness_28 = value;
	}

	inline static int32_t get_offset_of__GSpeed_29() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ____GSpeed_29)); }
	inline Vector4_t2243707581  get__GSpeed_29() const { return ____GSpeed_29; }
	inline Vector4_t2243707581 * get_address_of__GSpeed_29() { return &____GSpeed_29; }
	inline void set__GSpeed_29(Vector4_t2243707581  value)
	{
		____GSpeed_29 = value;
	}

	inline static int32_t get_offset_of__GDirectionAB_30() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ____GDirectionAB_30)); }
	inline Vector4_t2243707581  get__GDirectionAB_30() const { return ____GDirectionAB_30; }
	inline Vector4_t2243707581 * get_address_of__GDirectionAB_30() { return &____GDirectionAB_30; }
	inline void set__GDirectionAB_30(Vector4_t2243707581  value)
	{
		____GDirectionAB_30 = value;
	}

	inline static int32_t get_offset_of__GDirectionCD_31() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ____GDirectionCD_31)); }
	inline Vector4_t2243707581  get__GDirectionCD_31() const { return ____GDirectionCD_31; }
	inline Vector4_t2243707581 * get_address_of__GDirectionCD_31() { return &____GDirectionCD_31; }
	inline void set__GDirectionCD_31(Vector4_t2243707581  value)
	{
		____GDirectionCD_31 = value;
	}

	inline static int32_t get_offset_of_time_32() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___time_32)); }
	inline float get_time_32() const { return ___time_32; }
	inline float* get_address_of_time_32() { return &___time_32; }
	inline void set_time_32(float value)
	{
		___time_32 = value;
	}

	inline static int32_t get_offset_of_k_33() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___k_33)); }
	inline int32_t get_k_33() const { return ___k_33; }
	inline int32_t* get_address_of_k_33() { return &___k_33; }
	inline void set_k_33(int32_t value)
	{
		___k_33 = value;
	}

	inline static int32_t get_offset_of_isStarted_34() { return static_cast<int32_t>(offsetof(WaterRipplesSTL_t2890341929, ___isStarted_34)); }
	inline bool get_isStarted_34() const { return ___isStarted_34; }
	inline bool* get_address_of_isStarted_34() { return &___isStarted_34; }
	inline void set_isStarted_34(bool value)
	{
		___isStarted_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
