﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldContents
struct  WorldContents_t112941966  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button WorldContents::MyPageButton
	Button_t2872111280 * ___MyPageButton_2;
	// UnityEngine.UI.Button[] WorldContents::Menus
	ButtonU5BU5D_t3071100561* ___Menus_3;
	// UnityEngine.UI.Button WorldContents::WorldMapButton
	Button_t2872111280 * ___WorldMapButton_4;
	// UnityEngine.UI.RawImage WorldContents::MiniMap
	RawImage_t2749640213 * ___MiniMap_5;
	// UnityEngine.GameObject WorldContents::Navigate
	GameObject_t1756533147 * ___Navigate_6;
	// System.Boolean WorldContents::m_bisTutorialRecordReady
	bool ___m_bisTutorialRecordReady_7;

public:
	inline static int32_t get_offset_of_MyPageButton_2() { return static_cast<int32_t>(offsetof(WorldContents_t112941966, ___MyPageButton_2)); }
	inline Button_t2872111280 * get_MyPageButton_2() const { return ___MyPageButton_2; }
	inline Button_t2872111280 ** get_address_of_MyPageButton_2() { return &___MyPageButton_2; }
	inline void set_MyPageButton_2(Button_t2872111280 * value)
	{
		___MyPageButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___MyPageButton_2, value);
	}

	inline static int32_t get_offset_of_Menus_3() { return static_cast<int32_t>(offsetof(WorldContents_t112941966, ___Menus_3)); }
	inline ButtonU5BU5D_t3071100561* get_Menus_3() const { return ___Menus_3; }
	inline ButtonU5BU5D_t3071100561** get_address_of_Menus_3() { return &___Menus_3; }
	inline void set_Menus_3(ButtonU5BU5D_t3071100561* value)
	{
		___Menus_3 = value;
		Il2CppCodeGenWriteBarrier(&___Menus_3, value);
	}

	inline static int32_t get_offset_of_WorldMapButton_4() { return static_cast<int32_t>(offsetof(WorldContents_t112941966, ___WorldMapButton_4)); }
	inline Button_t2872111280 * get_WorldMapButton_4() const { return ___WorldMapButton_4; }
	inline Button_t2872111280 ** get_address_of_WorldMapButton_4() { return &___WorldMapButton_4; }
	inline void set_WorldMapButton_4(Button_t2872111280 * value)
	{
		___WorldMapButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___WorldMapButton_4, value);
	}

	inline static int32_t get_offset_of_MiniMap_5() { return static_cast<int32_t>(offsetof(WorldContents_t112941966, ___MiniMap_5)); }
	inline RawImage_t2749640213 * get_MiniMap_5() const { return ___MiniMap_5; }
	inline RawImage_t2749640213 ** get_address_of_MiniMap_5() { return &___MiniMap_5; }
	inline void set_MiniMap_5(RawImage_t2749640213 * value)
	{
		___MiniMap_5 = value;
		Il2CppCodeGenWriteBarrier(&___MiniMap_5, value);
	}

	inline static int32_t get_offset_of_Navigate_6() { return static_cast<int32_t>(offsetof(WorldContents_t112941966, ___Navigate_6)); }
	inline GameObject_t1756533147 * get_Navigate_6() const { return ___Navigate_6; }
	inline GameObject_t1756533147 ** get_address_of_Navigate_6() { return &___Navigate_6; }
	inline void set_Navigate_6(GameObject_t1756533147 * value)
	{
		___Navigate_6 = value;
		Il2CppCodeGenWriteBarrier(&___Navigate_6, value);
	}

	inline static int32_t get_offset_of_m_bisTutorialRecordReady_7() { return static_cast<int32_t>(offsetof(WorldContents_t112941966, ___m_bisTutorialRecordReady_7)); }
	inline bool get_m_bisTutorialRecordReady_7() const { return ___m_bisTutorialRecordReady_7; }
	inline bool* get_address_of_m_bisTutorialRecordReady_7() { return &___m_bisTutorialRecordReady_7; }
	inline void set_m_bisTutorialRecordReady_7(bool value)
	{
		___m_bisTutorialRecordReady_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
