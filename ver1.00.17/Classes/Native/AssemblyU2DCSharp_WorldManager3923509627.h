﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2620185962.h"
#include "AssemblyU2DCSharp_eAREATYPE1739175762.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// WorldContents
struct WorldContents_t112941966;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// AreaParentObject[]
struct AreaParentObjectU5BU5D_t1561363691;
// System.Collections.Generic.Dictionary`2<eAREATYPE,UnityEngine.GameObject>
struct Dictionary_2_t2335485196;
// System.Single[0...,0...]
struct SingleU5BU2CU5D_t577127398;
// System.Collections.Generic.List`1<LonLatitude>
struct List_1_t2162366495;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t3671312409;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldManager
struct  WorldManager_t3923509627  : public SingletonMonoBehaviour_1_t2620185962
{
public:
	// UnityEngine.Transform WorldManager::m_Planet
	Transform_t3275118058 * ___m_Planet_3;
	// UnityEngine.GameObject WorldManager::PlanetMap
	GameObject_t1756533147 * ___PlanetMap_4;
	// UnityEngine.Transform WorldManager::TitleMiniPlanet
	Transform_t3275118058 * ___TitleMiniPlanet_5;
	// UnityEngine.Transform WorldManager::MiniPlanet
	Transform_t3275118058 * ___MiniPlanet_6;
	// UnityEngine.Transform WorldManager::MyPageMiniPlanet
	Transform_t3275118058 * ___MyPageMiniPlanet_7;
	// UnityEngine.Transform WorldManager::m_DounutRock
	Transform_t3275118058 * ___m_DounutRock_8;
	// UnityEngine.Transform WorldManager::m_WaterSpace
	Transform_t3275118058 * ___m_WaterSpace_9;
	// UnityEngine.GameObject WorldManager::SnowEffect
	GameObject_t1756533147 * ___SnowEffect_10;
	// UnityEngine.GameObject WorldManager::PenStorm
	GameObject_t1756533147 * ___PenStorm_11;
	// UnityEngine.Transform WorldManager::m_OaWaterSpace
	Transform_t3275118058 * ___m_OaWaterSpace_12;
	// UnityEngine.Transform WorldManager::m_PenginArea
	Transform_t3275118058 * ___m_PenginArea_13;
	// WorldContents WorldManager::Contents
	WorldContents_t112941966 * ___Contents_14;
	// UnityEngine.GameObject WorldManager::GetModulePanel
	GameObject_t1756533147 * ___GetModulePanel_15;
	// UnityEngine.GameObject[] WorldManager::DnaUiList
	GameObjectU5BU5D_t3057952154* ___DnaUiList_16;
	// UnityEngine.GameObject[] WorldManager::EreaFloor
	GameObjectU5BU5D_t3057952154* ___EreaFloor_17;
	// AreaParentObject[] WorldManager::AreaParentObject
	AreaParentObjectU5BU5D_t1561363691* ___AreaParentObject_18;
	// System.Collections.Generic.Dictionary`2<eAREATYPE,UnityEngine.GameObject> WorldManager::areaObjects
	Dictionary_2_t2335485196 * ___areaObjects_19;
	// System.Int32 WorldManager::SelectSpot
	int32_t ___SelectSpot_20;
	// System.Single WorldManager::Latitude
	float ___Latitude_21;
	// System.Single WorldManager::Longitude
	float ___Longitude_22;
	// System.Single WorldManager::PlanetRadius
	float ___PlanetRadius_23;
	// UnityEngine.GameObject WorldManager::CubeRock
	GameObject_t1756533147 * ___CubeRock_24;
	// UnityEngine.GameObject WorldManager::DounutRock
	GameObject_t1756533147 * ___DounutRock_25;
	// UnityEngine.Transform WorldManager::DounutArea
	Transform_t3275118058 * ___DounutArea_26;
	// System.Single WorldManager::rockNum
	float ___rockNum_27;
	// System.Single[0...,0...] WorldManager::Ramlat
	SingleU5BU2CU5D_t577127398* ___Ramlat_28;
	// System.Single[0...,0...] WorldManager::Ramlon
	SingleU5BU2CU5D_t577127398* ___Ramlon_29;
	// System.Boolean WorldManager::inWater
	bool ___inWater_30;
	// eAREATYPE WorldManager::rockArea
	int32_t ___rockArea_31;
	// System.Single WorldManager::lat
	float ___lat_32;
	// System.Single WorldManager::lng
	float ___lng_33;
	// UnityEngine.Vector2 WorldManager::lonlat
	Vector2_t2243707579  ___lonlat_34;
	// UnityEngine.GameObject WorldManager::m_DounutRocks
	GameObject_t1756533147 * ___m_DounutRocks_35;
	// System.Collections.Generic.List`1<LonLatitude> WorldManager::LonlatData
	List_1_t2162366495 * ___LonlatData_36;
	// System.Boolean WorldManager::isActionSettingInWorld
	bool ___isActionSettingInWorld_37;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> WorldManager::rockDic
	Dictionary_2_t3671312409 * ___rockDic_38;
	// System.String[] WorldManager::m_areaNames
	StringU5BU5D_t1642385972* ___m_areaNames_39;
	// System.String[] WorldManager::m_areaLeads
	StringU5BU5D_t1642385972* ___m_areaLeads_40;
	// eAREATYPE WorldManager::NowArea
	int32_t ___NowArea_41;
	// eAREATYPE WorldManager::NextArea
	int32_t ___NextArea_42;

public:
	inline static int32_t get_offset_of_m_Planet_3() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_Planet_3)); }
	inline Transform_t3275118058 * get_m_Planet_3() const { return ___m_Planet_3; }
	inline Transform_t3275118058 ** get_address_of_m_Planet_3() { return &___m_Planet_3; }
	inline void set_m_Planet_3(Transform_t3275118058 * value)
	{
		___m_Planet_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Planet_3, value);
	}

	inline static int32_t get_offset_of_PlanetMap_4() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___PlanetMap_4)); }
	inline GameObject_t1756533147 * get_PlanetMap_4() const { return ___PlanetMap_4; }
	inline GameObject_t1756533147 ** get_address_of_PlanetMap_4() { return &___PlanetMap_4; }
	inline void set_PlanetMap_4(GameObject_t1756533147 * value)
	{
		___PlanetMap_4 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetMap_4, value);
	}

	inline static int32_t get_offset_of_TitleMiniPlanet_5() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___TitleMiniPlanet_5)); }
	inline Transform_t3275118058 * get_TitleMiniPlanet_5() const { return ___TitleMiniPlanet_5; }
	inline Transform_t3275118058 ** get_address_of_TitleMiniPlanet_5() { return &___TitleMiniPlanet_5; }
	inline void set_TitleMiniPlanet_5(Transform_t3275118058 * value)
	{
		___TitleMiniPlanet_5 = value;
		Il2CppCodeGenWriteBarrier(&___TitleMiniPlanet_5, value);
	}

	inline static int32_t get_offset_of_MiniPlanet_6() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___MiniPlanet_6)); }
	inline Transform_t3275118058 * get_MiniPlanet_6() const { return ___MiniPlanet_6; }
	inline Transform_t3275118058 ** get_address_of_MiniPlanet_6() { return &___MiniPlanet_6; }
	inline void set_MiniPlanet_6(Transform_t3275118058 * value)
	{
		___MiniPlanet_6 = value;
		Il2CppCodeGenWriteBarrier(&___MiniPlanet_6, value);
	}

	inline static int32_t get_offset_of_MyPageMiniPlanet_7() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___MyPageMiniPlanet_7)); }
	inline Transform_t3275118058 * get_MyPageMiniPlanet_7() const { return ___MyPageMiniPlanet_7; }
	inline Transform_t3275118058 ** get_address_of_MyPageMiniPlanet_7() { return &___MyPageMiniPlanet_7; }
	inline void set_MyPageMiniPlanet_7(Transform_t3275118058 * value)
	{
		___MyPageMiniPlanet_7 = value;
		Il2CppCodeGenWriteBarrier(&___MyPageMiniPlanet_7, value);
	}

	inline static int32_t get_offset_of_m_DounutRock_8() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_DounutRock_8)); }
	inline Transform_t3275118058 * get_m_DounutRock_8() const { return ___m_DounutRock_8; }
	inline Transform_t3275118058 ** get_address_of_m_DounutRock_8() { return &___m_DounutRock_8; }
	inline void set_m_DounutRock_8(Transform_t3275118058 * value)
	{
		___m_DounutRock_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_DounutRock_8, value);
	}

	inline static int32_t get_offset_of_m_WaterSpace_9() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_WaterSpace_9)); }
	inline Transform_t3275118058 * get_m_WaterSpace_9() const { return ___m_WaterSpace_9; }
	inline Transform_t3275118058 ** get_address_of_m_WaterSpace_9() { return &___m_WaterSpace_9; }
	inline void set_m_WaterSpace_9(Transform_t3275118058 * value)
	{
		___m_WaterSpace_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_WaterSpace_9, value);
	}

	inline static int32_t get_offset_of_SnowEffect_10() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___SnowEffect_10)); }
	inline GameObject_t1756533147 * get_SnowEffect_10() const { return ___SnowEffect_10; }
	inline GameObject_t1756533147 ** get_address_of_SnowEffect_10() { return &___SnowEffect_10; }
	inline void set_SnowEffect_10(GameObject_t1756533147 * value)
	{
		___SnowEffect_10 = value;
		Il2CppCodeGenWriteBarrier(&___SnowEffect_10, value);
	}

	inline static int32_t get_offset_of_PenStorm_11() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___PenStorm_11)); }
	inline GameObject_t1756533147 * get_PenStorm_11() const { return ___PenStorm_11; }
	inline GameObject_t1756533147 ** get_address_of_PenStorm_11() { return &___PenStorm_11; }
	inline void set_PenStorm_11(GameObject_t1756533147 * value)
	{
		___PenStorm_11 = value;
		Il2CppCodeGenWriteBarrier(&___PenStorm_11, value);
	}

	inline static int32_t get_offset_of_m_OaWaterSpace_12() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_OaWaterSpace_12)); }
	inline Transform_t3275118058 * get_m_OaWaterSpace_12() const { return ___m_OaWaterSpace_12; }
	inline Transform_t3275118058 ** get_address_of_m_OaWaterSpace_12() { return &___m_OaWaterSpace_12; }
	inline void set_m_OaWaterSpace_12(Transform_t3275118058 * value)
	{
		___m_OaWaterSpace_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_OaWaterSpace_12, value);
	}

	inline static int32_t get_offset_of_m_PenginArea_13() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_PenginArea_13)); }
	inline Transform_t3275118058 * get_m_PenginArea_13() const { return ___m_PenginArea_13; }
	inline Transform_t3275118058 ** get_address_of_m_PenginArea_13() { return &___m_PenginArea_13; }
	inline void set_m_PenginArea_13(Transform_t3275118058 * value)
	{
		___m_PenginArea_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_PenginArea_13, value);
	}

	inline static int32_t get_offset_of_Contents_14() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___Contents_14)); }
	inline WorldContents_t112941966 * get_Contents_14() const { return ___Contents_14; }
	inline WorldContents_t112941966 ** get_address_of_Contents_14() { return &___Contents_14; }
	inline void set_Contents_14(WorldContents_t112941966 * value)
	{
		___Contents_14 = value;
		Il2CppCodeGenWriteBarrier(&___Contents_14, value);
	}

	inline static int32_t get_offset_of_GetModulePanel_15() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___GetModulePanel_15)); }
	inline GameObject_t1756533147 * get_GetModulePanel_15() const { return ___GetModulePanel_15; }
	inline GameObject_t1756533147 ** get_address_of_GetModulePanel_15() { return &___GetModulePanel_15; }
	inline void set_GetModulePanel_15(GameObject_t1756533147 * value)
	{
		___GetModulePanel_15 = value;
		Il2CppCodeGenWriteBarrier(&___GetModulePanel_15, value);
	}

	inline static int32_t get_offset_of_DnaUiList_16() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___DnaUiList_16)); }
	inline GameObjectU5BU5D_t3057952154* get_DnaUiList_16() const { return ___DnaUiList_16; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_DnaUiList_16() { return &___DnaUiList_16; }
	inline void set_DnaUiList_16(GameObjectU5BU5D_t3057952154* value)
	{
		___DnaUiList_16 = value;
		Il2CppCodeGenWriteBarrier(&___DnaUiList_16, value);
	}

	inline static int32_t get_offset_of_EreaFloor_17() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___EreaFloor_17)); }
	inline GameObjectU5BU5D_t3057952154* get_EreaFloor_17() const { return ___EreaFloor_17; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_EreaFloor_17() { return &___EreaFloor_17; }
	inline void set_EreaFloor_17(GameObjectU5BU5D_t3057952154* value)
	{
		___EreaFloor_17 = value;
		Il2CppCodeGenWriteBarrier(&___EreaFloor_17, value);
	}

	inline static int32_t get_offset_of_AreaParentObject_18() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___AreaParentObject_18)); }
	inline AreaParentObjectU5BU5D_t1561363691* get_AreaParentObject_18() const { return ___AreaParentObject_18; }
	inline AreaParentObjectU5BU5D_t1561363691** get_address_of_AreaParentObject_18() { return &___AreaParentObject_18; }
	inline void set_AreaParentObject_18(AreaParentObjectU5BU5D_t1561363691* value)
	{
		___AreaParentObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___AreaParentObject_18, value);
	}

	inline static int32_t get_offset_of_areaObjects_19() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___areaObjects_19)); }
	inline Dictionary_2_t2335485196 * get_areaObjects_19() const { return ___areaObjects_19; }
	inline Dictionary_2_t2335485196 ** get_address_of_areaObjects_19() { return &___areaObjects_19; }
	inline void set_areaObjects_19(Dictionary_2_t2335485196 * value)
	{
		___areaObjects_19 = value;
		Il2CppCodeGenWriteBarrier(&___areaObjects_19, value);
	}

	inline static int32_t get_offset_of_SelectSpot_20() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___SelectSpot_20)); }
	inline int32_t get_SelectSpot_20() const { return ___SelectSpot_20; }
	inline int32_t* get_address_of_SelectSpot_20() { return &___SelectSpot_20; }
	inline void set_SelectSpot_20(int32_t value)
	{
		___SelectSpot_20 = value;
	}

	inline static int32_t get_offset_of_Latitude_21() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___Latitude_21)); }
	inline float get_Latitude_21() const { return ___Latitude_21; }
	inline float* get_address_of_Latitude_21() { return &___Latitude_21; }
	inline void set_Latitude_21(float value)
	{
		___Latitude_21 = value;
	}

	inline static int32_t get_offset_of_Longitude_22() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___Longitude_22)); }
	inline float get_Longitude_22() const { return ___Longitude_22; }
	inline float* get_address_of_Longitude_22() { return &___Longitude_22; }
	inline void set_Longitude_22(float value)
	{
		___Longitude_22 = value;
	}

	inline static int32_t get_offset_of_PlanetRadius_23() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___PlanetRadius_23)); }
	inline float get_PlanetRadius_23() const { return ___PlanetRadius_23; }
	inline float* get_address_of_PlanetRadius_23() { return &___PlanetRadius_23; }
	inline void set_PlanetRadius_23(float value)
	{
		___PlanetRadius_23 = value;
	}

	inline static int32_t get_offset_of_CubeRock_24() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___CubeRock_24)); }
	inline GameObject_t1756533147 * get_CubeRock_24() const { return ___CubeRock_24; }
	inline GameObject_t1756533147 ** get_address_of_CubeRock_24() { return &___CubeRock_24; }
	inline void set_CubeRock_24(GameObject_t1756533147 * value)
	{
		___CubeRock_24 = value;
		Il2CppCodeGenWriteBarrier(&___CubeRock_24, value);
	}

	inline static int32_t get_offset_of_DounutRock_25() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___DounutRock_25)); }
	inline GameObject_t1756533147 * get_DounutRock_25() const { return ___DounutRock_25; }
	inline GameObject_t1756533147 ** get_address_of_DounutRock_25() { return &___DounutRock_25; }
	inline void set_DounutRock_25(GameObject_t1756533147 * value)
	{
		___DounutRock_25 = value;
		Il2CppCodeGenWriteBarrier(&___DounutRock_25, value);
	}

	inline static int32_t get_offset_of_DounutArea_26() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___DounutArea_26)); }
	inline Transform_t3275118058 * get_DounutArea_26() const { return ___DounutArea_26; }
	inline Transform_t3275118058 ** get_address_of_DounutArea_26() { return &___DounutArea_26; }
	inline void set_DounutArea_26(Transform_t3275118058 * value)
	{
		___DounutArea_26 = value;
		Il2CppCodeGenWriteBarrier(&___DounutArea_26, value);
	}

	inline static int32_t get_offset_of_rockNum_27() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___rockNum_27)); }
	inline float get_rockNum_27() const { return ___rockNum_27; }
	inline float* get_address_of_rockNum_27() { return &___rockNum_27; }
	inline void set_rockNum_27(float value)
	{
		___rockNum_27 = value;
	}

	inline static int32_t get_offset_of_Ramlat_28() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___Ramlat_28)); }
	inline SingleU5BU2CU5D_t577127398* get_Ramlat_28() const { return ___Ramlat_28; }
	inline SingleU5BU2CU5D_t577127398** get_address_of_Ramlat_28() { return &___Ramlat_28; }
	inline void set_Ramlat_28(SingleU5BU2CU5D_t577127398* value)
	{
		___Ramlat_28 = value;
		Il2CppCodeGenWriteBarrier(&___Ramlat_28, value);
	}

	inline static int32_t get_offset_of_Ramlon_29() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___Ramlon_29)); }
	inline SingleU5BU2CU5D_t577127398* get_Ramlon_29() const { return ___Ramlon_29; }
	inline SingleU5BU2CU5D_t577127398** get_address_of_Ramlon_29() { return &___Ramlon_29; }
	inline void set_Ramlon_29(SingleU5BU2CU5D_t577127398* value)
	{
		___Ramlon_29 = value;
		Il2CppCodeGenWriteBarrier(&___Ramlon_29, value);
	}

	inline static int32_t get_offset_of_inWater_30() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___inWater_30)); }
	inline bool get_inWater_30() const { return ___inWater_30; }
	inline bool* get_address_of_inWater_30() { return &___inWater_30; }
	inline void set_inWater_30(bool value)
	{
		___inWater_30 = value;
	}

	inline static int32_t get_offset_of_rockArea_31() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___rockArea_31)); }
	inline int32_t get_rockArea_31() const { return ___rockArea_31; }
	inline int32_t* get_address_of_rockArea_31() { return &___rockArea_31; }
	inline void set_rockArea_31(int32_t value)
	{
		___rockArea_31 = value;
	}

	inline static int32_t get_offset_of_lat_32() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___lat_32)); }
	inline float get_lat_32() const { return ___lat_32; }
	inline float* get_address_of_lat_32() { return &___lat_32; }
	inline void set_lat_32(float value)
	{
		___lat_32 = value;
	}

	inline static int32_t get_offset_of_lng_33() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___lng_33)); }
	inline float get_lng_33() const { return ___lng_33; }
	inline float* get_address_of_lng_33() { return &___lng_33; }
	inline void set_lng_33(float value)
	{
		___lng_33 = value;
	}

	inline static int32_t get_offset_of_lonlat_34() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___lonlat_34)); }
	inline Vector2_t2243707579  get_lonlat_34() const { return ___lonlat_34; }
	inline Vector2_t2243707579 * get_address_of_lonlat_34() { return &___lonlat_34; }
	inline void set_lonlat_34(Vector2_t2243707579  value)
	{
		___lonlat_34 = value;
	}

	inline static int32_t get_offset_of_m_DounutRocks_35() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_DounutRocks_35)); }
	inline GameObject_t1756533147 * get_m_DounutRocks_35() const { return ___m_DounutRocks_35; }
	inline GameObject_t1756533147 ** get_address_of_m_DounutRocks_35() { return &___m_DounutRocks_35; }
	inline void set_m_DounutRocks_35(GameObject_t1756533147 * value)
	{
		___m_DounutRocks_35 = value;
		Il2CppCodeGenWriteBarrier(&___m_DounutRocks_35, value);
	}

	inline static int32_t get_offset_of_LonlatData_36() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___LonlatData_36)); }
	inline List_1_t2162366495 * get_LonlatData_36() const { return ___LonlatData_36; }
	inline List_1_t2162366495 ** get_address_of_LonlatData_36() { return &___LonlatData_36; }
	inline void set_LonlatData_36(List_1_t2162366495 * value)
	{
		___LonlatData_36 = value;
		Il2CppCodeGenWriteBarrier(&___LonlatData_36, value);
	}

	inline static int32_t get_offset_of_isActionSettingInWorld_37() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___isActionSettingInWorld_37)); }
	inline bool get_isActionSettingInWorld_37() const { return ___isActionSettingInWorld_37; }
	inline bool* get_address_of_isActionSettingInWorld_37() { return &___isActionSettingInWorld_37; }
	inline void set_isActionSettingInWorld_37(bool value)
	{
		___isActionSettingInWorld_37 = value;
	}

	inline static int32_t get_offset_of_rockDic_38() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___rockDic_38)); }
	inline Dictionary_2_t3671312409 * get_rockDic_38() const { return ___rockDic_38; }
	inline Dictionary_2_t3671312409 ** get_address_of_rockDic_38() { return &___rockDic_38; }
	inline void set_rockDic_38(Dictionary_2_t3671312409 * value)
	{
		___rockDic_38 = value;
		Il2CppCodeGenWriteBarrier(&___rockDic_38, value);
	}

	inline static int32_t get_offset_of_m_areaNames_39() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_areaNames_39)); }
	inline StringU5BU5D_t1642385972* get_m_areaNames_39() const { return ___m_areaNames_39; }
	inline StringU5BU5D_t1642385972** get_address_of_m_areaNames_39() { return &___m_areaNames_39; }
	inline void set_m_areaNames_39(StringU5BU5D_t1642385972* value)
	{
		___m_areaNames_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_areaNames_39, value);
	}

	inline static int32_t get_offset_of_m_areaLeads_40() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___m_areaLeads_40)); }
	inline StringU5BU5D_t1642385972* get_m_areaLeads_40() const { return ___m_areaLeads_40; }
	inline StringU5BU5D_t1642385972** get_address_of_m_areaLeads_40() { return &___m_areaLeads_40; }
	inline void set_m_areaLeads_40(StringU5BU5D_t1642385972* value)
	{
		___m_areaLeads_40 = value;
		Il2CppCodeGenWriteBarrier(&___m_areaLeads_40, value);
	}

	inline static int32_t get_offset_of_NowArea_41() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___NowArea_41)); }
	inline int32_t get_NowArea_41() const { return ___NowArea_41; }
	inline int32_t* get_address_of_NowArea_41() { return &___NowArea_41; }
	inline void set_NowArea_41(int32_t value)
	{
		___NowArea_41 = value;
	}

	inline static int32_t get_offset_of_NextArea_42() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627, ___NextArea_42)); }
	inline int32_t get_NextArea_42() const { return ___NextArea_42; }
	inline int32_t* get_address_of_NextArea_42() { return &___NextArea_42; }
	inline void set_NextArea_42(int32_t value)
	{
		___NextArea_42 = value;
	}
};

struct WorldManager_t3923509627_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> WorldManager::<>f__switch$map2
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2_43;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_43() { return static_cast<int32_t>(offsetof(WorldManager_t3923509627_StaticFields, ___U3CU3Ef__switchU24map2_43)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2_43() const { return ___U3CU3Ef__switchU24map2_43; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2_43() { return &___U3CU3Ef__switchU24map2_43; }
	inline void set_U3CU3Ef__switchU24map2_43(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2_43 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_43, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
