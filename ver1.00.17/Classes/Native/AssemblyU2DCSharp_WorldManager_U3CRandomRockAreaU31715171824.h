﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1697096169.h"

// LonLatitude
struct LonLatitude_t2793245363;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// WorldManager
struct WorldManager_t3923509627;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldManager/<RandomRockArea>c__Iterator0
struct  U3CRandomRockAreaU3Ec__Iterator0_t1715171824  : public Il2CppObject
{
public:
	// System.Int32 WorldManager/<RandomRockArea>c__Iterator0::<count>__0
	int32_t ___U3CcountU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<LonLatitude> WorldManager/<RandomRockArea>c__Iterator0::$locvar0
	Enumerator_t1697096169  ___U24locvar0_1;
	// LonLatitude WorldManager/<RandomRockArea>c__Iterator0::<lonlat>__1
	LonLatitude_t2793245363 * ___U3ClonlatU3E__1_2;
	// UnityEngine.GameObject WorldManager/<RandomRockArea>c__Iterator0::<rock>__2
	GameObject_t1756533147 * ___U3CrockU3E__2_3;
	// WorldManager WorldManager/<RandomRockArea>c__Iterator0::$this
	WorldManager_t3923509627 * ___U24this_4;
	// System.Object WorldManager/<RandomRockArea>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean WorldManager/<RandomRockArea>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WorldManager/<RandomRockArea>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CcountU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U3CcountU3E__0_0)); }
	inline int32_t get_U3CcountU3E__0_0() const { return ___U3CcountU3E__0_0; }
	inline int32_t* get_address_of_U3CcountU3E__0_0() { return &___U3CcountU3E__0_0; }
	inline void set_U3CcountU3E__0_0(int32_t value)
	{
		___U3CcountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U24locvar0_1)); }
	inline Enumerator_t1697096169  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t1697096169 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t1697096169  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3ClonlatU3E__1_2() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U3ClonlatU3E__1_2)); }
	inline LonLatitude_t2793245363 * get_U3ClonlatU3E__1_2() const { return ___U3ClonlatU3E__1_2; }
	inline LonLatitude_t2793245363 ** get_address_of_U3ClonlatU3E__1_2() { return &___U3ClonlatU3E__1_2; }
	inline void set_U3ClonlatU3E__1_2(LonLatitude_t2793245363 * value)
	{
		___U3ClonlatU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClonlatU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CrockU3E__2_3() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U3CrockU3E__2_3)); }
	inline GameObject_t1756533147 * get_U3CrockU3E__2_3() const { return ___U3CrockU3E__2_3; }
	inline GameObject_t1756533147 ** get_address_of_U3CrockU3E__2_3() { return &___U3CrockU3E__2_3; }
	inline void set_U3CrockU3E__2_3(GameObject_t1756533147 * value)
	{
		___U3CrockU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrockU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U24this_4)); }
	inline WorldManager_t3923509627 * get_U24this_4() const { return ___U24this_4; }
	inline WorldManager_t3923509627 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(WorldManager_t3923509627 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRandomRockAreaU3Ec__Iterator0_t1715171824, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
