﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.UI.RawImage
struct RawImage_t2749640213;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapBackgroundScroll
struct  WorldMapBackgroundScroll_t1215783667  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.RawImage WorldMapBackgroundScroll::m_Background
	RawImage_t2749640213 * ___m_Background_2;
	// UnityEngine.Vector2 WorldMapBackgroundScroll::m_vUV
	Vector2_t2243707579  ___m_vUV_3;

public:
	inline static int32_t get_offset_of_m_Background_2() { return static_cast<int32_t>(offsetof(WorldMapBackgroundScroll_t1215783667, ___m_Background_2)); }
	inline RawImage_t2749640213 * get_m_Background_2() const { return ___m_Background_2; }
	inline RawImage_t2749640213 ** get_address_of_m_Background_2() { return &___m_Background_2; }
	inline void set_m_Background_2(RawImage_t2749640213 * value)
	{
		___m_Background_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Background_2, value);
	}

	inline static int32_t get_offset_of_m_vUV_3() { return static_cast<int32_t>(offsetof(WorldMapBackgroundScroll_t1215783667, ___m_vUV_3)); }
	inline Vector2_t2243707579  get_m_vUV_3() const { return ___m_vUV_3; }
	inline Vector2_t2243707579 * get_address_of_m_vUV_3() { return &___m_vUV_3; }
	inline void set_m_vUV_3(Vector2_t2243707579  value)
	{
		___m_vUV_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
