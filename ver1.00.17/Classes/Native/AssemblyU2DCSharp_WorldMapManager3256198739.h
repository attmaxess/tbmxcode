﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1952875074.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapManager
struct  WorldMapManager_t3256198739  : public SingletonMonoBehaviour_1_t1952875074
{
public:
	// UnityEngine.GameObject WorldMapManager::WorldMapSkydome
	GameObject_t1756533147 * ___WorldMapSkydome_3;
	// UnityEngine.GameObject[] WorldMapManager::MiniAreas
	GameObjectU5BU5D_t3057952154* ___MiniAreas_4;
	// System.Single WorldMapManager::SkydomeRate
	float ___SkydomeRate_5;
	// System.Boolean WorldMapManager::IsCreatedMiniMap
	bool ___IsCreatedMiniMap_6;
	// System.Boolean WorldMapManager::AutoRotation
	bool ___AutoRotation_7;
	// System.Boolean WorldMapManager::Reverse
	bool ___Reverse_8;
	// System.Single WorldMapManager::AutoSpinX
	float ___AutoSpinX_9;
	// System.Single WorldMapManager::AutoSpinY
	float ___AutoSpinY_10;
	// System.Single WorldMapManager::AutoSpinReverseX
	float ___AutoSpinReverseX_11;
	// System.Single WorldMapManager::AutoSpinReverseY
	float ___AutoSpinReverseY_12;
	// UnityEngine.Vector3[] WorldMapManager::vAreaPoints
	Vector3U5BU5D_t1172311765* ___vAreaPoints_13;

public:
	inline static int32_t get_offset_of_WorldMapSkydome_3() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___WorldMapSkydome_3)); }
	inline GameObject_t1756533147 * get_WorldMapSkydome_3() const { return ___WorldMapSkydome_3; }
	inline GameObject_t1756533147 ** get_address_of_WorldMapSkydome_3() { return &___WorldMapSkydome_3; }
	inline void set_WorldMapSkydome_3(GameObject_t1756533147 * value)
	{
		___WorldMapSkydome_3 = value;
		Il2CppCodeGenWriteBarrier(&___WorldMapSkydome_3, value);
	}

	inline static int32_t get_offset_of_MiniAreas_4() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___MiniAreas_4)); }
	inline GameObjectU5BU5D_t3057952154* get_MiniAreas_4() const { return ___MiniAreas_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_MiniAreas_4() { return &___MiniAreas_4; }
	inline void set_MiniAreas_4(GameObjectU5BU5D_t3057952154* value)
	{
		___MiniAreas_4 = value;
		Il2CppCodeGenWriteBarrier(&___MiniAreas_4, value);
	}

	inline static int32_t get_offset_of_SkydomeRate_5() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___SkydomeRate_5)); }
	inline float get_SkydomeRate_5() const { return ___SkydomeRate_5; }
	inline float* get_address_of_SkydomeRate_5() { return &___SkydomeRate_5; }
	inline void set_SkydomeRate_5(float value)
	{
		___SkydomeRate_5 = value;
	}

	inline static int32_t get_offset_of_IsCreatedMiniMap_6() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___IsCreatedMiniMap_6)); }
	inline bool get_IsCreatedMiniMap_6() const { return ___IsCreatedMiniMap_6; }
	inline bool* get_address_of_IsCreatedMiniMap_6() { return &___IsCreatedMiniMap_6; }
	inline void set_IsCreatedMiniMap_6(bool value)
	{
		___IsCreatedMiniMap_6 = value;
	}

	inline static int32_t get_offset_of_AutoRotation_7() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___AutoRotation_7)); }
	inline bool get_AutoRotation_7() const { return ___AutoRotation_7; }
	inline bool* get_address_of_AutoRotation_7() { return &___AutoRotation_7; }
	inline void set_AutoRotation_7(bool value)
	{
		___AutoRotation_7 = value;
	}

	inline static int32_t get_offset_of_Reverse_8() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___Reverse_8)); }
	inline bool get_Reverse_8() const { return ___Reverse_8; }
	inline bool* get_address_of_Reverse_8() { return &___Reverse_8; }
	inline void set_Reverse_8(bool value)
	{
		___Reverse_8 = value;
	}

	inline static int32_t get_offset_of_AutoSpinX_9() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___AutoSpinX_9)); }
	inline float get_AutoSpinX_9() const { return ___AutoSpinX_9; }
	inline float* get_address_of_AutoSpinX_9() { return &___AutoSpinX_9; }
	inline void set_AutoSpinX_9(float value)
	{
		___AutoSpinX_9 = value;
	}

	inline static int32_t get_offset_of_AutoSpinY_10() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___AutoSpinY_10)); }
	inline float get_AutoSpinY_10() const { return ___AutoSpinY_10; }
	inline float* get_address_of_AutoSpinY_10() { return &___AutoSpinY_10; }
	inline void set_AutoSpinY_10(float value)
	{
		___AutoSpinY_10 = value;
	}

	inline static int32_t get_offset_of_AutoSpinReverseX_11() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___AutoSpinReverseX_11)); }
	inline float get_AutoSpinReverseX_11() const { return ___AutoSpinReverseX_11; }
	inline float* get_address_of_AutoSpinReverseX_11() { return &___AutoSpinReverseX_11; }
	inline void set_AutoSpinReverseX_11(float value)
	{
		___AutoSpinReverseX_11 = value;
	}

	inline static int32_t get_offset_of_AutoSpinReverseY_12() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___AutoSpinReverseY_12)); }
	inline float get_AutoSpinReverseY_12() const { return ___AutoSpinReverseY_12; }
	inline float* get_address_of_AutoSpinReverseY_12() { return &___AutoSpinReverseY_12; }
	inline void set_AutoSpinReverseY_12(float value)
	{
		___AutoSpinReverseY_12 = value;
	}

	inline static int32_t get_offset_of_vAreaPoints_13() { return static_cast<int32_t>(offsetof(WorldMapManager_t3256198739, ___vAreaPoints_13)); }
	inline Vector3U5BU5D_t1172311765* get_vAreaPoints_13() const { return ___vAreaPoints_13; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vAreaPoints_13() { return &___vAreaPoints_13; }
	inline void set_vAreaPoints_13(Vector3U5BU5D_t1172311765* value)
	{
		___vAreaPoints_13 = value;
		Il2CppCodeGenWriteBarrier(&___vAreaPoints_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
