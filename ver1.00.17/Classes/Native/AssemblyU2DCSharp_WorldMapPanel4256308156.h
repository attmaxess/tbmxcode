﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1967201810;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Shadow
struct Shadow_t4269599528;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// SelectAreaButtonListener
struct SelectAreaButtonListener_t2714250369;
// AreaButton[]
struct AreaButtonU5BU5D_t3372860966;
// AreaSelectPanel
struct AreaSelectPanel_t4230688919;
// AreaPoint[]
struct AreaPointU5BU5D_t2806345972;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// FeedbackEffect
struct FeedbackEffect_t3658613102;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t184887326;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3948421699;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapPanel
struct  WorldMapPanel_t4256308156  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventTrigger WorldMapPanel::CloseButtonTrigger
	EventTrigger_t1967201810 * ___CloseButtonTrigger_2;
	// UnityEngine.UI.Text WorldMapPanel::Go
	Text_t356221433 * ___Go_3;
	// UnityEngine.UI.Shadow WorldMapPanel::GoOutline
	Shadow_t4269599528 * ___GoOutline_4;
	// UnityEngine.GameObject[] WorldMapPanel::Frames
	GameObjectU5BU5D_t3057952154* ___Frames_5;
	// UnityEngine.Sprite[] WorldMapPanel::AreaIcons
	SpriteU5BU5D_t3359083662* ___AreaIcons_6;
	// UnityEngine.CanvasGroup WorldMapPanel::GoButtonGroup
	CanvasGroup_t3296560743 * ___GoButtonGroup_7;
	// SelectAreaButtonListener WorldMapPanel::SelectButtons
	SelectAreaButtonListener_t2714250369 * ___SelectButtons_8;
	// AreaButton[] WorldMapPanel::AreaButtons
	AreaButtonU5BU5D_t3372860966* ___AreaButtons_9;
	// AreaSelectPanel WorldMapPanel::SelectPanel
	AreaSelectPanel_t4230688919 * ___SelectPanel_10;
	// UnityEngine.CanvasGroup WorldMapPanel::SelectPanelGroup
	CanvasGroup_t3296560743 * ___SelectPanelGroup_11;
	// AreaPoint[] WorldMapPanel::AreaPoints
	AreaPointU5BU5D_t2806345972* ___AreaPoints_12;
	// System.Int32 WorldMapPanel::IndexArea
	int32_t ___IndexArea_13;
	// System.Single WorldMapPanel::Offset
	float ___Offset_14;
	// UnityEngine.AnimationCurve WorldMapPanel::OneBounceEasing
	AnimationCurve_t3306541151 * ___OneBounceEasing_15;
	// FeedbackEffect WorldMapPanel::WorldMapEffect
	FeedbackEffect_t3658613102 * ___WorldMapEffect_16;
	// UnityEngine.CanvasGroup WorldMapPanel::Panel
	CanvasGroup_t3296560743 * ___Panel_17;
	// System.Single WorldMapPanel::FadeOutTime
	float ___FadeOutTime_18;
	// UnityEngine.RectTransform WorldMapPanel::m_RectTransSelectButtons
	RectTransform_t3349966182 * ___m_RectTransSelectButtons_19;
	// UnityEngine.CanvasGroup[] WorldMapPanel::m_AreaButtonsGroup
	CanvasGroupU5BU5D_t184887326* ___m_AreaButtonsGroup_20;
	// System.Boolean WorldMapPanel::m_bSelectAreaTween
	bool ___m_bSelectAreaTween_21;
	// UnityEngine.RectTransform WorldMapPanel::m_RectTransCloseButton
	RectTransform_t3349966182 * ___m_RectTransCloseButton_22;
	// UnityEngine.RectTransform[] WorldMapPanel::m_RectTransFrames
	RectTransformU5BU5D_t3948421699* ___m_RectTransFrames_23;
	// System.Boolean WorldMapPanel::m_bSkip
	bool ___m_bSkip_24;

public:
	inline static int32_t get_offset_of_CloseButtonTrigger_2() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___CloseButtonTrigger_2)); }
	inline EventTrigger_t1967201810 * get_CloseButtonTrigger_2() const { return ___CloseButtonTrigger_2; }
	inline EventTrigger_t1967201810 ** get_address_of_CloseButtonTrigger_2() { return &___CloseButtonTrigger_2; }
	inline void set_CloseButtonTrigger_2(EventTrigger_t1967201810 * value)
	{
		___CloseButtonTrigger_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButtonTrigger_2, value);
	}

	inline static int32_t get_offset_of_Go_3() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___Go_3)); }
	inline Text_t356221433 * get_Go_3() const { return ___Go_3; }
	inline Text_t356221433 ** get_address_of_Go_3() { return &___Go_3; }
	inline void set_Go_3(Text_t356221433 * value)
	{
		___Go_3 = value;
		Il2CppCodeGenWriteBarrier(&___Go_3, value);
	}

	inline static int32_t get_offset_of_GoOutline_4() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___GoOutline_4)); }
	inline Shadow_t4269599528 * get_GoOutline_4() const { return ___GoOutline_4; }
	inline Shadow_t4269599528 ** get_address_of_GoOutline_4() { return &___GoOutline_4; }
	inline void set_GoOutline_4(Shadow_t4269599528 * value)
	{
		___GoOutline_4 = value;
		Il2CppCodeGenWriteBarrier(&___GoOutline_4, value);
	}

	inline static int32_t get_offset_of_Frames_5() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___Frames_5)); }
	inline GameObjectU5BU5D_t3057952154* get_Frames_5() const { return ___Frames_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Frames_5() { return &___Frames_5; }
	inline void set_Frames_5(GameObjectU5BU5D_t3057952154* value)
	{
		___Frames_5 = value;
		Il2CppCodeGenWriteBarrier(&___Frames_5, value);
	}

	inline static int32_t get_offset_of_AreaIcons_6() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___AreaIcons_6)); }
	inline SpriteU5BU5D_t3359083662* get_AreaIcons_6() const { return ___AreaIcons_6; }
	inline SpriteU5BU5D_t3359083662** get_address_of_AreaIcons_6() { return &___AreaIcons_6; }
	inline void set_AreaIcons_6(SpriteU5BU5D_t3359083662* value)
	{
		___AreaIcons_6 = value;
		Il2CppCodeGenWriteBarrier(&___AreaIcons_6, value);
	}

	inline static int32_t get_offset_of_GoButtonGroup_7() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___GoButtonGroup_7)); }
	inline CanvasGroup_t3296560743 * get_GoButtonGroup_7() const { return ___GoButtonGroup_7; }
	inline CanvasGroup_t3296560743 ** get_address_of_GoButtonGroup_7() { return &___GoButtonGroup_7; }
	inline void set_GoButtonGroup_7(CanvasGroup_t3296560743 * value)
	{
		___GoButtonGroup_7 = value;
		Il2CppCodeGenWriteBarrier(&___GoButtonGroup_7, value);
	}

	inline static int32_t get_offset_of_SelectButtons_8() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___SelectButtons_8)); }
	inline SelectAreaButtonListener_t2714250369 * get_SelectButtons_8() const { return ___SelectButtons_8; }
	inline SelectAreaButtonListener_t2714250369 ** get_address_of_SelectButtons_8() { return &___SelectButtons_8; }
	inline void set_SelectButtons_8(SelectAreaButtonListener_t2714250369 * value)
	{
		___SelectButtons_8 = value;
		Il2CppCodeGenWriteBarrier(&___SelectButtons_8, value);
	}

	inline static int32_t get_offset_of_AreaButtons_9() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___AreaButtons_9)); }
	inline AreaButtonU5BU5D_t3372860966* get_AreaButtons_9() const { return ___AreaButtons_9; }
	inline AreaButtonU5BU5D_t3372860966** get_address_of_AreaButtons_9() { return &___AreaButtons_9; }
	inline void set_AreaButtons_9(AreaButtonU5BU5D_t3372860966* value)
	{
		___AreaButtons_9 = value;
		Il2CppCodeGenWriteBarrier(&___AreaButtons_9, value);
	}

	inline static int32_t get_offset_of_SelectPanel_10() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___SelectPanel_10)); }
	inline AreaSelectPanel_t4230688919 * get_SelectPanel_10() const { return ___SelectPanel_10; }
	inline AreaSelectPanel_t4230688919 ** get_address_of_SelectPanel_10() { return &___SelectPanel_10; }
	inline void set_SelectPanel_10(AreaSelectPanel_t4230688919 * value)
	{
		___SelectPanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___SelectPanel_10, value);
	}

	inline static int32_t get_offset_of_SelectPanelGroup_11() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___SelectPanelGroup_11)); }
	inline CanvasGroup_t3296560743 * get_SelectPanelGroup_11() const { return ___SelectPanelGroup_11; }
	inline CanvasGroup_t3296560743 ** get_address_of_SelectPanelGroup_11() { return &___SelectPanelGroup_11; }
	inline void set_SelectPanelGroup_11(CanvasGroup_t3296560743 * value)
	{
		___SelectPanelGroup_11 = value;
		Il2CppCodeGenWriteBarrier(&___SelectPanelGroup_11, value);
	}

	inline static int32_t get_offset_of_AreaPoints_12() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___AreaPoints_12)); }
	inline AreaPointU5BU5D_t2806345972* get_AreaPoints_12() const { return ___AreaPoints_12; }
	inline AreaPointU5BU5D_t2806345972** get_address_of_AreaPoints_12() { return &___AreaPoints_12; }
	inline void set_AreaPoints_12(AreaPointU5BU5D_t2806345972* value)
	{
		___AreaPoints_12 = value;
		Il2CppCodeGenWriteBarrier(&___AreaPoints_12, value);
	}

	inline static int32_t get_offset_of_IndexArea_13() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___IndexArea_13)); }
	inline int32_t get_IndexArea_13() const { return ___IndexArea_13; }
	inline int32_t* get_address_of_IndexArea_13() { return &___IndexArea_13; }
	inline void set_IndexArea_13(int32_t value)
	{
		___IndexArea_13 = value;
	}

	inline static int32_t get_offset_of_Offset_14() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___Offset_14)); }
	inline float get_Offset_14() const { return ___Offset_14; }
	inline float* get_address_of_Offset_14() { return &___Offset_14; }
	inline void set_Offset_14(float value)
	{
		___Offset_14 = value;
	}

	inline static int32_t get_offset_of_OneBounceEasing_15() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___OneBounceEasing_15)); }
	inline AnimationCurve_t3306541151 * get_OneBounceEasing_15() const { return ___OneBounceEasing_15; }
	inline AnimationCurve_t3306541151 ** get_address_of_OneBounceEasing_15() { return &___OneBounceEasing_15; }
	inline void set_OneBounceEasing_15(AnimationCurve_t3306541151 * value)
	{
		___OneBounceEasing_15 = value;
		Il2CppCodeGenWriteBarrier(&___OneBounceEasing_15, value);
	}

	inline static int32_t get_offset_of_WorldMapEffect_16() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___WorldMapEffect_16)); }
	inline FeedbackEffect_t3658613102 * get_WorldMapEffect_16() const { return ___WorldMapEffect_16; }
	inline FeedbackEffect_t3658613102 ** get_address_of_WorldMapEffect_16() { return &___WorldMapEffect_16; }
	inline void set_WorldMapEffect_16(FeedbackEffect_t3658613102 * value)
	{
		___WorldMapEffect_16 = value;
		Il2CppCodeGenWriteBarrier(&___WorldMapEffect_16, value);
	}

	inline static int32_t get_offset_of_Panel_17() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___Panel_17)); }
	inline CanvasGroup_t3296560743 * get_Panel_17() const { return ___Panel_17; }
	inline CanvasGroup_t3296560743 ** get_address_of_Panel_17() { return &___Panel_17; }
	inline void set_Panel_17(CanvasGroup_t3296560743 * value)
	{
		___Panel_17 = value;
		Il2CppCodeGenWriteBarrier(&___Panel_17, value);
	}

	inline static int32_t get_offset_of_FadeOutTime_18() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___FadeOutTime_18)); }
	inline float get_FadeOutTime_18() const { return ___FadeOutTime_18; }
	inline float* get_address_of_FadeOutTime_18() { return &___FadeOutTime_18; }
	inline void set_FadeOutTime_18(float value)
	{
		___FadeOutTime_18 = value;
	}

	inline static int32_t get_offset_of_m_RectTransSelectButtons_19() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___m_RectTransSelectButtons_19)); }
	inline RectTransform_t3349966182 * get_m_RectTransSelectButtons_19() const { return ___m_RectTransSelectButtons_19; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransSelectButtons_19() { return &___m_RectTransSelectButtons_19; }
	inline void set_m_RectTransSelectButtons_19(RectTransform_t3349966182 * value)
	{
		___m_RectTransSelectButtons_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTransSelectButtons_19, value);
	}

	inline static int32_t get_offset_of_m_AreaButtonsGroup_20() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___m_AreaButtonsGroup_20)); }
	inline CanvasGroupU5BU5D_t184887326* get_m_AreaButtonsGroup_20() const { return ___m_AreaButtonsGroup_20; }
	inline CanvasGroupU5BU5D_t184887326** get_address_of_m_AreaButtonsGroup_20() { return &___m_AreaButtonsGroup_20; }
	inline void set_m_AreaButtonsGroup_20(CanvasGroupU5BU5D_t184887326* value)
	{
		___m_AreaButtonsGroup_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_AreaButtonsGroup_20, value);
	}

	inline static int32_t get_offset_of_m_bSelectAreaTween_21() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___m_bSelectAreaTween_21)); }
	inline bool get_m_bSelectAreaTween_21() const { return ___m_bSelectAreaTween_21; }
	inline bool* get_address_of_m_bSelectAreaTween_21() { return &___m_bSelectAreaTween_21; }
	inline void set_m_bSelectAreaTween_21(bool value)
	{
		___m_bSelectAreaTween_21 = value;
	}

	inline static int32_t get_offset_of_m_RectTransCloseButton_22() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___m_RectTransCloseButton_22)); }
	inline RectTransform_t3349966182 * get_m_RectTransCloseButton_22() const { return ___m_RectTransCloseButton_22; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransCloseButton_22() { return &___m_RectTransCloseButton_22; }
	inline void set_m_RectTransCloseButton_22(RectTransform_t3349966182 * value)
	{
		___m_RectTransCloseButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTransCloseButton_22, value);
	}

	inline static int32_t get_offset_of_m_RectTransFrames_23() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___m_RectTransFrames_23)); }
	inline RectTransformU5BU5D_t3948421699* get_m_RectTransFrames_23() const { return ___m_RectTransFrames_23; }
	inline RectTransformU5BU5D_t3948421699** get_address_of_m_RectTransFrames_23() { return &___m_RectTransFrames_23; }
	inline void set_m_RectTransFrames_23(RectTransformU5BU5D_t3948421699* value)
	{
		___m_RectTransFrames_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTransFrames_23, value);
	}

	inline static int32_t get_offset_of_m_bSkip_24() { return static_cast<int32_t>(offsetof(WorldMapPanel_t4256308156, ___m_bSkip_24)); }
	inline bool get_m_bSkip_24() const { return ___m_bSkip_24; }
	inline bool* get_address_of_m_bSkip_24() { return &___m_bSkip_24; }
	inline void set_m_bSkip_24(bool value)
	{
		___m_bSkip_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
