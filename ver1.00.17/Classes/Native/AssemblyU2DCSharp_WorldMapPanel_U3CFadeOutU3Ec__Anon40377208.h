﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// WorldMapPanel
struct WorldMapPanel_t4256308156;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapPanel/<FadeOut>c__AnonStorey0
struct  U3CFadeOutU3Ec__AnonStorey0_t40377208  : public Il2CppObject
{
public:
	// System.Single WorldMapPanel/<FadeOut>c__AnonStorey0::alpha
	float ___alpha_0;
	// WorldMapPanel WorldMapPanel/<FadeOut>c__AnonStorey0::$this
	WorldMapPanel_t4256308156 * ___U24this_1;

public:
	inline static int32_t get_offset_of_alpha_0() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__AnonStorey0_t40377208, ___alpha_0)); }
	inline float get_alpha_0() const { return ___alpha_0; }
	inline float* get_address_of_alpha_0() { return &___alpha_0; }
	inline void set_alpha_0(float value)
	{
		___alpha_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__AnonStorey0_t40377208, ___U24this_1)); }
	inline WorldMapPanel_t4256308156 * get_U24this_1() const { return ___U24this_1; }
	inline WorldMapPanel_t4256308156 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WorldMapPanel_t4256308156 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
