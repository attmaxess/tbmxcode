﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// WorldMapPanel
struct WorldMapPanel_t4256308156;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldMapPanel/<_Show>c__AnonStorey2
struct  U3C_ShowU3Ec__AnonStorey2_t2070401948  : public Il2CppObject
{
public:
	// System.Single WorldMapPanel/<_Show>c__AnonStorey2::normal
	float ___normal_0;
	// System.Single WorldMapPanel/<_Show>c__AnonStorey2::time
	float ___time_1;
	// WorldMapPanel WorldMapPanel/<_Show>c__AnonStorey2::$this
	WorldMapPanel_t4256308156 * ___U24this_2;

public:
	inline static int32_t get_offset_of_normal_0() { return static_cast<int32_t>(offsetof(U3C_ShowU3Ec__AnonStorey2_t2070401948, ___normal_0)); }
	inline float get_normal_0() const { return ___normal_0; }
	inline float* get_address_of_normal_0() { return &___normal_0; }
	inline void set_normal_0(float value)
	{
		___normal_0 = value;
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(U3C_ShowU3Ec__AnonStorey2_t2070401948, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3C_ShowU3Ec__AnonStorey2_t2070401948, ___U24this_2)); }
	inline WorldMapPanel_t4256308156 * get_U24this_2() const { return ___U24this_2; }
	inline WorldMapPanel_t4256308156 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WorldMapPanel_t4256308156 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
