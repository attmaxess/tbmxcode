﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldWall
struct  WorldWall_t284693796  : public MonoBehaviour_t1158329972
{
public:
	// System.Single WorldWall::Distance
	float ___Distance_2;

public:
	inline static int32_t get_offset_of_Distance_2() { return static_cast<int32_t>(offsetof(WorldWall_t284693796, ___Distance_2)); }
	inline float get_Distance_2() const { return ___Distance_2; }
	inline float* get_address_of_Distance_2() { return &___Distance_2; }
	inline void set_Distance_2(float value)
	{
		___Distance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
