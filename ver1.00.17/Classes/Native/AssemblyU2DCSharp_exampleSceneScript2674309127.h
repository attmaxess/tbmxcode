﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// exampleSceneScript
struct  exampleSceneScript_t2674309127  : public MonoBehaviour_t1158329972
{
public:
	// System.Single exampleSceneScript::scale
	float ___scale_2;
	// System.Single exampleSceneScript::intensity
	float ___intensity_3;
	// System.Single exampleSceneScript::alpha
	float ___alpha_4;
	// System.Single exampleSceneScript::alphasub
	float ___alphasub_5;
	// System.Single exampleSceneScript::pow
	float ___pow_6;
	// UnityEngine.Color exampleSceneScript::color
	Color_t2020392075  ___color_7;
	// UnityEngine.Material exampleSceneScript::fogMaterial
	Material_t193706927 * ___fogMaterial_8;

public:
	inline static int32_t get_offset_of_scale_2() { return static_cast<int32_t>(offsetof(exampleSceneScript_t2674309127, ___scale_2)); }
	inline float get_scale_2() const { return ___scale_2; }
	inline float* get_address_of_scale_2() { return &___scale_2; }
	inline void set_scale_2(float value)
	{
		___scale_2 = value;
	}

	inline static int32_t get_offset_of_intensity_3() { return static_cast<int32_t>(offsetof(exampleSceneScript_t2674309127, ___intensity_3)); }
	inline float get_intensity_3() const { return ___intensity_3; }
	inline float* get_address_of_intensity_3() { return &___intensity_3; }
	inline void set_intensity_3(float value)
	{
		___intensity_3 = value;
	}

	inline static int32_t get_offset_of_alpha_4() { return static_cast<int32_t>(offsetof(exampleSceneScript_t2674309127, ___alpha_4)); }
	inline float get_alpha_4() const { return ___alpha_4; }
	inline float* get_address_of_alpha_4() { return &___alpha_4; }
	inline void set_alpha_4(float value)
	{
		___alpha_4 = value;
	}

	inline static int32_t get_offset_of_alphasub_5() { return static_cast<int32_t>(offsetof(exampleSceneScript_t2674309127, ___alphasub_5)); }
	inline float get_alphasub_5() const { return ___alphasub_5; }
	inline float* get_address_of_alphasub_5() { return &___alphasub_5; }
	inline void set_alphasub_5(float value)
	{
		___alphasub_5 = value;
	}

	inline static int32_t get_offset_of_pow_6() { return static_cast<int32_t>(offsetof(exampleSceneScript_t2674309127, ___pow_6)); }
	inline float get_pow_6() const { return ___pow_6; }
	inline float* get_address_of_pow_6() { return &___pow_6; }
	inline void set_pow_6(float value)
	{
		___pow_6 = value;
	}

	inline static int32_t get_offset_of_color_7() { return static_cast<int32_t>(offsetof(exampleSceneScript_t2674309127, ___color_7)); }
	inline Color_t2020392075  get_color_7() const { return ___color_7; }
	inline Color_t2020392075 * get_address_of_color_7() { return &___color_7; }
	inline void set_color_7(Color_t2020392075  value)
	{
		___color_7 = value;
	}

	inline static int32_t get_offset_of_fogMaterial_8() { return static_cast<int32_t>(offsetof(exampleSceneScript_t2674309127, ___fogMaterial_8)); }
	inline Material_t193706927 * get_fogMaterial_8() const { return ___fogMaterial_8; }
	inline Material_t193706927 ** get_address_of_fogMaterial_8() { return &___fogMaterial_8; }
	inline void set_fogMaterial_8(Material_t193706927 * value)
	{
		___fogMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___fogMaterial_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
