﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_eLIGHTTYPE1469766701.h"

// UnityEngine.Light
struct Light_t494725636;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// lightObj
struct  lightObj_t383130471 
{
public:
	// eLIGHTTYPE lightObj::type
	int32_t ___type_0;
	// UnityEngine.Light lightObj::light
	Light_t494725636 * ___light_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(lightObj_t383130471, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_light_1() { return static_cast<int32_t>(offsetof(lightObj_t383130471, ___light_1)); }
	inline Light_t494725636 * get_light_1() const { return ___light_1; }
	inline Light_t494725636 ** get_address_of_light_1() { return &___light_1; }
	inline void set_light_1(Light_t494725636 * value)
	{
		___light_1 = value;
		Il2CppCodeGenWriteBarrier(&___light_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of lightObj
struct lightObj_t383130471_marshaled_pinvoke
{
	int32_t ___type_0;
	Light_t494725636 * ___light_1;
};
// Native definition for COM marshalling of lightObj
struct lightObj_t383130471_marshaled_com
{
	int32_t ___type_0;
	Light_t494725636 * ___light_1;
};
