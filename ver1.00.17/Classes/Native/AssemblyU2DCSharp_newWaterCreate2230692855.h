﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// newWaterCreate
struct  newWaterCreate_t2230692855  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] newWaterCreate::ttt
	GameObjectU5BU5D_t3057952154* ___ttt_2;
	// System.Int32[] newWaterCreate::ttn
	Int32U5BU5D_t3030399641* ___ttn_3;
	// System.Single[] newWaterCreate::SCTS
	SingleU5BU5D_t577127397* ___SCTS_4;
	// System.Single[] newWaterCreate::SCTE
	SingleU5BU5D_t577127397* ___SCTE_5;
	// UnityEngine.GameObject newWaterCreate::obj
	GameObject_t1756533147 * ___obj_6;
	// UnityEngine.GameObject newWaterCreate::ttk
	GameObject_t1756533147 * ___ttk_7;
	// UnityEngine.GameObject newWaterCreate::objc
	GameObject_t1756533147 * ___objc_8;
	// UnityEngine.GameObject newWaterCreate::obja
	GameObject_t1756533147 * ___obja_9;
	// UnityEngine.GameObject newWaterCreate::underwater
	GameObject_t1756533147 * ___underwater_10;
	// UnityEngine.GameObject newWaterCreate::cam
	GameObject_t1756533147 * ___cam_11;
	// UnityEngine.GameObject newWaterCreate::playT
	GameObject_t1756533147 * ___playT_12;
	// System.String newWaterCreate::WaterPath
	String_t* ___WaterPath_13;
	// System.Single newWaterCreate::Nts
	float ___Nts_14;
	// System.Single newWaterCreate::Ntmm
	float ___Ntmm_15;
	// System.Single newWaterCreate::Ets
	float ___Ets_16;
	// System.Single newWaterCreate::Etmm
	float ___Etmm_17;
	// System.Single newWaterCreate::per
	float ___per_18;
	// System.Int32 newWaterCreate::Ntm
	int32_t ___Ntm_19;
	// System.Int32 newWaterCreate::Etm
	int32_t ___Etm_20;
	// System.Int32 newWaterCreate::NtM
	int32_t ___NtM_21;
	// System.Int32 newWaterCreate::EtM
	int32_t ___EtM_22;
	// System.Int32 newWaterCreate::ner
	int32_t ___ner_23;
	// System.Boolean newWaterCreate::sizebool
	bool ___sizebool_24;
	// System.Single newWaterCreate::time
	float ___time_25;
	// System.Single newWaterCreate::times
	float ___times_26;
	// System.Single newWaterCreate::timee
	float ___timee_27;
	// System.Single newWaterCreate::timeStart
	float ___timeStart_28;
	// System.Single newWaterCreate::timeStartt
	float ___timeStartt_29;
	// System.Single newWaterCreate::timeEnd
	float ___timeEnd_30;
	// System.Single newWaterCreate::timesize
	float ___timesize_31;
	// System.Int32 newWaterCreate::n
	int32_t ___n_32;
	// System.Int32 newWaterCreate::nper
	int32_t ___nper_33;
	// System.Int32 newWaterCreate::r
	int32_t ___r_34;
	// System.Single newWaterCreate::number
	float ___number_35;
	// System.Int32 newWaterCreate::preRead
	int32_t ___preRead_36;
	// System.Single newWaterCreate::pertime
	float ___pertime_37;
	// UnityEngine.Shader newWaterCreate::WaterShader
	Shader_t2430389951 * ___WaterShader_38;
	// UnityEngine.Material newWaterCreate::WaterMaterial
	Material_t193706927 * ___WaterMaterial_39;
	// UnityEngine.GameObject newWaterCreate::TT
	GameObject_t1756533147 * ___TT_40;
	// UnityEngine.GameObject newWaterCreate::ST
	GameObject_t1756533147 * ___ST_41;
	// UnityEngine.GameObject newWaterCreate::ET
	GameObject_t1756533147 * ___ET_42;
	// UnityEngine.GameObject newWaterCreate::ERT
	GameObject_t1756533147 * ___ERT_43;
	// UnityEngine.Vector3 newWaterCreate::meshSize
	Vector3_t2243707580  ___meshSize_44;
	// System.Boolean newWaterCreate::readbefore
	bool ___readbefore_45;
	// System.Boolean newWaterCreate::watercam
	bool ___watercam_46;
	// System.Single newWaterCreate::meshxobjc
	float ___meshxobjc_47;
	// System.Single newWaterCreate::meshxmain
	float ___meshxmain_48;
	// System.Boolean newWaterCreate::waterreturn
	bool ___waterreturn_49;
	// System.Boolean newWaterCreate::isplay
	bool ___isplay_50;
	// System.String newWaterCreate::err
	String_t* ___err_51;
	// System.Int32 newWaterCreate::k
	int32_t ___k_52;
	// System.Int32 newWaterCreate::ln
	int32_t ___ln_53;
	// System.Int32 newWaterCreate::rn
	int32_t ___rn_54;
	// System.Int32 newWaterCreate::nk
	int32_t ___nk_55;
	// System.String newWaterCreate::readT
	String_t* ___readT_56;

public:
	inline static int32_t get_offset_of_ttt_2() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ttt_2)); }
	inline GameObjectU5BU5D_t3057952154* get_ttt_2() const { return ___ttt_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ttt_2() { return &___ttt_2; }
	inline void set_ttt_2(GameObjectU5BU5D_t3057952154* value)
	{
		___ttt_2 = value;
		Il2CppCodeGenWriteBarrier(&___ttt_2, value);
	}

	inline static int32_t get_offset_of_ttn_3() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ttn_3)); }
	inline Int32U5BU5D_t3030399641* get_ttn_3() const { return ___ttn_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_ttn_3() { return &___ttn_3; }
	inline void set_ttn_3(Int32U5BU5D_t3030399641* value)
	{
		___ttn_3 = value;
		Il2CppCodeGenWriteBarrier(&___ttn_3, value);
	}

	inline static int32_t get_offset_of_SCTS_4() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___SCTS_4)); }
	inline SingleU5BU5D_t577127397* get_SCTS_4() const { return ___SCTS_4; }
	inline SingleU5BU5D_t577127397** get_address_of_SCTS_4() { return &___SCTS_4; }
	inline void set_SCTS_4(SingleU5BU5D_t577127397* value)
	{
		___SCTS_4 = value;
		Il2CppCodeGenWriteBarrier(&___SCTS_4, value);
	}

	inline static int32_t get_offset_of_SCTE_5() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___SCTE_5)); }
	inline SingleU5BU5D_t577127397* get_SCTE_5() const { return ___SCTE_5; }
	inline SingleU5BU5D_t577127397** get_address_of_SCTE_5() { return &___SCTE_5; }
	inline void set_SCTE_5(SingleU5BU5D_t577127397* value)
	{
		___SCTE_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCTE_5, value);
	}

	inline static int32_t get_offset_of_obj_6() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___obj_6)); }
	inline GameObject_t1756533147 * get_obj_6() const { return ___obj_6; }
	inline GameObject_t1756533147 ** get_address_of_obj_6() { return &___obj_6; }
	inline void set_obj_6(GameObject_t1756533147 * value)
	{
		___obj_6 = value;
		Il2CppCodeGenWriteBarrier(&___obj_6, value);
	}

	inline static int32_t get_offset_of_ttk_7() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ttk_7)); }
	inline GameObject_t1756533147 * get_ttk_7() const { return ___ttk_7; }
	inline GameObject_t1756533147 ** get_address_of_ttk_7() { return &___ttk_7; }
	inline void set_ttk_7(GameObject_t1756533147 * value)
	{
		___ttk_7 = value;
		Il2CppCodeGenWriteBarrier(&___ttk_7, value);
	}

	inline static int32_t get_offset_of_objc_8() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___objc_8)); }
	inline GameObject_t1756533147 * get_objc_8() const { return ___objc_8; }
	inline GameObject_t1756533147 ** get_address_of_objc_8() { return &___objc_8; }
	inline void set_objc_8(GameObject_t1756533147 * value)
	{
		___objc_8 = value;
		Il2CppCodeGenWriteBarrier(&___objc_8, value);
	}

	inline static int32_t get_offset_of_obja_9() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___obja_9)); }
	inline GameObject_t1756533147 * get_obja_9() const { return ___obja_9; }
	inline GameObject_t1756533147 ** get_address_of_obja_9() { return &___obja_9; }
	inline void set_obja_9(GameObject_t1756533147 * value)
	{
		___obja_9 = value;
		Il2CppCodeGenWriteBarrier(&___obja_9, value);
	}

	inline static int32_t get_offset_of_underwater_10() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___underwater_10)); }
	inline GameObject_t1756533147 * get_underwater_10() const { return ___underwater_10; }
	inline GameObject_t1756533147 ** get_address_of_underwater_10() { return &___underwater_10; }
	inline void set_underwater_10(GameObject_t1756533147 * value)
	{
		___underwater_10 = value;
		Il2CppCodeGenWriteBarrier(&___underwater_10, value);
	}

	inline static int32_t get_offset_of_cam_11() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___cam_11)); }
	inline GameObject_t1756533147 * get_cam_11() const { return ___cam_11; }
	inline GameObject_t1756533147 ** get_address_of_cam_11() { return &___cam_11; }
	inline void set_cam_11(GameObject_t1756533147 * value)
	{
		___cam_11 = value;
		Il2CppCodeGenWriteBarrier(&___cam_11, value);
	}

	inline static int32_t get_offset_of_playT_12() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___playT_12)); }
	inline GameObject_t1756533147 * get_playT_12() const { return ___playT_12; }
	inline GameObject_t1756533147 ** get_address_of_playT_12() { return &___playT_12; }
	inline void set_playT_12(GameObject_t1756533147 * value)
	{
		___playT_12 = value;
		Il2CppCodeGenWriteBarrier(&___playT_12, value);
	}

	inline static int32_t get_offset_of_WaterPath_13() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___WaterPath_13)); }
	inline String_t* get_WaterPath_13() const { return ___WaterPath_13; }
	inline String_t** get_address_of_WaterPath_13() { return &___WaterPath_13; }
	inline void set_WaterPath_13(String_t* value)
	{
		___WaterPath_13 = value;
		Il2CppCodeGenWriteBarrier(&___WaterPath_13, value);
	}

	inline static int32_t get_offset_of_Nts_14() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___Nts_14)); }
	inline float get_Nts_14() const { return ___Nts_14; }
	inline float* get_address_of_Nts_14() { return &___Nts_14; }
	inline void set_Nts_14(float value)
	{
		___Nts_14 = value;
	}

	inline static int32_t get_offset_of_Ntmm_15() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___Ntmm_15)); }
	inline float get_Ntmm_15() const { return ___Ntmm_15; }
	inline float* get_address_of_Ntmm_15() { return &___Ntmm_15; }
	inline void set_Ntmm_15(float value)
	{
		___Ntmm_15 = value;
	}

	inline static int32_t get_offset_of_Ets_16() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___Ets_16)); }
	inline float get_Ets_16() const { return ___Ets_16; }
	inline float* get_address_of_Ets_16() { return &___Ets_16; }
	inline void set_Ets_16(float value)
	{
		___Ets_16 = value;
	}

	inline static int32_t get_offset_of_Etmm_17() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___Etmm_17)); }
	inline float get_Etmm_17() const { return ___Etmm_17; }
	inline float* get_address_of_Etmm_17() { return &___Etmm_17; }
	inline void set_Etmm_17(float value)
	{
		___Etmm_17 = value;
	}

	inline static int32_t get_offset_of_per_18() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___per_18)); }
	inline float get_per_18() const { return ___per_18; }
	inline float* get_address_of_per_18() { return &___per_18; }
	inline void set_per_18(float value)
	{
		___per_18 = value;
	}

	inline static int32_t get_offset_of_Ntm_19() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___Ntm_19)); }
	inline int32_t get_Ntm_19() const { return ___Ntm_19; }
	inline int32_t* get_address_of_Ntm_19() { return &___Ntm_19; }
	inline void set_Ntm_19(int32_t value)
	{
		___Ntm_19 = value;
	}

	inline static int32_t get_offset_of_Etm_20() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___Etm_20)); }
	inline int32_t get_Etm_20() const { return ___Etm_20; }
	inline int32_t* get_address_of_Etm_20() { return &___Etm_20; }
	inline void set_Etm_20(int32_t value)
	{
		___Etm_20 = value;
	}

	inline static int32_t get_offset_of_NtM_21() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___NtM_21)); }
	inline int32_t get_NtM_21() const { return ___NtM_21; }
	inline int32_t* get_address_of_NtM_21() { return &___NtM_21; }
	inline void set_NtM_21(int32_t value)
	{
		___NtM_21 = value;
	}

	inline static int32_t get_offset_of_EtM_22() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___EtM_22)); }
	inline int32_t get_EtM_22() const { return ___EtM_22; }
	inline int32_t* get_address_of_EtM_22() { return &___EtM_22; }
	inline void set_EtM_22(int32_t value)
	{
		___EtM_22 = value;
	}

	inline static int32_t get_offset_of_ner_23() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ner_23)); }
	inline int32_t get_ner_23() const { return ___ner_23; }
	inline int32_t* get_address_of_ner_23() { return &___ner_23; }
	inline void set_ner_23(int32_t value)
	{
		___ner_23 = value;
	}

	inline static int32_t get_offset_of_sizebool_24() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___sizebool_24)); }
	inline bool get_sizebool_24() const { return ___sizebool_24; }
	inline bool* get_address_of_sizebool_24() { return &___sizebool_24; }
	inline void set_sizebool_24(bool value)
	{
		___sizebool_24 = value;
	}

	inline static int32_t get_offset_of_time_25() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___time_25)); }
	inline float get_time_25() const { return ___time_25; }
	inline float* get_address_of_time_25() { return &___time_25; }
	inline void set_time_25(float value)
	{
		___time_25 = value;
	}

	inline static int32_t get_offset_of_times_26() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___times_26)); }
	inline float get_times_26() const { return ___times_26; }
	inline float* get_address_of_times_26() { return &___times_26; }
	inline void set_times_26(float value)
	{
		___times_26 = value;
	}

	inline static int32_t get_offset_of_timee_27() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___timee_27)); }
	inline float get_timee_27() const { return ___timee_27; }
	inline float* get_address_of_timee_27() { return &___timee_27; }
	inline void set_timee_27(float value)
	{
		___timee_27 = value;
	}

	inline static int32_t get_offset_of_timeStart_28() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___timeStart_28)); }
	inline float get_timeStart_28() const { return ___timeStart_28; }
	inline float* get_address_of_timeStart_28() { return &___timeStart_28; }
	inline void set_timeStart_28(float value)
	{
		___timeStart_28 = value;
	}

	inline static int32_t get_offset_of_timeStartt_29() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___timeStartt_29)); }
	inline float get_timeStartt_29() const { return ___timeStartt_29; }
	inline float* get_address_of_timeStartt_29() { return &___timeStartt_29; }
	inline void set_timeStartt_29(float value)
	{
		___timeStartt_29 = value;
	}

	inline static int32_t get_offset_of_timeEnd_30() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___timeEnd_30)); }
	inline float get_timeEnd_30() const { return ___timeEnd_30; }
	inline float* get_address_of_timeEnd_30() { return &___timeEnd_30; }
	inline void set_timeEnd_30(float value)
	{
		___timeEnd_30 = value;
	}

	inline static int32_t get_offset_of_timesize_31() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___timesize_31)); }
	inline float get_timesize_31() const { return ___timesize_31; }
	inline float* get_address_of_timesize_31() { return &___timesize_31; }
	inline void set_timesize_31(float value)
	{
		___timesize_31 = value;
	}

	inline static int32_t get_offset_of_n_32() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___n_32)); }
	inline int32_t get_n_32() const { return ___n_32; }
	inline int32_t* get_address_of_n_32() { return &___n_32; }
	inline void set_n_32(int32_t value)
	{
		___n_32 = value;
	}

	inline static int32_t get_offset_of_nper_33() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___nper_33)); }
	inline int32_t get_nper_33() const { return ___nper_33; }
	inline int32_t* get_address_of_nper_33() { return &___nper_33; }
	inline void set_nper_33(int32_t value)
	{
		___nper_33 = value;
	}

	inline static int32_t get_offset_of_r_34() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___r_34)); }
	inline int32_t get_r_34() const { return ___r_34; }
	inline int32_t* get_address_of_r_34() { return &___r_34; }
	inline void set_r_34(int32_t value)
	{
		___r_34 = value;
	}

	inline static int32_t get_offset_of_number_35() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___number_35)); }
	inline float get_number_35() const { return ___number_35; }
	inline float* get_address_of_number_35() { return &___number_35; }
	inline void set_number_35(float value)
	{
		___number_35 = value;
	}

	inline static int32_t get_offset_of_preRead_36() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___preRead_36)); }
	inline int32_t get_preRead_36() const { return ___preRead_36; }
	inline int32_t* get_address_of_preRead_36() { return &___preRead_36; }
	inline void set_preRead_36(int32_t value)
	{
		___preRead_36 = value;
	}

	inline static int32_t get_offset_of_pertime_37() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___pertime_37)); }
	inline float get_pertime_37() const { return ___pertime_37; }
	inline float* get_address_of_pertime_37() { return &___pertime_37; }
	inline void set_pertime_37(float value)
	{
		___pertime_37 = value;
	}

	inline static int32_t get_offset_of_WaterShader_38() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___WaterShader_38)); }
	inline Shader_t2430389951 * get_WaterShader_38() const { return ___WaterShader_38; }
	inline Shader_t2430389951 ** get_address_of_WaterShader_38() { return &___WaterShader_38; }
	inline void set_WaterShader_38(Shader_t2430389951 * value)
	{
		___WaterShader_38 = value;
		Il2CppCodeGenWriteBarrier(&___WaterShader_38, value);
	}

	inline static int32_t get_offset_of_WaterMaterial_39() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___WaterMaterial_39)); }
	inline Material_t193706927 * get_WaterMaterial_39() const { return ___WaterMaterial_39; }
	inline Material_t193706927 ** get_address_of_WaterMaterial_39() { return &___WaterMaterial_39; }
	inline void set_WaterMaterial_39(Material_t193706927 * value)
	{
		___WaterMaterial_39 = value;
		Il2CppCodeGenWriteBarrier(&___WaterMaterial_39, value);
	}

	inline static int32_t get_offset_of_TT_40() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___TT_40)); }
	inline GameObject_t1756533147 * get_TT_40() const { return ___TT_40; }
	inline GameObject_t1756533147 ** get_address_of_TT_40() { return &___TT_40; }
	inline void set_TT_40(GameObject_t1756533147 * value)
	{
		___TT_40 = value;
		Il2CppCodeGenWriteBarrier(&___TT_40, value);
	}

	inline static int32_t get_offset_of_ST_41() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ST_41)); }
	inline GameObject_t1756533147 * get_ST_41() const { return ___ST_41; }
	inline GameObject_t1756533147 ** get_address_of_ST_41() { return &___ST_41; }
	inline void set_ST_41(GameObject_t1756533147 * value)
	{
		___ST_41 = value;
		Il2CppCodeGenWriteBarrier(&___ST_41, value);
	}

	inline static int32_t get_offset_of_ET_42() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ET_42)); }
	inline GameObject_t1756533147 * get_ET_42() const { return ___ET_42; }
	inline GameObject_t1756533147 ** get_address_of_ET_42() { return &___ET_42; }
	inline void set_ET_42(GameObject_t1756533147 * value)
	{
		___ET_42 = value;
		Il2CppCodeGenWriteBarrier(&___ET_42, value);
	}

	inline static int32_t get_offset_of_ERT_43() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ERT_43)); }
	inline GameObject_t1756533147 * get_ERT_43() const { return ___ERT_43; }
	inline GameObject_t1756533147 ** get_address_of_ERT_43() { return &___ERT_43; }
	inline void set_ERT_43(GameObject_t1756533147 * value)
	{
		___ERT_43 = value;
		Il2CppCodeGenWriteBarrier(&___ERT_43, value);
	}

	inline static int32_t get_offset_of_meshSize_44() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___meshSize_44)); }
	inline Vector3_t2243707580  get_meshSize_44() const { return ___meshSize_44; }
	inline Vector3_t2243707580 * get_address_of_meshSize_44() { return &___meshSize_44; }
	inline void set_meshSize_44(Vector3_t2243707580  value)
	{
		___meshSize_44 = value;
	}

	inline static int32_t get_offset_of_readbefore_45() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___readbefore_45)); }
	inline bool get_readbefore_45() const { return ___readbefore_45; }
	inline bool* get_address_of_readbefore_45() { return &___readbefore_45; }
	inline void set_readbefore_45(bool value)
	{
		___readbefore_45 = value;
	}

	inline static int32_t get_offset_of_watercam_46() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___watercam_46)); }
	inline bool get_watercam_46() const { return ___watercam_46; }
	inline bool* get_address_of_watercam_46() { return &___watercam_46; }
	inline void set_watercam_46(bool value)
	{
		___watercam_46 = value;
	}

	inline static int32_t get_offset_of_meshxobjc_47() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___meshxobjc_47)); }
	inline float get_meshxobjc_47() const { return ___meshxobjc_47; }
	inline float* get_address_of_meshxobjc_47() { return &___meshxobjc_47; }
	inline void set_meshxobjc_47(float value)
	{
		___meshxobjc_47 = value;
	}

	inline static int32_t get_offset_of_meshxmain_48() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___meshxmain_48)); }
	inline float get_meshxmain_48() const { return ___meshxmain_48; }
	inline float* get_address_of_meshxmain_48() { return &___meshxmain_48; }
	inline void set_meshxmain_48(float value)
	{
		___meshxmain_48 = value;
	}

	inline static int32_t get_offset_of_waterreturn_49() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___waterreturn_49)); }
	inline bool get_waterreturn_49() const { return ___waterreturn_49; }
	inline bool* get_address_of_waterreturn_49() { return &___waterreturn_49; }
	inline void set_waterreturn_49(bool value)
	{
		___waterreturn_49 = value;
	}

	inline static int32_t get_offset_of_isplay_50() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___isplay_50)); }
	inline bool get_isplay_50() const { return ___isplay_50; }
	inline bool* get_address_of_isplay_50() { return &___isplay_50; }
	inline void set_isplay_50(bool value)
	{
		___isplay_50 = value;
	}

	inline static int32_t get_offset_of_err_51() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___err_51)); }
	inline String_t* get_err_51() const { return ___err_51; }
	inline String_t** get_address_of_err_51() { return &___err_51; }
	inline void set_err_51(String_t* value)
	{
		___err_51 = value;
		Il2CppCodeGenWriteBarrier(&___err_51, value);
	}

	inline static int32_t get_offset_of_k_52() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___k_52)); }
	inline int32_t get_k_52() const { return ___k_52; }
	inline int32_t* get_address_of_k_52() { return &___k_52; }
	inline void set_k_52(int32_t value)
	{
		___k_52 = value;
	}

	inline static int32_t get_offset_of_ln_53() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___ln_53)); }
	inline int32_t get_ln_53() const { return ___ln_53; }
	inline int32_t* get_address_of_ln_53() { return &___ln_53; }
	inline void set_ln_53(int32_t value)
	{
		___ln_53 = value;
	}

	inline static int32_t get_offset_of_rn_54() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___rn_54)); }
	inline int32_t get_rn_54() const { return ___rn_54; }
	inline int32_t* get_address_of_rn_54() { return &___rn_54; }
	inline void set_rn_54(int32_t value)
	{
		___rn_54 = value;
	}

	inline static int32_t get_offset_of_nk_55() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___nk_55)); }
	inline int32_t get_nk_55() const { return ___nk_55; }
	inline int32_t* get_address_of_nk_55() { return &___nk_55; }
	inline void set_nk_55(int32_t value)
	{
		___nk_55 = value;
	}

	inline static int32_t get_offset_of_readT_56() { return static_cast<int32_t>(offsetof(newWaterCreate_t2230692855, ___readT_56)); }
	inline String_t* get_readT_56() const { return ___readT_56; }
	inline String_t** get_address_of_readT_56() { return &___readT_56; }
	inline void set_readT_56(String_t* value)
	{
		___readT_56 = value;
		Il2CppCodeGenWriteBarrier(&___readT_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
