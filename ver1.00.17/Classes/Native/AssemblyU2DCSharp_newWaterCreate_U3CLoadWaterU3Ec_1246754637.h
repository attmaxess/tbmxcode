﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.ResourceRequest
struct ResourceRequest_t2560315377;
// newWaterCreate
struct newWaterCreate_t2230692855;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// newWaterCreate/<LoadWater>c__Iterator1
struct  U3CLoadWaterU3Ec__Iterator1_t1246754637  : public Il2CppObject
{
public:
	// System.String newWaterCreate/<LoadWater>c__Iterator1::<filename>__0
	String_t* ___U3CfilenameU3E__0_0;
	// System.Int32 newWaterCreate/<LoadWater>c__Iterator1::n
	int32_t ___n_1;
	// UnityEngine.ResourceRequest newWaterCreate/<LoadWater>c__Iterator1::<request>__0
	ResourceRequest_t2560315377 * ___U3CrequestU3E__0_2;
	// newWaterCreate newWaterCreate/<LoadWater>c__Iterator1::$this
	newWaterCreate_t2230692855 * ___U24this_3;
	// System.Object newWaterCreate/<LoadWater>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean newWaterCreate/<LoadWater>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 newWaterCreate/<LoadWater>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CfilenameU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadWaterU3Ec__Iterator1_t1246754637, ___U3CfilenameU3E__0_0)); }
	inline String_t* get_U3CfilenameU3E__0_0() const { return ___U3CfilenameU3E__0_0; }
	inline String_t** get_address_of_U3CfilenameU3E__0_0() { return &___U3CfilenameU3E__0_0; }
	inline void set_U3CfilenameU3E__0_0(String_t* value)
	{
		___U3CfilenameU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfilenameU3E__0_0, value);
	}

	inline static int32_t get_offset_of_n_1() { return static_cast<int32_t>(offsetof(U3CLoadWaterU3Ec__Iterator1_t1246754637, ___n_1)); }
	inline int32_t get_n_1() const { return ___n_1; }
	inline int32_t* get_address_of_n_1() { return &___n_1; }
	inline void set_n_1(int32_t value)
	{
		___n_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadWaterU3Ec__Iterator1_t1246754637, ___U3CrequestU3E__0_2)); }
	inline ResourceRequest_t2560315377 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline ResourceRequest_t2560315377 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(ResourceRequest_t2560315377 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadWaterU3Ec__Iterator1_t1246754637, ___U24this_3)); }
	inline newWaterCreate_t2230692855 * get_U24this_3() const { return ___U24this_3; }
	inline newWaterCreate_t2230692855 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(newWaterCreate_t2230692855 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadWaterU3Ec__Iterator1_t1246754637, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadWaterU3Ec__Iterator1_t1246754637, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadWaterU3Ec__Iterator1_t1246754637, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
