﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Renderer
struct Renderer_t257310565;
// touchBendingPlayerListener
struct touchBendingPlayerListener_t2574995411;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// touchBendingCollisionGS
struct  touchBendingCollisionGS_t3063605690  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material touchBendingCollisionGS::simpleBendingMaterial
	Material_t193706927 * ___simpleBendingMaterial_2;
	// UnityEngine.Material touchBendingCollisionGS::touchBendingMaterial
	Material_t193706927 * ___touchBendingMaterial_3;
	// System.Single touchBendingCollisionGS::stiffness
	float ___stiffness_4;
	// System.Single touchBendingCollisionGS::disturbance
	float ___disturbance_5;
	// System.Single touchBendingCollisionGS::duration
	float ___duration_6;
	// UnityEngine.Transform touchBendingCollisionGS::myTransform
	Transform_t3275118058 * ___myTransform_7;
	// UnityEngine.Renderer touchBendingCollisionGS::myRenderer
	Renderer_t257310565 * ___myRenderer_8;
	// UnityEngine.Matrix4x4 touchBendingCollisionGS::myMatrix
	Matrix4x4_t2933234003  ___myMatrix_9;
	// UnityEngine.Vector3 touchBendingCollisionGS::axis
	Vector3_t2243707580  ___axis_10;
	// UnityEngine.Vector3 touchBendingCollisionGS::axis1
	Vector3_t2243707580  ___axis1_11;
	// System.Boolean touchBendingCollisionGS::touched
	bool ___touched_12;
	// System.Boolean touchBendingCollisionGS::doubletouched
	bool ___doubletouched_13;
	// System.Boolean touchBendingCollisionGS::left
	bool ___left_14;
	// System.Boolean touchBendingCollisionGS::finished
	bool ___finished_15;
	// System.Boolean touchBendingCollisionGS::left1
	bool ___left1_16;
	// System.Boolean touchBendingCollisionGS::finished1
	bool ___finished1_17;
	// System.Single touchBendingCollisionGS::intialTouchForce
	float ___intialTouchForce_18;
	// System.Single touchBendingCollisionGS::touchBending
	float ___touchBending_19;
	// System.Single touchBendingCollisionGS::targetTouchBending
	float ___targetTouchBending_20;
	// System.Single touchBendingCollisionGS::easingControl
	float ___easingControl_21;
	// System.Single touchBendingCollisionGS::intialTouchForce1
	float ___intialTouchForce1_22;
	// System.Single touchBendingCollisionGS::touchBending1
	float ___touchBending1_23;
	// System.Single touchBendingCollisionGS::targetTouchBending1
	float ___targetTouchBending1_24;
	// System.Single touchBendingCollisionGS::easingControl1
	float ___easingControl1_25;
	// System.Int32 touchBendingCollisionGS::Player_ID
	int32_t ___Player_ID_26;
	// touchBendingPlayerListener touchBendingCollisionGS::PlayerVars
	touchBendingPlayerListener_t2574995411 * ___PlayerVars_27;
	// UnityEngine.Vector3 touchBendingCollisionGS::Player_Direction
	Vector3_t2243707580  ___Player_Direction_28;
	// System.Single touchBendingCollisionGS::Player_Speed
	float ___Player_Speed_29;
	// System.Int32 touchBendingCollisionGS::Player1_ID
	int32_t ___Player1_ID_30;
	// touchBendingPlayerListener touchBendingCollisionGS::PlayerVars1
	touchBendingPlayerListener_t2574995411 * ___PlayerVars1_31;
	// UnityEngine.Vector3 touchBendingCollisionGS::Player_Direction1
	Vector3_t2243707580  ___Player_Direction1_32;
	// System.Single touchBendingCollisionGS::Player_Speed1
	float ___Player_Speed1_33;
	// System.Single touchBendingCollisionGS::timer
	float ___timer_34;
	// System.Single touchBendingCollisionGS::timer1
	float ___timer1_35;
	// System.Single touchBendingCollisionGS::lerptime
	float ___lerptime_36;

public:
	inline static int32_t get_offset_of_simpleBendingMaterial_2() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___simpleBendingMaterial_2)); }
	inline Material_t193706927 * get_simpleBendingMaterial_2() const { return ___simpleBendingMaterial_2; }
	inline Material_t193706927 ** get_address_of_simpleBendingMaterial_2() { return &___simpleBendingMaterial_2; }
	inline void set_simpleBendingMaterial_2(Material_t193706927 * value)
	{
		___simpleBendingMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___simpleBendingMaterial_2, value);
	}

	inline static int32_t get_offset_of_touchBendingMaterial_3() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___touchBendingMaterial_3)); }
	inline Material_t193706927 * get_touchBendingMaterial_3() const { return ___touchBendingMaterial_3; }
	inline Material_t193706927 ** get_address_of_touchBendingMaterial_3() { return &___touchBendingMaterial_3; }
	inline void set_touchBendingMaterial_3(Material_t193706927 * value)
	{
		___touchBendingMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___touchBendingMaterial_3, value);
	}

	inline static int32_t get_offset_of_stiffness_4() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___stiffness_4)); }
	inline float get_stiffness_4() const { return ___stiffness_4; }
	inline float* get_address_of_stiffness_4() { return &___stiffness_4; }
	inline void set_stiffness_4(float value)
	{
		___stiffness_4 = value;
	}

	inline static int32_t get_offset_of_disturbance_5() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___disturbance_5)); }
	inline float get_disturbance_5() const { return ___disturbance_5; }
	inline float* get_address_of_disturbance_5() { return &___disturbance_5; }
	inline void set_disturbance_5(float value)
	{
		___disturbance_5 = value;
	}

	inline static int32_t get_offset_of_duration_6() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___duration_6)); }
	inline float get_duration_6() const { return ___duration_6; }
	inline float* get_address_of_duration_6() { return &___duration_6; }
	inline void set_duration_6(float value)
	{
		___duration_6 = value;
	}

	inline static int32_t get_offset_of_myTransform_7() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___myTransform_7)); }
	inline Transform_t3275118058 * get_myTransform_7() const { return ___myTransform_7; }
	inline Transform_t3275118058 ** get_address_of_myTransform_7() { return &___myTransform_7; }
	inline void set_myTransform_7(Transform_t3275118058 * value)
	{
		___myTransform_7 = value;
		Il2CppCodeGenWriteBarrier(&___myTransform_7, value);
	}

	inline static int32_t get_offset_of_myRenderer_8() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___myRenderer_8)); }
	inline Renderer_t257310565 * get_myRenderer_8() const { return ___myRenderer_8; }
	inline Renderer_t257310565 ** get_address_of_myRenderer_8() { return &___myRenderer_8; }
	inline void set_myRenderer_8(Renderer_t257310565 * value)
	{
		___myRenderer_8 = value;
		Il2CppCodeGenWriteBarrier(&___myRenderer_8, value);
	}

	inline static int32_t get_offset_of_myMatrix_9() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___myMatrix_9)); }
	inline Matrix4x4_t2933234003  get_myMatrix_9() const { return ___myMatrix_9; }
	inline Matrix4x4_t2933234003 * get_address_of_myMatrix_9() { return &___myMatrix_9; }
	inline void set_myMatrix_9(Matrix4x4_t2933234003  value)
	{
		___myMatrix_9 = value;
	}

	inline static int32_t get_offset_of_axis_10() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___axis_10)); }
	inline Vector3_t2243707580  get_axis_10() const { return ___axis_10; }
	inline Vector3_t2243707580 * get_address_of_axis_10() { return &___axis_10; }
	inline void set_axis_10(Vector3_t2243707580  value)
	{
		___axis_10 = value;
	}

	inline static int32_t get_offset_of_axis1_11() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___axis1_11)); }
	inline Vector3_t2243707580  get_axis1_11() const { return ___axis1_11; }
	inline Vector3_t2243707580 * get_address_of_axis1_11() { return &___axis1_11; }
	inline void set_axis1_11(Vector3_t2243707580  value)
	{
		___axis1_11 = value;
	}

	inline static int32_t get_offset_of_touched_12() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___touched_12)); }
	inline bool get_touched_12() const { return ___touched_12; }
	inline bool* get_address_of_touched_12() { return &___touched_12; }
	inline void set_touched_12(bool value)
	{
		___touched_12 = value;
	}

	inline static int32_t get_offset_of_doubletouched_13() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___doubletouched_13)); }
	inline bool get_doubletouched_13() const { return ___doubletouched_13; }
	inline bool* get_address_of_doubletouched_13() { return &___doubletouched_13; }
	inline void set_doubletouched_13(bool value)
	{
		___doubletouched_13 = value;
	}

	inline static int32_t get_offset_of_left_14() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___left_14)); }
	inline bool get_left_14() const { return ___left_14; }
	inline bool* get_address_of_left_14() { return &___left_14; }
	inline void set_left_14(bool value)
	{
		___left_14 = value;
	}

	inline static int32_t get_offset_of_finished_15() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___finished_15)); }
	inline bool get_finished_15() const { return ___finished_15; }
	inline bool* get_address_of_finished_15() { return &___finished_15; }
	inline void set_finished_15(bool value)
	{
		___finished_15 = value;
	}

	inline static int32_t get_offset_of_left1_16() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___left1_16)); }
	inline bool get_left1_16() const { return ___left1_16; }
	inline bool* get_address_of_left1_16() { return &___left1_16; }
	inline void set_left1_16(bool value)
	{
		___left1_16 = value;
	}

	inline static int32_t get_offset_of_finished1_17() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___finished1_17)); }
	inline bool get_finished1_17() const { return ___finished1_17; }
	inline bool* get_address_of_finished1_17() { return &___finished1_17; }
	inline void set_finished1_17(bool value)
	{
		___finished1_17 = value;
	}

	inline static int32_t get_offset_of_intialTouchForce_18() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___intialTouchForce_18)); }
	inline float get_intialTouchForce_18() const { return ___intialTouchForce_18; }
	inline float* get_address_of_intialTouchForce_18() { return &___intialTouchForce_18; }
	inline void set_intialTouchForce_18(float value)
	{
		___intialTouchForce_18 = value;
	}

	inline static int32_t get_offset_of_touchBending_19() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___touchBending_19)); }
	inline float get_touchBending_19() const { return ___touchBending_19; }
	inline float* get_address_of_touchBending_19() { return &___touchBending_19; }
	inline void set_touchBending_19(float value)
	{
		___touchBending_19 = value;
	}

	inline static int32_t get_offset_of_targetTouchBending_20() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___targetTouchBending_20)); }
	inline float get_targetTouchBending_20() const { return ___targetTouchBending_20; }
	inline float* get_address_of_targetTouchBending_20() { return &___targetTouchBending_20; }
	inline void set_targetTouchBending_20(float value)
	{
		___targetTouchBending_20 = value;
	}

	inline static int32_t get_offset_of_easingControl_21() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___easingControl_21)); }
	inline float get_easingControl_21() const { return ___easingControl_21; }
	inline float* get_address_of_easingControl_21() { return &___easingControl_21; }
	inline void set_easingControl_21(float value)
	{
		___easingControl_21 = value;
	}

	inline static int32_t get_offset_of_intialTouchForce1_22() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___intialTouchForce1_22)); }
	inline float get_intialTouchForce1_22() const { return ___intialTouchForce1_22; }
	inline float* get_address_of_intialTouchForce1_22() { return &___intialTouchForce1_22; }
	inline void set_intialTouchForce1_22(float value)
	{
		___intialTouchForce1_22 = value;
	}

	inline static int32_t get_offset_of_touchBending1_23() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___touchBending1_23)); }
	inline float get_touchBending1_23() const { return ___touchBending1_23; }
	inline float* get_address_of_touchBending1_23() { return &___touchBending1_23; }
	inline void set_touchBending1_23(float value)
	{
		___touchBending1_23 = value;
	}

	inline static int32_t get_offset_of_targetTouchBending1_24() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___targetTouchBending1_24)); }
	inline float get_targetTouchBending1_24() const { return ___targetTouchBending1_24; }
	inline float* get_address_of_targetTouchBending1_24() { return &___targetTouchBending1_24; }
	inline void set_targetTouchBending1_24(float value)
	{
		___targetTouchBending1_24 = value;
	}

	inline static int32_t get_offset_of_easingControl1_25() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___easingControl1_25)); }
	inline float get_easingControl1_25() const { return ___easingControl1_25; }
	inline float* get_address_of_easingControl1_25() { return &___easingControl1_25; }
	inline void set_easingControl1_25(float value)
	{
		___easingControl1_25 = value;
	}

	inline static int32_t get_offset_of_Player_ID_26() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___Player_ID_26)); }
	inline int32_t get_Player_ID_26() const { return ___Player_ID_26; }
	inline int32_t* get_address_of_Player_ID_26() { return &___Player_ID_26; }
	inline void set_Player_ID_26(int32_t value)
	{
		___Player_ID_26 = value;
	}

	inline static int32_t get_offset_of_PlayerVars_27() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___PlayerVars_27)); }
	inline touchBendingPlayerListener_t2574995411 * get_PlayerVars_27() const { return ___PlayerVars_27; }
	inline touchBendingPlayerListener_t2574995411 ** get_address_of_PlayerVars_27() { return &___PlayerVars_27; }
	inline void set_PlayerVars_27(touchBendingPlayerListener_t2574995411 * value)
	{
		___PlayerVars_27 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerVars_27, value);
	}

	inline static int32_t get_offset_of_Player_Direction_28() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___Player_Direction_28)); }
	inline Vector3_t2243707580  get_Player_Direction_28() const { return ___Player_Direction_28; }
	inline Vector3_t2243707580 * get_address_of_Player_Direction_28() { return &___Player_Direction_28; }
	inline void set_Player_Direction_28(Vector3_t2243707580  value)
	{
		___Player_Direction_28 = value;
	}

	inline static int32_t get_offset_of_Player_Speed_29() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___Player_Speed_29)); }
	inline float get_Player_Speed_29() const { return ___Player_Speed_29; }
	inline float* get_address_of_Player_Speed_29() { return &___Player_Speed_29; }
	inline void set_Player_Speed_29(float value)
	{
		___Player_Speed_29 = value;
	}

	inline static int32_t get_offset_of_Player1_ID_30() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___Player1_ID_30)); }
	inline int32_t get_Player1_ID_30() const { return ___Player1_ID_30; }
	inline int32_t* get_address_of_Player1_ID_30() { return &___Player1_ID_30; }
	inline void set_Player1_ID_30(int32_t value)
	{
		___Player1_ID_30 = value;
	}

	inline static int32_t get_offset_of_PlayerVars1_31() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___PlayerVars1_31)); }
	inline touchBendingPlayerListener_t2574995411 * get_PlayerVars1_31() const { return ___PlayerVars1_31; }
	inline touchBendingPlayerListener_t2574995411 ** get_address_of_PlayerVars1_31() { return &___PlayerVars1_31; }
	inline void set_PlayerVars1_31(touchBendingPlayerListener_t2574995411 * value)
	{
		___PlayerVars1_31 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerVars1_31, value);
	}

	inline static int32_t get_offset_of_Player_Direction1_32() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___Player_Direction1_32)); }
	inline Vector3_t2243707580  get_Player_Direction1_32() const { return ___Player_Direction1_32; }
	inline Vector3_t2243707580 * get_address_of_Player_Direction1_32() { return &___Player_Direction1_32; }
	inline void set_Player_Direction1_32(Vector3_t2243707580  value)
	{
		___Player_Direction1_32 = value;
	}

	inline static int32_t get_offset_of_Player_Speed1_33() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___Player_Speed1_33)); }
	inline float get_Player_Speed1_33() const { return ___Player_Speed1_33; }
	inline float* get_address_of_Player_Speed1_33() { return &___Player_Speed1_33; }
	inline void set_Player_Speed1_33(float value)
	{
		___Player_Speed1_33 = value;
	}

	inline static int32_t get_offset_of_timer_34() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___timer_34)); }
	inline float get_timer_34() const { return ___timer_34; }
	inline float* get_address_of_timer_34() { return &___timer_34; }
	inline void set_timer_34(float value)
	{
		___timer_34 = value;
	}

	inline static int32_t get_offset_of_timer1_35() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___timer1_35)); }
	inline float get_timer1_35() const { return ___timer1_35; }
	inline float* get_address_of_timer1_35() { return &___timer1_35; }
	inline void set_timer1_35(float value)
	{
		___timer1_35 = value;
	}

	inline static int32_t get_offset_of_lerptime_36() { return static_cast<int32_t>(offsetof(touchBendingCollisionGS_t3063605690, ___lerptime_36)); }
	inline float get_lerptime_36() const { return ___lerptime_36; }
	inline float* get_address_of_lerptime_36() { return &___lerptime_36; }
	inline void set_lerptime_36(float value)
	{
		___lerptime_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
