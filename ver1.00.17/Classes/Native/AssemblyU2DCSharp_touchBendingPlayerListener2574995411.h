﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// touchBendingPlayerListener
struct  touchBendingPlayerListener_t2574995411  : public MonoBehaviour_t1158329972
{
public:
	// System.Single touchBendingPlayerListener::maxSpeed
	float ___maxSpeed_2;
	// System.Single touchBendingPlayerListener::Player_DampSpeed
	float ___Player_DampSpeed_3;
	// UnityEngine.Transform touchBendingPlayerListener::myTransform
	Transform_t3275118058 * ___myTransform_4;
	// UnityEngine.Vector3 touchBendingPlayerListener::Player_Position
	Vector3_t2243707580  ___Player_Position_5;
	// UnityEngine.Vector3 touchBendingPlayerListener::Player_OldPosition
	Vector3_t2243707580  ___Player_OldPosition_6;
	// System.Single touchBendingPlayerListener::Player_Speed
	float ___Player_Speed_7;
	// System.Single touchBendingPlayerListener::Player_NewSpeed
	float ___Player_NewSpeed_8;
	// UnityEngine.Vector3 touchBendingPlayerListener::Player_Direction
	Vector3_t2243707580  ___Player_Direction_9;
	// System.Boolean touchBendingPlayerListener::Update_PlayerVars
	bool ___Update_PlayerVars_10;

public:
	inline static int32_t get_offset_of_maxSpeed_2() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___maxSpeed_2)); }
	inline float get_maxSpeed_2() const { return ___maxSpeed_2; }
	inline float* get_address_of_maxSpeed_2() { return &___maxSpeed_2; }
	inline void set_maxSpeed_2(float value)
	{
		___maxSpeed_2 = value;
	}

	inline static int32_t get_offset_of_Player_DampSpeed_3() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___Player_DampSpeed_3)); }
	inline float get_Player_DampSpeed_3() const { return ___Player_DampSpeed_3; }
	inline float* get_address_of_Player_DampSpeed_3() { return &___Player_DampSpeed_3; }
	inline void set_Player_DampSpeed_3(float value)
	{
		___Player_DampSpeed_3 = value;
	}

	inline static int32_t get_offset_of_myTransform_4() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___myTransform_4)); }
	inline Transform_t3275118058 * get_myTransform_4() const { return ___myTransform_4; }
	inline Transform_t3275118058 ** get_address_of_myTransform_4() { return &___myTransform_4; }
	inline void set_myTransform_4(Transform_t3275118058 * value)
	{
		___myTransform_4 = value;
		Il2CppCodeGenWriteBarrier(&___myTransform_4, value);
	}

	inline static int32_t get_offset_of_Player_Position_5() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___Player_Position_5)); }
	inline Vector3_t2243707580  get_Player_Position_5() const { return ___Player_Position_5; }
	inline Vector3_t2243707580 * get_address_of_Player_Position_5() { return &___Player_Position_5; }
	inline void set_Player_Position_5(Vector3_t2243707580  value)
	{
		___Player_Position_5 = value;
	}

	inline static int32_t get_offset_of_Player_OldPosition_6() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___Player_OldPosition_6)); }
	inline Vector3_t2243707580  get_Player_OldPosition_6() const { return ___Player_OldPosition_6; }
	inline Vector3_t2243707580 * get_address_of_Player_OldPosition_6() { return &___Player_OldPosition_6; }
	inline void set_Player_OldPosition_6(Vector3_t2243707580  value)
	{
		___Player_OldPosition_6 = value;
	}

	inline static int32_t get_offset_of_Player_Speed_7() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___Player_Speed_7)); }
	inline float get_Player_Speed_7() const { return ___Player_Speed_7; }
	inline float* get_address_of_Player_Speed_7() { return &___Player_Speed_7; }
	inline void set_Player_Speed_7(float value)
	{
		___Player_Speed_7 = value;
	}

	inline static int32_t get_offset_of_Player_NewSpeed_8() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___Player_NewSpeed_8)); }
	inline float get_Player_NewSpeed_8() const { return ___Player_NewSpeed_8; }
	inline float* get_address_of_Player_NewSpeed_8() { return &___Player_NewSpeed_8; }
	inline void set_Player_NewSpeed_8(float value)
	{
		___Player_NewSpeed_8 = value;
	}

	inline static int32_t get_offset_of_Player_Direction_9() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___Player_Direction_9)); }
	inline Vector3_t2243707580  get_Player_Direction_9() const { return ___Player_Direction_9; }
	inline Vector3_t2243707580 * get_address_of_Player_Direction_9() { return &___Player_Direction_9; }
	inline void set_Player_Direction_9(Vector3_t2243707580  value)
	{
		___Player_Direction_9 = value;
	}

	inline static int32_t get_offset_of_Update_PlayerVars_10() { return static_cast<int32_t>(offsetof(touchBendingPlayerListener_t2574995411, ___Update_PlayerVars_10)); }
	inline bool get_Update_PlayerVars_10() const { return ___Update_PlayerVars_10; }
	inline bool* get_address_of_Update_PlayerVars_10() { return &___Update_PlayerVars_10; }
	inline void set_Update_PlayerVars_10(bool value)
	{
		___Update_PlayerVars_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
