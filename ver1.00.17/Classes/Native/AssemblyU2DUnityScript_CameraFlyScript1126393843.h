﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFlyScript
struct  CameraFlyScript_t1126393843  : public MonoBehaviour_t1158329972
{
public:
	// System.Single CameraFlyScript::mainSpeed
	float ___mainSpeed_2;
	// System.Single CameraFlyScript::shiftAdd
	float ___shiftAdd_3;
	// System.Single CameraFlyScript::maxShift
	float ___maxShift_4;
	// System.Single CameraFlyScript::camSens
	float ___camSens_5;
	// UnityEngine.Vector3 CameraFlyScript::lastMouse
	Vector3_t2243707580  ___lastMouse_6;
	// System.Single CameraFlyScript::totalRun
	float ___totalRun_7;

public:
	inline static int32_t get_offset_of_mainSpeed_2() { return static_cast<int32_t>(offsetof(CameraFlyScript_t1126393843, ___mainSpeed_2)); }
	inline float get_mainSpeed_2() const { return ___mainSpeed_2; }
	inline float* get_address_of_mainSpeed_2() { return &___mainSpeed_2; }
	inline void set_mainSpeed_2(float value)
	{
		___mainSpeed_2 = value;
	}

	inline static int32_t get_offset_of_shiftAdd_3() { return static_cast<int32_t>(offsetof(CameraFlyScript_t1126393843, ___shiftAdd_3)); }
	inline float get_shiftAdd_3() const { return ___shiftAdd_3; }
	inline float* get_address_of_shiftAdd_3() { return &___shiftAdd_3; }
	inline void set_shiftAdd_3(float value)
	{
		___shiftAdd_3 = value;
	}

	inline static int32_t get_offset_of_maxShift_4() { return static_cast<int32_t>(offsetof(CameraFlyScript_t1126393843, ___maxShift_4)); }
	inline float get_maxShift_4() const { return ___maxShift_4; }
	inline float* get_address_of_maxShift_4() { return &___maxShift_4; }
	inline void set_maxShift_4(float value)
	{
		___maxShift_4 = value;
	}

	inline static int32_t get_offset_of_camSens_5() { return static_cast<int32_t>(offsetof(CameraFlyScript_t1126393843, ___camSens_5)); }
	inline float get_camSens_5() const { return ___camSens_5; }
	inline float* get_address_of_camSens_5() { return &___camSens_5; }
	inline void set_camSens_5(float value)
	{
		___camSens_5 = value;
	}

	inline static int32_t get_offset_of_lastMouse_6() { return static_cast<int32_t>(offsetof(CameraFlyScript_t1126393843, ___lastMouse_6)); }
	inline Vector3_t2243707580  get_lastMouse_6() const { return ___lastMouse_6; }
	inline Vector3_t2243707580 * get_address_of_lastMouse_6() { return &___lastMouse_6; }
	inline void set_lastMouse_6(Vector3_t2243707580  value)
	{
		___lastMouse_6 = value;
	}

	inline static int32_t get_offset_of_totalRun_7() { return static_cast<int32_t>(offsetof(CameraFlyScript_t1126393843, ___totalRun_7)); }
	inline float get_totalRun_7() const { return ___totalRun_7; }
	inline float* get_address_of_totalRun_7() { return &___totalRun_7; }
	inline void set_totalRun_7(float value)
	{
		___totalRun_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
