﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// moveMe
struct  moveMe_t881802511  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 moveMe::oneToThree
	int32_t ___oneToThree_2;

public:
	inline static int32_t get_offset_of_oneToThree_2() { return static_cast<int32_t>(offsetof(moveMe_t881802511, ___oneToThree_2)); }
	inline int32_t get_oneToThree_2() const { return ___oneToThree_2; }
	inline int32_t* get_address_of_oneToThree_2() { return &___oneToThree_2; }
	inline void set_oneToThree_2(int32_t value)
	{
		___oneToThree_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
