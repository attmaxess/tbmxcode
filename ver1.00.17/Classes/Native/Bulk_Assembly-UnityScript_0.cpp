﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_CameraFlyScript1126393843.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DUnityScript_CharacterMotor262030084.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovement944153967.h"
#include "AssemblyU2DUnityScript_CharacterMotorJumping1708272304.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovingPlatform365475463.h"
#include "AssemblyU2DUnityScript_CharacterMotorSliding3749388514.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "AssemblyU2DUnityScript_MovementTransferOnJump3438008145.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN1999783645.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen3108987245.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN2847579946.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "AssemblyU2DUnityScript_CharacterState1314841520.h"
#include "AssemblyU2DUnityScript_FPSInputController4241249601.h"
#include "AssemblyU2DUnityScript_MouseOrbit143029581.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "AssemblyU2DUnityScript_moveMe881802511.h"
#include "AssemblyU2DUnityScript_PlatformInputController4273899755.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "AssemblyU2DUnityScript_ScrollUV2335303814.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DUnityScript_ThirdPersonCamera2751132817.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "AssemblyU2DUnityScript_ThirdPersonController1841729452.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// CameraFlyScript
struct CameraFlyScript_t1126393843;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// CharacterMotor
struct CharacterMotor_t262030084;
// CharacterMotorMovement
struct CharacterMotorMovement_t944153967;
// CharacterMotorJumping
struct CharacterMotorJumping_t1708272304;
// CharacterMotorMovingPlatform
struct CharacterMotorMovingPlatform_t365475463;
// CharacterMotorSliding
struct CharacterMotorSliding_t3749388514;
// System.Type
struct Type_t;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Object
struct Object_t1021602117;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// UnityEngine.Collider
struct Collider_t3497673348;
// CharacterMotor/$SubtractNewPlatformVelocity$17
struct U24SubtractNewPlatformVelocityU2417_t1999783645;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// Boo.Lang.GenericGenerator`1<System.Object>
struct GenericGenerator_1_t3108987245;
// CharacterMotor/$SubtractNewPlatformVelocity$17/$
struct U24_t2847579946;
// Boo.Lang.GenericGeneratorEnumerator`1<System.Object>
struct GenericGeneratorEnumerator_1_t3445420457;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t3968615785;
// System.Object
struct Il2CppObject;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829;
// FPSInputController
struct FPSInputController_t4241249601;
// MouseOrbit
struct MouseOrbit_t143029581;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// moveMe
struct moveMe_t881802511;
// PlatformInputController
struct PlatformInputController_t4273899755;
// UnityEngine.Camera
struct Camera_t189460977;
// ScrollUV
struct ScrollUV_t2335303814;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material
struct Material_t193706927;
// ThirdPersonCamera
struct ThirdPersonCamera_t2751132817;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// ThirdPersonController
struct ThirdPersonController_t1841729452;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.AnimationState
struct AnimationState_t1303741697;
// UnityEngine.GameObject
struct GameObject_t1756533147;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t CameraFlyScript_Update_m834546700_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t CameraFlyScript_GetBaseInput_m3022531876_MetadataUsageId;
extern Il2CppClass* CharacterMotorMovement_t944153967_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotorJumping_t1708272304_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotorMovingPlatform_t365475463_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterMotorSliding_t3749388514_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor__ctor_m3361430202_MetadataUsageId;
extern const Il2CppType* CharacterController_t4094781467_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterController_t4094781467_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_Awake_m795959529_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843240766;
extern Il2CppCodeGenString* _stringLiteral487010350;
extern const uint32_t CharacterMotor_UpdateFunction_m2269033867_MetadataUsageId;
extern const uint32_t CharacterMotor_FixedUpdate_m1016746259_MetadataUsageId;
extern const uint32_t CharacterMotor_ApplyInputVelocityChange_m4116974862_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2053352307;
extern const uint32_t CharacterMotor_ApplyGravityAndJumping_m3598474464_MetadataUsageId;
extern Il2CppClass* U24SubtractNewPlatformVelocityU2417_t1999783645_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotor_SubtractNewPlatformVelocity_m2006412010_MetadataUsageId;
extern const uint32_t CharacterMotor_MoveWithPlatform_m4019888974_MetadataUsageId;
extern const uint32_t CharacterMotor_GetDesiredHorizontalVelocity_m555853785_MetadataUsageId;
extern const uint32_t CharacterMotor_CalculateJumpVerticalSpeed_m2495405676_MetadataUsageId;
extern const uint32_t CharacterMotor_TooSteep_m4157442919_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2724755691;
extern const uint32_t CharacterMotor_SetVelocity_m1132550922_MetadataUsageId;
extern const MethodInfo* GenericGenerator_1__ctor_m409073663_MethodInfo_var;
extern const uint32_t U24SubtractNewPlatformVelocityU2417__ctor_m1768541611_MetadataUsageId;
extern Il2CppClass* U24_t2847579946_il2cpp_TypeInfo_var;
extern const uint32_t U24SubtractNewPlatformVelocityU2417_GetEnumerator_m2985528835_MetadataUsageId;
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m509214543_MethodInfo_var;
extern const uint32_t U24__ctor_m4034603524_MetadataUsageId;
extern Il2CppClass* WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m799132706_MethodInfo_var;
extern const uint32_t U24_MoveNext_m333678758_MetadataUsageId;
extern Il2CppClass* KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMotorMovement__ctor_m1502129899_MetadataUsageId;
extern const Il2CppType* CharacterMotor_t262030084_0_0_0_var;
extern Il2CppClass* CharacterMotor_t262030084_il2cpp_TypeInfo_var;
extern const uint32_t FPSInputController_Awake_m1855081626_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern const uint32_t FPSInputController_Update_m3258574412_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var;
extern const uint32_t MouseOrbit_Start_m1629475745_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern const uint32_t MouseOrbit_LateUpdate_m536119640_MetadataUsageId;
extern const uint32_t MouseOrbit_ClampAngle_m4104644686_MetadataUsageId;
extern const uint32_t moveMe_Update_m2642796658_MetadataUsageId;
extern const uint32_t PlatformInputController_Awake_m679619692_MetadataUsageId;
extern const uint32_t PlatformInputController_Update_m3789481214_MetadataUsageId;
extern const uint32_t PlatformInputController_ConstantSlerp_m1768991218_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m772028041_MethodInfo_var;
extern const uint32_t ScrollUV_Update_m1167004397_MetadataUsageId;
extern const Il2CppType* ThirdPersonController_t1841729452_0_0_0_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ThirdPersonController_t1841729452_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t3497673348_m2974738468_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2172988211;
extern Il2CppCodeGenString* _stringLiteral1941325067;
extern const uint32_t ThirdPersonCamera_Awake_m2969377770_MetadataUsageId;
extern const uint32_t ThirdPersonCamera_DebugDrawStuff_m2634059124_MetadataUsageId;
extern const uint32_t ThirdPersonCamera_AngleDistance_m1831836461_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3645101712;
extern const uint32_t ThirdPersonCamera_Apply_m3614534807_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const uint32_t ThirdPersonCamera_SetUpRotation_m1930509600_MetadataUsageId;
extern const Il2CppType* Animation_t2068071072_0_0_0_var;
extern Il2CppClass* Animation_t2068071072_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1182925398;
extern Il2CppCodeGenString* _stringLiteral1772573736;
extern Il2CppCodeGenString* _stringLiteral2268382563;
extern Il2CppCodeGenString* _stringLiteral585509787;
extern Il2CppCodeGenString* _stringLiteral712064556;
extern const uint32_t ThirdPersonController_Awake_m1851122549_MetadataUsageId;
extern const uint32_t ThirdPersonController_UpdateSmoothedMovementDirection_m1607820556_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral49017975;
extern const uint32_t ThirdPersonController_ApplyJumping_m2303093686_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2473619578;
extern const uint32_t ThirdPersonController_ApplyGravity_m3630102144_MetadataUsageId;
extern const uint32_t ThirdPersonController_CalculateJumpVerticalSpeed_m266022954_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3302144998;
extern const uint32_t ThirdPersonController_Update_m3514131137_MetadataUsageId;
extern const uint32_t ThirdPersonController_IsMoving_m2115366220_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t ThirdPersonController_Reset_m1776449033_MetadataUsageId;

// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Keyframe_t1449471340  m_Items[1];

public:
	inline Keyframe_t1449471340  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t1449471340 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t1449471340  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t1449471340  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t1449471340 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t1449471340  value)
	{
		m_Items[index] = value;
	}
};


// System.Void Boo.Lang.GenericGenerator`1<System.Object>::.ctor()
extern "C"  void GenericGenerator_1__ctor_m409073663_gshared (GenericGenerator_1_t3108987245 * __this, const MethodInfo* method);
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::.ctor()
extern "C"  void GenericGeneratorEnumerator_1__ctor_m509214543_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method);
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Yield(System.Int32,!0)
extern "C"  bool GenericGeneratorEnumerator_1_Yield_m3421350270_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, int32_t p0, Il2CppObject * p1, const MethodInfo* method);
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::YieldDefault(System.Int32)
extern "C"  bool GenericGeneratorEnumerator_1_YieldDefault_m799132706_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, int32_t p0, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m146923508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m2407545601 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t2243707580  Transform_get_eulerAngles_m4066505159 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m2881310872 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CameraFlyScript::GetBaseInput()
extern "C"  Vector3_t2243707580  CameraFlyScript_GetBaseInput_m3022531876 (CameraFlyScript_t1126393843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C"  bool Input_GetKey_m3849524999 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m2354025655 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m3316827744 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMotorMovement::.ctor()
extern "C"  void CharacterMotorMovement__ctor_m1502129899 (CharacterMotorMovement_t944153967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMotorJumping::.ctor()
extern "C"  void CharacterMotorJumping__ctor_m1668297276 (CharacterMotorJumping_t1708272304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMotorMovingPlatform::.ctor()
extern "C"  void CharacterMotorMovingPlatform__ctor_m3422518867 (CharacterMotorMovingPlatform_t365475463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMotorSliding::.ctor()
extern "C"  void CharacterMotorSliding__ctor_m2145873446 (CharacterMotorSliding_t3749388514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponent_m4225719715 (Component_t3819376471 * __this, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CharacterMotor::ApplyInputVelocityChange(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CharacterMotor_ApplyInputVelocityChange_m4116974862 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CharacterMotor::ApplyGravityAndJumping(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CharacterMotor_ApplyGravityAndJumping_m3598474464 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterMotor::MoveWithPlatform()
extern "C"  bool CharacterMotor_MoveWithPlatform_m4019888974 (CharacterMotor_t262030084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_TransformPoint_m3272254198 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Inequality_m799191452 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m3456882757 (CharacterController_t4094781467 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_op_Multiply_m2426727589 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  p0, Quaternion_t4030073918  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_Inverse_m3931399088 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991 (Quaternion_t4030073918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Rotate_m4255273365 (Transform_t3275118058 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_stepOffset()
extern "C"  float CharacterController_get_stepOffset_m2492821916 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m860342598 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m2564622569 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2243707580  Vector3_get_up_m2725403797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m3872631309 (Il2CppObject * __this /* static, unused */, float p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t2933234003  Transform_get_localToWorldMatrix_m2868579006 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Division_m3315615850 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m305888255 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m3161182818 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1814096310 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m3888954684 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CharacterMotor::IsGroundedTest()
extern "C"  bool CharacterMotor_IsGroundedTest_m3567633754 (CharacterMotor_t262030084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m4199581575 (Component_t3819376471 * __this, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CharacterMotor::SubtractNewPlatformVelocity()
extern "C"  Il2CppObject * CharacterMotor_SubtractNewPlatformVelocity_m2006412010 (CharacterMotor_t262030084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.CharacterController::get_center()
extern "C"  Vector3_t2243707580  CharacterController_get_center_m3262687436 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_height()
extern "C"  float CharacterController_get_height_m2830713110 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_radius()
extern "C"  float CharacterController_get_radius_m2751828411 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformPoint_m2648491174 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Matrix4x4_MultiplyPoint3x4_m1007952212 (Matrix4x4_t2933234003 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMotor::UpdateFunction()
extern "C"  void CharacterMotor_UpdateFunction_m2269033867 (CharacterMotor_t262030084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2243707580  Vector3_get_normalized_m936072361 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Project(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Project_m1396027688 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CharacterMotor::GetDesiredHorizontalVelocity()
extern "C"  Vector3_t2243707580  CharacterMotor_GetDesiredHorizontalVelocity_m555853785 (CharacterMotor_t262030084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CharacterMotor::AdjustGroundVelocityToNormal(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CharacterMotor_AdjustGroundVelocityToNormal_m3451990381 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___hVelocity0, Vector3_t2243707580  ___groundNormal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1648492575 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2216684562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_Slerp_m846771032 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_normal()
extern "C"  Vector3_t2243707580  ControllerColliderHit_get_normal_m1098215280 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C"  Vector3_t2243707580  ControllerColliderHit_get_moveDirection_m3053186297 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_point()
extern "C"  Vector3_t2243707580  ControllerColliderHit_get_point_m3573703281 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.ControllerColliderHit::get_collider()
extern "C"  Collider_t3497673348 * ControllerColliderHit_get_collider_m3897495767 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$17::.ctor(CharacterMotor)
extern "C"  void U24SubtractNewPlatformVelocityU2417__ctor_m1768541611 (U24SubtractNewPlatformVelocityU2417_t1999783645 * __this, CharacterMotor_t262030084 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> CharacterMotor/$SubtractNewPlatformVelocity$17::GetEnumerator()
extern "C"  Il2CppObject* U24SubtractNewPlatformVelocityU2417_GetEnumerator_m2985528835 (U24SubtractNewPlatformVelocityU2417_t1999783645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformDirection_m3595190459 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m3698879322 (AnimationCurve_t3306541151 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_TransformDirection_m1639585047 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Cross_m4149044051 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CharacterController::get_slopeLimit()
extern "C"  float CharacterController_get_slopeLimit_m1930597355 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C"  void Component_SendMessage_m3615678587 (Component_t3819376471 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.GenericGenerator`1<System.Object>::.ctor()
#define GenericGenerator_1__ctor_m409073663(__this, method) ((  void (*) (GenericGenerator_1_t3108987245 *, const MethodInfo*))GenericGenerator_1__ctor_m409073663_gshared)(__this, method)
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$17/$::.ctor(CharacterMotor)
extern "C"  void U24__ctor_m4034603524 (U24_t2847579946 * __this, CharacterMotor_t262030084 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::.ctor()
#define GenericGeneratorEnumerator_1__ctor_m509214543(__this, method) ((  void (*) (GenericGeneratorEnumerator_1_t3445420457 *, const MethodInfo*))GenericGeneratorEnumerator_1__ctor_m509214543_gshared)(__this, method)
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C"  void WaitForFixedUpdate__ctor_m3781413380 (WaitForFixedUpdate_t3968615785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Yield(System.Int32,!0)
#define GenericGeneratorEnumerator_1_Yield_m3421350270(__this, p0, p1, method) ((  bool (*) (GenericGeneratorEnumerator_1_t3445420457 *, int32_t, Il2CppObject *, const MethodInfo*))GenericGeneratorEnumerator_1_Yield_m3421350270_gshared)(__this, p0, p1, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::YieldDefault(System.Int32)
#define GenericGeneratorEnumerator_1_YieldDefault_m799132706(__this, p0, method) ((  bool (*) (GenericGeneratorEnumerator_1_t3445420457 *, int32_t, const MethodInfo*))GenericGeneratorEnumerator_1_YieldDefault_m799132706_gshared)(__this, p0, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C"  void Keyframe__ctor_m2042404667 (Keyframe_t1449471340 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m2814448007 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m2098048324 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_op_Multiply_m1483423721 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C"  bool Input_GetButton_m38251721 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m2131864169 (Rigidbody_t4233889191 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m464100923 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C"  void Cursor_set_visible_m860533511 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float MouseOrbit_ClampAngle_m4104644686 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3411284563 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t2243707580  Transform_get_forward_m1833488937 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_UnaryNegation_m3383802608 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t2243707580  Transform_get_up_m1603627763 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_FromToRotation_m1685306068 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m700700634 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Angle_m2552334978 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m772028041(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t193706927 * Renderer_get_material_m2553789785 (Renderer_t257310565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_mainTextureOffset(UnityEngine.Vector2)
extern "C"  void Material_set_mainTextureOffset_m3533368774 (Material_t193706927 * __this, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1796096907 (Behaviour_t955675639 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t3497673348_m2974738468(__this, method) ((  Collider_t3497673348 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern "C"  Bounds_t3033363703  Collider_get_bounds_m3534458178 (Collider_t3497673348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t2243707580  Bounds_get_center_m129401026 (Bounds_t3033363703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t2243707580  Bounds_get_max_m4247050707 (Bounds_t3033363703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Debug_DrawLine_m2961609580 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C"  float Mathf_Repeat_m943844734 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single)
extern "C"  float Mathf_SmoothDampAngle_m2501423753 (Il2CppObject * __this /* static, unused */, float p0, float p1, float* p2, float p3, float p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern "C"  float Mathf_SmoothDamp_m1166236953 (Il2CppObject * __this /* static, unused */, float p0, float p1, float* p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t2243707580  Vector3_get_back_m4246539215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m633695927 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1201659139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t2243707580  Vector3_get_down_m2372302126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.Ray UnityEngine.Camera::ViewportPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t2469606224  Camera_ViewportPointToRay_m1799506792 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366 (Ray_t2469606224 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533 (Ray_t2469606224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m4133353720 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::RotateTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t2243707580  Vector3_RotateTowards_m4046679056 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1686556575 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Input::ResetInputAxes()
extern "C"  void Input_ResetInputAxes_m3212187783 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m2792523731 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C"  AnimationState_t1303741697 * Animation_get_Item_m4198128320 (Animation_t2068071072 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C"  void AnimationState_set_speed_m465014523 (AnimationState_t1303741697 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationState_set_wrapMode_m2342385428 (AnimationState_t1303741697 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::CrossFade(System.String)
extern "C"  void Animation_CrossFade_m3878519673 (Animation_t2068071072 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.CharacterController::get_velocity()
extern "C"  Vector3_t2243707580  CharacterController_get_velocity_m1484936086 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C"  void GameObject_set_tag_m717375123 (GameObject_t1756533147 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraFlyScript::.ctor()
extern "C"  void CameraFlyScript__ctor_m2454634147 (CameraFlyScript_t1126393843 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_mainSpeed_2((100.0f));
		__this->set_shiftAdd_3((250.0f));
		__this->set_maxShift_4((1000.0f));
		__this->set_camSens_5((0.25f));
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (((float)((float)((int32_t)255)))), (((float)((float)((int32_t)255)))), (((float)((float)((int32_t)255)))), /*hidden argument*/NULL);
		__this->set_lastMouse_6(L_0);
		__this->set_totalRun_7((1.0f));
		return;
	}
}
// System.Void CameraFlyScript::Update()
extern "C"  void CameraFlyScript_Update_m834546700 (CameraFlyScript_t1126393843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFlyScript_Update_m834546700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_lastMouse_6();
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_lastMouse_6(L_2);
		Vector3_t2243707580 * L_3 = __this->get_address_of_lastMouse_6();
		float L_4 = L_3->get_y_2();
		float L_5 = __this->get_camSens_5();
		Vector3_t2243707580 * L_6 = __this->get_address_of_lastMouse_6();
		float L_7 = L_6->get_x_1();
		float L_8 = __this->get_camSens_5();
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, ((float)((float)((-L_4))*(float)L_5)), ((float)((float)L_7*(float)L_8)), (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_lastMouse_6(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_eulerAngles_m4066505159(L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = (&V_4)->get_x_1();
		Vector3_t2243707580 * L_13 = __this->get_address_of_lastMouse_6();
		float L_14 = L_13->get_x_1();
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_eulerAngles_m4066505159(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = (&V_5)->get_y_2();
		Vector3_t2243707580 * L_18 = __this->get_address_of_lastMouse_6();
		float L_19 = L_18->get_y_2();
		Vector3_t2243707580  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2638739322(&L_20, ((float)((float)L_12+(float)L_14)), ((float)((float)L_17+(float)L_19)), (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_lastMouse_6(L_20);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = __this->get_lastMouse_6();
		NullCheck(L_21);
		Transform_set_eulerAngles_m2881310872(L_21, L_22, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastMouse_6(L_23);
		V_0 = (((float)((float)0)));
		Vector3_t2243707580  L_24 = CameraFlyScript_GetBaseInput_m3022531876(__this, /*hidden argument*/NULL);
		V_1 = L_24;
		bool L_25 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0159;
		}
	}
	{
		float L_26 = __this->get_totalRun_7();
		float L_27 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_totalRun_7(((float)((float)L_26+(float)L_27)));
		Vector3_t2243707580  L_28 = V_1;
		float L_29 = __this->get_totalRun_7();
		Vector3_t2243707580  L_30 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		float L_31 = __this->get_shiftAdd_3();
		Vector3_t2243707580  L_32 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		V_1 = L_32;
		float L_33 = (&V_1)->get_x_1();
		float L_34 = __this->get_maxShift_4();
		float L_35 = __this->get_maxShift_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_36 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_33, ((-L_34)), L_35, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_36);
		float L_37 = (&V_1)->get_y_2();
		float L_38 = __this->get_maxShift_4();
		float L_39 = __this->get_maxShift_4();
		float L_40 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_37, ((-L_38)), L_39, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_40);
		float L_41 = (&V_1)->get_z_3();
		float L_42 = __this->get_maxShift_4();
		float L_43 = __this->get_maxShift_4();
		float L_44 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_41, ((-L_42)), L_43, /*hidden argument*/NULL);
		(&V_1)->set_z_3(L_44);
		goto IL_0185;
	}

IL_0159:
	{
		float L_45 = __this->get_totalRun_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_46 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_45*(float)(0.5f))), (((float)((float)1))), (((float)((float)((int32_t)1000)))), /*hidden argument*/NULL);
		__this->set_totalRun_7(L_46);
		Vector3_t2243707580  L_47 = V_1;
		float L_48 = __this->get_mainSpeed_2();
		Vector3_t2243707580  L_49 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		V_1 = L_49;
	}

IL_0185:
	{
		Vector3_t2243707580  L_50 = V_1;
		float L_51 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		V_1 = L_52;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_53 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_01f5;
		}
	}
	{
		Transform_t3275118058 * L_54 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		Vector3_t2243707580  L_55 = Transform_get_position_m1104419803(L_54, /*hidden argument*/NULL);
		V_6 = L_55;
		float L_56 = (&V_6)->get_y_2();
		V_0 = L_56;
		Transform_t3275118058 * L_57 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_58 = V_1;
		NullCheck(L_57);
		Transform_Translate_m3316827744(L_57, L_58, /*hidden argument*/NULL);
		float L_59 = V_0;
		float L_60 = L_59;
		V_2 = L_60;
		Transform_t3275118058 * L_61 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t2243707580  L_62 = Transform_get_position_m1104419803(L_61, /*hidden argument*/NULL);
		Vector3_t2243707580  L_63 = L_62;
		V_3 = L_63;
		float L_64 = V_2;
		float L_65 = L_64;
		V_7 = L_65;
		(&V_3)->set_y_2(L_65);
		float L_66 = V_7;
		Transform_t3275118058 * L_67 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_68 = V_3;
		Vector3_t2243707580  L_69 = L_68;
		V_8 = L_69;
		NullCheck(L_67);
		Transform_set_position_m2469242620(L_67, L_69, /*hidden argument*/NULL);
		Vector3_t2243707580  L_70 = V_8;
		goto IL_0201;
	}

IL_01f5:
	{
		Transform_t3275118058 * L_71 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_72 = V_1;
		NullCheck(L_71);
		Transform_Translate_m3316827744(L_71, L_72, /*hidden argument*/NULL);
	}

IL_0201:
	{
		return;
	}
}
// UnityEngine.Vector3 CameraFlyScript::GetBaseInput()
extern "C"  Vector3_t2243707580  CameraFlyScript_GetBaseInput_m3022531876 (CameraFlyScript_t1126393843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFlyScript_GetBaseInput_m3022531876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)119), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Vector3_t2243707580  L_1 = V_0;
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, (((float)((float)0))), (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		Vector3_t2243707580  L_5 = V_0;
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, (((float)((float)0))), (((float)((float)0))), (((float)((float)(-1)))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		Vector3_t2243707580  L_9 = V_0;
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, (((float)((float)(-1)))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_0062:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0080;
		}
	}
	{
		Vector3_t2243707580  L_13 = V_0;
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (((float)((float)1))), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0080:
	{
		Vector3_t2243707580  L_16 = V_0;
		return L_16;
	}
}
// System.Void CameraFlyScript::Main()
extern "C"  void CameraFlyScript_Main_m3216879160 (CameraFlyScript_t1126393843 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CharacterMotor::.ctor()
extern "C"  void CharacterMotor__ctor_m3361430202 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor__ctor_m3361430202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_canControl_2((bool)1);
		__this->set_useFixedUpdate_3((bool)1);
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputMoveDirection_4(L_0);
		CharacterMotorMovement_t944153967 * L_1 = (CharacterMotorMovement_t944153967 *)il2cpp_codegen_object_new(CharacterMotorMovement_t944153967_il2cpp_TypeInfo_var);
		CharacterMotorMovement__ctor_m1502129899(L_1, /*hidden argument*/NULL);
		__this->set_movement_6(L_1);
		CharacterMotorJumping_t1708272304 * L_2 = (CharacterMotorJumping_t1708272304 *)il2cpp_codegen_object_new(CharacterMotorJumping_t1708272304_il2cpp_TypeInfo_var);
		CharacterMotorJumping__ctor_m1668297276(L_2, /*hidden argument*/NULL);
		__this->set_jumping_7(L_2);
		CharacterMotorMovingPlatform_t365475463 * L_3 = (CharacterMotorMovingPlatform_t365475463 *)il2cpp_codegen_object_new(CharacterMotorMovingPlatform_t365475463_il2cpp_TypeInfo_var);
		CharacterMotorMovingPlatform__ctor_m3422518867(L_3, /*hidden argument*/NULL);
		__this->set_movingPlatform_8(L_3);
		CharacterMotorSliding_t3749388514 * L_4 = (CharacterMotorSliding_t3749388514 *)il2cpp_codegen_object_new(CharacterMotorSliding_t3749388514_il2cpp_TypeInfo_var);
		CharacterMotorSliding__ctor_m2145873446(L_4, /*hidden argument*/NULL);
		__this->set_sliding_9(L_4);
		__this->set_grounded_10((bool)1);
		Vector3_t2243707580  L_5 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_groundNormal_11(L_5);
		Vector3_t2243707580  L_6 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastGroundNormal_12(L_6);
		return;
	}
}
// System.Void CharacterMotor::Awake()
extern "C"  void CharacterMotor_Awake_m795959529 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_Awake_m795959529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CharacterController_t4094781467_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_1 = Component_GetComponent_m4225719715(__this, L_0, /*hidden argument*/NULL);
		__this->set_controller_14(((CharacterController_t4094781467 *)CastclassSealed(L_1, CharacterController_t4094781467_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_tr_13(L_2);
		return;
	}
}
// System.Void CharacterMotor::UpdateFunction()
extern "C"  void CharacterMotor_UpdateFunction_m2269033867 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_UpdateFunction_m2269033867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t4030073918  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Quaternion_t4030073918  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	{
		CharacterMotorMovement_t944153967 * L_0 = __this->get_movement_6();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = L_0->get_velocity_9();
		V_0 = L_1;
		Vector3_t2243707580  L_2 = V_0;
		Vector3_t2243707580  L_3 = CharacterMotor_ApplyInputVelocityChange_m4116974862(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t2243707580  L_4 = V_0;
		Vector3_t2243707580  L_5 = CharacterMotor_ApplyGravityAndJumping_m3598474464(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector3_t2243707580  L_6 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		bool L_7 = CharacterMotor_MoveWithPlatform_m4019888974(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00dd;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_8 = __this->get_movingPlatform_8();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_activePlatform_3();
		CharacterMotorMovingPlatform_t365475463 * L_10 = __this->get_movingPlatform_8();
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = L_10->get_activeLocalPoint_4();
		NullCheck(L_9);
		Vector3_t2243707580  L_12 = Transform_TransformPoint_m3272254198(L_9, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		Vector3_t2243707580  L_13 = V_2;
		CharacterMotorMovingPlatform_t365475463 * L_14 = __this->get_movingPlatform_8();
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = L_14->get_activeGlobalPoint_5();
		Vector3_t2243707580  L_16 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		Vector3_t2243707580  L_17 = V_1;
		Vector3_t2243707580  L_18 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0078;
		}
	}
	{
		CharacterController_t4094781467 * L_20 = __this->get_controller_14();
		Vector3_t2243707580  L_21 = V_1;
		NullCheck(L_20);
		CharacterController_Move_m3456882757(L_20, L_21, /*hidden argument*/NULL);
	}

IL_0078:
	{
		CharacterMotorMovingPlatform_t365475463 * L_22 = __this->get_movingPlatform_8();
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = L_22->get_activePlatform_3();
		NullCheck(L_23);
		Quaternion_t4030073918  L_24 = Transform_get_rotation_m1033555130(L_23, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t365475463 * L_25 = __this->get_movingPlatform_8();
		NullCheck(L_25);
		Quaternion_t4030073918  L_26 = L_25->get_activeLocalRotation_6();
		Quaternion_t4030073918  L_27 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		Quaternion_t4030073918  L_28 = V_3;
		CharacterMotorMovingPlatform_t365475463 * L_29 = __this->get_movingPlatform_8();
		NullCheck(L_29);
		Quaternion_t4030073918  L_30 = L_29->get_activeGlobalRotation_7();
		Quaternion_t4030073918  L_31 = Quaternion_Inverse_m3931399088(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_32 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_28, L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		Vector3_t2243707580  L_33 = Quaternion_get_eulerAngles_m3302573991((&V_4), /*hidden argument*/NULL);
		V_12 = L_33;
		float L_34 = (&V_12)->get_y_2();
		V_5 = L_34;
		float L_35 = V_5;
		if ((((float)L_35) == ((float)(((float)((float)0))))))
		{
			goto IL_00dd;
		}
	}
	{
		Transform_t3275118058 * L_36 = __this->get_tr_13();
		float L_37 = V_5;
		NullCheck(L_36);
		Transform_Rotate_m4255273365(L_36, (((float)((float)0))), L_37, (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_00dd:
	{
		Transform_t3275118058 * L_38 = __this->get_tr_13();
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_position_m1104419803(L_38, /*hidden argument*/NULL);
		V_6 = L_39;
		Vector3_t2243707580  L_40 = V_0;
		float L_41 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_7 = L_42;
		CharacterController_t4094781467 * L_43 = __this->get_controller_14();
		NullCheck(L_43);
		float L_44 = CharacterController_get_stepOffset_m2492821916(L_43, /*hidden argument*/NULL);
		float L_45 = (&V_7)->get_x_1();
		float L_46 = (&V_7)->get_z_3();
		Vector3_t2243707580  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m2638739322(&L_47, L_45, (((float)((float)0))), L_46, /*hidden argument*/NULL);
		V_13 = L_47;
		float L_48 = Vector3_get_magnitude_m860342598((&V_13), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_49 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_44, L_48, /*hidden argument*/NULL);
		V_8 = L_49;
		bool L_50 = __this->get_grounded_10();
		if (!L_50)
		{
			goto IL_0147;
		}
	}
	{
		Vector3_t2243707580  L_51 = V_7;
		float L_52 = V_8;
		Vector3_t2243707580  L_53 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_55 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_51, L_54, /*hidden argument*/NULL);
		V_7 = L_55;
	}

IL_0147:
	{
		CharacterMotorMovingPlatform_t365475463 * L_56 = __this->get_movingPlatform_8();
		NullCheck(L_56);
		L_56->set_hitPlatform_2((Transform_t3275118058 *)NULL);
		Vector3_t2243707580  L_57 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_groundNormal_11(L_57);
		CharacterMotorMovement_t944153967 * L_58 = __this->get_movement_6();
		CharacterController_t4094781467 * L_59 = __this->get_controller_14();
		Vector3_t2243707580  L_60 = V_7;
		NullCheck(L_59);
		int32_t L_61 = CharacterController_Move_m3456882757(L_59, L_60, /*hidden argument*/NULL);
		NullCheck(L_58);
		L_58->set_collisionFlags_8(L_61);
		CharacterMotorMovement_t944153967 * L_62 = __this->get_movement_6();
		CharacterMotorMovement_t944153967 * L_63 = __this->get_movement_6();
		NullCheck(L_63);
		Vector3_t2243707580  L_64 = L_63->get_hitPoint_11();
		NullCheck(L_62);
		L_62->set_lastHitPoint_12(L_64);
		Vector3_t2243707580  L_65 = __this->get_groundNormal_11();
		__this->set_lastGroundNormal_12(L_65);
		CharacterMotorMovingPlatform_t365475463 * L_66 = __this->get_movingPlatform_8();
		NullCheck(L_66);
		bool L_67 = L_66->get_enabled_0();
		if (!L_67)
		{
			goto IL_021b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_68 = __this->get_movingPlatform_8();
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = L_68->get_activePlatform_3();
		CharacterMotorMovingPlatform_t365475463 * L_70 = __this->get_movingPlatform_8();
		NullCheck(L_70);
		Transform_t3275118058 * L_71 = L_70->get_hitPlatform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_72 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_69, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_021b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_73 = __this->get_movingPlatform_8();
		NullCheck(L_73);
		Transform_t3275118058 * L_74 = L_73->get_hitPlatform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_75 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_74, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_021b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_76 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_77 = __this->get_movingPlatform_8();
		NullCheck(L_77);
		Transform_t3275118058 * L_78 = L_77->get_hitPlatform_2();
		NullCheck(L_76);
		L_76->set_activePlatform_3(L_78);
		CharacterMotorMovingPlatform_t365475463 * L_79 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_80 = __this->get_movingPlatform_8();
		NullCheck(L_80);
		Transform_t3275118058 * L_81 = L_80->get_hitPlatform_2();
		NullCheck(L_81);
		Matrix4x4_t2933234003  L_82 = Transform_get_localToWorldMatrix_m2868579006(L_81, /*hidden argument*/NULL);
		NullCheck(L_79);
		L_79->set_lastMatrix_8(L_82);
		CharacterMotorMovingPlatform_t365475463 * L_83 = __this->get_movingPlatform_8();
		NullCheck(L_83);
		L_83->set_newPlatform_10((bool)1);
	}

IL_021b:
	{
		float L_84 = (&V_0)->get_x_1();
		float L_85 = (&V_0)->get_z_3();
		Vector3_t2243707580  L_86;
		memset(&L_86, 0, sizeof(L_86));
		Vector3__ctor_m2638739322(&L_86, L_84, (((float)((float)0))), L_85, /*hidden argument*/NULL);
		V_9 = L_86;
		CharacterMotorMovement_t944153967 * L_87 = __this->get_movement_6();
		Transform_t3275118058 * L_88 = __this->get_tr_13();
		NullCheck(L_88);
		Vector3_t2243707580  L_89 = Transform_get_position_m1104419803(L_88, /*hidden argument*/NULL);
		Vector3_t2243707580  L_90 = V_6;
		Vector3_t2243707580  L_91 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		float L_92 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_93 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_87);
		L_87->set_velocity_9(L_93);
		CharacterMotorMovement_t944153967 * L_94 = __this->get_movement_6();
		NullCheck(L_94);
		Vector3_t2243707580 * L_95 = L_94->get_address_of_velocity_9();
		float L_96 = L_95->get_x_1();
		CharacterMotorMovement_t944153967 * L_97 = __this->get_movement_6();
		NullCheck(L_97);
		Vector3_t2243707580 * L_98 = L_97->get_address_of_velocity_9();
		float L_99 = L_98->get_z_3();
		Vector3_t2243707580  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Vector3__ctor_m2638739322(&L_100, L_96, (((float)((float)0))), L_99, /*hidden argument*/NULL);
		V_10 = L_100;
		Vector3_t2243707580  L_101 = V_9;
		Vector3_t2243707580  L_102 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_103 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_101, L_102, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_02bc;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_104 = __this->get_movement_6();
		CharacterMotorMovement_t944153967 * L_105 = __this->get_movement_6();
		NullCheck(L_105);
		Vector3_t2243707580 * L_106 = L_105->get_address_of_velocity_9();
		float L_107 = L_106->get_y_2();
		Vector3_t2243707580  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Vector3__ctor_m2638739322(&L_108, (((float)((float)0))), L_107, (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_104);
		L_104->set_velocity_9(L_108);
		goto IL_0307;
	}

IL_02bc:
	{
		Vector3_t2243707580  L_109 = V_10;
		Vector3_t2243707580  L_110 = V_9;
		float L_111 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		float L_112 = Vector3_get_sqrMagnitude_m1814096310((&V_9), /*hidden argument*/NULL);
		V_11 = ((float)((float)L_111/(float)L_112));
		CharacterMotorMovement_t944153967 * L_113 = __this->get_movement_6();
		Vector3_t2243707580  L_114 = V_9;
		float L_115 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_116 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		Vector3_t2243707580  L_117 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_114, L_116, /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_118 = __this->get_movement_6();
		NullCheck(L_118);
		Vector3_t2243707580 * L_119 = L_118->get_address_of_velocity_9();
		float L_120 = L_119->get_y_2();
		Vector3_t2243707580  L_121 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_122 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		Vector3_t2243707580  L_123 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_117, L_122, /*hidden argument*/NULL);
		NullCheck(L_113);
		L_113->set_velocity_9(L_123);
	}

IL_0307:
	{
		CharacterMotorMovement_t944153967 * L_124 = __this->get_movement_6();
		NullCheck(L_124);
		Vector3_t2243707580 * L_125 = L_124->get_address_of_velocity_9();
		float L_126 = L_125->get_y_2();
		float L_127 = (&V_0)->get_y_2();
		if ((((float)L_126) >= ((float)((float)((float)L_127-(float)(0.001f))))))
		{
			goto IL_0368;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_128 = __this->get_movement_6();
		NullCheck(L_128);
		Vector3_t2243707580 * L_129 = L_128->get_address_of_velocity_9();
		float L_130 = L_129->get_y_2();
		if ((((float)L_130) >= ((float)(((float)((float)0))))))
		{
			goto IL_035c;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_131 = __this->get_movement_6();
		NullCheck(L_131);
		Vector3_t2243707580 * L_132 = L_131->get_address_of_velocity_9();
		float L_133 = (&V_0)->get_y_2();
		L_132->set_y_2(L_133);
		goto IL_0368;
	}

IL_035c:
	{
		CharacterMotorJumping_t1708272304 * L_134 = __this->get_jumping_7();
		NullCheck(L_134);
		L_134->set_holdingJumpButton_6((bool)0);
	}

IL_0368:
	{
		bool L_135 = __this->get_grounded_10();
		if (!L_135)
		{
			goto IL_042b;
		}
	}
	{
		bool L_136 = CharacterMotor_IsGroundedTest_m3567633754(__this, /*hidden argument*/NULL);
		if (L_136)
		{
			goto IL_042b;
		}
	}
	{
		__this->set_grounded_10((bool)0);
		CharacterMotorMovingPlatform_t365475463 * L_137 = __this->get_movingPlatform_8();
		NullCheck(L_137);
		bool L_138 = L_137->get_enabled_0();
		if (!L_138)
		{
			goto IL_03f3;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_139 = __this->get_movingPlatform_8();
		NullCheck(L_139);
		int32_t L_140 = L_139->get_movementTransfer_1();
		if ((((int32_t)L_140) == ((int32_t)1)))
		{
			goto IL_03b7;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_141 = __this->get_movingPlatform_8();
		NullCheck(L_141);
		int32_t L_142 = L_141->get_movementTransfer_1();
		if ((!(((uint32_t)L_142) == ((uint32_t)2))))
		{
			goto IL_03f3;
		}
	}

IL_03b7:
	{
		CharacterMotorMovement_t944153967 * L_143 = __this->get_movement_6();
		CharacterMotorMovingPlatform_t365475463 * L_144 = __this->get_movingPlatform_8();
		NullCheck(L_144);
		Vector3_t2243707580  L_145 = L_144->get_platformVelocity_9();
		NullCheck(L_143);
		L_143->set_frameVelocity_10(L_145);
		CharacterMotorMovement_t944153967 * L_146 = __this->get_movement_6();
		CharacterMotorMovement_t944153967 * L_147 = __this->get_movement_6();
		NullCheck(L_147);
		Vector3_t2243707580  L_148 = L_147->get_velocity_9();
		CharacterMotorMovingPlatform_t365475463 * L_149 = __this->get_movingPlatform_8();
		NullCheck(L_149);
		Vector3_t2243707580  L_150 = L_149->get_platformVelocity_9();
		Vector3_t2243707580  L_151 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_148, L_150, /*hidden argument*/NULL);
		NullCheck(L_146);
		L_146->set_velocity_9(L_151);
	}

IL_03f3:
	{
		Component_SendMessage_m4199581575(__this, _stringLiteral843240766, 1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_152 = __this->get_tr_13();
		Transform_t3275118058 * L_153 = __this->get_tr_13();
		NullCheck(L_153);
		Vector3_t2243707580  L_154 = Transform_get_position_m1104419803(L_153, /*hidden argument*/NULL);
		float L_155 = V_8;
		Vector3_t2243707580  L_156 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_157 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_155, L_156, /*hidden argument*/NULL);
		Vector3_t2243707580  L_158 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_154, L_157, /*hidden argument*/NULL);
		NullCheck(L_152);
		Transform_set_position_m2469242620(L_152, L_158, /*hidden argument*/NULL);
		goto IL_046d;
	}

IL_042b:
	{
		bool L_159 = __this->get_grounded_10();
		if (L_159)
		{
			goto IL_046d;
		}
	}
	{
		bool L_160 = CharacterMotor_IsGroundedTest_m3567633754(__this, /*hidden argument*/NULL);
		if (!L_160)
		{
			goto IL_046d;
		}
	}
	{
		__this->set_grounded_10((bool)1);
		CharacterMotorJumping_t1708272304 * L_161 = __this->get_jumping_7();
		NullCheck(L_161);
		L_161->set_jumping_5((bool)0);
		Il2CppObject * L_162 = CharacterMotor_SubtractNewPlatformVelocity_m2006412010(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_162, /*hidden argument*/NULL);
		Component_SendMessage_m4199581575(__this, _stringLiteral487010350, 1, /*hidden argument*/NULL);
	}

IL_046d:
	{
		bool L_163 = CharacterMotor_MoveWithPlatform_m4019888974(__this, /*hidden argument*/NULL);
		if (!L_163)
		{
			goto IL_053b;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_164 = __this->get_movingPlatform_8();
		Transform_t3275118058 * L_165 = __this->get_tr_13();
		NullCheck(L_165);
		Vector3_t2243707580  L_166 = Transform_get_position_m1104419803(L_165, /*hidden argument*/NULL);
		Vector3_t2243707580  L_167 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterController_t4094781467 * L_168 = __this->get_controller_14();
		NullCheck(L_168);
		Vector3_t2243707580  L_169 = CharacterController_get_center_m3262687436(L_168, /*hidden argument*/NULL);
		V_14 = L_169;
		float L_170 = (&V_14)->get_y_2();
		CharacterController_t4094781467 * L_171 = __this->get_controller_14();
		NullCheck(L_171);
		float L_172 = CharacterController_get_height_m2830713110(L_171, /*hidden argument*/NULL);
		CharacterController_t4094781467 * L_173 = __this->get_controller_14();
		NullCheck(L_173);
		float L_174 = CharacterController_get_radius_m2751828411(L_173, /*hidden argument*/NULL);
		Vector3_t2243707580  L_175 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_167, ((float)((float)((float)((float)L_170-(float)((float)((float)L_172*(float)(0.5f)))))+(float)L_174)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_176 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_166, L_175, /*hidden argument*/NULL);
		NullCheck(L_164);
		L_164->set_activeGlobalPoint_5(L_176);
		CharacterMotorMovingPlatform_t365475463 * L_177 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_178 = __this->get_movingPlatform_8();
		NullCheck(L_178);
		Transform_t3275118058 * L_179 = L_178->get_activePlatform_3();
		CharacterMotorMovingPlatform_t365475463 * L_180 = __this->get_movingPlatform_8();
		NullCheck(L_180);
		Vector3_t2243707580  L_181 = L_180->get_activeGlobalPoint_5();
		NullCheck(L_179);
		Vector3_t2243707580  L_182 = Transform_InverseTransformPoint_m2648491174(L_179, L_181, /*hidden argument*/NULL);
		NullCheck(L_177);
		L_177->set_activeLocalPoint_4(L_182);
		CharacterMotorMovingPlatform_t365475463 * L_183 = __this->get_movingPlatform_8();
		Transform_t3275118058 * L_184 = __this->get_tr_13();
		NullCheck(L_184);
		Quaternion_t4030073918  L_185 = Transform_get_rotation_m1033555130(L_184, /*hidden argument*/NULL);
		NullCheck(L_183);
		L_183->set_activeGlobalRotation_7(L_185);
		CharacterMotorMovingPlatform_t365475463 * L_186 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_187 = __this->get_movingPlatform_8();
		NullCheck(L_187);
		Transform_t3275118058 * L_188 = L_187->get_activePlatform_3();
		NullCheck(L_188);
		Quaternion_t4030073918  L_189 = Transform_get_rotation_m1033555130(L_188, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_190 = Quaternion_Inverse_m3931399088(NULL /*static, unused*/, L_189, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t365475463 * L_191 = __this->get_movingPlatform_8();
		NullCheck(L_191);
		Quaternion_t4030073918  L_192 = L_191->get_activeGlobalRotation_7();
		Quaternion_t4030073918  L_193 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_190, L_192, /*hidden argument*/NULL);
		NullCheck(L_186);
		L_186->set_activeLocalRotation_6(L_193);
	}

IL_053b:
	{
		return;
	}
}
// System.Void CharacterMotor::FixedUpdate()
extern "C"  void CharacterMotor_FixedUpdate_m1016746259 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_FixedUpdate_m1016746259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		CharacterMotorMovingPlatform_t365475463 * L_0 = __this->get_movingPlatform_8();
		NullCheck(L_0);
		bool L_1 = L_0->get_enabled_0();
		if (!L_1)
		{
			goto IL_00d6;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_2 = __this->get_movingPlatform_8();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = L_2->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00c6;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_5 = __this->get_movingPlatform_8();
		NullCheck(L_5);
		bool L_6 = L_5->get_newPlatform_10();
		if (L_6)
		{
			goto IL_009a;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_7 = __this->get_movingPlatform_8();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = L_7->get_platformVelocity_9();
		V_0 = L_8;
		CharacterMotorMovingPlatform_t365475463 * L_9 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_10 = __this->get_movingPlatform_8();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = L_10->get_activePlatform_3();
		NullCheck(L_11);
		Matrix4x4_t2933234003  L_12 = Transform_get_localToWorldMatrix_m2868579006(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		CharacterMotorMovingPlatform_t365475463 * L_13 = __this->get_movingPlatform_8();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = L_13->get_activeLocalPoint_4();
		Vector3_t2243707580  L_15 = Matrix4x4_MultiplyPoint3x4_m1007952212((&V_1), L_14, /*hidden argument*/NULL);
		CharacterMotorMovingPlatform_t365475463 * L_16 = __this->get_movingPlatform_8();
		NullCheck(L_16);
		Matrix4x4_t2933234003 * L_17 = L_16->get_address_of_lastMatrix_8();
		CharacterMotorMovingPlatform_t365475463 * L_18 = __this->get_movingPlatform_8();
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = L_18->get_activeLocalPoint_4();
		Vector3_t2243707580  L_20 = Matrix4x4_MultiplyPoint3x4_m1007952212(L_17, L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_15, L_20, /*hidden argument*/NULL);
		float L_22 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_platformVelocity_9(L_23);
	}

IL_009a:
	{
		CharacterMotorMovingPlatform_t365475463 * L_24 = __this->get_movingPlatform_8();
		CharacterMotorMovingPlatform_t365475463 * L_25 = __this->get_movingPlatform_8();
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = L_25->get_activePlatform_3();
		NullCheck(L_26);
		Matrix4x4_t2933234003  L_27 = Transform_get_localToWorldMatrix_m2868579006(L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_lastMatrix_8(L_27);
		CharacterMotorMovingPlatform_t365475463 * L_28 = __this->get_movingPlatform_8();
		NullCheck(L_28);
		L_28->set_newPlatform_10((bool)0);
		goto IL_00d6;
	}

IL_00c6:
	{
		CharacterMotorMovingPlatform_t365475463 * L_29 = __this->get_movingPlatform_8();
		Vector3_t2243707580  L_30 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_platformVelocity_9(L_30);
	}

IL_00d6:
	{
		bool L_31 = __this->get_useFixedUpdate_3();
		if (!L_31)
		{
			goto IL_00e7;
		}
	}
	{
		CharacterMotor_UpdateFunction_m2269033867(__this, /*hidden argument*/NULL);
	}

IL_00e7:
	{
		return;
	}
}
// System.Void CharacterMotor::Update()
extern "C"  void CharacterMotor_Update_m3811321445 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useFixedUpdate_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		CharacterMotor_UpdateFunction_m2269033867(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// UnityEngine.Vector3 CharacterMotor::ApplyInputVelocityChange(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CharacterMotor_ApplyInputVelocityChange_m4116974862 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_ApplyInputVelocityChange_m4116974862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = __this->get_canControl_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputMoveDirection_4(L_1);
	}

IL_0016:
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		bool L_2 = __this->get_grounded_10();
		if (!L_2)
		{
			goto IL_00b8;
		}
	}
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		if (!L_3)
		{
			goto IL_00b8;
		}
	}
	{
		Vector3_t2243707580 * L_4 = __this->get_address_of_groundNormal_11();
		float L_5 = L_4->get_x_1();
		Vector3_t2243707580 * L_6 = __this->get_address_of_groundNormal_11();
		float L_7 = L_6->get_z_3();
		Vector3_t2243707580  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m2638739322(&L_8, L_5, (((float)((float)0))), L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		Vector3_t2243707580  L_9 = Vector3_get_normalized_m936072361((&V_4), /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_t2243707580  L_10 = __this->get_inputMoveDirection_4();
		Vector3_t2243707580  L_11 = V_0;
		Vector3_t2243707580  L_12 = Vector3_Project_m1396027688(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Vector3_t2243707580  L_13 = V_0;
		Vector3_t2243707580  L_14 = V_1;
		CharacterMotorSliding_t3749388514 * L_15 = __this->get_sliding_9();
		NullCheck(L_15);
		float L_16 = L_15->get_speedControl_3();
		Vector3_t2243707580  L_17 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = __this->get_inputMoveDirection_4();
		Vector3_t2243707580  L_20 = V_1;
		Vector3_t2243707580  L_21 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		CharacterMotorSliding_t3749388514 * L_22 = __this->get_sliding_9();
		NullCheck(L_22);
		float L_23 = L_22->get_sidewaysControl_2();
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_18, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		Vector3_t2243707580  L_26 = V_0;
		CharacterMotorSliding_t3749388514 * L_27 = __this->get_sliding_9();
		NullCheck(L_27);
		float L_28 = L_27->get_slidingSpeed_1();
		Vector3_t2243707580  L_29 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		goto IL_00bf;
	}

IL_00b8:
	{
		Vector3_t2243707580  L_30 = CharacterMotor_GetDesiredHorizontalVelocity_m555853785(__this, /*hidden argument*/NULL);
		V_0 = L_30;
	}

IL_00bf:
	{
		CharacterMotorMovingPlatform_t365475463 * L_31 = __this->get_movingPlatform_8();
		NullCheck(L_31);
		bool L_32 = L_31->get_enabled_0();
		if (!L_32)
		{
			goto IL_00fb;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_33 = __this->get_movingPlatform_8();
		NullCheck(L_33);
		int32_t L_34 = L_33->get_movementTransfer_1();
		if ((!(((uint32_t)L_34) == ((uint32_t)2))))
		{
			goto IL_00fb;
		}
	}
	{
		Vector3_t2243707580  L_35 = V_0;
		CharacterMotorMovement_t944153967 * L_36 = __this->get_movement_6();
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = L_36->get_frameVelocity_10();
		Vector3_t2243707580  L_38 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
		V_0 = L_38;
		(&V_0)->set_y_2((((float)((float)0))));
	}

IL_00fb:
	{
		bool L_39 = __this->get_grounded_10();
		if (!L_39)
		{
			goto IL_0119;
		}
	}
	{
		Vector3_t2243707580  L_40 = V_0;
		Vector3_t2243707580  L_41 = __this->get_groundNormal_11();
		Vector3_t2243707580  L_42 = CharacterMotor_AdjustGroundVelocityToNormal_m3451990381(__this, L_40, L_41, /*hidden argument*/NULL);
		V_0 = L_42;
		goto IL_0126;
	}

IL_0119:
	{
		(&___velocity0)->set_y_2((((float)((float)0))));
	}

IL_0126:
	{
		bool L_43 = __this->get_grounded_10();
		float L_44 = VirtFuncInvoker1< float, bool >::Invoke(8 /* System.Single CharacterMotor::GetMaxAcceleration(System.Boolean) */, __this, L_43);
		float L_45 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_44*(float)L_45));
		Vector3_t2243707580  L_46 = V_0;
		Vector3_t2243707580  L_47 = ___velocity0;
		Vector3_t2243707580  L_48 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		float L_49 = Vector3_get_sqrMagnitude_m1814096310((&V_3), /*hidden argument*/NULL);
		float L_50 = V_2;
		float L_51 = V_2;
		if ((((float)L_49) <= ((float)((float)((float)L_50*(float)L_51)))))
		{
			goto IL_015e;
		}
	}
	{
		Vector3_t2243707580  L_52 = Vector3_get_normalized_m936072361((&V_3), /*hidden argument*/NULL);
		float L_53 = V_2;
		Vector3_t2243707580  L_54 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		V_3 = L_54;
	}

IL_015e:
	{
		bool L_55 = __this->get_grounded_10();
		if (L_55)
		{
			goto IL_0174;
		}
	}
	{
		bool L_56 = __this->get_canControl_2();
		if (!L_56)
		{
			goto IL_0181;
		}
	}

IL_0174:
	{
		Vector3_t2243707580  L_57 = ___velocity0;
		Vector3_t2243707580  L_58 = V_3;
		Vector3_t2243707580  L_59 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		___velocity0 = L_59;
	}

IL_0181:
	{
		bool L_60 = __this->get_grounded_10();
		if (!L_60)
		{
			goto IL_01a9;
		}
	}
	{
		float L_61 = (&___velocity0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_62 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_61, (((float)((float)0))), /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(L_62);
	}

IL_01a9:
	{
		Vector3_t2243707580  L_63 = ___velocity0;
		return L_63;
	}
}
// UnityEngine.Vector3 CharacterMotor::ApplyGravityAndJumping(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CharacterMotor_ApplyGravityAndJumping_m3598474464 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_ApplyGravityAndJumping_m3598474464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_inputJump_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_canControl_2();
		if (L_1)
		{
			goto IL_0030;
		}
	}

IL_0016:
	{
		CharacterMotorJumping_t1708272304 * L_2 = __this->get_jumping_7();
		NullCheck(L_2);
		L_2->set_holdingJumpButton_6((bool)0);
		CharacterMotorJumping_t1708272304 * L_3 = __this->get_jumping_7();
		NullCheck(L_3);
		L_3->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
	}

IL_0030:
	{
		bool L_4 = __this->get_inputJump_5();
		if (!L_4)
		{
			goto IL_0068;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_5 = __this->get_jumping_7();
		NullCheck(L_5);
		float L_6 = L_5->get_lastButtonDownTime_8();
		if ((((float)L_6) >= ((float)(((float)((float)0))))))
		{
			goto IL_0068;
		}
	}
	{
		bool L_7 = __this->get_canControl_2();
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_8 = __this->get_jumping_7();
		float L_9 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_lastButtonDownTime_8(L_9);
	}

IL_0068:
	{
		bool L_10 = __this->get_grounded_10();
		if (!L_10)
		{
			goto IL_00a7;
		}
	}
	{
		float L_11 = (&___velocity0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)0))), L_11, /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_13 = __this->get_movement_6();
		NullCheck(L_13);
		float L_14 = L_13->get_gravity_6();
		float L_15 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(((float)((float)L_12-(float)((float)((float)L_14*(float)L_15)))));
		goto IL_017f;
	}

IL_00a7:
	{
		CharacterMotorMovement_t944153967 * L_16 = __this->get_movement_6();
		NullCheck(L_16);
		Vector3_t2243707580 * L_17 = L_16->get_address_of_velocity_9();
		float L_18 = L_17->get_y_2();
		CharacterMotorMovement_t944153967 * L_19 = __this->get_movement_6();
		NullCheck(L_19);
		float L_20 = L_19->get_gravity_6();
		float L_21 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(((float)((float)L_18-(float)((float)((float)L_20*(float)L_21)))));
		CharacterMotorJumping_t1708272304 * L_22 = __this->get_jumping_7();
		NullCheck(L_22);
		bool L_23 = L_22->get_jumping_5();
		if (!L_23)
		{
			goto IL_0158;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_24 = __this->get_jumping_7();
		NullCheck(L_24);
		bool L_25 = L_24->get_holdingJumpButton_6();
		if (!L_25)
		{
			goto IL_0158;
		}
	}
	{
		float L_26 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterMotorJumping_t1708272304 * L_27 = __this->get_jumping_7();
		NullCheck(L_27);
		float L_28 = L_27->get_lastStartTime_7();
		CharacterMotorJumping_t1708272304 * L_29 = __this->get_jumping_7();
		NullCheck(L_29);
		float L_30 = L_29->get_extraHeight_2();
		CharacterMotorJumping_t1708272304 * L_31 = __this->get_jumping_7();
		NullCheck(L_31);
		float L_32 = L_31->get_baseHeight_1();
		float L_33 = VirtFuncInvoker1< float, float >::Invoke(9 /* System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single) */, __this, L_32);
		if ((((float)L_26) >= ((float)((float)((float)L_28+(float)((float)((float)L_30/(float)L_33)))))))
		{
			goto IL_0158;
		}
	}
	{
		Vector3_t2243707580  L_34 = ___velocity0;
		CharacterMotorJumping_t1708272304 * L_35 = __this->get_jumping_7();
		NullCheck(L_35);
		Vector3_t2243707580  L_36 = L_35->get_jumpDir_9();
		CharacterMotorMovement_t944153967 * L_37 = __this->get_movement_6();
		NullCheck(L_37);
		float L_38 = L_37->get_gravity_6();
		Vector3_t2243707580  L_39 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		float L_40 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_34, L_41, /*hidden argument*/NULL);
		___velocity0 = L_42;
	}

IL_0158:
	{
		float L_43 = (&___velocity0)->get_y_2();
		CharacterMotorMovement_t944153967 * L_44 = __this->get_movement_6();
		NullCheck(L_44);
		float L_45 = L_44->get_maxFallSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_46 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_43, ((-L_45)), /*hidden argument*/NULL);
		(&___velocity0)->set_y_2(L_46);
	}

IL_017f:
	{
		bool L_47 = __this->get_grounded_10();
		if (!L_47)
		{
			goto IL_030f;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_48 = __this->get_jumping_7();
		NullCheck(L_48);
		bool L_49 = L_48->get_enabled_0();
		if (!L_49)
		{
			goto IL_0303;
		}
	}
	{
		bool L_50 = __this->get_canControl_2();
		if (!L_50)
		{
			goto IL_0303;
		}
	}
	{
		float L_51 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharacterMotorJumping_t1708272304 * L_52 = __this->get_jumping_7();
		NullCheck(L_52);
		float L_53 = L_52->get_lastButtonDownTime_8();
		if ((((float)((float)((float)L_51-(float)L_53))) >= ((float)(0.2f))))
		{
			goto IL_0303;
		}
	}
	{
		__this->set_grounded_10((bool)0);
		CharacterMotorJumping_t1708272304 * L_54 = __this->get_jumping_7();
		NullCheck(L_54);
		L_54->set_jumping_5((bool)1);
		CharacterMotorJumping_t1708272304 * L_55 = __this->get_jumping_7();
		float L_56 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_55);
		L_55->set_lastStartTime_7(L_56);
		CharacterMotorJumping_t1708272304 * L_57 = __this->get_jumping_7();
		NullCheck(L_57);
		L_57->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
		CharacterMotorJumping_t1708272304 * L_58 = __this->get_jumping_7();
		NullCheck(L_58);
		L_58->set_holdingJumpButton_6((bool)1);
		bool L_59 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		if (!L_59)
		{
			goto IL_0233;
		}
	}
	{
		CharacterMotorJumping_t1708272304 * L_60 = __this->get_jumping_7();
		Vector3_t2243707580  L_61 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_62 = __this->get_groundNormal_11();
		CharacterMotorJumping_t1708272304 * L_63 = __this->get_jumping_7();
		NullCheck(L_63);
		float L_64 = L_63->get_steepPerpAmount_4();
		Vector3_t2243707580  L_65 = Vector3_Slerp_m846771032(NULL /*static, unused*/, L_61, L_62, L_64, /*hidden argument*/NULL);
		NullCheck(L_60);
		L_60->set_jumpDir_9(L_65);
		goto IL_0259;
	}

IL_0233:
	{
		CharacterMotorJumping_t1708272304 * L_66 = __this->get_jumping_7();
		Vector3_t2243707580  L_67 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_68 = __this->get_groundNormal_11();
		CharacterMotorJumping_t1708272304 * L_69 = __this->get_jumping_7();
		NullCheck(L_69);
		float L_70 = L_69->get_perpAmount_3();
		Vector3_t2243707580  L_71 = Vector3_Slerp_m846771032(NULL /*static, unused*/, L_67, L_68, L_70, /*hidden argument*/NULL);
		NullCheck(L_66);
		L_66->set_jumpDir_9(L_71);
	}

IL_0259:
	{
		(&___velocity0)->set_y_2((((float)((float)0))));
		Vector3_t2243707580  L_72 = ___velocity0;
		CharacterMotorJumping_t1708272304 * L_73 = __this->get_jumping_7();
		NullCheck(L_73);
		Vector3_t2243707580  L_74 = L_73->get_jumpDir_9();
		CharacterMotorJumping_t1708272304 * L_75 = __this->get_jumping_7();
		NullCheck(L_75);
		float L_76 = L_75->get_baseHeight_1();
		float L_77 = VirtFuncInvoker1< float, float >::Invoke(9 /* System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single) */, __this, L_76);
		Vector3_t2243707580  L_78 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_74, L_77, /*hidden argument*/NULL);
		Vector3_t2243707580  L_79 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_72, L_78, /*hidden argument*/NULL);
		___velocity0 = L_79;
		CharacterMotorMovingPlatform_t365475463 * L_80 = __this->get_movingPlatform_8();
		NullCheck(L_80);
		bool L_81 = L_80->get_enabled_0();
		if (!L_81)
		{
			goto IL_02f2;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_82 = __this->get_movingPlatform_8();
		NullCheck(L_82);
		int32_t L_83 = L_82->get_movementTransfer_1();
		if ((((int32_t)L_83) == ((int32_t)1)))
		{
			goto IL_02c5;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_84 = __this->get_movingPlatform_8();
		NullCheck(L_84);
		int32_t L_85 = L_84->get_movementTransfer_1();
		if ((!(((uint32_t)L_85) == ((uint32_t)2))))
		{
			goto IL_02f2;
		}
	}

IL_02c5:
	{
		CharacterMotorMovement_t944153967 * L_86 = __this->get_movement_6();
		CharacterMotorMovingPlatform_t365475463 * L_87 = __this->get_movingPlatform_8();
		NullCheck(L_87);
		Vector3_t2243707580  L_88 = L_87->get_platformVelocity_9();
		NullCheck(L_86);
		L_86->set_frameVelocity_10(L_88);
		Vector3_t2243707580  L_89 = ___velocity0;
		CharacterMotorMovingPlatform_t365475463 * L_90 = __this->get_movingPlatform_8();
		NullCheck(L_90);
		Vector3_t2243707580  L_91 = L_90->get_platformVelocity_9();
		Vector3_t2243707580  L_92 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_89, L_91, /*hidden argument*/NULL);
		___velocity0 = L_92;
	}

IL_02f2:
	{
		Component_SendMessage_m4199581575(__this, _stringLiteral2053352307, 1, /*hidden argument*/NULL);
		goto IL_030f;
	}

IL_0303:
	{
		CharacterMotorJumping_t1708272304 * L_93 = __this->get_jumping_7();
		NullCheck(L_93);
		L_93->set_holdingJumpButton_6((bool)0);
	}

IL_030f:
	{
		Vector3_t2243707580  L_94 = ___velocity0;
		return L_94;
	}
}
// System.Void CharacterMotor::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void CharacterMotor_OnControllerColliderHit_m3731083134 (CharacterMotor_t262030084 * __this, ControllerColliderHit_t4070855101 * ___hit0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		ControllerColliderHit_t4070855101 * L_0 = ___hit0;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = ControllerColliderHit_get_normal_m1098215280(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		if ((((float)L_2) <= ((float)(((float)((float)0))))))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t4070855101 * L_3 = ___hit0;
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = ControllerColliderHit_get_normal_m1098215280(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		Vector3_t2243707580 * L_6 = __this->get_address_of_groundNormal_11();
		float L_7 = L_6->get_y_2();
		if ((((float)L_5) <= ((float)L_7)))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t4070855101 * L_8 = ___hit0;
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = ControllerColliderHit_get_moveDirection_m3053186297(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_y_2();
		if ((((float)L_10) >= ((float)(((float)((float)0))))))
		{
			goto IL_00d9;
		}
	}
	{
		ControllerColliderHit_t4070855101 * L_11 = ___hit0;
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = ControllerColliderHit_get_point_m3573703281(L_11, /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_13 = __this->get_movement_6();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = L_13->get_lastHitPoint_12();
		Vector3_t2243707580  L_15 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = Vector3_get_sqrMagnitude_m1814096310((&V_3), /*hidden argument*/NULL);
		if ((((float)L_16) > ((float)(0.001f))))
		{
			goto IL_0085;
		}
	}
	{
		Vector3_t2243707580  L_17 = __this->get_lastGroundNormal_12();
		Vector3_t2243707580  L_18 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0096;
		}
	}

IL_0085:
	{
		ControllerColliderHit_t4070855101 * L_20 = ___hit0;
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = ControllerColliderHit_get_normal_m1098215280(L_20, /*hidden argument*/NULL);
		__this->set_groundNormal_11(L_21);
		goto IL_00a2;
	}

IL_0096:
	{
		Vector3_t2243707580  L_22 = __this->get_lastGroundNormal_12();
		__this->set_groundNormal_11(L_22);
	}

IL_00a2:
	{
		CharacterMotorMovingPlatform_t365475463 * L_23 = __this->get_movingPlatform_8();
		ControllerColliderHit_t4070855101 * L_24 = ___hit0;
		NullCheck(L_24);
		Collider_t3497673348 * L_25 = ControllerColliderHit_get_collider_m3897495767(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		L_23->set_hitPlatform_2(L_26);
		CharacterMotorMovement_t944153967 * L_27 = __this->get_movement_6();
		ControllerColliderHit_t4070855101 * L_28 = ___hit0;
		NullCheck(L_28);
		Vector3_t2243707580  L_29 = ControllerColliderHit_get_point_m3573703281(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		L_27->set_hitPoint_11(L_29);
		CharacterMotorMovement_t944153967 * L_30 = __this->get_movement_6();
		Vector3_t2243707580  L_31 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		L_30->set_frameVelocity_10(L_31);
	}

IL_00d9:
	{
		return;
	}
}
// System.Collections.IEnumerator CharacterMotor::SubtractNewPlatformVelocity()
extern "C"  Il2CppObject * CharacterMotor_SubtractNewPlatformVelocity_m2006412010 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_SubtractNewPlatformVelocity_m2006412010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U24SubtractNewPlatformVelocityU2417_t1999783645 * L_0 = (U24SubtractNewPlatformVelocityU2417_t1999783645 *)il2cpp_codegen_object_new(U24SubtractNewPlatformVelocityU2417_t1999783645_il2cpp_TypeInfo_var);
		U24SubtractNewPlatformVelocityU2417__ctor_m1768541611(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24SubtractNewPlatformVelocityU2417_GetEnumerator_m2985528835(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean CharacterMotor::MoveWithPlatform()
extern "C"  bool CharacterMotor_MoveWithPlatform_m4019888974 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_MoveWithPlatform_m4019888974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	bool G_B1_0 = false;
	bool G_B2_0 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	{
		CharacterMotorMovingPlatform_t365475463 * L_0 = __this->get_movingPlatform_8();
		NullCheck(L_0);
		bool L_1 = L_0->get_enabled_0();
		bool L_2 = L_1;
		G_B1_0 = L_2;
		if (!L_2)
		{
			G_B3_0 = ((int32_t)(L_2));
			goto IL_002d;
		}
	}
	{
		bool L_3 = __this->get_grounded_10();
		bool L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = ((int32_t)(L_4));
			goto IL_002d;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_5 = __this->get_movingPlatform_8();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_movementTransfer_1();
		G_B3_0 = ((((int32_t)L_6) == ((int32_t)3))? 1 : 0);
	}

IL_002d:
	{
		int32_t L_7 = G_B3_0;
		G_B4_0 = L_7;
		if (!L_7)
		{
			G_B5_0 = L_7;
			goto IL_0045;
		}
	}
	{
		CharacterMotorMovingPlatform_t365475463 * L_8 = __this->get_movingPlatform_8();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = L_8->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_10));
	}

IL_0045:
	{
		return (bool)G_B5_0;
	}
}
// UnityEngine.Vector3 CharacterMotor::GetDesiredHorizontalVelocity()
extern "C"  Vector3_t2243707580  CharacterMotor_GetDesiredHorizontalVelocity_m555853785 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_GetDesiredHorizontalVelocity_m555853785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Transform_t3275118058 * L_0 = __this->get_tr_13();
		Vector3_t2243707580  L_1 = __this->get_inputMoveDirection_4();
		NullCheck(L_0);
		Vector3_t2243707580  L_2 = Transform_InverseTransformDirection_m3595190459(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		float L_4 = VirtFuncInvoker1< float, Vector3_t2243707580  >::Invoke(17 /* System.Single CharacterMotor::MaxSpeedInDirection(UnityEngine.Vector3) */, __this, L_3);
		V_1 = L_4;
		bool L_5 = __this->get_grounded_10();
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_6 = __this->get_movement_6();
		NullCheck(L_6);
		Vector3_t2243707580 * L_7 = L_6->get_address_of_velocity_9();
		Vector3_t2243707580  L_8 = Vector3_get_normalized_m936072361(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = (&V_3)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = asinf(L_9);
		V_2 = ((float)((float)L_10*(float)(57.29578f)));
		float L_11 = V_1;
		CharacterMotorMovement_t944153967 * L_12 = __this->get_movement_6();
		NullCheck(L_12);
		AnimationCurve_t3306541151 * L_13 = L_12->get_slopeSpeedMultiplier_3();
		float L_14 = V_2;
		NullCheck(L_13);
		float L_15 = AnimationCurve_Evaluate_m3698879322(L_13, L_14, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_11*(float)L_15));
	}

IL_005d:
	{
		Transform_t3275118058 * L_16 = __this->get_tr_13();
		Vector3_t2243707580  L_17 = V_0;
		float L_18 = V_1;
		Vector3_t2243707580  L_19 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t2243707580  L_20 = Transform_TransformDirection_m1639585047(L_16, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// UnityEngine.Vector3 CharacterMotor::AdjustGroundVelocityToNormal(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  CharacterMotor_AdjustGroundVelocityToNormal_m3451990381 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___hVelocity0, Vector3_t2243707580  ___groundNormal1, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = ___hVelocity0;
		Vector3_t2243707580  L_2 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = ___groundNormal1;
		Vector3_t2243707580  L_5 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t2243707580  L_6 = Vector3_get_normalized_m936072361((&V_1), /*hidden argument*/NULL);
		float L_7 = Vector3_get_magnitude_m860342598((&___hVelocity0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean CharacterMotor::IsGroundedTest()
extern "C"  bool CharacterMotor_IsGroundedTest_m3567633754 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_groundNormal_11();
		float L_1 = L_0->get_y_2();
		return (bool)((((float)L_1) > ((float)(0.01f)))? 1 : 0);
	}
}
// System.Single CharacterMotor::GetMaxAcceleration(System.Boolean)
extern "C"  float CharacterMotor_GetMaxAcceleration_m1364223961 (CharacterMotor_t262030084 * __this, bool ___grounded0, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___grounded0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_1 = __this->get_movement_6();
		NullCheck(L_1);
		float L_2 = L_1->get_maxGroundAcceleration_4();
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_0016:
	{
		CharacterMotorMovement_t944153967 * L_3 = __this->get_movement_6();
		NullCheck(L_3);
		float L_4 = L_3->get_maxAirAcceleration_5();
		G_B3_0 = L_4;
		goto IL_0026;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Single CharacterMotor::CalculateJumpVerticalSpeed(System.Single)
extern "C"  float CharacterMotor_CalculateJumpVerticalSpeed_m2495405676 (CharacterMotor_t262030084 * __this, float ___targetJumpHeight0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_CalculateJumpVerticalSpeed_m2495405676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___targetJumpHeight0;
		CharacterMotorMovement_t944153967 * L_1 = __this->get_movement_6();
		NullCheck(L_1);
		float L_2 = L_1->get_gravity_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = sqrtf(((float)((float)((float)((float)(((float)((float)2)))*(float)L_0))*(float)L_2)));
		return L_3;
	}
}
// System.Boolean CharacterMotor::IsJumping()
extern "C"  bool CharacterMotor_IsJumping_m149011362 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		CharacterMotorJumping_t1708272304 * L_0 = __this->get_jumping_7();
		NullCheck(L_0);
		bool L_1 = L_0->get_jumping_5();
		return L_1;
	}
}
// System.Boolean CharacterMotor::IsSliding()
extern "C"  bool CharacterMotor_IsSliding_m1686387796 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	bool G_B2_0 = false;
	bool G_B1_0 = false;
	bool G_B4_0 = false;
	bool G_B3_0 = false;
	{
		bool L_0 = __this->get_grounded_10();
		bool L_1 = L_0;
		G_B1_0 = L_1;
		if (!L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		CharacterMotorSliding_t3749388514 * L_2 = __this->get_sliding_9();
		NullCheck(L_2);
		bool L_3 = L_2->get_enabled_0();
		G_B2_0 = L_3;
	}

IL_0018:
	{
		bool L_4 = G_B2_0;
		G_B3_0 = L_4;
		if (!L_4)
		{
			G_B4_0 = L_4;
			goto IL_0025;
		}
	}
	{
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean CharacterMotor::TooSteep() */, __this);
		G_B4_0 = L_5;
	}

IL_0025:
	{
		return G_B4_0;
	}
}
// System.Boolean CharacterMotor::IsTouchingCeiling()
extern "C"  bool CharacterMotor_IsTouchingCeiling_m996322722 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		CharacterMotorMovement_t944153967 * L_0 = __this->get_movement_6();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_collisionFlags_8();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_1&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean CharacterMotor::IsGrounded()
extern "C"  bool CharacterMotor_IsGrounded_m2399482598 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_grounded_10();
		return L_0;
	}
}
// System.Boolean CharacterMotor::TooSteep()
extern "C"  bool CharacterMotor_TooSteep_m4157442919 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_TooSteep_m4157442919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_groundNormal_11();
		float L_1 = L_0->get_y_2();
		CharacterController_t4094781467 * L_2 = __this->get_controller_14();
		NullCheck(L_2);
		float L_3 = CharacterController_get_slopeLimit_m1930597355(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = cosf(((float)((float)L_3*(float)(0.0174532924f))));
		return (bool)((((int32_t)((((float)L_1) > ((float)L_4))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector3 CharacterMotor::GetDirection()
extern "C"  Vector3_t2243707580  CharacterMotor_GetDirection_m1937069325 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_inputMoveDirection_4();
		return L_0;
	}
}
// System.Void CharacterMotor::SetControllable(System.Boolean)
extern "C"  void CharacterMotor_SetControllable_m3986683404 (CharacterMotor_t262030084 * __this, bool ___controllable0, const MethodInfo* method)
{
	{
		bool L_0 = ___controllable0;
		__this->set_canControl_2(L_0);
		return;
	}
}
// System.Single CharacterMotor::MaxSpeedInDirection(UnityEngine.Vector3)
extern "C"  float CharacterMotor_MaxSpeedInDirection_m1344582432 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___desiredMovementDirection0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float G_B6_0 = 0.0f;
	float G_B5_0 = 0.0f;
	{
		Vector3_t2243707580  L_0 = ___desiredMovementDirection0;
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B6_0 = (((float)((float)0)));
		goto IL_00ac;
	}

IL_0017:
	{
		float L_3 = (&___desiredMovementDirection0)->get_z_3();
		if ((((float)L_3) <= ((float)(((float)((float)0))))))
		{
			goto IL_0039;
		}
	}
	{
		CharacterMotorMovement_t944153967 * L_4 = __this->get_movement_6();
		NullCheck(L_4);
		float L_5 = L_4->get_maxForwardSpeed_0();
		G_B5_0 = L_5;
		goto IL_0044;
	}

IL_0039:
	{
		CharacterMotorMovement_t944153967 * L_6 = __this->get_movement_6();
		NullCheck(L_6);
		float L_7 = L_6->get_maxBackwardsSpeed_2();
		G_B5_0 = L_7;
	}

IL_0044:
	{
		CharacterMotorMovement_t944153967 * L_8 = __this->get_movement_6();
		NullCheck(L_8);
		float L_9 = L_8->get_maxSidewaysSpeed_1();
		V_0 = ((float)((float)G_B5_0/(float)L_9));
		float L_10 = (&___desiredMovementDirection0)->get_x_1();
		float L_11 = (&___desiredMovementDirection0)->get_z_3();
		float L_12 = V_0;
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, L_10, (((float)((float)0))), ((float)((float)L_11/(float)L_12)), /*hidden argument*/NULL);
		V_3 = L_13;
		Vector3_t2243707580  L_14 = Vector3_get_normalized_m936072361((&V_3), /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		float L_16 = (&V_1)->get_z_3();
		float L_17 = V_0;
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322(&L_18, L_15, (((float)((float)0))), ((float)((float)L_16*(float)L_17)), /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = Vector3_get_magnitude_m860342598((&V_4), /*hidden argument*/NULL);
		CharacterMotorMovement_t944153967 * L_20 = __this->get_movement_6();
		NullCheck(L_20);
		float L_21 = L_20->get_maxSidewaysSpeed_1();
		V_2 = ((float)((float)L_19*(float)L_21));
		float L_22 = V_2;
		G_B6_0 = L_22;
		goto IL_00ac;
	}

IL_00ac:
	{
		return G_B6_0;
	}
}
// System.Void CharacterMotor::SetVelocity(UnityEngine.Vector3)
extern "C"  void CharacterMotor_SetVelocity_m1132550922 (CharacterMotor_t262030084 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotor_SetVelocity_m1132550922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_grounded_10((bool)0);
		CharacterMotorMovement_t944153967 * L_0 = __this->get_movement_6();
		Vector3_t2243707580  L_1 = ___velocity0;
		NullCheck(L_0);
		L_0->set_velocity_9(L_1);
		CharacterMotorMovement_t944153967 * L_2 = __this->get_movement_6();
		Vector3_t2243707580  L_3 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_frameVelocity_10(L_3);
		Component_SendMessage_m3615678587(__this, _stringLiteral2724755691, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterMotor::Main()
extern "C"  void CharacterMotor_Main_m2676787163 (CharacterMotor_t262030084 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$17::.ctor(CharacterMotor)
extern "C"  void U24SubtractNewPlatformVelocityU2417__ctor_m1768541611 (U24SubtractNewPlatformVelocityU2417_t1999783645 * __this, CharacterMotor_t262030084 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SubtractNewPlatformVelocityU2417__ctor_m1768541611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGenerator_1__ctor_m409073663(__this, /*hidden argument*/GenericGenerator_1__ctor_m409073663_MethodInfo_var);
		CharacterMotor_t262030084 * L_0 = ___self_0;
		__this->set_U24self_U2420_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> CharacterMotor/$SubtractNewPlatformVelocity$17::GetEnumerator()
extern "C"  Il2CppObject* U24SubtractNewPlatformVelocityU2417_GetEnumerator_m2985528835 (U24SubtractNewPlatformVelocityU2417_t1999783645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SubtractNewPlatformVelocityU2417_GetEnumerator_m2985528835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharacterMotor_t262030084 * L_0 = __this->get_U24self_U2420_0();
		U24_t2847579946 * L_1 = (U24_t2847579946 *)il2cpp_codegen_object_new(U24_t2847579946_il2cpp_TypeInfo_var);
		U24__ctor_m4034603524(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void CharacterMotor/$SubtractNewPlatformVelocity$17/$::.ctor(CharacterMotor)
extern "C"  void U24__ctor_m4034603524 (U24_t2847579946 * __this, CharacterMotor_t262030084 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m4034603524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m509214543(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m509214543_MethodInfo_var);
		CharacterMotor_t262030084 * L_0 = ___self_0;
		__this->set_U24self_U2419_3(L_0);
		return;
	}
}
// System.Boolean CharacterMotor/$SubtractNewPlatformVelocity$17/$::MoveNext()
extern "C"  bool U24_MoveNext_m333678758 (U24_t2847579946 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m333678758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B13_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t3445420457 *)__this)->get__state_1();
		switch (L_0)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_012c;
			}
			case 2:
			{
				goto IL_009c;
			}
			case 3:
			{
				goto IL_00ad;
			}
			case 4:
			{
				goto IL_00ef;
			}
		}
	}

IL_001f:
	{
		CharacterMotor_t262030084 * L_1 = __this->get_U24self_U2419_3();
		NullCheck(L_1);
		CharacterMotorMovingPlatform_t365475463 * L_2 = L_1->get_movingPlatform_8();
		NullCheck(L_2);
		bool L_3 = L_2->get_enabled_0();
		if (!L_3)
		{
			goto IL_0124;
		}
	}
	{
		CharacterMotor_t262030084 * L_4 = __this->get_U24self_U2419_3();
		NullCheck(L_4);
		CharacterMotorMovingPlatform_t365475463 * L_5 = L_4->get_movingPlatform_8();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_movementTransfer_1();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0060;
		}
	}
	{
		CharacterMotor_t262030084 * L_7 = __this->get_U24self_U2419_3();
		NullCheck(L_7);
		CharacterMotorMovingPlatform_t365475463 * L_8 = L_7->get_movingPlatform_8();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_movementTransfer_1();
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_0124;
		}
	}

IL_0060:
	{
		CharacterMotor_t262030084 * L_10 = __this->get_U24self_U2419_3();
		NullCheck(L_10);
		CharacterMotorMovingPlatform_t365475463 * L_11 = L_10->get_movingPlatform_8();
		NullCheck(L_11);
		bool L_12 = L_11->get_newPlatform_10();
		if (!L_12)
		{
			goto IL_00ef;
		}
	}
	{
		CharacterMotor_t262030084 * L_13 = __this->get_U24self_U2419_3();
		NullCheck(L_13);
		CharacterMotorMovingPlatform_t365475463 * L_14 = L_13->get_movingPlatform_8();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = L_14->get_activePlatform_3();
		__this->set_U24platformU2418_2(L_15);
		WaitForFixedUpdate_t3968615785 * L_16 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_16, /*hidden argument*/NULL);
		bool L_17 = GenericGeneratorEnumerator_1_Yield_m3421350270(__this, 2, L_16, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var);
		G_B13_0 = ((int32_t)(L_17));
		goto IL_012d;
	}

IL_009c:
	{
		WaitForFixedUpdate_t3968615785 * L_18 = (WaitForFixedUpdate_t3968615785 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t3968615785_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m3781413380(L_18, /*hidden argument*/NULL);
		bool L_19 = GenericGeneratorEnumerator_1_Yield_m3421350270(__this, 3, L_18, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var);
		G_B13_0 = ((int32_t)(L_19));
		goto IL_012d;
	}

IL_00ad:
	{
		CharacterMotor_t262030084 * L_20 = __this->get_U24self_U2419_3();
		NullCheck(L_20);
		bool L_21 = L_20->get_grounded_10();
		if (!L_21)
		{
			goto IL_00ef;
		}
	}
	{
		Transform_t3275118058 * L_22 = __this->get_U24platformU2418_2();
		CharacterMotor_t262030084 * L_23 = __this->get_U24self_U2419_3();
		NullCheck(L_23);
		CharacterMotorMovingPlatform_t365475463 * L_24 = L_23->get_movingPlatform_8();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = L_24->get_activePlatform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ef;
		}
	}
	{
		int32_t L_27 = 1;
		Il2CppObject * L_28 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_27);
		bool L_29 = GenericGeneratorEnumerator_1_Yield_m3421350270(__this, 4, L_28, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m3421350270_MethodInfo_var);
		G_B13_0 = ((int32_t)(L_29));
		goto IL_012d;
	}

IL_00ef:
	{
		CharacterMotor_t262030084 * L_30 = __this->get_U24self_U2419_3();
		NullCheck(L_30);
		CharacterMotorMovement_t944153967 * L_31 = L_30->get_movement_6();
		CharacterMotor_t262030084 * L_32 = __this->get_U24self_U2419_3();
		NullCheck(L_32);
		CharacterMotorMovement_t944153967 * L_33 = L_32->get_movement_6();
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = L_33->get_velocity_9();
		CharacterMotor_t262030084 * L_35 = __this->get_U24self_U2419_3();
		NullCheck(L_35);
		CharacterMotorMovingPlatform_t365475463 * L_36 = L_35->get_movingPlatform_8();
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = L_36->get_platformVelocity_9();
		Vector3_t2243707580  L_38 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_34, L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_velocity_9(L_38);
	}

IL_0124:
	{
		GenericGeneratorEnumerator_1_YieldDefault_m799132706(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m799132706_MethodInfo_var);
	}

IL_012c:
	{
		G_B13_0 = 0;
	}

IL_012d:
	{
		return (bool)G_B13_0;
	}
}
// System.Void CharacterMotorJumping::.ctor()
extern "C"  void CharacterMotorJumping__ctor_m1668297276 (CharacterMotorJumping_t1708272304 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_enabled_0((bool)1);
		__this->set_baseHeight_1((1.0f));
		__this->set_extraHeight_2((4.1f));
		__this->set_steepPerpAmount_4((0.5f));
		__this->set_lastButtonDownTime_8((((float)((float)((int32_t)-100)))));
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_jumpDir_9(L_0);
		return;
	}
}
// System.Void CharacterMotorMovement::.ctor()
extern "C"  void CharacterMotorMovement__ctor_m1502129899 (CharacterMotorMovement_t944153967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMotorMovement__ctor_m1502129899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_maxForwardSpeed_0((10.0f));
		__this->set_maxSidewaysSpeed_1((10.0f));
		__this->set_maxBackwardsSpeed_2((10.0f));
		KeyframeU5BU5D_t449065829* L_0 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		Keyframe_t1449471340  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m2042404667(&L_1, (((float)((float)((int32_t)-90)))), (((float)((float)1))), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		KeyframeU5BU5D_t449065829* L_2 = L_0;
		NullCheck(L_2);
		Keyframe_t1449471340  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m2042404667(&L_3, (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		KeyframeU5BU5D_t449065829* L_4 = L_2;
		NullCheck(L_4);
		Keyframe_t1449471340  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Keyframe__ctor_m2042404667(&L_5, (((float)((float)((int32_t)90)))), (((float)((float)0))), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		AnimationCurve_t3306541151 * L_6 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_6, L_4, /*hidden argument*/NULL);
		__this->set_slopeSpeedMultiplier_3(L_6);
		__this->set_maxGroundAcceleration_4((30.0f));
		__this->set_maxAirAcceleration_5((20.0f));
		__this->set_gravity_6((10.0f));
		__this->set_maxFallSpeed_7((20.0f));
		Vector3_t2243707580  L_7 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_frameVelocity_10(L_7);
		Vector3_t2243707580  L_8 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_hitPoint_11(L_8);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (std::numeric_limits<float>::infinity()), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_lastHitPoint_12(L_9);
		return;
	}
}
// System.Void CharacterMotorMovingPlatform::.ctor()
extern "C"  void CharacterMotorMovingPlatform__ctor_m3422518867 (CharacterMotorMovingPlatform_t365475463 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_enabled_0((bool)1);
		__this->set_movementTransfer_1(2);
		return;
	}
}
// System.Void CharacterMotorSliding::.ctor()
extern "C"  void CharacterMotorSliding__ctor_m2145873446 (CharacterMotorSliding_t3749388514 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_enabled_0((bool)1);
		__this->set_slidingSpeed_1((((float)((float)((int32_t)15)))));
		__this->set_sidewaysControl_2((1.0f));
		__this->set_speedControl_3((0.4f));
		return;
	}
}
// System.Void FPSInputController::.ctor()
extern "C"  void FPSInputController__ctor_m2375921577 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSInputController::Awake()
extern "C"  void FPSInputController_Awake_m1855081626 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FPSInputController_Awake_m1855081626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CharacterMotor_t262030084_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_1 = Component_GetComponent_m4225719715(__this, L_0, /*hidden argument*/NULL);
		__this->set_motor_2(((CharacterMotor_t262030084 *)CastclassClass(L_1, CharacterMotor_t262030084_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void FPSInputController::Update()
extern "C"  void FPSInputController_Update_m3258574412 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FPSInputController_Update_m3258574412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, L_0, (((float)((float)0))), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		float L_6 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		float L_8 = V_1;
		Vector3_t2243707580  L_9 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)1))), L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = V_1;
		float L_13 = V_1;
		V_1 = ((float)((float)L_12*(float)L_13));
		Vector3_t2243707580  L_14 = V_0;
		float L_15 = V_1;
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0051:
	{
		CharacterMotor_t262030084 * L_17 = __this->get_motor_2();
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t4030073918  L_19 = Transform_get_rotation_m1033555130(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = V_0;
		Vector3_t2243707580  L_21 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_inputMoveDirection_4(L_21);
		CharacterMotor_t262030084 * L_22 = __this->get_motor_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_23 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_inputJump_5(L_23);
		return;
	}
}
// System.Void FPSInputController::Main()
extern "C"  void FPSInputController_Main_m1154765804 (FPSInputController_t4241249601 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MouseOrbit::.ctor()
extern "C"  void MouseOrbit__ctor_m1217624273 (MouseOrbit_t143029581 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_distance_3((10.0f));
		__this->set_xSpeed_4((250.0f));
		__this->set_ySpeed_5((120.0f));
		__this->set_yMinLimit_6(((int32_t)-20));
		__this->set_yMaxLimit_7(((int32_t)80));
		return;
	}
}
// System.Void MouseOrbit::Start()
extern "C"  void MouseOrbit_Start_m1629475745 (MouseOrbit_t143029581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseOrbit_Start_m1629475745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_eulerAngles_m4066505159(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		__this->set_x_8(L_2);
		float L_3 = (&V_0)->get_x_1();
		__this->set_y_9(L_3);
		Rigidbody_t4233889191 * L_4 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		Rigidbody_t4233889191 * L_6 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_MethodInfo_var);
		NullCheck(L_6);
		Rigidbody_set_freezeRotation_m2131864169(L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void MouseOrbit::LateUpdate()
extern "C"  void MouseOrbit_LateUpdate_m536119640 (MouseOrbit_t143029581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseOrbit_LateUpdate_m536119640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3275118058 * L_0 = __this->get_target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0121;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0121;
		}
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		if ((((float)L_5) >= ((float)(((float)((float)((int32_t)250)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_6 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = (&V_0)->get_y_2();
		if ((((float)((float)((float)(((float)((float)L_6)))-(float)L_7))) >= ((float)(((float)((float)((int32_t)340)))))))
		{
			goto IL_005c;
		}
	}
	{
		goto IL_0127;
	}

IL_005c:
	{
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		float L_8 = __this->get_x_8();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_9 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_10 = __this->get_xSpeed_4();
		__this->set_x_8(((float)((float)L_8+(float)((float)((float)((float)((float)L_9*(float)L_10))*(float)(0.02f))))));
		float L_11 = __this->get_y_9();
		float L_12 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_13 = __this->get_ySpeed_5();
		__this->set_y_9(((float)((float)L_11-(float)((float)((float)((float)((float)L_12*(float)L_13))*(float)(0.02f))))));
		float L_14 = __this->get_y_9();
		int32_t L_15 = __this->get_yMinLimit_6();
		int32_t L_16 = __this->get_yMaxLimit_7();
		float L_17 = MouseOrbit_ClampAngle_m4104644686(NULL /*static, unused*/, L_14, (((float)((float)L_15))), (((float)((float)L_16))), /*hidden argument*/NULL);
		__this->set_y_9(L_17);
		float L_18 = __this->get_y_9();
		float L_19 = __this->get_x_8();
		Quaternion_t4030073918  L_20 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_18, L_19, (((float)((float)0))), /*hidden argument*/NULL);
		V_1 = L_20;
		Quaternion_t4030073918  L_21 = V_1;
		float L_22 = __this->get_distance_3();
		Vector3_t2243707580  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m2638739322(&L_23, (((float)((float)0))), (((float)((float)0))), ((-L_22)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_24 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Transform_t3275118058 * L_25 = __this->get_target_2();
		NullCheck(L_25);
		Vector3_t2243707580  L_26 = Transform_get_position_m1104419803(L_25, /*hidden argument*/NULL);
		Vector3_t2243707580  L_27 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_29 = V_1;
		NullCheck(L_28);
		Transform_set_rotation_m3411284563(L_28, L_29, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = V_2;
		NullCheck(L_30);
		Transform_set_position_m2469242620(L_30, L_31, /*hidden argument*/NULL);
		goto IL_0127;
	}

IL_0121:
	{
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_0127:
	{
		return;
	}
}
// System.Single MouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float MouseOrbit_ClampAngle_m4104644686 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseOrbit_ClampAngle_m4104644686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___angle0;
		if ((((float)L_0) >= ((float)(((float)((float)((int32_t)-360)))))))
		{
			goto IL_001a;
		}
	}
	{
		float L_1 = ___angle0;
		___angle0 = ((float)((float)L_1+(float)(((float)((float)((int32_t)360))))));
	}

IL_001a:
	{
		float L_2 = ___angle0;
		if ((((float)L_2) <= ((float)(((float)((float)((int32_t)360)))))))
		{
			goto IL_0034;
		}
	}
	{
		float L_3 = ___angle0;
		___angle0 = ((float)((float)L_3-(float)(((float)((float)((int32_t)360))))));
	}

IL_0034:
	{
		float L_4 = ___angle0;
		float L_5 = ___min1;
		float L_6 = ___max2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void MouseOrbit::Main()
extern "C"  void MouseOrbit_Main_m3154091352 (MouseOrbit_t143029581 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void moveMe::.ctor()
extern "C"  void moveMe__ctor_m591793815 (moveMe_t881802511 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void moveMe::Update()
extern "C"  void moveMe_Update_m2642796658 (moveMe_t881802511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (moveMe_Update_m2642796658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t2243707580  V_15;
	memset(&V_15, 0, sizeof(V_15));
	float V_16 = 0.0f;
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	float V_19 = 0.0f;
	Vector3_t2243707580  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t2243707580  V_21;
	memset(&V_21, 0, sizeof(V_21));
	float V_22 = 0.0f;
	Vector3_t2243707580  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_t2243707580  V_24;
	memset(&V_24, 0, sizeof(V_24));
	float V_25 = 0.0f;
	Vector3_t2243707580  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t2243707580  V_27;
	memset(&V_27, 0, sizeof(V_27));
	float V_28 = 0.0f;
	Vector3_t2243707580  V_29;
	memset(&V_29, 0, sizeof(V_29));
	{
		int32_t L_0 = __this->get_oneToThree_2();
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		__this->set_oneToThree_2(1);
	}

IL_0013:
	{
		int32_t L_1 = __this->get_oneToThree_2();
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_0026;
		}
	}
	{
		__this->set_oneToThree_2(3);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_013e;
		}
	}
	{
		int32_t L_3 = __this->get_oneToThree_2();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_008c;
		}
	}
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		V_12 = L_5;
		float L_6 = (&V_12)->get_x_1();
		float L_7 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = ((float)((float)L_6+(float)L_7));
		V_0 = L_8;
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = L_10;
		V_1 = L_11;
		float L_12 = V_0;
		float L_13 = L_12;
		V_13 = L_13;
		(&V_1)->set_x_1(L_13);
		float L_14 = V_13;
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = V_1;
		Vector3_t2243707580  L_17 = L_16;
		V_14 = L_17;
		NullCheck(L_15);
		Transform_set_position_m2469242620(L_15, L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = V_14;
	}

IL_008c:
	{
		int32_t L_19 = __this->get_oneToThree_2();
		if ((!(((uint32_t)L_19) == ((uint32_t)2))))
		{
			goto IL_00e3;
		}
	}
	{
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		V_15 = L_21;
		float L_22 = (&V_15)->get_y_2();
		float L_23 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = ((float)((float)L_22+(float)L_23));
		V_2 = L_24;
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t2243707580  L_26 = Transform_get_position_m1104419803(L_25, /*hidden argument*/NULL);
		Vector3_t2243707580  L_27 = L_26;
		V_3 = L_27;
		float L_28 = V_2;
		float L_29 = L_28;
		V_16 = L_29;
		(&V_3)->set_y_2(L_29);
		float L_30 = V_16;
		Transform_t3275118058 * L_31 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_32 = V_3;
		Vector3_t2243707580  L_33 = L_32;
		V_17 = L_33;
		NullCheck(L_31);
		Transform_set_position_m2469242620(L_31, L_33, /*hidden argument*/NULL);
		Vector3_t2243707580  L_34 = V_17;
	}

IL_00e3:
	{
		int32_t L_35 = __this->get_oneToThree_2();
		if ((!(((uint32_t)L_35) == ((uint32_t)3))))
		{
			goto IL_013e;
		}
	}
	{
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_position_m1104419803(L_36, /*hidden argument*/NULL);
		V_18 = L_37;
		float L_38 = (&V_18)->get_z_3();
		float L_39 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_40 = ((float)((float)L_38+(float)L_39));
		V_4 = L_40;
		Transform_t3275118058 * L_41 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t2243707580  L_42 = Transform_get_position_m1104419803(L_41, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = L_42;
		V_5 = L_43;
		float L_44 = V_4;
		float L_45 = L_44;
		V_19 = L_45;
		(&V_5)->set_z_3(L_45);
		float L_46 = V_19;
		Transform_t3275118058 * L_47 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_48 = V_5;
		Vector3_t2243707580  L_49 = L_48;
		V_20 = L_49;
		NullCheck(L_47);
		Transform_set_position_m2469242620(L_47, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_20;
	}

IL_013e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_51 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_025e;
		}
	}
	{
		int32_t L_52 = __this->get_oneToThree_2();
		if ((!(((uint32_t)L_52) == ((uint32_t)1))))
		{
			goto IL_01a8;
		}
	}
	{
		Transform_t3275118058 * L_53 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t2243707580  L_54 = Transform_get_position_m1104419803(L_53, /*hidden argument*/NULL);
		V_21 = L_54;
		float L_55 = (&V_21)->get_x_1();
		float L_56 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_57 = ((float)((float)L_55-(float)L_56));
		V_6 = L_57;
		Transform_t3275118058 * L_58 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		Vector3_t2243707580  L_59 = Transform_get_position_m1104419803(L_58, /*hidden argument*/NULL);
		Vector3_t2243707580  L_60 = L_59;
		V_7 = L_60;
		float L_61 = V_6;
		float L_62 = L_61;
		V_22 = L_62;
		(&V_7)->set_x_1(L_62);
		float L_63 = V_22;
		Transform_t3275118058 * L_64 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_65 = V_7;
		Vector3_t2243707580  L_66 = L_65;
		V_23 = L_66;
		NullCheck(L_64);
		Transform_set_position_m2469242620(L_64, L_66, /*hidden argument*/NULL);
		Vector3_t2243707580  L_67 = V_23;
	}

IL_01a8:
	{
		int32_t L_68 = __this->get_oneToThree_2();
		if ((!(((uint32_t)L_68) == ((uint32_t)2))))
		{
			goto IL_0203;
		}
	}
	{
		Transform_t3275118058 * L_69 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_69);
		Vector3_t2243707580  L_70 = Transform_get_position_m1104419803(L_69, /*hidden argument*/NULL);
		V_24 = L_70;
		float L_71 = (&V_24)->get_y_2();
		float L_72 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_73 = ((float)((float)L_71-(float)L_72));
		V_8 = L_73;
		Transform_t3275118058 * L_74 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_74);
		Vector3_t2243707580  L_75 = Transform_get_position_m1104419803(L_74, /*hidden argument*/NULL);
		Vector3_t2243707580  L_76 = L_75;
		V_9 = L_76;
		float L_77 = V_8;
		float L_78 = L_77;
		V_25 = L_78;
		(&V_9)->set_y_2(L_78);
		float L_79 = V_25;
		Transform_t3275118058 * L_80 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_81 = V_9;
		Vector3_t2243707580  L_82 = L_81;
		V_26 = L_82;
		NullCheck(L_80);
		Transform_set_position_m2469242620(L_80, L_82, /*hidden argument*/NULL);
		Vector3_t2243707580  L_83 = V_26;
	}

IL_0203:
	{
		int32_t L_84 = __this->get_oneToThree_2();
		if ((!(((uint32_t)L_84) == ((uint32_t)3))))
		{
			goto IL_025e;
		}
	}
	{
		Transform_t3275118058 * L_85 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_85);
		Vector3_t2243707580  L_86 = Transform_get_position_m1104419803(L_85, /*hidden argument*/NULL);
		V_27 = L_86;
		float L_87 = (&V_27)->get_z_3();
		float L_88 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_89 = ((float)((float)L_87-(float)L_88));
		V_10 = L_89;
		Transform_t3275118058 * L_90 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_90);
		Vector3_t2243707580  L_91 = Transform_get_position_m1104419803(L_90, /*hidden argument*/NULL);
		Vector3_t2243707580  L_92 = L_91;
		V_11 = L_92;
		float L_93 = V_10;
		float L_94 = L_93;
		V_28 = L_94;
		(&V_11)->set_z_3(L_94);
		float L_95 = V_28;
		Transform_t3275118058 * L_96 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_97 = V_11;
		Vector3_t2243707580  L_98 = L_97;
		V_29 = L_98;
		NullCheck(L_96);
		Transform_set_position_m2469242620(L_96, L_98, /*hidden argument*/NULL);
		Vector3_t2243707580  L_99 = V_29;
	}

IL_025e:
	{
		return;
	}
}
// System.Void moveMe::Main()
extern "C"  void moveMe_Main_m2382152610 (moveMe_t881802511 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlatformInputController::.ctor()
extern "C"  void PlatformInputController__ctor_m2569273223 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_autoRotate_2((bool)1);
		__this->set_maxRotationSpeed_3((((float)((float)((int32_t)360)))));
		return;
	}
}
// System.Void PlatformInputController::Awake()
extern "C"  void PlatformInputController_Awake_m679619692 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformInputController_Awake_m679619692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CharacterMotor_t262030084_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_1 = Component_GetComponent_m4225719715(__this, L_0, /*hidden argument*/NULL);
		__this->set_motor_4(((CharacterMotor_t262030084 *)CastclassClass(L_1, CharacterMotor_t262030084_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void PlatformInputController::Update()
extern "C"  void PlatformInputController_Update_m3789481214 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformInputController_Update_m3789481214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, L_0, L_1, (((float)((float)0))), /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		float L_6 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		float L_8 = V_1;
		Vector3_t2243707580  L_9 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)1))), L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = V_1;
		float L_13 = V_1;
		V_1 = ((float)((float)L_12*(float)L_13));
		Vector3_t2243707580  L_14 = V_0;
		float L_15 = V_1;
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0051:
	{
		Camera_t189460977 * L_17 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t4030073918  L_19 = Transform_get_rotation_m1033555130(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = V_0;
		Vector3_t2243707580  L_21 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		Camera_t189460977 * L_22 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_forward_m1833488937(L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_up_m1603627763(L_26, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_28 = Quaternion_FromToRotation_m1685306068(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		Quaternion_t4030073918  L_29 = V_2;
		Vector3_t2243707580  L_30 = V_0;
		Vector3_t2243707580  L_31 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		CharacterMotor_t262030084 * L_32 = __this->get_motor_4();
		Vector3_t2243707580  L_33 = V_0;
		NullCheck(L_32);
		L_32->set_inputMoveDirection_4(L_33);
		CharacterMotor_t262030084 * L_34 = __this->get_motor_4();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_35 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		NullCheck(L_34);
		L_34->set_inputJump_5(L_35);
		bool L_36 = __this->get_autoRotate_2();
		if (!L_36)
		{
			goto IL_011f;
		}
	}
	{
		float L_37 = Vector3_get_sqrMagnitude_m1814096310((&V_0), /*hidden argument*/NULL);
		if ((((float)L_37) <= ((float)(0.01f))))
		{
			goto IL_011f;
		}
	}
	{
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_forward_m1833488937(L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = V_0;
		float L_41 = __this->get_maxRotationSpeed_3();
		float L_42 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = VirtFuncInvoker3< Vector3_t2243707580 , Vector3_t2243707580 , Vector3_t2243707580 , float >::Invoke(7 /* UnityEngine.Vector3 PlatformInputController::ConstantSlerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */, __this, L_39, L_40, ((float)((float)L_41*(float)L_42)));
		V_3 = L_43;
		Vector3_t2243707580  L_44 = V_3;
		Transform_t3275118058 * L_45 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector3_t2243707580  L_46 = Transform_get_up_m1603627763(L_45, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = VirtFuncInvoker2< Vector3_t2243707580 , Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(6 /* UnityEngine.Vector3 PlatformInputController::ProjectOntoPlane(UnityEngine.Vector3,UnityEngine.Vector3) */, __this, L_44, L_46);
		V_3 = L_47;
		Transform_t3275118058 * L_48 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_49 = V_3;
		Transform_t3275118058 * L_50 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_50);
		Vector3_t2243707580  L_51 = Transform_get_up_m1603627763(L_50, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_52 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_set_rotation_m3411284563(L_48, L_52, /*hidden argument*/NULL);
	}

IL_011f:
	{
		return;
	}
}
// UnityEngine.Vector3 PlatformInputController::ProjectOntoPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  PlatformInputController_ProjectOntoPlane_m3298465390 (PlatformInputController_t4273899755 * __this, Vector3_t2243707580  ___v0, Vector3_t2243707580  ___normal1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___v0;
		Vector3_t2243707580  L_1 = ___v0;
		Vector3_t2243707580  L_2 = ___normal1;
		Vector3_t2243707580  L_3 = Vector3_Project_m1396027688(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 PlatformInputController::ConstantSlerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  PlatformInputController_ConstantSlerp_m1768991218 (PlatformInputController_t4273899755 * __this, Vector3_t2243707580  ___from0, Vector3_t2243707580  ___to1, float ___angle2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformInputController_ConstantSlerp_m1768991218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___angle2;
		Vector3_t2243707580  L_1 = ___from0;
		Vector3_t2243707580  L_2 = ___to1;
		float L_3 = Vector3_Angle_m2552334978(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)1))), ((float)((float)L_0/(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t2243707580  L_5 = ___from0;
		Vector3_t2243707580  L_6 = ___to1;
		float L_7 = V_0;
		Vector3_t2243707580  L_8 = Vector3_Slerp_m846771032(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void PlatformInputController::Main()
extern "C"  void PlatformInputController_Main_m410404690 (PlatformInputController_t4273899755 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ScrollUV::.ctor()
extern "C"  void ScrollUV__ctor_m4170097832 (ScrollUV_t2335303814 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_scrollSpeed_X_2((0.5f));
		__this->set_scrollSpeed_Y_3((0.5f));
		return;
	}
}
// System.Void ScrollUV::Update()
extern "C"  void ScrollUV_Update_m1167004397 (ScrollUV_t2335303814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScrollUV_Update_m1167004397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_scrollSpeed_X_2();
		V_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_scrollSpeed_Y_3();
		V_1 = ((float)((float)L_2*(float)L_3));
		Renderer_t257310565 * L_4 = Component_GetComponent_TisRenderer_t257310565_m772028041(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m772028041_MethodInfo_var);
		NullCheck(L_4);
		Material_t193706927 * L_5 = Renderer_get_material_m2553789785(L_4, /*hidden argument*/NULL);
		float L_6 = V_0;
		float L_7 = V_1;
		Vector2_t2243707579  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_m3067419446(&L_8, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_mainTextureOffset_m3533368774(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScrollUV::Main()
extern "C"  void ScrollUV_Main_m3824552235 (ScrollUV_t2335303814 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ThirdPersonCamera::.ctor()
extern "C"  void ThirdPersonCamera__ctor_m2338492013 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_distance_4((7.0f));
		__this->set_height_5((3.0f));
		__this->set_angularSmoothLag_6((0.3f));
		__this->set_angularMaxSpeed_7((15.0f));
		__this->set_heightSmoothLag_8((0.3f));
		__this->set_snapSmoothLag_9((0.2f));
		__this->set_snapMaxSpeed_10((720.0f));
		__this->set_clampHeadPositionScreenSpace_11((0.75f));
		__this->set_lockCameraTimeout_12((0.2f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_headOffset_13(L_0);
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_centerOffset_14(L_1);
		__this->set_targetHeight_19((100000.0f));
		return;
	}
}
// System.Void ThirdPersonCamera::Awake()
extern "C"  void ThirdPersonCamera_Awake_m2969377770 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCamera_Awake_m2969377770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharacterController_t4094781467 * V_0 = NULL;
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t3033363703  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Transform_t3275118058 * L_0 = __this->get_cameraTransform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002f;
		}
	}
	{
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		Camera_t189460977 * L_4 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		__this->set_cameraTransform_2(L_5);
	}

IL_002f:
	{
		Transform_t3275118058 * L_6 = __this->get_cameraTransform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2172988211, /*hidden argument*/NULL);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0050:
	{
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set__target_3(L_8);
		Transform_t3275118058 * L_9 = __this->get__target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008c;
		}
	}
	{
		Transform_t3275118058 * L_11 = __this->get__target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ThirdPersonController_t1841729452_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_11);
		Component_t3819376471 * L_13 = Component_GetComponent_m4225719715(L_11, L_12, /*hidden argument*/NULL);
		__this->set_controller_18(((ThirdPersonController_t1841729452 *)CastclassClass(L_13, ThirdPersonController_t1841729452_il2cpp_TypeInfo_var)));
	}

IL_008c:
	{
		ThirdPersonController_t1841729452 * L_14 = __this->get_controller_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0118;
		}
	}
	{
		Transform_t3275118058 * L_16 = __this->get__target_3();
		NullCheck(L_16);
		Collider_t3497673348 * L_17 = Component_GetComponent_TisCollider_t3497673348_m2974738468(L_16, /*hidden argument*/Component_GetComponent_TisCollider_t3497673348_m2974738468_MethodInfo_var);
		V_0 = ((CharacterController_t4094781467 *)CastclassSealed(L_17, CharacterController_t4094781467_il2cpp_TypeInfo_var));
		CharacterController_t4094781467 * L_18 = V_0;
		NullCheck(L_18);
		Bounds_t3033363703  L_19 = Collider_get_bounds_m3534458178(L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		Vector3_t2243707580  L_20 = Bounds_get_center_m129401026((&V_1), /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = __this->get__target_3();
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		__this->set_centerOffset_14(L_23);
		Vector3_t2243707580  L_24 = __this->get_centerOffset_14();
		__this->set_headOffset_13(L_24);
		Vector3_t2243707580 * L_25 = __this->get_address_of_headOffset_13();
		CharacterController_t4094781467 * L_26 = V_0;
		NullCheck(L_26);
		Bounds_t3033363703  L_27 = Collider_get_bounds_m3534458178(L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		Vector3_t2243707580  L_28 = Bounds_get_max_m4247050707((&V_2), /*hidden argument*/NULL);
		V_3 = L_28;
		float L_29 = (&V_3)->get_y_2();
		Transform_t3275118058 * L_30 = __this->get__target_3();
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_position_m1104419803(L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		float L_32 = (&V_4)->get_y_2();
		L_25->set_y_2(((float)((float)L_29-(float)L_32)));
		goto IL_0122;
	}

IL_0118:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1941325067, /*hidden argument*/NULL);
	}

IL_0122:
	{
		Transform_t3275118058 * L_33 = __this->get__target_3();
		Vector3_t2243707580  L_34 = __this->get_centerOffset_14();
		VirtActionInvoker2< Transform_t3275118058 *, Vector3_t2243707580  >::Invoke(9 /* System.Void ThirdPersonCamera::Cut(UnityEngine.Transform,UnityEngine.Vector3) */, __this, L_33, L_34);
		return;
	}
}
// System.Void ThirdPersonCamera::DebugDrawStuff()
extern "C"  void ThirdPersonCamera_DebugDrawStuff_m2634059124 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCamera_DebugDrawStuff_m2634059124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get__target_3();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get__target_3();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = __this->get_headOffset_13();
		Vector3_t2243707580  L_5 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m2961609580(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ThirdPersonCamera::AngleDistance(System.Single,System.Single)
extern "C"  float ThirdPersonCamera_AngleDistance_m1831836461 (ThirdPersonCamera_t2751132817 * __this, float ___a0, float ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCamera_AngleDistance_m1831836461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Repeat_m943844734(NULL /*static, unused*/, L_0, (((float)((float)((int32_t)360)))), /*hidden argument*/NULL);
		___a0 = L_1;
		float L_2 = ___b1;
		float L_3 = Mathf_Repeat_m943844734(NULL /*static, unused*/, L_2, (((float)((float)((int32_t)360)))), /*hidden argument*/NULL);
		___b1 = L_3;
		float L_4 = ___b1;
		float L_5 = ___a0;
		float L_6 = fabsf(((float)((float)L_4-(float)L_5)));
		return L_6;
	}
}
// System.Void ThirdPersonCamera::Apply(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void ThirdPersonCamera_Apply_m3614534807 (ThirdPersonCamera_t2751132817 * __this, Transform_t3275118058 * ___dummyTarget0, Vector3_t2243707580  ___dummyCenter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCamera_Apply_m3614534807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Quaternion_t4030073918  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	{
		ThirdPersonController_t1841729452 * L_0 = __this->get_controller_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_0251;
	}

IL_0015:
	{
		Transform_t3275118058 * L_2 = __this->get__target_3();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = __this->get_centerOffset_14();
		Vector3_t2243707580  L_5 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t3275118058 * L_6 = __this->get__target_3();
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = __this->get_headOffset_13();
		Vector3_t2243707580  L_9 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Transform_t3275118058 * L_10 = __this->get__target_3();
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_eulerAngles_m4066505159(L_10, /*hidden argument*/NULL);
		V_10 = L_11;
		float L_12 = (&V_10)->get_y_2();
		V_2 = L_12;
		Transform_t3275118058 * L_13 = __this->get_cameraTransform_2();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_eulerAngles_m4066505159(L_13, /*hidden argument*/NULL);
		V_11 = L_14;
		float L_15 = (&V_11)->get_y_2();
		V_3 = L_15;
		float L_16 = V_2;
		V_4 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_17 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral3645101712, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0086;
		}
	}
	{
		__this->set_snap_17((bool)1);
	}

IL_0086:
	{
		bool L_18 = __this->get_snap_17();
		if (!L_18)
		{
			goto IL_00ca;
		}
	}
	{
		float L_19 = V_3;
		float L_20 = V_2;
		float L_21 = VirtFuncInvoker2< float, float, float >::Invoke(6 /* System.Single ThirdPersonCamera::AngleDistance(System.Single,System.Single) */, __this, L_19, L_20);
		if ((((float)L_21) >= ((float)(3.0f))))
		{
			goto IL_00aa;
		}
	}
	{
		__this->set_snap_17((bool)0);
	}

IL_00aa:
	{
		float L_22 = V_3;
		float L_23 = V_4;
		float* L_24 = __this->get_address_of_angleVelocity_16();
		float L_25 = __this->get_snapSmoothLag_9();
		float L_26 = __this->get_snapMaxSpeed_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_27 = Mathf_SmoothDampAngle_m2501423753(NULL /*static, unused*/, L_22, L_23, L_24, L_25, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		goto IL_012d;
	}

IL_00ca:
	{
		ThirdPersonController_t1841729452 * L_28 = __this->get_controller_18();
		NullCheck(L_28);
		float L_29 = VirtFuncInvoker0< float >::Invoke(17 /* System.Single ThirdPersonController::GetLockCameraTimer() */, L_28);
		float L_30 = __this->get_lockCameraTimeout_12();
		if ((((float)L_29) >= ((float)L_30)))
		{
			goto IL_00e3;
		}
	}
	{
		float L_31 = V_3;
		V_4 = L_31;
	}

IL_00e3:
	{
		float L_32 = V_3;
		float L_33 = V_4;
		float L_34 = VirtFuncInvoker2< float, float, float >::Invoke(6 /* System.Single ThirdPersonCamera::AngleDistance(System.Single,System.Single) */, __this, L_32, L_33);
		if ((((float)L_34) <= ((float)(((float)((float)((int32_t)160)))))))
		{
			goto IL_0112;
		}
	}
	{
		ThirdPersonController_t1841729452 * L_35 = __this->get_controller_18();
		NullCheck(L_35);
		bool L_36 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean ThirdPersonController::IsMovingBackwards() */, L_35);
		if (!L_36)
		{
			goto IL_0112;
		}
	}
	{
		float L_37 = V_4;
		V_4 = ((float)((float)L_37+(float)(((float)((float)((int32_t)180))))));
	}

IL_0112:
	{
		float L_38 = V_3;
		float L_39 = V_4;
		float* L_40 = __this->get_address_of_angleVelocity_16();
		float L_41 = __this->get_angularSmoothLag_6();
		float L_42 = __this->get_angularMaxSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_43 = Mathf_SmoothDampAngle_m2501423753(NULL /*static, unused*/, L_38, L_39, L_40, L_41, L_42, /*hidden argument*/NULL);
		V_3 = L_43;
	}

IL_012d:
	{
		ThirdPersonController_t1841729452 * L_44 = __this->get_controller_18();
		NullCheck(L_44);
		bool L_45 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean ThirdPersonController::IsJumping() */, L_44);
		if (!L_45)
		{
			goto IL_0183;
		}
	}
	{
		float L_46 = (&V_0)->get_y_2();
		float L_47 = __this->get_height_5();
		V_5 = ((float)((float)L_46+(float)L_47));
		float L_48 = V_5;
		float L_49 = __this->get_targetHeight_19();
		if ((((float)L_48) < ((float)L_49)))
		{
			goto IL_016a;
		}
	}
	{
		float L_50 = V_5;
		float L_51 = __this->get_targetHeight_19();
		if ((((float)((float)((float)L_50-(float)L_51))) <= ((float)(((float)((float)5))))))
		{
			goto IL_017e;
		}
	}

IL_016a:
	{
		float L_52 = (&V_0)->get_y_2();
		float L_53 = __this->get_height_5();
		__this->set_targetHeight_19(((float)((float)L_52+(float)L_53)));
	}

IL_017e:
	{
		goto IL_0197;
	}

IL_0183:
	{
		float L_54 = (&V_0)->get_y_2();
		float L_55 = __this->get_height_5();
		__this->set_targetHeight_19(((float)((float)L_54+(float)L_55)));
	}

IL_0197:
	{
		Transform_t3275118058 * L_56 = __this->get_cameraTransform_2();
		NullCheck(L_56);
		Vector3_t2243707580  L_57 = Transform_get_position_m1104419803(L_56, /*hidden argument*/NULL);
		V_12 = L_57;
		float L_58 = (&V_12)->get_y_2();
		V_6 = L_58;
		float L_59 = V_6;
		float L_60 = __this->get_targetHeight_19();
		float* L_61 = __this->get_address_of_heightVelocity_15();
		float L_62 = __this->get_heightSmoothLag_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_63 = Mathf_SmoothDamp_m1166236953(NULL /*static, unused*/, L_59, L_60, L_61, L_62, /*hidden argument*/NULL);
		V_6 = L_63;
		float L_64 = V_3;
		Quaternion_t4030073918  L_65 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (((float)((float)0))), L_64, (((float)((float)0))), /*hidden argument*/NULL);
		V_7 = L_65;
		Transform_t3275118058 * L_66 = __this->get_cameraTransform_2();
		Vector3_t2243707580  L_67 = V_0;
		NullCheck(L_66);
		Transform_set_position_m2469242620(L_66, L_67, /*hidden argument*/NULL);
		Transform_t3275118058 * L_68 = __this->get_cameraTransform_2();
		Transform_t3275118058 * L_69 = __this->get_cameraTransform_2();
		NullCheck(L_69);
		Vector3_t2243707580  L_70 = Transform_get_position_m1104419803(L_69, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_71 = V_7;
		Vector3_t2243707580  L_72 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_73 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_71, L_72, /*hidden argument*/NULL);
		float L_74 = __this->get_distance_4();
		Vector3_t2243707580  L_75 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
		Vector3_t2243707580  L_76 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_70, L_75, /*hidden argument*/NULL);
		NullCheck(L_68);
		Transform_set_position_m2469242620(L_68, L_76, /*hidden argument*/NULL);
		float L_77 = V_6;
		float L_78 = L_77;
		V_8 = L_78;
		Transform_t3275118058 * L_79 = __this->get_cameraTransform_2();
		NullCheck(L_79);
		Vector3_t2243707580  L_80 = Transform_get_position_m1104419803(L_79, /*hidden argument*/NULL);
		Vector3_t2243707580  L_81 = L_80;
		V_9 = L_81;
		float L_82 = V_8;
		float L_83 = L_82;
		V_13 = L_83;
		(&V_9)->set_y_2(L_83);
		float L_84 = V_13;
		Transform_t3275118058 * L_85 = __this->get_cameraTransform_2();
		Vector3_t2243707580  L_86 = V_9;
		Vector3_t2243707580  L_87 = L_86;
		V_14 = L_87;
		NullCheck(L_85);
		Transform_set_position_m2469242620(L_85, L_87, /*hidden argument*/NULL);
		Vector3_t2243707580  L_88 = V_14;
		Vector3_t2243707580  L_89 = V_0;
		Vector3_t2243707580  L_90 = V_1;
		VirtActionInvoker2< Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(10 /* System.Void ThirdPersonCamera::SetUpRotation(UnityEngine.Vector3,UnityEngine.Vector3) */, __this, L_89, L_90);
	}

IL_0251:
	{
		return;
	}
}
// System.Void ThirdPersonCamera::LateUpdate()
extern "C"  void ThirdPersonCamera_LateUpdate_m3321567768 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker2< Transform_t3275118058 *, Vector3_t2243707580  >::Invoke(7 /* System.Void ThirdPersonCamera::Apply(UnityEngine.Transform,UnityEngine.Vector3) */, __this, L_0, L_1);
		return;
	}
}
// System.Void ThirdPersonCamera::Cut(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void ThirdPersonCamera_Cut_m1311324977 (ThirdPersonCamera_t2751132817 * __this, Transform_t3275118058 * ___dummyTarget0, Vector3_t2243707580  ___dummyCenter1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = __this->get_heightSmoothLag_8();
		V_0 = L_0;
		float L_1 = __this->get_snapMaxSpeed_10();
		V_1 = L_1;
		float L_2 = __this->get_snapSmoothLag_9();
		V_2 = L_2;
		__this->set_snapMaxSpeed_10((((float)((float)((int32_t)10000)))));
		__this->set_snapSmoothLag_9((0.001f));
		__this->set_heightSmoothLag_8((0.001f));
		__this->set_snap_17((bool)1);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker2< Transform_t3275118058 *, Vector3_t2243707580  >::Invoke(7 /* System.Void ThirdPersonCamera::Apply(UnityEngine.Transform,UnityEngine.Vector3) */, __this, L_3, L_4);
		float L_5 = V_0;
		__this->set_heightSmoothLag_8(L_5);
		float L_6 = V_1;
		__this->set_snapMaxSpeed_10(L_6);
		float L_7 = V_2;
		__this->set_snapSmoothLag_9(L_7);
		return;
	}
}
// System.Void ThirdPersonCamera::SetUpRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void ThirdPersonCamera_SetUpRotation_m1930509600 (ThirdPersonCamera_t2751132817 * __this, Vector3_t2243707580  ___centerPos0, Vector3_t2243707580  ___headPos1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCamera_SetUpRotation_m1930509600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Ray_t2469606224  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Ray_t2469606224  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	{
		Transform_t3275118058 * L_0 = __this->get_cameraTransform_2();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t2243707580  L_2 = ___centerPos0;
		Vector3_t2243707580  L_3 = V_0;
		Vector3_t2243707580  L_4 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_1();
		float L_6 = (&V_1)->get_z_3();
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, L_5, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_8 = Quaternion_LookRotation_m633695927(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t2243707580  L_9 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = __this->get_distance_4();
		Vector3_t2243707580  L_11 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_get_down_m2372302126(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = __this->get_height_5();
		Vector3_t2243707580  L_14 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		Transform_t3275118058 * L_16 = __this->get_cameraTransform_2();
		Quaternion_t4030073918  L_17 = V_2;
		Vector3_t2243707580  L_18 = V_3;
		Quaternion_t4030073918  L_19 = Quaternion_LookRotation_m633695927(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_20 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_rotation_m3411284563(L_16, L_20, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = __this->get_cameraTransform_2();
		NullCheck(L_21);
		Camera_t189460977 * L_22 = Component_GetComponent_TisCamera_t189460977_m3276577584(L_21, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		Vector3_t2243707580  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m2638739322(&L_23, (0.5f), (0.5f), (((float)((float)1))), /*hidden argument*/NULL);
		NullCheck(L_22);
		Ray_t2469606224  L_24 = Camera_ViewportPointToRay_m1799506792(L_22, L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		Transform_t3275118058 * L_25 = __this->get_cameraTransform_2();
		NullCheck(L_25);
		Camera_t189460977 * L_26 = Component_GetComponent_TisCamera_t189460977_m3276577584(L_25, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		float L_27 = __this->get_clampHeadPositionScreenSpace_11();
		Vector3_t2243707580  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2638739322(&L_28, (0.5f), L_27, (((float)((float)1))), /*hidden argument*/NULL);
		NullCheck(L_26);
		Ray_t2469606224  L_29 = Camera_ViewportPointToRay_m1799506792(L_26, L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		float L_30 = __this->get_distance_4();
		Vector3_t2243707580  L_31 = Ray_GetPoint_m1353702366((&V_4), L_30, /*hidden argument*/NULL);
		V_6 = L_31;
		float L_32 = __this->get_distance_4();
		Vector3_t2243707580  L_33 = Ray_GetPoint_m1353702366((&V_5), L_32, /*hidden argument*/NULL);
		V_7 = L_33;
		Vector3_t2243707580  L_34 = Ray_get_direction_m4059191533((&V_4), /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Ray_get_direction_m4059191533((&V_5), /*hidden argument*/NULL);
		float L_36 = Vector3_Angle_m2552334978(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		V_8 = L_36;
		float L_37 = V_8;
		float L_38 = (&V_6)->get_y_2();
		float L_39 = (&V_7)->get_y_2();
		V_9 = ((float)((float)L_37/(float)((float)((float)L_38-(float)L_39))));
		float L_40 = V_9;
		float L_41 = (&V_6)->get_y_2();
		float L_42 = (&___centerPos0)->get_y_2();
		V_10 = ((float)((float)L_40*(float)((float)((float)L_41-(float)L_42))));
		float L_43 = V_10;
		float L_44 = V_8;
		if ((((float)L_43) >= ((float)L_44)))
		{
			goto IL_0124;
		}
	}
	{
		V_10 = (((float)((float)0)));
		goto IL_0152;
	}

IL_0124:
	{
		float L_45 = V_10;
		float L_46 = V_8;
		V_10 = ((float)((float)L_45-(float)L_46));
		Transform_t3275118058 * L_47 = __this->get_cameraTransform_2();
		Transform_t3275118058 * L_48 = __this->get_cameraTransform_2();
		NullCheck(L_48);
		Quaternion_t4030073918  L_49 = Transform_get_rotation_m1033555130(L_48, /*hidden argument*/NULL);
		float L_50 = V_10;
		Quaternion_t4030073918  L_51 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, ((-L_50)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_52 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_rotation_m3411284563(L_47, L_52, /*hidden argument*/NULL);
	}

IL_0152:
	{
		return;
	}
}
// UnityEngine.Vector3 ThirdPersonCamera::GetCenterOffset()
extern "C"  Vector3_t2243707580  ThirdPersonCamera_GetCenterOffset_m1611501039 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_centerOffset_14();
		return L_0;
	}
}
// System.Void ThirdPersonCamera::Main()
extern "C"  void ThirdPersonCamera_Main_m2234253284 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ThirdPersonController::.ctor()
extern "C"  void ThirdPersonController__ctor_m3734179912 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_walkMaxAnimationSpeed_6((0.75f));
		__this->set_trotMaxAnimationSpeed_7((1.0f));
		__this->set_runMaxAnimationSpeed_8((1.0f));
		__this->set_jumpAnimationSpeed_9((1.15f));
		__this->set_landAnimationSpeed_10((1.0f));
		__this->set_walkSpeed_13((2.0f));
		__this->set_trotSpeed_14((4.0f));
		__this->set_runSpeed_15((6.0f));
		__this->set_inAirControlAcceleration_16((3.0f));
		__this->set_jumpHeight_17((0.5f));
		__this->set_gravity_18((20.0f));
		__this->set_speedSmoothing_19((10.0f));
		__this->set_rotateSpeed_20((500.0f));
		__this->set_trotAfterSeconds_21((3.0f));
		__this->set_canJump_22((bool)1);
		__this->set_jumpRepeatTime_23((0.05f));
		__this->set_jumpTimeout_24((0.15f));
		__this->set_groundedTimeout_25((0.25f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_moveDirection_27(L_0);
		__this->set_lastJumpButtonTime_36((-10.0f));
		__this->set_lastJumpTime_37((-1.0f));
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inAirVelocity_39(L_1);
		__this->set_isControllable_41((bool)1);
		return;
	}
}
// System.Void ThirdPersonController::Awake()
extern "C"  void ThirdPersonController_Awake_m1851122549 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_Awake_m1851122549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_2 = Transform_TransformDirection_m1639585047(L_0, L_1, /*hidden argument*/NULL);
		__this->set_moveDirection_27(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Animation_t2068071072_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_4 = Component_GetComponent_m4225719715(__this, L_3, /*hidden argument*/NULL);
		__this->set__animation_11(((Animation_t2068071072 *)CastclassSealed(L_4, Animation_t2068071072_il2cpp_TypeInfo_var)));
		Animation_t2068071072 * L_5 = __this->get__animation_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1182925398, /*hidden argument*/NULL);
	}

IL_004b:
	{
		AnimationClip_t3510324950 * L_7 = __this->get_idleAnimation_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_006c;
		}
	}
	{
		__this->set__animation_11((Animation_t2068071072 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1772573736, /*hidden argument*/NULL);
	}

IL_006c:
	{
		AnimationClip_t3510324950 * L_9 = __this->get_walkAnimation_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_008d;
		}
	}
	{
		__this->set__animation_11((Animation_t2068071072 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2268382563, /*hidden argument*/NULL);
	}

IL_008d:
	{
		AnimationClip_t3510324950 * L_11 = __this->get_runAnimation_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00ae;
		}
	}
	{
		__this->set__animation_11((Animation_t2068071072 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral585509787, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		AnimationClip_t3510324950 * L_13 = __this->get_jumpPoseAnimation_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00da;
		}
	}
	{
		bool L_15 = __this->get_canJump_22();
		if (!L_15)
		{
			goto IL_00da;
		}
	}
	{
		__this->set__animation_11((Animation_t2068071072 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral712064556, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void ThirdPersonController::UpdateSmoothedMovementDirection()
extern "C"  void ThirdPersonController_UpdateSmoothedMovementDirection_m1607820556 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_UpdateSmoothedMovementDirection_m1607820556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	bool V_1 = false;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	bool V_6 = false;
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t G_B5_0 = 0;
	ThirdPersonController_t1841729452 * G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	ThirdPersonController_t1841729452 * G_B4_1 = NULL;
	{
		Camera_t189460977 * L_0 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean ThirdPersonController::IsGrounded() */, __this);
		V_1 = L_2;
		Transform_t3275118058 * L_3 = V_0;
		Vector3_t2243707580  L_4 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_5 = Transform_TransformDirection_m1639585047(L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		(&V_2)->set_y_2((((float)((float)0))));
		Vector3_t2243707580  L_6 = Vector3_get_normalized_m936072361((&V_2), /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = (&V_2)->get_z_3();
		float L_8 = (&V_2)->get_x_1();
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, L_7, (((float)((float)0))), ((-L_8)), /*hidden argument*/NULL);
		V_3 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_10 = Input_GetAxisRaw_m4133353720(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = Input_GetAxisRaw_m4133353720(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = V_4;
		if ((((float)L_12) >= ((float)(-0.2f))))
		{
			goto IL_0076;
		}
	}
	{
		__this->set_movingBack_33((bool)1);
		goto IL_007d;
	}

IL_0076:
	{
		__this->set_movingBack_33((bool)0);
	}

IL_007d:
	{
		bool L_13 = __this->get_isMoving_34();
		V_6 = L_13;
		float L_14 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_15 = fabsf(L_14);
		int32_t L_16 = ((((float)L_15) > ((float)(0.1f)))? 1 : 0);
		G_B4_0 = L_16;
		G_B4_1 = __this;
		if (L_16)
		{
			G_B5_0 = L_16;
			G_B5_1 = __this;
			goto IL_00a9;
		}
	}
	{
		float L_17 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_18 = fabsf(L_17);
		G_B5_0 = ((((float)L_18) > ((float)(0.1f)))? 1 : 0);
		G_B5_1 = G_B4_1;
	}

IL_00a9:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_isMoving_34((bool)G_B5_0);
		float L_19 = V_5;
		Vector3_t2243707580  L_20 = V_3;
		Vector3_t2243707580  L_21 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		float L_22 = V_4;
		Vector3_t2243707580  L_23 = V_2;
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
		V_7 = L_25;
		bool L_26 = V_1;
		if (!L_26)
		{
			goto IL_0247;
		}
	}
	{
		float L_27 = __this->get_lockCameraTimer_26();
		float L_28 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lockCameraTimer_26(((float)((float)L_27+(float)L_28)));
		bool L_29 = __this->get_isMoving_34();
		bool L_30 = V_6;
		if ((((int32_t)L_29) == ((int32_t)L_30)))
		{
			goto IL_00f2;
		}
	}
	{
		__this->set_lockCameraTimer_26((((float)((float)0))));
	}

IL_00f2:
	{
		Vector3_t2243707580  L_31 = V_7;
		Vector3_t2243707580  L_32 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_33 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_016e;
		}
	}
	{
		float L_34 = __this->get_moveSpeed_29();
		float L_35 = __this->get_walkSpeed_13();
		if ((((float)L_34) >= ((float)((float)((float)L_35*(float)(0.9f))))))
		{
			goto IL_0132;
		}
	}
	{
		bool L_36 = V_1;
		if (!L_36)
		{
			goto IL_0132;
		}
	}
	{
		Vector3_t2243707580  L_37 = Vector3_get_normalized_m936072361((&V_7), /*hidden argument*/NULL);
		__this->set_moveDirection_27(L_37);
		goto IL_016e;
	}

IL_0132:
	{
		Vector3_t2243707580  L_38 = __this->get_moveDirection_27();
		Vector3_t2243707580  L_39 = V_7;
		float L_40 = __this->get_rotateSpeed_20();
		float L_41 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = Vector3_RotateTowards_m4046679056(NULL /*static, unused*/, L_38, L_39, ((float)((float)((float)((float)L_40*(float)(0.0174532924f)))*(float)L_41)), (((float)((float)((int32_t)1000)))), /*hidden argument*/NULL);
		__this->set_moveDirection_27(L_42);
		Vector3_t2243707580 * L_43 = __this->get_address_of_moveDirection_27();
		Vector3_t2243707580  L_44 = Vector3_get_normalized_m936072361(L_43, /*hidden argument*/NULL);
		__this->set_moveDirection_27(L_44);
	}

IL_016e:
	{
		float L_45 = __this->get_speedSmoothing_19();
		float L_46 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = ((float)((float)L_45*(float)L_46));
		float L_47 = Vector3_get_magnitude_m860342598((&V_7), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_48 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_47, (1.0f), /*hidden argument*/NULL);
		V_9 = L_48;
		__this->set__characterState_12(0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_49 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_01b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_50 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)303), /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01cb;
		}
	}

IL_01b4:
	{
		float L_51 = V_9;
		float L_52 = __this->get_runSpeed_15();
		V_9 = ((float)((float)L_51*(float)L_52));
		__this->set__characterState_12(3);
		goto IL_020b;
	}

IL_01cb:
	{
		float L_53 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_54 = __this->get_trotAfterSeconds_21();
		float L_55 = __this->get_walkTimeStart_35();
		if ((((float)((float)((float)L_53-(float)L_54))) <= ((float)L_55)))
		{
			goto IL_01f9;
		}
	}
	{
		float L_56 = V_9;
		float L_57 = __this->get_trotSpeed_14();
		V_9 = ((float)((float)L_56*(float)L_57));
		__this->set__characterState_12(2);
		goto IL_020b;
	}

IL_01f9:
	{
		float L_58 = V_9;
		float L_59 = __this->get_walkSpeed_13();
		V_9 = ((float)((float)L_58*(float)L_59));
		__this->set__characterState_12(1);
	}

IL_020b:
	{
		float L_60 = __this->get_moveSpeed_29();
		float L_61 = V_9;
		float L_62 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_63 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_60, L_61, L_62, /*hidden argument*/NULL);
		__this->set_moveSpeed_29(L_63);
		float L_64 = __this->get_moveSpeed_29();
		float L_65 = __this->get_walkSpeed_13();
		if ((((float)L_64) >= ((float)((float)((float)L_65*(float)(0.3f))))))
		{
			goto IL_0242;
		}
	}
	{
		float L_66 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_walkTimeStart_35(L_66);
	}

IL_0242:
	{
		goto IL_0292;
	}

IL_0247:
	{
		bool L_67 = __this->get_jumping_31();
		if (!L_67)
		{
			goto IL_025a;
		}
	}
	{
		__this->set_lockCameraTimer_26((((float)((float)0))));
	}

IL_025a:
	{
		bool L_68 = __this->get_isMoving_34();
		if (!L_68)
		{
			goto IL_0292;
		}
	}
	{
		Vector3_t2243707580  L_69 = __this->get_inAirVelocity_39();
		Vector3_t2243707580  L_70 = Vector3_get_normalized_m936072361((&V_7), /*hidden argument*/NULL);
		float L_71 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_72 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_70, L_71, /*hidden argument*/NULL);
		float L_73 = __this->get_inAirControlAcceleration_16();
		Vector3_t2243707580  L_74 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_72, L_73, /*hidden argument*/NULL);
		Vector3_t2243707580  L_75 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_69, L_74, /*hidden argument*/NULL);
		__this->set_inAirVelocity_39(L_75);
	}

IL_0292:
	{
		return;
	}
}
// System.Void ThirdPersonController::ApplyJumping()
extern "C"  void ThirdPersonController_ApplyJumping_m2303093686 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_ApplyJumping_m2303093686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_lastJumpTime_37();
		float L_1 = __this->get_jumpRepeatTime_23();
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)((float)((float)L_0+(float)L_1))) <= ((float)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		goto IL_0067;
	}

IL_001c:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean ThirdPersonController::IsGrounded() */, __this);
		if (!L_3)
		{
			goto IL_0067;
		}
	}
	{
		bool L_4 = __this->get_canJump_22();
		if (!L_4)
		{
			goto IL_0067;
		}
	}
	{
		float L_5 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = __this->get_lastJumpButtonTime_36();
		float L_7 = __this->get_jumpTimeout_24();
		if ((((float)L_5) >= ((float)((float)((float)L_6+(float)L_7)))))
		{
			goto IL_0067;
		}
	}
	{
		float L_8 = __this->get_jumpHeight_17();
		float L_9 = VirtFuncInvoker1< float, float >::Invoke(8 /* System.Single ThirdPersonController::CalculateJumpVerticalSpeed(System.Single) */, __this, L_8);
		__this->set_verticalSpeed_28(L_9);
		Component_SendMessage_m4199581575(__this, _stringLiteral49017975, 1, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void ThirdPersonController::ApplyGravity()
extern "C"  void ThirdPersonController_ApplyGravity_m3630102144 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_ApplyGravity_m3630102144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = __this->get_isControllable_41();
		if (!L_0)
		{
			goto IL_007d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = __this->get_jumping_31();
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		bool L_3 = __this->get_jumpingReachedApex_32();
		if (L_3)
		{
			goto IL_004c;
		}
	}
	{
		float L_4 = __this->get_verticalSpeed_28();
		if ((((float)L_4) > ((float)(((float)((float)0))))))
		{
			goto IL_004c;
		}
	}
	{
		__this->set_jumpingReachedApex_32((bool)1);
		Component_SendMessage_m4199581575(__this, _stringLiteral2473619578, 1, /*hidden argument*/NULL);
	}

IL_004c:
	{
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean ThirdPersonController::IsGrounded() */, __this);
		if (!L_5)
		{
			goto IL_0064;
		}
	}
	{
		__this->set_verticalSpeed_28((((float)((float)0))));
		goto IL_007d;
	}

IL_0064:
	{
		float L_6 = __this->get_verticalSpeed_28();
		float L_7 = __this->get_gravity_18();
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_verticalSpeed_28(((float)((float)L_6-(float)((float)((float)L_7*(float)L_8)))));
	}

IL_007d:
	{
		return;
	}
}
// System.Single ThirdPersonController::CalculateJumpVerticalSpeed(System.Single)
extern "C"  float ThirdPersonController_CalculateJumpVerticalSpeed_m266022954 (ThirdPersonController_t1841729452 * __this, float ___targetJumpHeight0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_CalculateJumpVerticalSpeed_m266022954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___targetJumpHeight0;
		float L_1 = __this->get_gravity_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = sqrtf(((float)((float)((float)((float)(((float)((float)2)))*(float)L_0))*(float)L_1)));
		return L_2;
	}
}
// System.Void ThirdPersonController::DidJump()
extern "C"  void ThirdPersonController_DidJump_m2182722107 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		__this->set_jumping_31((bool)1);
		__this->set_jumpingReachedApex_32((bool)0);
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastJumpTime_37(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_2();
		__this->set_lastJumpStartHeight_38(L_3);
		__this->set_lastJumpButtonTime_36((((float)((float)((int32_t)-10)))));
		__this->set__characterState_12(4);
		return;
	}
}
// System.Void ThirdPersonController::Update()
extern "C"  void ThirdPersonController_Update_m3514131137 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_Update_m3514131137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	CharacterController_t4094781467 * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		bool L_0 = __this->get_isControllable_41();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Input_ResetInputAxes_m3212187783(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastJumpButtonTime_36(L_2);
	}

IL_002a:
	{
		VirtActionInvoker0::Invoke(5 /* System.Void ThirdPersonController::UpdateSmoothedMovementDirection() */, __this);
		VirtActionInvoker0::Invoke(7 /* System.Void ThirdPersonController::ApplyGravity() */, __this);
		VirtActionInvoker0::Invoke(6 /* System.Void ThirdPersonController::ApplyJumping() */, __this);
		Vector3_t2243707580  L_3 = __this->get_moveDirection_27();
		float L_4 = __this->get_moveSpeed_29();
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_verticalSpeed_28();
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, (((float)((float)0))), L_6, (((float)((float)0))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = __this->get_inAirVelocity_39();
		Vector3_t2243707580  L_10 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Vector3_t2243707580  L_11 = V_0;
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CharacterController_t4094781467_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_15 = Component_GetComponent_m4225719715(__this, L_14, /*hidden argument*/NULL);
		V_1 = ((CharacterController_t4094781467 *)CastclassSealed(L_15, CharacterController_t4094781467_il2cpp_TypeInfo_var));
		CharacterController_t4094781467 * L_16 = V_1;
		Vector3_t2243707580  L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = CharacterController_Move_m3456882757(L_16, L_17, /*hidden argument*/NULL);
		__this->set_collisionFlags_30(L_18);
		Animation_t2068071072 * L_19 = __this->get__animation_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_02bc;
		}
	}
	{
		int32_t L_21 = __this->get__characterState_12();
		if ((!(((uint32_t)L_21) == ((uint32_t)4))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_22 = __this->get_jumpingReachedApex_32();
		if (L_22)
		{
			goto IL_011b;
		}
	}
	{
		Animation_t2068071072 * L_23 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_24 = __this->get_jumpPoseAnimation_5();
		NullCheck(L_24);
		String_t* L_25 = Object_get_name_m2079638459(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		AnimationState_t1303741697 * L_26 = Animation_get_Item_m4198128320(L_23, L_25, /*hidden argument*/NULL);
		float L_27 = __this->get_jumpAnimationSpeed_9();
		NullCheck(L_26);
		AnimationState_set_speed_m465014523(L_26, L_27, /*hidden argument*/NULL);
		Animation_t2068071072 * L_28 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_29 = __this->get_jumpPoseAnimation_5();
		NullCheck(L_29);
		String_t* L_30 = Object_get_name_m2079638459(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		AnimationState_t1303741697 * L_31 = Animation_get_Item_m4198128320(L_28, L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		AnimationState_set_wrapMode_m2342385428(L_31, 8, /*hidden argument*/NULL);
		Animation_t2068071072 * L_32 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_33 = __this->get_jumpPoseAnimation_5();
		NullCheck(L_33);
		String_t* L_34 = Object_get_name_m2079638459(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Animation_CrossFade_m3878519673(L_32, L_34, /*hidden argument*/NULL);
		goto IL_016f;
	}

IL_011b:
	{
		Animation_t2068071072 * L_35 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_36 = __this->get_jumpPoseAnimation_5();
		NullCheck(L_36);
		String_t* L_37 = Object_get_name_m2079638459(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		AnimationState_t1303741697 * L_38 = Animation_get_Item_m4198128320(L_35, L_37, /*hidden argument*/NULL);
		float L_39 = __this->get_landAnimationSpeed_10();
		NullCheck(L_38);
		AnimationState_set_speed_m465014523(L_38, ((-L_39)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_40 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_41 = __this->get_jumpPoseAnimation_5();
		NullCheck(L_41);
		String_t* L_42 = Object_get_name_m2079638459(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		AnimationState_t1303741697 * L_43 = Animation_get_Item_m4198128320(L_40, L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		AnimationState_set_wrapMode_m2342385428(L_43, 8, /*hidden argument*/NULL);
		Animation_t2068071072 * L_44 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_45 = __this->get_jumpPoseAnimation_5();
		NullCheck(L_45);
		String_t* L_46 = Object_get_name_m2079638459(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		Animation_CrossFade_m3878519673(L_44, L_46, /*hidden argument*/NULL);
	}

IL_016f:
	{
		goto IL_02bc;
	}

IL_0174:
	{
		CharacterController_t4094781467 * L_47 = V_1;
		NullCheck(L_47);
		Vector3_t2243707580  L_48 = CharacterController_get_velocity_m1484936086(L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		float L_49 = Vector3_get_sqrMagnitude_m1814096310((&V_3), /*hidden argument*/NULL);
		if ((((float)L_49) >= ((float)(0.1f))))
		{
			goto IL_01a7;
		}
	}
	{
		Animation_t2068071072 * L_50 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_51 = __this->get_idleAnimation_2();
		NullCheck(L_51);
		String_t* L_52 = Object_get_name_m2079638459(L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		Animation_CrossFade_m3878519673(L_50, L_52, /*hidden argument*/NULL);
		goto IL_02bc;
	}

IL_01a7:
	{
		int32_t L_53 = __this->get__characterState_12();
		if ((!(((uint32_t)L_53) == ((uint32_t)3))))
		{
			goto IL_0205;
		}
	}
	{
		Animation_t2068071072 * L_54 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_55 = __this->get_runAnimation_4();
		NullCheck(L_55);
		String_t* L_56 = Object_get_name_m2079638459(L_55, /*hidden argument*/NULL);
		NullCheck(L_54);
		AnimationState_t1303741697 * L_57 = Animation_get_Item_m4198128320(L_54, L_56, /*hidden argument*/NULL);
		CharacterController_t4094781467 * L_58 = V_1;
		NullCheck(L_58);
		Vector3_t2243707580  L_59 = CharacterController_get_velocity_m1484936086(L_58, /*hidden argument*/NULL);
		V_4 = L_59;
		float L_60 = Vector3_get_magnitude_m860342598((&V_4), /*hidden argument*/NULL);
		float L_61 = __this->get_runMaxAnimationSpeed_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_62 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_60, (((float)((float)0))), L_61, /*hidden argument*/NULL);
		NullCheck(L_57);
		AnimationState_set_speed_m465014523(L_57, L_62, /*hidden argument*/NULL);
		Animation_t2068071072 * L_63 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_64 = __this->get_runAnimation_4();
		NullCheck(L_64);
		String_t* L_65 = Object_get_name_m2079638459(L_64, /*hidden argument*/NULL);
		NullCheck(L_63);
		Animation_CrossFade_m3878519673(L_63, L_65, /*hidden argument*/NULL);
		goto IL_02bc;
	}

IL_0205:
	{
		int32_t L_66 = __this->get__characterState_12();
		if ((!(((uint32_t)L_66) == ((uint32_t)2))))
		{
			goto IL_0263;
		}
	}
	{
		Animation_t2068071072 * L_67 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_68 = __this->get_walkAnimation_3();
		NullCheck(L_68);
		String_t* L_69 = Object_get_name_m2079638459(L_68, /*hidden argument*/NULL);
		NullCheck(L_67);
		AnimationState_t1303741697 * L_70 = Animation_get_Item_m4198128320(L_67, L_69, /*hidden argument*/NULL);
		CharacterController_t4094781467 * L_71 = V_1;
		NullCheck(L_71);
		Vector3_t2243707580  L_72 = CharacterController_get_velocity_m1484936086(L_71, /*hidden argument*/NULL);
		V_5 = L_72;
		float L_73 = Vector3_get_magnitude_m860342598((&V_5), /*hidden argument*/NULL);
		float L_74 = __this->get_trotMaxAnimationSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_75 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_73, (((float)((float)0))), L_74, /*hidden argument*/NULL);
		NullCheck(L_70);
		AnimationState_set_speed_m465014523(L_70, L_75, /*hidden argument*/NULL);
		Animation_t2068071072 * L_76 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_77 = __this->get_walkAnimation_3();
		NullCheck(L_77);
		String_t* L_78 = Object_get_name_m2079638459(L_77, /*hidden argument*/NULL);
		NullCheck(L_76);
		Animation_CrossFade_m3878519673(L_76, L_78, /*hidden argument*/NULL);
		goto IL_02bc;
	}

IL_0263:
	{
		int32_t L_79 = __this->get__characterState_12();
		if ((!(((uint32_t)L_79) == ((uint32_t)1))))
		{
			goto IL_02bc;
		}
	}
	{
		Animation_t2068071072 * L_80 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_81 = __this->get_walkAnimation_3();
		NullCheck(L_81);
		String_t* L_82 = Object_get_name_m2079638459(L_81, /*hidden argument*/NULL);
		NullCheck(L_80);
		AnimationState_t1303741697 * L_83 = Animation_get_Item_m4198128320(L_80, L_82, /*hidden argument*/NULL);
		CharacterController_t4094781467 * L_84 = V_1;
		NullCheck(L_84);
		Vector3_t2243707580  L_85 = CharacterController_get_velocity_m1484936086(L_84, /*hidden argument*/NULL);
		V_6 = L_85;
		float L_86 = Vector3_get_magnitude_m860342598((&V_6), /*hidden argument*/NULL);
		float L_87 = __this->get_walkMaxAnimationSpeed_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_88 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_86, (((float)((float)0))), L_87, /*hidden argument*/NULL);
		NullCheck(L_83);
		AnimationState_set_speed_m465014523(L_83, L_88, /*hidden argument*/NULL);
		Animation_t2068071072 * L_89 = __this->get__animation_11();
		AnimationClip_t3510324950 * L_90 = __this->get_walkAnimation_3();
		NullCheck(L_90);
		String_t* L_91 = Object_get_name_m2079638459(L_90, /*hidden argument*/NULL);
		NullCheck(L_89);
		Animation_CrossFade_m3878519673(L_89, L_91, /*hidden argument*/NULL);
	}

IL_02bc:
	{
		bool L_92 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean ThirdPersonController::IsGrounded() */, __this);
		if (!L_92)
		{
			goto IL_02e2;
		}
	}
	{
		Transform_t3275118058 * L_93 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_94 = __this->get_moveDirection_27();
		Quaternion_t4030073918  L_95 = Quaternion_LookRotation_m633695927(NULL /*static, unused*/, L_94, /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_set_rotation_m3411284563(L_93, L_95, /*hidden argument*/NULL);
		goto IL_030f;
	}

IL_02e2:
	{
		Vector3_t2243707580  L_96 = V_0;
		V_2 = L_96;
		(&V_2)->set_y_2((((float)((float)0))));
		float L_97 = Vector3_get_sqrMagnitude_m1814096310((&V_2), /*hidden argument*/NULL);
		if ((((float)L_97) <= ((float)(0.001f))))
		{
			goto IL_030f;
		}
	}
	{
		Transform_t3275118058 * L_98 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_99 = V_2;
		Quaternion_t4030073918  L_100 = Quaternion_LookRotation_m633695927(NULL /*static, unused*/, L_99, /*hidden argument*/NULL);
		NullCheck(L_98);
		Transform_set_rotation_m3411284563(L_98, L_100, /*hidden argument*/NULL);
	}

IL_030f:
	{
		bool L_101 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean ThirdPersonController::IsGrounded() */, __this);
		if (!L_101)
		{
			goto IL_034e;
		}
	}
	{
		float L_102 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastGroundedTime_40(L_102);
		Vector3_t2243707580  L_103 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inAirVelocity_39(L_103);
		bool L_104 = __this->get_jumping_31();
		if (!L_104)
		{
			goto IL_034e;
		}
	}
	{
		__this->set_jumping_31((bool)0);
		Component_SendMessage_m4199581575(__this, _stringLiteral3302144998, 1, /*hidden argument*/NULL);
	}

IL_034e:
	{
		return;
	}
}
// System.Void ThirdPersonController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void ThirdPersonController_OnControllerColliderHit_m3958616428 (ThirdPersonController_t1841729452 * __this, ControllerColliderHit_t4070855101 * ___hit0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ControllerColliderHit_t4070855101 * L_0 = ___hit0;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = ControllerColliderHit_get_moveDirection_m3053186297(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		if ((((float)L_2) <= ((float)(0.01f))))
		{
			goto IL_001d;
		}
	}
	{
		goto IL_001d;
	}

IL_001d:
	{
		return;
	}
}
// System.Single ThirdPersonController::GetSpeed()
extern "C"  float ThirdPersonController_GetSpeed_m472727607 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_moveSpeed_29();
		return L_0;
	}
}
// System.Boolean ThirdPersonController::IsJumping()
extern "C"  bool ThirdPersonController_IsJumping_m228886352 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_jumping_31();
		return L_0;
	}
}
// System.Boolean ThirdPersonController::IsGrounded()
extern "C"  bool ThirdPersonController_IsGrounded_m1308405076 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_collisionFlags_30();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector3 ThirdPersonController::GetDirection()
extern "C"  Vector3_t2243707580  ThirdPersonController_GetDirection_m137009309 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_moveDirection_27();
		return L_0;
	}
}
// System.Boolean ThirdPersonController::IsMovingBackwards()
extern "C"  bool ThirdPersonController_IsMovingBackwards_m414282108 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_movingBack_33();
		return L_0;
	}
}
// System.Single ThirdPersonController::GetLockCameraTimer()
extern "C"  float ThirdPersonController_GetLockCameraTimer_m3631191617 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_lockCameraTimer_26();
		return L_0;
	}
}
// System.Boolean ThirdPersonController::IsMoving()
extern "C"  bool ThirdPersonController_IsMoving_m2115366220 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_IsMoving_m2115366220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxisRaw_m4133353720(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = Input_GetAxisRaw_m4133353720(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		float L_3 = fabsf(L_2);
		return (bool)((((float)((float)((float)L_1+(float)L_3))) > ((float)(0.5f)))? 1 : 0);
	}
}
// System.Boolean ThirdPersonController::HasJumpReachedApex()
extern "C"  bool ThirdPersonController_HasJumpReachedApex_m3256184752 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_jumpingReachedApex_32();
		return L_0;
	}
}
// System.Boolean ThirdPersonController::IsGroundedWithTimeout()
extern "C"  bool ThirdPersonController_IsGroundedWithTimeout_m2975589771 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_lastGroundedTime_40();
		float L_1 = __this->get_groundedTimeout_25();
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)((((float)((float)((float)L_0+(float)L_1))) > ((float)L_2))? 1 : 0);
	}
}
// System.Void ThirdPersonController::Reset()
extern "C"  void ThirdPersonController_Reset_m1776449033 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonController_Reset_m1776449033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_set_tag_m717375123(L_0, _stringLiteral1875862075, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdPersonController::Main()
extern "C"  void ThirdPersonController_Main_m3522903707 (ThirdPersonController_t1841729452 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
