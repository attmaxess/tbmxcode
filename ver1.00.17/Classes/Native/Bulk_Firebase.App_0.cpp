﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Firebase_App_U3CModuleU3E3783534214.h"
#include "Firebase_App_Firebase_AppUtil2859790353.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Exception1927440687.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE3515465473.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException1412130252.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelp549488518.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2876249339.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2443153790.h"
#include "mscorlib_System_ApplicationException474868623.h"
#include "mscorlib_System_ArithmeticException3261462543.h"
#include "mscorlib_System_DivideByZeroException1660837001.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_IO_IOException2458421087.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "mscorlib_System_OutOfMemoryException1181064283.h"
#include "mscorlib_System_OverflowException1075868493.h"
#include "mscorlib_System_SystemException3877406272.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGPendingEx4193433529.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelpe18001273.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2125486988.h"
#include "Firebase_App_Firebase_FirebaseApp_DestroyDelegate3635929227.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge652733044.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2513902766.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3409525828.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3874796154.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va828546831.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler2907300047.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_M323097478.h"
#include "mscorlib_System_Attribute542643598.h"
#include "Firebase_App_Firebase_FirebaseHttpRequest1911360314.h"
#include "System_System_Uri19570940.h"
#include "System_Core_System_Func_2_gen1644515999.h"
#include "Firebase_App_Firebase_UnityHttpRequest967994112.h"
#include "Firebase_App_Firebase_UnityDownloadStream3205965147.h"
#include "mscorlib_System_Byte3683104436.h"
#include "Firebase_App_Firebase_UnityDownloadStream_Blocking1283191913.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Threading_EventWaitHandle2091316307.h"
#include "Firebase_App_Firebase_UnityHttpRequest_StreamingDo1548287308.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext1051733884.h"
#include "mscorlib_System_Threading_SynchronizationContext3857790437.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_S692674473.h"
#include "System_System_Collections_Generic_Queue_1_gen279829387.h"
#include "mscorlib_System_Threading_SendOrPostCallback296893742.h"
#include "Unity_Compat_System_Tuple_2_gen460172552.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4228867588.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1310454857.h"
#include "mscorlib_System_UInt322149682021.h"
#include "Firebase_App_MonoPInvokeCallbackAttribute1970456718.h"

// System.Exception
struct Exception_t1927440687;
// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate
struct LogMessageDelegate_t1988210674;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t1412130252;
// Firebase.AppUtilPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t549488518;
// System.String
struct String_t;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t2876249339;
// System.Object
struct Il2CppObject;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t2443153790;
// System.ApplicationException
struct ApplicationException_t474868623;
// System.ArithmeticException
struct ArithmeticException_t3261462543;
// System.DivideByZeroException
struct DivideByZeroException_t1660837001;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3527622107;
// System.InvalidCastException
struct InvalidCastException_t3625212209;
// System.InvalidOperationException
struct InvalidOperationException_t721527559;
// System.IO.IOException
struct IOException_t2458421087;
// System.NullReferenceException
struct NullReferenceException_t3156209119;
// System.OutOfMemoryException
struct OutOfMemoryException_t1181064283;
// System.OverflowException
struct OverflowException_t1075868493;
// System.SystemException
struct SystemException_t3877406272;
// System.ArgumentException
struct ArgumentException_t3259014390;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t279959794;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Type
struct Type_t;
// Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t18001273;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// Firebase.FirebaseApp/DestroyDelegate
struct DestroyDelegate_t3635929227;
// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>
struct Dictionary_2_t652733044;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>
struct Dictionary_2_t2125486988;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Firebase.FirebaseApp>
struct ValueCollection_t828546831;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t984569266;
// System.Collections.Generic.List`1<Firebase.FirebaseApp>
struct List_1_t3874796154;
// System.Collections.Generic.IEnumerable`1<Firebase.FirebaseApp>
struct IEnumerable_1_t502834771;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// Firebase.FirebaseApp/FirebaseHandler
struct FirebaseHandler_t2907300047;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Firebase.FirebaseApp/FirebaseHandler/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t323097478;
// System.Attribute
struct Attribute_t542643598;
// Firebase.FirebaseHttpRequest
struct FirebaseHttpRequest_t1911360314;
// System.Uri
struct Uri_t19570940;
// System.Func`2<System.Uri,Firebase.FirebaseHttpRequest>
struct Func_2_t1644515999;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// Firebase.UnityHttpRequest
struct UnityHttpRequest_t967994112;
// Firebase.UnityDownloadStream
struct UnityDownloadStream_t3205965147;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Firebase.UnityDownloadStream/BlockingCollection`1<System.Byte[]>
struct BlockingCollection_1_t1283191913;
// Firebase.UnityDownloadStream/BlockingCollection`1<System.Object>
struct BlockingCollection_1_t575307195;
// System.IO.Stream
struct Stream_t3255436806;
// System.Array
struct Il2CppArray;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t2091316307;
// Firebase.UnityHttpRequest/StreamingDownloadHandler
struct StreamingDownloadHandler_t1548287308;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Firebase.UnitySynchronizationContext
struct UnitySynchronizationContext_t1051733884;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t3857790437;
// System.Threading.Thread
struct Thread_t241561612;
// Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir
struct SynchronizationContextBehavoir_t692674473;
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>
struct Queue_1_t279829387;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t296893742;
// System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>
struct Tuple_2_t460172552;
// System.Tuple`2<System.Object,System.Object>
struct Tuple_2_t1036200771;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2509106130;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.ManualResetEvent>
struct Dictionary_2_t4228867588;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t1310454857;
// MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1970456718;
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AppUtil_PollCallbacks_m1948862416_MetadataUsageId;
extern const uint32_t AppUtil_AppEnableLogCallback_m1418645513_MetadataUsageId;
extern const uint32_t AppUtil_SetEnabledAllAppCallbacks_m2417423293_MetadataUsageId;
extern const uint32_t AppUtil_SetLogFunction_m1947305073_MetadataUsageId;
extern Il2CppClass* SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGStringHelper_t549488518_il2cpp_TypeInfo_var;
extern const uint32_t AppUtilPINVOKE__cctor_m1477430606_MetadataUsageId;
extern Il2CppClass* ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var;
extern Il2CppClass* ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingIOException_m2228906713_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingSystemException_m2519873314_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MethodInfo_var;
extern const uint32_t SWIGExceptionHelper__cctor_m2709485170_MetadataUsageId;
extern Il2CppClass* ApplicationException_t474868623_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MetadataUsageId;
extern Il2CppClass* ArithmeticException_t3261462543_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MetadataUsageId;
extern Il2CppClass* DivideByZeroException_t1660837001_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MetadataUsageId;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MetadataUsageId;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MetadataUsageId;
extern Il2CppClass* IOException_t2458421087_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIOException_m2228906713_MetadataUsageId;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MetadataUsageId;
extern Il2CppClass* OutOfMemoryException_t1181064283_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MetadataUsageId;
extern Il2CppClass* OverflowException_t1075868493_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MetadataUsageId;
extern Il2CppClass* SystemException_t3877406272_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingSystemException_m2519873314_MetadataUsageId;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4083594583;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MetadataUsageId;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MetadataUsageId;
extern const uint32_t SWIGPendingException_get_Pending_m1368465104_MetadataUsageId;
extern const Il2CppType* AppUtilPINVOKE_t3515465473_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1125746324;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t SWIGPendingException_Set_m944203304_MetadataUsageId;
extern const uint32_t SWIGPendingException_Retrieve_m197352211_MetadataUsageId;
extern Il2CppClass* SWIGStringDelegate_t18001273_il2cpp_TypeInfo_var;
extern const MethodInfo* SWIGStringHelper_CreateString_m687016595_MethodInfo_var;
extern const uint32_t SWIGStringHelper__cctor_m3676208588_MetadataUsageId;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_Finalize_m3651549934_MetadataUsageId;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DestroyDelegate_t3635929227_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseApp_U3CDisposeU3Em__0_m3004522492_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m196626582_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m2831909098_MethodInfo_var;
extern const uint32_t FirebaseApp_Dispose_m3098692549_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Item_m3177056351_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1952206578_MethodInfo_var;
extern const uint32_t FirebaseApp_RemoveReference_m1475253910_MetadataUsageId;
extern Il2CppClass* List_1_t3874796154_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m373261557_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3695390092_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m1059231906_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m3658164290_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3866234593_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3438154773_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m112670625_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1826530985_MethodInfo_var;
extern const uint32_t FirebaseApp_EmptyAppDictionaries_m4102170024_MetadataUsageId;
extern const uint32_t FirebaseApp_get_Name_m3737532545_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t2125486988_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t652733044_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2513902766_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2836329251_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1865841403_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m186159185_MethodInfo_var;
extern const uint32_t FirebaseApp__cctor_m2391020073_MetadataUsageId;
extern const uint32_t FirebaseApp_U3CDisposeU3Em__0_m3004522492_MetadataUsageId;
extern const Il2CppType* FirebaseHandler_t2907300047_0_0_0_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* LogMessageDelegate_t1988210674_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseHandler_LogMessage_m1734897910_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1742625763;
extern const uint32_t FirebaseHandler_Create_m3248912600_MetadataUsageId;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseHandler_LogMessage_m1734897910_MetadataUsageId;
extern const uint32_t FirebaseHandler_OnApplicationQuit_m2676139308_MetadataUsageId;
extern Il2CppClass* LogLevel_t543421840_il2cpp_TypeInfo_var;
extern const uint32_t LogMessageDelegate_BeginInvoke_m71829407_MetadataUsageId;
extern Il2CppClass* Func_2_t1644515999_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseHttpRequest_U3C_factoryU3Em__0_m3260697191_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3456221722_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral677017058;
extern const uint32_t FirebaseHttpRequest__cctor_m244392713_MetadataUsageId;
extern Il2CppClass* UnityHttpRequest_t967994112_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseHttpRequest_U3C_factoryU3Em__0_m3260697191_MetadataUsageId;
extern const MethodInfo* BlockingCollection_1_Add_m3386735512_MethodInfo_var;
extern const uint32_t UnityDownloadStream_ReceiveData_m1845172324_MetadataUsageId;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t UnityDownloadStream_CompleteContent_m4053627945_MetadataUsageId;
extern const MethodInfo* BlockingCollection_1_Take_m1432993353_MethodInfo_var;
extern const uint32_t UnityDownloadStream_Read_m3691296292_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t UnityDownloadStream_Seek_m56704085_MetadataUsageId;
extern const uint32_t UnityDownloadStream_SetLength_m2504080029_MetadataUsageId;
extern const uint32_t UnityDownloadStream_Write_m3870116827_MetadataUsageId;
extern const uint32_t UnityDownloadStream_get_Position_m2552194688_MetadataUsageId;
extern const uint32_t UnityDownloadStream_set_Position_m3826165111_MetadataUsageId;
extern Il2CppClass* NameValueCollection_t3047564564_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* ManualResetEvent_t926074657_il2cpp_TypeInfo_var;
extern const uint32_t UnityHttpRequest__ctor_m1552832513_MetadataUsageId;
extern Il2CppClass* IEnumerable_1_t1993471762_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3471835840_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const uint32_t UnityHttpRequest_RecordResult_m2645839245_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m243759142_MethodInfo_var;
extern const uint32_t StreamingDownloadHandler_ReceiveData_m3730863066_MetadataUsageId;
extern Il2CppClass* Thread_t241561612_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext__ctor_m1343069354_MetadataUsageId;
extern const uint32_t UnitySynchronizationContext_Install_m3862032013_MetadataUsageId;
extern Il2CppClass* Tuple_2_t460172552_il2cpp_TypeInfo_var;
extern const MethodInfo* Tuple_2__ctor_m4133476842_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m2589358569_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext_Post_m4267436977_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t4228867588_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m446622171_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext__cctor_m3960838443_MetadataUsageId;
extern Il2CppClass* Queue_1_t279829387_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m4231618664_MethodInfo_var;
extern const uint32_t SynchronizationContextBehavoir_get_CallbackQueue_m2312663759_MetadataUsageId;
extern Il2CppClass* U3CStartU3Ec__Iterator0_t1310454857_il2cpp_TypeInfo_var;
extern const uint32_t SynchronizationContextBehavoir_Start_m3359373602_MetadataUsageId;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_get_Count_m177438226_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1588528382_MethodInfo_var;
extern const MethodInfo* Tuple_2_get_Item1_m2647242292_MethodInfo_var;
extern const MethodInfo* Tuple_2_get_Item2_m3295131920_MethodInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m4183249272_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m4016881843_MetadataUsageId;

// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m4215165900_gshared (Dictionary_2_t3131474613 * __this, IntPtr_t p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m112127646_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::get_Item(!0)
extern "C"  int32_t Dictionary_2_get_Item_m3177056351_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1952206578_gshared (Dictionary_2_t2513902766 * __this, IntPtr_t p0, int32_t p1, const MethodInfo* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t984569266 * Dictionary_2_get_Values_m2233445381_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1__ctor_m2848015482_gshared (List_1_t2058570427 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3142955910_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2325793156_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2049292515_gshared (Dictionary_2_t3131474613 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m186159185_gshared (Dictionary_2_t2513902766 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m2049635786_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1684831714_gshared (Func_2_t2825504181 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void Firebase.UnityDownloadStream/BlockingCollection`1<System.Object>::Add(T)
extern "C"  void BlockingCollection_1_Add_m2357368053_gshared (BlockingCollection_1_t575307195 * __this, Il2CppObject * p0, const MethodInfo* method);
// T Firebase.UnityDownloadStream/BlockingCollection`1<System.Object>::Take()
extern "C"  Il2CppObject * BlockingCollection_1_Take_m2773562012_gshared (BlockingCollection_1_t575307195 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4209421183_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void System.Tuple`2<System.Object,System.Object>::.ctor(!0,!1)
extern "C"  void Tuple_2__ctor_m4275432376_gshared (Tuple_2_t1036200771 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(!0)
extern "C"  void Queue_1_Enqueue_m4123136646_gshared (Queue_1_t2509106130 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3997847064_gshared (Dictionary_2_t1697274930 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C"  void Queue_1__ctor_m1845245813_gshared (Queue_1_t2509106130 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m3795587777_gshared (Queue_1_t2509106130 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C"  Il2CppObject * Queue_1_Dequeue_m3320287779_gshared (Queue_1_t2509106130 * __this, const MethodInfo* method);
// !0 System.Tuple`2<System.Object,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_2_get_Item1_m321736064_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method);
// !1 System.Tuple`2<System.Object,System.Object>::get_Item2()
extern "C"  Il2CppObject * Tuple_2_get_Item2_m2379020804_gshared (Tuple_2_t1036200771 * __this, const MethodInfo* method);

// System.Void Firebase.AppUtilPINVOKE::Firebase_App_PollCallbacks()
extern "C"  void AppUtilPINVOKE_Firebase_App_PollCallbacks_m784944262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern "C"  bool SWIGPendingException_get_Pending_m1368465104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern "C"  Exception_t1927440687 * SWIGPendingException_Retrieve_m197352211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m3057939313 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m3339389901 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetLogFunction_m3733654825 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern "C"  void SWIGExceptionHelper__ctor_m432919667 (SWIGExceptionHelper_t1412130252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1408369181 (SWIGStringHelper_t549488518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingApplicationException_m1534282613 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArithmeticException_m4135332127 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIOException_m2228906713 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOverflowException_m3154125439 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingSystemException_m2519873314 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentException_m3648622388 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionDelegate__ctor_m1540989664 (ExceptionDelegate_t2876249339 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionArgumentDelegate__ctor_m2902104247 (ExceptionArgumentDelegate_t2443153790 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m1809271624 (Il2CppObject * __this /* static, unused */, ExceptionDelegate_t2876249339 * ___applicationDelegate0, ExceptionDelegate_t2876249339 * ___arithmeticDelegate1, ExceptionDelegate_t2876249339 * ___divideByZeroDelegate2, ExceptionDelegate_t2876249339 * ___indexOutOfRangeDelegate3, ExceptionDelegate_t2876249339 * ___invalidCastDelegate4, ExceptionDelegate_t2876249339 * ___invalidOperationDelegate5, ExceptionDelegate_t2876249339 * ___ioDelegate6, ExceptionDelegate_t2876249339 * ___nullReferenceDelegate7, ExceptionDelegate_t2876249339 * ___outOfMemoryDelegate8, ExceptionDelegate_t2876249339 * ___overflowDelegate9, ExceptionDelegate_t2876249339 * ___systemExceptionDelegate10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m386473544 (Il2CppObject * __this /* static, unused */, ExceptionArgumentDelegate_t2443153790 * ___argumentDelegate0, ExceptionArgumentDelegate_t2443153790 * ___argumentNullDelegate1, ExceptionArgumentDelegate_t2443153790 * ___argumentOutOfRangeDelegate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationException::.ctor(System.String,System.Exception)
extern "C"  void ApplicationException__ctor_m856993678 (ApplicationException_t474868623 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern "C"  void SWIGPendingException_Set_m944203304 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArithmeticException::.ctor(System.String,System.Exception)
extern "C"  void ArithmeticException__ctor_m2264388592 (ArithmeticException_t3261462543 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DivideByZeroException::.ctor(System.String,System.Exception)
extern "C"  void DivideByZeroException__ctor_m4006599682 (DivideByZeroException_t1660837001 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String,System.Exception)
extern "C"  void IndexOutOfRangeException__ctor_m667958210 (IndexOutOfRangeException_t3527622107 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidCastException::.ctor(System.String,System.Exception)
extern "C"  void InvalidCastException__ctor_m3097122796 (InvalidCastException_t3625212209 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String,System.Exception)
extern "C"  void InvalidOperationException__ctor_m725121084 (InvalidOperationException_t721527559 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IOException::.ctor(System.String,System.Exception)
extern "C"  void IOException__ctor_m847281350 (IOException_t2458421087 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NullReferenceException::.ctor(System.String,System.Exception)
extern "C"  void NullReferenceException__ctor_m4167886794 (NullReferenceException_t3156209119 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.OutOfMemoryException::.ctor(System.String,System.Exception)
extern "C"  void OutOfMemoryException__ctor_m2926800194 (OutOfMemoryException_t1181064283 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.OverflowException::.ctor(System.String,System.Exception)
extern "C"  void OverflowException__ctor_m579335998 (OverflowException_t1075868493 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.SystemException::.ctor(System.String,System.Exception)
extern "C"  void SystemException__ctor_m3356086419 (SystemException_t3877406272 * __this, String_t* p0, Exception_t1927440687 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String,System.Exception)
extern "C"  void ArgumentException__ctor_m3312963299 (ArgumentException_t3259014390 * __this, String_t* p0, String_t* p1, Exception_t1927440687 * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
extern "C"  void ArgumentNullException__ctor_m2624491786 (ArgumentNullException_t628810857 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m4234257711 (ArgumentOutOfRangeException_t279959794 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern "C"  void ExceptionArgumentDelegate_Invoke_m3328736243 (ExceptionArgumentDelegate_t2443153790 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern "C"  void ExceptionDelegate_Invoke_m4439704 (ExceptionDelegate_t2876249339 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2136705809 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m687016595 (Il2CppObject * __this /* static, unused */, String_t* ___cString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIGStringDelegate__ctor_m579429766 (SWIGStringDelegate_t18001273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m4196108411 (Il2CppObject * __this /* static, unused */, SWIGStringDelegate_t18001273 * ___stringDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern "C"  String_t* SWIGStringDelegate_Invoke_m1812140305 (SWIGStringDelegate_t18001273 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::Dispose()
extern "C"  void FirebaseApp_Dispose_m3098692549 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::RemoveReference(Firebase.FirebaseApp)
extern "C"  void FirebaseApp_RemoveReference_m1475253910 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m4087144328 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C"  IntPtr_t HandleRef_get_Handle_m769981143 (HandleRef_t2419939847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m3044532593 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DestroyDelegate__ctor_m984049708 (DestroyDelegate_t3635929227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>::Remove(!0)
#define Dictionary_2_Remove_m196626582(__this, p0, method) ((  bool (*) (Dictionary_2_t652733044 *, IntPtr_t, const MethodInfo*))Dictionary_2_Remove_m4215165900_gshared)(__this, p0, method)
// System.String Firebase.FirebaseApp::get_Name()
extern "C"  String_t* FirebaseApp_get_Name_m3737532545 (FirebaseApp_t210707726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::Remove(!0)
#define Dictionary_2_Remove_m2831909098(__this, p0, method) ((  bool (*) (Dictionary_2_t2125486988 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m112127646_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::get_Item(!0)
#define Dictionary_2_get_Item_m3177056351(__this, p0, method) ((  int32_t (*) (Dictionary_2_t2513902766 *, IntPtr_t, const MethodInfo*))Dictionary_2_get_Item_m3177056351_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1952206578(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2513902766 *, IntPtr_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1952206578_gshared)(__this, p0, p1, method)
// System.Void Firebase.FirebaseApp/DestroyDelegate::Invoke()
extern "C"  void DestroyDelegate_Invoke_m3785470502 (DestroyDelegate_t3635929227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C"  void HandleRef__ctor_m1370162289 (HandleRef_t2419939847 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::get_Values()
#define Dictionary_2_get_Values_m373261557(__this, method) ((  ValueCollection_t828546831 * (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_get_Values_m2233445381_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Firebase.FirebaseApp>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1__ctor_m3695390092(__this, p0, method) ((  void (*) (List_1_t3874796154 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2848015482_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>::Clear()
#define Dictionary_2_Clear_m1059231906(__this, method) ((  void (*) (Dictionary_2_t652733044 *, const MethodInfo*))Dictionary_2_Clear_m3142955910_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::Clear()
#define Dictionary_2_Clear_m3658164290(__this, method) ((  void (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Firebase.FirebaseApp>::GetEnumerator()
#define List_1_GetEnumerator_m3866234593(__this, method) ((  Enumerator_t3409525828  (*) (List_1_t3874796154 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Firebase.FirebaseApp>::get_Current()
#define Enumerator_get_Current_m3438154773(__this, method) ((  FirebaseApp_t210707726 * (*) (Enumerator_t3409525828 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.FirebaseApp>::MoveNext()
#define Enumerator_MoveNext_m112670625(__this, method) ((  bool (*) (Enumerator_t3409525828 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.FirebaseApp>::Dispose()
#define Enumerator_Dispose_m1826530985(__this, method) ((  void (*) (Enumerator_t3409525828 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_Name_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_Name_get_m2545680430 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>::.ctor()
#define Dictionary_2__ctor_m2836329251(__this, method) ((  void (*) (Dictionary_2_t2125486988 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>::.ctor()
#define Dictionary_2__ctor_m1865841403(__this, method) ((  void (*) (Dictionary_2_t652733044 *, const MethodInfo*))Dictionary_2__ctor_m2049292515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::.ctor()
#define Dictionary_2__ctor_m186159185(__this, method) ((  void (*) (Dictionary_2_t2513902766 *, const MethodInfo*))Dictionary_2__ctor_m186159185_gshared)(__this, method)
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FirebaseApp(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FirebaseApp_m1137114670 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::LogMessage(Firebase.LogLevel,System.String)
extern "C"  void FirebaseHandler_LogMessage_m1734897910 (Il2CppObject * __this /* static, unused */, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m3989224144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtil_SetEnabledAllAppCallbacks_m2417423293 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void LogMessageDelegate__ctor_m1146820131 (LogMessageDelegate_t1988210674 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtil_SetLogFunction_m1947305073 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___arg00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtil_AppEnableLogCallback_m1418645513 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m962601984 (GameObject_t1756533147 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<Firebase.FirebaseApp/FirebaseHandler>()
#define GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967(__this, method) ((  FirebaseHandler_t2907300047 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2049635786_gshared)(__this, method)
// System.Void Firebase.UnitySynchronizationContext::Install(UnityEngine.GameObject)
extern "C"  void UnitySynchronizationContext_Install_m3862032013 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtil::PollCallbacks()
extern "C"  void AppUtil_PollCallbacks_m1948862416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp::EmptyAppDictionaries()
extern "C"  void FirebaseApp_EmptyAppDictionaries_m4102170024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::Invoke(Firebase.LogLevel,System.String)
extern "C"  void LogMessageDelegate_Invoke_m495697698 (LogMessageDelegate_t1988210674 * __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1730479323 (Attribute_t542643598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<System.Uri,Firebase.FirebaseHttpRequest>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3456221722(__this, p0, p1, method) ((  void (*) (Func_2_t1644515999 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, p0, p1, method)
// System.Void Firebase.UnityHttpRequest::.ctor(System.Uri)
extern "C"  void UnityHttpRequest__ctor_m1552832513 (UnityHttpRequest_t967994112 * __this, Uri_t19570940 * ___requestUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.UnityDownloadStream/BlockingCollection`1<System.Byte[]>::Add(T)
#define BlockingCollection_1_Add_m3386735512(__this, p0, method) ((  void (*) (BlockingCollection_1_t1283191913 *, ByteU5BU5D_t3397334013*, const MethodInfo*))BlockingCollection_1_Add_m2357368053_gshared)(__this, p0, method)
// System.Void System.IO.Stream::Dispose(System.Boolean)
extern "C"  void Stream_Dispose_m440254723 (Stream_t3255436806 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Firebase.UnityDownloadStream/BlockingCollection`1<System.Byte[]>::Take()
#define BlockingCollection_1_Take_m1432993353(__this, method) ((  ByteU5BU5D_t3397334013* (*) (BlockingCollection_1_t1283191913 *, const MethodInfo*))BlockingCollection_1_Take_m2773562012_gshared)(__this, method)
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m4290821911 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m3808317496 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, int32_t p1, Il2CppArray * p2, int32_t p3, int32_t p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameValueCollection::.ctor()
extern "C"  void NameValueCollection__ctor_m1767369537 (NameValueCollection_t3047564564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.MemoryStream::.ctor()
extern "C"  void MemoryStream__ctor_m1043059966 (MemoryStream_t743994179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ManualResetEvent::.ctor(System.Boolean)
extern "C"  void ManualResetEvent__ctor_m3470249043 (ManualResetEvent_t926074657 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseHttpRequest::.ctor(System.Uri)
extern "C"  void FirebaseHttpRequest__ctor_m280494807 (FirebaseHttpRequest_t1911360314 * __this, Uri_t19570940 * ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m1372024679(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const MethodInfo*))KeyValuePair_2_get_Key_m3385717033_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m1710042386(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const MethodInfo*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.Void System.Collections.Specialized.NameValueCollection::set_Item(System.String,System.String)
extern "C"  void NameValueCollection_set_Item_m3775607929 (NameValueCollection_t3047564564 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.EventWaitHandle::Set()
extern "C"  bool EventWaitHandle_Set_m2975776670 (EventWaitHandle_t2091316307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.UnityDownloadStream::CompleteContent()
extern "C"  void UnityDownloadStream_CompleteContent_m4053627945 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.UnityDownloadStream::ReceiveContentLength(System.Int32)
extern "C"  void UnityDownloadStream_ReceiveContentLength_m4185953370 (UnityDownloadStream_t3205965147 * __this, int32_t ___contentLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
#define Dictionary_2__ctor_m760167321(__this, method) ((  void (*) (Dictionary_2_t3943999495 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.String System.Int64::ToString()
extern "C"  String_t* Int64_ToString_m689375889 (int64_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m243759142(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3943999495 *, String_t*, String_t*, const MethodInfo*))Dictionary_2_Add_m4209421183_gshared)(__this, p0, p1, method)
// System.Void Firebase.UnityHttpRequest::RecordResult(System.Int32,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.String)
extern "C"  void UnityHttpRequest_RecordResult_m2645839245 (UnityHttpRequest_t967994112 * __this, int32_t ___rqResponseCode0, Il2CppObject* ___rqResponseHeaders1, String_t* ___rqError2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.UnityDownloadStream::ReceiveData(System.Byte[],System.Int32)
extern "C"  bool UnityDownloadStream_ReceiveData_m1845172324 (UnityDownloadStream_t3205965147 * __this, ByteU5BU5D_t3397334013* ___data0, int32_t ___dataLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::.ctor()
extern "C"  void SynchronizationContext__ctor_m1576930414 (SynchronizationContext_t3857790437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C"  Thread_t241561612 * Thread_get_CurrentThread_m3667342817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C"  int32_t Thread_get_ManagedThreadId_m1995754972 (Thread_t241561612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir>()
#define GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785(__this, method) ((  SynchronizationContextBehavoir_t692674473 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2049635786_gshared)(__this, method)
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::get_CallbackQueue()
extern "C"  Queue_1_t279829387 * SynchronizationContextBehavoir_get_CallbackQueue_m2312663759 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern "C"  void UnitySynchronizationContext__ctor_m1343069354 (UnitySynchronizationContext_t1051733884 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::get_Current()
extern "C"  SynchronizationContext_t3857790437 * SynchronizationContext_get_Current_m2617498083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::SetSynchronizationContext(System.Threading.SynchronizationContext)
extern "C"  void SynchronizationContext_SetSynchronizationContext_m951246343 (Il2CppObject * __this /* static, unused */, SynchronizationContext_t3857790437 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::.ctor(!0,!1)
#define Tuple_2__ctor_m4133476842(__this, p0, p1, method) ((  void (*) (Tuple_2_t460172552 *, SendOrPostCallback_t296893742 *, Il2CppObject *, const MethodInfo*))Tuple_2__ctor_m4275432376_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>::Enqueue(!0)
#define Queue_1_Enqueue_m2589358569(__this, p0, method) ((  void (*) (Queue_1_t279829387 *, Tuple_2_t460172552 *, const MethodInfo*))Queue_1_Enqueue_m4123136646_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.ManualResetEvent>::.ctor()
#define Dictionary_2__ctor_m446622171(__this, method) ((  void (*) (Dictionary_2_t4228867588 *, const MethodInfo*))Dictionary_2__ctor_m3997847064_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>::.ctor()
#define Queue_1__ctor_m4231618664(__this, method) ((  void (*) (Queue_1_t279829387 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3829161492 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>::get_Count()
#define Queue_1_get_Count_m177438226(__this, method) ((  int32_t (*) (Queue_1_t279829387 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// !0 System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>::Dequeue()
#define Queue_1_Dequeue_m1588528382(__this, method) ((  Tuple_2_t460172552 * (*) (Queue_1_t279829387 *, const MethodInfo*))Queue_1_Dequeue_m3320287779_gshared)(__this, method)
// !0 System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::get_Item1()
#define Tuple_2_get_Item1_m2647242292(__this, method) ((  SendOrPostCallback_t296893742 * (*) (Tuple_2_t460172552 *, const MethodInfo*))Tuple_2_get_Item1_m321736064_gshared)(__this, method)
// !1 System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>::get_Item2()
#define Tuple_2_get_Item2_m3295131920(__this, method) ((  Il2CppObject * (*) (Tuple_2_t460172552 *, const MethodInfo*))Tuple_2_get_Item2_m2379020804_gshared)(__this, method)
// System.Void System.Threading.SendOrPostCallback::Invoke(System.Object)
extern "C"  void SendOrPostCallback_Invoke_m4266976241 (SendOrPostCallback_t296893742 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.AppUtil::PollCallbacks()
extern "C"  void AppUtil_PollCallbacks_m1948862416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_PollCallbacks_m1948862416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_PollCallbacks_m784944262(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_0 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		return;
	}
}
// System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtil_AppEnableLogCallback_m1418645513 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_AppEnableLogCallback_m1418645513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___arg00;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m3057939313(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtil_SetEnabledAllAppCallbacks_m2417423293 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_SetEnabledAllAppCallbacks_m2417423293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___arg00;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m3339389901(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Firebase.AppUtil::SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtil_SetLogFunction_m1947305073 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___arg00, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_SetLogFunction_m1947305073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LogMessageDelegate_t1988210674 * L_0 = ___arg00;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_SetLogFunction_m3733654825(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE::.cctor()
extern "C"  void AppUtilPINVOKE__cctor_m1477430606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtilPINVOKE__cctor_m1477430606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGExceptionHelper_t1412130252 * L_0 = (SWIGExceptionHelper_t1412130252 *)il2cpp_codegen_object_new(SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var);
		SWIGExceptionHelper__ctor_m432919667(L_0, /*hidden argument*/NULL);
		((AppUtilPINVOKE_t3515465473_StaticFields*)AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var->static_fields)->set_swigExceptionHelper_0(L_0);
		SWIGStringHelper_t549488518 * L_1 = (SWIGStringHelper_t549488518 *)il2cpp_codegen_object_new(SWIGStringHelper_t549488518_il2cpp_TypeInfo_var);
		SWIGStringHelper__ctor_m1408369181(L_1, /*hidden argument*/NULL);
		((AppUtilPINVOKE_t3515465473_StaticFields*)AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var->static_fields)->set_swigStringHelper_1(L_1);
		return;
	}
}
extern "C" void DEFAULT_CALL Firebase_App_delete_FirebaseApp(void*);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FirebaseApp(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FirebaseApp_m1137114670 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_delete_FirebaseApp)(____jarg10_marshaled);

}
extern "C" char* DEFAULT_CALL Firebase_App_FirebaseApp_Name_get(void*);
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_Name_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_Name_get_m2545680430 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FirebaseApp_Name_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_App_PollCallbacks();
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_PollCallbacks()
extern "C"  void AppUtilPINVOKE_Firebase_App_PollCallbacks_m784944262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_PollCallbacks)();

}
extern "C" void DEFAULT_CALL Firebase_App_AppEnableLogCallback(int32_t);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m3057939313 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_AppEnableLogCallback)(static_cast<int32_t>(___jarg10));

}
extern "C" void DEFAULT_CALL Firebase_App_SetEnabledAllAppCallbacks(int32_t);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m3339389901 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_SetEnabledAllAppCallbacks)(static_cast<int32_t>(___jarg10));

}
extern "C" void DEFAULT_CALL Firebase_App_SetLogFunction(Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetLogFunction_m3733654825 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___jarg10' to native representation
	Il2CppMethodPointer ____jarg10_marshaled = NULL;
	____jarg10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___jarg10));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_SetLogFunction)(____jarg10_marshaled);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m1534282613(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingApplicationException_m1534282613(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m4135332127(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArithmeticException_m4135332127(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m2228906713(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIOException_m2228906713(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_m3154125439(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOverflowException_m3154125439(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m2519873314(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingSystemException_m2519873314(NULL, ____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_m3648622388(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentException_m3648622388(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.cctor()
extern "C"  void SWIGExceptionHelper__cctor_m2709485170 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper__cctor_m2709485170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_1 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_applicationDelegate_0(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_3 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_3, NULL, L_2, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_arithmeticDelegate_1(L_3);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_5 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_5, NULL, L_4, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_divideByZeroDelegate_2(L_5);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_7 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_7, NULL, L_6, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_indexOutOfRangeDelegate_3(L_7);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_9 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_9, NULL, L_8, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_invalidCastDelegate_4(L_9);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_11 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_11, NULL, L_10, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_invalidOperationDelegate_5(L_11);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingIOException_m2228906713_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_13 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_13, NULL, L_12, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_ioDelegate_6(L_13);
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_15 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_15, NULL, L_14, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_nullReferenceDelegate_7(L_15);
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_17 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_17, NULL, L_16, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_outOfMemoryDelegate_8(L_17);
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_19 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_19, NULL, L_18, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_overflowDelegate_9(L_19);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingSystemException_m2519873314_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_21 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_21, NULL, L_20, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_systemDelegate_10(L_21);
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MethodInfo_var);
		ExceptionArgumentDelegate_t2443153790 * L_23 = (ExceptionArgumentDelegate_t2443153790 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2902104247(L_23, NULL, L_22, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_argumentDelegate_11(L_23);
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MethodInfo_var);
		ExceptionArgumentDelegate_t2443153790 * L_25 = (ExceptionArgumentDelegate_t2443153790 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2902104247(L_25, NULL, L_24, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_argumentNullDelegate_12(L_25);
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MethodInfo_var);
		ExceptionArgumentDelegate_t2443153790 * L_27 = (ExceptionArgumentDelegate_t2443153790 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2902104247(L_27, NULL, L_26, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_argumentOutOfRangeDelegate_13(L_27);
		ExceptionDelegate_t2876249339 * L_28 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_applicationDelegate_0();
		ExceptionDelegate_t2876249339 * L_29 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_arithmeticDelegate_1();
		ExceptionDelegate_t2876249339 * L_30 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_divideByZeroDelegate_2();
		ExceptionDelegate_t2876249339 * L_31 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_indexOutOfRangeDelegate_3();
		ExceptionDelegate_t2876249339 * L_32 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_invalidCastDelegate_4();
		ExceptionDelegate_t2876249339 * L_33 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_invalidOperationDelegate_5();
		ExceptionDelegate_t2876249339 * L_34 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_ioDelegate_6();
		ExceptionDelegate_t2876249339 * L_35 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_nullReferenceDelegate_7();
		ExceptionDelegate_t2876249339 * L_36 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_outOfMemoryDelegate_8();
		ExceptionDelegate_t2876249339 * L_37 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_overflowDelegate_9();
		ExceptionDelegate_t2876249339 * L_38 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_systemDelegate_10();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m1809271624(NULL /*static, unused*/, L_28, L_29, L_30, L_31, L_32, L_33, L_34, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		ExceptionArgumentDelegate_t2443153790 * L_39 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_argumentDelegate_11();
		ExceptionArgumentDelegate_t2443153790 * L_40 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_argumentNullDelegate_12();
		ExceptionArgumentDelegate_t2443153790 * L_41 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_argumentOutOfRangeDelegate_13();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m386473544(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern "C"  void SWIGExceptionHelper__ctor_m432919667 (SWIGExceptionHelper_t1412130252 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacks_AppUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m1809271624 (Il2CppObject * __this /* static, unused */, ExceptionDelegate_t2876249339 * ___applicationDelegate0, ExceptionDelegate_t2876249339 * ___arithmeticDelegate1, ExceptionDelegate_t2876249339 * ___divideByZeroDelegate2, ExceptionDelegate_t2876249339 * ___indexOutOfRangeDelegate3, ExceptionDelegate_t2876249339 * ___invalidCastDelegate4, ExceptionDelegate_t2876249339 * ___invalidOperationDelegate5, ExceptionDelegate_t2876249339 * ___ioDelegate6, ExceptionDelegate_t2876249339 * ___nullReferenceDelegate7, ExceptionDelegate_t2876249339 * ___outOfMemoryDelegate8, ExceptionDelegate_t2876249339 * ___overflowDelegate9, ExceptionDelegate_t2876249339 * ___systemExceptionDelegate10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___applicationDelegate0' to native representation
	Il2CppMethodPointer ____applicationDelegate0_marshaled = NULL;
	____applicationDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___applicationDelegate0));

	// Marshaling of parameter '___arithmeticDelegate1' to native representation
	Il2CppMethodPointer ____arithmeticDelegate1_marshaled = NULL;
	____arithmeticDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___arithmeticDelegate1));

	// Marshaling of parameter '___divideByZeroDelegate2' to native representation
	Il2CppMethodPointer ____divideByZeroDelegate2_marshaled = NULL;
	____divideByZeroDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___divideByZeroDelegate2));

	// Marshaling of parameter '___indexOutOfRangeDelegate3' to native representation
	Il2CppMethodPointer ____indexOutOfRangeDelegate3_marshaled = NULL;
	____indexOutOfRangeDelegate3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___indexOutOfRangeDelegate3));

	// Marshaling of parameter '___invalidCastDelegate4' to native representation
	Il2CppMethodPointer ____invalidCastDelegate4_marshaled = NULL;
	____invalidCastDelegate4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___invalidCastDelegate4));

	// Marshaling of parameter '___invalidOperationDelegate5' to native representation
	Il2CppMethodPointer ____invalidOperationDelegate5_marshaled = NULL;
	____invalidOperationDelegate5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___invalidOperationDelegate5));

	// Marshaling of parameter '___ioDelegate6' to native representation
	Il2CppMethodPointer ____ioDelegate6_marshaled = NULL;
	____ioDelegate6_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___ioDelegate6));

	// Marshaling of parameter '___nullReferenceDelegate7' to native representation
	Il2CppMethodPointer ____nullReferenceDelegate7_marshaled = NULL;
	____nullReferenceDelegate7_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___nullReferenceDelegate7));

	// Marshaling of parameter '___outOfMemoryDelegate8' to native representation
	Il2CppMethodPointer ____outOfMemoryDelegate8_marshaled = NULL;
	____outOfMemoryDelegate8_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___outOfMemoryDelegate8));

	// Marshaling of parameter '___overflowDelegate9' to native representation
	Il2CppMethodPointer ____overflowDelegate9_marshaled = NULL;
	____overflowDelegate9_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___overflowDelegate9));

	// Marshaling of parameter '___systemExceptionDelegate10' to native representation
	Il2CppMethodPointer ____systemExceptionDelegate10_marshaled = NULL;
	____systemExceptionDelegate10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___systemExceptionDelegate10));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacks_AppUtil)(____applicationDelegate0_marshaled, ____arithmeticDelegate1_marshaled, ____divideByZeroDelegate2_marshaled, ____indexOutOfRangeDelegate3_marshaled, ____invalidCastDelegate4_marshaled, ____invalidOperationDelegate5_marshaled, ____ioDelegate6_marshaled, ____nullReferenceDelegate7_marshaled, ____outOfMemoryDelegate8_marshaled, ____overflowDelegate9_marshaled, ____systemExceptionDelegate10_marshaled);

}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacksArgument_AppUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m386473544 (Il2CppObject * __this /* static, unused */, ExceptionArgumentDelegate_t2443153790 * ___argumentDelegate0, ExceptionArgumentDelegate_t2443153790 * ___argumentNullDelegate1, ExceptionArgumentDelegate_t2443153790 * ___argumentOutOfRangeDelegate2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___argumentDelegate0' to native representation
	Il2CppMethodPointer ____argumentDelegate0_marshaled = NULL;
	____argumentDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentDelegate0));

	// Marshaling of parameter '___argumentNullDelegate1' to native representation
	Il2CppMethodPointer ____argumentNullDelegate1_marshaled = NULL;
	____argumentNullDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentNullDelegate1));

	// Marshaling of parameter '___argumentOutOfRangeDelegate2' to native representation
	Il2CppMethodPointer ____argumentOutOfRangeDelegate2_marshaled = NULL;
	____argumentOutOfRangeDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentOutOfRangeDelegate2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacksArgument_AppUtil)(____argumentDelegate0_marshaled, ____argumentNullDelegate1_marshaled, ____argumentOutOfRangeDelegate2_marshaled);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingApplicationException_m1534282613 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationException_t474868623 * L_2 = (ApplicationException_t474868623 *)il2cpp_codegen_object_new(ApplicationException_t474868623_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m856993678(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArithmeticException_m4135332127 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArithmeticException_t3261462543 * L_2 = (ArithmeticException_t3261462543 *)il2cpp_codegen_object_new(ArithmeticException_t3261462543_il2cpp_TypeInfo_var);
		ArithmeticException__ctor_m2264388592(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		DivideByZeroException_t1660837001 * L_2 = (DivideByZeroException_t1660837001 *)il2cpp_codegen_object_new(DivideByZeroException_t1660837001_il2cpp_TypeInfo_var);
		DivideByZeroException__ctor_m4006599682(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IndexOutOfRangeException_t3527622107 * L_2 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m667958210(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidCastException_t3625212209 * L_2 = (InvalidCastException_t3625212209 *)il2cpp_codegen_object_new(InvalidCastException_t3625212209_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m3097122796(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_2 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m725121084(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingIOException_m2228906713 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIOException_m2228906713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOException_t2458421087 * L_2 = (IOException_t2458421087 *)il2cpp_codegen_object_new(IOException_t2458421087_il2cpp_TypeInfo_var);
		IOException__ctor_m847281350(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m4167886794(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		OutOfMemoryException_t1181064283 * L_2 = (OutOfMemoryException_t1181064283 *)il2cpp_codegen_object_new(OutOfMemoryException_t1181064283_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m2926800194(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingOverflowException_m3154125439 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		OverflowException_t1075868493 * L_2 = (OverflowException_t1075868493 *)il2cpp_codegen_object_new(OverflowException_t1075868493_il2cpp_TypeInfo_var);
		OverflowException__ctor_m579335998(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern "C"  void SWIGExceptionHelper_SetPendingSystemException_m2519873314 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingSystemException_m2519873314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		SystemException_t3877406272 * L_2 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m3356086419(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentException_m3648622388 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___paramName1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3312963299(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t1927440687 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_2, _stringLiteral4083594583, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentNullException_t628810857 * L_8 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern "C"  void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t1927440687 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_2, _stringLiteral4083594583, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t2443153790 (ExceptionArgumentDelegate_t2443153790 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Marshaling of parameter '___paramName1' to native representation
	char* ____paramName1_marshaled = NULL;
	____paramName1_marshaled = il2cpp_codegen_marshal_string(___paramName1);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled, ____paramName1_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName1' native representation
	il2cpp_codegen_marshal_free(____paramName1_marshaled);
	____paramName1_marshaled = NULL;

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionArgumentDelegate__ctor_m2902104247 (ExceptionArgumentDelegate_t2443153790 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern "C"  void ExceptionArgumentDelegate_Invoke_m3328736243 (ExceptionArgumentDelegate_t2443153790 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionArgumentDelegate_Invoke_m3328736243((ExceptionArgumentDelegate_t2443153790 *)__this->get_prev_9(),___message0, ___paramName1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExceptionArgumentDelegate_BeginInvoke_m1234147944 (ExceptionArgumentDelegate_t2443153790 * __this, String_t* ___message0, String_t* ___paramName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___message0;
	__d_args[1] = ___paramName1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionArgumentDelegate_EndInvoke_m2954088285 (ExceptionArgumentDelegate_t2443153790 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ExceptionDelegate_t2876249339 (ExceptionDelegate_t2876249339 * __this, String_t* ___message0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionDelegate__ctor_m1540989664 (ExceptionDelegate_t2876249339 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern "C"  void ExceptionDelegate_Invoke_m4439704 (ExceptionDelegate_t2876249339 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionDelegate_Invoke_m4439704((ExceptionDelegate_t2876249339 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExceptionDelegate_BeginInvoke_m979767613 (ExceptionDelegate_t2876249339 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionDelegate_EndInvoke_m405205414 (ExceptionDelegate_t2876249339 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern "C"  bool SWIGPendingException_get_Pending_m1368465104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_get_Pending_m1368465104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0019:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern "C"  void SWIGPendingException_Set_m944203304 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Set_m944203304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1125746324, L_2, _stringLiteral372029317, /*hidden argument*/NULL);
		Exception_t1927440687 * L_4 = ___e0;
		ApplicationException_t474868623 * L_5 = (ApplicationException_t474868623 *)il2cpp_codegen_object_new(ApplicationException_t474868623_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m856993678(L_5, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002a:
	{
		Exception_t1927440687 * L_6 = ___e0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->set_pendingException_0(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppUtilPINVOKE_t3515465473_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_7;
		Il2CppObject * L_8 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_9 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->set_numExceptionsPending_1(((int32_t)((int32_t)L_9+(int32_t)1)));
		IL2CPP_LEAVE(0x59, FINALLY_0052);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0059:
	{
		return;
	}
}
// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern "C"  Exception_t1927440687 * SWIGPendingException_Retrieve_m197352211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Retrieve_m197352211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t1927440687 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_1)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_0 = L_2;
		((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->set_pendingException_0((Exception_t1927440687 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppUtilPINVOKE_t3515465473_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0034:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_5 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->set_numExceptionsPending_1(((int32_t)((int32_t)L_5-(int32_t)1)));
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004c:
	{
		Exception_t1927440687 * L_7 = V_0;
		return L_7;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::.cctor()
extern "C"  void SWIGPendingException__cctor_m4006952997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m687016595(char* ___cString0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___cString0' to managed representation
	String_t* ____cString0_unmarshaled = NULL;
	____cString0_unmarshaled = il2cpp_codegen_marshal_string_result(___cString0);

	// Managed method invocation
	String_t* returnValue = SWIGStringHelper_CreateString_m687016595(NULL, ____cString0_unmarshaled, NULL);

	// Marshaling of return value back from managed representation
	char* _returnValue_marshaled = NULL;
	_returnValue_marshaled = il2cpp_codegen_marshal_string(returnValue);

	return _returnValue_marshaled;
}
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.cctor()
extern "C"  void SWIGStringHelper__cctor_m3676208588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGStringHelper__cctor_m3676208588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SWIGStringHelper_CreateString_m687016595_MethodInfo_var);
		SWIGStringDelegate_t18001273 * L_1 = (SWIGStringDelegate_t18001273 *)il2cpp_codegen_object_new(SWIGStringDelegate_t18001273_il2cpp_TypeInfo_var);
		SWIGStringDelegate__ctor_m579429766(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGStringHelper_t549488518_StaticFields*)SWIGStringHelper_t549488518_il2cpp_TypeInfo_var->static_fields)->set_stringDelegate_0(L_1);
		SWIGStringDelegate_t18001273 * L_2 = ((SWIGStringHelper_t549488518_StaticFields*)SWIGStringHelper_t549488518_il2cpp_TypeInfo_var->static_fields)->get_stringDelegate_0();
		SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m4196108411(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1408369181 (SWIGStringHelper_t549488518 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterStringCallback_AppUtil(Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m4196108411 (Il2CppObject * __this /* static, unused */, SWIGStringDelegate_t18001273 * ___stringDelegate0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___stringDelegate0' to native representation
	Il2CppMethodPointer ____stringDelegate0_marshaled = NULL;
	____stringDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___stringDelegate0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterStringCallback_AppUtil)(____stringDelegate0_marshaled);

}
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m687016595 (Il2CppObject * __this /* static, unused */, String_t* ___cString0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___cString0;
		return L_0;
	}
}
extern "C"  String_t* DelegatePInvokeWrapper_SWIGStringDelegate_t18001273 (SWIGStringDelegate_t18001273 * __this, String_t* ___message0, const MethodInfo* method)
{
	typedef char* (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	char* returnValue = il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIGStringDelegate__ctor_m579429766 (SWIGStringDelegate_t18001273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern "C"  String_t* SWIGStringDelegate_Invoke_m1812140305 (SWIGStringDelegate_t18001273 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SWIGStringDelegate_Invoke_m1812140305((SWIGStringDelegate_t18001273 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef String_t* (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SWIGStringDelegate_BeginInvoke_m1506460307 (SWIGStringDelegate_t18001273 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern "C"  String_t* SWIGStringDelegate_EndInvoke_m3347905093 (SWIGStringDelegate_t18001273 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (String_t*)__result;
}
// System.Void Firebase.FirebaseApp::Finalize()
extern "C"  void FirebaseApp_Finalize_m3651549934 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_Finalize_m3651549934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FirebaseApp_Dispose_m3098692549(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_RemoveReference_m1475253910(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x18, FINALLY_0011);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0018:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp::Dispose()
extern "C"  void FirebaseApp_Dispose_m3098692549 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_Dispose_m3098692549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_2 = __this->get_address_of_swigCPtr_0();
			IntPtr_t L_3 = HandleRef_get_Handle_m769981143(L_2, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_5 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_005f;
			}
		}

IL_0026:
		{
			IntPtr_t L_6;
			L_6.set_m_value_0((void*)(void*)FirebaseApp_U3CDisposeU3Em__0_m3004522492_MethodInfo_var);
			DestroyDelegate_t3635929227 * L_7 = (DestroyDelegate_t3635929227 *)il2cpp_codegen_object_new(DestroyDelegate_t3635929227_il2cpp_TypeInfo_var);
			DestroyDelegate__ctor_m984049708(L_7, __this, L_6, /*hidden argument*/NULL);
			__this->set_destroy_5(L_7);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t652733044 * L_8 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrToProxy_3();
			HandleRef_t2419939847 * L_9 = __this->get_address_of_swigCPtr_0();
			IntPtr_t L_10 = HandleRef_get_Handle_m769981143(L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			Dictionary_2_Remove_m196626582(L_8, L_10, /*hidden argument*/Dictionary_2_Remove_m196626582_MethodInfo_var);
			Dictionary_2_t2125486988 * L_11 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			String_t* L_12 = FirebaseApp_get_Name_m3737532545(__this, /*hidden argument*/NULL);
			NullCheck(L_11);
			Dictionary_2_Remove_m2831909098(L_11, L_12, /*hidden argument*/Dictionary_2_Remove_m2831909098_MethodInfo_var);
		}

IL_005f:
		{
			IL2CPP_LEAVE(0x6B, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x6B, IL_006b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006b:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp::RemoveReference(Firebase.FirebaseApp)
extern "C"  void FirebaseApp_RemoveReference_m1475253910 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_RemoveReference_m1475253910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			FirebaseApp_t210707726 * L_2 = ___app0;
			NullCheck(L_2);
			HandleRef_t2419939847 * L_3 = L_2->get_address_of_swigCPtr_0();
			IntPtr_t L_4 = HandleRef_get_Handle_m769981143(L_3, /*hidden argument*/NULL);
			V_1 = L_4;
			IntPtr_t L_5 = V_1;
			IntPtr_t L_6 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_7 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0071;
			}
		}

IL_0028:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t2513902766 * L_8 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_9 = V_1;
			Dictionary_2_t2513902766 * L_10 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_11 = V_1;
			NullCheck(L_10);
			int32_t L_12 = Dictionary_2_get_Item_m3177056351(L_10, L_11, /*hidden argument*/Dictionary_2_get_Item_m3177056351_MethodInfo_var);
			NullCheck(L_8);
			Dictionary_2_set_Item_m1952206578(L_8, L_9, ((int32_t)((int32_t)L_12-(int32_t)1)), /*hidden argument*/Dictionary_2_set_Item_m1952206578_MethodInfo_var);
			Dictionary_2_t2513902766 * L_13 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_14 = V_1;
			NullCheck(L_13);
			int32_t L_15 = Dictionary_2_get_Item_m3177056351(L_13, L_14, /*hidden argument*/Dictionary_2_get_Item_m3177056351_MethodInfo_var);
			if (L_15)
			{
				goto IL_0071;
			}
		}

IL_0050:
		{
			FirebaseApp_t210707726 * L_16 = ___app0;
			NullCheck(L_16);
			bool L_17 = L_16->get_swigCMemOwn_1();
			if (!L_17)
			{
				goto IL_0071;
			}
		}

IL_005b:
		{
			FirebaseApp_t210707726 * L_18 = ___app0;
			NullCheck(L_18);
			DestroyDelegate_t3635929227 * L_19 = L_18->get_destroy_5();
			if (!L_19)
			{
				goto IL_0071;
			}
		}

IL_0066:
		{
			FirebaseApp_t210707726 * L_20 = ___app0;
			NullCheck(L_20);
			DestroyDelegate_t3635929227 * L_21 = L_20->get_destroy_5();
			NullCheck(L_21);
			DestroyDelegate_Invoke_m3785470502(L_21, /*hidden argument*/NULL);
		}

IL_0071:
		{
			FirebaseApp_t210707726 * L_22 = ___app0;
			NullCheck(L_22);
			L_22->set_swigCMemOwn_1((bool)0);
			FirebaseApp_t210707726 * L_23 = ___app0;
			IntPtr_t L_24 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_25;
			memset(&L_25, 0, sizeof(L_25));
			HandleRef__ctor_m1370162289(&L_25, NULL, L_24, /*hidden argument*/NULL);
			NullCheck(L_23);
			L_23->set_swigCPtr_0(L_25);
			IL2CPP_LEAVE(0x95, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_26 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0095:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp::EmptyAppDictionaries()
extern "C"  void FirebaseApp_EmptyAppDictionaries_m4102170024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_EmptyAppDictionaries_m4102170024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	List_1_t3874796154 * V_1 = NULL;
	FirebaseApp_t210707726 * V_2 = NULL;
	Enumerator_t3409525828  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t2125486988 * L_2 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			NullCheck(L_2);
			ValueCollection_t828546831 * L_3 = Dictionary_2_get_Values_m373261557(L_2, /*hidden argument*/Dictionary_2_get_Values_m373261557_MethodInfo_var);
			List_1_t3874796154 * L_4 = (List_1_t3874796154 *)il2cpp_codegen_object_new(List_1_t3874796154_il2cpp_TypeInfo_var);
			List_1__ctor_m3695390092(L_4, L_3, /*hidden argument*/List_1__ctor_m3695390092_MethodInfo_var);
			V_1 = L_4;
			Dictionary_2_t652733044 * L_5 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrToProxy_3();
			NullCheck(L_5);
			Dictionary_2_Clear_m1059231906(L_5, /*hidden argument*/Dictionary_2_Clear_m1059231906_MethodInfo_var);
			Dictionary_2_t2125486988 * L_6 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			NullCheck(L_6);
			Dictionary_2_Clear_m3658164290(L_6, /*hidden argument*/Dictionary_2_Clear_m3658164290_MethodInfo_var);
			List_1_t3874796154 * L_7 = V_1;
			NullCheck(L_7);
			Enumerator_t3409525828  L_8 = List_1_GetEnumerator_m3866234593(L_7, /*hidden argument*/List_1_GetEnumerator_m3866234593_MethodInfo_var);
			V_3 = L_8;
		}

IL_0037:
		try
		{ // begin try (depth: 2)
			{
				goto IL_004a;
			}

IL_003c:
			{
				FirebaseApp_t210707726 * L_9 = Enumerator_get_Current_m3438154773((&V_3), /*hidden argument*/Enumerator_get_Current_m3438154773_MethodInfo_var);
				V_2 = L_9;
				FirebaseApp_t210707726 * L_10 = V_2;
				NullCheck(L_10);
				FirebaseApp_Dispose_m3098692549(L_10, /*hidden argument*/NULL);
			}

IL_004a:
			{
				bool L_11 = Enumerator_MoveNext_m112670625((&V_3), /*hidden argument*/Enumerator_MoveNext_m112670625_MethodInfo_var);
				if (L_11)
				{
					goto IL_003c;
				}
			}

IL_0056:
			{
				IL2CPP_LEAVE(0x69, FINALLY_005b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_005b;
		}

FINALLY_005b:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m1826530985((&V_3), /*hidden argument*/Enumerator_Dispose_m1826530985_MethodInfo_var);
			IL2CPP_END_FINALLY(91)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(91)
		{
			IL2CPP_JUMP_TBL(0x69, IL_0069)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x75, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(110)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0075:
	{
		return;
	}
}
// System.String Firebase.FirebaseApp::get_Name()
extern "C"  String_t* FirebaseApp_get_Name_m3737532545 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_get_Name_m3737532545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		String_t* L_1 = AppUtilPINVOKE_Firebase_App_FirebaseApp_Name_get_m2545680430(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.FirebaseApp::.cctor()
extern "C"  void FirebaseApp__cctor_m2391020073 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp__cctor_m2391020073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2125486988 * L_0 = (Dictionary_2_t2125486988 *)il2cpp_codegen_object_new(Dictionary_2_t2125486988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2836329251(L_0, /*hidden argument*/Dictionary_2__ctor_m2836329251_MethodInfo_var);
		((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->set_nameToProxy_2(L_0);
		Dictionary_2_t652733044 * L_1 = (Dictionary_2_t652733044 *)il2cpp_codegen_object_new(Dictionary_2_t652733044_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1865841403(L_1, /*hidden argument*/Dictionary_2__ctor_m1865841403_MethodInfo_var);
		((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->set_cPtrToProxy_3(L_1);
		Dictionary_2_t2513902766 * L_2 = (Dictionary_2_t2513902766 *)il2cpp_codegen_object_new(Dictionary_2_t2513902766_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m186159185(L_2, /*hidden argument*/Dictionary_2__ctor_m186159185_MethodInfo_var);
		((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->set_cPtrRefCount_4(L_2);
		return;
	}
}
// System.Void Firebase.FirebaseApp::<Dispose>m__0()
extern "C"  void FirebaseApp_U3CDisposeU3Em__0_m3004522492 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_U3CDisposeU3Em__0_m3004522492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_delete_FirebaseApp_m1137114670(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_DestroyDelegate_t3635929227 (DestroyDelegate_t3635929227 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Firebase.FirebaseApp/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DestroyDelegate__ctor_m984049708 (DestroyDelegate_t3635929227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.FirebaseApp/DestroyDelegate::Invoke()
extern "C"  void DestroyDelegate_Invoke_m3785470502 (DestroyDelegate_t3635929227 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DestroyDelegate_Invoke_m3785470502((DestroyDelegate_t3635929227 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Firebase.FirebaseApp/DestroyDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DestroyDelegate_BeginInvoke_m3002495555 (DestroyDelegate_t3635929227 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Firebase.FirebaseApp/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DestroyDelegate_EndInvoke_m1272627102 (DestroyDelegate_t3635929227 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseHandler_LogMessage_m1734897910(int32_t ___log_level0, char* ___message1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message1' to managed representation
	String_t* ____message1_unmarshaled = NULL;
	____message1_unmarshaled = il2cpp_codegen_marshal_string_result(___message1);

	// Managed method invocation
	FirebaseHandler_LogMessage_m1734897910(NULL, ___log_level0, ____message1_unmarshaled, NULL);

}
// System.Void Firebase.FirebaseApp/FirebaseHandler::.ctor()
extern "C"  void FirebaseHandler__ctor_m1949563562 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::Create()
extern "C"  void FirebaseHandler_Create_m3248912600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHandler_Create_m3248912600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(FirebaseHandler_t2907300047_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			GameObject_t1756533147 * L_2 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_0026;
			}
		}

IL_0021:
		{
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		}

IL_0026:
		{
			int32_t L_4 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((int32_t)L_4) == ((int32_t)((int32_t)11))))
			{
				goto IL_0038;
			}
		}

IL_0032:
		{
			AppUtil_SetEnabledAllAppCallbacks_m2417423293(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			LogMessageDelegate_t1988210674 * L_5 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_3();
			if (L_5)
			{
				goto IL_0050;
			}
		}

IL_003f:
		{
			IntPtr_t L_6;
			L_6.set_m_value_0((void*)(void*)FirebaseHandler_LogMessage_m1734897910_MethodInfo_var);
			LogMessageDelegate_t1988210674 * L_7 = (LogMessageDelegate_t1988210674 *)il2cpp_codegen_object_new(LogMessageDelegate_t1988210674_il2cpp_TypeInfo_var);
			LogMessageDelegate__ctor_m1146820131(L_7, NULL, L_6, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_3(L_7);
		}

IL_0050:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			LogMessageDelegate_t1988210674 * L_8 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_3();
			AppUtil_SetLogFunction_m1947305073(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			AppUtil_AppEnableLogCallback_m1418645513(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_9 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
			GameObject__ctor_m962601984(L_9, _stringLiteral1742625763, /*hidden argument*/NULL);
			((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->set_firebaseHandler_2(L_9);
			GameObject_t1756533147 * L_10 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			NullCheck(L_10);
			GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967(L_10, /*hidden argument*/GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967_MethodInfo_var);
			GameObject_t1756533147 * L_11 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
			UnitySynchronizationContext_Install_m3862032013(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_12 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(147)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0x9A, IL_009a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009a:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::LogMessage(Firebase.LogLevel,System.String)
extern "C"  void FirebaseHandler_LogMessage_m1734897910 (Il2CppObject * __this /* static, unused */, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHandler_LogMessage_m1734897910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___log_level0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0023;
			}
			case 2:
			{
				goto IL_0023;
			}
			case 3:
			{
				goto IL_002e;
			}
			case 4:
			{
				goto IL_0039;
			}
			case 5:
			{
				goto IL_0044;
			}
		}
	}
	{
		goto IL_0049;
	}

IL_0023:
	{
		String_t* L_1 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0049;
	}

IL_002e:
	{
		String_t* L_2 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0049;
	}

IL_0039:
	{
		String_t* L_3 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0049;
	}

IL_0044:
	{
		goto IL_0049;
	}

IL_0049:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::Update()
extern "C"  void FirebaseHandler_Update_m164025069 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method)
{
	{
		AppUtil_PollCallbacks_m1948862416(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::OnApplicationQuit()
extern "C"  void FirebaseHandler_OnApplicationQuit_m2676139308 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHandler_OnApplicationQuit_m2676139308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->set_firebaseHandler_2((GameObject_t1756533147 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_EmptyAppDictionaries_m4102170024(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppUtil_AppEnableLogCallback_m1418645513(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		AppUtil_SetLogFunction_m1947305073(NULL /*static, unused*/, (LogMessageDelegate_t1988210674 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::.cctor()
extern "C"  void FirebaseHandler__cctor_m901621883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_LogMessageDelegate_t1988210674 (LogMessageDelegate_t1988210674 * __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message1' to native representation
	char* ____message1_marshaled = NULL;
	____message1_marshaled = il2cpp_codegen_marshal_string(___message1);

	// Native function invocation
	il2cppPInvokeFunc(___log_level0, ____message1_marshaled);

	// Marshaling cleanup of parameter '___message1' native representation
	il2cpp_codegen_marshal_free(____message1_marshaled);
	____message1_marshaled = NULL;

}
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void LogMessageDelegate__ctor_m1146820131 (LogMessageDelegate_t1988210674 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::Invoke(Firebase.LogLevel,System.String)
extern "C"  void LogMessageDelegate_Invoke_m495697698 (LogMessageDelegate_t1988210674 * __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogMessageDelegate_Invoke_m495697698((LogMessageDelegate_t1988210674 *)__this->get_prev_9(),___log_level0, ___message1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___log_level0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___log_level0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::BeginInvoke(Firebase.LogLevel,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LogMessageDelegate_BeginInvoke_m71829407 (LogMessageDelegate_t1988210674 * __this, int32_t ___log_level0, String_t* ___message1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogMessageDelegate_BeginInvoke_m71829407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(LogLevel_t543421840_il2cpp_TypeInfo_var, &___log_level0);
	__d_args[1] = ___message1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void LogMessageDelegate_EndInvoke_m368326581 (LogMessageDelegate_t1988210674 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.FirebaseApp/FirebaseHandler/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m287189802 (MonoPInvokeCallbackAttribute_t323097478 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseHttpRequest::.ctor(System.Uri)
extern "C"  void FirebaseHttpRequest__ctor_m280494807 (FirebaseHttpRequest_t1911360314 * __this, Uri_t19570940 * ___url0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Uri_t19570940 * L_0 = ___url0;
		__this->set__url_1(L_0);
		return;
	}
}
// System.Void Firebase.FirebaseHttpRequest::.cctor()
extern "C"  void FirebaseHttpRequest__cctor_m244392713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHttpRequest__cctor_m244392713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)FirebaseHttpRequest_U3C_factoryU3Em__0_m3260697191_MethodInfo_var);
		Func_2_t1644515999 * L_1 = (Func_2_t1644515999 *)il2cpp_codegen_object_new(Func_2_t1644515999_il2cpp_TypeInfo_var);
		Func_2__ctor_m3456221722(L_1, NULL, L_0, /*hidden argument*/Func_2__ctor_m3456221722_MethodInfo_var);
		((FirebaseHttpRequest_t1911360314_StaticFields*)FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var->static_fields)->set__factory_0(L_1);
		((FirebaseHttpRequest_t1911360314_StaticFields*)FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var->static_fields)->set_ContentLength_2(_stringLiteral677017058);
		((FirebaseHttpRequest_t1911360314_StaticFields*)FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var->static_fields)->set_Timeout_3(((int32_t)10000));
		((FirebaseHttpRequest_t1911360314_StaticFields*)FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var->static_fields)->set_NetworkUnavailable_4(((int32_t)-2));
		return;
	}
}
// Firebase.FirebaseHttpRequest Firebase.FirebaseHttpRequest::<_factory>m__0(System.Uri)
extern "C"  FirebaseHttpRequest_t1911360314 * FirebaseHttpRequest_U3C_factoryU3Em__0_m3260697191 (Il2CppObject * __this /* static, unused */, Uri_t19570940 * ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHttpRequest_U3C_factoryU3Em__0_m3260697191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Uri_t19570940 * L_0 = ___uri0;
		UnityHttpRequest_t967994112 * L_1 = (UnityHttpRequest_t967994112 *)il2cpp_codegen_object_new(UnityHttpRequest_t967994112_il2cpp_TypeInfo_var);
		UnityHttpRequest__ctor_m1552832513(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Firebase.UnityDownloadStream::ReceiveData(System.Byte[],System.Int32)
extern "C"  bool UnityDownloadStream_ReceiveData_m1845172324 (UnityDownloadStream_t3205965147 * __this, ByteU5BU5D_t3397334013* ___data0, int32_t ___dataLength1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_ReceiveData_m1845172324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BlockingCollection_1_t1283191913 * L_0 = __this->get__packetQueue_3();
		ByteU5BU5D_t3397334013* L_1 = ___data0;
		NullCheck(L_0);
		BlockingCollection_1_Add_m3386735512(L_0, L_1, /*hidden argument*/BlockingCollection_1_Add_m3386735512_MethodInfo_var);
		bool L_2 = __this->get__isDisposed_5();
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Firebase.UnityDownloadStream::CompleteContent()
extern "C"  void UnityDownloadStream_CompleteContent_m4053627945 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_CompleteContent_m4053627945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BlockingCollection_1_t1283191913 * L_0 = __this->get__packetQueue_3();
		NullCheck(L_0);
		BlockingCollection_1_Add_m3386735512(L_0, ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/BlockingCollection_1_Add_m3386735512_MethodInfo_var);
		return;
	}
}
// System.Void Firebase.UnityDownloadStream::ReceiveContentLength(System.Int32)
extern "C"  void UnityDownloadStream_ReceiveContentLength_m4185953370 (UnityDownloadStream_t3205965147 * __this, int32_t ___contentLength0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___contentLength0;
		__this->set__size_4((((int64_t)((int64_t)L_0))));
		return;
	}
}
// System.Void Firebase.UnityDownloadStream::Dispose(System.Boolean)
extern "C"  void UnityDownloadStream_Dispose_m1037433029 (UnityDownloadStream_t3205965147 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		__this->set__isDisposed_5((bool)1);
		bool L_0 = ___disposing0;
		Stream_Dispose_m440254723(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.UnityDownloadStream::Flush()
extern "C"  void UnityDownloadStream_Flush_m4079837961 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Firebase.UnityDownloadStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t UnityDownloadStream_Read_m3691296292 (UnityDownloadStream_t3205965147 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_Read_m3691296292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get__currentPacket_1();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		BlockingCollection_1_t1283191913 * L_1 = __this->get__packetQueue_3();
		NullCheck(L_1);
		ByteU5BU5D_t3397334013* L_2 = BlockingCollection_1_Take_m1432993353(L_1, /*hidden argument*/BlockingCollection_1_Take_m1432993353_MethodInfo_var);
		__this->set__currentPacket_1(L_2);
		__this->set__currentPacketPosition_2(0);
	}

IL_0023:
	{
		ByteU5BU5D_t3397334013* L_3 = __this->get__currentPacket_1();
		NullCheck(L_3);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))
		{
			goto IL_0032;
		}
	}
	{
		return 0;
	}

IL_0032:
	{
		int32_t L_4 = ___count2;
		ByteU5BU5D_t3397334013* L_5 = __this->get__currentPacket_1();
		NullCheck(L_5);
		int32_t L_6 = __this->get__currentPacketPosition_2();
		int32_t L_7 = Math_Min_m4290821911(NULL /*static, unused*/, L_4, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))-(int32_t)L_6)), /*hidden argument*/NULL);
		V_0 = L_7;
		ByteU5BU5D_t3397334013* L_8 = __this->get__currentPacket_1();
		int32_t L_9 = __this->get__currentPacketPosition_2();
		ByteU5BU5D_t3397334013* L_10 = ___buffer0;
		int32_t L_11 = ___offset1;
		int32_t L_12 = V_0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, L_9, (Il2CppArray *)(Il2CppArray *)L_10, L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = __this->get__currentPacketPosition_2();
		int32_t L_14 = V_0;
		__this->set__currentPacketPosition_2(((int32_t)((int32_t)L_13+(int32_t)L_14)));
		int32_t L_15 = __this->get__currentPacketPosition_2();
		ByteU5BU5D_t3397334013* L_16 = __this->get__currentPacket_1();
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0084;
		}
	}
	{
		__this->set__currentPacket_1((ByteU5BU5D_t3397334013*)NULL);
	}

IL_0084:
	{
		int32_t L_17 = V_0;
		return L_17;
	}
}
// System.Int64 Firebase.UnityDownloadStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t UnityDownloadStream_Seek_m56704085 (UnityDownloadStream_t3205965147 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_Seek_m56704085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Firebase.UnityDownloadStream::SetLength(System.Int64)
extern "C"  void UnityDownloadStream_SetLength_m2504080029 (UnityDownloadStream_t3205965147 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_SetLength_m2504080029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Firebase.UnityDownloadStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void UnityDownloadStream_Write_m3870116827 (UnityDownloadStream_t3205965147 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_Write_m3870116827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Firebase.UnityDownloadStream::get_CanRead()
extern "C"  bool UnityDownloadStream_get_CanRead_m3341972562 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Firebase.UnityDownloadStream::get_CanSeek()
extern "C"  bool UnityDownloadStream_get_CanSeek_m3140717254 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean Firebase.UnityDownloadStream::get_CanWrite()
extern "C"  bool UnityDownloadStream_get_CanWrite_m744089833 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int64 Firebase.UnityDownloadStream::get_Length()
extern "C"  int64_t UnityDownloadStream_get_Length_m3006658381 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get__size_4();
		return L_0;
	}
}
// System.Int64 Firebase.UnityDownloadStream::get_Position()
extern "C"  int64_t UnityDownloadStream_get_Position_m2552194688 (UnityDownloadStream_t3205965147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_get_Position_m2552194688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Firebase.UnityDownloadStream::set_Position(System.Int64)
extern "C"  void UnityDownloadStream_set_Position_m3826165111 (UnityDownloadStream_t3205965147 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityDownloadStream_set_Position_m3826165111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Firebase.UnityHttpRequest::.ctor(System.Uri)
extern "C"  void UnityHttpRequest__ctor_m1552832513 (UnityHttpRequest_t967994112 * __this, Uri_t19570940 * ___requestUrl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityHttpRequest__ctor_m1552832513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NameValueCollection_t3047564564 * L_0 = (NameValueCollection_t3047564564 *)il2cpp_codegen_object_new(NameValueCollection_t3047564564_il2cpp_TypeInfo_var);
		NameValueCollection__ctor_m1767369537(L_0, /*hidden argument*/NULL);
		__this->set__requestHeaders_5(L_0);
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_1, /*hidden argument*/NULL);
		__this->set__requestBody_6(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var);
		int32_t L_2 = ((FirebaseHttpRequest_t1911360314_StaticFields*)FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var->static_fields)->get_NetworkUnavailable_4();
		__this->set__responseCode_7((((int64_t)((int64_t)L_2))));
		NameValueCollection_t3047564564 * L_3 = (NameValueCollection_t3047564564 *)il2cpp_codegen_object_new(NameValueCollection_t3047564564_il2cpp_TypeInfo_var);
		NameValueCollection__ctor_m1767369537(L_3, /*hidden argument*/NULL);
		__this->set__responseHeaders_8(L_3);
		ManualResetEvent_t926074657 * L_4 = (ManualResetEvent_t926074657 *)il2cpp_codegen_object_new(ManualResetEvent_t926074657_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m3470249043(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set__sync_9(L_4);
		Uri_t19570940 * L_5 = ___requestUrl0;
		FirebaseHttpRequest__ctor_m280494807(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.UnityHttpRequest::RecordResult(System.Int32,System.Collections.Generic.IDictionary`2<System.String,System.String>,System.String)
extern "C"  void UnityHttpRequest_RecordResult_m2645839245 (UnityHttpRequest_t967994112 * __this, int32_t ___rqResponseCode0, Il2CppObject* ___rqResponseHeaders1, String_t* ___rqError2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityHttpRequest_RecordResult_m2645839245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	KeyValuePair_2_t1701344717  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Il2CppObject* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NameValueCollection_t3047564564 * L_0 = __this->get__responseHeaders_8();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___rqResponseCode0;
			__this->set__responseCode_7((((int64_t)((int64_t)L_2))));
			Il2CppObject* L_3 = ___rqResponseHeaders1;
			V_1 = L_3;
			Il2CppObject* L_4 = V_1;
			if (!L_4)
			{
				goto IL_0066;
			}
		}

IL_001d:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck(L_5);
			Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::GetEnumerator() */, IEnumerable_1_t1993471762_il2cpp_TypeInfo_var, L_5);
			V_3 = L_6;
		}

IL_0024:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0049;
			}

IL_0029:
			{
				Il2CppObject* L_7 = V_3;
				NullCheck(L_7);
				KeyValuePair_2_t1701344717  L_8 = InterfaceFuncInvoker0< KeyValuePair_2_t1701344717  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Current() */, IEnumerator_1_t3471835840_il2cpp_TypeInfo_var, L_7);
				V_2 = L_8;
				NameValueCollection_t3047564564 * L_9 = __this->get__responseHeaders_8();
				String_t* L_10 = KeyValuePair_2_get_Key_m1372024679((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
				String_t* L_11 = KeyValuePair_2_get_Value_m1710042386((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
				NullCheck(L_9);
				NameValueCollection_set_Item_m3775607929(L_9, L_10, L_11, /*hidden argument*/NULL);
			}

IL_0049:
			{
				Il2CppObject* L_12 = V_3;
				NullCheck(L_12);
				bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_12);
				if (L_13)
				{
					goto IL_0029;
				}
			}

IL_0054:
			{
				IL2CPP_LEAVE(0x66, FINALLY_0059);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0059;
		}

FINALLY_0059:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_14 = V_3;
				if (!L_14)
				{
					goto IL_0065;
				}
			}

IL_005f:
			{
				Il2CppObject* L_15 = V_3;
				NullCheck(L_15);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_15);
			}

IL_0065:
			{
				IL2CPP_END_FINALLY(89)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(89)
		{
			IL2CPP_JUMP_TBL(0x66, IL_0066)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0066:
		{
			String_t* L_16 = ___rqError2;
			__this->set__error_10(L_16);
			ManualResetEvent_t926074657 * L_17 = __this->get__sync_9();
			NullCheck(L_17);
			EventWaitHandle_Set_m2975776670(L_17, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x85, FINALLY_007e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007e;
	}

FINALLY_007e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_18 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(126)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(126)
	{
		IL2CPP_JUMP_TBL(0x85, IL_0085)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0085:
	{
		return;
	}
}
// System.Void Firebase.UnityHttpRequest/StreamingDownloadHandler::CompleteContent()
extern "C"  void StreamingDownloadHandler_CompleteContent_m3712720995 (StreamingDownloadHandler_t1548287308 * __this, const MethodInfo* method)
{
	{
		UnityDownloadStream_t3205965147 * L_0 = __this->get__stream_1();
		NullCheck(L_0);
		UnityDownloadStream_CompleteContent_m4053627945(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.UnityHttpRequest/StreamingDownloadHandler::ReceiveContentLength(System.Int32)
extern "C"  void StreamingDownloadHandler_ReceiveContentLength_m1673443460 (StreamingDownloadHandler_t1548287308 * __this, int32_t ___contentLength0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___contentLength0;
		__this->set__contentLength_4((((int64_t)((int64_t)L_0))));
		UnityDownloadStream_t3205965147 * L_1 = __this->get__stream_1();
		int32_t L_2 = ___contentLength0;
		NullCheck(L_1);
		UnityDownloadStream_ReceiveContentLength_m4185953370(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Firebase.UnityHttpRequest/StreamingDownloadHandler::ReceiveData(System.Byte[],System.Int32)
extern "C"  bool StreamingDownloadHandler_ReceiveData_m3730863066 (StreamingDownloadHandler_t1548287308 * __this, ByteU5BU5D_t3397334013* ___data0, int32_t ___dataLength1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StreamingDownloadHandler_ReceiveData_m3730863066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	{
		int64_t L_0 = __this->get__totalDownloaded_3();
		int32_t L_1 = ___dataLength1;
		__this->set__totalDownloaded_3(((int64_t)((int64_t)L_0+(int64_t)(((int64_t)((int64_t)L_1))))));
		int64_t L_2 = __this->get__totalDownloaded_3();
		if ((((int64_t)L_2) <= ((int64_t)(((int64_t)((int64_t)((int32_t)10240)))))))
		{
			goto IL_0054;
		}
	}
	{
		Dictionary_2_t3943999495 * L_3 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_3, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		V_0 = L_3;
		Dictionary_2_t3943999495 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var);
		String_t* L_5 = ((FirebaseHttpRequest_t1911360314_StaticFields*)FirebaseHttpRequest_t1911360314_il2cpp_TypeInfo_var->static_fields)->get_ContentLength_2();
		int64_t* L_6 = __this->get_address_of__contentLength_4();
		String_t* L_7 = Int64_ToString_m689375889(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Add_m243759142(L_4, L_5, L_7, /*hidden argument*/Dictionary_2_Add_m243759142_MethodInfo_var);
		UnityHttpRequest_t967994112 * L_8 = __this->get__fbRq_2();
		Dictionary_2_t3943999495 * L_9 = V_0;
		NullCheck(L_8);
		UnityHttpRequest_RecordResult_m2645839245(L_8, ((int32_t)200), L_9, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_0054:
	{
		UnityDownloadStream_t3205965147 * L_10 = __this->get__stream_1();
		ByteU5BU5D_t3397334013* L_11 = ___data0;
		int32_t L_12 = ___dataLength1;
		NullCheck(L_10);
		bool L_13 = UnityDownloadStream_ReceiveData_m1845172324(L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void Firebase.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern "C"  void UnitySynchronizationContext__ctor_m1343069354 (UnitySynchronizationContext_t1051733884 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext__ctor_m1343069354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SynchronizationContext__ctor_m1576930414(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t241561612_il2cpp_TypeInfo_var);
		Thread_t241561612 * L_0 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Thread_get_ManagedThreadId_m1995754972(L_0, /*hidden argument*/NULL);
		__this->set_mainThreadId_4(L_1);
		GameObject_t1756533147 * L_2 = ___gameObject0;
		NullCheck(L_2);
		SynchronizationContextBehavoir_t692674473 * L_3 = GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785(L_2, /*hidden argument*/GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785_MethodInfo_var);
		__this->set_behavior_3(L_3);
		SynchronizationContextBehavoir_t692674473 * L_4 = __this->get_behavior_3();
		NullCheck(L_4);
		Queue_1_t279829387 * L_5 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_4, /*hidden argument*/NULL);
		__this->set_queue_2(L_5);
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext::Install(UnityEngine.GameObject)
extern "C"  void UnitySynchronizationContext_Install_m3862032013 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_Install_m3862032013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext_t1051733884 * L_0 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		GameObject_t1756533147 * L_1 = ___gameObject0;
		UnitySynchronizationContext_t1051733884 * L_2 = (UnitySynchronizationContext_t1051733884 *)il2cpp_codegen_object_new(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext__ctor_m1343069354(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->set__instance_1(L_2);
	}

IL_0015:
	{
		SynchronizationContext_t3857790437 * L_3 = SynchronizationContext_get_Current_m2617498083(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext_t1051733884 * L_4 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		SynchronizationContext_SetSynchronizationContext_m951246343(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern "C"  void UnitySynchronizationContext_Post_m4267436977 (UnitySynchronizationContext_t1051733884 * __this, SendOrPostCallback_t296893742 * ___d0, Il2CppObject * ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_Post_m4267436977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t279829387 * L_0 = __this->get_queue_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t279829387 * L_2 = __this->get_queue_2();
		SendOrPostCallback_t296893742 * L_3 = ___d0;
		Il2CppObject * L_4 = ___state1;
		Tuple_2_t460172552 * L_5 = (Tuple_2_t460172552 *)il2cpp_codegen_object_new(Tuple_2_t460172552_il2cpp_TypeInfo_var);
		Tuple_2__ctor_m4133476842(L_5, L_3, L_4, /*hidden argument*/Tuple_2__ctor_m4133476842_MethodInfo_var);
		NullCheck(L_2);
		Queue_1_Enqueue_m2589358569(L_2, L_5, /*hidden argument*/Queue_1_Enqueue_m2589358569_MethodInfo_var);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext::.cctor()
extern "C"  void UnitySynchronizationContext__cctor_m3960838443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext__cctor_m3960838443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->set__instance_1((UnitySynchronizationContext_t1051733884 *)NULL);
		Dictionary_2_t4228867588 * L_0 = (Dictionary_2_t4228867588 *)il2cpp_codegen_object_new(Dictionary_2_t4228867588_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m446622171(L_0, /*hidden argument*/Dictionary_2__ctor_m446622171_MethodInfo_var);
		((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->set_signalDictionary_5(L_0);
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::.ctor()
extern "C"  void SynchronizationContextBehavoir__ctor_m3448616242 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::get_CallbackQueue()
extern "C"  Queue_1_t279829387 * SynchronizationContextBehavoir_get_CallbackQueue_m2312663759 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynchronizationContextBehavoir_get_CallbackQueue_m2312663759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Queue_1_t279829387 * L_0 = __this->get_callbackQueue_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Queue_1_t279829387 * L_1 = (Queue_1_t279829387 *)il2cpp_codegen_object_new(Queue_1_t279829387_il2cpp_TypeInfo_var);
		Queue_1__ctor_m4231618664(L_1, /*hidden argument*/Queue_1__ctor_m4231618664_MethodInfo_var);
		__this->set_callbackQueue_2(L_1);
	}

IL_0016:
	{
		Queue_1_t279829387 * L_2 = __this->get_callbackQueue_2();
		return L_2;
	}
}
// System.Collections.IEnumerator Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::Start()
extern "C"  Il2CppObject * SynchronizationContextBehavoir_Start_m3359373602 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynchronizationContextBehavoir_Start_m3359373602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t1310454857 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t1310454857 * L_0 = (U3CStartU3Ec__Iterator0_t1310454857 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t1310454857_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m3829161492(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t1310454857 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CStartU3Ec__Iterator0_t1310454857 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3829161492 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m4183249272 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m4183249272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00e8;
			}
		}
	}
	{
		goto IL_00f4;
	}

IL_0021:
	{
		__this->set_U3CentryU3E__0_0((Tuple_2_t460172552 *)NULL);
		SynchronizationContextBehavoir_t692674473 * L_2 = __this->get_U24this_2();
		NullCheck(L_2);
		Queue_1_t279829387 * L_3 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_2, /*hidden argument*/NULL);
		__this->set_U24locvar0_1(L_3);
		Il2CppObject * L_4 = __this->get_U24locvar0_1();
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			SynchronizationContextBehavoir_t692674473 * L_5 = __this->get_U24this_2();
			NullCheck(L_5);
			Queue_1_t279829387 * L_6 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			int32_t L_7 = Queue_1_get_Count_m177438226(L_6, /*hidden argument*/Queue_1_get_Count_m177438226_MethodInfo_var);
			if ((((int32_t)L_7) <= ((int32_t)0)))
			{
				goto IL_0070;
			}
		}

IL_005a:
		{
			SynchronizationContextBehavoir_t692674473 * L_8 = __this->get_U24this_2();
			NullCheck(L_8);
			Queue_1_t279829387 * L_9 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_8, /*hidden argument*/NULL);
			NullCheck(L_9);
			Tuple_2_t460172552 * L_10 = Queue_1_Dequeue_m1588528382(L_9, /*hidden argument*/Queue_1_Dequeue_m1588528382_MethodInfo_var);
			__this->set_U3CentryU3E__0_0(L_10);
		}

IL_0070:
		{
			IL2CPP_LEAVE(0x81, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = __this->get_U24locvar0_1();
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(117)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x81, IL_0081)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0081:
	{
		Tuple_2_t460172552 * L_12 = __this->get_U3CentryU3E__0_0();
		if (!L_12)
		{
			goto IL_00cd;
		}
	}
	{
		Tuple_2_t460172552 * L_13 = __this->get_U3CentryU3E__0_0();
		NullCheck(L_13);
		SendOrPostCallback_t296893742 * L_14 = Tuple_2_get_Item1_m2647242292(L_13, /*hidden argument*/Tuple_2_get_Item1_m2647242292_MethodInfo_var);
		if (!L_14)
		{
			goto IL_00cd;
		}
	}

IL_009c:
	try
	{ // begin try (depth: 1)
		Tuple_2_t460172552 * L_15 = __this->get_U3CentryU3E__0_0();
		NullCheck(L_15);
		SendOrPostCallback_t296893742 * L_16 = Tuple_2_get_Item1_m2647242292(L_15, /*hidden argument*/Tuple_2_get_Item1_m2647242292_MethodInfo_var);
		Tuple_2_t460172552 * L_17 = __this->get_U3CentryU3E__0_0();
		NullCheck(L_17);
		Il2CppObject * L_18 = Tuple_2_get_Item2_m3295131920(L_17, /*hidden argument*/Tuple_2_get_Item2_m3295131920_MethodInfo_var);
		NullCheck(L_16);
		SendOrPostCallback_Invoke_m4266976241(L_16, L_18, /*hidden argument*/NULL);
		goto IL_00cd;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_00bc;
		throw e;
	}

CATCH_00bc:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_00cd;
	} // end catch (depth: 1)

IL_00cd:
	{
		__this->set_U24current_3(NULL);
		bool L_21 = __this->get_U24disposing_4();
		if (L_21)
		{
			goto IL_00e3;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00e3:
	{
		goto IL_00f6;
	}

IL_00e8:
	{
		goto IL_0021;
	}
	// Dead block : IL_00ed: ldarg.0

IL_00f4:
	{
		return (bool)0;
	}

IL_00f6:
	{
		return (bool)1;
	}
}
// System.Object Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m222512858 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m225009554 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3034070173 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m4016881843 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m4016881843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m3537801838 (MonoPInvokeCallbackAttribute_t1970456718 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
