﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Firebase_Messaging_U3CModuleU3E3783534214.h"
#include "Firebase_Messaging_Firebase_Messaging_TokenReceive1675589073.h"
#include "mscorlib_System_String2029220233.h"

// Firebase.Messaging.TokenReceivedEventArgs
struct TokenReceivedEventArgs_t1675589073;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Firebase.Messaging.TokenReceivedEventArgs::get_Token()
extern "C"  String_t* TokenReceivedEventArgs_get_Token_m109762161 (TokenReceivedEventArgs_t1675589073 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTokenU3Ek__BackingField_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
