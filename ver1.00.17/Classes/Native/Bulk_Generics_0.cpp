﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_2_gen1383471493.h"
#include "AssemblyU2DCSharp_ConnectionAPISample_MessageTypes223757197.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar3092359764.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1263084566.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType0_2_gen3333162619.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType1_2_gen3010424510.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType2_3_gen3651135450.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare645512719.h"
#include "AssemblyU2DCSharp_U3CU3E__AnonType2_3_gen4235369943.h"
#include "AssemblyU2DCSharp_ApiMobilityManager_Serialization_588673888.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_ApiMobilityManager1284346316.h"
#include "AssemblyU2DCSharp_RecordingManager3075979958.h"
#include "AssemblyU2DCSharp_CreateManager3918627545.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_TutorialManager2168024773.h"
#include "AssemblyU2DCSharp_ApiUserManager2556825810.h"
#include "AssemblyU2DCSharp_ApiModuleManager_Serialization_11716271607.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager_ModuleMot3647087565.h"
#include "AssemblyU2DCSharp_ApiModuleManager3939700127.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager2960242393.h"
#include "AssemblyU2DCSharp_ApiReactionManager_Serialization1156466558.h"
#include "AssemblyU2DCSharp_ApiReactionManager2857272874.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen3108987245.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "Boo_Lang_Boo_Lang_List_1_U3CGetEnumeratorU3Ec__Ite3055131479.h"
#include "mscorlib_System_UInt322149682021.h"
#include "Boo_Lang_Boo_Lang_List_1_gen61287617.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2014832765.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1565154527.h"
#include "mscorlib_System_Double4078015681.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3853983590.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2691184179.h"
#include "mscorlib_System_Int64909078037.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen176588141.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3931788163.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen396335760.h"
#include "mscorlib_System_UInt642909196914.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1517212764.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1168894472.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813723.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1890955609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1441277371.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3730106434.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2567307023.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen52710985.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3807911007.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen272458604.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1393335608.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1045017316.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936567.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3925803634.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2833266637.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode2539919096.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice2468589887.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen456598886.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3659029185.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3160108754.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2067571757.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3199111798.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2106574801.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1705766462.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g613229465.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2082658550.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g990121553.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1186869890.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1648829745.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g556292748.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2937657178.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1845120181.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1905502397.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen102288586.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3304718885.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1672798003.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g580261006.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3065187631.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1972650634.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2158331857.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1622518059.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g529981062.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1635203449.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g542666452.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1108663466.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_ge16126469.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3261425374.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2168888377.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen3418705418.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4036277265.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen3423337902.h"
#include "AssemblyU2DCSharp_EnhancedUI_SmallList_1_gen4132056671.h"
#include "AssemblyU2DCSharp_EnhancedUI_SmallList_1_gen454661222.h"
#include "AssemblyU2DCSharp_EnhancedUI_SmallList_1_gen4136689155.h"
#include "AssemblyU2DCSharp_EnumString_1_gen4119734292.h"
#include "AssemblyU2DCSharp_EnumStringBase3052423213.h"
#include "AssemblyU2DCSharp_Localization_LocalizeKey3348421234.h"
#include "mscorlib_System_Nullable_1_gen1611487549.h"
#include "AssemblyU2DCSharp_EnumTableCache_1_gen3575987435.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge968233200.h"
#include "Firebase_App_Firebase_UnityDownloadStream_BlockingC575307195.h"
#include "System_System_Threading_Semaphore159839144.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "AssemblyU2DCSharp_GameObjectExtensions_U3CGetCompo1576971222.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_GameObjectExtensions_U3CGetCompon987727325.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HitBuilder_1_gen3202764555.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1037045868.h"
#include "AssemblyU2DCSharp_HypertextHelper_ObjectPool_1_gen2355888186.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "System_System_Collections_Generic_Stack_1_gen3777177449.h"
#include "AssemblyU2DCSharp_ListPool_1_gen2027455341.h"
#include "AssemblyU2DCSharp_ObjectPool_1_gen3678846538.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3425156178.h"
#include "AssemblyU2DCSharp_ListPool_1_gen542264864.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "AssemblyU2DCSharp_ObjectPool_1_gen2193656061.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1939965701.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3926562242.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2548729379.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3447138843.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterIm1381013311.h"
#include "AssemblyU2DCSharp_MotionManager_Serialization_1_ge1266378047.h"
#include "AssemblyU2DCSharp_MotionManager3855993783.h"
#include "AssemblyU2DCSharp_MotionManager_SerializationInWorl927334316.h"

// <>__AnonType0`2<ConnectionAPISample/MessageTypes,System.Object>
struct U3CU3E__AnonType0_2_t1383471493;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// <>__AnonType0`2<System.Object,System.Object>
struct U3CU3E__AnonType0_2_t3333162619;
// <>__AnonType1`2<System.Object,System.Object>
struct U3CU3E__AnonType1_2_t3010424510;
// <>__AnonType2`3<System.Object,System.Int32,System.Object>
struct U3CU3E__AnonType2_3_t3651135450;
// <>__AnonType2`3<System.Object,System.Object,System.Object>
struct U3CU3E__AnonType2_3_t4235369943;
// ApiMobilityManager/Serialization`1<System.Object>
struct Serialization_1_t588673888;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// ApiMobilityManager
struct ApiMobilityManager_t1284346316;
// RecordingManager
struct RecordingManager_t3075979958;
// CreateManager
struct CreateManager_t3918627545;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// TutorialManager
struct TutorialManager_t2168024773;
// ApiUserManager
struct ApiUserManager_t2556825810;
// ApiModuleManager/Serialization`1<System.Object>
struct Serialization_1_t1716271607;
// ApiModuleMotionManager/ModuleMotionSerialization`1<System.Object>
struct ModuleMotionSerialization_1_t3647087565;
// ApiModuleManager
struct ApiModuleManager_t3939700127;
// ApiModuleMotionManager
struct ApiModuleMotionManager_t2960242393;
// ApiReactionManager/Serialization`1<System.Object>
struct Serialization_1_t1156466558;
// ApiReactionManager
struct ApiReactionManager_t2857272874;
// Boo.Lang.GenericGenerator`1<System.Object>
struct GenericGenerator_1_t3108987245;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Type
struct Type_t;
// Boo.Lang.GenericGeneratorEnumerator`1<System.Object>
struct GenericGeneratorEnumerator_1_t3445420457;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator6_t3055131479;
// System.InvalidOperationException
struct InvalidOperationException_t721527559;
// Boo.Lang.List`1<System.Object>
struct List_1_t61287617;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3527622107;
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t2014832765;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// DG.Tweening.Core.DOGetter`1<System.Double>
struct DOGetter_1_t1565154527;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t3853983590;
// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t2691184179;
// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t176588141;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3858616074;
// DG.Tweening.Core.DOGetter`1<System.UInt32>
struct DOGetter_1_t3931788163;
// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t396335760;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t3802498217;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t1517212764;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t1168894472;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t4025813721;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t4025813722;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t4025813723;
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t1890955609;
// DG.Tweening.Core.DOSetter`1<System.Double>
struct DOSetter_1_t1441277371;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t3730106434;
// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t2567307023;
// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t52710985;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t3734738918;
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t3807911007;
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t272458604;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t3678621061;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1393335608;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1045017316;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3901936565;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t3901936566;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t3901936567;
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t3925803634;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// DG.Tweening.Tween
struct Tween_t278478013;
// DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t456598886;
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3160108754;
// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3199111798;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1705766462;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t2082658550;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct TweenerCore_3_t1648829745;
// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t2937657178;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t2998039394;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t102288586;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t1672798003;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t3065187631;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3250868854;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t1622518059;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1635203449;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1108663466;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3261425374;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t2833266637;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3659029185;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2067571757;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2106574801;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t613229465;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_t990121553;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t1186869890;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct ABSTweenPlugin_3_t556292748;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t1845120181;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t1905502397;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3304718885;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct ABSTweenPlugin_3_t580261006;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct ABSTweenPlugin_3_t1972650634;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2158331857;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t529981062;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct ABSTweenPlugin_3_t542666452;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t16126469;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2168888377;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3418705418;
// DG.Tweening.TweenCallback`1<System.Object>
struct TweenCallback_1_t4036277265;
// DG.Tweening.TweenCallback`1<System.Single>
struct TweenCallback_1_t3423337902;
// EnhancedUI.SmallList`1<System.Int32>
struct SmallList_1_t4132056671;
// EnhancedUI.SmallList`1<System.Object>
struct SmallList_1_t454661222;
// EnhancedUI.SmallList`1<System.Single>
struct SmallList_1_t4136689155;
// EnumString`1<Localization.LocalizeKey>
struct EnumString_1_t4119734292;
// EnumStringBase
struct EnumStringBase_t3052423213;
// Firebase.UnityDownloadStream/BlockingCollection`1<System.Object>
struct BlockingCollection_1_t575307195;
// System.Threading.Semaphore
struct Semaphore_t159839144;
// GameObjectExtensions/<GetComponentsInChildrenWithoutSelf>c__AnonStorey2`1<System.Object>
struct U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey2_1_t1576971222;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Object
struct Object_t1021602117;
// GameObjectExtensions/<GetComponentsInChildrenWithoutSelf>c__AnonStorey3`1<System.Object>
struct U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey3_1_t987727325;
// HitBuilder`1<System.Object>
struct HitBuilder_1_t3202764555;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1037045868;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// HypertextHelper.ObjectPool`1<System.Object>
struct ObjectPool_1_t2355888186;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// LitJson.ExporterFunc`1<System.Object>
struct ExporterFunc_1_t3926562242;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;
// LitJson.ImporterFunc`2<System.Object,System.Object>
struct ImporterFunc_2_t2548729379;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey0`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStorey0_1_t3447138843;
// LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStorey1_2_t1381013311;
// MotionManager/Serialization`1<System.Object>
struct Serialization_1_t1266378047;
// MotionManager
struct MotionManager_t3855993783;
// MotionManager/SerializationInWorldActionSetting`1<System.Object>
struct SerializationInWorldActionSetting_1_t927334316;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral1806356649;
extern Il2CppCodeGenString* _stringLiteral1382205792;
extern Il2CppCodeGenString* _stringLiteral1947253693;
extern const uint32_t U3CU3E__AnonType0_2_ToString_m3123070529_MetadataUsageId;
extern const uint32_t U3CU3E__AnonType0_2_ToString_m2057022320_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1278437986;
extern Il2CppCodeGenString* _stringLiteral286592186;
extern const uint32_t U3CU3E__AnonType1_2_ToString_m2226408529_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2856482426;
extern Il2CppCodeGenString* _stringLiteral947593626;
extern Il2CppCodeGenString* _stringLiteral1517356551;
extern const uint32_t U3CU3E__AnonType2_3_ToString_m1800031042_MetadataUsageId;
extern const uint32_t U3CU3E__AnonType2_3_ToString_m690164709_MetadataUsageId;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m1186797120_MethodInfo_var;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m369094862_MethodInfo_var;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m2875622201_MethodInfo_var;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m1552479802_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral890260275;
extern Il2CppCodeGenString* _stringLiteral3016923933;
extern const uint32_t Serialization_1__ctor_m1737129539_MetadataUsageId;
extern const uint32_t Serialization_1__ctor_m1355904736_MetadataUsageId;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m3975349435_MethodInfo_var;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m415312105_MethodInfo_var;
extern const uint32_t ModuleMotionSerialization_1__ctor_m3685629770_MetadataUsageId;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m539795794_MethodInfo_var;
extern const uint32_t Serialization_1__ctor_m727232031_MetadataUsageId;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral550572160;
extern const uint32_t GenericGenerator_1_ToString_m1222804785_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t GenericGeneratorEnumerator_1_Reset_m4255715191_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t GenericGeneratorEnumerator_1_YieldDefault_m799132706_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral850795601;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator6_MoveNext_m3471029548_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator6_Reset_m4037020951_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t List_1_ToString_m3353273637_MetadataUsageId;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Equals_m2416030142_MetadataUsageId;
extern const uint32_t List_1_Clear_m2512712227_MetadataUsageId;
extern const uint32_t List_1_IndexOf_m131628331_MetadataUsageId;
extern const uint32_t List_1_InnerRemoveAt_m467546350_MetadataUsageId;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern const uint32_t List_1_CheckIndex_m2149663805_MetadataUsageId;
extern const uint32_t List_1_Coerce_m3843713328_MetadataUsageId;
extern Il2CppClass* Color2_t232726623_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m736671164_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId;
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId;
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m681046145_MetadataUsageId;
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1803465319_MetadataUsageId;
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral552937255;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3004544632_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1422288232;
extern const uint32_t TweenerCore_3_ChangeValues_m266369415_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3163669337_MetadataUsageId;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1180323095_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1112518902_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2531960211_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2094047477_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2533780907_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3831419980_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2571270975_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3448887169_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1918761711_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2139817550_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m1168812923_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m1266531737_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1198377687_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3792476854_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m4189518099_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m1363945525_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m749792945_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3971101479_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m536230576_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m3838998779_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2015486485_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1849842838_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3084865231_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m114545046_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2802707620_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m4074697550_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m806842373_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m4204159460_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m621478568_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m418004407_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1875838422_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m570000243_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m1883068437_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2978185163_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3818406250_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m592431499_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m44760597_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3146716579_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m65087268_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2618008744_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2005239399_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3999115877_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2916579489_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2776308754_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m839318221_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2925494578_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3629558540_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m231619286_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1025968471_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m1594244980_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3855338832_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3640631583_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m508084142_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2621469739_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2299353629_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1012438200_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2708020849_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m3787576666_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3391320434_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1432615606_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3270859719_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2725288820_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m841334684_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m4171196319_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m217970222_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m624671403_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2965636509_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1407812639_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1286718126_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m868439851_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3552936477_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m140499649_MetadataUsageId;
extern const uint32_t TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId;
extern const uint32_t TweenCallback_1_BeginInvoke_m3843094324_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SmallList_1_ResizeArray_m3185937283_MetadataUsageId;
extern const uint32_t SmallList_1_First_m2594423969_MetadataUsageId;
extern const uint32_t SmallList_1_Last_m2505363309_MetadataUsageId;
extern const uint32_t SmallList_1_RemoveAt_m800828173_MetadataUsageId;
extern const uint32_t SmallList_1_Remove_m3611915957_MetadataUsageId;
extern const uint32_t SmallList_1_RemoveEnd_m3820750818_MetadataUsageId;
extern const uint32_t SmallList_1_ResizeArray_m1905725376_MetadataUsageId;
extern const uint32_t SmallList_1_First_m3892522670_MetadataUsageId;
extern const uint32_t SmallList_1_Last_m1188387254_MetadataUsageId;
extern const uint32_t SmallList_1_RemoveAt_m4084153238_MetadataUsageId;
extern const uint32_t SmallList_1_Remove_m1208117324_MetadataUsageId;
extern const uint32_t SmallList_1_RemoveEnd_m687006973_MetadataUsageId;
extern const uint32_t SmallList_1_ResizeArray_m4051651571_MetadataUsageId;
extern const uint32_t SmallList_1_First_m3377933817_MetadataUsageId;
extern const uint32_t SmallList_1_Last_m2122658061_MetadataUsageId;
extern const uint32_t SmallList_1_RemoveAt_m2570345085_MetadataUsageId;
extern const uint32_t SmallList_1_Remove_m3906016309_MetadataUsageId;
extern const uint32_t SmallList_1_RemoveEnd_m3791543898_MetadataUsageId;
extern const uint32_t EnumString_1_get_Type_m3947132266_MetadataUsageId;
extern Il2CppClass* Nullable_1_t1611487549_il2cpp_TypeInfo_var;
extern const uint32_t EnumString_1_set_Text_m193384397_MetadataUsageId;
extern const uint32_t EnumTableCache_1_ParseOrNull_m3764123207_MetadataUsageId;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t EnumTableCache_1_Init_m2830604264_MetadataUsageId;
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern const uint32_t BlockingCollection_1_Take_m2773562012_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey2_1_U3CU3Em__0_m261577356_MetadataUsageId;
extern const uint32_t U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey3_1_U3CU3Em__0_m2203408679_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t1037045868_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3038559641_MethodInfo_var;
extern const uint32_t HitBuilder_1__ctor_m1281725022_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Add_m2434562209_MethodInfo_var;
extern const uint32_t HitBuilder_1_SetCustomDimension_m55290377_MetadataUsageId;
extern const uint32_t HitBuilder_1_SetCustomMetric_m3149080737_MetadataUsageId;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral828239950;
extern const uint32_t HitBuilder_1_SetCampaignSource_m3534791218_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral273729679;
extern const uint32_t ObjectPool_1_Release_m1415969117_MetadataUsageId;
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m4180938475_MethodInfo_var;
extern const uint32_t Serialization_1__ctor_m281877202_MetadataUsageId;
extern const uint32_t SerializationInWorldActionSetting_1__ctor_m1694703141_MetadataUsageId;

// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t577127397  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};


// T SingletonMonoBehaviour`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t232726623  DOGetter_1_Invoke_m1748194493_gshared (DOGetter_1_t2014832765 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m4107339380_gshared (DOGetter_1_t1565154527 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m2109831897_gshared (DOGetter_1_t3853983590 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m111587642_gshared (DOGetter_1_t2691184179 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m882816534_gshared (DOGetter_1_t176588141 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m3647275797_gshared (DOGetter_1_t3858616074 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m370264108_gshared (DOGetter_1_t3931788163 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m2202291235_gshared (DOGetter_1_t396335760 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t2020392075  DOGetter_1_Invoke_m222071538_gshared (DOGetter_1_t3802498217 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t4030073918  DOGetter_1_Invoke_m1844179403_gshared (DOGetter_1_t1517212764 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t3681755626  DOGetter_1_Invoke_m205270839_gshared (DOGetter_1_t1168894472 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t2243707579  DOGetter_1_Invoke_m3450181142_gshared (DOGetter_1_t4025813721 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t2243707580  DOGetter_1_Invoke_m3486946741_gshared (DOGetter_1_t4025813722 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t2243707581  DOGetter_1_Invoke_m3680105936_gshared (DOGetter_1_t4025813723 * __this, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3163525136_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1726747861_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m748616666_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2393688041_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1999305269_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m184768384_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m236045679_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4121538054_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3892954541_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m903072570_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1386366628_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2956451219_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4169715988_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m212800661_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3615052821_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m872757678_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Single>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3090779465_gshared (TweenCallback_1_t3423337902 * __this, float ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<Localization.LocalizeKey>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3487298155_gshared (Nullable_1_t1611487549 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<Localization.LocalizeKey>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m2203533598_gshared (Nullable_1_t1611487549 * __this, int32_t p0, const MethodInfo* method);
// !0 System.Nullable`1<Localization.LocalizeKey>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1727227211_gshared (Nullable_1_t1611487549 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3997847064_gshared (Dictionary_2_t1697274930 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1296007576_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void LitJson.ExporterFunc`1<System.Object>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m1089798616_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::Invoke(TJson)
extern "C"  Il2CppObject * ImporterFunc_2_Invoke_m3373032134_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___input0, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m626692867 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T SingletonMonoBehaviour`1<ApiMobilityManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m1186797120(__this /* static, unused */, method) ((  ApiMobilityManager_t1284346316 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// T SingletonMonoBehaviour`1<RecordingManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m369094862(__this /* static, unused */, method) ((  RecordingManager_t3075979958 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// T SingletonMonoBehaviour`1<CreateManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m3710663101(__this /* static, unused */, method) ((  CreateManager_t3918627545 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2243707580  Transform_get_localScale_m3074381503 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T SingletonMonoBehaviour`1<TutorialManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m2875622201(__this /* static, unused */, method) ((  TutorialManager_t2168024773 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// T SingletonMonoBehaviour`1<ApiUserManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m1552479802(__this /* static, unused */, method) ((  ApiUserManager_t2556825810 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// T SingletonMonoBehaviour`1<ApiModuleManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m3975349435(__this /* static, unused */, method) ((  ApiModuleManager_t3939700127 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// T SingletonMonoBehaviour`1<ApiModuleMotionManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m415312105(__this /* static, unused */, method) ((  ApiModuleMotionManager_t2960242393 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// T SingletonMonoBehaviour`1<ApiReactionManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m539795794(__this /* static, unused */, method) ((  ApiReactionManager_t2857272874 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2024975688 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m2801133788 (InvalidOperationException_t721527559 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m3808317496 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, int32_t p1, Il2CppArray * p2, int32_t p3, int32_t p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boo.Lang.Builtins::join(System.Collections.IEnumerable,System.String)
extern "C"  String_t* Builtins_join_m2036613869 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerable0, String_t* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m3900584722 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boo.Lang.Runtime.RuntimeServices::EqualityOperator(System.Object,System.Object)
extern "C"  bool RuntimeServices_EqualityOperator_m2233200645 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___lhs0, Il2CppObject * ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C"  int32_t Math_Max_m2671311541 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor()
extern "C"  void IndexOutOfRangeException__ctor_m3497760912 (IndexOutOfRangeException_t3527622107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Boo.Lang.Runtime.RuntimeServices::Coerce(System.Object,System.Type)
extern "C"  Il2CppObject * RuntimeServices_Coerce_m43784504 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___toType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
#define DOGetter_1_Invoke_m1748194493(__this, method) ((  Color2_t232726623  (*) (DOGetter_1_t2014832765 *, const MethodInfo*))DOGetter_1_Invoke_m1748194493_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
#define DOGetter_1_Invoke_m4107339380(__this, method) ((  double (*) (DOGetter_1_t1565154527 *, const MethodInfo*))DOGetter_1_Invoke_m4107339380_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
#define DOGetter_1_Invoke_m2109831897(__this, method) ((  int32_t (*) (DOGetter_1_t3853983590 *, const MethodInfo*))DOGetter_1_Invoke_m2109831897_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
#define DOGetter_1_Invoke_m111587642(__this, method) ((  int64_t (*) (DOGetter_1_t2691184179 *, const MethodInfo*))DOGetter_1_Invoke_m111587642_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
#define DOGetter_1_Invoke_m882816534(__this, method) ((  Il2CppObject * (*) (DOGetter_1_t176588141 *, const MethodInfo*))DOGetter_1_Invoke_m882816534_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
#define DOGetter_1_Invoke_m3647275797(__this, method) ((  float (*) (DOGetter_1_t3858616074 *, const MethodInfo*))DOGetter_1_Invoke_m3647275797_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
#define DOGetter_1_Invoke_m370264108(__this, method) ((  uint32_t (*) (DOGetter_1_t3931788163 *, const MethodInfo*))DOGetter_1_Invoke_m370264108_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
#define DOGetter_1_Invoke_m2202291235(__this, method) ((  uint64_t (*) (DOGetter_1_t396335760 *, const MethodInfo*))DOGetter_1_Invoke_m2202291235_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
#define DOGetter_1_Invoke_m222071538(__this, method) ((  Color_t2020392075  (*) (DOGetter_1_t3802498217 *, const MethodInfo*))DOGetter_1_Invoke_m222071538_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
#define DOGetter_1_Invoke_m1844179403(__this, method) ((  Quaternion_t4030073918  (*) (DOGetter_1_t1517212764 *, const MethodInfo*))DOGetter_1_Invoke_m1844179403_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
#define DOGetter_1_Invoke_m205270839(__this, method) ((  Rect_t3681755626  (*) (DOGetter_1_t1168894472 *, const MethodInfo*))DOGetter_1_Invoke_m205270839_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
#define DOGetter_1_Invoke_m3450181142(__this, method) ((  Vector2_t2243707579  (*) (DOGetter_1_t4025813721 *, const MethodInfo*))DOGetter_1_Invoke_m3450181142_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
#define DOGetter_1_Invoke_m3486946741(__this, method) ((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))DOGetter_1_Invoke_m3486946741_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
#define DOGetter_1_Invoke_m3680105936(__this, method) ((  Vector4_t2243707581  (*) (DOGetter_1_t4025813723 *, const MethodInfo*))DOGetter_1_Invoke_m3680105936_gshared)(__this, method)
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
#define DOSetter_1_Invoke_m3163525136(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1890955609 *, Color2_t232726623 , const MethodInfo*))DOSetter_1_Invoke_m3163525136_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
#define DOSetter_1_Invoke_m1726747861(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1441277371 *, double, const MethodInfo*))DOSetter_1_Invoke_m1726747861_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
#define DOSetter_1_Invoke_m748616666(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3730106434 *, int32_t, const MethodInfo*))DOSetter_1_Invoke_m748616666_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
#define DOSetter_1_Invoke_m2393688041(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t2567307023 *, int64_t, const MethodInfo*))DOSetter_1_Invoke_m2393688041_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
#define DOSetter_1_Invoke_m1999305269(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t52710985 *, Il2CppObject *, const MethodInfo*))DOSetter_1_Invoke_m1999305269_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
#define DOSetter_1_Invoke_m184768384(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3734738918 *, float, const MethodInfo*))DOSetter_1_Invoke_m184768384_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
#define DOSetter_1_Invoke_m236045679(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3807911007 *, uint32_t, const MethodInfo*))DOSetter_1_Invoke_m236045679_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
#define DOSetter_1_Invoke_m4121538054(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t272458604 *, uint64_t, const MethodInfo*))DOSetter_1_Invoke_m4121538054_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
#define DOSetter_1_Invoke_m3892954541(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3678621061 *, Color_t2020392075 , const MethodInfo*))DOSetter_1_Invoke_m3892954541_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
#define DOSetter_1_Invoke_m903072570(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1393335608 *, Quaternion_t4030073918 , const MethodInfo*))DOSetter_1_Invoke_m903072570_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
#define DOSetter_1_Invoke_m1386366628(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1045017316 *, Rect_t3681755626 , const MethodInfo*))DOSetter_1_Invoke_m1386366628_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
#define DOSetter_1_Invoke_m2956451219(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936565 *, Vector2_t2243707579 , const MethodInfo*))DOSetter_1_Invoke_m2956451219_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
#define DOSetter_1_Invoke_m4169715988(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936566 *, Vector3_t2243707580 , const MethodInfo*))DOSetter_1_Invoke_m4169715988_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
#define DOSetter_1_Invoke_m212800661(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936567 *, Vector4_t2243707581 , const MethodInfo*))DOSetter_1_Invoke_m212800661_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Tweener::.ctor()
extern "C"  void Tweener__ctor_m102043033 (Tweener_t760404022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern "C"  void Debugger_LogWarning_m1294243619 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Tween::Reset()
extern "C"  void Tween_Reset_m2158413979 (Tween_t278478013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
#define TweenCallback_1_Invoke_m3615052821(__this, ___value0, method) ((  void (*) (TweenCallback_1_t3418705418 *, int32_t, const MethodInfo*))TweenCallback_1_Invoke_m3615052821_gshared)(__this, ___value0, method)
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
#define TweenCallback_1_Invoke_m872757678(__this, ___value0, method) ((  void (*) (TweenCallback_1_t4036277265 *, Il2CppObject *, const MethodInfo*))TweenCallback_1_Invoke_m872757678_gshared)(__this, ___value0, method)
// System.Void DG.Tweening.TweenCallback`1<System.Single>::Invoke(T)
#define TweenCallback_1_Invoke_m3090779465(__this, ___value0, method) ((  void (*) (TweenCallback_1_t3423337902 *, float, const MethodInfo*))TweenCallback_1_Invoke_m3090779465_gshared)(__this, ___value0, method)
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m1875893177 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::CopyTo(System.Array,System.Int32)
extern "C"  void Array_CopyTo_m4061033315 (Il2CppArray * __this, Il2CppArray * p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Int32::Equals(System.Object)
extern "C"  bool Int32_Equals_m753832628 (int32_t* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Object)
extern "C"  bool Single_Equals_m3679433096 (float* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnumStringBase::.ctor()
extern "C"  void EnumStringBase__ctor_m4120175636 (EnumStringBase_t3052423213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EnumStringBase::get_Text()
extern "C"  String_t* EnumStringBase_get_Text_m1884861569 (EnumStringBase_t3052423213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnumStringBase::set_Text(System.String)
extern "C"  void EnumStringBase_set_Text_m2870456262 (EnumStringBase_t3052423213 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Nullable`1<Localization.LocalizeKey>::get_HasValue()
#define Nullable_1_get_HasValue_m3487298155(__this, method) ((  bool (*) (Nullable_1_t1611487549 *, const MethodInfo*))Nullable_1_get_HasValue_m3487298155_gshared)(__this, method)
// System.Void System.Nullable`1<Localization.LocalizeKey>::.ctor(!0)
#define Nullable_1__ctor_m2203533598(__this, p0, method) ((  void (*) (Nullable_1_t1611487549 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2203533598_gshared)(__this, p0, method)
// !0 System.Nullable`1<Localization.LocalizeKey>::get_Value()
#define Nullable_1_get_Value_m1727227211(__this, method) ((  int32_t (*) (Nullable_1_t1611487549 *, const MethodInfo*))Nullable_1_get_Value_m1727227211_gshared)(__this, method)
// System.Boolean System.Type::get_IsEnum()
extern "C"  bool Type_get_IsEnum_m313908919 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array System.Enum::GetValues(System.Type)
extern "C"  Il2CppArray * Enum_GetValues_m2107059536 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m1498215565 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Array::GetEnumerator()
extern "C"  Il2CppObject * Array_GetEnumerator_m2284404958 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2136705809 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Semaphore::Release(System.Int32)
extern "C"  int32_t Semaphore_Release_m1642555081 (Semaphore_t159839144 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.String>::.ctor()
#define Dictionary_2__ctor_m3038559641(__this, method) ((  void (*) (Dictionary_2_t1037045868 *, const MethodInfo*))Dictionary_2__ctor_m3997847064_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m2434562209(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1037045868 *, int32_t, String_t*, const MethodInfo*))Dictionary_2_Add_m1296007576_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.ExporterFunc`1<System.Object>::Invoke(T,LitJson.JsonWriter)
#define ExporterFunc_1_Invoke_m1089798616(__this, ___obj0, ___writer1, method) ((  void (*) (ExporterFunc_1_t3926562242 *, Il2CppObject *, JsonWriter_t1927598499 *, const MethodInfo*))ExporterFunc_1_Invoke_m1089798616_gshared)(__this, ___obj0, ___writer1, method)
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::Invoke(TJson)
#define ImporterFunc_2_Invoke_m3373032134(__this, ___input0, method) ((  Il2CppObject * (*) (ImporterFunc_2_t2548729379 *, Il2CppObject *, const MethodInfo*))ImporterFunc_2_Invoke_m3373032134_gshared)(__this, ___input0, method)
// T SingletonMonoBehaviour`1<MotionManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m4180938475(__this /* static, unused */, method) ((  MotionManager_t3855993783 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m3908763980_gshared)(__this /* static, unused */, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <>__AnonType0`2<ConnectionAPISample/MessageTypes,System.Object>::.ctor(<Type>__T,<Value>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m2038307413_gshared (U3CU3E__AnonType0_2_t1383471493 * __this, int32_t ___Type0, Il2CppObject * ___Value1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___Type0;
		__this->set_U3CTypeU3E_0(L_0);
		Il2CppObject * L_1 = ___Value1;
		__this->set_U3CValueU3E_1(L_1);
		return;
	}
}
// <Type>__T <>__AnonType0`2<ConnectionAPISample/MessageTypes,System.Object>::get_Type()
extern "C"  int32_t U3CU3E__AnonType0_2_get_Type_m76673024_gshared (U3CU3E__AnonType0_2_t1383471493 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CTypeU3E_0();
		return L_0;
	}
}
// <Value>__T <>__AnonType0`2<ConnectionAPISample/MessageTypes,System.Object>::get_Value()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Value_m167668938_gshared (U3CU3E__AnonType0_2_t1383471493 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType0`2<ConnectionAPISample/MessageTypes,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m142196415_gshared (U3CU3E__AnonType0_2_t1383471493 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType0_2_t1383471493 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType0_2_t1383471493 *)((U3CU3E__AnonType0_2_t1383471493 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		U3CU3E__AnonType0_2_t1383471493 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t3092359764 * L_2 = ((  EqualityComparer_1_t3092359764 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_3 = (int32_t)__this->get_U3CTypeU3E_0();
		U3CU3E__AnonType0_2_t1383471493 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_U3CTypeU3E_0();
		NullCheck((EqualityComparer_1_t3092359764 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<ConnectionAPISample/MessageTypes>::Equals(!0,!0) */, (EqualityComparer_1_t3092359764 *)L_2, (int32_t)L_3, (int32_t)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_7 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		U3CU3E__AnonType0_2_t1383471493 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CValueU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType0`2<ConnectionAPISample/MessageTypes,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m2588248233_gshared (U3CU3E__AnonType0_2_t1383471493 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t3092359764 * L_0 = ((  EqualityComparer_1_t3092359764 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_1 = (int32_t)__this->get_U3CTypeU3E_0();
		NullCheck((EqualityComparer_1_t3092359764 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<ConnectionAPISample/MessageTypes>::GetHashCode(!0) */, (EqualityComparer_1_t3092359764 *)L_0, (int32_t)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_3 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType0`2<ConnectionAPISample/MessageTypes,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m3123070529_gshared (U3CU3E__AnonType0_2_t1383471493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType0_2_ToString_m3123070529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1806356649);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1806356649);
		StringU5BU5D_t1642385972* L_2 = (StringU5BU5D_t1642385972*)L_1;
		int32_t L_3 = (int32_t)__this->get_U3CTypeU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
	}
	{
		int32_t L_4 = (int32_t)__this->get_U3CTypeU3E_0();
		V_0 = (int32_t)L_4;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (&V_0));
		NullCheck((Il2CppObject *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_5);
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_7;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral1382205792);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1382205792);
		StringU5BU5D_t1642385972* L_9 = (StringU5BU5D_t1642385972*)L_8;
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_9;
		G_B4_2 = L_9;
		if (!L_10)
		{
			G_B5_0 = 4;
			G_B5_1 = L_9;
			G_B5_2 = L_9;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		V_1 = (Il2CppObject *)L_11;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_14 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral1947253693);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1947253693);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void <>__AnonType0`2<System.Object,System.Object>::.ctor(<Type>__T,<Value>__T)
extern "C"  void U3CU3E__AnonType0_2__ctor_m3768725458_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, Il2CppObject * ___Type0, Il2CppObject * ___Value1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___Type0;
		__this->set_U3CTypeU3E_0(L_0);
		Il2CppObject * L_1 = ___Value1;
		__this->set_U3CValueU3E_1(L_1);
		return;
	}
}
// <Type>__T <>__AnonType0`2<System.Object,System.Object>::get_Type()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Type_m4183668223_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CTypeU3E_0();
		return L_0;
	}
}
// <Value>__T <>__AnonType0`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * U3CU3E__AnonType0_2_get_Value_m2849316499_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType0`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_2_Equals_m2151427210_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType0_2_t3333162619 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType0_2_t3333162619 *)((U3CU3E__AnonType0_2_t3333162619 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		U3CU3E__AnonType0_2_t3333162619 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_2 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CTypeU3E_0();
		U3CU3E__AnonType0_2_t3333162619 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CTypeU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_7 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		U3CU3E__AnonType0_2_t3333162619 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CValueU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType0`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_2_GetHashCode_m2678831190_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_0 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CTypeU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_3 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType0`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType0_2_ToString_m2057022320_gshared (U3CU3E__AnonType0_2_t3333162619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType0_2_ToString_m2057022320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1806356649);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1806356649);
		StringU5BU5D_t1642385972* L_2 = (StringU5BU5D_t1642385972*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CTypeU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CTypeU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1382205792);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1382205792);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CValueU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1947253693);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1947253693);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType1`2<System.Object,System.Object>::.ctor(<Street>__T,<Zip>__T)
extern "C"  void U3CU3E__AnonType1_2__ctor_m3761835444_gshared (U3CU3E__AnonType1_2_t3010424510 * __this, Il2CppObject * ___Street0, Il2CppObject * ___Zip1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___Street0;
		__this->set_U3CStreetU3E_0(L_0);
		Il2CppObject * L_1 = ___Zip1;
		__this->set_U3CZipU3E_1(L_1);
		return;
	}
}
// <Street>__T <>__AnonType1`2<System.Object,System.Object>::get_Street()
extern "C"  Il2CppObject * U3CU3E__AnonType1_2_get_Street_m1119223450_gshared (U3CU3E__AnonType1_2_t3010424510 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CStreetU3E_0();
		return L_0;
	}
}
// <Zip>__T <>__AnonType1`2<System.Object,System.Object>::get_Zip()
extern "C"  Il2CppObject * U3CU3E__AnonType1_2_get_Zip_m462264472_gshared (U3CU3E__AnonType1_2_t3010424510 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CZipU3E_1();
		return L_0;
	}
}
// System.Boolean <>__AnonType1`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType1_2_Equals_m3006980687_gshared (U3CU3E__AnonType1_2_t3010424510 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType1_2_t3010424510 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType1_2_t3010424510 *)((U3CU3E__AnonType1_2_t3010424510 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		U3CU3E__AnonType1_2_t3010424510 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_2 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CStreetU3E_0();
		U3CU3E__AnonType1_2_t3010424510 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CStreetU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_7 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CZipU3E_1();
		U3CU3E__AnonType1_2_t3010424510 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CZipU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B4_0 = 0;
	}

IL_0041:
	{
		G_B6_0 = G_B4_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B6_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B6_0;
	}
}
// System.Int32 <>__AnonType1`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType1_2_GetHashCode_m2780311313_gshared (U3CU3E__AnonType1_2_t3010424510 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_0 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CStreetU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_3 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CZipU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_3, (Il2CppObject *)L_4);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)));
		int32_t L_6 = V_0;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)13)))));
		int32_t L_8 = V_0;
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8^(int32_t)((int32_t)((int32_t)L_9>>(int32_t)7))));
		int32_t L_10 = V_0;
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)3))));
		int32_t L_12 = V_0;
		int32_t L_13 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)17)))));
		int32_t L_14 = V_0;
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)5))));
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.String <>__AnonType1`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType1_2_ToString_m2226408529_gshared (U3CU3E__AnonType1_2_t3010424510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType1_2_ToString_m2226408529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1278437986);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1278437986);
		StringU5BU5D_t1642385972* L_2 = (StringU5BU5D_t1642385972*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CStreetU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CStreetU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral286592186);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral286592186);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CZipU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CZipU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1947253693);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1947253693);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void <>__AnonType2`3<System.Object,System.Int32,System.Object>::.ctor(<Name>__T,<Age>__T,<Address>__T)
extern "C"  void U3CU3E__AnonType2_3__ctor_m1556224621_gshared (U3CU3E__AnonType2_3_t3651135450 * __this, Il2CppObject * ___Name0, int32_t ___Age1, Il2CppObject * ___Address2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___Name0;
		__this->set_U3CNameU3E_0(L_0);
		int32_t L_1 = ___Age1;
		__this->set_U3CAgeU3E_1(L_1);
		Il2CppObject * L_2 = ___Address2;
		__this->set_U3CAddressU3E_2(L_2);
		return;
	}
}
// <Name>__T <>__AnonType2`3<System.Object,System.Int32,System.Object>::get_Name()
extern "C"  Il2CppObject * U3CU3E__AnonType2_3_get_Name_m3623813357_gshared (U3CU3E__AnonType2_3_t3651135450 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		return L_0;
	}
}
// <Age>__T <>__AnonType2`3<System.Object,System.Int32,System.Object>::get_Age()
extern "C"  int32_t U3CU3E__AnonType2_3_get_Age_m2635766761_gshared (U3CU3E__AnonType2_3_t3651135450 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CAgeU3E_1();
		return L_0;
	}
}
// <Address>__T <>__AnonType2`3<System.Object,System.Int32,System.Object>::get_Address()
extern "C"  Il2CppObject * U3CU3E__AnonType2_3_get_Address_m719751741_gshared (U3CU3E__AnonType2_3_t3651135450 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		return L_0;
	}
}
// System.Boolean <>__AnonType2`3<System.Object,System.Int32,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType2_3_Equals_m1755628866_gshared (U3CU3E__AnonType2_3_t3651135450 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType2_3_t3651135450 * V_0 = NULL;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType2_3_t3651135450 *)((U3CU3E__AnonType2_3_t3651135450 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		U3CU3E__AnonType2_3_t3651135450 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_2 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		U3CU3E__AnonType2_3_t3651135450 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CNameU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t645512719 * L_7 = ((  EqualityComparer_1_t645512719 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_8 = (int32_t)__this->get_U3CAgeU3E_1();
		U3CU3E__AnonType2_3_t3651135450 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)L_9->get_U3CAgeU3E_1();
		NullCheck((EqualityComparer_1_t645512719 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(!0,!0) */, (EqualityComparer_1_t645512719 *)L_7, (int32_t)L_8, (int32_t)L_10);
		if (!L_11)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8));
		EqualityComparer_1_t1263084566 * L_12 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		U3CU3E__AnonType2_3_t3651135450 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject * L_15 = (Il2CppObject *)L_14->get_U3CAddressU3E_2();
		NullCheck((EqualityComparer_1_t1263084566 *)L_12);
		bool L_16 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_12, (Il2CppObject *)L_13, (Il2CppObject *)L_15);
		G_B5_0 = ((int32_t)(L_16));
		goto IL_005c;
	}

IL_005b:
	{
		G_B5_0 = 0;
	}

IL_005c:
	{
		G_B7_0 = G_B5_0;
		goto IL_005f;
	}

IL_005e:
	{
		G_B7_0 = 0;
	}

IL_005f:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 <>__AnonType2`3<System.Object,System.Int32,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType2_3_GetHashCode_m3934398586_gshared (U3CU3E__AnonType2_3_t3651135450 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_0 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t645512719 * L_3 = ((  EqualityComparer_1_t645512719 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_4 = (int32_t)__this->get_U3CAgeU3E_1();
		NullCheck((EqualityComparer_1_t645512719 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(!0) */, (EqualityComparer_1_t645512719 *)L_3, (int32_t)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8));
		EqualityComparer_1_t1263084566 * L_6 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		NullCheck((EqualityComparer_1_t1263084566 *)L_6);
		int32_t L_8 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_6, (Il2CppObject *)L_7);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)))^(int32_t)L_8))*(int32_t)((int32_t)16777619)));
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)((int32_t)((int32_t)L_10<<(int32_t)((int32_t)13)))));
		int32_t L_11 = V_0;
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_11^(int32_t)((int32_t)((int32_t)L_12>>(int32_t)7))));
		int32_t L_13 = V_0;
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)((int32_t)((int32_t)L_14<<(int32_t)3))));
		int32_t L_15 = V_0;
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15^(int32_t)((int32_t)((int32_t)L_16>>(int32_t)((int32_t)17)))));
		int32_t L_17 = V_0;
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)((int32_t)((int32_t)L_18<<(int32_t)5))));
		int32_t L_19 = V_0;
		return L_19;
	}
}
// System.String <>__AnonType2`3<System.Object,System.Int32,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType2_3_ToString_m1800031042_gshared (U3CU3E__AnonType2_3_t3651135450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType2_3_ToString_m1800031042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	int32_t G_B8_0 = 0;
	StringU5BU5D_t1642385972* G_B8_1 = NULL;
	StringU5BU5D_t1642385972* G_B8_2 = NULL;
	int32_t G_B7_0 = 0;
	StringU5BU5D_t1642385972* G_B7_1 = NULL;
	StringU5BU5D_t1642385972* G_B7_2 = NULL;
	String_t* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	StringU5BU5D_t1642385972* G_B9_2 = NULL;
	StringU5BU5D_t1642385972* G_B9_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral2856482426);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2856482426);
		StringU5BU5D_t1642385972* L_2 = (StringU5BU5D_t1642385972*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral947593626);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral947593626);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		int32_t L_9 = (int32_t)__this->get_U3CAgeU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		int32_t L_10 = (int32_t)__this->get_U3CAgeU3E_1();
		V_1 = (int32_t)L_10;
		String_t* L_11 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1517356551);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1517356551);
		StringU5BU5D_t1642385972* L_14 = (StringU5BU5D_t1642385972*)L_13;
		Il2CppObject * L_15 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		G_B7_0 = 6;
		G_B7_1 = L_14;
		G_B7_2 = L_14;
		if (!L_15)
		{
			G_B8_0 = 6;
			G_B8_1 = L_14;
			G_B8_2 = L_14;
			goto IL_00b3;
		}
	}
	{
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		V_2 = (Il2CppObject *)L_16;
		NullCheck((Il2CppObject *)(*(&V_2)));
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_2)));
		G_B9_0 = L_17;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_00b8;
	}

IL_00b3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B9_0 = L_18;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_00b8:
	{
		NullCheck(G_B9_2);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (String_t*)G_B9_0);
		StringU5BU5D_t1642385972* L_19 = (StringU5BU5D_t1642385972*)G_B9_3;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral1947253693);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral1947253693);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void <>__AnonType2`3<System.Object,System.Object,System.Object>::.ctor(<Name>__T,<Age>__T,<Address>__T)
extern "C"  void U3CU3E__AnonType2_3__ctor_m3691860610_gshared (U3CU3E__AnonType2_3_t4235369943 * __this, Il2CppObject * ___Name0, Il2CppObject * ___Age1, Il2CppObject * ___Address2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___Name0;
		__this->set_U3CNameU3E_0(L_0);
		Il2CppObject * L_1 = ___Age1;
		__this->set_U3CAgeU3E_1(L_1);
		Il2CppObject * L_2 = ___Address2;
		__this->set_U3CAddressU3E_2(L_2);
		return;
	}
}
// <Name>__T <>__AnonType2`3<System.Object,System.Object,System.Object>::get_Name()
extern "C"  Il2CppObject * U3CU3E__AnonType2_3_get_Name_m1976381874_gshared (U3CU3E__AnonType2_3_t4235369943 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		return L_0;
	}
}
// <Age>__T <>__AnonType2`3<System.Object,System.Object,System.Object>::get_Age()
extern "C"  Il2CppObject * U3CU3E__AnonType2_3_get_Age_m152335376_gshared (U3CU3E__AnonType2_3_t4235369943 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CAgeU3E_1();
		return L_0;
	}
}
// <Address>__T <>__AnonType2`3<System.Object,System.Object,System.Object>::get_Address()
extern "C"  Il2CppObject * U3CU3E__AnonType2_3_get_Address_m1345415760_gshared (U3CU3E__AnonType2_3_t4235369943 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		return L_0;
	}
}
// System.Boolean <>__AnonType2`3<System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType2_3_Equals_m3953652679_gshared (U3CU3E__AnonType2_3_t4235369943 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	U3CU3E__AnonType2_3_t4235369943 * V_0 = NULL;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		V_0 = (U3CU3E__AnonType2_3_t4235369943 *)((U3CU3E__AnonType2_3_t4235369943 *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		U3CU3E__AnonType2_3_t4235369943 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_2 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		U3CU3E__AnonType2_3_t4235369943 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_U3CNameU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_2);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_7 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CAgeU3E_1();
		U3CU3E__AnonType2_3_t4235369943 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)L_9->get_U3CAgeU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_7);
		bool L_11 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_10);
		if (!L_11)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8));
		EqualityComparer_1_t1263084566 * L_12 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		U3CU3E__AnonType2_3_t4235369943 * L_14 = V_0;
		NullCheck(L_14);
		Il2CppObject * L_15 = (Il2CppObject *)L_14->get_U3CAddressU3E_2();
		NullCheck((EqualityComparer_1_t1263084566 *)L_12);
		bool L_16 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t1263084566 *)L_12, (Il2CppObject *)L_13, (Il2CppObject *)L_15);
		G_B5_0 = ((int32_t)(L_16));
		goto IL_005c;
	}

IL_005b:
	{
		G_B5_0 = 0;
	}

IL_005c:
	{
		G_B7_0 = G_B5_0;
		goto IL_005f;
	}

IL_005e:
	{
		G_B7_0 = 0;
	}

IL_005f:
	{
		return (bool)G_B7_0;
	}
}
// System.Int32 <>__AnonType2`3<System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType2_3_GetHashCode_m74822245_gshared (U3CU3E__AnonType2_3_t4235369943 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		EqualityComparer_1_t1263084566 * L_0 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		NullCheck((EqualityComparer_1_t1263084566 *)L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_0, (Il2CppObject *)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		EqualityComparer_1_t1263084566 * L_3 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CAgeU3E_1();
		NullCheck((EqualityComparer_1_t1263084566 *)L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_3, (Il2CppObject *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8));
		EqualityComparer_1_t1263084566 * L_6 = ((  EqualityComparer_1_t1263084566 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		NullCheck((EqualityComparer_1_t1263084566 *)L_6);
		int32_t L_8 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::GetHashCode(!0) */, (EqualityComparer_1_t1263084566 *)L_6, (Il2CppObject *)L_7);
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-2128831035)^(int32_t)L_2))*(int32_t)((int32_t)16777619)))^(int32_t)L_5))*(int32_t)((int32_t)16777619)))^(int32_t)L_8))*(int32_t)((int32_t)16777619)));
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)((int32_t)((int32_t)L_10<<(int32_t)((int32_t)13)))));
		int32_t L_11 = V_0;
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_11^(int32_t)((int32_t)((int32_t)L_12>>(int32_t)7))));
		int32_t L_13 = V_0;
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)((int32_t)((int32_t)L_14<<(int32_t)3))));
		int32_t L_15 = V_0;
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_15^(int32_t)((int32_t)((int32_t)L_16>>(int32_t)((int32_t)17)))));
		int32_t L_17 = V_0;
		int32_t L_18 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_17+(int32_t)((int32_t)((int32_t)L_18<<(int32_t)5))));
		int32_t L_19 = V_0;
		return L_19;
	}
}
// System.String <>__AnonType2`3<System.Object,System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType2_3_ToString_m690164709_gshared (U3CU3E__AnonType2_3_t4235369943 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3E__AnonType2_3_ToString_m690164709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	int32_t G_B8_0 = 0;
	StringU5BU5D_t1642385972* G_B8_1 = NULL;
	StringU5BU5D_t1642385972* G_B8_2 = NULL;
	int32_t G_B7_0 = 0;
	StringU5BU5D_t1642385972* G_B7_1 = NULL;
	StringU5BU5D_t1642385972* G_B7_2 = NULL;
	String_t* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	StringU5BU5D_t1642385972* G_B9_2 = NULL;
	StringU5BU5D_t1642385972* G_B9_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029399);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029399);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral2856482426);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2856482426);
		StringU5BU5D_t1642385972* L_2 = (StringU5BU5D_t1642385972*)L_1;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		G_B1_0 = 2;
		G_B1_1 = L_2;
		G_B1_2 = L_2;
		if (!L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = L_2;
			G_B2_2 = L_2;
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CNameU3E_0();
		V_0 = (Il2CppObject *)L_4;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0046;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0046:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral947593626);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral947593626);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CAgeU3E_1();
		G_B4_0 = 4;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 4;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_007a;
		}
	}
	{
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CAgeU3E_1();
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_007f;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_007f:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1517356551);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1517356551);
		StringU5BU5D_t1642385972* L_14 = (StringU5BU5D_t1642385972*)L_13;
		Il2CppObject * L_15 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		G_B7_0 = 6;
		G_B7_1 = L_14;
		G_B7_2 = L_14;
		if (!L_15)
		{
			G_B8_0 = 6;
			G_B8_1 = L_14;
			G_B8_2 = L_14;
			goto IL_00b3;
		}
	}
	{
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_U3CAddressU3E_2();
		V_2 = (Il2CppObject *)L_16;
		NullCheck((Il2CppObject *)(*(&V_2)));
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_2)));
		G_B9_0 = L_17;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_00b8;
	}

IL_00b3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B9_0 = L_18;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_00b8:
	{
		NullCheck(G_B9_2);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (String_t*)G_B9_0);
		StringU5BU5D_t1642385972* L_19 = (StringU5BU5D_t1642385972*)G_B9_3;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral1947253693);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral1947253693);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void ApiMobilityManager/Serialization`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Serialization_1__ctor_m1737129539_gshared (Serialization_1_t588673888 * __this, List_1_t2058570427 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serialization_1__ctor_m1737129539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		ApiMobilityManager_t1284346316 * L_0 = SingletonMonoBehaviour_1_get_Instance_m1186797120(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m1186797120_MethodInfo_var);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)L_0->get_mobilityName_9();
		__this->set_name_0(L_1);
		__this->set_situation_1(_stringLiteral890260275);
		__this->set_description_2(_stringLiteral3016923933);
		RecordingManager_t3075979958 * L_2 = SingletonMonoBehaviour_1_get_Instance_m369094862(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m369094862_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_copyedMobilityId_34();
		__this->set_parentId_3(L_3);
		ApiMobilityManager_t1284346316 * L_4 = SingletonMonoBehaviour_1_get_Instance_m1186797120(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m1186797120_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)L_4->get_corePartsColorId_12();
		__this->set_corePartsColor_5(L_5);
		CreateManager_t3918627545 * L_6 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = (GameObject_t1756533147 *)L_6->get_m_Core_19();
		NullCheck((GameObject_t1756533147 *)L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_7, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_8);
		Vector3_t2243707580  L_9 = Transform_get_localScale_m3074381503((Transform_t3275118058 *)L_8, /*hidden argument*/NULL);
		V_0 = (Vector3_t2243707580 )L_9;
		float L_10 = (float)(&V_0)->get_x_1();
		__this->set_corePartsScaleX_6(L_10);
		CreateManager_t3918627545 * L_11 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = (GameObject_t1756533147 *)L_11->get_m_Core_19();
		NullCheck((GameObject_t1756533147 *)L_12);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_12, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_13);
		Vector3_t2243707580  L_14 = Transform_get_localScale_m3074381503((Transform_t3275118058 *)L_13, /*hidden argument*/NULL);
		V_1 = (Vector3_t2243707580 )L_14;
		float L_15 = (float)(&V_1)->get_y_2();
		__this->set_corePartsScaleY_7(L_15);
		CreateManager_t3918627545 * L_16 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = (GameObject_t1756533147 *)L_16->get_m_Core_19();
		NullCheck((GameObject_t1756533147 *)L_17);
		Transform_t3275118058 * L_18 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_17, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_18);
		Vector3_t2243707580  L_19 = Transform_get_localScale_m3074381503((Transform_t3275118058 *)L_18, /*hidden argument*/NULL);
		V_2 = (Vector3_t2243707580 )L_19;
		float L_20 = (float)(&V_2)->get_z_3();
		__this->set_corePartsScaleZ_8(L_20);
		CreateManager_t3918627545 * L_21 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_21);
		StringU5BU5D_t1642385972* L_22 = (StringU5BU5D_t1642385972*)L_21->get_corePartsName_12();
		NullCheck(L_22);
		int32_t L_23 = 0;
		String_t* L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		__this->set_corePartsName_9(L_24);
		TutorialManager_t2168024773 * L_25 = SingletonMonoBehaviour_1_get_Instance_m2875622201(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m2875622201_MethodInfo_var);
		NullCheck(L_25);
		bool L_26 = (bool)L_25->get_m_bIsTutorial_3();
		__this->set_tutorialFlg_10(L_26);
		ApiUserManager_t2556825810 * L_27 = SingletonMonoBehaviour_1_get_Instance_m1552479802(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m1552479802_MethodInfo_var);
		NullCheck(L_27);
		int32_t L_28 = (int32_t)L_27->get_loginedUserId_5();
		__this->set_createdUserId_12(L_28);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t2058570427 * L_29 = ___target0;
		__this->set_childPartsList_11(L_29);
		return;
	}
}
// System.Collections.Generic.List`1<T> ApiMobilityManager/Serialization`1<System.Object>::ToList()
extern "C"  List_1_t2058570427 * Serialization_1_ToList_m3797051433_gshared (Serialization_1_t588673888 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_childPartsList_11();
		return L_0;
	}
}
// System.Void ApiModuleManager/Serialization`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Serialization_1__ctor_m1355904736_gshared (Serialization_1_t1716271607 * __this, List_1_t2058570427 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serialization_1__ctor_m1355904736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RecordingManager_t3075979958 * L_0 = SingletonMonoBehaviour_1_get_Instance_m369094862(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m369094862_MethodInfo_var);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_copyedMobilityId_34();
		__this->set_parentId_0(L_1);
		ApiMobilityManager_t1284346316 * L_2 = SingletonMonoBehaviour_1_get_Instance_m1186797120(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m1186797120_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_corePartsColorId_12();
		__this->set_corePartsColor_2(L_3);
		CreateManager_t3918627545 * L_4 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = (GameObject_t1756533147 *)L_4->get_m_Core_19();
		NullCheck((GameObject_t1756533147 *)L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_5, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_6);
		Vector3_t2243707580  L_7 = Transform_get_localScale_m3074381503((Transform_t3275118058 *)L_6, /*hidden argument*/NULL);
		V_0 = (Vector3_t2243707580 )L_7;
		float L_8 = (float)(&V_0)->get_x_1();
		__this->set_corePartsScaleX_3(L_8);
		CreateManager_t3918627545 * L_9 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = (GameObject_t1756533147 *)L_9->get_m_Core_19();
		NullCheck((GameObject_t1756533147 *)L_10);
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_10, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_11);
		Vector3_t2243707580  L_12 = Transform_get_localScale_m3074381503((Transform_t3275118058 *)L_11, /*hidden argument*/NULL);
		V_1 = (Vector3_t2243707580 )L_12;
		float L_13 = (float)(&V_1)->get_y_2();
		__this->set_corePartsScaleY_4(L_13);
		CreateManager_t3918627545 * L_14 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_14);
		GameObject_t1756533147 * L_15 = (GameObject_t1756533147 *)L_14->get_m_Core_19();
		NullCheck((GameObject_t1756533147 *)L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139((GameObject_t1756533147 *)L_15, /*hidden argument*/NULL);
		NullCheck((Transform_t3275118058 *)L_16);
		Vector3_t2243707580  L_17 = Transform_get_localScale_m3074381503((Transform_t3275118058 *)L_16, /*hidden argument*/NULL);
		V_2 = (Vector3_t2243707580 )L_17;
		float L_18 = (float)(&V_2)->get_z_3();
		__this->set_corePartsScaleZ_5(L_18);
		CreateManager_t3918627545 * L_19 = SingletonMonoBehaviour_1_get_Instance_m3710663101(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3710663101_MethodInfo_var);
		NullCheck(L_19);
		StringU5BU5D_t1642385972* L_20 = (StringU5BU5D_t1642385972*)L_19->get_corePartsName_12();
		NullCheck(L_20);
		int32_t L_21 = 0;
		String_t* L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		__this->set_corePartsName_6(L_22);
		ApiUserManager_t2556825810 * L_23 = SingletonMonoBehaviour_1_get_Instance_m1552479802(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m1552479802_MethodInfo_var);
		NullCheck(L_23);
		int32_t L_24 = (int32_t)L_23->get_loginedUserId_5();
		__this->set_createdUserId_12(L_24);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t2058570427 * L_25 = ___target0;
		__this->set_childPartsList_11(L_25);
		return;
	}
}
// System.Collections.Generic.List`1<T> ApiModuleManager/Serialization`1<System.Object>::ToList()
extern "C"  List_1_t2058570427 * Serialization_1_ToList_m2768144650_gshared (Serialization_1_t1716271607 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_childPartsList_11();
		return L_0;
	}
}
// System.Void ApiModuleMotionManager/ModuleMotionSerialization`1<System.Object>::.ctor(T)
extern "C"  void ModuleMotionSerialization_1__ctor_m3685629770_gshared (ModuleMotionSerialization_1_t3647087565 * __this, Il2CppObject * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ModuleMotionSerialization_1__ctor_m3685629770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ApiModuleManager_t3939700127 * L_0 = SingletonMonoBehaviour_1_get_Instance_m3975349435(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m3975349435_MethodInfo_var);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_moduleId_6();
		__this->set_moduleId_0(L_1);
		ApiModuleMotionManager_t2960242393 * L_2 = SingletonMonoBehaviour_1_get_Instance_m415312105(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m415312105_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_moduleActionId_4();
		__this->set_actionId_1(L_3);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		__this->set_item_2(L_4);
		return;
	}
}
// System.Void ApiReactionManager/Serialization`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Serialization_1__ctor_m727232031_gshared (Serialization_1_t1156466558 * __this, List_1_t2058570427 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serialization_1__ctor_m727232031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ApiReactionManager_t2857272874 * L_0 = SingletonMonoBehaviour_1_get_Instance_m539795794(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m539795794_MethodInfo_var);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_recationMobimoId_23();
		__this->set_mobilityId_0(L_1);
		ApiUserManager_t2556825810 * L_2 = SingletonMonoBehaviour_1_get_Instance_m1552479802(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m1552479802_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_loginedUserId_5();
		__this->set_userId_1(L_3);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t2058570427 * L_4 = ___target0;
		__this->set_reactions_2(L_4);
		return;
	}
}
// System.Collections.Generic.List`1<T> ApiReactionManager/Serialization`1<System.Object>::ToList()
extern "C"  List_1_t2058570427 * Serialization_1_ToList_m2040144313_gshared (Serialization_1_t1156466558 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_reactions_2();
		return L_0;
	}
}
// System.Void Boo.Lang.GenericGenerator`1<System.Object>::.ctor()
extern "C"  void GenericGenerator_1__ctor_m409073663_gshared (GenericGenerator_1_t3108987245 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Boo.Lang.GenericGenerator`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * GenericGenerator_1_System_Collections_IEnumerable_GetEnumerator_m3032861675_gshared (GenericGenerator_1_t3108987245 * __this, const MethodInfo* method)
{
	{
		NullCheck((GenericGenerator_1_t3108987245 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<T> Boo.Lang.GenericGenerator`1<System.Object>::GetEnumerator() */, (GenericGenerator_1_t3108987245 *)__this);
		return L_0;
	}
}
// System.String Boo.Lang.GenericGenerator`1<System.Object>::ToString()
extern "C"  String_t* GenericGenerator_1_ToString_m1222804785_gshared (GenericGenerator_1_t3108987245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericGenerator_1_ToString_m1222804785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2024975688(NULL /*static, unused*/, (String_t*)_stringLiteral550572160, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::.ctor()
extern "C"  void GenericGeneratorEnumerator_1__ctor_m509214543_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set__state_1(0);
		return;
	}
}
// System.Object Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GenericGeneratorEnumerator_1_System_Collections_IEnumerator_get_Current_m1921234798_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__current_0();
		return L_0;
	}
}
// T Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * GenericGeneratorEnumerator_1_get_Current_m3586948289_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__current_0();
		return L_0;
	}
}
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Dispose()
extern "C"  void GenericGeneratorEnumerator_1_Dispose_m1715637737_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Reset()
extern "C"  void GenericGeneratorEnumerator_1_Reset_m4255715191_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericGeneratorEnumerator_1_Reset_m4255715191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Yield(System.Int32,T)
extern "C"  bool GenericGeneratorEnumerator_1_Yield_m3421350270_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, int32_t ___state0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___state0;
		__this->set__state_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set__current_0(L_1);
		return (bool)1;
	}
}
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::YieldDefault(System.Int32)
extern "C"  bool GenericGeneratorEnumerator_1_YieldDefault_m799132706_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, int32_t ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericGeneratorEnumerator_1_YieldDefault_m799132706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___state0;
		__this->set__state_1(L_0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		__this->set__current_0(L_1);
		return (bool)1;
	}
}
// System.Void Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator6__ctor_m963943484_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m925963075_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1245731780_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator6_MoveNext_m3471029548_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator6_MoveNext_m3471029548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00ae;
			}
		}
	}
	{
		goto IL_00d9;
	}

IL_0021:
	{
		List_1_t61287617 * L_2 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__count_2();
		__this->set_U3CoriginalCountU3E__0_0(L_3);
		List_1_t61287617 * L_4 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_4->get__items_1();
		__this->set_U3CoriginalItemsU3E__1_1(L_5);
		__this->set_U3CiU3E__2_2(0);
		goto IL_00bc;
	}

IL_004f:
	{
		int32_t L_6 = (int32_t)__this->get_U3CoriginalCountU3E__0_0();
		List_1_t61287617 * L_7 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__count_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_007b;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)__this->get_U3CoriginalItemsU3E__1_1();
		List_1_t61287617 * L_10 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_10);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10->get__items_1();
		if ((((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_9) == ((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_11)))
		{
			goto IL_0086;
		}
	}

IL_007b:
	{
		InvalidOperationException_t721527559 * L_12 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_12, (String_t*)_stringLiteral850795601, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0086:
	{
		List_1_t61287617 * L_13 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get__items_1();
		int32_t L_15 = (int32_t)__this->get_U3CiU3E__2_2();
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		__this->set_U24current_4(L_17);
		__this->set_U24PC_3(1);
		goto IL_00db;
	}

IL_00ae:
	{
		int32_t L_18 = (int32_t)__this->get_U3CiU3E__2_2();
		__this->set_U3CiU3E__2_2(((int32_t)((int32_t)L_18+(int32_t)1)));
	}

IL_00bc:
	{
		int32_t L_19 = (int32_t)__this->get_U3CiU3E__2_2();
		List_1_t61287617 * L_20 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get__count_2();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_00d9:
	{
		return (bool)0;
	}

IL_00db:
	{
		return (bool)1;
	}
	// Dead block : IL_00dd: ldloc.1
}
// System.Void Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator6_Dispose_m1678474553_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator6_Reset_m4037020951_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator6_Reset_m4037020951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Boo.Lang.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2786657214_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_0 = ((List_1_t61287617_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_0();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::.cctor()
extern "C"  void List_1__cctor_m926686541_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t61287617_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_0(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void List_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m217229263_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Collections.IEnumerator Boo.Lang.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3060334751_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject*)__this);
		Il2CppObject* L_0 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)__this);
		return L_0;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void List_1_System_Collections_Generic_IListU3CTU3E_Insert_m2474991156_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___item1;
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void List_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1444197752_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3517243408_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		bool L_1 = ((  bool (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3135844156_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((List_1_t61287617 *)__this);
		VirtFuncInvoker1< List_1_t61287617 *, Il2CppObject * >::Invoke(34 /* Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Add(T) */, (List_1_t61287617 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t61287617 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return ((int32_t)((int32_t)L_1-(int32_t)1));
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2107483803_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m187661787_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3492045770_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_2;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2033159042_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		bool L_2 = ((  bool (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_2;
	}
}
// System.Object Boo.Lang.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m936735699_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return L_1;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m611384076_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void List_1_System_Collections_IList_RemoveAt_m1396596113_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1643189175_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m92731834_gshared (List_1_t61287617 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		int32_t L_3 = (int32_t)__this->get__count_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m4001062748_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Boo.Lang.List`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* List_1_GetEnumerator_m1902460426_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * L_0 = (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 19));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1360236987_gshared (List_1_t61287617 * __this, ObjectU5BU5D_t3614634134* ___target0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		ObjectU5BU5D_t3614634134* L_1 = ___target0;
		int32_t L_2 = ___index1;
		int32_t L_3 = (int32_t)__this->get__count_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::get_IsSynchronized()
extern "C"  bool List_1_get_IsSynchronized_m4219972939_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object Boo.Lang.List`1<System.Object>::get_SyncRoot()
extern "C"  Il2CppObject * List_1_get_SyncRoot_m2491646691_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		return (Il2CppObject *)L_0;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::get_IsReadOnly()
extern "C"  bool List_1_get_IsReadOnly_m2003327643_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// T Boo.Lang.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m3413770970_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck(L_0);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2985722127_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Il2CppObject * L_4 = ___value1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)L_4);
		return;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Push(T)
extern "C"  List_1_t61287617 * List_1_Push_m2231078188_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		List_1_t61287617 * L_1 = VirtFuncInvoker1< List_1_t61287617 *, Il2CppObject * >::Invoke(34 /* Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Add(T) */, (List_1_t61287617 *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Add(T)
extern "C"  List_1_t61287617 * List_1_Add_m1792075939_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t61287617 *)__this, (int32_t)((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_2 = (int32_t)__this->get__count_2();
		Il2CppObject * L_3 = ___item0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_3);
		int32_t L_4 = (int32_t)__this->get__count_2();
		__this->set__count_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		return __this;
	}
}
// System.String Boo.Lang.List`1<System.Object>::ToString()
extern "C"  String_t* List_1_ToString_m3353273637_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_ToString_m3353273637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((List_1_t61287617 *)__this);
		String_t* L_0 = ((  String_t* (*) (List_1_t61287617 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)((List_1_t61287617 *)__this, (String_t*)_stringLiteral811305474, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral372029431, (String_t*)L_0, (String_t*)_stringLiteral372029425, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Boo.Lang.List`1<System.Object>::Join(System.String)
extern "C"  String_t* List_1_Join_m2084635115_gshared (List_1_t61287617 * __this, String_t* ___separator0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___separator0;
		String_t* L_1 = Builtins_join_m2036613869(NULL /*static, unused*/, (Il2CppObject *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::GetHashCode()
extern "C"  int32_t List_1_GetHashCode_m1730152805_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		V_0 = (int32_t)L_0;
		V_1 = (int32_t)0;
		goto IL_003a;
	}

IL_000e:
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_2;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = V_0;
		NullCheck((Il2CppObject *)(*(&V_2)));
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_2)));
		V_0 = (int32_t)((int32_t)((int32_t)L_6^(int32_t)L_7));
	}

IL_0036:
	{
		int32_t L_8 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_11 = V_0;
		return L_11;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::Equals(System.Object)
extern "C"  bool List_1_Equals_m34533115_gshared (List_1_t61287617 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if ((((Il2CppObject*)(List_1_t61287617 *)__this) == ((Il2CppObject*)(Il2CppObject *)L_0)))
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = ___other0;
		NullCheck((List_1_t61287617 *)__this);
		bool L_2 = ((  bool (*) (List_1_t61287617 *, List_1_t61287617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t61287617 *)__this, (List_1_t61287617 *)((List_1_t61287617 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 23))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
	}

IL_0016:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::Equals(Boo.Lang.List`1<T>)
extern "C"  bool List_1_Equals_m2416030142_gshared (List_1_t61287617 * __this, List_1_t61287617 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Equals_m2416030142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t61287617 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		List_1_t61287617 * L_1 = ___other0;
		bool L_2 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, (Il2CppObject *)__this, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		int32_t L_3 = (int32_t)__this->get__count_2();
		List_1_t61287617 * L_4 = ___other0;
		NullCheck((List_1_t61287617 *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t61287617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t61287617 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		V_0 = (int32_t)0;
		goto IL_005d;
	}

IL_0030:
	{
		ObjectU5BU5D_t3614634134* L_6 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		List_1_t61287617 * L_10 = ___other0;
		int32_t L_11 = V_0;
		NullCheck((List_1_t61287617 *)L_10);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t61287617 *)L_10, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		bool L_13 = RuntimeServices_EqualityOperator_m2233200645(NULL /*static, unused*/, (Il2CppObject *)L_9, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0059;
		}
	}
	{
		return (bool)0;
	}

IL_0059:
	{
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0030;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m2512712227_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Clear_m2512712227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0020;
	}

IL_0007:
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = V_0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_2 = V_1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Il2CppObject *)L_2);
		int32_t L_3 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0007;
		}
	}
	{
		__this->set__count_2(0);
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::Contains(T)
extern "C"  bool List_1_Contains_m872023821_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return (bool)((((int32_t)((((int32_t)(-1)) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m131628331_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_IndexOf_m131628331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_002e;
	}

IL_0007:
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Il2CppObject * L_4 = ___item0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		bool L_5 = RuntimeServices_EqualityOperator_m2233200645(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		return L_6;
	}

IL_002a:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Insert(System.Int32,T)
extern "C"  List_1_t61287617 * List_1_Insert_m1948090966_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (int32_t)L_1;
		int32_t L_2 = (int32_t)__this->get__count_2();
		int32_t L_3 = V_0;
		int32_t L_4 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t61287617 *)__this, (int32_t)((int32_t)((int32_t)L_4+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		int32_t L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_0045;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_8 = V_0;
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__count_2();
		int32_t L_12 = V_0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)L_8, (Il2CppArray *)(Il2CppArray *)L_9, (int32_t)((int32_t)((int32_t)L_10+(int32_t)1)), (int32_t)((int32_t)((int32_t)L_11-(int32_t)L_12)), /*hidden argument*/NULL);
	}

IL_0045:
	{
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_14 = V_0;
		Il2CppObject * L_15 = ___item1;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
		int32_t L_16 = (int32_t)__this->get__count_2();
		__this->set__count_2(((int32_t)((int32_t)L_16+(int32_t)1)));
		return __this;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Remove(T)
extern "C"  List_1_t61287617 * List_1_Remove_m3021996592_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		((  bool (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return __this;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  List_1_t61287617 * List_1_RemoveAt_m3714963554_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return __this;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::EnsureCapacity(System.Int32)
extern "C"  void List_1_EnsureCapacity_m3110424457_gshared (List_1_t61287617 * __this, int32_t ___minCapacity0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		int32_t L_0 = ___minCapacity0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_1);
		if ((((int32_t)L_0) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_2 = ___minCapacity0;
		NullCheck((List_1_t61287617 *)__this);
		ObjectU5BU5D_t3614634134* L_3 = ((  ObjectU5BU5D_t3614634134* (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (ObjectU5BU5D_t3614634134*)L_3;
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		ObjectU5BU5D_t3614634134* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get__count_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_7 = V_0;
		__this->set__items_1(L_7);
	}

IL_0031:
	{
		return;
	}
}
// T[] Boo.Lang.List`1<System.Object>::NewArray(System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* List_1_NewArray_m742551263_gshared (List_1_t61287617 * __this, int32_t ___minCapacity0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_0);
		int32_t L_1 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)1, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = (int32_t)((int32_t)((int32_t)L_1*(int32_t)2));
		int32_t L_2 = V_0;
		int32_t L_3 = ___minCapacity0;
		int32_t L_4 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_4));
	}
}
// System.Void Boo.Lang.List`1<System.Object>::InnerRemoveAt(System.Int32)
extern "C"  void List_1_InnerRemoveAt_m467546350_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_InnerRemoveAt_m467546350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		__this->set__count_2(((int32_t)((int32_t)L_0-(int32_t)1)));
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_2 = ___index0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_3);
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_6 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_7 = ___index0;
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_9 = ___index0;
		int32_t L_10 = (int32_t)__this->get__count_2();
		int32_t L_11 = ___index0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)((int32_t)((int32_t)L_7+(int32_t)1)), (Il2CppArray *)(Il2CppArray *)L_8, (int32_t)L_9, (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::InnerRemove(T)
extern "C"  bool List_1_InnerRemove_m845891310_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return (bool)1;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::CheckIndex(System.Int32)
extern "C"  int32_t List_1_CheckIndex_m2149663805_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2149663805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		IndexOutOfRangeException_t3527622107 * L_2 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3497760912(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		int32_t L_3 = ___index0;
		return L_3;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::NormalizeIndex(System.Int32)
extern "C"  int32_t List_1_NormalizeIndex_m2608178430_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__count_2();
		G_B3_0 = ((int32_t)((int32_t)L_1+(int32_t)L_2));
		goto IL_0015;
	}

IL_0014:
	{
		int32_t L_3 = ___index0;
		G_B3_0 = L_3;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// T Boo.Lang.List`1<System.Object>::Coerce(System.Object)
extern "C"  Il2CppObject * List_1_Coerce_m3843713328_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Coerce_m3843713328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9))))
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)));
	}

IL_0012:
	{
		Il2CppObject * L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, (Il2CppObject *)L_2, (Type_t *)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)));
	}
}
// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2103134696_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t232726623  DOGetter_1_Invoke_m1748194493_gshared (DOGetter_1_t2014832765 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1748194493((DOGetter_1_t2014832765 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color2_t232726623  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color2_t232726623  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3134053505_gshared (DOGetter_1_t2014832765 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  Color2_t232726623  DOGetter_1_EndInvoke_m3467158547_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color2_t232726623 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m188978433_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m4107339380_gshared (DOGetter_1_t1565154527 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m4107339380((DOGetter_1_t1565154527 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Double>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2666210620_gshared (DOGetter_1_t1565154527 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double DOGetter_1_EndInvoke_m4233629864_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m604038714_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m2109831897_gshared (DOGetter_1_t3853983590 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2109831897((DOGetter_1_t3853983590 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1887709557_gshared (DOGetter_1_t3853983590 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t DOGetter_1_EndInvoke_m912235863_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1068054853_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m111587642_gshared (DOGetter_1_t2691184179 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m111587642((DOGetter_1_t2691184179 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2803366776_gshared (DOGetter_1_t2691184179 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t DOGetter_1_EndInvoke_m1345976682_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2292700265_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m882816534_gshared (DOGetter_1_t176588141 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m882816534((DOGetter_1_t176588141 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3769142830_gshared (DOGetter_1_t176588141 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * DOGetter_1_EndInvoke_m4273483522_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3288120768_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m3647275797_gshared (DOGetter_1_t3858616074 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3647275797((DOGetter_1_t3858616074 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1690324125_gshared (DOGetter_1_t3858616074 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float DOGetter_1_EndInvoke_m2074726563_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1362595967_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m370264108_gshared (DOGetter_1_t3931788163 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m370264108((DOGetter_1_t3931788163 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3143608020_gshared (DOGetter_1_t3931788163 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t DOGetter_1_EndInvoke_m859660492_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3368190962_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m2202291235_gshared (DOGetter_1_t396335760 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2202291235((DOGetter_1_t396335760 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1007973739_gshared (DOGetter_1_t396335760 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t DOGetter_1_EndInvoke_m3446459513_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m4239784889_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t2020392075  DOGetter_1_Invoke_m222071538_gshared (DOGetter_1_t3802498217 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m222071538((DOGetter_1_t3802498217 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color_t2020392075  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t2020392075  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3894549130_gshared (DOGetter_1_t3802498217 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t2020392075  DOGetter_1_EndInvoke_m2918527242_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t2020392075 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3923641982_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t4030073918  DOGetter_1_Invoke_m1844179403_gshared (DOGetter_1_t1517212764 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1844179403((DOGetter_1_t1517212764 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2685409055_gshared (DOGetter_1_t1517212764 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  Quaternion_t4030073918  DOGetter_1_EndInvoke_m4117334265_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Quaternion_t4030073918 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2167974952_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t3681755626  DOGetter_1_Invoke_m205270839_gshared (DOGetter_1_t1168894472 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m205270839((DOGetter_1_t1168894472 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2542487891_gshared (DOGetter_1_t1168894472 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  Rect_t3681755626  DOGetter_1_EndInvoke_m47398349_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Rect_t3681755626 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2108428307_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t2243707579  DOGetter_1_Invoke_m3450181142_gshared (DOGetter_1_t4025813721 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3450181142((DOGetter_1_t4025813721 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2030350446_gshared (DOGetter_1_t4025813721 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t2243707579  DOGetter_1_EndInvoke_m1102230086_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector2_t2243707579 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1323234132_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t2243707580  DOGetter_1_Invoke_m3486946741_gshared (DOGetter_1_t4025813722 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3486946741((DOGetter_1_t4025813722 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2677033165_gshared (DOGetter_1_t4025813722 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t2243707580  DOGetter_1_EndInvoke_m3095315399_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector3_t2243707580 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m664413081_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t2243707581  DOGetter_1_Invoke_m3680105936_gshared (DOGetter_1_t4025813723 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3680105936((DOGetter_1_t4025813723 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1764844456_gshared (DOGetter_1_t4025813723 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  Vector4_t2243707581  DOGetter_1_EndInvoke_m2636742600_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector4_t2243707581 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1733732172_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3163525136_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3163525136((DOSetter_1_t1890955609 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3915298133_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color2_t232726623_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2796185966_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3181129925_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1726747861_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1726747861((DOSetter_1_t1441277371 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1311537432_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t4078015681_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2118077943_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m215017974_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m748616666_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m748616666((DOSetter_1_t3730106434 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3860168801_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3034368096_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1460312617_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2393688041_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2393688041((DOSetter_1_t2567307023 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m736671164_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m736671164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m977414887_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m4187492813_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1999305269_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1999305269((DOSetter_1_t52710985 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m4262435930_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pNewValue0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m936279787_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2613085532_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m184768384_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m184768384((DOSetter_1_t3734738918 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3656965161_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1222853090_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2057781435_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m236045679_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m236045679((DOSetter_1_t3807911007 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2436040648_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2552133209_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1650381910_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4121538054_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4121538054((DOSetter_1_t272458604 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2966874951_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m480288392_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3548377173_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3892954541_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3892954541((DOSetter_1_t3678621061 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2663926534_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2243481199_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2116426170_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m903072570_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m903072570((DOSetter_1_t1393335608 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1885597315_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Quaternion_t4030073918_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2987470368_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2570159628_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1386366628_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1386366628((DOSetter_1_t1045017316 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3580342655_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t3681755626_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1624612738_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1453722671_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2956451219_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2956451219((DOSetter_1_t3901936565 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2720752834_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3251197393_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m668528496_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4169715988_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4169715988((DOSetter_1_t3901936566 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m681046145_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m681046145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m949315410_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m9707445_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m212800661_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m212800661((DOSetter_1_t3901936567 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2980016964_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1324394315_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1803465319_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1803465319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3004544632_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3004544632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m157243687_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3909355140_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m266369415_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m266369415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , Color2_t232726623 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1733021291_gshared (TweenerCore_3_t3925803634 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3925803634 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2833266637 *)L_0, (TweenerCore_3_t3925803634 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m731417792_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2833266637 * L_1 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3925803634 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2833266637 *)L_1, (TweenerCore_3_t3925803634 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305  L_2 = ((  ColorOptions_t2213017305  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2014832765 *)NULL);
		__this->set_setter_58((DOSetter_1_t1890955609 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3163669337_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3163669337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2014832765 * L_0 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2014832765 *)L_0);
		((  Color2_t232726623  (*) (DOGetter_1_t2014832765 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t2014832765 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2044147238_gshared (TweenerCore_3_t3925803634 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m4133666392_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3682676813_gshared (TweenerCore_3_t3925803634 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2833266637 * L_5 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_8 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_9 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_10 = V_0;
		Color2_t232726623  L_11 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_12 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2014832765 *)L_8, (DOSetter_1_t1890955609 *)L_9, (float)L_10, (Color2_t232726623 )L_11, (Color2_t232726623 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2833266637 * L_16 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_19 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_20 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_21 = V_0;
		Color2_t232726623  L_22 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_23 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2014832765 *)L_19, (DOSetter_1_t1890955609 *)L_20, (float)L_21, (Color2_t232726623 )L_22, (Color2_t232726623 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1180323095_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1180323095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1112518902_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1112518902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m647570427_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m912926810_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2531960211_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2531960211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, double, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (double)((*(double*)((double*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3460574231_gshared (TweenerCore_3_t456598886 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t456598886 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3659029185 *)L_0, (TweenerCore_3_t456598886 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3599961708_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3659029185 * L_1 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t456598886 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3659029185 *)L_1, (TweenerCore_3_t456598886 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1565154527 *)NULL);
		__this->set_setter_58((DOSetter_1_t1441277371 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2094047477_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2094047477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1565154527 * L_0 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1565154527 *)L_0);
		((  double (*) (DOGetter_1_t1565154527 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1565154527 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3059104922_gshared (TweenerCore_3_t456598886 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m514828332_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3426149681_gshared (TweenerCore_3_t456598886 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3659029185 * L_5 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_8 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_9 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_10 = V_0;
		double L_11 = (double)__this->get_startValue_53();
		double L_12 = (double)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1565154527 *)L_8, (DOSetter_1_t1441277371 *)L_9, (float)L_10, (double)L_11, (double)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3659029185 * L_16 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_19 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_20 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_21 = V_0;
		double L_22 = (double)__this->get_startValue_53();
		double L_23 = (double)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1565154527 *)L_19, (DOSetter_1_t1441277371 *)L_20, (float)L_21, (double)L_22, (double)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2533780907_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2533780907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3831419980_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3831419980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3691364999_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3330381752_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2571270975_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2571270975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m230978731_gshared (TweenerCore_3_t3160108754 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3160108754 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2067571757 *)L_0, (TweenerCore_3_t3160108754 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m387790262_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2067571757 * L_1 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3160108754 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2067571757 *)L_1, (TweenerCore_3_t3160108754 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3853983590 *)NULL);
		__this->set_setter_58((DOSetter_1_t3730106434 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3448887169_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3448887169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3853983590 * L_0 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3853983590 *)L_0);
		((  int32_t (*) (DOGetter_1_t3853983590 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3853983590 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3455091692_gshared (TweenerCore_3_t3160108754 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3282992694_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2587998509_gshared (TweenerCore_3_t3160108754 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2067571757 * L_5 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_8 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_9 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_startValue_53();
		int32_t L_12 = (int32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3853983590 *)L_8, (DOSetter_1_t3730106434 *)L_9, (float)L_10, (int32_t)L_11, (int32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2067571757 * L_16 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_19 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_20 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_21 = V_0;
		int32_t L_22 = (int32_t)__this->get_startValue_53();
		int32_t L_23 = (int32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3853983590 *)L_19, (DOSetter_1_t3730106434 *)L_20, (float)L_21, (int32_t)L_22, (int32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1918761711_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1918761711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m2139817550_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2139817550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3842480115_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2582633306_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m1168812923_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m1168812923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, int64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (int64_t)((*(int64_t*)((int64_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m4270264951_gshared (TweenerCore_3_t3199111798 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3199111798 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2106574801 *)L_0, (TweenerCore_3_t3199111798 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1606553284_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2106574801 * L_1 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3199111798 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2106574801 *)L_1, (TweenerCore_3_t3199111798 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2691184179 *)NULL);
		__this->set_setter_58((DOSetter_1_t2567307023 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m1266531737_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1266531737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2691184179 * L_0 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2691184179 *)L_0);
		((  int64_t (*) (DOGetter_1_t2691184179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t2691184179 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m690694406_gshared (TweenerCore_3_t3199111798 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1097327556_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m1754088405_gshared (TweenerCore_3_t3199111798 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2106574801 * L_5 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_8 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_9 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_10 = V_0;
		int64_t L_11 = (int64_t)__this->get_startValue_53();
		int64_t L_12 = (int64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2691184179 *)L_8, (DOSetter_1_t2567307023 *)L_9, (float)L_10, (int64_t)L_11, (int64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2106574801 * L_16 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_19 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_20 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_21 = V_0;
		int64_t L_22 = (int64_t)__this->get_startValue_53();
		int64_t L_23 = (int64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2691184179 *)L_19, (DOSetter_1_t2567307023 *)L_20, (float)L_21, (int64_t)L_22, (int64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1198377687_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1198377687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3792476854_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3792476854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1306328571_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m188691034_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m4189518099_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m4189518099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2965628695_gshared (TweenerCore_3_t1705766462 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1705766462 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t613229465 *)L_0, (TweenerCore_3_t1705766462 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2741938284_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t613229465 * L_1 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1705766462 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t613229465 *)L_1, (TweenerCore_3_t1705766462 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m1363945525_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1363945525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t176588141 * L_0 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t176588141 *)L_0);
		((  Il2CppObject * (*) (DOGetter_1_t176588141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t176588141 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2609812250_gshared (TweenerCore_3_t1705766462 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3258896428_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m749792945_gshared (TweenerCore_3_t1705766462 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m749792945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t613229465 * L_5 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t613229465 * L_16 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3971101479_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3971101479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m536230576_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m536230576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2107526987_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2269915508_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m3838998779_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3838998779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m528864199_gshared (TweenerCore_3_t2082658550 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2082658550 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t990121553 *)L_0, (TweenerCore_3_t2082658550 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m492887674_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t990121553 * L_1 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2082658550 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t990121553 *)L_1, (TweenerCore_3_t2082658550 *)__this);
	}

IL_001a:
	{
		StringOptions_t2885323933  L_2 = ((  StringOptions_t2885323933  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2015486485_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2015486485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t176588141 * L_0 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t176588141 *)L_0);
		((  Il2CppObject * (*) (DOGetter_1_t176588141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t176588141 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3197074312_gshared (TweenerCore_3_t2082658550 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2775364066_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m4039624089_gshared (TweenerCore_3_t2082658550 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t990121553 * L_5 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_6 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_5);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_5, (StringOptions_t2885323933 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t990121553 * L_16 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_17 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_16);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_16, (StringOptions_t2885323933 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1849842838_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1849842838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3084865231_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3084865231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m407697134_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2067864171_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m114545046_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m114545046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)((*(float*)((float*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3054976738_gshared (TweenerCore_3_t2279406887 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2279406887 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1186869890 *)L_0, (TweenerCore_3_t2279406887 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3808154951_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1186869890 * L_1 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2279406887 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1186869890 *)L_1, (TweenerCore_3_t2279406887 *)__this);
	}

IL_001a:
	{
		FloatOptions_t1421548266  L_2 = ((  FloatOptions_t1421548266  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3858616074 *)NULL);
		__this->set_setter_58((DOSetter_1_t3734738918 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2802707620_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2802707620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3858616074 * L_0 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3858616074 *)L_0);
		((  float (*) (DOGetter_1_t3858616074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3858616074 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2385689477_gshared (TweenerCore_3_t2279406887 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3559394027_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3135202618_gshared (TweenerCore_3_t2279406887 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1186869890 * L_5 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_6 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_8 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_9 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_10 = V_0;
		float L_11 = (float)__this->get_startValue_53();
		float L_12 = (float)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_5);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_5, (FloatOptions_t1421548266 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3858616074 *)L_8, (DOSetter_1_t3734738918 *)L_9, (float)L_10, (float)L_11, (float)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1186869890 * L_16 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_17 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_19 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_20 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_21 = V_0;
		float L_22 = (float)__this->get_startValue_53();
		float L_23 = (float)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_16);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_16, (FloatOptions_t1421548266 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3858616074 *)L_19, (DOSetter_1_t3734738918 *)L_20, (float)L_21, (float)L_22, (float)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m4074697550_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4074697550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m806842373_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m806842373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2418838464_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m949598801_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m4204159460_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m4204159460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, uint32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1000169988_gshared (TweenerCore_3_t1648829745 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1648829745 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t556292748 *)L_0, (TweenerCore_3_t1648829745 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2146775889_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t556292748 * L_1 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1648829745 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t556292748 *)L_1, (TweenerCore_3_t1648829745 *)__this);
	}

IL_001a:
	{
		UintOptions_t2267095136  L_2 = ((  UintOptions_t2267095136  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3931788163 *)NULL);
		__this->set_setter_58((DOSetter_1_t3807911007 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m621478568_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m621478568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3931788163 * L_0 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3931788163 *)L_0);
		((  uint32_t (*) (DOGetter_1_t3931788163 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3931788163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3527224199_gshared (TweenerCore_3_t1648829745 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3369460217_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3033691518_gshared (TweenerCore_3_t1648829745 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t556292748 * L_5 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_6 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_8 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_9 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_10 = V_0;
		uint32_t L_11 = (uint32_t)__this->get_startValue_53();
		uint32_t L_12 = (uint32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_5);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_5, (UintOptions_t2267095136 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3931788163 *)L_8, (DOSetter_1_t3807911007 *)L_9, (float)L_10, (uint32_t)L_11, (uint32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t556292748 * L_16 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_17 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_19 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_20 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_21 = V_0;
		uint32_t L_22 = (uint32_t)__this->get_startValue_53();
		uint32_t L_23 = (uint32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_16);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_16, (UintOptions_t2267095136 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3931788163 *)L_19, (DOSetter_1_t3807911007 *)L_20, (float)L_21, (uint32_t)L_22, (uint32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m418004407_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m418004407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1875838422_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1875838422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2663196443_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1802430330_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m570000243_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m570000243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, uint64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m175513655_gshared (TweenerCore_3_t2937657178 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2937657178 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1845120181 *)L_0, (TweenerCore_3_t2937657178 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2800505548_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1845120181 * L_1 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2937657178 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1845120181 *)L_1, (TweenerCore_3_t2937657178 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t396335760 *)NULL);
		__this->set_setter_58((DOSetter_1_t272458604 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m1883068437_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1883068437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t396335760 * L_0 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t396335760 *)L_0);
		((  uint64_t (*) (DOGetter_1_t396335760 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t396335760 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1164357562_gshared (TweenerCore_3_t2937657178 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m501231052_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2278209809_gshared (TweenerCore_3_t2937657178 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1845120181 * L_5 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_8 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_9 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_10 = V_0;
		uint64_t L_11 = (uint64_t)__this->get_startValue_53();
		uint64_t L_12 = (uint64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t396335760 *)L_8, (DOSetter_1_t272458604 *)L_9, (float)L_10, (uint64_t)L_11, (uint64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1845120181 * L_16 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_19 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_20 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_21 = V_0;
		uint64_t L_22 = (uint64_t)__this->get_startValue_53();
		uint64_t L_23 = (uint64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t396335760 *)L_19, (DOSetter_1_t272458604 *)L_20, (float)L_21, (uint64_t)L_22, (uint64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2978185163_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2978185163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3818406250_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3818406250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m437196795_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3105054398_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m592431499_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m592431499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , Color_t2020392075 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1414404375_gshared (TweenerCore_3_t2998039394 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2998039394 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1905502397 *)L_0, (TweenerCore_3_t2998039394 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3158058770_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1905502397 * L_1 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2998039394 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1905502397 *)L_1, (TweenerCore_3_t2998039394 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305  L_2 = ((  ColorOptions_t2213017305  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3802498217 *)NULL);
		__this->set_setter_58((DOSetter_1_t3678621061 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m44760597_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m44760597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3802498217 * L_0 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3802498217 *)L_0);
		((  Color_t2020392075  (*) (DOGetter_1_t3802498217 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3802498217 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1284389728_gshared (TweenerCore_3_t2998039394 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1966872506_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2248740889_gshared (TweenerCore_3_t2998039394 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1905502397 * L_5 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_8 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_9 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_10 = V_0;
		Color_t2020392075  L_11 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_12 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3802498217 *)L_8, (DOSetter_1_t3678621061 *)L_9, (float)L_10, (Color_t2020392075 )L_11, (Color_t2020392075 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1905502397 * L_16 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_19 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_20 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_21 = V_0;
		Color_t2020392075  L_22 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_23 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3802498217 *)L_19, (DOSetter_1_t3678621061 *)L_20, (float)L_21, (Color_t2020392075 )L_22, (Color_t2020392075 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3146716579_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3146716579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m65087268_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m65087268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, Quaternion_t4030073918 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3873261087_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2618008744_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2618008744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, Quaternion_t4030073918 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2005239399_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2005239399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, Quaternion_t4030073918 , Quaternion_t4030073918 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m856670555_gshared (TweenerCore_3_t102288586 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3304718885 * L_0 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t102288586 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3304718885 *)L_0, (TweenerCore_3_t102288586 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1862249806_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3304718885 * L_0 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3304718885 * L_1 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t102288586 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3304718885 *)L_1, (TweenerCore_3_t102288586 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1517212764 *)NULL);
		__this->set_setter_58((DOSetter_1_t1393335608 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3999115877_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3999115877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1517212764 * L_0 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1517212764 *)L_0);
		((  Quaternion_t4030073918  (*) (DOGetter_1_t1517212764 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1517212764 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m116258440_gshared (TweenerCore_3_t102288586 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1801576590_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2916579489_gshared (TweenerCore_3_t102288586 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2916579489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3304718885 * L_5 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_8 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_9 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_10 = V_0;
		Quaternion_t4030073918  L_11 = (Quaternion_t4030073918 )__this->get_startValue_53();
		Quaternion_t4030073918  L_12 = (Quaternion_t4030073918 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Quaternion_t4030073918 , Quaternion_t4030073918 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3304718885 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1517212764 *)L_8, (DOSetter_1_t1393335608 *)L_9, (float)L_10, (Quaternion_t4030073918 )L_11, (Quaternion_t4030073918 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3304718885 * L_16 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_19 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_20 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_21 = V_0;
		Quaternion_t4030073918  L_22 = (Quaternion_t4030073918 )__this->get_startValue_53();
		Quaternion_t4030073918  L_23 = (Quaternion_t4030073918 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Quaternion_t4030073918 , Quaternion_t4030073918 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3304718885 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1517212764 *)L_19, (DOSetter_1_t1393335608 *)L_20, (float)L_21, (Quaternion_t4030073918 )L_22, (Quaternion_t4030073918 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2776308754_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2776308754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m839318221_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m839318221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m398039294_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3657890433_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2925494578_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2925494578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2275858234_gshared (TweenerCore_3_t1672798003 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1672798003 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t580261006 *)L_0, (TweenerCore_3_t1672798003 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1088438225_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t580261006 * L_1 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1672798003 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t580261006 *)L_1, (TweenerCore_3_t1672798003 *)__this);
	}

IL_001a:
	{
		QuaternionOptions_t466049668  L_2 = ((  QuaternionOptions_t466049668  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1517212764 *)NULL);
		__this->set_setter_58((DOSetter_1_t1393335608 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3629558540_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3629558540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1517212764 * L_0 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1517212764 *)L_0);
		((  Quaternion_t4030073918  (*) (DOGetter_1_t1517212764 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1517212764 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1578326919_gshared (TweenerCore_3_t1672798003 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3129492921_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2434462778_gshared (TweenerCore_3_t1672798003 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t580261006 * L_5 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_6 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_8 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_9 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_5);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_5, (QuaternionOptions_t466049668 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1517212764 *)L_8, (DOSetter_1_t1393335608 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t580261006 * L_16 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_17 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_19 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_20 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_16);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_16, (QuaternionOptions_t466049668 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1517212764 *)L_19, (DOSetter_1_t1393335608 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m231619286_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m231619286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1025968471_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1025968471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3831761276_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2524932243_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m1594244980_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m1594244980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , Rect_t3681755626 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1170036736_gshared (TweenerCore_3_t3065187631 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3065187631 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1972650634 *)L_0, (TweenerCore_3_t3065187631 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m48799595_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1972650634 * L_1 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3065187631 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1972650634 *)L_1, (TweenerCore_3_t3065187631 *)__this);
	}

IL_001a:
	{
		RectOptions_t3393635162  L_2 = ((  RectOptions_t3393635162  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1168894472 *)NULL);
		__this->set_setter_58((DOSetter_1_t1045017316 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3855338832_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3855338832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1168894472 * L_0 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1168894472 *)L_0);
		((  Rect_t3681755626  (*) (DOGetter_1_t1168894472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1168894472 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3599180177_gshared (TweenerCore_3_t3065187631 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2071201127_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m4231943806_gshared (TweenerCore_3_t3065187631 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1972650634 * L_5 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_6 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_8 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_9 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_10 = V_0;
		Rect_t3681755626  L_11 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_12 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_5);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_5, (RectOptions_t3393635162 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1168894472 *)L_8, (DOSetter_1_t1045017316 *)L_9, (float)L_10, (Rect_t3681755626 )L_11, (Rect_t3681755626 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1972650634 * L_16 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_17 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_19 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_20 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_21 = V_0;
		Rect_t3681755626  L_22 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_23 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_16);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_16, (RectOptions_t3393635162 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1168894472 *)L_19, (DOSetter_1_t1045017316 *)L_20, (float)L_21, (Rect_t3681755626 )L_22, (Rect_t3681755626 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3640631583_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3640631583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m508084142_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m508084142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4014959587_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m863616202_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2621469739_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2621469739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , Vector2_t2243707579 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2801490071_gshared (TweenerCore_3_t3250868854 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3250868854 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2158331857 *)L_0, (TweenerCore_3_t3250868854 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3315574948_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2158331857 * L_1 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3250868854 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2158331857 *)L_1, (TweenerCore_3_t3250868854 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261  L_2 = ((  VectorOptions_t293385261  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813721 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936565 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2299353629_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2299353629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813721 * L_0 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813721 *)L_0);
		((  Vector2_t2243707579  (*) (DOGetter_1_t4025813721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813721 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1266215114_gshared (TweenerCore_3_t3250868854 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2050368132_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3268195009_gshared (TweenerCore_3_t3250868854 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2158331857 * L_5 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_8 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_9 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector2_t2243707579  L_11 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_12 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813721 *)L_8, (DOSetter_1_t3901936565 *)L_9, (float)L_10, (Vector2_t2243707579 )L_11, (Vector2_t2243707579 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2158331857 * L_16 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_19 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_20 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector2_t2243707579  L_22 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_23 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813721 *)L_19, (DOSetter_1_t3901936565 *)L_20, (float)L_21, (Vector2_t2243707579 )L_22, (Vector2_t2243707579 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1012438200_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1012438200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m2708020849_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2708020849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2479530614_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2654203693_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m3787576666_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3787576666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2363190842_gshared (TweenerCore_3_t1622518059 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1622518059 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t529981062 *)L_0, (TweenerCore_3_t1622518059 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2389620197_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t529981062 * L_1 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1622518059 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t529981062 *)L_1, (TweenerCore_3_t1622518059 *)__this);
	}

IL_001a:
	{
		PathOptions_t2659884781  L_2 = ((  PathOptions_t2659884781  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3391320434_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3391320434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813722 * L_0 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813722 *)L_0);
		((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813722 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2529672523_gshared (TweenerCore_3_t1622518059 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2279545925_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m1795781652_gshared (TweenerCore_3_t1622518059 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t529981062 * L_5 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_6 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_5);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_5, (PathOptions_t2659884781 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t529981062 * L_16 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_17 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_16);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_16, (PathOptions_t2659884781 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1432615606_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1432615606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3270859719_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3270859719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4171376220_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2062234395_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2725288820_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2725288820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2915691848_gshared (TweenerCore_3_t1635203449 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1635203449 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t542666452 *)L_0, (TweenerCore_3_t1635203449 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3865103419_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t542666452 * L_1 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1635203449 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t542666452 *)L_1, (TweenerCore_3_t1635203449 *)__this);
	}

IL_001a:
	{
		Vector3ArrayOptions_t2672570171  L_2 = ((  Vector3ArrayOptions_t2672570171  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m841334684_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m841334684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813722 * L_0 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813722 *)L_0);
		((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813722 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1806502181_gshared (TweenerCore_3_t1635203449 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2282744407_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3384733786_gshared (TweenerCore_3_t1635203449 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t542666452 * L_5 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_6 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_5);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_5, (Vector3ArrayOptions_t2672570171 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t542666452 * L_16 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_17 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_16);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_16, (Vector3ArrayOptions_t2672570171 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m4171196319_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4171196319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m217970222_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m217970222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2834813539_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3688747850_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m624671403_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m624671403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1933156119_gshared (TweenerCore_3_t1108663466 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1108663466 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t16126469 *)L_0, (TweenerCore_3_t1108663466 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3846139684_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t16126469 * L_1 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1108663466 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t16126469 *)L_1, (TweenerCore_3_t1108663466 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261  L_2 = ((  VectorOptions_t293385261  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2965636509_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2965636509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813722 * L_0 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813722 *)L_0);
		((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813722 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m283403338_gshared (TweenerCore_3_t1108663466 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2716651012_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3434606145_gshared (TweenerCore_3_t1108663466 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t16126469 * L_5 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t16126469 * L_16 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1407812639_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1407812639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1286718126_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1286718126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2737049315_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3143172042_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m868439851_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m868439851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , Vector4_t2243707581 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1394097047_gshared (TweenerCore_3_t3261425374 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3261425374 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2168888377 *)L_0, (TweenerCore_3_t3261425374 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1082756004_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2168888377 * L_1 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3261425374 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2168888377 *)L_1, (TweenerCore_3_t3261425374 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261  L_2 = ((  VectorOptions_t293385261  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813723 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936567 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3552936477_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3552936477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813723 * L_0 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813723 *)L_0);
		((  Vector4_t2243707581  (*) (DOGetter_1_t4025813723 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813723 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m465544650_gshared (TweenerCore_3_t3261425374 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3303950980_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m140499649_gshared (TweenerCore_3_t3261425374 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m140499649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2168888377 * L_5 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_8 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_9 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector4_t2243707581  L_11 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_12 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813723 *)L_8, (DOSetter_1_t3901936567 *)L_9, (float)L_10, (Vector4_t2243707581 )L_11, (Vector4_t2243707581 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2168888377 * L_16 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_19 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_20 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector4_t2243707581  L_22 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_23 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813723 *)L_19, (DOSetter_1_t3901936567 *)L_20, (float)L_21, (Vector4_t2243707581 )L_22, (Vector4_t2243707581 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3622767690_gshared (ABSTweenPlugin_3_t2833266637 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1578194524_gshared (ABSTweenPlugin_3_t3659029185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m36425734_gshared (ABSTweenPlugin_3_t2067571757 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m223063124_gshared (ABSTweenPlugin_3_t2106574801 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2153224412_gshared (ABSTweenPlugin_3_t613229465 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m912798746_gshared (ABSTweenPlugin_3_t990121553 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4275081527_gshared (ABSTweenPlugin_3_t1186869890 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4159300161_gshared (ABSTweenPlugin_3_t556292748 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1254825468_gshared (ABSTweenPlugin_3_t1845120181 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1367930952_gshared (ABSTweenPlugin_3_t1905502397 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3467964222_gshared (ABSTweenPlugin_3_t3304718885 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m862077949_gshared (ABSTweenPlugin_3_t580261006 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1871528759_gshared (ABSTweenPlugin_3_t1972650634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1914255460_gshared (ABSTweenPlugin_3_t2158331857 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2241027021_gshared (ABSTweenPlugin_3_t529981062 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3497636823_gshared (ABSTweenPlugin_3_t542666452 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3289379812_gshared (ABSTweenPlugin_3_t16126469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3385119076_gshared (ABSTweenPlugin_3_t2168888377 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3015172213_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3615052821_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m3615052821((TweenCallback_1_t3418705418 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m711035500_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m1922991223_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3888348002_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m872757678_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m872757678((TweenCallback_1_t4036277265 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m138693653_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m3980063300_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m4000929921_gshared (TweenCallback_1_t3423337902 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3090779465_gshared (TweenCallback_1_t3423337902 * __this, float ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m3090779465((TweenCallback_1_t3423337902 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m3843094324_gshared (TweenCallback_1_t3423337902 * __this, float ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m3843094324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m801412299_gshared (TweenCallback_1_t3423337902 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void EnhancedUI.SmallList`1<System.Int32>::.ctor()
extern "C"  void SmallList_1__ctor_m3637520136_gshared (SmallList_1_t4132056671 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t SmallList_1_get_Item_m1701311436_gshared (SmallList_1_t4132056671 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Int32>::set_Item(System.Int32,T)
extern "C"  void SmallList_1_set_Item_m3952476333_gshared (SmallList_1_t4132056671 * __this, int32_t ___i0, int32_t ___value1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_1 = ___i0;
		int32_t L_2 = ___value1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int32_t)L_2);
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Int32>::ResizeArray()
extern "C"  void SmallList_1_ResizeArray_m3185937283_gshared (SmallList_1_t4132056671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_ResizeArray_m3185937283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m1875893177(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)64), /*hidden argument*/NULL);
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_2));
		goto IL_002f;
	}

IL_0027:
	{
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)((int32_t)64)));
	}

IL_002f:
	{
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_3)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		Int32U5BU5D_t3030399641* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0053:
	{
		Int32U5BU5D_t3030399641* L_7 = V_0;
		__this->set_data_0(L_7);
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Int32>::Clear()
extern "C"  void SmallList_1_Clear_m3702821337_gshared (SmallList_1_t4132056671 * __this, const MethodInfo* method)
{
	{
		__this->set_Count_1(0);
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Int32>::First()
extern "C"  int32_t SmallList_1_First_m2594423969_gshared (SmallList_1_t4132056671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_First_m2594423969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0016:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0020:
	{
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		NullCheck(L_3);
		int32_t L_4 = 0;
		int32_t L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// T EnhancedUI.SmallList`1<System.Int32>::Last()
extern "C"  int32_t SmallList_1_Last_m2505363309_gshared (SmallList_1_t4132056671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_Last_m2505363309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0016:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0020:
	{
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)((int32_t)L_4-(int32_t)1));
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Int32>::Add(T)
extern "C"  void SmallList_1_Add_m3002429181_gshared (SmallList_1_t4132056671 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((SmallList_1_t4132056671 *)__this);
		((  void (*) (SmallList_1_t4132056671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((SmallList_1_t4132056671 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0024:
	{
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		int32_t L_5 = ___item0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (int32_t)L_5);
		int32_t L_6 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_6+(int32_t)1)));
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Int32>::AddStart(T)
extern "C"  void SmallList_1_AddStart_m3077345645_gshared (SmallList_1_t4132056671 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___item0;
		NullCheck((SmallList_1_t4132056671 *)__this);
		((  void (*) (SmallList_1_t4132056671 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((SmallList_1_t4132056671 *)__this, (int32_t)L_0, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Int32>::Insert(T,System.Int32)
extern "C"  void SmallList_1_Insert_m1159669334_gshared (SmallList_1_t4132056671 * __this, int32_t ___item0, int32_t ___index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((SmallList_1_t4132056671 *)__this);
		((  void (*) (SmallList_1_t4132056671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((SmallList_1_t4132056671 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0024:
	{
		int32_t L_3 = (int32_t)__this->get_Count_1();
		V_0 = (int32_t)L_3;
		goto IL_004e;
	}

IL_0030:
	{
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_5 = V_0;
		Int32U5BU5D_t3030399641* L_6 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = ((int32_t)((int32_t)L_7-(int32_t)1));
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_9);
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
	}

IL_004e:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___index1;
		if ((((int32_t)L_11) > ((int32_t)L_12)))
		{
			goto IL_0030;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_13 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_14 = ___index1;
		int32_t L_15 = ___item0;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (int32_t)L_15);
		int32_t L_16 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_16+(int32_t)1)));
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Int32>::RemoveStart()
extern "C"  int32_t SmallList_1_RemoveStart_m1528215397_gshared (SmallList_1_t4132056671 * __this, const MethodInfo* method)
{
	{
		NullCheck((SmallList_1_t4132056671 *)__this);
		int32_t L_0 = ((  int32_t (*) (SmallList_1_t4132056671 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((SmallList_1_t4132056671 *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// T EnhancedUI.SmallList`1<System.Int32>::RemoveAt(System.Int32)
extern "C"  int32_t SmallList_1_RemoveAt_m800828173_gshared (SmallList_1_t4132056671 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_RemoveAt_m800828173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0080;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_3 = ___index0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = (int32_t)L_5;
		int32_t L_6 = ___index0;
		V_1 = (int32_t)L_6;
		goto IL_0048;
	}

IL_002a:
	{
		Int32U5BU5D_t3030399641* L_7 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_8 = V_1;
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = ((int32_t)((int32_t)L_10+(int32_t)1));
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)L_12);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_14) < ((int32_t)((int32_t)((int32_t)L_15-(int32_t)1)))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_16 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_16-(int32_t)1)));
		Int32U5BU5D_t3030399641* L_17 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_18 = (int32_t)__this->get_Count_1();
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_19 = V_2;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (int32_t)L_19);
		int32_t L_20 = V_0;
		return L_20;
	}

IL_0080:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_21 = V_2;
		return L_21;
	}
}
// T EnhancedUI.SmallList`1<System.Int32>::Remove(T)
extern "C"  int32_t SmallList_1_Remove_m3611915957_gshared (SmallList_1_t4132056671 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_Remove_m3611915957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004d;
	}

IL_001d:
	{
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = ___item0;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_5);
		bool L_7 = Int32_Equals_m753832628((int32_t*)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_0;
		NullCheck((SmallList_1_t4132056671 *)__this);
		int32_t L_9 = ((  int32_t (*) (SmallList_1_t4132056671 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((SmallList_1_t4132056671 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_9;
	}

IL_0049:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_001d;
		}
	}

IL_0059:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_13 = V_1;
		return L_13;
	}
}
// T EnhancedUI.SmallList`1<System.Int32>::RemoveEnd()
extern "C"  int32_t SmallList_1_RemoveEnd_m3820750818_gshared (SmallList_1_t4132056671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_RemoveEnd_m3820750818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_2-(int32_t)1)));
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_0 = (int32_t)L_6;
		Int32U5BU5D_t3030399641* L_7 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_8 = (int32_t)__this->get_Count_1();
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_9 = V_1;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)L_9);
		int32_t L_10 = V_0;
		return L_10;
	}

IL_0052:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_11 = V_1;
		return L_11;
	}
}
// System.Boolean EnhancedUI.SmallList`1<System.Int32>::Contains(T)
extern "C"  bool SmallList_1_Contains_m131744795_gshared (SmallList_1_t4132056671 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003e;
	}

IL_0014:
	{
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get_data_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = ___item0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_4);
		bool L_6 = Int32_Equals_m753832628((int32_t*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		return (bool)1;
	}

IL_003a:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Object>::.ctor()
extern "C"  void SmallList_1__ctor_m2370590199_gshared (SmallList_1_t454661222 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * SmallList_1_get_Item_m3672202013_gshared (SmallList_1_t454661222 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void SmallList_1_set_Item_m878800446_gshared (SmallList_1_t454661222 * __this, int32_t ___i0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_1 = ___i0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Il2CppObject *)L_2);
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Object>::ResizeArray()
extern "C"  void SmallList_1_ResizeArray_m1905725376_gshared (SmallList_1_t454661222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_ResizeArray_m1905725376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m1875893177(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)64), /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_2));
		goto IL_002f;
	}

IL_0027:
	{
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)((int32_t)64)));
	}

IL_002f:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_3)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		ObjectU5BU5D_t3614634134* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0053:
	{
		ObjectU5BU5D_t3614634134* L_7 = V_0;
		__this->set_data_0(L_7);
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Object>::Clear()
extern "C"  void SmallList_1_Clear_m2642997124_gshared (SmallList_1_t454661222 * __this, const MethodInfo* method)
{
	{
		__this->set_Count_1(0);
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Object>::First()
extern "C"  Il2CppObject * SmallList_1_First_m3892522670_gshared (SmallList_1_t454661222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_First_m3892522670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0016:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_2 = V_0;
		return L_2;
	}

IL_0020:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		NullCheck(L_3);
		int32_t L_4 = 0;
		Il2CppObject * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// T EnhancedUI.SmallList`1<System.Object>::Last()
extern "C"  Il2CppObject * SmallList_1_Last_m1188387254_gshared (SmallList_1_t454661222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_Last_m1188387254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0016:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_2 = V_0;
		return L_2;
	}

IL_0020:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)((int32_t)L_4-(int32_t)1));
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Object>::Add(T)
extern "C"  void SmallList_1_Add_m159922312_gshared (SmallList_1_t454661222 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((SmallList_1_t454661222 *)__this);
		((  void (*) (SmallList_1_t454661222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((SmallList_1_t454661222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0024:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		Il2CppObject * L_5 = ___item0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Il2CppObject *)L_5);
		int32_t L_6 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_6+(int32_t)1)));
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Object>::AddStart(T)
extern "C"  void SmallList_1_AddStart_m3370975066_gshared (SmallList_1_t454661222 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((SmallList_1_t454661222 *)__this);
		((  void (*) (SmallList_1_t454661222 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((SmallList_1_t454661222 *)__this, (Il2CppObject *)L_0, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Object>::Insert(T,System.Int32)
extern "C"  void SmallList_1_Insert_m2240984381_gshared (SmallList_1_t454661222 * __this, Il2CppObject * ___item0, int32_t ___index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((SmallList_1_t454661222 *)__this);
		((  void (*) (SmallList_1_t454661222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((SmallList_1_t454661222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0024:
	{
		int32_t L_3 = (int32_t)__this->get_Count_1();
		V_0 = (int32_t)L_3;
		goto IL_004e;
	}

IL_0030:
	{
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_5 = V_0;
		ObjectU5BU5D_t3614634134* L_6 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = ((int32_t)((int32_t)L_7-(int32_t)1));
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_9);
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
	}

IL_004e:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___index1;
		if ((((int32_t)L_11) > ((int32_t)L_12)))
		{
			goto IL_0030;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_14 = ___index1;
		Il2CppObject * L_15 = ___item0;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
		int32_t L_16 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_16+(int32_t)1)));
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Object>::RemoveStart()
extern "C"  Il2CppObject * SmallList_1_RemoveStart_m49916000_gshared (SmallList_1_t454661222 * __this, const MethodInfo* method)
{
	{
		NullCheck((SmallList_1_t454661222 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (SmallList_1_t454661222 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((SmallList_1_t454661222 *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// T EnhancedUI.SmallList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  Il2CppObject * SmallList_1_RemoveAt_m4084153238_gshared (SmallList_1_t454661222 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_RemoveAt_m4084153238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0080;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_3 = ___index0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = (Il2CppObject *)L_5;
		int32_t L_6 = ___index0;
		V_1 = (int32_t)L_6;
		goto IL_0048;
	}

IL_002a:
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_8 = V_1;
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = ((int32_t)((int32_t)L_10+(int32_t)1));
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Il2CppObject *)L_12);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_14) < ((int32_t)((int32_t)((int32_t)L_15-(int32_t)1)))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_16 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_16-(int32_t)1)));
		ObjectU5BU5D_t3614634134* L_17 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_18 = (int32_t)__this->get_Count_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_19 = V_2;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_19);
		Il2CppObject * L_20 = V_0;
		return L_20;
	}

IL_0080:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_21 = V_2;
		return L_21;
	}
}
// T EnhancedUI.SmallList`1<System.Object>::Remove(T)
extern "C"  Il2CppObject * SmallList_1_Remove_m1208117324_gshared (SmallList_1_t454661222 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_Remove_m1208117324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004d;
	}

IL_001d:
	{
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_4 = ___item0;
		NullCheck((Il2CppObject *)(*((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))));
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3)))), (Il2CppObject *)L_4);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_6 = V_0;
		NullCheck((SmallList_1_t454661222 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (SmallList_1_t454661222 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((SmallList_1_t454661222 *)__this, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_7;
	}

IL_0049:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_001d;
		}
	}

IL_0059:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_11 = V_1;
		return L_11;
	}
}
// T EnhancedUI.SmallList`1<System.Object>::RemoveEnd()
extern "C"  Il2CppObject * SmallList_1_RemoveEnd_m687006973_gshared (SmallList_1_t454661222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_RemoveEnd_m687006973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_2-(int32_t)1)));
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_0 = (Il2CppObject *)L_6;
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_8 = (int32_t)__this->get_Count_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_9 = V_1;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Il2CppObject *)L_9);
		Il2CppObject * L_10 = V_0;
		return L_10;
	}

IL_0052:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_11 = V_1;
		return L_11;
	}
}
// System.Boolean EnhancedUI.SmallList`1<System.Object>::Contains(T)
extern "C"  bool SmallList_1_Contains_m1920006740_gshared (SmallList_1_t454661222 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003e;
	}

IL_0014:
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_data_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Il2CppObject * L_3 = ___item0;
		NullCheck((Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		return (bool)1;
	}

IL_003a:
	{
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Single>::.ctor()
extern "C"  void SmallList_1__ctor_m4083048422_gshared (SmallList_1_t4136689155 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Single>::get_Item(System.Int32)
extern "C"  float SmallList_1_get_Item_m2955470980_gshared (SmallList_1_t4136689155 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Single>::set_Item(System.Int32,T)
extern "C"  void SmallList_1_set_Item_m1190287797_gshared (SmallList_1_t4136689155 * __this, int32_t ___i0, float ___value1, const MethodInfo* method)
{
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_1 = ___i0;
		float L_2 = ___value1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (float)L_2);
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Single>::ResizeArray()
extern "C"  void SmallList_1_ResizeArray_m4051651571_gshared (SmallList_1_t4136689155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_ResizeArray_m4051651571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		SingleU5BU5D_t577127397* L_1 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m1875893177(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)64), /*hidden argument*/NULL);
		V_0 = (SingleU5BU5D_t577127397*)((SingleU5BU5D_t577127397*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)L_2));
		goto IL_002f;
	}

IL_0027:
	{
		V_0 = (SingleU5BU5D_t577127397*)((SingleU5BU5D_t577127397*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (uint32_t)((int32_t)64)));
	}

IL_002f:
	{
		SingleU5BU5D_t577127397* L_3 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_3)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		SingleU5BU5D_t577127397* L_5 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		SingleU5BU5D_t577127397* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0, /*hidden argument*/NULL);
	}

IL_0053:
	{
		SingleU5BU5D_t577127397* L_7 = V_0;
		__this->set_data_0(L_7);
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Single>::Clear()
extern "C"  void SmallList_1_Clear_m2678824321_gshared (SmallList_1_t4136689155 * __this, const MethodInfo* method)
{
	{
		__this->set_Count_1(0);
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Single>::First()
extern "C"  float SmallList_1_First_m3377933817_gshared (SmallList_1_t4136689155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_First_m3377933817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0016:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		return L_2;
	}

IL_0020:
	{
		SingleU5BU5D_t577127397* L_3 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		NullCheck(L_3);
		int32_t L_4 = 0;
		float L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// T EnhancedUI.SmallList`1<System.Single>::Last()
extern "C"  float SmallList_1_Last_m2122658061_gshared (SmallList_1_t4136689155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_Last_m2122658061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (L_1)
		{
			goto IL_0020;
		}
	}

IL_0016:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		return L_2;
	}

IL_0020:
	{
		SingleU5BU5D_t577127397* L_3 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)((int32_t)L_4-(int32_t)1));
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Single>::Add(T)
extern "C"  void SmallList_1_Add_m271778453_gshared (SmallList_1_t4136689155 * __this, float ___item0, const MethodInfo* method)
{
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		SingleU5BU5D_t577127397* L_2 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((SmallList_1_t4136689155 *)__this);
		((  void (*) (SmallList_1_t4136689155 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((SmallList_1_t4136689155 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0024:
	{
		SingleU5BU5D_t577127397* L_3 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		float L_5 = ___item0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (float)L_5);
		int32_t L_6 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_6+(int32_t)1)));
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Single>::AddStart(T)
extern "C"  void SmallList_1_AddStart_m1022833253_gshared (SmallList_1_t4136689155 * __this, float ___item0, const MethodInfo* method)
{
	{
		float L_0 = ___item0;
		NullCheck((SmallList_1_t4136689155 *)__this);
		((  void (*) (SmallList_1_t4136689155 *, float, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((SmallList_1_t4136689155 *)__this, (float)L_0, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void EnhancedUI.SmallList`1<System.Single>::Insert(T,System.Int32)
extern "C"  void SmallList_1_Insert_m2749435468_gshared (SmallList_1_t4136689155 * __this, float ___item0, int32_t ___index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		SingleU5BU5D_t577127397* L_2 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((SmallList_1_t4136689155 *)__this);
		((  void (*) (SmallList_1_t4136689155 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((SmallList_1_t4136689155 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
	}

IL_0024:
	{
		int32_t L_3 = (int32_t)__this->get_Count_1();
		V_0 = (int32_t)L_3;
		goto IL_004e;
	}

IL_0030:
	{
		SingleU5BU5D_t577127397* L_4 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_5 = V_0;
		SingleU5BU5D_t577127397* L_6 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = ((int32_t)((int32_t)L_7-(int32_t)1));
		float L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (float)L_9);
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10-(int32_t)1));
	}

IL_004e:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = ___index1;
		if ((((int32_t)L_11) > ((int32_t)L_12)))
		{
			goto IL_0030;
		}
	}
	{
		SingleU5BU5D_t577127397* L_13 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_14 = ___index1;
		float L_15 = ___item0;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (float)L_15);
		int32_t L_16 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_16+(int32_t)1)));
		return;
	}
}
// T EnhancedUI.SmallList`1<System.Single>::RemoveStart()
extern "C"  float SmallList_1_RemoveStart_m3662553693_gshared (SmallList_1_t4136689155 * __this, const MethodInfo* method)
{
	{
		NullCheck((SmallList_1_t4136689155 *)__this);
		float L_0 = ((  float (*) (SmallList_1_t4136689155 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((SmallList_1_t4136689155 *)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// T EnhancedUI.SmallList`1<System.Single>::RemoveAt(System.Int32)
extern "C"  float SmallList_1_RemoveAt_m2570345085_gshared (SmallList_1_t4136689155 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_RemoveAt_m2570345085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0080;
		}
	}
	{
		SingleU5BU5D_t577127397* L_2 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_3 = ___index0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		float L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = (float)L_5;
		int32_t L_6 = ___index0;
		V_1 = (int32_t)L_6;
		goto IL_0048;
	}

IL_002a:
	{
		SingleU5BU5D_t577127397* L_7 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_8 = V_1;
		SingleU5BU5D_t577127397* L_9 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = ((int32_t)((int32_t)L_10+(int32_t)1));
		float L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (float)L_12);
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_14) < ((int32_t)((int32_t)((int32_t)L_15-(int32_t)1)))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_16 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_16-(int32_t)1)));
		SingleU5BU5D_t577127397* L_17 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_18 = (int32_t)__this->get_Count_1();
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_2));
		float L_19 = V_2;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (float)L_19);
		float L_20 = V_0;
		return L_20;
	}

IL_0080:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_2));
		float L_21 = V_2;
		return L_21;
	}
}
// T EnhancedUI.SmallList`1<System.Single>::Remove(T)
extern "C"  float SmallList_1_Remove_m3906016309_gshared (SmallList_1_t4136689155 * __this, float ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_Remove_m3906016309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004d;
	}

IL_001d:
	{
		SingleU5BU5D_t577127397* L_2 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		float L_4 = ___item0;
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_5);
		bool L_7 = Single_Equals_m3679433096((float*)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))), (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_0;
		NullCheck((SmallList_1_t4136689155 *)__this);
		float L_9 = ((  float (*) (SmallList_1_t4136689155 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((SmallList_1_t4136689155 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_9;
	}

IL_0049:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_001d;
		}
	}

IL_0059:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_1));
		float L_13 = V_1;
		return L_13;
	}
}
// T EnhancedUI.SmallList`1<System.Single>::RemoveEnd()
extern "C"  float SmallList_1_RemoveEnd_m3791543898_gshared (SmallList_1_t4136689155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmallList_1_RemoveEnd_m3791543898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_Count_1();
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_Count_1();
		__this->set_Count_1(((int32_t)((int32_t)L_2-(int32_t)1)));
		SingleU5BU5D_t577127397* L_3 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_4 = (int32_t)__this->get_Count_1();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_0 = (float)L_6;
		SingleU5BU5D_t577127397* L_7 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_8 = (int32_t)__this->get_Count_1();
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_1));
		float L_9 = V_1;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (float)L_9);
		float L_10 = V_0;
		return L_10;
	}

IL_0052:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_1));
		float L_11 = V_1;
		return L_11;
	}
}
// System.Boolean EnhancedUI.SmallList`1<System.Single>::Contains(T)
extern "C"  bool SmallList_1_Contains_m1491821883_gshared (SmallList_1_t4136689155 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t577127397* L_0 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003e;
	}

IL_0014:
	{
		SingleU5BU5D_t577127397* L_1 = (SingleU5BU5D_t577127397*)__this->get_data_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		float L_3 = ___item0;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), &L_4);
		bool L_6 = Single_Equals_m3679433096((float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		return (bool)1;
	}

IL_003a:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_Count_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void EnumString`1<Localization.LocalizeKey>::.ctor()
extern "C"  void EnumString_1__ctor_m3389165715_gshared (EnumString_1_t4119734292 * __this, const MethodInfo* method)
{
	{
		NullCheck((EnumStringBase_t3052423213 *)__this);
		EnumStringBase__ctor_m4120175636((EnumStringBase_t3052423213 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnumString`1<Localization.LocalizeKey>::.ctor(T)
extern "C"  void EnumString_1__ctor_m4266055227_gshared (EnumString_1_t4119734292 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		NullCheck((EnumStringBase_t3052423213 *)__this);
		EnumStringBase__ctor_m4120175636((EnumStringBase_t3052423213 *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___value0;
		NullCheck((EnumString_1_t4119734292 *)__this);
		((  void (*) (EnumString_1_t4119734292 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((EnumString_1_t4119734292 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// T EnumString`1<Localization.LocalizeKey>::op_Implicit(EnumString`1<T>)
extern "C"  int32_t EnumString_1_op_Implicit_m38252808_gshared (Il2CppObject * __this /* static, unused */, EnumString_1_t4119734292 * ___Obj0, const MethodInfo* method)
{
	{
		EnumString_1_t4119734292 * L_0 = ___Obj0;
		NullCheck((EnumString_1_t4119734292 *)L_0);
		int32_t L_1 = ((  int32_t (*) (EnumString_1_t4119734292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((EnumString_1_t4119734292 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Type EnumString`1<Localization.LocalizeKey>::get_Type()
extern "C"  Type_t * EnumString_1_get_Type_m3947132266_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumString_1_get_Type_m3947132266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Nullable`1<T> EnumString`1<Localization.LocalizeKey>::get_Cache()
extern "C"  Nullable_1_t1611487549  EnumString_1_get_Cache_m832930558_gshared (EnumString_1_t4119734292 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t1611487549  L_0 = (Nullable_1_t1611487549 )__this->get_m_Cache_1();
		return L_0;
	}
}
// System.String EnumString`1<Localization.LocalizeKey>::get_Text()
extern "C"  String_t* EnumString_1_get_Text_m380526242_gshared (EnumString_1_t4119734292 * __this, const MethodInfo* method)
{
	{
		NullCheck((EnumStringBase_t3052423213 *)__this);
		String_t* L_0 = EnumStringBase_get_Text_m1884861569((EnumStringBase_t3052423213 *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void EnumString`1<Localization.LocalizeKey>::set_Text(System.String)
extern "C"  void EnumString_1_set_Text_m193384397_gshared (EnumString_1_t4119734292 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumString_1_set_Text_m193384397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t1611487549  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___value0;
		NullCheck((EnumStringBase_t3052423213 *)__this);
		EnumStringBase_set_Text_m2870456262((EnumStringBase_t3052423213 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		Initobj (Nullable_1_t1611487549_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t1611487549  L_1 = V_0;
		__this->set_m_Cache_1(L_1);
		return;
	}
}
// T EnumString`1<Localization.LocalizeKey>::get_Value()
extern "C"  int32_t EnumString_1_get_Value_m3761765300_gshared (EnumString_1_t4119734292 * __this, const MethodInfo* method)
{
	Nullable_1_t1611487549  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t1611487549  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Nullable_1_t1611487549  L_0 = (Nullable_1_t1611487549 )__this->get_m_Cache_1();
		V_0 = (Nullable_1_t1611487549 )L_0;
		bool L_1 = Nullable_1_get_HasValue_m3487298155((Nullable_1_t1611487549 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		if (!((((int32_t)L_1) == ((int32_t)0))? 1 : 0))
		{
			goto IL_002c;
		}
	}
	{
		NullCheck((EnumStringBase_t3052423213 *)__this);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String EnumStringBase::get_Text() */, (EnumStringBase_t3052423213 *)__this);
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Nullable_1_t1611487549  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Nullable_1__ctor_m2203533598(&L_4, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_m_Cache_1(L_4);
	}

IL_002c:
	{
		Nullable_1_t1611487549  L_5 = (Nullable_1_t1611487549 )__this->get_m_Cache_1();
		V_1 = (Nullable_1_t1611487549 )L_5;
		int32_t L_6 = Nullable_1_get_Value_m1727227211((Nullable_1_t1611487549 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_6;
	}
}
// System.Void EnumString`1<Localization.LocalizeKey>::set_Value(T)
extern "C"  void EnumString_1_set_Value_m3795483797_gshared (EnumString_1_t4119734292 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (&___value0));
		NullCheck((Il2CppObject *)L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_0);
		NullCheck((EnumStringBase_t3052423213 *)__this);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void EnumStringBase::set_Text(System.String) */, (EnumStringBase_t3052423213 *)__this, (String_t*)L_1);
		int32_t L_2 = ___value0;
		Nullable_1_t1611487549  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m2203533598(&L_3, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_m_Cache_1(L_3);
		return;
	}
}
// System.Boolean EnumString`1<Localization.LocalizeKey>::IsDefined(System.String)
extern "C"  bool EnumString_1_IsDefined_m522389272_gshared (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_1;
	}
}
// T EnumString`1<Localization.LocalizeKey>::ParseOrDefault(System.String)
extern "C"  int32_t EnumString_1_ParseOrDefault_m1766823833_gshared (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		int32_t L_1 = ((  int32_t (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_1;
	}
}
// System.Nullable`1<T> EnumString`1<Localization.LocalizeKey>::ParseOrNull(System.String)
extern "C"  Nullable_1_t1611487549  EnumString_1_ParseOrNull_m2123473538_gshared (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Nullable_1_t1611487549  L_1 = ((  Nullable_1_t1611487549  (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (String_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_1;
	}
}
// System.Int32 EnumTableCache`1<Localization.LocalizeKey>::get_Count()
extern "C"  int32_t EnumTableCache_1_get_Count_m2742748690_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Dictionary_2_t968233200 * L_0 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Table_1();
		NullCheck((Dictionary_2_t968233200 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t968233200 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Dictionary_2_t968233200 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// T EnumTableCache`1<Localization.LocalizeKey>::GetValue(System.String)
extern "C"  int32_t EnumTableCache_1_GetValue_m3673313412_gshared (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Initialized_0();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Dictionary_2_t968233200 * L_1 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Table_1();
		String_t* L_2 = ___name0;
		NullCheck((Dictionary_2_t968233200 *)L_1);
		int32_t L_3 = ((  int32_t (*) (Dictionary_2_t968233200 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->methodPointer)((Dictionary_2_t968233200 *)L_1, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_3;
	}
}
// T EnumTableCache`1<Localization.LocalizeKey>::ParseOrDefault(System.String)
extern "C"  int32_t EnumTableCache_1_ParseOrDefault_m3237788542_gshared (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Initialized_0();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Dictionary_2_t968233200 * L_1 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Table_1();
		String_t* L_2 = ___name0;
		NullCheck((Dictionary_2_t968233200 *)L_1);
		((  bool (*) (Dictionary_2_t968233200 *, String_t*, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Dictionary_2_t968233200 *)L_1, (String_t*)L_2, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Nullable`1<T> EnumTableCache`1<Localization.LocalizeKey>::ParseOrNull(System.String)
extern "C"  Nullable_1_t1611487549  EnumTableCache_1_ParseOrNull_m3764123207_gshared (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumTableCache_1_ParseOrNull_m3764123207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Nullable_1_t1611487549  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Initialized_0();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Dictionary_2_t968233200 * L_1 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Table_1();
		String_t* L_2 = ___name0;
		NullCheck((Dictionary_2_t968233200 *)L_1);
		bool L_3 = ((  bool (*) (Dictionary_2_t968233200 *, String_t*, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((Dictionary_2_t968233200 *)L_1, (String_t*)L_2, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_4 = V_0;
		Nullable_1_t1611487549  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Nullable_1__ctor_m2203533598(&L_5, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}

IL_0028:
	{
		Initobj (Nullable_1_t1611487549_il2cpp_TypeInfo_var, (&V_1));
		Nullable_1_t1611487549  L_6 = V_1;
		return L_6;
	}
}
// System.Boolean EnumTableCache`1<Localization.LocalizeKey>::IsDefined(System.String)
extern "C"  bool EnumTableCache_1_IsDefined_m786775621_gshared (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Initialized_0();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Dictionary_2_t968233200 * L_1 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Table_1();
		String_t* L_2 = ___name0;
		NullCheck((Dictionary_2_t968233200 *)L_1);
		bool L_3 = ((  bool (*) (Dictionary_2_t968233200 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)((Dictionary_2_t968233200 *)L_1, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_3;
	}
}
// System.Void EnumTableCache`1<Localization.LocalizeKey>::Init()
extern "C"  void EnumTableCache_1_Init_m2830604264_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumTableCache_1_Init_m2830604264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppArray * V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	String_t* V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Initialized_0();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_1;
		Type_t * L_2 = V_0;
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsEnum_m313908919((Type_t *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		Type_t * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppArray * L_5 = Enum_GetValues_m2107059536(NULL /*static, unused*/, (Type_t *)L_4, /*hidden argument*/NULL);
		V_1 = (Il2CppArray *)L_5;
		Il2CppArray * L_6 = V_1;
		NullCheck((Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Length_m1498215565((Il2CppArray *)L_6, /*hidden argument*/NULL);
		Dictionary_2_t968233200 * L_8 = (Dictionary_2_t968233200 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (Dictionary_2_t968233200 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->methodPointer)(L_8, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set_m_Table_1(L_8);
		Il2CppArray * L_9 = V_1;
		NullCheck((Il2CppArray *)L_9);
		Il2CppObject * L_10 = Array_GetEnumerator_m2284404958((Il2CppArray *)L_9, /*hidden argument*/NULL);
		V_3 = (Il2CppObject *)L_10;
	}

IL_0040:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006d;
		}

IL_0045:
		{
			Il2CppObject * L_11 = V_3;
			NullCheck((Il2CppObject *)L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			V_2 = (int32_t)((*(int32_t*)((int32_t*)UnBox(L_12, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)))));
			Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11), (&V_2));
			NullCheck((Il2CppObject *)L_13);
			String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_13);
			V_4 = (String_t*)L_14;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			Dictionary_2_t968233200 * L_15 = ((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_m_Table_1();
			String_t* L_16 = V_4;
			int32_t L_17 = V_2;
			NullCheck((Dictionary_2_t968233200 *)L_15);
			((  void (*) (Dictionary_2_t968233200 *, String_t*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->methodPointer)((Dictionary_2_t968233200 *)L_15, (String_t*)L_16, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		}

IL_006d:
		{
			Il2CppObject * L_18 = V_3;
			NullCheck((Il2CppObject *)L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			if (L_19)
			{
				goto IL_0045;
			}
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x93, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_20 = V_3;
			Il2CppObject * L_21 = (Il2CppObject *)((Il2CppObject *)IsInst(L_20, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_5 = (Il2CppObject *)L_21;
			if (!L_21)
			{
				goto IL_0092;
			}
		}

IL_008b:
		{
			Il2CppObject * L_22 = V_5;
			NullCheck((Il2CppObject *)L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_22);
		}

IL_0092:
		{
			IL2CPP_END_FINALLY(125)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0093:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((EnumTableCache_1_t3575987435_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set_m_Initialized_0((bool)1);
		return;
	}
}
// System.Void EnumTableCache`1<Localization.LocalizeKey>::.cctor()
extern "C"  void EnumTableCache_1__cctor_m3570534191_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.UnityDownloadStream/BlockingCollection`1<System.Object>::Dispose()
extern "C"  void BlockingCollection_1_Dispose_m1100081259_gshared (BlockingCollection_1_t575307195 * __this, const MethodInfo* method)
{
	{
		Semaphore_t159839144 * L_0 = (Semaphore_t159839144 *)__this->get__count_0();
		NullCheck((WaitHandle_t677569169 *)L_0);
		VirtActionInvoker0::Invoke(7 /* System.Void System.Threading.WaitHandle::Close() */, (WaitHandle_t677569169 *)L_0);
		return;
	}
}
// System.Void Firebase.UnityDownloadStream/BlockingCollection`1<System.Object>::Add(T)
extern "C"  void BlockingCollection_1_Add_m2357368053_gshared (BlockingCollection_1_t575307195 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2509106130 * L_0 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t2509106130 * L_2 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
		Il2CppObject * L_3 = ___item0;
		NullCheck((Queue_1_t2509106130 *)L_2);
		((  void (*) (Queue_1_t2509106130 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Queue_1_t2509106130 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Semaphore_t159839144 * L_5 = (Semaphore_t159839144 *)__this->get__count_0();
		NullCheck((Semaphore_t159839144 *)L_5);
		Semaphore_Release_m1642555081((Semaphore_t159839144 *)L_5, (int32_t)1, /*hidden argument*/NULL);
		return;
	}
}
// T Firebase.UnityDownloadStream/BlockingCollection`1<System.Object>::Take()
extern "C"  Il2CppObject * BlockingCollection_1_Take_m2773562012_gshared (BlockingCollection_1_t575307195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlockingCollection_1_Take_m2773562012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Semaphore_t159839144 * L_0 = (Semaphore_t159839144 *)__this->get__count_0();
			NullCheck((WaitHandle_t677569169 *)L_0);
			bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne() */, (WaitHandle_t677569169 *)L_0);
			if (!L_1)
			{
				goto IL_0035;
			}
		}

IL_0010:
		{
			Queue_1_t2509106130 * L_2 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
			V_0 = (Il2CppObject *)L_2;
			Il2CppObject * L_3 = V_0;
			Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		}

IL_001d:
		try
		{ // begin try (depth: 2)
			Queue_1_t2509106130 * L_4 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
			NullCheck((Queue_1_t2509106130 *)L_4);
			Il2CppObject * L_5 = ((  Il2CppObject * (*) (Queue_1_t2509106130 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Queue_1_t2509106130 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			V_1 = (Il2CppObject *)L_5;
			IL2CPP_LEAVE(0x4A, FINALLY_002e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_002e;
		}

FINALLY_002e:
		{ // begin finally (depth: 2)
			Il2CppObject * L_6 = V_0;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(46)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(46)
		{
			IL2CPP_JUMP_TBL(0x4A, IL_004a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0035:
		{
			goto IL_0040;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_003a;
		throw e;
	}

CATCH_003a:
	{ // begin catch(System.ObjectDisposedException)
		goto IL_0040;
	} // end catch (depth: 1)

IL_0040:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_7 = V_2;
		return L_7;
	}

IL_004a:
	{
		Il2CppObject * L_8 = V_1;
		return L_8;
	}
}
// System.Void GameObjectExtensions/<GetComponentsInChildrenWithoutSelf>c__AnonStorey2`1<System.Object>::.ctor()
extern "C"  void U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey2_1__ctor_m604619897_gshared (U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey2_1_t1576971222 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameObjectExtensions/<GetComponentsInChildrenWithoutSelf>c__AnonStorey2`1<System.Object>::<>m__0(T)
extern "C"  bool U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey2_1_U3CU3Em__0_m261577356_gshared (U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey2_1_t1576971222 * __this, Il2CppObject * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey2_1_U3CU3Em__0_m261577356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)__this->get_self_0();
		NullCheck((Component_t3819376471 *)(*(&___c0)));
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835((Component_t3819376471 *)(*(&___c0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GameObjectExtensions/<GetComponentsInChildrenWithoutSelf>c__AnonStorey3`1<System.Object>::.ctor()
extern "C"  void U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey3_1__ctor_m4141474778_gshared (U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey3_1_t987727325 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameObjectExtensions/<GetComponentsInChildrenWithoutSelf>c__AnonStorey3`1<System.Object>::<>m__0(T)
extern "C"  bool U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey3_1_U3CU3Em__0_m2203408679_gshared (U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey3_1_t987727325 * __this, Il2CppObject * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetComponentsInChildrenWithoutSelfU3Ec__AnonStorey3_1_U3CU3Em__0_m2203408679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Component_t3819376471 * L_0 = (Component_t3819376471 *)__this->get_self_0();
		NullCheck((Component_t3819376471 *)L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_0, /*hidden argument*/NULL);
		NullCheck((Component_t3819376471 *)(*(&___c0)));
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835((Component_t3819376471 *)(*(&___c0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, (Object_t1021602117 *)L_1, (Object_t1021602117 *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void HitBuilder`1<System.Object>::.ctor()
extern "C"  void HitBuilder_1__ctor_m1281725022_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitBuilder_1__ctor_m1281725022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1037045868 * L_0 = (Dictionary_2_t1037045868 *)il2cpp_codegen_object_new(Dictionary_2_t1037045868_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3038559641(L_0, /*hidden argument*/Dictionary_2__ctor_m3038559641_MethodInfo_var);
		__this->set_customDimensions_0(L_0);
		Dictionary_2_t1037045868 * L_1 = (Dictionary_2_t1037045868 *)il2cpp_codegen_object_new(Dictionary_2_t1037045868_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3038559641(L_1, /*hidden argument*/Dictionary_2__ctor_m3038559641_MethodInfo_var);
		__this->set_customMetrics_1(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_campaignName_2(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_campaignSource_3(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_campaignMedium_4(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_campaignKeyword_5(L_5);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_campaignContent_6(L_6);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_campaignID_7(L_7);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_gclid_8(L_8);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_dclid_9(L_9);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T HitBuilder`1<System.Object>::SetCustomDimension(System.Int32,System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCustomDimension_m55290377_gshared (HitBuilder_1_t3202764555 * __this, int32_t ___dimensionNumber0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitBuilder_1_SetCustomDimension_m55290377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1037045868 * L_0 = (Dictionary_2_t1037045868 *)__this->get_customDimensions_0();
		int32_t L_1 = ___dimensionNumber0;
		String_t* L_2 = ___value1;
		NullCheck((Dictionary_2_t1037045868 *)L_0);
		Dictionary_2_Add_m2434562209((Dictionary_2_t1037045868 *)L_0, (int32_t)L_1, (String_t*)L_2, /*hidden argument*/Dictionary_2_Add_m2434562209_MethodInfo_var);
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_3;
	}
}
// System.Collections.Generic.Dictionary`2<System.Int32,System.String> HitBuilder`1<System.Object>::GetCustomDimensions()
extern "C"  Dictionary_2_t1037045868 * HitBuilder_1_GetCustomDimensions_m1343269273_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1037045868 * L_0 = (Dictionary_2_t1037045868 *)__this->get_customDimensions_0();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetCustomMetric(System.Int32,System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCustomMetric_m3149080737_gshared (HitBuilder_1_t3202764555 * __this, int32_t ___metricNumber0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitBuilder_1_SetCustomMetric_m3149080737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1037045868 * L_0 = (Dictionary_2_t1037045868 *)__this->get_customMetrics_1();
		int32_t L_1 = ___metricNumber0;
		String_t* L_2 = ___value1;
		NullCheck((Dictionary_2_t1037045868 *)L_0);
		Dictionary_2_Add_m2434562209((Dictionary_2_t1037045868 *)L_0, (int32_t)L_1, (String_t*)L_2, /*hidden argument*/Dictionary_2_Add_m2434562209_MethodInfo_var);
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_3;
	}
}
// System.Collections.Generic.Dictionary`2<System.Int32,System.String> HitBuilder`1<System.Object>::GetCustomMetrics()
extern "C"  Dictionary_2_t1037045868 * HitBuilder_1_GetCustomMetrics_m2837698825_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1037045868 * L_0 = (Dictionary_2_t1037045868 *)__this->get_customMetrics_1();
		return L_0;
	}
}
// System.String HitBuilder`1<System.Object>::GetCampaignName()
extern "C"  String_t* HitBuilder_1_GetCampaignName_m2634385986_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_campaignName_2();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetCampaignName(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCampaignName_m1882226512_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___campaignName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___campaignName0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___campaignName0;
		__this->set_campaignName_2(L_1);
	}

IL_000d:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.String HitBuilder`1<System.Object>::GetCampaignSource()
extern "C"  String_t* HitBuilder_1_GetCampaignSource_m3775180512_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_campaignSource_3();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetCampaignSource(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCampaignSource_m3534791218_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___campaignSource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitBuilder_1_SetCampaignSource_m3534791218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___campaignSource0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = ___campaignSource0;
		__this->set_campaignSource_3(L_1);
		goto IL_001c;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral828239950, /*hidden argument*/NULL);
	}

IL_001c:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.String HitBuilder`1<System.Object>::GetCampaignMedium()
extern "C"  String_t* HitBuilder_1_GetCampaignMedium_m1762759302_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_campaignMedium_4();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetCampaignMedium(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCampaignMedium_m3774949100_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___campaignMedium0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___campaignMedium0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___campaignMedium0;
		__this->set_campaignMedium_4(L_1);
	}

IL_000d:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.String HitBuilder`1<System.Object>::GetCampaignKeyword()
extern "C"  String_t* HitBuilder_1_GetCampaignKeyword_m3186318146_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_campaignKeyword_5();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetCampaignKeyword(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCampaignKeyword_m3898195884_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___campaignKeyword0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___campaignKeyword0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___campaignKeyword0;
		__this->set_campaignKeyword_5(L_1);
	}

IL_000d:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.String HitBuilder`1<System.Object>::GetCampaignContent()
extern "C"  String_t* HitBuilder_1_GetCampaignContent_m2375944604_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_campaignContent_6();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetCampaignContent(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCampaignContent_m2554828466_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___campaignContent0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___campaignContent0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___campaignContent0;
		__this->set_campaignContent_6(L_1);
	}

IL_000d:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.String HitBuilder`1<System.Object>::GetCampaignID()
extern "C"  String_t* HitBuilder_1_GetCampaignID_m1050518328_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_campaignID_7();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetCampaignID(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetCampaignID_m1835493834_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___campaignID0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___campaignID0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___campaignID0;
		__this->set_campaignID_7(L_1);
	}

IL_000d:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.String HitBuilder`1<System.Object>::GetGclid()
extern "C"  String_t* HitBuilder_1_GetGclid_m1677745492_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_gclid_8();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetGclid(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetGclid_m3762522462_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___gclid0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___gclid0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___gclid0;
		__this->set_gclid_8(L_1);
	}

IL_000d:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.String HitBuilder`1<System.Object>::GetDclid()
extern "C"  String_t* HitBuilder_1_GetDclid_m1677779313_gshared (HitBuilder_1_t3202764555 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_dclid_9();
		return L_0;
	}
}
// T HitBuilder`1<System.Object>::SetDclid(System.String)
extern "C"  Il2CppObject * HitBuilder_1_SetDclid_m92508285_gshared (HitBuilder_1_t3202764555 * __this, String_t* ___dclid0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___dclid0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___dclid0;
		__this->set_dclid_9(L_1);
	}

IL_000d:
	{
		NullCheck((HitBuilder_1_t3202764555 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* T HitBuilder`1<System.Object>::GetThis() */, (HitBuilder_1_t3202764555 *)__this);
		return L_2;
	}
}
// System.Void HypertextHelper.ObjectPool`1<System.Object>::.ctor(System.Action`1<T>,System.Action`1<T>)
extern "C"  void ObjectPool_1__ctor_m2047446034_gshared (ObjectPool_1_t2355888186 * __this, Action_1_t2491248677 * ___onGet0, Action_1_t2491248677 * ___onRelease1, const MethodInfo* method)
{
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__stack_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Action_1_t2491248677 * L_1 = ___onGet0;
		__this->set__onGet_1(L_1);
		Action_1_t2491248677 * L_2 = ___onRelease1;
		__this->set__onRelease_2(L_2);
		return;
	}
}
// System.Int32 HypertextHelper.ObjectPool`1<System.Object>::get_CountAll()
extern "C"  int32_t ObjectPool_1_get_CountAll_m1883136849_gshared (ObjectPool_1_t2355888186 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CCountAllU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void HypertextHelper.ObjectPool`1<System.Object>::set_CountAll(System.Int32)
extern "C"  void ObjectPool_1_set_CountAll_m598963688_gshared (ObjectPool_1_t2355888186 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CCountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 HypertextHelper.ObjectPool`1<System.Object>::get_CountActive()
extern "C"  int32_t ObjectPool_1_get_CountActive_m1326254420_gshared (ObjectPool_1_t2355888186 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObjectPool_1_t2355888186 *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t2355888186 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2355888186 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2355888186 *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t2355888186 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ObjectPool_1_t2355888186 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return ((int32_t)((int32_t)L_0-(int32_t)L_1));
	}
}
// System.Int32 HypertextHelper.ObjectPool`1<System.Object>::get_CountInactive()
extern "C"  int32_t ObjectPool_1_get_CountInactive_m3987331687_gshared (ObjectPool_1_t2355888186 * __this, const MethodInfo* method)
{
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)__this->get__stack_0();
		NullCheck((Stack_1_t3777177449 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3777177449 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_1;
	}
}
// T HypertextHelper.ObjectPool`1<System.Object>::Get()
extern "C"  Il2CppObject * ObjectPool_1_Get_m36874213_gshared (ObjectPool_1_t2355888186 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)__this->get__stack_0();
		NullCheck((Stack_1_t3777177449 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3777177449 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (Il2CppObject *)L_2;
		NullCheck((ObjectPool_1_t2355888186 *)__this);
		int32_t L_3 = ((  int32_t (*) (ObjectPool_1_t2355888186 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2355888186 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2355888186 *)__this);
		((  void (*) (ObjectPool_1_t2355888186 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((ObjectPool_1_t2355888186 *)__this, (int32_t)((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		goto IL_0035;
	}

IL_0029:
	{
		Stack_1_t3777177449 * L_4 = (Stack_1_t3777177449 *)__this->get__stack_0();
		NullCheck((Stack_1_t3777177449 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Stack_1_t3777177449 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (Il2CppObject *)L_5;
	}

IL_0035:
	{
		Action_1_t2491248677 * L_6 = (Action_1_t2491248677 *)__this->get__onGet_1();
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		Action_1_t2491248677 * L_7 = (Action_1_t2491248677 *)__this->get__onGet_1();
		Il2CppObject * L_8 = V_0;
		NullCheck((Action_1_t2491248677 *)L_7);
		((  void (*) (Action_1_t2491248677 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Action_1_t2491248677 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_004c:
	{
		Il2CppObject * L_9 = V_0;
		return L_9;
	}
}
// System.Void HypertextHelper.ObjectPool`1<System.Object>::Release(T)
extern "C"  void ObjectPool_1_Release_m1415969117_gshared (ObjectPool_1_t2355888186 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m1415969117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)__this->get__stack_0();
		NullCheck((Stack_1_t3777177449 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3777177449 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		Stack_1_t3777177449 * L_2 = (Stack_1_t3777177449 *)__this->get__stack_0();
		NullCheck((Stack_1_t3777177449 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Stack_1_t3777177449 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Il2CppObject * L_4 = ___element0;
		bool L_5 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral273729679, /*hidden argument*/NULL);
	}

IL_003b:
	{
		Action_1_t2491248677 * L_6 = (Action_1_t2491248677 *)__this->get__onRelease_2();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		Action_1_t2491248677 * L_7 = (Action_1_t2491248677 *)__this->get__onRelease_2();
		Il2CppObject * L_8 = ___element0;
		NullCheck((Action_1_t2491248677 *)L_7);
		((  void (*) (Action_1_t2491248677 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Action_1_t2491248677 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0052:
	{
		Stack_1_t3777177449 * L_9 = (Stack_1_t3777177449 *)__this->get__stack_0();
		Il2CppObject * L_10 = ___element0;
		NullCheck((Stack_1_t3777177449 *)L_9);
		((  void (*) (Stack_1_t3777177449 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Stack_1_t3777177449 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return;
	}
}
// System.Collections.Generic.List`1<T> ListPool`1<System.Object>::Get()
extern "C"  List_1_t2058570427 * ListPool_1_Get_m2902747731_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3678846538 * L_0 = ((ListPool_1_t2027455341_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3678846538 *)L_0);
		List_1_t2058570427 * L_1 = ((  List_1_t2058570427 * (*) (ObjectPool_1_t3678846538 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3678846538 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3890150051_gshared (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3678846538 * L_0 = ((ListPool_1_t2027455341_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t2058570427 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3678846538 *)L_0);
		((  void (*) (ObjectPool_1_t3678846538 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3678846538 *)L_0, (List_1_t2058570427 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m2594971495_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t3425156178 * L_1 = (UnityAction_1_t3425156178 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t3425156178 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3678846538 * L_2 = (ObjectPool_1_t3678846538 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3678846538 *, UnityAction_1_t3425156178 *, UnityAction_1_t3425156178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t3425156178 *)NULL, (UnityAction_1_t3425156178 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t2027455341_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void ListPool`1<System.Object>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m3962787535_gshared (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = ___l0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> ListPool`1<UnityEngine.UIVertex>::Get()
extern "C"  List_1_t573379950 * ListPool_1_Get_m3015839588_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2193656061 * L_0 = ((ListPool_1_t542264864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2193656061 *)L_0);
		List_1_t573379950 * L_1 = ((  List_1_t573379950 * (*) (ObjectPool_1_t2193656061 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t2193656061 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m4282624532_gshared (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2193656061 * L_0 = ((ListPool_1_t542264864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t573379950 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2193656061 *)L_0);
		((  void (*) (ObjectPool_1_t2193656061 *, List_1_t573379950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2193656061 *)L_0, (List_1_t573379950 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void ListPool_1__cctor_m2533549800_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1939965701 * L_1 = (UnityAction_1_t1939965701 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1939965701 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t2193656061 * L_2 = (ObjectPool_1_t2193656061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t2193656061 *, UnityAction_1_t1939965701 *, UnityAction_1_t1939965701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1939965701 *)NULL, (UnityAction_1_t1939965701 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t542264864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m2390027936_gshared (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___l0, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___l0;
		NullCheck((List_1_t573379950 *)L_0);
		((  void (*) (List_1_t573379950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t573379950 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void LitJson.ExporterFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m104989128_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m1089798616_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m1089798616((ExporterFunc_1_t3926562242 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<System.Object>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m729158479_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m2442068774_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ImporterFunc`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc_2__ctor_m2910045588_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::Invoke(TJson)
extern "C"  Il2CppObject * ImporterFunc_2_Invoke_m3373032134_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImporterFunc_2_Invoke_m3373032134((ImporterFunc_2_t2548729379 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ImporterFunc`2<System.Object,System.Object>::BeginInvoke(TJson,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImporterFunc_2_BeginInvoke_m2065437887_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___input0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ImporterFunc_2_EndInvoke_m2375822358_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey0`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey0_1__ctor_m2178726344_gshared (U3CRegisterExporterU3Ec__AnonStorey0_1_t3447138843 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey0`1<System.Object>::<>m__0(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey0_1_U3CU3Em__0_m1160532603_gshared (U3CRegisterExporterU3Ec__AnonStorey0_1_t3447138843 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t3926562242 * L_0 = (ExporterFunc_1_t3926562242 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t3926562242 *)L_0);
		((  void (*) (ExporterFunc_1_t3926562242 *, Il2CppObject *, JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t3926562242 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStorey1_2__ctor_m2787776139_gshared (U3CRegisterImporterU3Ec__AnonStorey1_2_t1381013311 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1`2<System.Object,System.Object>::<>m__0(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStorey1_2_U3CU3Em__0_m1308246711_gshared (U3CRegisterImporterU3Ec__AnonStorey1_2_t1381013311 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	{
		ImporterFunc_2_t2548729379 * L_0 = (ImporterFunc_2_t2548729379 *)__this->get_importer_0();
		Il2CppObject * L_1 = ___input0;
		NullCheck((ImporterFunc_2_t2548729379 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ImporterFunc_2_t2548729379 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ImporterFunc_2_t2548729379 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_2;
	}
}
// System.Void MotionManager/Serialization`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Serialization_1__ctor_m281877202_gshared (Serialization_1_t1266378047 * __this, List_1_t2058570427 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serialization_1__ctor_m281877202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MotionManager_t3855993783 * L_0 = SingletonMonoBehaviour_1_get_Instance_m4180938475(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m4180938475_MethodInfo_var);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_m_MobId_6();
		__this->set_mobilityId_0(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t2058570427 * L_2 = ___target0;
		__this->set_items_1(L_2);
		return;
	}
}
// System.Collections.Generic.List`1<T> MotionManager/Serialization`1<System.Object>::ToList()
extern "C"  List_1_t2058570427 * Serialization_1_ToList_m1151286062_gshared (Serialization_1_t1266378047 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_1();
		return L_0;
	}
}
// System.Void MotionManager/SerializationInWorldActionSetting`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void SerializationInWorldActionSetting_1__ctor_m1694703141_gshared (SerializationInWorldActionSetting_1_t927334316 * __this, List_1_t2058570427 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationInWorldActionSetting_1__ctor_m1694703141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MotionManager_t3855993783 * L_0 = SingletonMonoBehaviour_1_get_Instance_m4180938475(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m4180938475_MethodInfo_var);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)L_0->get_m_MobId_6();
		__this->set_mobilityId_0(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t2058570427 * L_2 = ___target0;
		__this->set_items_1(L_2);
		return;
	}
}
// System.Collections.Generic.List`1<T> MotionManager/SerializationInWorldActionSetting`1<System.Object>::ToList()
extern "C"  List_1_t2058570427 * SerializationInWorldActionSetting_1_ToList_m3148498571_gshared (SerializationInWorldActionSetting_1_t927334316 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_items_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
