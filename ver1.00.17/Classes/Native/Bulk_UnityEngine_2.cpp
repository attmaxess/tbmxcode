﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045.h"
#include "UnityEngine_UnityEngine_MeshCollider2718867283.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_MeshTopology3586470668.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Motion2415020824.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_NativeClassAttribute1576243993.h"
#include "mscorlib_System_Attribute542643598.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1520641178.h"
#include "UnityEngine_UnityEngine_AudioType4076847944.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2120336905.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection301283622.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3517219175.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2252784345.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Guid2533601593.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection536719976.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1660627182.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1899782350.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2291506050.h"
#include "System_Core_System_Func_2_gen2919696197.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2167079021.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen339633637.h"
#include "UnityEngine_UnityEngine_NetworkReachability1092747145.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "UnityEngine_UnityEngine_OperatingSystemFamily1896948788.h"
#include "UnityEngine_UnityEngine_ParticleEmitter4099167268.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule6751348.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionMod2748003162.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ShapeModule2888832346.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior3921148531.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2419492168.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CStopU3Ec1810852071.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MinMaxCurve122563058.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MinMaxGradi3708298175.h"
#include "UnityEngine_UnityEngine_ParticleSystemCurveMode1659312557.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_ParticleSystemGradientMode4205168402.h"
#include "UnityEngine_UnityEngine_Gradient3600583008.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_ParticleSystemShapeType2783609472.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151.h"
#include "UnityEngine_UnityEngine_PhysicMaterialCombine2188622696.h"
#include "UnityEngine_UnityEngine_Physics634932869.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_ContactFilter2D1672660996.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4166282325.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3229544204.h"
#include "mscorlib_System_Exception1927440687.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2472773525.h"
#include "UnityEngine_UnityEngine_PrimitiveType2454390065.h"
#include "UnityEngine_UnityEngine_Projector1121484678.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062.h"
#include "UnityEngine_UnityEngine_ShadowProjection2972094683.h"
#include "UnityEngine_UnityEngine_ShadowResolution1318053882.h"
#include "UnityEngine_UnityEngine_ColorSpace627621177.h"
#include "UnityEngine_UnityEngine_Random1170710517.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921.h"
#include "UnityEngine_UnityEngine_RangeInt2323401134.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge3306019089.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis3420330537.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180.h"
#include "UnityEngine_UnityEngine_RenderBuffer2767087968.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Rendering_ShadowCastingMod4042762198.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Rendering_LightProbeUsage664674855.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock3303648957.h"
#include "UnityEngine_UnityEngine_Rendering_AmbientMode3622873413.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask926634530.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction457874581.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonic364136731.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp2936374925.h"
#include "UnityEngine_UnityEngine_RenderingPath1538007819.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_RenderSettings1057812535.h"
#include "UnityEngine_UnityEngine_FogMode2386547659.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat3360518468.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite2842868372.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints251614631.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode841589752.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation993299413.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_ScaleMode324459649.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774.h"
#include "UnityEngine_UnityEngine_Screen786852042.h"
#include "UnityEngine_UnityEngine_Scripting_APIUpdating_Moved922195725.h"
#include "UnityEngine_UnityEngine_Scripting_GeneratedByOldBin107439586.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"

// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Motion
struct Motion_t2415020824;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t1576243993;
// System.Attribute
struct Attribute_t542643598;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1216180266;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t1520641178;
// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct MessageEventArgs_t301283622;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3517219175;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct PlayerEditorConnectionEvents_t2252784345;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2110227463;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct List_1_t1660627182;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct ConnectionChangeEvent_t536719976;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350;
// System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>
struct Func_2_t2919696197;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct IEnumerable_1_t2583633095;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct UnityEvent_1_t339633637;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t2727799310;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct MessageTypeSubscribers_t2291506050;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct MessageEvent_t2167079021;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// System.Type
struct Type_t;
// System.ArgumentException
struct ArgumentException_t3259014390;
// UnityEngine.ParticleEmitter
struct ParticleEmitter_t4099167268;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/IteratorDelegate
struct IteratorDelegate_t2419492168;
// UnityEngine.ParticleSystem/<Stop>c__AnonStorey1
struct U3CStopU3Ec__AnonStorey1_t1810852071;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t578636151;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_t4166282325;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t3229544204;
// System.Exception
struct Exception_t1927440687;
// UnityEngine.PreferBinarySerialization
struct PreferBinarySerialization_t2472773525;
// UnityEngine.Projector
struct Projector_t1121484678;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t2606999759;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.RangeAttribute
struct RangeAttribute_t3336560921;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// System.Delegate
struct Delegate_t3022476291;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t3033456180;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3303648957;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t3527622107;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.RequireComponent
struct RequireComponent_t864575032;
// UnityEngine.ResourceRequest
struct ResourceRequest_t2560315377;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t1903595547;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t3051495417;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t606618774;
// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t922195725;
// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct GeneratedByOldBindingsGeneratorAttribute_t107439586;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_t4182602970;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t1913052472;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3212052468;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviour_print_m3437620292_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Motion__ctor_m784531835_MetadataUsageId;
extern Il2CppClass* PlayerEditorConnectionEvents_t2252784345_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1598946593_MethodInfo_var;
extern const uint32_t PlayerConnection__ctor_m956924263_MetadataUsageId;
extern Il2CppClass* PlayerConnection_t3517219175_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_get_instance_m3885313185_MetadataUsageId;
extern const MethodInfo* ScriptableObject_CreateInstance_TisPlayerConnection_t3517219175_m2823640524_MethodInfo_var;
extern const uint32_t PlayerConnection_CreateInstance_m2276347858_MetadataUsageId;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t PlayerConnection_MessageCallbackInternal_m267902474_MetadataUsageId;
extern const MethodInfo* List_1_Add_m688682013_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_Invoke_m1903741765_MethodInfo_var;
extern const uint32_t PlayerConnection_ConnectedCallbackInternal_m2685675347_MetadataUsageId;
extern const uint32_t PlayerConnection_DisconnectedCallback_m4115432464_MetadataUsageId;
extern Il2CppClass* List_1_t1660627182_il2cpp_TypeInfo_var;
extern Il2CppClass* ConnectionChangeEvent_t536719976_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1357675898_MethodInfo_var;
extern const uint32_t PlayerEditorConnectionEvents__ctor_m603950945_MetadataUsageId;
extern Il2CppClass* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2919696197_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageEventArgs_t301283622_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2583633095_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4061997173_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m520813337_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2608774846_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisMessageTypeSubscribers_t2291506050_m887802803_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisMessageTypeSubscribers_t2291506050_m3682874628_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_Invoke_m354862256_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1281569593;
extern const uint32_t PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m3217020342_MetadataUsageId;
extern const uint32_t U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m520813337_MetadataUsageId;
extern const MethodInfo* UnityEvent_1__ctor_m2948712401_MethodInfo_var;
extern const uint32_t ConnectionChangeEvent__ctor_m616483304_MetadataUsageId;
extern const MethodInfo* UnityEvent_1__ctor_m3765336006_MethodInfo_var;
extern const uint32_t MessageEvent__ctor_m3253032545_MetadataUsageId;
extern Il2CppClass* MessageEvent_t2167079021_il2cpp_TypeInfo_var;
extern const uint32_t MessageTypeSubscribers__ctor_m743474932_MetadataUsageId;
extern const uint32_t Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId;
extern const uint32_t Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId;
extern const uint32_t Object_Destroy_m4145850038_MetadataUsageId;
extern const uint32_t Object_DestroyImmediate_m95027445_MetadataUsageId;
extern const uint32_t Object_DestroyObject_m2343493981_MetadataUsageId;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_GetInstanceID_m1920497914_MetadataUsageId;
extern const uint32_t Object_Equals_m4029628913_MetadataUsageId;
extern const uint32_t Object_op_Implicit_m2856731593_MetadataUsageId;
extern const uint32_t Object_CompareBaseObjects_m3953996214_MetadataUsageId;
extern const uint32_t Object_IsNativeObjectAlive_m4056217615_MetadataUsageId;
extern Il2CppClass* ScriptableObject_t1975622470_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern Il2CppCodeGenString* _stringLiteral1912870611;
extern const uint32_t Object_Instantiate_m938141395_MetadataUsageId;
extern const uint32_t Object_Instantiate_m2160322936_MetadataUsageId;
extern const uint32_t Object_Instantiate_m2439155489_MetadataUsageId;
extern const uint32_t Object_Instantiate_m2177117080_MetadataUsageId;
extern const uint32_t Object_Instantiate_m2489341053_MetadataUsageId;
extern const uint32_t Object_CheckNullArgument_m1711119106_MetadataUsageId;
extern const uint32_t Object_FindObjectOfType_m2330404063_MetadataUsageId;
extern const uint32_t Object_op_Equality_m3764089466_MetadataUsageId;
extern const uint32_t Object_op_Inequality_m2402264703_MetadataUsageId;
extern const uint32_t Object__cctor_m2991092887_MetadataUsageId;
extern Il2CppClass* ParticleSystem_t3394631041_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t2419492168_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CPlayU3Em__0_m4250446697_MethodInfo_var;
extern const uint32_t ParticleSystem_Play_m1705837075_MetadataUsageId;
extern Il2CppClass* U3CStopU3Ec__AnonStorey1_t1810852071_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m2516318650_MethodInfo_var;
extern const uint32_t ParticleSystem_Stop_m3810114649_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504_MethodInfo_var;
extern const uint32_t ParticleSystem_IterateParticleSystemsRecursive_m3260878897_MetadataUsageId;
struct AnimationCurve_t3306541151_marshaled_pinvoke;
struct AnimationCurve_t3306541151;;
struct AnimationCurve_t3306541151_marshaled_pinvoke;;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxCurve_t122563058_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct AnimationCurve_t3306541151_marshaled_com;
struct AnimationCurve_t3306541151_marshaled_com;;
struct Gradient_t3600583008_marshaled_pinvoke;
struct Gradient_t3600583008;;
struct Gradient_t3600583008_marshaled_pinvoke;;
extern Il2CppClass* Gradient_t3600583008_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxGradient_t3708298175_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct Gradient_t3600583008_marshaled_com;
struct Gradient_t3600583008_marshaled_com;;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m1220041042_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m122312471_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m3913913442_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m2560154475_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m2303387255_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m2368200185_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m564567838_MetadataUsageId;
extern const uint32_t Physics2D_Internal_Raycast_m2213595168_MetadataUsageId;
extern const uint32_t Physics2D_Internal_RaycastNonAlloc_m1874107548_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m253330691_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m2808325432_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m120415839_MetadataUsageId;
extern Il2CppClass* List_1_t4166282325_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2338710192_MethodInfo_var;
extern const uint32_t Physics2D__cctor_m3532647019_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m2870142810_MetadataUsageId;
extern Il2CppClass* PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2331435015;
extern const uint32_t PlayerPrefs_SetInt_m3351928596_MetadataUsageId;
extern const uint32_t PlayerPrefs_SetString_m2547809843_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t PlayerPrefs_GetString_m1903615000_MetadataUsageId;
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3730391696_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3587482509;
extern const uint32_t Quaternion_ToString_m2638853272_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1807026812;
extern const uint32_t Ray_ToString_m2019179238_MetadataUsageId;
extern const uint32_t RaycastHit_get_rigidbody_m480380820_MetadataUsageId;
extern const uint32_t RaycastHit_get_transform_m3290290036_MetadataUsageId;
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m3806390726_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1853817013;
extern const uint32_t Rect_ToString_m2728794442_MetadataUsageId;
extern Il2CppClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t RectOffset_t3387826427_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t RectOffset_t3387826427_com_FromNativeMethodDefinition_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3899275604;
extern const uint32_t RectOffset_ToString_m1281517011_MetadataUsageId;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1603911943_MetadataUsageId;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m4209881182_MetadataUsageId;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral895140023;
extern const uint32_t RectTransform_GetLocalCorners_m1836626405_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3518247078;
extern const uint32_t RectTransform_GetWorldCorners_m3873546362_MetadataUsageId;
extern const uint32_t RectTransform_GetParentSize_m1571597933_MetadataUsageId;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810_MetadataUsageId;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080_MetadataUsageId;
extern const uint32_t RectTransformUtility_ScreenPointToRay_m1842507230_MetadataUsageId;
extern const uint32_t RectTransformUtility_WorldToScreenPoint_m1650782138_MetadataUsageId;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m3920364518_MetadataUsageId;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m532748168_MetadataUsageId;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1244853728_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m560908615_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m93024038_MetadataUsageId;
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m1866023382_MetadataUsageId;
extern Il2CppClass* RemoteSettings_t392466225_il2cpp_TypeInfo_var;
extern const uint32_t RemoteSettings_CallOnUpdate_m1624968574_MetadataUsageId;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1739942068;
extern const uint32_t SphericalHarmonicsL2_get_Item_m1027885444_MetadataUsageId;
extern Il2CppClass* SphericalHarmonicsL2_t364136731_il2cpp_TypeInfo_var;
extern const uint32_t SphericalHarmonicsL2_Equals_m3577919107_MetadataUsageId;
extern const Il2CppType* Object_t1021602117_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2041782325_MetadataUsageId;
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m3588907349_MetadataUsageId;
extern Il2CppClass* SceneManager_t90660965_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m1528820797_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m4005732915_MetadataUsageId;
extern const MethodInfo* UnityAction_1_Invoke_m3061904506_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m4108957131_MetadataUsageId;
extern const MethodInfo* UnityAction_2_Invoke_m670567184_MethodInfo_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m1162592635_MetadataUsageId;
extern const uint32_t ScriptableObject__ctor_m2671490429_MetadataUsageId;

// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Object_t1021602117 * m_Items[1];

public:
	inline Object_t1021602117 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t1021602117 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t1021602117 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t1021602117 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t1021602117 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t1021602117 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RaycastHit_t87180320  m_Items[1];

public:
	inline RaycastHit_t87180320  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit_t87180320 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit_t87180320  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit_t87180320  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit_t87180320 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit_t87180320  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RaycastHit2D_t4063908774  m_Items[1];

public:
	inline RaycastHit2D_t4063908774  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit2D_t4063908774 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit2D_t4063908774  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit2D_t4063908774  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit2D_t4063908774 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit2D_t4063908774  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Material_t193706927 * m_Items[1];

public:
	inline Material_t193706927 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t193706927 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t193706927 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Material_t193706927 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t193706927 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t193706927 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(const AnimationCurve_t3306541151_marshaled_pinvoke& marshaled, AnimationCurve_t3306541151& unmarshaled);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(AnimationCurve_t3306541151_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_com& marshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com_back(const AnimationCurve_t3306541151_marshaled_com& marshaled, AnimationCurve_t3306541151& unmarshaled);
extern "C" void AnimationCurve_t3306541151_marshal_com_cleanup(AnimationCurve_t3306541151_marshaled_com& marshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_pinvoke& marshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke_back(const Gradient_t3600583008_marshaled_pinvoke& marshaled, Gradient_t3600583008& unmarshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke_cleanup(Gradient_t3600583008_marshaled_pinvoke& marshaled);
extern "C" void Gradient_t3600583008_marshal_com(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_com& marshaled);
extern "C" void Gradient_t3600583008_marshal_com_back(const Gradient_t3600583008_marshaled_com& marshaled, Gradient_t3600583008& unmarshaled);
extern "C" void Gradient_t3600583008_marshal_com_cleanup(Gradient_t3600583008_marshaled_com& marshaled);

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1598946593_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m926060499_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m688682013_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1903741765_gshared (UnityEvent_1_t2110227463 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1354888807_gshared (Func_2_t3961629604 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m1516493223_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m2208185096_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m838874366_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2948712401_gshared (UnityEvent_1_t2110227463 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2073978020_gshared (UnityEvent_1_t2727799310 * __this, const MethodInfo* method);
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1528820797_gshared (UnityAction_2_t1903595547 * __this, Scene_t1684909666  p0, int32_t p1, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3061904506_gshared (UnityAction_1_t3051495417 * __this, Scene_t1684909666  p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m670567184_gshared (UnityAction_2_t606618774 * __this, Scene_t1684909666  p0, Scene_t1684909666  p1, const MethodInfo* method);

// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2699265412 (Behaviour_t955675639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m3154116776 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3504849565 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m296997955 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1923670638 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m197157284 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1730479323 (Attribute_t542643598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m2790580781 (NativeClassAttribute_t1576243993 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.DownloadHandler::InternalDestroy()
extern "C"  void DownloadHandler_InternalDestroy_m761762988 (DownloadHandler_t1216180266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m4087144328 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m953228702 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.DownloadHandler::.ctor()
extern "C"  void DownloadHandler__ctor_m328121149 (DownloadHandler_t1216180266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.DownloadHandlerAudioClip::InternalCreateAudioClip(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandlerAudioClip_InternalCreateAudioClip_m751632673 (DownloadHandlerAudioClip_t1520641178 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m603950945 (PlayerEditorConnectionEvents_t2252784345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m1598946593(__this, method) ((  void (*) (List_1_t1440998580 *, const MethodInfo*))List_1__ctor_m1598946593_gshared)(__this, method)
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m2671490429 (ScriptableObject_t1975622470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t3517219175 * PlayerConnection_CreateInstance_m2276347858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.ScriptableObject::CreateInstance<UnityEngine.Networking.PlayerConnection.PlayerConnection>()
#define ScriptableObject_CreateInstance_TisPlayerConnection_t3517219175_m2823640524(__this /* static, unused */, method) ((  PlayerConnection_t3517219175 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m926060499_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2204253440 (Object_t1021602117 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern "C"  void Marshal_Copy_m1683535972 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, ByteU5BU5D_t3397334013* p1, int32_t p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t3517219175 * PlayerConnection_get_instance_m3885313185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Guid::.ctor(System.String)
extern "C"  void Guid__ctor_m2599802704 (Guid_t * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m3217020342 (PlayerEditorConnectionEvents_t2252784345 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3397334013* ___data1, int32_t ___playerId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m688682013(__this, p0, method) ((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_Add_m688682013_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
#define UnityEvent_1_Invoke_m1903741765(__this, p0, method) ((  void (*) (UnityEvent_1_t2110227463 *, int32_t, const MethodInfo*))UnityEvent_1_Invoke_m1903741765_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::.ctor()
#define List_1__ctor_m1357675898(__this, method) ((  void (*) (List_1_t1660627182 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m616483304 (ConnectionChangeEvent_t536719976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m4027042344 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2608774846(__this, p0, p1, method) ((  void (*) (Func_2_t2919696197 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1354888807_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisMessageTypeSubscribers_t2291506050_m887802803(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2919696197 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m1516493223_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisMessageTypeSubscribers_t2291506050_m3682874628(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m2208185096_gshared)(__this /* static, unused */, p0, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m2154443826 (MessageEventArgs_t301283622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::Invoke(T0)
#define UnityEvent_1_Invoke_m354862256(__this, p0, method) ((  void (*) (UnityEvent_1_t339633637 *, MessageEventArgs_t301283622 *, const MethodInfo*))UnityEvent_1_Invoke_m838874366_gshared)(__this, p0, method)
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m1191914268 (MessageTypeSubscribers_t2291506050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Guid::op_Equality(System.Guid,System.Guid)
extern "C"  bool Guid_op_Equality_m789465560 (Il2CppObject * __this /* static, unused */, Guid_t  p0, Guid_t  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
#define UnityEvent_1__ctor_m2948712401(__this, method) ((  void (*) (UnityEvent_1_t2110227463 *, const MethodInfo*))UnityEvent_1__ctor_m2948712401_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>::.ctor()
#define UnityEvent_1__ctor_m3765336006(__this, method) ((  void (*) (UnityEvent_1_t339633637 *, const MethodInfo*))UnityEvent_1__ctor_m2073978020_gshared)(__this, method)
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m3253032545 (MessageEvent_t2167079021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580 * ___pos1, Quaternion_t4030073918 * ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580 * ___pos2, Quaternion_t4030073918 * ___rot3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m4279412553 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3563317232 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m282495858 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m1573482188 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IntPtr::ToInt64()
extern "C"  int64_t IntPtr_ToInt64_m39971741 (IntPtr_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int64)
extern "C"  void IntPtr__ctor_m3803259710 (IntPtr_t* __this, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m1073656736 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m1715190285 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3953996214 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___lhs0, Object_t1021602117 * ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m4056217615 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m3900584722 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m943750213 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m3044532593 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m1711119106 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m3739475201 (ArgumentException_t3259014390 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingle_m2776302597 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingleWithParent_m509082884 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580  ___pos2, Quaternion_t4030073918  ___rot3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingle_m260620116 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1021602117 * Object_Instantiate_m2489341053 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, bool ___instantiateInWorldSpace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingleWithParent_m665572246 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfType_m2121813744 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_set_worldVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_set_worldVelocity_m1083072100 (ParticleEmitter_t4099167268 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m4099059742 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1076689768 (EmissionModule_t2748003162 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ShapeModule__ctor_m2156531686 (ShapeModule_t2888832346 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m1705837075 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/IteratorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void IteratorDelegate__ctor_m3692393942 (IteratorDelegate_t2419492168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystems(System.Boolean,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystems_m1240416587 (ParticleSystem_t3394631041 * __this, bool ___recurse0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m3810114649 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::.ctor()
extern "C"  void U3CStopU3Ec__AnonStorey1__ctor_m1832369351 (U3CStopU3Ec__AnonStorey1_t1810852071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::Invoke(UnityEngine.ParticleSystem)
extern "C"  bool IteratorDelegate_Invoke_m3389138368 (IteratorDelegate_t2419492168 * __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystemsRecursive(UnityEngine.Transform,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystemsRecursive_m3260878897 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m881385315 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504(__this, method) ((  ParticleSystem_t3394631041 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Play_m2372735108 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)
extern "C"  bool ParticleSystem_Internal_Stop_m3380666254 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, int32_t ___stopBehavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_SetRateOverTime_m1613592011 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void EmissionModule_set_rateOverTime_m2031054960 (EmissionModule_t2748003162 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetStartLifetime_m2660765731 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_startLifetime_m682695196 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetStartSpeed_m1948181975 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeed(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_startSpeed_m2177648984 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetStartSizeX_m3050019665 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSize(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_startSize_m2538003034 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_SetStartColor_m2686940116 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern "C"  void MainModule_set_startColor_m956995175 (MainModule_t6751348 * __this, MinMaxGradient_t3708298175  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m3707994114 (AnimationCurve_t3306541151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m1329384447 (MinMaxCurve_t122563058 * __this, float ___constant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m954570311 (Gradient_t3600583008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m2650940523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern "C"  void MinMaxGradient__ctor_m3588183433 (MinMaxGradient_t3708298175 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetShapeType(UnityEngine.ParticleSystem,System.Int32)
extern "C"  void ShapeModule_SetShapeType_m1168176288 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_shapeType(UnityEngine.ParticleSystemShapeType)
extern "C"  void ShapeModule_set_shapeType_m564654877 (ShapeModule_t2888832346 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetRadius(UnityEngine.ParticleSystem,System.Single)
extern "C"  void ShapeModule_SetRadius_m2622004587 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_radius(System.Single)
extern "C"  void ShapeModule_set_radius_m197221568 (ShapeModule_t2888832346 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetAngle(UnityEngine.ParticleSystem,System.Single)
extern "C"  void ShapeModule_SetAngle_m1851062332 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_angle(System.Single)
extern "C"  void ShapeModule_set_angle_m1034890115 (ShapeModule_t2888832346 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetLength(UnityEngine.ParticleSystem,System.Single)
extern "C"  void ShapeModule_SetLength_m4061315987 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_length(System.Single)
extern "C"  void ShapeModule_set_length_m2894234238 (ShapeModule_t2888832346 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_get_gravity_m3802008392 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_set_gravity_m2999035988 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3475924638 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m3442924926 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m2036777053 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1160243045 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1844392139 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t2243707580  Ray_get_origin_m3339262500 (Ray_t2469606224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533 (Ray_t2469606224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m233619224 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m410413656 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3908263591 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_INTERNAL_CALL_RaycastAll_m2126789092 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m93849932 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m2303387255 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern "C"  ContactFilter2D_t1672660996  ContactFilter2D_CreateLegacyFilter_m1912787689 (Il2CppObject * __this /* static, unused */, int32_t ___layerMask0, float ___minDepth1, float ___maxDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Raycast_m2213595168 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, ContactFilter2D_t1672660996  ___contactFilter3, RaycastHit2D_t4063908774 * ___raycastHit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_Raycast_m564567838 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, ContactFilter2D_t1672660996  ___contactFilter2, RaycastHit2DU5BU5D_t4176517891* ___results3, float ___distance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::Internal_RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_Internal_RaycastNonAlloc_m1874107548 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, ContactFilter2D_t1672660996  ___contactFilter3, RaycastHit2DU5BU5D_t4176517891* ___results4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m489831109 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___origin0, Vector2_t2243707579 * ___direction1, float ___distance2, ContactFilter2D_t1672660996 * ___contactFilter3, RaycastHit2D_t4063908774 * ___raycastHit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m2253171281 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___origin0, Vector2_t2243707579 * ___direction1, float ___distance2, ContactFilter2D_t1672660996 * ___contactFilter3, RaycastHit2DU5BU5D_t4176517891* ___results4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998 (Il2CppObject * __this /* static, unused */, Ray_t2469606224 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
#define List_1__ctor_m2338710192(__this, method) ((  void (*) (List_1_t4166282325 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Normalize_m2140428981 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m3161182818 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___lhs0, Vector3_t2243707580  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m3187718367 (Plane_t3727654732 * __this, Vector3_t2243707580  ___inNormal0, Vector3_t2243707580  ___inPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t2243707580  Plane_get_normal_m1872443823 (Plane_t3727654732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m1834776091 (Plane_t3727654732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m1064446634 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m2870142810 (Plane_t3727654732 * __this, Ray_t2469606224  ___ray0, float* ___enter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C"  bool PlayerPrefs_TrySetInt_m965228897 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C"  void PlayerPrefsException__ctor_m1748847897 (PlayerPrefsException_t3229544204 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m136681260 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C"  bool PlayerPrefs_TrySetSetString_m3460332240 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C"  String_t* PlayerPrefs_GetString_m1246323344 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m485833136 (Exception_t1927440687 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3196903881 (Quaternion_t4030073918 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m3310327005 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t2243707580 * ___axis1, Quaternion_t4030073918 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m2839236110 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___fromDirection0, Vector3_t2243707580 * ___toDirection1, Quaternion_t4030073918 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m3606560944 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___forward0, Vector3_t2243707580 * ___upwards1, Quaternion_t4030073918 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2243707580  Vector3_get_up_m2725403797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m1920404743 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m1043108654 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Quaternion_t4030073918 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t2243707580  Quaternion_Internal_ToEulerRad_m2807508879 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, float ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_Internal_MakePositive_m2921671247 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991 (Quaternion_t4030073918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Internal_FromEulerRad_m1121344272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Quaternion_set_eulerAngles_m3695252804 (Quaternion_t4030073918 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Vector3_t2243707580 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___euler0, Quaternion_t4030073918 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m952616600 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m2308156925 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3102305584 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m2270520528 (Quaternion_t4030073918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m3359827399 (float* __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m3730391696 (Quaternion_t4030073918 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m2949645127 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m2638853272 (Quaternion_t4030073918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m374035151 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_insideUnitSphere_m3752571035 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3663555848 (PropertyAttribute_t2606999759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m913869897 (RangeInt_t2323401134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2243707580  Vector3_get_normalized_m936072361 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m3379034047 (Ray_t2469606224 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366 (Ray_t2469606224 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m2019179238 (Ray_t2469606224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t2243707580  RaycastHit_get_point_m326143462 (RaycastHit_t87180320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m817665579 (RaycastHit_t87180320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_normal_m1788712468 (RaycastHit_t87180320 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m1178709367 (RaycastHit_t87180320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t3497673348 * RaycastHit_get_collider_m301198172 (RaycastHit_t87180320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t4233889191 * Collider_get_attachedRigidbody_m3279305420 (Collider_t3497673348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t4233889191 * RaycastHit_get_rigidbody_m480380820 (RaycastHit_t87180320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m3290290036 (RaycastHit_t87180320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_point_m442317739 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_normal_m3768105386 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m1296150410 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m3896897500 (Rect_t3681755626 * __this, Rect_t3681755626  ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m1393582490 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m3783700513 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1393582395 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m4294916608 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2243707579  Rect_get_position_m24550734 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2243707579  Rect_get_center_m3049923624 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m1161102488 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1161103577 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2243707579  Rect_get_min_m2549872833 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m4214255623 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m734445288 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_min(UnityEngine.Vector2)
extern "C"  void Rect_set_min_m2178285060 (Rect_t3681755626 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2915145014 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2915146103 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2243707579  Rect_get_max_m96665935 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m3501625033 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m21814698 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_max(UnityEngine.Vector2)
extern "C"  void Rect_set_max_m2794417386 (Rect_t3681755626 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m1138015702 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m1921257731 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m3128694305 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m2019122814 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2243707579  Rect_get_size_m3833121112 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m1334685290 (Rect_t3681755626 * __this, Vector2_t2243707579  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1334685291 (Rect_t3681755626 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3,System.Boolean)
extern "C"  bool Rect_Contains_m4048719162 (Rect_t3681755626 * __this, Vector3_t2243707580  ___point0, bool ___allowInverse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m210444568 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t3681755626  Rect_OrderMinMax_m1783437776 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m4145874649 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, bool ___allowInverse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m2793663577 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___lhs0, Rect_t3681755626  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m559954498 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m3806390726 (Rect_t3681755626 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m2728794442 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m4361650 (RectOffset_t3387826427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m620681523 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m1671272302 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3579196427 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m4065521443 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Add_m3705097782 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___self0, Rect_t3681755626 * ___rect1, Rect_t3681755626 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Remove_m1194095901 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___self0, Rect_t3681755626 * ___rect1, Rect_t3681755626 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m3198970074 (RectOffset_t3387826427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m439065308 (RectOffset_t3387826427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m281378687 (RectOffset_t3387826427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m3629049358 (RectOffset_t3387826427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m4112328858 (RectOffset_t3387826427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1177342209 (RectTransform_t3349966182 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m3180545469 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m885423409 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m834202955 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m1551648727 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m3564306187 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m693024247 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m3975625099 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m1402803191 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m3003734630 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m2764958706 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m1090213637 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t3681755626  RectTransform_get_rect_m73954734 (RectTransform_t3349966182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m1836626405 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* ___fourCornersArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_TransformPoint_m3272254198 (Transform_t3275118058 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2243707579  RectTransform_get_anchoredPosition_m3570822376 (RectTransform_t3349966182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2243707579  RectTransform_get_sizeDelta_m2157326342 (RectTransform_t3349966182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2243707579  RectTransform_get_pivot_m759087479 (RectTransform_t3349966182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_Scale_m3228063809 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Subtraction_m1984215297 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m2319668137 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2243707579  Vector2_get_one_m3174311904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Addition_m1389598521 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2077229449 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMin_m1497323108 (RectTransform_t3349966182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m3881967114 (Vector2_t2243707579 * __this, int32_t ___index0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4247668187 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMax_m3816015142 (RectTransform_t3349966182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2955899993 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m2792130561 (Vector2_t2243707579 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t2243707579  RectTransform_GetParentSize_m1571597933 (RectTransform_t3349966182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m147407266 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___exists0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m3966848876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t2469606224  RectTransformUtility_ScreenPointToRay_m1842507230 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, Vector2_t2243707579  ___screenPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t2243707580  Vector3_get_back_m4246539215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_op_Multiply_m1483423721 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, Vector3_t2243707580  ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, Vector3_t2243707580 * ___worldPoint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformPoint_m2648491174 (Transform_t3275118058 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m1064335535 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t2469606224  Camera_ScreenPointToRay_m614889538 (Camera_t189460977 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1201659139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_WorldToScreenPoint_m638747266 (Camera_t189460977 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3920364518 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m1360548980 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m532748168 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  RectTransformUtility_GetTransposed_m1770338235 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579 * ___screenPoint1, Camera_t189460977 * ___cam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, Vector2_t2243707579 * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, Canvas_t209405766 * ___canvas1, Rect_t3681755626 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m159598802 (UpdatedEventHandler_t3033456180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Renderer_INTERNAL_get_bounds_m1299274192 (Renderer_t257310565 * __this, Bounds_t3033363703 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m1847153122 (IndexOutOfRangeException_t3527622107 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::get_Item(System.Int32,System.Int32)
extern "C"  float SphericalHarmonicsL2_get_Item_m1027885444 (SphericalHarmonicsL2_t364136731 * __this, int32_t ___rgb0, int32_t ___coefficient1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m2919647449 (SphericalHarmonicsL2_t364136731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  bool SphericalHarmonicsL2_op_Equality_m3825067444 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t364136731  ___lhs0, SphericalHarmonicsL2_t364136731  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern "C"  bool SphericalHarmonicsL2_Equals_m3577919107 (SphericalHarmonicsL2_t364136731 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_fogColor_m1991092082 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2591134422 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientSkyColor_m960408383 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientSkyColor_m4004022691 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientEquatorColor_m3815109999 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientEquatorColor_m2094865419 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientGroundColor_m1874210147 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientGroundColor_m3148534439 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientLight_m481583081 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void RenderSettings_INTERNAL_get_ambientProbe_m1343118615 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t364136731 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m4198984292 (Texture_t2243626319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m1679754911 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___rt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m3365909665 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m2928390123 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m580875098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m286621198 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, bool ___sRGB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m1927959284 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m2317917654 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m989760279 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m2780941261 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m2900429544 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetColorBuffer_m2909232564 (RenderTexture_t2666733923 * __this, RenderBuffer_t2767087968 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetDepthBuffer_m1144066090 (RenderTexture_t2666733923 * __this, RenderBuffer_t2767087968 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m2914860946 (AsyncOperation_t3814632279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1021602117 * Resources_Load_m243305716 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m205306948 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m1938461825 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m3384354677 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3164777073 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m929838442 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___torque1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, Vector3_t2243707580 * ___position2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___worldPoint1, Vector3_t2243707580 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_centerOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_centerOfMass_m4044734456 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_position_m2227872991 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_get_rotation_m109669412 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m2709492198 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Quaternion_t4030073918 * ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m1555912301 (Scene_t1684909666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
extern "C"  int32_t Scene_GetBuildIndexInternal_m287561822 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3735680091 (Scene_t1684909666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m3223653899 (Scene_t1684909666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m3588907349 (Scene_t1684909666 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318 (Il2CppObject * __this /* static, unused */, Scene_t1684909666 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsync_m2648120039 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m1528820797(__this, p0, p1, method) ((  void (*) (UnityAction_2_t1903595547 *, Scene_t1684909666 , int32_t, const MethodInfo*))UnityAction_2_Invoke_m1528820797_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
#define UnityAction_1_Invoke_m3061904506(__this, p0, method) ((  void (*) (UnityAction_1_t3051495417 *, Scene_t1684909666 , const MethodInfo*))UnityAction_1_Invoke_m3061904506_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m670567184(__this, p0, p1, method) ((  void (*) (UnityAction_2_t606618774 *, Scene_t1684909666 , Scene_t1684909666 , const MethodInfo*))UnityAction_2_Invoke_m670567184_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m1778903390 (Il2CppObject * __this /* static, unused */, ScriptableObject_t1975622470 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstanceFromType_m4271875689 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String,System.Boolean)
extern "C"  void MovedFromAttribute__ctor_m1535041828 (MovedFromAttribute_t922195725 * __this, String_t* ___sourceNamespace0, bool ___isInDifferentAssembly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_Namespace(System.String)
extern "C"  void MovedFromAttribute_set_Namespace_m1786632593 (MovedFromAttribute_t922195725 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_IsInDifferentAssembly(System.Boolean)
extern "C"  void MovedFromAttribute_set_IsInDifferentAssembly_m4026643849 (MovedFromAttribute_t922195725 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.MeshCollider::get_convex()
extern "C"  bool MeshCollider_get_convex_m2666654546 (MeshCollider_t2718867283 * __this, const MethodInfo* method)
{
	typedef bool (*MeshCollider_get_convex_m2666654546_ftn) (MeshCollider_t2718867283 *);
	static MeshCollider_get_convex_m2666654546_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_get_convex_m2666654546_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::get_convex()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
extern "C"  void MeshCollider_set_convex_m2548365563 (MeshCollider_t2718867283 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MeshCollider_set_convex_m2548365563_ftn) (MeshCollider_t2718867283 *, bool);
	static MeshCollider_set_convex_m2548365563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_set_convex_m2548365563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::set_convex(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_mesh_m977520135 (MeshFilter_t3026937449 * __this, const MethodInfo* method)
{
	typedef Mesh_t1356156583 * (*MeshFilter_get_mesh_m977520135_ftn) (MeshFilter_t3026937449 *);
	static MeshFilter_get_mesh_m977520135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_mesh_m977520135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_mesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_mesh_m3839924176 (MeshFilter_t3026937449 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_mesh_m3839924176_ftn) (MeshFilter_t3026937449 *, Mesh_t1356156583 *);
	static MeshFilter_set_mesh_m3839924176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_mesh_m3839924176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_sharedMesh_m1310789932 (MeshFilter_t3026937449 * __this, const MethodInfo* method)
{
	typedef Mesh_t1356156583 * (*MeshFilter_get_sharedMesh_m1310789932_ftn) (MeshFilter_t3026937449 *);
	static MeshFilter_get_sharedMesh_m1310789932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_sharedMesh_m1310789932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_sharedMesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_sharedMesh_m2225370173 (MeshFilter_t3026937449 * __this, Mesh_t1356156583 * ___value0, const MethodInfo* method)
{
	typedef void (*MeshFilter_set_sharedMesh_m2225370173_ftn) (MeshFilter_t3026937449 *, Mesh_t1356156583 *);
	static MeshFilter_set_sharedMesh_m2225370173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_set_sharedMesh_m2225370173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m2699265412(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m3154116776 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3504849565 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_IsInvokingAll()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m666563676 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m666563676_ftn) (MonoBehaviour_t1158329972 *, String_t*, float);
	static MonoBehaviour_Invoke_m666563676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m666563676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m3468262484 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m3468262484_ftn) (MonoBehaviour_t1158329972 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m3468262484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m3468262484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1, ___repeatRate2);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C"  void MonoBehaviour_CancelInvoke_m744713777 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m3154116776(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C"  void MonoBehaviour_CancelInvoke_m2508161963 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m2508161963_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_CancelInvoke_m2508161963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m2508161963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern "C"  bool MonoBehaviour_IsInvoking_m1469271462 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_IsInvoking_m1469271462_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_IsInvoking_m1469271462_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_IsInvoking_m1469271462_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::IsInvoking(System.String)");
	return _il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern "C"  bool MonoBehaviour_IsInvoking_m345622956 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = MonoBehaviour_Internal_IsInvokingAll_m3504849565(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	Coroutine_t2299508840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t2299508840 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_m1744905232 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	Coroutine_t2299508840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Coroutine_t2299508840 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_Internal_m1014513456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine0);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m296997955 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_m296997955_ftn) (MonoBehaviour_t1158329972 *, String_t*, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_m296997955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m296997955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName0, ___value1);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m1399371129 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Coroutine_t2299508840 * V_1 = NULL;
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_0;
		Coroutine_t2299508840 * L_2 = MonoBehaviour_StartCoroutine_m296997955(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Coroutine_t2299508840 * L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m987450539 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m987450539_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_StopCoroutine_m987450539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m987450539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1170478282 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1668572632 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	{
		Coroutine_t2299508840 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m1923670638(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1923670638 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn) (MonoBehaviour_t1158329972 *, Coroutine_t2299508840 *);
	static MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1675795839 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopAllCoroutines_m1675795839_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_StopAllCoroutines_m1675795839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopAllCoroutines_m1675795839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopAllCoroutines()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m3437620292 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviour_print_m3437620292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern "C"  bool MonoBehaviour_get_useGUILayout_m524237270 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_get_useGUILayout_m524237270_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_get_useGUILayout_m524237270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_get_useGUILayout_m524237270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::get_useGUILayout()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m2666356651 (MonoBehaviour_t1158329972 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_set_useGUILayout_m2666356651_ftn) (MonoBehaviour_t1158329972 *, bool);
	static MonoBehaviour_set_useGUILayout_m2666356651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_set_useGUILayout_m2666356651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Motion::.ctor()
extern "C"  void Motion__ctor_m784531835 (Motion_t2415020824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Motion__ctor_m784531835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NativeClassAttribute::.ctor(System.String)
extern "C"  void NativeClassAttribute__ctor_m3215217838 (NativeClassAttribute_t1576243993 * __this, String_t* ___qualifiedCppName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___qualifiedCppName0;
		NativeClassAttribute_set_QualifiedNativeName_m2790580781(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NativeClassAttribute::set_QualifiedNativeName(System.String)
extern "C"  void NativeClassAttribute_set_QualifiedNativeName_m2790580781 (NativeClassAttribute_t1576243993 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CQualifiedNativeNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke(const DownloadHandler_t1216180266& unmarshaled, DownloadHandler_t1216180266_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_back(const DownloadHandler_t1216180266_marshaled_pinvoke& marshaled, DownloadHandler_t1216180266& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_cleanup(DownloadHandler_t1216180266_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_com(const DownloadHandler_t1216180266& unmarshaled, DownloadHandler_t1216180266_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandler_t1216180266_marshal_com_back(const DownloadHandler_t1216180266_marshaled_com& marshaled, DownloadHandler_t1216180266& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_com_cleanup(DownloadHandler_t1216180266_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandler::.ctor()
extern "C"  void DownloadHandler__ctor_m328121149 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalDestroy()
extern "C"  void DownloadHandler_InternalDestroy_m761762988 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalDestroy_m761762988_ftn) (DownloadHandler_t1216180266 *);
	static DownloadHandler_InternalDestroy_m761762988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalDestroy_m761762988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.DownloadHandler::Finalize()
extern "C"  void DownloadHandler_Finalize_m3781185347 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		DownloadHandler_InternalDestroy_m761762988(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::Dispose()
extern "C"  void DownloadHandler_Dispose_m2043077428 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	{
		DownloadHandler_InternalDestroy_m761762988(__this, /*hidden argument*/NULL);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.DownloadHandler::ReceiveData(System.Byte[],System.Int32)
extern "C"  bool DownloadHandler_ReceiveData_m2362198966 (DownloadHandler_t1216180266 * __this, ByteU5BU5D_t3397334013* ___data0, int32_t ___dataLength1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::ReceiveContentLength(System.Int32)
extern "C"  void DownloadHandler_ReceiveContentLength_m3588122286 (DownloadHandler_t1216180266 * __this, int32_t ___contentLength0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::CompleteContent()
extern "C"  void DownloadHandler_CompleteContent_m156041789 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke(const DownloadHandlerAudioClip_t1520641178& unmarshaled, DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_back(const DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled, DownloadHandlerAudioClip_t1520641178& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_cleanup(DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com(const DownloadHandlerAudioClip_t1520641178& unmarshaled, DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com_back(const DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled, DownloadHandlerAudioClip_t1520641178& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com_cleanup(DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerAudioClip::.ctor(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandlerAudioClip__ctor_m1779320660 (DownloadHandlerAudioClip_t1520641178 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m328121149(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		int32_t L_1 = ___audioType1;
		DownloadHandlerAudioClip_InternalCreateAudioClip_m751632673(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandlerAudioClip::InternalCreateAudioClip(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandlerAudioClip_InternalCreateAudioClip_m751632673 (DownloadHandlerAudioClip_t1520641178 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method)
{
	typedef void (*DownloadHandlerAudioClip_InternalCreateAudioClip_m751632673_ftn) (DownloadHandlerAudioClip_t1520641178 *, String_t*, int32_t);
	static DownloadHandlerAudioClip_InternalCreateAudioClip_m751632673_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandlerAudioClip_InternalCreateAudioClip_m751632673_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandlerAudioClip::InternalCreateAudioClip(System.String,UnityEngine.AudioType)");
	_il2cpp_icall_func(__this, ___url0, ___audioType1);
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerScript
extern "C" void DownloadHandlerScript_t2120336905_marshal_pinvoke(const DownloadHandlerScript_t2120336905& unmarshaled, DownloadHandlerScript_t2120336905_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerScript_t2120336905_marshal_pinvoke_back(const DownloadHandlerScript_t2120336905_marshaled_pinvoke& marshaled, DownloadHandlerScript_t2120336905& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerScript
extern "C" void DownloadHandlerScript_t2120336905_marshal_pinvoke_cleanup(DownloadHandlerScript_t2120336905_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerScript
extern "C" void DownloadHandlerScript_t2120336905_marshal_com(const DownloadHandlerScript_t2120336905& unmarshaled, DownloadHandlerScript_t2120336905_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerScript_t2120336905_marshal_com_back(const DownloadHandlerScript_t2120336905_marshaled_com& marshaled, DownloadHandlerScript_t2120336905& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerScript
extern "C" void DownloadHandlerScript_t2120336905_marshal_com_cleanup(DownloadHandlerScript_t2120336905_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern "C"  void MessageEventArgs__ctor_m2154443826 (MessageEventArgs_t301283622 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::.ctor()
extern "C"  void PlayerConnection__ctor_m956924263 (PlayerConnection_t3517219175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection__ctor_m956924263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerEditorConnectionEvents_t2252784345 * L_0 = (PlayerEditorConnectionEvents_t2252784345 *)il2cpp_codegen_object_new(PlayerEditorConnectionEvents_t2252784345_il2cpp_TypeInfo_var);
		PlayerEditorConnectionEvents__ctor_m603950945(L_0, /*hidden argument*/NULL);
		__this->set_m_PlayerEditorConnectionEvents_2(L_0);
		List_1_t1440998580 * L_1 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_1, /*hidden argument*/List_1__ctor_m1598946593_MethodInfo_var);
		__this->set_m_connectedPlayers_3(L_1);
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern "C"  PlayerConnection_t3517219175 * PlayerConnection_get_instance_m3885313185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_get_instance_m3885313185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t3517219175 * V_0 = NULL;
	{
		PlayerConnection_t3517219175 * L_0 = ((PlayerConnection_t3517219175_StaticFields*)PlayerConnection_t3517219175_il2cpp_TypeInfo_var->static_fields)->get_s_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		PlayerConnection_t3517219175 * L_2 = PlayerConnection_CreateInstance_m2276347858(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0028;
	}

IL_001d:
	{
		PlayerConnection_t3517219175 * L_3 = ((PlayerConnection_t3517219175_StaticFields*)PlayerConnection_t3517219175_il2cpp_TypeInfo_var->static_fields)->get_s_Instance_4();
		V_0 = L_3;
		goto IL_0028;
	}

IL_0028:
	{
		PlayerConnection_t3517219175 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern "C"  PlayerConnection_t3517219175 * PlayerConnection_CreateInstance_m2276347858 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_CreateInstance_m2276347858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayerConnection_t3517219175 * V_0 = NULL;
	{
		PlayerConnection_t3517219175 * L_0 = ScriptableObject_CreateInstance_TisPlayerConnection_t3517219175_m2823640524(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayerConnection_t3517219175_m2823640524_MethodInfo_var);
		((PlayerConnection_t3517219175_StaticFields*)PlayerConnection_t3517219175_il2cpp_TypeInfo_var->static_fields)->set_s_Instance_4(L_0);
		PlayerConnection_t3517219175 * L_1 = ((PlayerConnection_t3517219175_StaticFields*)PlayerConnection_t3517219175_il2cpp_TypeInfo_var->static_fields)->get_s_Instance_4();
		NullCheck(L_1);
		Object_set_hideFlags_m2204253440(L_1, ((int32_t)61), /*hidden argument*/NULL);
		PlayerConnection_t3517219175 * L_2 = ((PlayerConnection_t3517219175_StaticFields*)PlayerConnection_t3517219175_il2cpp_TypeInfo_var->static_fields)->get_s_Instance_4();
		V_0 = L_2;
		goto IL_0022;
	}

IL_0022:
	{
		PlayerConnection_t3517219175 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::MessageCallbackInternal(System.IntPtr,System.UInt64,System.UInt64,System.String)
extern "C"  void PlayerConnection_MessageCallbackInternal_m267902474 (Il2CppObject * __this /* static, unused */, IntPtr_t ___data0, uint64_t ___size1, uint64_t ___guid2, String_t* ___messageId3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_MessageCallbackInternal_m267902474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		V_0 = (ByteU5BU5D_t3397334013*)NULL;
		uint64_t L_0 = ___size1;
		if ((!(((uint64_t)L_0) > ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_001f;
		}
	}
	{
		uint64_t L_1 = ___size1;
		if ((uint64_t)(L_1) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_1))));
		IntPtr_t L_2 = ___data0;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		uint64_t L_4 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Marshal_Copy_m1683535972(NULL /*static, unused*/, L_2, L_3, 0, (((int32_t)((int32_t)L_4))), /*hidden argument*/NULL);
	}

IL_001f:
	{
		PlayerConnection_t3517219175 * L_5 = PlayerConnection_get_instance_m3885313185(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		PlayerEditorConnectionEvents_t2252784345 * L_6 = L_5->get_m_PlayerEditorConnectionEvents_2();
		String_t* L_7 = ___messageId3;
		Guid_t  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Guid__ctor_m2599802704(&L_8, L_7, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_9 = V_0;
		uint64_t L_10 = ___guid2;
		NullCheck(L_6);
		PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m3217020342(L_6, L_8, L_9, (((int32_t)((int32_t)L_10))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::ConnectedCallbackInternal(System.Int32)
extern "C"  void PlayerConnection_ConnectedCallbackInternal_m2685675347 (Il2CppObject * __this /* static, unused */, int32_t ___playerId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_ConnectedCallbackInternal_m2685675347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t3517219175 * L_0 = PlayerConnection_get_instance_m3885313185(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t1440998580 * L_1 = L_0->get_m_connectedPlayers_3();
		int32_t L_2 = ___playerId0;
		NullCheck(L_1);
		List_1_Add_m688682013(L_1, L_2, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
		PlayerConnection_t3517219175 * L_3 = PlayerConnection_get_instance_m3885313185(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		PlayerEditorConnectionEvents_t2252784345 * L_4 = L_3->get_m_PlayerEditorConnectionEvents_2();
		NullCheck(L_4);
		ConnectionChangeEvent_t536719976 * L_5 = L_4->get_connectionEvent_1();
		int32_t L_6 = ___playerId0;
		NullCheck(L_5);
		UnityEvent_1_Invoke_m1903741765(L_5, L_6, /*hidden argument*/UnityEvent_1_Invoke_m1903741765_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectedCallback(System.Int32)
extern "C"  void PlayerConnection_DisconnectedCallback_m4115432464 (Il2CppObject * __this /* static, unused */, int32_t ___playerId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerConnection_DisconnectedCallback_m4115432464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayerConnection_t3517219175 * L_0 = PlayerConnection_get_instance_m3885313185(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		PlayerEditorConnectionEvents_t2252784345 * L_1 = L_0->get_m_PlayerEditorConnectionEvents_2();
		NullCheck(L_1);
		ConnectionChangeEvent_t536719976 * L_2 = L_1->get_disconnectionEvent_2();
		int32_t L_3 = ___playerId0;
		NullCheck(L_2);
		UnityEvent_1_Invoke_m1903741765(L_2, L_3, /*hidden argument*/UnityEvent_1_Invoke_m1903741765_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern "C"  void PlayerEditorConnectionEvents__ctor_m603950945 (PlayerEditorConnectionEvents_t2252784345 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents__ctor_m603950945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1660627182 * L_0 = (List_1_t1660627182 *)il2cpp_codegen_object_new(List_1_t1660627182_il2cpp_TypeInfo_var);
		List_1__ctor_m1357675898(L_0, /*hidden argument*/List_1__ctor_m1357675898_MethodInfo_var);
		__this->set_messageTypeSubscribers_0(L_0);
		ConnectionChangeEvent_t536719976 * L_1 = (ConnectionChangeEvent_t536719976 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t536719976_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m616483304(L_1, /*hidden argument*/NULL);
		__this->set_connectionEvent_1(L_1);
		ConnectionChangeEvent_t536719976 * L_2 = (ConnectionChangeEvent_t536719976 *)il2cpp_codegen_object_new(ConnectionChangeEvent_t536719976_il2cpp_TypeInfo_var);
		ConnectionChangeEvent__ctor_m616483304(L_2, /*hidden argument*/NULL);
		__this->set_disconnectionEvent_2(L_2);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern "C"  void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m3217020342 (PlayerEditorConnectionEvents_t2252784345 * __this, Guid_t  ___messageId0, ByteU5BU5D_t3397334013* ___data1, int32_t ___playerId2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m3217020342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	MessageEventArgs_t301283622 * V_2 = NULL;
	MessageEventArgs_t301283622 * V_3 = NULL;
	MessageTypeSubscribers_t2291506050 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * L_0 = (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 *)il2cpp_codegen_object_new(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350_il2cpp_TypeInfo_var);
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m4027042344(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * L_1 = V_0;
		Guid_t  L_2 = ___messageId0;
		NullCheck(L_1);
		L_1->set_messageId_0(L_2);
		List_1_t1660627182 * L_3 = __this->get_messageTypeSubscribers_0();
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m520813337_MethodInfo_var);
		Func_2_t2919696197 * L_6 = (Func_2_t2919696197 *)il2cpp_codegen_object_new(Func_2_t2919696197_il2cpp_TypeInfo_var);
		Func_2__ctor_m2608774846(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m2608774846_MethodInfo_var);
		Il2CppObject* L_7 = Enumerable_Where_TisMessageTypeSubscribers_t2291506050_m887802803(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_Where_TisMessageTypeSubscribers_t2291506050_m887802803_MethodInfo_var);
		V_1 = L_7;
		Il2CppObject* L_8 = V_1;
		bool L_9 = Enumerable_Any_TisMessageTypeSubscribers_t2291506050_m3682874628(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_Any_TisMessageTypeSubscribers_t2291506050_m3682874628_MethodInfo_var);
		if (L_9)
		{
			goto IL_0051;
		}
	}
	{
		U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * L_10 = V_0;
		NullCheck(L_10);
		Guid_t  L_11 = L_10->get_messageId_0();
		Guid_t  L_12 = L_11;
		Il2CppObject * L_13 = Box(Guid_t_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1281569593, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		goto IL_00ad;
	}

IL_0051:
	{
		MessageEventArgs_t301283622 * L_15 = (MessageEventArgs_t301283622 *)il2cpp_codegen_object_new(MessageEventArgs_t301283622_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m2154443826(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		MessageEventArgs_t301283622 * L_16 = V_3;
		int32_t L_17 = ___playerId2;
		NullCheck(L_16);
		L_16->set_playerId_0(L_17);
		MessageEventArgs_t301283622 * L_18 = V_3;
		ByteU5BU5D_t3397334013* L_19 = ___data1;
		NullCheck(L_18);
		L_18->set_data_1(L_19);
		MessageEventArgs_t301283622 * L_20 = V_3;
		V_2 = L_20;
		Il2CppObject* L_21 = V_1;
		NullCheck(L_21);
		Il2CppObject* L_22 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::GetEnumerator() */, IEnumerable_1_t2583633095_il2cpp_TypeInfo_var, L_21);
		V_5 = L_22;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008d;
		}

IL_0075:
		{
			Il2CppObject* L_23 = V_5;
			NullCheck(L_23);
			MessageTypeSubscribers_t2291506050 * L_24 = InterfaceFuncInvoker0< MessageTypeSubscribers_t2291506050 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>::get_Current() */, IEnumerator_1_t4061997173_il2cpp_TypeInfo_var, L_23);
			V_4 = L_24;
			MessageTypeSubscribers_t2291506050 * L_25 = V_4;
			NullCheck(L_25);
			MessageEvent_t2167079021 * L_26 = L_25->get_messageCallback_2();
			MessageEventArgs_t301283622 * L_27 = V_2;
			NullCheck(L_26);
			UnityEvent_1_Invoke_m354862256(L_26, L_27, /*hidden argument*/UnityEvent_1_Invoke_m354862256_MethodInfo_var);
		}

IL_008d:
		{
			Il2CppObject* L_28 = V_5;
			NullCheck(L_28);
			bool L_29 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_28);
			if (L_29)
			{
				goto IL_0075;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xAD, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_30 = V_5;
			if (!L_30)
			{
				goto IL_00ac;
			}
		}

IL_00a5:
		{
			Il2CppObject* L_31 = V_5;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_31);
		}

IL_00ac:
		{
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ad:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::.ctor()
extern "C"  void U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0__ctor_m4027042344 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::<>m__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern "C"  bool U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m520813337 (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350 * __this, MessageTypeSubscribers_t2291506050 * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_U3CU3Em__0_m520813337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		MessageTypeSubscribers_t2291506050 * L_0 = ___x0;
		NullCheck(L_0);
		Guid_t  L_1 = MessageTypeSubscribers_get_MessageTypeId_m1191914268(L_0, /*hidden argument*/NULL);
		Guid_t  L_2 = __this->get_messageId_0();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		bool L_3 = Guid_op_Equality_m789465560(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern "C"  void ConnectionChangeEvent__ctor_m616483304 (ConnectionChangeEvent_t536719976 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConnectionChangeEvent__ctor_m616483304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m2948712401(__this, /*hidden argument*/UnityEvent_1__ctor_m2948712401_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern "C"  void MessageEvent__ctor_m3253032545 (MessageEvent_t2167079021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageEvent__ctor_m3253032545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m3765336006(__this, /*hidden argument*/UnityEvent_1__ctor_m3765336006_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern "C"  void MessageTypeSubscribers__ctor_m743474932 (MessageTypeSubscribers_t2291506050 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageTypeSubscribers__ctor_m743474932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_subscriberCount_1(0);
		MessageEvent_t2167079021 * L_0 = (MessageEvent_t2167079021 *)il2cpp_codegen_object_new(MessageEvent_t2167079021_il2cpp_TypeInfo_var);
		MessageEvent__ctor_m3253032545(L_0, /*hidden argument*/NULL);
		__this->set_messageCallback_2(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern "C"  Guid_t  MessageTypeSubscribers_get_MessageTypeId_m1191914268 (MessageTypeSubscribers_t2291506050 * __this, const MethodInfo* method)
{
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_messageTypeId_0();
		Guid_t  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Guid__ctor_m2599802704(&L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Guid_t  L_2 = V_0;
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_pinvoke_back(const Object_t1021602117_marshaled_pinvoke& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke_cleanup(Object_t1021602117_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_com_back(const Object_t1021602117_marshaled_com& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com_cleanup(Object_t1021602117_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m197157284 (Object_t1021602117 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingle_m260620116 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingle_m260620116_ftn) (Object_t1021602117 *);
	static Object_Internal_CloneSingle_m260620116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m260620116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingleWithParent_m665572246 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingleWithParent_m665572246_ftn) (Object_t1021602117 *, Transform_t3275118058 *, bool);
	static Object_Internal_CloneSingleWithParent_m665572246_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingleWithParent_m665572246_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)");
	return _il2cpp_icall_func(___data0, ___parent1, ___worldPositionStays2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingle_m2776302597 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Object_t1021602117 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580 * ___pos1, Quaternion_t4030073918 * ___rot2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn) (Object_t1021602117 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingleWithParent_m509082884 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580  ___pos2, Quaternion_t4030073918  ___rot3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___data0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849(NULL /*static, unused*/, L_0, L_1, (&___pos2), (&___rot3), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580 * ___pos2, Quaternion_t4030073918 * ___rot3, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn) (Object_t1021602117 *, Transform_t3275118058 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___parent1, ___pos2, ___rot3);
}
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn) ();
	static Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern "C"  void Object_EnsureRunningOnMainThread_m3042842193 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef void (*Object_EnsureRunningOnMainThread_m3042842193_ftn) (Object_t1021602117 *);
	static Object_EnsureRunningOnMainThread_m3042842193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_EnsureRunningOnMainThread_m3042842193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::EnsureRunningOnMainThread()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m4279412553 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m4279412553_ftn) (Object_t1021602117 *, float);
	static Object_Destroy_m4279412553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m4279412553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m4145850038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3563317232 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m3563317232_ftn) (Object_t1021602117 *, bool);
	static Object_DestroyImmediate_m3563317232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m3563317232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m95027445 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m95027445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t1021602117 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3563317232(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfType_m2121813744 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfType_m2121813744_ftn) (Type_t *);
	static Object_FindObjectsOfType_m2121813744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m2121813744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m2079638459_ftn) (Object_t1021602117 *);
	static Object_get_name_m2079638459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m2079638459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m4157836998 (Object_t1021602117 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m4157836998_ftn) (Object_t1021602117 *, String_t*);
	static Object_set_name_m4157836998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m4157836998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m2330762974_ftn) (Object_t1021602117 *);
	static Object_DontDestroyOnLoad_m2330762974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m2330762974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m4158950869 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m4158950869_ftn) (Object_t1021602117 *);
	static Object_get_hideFlags_m4158950869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m4158950869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2204253440 (Object_t1021602117 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m2204253440_ftn) (Object_t1021602117 *, int32_t);
	static Object_set_hideFlags_m2204253440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2204253440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m282495858 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m282495858_ftn) (Object_t1021602117 *, float);
	static Object_DestroyObject_m282495858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m282495858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern "C"  void Object_DestroyObject_m2343493981 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyObject_m2343493981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyObject_m282495858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindSceneObjectsOfType_m1833688338 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindSceneObjectsOfType_m1833688338_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m1833688338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m1833688338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfTypeIncludingAssets_m3988851426 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m1947404527 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m1947404527_ftn) (Object_t1021602117 *);
	static Object_ToString_m1947404527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m1947404527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m2570795274 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m2570795274_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m2570795274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m2570795274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m1920497914 (Object_t1021602117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_GetInstanceID_m1920497914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_4 = Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1(L_4);
	}

IL_0032:
	{
		IntPtr_t* L_5 = __this->get_address_of_m_CachedPtr_0();
		int64_t L_6 = IntPtr_ToInt64_m39971741(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		IntPtr_t L_8;
		memset(&L_8, 0, sizeof(L_8));
		IntPtr__ctor_m3803259710(&L_8, ((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)L_7))))), /*hidden argument*/NULL);
		void* L_9 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = (*((int32_t*)L_9));
		goto IL_0056;
	}

IL_0056:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m3431642059 (Object_t1021602117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m1715190285(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern "C"  bool Object_Equals_m4029628913 (Object_t1021602117 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m4029628913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = ___other0;
		V_0 = ((Object_t1021602117 *)IsInstClass(L_0, Object_t1021602117_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_4 = ___other0;
		if (((Object_t1021602117 *)IsInstClass(L_4, Object_t1021602117_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t1021602117 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m2856731593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3953996214 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___lhs0, Object_t1021602117 * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m3953996214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t1021602117 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t1021602117 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t1021602117 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t1021602117 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t1021602117 * L_10 = ___lhs0;
		Object_t1021602117 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m4056217615 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m4056217615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m943750213(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m943750213 (Object_t1021602117 * __this, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		IntPtr_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1021602117 * Object_Instantiate_m938141395 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m938141395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		if (!((ScriptableObject_t1975622470 *)IsInstClass(L_1, ScriptableObject_t1975622470_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, _stringLiteral1912870611, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Object_t1021602117 * L_3 = ___original0;
		Vector3_t2243707580  L_4 = ___position1;
		Quaternion_t4030073918  L_5 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_6 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		Object_t1021602117 * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Object_t1021602117 * Object_Instantiate_m2160322936 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2160322936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		Vector3_t2243707580  L_3 = ___position1;
		Quaternion_t4030073918  L_4 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_5 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0035;
	}

IL_001b:
	{
		Object_t1021602117 * L_6 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_6, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_7 = ___original0;
		Transform_t3275118058 * L_8 = ___parent3;
		Vector3_t2243707580  L_9 = ___position1;
		Quaternion_t4030073918  L_10 = ___rotation2;
		Object_t1021602117 * L_11 = Object_Internal_InstantiateSingleWithParent_m509082884(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0035;
	}

IL_0035:
	{
		Object_t1021602117 * L_12 = V_0;
		return L_12;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern "C"  Object_t1021602117 * Object_Instantiate_m2439155489 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2439155489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		Object_t1021602117 * L_2 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern "C"  Object_t1021602117 * Object_Instantiate_m2177117080 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2177117080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = ___original0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_Instantiate_m2489341053(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1021602117 * Object_Instantiate_m2489341053 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, bool ___instantiateInWorldSpace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2489341053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0032;
	}

IL_0019:
	{
		Object_t1021602117 * L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_4, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_5 = ___original0;
		Transform_t3275118058 * L_6 = ___parent1;
		bool L_7 = ___instantiateInWorldSpace2;
		Object_t1021602117 * L_8 = Object_Internal_CloneSingleWithParent_m665572246(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0032;
	}

IL_0032:
	{
		Object_t1021602117 * L_9 = V_0;
		return L_9;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m1711119106 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m1711119106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000e:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t1021602117 * Object_FindObjectOfType_m2330404063 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_m2330404063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t4217747464* V_0 = NULL;
	Object_t1021602117 * V_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t4217747464* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		ObjectU5BU5D_t4217747464* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		Object_t1021602117 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		goto IL_0021;
	}

IL_001a:
	{
		V_1 = (Object_t1021602117 *)NULL;
		goto IL_0021;
	}

IL_0021:
	{
		Object_t1021602117 * L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m3764089466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m2402264703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern "C"  void Object__cctor_m2991092887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m2991092887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// System.Void UnityEngine.ParticleEmitter::set_worldVelocity(UnityEngine.Vector3)
extern "C"  void ParticleEmitter_set_worldVelocity_m2375842138 (ParticleEmitter_t4099167268 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		ParticleEmitter_INTERNAL_set_worldVelocity_m1083072100(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleEmitter::INTERNAL_set_worldVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_set_worldVelocity_m1083072100 (ParticleEmitter_t4099167268 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*ParticleEmitter_INTERNAL_set_worldVelocity_m1083072100_ftn) (ParticleEmitter_t4099167268 *, Vector3_t2243707580 *);
	static ParticleEmitter_INTERNAL_set_worldVelocity_m1083072100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleEmitter_INTERNAL_set_worldVelocity_m1083072100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleEmitter::INTERNAL_set_worldVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern "C"  bool ParticleSystem_get_isPlaying_m1575756079 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isPlaying_m1575756079_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_get_isPlaying_m1575756079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPlaying_m1575756079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_startSize(System.Single)
extern "C"  void ParticleSystem_set_startSize_m657274193 (ParticleSystem_t3394631041 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_startSize_m657274193_ftn) (ParticleSystem_t3394631041 *, float);
	static ParticleSystem_set_startSize_m657274193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_startSize_m657274193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_startSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t6751348  ParticleSystem_get_main_m1338387869 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	MainModule_t6751348  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t6751348  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m4099059742(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t6751348  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t2748003162  ParticleSystem_get_emission_m3968992617 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	EmissionModule_t2748003162  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t2748003162  L_0;
		memset(&L_0, 0, sizeof(L_0));
		EmissionModule__ctor_m1076689768(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		EmissionModule_t2748003162  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape()
extern "C"  ShapeModule_t2888832346  ParticleSystem_get_shape_m1960653537 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	ShapeModule_t2888832346  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ShapeModule_t2888832346  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ShapeModule__ctor_m2156531686(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ShapeModule_t2888832346  L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Play_m2372735108 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Play_m2372735108_ftn) (ParticleSystem_t3394631041 *);
	static ParticleSystem_Internal_Play_m2372735108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Play_m2372735108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)
extern "C"  bool ParticleSystem_Internal_Stop_m3380666254 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___self0, int32_t ___stopBehavior1, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Stop_m3380666254_ftn) (ParticleSystem_t3394631041 *, int32_t);
	static ParticleSystem_Internal_Stop_m3380666254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Stop_m3380666254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)");
	return _il2cpp_icall_func(___self0, ___stopBehavior1);
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C"  void ParticleSystem_Play_m4171585816 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Play_m1705837075(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m1705837075 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Play_m1705837075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t3394631041 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t3394631041 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t2419492168 * L_1 = ((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CPlayU3Em__0_m4250446697_MethodInfo_var);
		IteratorDelegate_t2419492168 * L_3 = (IteratorDelegate_t2419492168 *)il2cpp_codegen_object_new(IteratorDelegate_t2419492168_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m3692393942(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		IteratorDelegate_t2419492168 * L_4 = ((ParticleSystem_t3394631041_StaticFields*)ParticleSystem_t3394631041_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m1240416587(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m941760450 (ParticleSystem_t3394631041 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		V_1 = (bool)1;
		bool L_0 = V_1;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m3810114649(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m3810114649 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Stop_m3810114649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStopU3Ec__AnonStorey1_t1810852071 * V_0 = NULL;
	{
		U3CStopU3Ec__AnonStorey1_t1810852071 * L_0 = (U3CStopU3Ec__AnonStorey1_t1810852071 *)il2cpp_codegen_object_new(U3CStopU3Ec__AnonStorey1_t1810852071_il2cpp_TypeInfo_var);
		U3CStopU3Ec__AnonStorey1__ctor_m1832369351(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStopU3Ec__AnonStorey1_t1810852071 * L_1 = V_0;
		int32_t L_2 = ___stopBehavior1;
		NullCheck(L_1);
		L_1->set_stopBehavior_0(L_2);
		bool L_3 = ___withChildren0;
		U3CStopU3Ec__AnonStorey1_t1810852071 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m2516318650_MethodInfo_var);
		IteratorDelegate_t2419492168 * L_6 = (IteratorDelegate_t2419492168 *)il2cpp_codegen_object_new(IteratorDelegate_t2419492168_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m3692393942(L_6, L_4, L_5, /*hidden argument*/NULL);
		ParticleSystem_IterateParticleSystems_m1240416587(__this, L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystems(System.Boolean,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystems_m1240416587 (ParticleSystem_t3394631041 * __this, bool ___recurse0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		IteratorDelegate_t2419492168 * L_0 = ___func1;
		NullCheck(L_0);
		bool L_1 = IteratorDelegate_Invoke_m3389138368(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___recurse0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IteratorDelegate_t2419492168 * L_5 = ___func1;
		bool L_6 = ParticleSystem_IterateParticleSystemsRecursive_m3260878897(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = (bool)((int32_t)((int32_t)L_3|(int32_t)L_6));
	}

IL_001e:
	{
		bool L_7 = V_0;
		V_1 = L_7;
		goto IL_0025;
	}

IL_0025:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystemsRecursive(UnityEngine.Transform,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystemsRecursive_m3260878897 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___transform0, IteratorDelegate_t2419492168 * ___func1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_IterateParticleSystemsRecursive_m3260878897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Transform_t3275118058 * V_3 = NULL;
	ParticleSystem_t3394631041 * V_4 = NULL;
	bool V_5 = false;
	{
		V_0 = (bool)0;
		Transform_t3275118058 * L_0 = ___transform0;
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m881385315(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0057;
	}

IL_0011:
	{
		Transform_t3275118058 * L_2 = ___transform0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Transform_t3275118058 * L_4 = Transform_GetChild_m3838588184(L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		Transform_t3275118058 * L_5 = V_3;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		ParticleSystem_t3394631041 * L_7 = GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504(L_6, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t3394631041_m2067134504_MethodInfo_var);
		V_4 = L_7;
		ParticleSystem_t3394631041 * L_8 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0052;
		}
	}
	{
		IteratorDelegate_t2419492168 * L_10 = ___func1;
		ParticleSystem_t3394631041 * L_11 = V_4;
		NullCheck(L_10);
		bool L_12 = IteratorDelegate_Invoke_m3389138368(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_0049;
		}
	}
	{
		goto IL_005e;
	}

IL_0049:
	{
		Transform_t3275118058 * L_14 = V_3;
		IteratorDelegate_t2419492168 * L_15 = ___func1;
		ParticleSystem_IterateParticleSystemsRecursive_m3260878897(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0052:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0011;
		}
	}

IL_005e:
	{
		bool L_19 = V_0;
		V_5 = L_19;
		goto IL_0066;
	}

IL_0066:
	{
		bool L_20 = V_5;
		return L_20;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Play>m__0(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CPlayU3Em__0_m4250446697 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Play_m2372735108(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::.ctor()
extern "C"  void U3CStopU3Ec__AnonStorey1__ctor_m1832369351 (U3CStopU3Ec__AnonStorey1_t1810852071 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::<>m__0(UnityEngine.ParticleSystem)
extern "C"  bool U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m2516318650 (U3CStopU3Ec__AnonStorey1_t1810852071 * __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t3394631041 * L_0 = ___ps0;
		int32_t L_1 = __this->get_stopBehavior_0();
		bool L_2 = ParticleSystem_Internal_Stop_m3380666254(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_pinvoke(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_back(const EmissionModule_t2748003162_marshaled_pinvoke& marshaled, EmissionModule_t2748003162& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_pinvoke_cleanup(EmissionModule_t2748003162_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_com(const EmissionModule_t2748003162& unmarshaled, EmissionModule_t2748003162_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t2748003162_marshal_com_back(const EmissionModule_t2748003162_marshaled_com& marshaled, EmissionModule_t2748003162& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t2748003162_marshal_com_cleanup(EmissionModule_t2748003162_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1076689768 (EmissionModule_t2748003162 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void EmissionModule__ctor_m1076689768_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	EmissionModule_t2748003162 * _thisAdjusted = reinterpret_cast<EmissionModule_t2748003162 *>(__this + 1);
	EmissionModule__ctor_m1076689768(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void EmissionModule_set_rateOverTime_m2031054960 (EmissionModule_t2748003162 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		EmissionModule_SetRateOverTime_m1613592011(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_rateOverTime_m2031054960_AdjustorThunk (Il2CppObject * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	EmissionModule_t2748003162 * _thisAdjusted = reinterpret_cast<EmissionModule_t2748003162 *>(__this + 1);
	EmissionModule_set_rateOverTime_m2031054960(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_SetRateOverTime_m1613592011 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method)
{
	typedef void (*EmissionModule_SetRateOverTime_m1613592011_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static EmissionModule_SetRateOverTime_m1613592011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetRateOverTime_m1613592011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/IteratorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void IteratorDelegate__ctor_m3692393942 (IteratorDelegate_t2419492168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::Invoke(UnityEngine.ParticleSystem)
extern "C"  bool IteratorDelegate_Invoke_m3389138368 (IteratorDelegate_t2419492168 * __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		IteratorDelegate_Invoke_m3389138368((IteratorDelegate_t2419492168 *)__this->get_prev_9(),___ps0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, ParticleSystem_t3394631041 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.ParticleSystem/IteratorDelegate::BeginInvoke(UnityEngine.ParticleSystem,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * IteratorDelegate_BeginInvoke_m1741046139 (IteratorDelegate_t2419492168 * __this, ParticleSystem_t3394631041 * ___ps0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ps0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool IteratorDelegate_EndInvoke_m121475984 (IteratorDelegate_t2419492168 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_pinvoke_back(const MainModule_t6751348_marshaled_pinvoke& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke_cleanup(MainModule_t6751348_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_com_back(const MainModule_t6751348_marshaled_com& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com_cleanup(MainModule_t6751348_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m4099059742 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m4099059742_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule__ctor_m4099059742(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_startLifetime_m682695196 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartLifetime_m2660765731(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startLifetime_m682695196_AdjustorThunk (Il2CppObject * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_startLifetime_m682695196(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeed(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_startSpeed_m2177648984 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartSpeed_m1948181975(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startSpeed_m2177648984_AdjustorThunk (Il2CppObject * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_startSpeed_m2177648984(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSize(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void MainModule_set_startSize_m2538003034 (MainModule_t6751348 * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartSizeX_m3050019665(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startSize_m2538003034_AdjustorThunk (Il2CppObject * __this, MinMaxCurve_t122563058  ___value0, const MethodInfo* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_startSize_m2538003034(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern "C"  void MainModule_set_startColor_m956995175 (MainModule_t6751348 * __this, MinMaxGradient_t3708298175  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartColor_m2686940116(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startColor_m956995175_AdjustorThunk (Il2CppObject * __this, MinMaxGradient_t3708298175  ___value0, const MethodInfo* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_startColor_m956995175(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetStartLifetime_m2660765731 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method)
{
	typedef void (*MainModule_SetStartLifetime_m2660765731_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static MainModule_SetStartLifetime_m2660765731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartLifetime_m2660765731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetStartSpeed_m1948181975 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method)
{
	typedef void (*MainModule_SetStartSpeed_m1948181975_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static MainModule_SetStartSpeed_m1948181975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartSpeed_m1948181975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_SetStartSizeX_m3050019665 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxCurve_t122563058 * ___curve1, const MethodInfo* method)
{
	typedef void (*MainModule_SetStartSizeX_m3050019665_ftn) (ParticleSystem_t3394631041 *, MinMaxCurve_t122563058 *);
	static MainModule_SetStartSizeX_m3050019665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartSizeX_m3050019665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_SetStartColor_m2686940116 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const MethodInfo* method)
{
	typedef void (*MainModule_SetStartColor_m2686940116_ftn) (ParticleSystem_t3394631041 *, MinMaxGradient_t3708298175 *);
	static MainModule_SetStartColor_m2686940116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartColor_m2686940116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)");
	_il2cpp_icall_func(___system0, ___gradient1);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke(const MinMaxCurve_t122563058& unmarshaled, MinMaxCurve_t122563058_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3306541151_marshal_pinvoke(*unmarshaled.get_m_CurveMin_2(), marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3306541151_marshal_pinvoke(*unmarshaled.get_m_CurveMax_3(), marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke_back(const MinMaxCurve_t122563058_marshaled_pinvoke& marshaled, MinMaxCurve_t122563058& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_t122563058_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	unmarshaled.set_m_CurveMin_2((AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3707994114(unmarshaled.get_m_CurveMin_2(), NULL);
	AnimationCurve_t3306541151_marshal_pinvoke_back(marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	unmarshaled.set_m_CurveMax_3((AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3707994114(unmarshaled.get_m_CurveMax_3(), NULL);
	AnimationCurve_t3306541151_marshal_pinvoke_back(marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_pinvoke_cleanup(MinMaxCurve_t122563058_marshaled_pinvoke& marshaled)
{
	AnimationCurve_t3306541151_marshal_pinvoke_cleanup(marshaled.___m_CurveMin_2);
	AnimationCurve_t3306541151_marshal_pinvoke_cleanup(marshaled.___m_CurveMax_3);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_com(const MinMaxCurve_t122563058& unmarshaled, MinMaxCurve_t122563058_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3306541151_marshal_com(*unmarshaled.get_m_CurveMin_2(), *marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3306541151_marshal_com(*unmarshaled.get_m_CurveMax_3(), *marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t122563058_marshal_com_back(const MinMaxCurve_t122563058_marshaled_com& marshaled, MinMaxCurve_t122563058& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	if (unmarshaled.get_m_CurveMin_2() != NULL)
	{
		AnimationCurve__ctor_m3707994114(unmarshaled.get_m_CurveMin_2(), NULL);
		AnimationCurve_t3306541151_marshal_com_back(*marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	}
	if (unmarshaled.get_m_CurveMax_3() != NULL)
	{
		AnimationCurve__ctor_m3707994114(unmarshaled.get_m_CurveMax_3(), NULL);
		AnimationCurve_t3306541151_marshal_com_back(*marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	}
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t122563058_marshal_com_cleanup(MinMaxCurve_t122563058_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_CurveMin_2) != NULL) AnimationCurve_t3306541151_marshal_com_cleanup(*marshaled.___m_CurveMin_2);
	if (&(*marshaled.___m_CurveMax_3) != NULL) AnimationCurve_t3306541151_marshal_com_cleanup(*marshaled.___m_CurveMax_3);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m1329384447 (MinMaxCurve_t122563058 * __this, float ___constant0, const MethodInfo* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_CurveMultiplier_1((0.0f));
		__this->set_m_CurveMin_2((AnimationCurve_t3306541151 *)NULL);
		__this->set_m_CurveMax_3((AnimationCurve_t3306541151 *)NULL);
		__this->set_m_ConstantMin_4((0.0f));
		float L_0 = ___constant0;
		__this->set_m_ConstantMax_5(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve__ctor_m1329384447_AdjustorThunk (Il2CppObject * __this, float ___constant0, const MethodInfo* method)
{
	MinMaxCurve_t122563058 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t122563058 *>(__this + 1);
	MinMaxCurve__ctor_m1329384447(_thisAdjusted, ___constant0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern "C"  MinMaxCurve_t122563058  MinMaxCurve_op_Implicit_m263613603 (Il2CppObject * __this /* static, unused */, float ___constant0, const MethodInfo* method)
{
	MinMaxCurve_t122563058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___constant0;
		MinMaxCurve_t122563058  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MinMaxCurve__ctor_m1329384447(&L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MinMaxCurve_t122563058  L_2 = V_0;
		return L_2;
	}
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke(const MinMaxGradient_t3708298175& unmarshaled, MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	if (unmarshaled.get_m_GradientMin_1() != NULL) Gradient_t3600583008_marshal_pinvoke(*unmarshaled.get_m_GradientMin_1(), marshaled.___m_GradientMin_1);
	if (unmarshaled.get_m_GradientMax_2() != NULL) Gradient_t3600583008_marshal_pinvoke(*unmarshaled.get_m_GradientMax_2(), marshaled.___m_GradientMax_2);
	marshaled.___m_ColorMin_3 = unmarshaled.get_m_ColorMin_3();
	marshaled.___m_ColorMax_4 = unmarshaled.get_m_ColorMax_4();
}
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke_back(const MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled, MinMaxGradient_t3708298175& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxGradient_t3708298175_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	unmarshaled.set_m_GradientMin_1((Gradient_t3600583008 *)il2cpp_codegen_object_new(Gradient_t3600583008_il2cpp_TypeInfo_var));
	Gradient__ctor_m954570311(unmarshaled.get_m_GradientMin_1(), NULL);
	Gradient_t3600583008_marshal_pinvoke_back(marshaled.___m_GradientMin_1, *unmarshaled.get_m_GradientMin_1());
	unmarshaled.set_m_GradientMax_2((Gradient_t3600583008 *)il2cpp_codegen_object_new(Gradient_t3600583008_il2cpp_TypeInfo_var));
	Gradient__ctor_m954570311(unmarshaled.get_m_GradientMax_2(), NULL);
	Gradient_t3600583008_marshal_pinvoke_back(marshaled.___m_GradientMax_2, *unmarshaled.get_m_GradientMax_2());
	Color_t2020392075  unmarshaled_m_ColorMin_temp_3;
	memset(&unmarshaled_m_ColorMin_temp_3, 0, sizeof(unmarshaled_m_ColorMin_temp_3));
	unmarshaled_m_ColorMin_temp_3 = marshaled.___m_ColorMin_3;
	unmarshaled.set_m_ColorMin_3(unmarshaled_m_ColorMin_temp_3);
	Color_t2020392075  unmarshaled_m_ColorMax_temp_4;
	memset(&unmarshaled_m_ColorMax_temp_4, 0, sizeof(unmarshaled_m_ColorMax_temp_4));
	unmarshaled_m_ColorMax_temp_4 = marshaled.___m_ColorMax_4;
	unmarshaled.set_m_ColorMax_4(unmarshaled_m_ColorMax_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke_cleanup(MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled)
{
	Gradient_t3600583008_marshal_pinvoke_cleanup(marshaled.___m_GradientMin_1);
	Gradient_t3600583008_marshal_pinvoke_cleanup(marshaled.___m_GradientMax_2);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_com(const MinMaxGradient_t3708298175& unmarshaled, MinMaxGradient_t3708298175_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	if (unmarshaled.get_m_GradientMin_1() != NULL) Gradient_t3600583008_marshal_com(*unmarshaled.get_m_GradientMin_1(), *marshaled.___m_GradientMin_1);
	if (unmarshaled.get_m_GradientMax_2() != NULL) Gradient_t3600583008_marshal_com(*unmarshaled.get_m_GradientMax_2(), *marshaled.___m_GradientMax_2);
	marshaled.___m_ColorMin_3 = unmarshaled.get_m_ColorMin_3();
	marshaled.___m_ColorMax_4 = unmarshaled.get_m_ColorMax_4();
}
extern "C" void MinMaxGradient_t3708298175_marshal_com_back(const MinMaxGradient_t3708298175_marshaled_com& marshaled, MinMaxGradient_t3708298175& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	if (unmarshaled.get_m_GradientMin_1() != NULL)
	{
		Gradient__ctor_m954570311(unmarshaled.get_m_GradientMin_1(), NULL);
		Gradient_t3600583008_marshal_com_back(*marshaled.___m_GradientMin_1, *unmarshaled.get_m_GradientMin_1());
	}
	if (unmarshaled.get_m_GradientMax_2() != NULL)
	{
		Gradient__ctor_m954570311(unmarshaled.get_m_GradientMax_2(), NULL);
		Gradient_t3600583008_marshal_com_back(*marshaled.___m_GradientMax_2, *unmarshaled.get_m_GradientMax_2());
	}
	Color_t2020392075  unmarshaled_m_ColorMin_temp_3;
	memset(&unmarshaled_m_ColorMin_temp_3, 0, sizeof(unmarshaled_m_ColorMin_temp_3));
	unmarshaled_m_ColorMin_temp_3 = marshaled.___m_ColorMin_3;
	unmarshaled.set_m_ColorMin_3(unmarshaled_m_ColorMin_temp_3);
	Color_t2020392075  unmarshaled_m_ColorMax_temp_4;
	memset(&unmarshaled_m_ColorMax_temp_4, 0, sizeof(unmarshaled_m_ColorMax_temp_4));
	unmarshaled_m_ColorMax_temp_4 = marshaled.___m_ColorMax_4;
	unmarshaled.set_m_ColorMax_4(unmarshaled_m_ColorMax_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_com_cleanup(MinMaxGradient_t3708298175_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_GradientMin_1) != NULL) Gradient_t3600583008_marshal_com_cleanup(*marshaled.___m_GradientMin_1);
	if (&(*marshaled.___m_GradientMax_2) != NULL) Gradient_t3600583008_marshal_com_cleanup(*marshaled.___m_GradientMax_2);
}
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern "C"  void MinMaxGradient__ctor_m3588183433 (MinMaxGradient_t3708298175 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_GradientMin_1((Gradient_t3600583008 *)NULL);
		__this->set_m_GradientMax_2((Gradient_t3600583008 *)NULL);
		Color_t2020392075  L_0 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_ColorMin_3(L_0);
		Color_t2020392075  L_1 = ___color0;
		__this->set_m_ColorMax_4(L_1);
		return;
	}
}
extern "C"  void MinMaxGradient__ctor_m3588183433_AdjustorThunk (Il2CppObject * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	MinMaxGradient_t3708298175 * _thisAdjusted = reinterpret_cast<MinMaxGradient_t3708298175 *>(__this + 1);
	MinMaxGradient__ctor_m3588183433(_thisAdjusted, ___color0, method);
}
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MinMaxGradient::op_Implicit(UnityEngine.Color)
extern "C"  MinMaxGradient_t3708298175  MinMaxGradient_op_Implicit_m70445640 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, const MethodInfo* method)
{
	MinMaxGradient_t3708298175  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = ___color0;
		MinMaxGradient_t3708298175  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MinMaxGradient__ctor_m3588183433(&L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MinMaxGradient_t3708298175  L_2 = V_0;
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t2888832346_marshal_pinvoke(const ShapeModule_t2888832346& unmarshaled, ShapeModule_t2888832346_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ShapeModule_t2888832346_marshal_pinvoke_back(const ShapeModule_t2888832346_marshaled_pinvoke& marshaled, ShapeModule_t2888832346& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t2888832346_marshal_pinvoke_cleanup(ShapeModule_t2888832346_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t2888832346_marshal_com(const ShapeModule_t2888832346& unmarshaled, ShapeModule_t2888832346_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ShapeModule_t2888832346_marshal_com_back(const ShapeModule_t2888832346_marshaled_com& marshaled, ShapeModule_t2888832346& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t2888832346_marshal_com_cleanup(ShapeModule_t2888832346_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ShapeModule__ctor_m2156531686 (ShapeModule_t2888832346 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ShapeModule__ctor_m2156531686_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const MethodInfo* method)
{
	ShapeModule_t2888832346 * _thisAdjusted = reinterpret_cast<ShapeModule_t2888832346 *>(__this + 1);
	ShapeModule__ctor_m2156531686(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_shapeType(UnityEngine.ParticleSystemShapeType)
extern "C"  void ShapeModule_set_shapeType_m564654877 (ShapeModule_t2888832346 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = ___value0;
		ShapeModule_SetShapeType_m1168176288(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ShapeModule_set_shapeType_m564654877_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	ShapeModule_t2888832346 * _thisAdjusted = reinterpret_cast<ShapeModule_t2888832346 *>(__this + 1);
	ShapeModule_set_shapeType_m564654877(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_radius(System.Single)
extern "C"  void ShapeModule_set_radius_m197221568 (ShapeModule_t2888832346 * __this, float ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		ShapeModule_SetRadius_m2622004587(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ShapeModule_set_radius_m197221568_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	ShapeModule_t2888832346 * _thisAdjusted = reinterpret_cast<ShapeModule_t2888832346 *>(__this + 1);
	ShapeModule_set_radius_m197221568(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_angle(System.Single)
extern "C"  void ShapeModule_set_angle_m1034890115 (ShapeModule_t2888832346 * __this, float ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		ShapeModule_SetAngle_m1851062332(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ShapeModule_set_angle_m1034890115_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	ShapeModule_t2888832346 * _thisAdjusted = reinterpret_cast<ShapeModule_t2888832346 *>(__this + 1);
	ShapeModule_set_angle_m1034890115(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_length(System.Single)
extern "C"  void ShapeModule_set_length_m2894234238 (ShapeModule_t2888832346 * __this, float ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		ShapeModule_SetLength_m4061315987(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ShapeModule_set_length_m2894234238_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	ShapeModule_t2888832346 * _thisAdjusted = reinterpret_cast<ShapeModule_t2888832346 *>(__this + 1);
	ShapeModule_set_length_m2894234238(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetShapeType(UnityEngine.ParticleSystem,System.Int32)
extern "C"  void ShapeModule_SetShapeType_m1168176288 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, int32_t ___value1, const MethodInfo* method)
{
	typedef void (*ShapeModule_SetShapeType_m1168176288_ftn) (ParticleSystem_t3394631041 *, int32_t);
	static ShapeModule_SetShapeType_m1168176288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ShapeModule_SetShapeType_m1168176288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ShapeModule::SetShapeType(UnityEngine.ParticleSystem,System.Int32)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetRadius(UnityEngine.ParticleSystem,System.Single)
extern "C"  void ShapeModule_SetRadius_m2622004587 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const MethodInfo* method)
{
	typedef void (*ShapeModule_SetRadius_m2622004587_ftn) (ParticleSystem_t3394631041 *, float);
	static ShapeModule_SetRadius_m2622004587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ShapeModule_SetRadius_m2622004587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ShapeModule::SetRadius(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetAngle(UnityEngine.ParticleSystem,System.Single)
extern "C"  void ShapeModule_SetAngle_m1851062332 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const MethodInfo* method)
{
	typedef void (*ShapeModule_SetAngle_m1851062332_ftn) (ParticleSystem_t3394631041 *, float);
	static ShapeModule_SetAngle_m1851062332_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ShapeModule_SetAngle_m1851062332_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ShapeModule::SetAngle(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetLength(UnityEngine.ParticleSystem,System.Single)
extern "C"  void ShapeModule_SetLength_m4061315987 (Il2CppObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, float ___value1, const MethodInfo* method)
{
	typedef void (*ShapeModule_SetLength_m4061315987_ftn) (ParticleSystem_t3394631041 *, float);
	static ShapeModule_SetLength_m4061315987_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ShapeModule_SetLength_m4061315987_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ShapeModule::SetLength(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Void UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)
extern "C"  void PhysicMaterial_set_frictionCombine_m695947105 (PhysicMaterial_t578636151 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*PhysicMaterial_set_frictionCombine_m695947105_ftn) (PhysicMaterial_t578636151 *, int32_t);
	static PhysicMaterial_set_frictionCombine_m695947105_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicMaterial_set_frictionCombine_m695947105_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C"  Vector3_t2243707580  Physics_get_gravity_m195764919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Physics_INTERNAL_get_gravity_m3802008392(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Physics::set_gravity(UnityEngine.Vector3)
extern "C"  void Physics_set_gravity_m2705728856 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Physics_INTERNAL_set_gravity_m2999035988(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_get_gravity_m3802008392 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_get_gravity_m3802008392_ftn) (Vector3_t2243707580 *);
	static Physics_INTERNAL_get_gravity_m3802008392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_get_gravity_m3802008392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_set_gravity_m2999035988 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_set_gravity_m2999035988_ftn) (Vector3_t2243707580 *);
	static Physics_INTERNAL_set_gravity_m2999035988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_set_gravity_m2999035988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2874007225 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_Raycast_m89212106 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_Raycast_m2667915561 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3475924638 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = ___queryTriggerInteraction4;
		bool L_5 = Physics_Internal_RaycastTest_m3442924926(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1929115794 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2994111303 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_7 = V_2;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m4027183840 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_7 = V_3;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m2036777053 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = ___queryTriggerInteraction5;
		bool L_6 = Physics_Internal_Raycast_m1160243045(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2691929452 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1844392139(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern "C"  bool Physics_Raycast_m780162053 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1844392139(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_5 = V_2;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern "C"  bool Physics_Raycast_m2686676054 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1844392139(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_5 = V_3;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1844392139 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		bool L_5 = Physics_Raycast_m3475924638(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2009151399 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2308457076 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m2736931691 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m233619224 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		RaycastHit_t87180320 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m233036521 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1214023521* V_1 = NULL;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_RaycastAll_m410413656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3928448900 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t1214023521* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_RaycastAll_m410413656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_2;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m1246652201 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t1214023521* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_RaycastAll_m410413656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		RaycastHitU5BU5D_t1214023521* L_5 = V_3;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m410413656 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_t1214023521* L_5 = Physics_RaycastAll_m3908263591(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		RaycastHitU5BU5D_t1214023521* L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3908263591 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1214023521* V_0 = NULL;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3256436970 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1214023521* V_1 = NULL;
	{
		V_0 = 0;
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3484190429 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t1214023521* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		float L_0 = ___maxDistance2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		goto IL_0018;
	}

IL_0018:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_2;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3650851272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t1214023521* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		float L_0 = V_2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHitU5BU5D_t1214023521* L_4 = V_3;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_INTERNAL_CALL_RaycastAll_m2126789092 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t1214023521* (*Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1160243045 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	bool V_0 = false;
	{
		RaycastHit_t87180320 * L_0 = ___hitInfo2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___layermask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m93849932(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m93849932 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, RaycastHit_t87180320 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layermask4, ___queryTriggerInteraction5);
}
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m3442924926 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		bool L_3 = Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_RaycastTest_m3440840981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern "C"  bool Physics2D_get_queriesHitTriggers_m361275035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Physics2D_get_queriesHitTriggers_m361275035_ftn) ();
	static Physics2D_get_queriesHitTriggers_m361275035_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_get_queriesHitTriggers_m361275035_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::get_queriesHitTriggers()");
	return _il2cpp_icall_func();
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m1220041042 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1220041042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	RaycastHit2D_t4063908774  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = ___minDepth4;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2D_t4063908774  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m122312471 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m122312471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	RaycastHit2D_t4063908774  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHit2D_t4063908774  L_7 = V_2;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m3913913442 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m3913913442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	RaycastHit2D_t4063908774  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		V_2 = ((int32_t)-5);
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = V_2;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		RaycastHit2D_t4063908774  L_7 = V_3;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m2560154475 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2560154475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	RaycastHit2D_t4063908774  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		V_2 = ((int32_t)-5);
		V_3 = (std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = V_3;
		int32_t L_3 = V_2;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		goto IL_0028;
	}

IL_0028:
	{
		RaycastHit2D_t4063908774  L_7 = V_4;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m2303387255 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2303387255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t1672660996  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t4063908774  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RaycastHit2D_t4063908774  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = ___layerMask3;
		float L_1 = ___minDepth4;
		float L_2 = ___maxDepth5;
		ContactFilter2D_t1672660996  L_3 = ContactFilter2D_CreateLegacyFilter_m1912787689(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2243707579  L_4 = ___origin0;
		Vector2_t2243707579  L_5 = ___direction1;
		float L_6 = ___distance2;
		ContactFilter2D_t1672660996  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m2213595168(NULL /*static, unused*/, L_4, L_5, L_6, L_7, (&V_1), /*hidden argument*/NULL);
		RaycastHit2D_t4063908774  L_8 = V_1;
		V_2 = L_8;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHit2D_t4063908774  L_9 = V_2;
		return L_9;
	}
}
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_Raycast_m2368200185 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, ContactFilter2D_t1672660996  ___contactFilter2, RaycastHit2DU5BU5D_t4176517891* ___results3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2368200185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		ContactFilter2D_t1672660996  L_2 = ___contactFilter2;
		RaycastHit2DU5BU5D_t4176517891* L_3 = ___results3;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		int32_t L_5 = Physics2D_Raycast_m564567838(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0017;
	}

IL_0017:
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_Raycast_m564567838 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, ContactFilter2D_t1672660996  ___contactFilter2, RaycastHit2DU5BU5D_t4176517891* ___results3, float ___distance4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m564567838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance4;
		ContactFilter2D_t1672660996  L_3 = ___contactFilter2;
		RaycastHit2DU5BU5D_t4176517891* L_4 = ___results3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		int32_t L_5 = Physics2D_Internal_RaycastNonAlloc_m1874107548(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Raycast_m2213595168 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, ContactFilter2D_t1672660996  ___contactFilter3, RaycastHit2D_t4063908774 * ___raycastHit4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Raycast_m2213595168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___distance2;
		RaycastHit2D_t4063908774 * L_1 = ___raycastHit4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m489831109(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, (&___contactFilter3), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m489831109 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___origin0, Vector2_t2243707579 * ___direction1, float ___distance2, ContactFilter2D_t1672660996 * ___contactFilter3, RaycastHit2D_t4063908774 * ___raycastHit4, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m489831109_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, float, ContactFilter2D_t1672660996 *, RaycastHit2D_t4063908774 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m489831109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m489831109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___contactFilter3, ___raycastHit4);
}
// System.Int32 UnityEngine.Physics2D::Internal_RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_Internal_RaycastNonAlloc_m1874107548 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, ContactFilter2D_t1672660996  ___contactFilter3, RaycastHit2DU5BU5D_t4176517891* ___results4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_RaycastNonAlloc_m1874107548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___distance2;
		RaycastHit2DU5BU5D_t4176517891* L_1 = ___results4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		int32_t L_2 = Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m2253171281(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, (&___contactFilter3), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0015;
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m2253171281 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___origin0, Vector2_t2243707579 * ___direction1, float ___distance2, ContactFilter2D_t1672660996 * ___contactFilter3, RaycastHit2DU5BU5D_t4176517891* ___results4, const MethodInfo* method)
{
	typedef int32_t (*Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m2253171281_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, float, ContactFilter2D_t1672660996 *, RaycastHit2DU5BU5D_t4176517891*);
	static Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m2253171281_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m2253171281_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])");
	return _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___contactFilter3, ___results4);
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_GetRayIntersectionAll_m253330691 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m253330691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2DU5BU5D_t4176517891* V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		RaycastHit2DU5BU5D_t4176517891* L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_GetRayIntersectionAll_m2808325432 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m2808325432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaycastHit2DU5BU5D_t4176517891* V_1 = NULL;
	{
		V_0 = ((int32_t)-5);
		float L_0 = ___distance1;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		RaycastHit2DU5BU5D_t4176517891* L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_GetRayIntersectionAll_m120415839 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m120415839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	RaycastHit2DU5BU5D_t4176517891* V_2 = NULL;
	{
		V_0 = ((int32_t)-5);
		V_1 = (std::numeric_limits<float>::infinity());
		float L_0 = V_1;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2DU5BU5D_t4176517891* L_3 = V_2;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998 (Il2CppObject * __this /* static, unused */, Ray_t2469606224 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t4176517891* (*Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn) (Ray_t2469606224 *, float, int32_t);
	static Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___ray0, ___distance1, ___layerMask2);
}
// UnityEngine.Rigidbody2D UnityEngine.Physics2D::GetRigidbodyFromInstanceID(System.Int32)
extern "C"  Rigidbody2D_t502193897 * Physics2D_GetRigidbodyFromInstanceID_m3260338451 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef Rigidbody2D_t502193897 * (*Physics2D_GetRigidbodyFromInstanceID_m3260338451_ftn) (int32_t);
	static Physics2D_GetRigidbodyFromInstanceID_m3260338451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_GetRigidbodyFromInstanceID_m3260338451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::GetRigidbodyFromInstanceID(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// UnityEngine.Collider2D UnityEngine.Physics2D::GetColliderFromInstanceID(System.Int32)
extern "C"  Collider2D_t646061738 * Physics2D_GetColliderFromInstanceID_m95920619 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef Collider2D_t646061738 * (*Physics2D_GetColliderFromInstanceID_m95920619_ftn) (int32_t);
	static Physics2D_GetColliderFromInstanceID_m95920619_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_GetColliderFromInstanceID_m95920619_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::GetColliderFromInstanceID(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern "C"  void Physics2D__cctor_m3532647019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m3532647019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4166282325 * L_0 = (List_1_t4166282325 *)il2cpp_codegen_object_new(List_1_t4166282325_il2cpp_TypeInfo_var);
		List_1__ctor_m2338710192(L_0, /*hidden argument*/List_1__ctor_m2338710192_MethodInfo_var);
		((Physics2D_t2540166467_StaticFields*)Physics2D_t2540166467_il2cpp_TypeInfo_var->static_fields)->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m3187718367 (Plane_t3727654732 * __this, Vector3_t2243707580  ___inNormal0, Vector3_t2243707580  ___inPoint1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___inNormal0;
		Vector3_t2243707580  L_1 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t2243707580  L_2 = ___inNormal0;
		Vector3_t2243707580  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m3187718367_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___inNormal0, Vector3_t2243707580  ___inPoint1, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	Plane__ctor_m3187718367(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t2243707580  Plane_get_normal_m1872443823 (Plane_t3727654732 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Plane_get_normal_m1872443823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_get_normal_m1872443823(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m1834776091 (Plane_t3727654732 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Plane_get_distance_m1834776091_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_get_distance_m1834776091(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m2870142810 (Plane_t3727654732 * __this, Ray_t2469606224  ___ray0, float* ___enter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m2870142810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector3_t2243707580  L_0 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Plane_get_normal_m1872443823(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Plane_get_normal_m1872443823(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m1834776091(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		V_2 = (bool)0;
		goto IL_0062;
	}

IL_004e:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		V_2 = (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
		goto IL_0062;
	}

IL_0062:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
extern "C"  bool Plane_Raycast_m2870142810_AdjustorThunk (Il2CppObject * __this, Ray_t2469606224  ___ray0, float* ___enter1, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_Raycast_m2870142810(_thisAdjusted, ___ray0, ___enter1, method);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C"  bool PlayerPrefs_TrySetInt_m965228897 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m965228897_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m965228897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m965228897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C"  bool PlayerPrefs_TrySetSetString_m3460332240 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m3460332240_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m3460332240_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m3460332240_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m3351928596 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetInt_m3351928596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		int32_t L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetInt_m965228897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		PlayerPrefsException_t3229544204 * L_3 = (PlayerPrefsException_t3229544204 *)il2cpp_codegen_object_new(PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m1748847897(L_3, _stringLiteral2331435015, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0018:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m136681260 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m136681260_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m136681260_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m136681260_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m2889062785 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___key0;
		int32_t L_1 = V_0;
		int32_t L_2 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		int32_t L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern "C"  void PlayerPrefs_SetString_m2547809843 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetString_m2547809843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetSetString_m3460332240(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		PlayerPrefsException_t3229544204 * L_3 = (PlayerPrefsException_t3229544204 *)il2cpp_codegen_object_new(PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m1748847897(L_3, _stringLiteral2331435015, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0018:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C"  String_t* PlayerPrefs_GetString_m1246323344 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m1246323344_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m1246323344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m1246323344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern "C"  String_t* PlayerPrefs_GetString_m1903615000 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_GetString_m1903615000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		V_0 = _stringLiteral371857150;
		String_t* L_0 = ___key0;
		String_t* L_1 = V_0;
		String_t* L_2 = PlayerPrefs_GetString_m1246323344(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		String_t* L_3 = V_1;
		return L_3;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C"  bool PlayerPrefs_HasKey_m1212656251 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m1212656251_ftn) (String_t*);
	static PlayerPrefs_HasKey_m1212656251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m1212656251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
extern "C"  void PlayerPrefs_DeleteKey_m2519743590 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteKey_m2519743590_ftn) (String_t*);
	static PlayerPrefs_DeleteKey_m2519743590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteKey_m2519743590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteKey(System.String)");
	_il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C"  void PlayerPrefs_Save_m212854761 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_Save_m212854761_ftn) ();
	static PlayerPrefs_Save_m212854761_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_Save_m212854761_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::Save()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C"  void PlayerPrefsException__ctor_m1748847897 (PlayerPrefsException_t3229544204 * __this, String_t* ___error0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error0;
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PreferBinarySerialization::.ctor()
extern "C"  void PreferBinarySerialization__ctor_m2043201510 (PreferBinarySerialization_t2472773525 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityEngine.Projector::get_material()
extern "C"  Material_t193706927 * Projector_get_material_m3492569908 (Projector_t1121484678 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Projector_get_material_m3492569908_ftn) (Projector_t1121484678 *);
	static Projector_get_material_m3492569908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Projector_get_material_m3492569908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Projector::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3663555848 (PropertyAttribute_t2606999759 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
extern "C"  int32_t QualitySettings_get_pixelLightCount_m3399686231 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_pixelLightCount_m3399686231_ftn) ();
	static QualitySettings_get_pixelLightCount_m3399686231_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_pixelLightCount_m3399686231_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_pixelLightCount()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
extern "C"  void QualitySettings_set_pixelLightCount_m2777101340 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_pixelLightCount_m2777101340_ftn) (int32_t);
	static QualitySettings_set_pixelLightCount_m2777101340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_pixelLightCount_m2777101340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ShadowProjection UnityEngine.QualitySettings::get_shadowProjection()
extern "C"  int32_t QualitySettings_get_shadowProjection_m3372477940 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_shadowProjection_m3372477940_ftn) ();
	static QualitySettings_get_shadowProjection_m3372477940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_shadowProjection_m3372477940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_shadowProjection()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_shadowProjection(UnityEngine.ShadowProjection)
extern "C"  void QualitySettings_set_shadowProjection_m3682877915 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowProjection_m3682877915_ftn) (int32_t);
	static QualitySettings_set_shadowProjection_m3682877915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowProjection_m3682877915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowProjection(UnityEngine.ShadowProjection)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.QualitySettings::get_shadowDistance()
extern "C"  float QualitySettings_get_shadowDistance_m1511807063 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*QualitySettings_get_shadowDistance_m1511807063_ftn) ();
	static QualitySettings_get_shadowDistance_m1511807063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_shadowDistance_m1511807063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_shadowDistance()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern "C"  void QualitySettings_set_shadowDistance_m2077100724 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowDistance_m2077100724_ftn) (float);
	static QualitySettings_set_shadowDistance_m2077100724_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowDistance_m2077100724_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.QualitySettings::set_shadowResolution(UnityEngine.ShadowResolution)
extern "C"  void QualitySettings_set_shadowResolution_m3103104847 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowResolution_m3103104847_ftn) (int32_t);
	static QualitySettings_set_shadowResolution_m3103104847_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowResolution_m3103104847_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowResolution(UnityEngine.ShadowResolution)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m580875098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m580875098_ftn) ();
	static QualitySettings_get_activeColorSpace_m580875098_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m580875098_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3196903881 (Quaternion_t4030073918 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m3196903881_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	Quaternion__ctor_m3196903881(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_AngleAxis_m2806222563 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t2243707580  ___axis1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m3310327005(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Quaternion_t4030073918  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m3310327005 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t2243707580 * ___axis1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn) (float, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_FromToRotation_m1685306068 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___fromDirection0, Vector3_t2243707580  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_FromToRotation_m2839236110(NULL /*static, unused*/, (&___fromDirection0), (&___toDirection1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m2839236110 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___fromDirection0, Vector3_t2243707580 * ___toDirection1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___fromDirection0, ___toDirection1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m700700634 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___forward0, Vector3_t2243707580  ___upwards1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m3606560944(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m633695927 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___forward0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Quaternion_INTERNAL_CALL_LookRotation_m3606560944(NULL /*static, unused*/, (&___forward0), (&V_0), (&V_1), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_1;
		V_2 = L_1;
		goto IL_0019;
	}

IL_0019:
	{
		Quaternion_t4030073918  L_2 = V_2;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m3606560944 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___forward0, Vector3_t2243707580 * ___upwards1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Lerp_m3623055223 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Lerp_m1920404743(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t4030073918  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m1920404743 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *, float, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_Inverse_m3931399088 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Inverse_m1043108654(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m1043108654 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = Quaternion_Internal_ToEulerRad_m2807508879(NULL /*static, unused*/, (*(Quaternion_t4030073918 *)__this), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Quaternion_Internal_MakePositive_m2921671247(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t2243707580  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_get_eulerAngles_m3302573991(_thisAdjusted, method);
}
// System.Void UnityEngine.Quaternion::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Quaternion_set_eulerAngles_m3695252804 (Quaternion_t4030073918 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Quaternion_t4030073918 *)__this) = L_2;
		return;
	}
}
extern "C"  void Quaternion_set_eulerAngles_m3695252804_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	Quaternion_set_eulerAngles_m3695252804(_thisAdjusted, ___value0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		Quaternion_t4030073918  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m3586339259 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = ___euler0;
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Quaternion_t4030073918  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t2243707580  Quaternion_Internal_ToEulerRad_m2807508879 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Vector3_t2243707580 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn) (Quaternion_t4030073918 *, Vector3_t2243707580 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Internal_FromEulerRad_m1121344272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___euler0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn) (Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t4030073918  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m3196903881(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Quaternion_t4030073918  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_op_Multiply_m2426727589 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_w_3();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_x_0();
		float L_3 = (&___rhs1)->get_w_3();
		float L_4 = (&___lhs0)->get_y_1();
		float L_5 = (&___rhs1)->get_z_2();
		float L_6 = (&___lhs0)->get_z_2();
		float L_7 = (&___rhs1)->get_y_1();
		float L_8 = (&___lhs0)->get_w_3();
		float L_9 = (&___rhs1)->get_y_1();
		float L_10 = (&___lhs0)->get_y_1();
		float L_11 = (&___rhs1)->get_w_3();
		float L_12 = (&___lhs0)->get_z_2();
		float L_13 = (&___rhs1)->get_x_0();
		float L_14 = (&___lhs0)->get_x_0();
		float L_15 = (&___rhs1)->get_z_2();
		float L_16 = (&___lhs0)->get_w_3();
		float L_17 = (&___rhs1)->get_z_2();
		float L_18 = (&___lhs0)->get_z_2();
		float L_19 = (&___rhs1)->get_w_3();
		float L_20 = (&___lhs0)->get_x_0();
		float L_21 = (&___rhs1)->get_y_1();
		float L_22 = (&___lhs0)->get_y_1();
		float L_23 = (&___rhs1)->get_x_0();
		float L_24 = (&___lhs0)->get_w_3();
		float L_25 = (&___rhs1)->get_w_3();
		float L_26 = (&___lhs0)->get_x_0();
		float L_27 = (&___rhs1)->get_x_0();
		float L_28 = (&___lhs0)->get_y_1();
		float L_29 = (&___rhs1)->get_y_1();
		float L_30 = (&___lhs0)->get_z_2();
		float L_31 = (&___rhs1)->get_z_2();
		Quaternion_t4030073918  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m3196903881(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0108;
	}

IL_0108:
	{
		Quaternion_t4030073918  L_33 = V_0;
		return L_33;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_op_Multiply_m1483423721 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, Vector3_t2243707580  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		float L_0 = (&___rotation0)->get_x_0();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_1();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_2();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_0();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_1();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_2();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_0();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_0();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_1();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_3();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_3();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_3();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t2243707580  L_48 = V_12;
		V_13 = L_48;
		goto IL_0136;
	}

IL_0136:
	{
		Vector3_t2243707580  L_49 = V_13;
		return L_49;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m2308156925 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Quaternion_t4030073918  L_0 = ___lhs0;
		Quaternion_t4030073918  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m952616600(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) > ((float)(0.999999f)))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m3629786166 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Quaternion_t4030073918  L_0 = ___lhs0;
		Quaternion_t4030073918  L_1 = ___rhs1;
		bool L_2 = Quaternion_op_Equality_m2308156925(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m952616600 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		float L_4 = (&___a0)->get_z_2();
		float L_5 = (&___b1)->get_z_2();
		float L_6 = (&___a0)->get_w_3();
		float L_7 = (&___b1)->get_w_3();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_Internal_MakePositive_m2921671247 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (-0.005729578f);
		float L_0 = V_0;
		V_1 = ((float)((float)(360.0f)+(float)L_0));
		float L_1 = (&___euler0)->get_x_1();
		float L_2 = V_0;
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_0034;
		}
	}
	{
		Vector3_t2243707580 * L_3 = (&___euler0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4+(float)(360.0f))));
		goto IL_0054;
	}

IL_0034:
	{
		float L_5 = (&___euler0)->get_x_1();
		float L_6 = V_1;
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0054;
		}
	}
	{
		Vector3_t2243707580 * L_7 = (&___euler0);
		float L_8 = L_7->get_x_1();
		L_7->set_x_1(((float)((float)L_8-(float)(360.0f))));
	}

IL_0054:
	{
		float L_9 = (&___euler0)->get_y_2();
		float L_10 = V_0;
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0079;
		}
	}
	{
		Vector3_t2243707580 * L_11 = (&___euler0);
		float L_12 = L_11->get_y_2();
		L_11->set_y_2(((float)((float)L_12+(float)(360.0f))));
		goto IL_0099;
	}

IL_0079:
	{
		float L_13 = (&___euler0)->get_y_2();
		float L_14 = V_1;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0099;
		}
	}
	{
		Vector3_t2243707580 * L_15 = (&___euler0);
		float L_16 = L_15->get_y_2();
		L_15->set_y_2(((float)((float)L_16-(float)(360.0f))));
	}

IL_0099:
	{
		float L_17 = (&___euler0)->get_z_3();
		float L_18 = V_0;
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_00be;
		}
	}
	{
		Vector3_t2243707580 * L_19 = (&___euler0);
		float L_20 = L_19->get_z_3();
		L_19->set_z_3(((float)((float)L_20+(float)(360.0f))));
		goto IL_00de;
	}

IL_00be:
	{
		float L_21 = (&___euler0)->get_z_3();
		float L_22 = V_1;
		if ((!(((float)L_21) > ((float)L_22))))
		{
			goto IL_00de;
		}
	}
	{
		Vector3_t2243707580 * L_23 = (&___euler0);
		float L_24 = L_23->get_z_3();
		L_23->set_z_3(((float)((float)L_24-(float)(360.0f))));
	}

IL_00de:
	{
		Vector3_t2243707580  L_25 = ___euler0;
		V_2 = L_25;
		goto IL_00e5;
	}

IL_00e5:
	{
		Vector3_t2243707580  L_26 = V_2;
		return L_26;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m2270520528 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m3102305584(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m2270520528_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_GetHashCode_m2270520528(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m3730391696 (Quaternion_t4030073918 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3730391696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t4030073918_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_1, Quaternion_t4030073918_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m3359827399(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Quaternion_Equals_m3730391696_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_Equals_m3730391696(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m2638853272 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m2638853272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3587482509, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Quaternion_ToString_m2638853272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_ToString_m2638853272(_thisAdjusted, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2884721203 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method)
{
	typedef float (*Random_Range_m2884721203_ftn) (float, float);
	static Random_Range_m2884721203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m2884721203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m374035151(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m374035151 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m374035151_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m374035151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m374035151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Single UnityEngine.Random::get_value()
extern "C"  float Random_get_value_m976649312 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Random_get_value_m976649312_ftn) ();
	static Random_get_value_m976649312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_value_m976649312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_value()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern "C"  Vector3_t2243707580  Random_get_insideUnitSphere_m4012327022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Random_INTERNAL_get_insideUnitSphere_m3752571035(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_insideUnitSphere_m3752571035 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_insideUnitSphere_m3752571035_ftn) (Vector3_t2243707580 *);
	static Random_INTERNAL_get_insideUnitSphere_m3752571035_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_insideUnitSphere_m3752571035_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1657271662 (RangeAttribute_t3336560921 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3663555848(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m913869897 (RangeInt_t2323401134 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_start_0();
		int32_t L_1 = __this->get_length_1();
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t RangeInt_get_end_m913869897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RangeInt_t2323401134 * _thisAdjusted = reinterpret_cast<RangeInt_t2323401134 *>(__this + 1);
	return RangeInt_get_end_m913869897(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m3379034047 (Ray_t2469606224 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t2243707580  L_1 = Vector3_get_normalized_m936072361((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m3379034047_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	Ray__ctor_m3379034047(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t2243707580  Ray_get_origin_m3339262500 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Origin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Ray_get_origin_m3339262500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_get_origin_m3339262500(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_get_direction_m4059191533(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366 (Ray_t2469606224 * __this, float ___distance0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Origin_0();
		Vector3_t2243707580  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t2243707580  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_GetPoint_m1353702366(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m2019179238 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m2019179238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t2243707580  L_1 = __this->get_m_Origin_0();
		Vector3_t2243707580  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		Vector3_t2243707580  L_5 = __this->get_m_Direction_1();
		Vector3_t2243707580  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1807026812, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m2019179238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_ToString_m2019179238(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_pinvoke(const RaycastHit_t87180320& unmarshaled, RaycastHit_t87180320_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t87180320_marshal_pinvoke_back(const RaycastHit_t87180320_marshaled_pinvoke& marshaled, RaycastHit_t87180320& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_pinvoke_cleanup(RaycastHit_t87180320_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_com(const RaycastHit_t87180320& unmarshaled, RaycastHit_t87180320_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t87180320_marshal_com_back(const RaycastHit_t87180320_marshaled_com& marshaled, RaycastHit_t87180320& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_com_cleanup(RaycastHit_t87180320_marshaled_com& marshaled)
{
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t2243707580  RaycastHit_get_point_m326143462 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Point_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  RaycastHit_get_point_m326143462_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_point_m326143462(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m817665579 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m817665579_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_normal_m817665579(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
extern "C"  void RaycastHit_set_normal_m1788712468 (RaycastHit_t87180320 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_m_Normal_1(L_0);
		return;
	}
}
extern "C"  void RaycastHit_set_normal_m1788712468_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	RaycastHit_set_normal_m1788712468(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m1178709367 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit_get_distance_m1178709367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_distance_m1178709367(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t3497673348 * RaycastHit_get_collider_m301198172 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	Collider_t3497673348 * V_0 = NULL;
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider_t3497673348 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider_t3497673348 * RaycastHit_get_collider_m301198172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_collider_m301198172(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t4233889191 * RaycastHit_get_rigidbody_m480380820 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_rigidbody_m480380820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t4233889191 * V_0 = NULL;
	Rigidbody_t4233889191 * G_B3_0 = NULL;
	{
		Collider_t3497673348 * L_0 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Collider_t3497673348 * L_2 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t4233889191 * L_3 = Collider_get_attachedRigidbody_m3279305420(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((Rigidbody_t4233889191 *)(NULL));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		Rigidbody_t4233889191 * L_4 = V_0;
		return L_4;
	}
}
extern "C"  Rigidbody_t4233889191 * RaycastHit_get_rigidbody_m480380820_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_rigidbody_m480380820(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m3290290036 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_transform_m3290290036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t4233889191 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = RaycastHit_get_rigidbody_m480380820(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t4233889191 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Rigidbody_t4233889191 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0049;
	}

IL_0020:
	{
		Collider_t3497673348 * L_5 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Collider_t3497673348 * L_7 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_0049;
	}

IL_0042:
	{
		V_1 = (Transform_t3275118058 *)NULL;
		goto IL_0049;
	}

IL_0049:
	{
		Transform_t3275118058 * L_9 = V_1;
		return L_9;
	}
}
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m3290290036_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_transform_m3290290036(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke(const RaycastHit2D_t4063908774& unmarshaled, RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_back(const RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled, RaycastHit2D_t4063908774& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_cleanup(RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_com(const RaycastHit2D_t4063908774& unmarshaled, RaycastHit2D_t4063908774_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t4063908774_marshal_com_back(const RaycastHit2D_t4063908774_marshaled_com& marshaled, RaycastHit2D_t4063908774& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_com_cleanup(RaycastHit2D_t4063908774_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_point_m442317739 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = __this->get_m_Point_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2243707579  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2243707579  RaycastHit2D_get_point_m442317739_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_point_m442317739(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_normal_m3768105386 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = __this->get_m_Normal_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2243707579  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2243707579  RaycastHit2D_get_normal_m3768105386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_normal_m3768105386(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m1296150410 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Fraction_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m1296150410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_fraction_m1296150410(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	Collider2D_t646061738 * V_0 = NULL;
	{
		Collider2D_t646061738 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider2D_t646061738 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_collider_m2568504212(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m1220545469_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect__ctor_m1220545469(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m3896897500 (Rect_t3681755626 * __this, Rect_t3681755626  ___source0, const MethodInfo* method)
{
	{
		float L_0 = (&___source0)->get_m_XMin_0();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___source0)->get_m_YMin_1();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___source0)->get_m_Width_2();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___source0)->get_m_Height_3();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3896897500_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___source0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect__ctor_m3896897500(_thisAdjusted, ___source0, method);
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  Rect_t3681755626  Rect_MinMaxRect_m4237641803 (Il2CppObject * __this /* static, unused */, float ___xmin0, float ___ymin1, float ___xmax2, float ___ymax3, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___xmin0;
		float L_1 = ___ymin1;
		float L_2 = ___xmax2;
		float L_3 = ___xmin0;
		float L_4 = ___ymax3;
		float L_5 = ___ymin1;
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0014;
	}

IL_0014:
	{
		Rect_t3681755626  L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m1393582490 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m1393582490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_x_m1393582490(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m3783700513 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m3783700513_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_x_m3783700513(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1393582395 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m1393582395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_y_m1393582395(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m4294916608 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m4294916608_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_y_m4294916608(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2243707579  Rect_get_position_m24550734 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_position_m24550734_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_position_m24550734(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2243707579  Rect_get_center_m3049923624 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		Vector2_t2243707579  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_center_m3049923624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_center_m3049923624(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2243707579  Rect_get_min_m2549872833 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_min_m2549872833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_min_m2549872833(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_min(UnityEngine.Vector2)
extern "C"  void Rect_set_min_m2178285060 (Rect_t3681755626 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_0();
		Rect_set_xMin_m4214255623(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (&___value0)->get_y_1();
		Rect_set_yMin_m734445288(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Rect_set_min_m2178285060_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_min_m2178285060(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2243707579  Rect_get_max_m96665935 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_max_m96665935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_max_m96665935(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_max(UnityEngine.Vector2)
extern "C"  void Rect_set_max_m2794417386 (Rect_t3681755626 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_0();
		Rect_set_xMax_m3501625033(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (&___value0)->get_y_1();
		Rect_set_yMax_m21814698(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Rect_set_max_m2794417386_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_max_m2794417386(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m1138015702 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m1138015702_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_width_m1138015702(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m1921257731 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m1921257731_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_width_m1921257731(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m3128694305 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m3128694305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_height_m3128694305(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m2019122814 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m2019122814_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_height_m2019122814(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2243707579  Rect_get_size_m3833121112 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_size_m3833121112_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_size_m3833121112(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m1161102488 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m1161102488_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMin_m1161102488(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m4214255623 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m4214255623_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_xMin_m4214255623(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1161103577 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m1161103577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMin_m1161103577(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m734445288 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m734445288_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_yMin_m734445288(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2915145014 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m2915145014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMax_m2915145014(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m3501625033 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m3501625033_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_xMax_m3501625033(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2915146103 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m2915146103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMax_m2915146103(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m21814698 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m21814698_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_yMax_m21814698(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m1334685290 (Rect_t3681755626 * __this, Vector2_t2243707579  ___point0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_0();
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_0();
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_1();
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_1();
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m1334685290_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___point0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m1334685290(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1334685291 (Rect_t3681755626 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m1334685291_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m1334685291(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3,System.Boolean)
extern "C"  bool Rect_Contains_m4048719162 (Rect_t3681755626 * __this, Vector3_t2243707580  ___point0, bool ___allowInverse1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		bool L_0 = ___allowInverse1;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Vector3_t2243707580  L_1 = ___point0;
		bool L_2 = Rect_Contains_m1334685291(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_00fd;
	}

IL_0015:
	{
		V_1 = (bool)0;
		float L_3 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = (&___point0)->get_x_1();
		float L_5 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) <= ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = (&___point0)->get_x_1();
		float L_7 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((((float)L_6) > ((float)L_7)))
		{
			goto IL_007f;
		}
	}

IL_004b:
	{
		float L_8 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		if ((!(((float)L_8) >= ((float)(0.0f)))))
		{
			goto IL_0081;
		}
	}
	{
		float L_9 = (&___point0)->get_x_1();
		float L_10 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)L_10))))
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = (&___point0)->get_x_1();
		float L_12 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_0081;
		}
	}

IL_007f:
	{
		V_1 = (bool)1;
	}

IL_0081:
	{
		bool L_13 = V_1;
		if (!L_13)
		{
			goto IL_00f6;
		}
	}
	{
		float L_14 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		if ((!(((float)L_14) < ((float)(0.0f)))))
		{
			goto IL_00bb;
		}
	}
	{
		float L_15 = (&___point0)->get_y_2();
		float L_16 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_15) <= ((float)L_16))))
		{
			goto IL_00bb;
		}
	}
	{
		float L_17 = (&___point0)->get_y_2();
		float L_18 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		if ((((float)L_17) > ((float)L_18)))
		{
			goto IL_00ef;
		}
	}

IL_00bb:
	{
		float L_19 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		if ((!(((float)L_19) >= ((float)(0.0f)))))
		{
			goto IL_00f6;
		}
	}
	{
		float L_20 = (&___point0)->get_y_2();
		float L_21 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_20) >= ((float)L_21))))
		{
			goto IL_00f6;
		}
	}
	{
		float L_22 = (&___point0)->get_y_2();
		float L_23 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		if ((!(((float)L_22) < ((float)L_23))))
		{
			goto IL_00f6;
		}
	}

IL_00ef:
	{
		V_0 = (bool)1;
		goto IL_00fd;
	}

IL_00f6:
	{
		V_0 = (bool)0;
		goto IL_00fd;
	}

IL_00fd:
	{
		bool L_24 = V_0;
		return L_24;
	}
}
extern "C"  bool Rect_Contains_m4048719162_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___point0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m4048719162(_thisAdjusted, ___point0, ___allowInverse1, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t3681755626  Rect_OrderMinMax_m1783437776 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = Rect_get_xMin_m1161102488((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m2915145014((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0034;
		}
	}
	{
		float L_2 = Rect_get_xMin_m1161102488((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m2915145014((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m4214255623((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m3501625033((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		float L_5 = Rect_get_yMin_m1161103577((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m2915146103((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = Rect_get_yMin_m1161103577((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m2915146103((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m734445288((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m21814698((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0067:
	{
		Rect_t3681755626  L_10 = ___rect0;
		V_2 = L_10;
		goto IL_006e;
	}

IL_006e:
	{
		Rect_t3681755626  L_11 = V_2;
		return L_11;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m210444568 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m2915145014((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = Rect_get_xMin_m1161102488((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = Rect_get_yMax_m2915146103((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = Rect_get_yMin_m1161103577((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Overlaps_m210444568_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___other0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Overlaps_m210444568(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m4145874649 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		V_0 = (*(Rect_t3681755626 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Rect_t3681755626  L_1 = V_0;
		Rect_t3681755626  L_2 = Rect_OrderMinMax_m1783437776(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t3681755626  L_3 = ___other0;
		Rect_t3681755626  L_4 = Rect_OrderMinMax_m1783437776(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001f:
	{
		Rect_t3681755626  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m210444568((&V_0), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002d;
	}

IL_002d:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
extern "C"  bool Rect_Overlaps_m4145874649_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Overlaps_m4145874649(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m3595915756 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___lhs0, Rect_t3681755626  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Rect_t3681755626  L_0 = ___lhs0;
		Rect_t3681755626  L_1 = ___rhs1;
		bool L_2 = Rect_op_Equality_m2793663577(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m2793663577 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___lhs0, Rect_t3681755626  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m1393582490((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m1393582490((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004c;
		}
	}
	{
		float L_2 = Rect_get_y_m1393582395((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m1393582395((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004c;
		}
	}
	{
		float L_4 = Rect_get_width_m1138015702((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m1138015702((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004c;
		}
	}
	{
		float L_6 = Rect_get_height_m3128694305((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m3128694305((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004d;
	}

IL_004c:
	{
		G_B5_0 = 0;
	}

IL_004d:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m559954498 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m3102305584((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m3102305584((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m3102305584((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m3102305584((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m559954498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_GetHashCode_m559954498(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m3806390726 (Rect_t3681755626 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m3806390726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t3681755626_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_1, Rect_t3681755626_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m3359827399((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m1393582395((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m3359827399((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m1138015702((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m3359827399((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m3359827399((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m3806390726_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Equals_m3806390726(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m2728794442 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2728794442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1853817013, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m2728794442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_ToString_m2728794442(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_pinvoke(const RectOffset_t3387826427& unmarshaled, RectOffset_t3387826427_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if ((unmarshaled.get_m_SourceStyle_1())->klass->is_import_or_windows_runtime)
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t3387826427_marshal_pinvoke_back(const RectOffset_t3387826427_marshaled_pinvoke& marshaled, RectOffset_t3387826427& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t3387826427_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<Il2CppObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_pinvoke_cleanup(RectOffset_t3387826427_marshaled_pinvoke& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_com(const RectOffset_t3387826427& unmarshaled, RectOffset_t3387826427_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if ((unmarshaled.get_m_SourceStyle_1())->klass->is_import_or_windows_runtime)
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, true);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t3387826427_marshal_com_back(const RectOffset_t3387826427_marshaled_com& marshaled, RectOffset_t3387826427& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t3387826427_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<Il2CppObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_com_cleanup(RectOffset_t3387826427_marshaled_com& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2227510254 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		RectOffset_Init_m4361650(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Object,System.IntPtr)
extern "C"  void RectOffset__ctor_m1265077918 (RectOffset_t3387826427 * __this, Il2CppObject * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void RectOffset__ctor_m4133355596 (RectOffset_t3387826427 * __this, int32_t ___left0, int32_t ___right1, int32_t ___top2, int32_t ___bottom3, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		RectOffset_Init_m4361650(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left0;
		RectOffset_set_left_m620681523(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right1;
		RectOffset_set_right_m1671272302(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top2;
		RectOffset_set_top_m3579196427(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom3;
		RectOffset_set_bottom_m4065521443(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m4361650 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m4361650_ftn) (RectOffset_t3387826427 *);
	static RectOffset_Init_m4361650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m4361650_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m3198970074 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m3198970074_ftn) (RectOffset_t3387826427 *);
	static RectOffset_Cleanup_m3198970074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m3198970074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m439065308 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m439065308_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_left_m439065308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m439065308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m620681523 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m620681523_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_left_m620681523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m620681523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m281378687 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m281378687_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_right_m281378687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m281378687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m1671272302 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m1671272302_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_right_m1671272302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m1671272302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m3629049358 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m3629049358_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_top_m3629049358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m3629049358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3579196427 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m3579196427_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_top_m3579196427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3579196427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m4112328858 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m4112328858_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_bottom_m4112328858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m4112328858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m4065521443 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m4065521443_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_bottom_m4065521443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m4065521443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m3818523637 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m3818523637_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_horizontal_m3818523637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m3818523637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m3856345169 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3856345169_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_vertical_m3856345169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3856345169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Add(UnityEngine.Rect)
extern "C"  Rect_t3681755626  RectOffset_Add_m3336105852 (RectOffset_t3387826427 * __this, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectOffset_INTERNAL_CALL_Add_m3705097782(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Rect_t3681755626  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Add_m3705097782 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___self0, Rect_t3681755626 * ___rect1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Add_m3705097782_ftn) (RectOffset_t3387826427 *, Rect_t3681755626 *, Rect_t3681755626 *);
	static RectOffset_INTERNAL_CALL_Add_m3705097782_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Add_m3705097782_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C"  Rect_t3681755626  RectOffset_Remove_m1330811977 (RectOffset_t3387826427 * __this, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectOffset_INTERNAL_CALL_Remove_m1194095901(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Rect_t3681755626  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Remove_m1194095901 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___self0, Rect_t3681755626 * ___rect1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn) (RectOffset_t3387826427 *, Rect_t3681755626 *, Rect_t3681755626 *);
	static RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m901770914 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0012;
			}
		}

IL_000c:
		{
			RectOffset_Cleanup_m3198970074(__this, /*hidden argument*/NULL);
		}

IL_0012:
		{
			IL2CPP_LEAVE(0x1E, FINALLY_0017);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0017;
	}

FINALLY_0017:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(23)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(23)
	{
		IL2CPP_JUMP_TBL(0x1E, IL_001e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_001e:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern "C"  String_t* RectOffset_ToString_m1281517011 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m1281517011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m439065308(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m281378687(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m3629049358(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m4112328858(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3899275604, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t3681755626  RectTransform_get_rect_m73954734 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_rect_m1177342209(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3681755626  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1177342209 (RectTransform_t3349966182 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1177342209_ftn) (RectTransform_t3349966182 *, Rect_t3681755626 *);
	static RectTransform_INTERNAL_get_rect_m1177342209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1177342209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMin_m1497323108 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMin_m3180545469(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4247668187 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m885423409(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m3180545469 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m885423409 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m885423409_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchorMin_m885423409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m885423409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMax_m3816015142 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMax_m834202955(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2955899993 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m1551648727(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m834202955 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m834202955_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchorMax_m834202955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m834202955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m1551648727 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2243707579  RectTransform_get_anchoredPosition_m3570822376 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m3564306187(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2077229449 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m693024247(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m3564306187 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m693024247 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2243707579  RectTransform_get_sizeDelta_m2157326342 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_sizeDelta_m3975625099(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m2319668137 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m1402803191(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m3975625099 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m1402803191 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2243707579  RectTransform_get_pivot_m759087479 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_pivot_m3003734630(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2243707579  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m1360548980 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m2764958706(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m3003734630 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m3003734630_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_pivot_m3003734630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m3003734630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m2764958706 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m2764958706_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_pivot_m2764958706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m2764958706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1603911943 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t2020713228 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1603911943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t2020713228 * V_0 = NULL;
	ReapplyDrivenProperties_t2020713228 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t2020713228 * L_2 = V_1;
		ReapplyDrivenProperties_t2020713228 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t2020713228 * L_5 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t2020713228 *>((((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t2020713228 *)CastclassSealed(L_4, ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t2020713228 * L_7 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_7) == ((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m4209881182 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t2020713228 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m4209881182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t2020713228 * V_0 = NULL;
	ReapplyDrivenProperties_t2020713228 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t2020713228 * L_2 = V_1;
		ReapplyDrivenProperties_t2020713228 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t2020713228 * L_5 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t2020713228 *>((((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t2020713228 *)CastclassSealed(L_4, ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t2020713228 * L_7 = V_0;
		ReapplyDrivenProperties_t2020713228 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_7) == ((Il2CppObject*)(ReapplyDrivenProperties_t2020713228 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern "C"  void RectTransform_SendReapplyDrivenProperties_m90487700 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t3349966182 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m1090213637(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m1836626405 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m1836626405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t1172311765* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral895140023, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0020:
	{
		Rect_t3681755626  L_2 = RectTransform_get_rect_m73954734(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m1393582490((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m1393582395((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t1172311765* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector3U5BU5D_t1172311765* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_14;
		Vector3U5BU5D_t1172311765* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_18;
		Vector3U5BU5D_t1172311765* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t2243707580  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2638739322(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_22;
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetWorldCorners_m3873546362 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m3873546362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t1172311765* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3518247078, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0020:
	{
		Vector3U5BU5D_t1172311765* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m1836626405(__this, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0057;
	}

IL_0035:
	{
		Vector3U5BU5D_t1172311765* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t3275118058 * L_6 = V_0;
		Vector3U5BU5D_t1172311765* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		NullCheck(L_6);
		Vector3_t2243707580  L_9 = Transform_TransformPoint_m3272254198(L_6, (*(Vector3_t2243707580 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_0035;
		}
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m2982698987 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___value0;
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t2243707579  L_7 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = V_0;
		Vector2_t2243707579  L_9 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m2319668137(__this, L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = V_0;
		Vector2_t2243707579  L_12 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t2243707579  L_15 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m2077229449(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m3702115945 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___value0;
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t2243707579  L_9 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = V_0;
		Vector2_t2243707579  L_11 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m2319668137(__this, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = V_0;
		Vector2_t2243707579  L_14 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_15 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m2077229449(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m2835026182 (RectTransform_t3349966182 * __this, int32_t ___edge0, float ___inset1, float ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t2243707579 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t2243707579 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t2243707579 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}

IL_000f:
	{
		G_B4_0 = 1;
		goto IL_0016;
	}

IL_0015:
	{
		G_B4_0 = 0;
	}

IL_0016:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B7_0 = 1;
	}

IL_0025:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0033;
	}

IL_0032:
	{
		G_B10_0 = 0;
	}

IL_0033:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t2243707579  L_5 = RectTransform_get_anchorMin_m1497323108(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m3881967114((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = V_3;
		RectTransform_set_anchorMin_m4247668187(__this, L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = RectTransform_get_anchorMax_m3816015142(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m3881967114((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = V_3;
		RectTransform_set_anchorMax_m2955899993(__this, L_12, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m3881967114((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = V_4;
		RectTransform_set_sizeDelta_m2319668137(__this, L_16, /*hidden argument*/NULL);
		Vector2_t2243707579  L_17 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ad;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t2243707579  L_22 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m2792130561((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c1;
	}

IL_00ad:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t2243707579  L_27 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m2792130561((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c1:
	{
		Vector2_set_Item_m3881967114(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_30 = V_5;
		RectTransform_set_anchoredPosition_m2077229449(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m2368352721 (RectTransform_t3349966182 * __this, int32_t ___axis0, float ___size1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t2243707579  L_1 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t2243707579  L_4 = RectTransform_GetParentSize_m1571597933(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m2792130561((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = RectTransform_get_anchorMax_m3816015142(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m2792130561((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = RectTransform_get_anchorMin_m1497323108(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m2792130561((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = V_1;
		RectTransform_set_sizeDelta_m2319668137(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t2243707579  RectTransform_GetParentSize_m1571597933 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m1571597933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3349966182 * V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3275118058 * L_0 = Transform_get_parent_m147407266(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3349966182 *)IsInstSealed(L_0, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0037;
	}

IL_0023:
	{
		RectTransform_t3349966182 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t3681755626  L_5 = RectTransform_get_rect_m73954734(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t2243707579  L_6 = Rect_get_size_m3833121112((&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0037;
	}

IL_0037:
	{
		Vector2_t2243707579  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m210072638 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m1090213637 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m1090213637((ReapplyDrivenProperties_t2020713228 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m2337529776 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m2375002944 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, Vector3_t2243707580 * ___worldPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t3727654732  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		Vector3_t2243707580 * L_0 = ___worldPoint3;
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_0) = L_2;
		Camera_t189460977 * L_3 = ___cam2;
		Vector2_t2243707579  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Ray_t2469606224  L_5 = RectTransformUtility_ScreenPointToRay_m1842507230(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t3349966182 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m1033555130(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		Plane__ctor_m3187718367((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t2469606224  L_12 = V_0;
		bool L_13 = Plane_Raycast_m2870142810((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_004c;
		}
	}
	{
		V_3 = (bool)0;
		goto IL_0061;
	}

IL_004c:
	{
		Vector3_t2243707580 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t2243707580  L_16 = Ray_GetPoint_m1353702366((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_14) = L_16;
		V_3 = (bool)1;
		goto IL_0061;
	}

IL_0061:
	{
		bool L_17 = V_3;
		return L_17;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, Vector2_t2243707579 * ___localPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t2243707579 * L_0 = ___localPoint3;
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)L_0) = L_1;
		RectTransform_t3349966182 * L_2 = ___rect0;
		Vector2_t2243707579  L_3 = ___screenPoint1;
		Camera_t189460977 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Vector2_t2243707579 * L_6 = ___localPoint3;
		RectTransform_t3349966182 * L_7 = ___rect0;
		Vector3_t2243707580  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t2243707580  L_9 = Transform_InverseTransformPoint_m2648491174(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)L_6) = L_10;
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)0;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_11 = V_1;
		return L_11;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t2469606224  RectTransformUtility_ScreenPointToRay_m1842507230 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, Vector2_t2243707579  ___screenPos1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToRay_m1842507230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t189460977 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Camera_t189460977 * L_2 = ___cam0;
		Vector2_t2243707579  L_3 = ___screenPos1;
		Vector3_t2243707580  L_4 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t2469606224  L_5 = Camera_ScreenPointToRay_m614889538(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_004a;
	}

IL_001f:
	{
		Vector2_t2243707579  L_6 = ___screenPos1;
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t2243707580 * L_8 = (&V_1);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t2243707580  L_10 = V_1;
		Vector3_t2243707580  L_11 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t2469606224  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m3379034047(&L_12, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_004a;
	}

IL_004a:
	{
		Ray_t2469606224  L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  RectTransformUtility_WorldToScreenPoint_m1650782138 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, Vector3_t2243707580  ___worldPoint1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_WorldToScreenPoint_m1650782138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t189460977 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		float L_2 = (&___worldPoint1)->get_x_1();
		float L_3 = (&___worldPoint1)->get_y_2();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0038;
	}

IL_0026:
	{
		Camera_t189460977 * L_5 = ___cam0;
		Vector3_t2243707580  L_6 = ___worldPoint1;
		NullCheck(L_5);
		Vector3_t2243707580  L_7 = Camera_WorldToScreenPoint_m638747266(L_5, L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0038;
	}

IL_0038:
	{
		Vector2_t2243707579  L_9 = V_0;
		return L_9;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3920364518 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m3920364518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3349966182 * V_1 = NULL;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00f3;
	}

IL_0012:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 0;
		goto IL_0048;
	}

IL_0020:
	{
		RectTransform_t3349966182 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		RectTransform_t3349966182 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3920364518(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_11 = V_0;
		RectTransform_t3349966182 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m881385315(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0055:
	{
		RectTransform_t3349966182 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2243707579  L_15 = RectTransform_get_pivot_m759087479(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m2792130561((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_19 = ___rect0;
		Vector2_t2243707579  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m1360548980(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0084;
		}
	}
	{
		goto IL_00f3;
	}

IL_0084:
	{
		RectTransform_t3349966182 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t2243707579  L_23 = RectTransform_get_anchoredPosition_m3570822376(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m2792130561((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_27 = ___rect0;
		Vector2_t2243707579  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m2077229449(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t2243707579  L_30 = RectTransform_get_anchorMin_m1497323108(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t3349966182 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2243707579  L_32 = RectTransform_get_anchorMax_m3816015142(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m2792130561((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m2792130561((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m3881967114((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_40 = ___rect0;
		Vector2_t2243707579  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m4247668187(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_42 = ___rect0;
		Vector2_t2243707579  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m2955899993(L_42, L_43, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m532748168 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m532748168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3349966182 * V_1 = NULL;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00b4;
	}

IL_0012:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0020:
	{
		RectTransform_t3349966182 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		RectTransform_t3349966182 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m532748168(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_10 = V_0;
		RectTransform_t3349966182 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m881385315(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0054:
	{
		RectTransform_t3349966182 * L_13 = ___rect0;
		RectTransform_t3349966182 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2243707579  L_15 = RectTransform_get_pivot_m759087479(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_16 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m1360548980(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_17 = ___rect0;
		RectTransform_t3349966182 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = RectTransform_get_sizeDelta_m2157326342(L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_20 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m2319668137(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00b4;
	}

IL_0081:
	{
		RectTransform_t3349966182 * L_22 = ___rect0;
		RectTransform_t3349966182 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = RectTransform_get_anchoredPosition_m3570822376(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_25 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m2077229449(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_26 = ___rect0;
		RectTransform_t3349966182 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t2243707579  L_28 = RectTransform_get_anchorMin_m1497323108(L_27, /*hidden argument*/NULL);
		Vector2_t2243707579  L_29 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m4247668187(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_30 = ___rect0;
		RectTransform_t3349966182 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2243707579  L_32 = RectTransform_get_anchorMax_m3816015142(L_31, /*hidden argument*/NULL);
		Vector2_t2243707579  L_33 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m2955899993(L_30, L_33, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  RectTransformUtility_GetTransposed_m1770338235 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___input0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___input0)->get_y_1();
		float L_1 = (&___input0)->get_x_0();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1244853728 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1244853728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		Camera_t189460977 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579 * ___screenPoint1, Camera_t189460977 * ___cam2, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *, Camera_t189460977 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern "C"  Vector2_t2243707579  RectTransformUtility_PixelAdjustPoint_m560908615 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m560908615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = ___elementTransform1;
		Canvas_t209405766 * L_1 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669(NULL /*static, unused*/, (&___point0), L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = V_0;
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Vector2_t2243707579  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, Vector2_t2243707579 * ___value3, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn) (Vector2_t2243707579 *, Transform_t3275118058 *, Canvas_t209405766 *, Vector2_t2243707579 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___value3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C"  Rect_t3681755626  RectTransformUtility_PixelAdjustRect_m93024038 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, Canvas_t209405766 * ___canvas1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m93024038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t3349966182 * L_0 = ___rectTransform0;
		Canvas_t209405766 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Rect_t3681755626  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, Canvas_t209405766 * ___canvas1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn) (RectTransform_t3349966182 *, Canvas_t209405766 *, Rect_t3681755626 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern "C"  void RectTransformUtility__cctor_m1866023382 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m1866023382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((RectTransformUtility_t2941082270_StaticFields*)RectTransformUtility_t2941082270_il2cpp_TypeInfo_var->static_fields)->set_s_Corners_0(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Void UnityEngine.RemoteSettings::CallOnUpdate()
extern "C"  void RemoteSettings_CallOnUpdate_m1624968574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings_CallOnUpdate_m1624968574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UpdatedEventHandler_t3033456180 * V_0 = NULL;
	{
		UpdatedEventHandler_t3033456180 * L_0 = ((RemoteSettings_t392466225_StaticFields*)RemoteSettings_t392466225_il2cpp_TypeInfo_var->static_fields)->get_Updated_0();
		V_0 = L_0;
		UpdatedEventHandler_t3033456180 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UpdatedEventHandler_t3033456180 * L_2 = V_0;
		NullCheck(L_2);
		UpdatedEventHandler_Invoke_m159598802(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180 (UpdatedEventHandler_t3033456180 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdatedEventHandler__ctor_m1393569768 (UpdatedEventHandler_t3033456180 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m159598802 (UpdatedEventHandler_t3033456180 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdatedEventHandler_Invoke_m159598802((UpdatedEventHandler_t3033456180 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RemoteSettings/UpdatedEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdatedEventHandler_BeginInvoke_m4238510153 (UpdatedEventHandler_t3033456180 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdatedEventHandler_EndInvoke_m224684362 (UpdatedEventHandler_t3033456180 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m2362836534 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_enabled_m2362836534_ftn) (Renderer_t257310565 *);
	static Renderer_get_enabled_m2362836534_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m2362836534_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m142717579 (Renderer_t257310565 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m142717579_ftn) (Renderer_t257310565 *, bool);
	static Renderer_set_enabled_m142717579_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m142717579_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern "C"  void Renderer_set_shadowCastingMode_m2840732934 (Renderer_t257310565 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_shadowCastingMode_m2840732934_ftn) (Renderer_t257310565 *, int32_t);
	static Renderer_set_shadowCastingMode_m2840732934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_shadowCastingMode_m2840732934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t193706927 * Renderer_get_material_m2553789785 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Renderer_get_material_m2553789785_ftn) (Renderer_t257310565 *);
	static Renderer_get_material_m2553789785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2553789785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1053097112 (Renderer_t257310565 * __this, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m1053097112_ftn) (Renderer_t257310565 *, Material_t193706927 *);
	static Renderer_set_material_m1053097112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m1053097112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t193706927 * Renderer_get_sharedMaterial_m155010392 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Renderer_get_sharedMaterial_m155010392_ftn) (Renderer_t257310565 *);
	static Renderer_get_sharedMaterial_m155010392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m155010392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m391095487 (Renderer_t257310565 * __this, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterial_m391095487_ftn) (Renderer_t257310565 *, Material_t193706927 *);
	static Renderer_set_sharedMaterial_m391095487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m391095487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t3123989686* Renderer_get_materials_m810004692 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t3123989686* (*Renderer_get_materials_m810004692_ftn) (Renderer_t257310565 *);
	static Renderer_get_materials_m810004692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m810004692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern "C"  void Renderer_set_materials_m1556465155 (Renderer_t257310565 * __this, MaterialU5BU5D_t3123989686* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_materials_m1556465155_ftn) (Renderer_t257310565 *, MaterialU5BU5D_t3123989686*);
	static Renderer_set_materials_m1556465155_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_materials_m1556465155_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_materials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t3123989686* Renderer_get_sharedMaterials_m4026934221 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t3123989686* (*Renderer_get_sharedMaterials_m4026934221_ftn) (Renderer_t257310565 *);
	static Renderer_get_sharedMaterials_m4026934221_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m4026934221_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t3033363703  Renderer_get_bounds_m3832626589 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Renderer_INTERNAL_get_bounds_m1299274192(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t3033363703  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Bounds_t3033363703  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Renderer_INTERNAL_get_bounds_m1299274192 (Renderer_t257310565 * __this, Bounds_t3033363703 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_bounds_m1299274192_ftn) (Renderer_t257310565 *, Bounds_t3033363703 *);
	static Renderer_INTERNAL_get_bounds_m1299274192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_bounds_m1299274192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_lightProbeUsage(UnityEngine.Rendering.LightProbeUsage)
extern "C"  void Renderer_set_lightProbeUsage_m1212616902 (Renderer_t257310565 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_lightProbeUsage_m1212616902_ftn) (Renderer_t257310565 *, int32_t);
	static Renderer_set_lightProbeUsage_m1212616902_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_lightProbeUsage_m1212616902_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_lightProbeUsage(UnityEngine.Rendering.LightProbeUsage)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern "C"  void Renderer_SetPropertyBlock_m1151988246 (Renderer_t257310565 * __this, MaterialPropertyBlock_t3303648957 * ___properties0, const MethodInfo* method)
{
	typedef void (*Renderer_SetPropertyBlock_m1151988246_ftn) (Renderer_t257310565 *, MaterialPropertyBlock_t3303648957 *);
	static Renderer_SetPropertyBlock_m1151988246_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_SetPropertyBlock_m1151988246_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(__this, ___properties0);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m2403577271 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m2403577271_ftn) (Renderer_t257310565 *);
	static Renderer_get_sortingLayerID_m2403577271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m2403577271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m1544525007 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m1544525007_ftn) (Renderer_t257310565 *);
	static Renderer_get_sortingOrder_m1544525007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m1544525007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::get_Item(System.Int32,System.Int32)
extern "C"  float SphericalHarmonicsL2_get_Item_m1027885444 (SphericalHarmonicsL2_t364136731 * __this, int32_t ___rgb0, int32_t ___coefficient1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SphericalHarmonicsL2_get_Item_m1027885444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = ___rgb0;
		int32_t L_1 = ___coefficient1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_007f;
			}
			case 1:
			{
				goto IL_008b;
			}
			case 2:
			{
				goto IL_0097;
			}
			case 3:
			{
				goto IL_00a3;
			}
			case 4:
			{
				goto IL_00af;
			}
			case 5:
			{
				goto IL_00bb;
			}
			case 6:
			{
				goto IL_00c7;
			}
			case 7:
			{
				goto IL_00d3;
			}
			case 8:
			{
				goto IL_00df;
			}
			case 9:
			{
				goto IL_00eb;
			}
			case 10:
			{
				goto IL_00f7;
			}
			case 11:
			{
				goto IL_0103;
			}
			case 12:
			{
				goto IL_010f;
			}
			case 13:
			{
				goto IL_011b;
			}
			case 14:
			{
				goto IL_0127;
			}
			case 15:
			{
				goto IL_0133;
			}
			case 16:
			{
				goto IL_013f;
			}
			case 17:
			{
				goto IL_014b;
			}
			case 18:
			{
				goto IL_0157;
			}
			case 19:
			{
				goto IL_0163;
			}
			case 20:
			{
				goto IL_016f;
			}
			case 21:
			{
				goto IL_017b;
			}
			case 22:
			{
				goto IL_0187;
			}
			case 23:
			{
				goto IL_0193;
			}
			case 24:
			{
				goto IL_019f;
			}
			case 25:
			{
				goto IL_01ab;
			}
			case 26:
			{
				goto IL_01b7;
			}
		}
	}
	{
		goto IL_01c3;
	}

IL_007f:
	{
		float L_3 = __this->get_shr0_0();
		V_1 = L_3;
		goto IL_01ce;
	}

IL_008b:
	{
		float L_4 = __this->get_shr1_1();
		V_1 = L_4;
		goto IL_01ce;
	}

IL_0097:
	{
		float L_5 = __this->get_shr2_2();
		V_1 = L_5;
		goto IL_01ce;
	}

IL_00a3:
	{
		float L_6 = __this->get_shr3_3();
		V_1 = L_6;
		goto IL_01ce;
	}

IL_00af:
	{
		float L_7 = __this->get_shr4_4();
		V_1 = L_7;
		goto IL_01ce;
	}

IL_00bb:
	{
		float L_8 = __this->get_shr5_5();
		V_1 = L_8;
		goto IL_01ce;
	}

IL_00c7:
	{
		float L_9 = __this->get_shr6_6();
		V_1 = L_9;
		goto IL_01ce;
	}

IL_00d3:
	{
		float L_10 = __this->get_shr7_7();
		V_1 = L_10;
		goto IL_01ce;
	}

IL_00df:
	{
		float L_11 = __this->get_shr8_8();
		V_1 = L_11;
		goto IL_01ce;
	}

IL_00eb:
	{
		float L_12 = __this->get_shg0_9();
		V_1 = L_12;
		goto IL_01ce;
	}

IL_00f7:
	{
		float L_13 = __this->get_shg1_10();
		V_1 = L_13;
		goto IL_01ce;
	}

IL_0103:
	{
		float L_14 = __this->get_shg2_11();
		V_1 = L_14;
		goto IL_01ce;
	}

IL_010f:
	{
		float L_15 = __this->get_shg3_12();
		V_1 = L_15;
		goto IL_01ce;
	}

IL_011b:
	{
		float L_16 = __this->get_shg4_13();
		V_1 = L_16;
		goto IL_01ce;
	}

IL_0127:
	{
		float L_17 = __this->get_shg5_14();
		V_1 = L_17;
		goto IL_01ce;
	}

IL_0133:
	{
		float L_18 = __this->get_shg6_15();
		V_1 = L_18;
		goto IL_01ce;
	}

IL_013f:
	{
		float L_19 = __this->get_shg7_16();
		V_1 = L_19;
		goto IL_01ce;
	}

IL_014b:
	{
		float L_20 = __this->get_shg8_17();
		V_1 = L_20;
		goto IL_01ce;
	}

IL_0157:
	{
		float L_21 = __this->get_shb0_18();
		V_1 = L_21;
		goto IL_01ce;
	}

IL_0163:
	{
		float L_22 = __this->get_shb1_19();
		V_1 = L_22;
		goto IL_01ce;
	}

IL_016f:
	{
		float L_23 = __this->get_shb2_20();
		V_1 = L_23;
		goto IL_01ce;
	}

IL_017b:
	{
		float L_24 = __this->get_shb3_21();
		V_1 = L_24;
		goto IL_01ce;
	}

IL_0187:
	{
		float L_25 = __this->get_shb4_22();
		V_1 = L_25;
		goto IL_01ce;
	}

IL_0193:
	{
		float L_26 = __this->get_shb5_23();
		V_1 = L_26;
		goto IL_01ce;
	}

IL_019f:
	{
		float L_27 = __this->get_shb6_24();
		V_1 = L_27;
		goto IL_01ce;
	}

IL_01ab:
	{
		float L_28 = __this->get_shb7_25();
		V_1 = L_28;
		goto IL_01ce;
	}

IL_01b7:
	{
		float L_29 = __this->get_shb8_26();
		V_1 = L_29;
		goto IL_01ce;
	}

IL_01c3:
	{
		IndexOutOfRangeException_t3527622107 * L_30 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_30, _stringLiteral1739942068, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_01ce:
	{
		float L_31 = V_1;
		return L_31;
	}
}
extern "C"  float SphericalHarmonicsL2_get_Item_m1027885444_AdjustorThunk (Il2CppObject * __this, int32_t ___rgb0, int32_t ___coefficient1, const MethodInfo* method)
{
	SphericalHarmonicsL2_t364136731 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t364136731 *>(__this + 1);
	return SphericalHarmonicsL2_get_Item_m1027885444(_thisAdjusted, ___rgb0, ___coefficient1, method);
}
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m2919647449 (SphericalHarmonicsL2_t364136731 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)17);
		int32_t L_0 = V_0;
		float* L_1 = __this->get_address_of_shr0_0();
		int32_t L_2 = Single_GetHashCode_m3102305584(L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)23)))+(int32_t)L_2));
		int32_t L_3 = V_0;
		float* L_4 = __this->get_address_of_shr1_1();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)23)))+(int32_t)L_5));
		int32_t L_6 = V_0;
		float* L_7 = __this->get_address_of_shr2_2();
		int32_t L_8 = Single_GetHashCode_m3102305584(L_7, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)23)))+(int32_t)L_8));
		int32_t L_9 = V_0;
		float* L_10 = __this->get_address_of_shr3_3();
		int32_t L_11 = Single_GetHashCode_m3102305584(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)((int32_t)23)))+(int32_t)L_11));
		int32_t L_12 = V_0;
		float* L_13 = __this->get_address_of_shr4_4();
		int32_t L_14 = Single_GetHashCode_m3102305584(L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)((int32_t)23)))+(int32_t)L_14));
		int32_t L_15 = V_0;
		float* L_16 = __this->get_address_of_shr5_5();
		int32_t L_17 = Single_GetHashCode_m3102305584(L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)((int32_t)23)))+(int32_t)L_17));
		int32_t L_18 = V_0;
		float* L_19 = __this->get_address_of_shr6_6();
		int32_t L_20 = Single_GetHashCode_m3102305584(L_19, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)((int32_t)23)))+(int32_t)L_20));
		int32_t L_21 = V_0;
		float* L_22 = __this->get_address_of_shr7_7();
		int32_t L_23 = Single_GetHashCode_m3102305584(L_22, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_21*(int32_t)((int32_t)23)))+(int32_t)L_23));
		int32_t L_24 = V_0;
		float* L_25 = __this->get_address_of_shr8_8();
		int32_t L_26 = Single_GetHashCode_m3102305584(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_24*(int32_t)((int32_t)23)))+(int32_t)L_26));
		int32_t L_27 = V_0;
		float* L_28 = __this->get_address_of_shg0_9();
		int32_t L_29 = Single_GetHashCode_m3102305584(L_28, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)((int32_t)23)))+(int32_t)L_29));
		int32_t L_30 = V_0;
		float* L_31 = __this->get_address_of_shg1_10();
		int32_t L_32 = Single_GetHashCode_m3102305584(L_31, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)23)))+(int32_t)L_32));
		int32_t L_33 = V_0;
		float* L_34 = __this->get_address_of_shg2_11();
		int32_t L_35 = Single_GetHashCode_m3102305584(L_34, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_33*(int32_t)((int32_t)23)))+(int32_t)L_35));
		int32_t L_36 = V_0;
		float* L_37 = __this->get_address_of_shg3_12();
		int32_t L_38 = Single_GetHashCode_m3102305584(L_37, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_36*(int32_t)((int32_t)23)))+(int32_t)L_38));
		int32_t L_39 = V_0;
		float* L_40 = __this->get_address_of_shg4_13();
		int32_t L_41 = Single_GetHashCode_m3102305584(L_40, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_39*(int32_t)((int32_t)23)))+(int32_t)L_41));
		int32_t L_42 = V_0;
		float* L_43 = __this->get_address_of_shg5_14();
		int32_t L_44 = Single_GetHashCode_m3102305584(L_43, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_42*(int32_t)((int32_t)23)))+(int32_t)L_44));
		int32_t L_45 = V_0;
		float* L_46 = __this->get_address_of_shg6_15();
		int32_t L_47 = Single_GetHashCode_m3102305584(L_46, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)((int32_t)23)))+(int32_t)L_47));
		int32_t L_48 = V_0;
		float* L_49 = __this->get_address_of_shg7_16();
		int32_t L_50 = Single_GetHashCode_m3102305584(L_49, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_48*(int32_t)((int32_t)23)))+(int32_t)L_50));
		int32_t L_51 = V_0;
		float* L_52 = __this->get_address_of_shg8_17();
		int32_t L_53 = Single_GetHashCode_m3102305584(L_52, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_51*(int32_t)((int32_t)23)))+(int32_t)L_53));
		int32_t L_54 = V_0;
		float* L_55 = __this->get_address_of_shb0_18();
		int32_t L_56 = Single_GetHashCode_m3102305584(L_55, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_54*(int32_t)((int32_t)23)))+(int32_t)L_56));
		int32_t L_57 = V_0;
		float* L_58 = __this->get_address_of_shb1_19();
		int32_t L_59 = Single_GetHashCode_m3102305584(L_58, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_57*(int32_t)((int32_t)23)))+(int32_t)L_59));
		int32_t L_60 = V_0;
		float* L_61 = __this->get_address_of_shb2_20();
		int32_t L_62 = Single_GetHashCode_m3102305584(L_61, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60*(int32_t)((int32_t)23)))+(int32_t)L_62));
		int32_t L_63 = V_0;
		float* L_64 = __this->get_address_of_shb3_21();
		int32_t L_65 = Single_GetHashCode_m3102305584(L_64, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63*(int32_t)((int32_t)23)))+(int32_t)L_65));
		int32_t L_66 = V_0;
		float* L_67 = __this->get_address_of_shb4_22();
		int32_t L_68 = Single_GetHashCode_m3102305584(L_67, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)((int32_t)23)))+(int32_t)L_68));
		int32_t L_69 = V_0;
		float* L_70 = __this->get_address_of_shb5_23();
		int32_t L_71 = Single_GetHashCode_m3102305584(L_70, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)((int32_t)23)))+(int32_t)L_71));
		int32_t L_72 = V_0;
		float* L_73 = __this->get_address_of_shb6_24();
		int32_t L_74 = Single_GetHashCode_m3102305584(L_73, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_72*(int32_t)((int32_t)23)))+(int32_t)L_74));
		int32_t L_75 = V_0;
		float* L_76 = __this->get_address_of_shb7_25();
		int32_t L_77 = Single_GetHashCode_m3102305584(L_76, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_75*(int32_t)((int32_t)23)))+(int32_t)L_77));
		int32_t L_78 = V_0;
		float* L_79 = __this->get_address_of_shb8_26();
		int32_t L_80 = Single_GetHashCode_m3102305584(L_79, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_78*(int32_t)((int32_t)23)))+(int32_t)L_80));
		int32_t L_81 = V_0;
		V_1 = L_81;
		goto IL_0279;
	}

IL_0279:
	{
		int32_t L_82 = V_1;
		return L_82;
	}
}
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m2919647449_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SphericalHarmonicsL2_t364136731 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t364136731 *>(__this + 1);
	return SphericalHarmonicsL2_GetHashCode_m2919647449(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern "C"  bool SphericalHarmonicsL2_Equals_m3577919107 (SphericalHarmonicsL2_t364136731 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SphericalHarmonicsL2_Equals_m3577919107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	SphericalHarmonicsL2_t364136731  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, SphericalHarmonicsL2_t364136731_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002c;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(SphericalHarmonicsL2_t364136731 *)((SphericalHarmonicsL2_t364136731 *)UnBox(L_1, SphericalHarmonicsL2_t364136731_il2cpp_TypeInfo_var))));
		SphericalHarmonicsL2_t364136731  L_2 = V_1;
		bool L_3 = SphericalHarmonicsL2_op_Equality_m3825067444(NULL /*static, unused*/, (*(SphericalHarmonicsL2_t364136731 *)__this), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool SphericalHarmonicsL2_Equals_m3577919107_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	SphericalHarmonicsL2_t364136731 * _thisAdjusted = reinterpret_cast<SphericalHarmonicsL2_t364136731 *>(__this + 1);
	return SphericalHarmonicsL2_Equals_m3577919107(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  bool SphericalHarmonicsL2_op_Equality_m3825067444 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t364136731  ___lhs0, SphericalHarmonicsL2_t364136731  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B28_0 = 0;
	{
		float L_0 = (&___lhs0)->get_shr0_0();
		float L_1 = (&___rhs1)->get_shr0_0();
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_0201;
		}
	}
	{
		float L_2 = (&___lhs0)->get_shr1_1();
		float L_3 = (&___rhs1)->get_shr1_1();
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0201;
		}
	}
	{
		float L_4 = (&___lhs0)->get_shr2_2();
		float L_5 = (&___rhs1)->get_shr2_2();
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_0201;
		}
	}
	{
		float L_6 = (&___lhs0)->get_shr3_3();
		float L_7 = (&___rhs1)->get_shr3_3();
		if ((!(((float)L_6) == ((float)L_7))))
		{
			goto IL_0201;
		}
	}
	{
		float L_8 = (&___lhs0)->get_shr4_4();
		float L_9 = (&___rhs1)->get_shr4_4();
		if ((!(((float)L_8) == ((float)L_9))))
		{
			goto IL_0201;
		}
	}
	{
		float L_10 = (&___lhs0)->get_shr5_5();
		float L_11 = (&___rhs1)->get_shr5_5();
		if ((!(((float)L_10) == ((float)L_11))))
		{
			goto IL_0201;
		}
	}
	{
		float L_12 = (&___lhs0)->get_shr6_6();
		float L_13 = (&___rhs1)->get_shr6_6();
		if ((!(((float)L_12) == ((float)L_13))))
		{
			goto IL_0201;
		}
	}
	{
		float L_14 = (&___lhs0)->get_shr7_7();
		float L_15 = (&___rhs1)->get_shr7_7();
		if ((!(((float)L_14) == ((float)L_15))))
		{
			goto IL_0201;
		}
	}
	{
		float L_16 = (&___lhs0)->get_shr8_8();
		float L_17 = (&___rhs1)->get_shr8_8();
		if ((!(((float)L_16) == ((float)L_17))))
		{
			goto IL_0201;
		}
	}
	{
		float L_18 = (&___lhs0)->get_shg0_9();
		float L_19 = (&___rhs1)->get_shg0_9();
		if ((!(((float)L_18) == ((float)L_19))))
		{
			goto IL_0201;
		}
	}
	{
		float L_20 = (&___lhs0)->get_shg1_10();
		float L_21 = (&___rhs1)->get_shg1_10();
		if ((!(((float)L_20) == ((float)L_21))))
		{
			goto IL_0201;
		}
	}
	{
		float L_22 = (&___lhs0)->get_shg2_11();
		float L_23 = (&___rhs1)->get_shg2_11();
		if ((!(((float)L_22) == ((float)L_23))))
		{
			goto IL_0201;
		}
	}
	{
		float L_24 = (&___lhs0)->get_shg3_12();
		float L_25 = (&___rhs1)->get_shg3_12();
		if ((!(((float)L_24) == ((float)L_25))))
		{
			goto IL_0201;
		}
	}
	{
		float L_26 = (&___lhs0)->get_shg4_13();
		float L_27 = (&___rhs1)->get_shg4_13();
		if ((!(((float)L_26) == ((float)L_27))))
		{
			goto IL_0201;
		}
	}
	{
		float L_28 = (&___lhs0)->get_shg5_14();
		float L_29 = (&___rhs1)->get_shg5_14();
		if ((!(((float)L_28) == ((float)L_29))))
		{
			goto IL_0201;
		}
	}
	{
		float L_30 = (&___lhs0)->get_shg6_15();
		float L_31 = (&___rhs1)->get_shg6_15();
		if ((!(((float)L_30) == ((float)L_31))))
		{
			goto IL_0201;
		}
	}
	{
		float L_32 = (&___lhs0)->get_shg7_16();
		float L_33 = (&___rhs1)->get_shg7_16();
		if ((!(((float)L_32) == ((float)L_33))))
		{
			goto IL_0201;
		}
	}
	{
		float L_34 = (&___lhs0)->get_shg8_17();
		float L_35 = (&___rhs1)->get_shg8_17();
		if ((!(((float)L_34) == ((float)L_35))))
		{
			goto IL_0201;
		}
	}
	{
		float L_36 = (&___lhs0)->get_shb0_18();
		float L_37 = (&___rhs1)->get_shb0_18();
		if ((!(((float)L_36) == ((float)L_37))))
		{
			goto IL_0201;
		}
	}
	{
		float L_38 = (&___lhs0)->get_shb1_19();
		float L_39 = (&___rhs1)->get_shb1_19();
		if ((!(((float)L_38) == ((float)L_39))))
		{
			goto IL_0201;
		}
	}
	{
		float L_40 = (&___lhs0)->get_shb2_20();
		float L_41 = (&___rhs1)->get_shb2_20();
		if ((!(((float)L_40) == ((float)L_41))))
		{
			goto IL_0201;
		}
	}
	{
		float L_42 = (&___lhs0)->get_shb3_21();
		float L_43 = (&___rhs1)->get_shb3_21();
		if ((!(((float)L_42) == ((float)L_43))))
		{
			goto IL_0201;
		}
	}
	{
		float L_44 = (&___lhs0)->get_shb4_22();
		float L_45 = (&___rhs1)->get_shb4_22();
		if ((!(((float)L_44) == ((float)L_45))))
		{
			goto IL_0201;
		}
	}
	{
		float L_46 = (&___lhs0)->get_shb5_23();
		float L_47 = (&___rhs1)->get_shb5_23();
		if ((!(((float)L_46) == ((float)L_47))))
		{
			goto IL_0201;
		}
	}
	{
		float L_48 = (&___lhs0)->get_shb6_24();
		float L_49 = (&___rhs1)->get_shb6_24();
		if ((!(((float)L_48) == ((float)L_49))))
		{
			goto IL_0201;
		}
	}
	{
		float L_50 = (&___lhs0)->get_shb7_25();
		float L_51 = (&___rhs1)->get_shb7_25();
		if ((!(((float)L_50) == ((float)L_51))))
		{
			goto IL_0201;
		}
	}
	{
		float L_52 = (&___lhs0)->get_shb8_26();
		float L_53 = (&___rhs1)->get_shb8_26();
		G_B28_0 = ((((float)L_52) == ((float)L_53))? 1 : 0);
		goto IL_0202;
	}

IL_0201:
	{
		G_B28_0 = 0;
	}

IL_0202:
	{
		V_0 = (bool)G_B28_0;
		goto IL_0208;
	}

IL_0208:
	{
		bool L_54 = V_0;
		return L_54;
	}
}
// System.Boolean UnityEngine.RenderSettings::get_fog()
extern "C"  bool RenderSettings_get_fog_m2930557241 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*RenderSettings_get_fog_m2930557241_ftn) ();
	static RenderSettings_get_fog_m2930557241_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fog_m2930557241_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fog()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
extern "C"  void RenderSettings_set_fog_m4141163796 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fog_m4141163796_ftn) (bool);
	static RenderSettings_set_fog_m4141163796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fog_m4141163796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fog(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
extern "C"  int32_t RenderSettings_get_fogMode_m2733924375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_fogMode_m2733924375_ftn) ();
	static RenderSettings_get_fogMode_m2733924375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogMode_m2733924375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogMode()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogMode(UnityEngine.FogMode)
extern "C"  void RenderSettings_set_fogMode_m2268994450 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogMode_m2268994450_ftn) (int32_t);
	static RenderSettings_set_fogMode_m2268994450_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogMode_m2268994450_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogMode(UnityEngine.FogMode)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_fogColor()
extern "C"  Color_t2020392075  RenderSettings_get_fogColor_m2527731577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderSettings_INTERNAL_get_fogColor_m1991092082(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Color_t2020392075  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_fogColor_m3716873294 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_fogColor_m2591134422(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_fogColor_m1991092082 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_fogColor_m1991092082_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_get_fogColor_m1991092082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_fogColor_m1991092082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_fogColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2591134422 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_fogColor_m2591134422_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_set_fogColor_m2591134422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_fogColor_m2591134422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_fogDensity()
extern "C"  float RenderSettings_get_fogDensity_m943703443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogDensity_m943703443_ftn) ();
	static RenderSettings_get_fogDensity_m943703443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogDensity_m943703443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogDensity()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
extern "C"  void RenderSettings_set_fogDensity_m2940444706 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogDensity_m2940444706_ftn) (float);
	static RenderSettings_set_fogDensity_m2940444706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogDensity_m2940444706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogDensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_fogStartDistance()
extern "C"  float RenderSettings_get_fogStartDistance_m1708962516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogStartDistance_m1708962516_ftn) ();
	static RenderSettings_get_fogStartDistance_m1708962516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogStartDistance_m1708962516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogStartDistance()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogStartDistance(System.Single)
extern "C"  void RenderSettings_set_fogStartDistance_m1252269807 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogStartDistance_m1252269807_ftn) (float);
	static RenderSettings_set_fogStartDistance_m1252269807_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogStartDistance_m1252269807_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogStartDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_fogEndDistance()
extern "C"  float RenderSettings_get_fogEndDistance_m110551859 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_fogEndDistance_m110551859_ftn) ();
	static RenderSettings_get_fogEndDistance_m110551859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_fogEndDistance_m110551859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_fogEndDistance()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_fogEndDistance(System.Single)
extern "C"  void RenderSettings_set_fogEndDistance_m2228540048 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogEndDistance_m2228540048_ftn) (float);
	static RenderSettings_set_fogEndDistance_m2228540048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogEndDistance_m2228540048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogEndDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Rendering.AmbientMode UnityEngine.RenderSettings::get_ambientMode()
extern "C"  int32_t RenderSettings_get_ambientMode_m2092596895 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*RenderSettings_get_ambientMode_m2092596895_ftn) ();
	static RenderSettings_get_ambientMode_m2092596895_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_ambientMode_m2092596895_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_ambientMode()");
	return _il2cpp_icall_func();
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientSkyColor()
extern "C"  Color_t2020392075  RenderSettings_get_ambientSkyColor_m1730334944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderSettings_INTERNAL_get_ambientSkyColor_m960408383(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Color_t2020392075  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientSkyColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientSkyColor_m1960540137 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientSkyColor_m4004022691(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientSkyColor_m960408383 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientSkyColor_m960408383_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_get_ambientSkyColor_m960408383_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientSkyColor_m960408383_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientSkyColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientSkyColor_m4004022691 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientSkyColor_m4004022691_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_set_ambientSkyColor_m4004022691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientSkyColor_m4004022691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientSkyColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientEquatorColor()
extern "C"  Color_t2020392075  RenderSettings_get_ambientEquatorColor_m1885641776 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderSettings_INTERNAL_get_ambientEquatorColor_m3815109999(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Color_t2020392075  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientEquatorColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientEquatorColor_m342085257 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientEquatorColor_m2094865419(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientEquatorColor_m3815109999 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientEquatorColor_m3815109999_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_get_ambientEquatorColor_m3815109999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientEquatorColor_m3815109999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientEquatorColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientEquatorColor_m2094865419 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientEquatorColor_m2094865419_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_set_ambientEquatorColor_m2094865419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientEquatorColor_m2094865419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientEquatorColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientGroundColor()
extern "C"  Color_t2020392075  RenderSettings_get_ambientGroundColor_m1050717572 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderSettings_INTERNAL_get_ambientGroundColor_m1874210147(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Color_t2020392075  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderSettings::set_ambientGroundColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientGroundColor_m582373529 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientGroundColor_m3148534439(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientGroundColor_m1874210147 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientGroundColor_m1874210147_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_get_ambientGroundColor_m1874210147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientGroundColor_m1874210147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientGroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientGroundColor_m3148534439 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientGroundColor_m3148534439_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_set_ambientGroundColor_m3148534439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientGroundColor_m3148534439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientGroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Color UnityEngine.RenderSettings::get_ambientLight()
extern "C"  Color_t2020392075  RenderSettings_get_ambientLight_m134356022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderSettings_INTERNAL_get_ambientLight_m481583081(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Color_t2020392075  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_get_ambientLight_m481583081 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientLight_m481583081_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_get_ambientLight_m481583081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientLight_m481583081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientLight(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.RenderSettings::get_ambientIntensity()
extern "C"  float RenderSettings_get_ambientIntensity_m3199972154 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*RenderSettings_get_ambientIntensity_m3199972154_ftn) ();
	static RenderSettings_get_ambientIntensity_m3199972154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_ambientIntensity_m3199972154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_ambientIntensity()");
	return _il2cpp_icall_func();
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.RenderSettings::get_ambientProbe()
extern "C"  SphericalHarmonicsL2_t364136731  RenderSettings_get_ambientProbe_m1552962884 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	SphericalHarmonicsL2_t364136731  V_0;
	memset(&V_0, 0, sizeof(V_0));
	SphericalHarmonicsL2_t364136731  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderSettings_INTERNAL_get_ambientProbe_m1343118615(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		SphericalHarmonicsL2_t364136731  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		SphericalHarmonicsL2_t364136731  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_get_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C"  void RenderSettings_INTERNAL_get_ambientProbe_m1343118615 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t364136731 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_get_ambientProbe_m1343118615_ftn) (SphericalHarmonicsL2_t364136731 *);
	static RenderSettings_INTERNAL_get_ambientProbe_m1343118615_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_get_ambientProbe_m1343118615_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_get_ambientProbe(UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.Material UnityEngine.RenderSettings::get_skybox()
extern "C"  Material_t193706927 * RenderSettings_get_skybox_m654921356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t193706927 * (*RenderSettings_get_skybox_m654921356_ftn) ();
	static RenderSettings_get_skybox_m654921356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_get_skybox_m654921356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::get_skybox()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m2565496707 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_skybox_m2565496707_ftn) (Material_t193706927 *);
	static RenderSettings_set_skybox_m2565496707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_skybox_m2565496707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture__ctor_m2960228168 (RenderTexture_t2666733923 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, const MethodInfo* method)
{
	{
		Texture__ctor_m4198984292(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1679754911(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.Texture::set_width(System.Int32) */, __this, L_0);
		int32_t L_1 = ___height1;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UnityEngine.Texture::set_height(System.Int32) */, __this, L_1);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m3365909665(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTexture_set_format_m2928390123(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = QualitySettings_get_activeColorSpace_m580875098(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m286621198(NULL /*static, unused*/, __this, (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void RenderTexture__ctor_m4075605457 (RenderTexture_t2666733923 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, const MethodInfo* method)
{
	{
		Texture__ctor_m4198984292(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m1679754911(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.Texture::set_width(System.Int32) */, __this, L_0);
		int32_t L_1 = ___height1;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UnityEngine.Texture::set_height(System.Int32) */, __this, L_1);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m3365909665(__this, L_2, /*hidden argument*/NULL);
		RenderTexture_set_format_m2928390123(__this, 7, /*hidden argument*/NULL);
		int32_t L_3 = QualitySettings_get_activeColorSpace_m580875098(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m286621198(NULL /*static, unused*/, __this, (bool)((((int32_t)L_3) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m1679754911 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___rt0, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m1679754911_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_CreateRenderTexture_m1679754911_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m1679754911_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m1927959284 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, const MethodInfo* method)
{
	typedef RenderTexture_t2666733923 * (*RenderTexture_GetTemporary_m1927959284_ftn) (int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	static RenderTexture_GetTemporary_m1927959284_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetTemporary_m1927959284_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)");
	return _il2cpp_icall_func(___width0, ___height1, ___depthBuffer2, ___format3, ___readWrite4, ___antiAliasing5);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m1800623300 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RenderTexture_t2666733923 * V_2 = NULL;
	{
		V_0 = 1;
		V_1 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = ___format3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t2666733923 * L_6 = RenderTexture_GetTemporary_m1927959284(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_0016;
	}

IL_0016:
	{
		RenderTexture_t2666733923 * L_7 = V_2;
		return L_7;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m1924862769 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RenderTexture_t2666733923 * V_3 = NULL;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t2666733923 * L_6 = RenderTexture_GetTemporary_m1927959284(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_0018;
	}

IL_0018:
	{
		RenderTexture_t2666733923 * L_7 = V_3;
		return L_7;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32)
extern "C"  RenderTexture_t2666733923 * RenderTexture_GetTemporary_m2226167488 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t2666733923 * V_4 = NULL;
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		V_3 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = V_3;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t2666733923 * L_6 = RenderTexture_GetTemporary_m1927959284(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		goto IL_001b;
	}

IL_001b:
	{
		RenderTexture_t2666733923 * L_7 = V_4;
		return L_7;
	}
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m1186631014 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___temp0, const MethodInfo* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m1186631014_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_ReleaseTemporary_m1186631014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m1186631014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m2317917654 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m2317917654_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_GetWidth_m2317917654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m2317917654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m989760279 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m989760279_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_Internal_SetWidth_m989760279_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m989760279_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m2780941261 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m2780941261_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_GetHeight_m2780941261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m2780941261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m2900429544 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m2900429544_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_Internal_SetHeight_m2900429544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m2900429544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m286621198 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, bool ___sRGB1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m286621198_ftn) (RenderTexture_t2666733923 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m286621198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m286621198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___sRGB1);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m1471807677 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m2317917654(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C"  void RenderTexture_set_width_m592588466 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetWidth_m989760279(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m1108175848 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m2780941261(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C"  void RenderTexture_set_height_m1787187 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetHeight_m2900429544(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m3365909665 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m3365909665_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_set_depth_m3365909665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m3365909665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)
extern "C"  void RenderTexture_set_isPowerOfTwo_m3073203624 (RenderTexture_t2666733923 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_isPowerOfTwo_m3073203624_ftn) (RenderTexture_t2666733923 *, bool);
	static RenderTexture_set_isPowerOfTwo_m3073203624_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_isPowerOfTwo_m3073203624_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
extern "C"  int32_t RenderTexture_get_format_m2563201870 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_get_format_m2563201870_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_get_format_m2563201870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_format_m2563201870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_format()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m2928390123 (RenderTexture_t2666733923 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m2928390123_ftn) (RenderTexture_t2666733923 *, int32_t);
	static RenderTexture_set_format_m2928390123_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m2928390123_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::DiscardContents()
extern "C"  void RenderTexture_DiscardContents_m748499988 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_DiscardContents_m4035611219_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::MarkRestoreExpected()
extern "C"  void RenderTexture_MarkRestoreExpected_m2952280671 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_MarkRestoreExpected_m181526572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_MarkRestoreExpected(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern "C"  RenderBuffer_t2767087968  RenderTexture_get_colorBuffer_m2259248236 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	RenderBuffer_t2767087968  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderBuffer_t2767087968  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderTexture_GetColorBuffer_m2909232564(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t2767087968  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		RenderBuffer_t2767087968  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern "C"  RenderBuffer_t2767087968  RenderTexture_get_depthBuffer_m583581910 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	RenderBuffer_t2767087968  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderBuffer_t2767087968  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderTexture_GetDepthBuffer_m1144066090(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t2767087968  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		RenderBuffer_t2767087968  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetColorBuffer_m2909232564 (RenderTexture_t2666733923 * __this, RenderBuffer_t2767087968 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetColorBuffer_m2909232564_ftn) (RenderTexture_t2666733923 *, RenderBuffer_t2767087968 *);
	static RenderTexture_GetColorBuffer_m2909232564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetColorBuffer_m2909232564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetDepthBuffer_m1144066090 (RenderTexture_t2666733923 * __this, RenderBuffer_t2767087968 * ___res0, const MethodInfo* method)
{
	typedef void (*RenderTexture_GetDepthBuffer_m1144066090_ftn) (RenderTexture_t2666733923 *, RenderBuffer_t2767087968 *);
	static RenderTexture_GetDepthBuffer_m1144066090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetDepthBuffer_m1144066090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_set_active_m55464043 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_active_m55464043_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_set_active_m55464043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m55464043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m3475141952 (RequireComponent_t864575032 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke(const ResourceRequest_t2560315377& unmarshaled, ResourceRequest_t2560315377_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_back(const ResourceRequest_t2560315377_marshaled_pinvoke& marshaled, ResourceRequest_t2560315377& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_cleanup(ResourceRequest_t2560315377_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_com(const ResourceRequest_t2560315377& unmarshaled, ResourceRequest_t2560315377_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t2560315377_marshal_com_back(const ResourceRequest_t2560315377_marshaled_com& marshaled, ResourceRequest_t2560315377& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_com_cleanup(ResourceRequest_t2560315377_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m3340010930 (ResourceRequest_t2560315377 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2914860946(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t1021602117 * ResourceRequest_get_asset_m3527928488 (ResourceRequest_t2560315377 * __this, const MethodInfo* method)
{
	Object_t1021602117 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t1021602117 * Resources_Load_m2041782325 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2041782325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Object_t1021602117_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Object_t1021602117 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1021602117 * Resources_Load_m243305716 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Resources_Load_m243305716_ftn) (String_t*, Type_t *);
	static Resources_Load_m243305716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m243305716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsync(System.String,System.Type)
extern "C"  ResourceRequest_t2560315377 * Resources_LoadAsync_m1208818052 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___type1, const MethodInfo* method)
{
	typedef ResourceRequest_t2560315377 * (*Resources_LoadAsync_m1208818052_ftn) (String_t*, Type_t *);
	static Resources_LoadAsync_m1208818052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_LoadAsync_m1208818052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::LoadAsync(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___type1);
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t1021602117 * Resources_GetBuiltinResource_m582410469 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Resources_GetBuiltinResource_m582410469_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m582410469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m582410469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	return _il2cpp_icall_func(___type0, ___path1);
}
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C"  AsyncOperation_t3814632279 * Resources_UnloadUnusedAssets_m2770025609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef AsyncOperation_t3814632279 * (*Resources_UnloadUnusedAssets_m2770025609_ftn) ();
	static Resources_UnloadUnusedAssets_m2770025609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadUnusedAssets_m2770025609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadUnusedAssets()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Rigidbody::.ctor()
extern "C"  void Rigidbody__ctor_m4251865400 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m205306948(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t2243707580  Rigidbody_get_velocity_m2022666970 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_velocity_m1938461825(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m2514070071 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m3384354677(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m1938461825 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m1938461825_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_velocity_m1938461825_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m1938461825_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m3384354677 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m3384354677_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_set_velocity_m3384354677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m3384354677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern "C"  void Rigidbody_set_drag_m431766562 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_drag_m431766562_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_drag_m431766562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_drag_m431766562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
extern "C"  void Rigidbody_set_angularDrag_m1042416512 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_angularDrag_m1042416512_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_angularDrag_m1042416512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_angularDrag_m1042416512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_angularDrag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_mass()
extern "C"  float Rigidbody_get_mass_m2290366311 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_mass_m2290366311_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_get_mass_m2290366311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_mass_m2290366311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern "C"  void Rigidbody_set_mass_m4155402692 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_mass_m4155402692_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_mass_m4155402692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_mass_m4155402692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern "C"  void Rigidbody_set_useGravity_m2606656539 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useGravity_m2606656539_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_useGravity_m2606656539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_m2606656539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody_set_isKinematic_m738793415 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m738793415_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_isKinematic_m738793415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m738793415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m2131864169 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m2131864169_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_freezeRotation_m2131864169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m2131864169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern "C"  void Rigidbody_set_constraints_m3474016139 (Rigidbody_t4233889191 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_constraints_m3474016139_ftn) (Rigidbody_t4233889191 *, int32_t);
	static Rigidbody_set_constraints_m3474016139_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_constraints_m3474016139_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)
extern "C"  void Rigidbody_set_collisionDetectionMode_m314488966 (Rigidbody_t4233889191 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_collisionDetectionMode_m314488966_ftn) (Rigidbody_t4233889191 *, int32_t);
	static Rigidbody_set_collisionDetectionMode_m314488966_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_collisionDetectionMode_m314488966_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m3219459786 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m3164777073(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForce_m2836187433 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m3164777073(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3164777073 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddTorque_m2680584150 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___torque0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddTorque_m929838442(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m929838442 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForceAtPosition_m196637080 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, Vector3_t2243707580  ___position1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, Vector3_t2243707580 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::GetPointVelocity(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Rigidbody_GetPointVelocity_m1454837702 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___worldPoint0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474(NULL /*static, unused*/, __this, (&___worldPoint0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___worldPoint1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_GetPointVelocity_m726787474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_GetPointVelocity(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___worldPoint1, ___value2);
}
// System.Void UnityEngine.Rigidbody::set_centerOfMass(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_centerOfMass_m2199195688 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_centerOfMass_m4044734456(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_centerOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_centerOfMass_m4044734456 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_centerOfMass_m4044734456_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_set_centerOfMass_m4044734456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_centerOfMass_m4044734456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_centerOfMass(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C"  Vector3_t2243707580  Rigidbody_get_position_m3465583110 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_position_m2227872991(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2243707580  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_position_m2227872991 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_position_m2227872991_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_position_m2227872991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_position_m2227872991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
extern "C"  Quaternion_t4030073918  Rigidbody_get_rotation_m4203325509 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_rotation_m109669412(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_get_rotation_m109669412 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_rotation_m109669412_ftn) (Rigidbody_t4233889191 *, Quaternion_t4030073918 *);
	static Rigidbody_INTERNAL_get_rotation_m109669412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_rotation_m109669412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C"  void Rigidbody_MovePosition_m1810529681 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m2709492198(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m2709492198 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_MoveRotation_m3412525692 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918  ___rot0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919(NULL /*static, unused*/, __this, (&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Quaternion_t4030073918 * ___rot1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn) (Rigidbody_t4233889191 *, Quaternion_t4030073918 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___rot1);
}
// System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
extern "C"  void Rigidbody_set_interpolation_m3914346443 (Rigidbody_t4233889191 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_interpolation_m3914346443_ftn) (Rigidbody_t4233889191 *, int32_t);
	static Rigidbody_set_interpolation_m3914346443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_interpolation_m3914346443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m1555912301 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_get_handle_m1555912301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_get_handle_m1555912301(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3735680091 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Scene_get_handle_m1555912301(__this, /*hidden argument*/NULL);
		int32_t L_1 = Scene_GetBuildIndexInternal_m287561822(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t Scene_get_buildIndex_m3735680091_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_get_buildIndex_m3735680091(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m3223653899 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_GetHashCode_m3223653899_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_GetHashCode_m3223653899(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m3588907349 (Scene_t1684909666 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m3588907349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Scene_t1684909666_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Scene_t1684909666 *)((Scene_t1684909666 *)UnBox(L_1, Scene_t1684909666_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m1555912301(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m1555912301((&V_1), /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Scene_Equals_m3588907349_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_Equals_m3588907349(_thisAdjusted, ___other0, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
extern "C"  int32_t Scene_GetBuildIndexInternal_m287561822 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef int32_t (*Scene_GetBuildIndexInternal_m287561822_ftn) (int32_t);
	static Scene_GetBuildIndexInternal_m287561822_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetBuildIndexInternal_m287561822_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Scene_t1684909666  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Scene_t1684909666  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318 (Il2CppObject * __this /* static, unused */, Scene_t1684909666 * ___value0, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn) (Scene_t1684909666 *);
	static SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m592643733 (Il2CppObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	Il2CppObject * G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	Il2CppObject * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	Il2CppObject * G_B3_2 = NULL;
	{
		int32_t L_0 = ___sceneBuildIndex0;
		int32_t L_1 = ___mode1;
		G_B1_0 = L_0;
		G_B1_1 = NULL;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = L_0;
			G_B2_1 = NULL;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, (String_t*)G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsync_m4130852156 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	AsyncOperation_t3814632279 * V_1 = NULL;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		AsyncOperation_t3814632279 * L_2 = SceneManager_LoadSceneAsync_m2648120039(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		AsyncOperation_t3814632279 * L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsync_m2648120039 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	AsyncOperation_t3814632279 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		AsyncOperation_t3814632279 * L_2 = SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001d;
	}

IL_001d:
	{
		AsyncOperation_t3814632279 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method)
{
	typedef AsyncOperation_t3814632279 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_Internal_SceneLoaded_m4005732915 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___scene0, int32_t ___mode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m4005732915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t1903595547 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t1903595547 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		Scene_t1684909666  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m1528820797(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m1528820797_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_SceneUnloaded_m4108957131 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___scene0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m4108957131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_1_t3051495417 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UnityAction_1_t3051495417 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		Scene_t1684909666  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m3061904506(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m3061904506_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m1162592635 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___previousActiveScene0, Scene_t1684909666  ___newActiveScene1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m1162592635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t606618774 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t606618774 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		Scene_t1684909666  L_2 = ___previousActiveScene0;
		Scene_t1684909666  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m670567184(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m670567184_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m41137238_ftn) ();
	static Screen_get_width_m41137238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m41137238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1051800773_ftn) ();
	static Screen_get_height_m1051800773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1051800773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m3345126327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m3345126327_ftn) ();
	static Screen_get_dpi_m3345126327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m3345126327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke(const ScriptableObject_t1975622470& unmarshaled, ScriptableObject_t1975622470_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_back(const ScriptableObject_t1975622470_marshaled_pinvoke& marshaled, ScriptableObject_t1975622470& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_cleanup(ScriptableObject_t1975622470_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_com(const ScriptableObject_t1975622470& unmarshaled, ScriptableObject_t1975622470_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t1975622470_marshal_com_back(const ScriptableObject_t1975622470_marshaled_com& marshaled, ScriptableObject_t1975622470& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_com_cleanup(ScriptableObject_t1975622470_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m2671490429 (ScriptableObject_t1975622470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m2671490429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m1778903390(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m1778903390 (Il2CppObject * __this /* static, unused */, ScriptableObject_t1975622470 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn) (ScriptableObject_t1975622470 *);
	static ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstance_m3921674852 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t1975622470 * (*ScriptableObject_CreateInstance_m3921674852_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m3921674852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m3921674852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstance_m3271154163 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	ScriptableObject_t1975622470 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t1975622470 * L_1 = ScriptableObject_CreateInstanceFromType_m4271875689(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		ScriptableObject_t1975622470 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstanceFromType_m4271875689 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t1975622470 * (*ScriptableObject_CreateInstanceFromType_m4271875689_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m4271875689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m4271875689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String)
extern "C"  void MovedFromAttribute__ctor_m4050085439 (MovedFromAttribute_t922195725 * __this, String_t* ___sourceNamespace0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___sourceNamespace0;
		MovedFromAttribute__ctor_m1535041828(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String,System.Boolean)
extern "C"  void MovedFromAttribute__ctor_m1535041828 (MovedFromAttribute_t922195725 * __this, String_t* ___sourceNamespace0, bool ___isInDifferentAssembly1, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sourceNamespace0;
		MovedFromAttribute_set_Namespace_m1786632593(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___isInDifferentAssembly1;
		MovedFromAttribute_set_IsInDifferentAssembly_m4026643849(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_Namespace(System.String)
extern "C"  void MovedFromAttribute_set_Namespace_m1786632593 (MovedFromAttribute_t922195725 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNamespaceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_IsInDifferentAssembly(System.Boolean)
extern "C"  void MovedFromAttribute_set_IsInDifferentAssembly_m4026643849 (MovedFromAttribute_t922195725 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsInDifferentAssemblyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute::.ctor()
extern "C"  void GeneratedByOldBindingsGeneratorAttribute__ctor_m3745565094 (GeneratedByOldBindingsGeneratorAttribute_t107439586 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m2437378488 (PreserveAttribute_t4182602970 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m2374853658 (RequiredByNativeCodeAttribute_t1913052472 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m2459832290 (UsedByNativeCodeAttribute_t3212052468 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
