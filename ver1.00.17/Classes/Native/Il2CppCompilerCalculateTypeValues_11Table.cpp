﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2866209745.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3672778802.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1703410334.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1957337331.h"
#include "System_Core_U3CModuleU3E3783534214.h"
#include "System_Core_System_Runtime_CompilerServices_Extens1840441203.h"
#include "System_Core_Locale4255929014.h"
#include "System_Core_System_MonoTODOAttribute3487514019.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder3965881084.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr1394030013.h"
#include "System_Core_System_Linq_Check578192424.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Linq_Enumerable_Fallback2408918324.h"
#include "System_Core_System_Linq_SortDirection759359329.h"
#include "System_Core_System_Security_Cryptography_Aes2354947465.h"
#include "System_Core_System_Security_Cryptography_AesManage3721278648.h"
#include "System_Core_System_Security_Cryptography_AesTransf3733702461.h"
#include "System_Core_System_Action3226471752.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242844921915.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A116038562.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242038352954.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U242672183894.h"
#include "System_U3CModuleU3E3783534214.h"
#include "System_Locale4255929014.h"
#include "System_System_MonoTODOAttribute3487514019.h"
#include "System_System_MonoNotSupportedAttribute2197455930.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120.h"
#include "System_System_ComponentModel_Design_DesignerTransa3880697857.h"
#include "System_System_ComponentModel_Design_RuntimeLicense1397748562.h"
#include "System_System_ComponentModel_ArrayConverter2804512129.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayPr599180064.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938.h"
#include "System_System_ComponentModel_AsyncOperation1185541675.h"
#include "System_System_ComponentModel_AsyncOperationManager3553158318.h"
#include "System_System_ComponentModel_AttributeCollection1925812292.h"
#include "System_System_ComponentModel_BackgroundWorker4230068110.h"
#include "System_System_ComponentModel_BackgroundWorker_Proce308732489.h"
#include "System_System_ComponentModel_BaseNumberConverter1130358776.h"
#include "System_System_ComponentModel_BindableSupport1735231582.h"
#include "System_System_ComponentModel_BooleanConverter284715810.h"
#include "System_System_ComponentModel_BrowsableAttribute2487167291.h"
#include "System_System_ComponentModel_ByteConverter1265255600.h"
#include "System_System_ComponentModel_CancelEventArgs1976499267.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (U24ArrayTypeU2464_t2866209746)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2866209746 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (U24ArrayTypeU2412_t3672778803)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778803 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (U24ArrayTypeU2416_t1703410335)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410335 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (U24ArrayTypeU244_t1957337331)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t1957337331 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (U3CModuleU3E_t3783534216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { sizeof (ExtensionAttribute_t1840441203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { sizeof (Locale_t4255929016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (MonoTODOAttribute_t3487514020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (KeyBuilder_t3965881086), -1, sizeof(KeyBuilder_t3965881086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1108[1] = 
{
	KeyBuilder_t3965881086_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { sizeof (SymmetricTransform_t1394030014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1109[12] = 
{
	SymmetricTransform_t1394030014::get_offset_of_algo_0(),
	SymmetricTransform_t1394030014::get_offset_of_encrypt_1(),
	SymmetricTransform_t1394030014::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t1394030014::get_offset_of_temp_3(),
	SymmetricTransform_t1394030014::get_offset_of_temp2_4(),
	SymmetricTransform_t1394030014::get_offset_of_workBuff_5(),
	SymmetricTransform_t1394030014::get_offset_of_workout_6(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t1394030014::get_offset_of_m_disposed_9(),
	SymmetricTransform_t1394030014::get_offset_of_lastBlock_10(),
	SymmetricTransform_t1394030014::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1110[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1111[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1112[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1113[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (Check_t578192424), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (Enumerable_t2148412300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (Fallback_t2408918324)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1116[3] = 
{
	Fallback_t2408918324::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1117[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1118[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1119[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1121[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1122[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1123[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1124[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1125[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (SortDirection_t759359329)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1126[3] = 
{
	SortDirection_t759359329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1127[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (Aes_t2354947465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (AesManaged_t3721278648), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (AesTransform_t3733702461), -1, sizeof(AesTransform_t3733702461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1130[14] = 
{
	AesTransform_t3733702461::get_offset_of_expandedKey_12(),
	AesTransform_t3733702461::get_offset_of_Nk_13(),
	AesTransform_t3733702461::get_offset_of_Nr_14(),
	AesTransform_t3733702461_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t3733702461_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T0_18(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T1_19(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T2_20(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T3_21(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (Action_t3226471752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305139), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1137[12] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (U24ArrayTypeU24136_t2844921916)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t2844921916 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { sizeof (U24ArrayTypeU24120_t116038563)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t116038563 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { sizeof (U24ArrayTypeU24256_t2038352956)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352956 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { sizeof (U24ArrayTypeU241024_t2672183895)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t2672183895 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (U3CModuleU3E_t3783534217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (Locale_t4255929017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (MonoTODOAttribute_t3487514021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1144[1] = 
{
	MonoTODOAttribute_t3487514021::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (MonoNotSupportedAttribute_t2197455930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1146[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1147[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1148[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1149[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1150[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1151[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1152[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1153[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1154[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1155[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1156[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1157[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1158[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1159[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1160[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1161[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1162[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1163[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { sizeof (HybridDictionary_t290043810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1164[3] = 
{
	HybridDictionary_t290043810::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t290043810::get_offset_of_hashtable_1(),
	HybridDictionary_t290043810::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1165[4] = 
{
	ListDictionary_t3458713452::get_offset_of_count_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_head_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1166[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (DictionaryNodeEnumerator_t1923170152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1167[4] = 
{
	DictionaryNodeEnumerator_t1923170152::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (DictionaryNodeCollection_t528898270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1168[2] = 
{
	DictionaryNodeCollection_t528898270::get_offset_of_dict_0(),
	DictionaryNodeCollection_t528898270::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (DictionaryNodeCollectionEnumerator_t2037848305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1169[2] = 
{
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (NameObjectCollectionBase_t2034248631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1170[10] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2034248631::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (_Item_t3244489099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1171[2] = 
{
	_Item_t3244489099::get_offset_of_key_0(),
	_Item_t3244489099::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (_KeysEnumerator_t1718269396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1172[2] = 
{
	_KeysEnumerator_t1718269396::get_offset_of_m_collection_0(),
	_KeysEnumerator_t1718269396::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1173[1] = 
{
	KeysCollection_t633582367::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1174[2] = 
{
	NameValueCollection_t3047564564::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t3047564564::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (InstanceDescriptor_t1404033120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1175[3] = 
{
	InstanceDescriptor_t1404033120::get_offset_of_member_0(),
	InstanceDescriptor_t1404033120::get_offset_of_arguments_1(),
	InstanceDescriptor_t1404033120::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (DesignerTransaction_t3880697857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1176[2] = 
{
	DesignerTransaction_t3880697857::get_offset_of_committed_0(),
	DesignerTransaction_t3880697857::get_offset_of_canceled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { sizeof (RuntimeLicenseContext_t1397748562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1185[2] = 
{
	RuntimeLicenseContext_t1397748562::get_offset_of_extraassemblies_0(),
	RuntimeLicenseContext_t1397748562::get_offset_of_keys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { sizeof (ArrayConverter_t2804512129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { sizeof (ArrayPropertyDescriptor_t599180064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1187[2] = 
{
	ArrayPropertyDescriptor_t599180064::get_offset_of_index_6(),
	ArrayPropertyDescriptor_t599180064::get_offset_of_array_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (AsyncCompletedEventArgs_t83270938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1188[3] = 
{
	AsyncCompletedEventArgs_t83270938::get_offset_of__error_1(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__cancelled_2(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__userState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (AsyncOperation_t1185541675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1189[3] = 
{
	AsyncOperation_t1185541675::get_offset_of_ctx_0(),
	AsyncOperation_t1185541675::get_offset_of_state_1(),
	AsyncOperation_t1185541675::get_offset_of_done_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (AsyncOperationManager_t3553158318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (AttributeCollection_t1925812292), -1, sizeof(AttributeCollection_t1925812292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1191[2] = 
{
	AttributeCollection_t1925812292::get_offset_of_attrList_0(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (BackgroundWorker_t4230068110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1192[7] = 
{
	BackgroundWorker_t4230068110::get_offset_of_async_0(),
	BackgroundWorker_t4230068110::get_offset_of_cancel_pending_1(),
	BackgroundWorker_t4230068110::get_offset_of_report_progress_2(),
	BackgroundWorker_t4230068110::get_offset_of_support_cancel_3(),
	BackgroundWorker_t4230068110::get_offset_of_DoWork_4(),
	BackgroundWorker_t4230068110::get_offset_of_ProgressChanged_5(),
	BackgroundWorker_t4230068110::get_offset_of_RunWorkerCompleted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (ProcessWorkerEventHandler_t308732489), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (BaseNumberConverter_t1130358776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1194[1] = 
{
	BaseNumberConverter_t1130358776::get_offset_of_InnerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (BindableSupport_t1735231582)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1195[4] = 
{
	BindableSupport_t1735231582::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (BooleanConverter_t284715810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { sizeof (BrowsableAttribute_t2487167291), -1, sizeof(BrowsableAttribute_t2487167291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1197[4] = 
{
	BrowsableAttribute_t2487167291::get_offset_of_browsable_0(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Default_1(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_No_2(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { sizeof (ByteConverter_t1265255600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { sizeof (CancelEventArgs_t1976499267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1199[1] = 
{
	CancelEventArgs_t1976499267::get_offset_of_cancel_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
