﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_CategoryAttribute540457070.h"
#include "System_System_ComponentModel_CharConverter437233350.h"
#include "System_System_ComponentModel_CollectionChangeActio3495844100.h"
#include "System_System_ComponentModel_CollectionChangeEvent1734749345.h"
#include "System_System_ComponentModel_CollectionConverter2459375096.h"
#include "System_System_ComponentModel_Component2826673791.h"
#include "System_System_ComponentModel_ComponentCollection737017907.h"
#include "System_System_ComponentModel_ComponentConverter3121608223.h"
#include "System_System_ComponentModel_CultureInfoConverter2239982248.h"
#include "System_System_ComponentModel_CultureInfoConverter_2714931249.h"
#include "System_System_ComponentModel_CustomTypeDescriptor1720788626.h"
#include "System_System_ComponentModel_DateTimeConverter2436647419.h"
#include "System_System_ComponentModel_DecimalConverter1618403211.h"
#include "System_System_ComponentModel_DefaultEventAttribute1079704873.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib1962767338.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DescriptionAttribute3207779672.h"
#include "System_System_ComponentModel_DesignOnlyAttribute2394309572.h"
#include "System_System_ComponentModel_DesignTimeVisibleAttr2120749151.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1270090451.h"
#include "System_System_ComponentModel_DesignerSerialization3751360903.h"
#include "System_System_ComponentModel_DesignerSerialization2980019899.h"
#include "System_System_ComponentModel_DisplayNameAttribute1935407093.h"
#include "System_System_ComponentModel_DoWorkEventArgs62745097.h"
#include "System_System_ComponentModel_DoubleConverter864652623.h"
#include "System_System_ComponentModel_EditorAttribute3559776959.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655.h"
#include "System_System_ComponentModel_EnumConverter2538808523.h"
#include "System_System_ComponentModel_EnumConverter_EnumCom2635778853.h"
#include "System_System_ComponentModel_EventDescriptor962731901.h"
#include "System_System_ComponentModel_EventDescriptorCollec3053042509.h"
#include "System_System_ComponentModel_ListEntry935815304.h"
#include "System_System_ComponentModel_EventHandlerList1298116880.h"
#include "System_System_ComponentModel_ExpandableObjectConve1139197709.h"
#include "System_System_ComponentModel_GuidConverter1547586607.h"
#include "System_System_ComponentModel_ImmutableObjectAttrib1990201979.h"
#include "System_System_ComponentModel_Int16Converter903627590.h"
#include "System_System_ComponentModel_Int32Converter957938388.h"
#include "System_System_ComponentModel_Int64Converter3186343659.h"
#include "System_System_ComponentModel_InvalidEnumArgumentEx3709744516.h"
#include "System_System_ComponentModel_LicFileLicenseProvide2962157156.h"
#include "System_System_ComponentModel_LicFileLicense1222627691.h"
#include "System_System_ComponentModel_License3326651051.h"
#include "System_System_ComponentModel_LicenseContext192650050.h"
#include "System_System_ComponentModel_LicenseException1281499302.h"
#include "System_System_ComponentModel_LicenseManager764408642.h"
#include "System_System_ComponentModel_LicenseProvider3536803528.h"
#include "System_System_ComponentModel_LicenseProviderAttrib1207066332.h"
#include "System_System_ComponentModel_LicenseUsageMode2996119499.h"
#include "System_System_ComponentModel_ListBindableAttribute1092011273.h"
#include "System_System_ComponentModel_ListChangedEventArgs3132270315.h"
#include "System_System_ComponentModel_ListChangedType3463990274.h"
#include "System_System_ComponentModel_ListSortDescription3194554012.h"
#include "System_System_ComponentModel_ListSortDescriptionCo2357028590.h"
#include "System_System_ComponentModel_ListSortDirection4186912589.h"
#include "System_System_ComponentModel_LocalizableAttribute1267247394.h"
#include "System_System_ComponentModel_MarshalByValueCompone3997823175.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553.h"
#include "System_System_ComponentModel_MemberDescriptor_Membe856477673.h"
#include "System_System_ComponentModel_MergablePropertyAttri1597820506.h"
#include "System_System_ComponentModel_MultilineStringConver3643519256.h"
#include "System_System_ComponentModel_NotifyParentPropertyAt801973150.h"
#include "System_System_ComponentModel_NullableConverter1941973167.h"
#include "System_System_ComponentModel_PasswordPropertyTextA2834530705.h"
#include "System_System_ComponentModel_ProgressChangedEventAr711712958.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3166009492.h"
#include "System_System_ComponentModel_ReadOnlyAttribute4102148880.h"
#include "System_System_ComponentModel_RecommendedAsConfigura420947846.h"
#include "System_System_ComponentModel_ReferenceConverter3131270729.h"
#include "System_System_ComponentModel_ReflectionEventDescri3740223310.h"
#include "System_System_ComponentModel_ReflectionPropertyDes2570125387.h"
#include "System_System_ComponentModel_RefreshEventArgs2077477224.h"
#include "System_System_ComponentModel_RefreshProperties2240171922.h"
#include "System_System_ComponentModel_RefreshPropertiesAttr2234294918.h"
#include "System_System_ComponentModel_RunWorkerCompletedEve3512304465.h"
#include "System_System_ComponentModel_SByteConverter4003686519.h"
#include "System_System_ComponentModel_SingleConverter3693313828.h"
#include "System_System_ComponentModel_StringConverter3749524419.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (CategoryAttribute_t540457070), -1, sizeof(CategoryAttribute_t540457070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1200[17] = 
{
	CategoryAttribute_t540457070::get_offset_of_category_0(),
	CategoryAttribute_t540457070::get_offset_of_IsLocalized_1(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_action_2(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_appearance_3(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_behaviour_4(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_data_5(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_def_6(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_design_7(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_drag_drop_8(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_focus_9(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_format_10(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_key_11(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_layout_12(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_mouse_13(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_window_style_14(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_async_15(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_lockobj_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (CharConverter_t437233350), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (CollectionChangeAction_t3495844100)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1202[4] = 
{
	CollectionChangeAction_t3495844100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (CollectionChangeEventArgs_t1734749345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1203[2] = 
{
	CollectionChangeEventArgs_t1734749345::get_offset_of_changeAction_1(),
	CollectionChangeEventArgs_t1734749345::get_offset_of_theElement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (CollectionConverter_t2459375096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (Component_t2826673791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1205[3] = 
{
	Component_t2826673791::get_offset_of_event_handlers_1(),
	Component_t2826673791::get_offset_of_mySite_2(),
	Component_t2826673791::get_offset_of_disposedEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (ComponentCollection_t737017907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (ComponentConverter_t3121608223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (CultureInfoConverter_t2239982248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1208[1] = 
{
	CultureInfoConverter_t2239982248::get_offset_of__standardValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (CultureInfoComparer_t2714931249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (CustomTypeDescriptor_t1720788626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1210[1] = 
{
	CustomTypeDescriptor_t1720788626::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (DateTimeConverter_t2436647419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (DecimalConverter_t1618403211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (DefaultEventAttribute_t1079704873), -1, sizeof(DefaultEventAttribute_t1079704873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1213[2] = 
{
	DefaultEventAttribute_t1079704873::get_offset_of_eventName_0(),
	DefaultEventAttribute_t1079704873_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (DefaultPropertyAttribute_t1962767338), -1, sizeof(DefaultPropertyAttribute_t1962767338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1214[2] = 
{
	DefaultPropertyAttribute_t1962767338::get_offset_of_property_name_0(),
	DefaultPropertyAttribute_t1962767338_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (DefaultValueAttribute_t1302720498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1215[1] = 
{
	DefaultValueAttribute_t1302720498::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (DescriptionAttribute_t3207779672), -1, sizeof(DescriptionAttribute_t3207779672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1216[2] = 
{
	DescriptionAttribute_t3207779672::get_offset_of_desc_0(),
	DescriptionAttribute_t3207779672_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (DesignOnlyAttribute_t2394309572), -1, sizeof(DesignOnlyAttribute_t2394309572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1217[4] = 
{
	DesignOnlyAttribute_t2394309572::get_offset_of_design_only_0(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_Default_1(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_No_2(),
	DesignOnlyAttribute_t2394309572_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (DesignTimeVisibleAttribute_t2120749151), -1, sizeof(DesignTimeVisibleAttribute_t2120749151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1218[4] = 
{
	DesignTimeVisibleAttribute_t2120749151::get_offset_of_visible_0(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_Default_1(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_No_2(),
	DesignTimeVisibleAttribute_t2120749151_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (DesignerAttribute_t2778719479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1219[2] = 
{
	DesignerAttribute_t2778719479::get_offset_of_name_0(),
	DesignerAttribute_t2778719479::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (DesignerCategoryAttribute_t1270090451), -1, sizeof(DesignerCategoryAttribute_t1270090451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1220[5] = 
{
	DesignerCategoryAttribute_t1270090451::get_offset_of_category_0(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Component_1(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Form_2(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Generic_3(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (DesignerSerializationVisibility_t3751360903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1221[4] = 
{
	DesignerSerializationVisibility_t3751360903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (DesignerSerializationVisibilityAttribute_t2980019899), -1, sizeof(DesignerSerializationVisibilityAttribute_t2980019899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1222[5] = 
{
	DesignerSerializationVisibilityAttribute_t2980019899::get_offset_of_visibility_0(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Default_1(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Content_2(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Hidden_3(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (DisplayNameAttribute_t1935407093), -1, sizeof(DisplayNameAttribute_t1935407093_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1223[2] = 
{
	DisplayNameAttribute_t1935407093_StaticFields::get_offset_of_Default_0(),
	DisplayNameAttribute_t1935407093::get_offset_of_attributeDisplayName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (DoWorkEventArgs_t62745097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1224[3] = 
{
	DoWorkEventArgs_t62745097::get_offset_of_arg_1(),
	DoWorkEventArgs_t62745097::get_offset_of_result_2(),
	DoWorkEventArgs_t62745097::get_offset_of_cancel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (DoubleConverter_t864652623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (EditorAttribute_t3559776959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1226[2] = 
{
	EditorAttribute_t3559776959::get_offset_of_name_0(),
	EditorAttribute_t3559776959::get_offset_of_basename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (EditorBrowsableAttribute_t1050682502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1227[1] = 
{
	EditorBrowsableAttribute_t1050682502::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (EditorBrowsableState_t373498655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1228[4] = 
{
	EditorBrowsableState_t373498655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (EnumConverter_t2538808523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1229[2] = 
{
	EnumConverter_t2538808523::get_offset_of_type_0(),
	EnumConverter_t2538808523::get_offset_of_stdValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (EnumComparer_t2635778853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (EventDescriptor_t962731901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (EventDescriptorCollection_t3053042509), -1, sizeof(EventDescriptorCollection_t3053042509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1232[3] = 
{
	EventDescriptorCollection_t3053042509::get_offset_of_eventList_0(),
	EventDescriptorCollection_t3053042509::get_offset_of_isReadOnly_1(),
	EventDescriptorCollection_t3053042509_StaticFields::get_offset_of_Empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (ListEntry_t935815304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1233[3] = 
{
	ListEntry_t935815304::get_offset_of_key_0(),
	ListEntry_t935815304::get_offset_of_value_1(),
	ListEntry_t935815304::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (EventHandlerList_t1298116880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1234[2] = 
{
	EventHandlerList_t1298116880::get_offset_of_entries_0(),
	EventHandlerList_t1298116880::get_offset_of_null_entry_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (ExpandableObjectConverter_t1139197709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (GuidConverter_t1547586607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (ImmutableObjectAttribute_t1990201979), -1, sizeof(ImmutableObjectAttribute_t1990201979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1255[4] = 
{
	ImmutableObjectAttribute_t1990201979::get_offset_of_immutable_0(),
	ImmutableObjectAttribute_t1990201979_StaticFields::get_offset_of_Default_1(),
	ImmutableObjectAttribute_t1990201979_StaticFields::get_offset_of_No_2(),
	ImmutableObjectAttribute_t1990201979_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (Int16Converter_t903627590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (Int32Converter_t957938388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (Int64Converter_t3186343659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (InvalidEnumArgumentException_t3709744516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (LicFileLicenseProvider_t2962157156), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (LicFileLicense_t1222627691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1261[1] = 
{
	LicFileLicense_t1222627691::get_offset_of__key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (License_t3326651051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { sizeof (LicenseContext_t192650050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (LicenseException_t1281499302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1264[1] = 
{
	LicenseException_t1281499302::get_offset_of_type_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (LicenseManager_t764408642), -1, sizeof(LicenseManager_t764408642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1265[3] = 
{
	LicenseManager_t764408642_StaticFields::get_offset_of_mycontext_0(),
	LicenseManager_t764408642_StaticFields::get_offset_of_contextLockUser_1(),
	LicenseManager_t764408642_StaticFields::get_offset_of_lockObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (LicenseProvider_t3536803528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (LicenseProviderAttribute_t1207066332), -1, sizeof(LicenseProviderAttribute_t1207066332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1267[2] = 
{
	LicenseProviderAttribute_t1207066332::get_offset_of_Provider_0(),
	LicenseProviderAttribute_t1207066332_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (LicenseUsageMode_t2996119499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1268[3] = 
{
	LicenseUsageMode_t2996119499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (ListBindableAttribute_t1092011273), -1, sizeof(ListBindableAttribute_t1092011273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1269[4] = 
{
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_Default_0(),
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_No_1(),
	ListBindableAttribute_t1092011273_StaticFields::get_offset_of_Yes_2(),
	ListBindableAttribute_t1092011273::get_offset_of_bindable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (ListChangedEventArgs_t3132270315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1270[4] = 
{
	ListChangedEventArgs_t3132270315::get_offset_of_changedType_1(),
	ListChangedEventArgs_t3132270315::get_offset_of_oldIndex_2(),
	ListChangedEventArgs_t3132270315::get_offset_of_newIndex_3(),
	ListChangedEventArgs_t3132270315::get_offset_of_propDesc_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (ListChangedType_t3463990274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1271[9] = 
{
	ListChangedType_t3463990274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (ListSortDescription_t3194554012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1272[2] = 
{
	ListSortDescription_t3194554012::get_offset_of_propertyDescriptor_0(),
	ListSortDescription_t3194554012::get_offset_of_sortDirection_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (ListSortDescriptionCollection_t2357028590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1273[1] = 
{
	ListSortDescriptionCollection_t2357028590::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (ListSortDirection_t4186912589)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1274[3] = 
{
	ListSortDirection_t4186912589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (LocalizableAttribute_t1267247394), -1, sizeof(LocalizableAttribute_t1267247394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1275[4] = 
{
	LocalizableAttribute_t1267247394::get_offset_of_localizable_0(),
	LocalizableAttribute_t1267247394_StaticFields::get_offset_of_Default_1(),
	LocalizableAttribute_t1267247394_StaticFields::get_offset_of_No_2(),
	LocalizableAttribute_t1267247394_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (MarshalByValueComponent_t3997823175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1276[3] = 
{
	MarshalByValueComponent_t3997823175::get_offset_of_eventList_0(),
	MarshalByValueComponent_t3997823175::get_offset_of_mySite_1(),
	MarshalByValueComponent_t3997823175::get_offset_of_disposedEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (MemberDescriptor_t3749827553), -1, sizeof(MemberDescriptor_t3749827553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1277[4] = 
{
	MemberDescriptor_t3749827553::get_offset_of_name_0(),
	MemberDescriptor_t3749827553::get_offset_of_attrs_1(),
	MemberDescriptor_t3749827553::get_offset_of_attrCollection_2(),
	MemberDescriptor_t3749827553_StaticFields::get_offset_of_default_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (MemberDescriptorComparer_t856477673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (MergablePropertyAttribute_t1597820506), -1, sizeof(MergablePropertyAttribute_t1597820506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1279[4] = 
{
	MergablePropertyAttribute_t1597820506::get_offset_of_mergable_0(),
	MergablePropertyAttribute_t1597820506_StaticFields::get_offset_of_Default_1(),
	MergablePropertyAttribute_t1597820506_StaticFields::get_offset_of_No_2(),
	MergablePropertyAttribute_t1597820506_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (MultilineStringConverter_t3643519256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (NotifyParentPropertyAttribute_t801973150), -1, sizeof(NotifyParentPropertyAttribute_t801973150_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1281[4] = 
{
	NotifyParentPropertyAttribute_t801973150::get_offset_of_notifyParent_0(),
	NotifyParentPropertyAttribute_t801973150_StaticFields::get_offset_of_Default_1(),
	NotifyParentPropertyAttribute_t801973150_StaticFields::get_offset_of_No_2(),
	NotifyParentPropertyAttribute_t801973150_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (NullableConverter_t1941973167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1282[3] = 
{
	NullableConverter_t1941973167::get_offset_of_nullableType_0(),
	NullableConverter_t1941973167::get_offset_of_underlyingType_1(),
	NullableConverter_t1941973167::get_offset_of_underlyingTypeConverter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (PasswordPropertyTextAttribute_t2834530705), -1, sizeof(PasswordPropertyTextAttribute_t2834530705_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1283[4] = 
{
	PasswordPropertyTextAttribute_t2834530705_StaticFields::get_offset_of_Default_0(),
	PasswordPropertyTextAttribute_t2834530705_StaticFields::get_offset_of_No_1(),
	PasswordPropertyTextAttribute_t2834530705_StaticFields::get_offset_of_Yes_2(),
	PasswordPropertyTextAttribute_t2834530705::get_offset_of__password_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (ProgressChangedEventArgs_t711712958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1284[2] = 
{
	ProgressChangedEventArgs_t711712958::get_offset_of_progress_1(),
	ProgressChangedEventArgs_t711712958::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (PropertyChangedEventArgs_t1689446432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1285[1] = 
{
	PropertyChangedEventArgs_t1689446432::get_offset_of_propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (PropertyDescriptor_t4250402154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1286[2] = 
{
	PropertyDescriptor_t4250402154::get_offset_of_converter_4(),
	PropertyDescriptor_t4250402154::get_offset_of_notifiers_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (PropertyDescriptorCollection_t3166009492), -1, sizeof(PropertyDescriptorCollection_t3166009492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1287[3] = 
{
	PropertyDescriptorCollection_t3166009492_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_properties_1(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (ReadOnlyAttribute_t4102148880), -1, sizeof(ReadOnlyAttribute_t4102148880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1288[4] = 
{
	ReadOnlyAttribute_t4102148880::get_offset_of_read_only_0(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_No_1(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Yes_2(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (RecommendedAsConfigurableAttribute_t420947846), -1, sizeof(RecommendedAsConfigurableAttribute_t420947846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1289[4] = 
{
	RecommendedAsConfigurableAttribute_t420947846::get_offset_of_recommendedAsConfigurable_0(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_Default_1(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_No_2(),
	RecommendedAsConfigurableAttribute_t420947846_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (ReferenceConverter_t3131270729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1290[1] = 
{
	ReferenceConverter_t3131270729::get_offset_of_reference_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (ReflectionEventDescriptor_t3740223310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1291[5] = 
{
	ReflectionEventDescriptor_t3740223310::get_offset_of__eventType_4(),
	ReflectionEventDescriptor_t3740223310::get_offset_of__componentType_5(),
	ReflectionEventDescriptor_t3740223310::get_offset_of__eventInfo_6(),
	ReflectionEventDescriptor_t3740223310::get_offset_of_add_method_7(),
	ReflectionEventDescriptor_t3740223310::get_offset_of_remove_method_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (ReflectionPropertyDescriptor_t2570125387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1292[6] = 
{
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__member_6(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__componentType_7(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__propertyType_8(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of_getter_9(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of_setter_10(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of_accessors_inited_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (RefreshEventArgs_t2077477224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1293[2] = 
{
	RefreshEventArgs_t2077477224::get_offset_of_component_1(),
	RefreshEventArgs_t2077477224::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (RefreshProperties_t2240171922)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1294[4] = 
{
	RefreshProperties_t2240171922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (RefreshPropertiesAttribute_t2234294918), -1, sizeof(RefreshPropertiesAttribute_t2234294918_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1295[4] = 
{
	RefreshPropertiesAttribute_t2234294918::get_offset_of_refresh_0(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_All_1(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_Default_2(),
	RefreshPropertiesAttribute_t2234294918_StaticFields::get_offset_of_Repaint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (RunWorkerCompletedEventArgs_t3512304465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1296[1] = 
{
	RunWorkerCompletedEventArgs_t3512304465::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (SByteConverter_t4003686519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (SingleConverter_t3693313828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (StringConverter_t3749524419), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
