﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Security_Cryptography_X509Certificate650873211.h"
#include "System_System_Security_Cryptography_X509Certificat3763443773.h"
#include "System_System_Security_Cryptography_X509Certificat3221716179.h"
#include "System_System_Security_Cryptography_X509Certificat1038124237.h"
#include "System_System_Security_Cryptography_X509Certificat2461349531.h"
#include "System_System_Security_Cryptography_X509Certificat2669466891.h"
#include "System_System_Security_Cryptography_X509Certificat2166064554.h"
#include "System_System_Security_Cryptography_X509Certificat2065307963.h"
#include "System_System_Security_Cryptography_X509Certificat1617430119.h"
#include "System_System_Security_Cryptography_X509Certificat2508879999.h"
#include "System_System_Security_Cryptography_X509Certificate110301003.h"
#include "System_System_Security_Cryptography_X509Certificat2169036324.h"
#include "System_System_Security_Cryptography_AsnDecodeStatu1962003286.h"
#include "System_System_Security_Cryptography_AsnEncodedData463456204.h"
#include "System_System_Security_Cryptography_Oid3221867120.h"
#include "System_System_Security_Cryptography_OidCollection3790243618.h"
#include "System_System_Security_Cryptography_OidEnumerator3674631724.h"
#include "System_System_Text_RegularExpressions_BaseMachine4008011478.h"
#include "System_System_Text_RegularExpressions_BaseMachine_1618777330.h"
#include "System_System_Text_RegularExpressions_Capture4157900610.h"
#include "System_System_Text_RegularExpressions_CaptureColle1671345504.h"
#include "System_System_Text_RegularExpressions_Group3761430853.h"
#include "System_System_Text_RegularExpressions_GroupCollecti939014605.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"
#include "System_System_Text_RegularExpressions_MatchCollect3718216671.h"
#include "System_System_Text_RegularExpressions_MatchCollecti501456973.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "System_System_Text_RegularExpressions_OpCode586571952.h"
#include "System_System_Text_RegularExpressions_OpFlags378191910.h"
#include "System_System_Text_RegularExpressions_Position3781184359.h"
#include "System_System_Text_RegularExpressions_FactoryCache2051534610.h"
#include "System_System_Text_RegularExpressions_FactoryCache_655155419.h"
#include "System_System_Text_RegularExpressions_MRUList33178162.h"
#include "System_System_Text_RegularExpressions_MRUList_Node1107172180.h"
#include "System_System_Text_RegularExpressions_Category1984577050.h"
#include "System_System_Text_RegularExpressions_CategoryUtil3840220623.h"
#include "System_System_Text_RegularExpressions_LinkRef2090853131.h"
#include "System_System_Text_RegularExpressions_InterpreterFa556462562.h"
#include "System_System_Text_RegularExpressions_PatternCompil637049905.h"
#include "System_System_Text_RegularExpressions_PatternCompi3979537293.h"
#include "System_System_Text_RegularExpressions_PatternCompi3337276394.h"
#include "System_System_Text_RegularExpressions_LinkStack954792760.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "System_System_Text_RegularExpressions_Interpreter3731288230.h"
#include "System_System_Text_RegularExpressions_Interpreter_I273560425.h"
#include "System_System_Text_RegularExpressions_Interpreter_1827616978.h"
#include "System_System_Text_RegularExpressions_Interpreter_2395763083.h"
#include "System_System_Text_RegularExpressions_Interval2354235237.h"
#include "System_System_Text_RegularExpressions_IntervalColl4130821325.h"
#include "System_System_Text_RegularExpressions_IntervalColl1928086041.h"
#include "System_System_Text_RegularExpressions_IntervalColl1824458113.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse2756028923.h"
#include "System_System_Text_RegularExpressions_QuickSearch1036078825.h"
#include "System_System_Text_RegularExpressions_ReplacementE1001703513.h"
#include "System_System_Text_RegularExpressions_Syntax_Expres238836340.h"
#include "System_System_Text_RegularExpressions_Syntax_Expres368137076.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo1921307915.h"
#include "System_System_Text_RegularExpressions_Syntax_Group2558408851.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul3083097024.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3690174926.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan3168604284.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBac607185170.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet3426306051.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser1490870658.h"
#include "System_System_Text_RegularExpressions_Syntax_Captur196851652.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3255443744.h"
#include "System_System_Text_RegularExpressions_Syntax_Alter3506694545.h"
#include "System_System_Text_RegularExpressions_Syntax_Liter2896011011.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit2152361535.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1540574699.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs1461652789.h"
#include "System_System_Text_RegularExpressions_Syntax_Charac655244183.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho1392970135.h"
#include "System_System_Threading_Semaphore159839144.h"
#include "System_System_Threading_SemaphoreFullException1495620572.h"
#include "System_System_DefaultUriParser1591960796.h"
#include "System_System_GenericUriParser2599285286.h"
#include "System_System_Uri19570940.h"
#include "System_System_Uri_UriScheme1876590943.h"
#include "System_System_UriBuilder2016461725.h"
#include "System_System_UriComponents3302767704.h"
#include "System_System_UriFormat2764505239.h"
#include "System_System_UriFormatException3682083048.h"
#include "System_System_UriHostNameType2148127109.h"
#include "System_System_UriKind1128731744.h"
#include "System_System_UriParser1012511323.h"
#include "System_System_UriPartial112107391.h"
#include "System_System_UriTypeConverter3912970448.h"
#include "System_System_ComponentModel_AsyncCompletedEventHan626974191.h"
#include "System_System_ComponentModel_CollectionChangeEventH790626706.h"
#include "System_System_ComponentModel_DoWorkEventHandler941110040.h"
#include "System_System_ComponentModel_ListChangedEventHandl2276411942.h"
#include "System_System_ComponentModel_ProgressChangedEventHa839864825.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059.h"
#include "System_System_ComponentModel_RefreshEventHandler456069287.h"
#include "System_System_ComponentModel_RunWorkerCompletedEve2492476920.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (X509ExtensionCollection_t650873211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1400[1] = 
{
	X509ExtensionCollection_t650873211::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (X509ExtensionEnumerator_t3763443773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1401[1] = 
{
	X509ExtensionEnumerator_t3763443773::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (X509FindType_t3221716179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1402[16] = 
{
	X509FindType_t3221716179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (X509KeyUsageExtension_t1038124237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1403[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t1038124237::get_offset_of__keyUsages_7(),
	X509KeyUsageExtension_t1038124237::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (X509KeyUsageFlags_t2461349531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1404[11] = 
{
	X509KeyUsageFlags_t2461349531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (X509NameType_t2669466891)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1405[7] = 
{
	X509NameType_t2669466891::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (X509RevocationFlag_t2166064554)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1406[4] = 
{
	X509RevocationFlag_t2166064554::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (X509RevocationMode_t2065307963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1407[4] = 
{
	X509RevocationMode_t2065307963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (X509Store_t1617430119), -1, sizeof(X509Store_t1617430119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1408[6] = 
{
	X509Store_t1617430119::get_offset_of__name_0(),
	X509Store_t1617430119::get_offset_of__location_1(),
	X509Store_t1617430119::get_offset_of_list_2(),
	X509Store_t1617430119::get_offset_of__flags_3(),
	X509Store_t1617430119::get_offset_of_store_4(),
	X509Store_t1617430119_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (X509SubjectKeyIdentifierExtension_t2508879999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1409[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__subjectKeyIdentifier_6(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__ski_7(),
	X509SubjectKeyIdentifierExtension_t2508879999::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_t110301003)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1410[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_t110301003::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (X509VerificationFlags_t2169036324)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1411[15] = 
{
	X509VerificationFlags_t2169036324::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (AsnDecodeStatus_t1962003286)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1412[7] = 
{
	AsnDecodeStatus_t1962003286::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (AsnEncodedData_t463456204), -1, sizeof(AsnEncodedData_t463456204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1413[3] = 
{
	AsnEncodedData_t463456204::get_offset_of__oid_0(),
	AsnEncodedData_t463456204::get_offset_of__raw_1(),
	AsnEncodedData_t463456204_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (Oid_t3221867120), -1, sizeof(Oid_t3221867120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1414[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Oid_t3221867120::get_offset_of__value_20(),
	Oid_t3221867120::get_offset_of__name_21(),
	Oid_t3221867120_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_22(),
	Oid_t3221867120_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (OidCollection_t3790243618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1415[2] = 
{
	OidCollection_t3790243618::get_offset_of__list_0(),
	OidCollection_t3790243618::get_offset_of__readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (OidEnumerator_t3674631724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1416[2] = 
{
	OidEnumerator_t3674631724::get_offset_of__collection_0(),
	OidEnumerator_t3674631724::get_offset_of__position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (BaseMachine_t4008011478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1417[1] = 
{
	BaseMachine_t4008011478::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (MatchAppendEvaluator_t1618777330), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (Capture_t4157900610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1419[3] = 
{
	Capture_t4157900610::get_offset_of_index_0(),
	Capture_t4157900610::get_offset_of_length_1(),
	Capture_t4157900610::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (CaptureCollection_t1671345504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1420[1] = 
{
	CaptureCollection_t1671345504::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (Group_t3761430853), -1, sizeof(Group_t3761430853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1421[3] = 
{
	Group_t3761430853_StaticFields::get_offset_of_Fail_3(),
	Group_t3761430853::get_offset_of_success_4(),
	Group_t3761430853::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (GroupCollection_t939014605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1422[2] = 
{
	GroupCollection_t939014605::get_offset_of_list_0(),
	GroupCollection_t939014605::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (Match_t3164245899), -1, sizeof(Match_t3164245899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1423[5] = 
{
	Match_t3164245899::get_offset_of_regex_6(),
	Match_t3164245899::get_offset_of_machine_7(),
	Match_t3164245899::get_offset_of_text_length_8(),
	Match_t3164245899::get_offset_of_groups_9(),
	Match_t3164245899_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (MatchCollection_t3718216671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1424[2] = 
{
	MatchCollection_t3718216671::get_offset_of_current_0(),
	MatchCollection_t3718216671::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (Enumerator_t501456973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1425[2] = 
{
	Enumerator_t501456973::get_offset_of_index_0(),
	Enumerator_t501456973::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (Regex_t1803876613), -1, sizeof(Regex_t1803876613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1426[14] = 
{
	Regex_t1803876613_StaticFields::get_offset_of_cache_0(),
	Regex_t1803876613::get_offset_of_machineFactory_1(),
	Regex_t1803876613::get_offset_of_mapping_2(),
	Regex_t1803876613::get_offset_of_group_count_3(),
	Regex_t1803876613::get_offset_of_gap_4(),
	Regex_t1803876613::get_offset_of_refsInitialized_5(),
	Regex_t1803876613::get_offset_of_group_names_6(),
	Regex_t1803876613::get_offset_of_group_numbers_7(),
	Regex_t1803876613::get_offset_of_pattern_8(),
	Regex_t1803876613::get_offset_of_roptions_9(),
	Regex_t1803876613::get_offset_of_capnames_10(),
	Regex_t1803876613::get_offset_of_caps_11(),
	Regex_t1803876613::get_offset_of_capsize_12(),
	Regex_t1803876613::get_offset_of_capslist_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (RegexOptions_t2418259727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1427[10] = 
{
	RegexOptions_t2418259727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (OpCode_t586571952)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1428[26] = 
{
	OpCode_t586571952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (OpFlags_t378191910)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1429[6] = 
{
	OpFlags_t378191910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (Position_t3781184359)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1430[11] = 
{
	Position_t3781184359::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (FactoryCache_t2051534610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1433[3] = 
{
	FactoryCache_t2051534610::get_offset_of_capacity_0(),
	FactoryCache_t2051534610::get_offset_of_factories_1(),
	FactoryCache_t2051534610::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (Key_t655155419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1434[2] = 
{
	Key_t655155419::get_offset_of_pattern_0(),
	Key_t655155419::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (MRUList_t33178162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1435[2] = 
{
	MRUList_t33178162::get_offset_of_head_0(),
	MRUList_t33178162::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (Node_t1107172180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1436[3] = 
{
	Node_t1107172180::get_offset_of_value_0(),
	Node_t1107172180::get_offset_of_previous_1(),
	Node_t1107172180::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (Category_t1984577050)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1437[146] = 
{
	Category_t1984577050::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (CategoryUtils_t3840220623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (LinkRef_t2090853131), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (InterpreterFactory_t556462562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1441[4] = 
{
	InterpreterFactory_t556462562::get_offset_of_mapping_0(),
	InterpreterFactory_t556462562::get_offset_of_pattern_1(),
	InterpreterFactory_t556462562::get_offset_of_namesMapping_2(),
	InterpreterFactory_t556462562::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (PatternCompiler_t637049905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1442[1] = 
{
	PatternCompiler_t637049905::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (PatternLinkStack_t3979537293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1443[1] = 
{
	PatternLinkStack_t3979537293::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (Link_t3337276394)+ sizeof (Il2CppObject), sizeof(Link_t3337276394 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1444[2] = 
{
	Link_t3337276394::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t3337276394::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (LinkStack_t954792760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1445[1] = 
{
	LinkStack_t954792760::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (Mark_t2724874473)+ sizeof (Il2CppObject), sizeof(Mark_t2724874473 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1446[3] = 
{
	Mark_t2724874473::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t2724874473::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t2724874473::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (Interpreter_t3731288230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1447[16] = 
{
	Interpreter_t3731288230::get_offset_of_program_1(),
	Interpreter_t3731288230::get_offset_of_program_start_2(),
	Interpreter_t3731288230::get_offset_of_text_3(),
	Interpreter_t3731288230::get_offset_of_text_end_4(),
	Interpreter_t3731288230::get_offset_of_group_count_5(),
	Interpreter_t3731288230::get_offset_of_match_min_6(),
	Interpreter_t3731288230::get_offset_of_qs_7(),
	Interpreter_t3731288230::get_offset_of_scan_ptr_8(),
	Interpreter_t3731288230::get_offset_of_repeat_9(),
	Interpreter_t3731288230::get_offset_of_fast_10(),
	Interpreter_t3731288230::get_offset_of_stack_11(),
	Interpreter_t3731288230::get_offset_of_deep_12(),
	Interpreter_t3731288230::get_offset_of_marks_13(),
	Interpreter_t3731288230::get_offset_of_mark_start_14(),
	Interpreter_t3731288230::get_offset_of_mark_end_15(),
	Interpreter_t3731288230::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (IntStack_t273560425)+ sizeof (Il2CppObject), sizeof(IntStack_t273560425_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1448[2] = 
{
	IntStack_t273560425::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t273560425::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (RepeatContext_t1827616978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1449[7] = 
{
	RepeatContext_t1827616978::get_offset_of_start_0(),
	RepeatContext_t1827616978::get_offset_of_min_1(),
	RepeatContext_t1827616978::get_offset_of_max_2(),
	RepeatContext_t1827616978::get_offset_of_lazy_3(),
	RepeatContext_t1827616978::get_offset_of_expr_pc_4(),
	RepeatContext_t1827616978::get_offset_of_previous_5(),
	RepeatContext_t1827616978::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (Mode_t2395763083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1450[4] = 
{
	Mode_t2395763083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (Interval_t2354235237)+ sizeof (Il2CppObject), sizeof(Interval_t2354235237_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1451[3] = 
{
	Interval_t2354235237::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2354235237::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2354235237::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (IntervalCollection_t4130821325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1452[1] = 
{
	IntervalCollection_t4130821325::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (Enumerator_t1928086041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1453[2] = 
{
	Enumerator_t1928086041::get_offset_of_list_0(),
	Enumerator_t1928086041::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (CostDelegate_t1824458113), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (Parser_t2756028923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1455[6] = 
{
	Parser_t2756028923::get_offset_of_pattern_0(),
	Parser_t2756028923::get_offset_of_ptr_1(),
	Parser_t2756028923::get_offset_of_caps_2(),
	Parser_t2756028923::get_offset_of_refs_3(),
	Parser_t2756028923::get_offset_of_num_groups_4(),
	Parser_t2756028923::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (QuickSearch_t1036078825), -1, sizeof(QuickSearch_t1036078825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1456[7] = 
{
	QuickSearch_t1036078825::get_offset_of_str_0(),
	QuickSearch_t1036078825::get_offset_of_len_1(),
	QuickSearch_t1036078825::get_offset_of_ignore_2(),
	QuickSearch_t1036078825::get_offset_of_reverse_3(),
	QuickSearch_t1036078825::get_offset_of_shift_4(),
	QuickSearch_t1036078825::get_offset_of_shiftExtended_5(),
	QuickSearch_t1036078825_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (ReplacementEvaluator_t1001703513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1457[4] = 
{
	ReplacementEvaluator_t1001703513::get_offset_of_regex_0(),
	ReplacementEvaluator_t1001703513::get_offset_of_n_pieces_1(),
	ReplacementEvaluator_t1001703513::get_offset_of_pieces_2(),
	ReplacementEvaluator_t1001703513::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (ExpressionCollection_t238836340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (Expression_t368137076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (CompositeExpression_t1921307915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1460[1] = 
{
	CompositeExpression_t1921307915::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (Group_t2558408851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (RegularExpression_t3083097024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1462[1] = 
{
	RegularExpression_t3083097024::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (CapturingGroup_t3690174926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1463[2] = 
{
	CapturingGroup_t3690174926::get_offset_of_gid_1(),
	CapturingGroup_t3690174926::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (BalancingGroup_t3168604284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1464[1] = 
{
	BalancingGroup_t3168604284::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (NonBacktrackingGroup_t607185170), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (Repetition_t3426306051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1466[3] = 
{
	Repetition_t3426306051::get_offset_of_min_1(),
	Repetition_t3426306051::get_offset_of_max_2(),
	Repetition_t3426306051::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (Assertion_t1490870658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (CaptureAssertion_t196851652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1468[3] = 
{
	CaptureAssertion_t196851652::get_offset_of_alternate_1(),
	CaptureAssertion_t196851652::get_offset_of_group_2(),
	CaptureAssertion_t196851652::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (ExpressionAssertion_t3255443744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1469[2] = 
{
	ExpressionAssertion_t3255443744::get_offset_of_reverse_1(),
	ExpressionAssertion_t3255443744::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (Alternation_t3506694545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (Literal_t2896011011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1471[2] = 
{
	Literal_t2896011011::get_offset_of_str_0(),
	Literal_t2896011011::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (PositionAssertion_t2152361535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1472[1] = 
{
	PositionAssertion_t2152361535::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (Reference_t1540574699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1473[2] = 
{
	Reference_t1540574699::get_offset_of_group_0(),
	Reference_t1540574699::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (BackslashNumber_t1461652789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1474[2] = 
{
	BackslashNumber_t1461652789::get_offset_of_literal_2(),
	BackslashNumber_t1461652789::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (CharacterClass_t655244183), -1, sizeof(CharacterClass_t655244183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1475[6] = 
{
	CharacterClass_t655244183_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t655244183::get_offset_of_negate_1(),
	CharacterClass_t655244183::get_offset_of_ignore_2(),
	CharacterClass_t655244183::get_offset_of_pos_cats_3(),
	CharacterClass_t655244183::get_offset_of_neg_cats_4(),
	CharacterClass_t655244183::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (AnchorInfo_t1392970135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1476[6] = 
{
	AnchorInfo_t1392970135::get_offset_of_expr_0(),
	AnchorInfo_t1392970135::get_offset_of_pos_1(),
	AnchorInfo_t1392970135::get_offset_of_offset_2(),
	AnchorInfo_t1392970135::get_offset_of_str_3(),
	AnchorInfo_t1392970135::get_offset_of_width_4(),
	AnchorInfo_t1392970135::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (Semaphore_t159839144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (SemaphoreFullException_t1495620572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (DefaultUriParser_t1591960796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (GenericUriParser_t2599285286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (Uri_t19570940), -1, sizeof(Uri_t19570940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1481[38] = 
{
	0,
	Uri_t19570940::get_offset_of_isUnixFilePath_1(),
	Uri_t19570940::get_offset_of_source_2(),
	Uri_t19570940::get_offset_of_scheme_3(),
	Uri_t19570940::get_offset_of_host_4(),
	Uri_t19570940::get_offset_of_port_5(),
	Uri_t19570940::get_offset_of_path_6(),
	Uri_t19570940::get_offset_of_query_7(),
	Uri_t19570940::get_offset_of_fragment_8(),
	Uri_t19570940::get_offset_of_userinfo_9(),
	Uri_t19570940::get_offset_of_isUnc_10(),
	Uri_t19570940::get_offset_of_isOpaquePart_11(),
	Uri_t19570940::get_offset_of_isAbsoluteUri_12(),
	Uri_t19570940::get_offset_of_segments_13(),
	Uri_t19570940::get_offset_of_userEscaped_14(),
	Uri_t19570940::get_offset_of_cachedAbsoluteUri_15(),
	Uri_t19570940::get_offset_of_cachedToString_16(),
	Uri_t19570940::get_offset_of_cachedLocalPath_17(),
	Uri_t19570940::get_offset_of_cachedHashCode_18(),
	Uri_t19570940_StaticFields::get_offset_of_hexUpperChars_19(),
	Uri_t19570940_StaticFields::get_offset_of_SchemeDelimiter_20(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFile_21(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeFtp_22(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeGopher_23(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttp_24(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeHttps_25(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeMailto_26(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNews_27(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNntp_28(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetPipe_29(),
	Uri_t19570940_StaticFields::get_offset_of_UriSchemeNetTcp_30(),
	Uri_t19570940_StaticFields::get_offset_of_schemes_31(),
	Uri_t19570940::get_offset_of_parser_32(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map12_33(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_34(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_35(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_36(),
	Uri_t19570940_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (UriScheme_t1876590943)+ sizeof (Il2CppObject), sizeof(UriScheme_t1876590943_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1482[3] = 
{
	UriScheme_t1876590943::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1876590943::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1876590943::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (UriBuilder_t2016461725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1483[10] = 
{
	UriBuilder_t2016461725::get_offset_of_scheme_0(),
	UriBuilder_t2016461725::get_offset_of_host_1(),
	UriBuilder_t2016461725::get_offset_of_port_2(),
	UriBuilder_t2016461725::get_offset_of_path_3(),
	UriBuilder_t2016461725::get_offset_of_query_4(),
	UriBuilder_t2016461725::get_offset_of_fragment_5(),
	UriBuilder_t2016461725::get_offset_of_username_6(),
	UriBuilder_t2016461725::get_offset_of_password_7(),
	UriBuilder_t2016461725::get_offset_of_uri_8(),
	UriBuilder_t2016461725::get_offset_of_modified_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (UriComponents_t3302767704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1484[17] = 
{
	UriComponents_t3302767704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (UriFormat_t2764505239)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1485[4] = 
{
	UriFormat_t2764505239::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (UriFormatException_t3682083048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (UriHostNameType_t2148127109)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1487[6] = 
{
	UriHostNameType_t2148127109::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (UriKind_t1128731744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1488[4] = 
{
	UriKind_t1128731744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (UriParser_t1012511323), -1, sizeof(UriParser_t1012511323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1489[6] = 
{
	UriParser_t1012511323_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t1012511323_StaticFields::get_offset_of_table_1(),
	UriParser_t1012511323::get_offset_of_scheme_name_2(),
	UriParser_t1012511323::get_offset_of_default_port_3(),
	UriParser_t1012511323_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t1012511323_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (UriPartial_t112107391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1490[5] = 
{
	UriPartial_t112107391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (UriTypeConverter_t3912970448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (AsyncCompletedEventHandler_t626974191), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (CollectionChangeEventHandler_t790626706), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (DoWorkEventHandler_t941110040), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (ListChangedEventHandler_t2276411942), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (ProgressChangedEventHandler_t839864825), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (PropertyChangedEventHandler_t3042952059), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (RefreshEventHandler_t456069287), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (RunWorkerCompletedEventHandler_t2492476920), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
