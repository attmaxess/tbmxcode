﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Security_LocalCertificateSelecti3696771181.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Text_RegularExpressions_MatchEvaluato710107290.h"
#include "System_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayT116038554.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3672778802.h"
#include "Unity_Compat_U3CModuleU3E3783534214.h"
#include "Unity_Tasks_U3CModuleU3E3783534214.h"
#include "UnityEngine_U3CModuleU3E3783534214.h"
#include "UnityEngine_UnityEngine_NetworkReachability1092747145.h"
#include "UnityEngine_UnityEngine_Application354826772.h"
#include "UnityEngine_UnityEngine_Application_LowMemoryCallba642977590.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1038783543.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2674559435.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction1786092740.h"
#include "UnityEngine_UnityEngine_WaitUntil722209015.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera_StereoscopicEye1438019089.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_ComputeBuffer1827099467.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1903422412.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1057617917.h"
#include "UnityEngine_UnityEngine_CullingGroup1091689465.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2480912210.h"
#include "UnityEngine_UnityEngine_CursorLockMode3372615096.h"
#include "UnityEngine_UnityEngine_Cursor873194084.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509.h"
#include "UnityEngine_UnityEngine_Debug1368543263.h"
#include "UnityEngine_UnityEngine_Display3666191348.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815.h"
#include "UnityEngine_UnityEngine_FlareLayer1985082419.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2156144444.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1170095138.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_453887929.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Gizmos2256232573.h"
#include "UnityEngine_UnityEngine_Gradient3600583008.h"
#include "UnityEngine_UnityEngine_RenderSettings1057812535.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_LensFlare529161798.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Projector1121484678.h"
#include "UnityEngine_UnityEngine_Skybox2033495038.h"
#include "UnityEngine_UnityEngine_TrailRenderer2490637367.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock3303648957.h"
#include "UnityEngine_UnityEngine_Graphics2412809155.h"
#include "UnityEngine_UnityEngine_Screen786852042.h"
#include "UnityEngine_UnityEngine_GL1765937205.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_ImageEffectTransformsToLDR4052226528.h"
#include "UnityEngine_UnityEngine_ImageEffectAllowedInSceneV3940936018.h"
#include "UnityEngine_UnityEngine_ImageEffectOpaque3094350515.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Intern1040270188.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard601950206.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "UnityEngine_UnityEngine_IMECompositionMode1898275508.h"
#include "UnityEngine_UnityEngine_TouchType2732027771.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Gyroscope1705362817.h"
#include "UnityEngine_UnityEngine_Input1785128008.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Light494725636.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Mathf2336485820.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3229544204.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001.h"
#include "UnityEngine_UnityEngine_Random1170710517.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_SortingLayer221838959.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonic364136731.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (LocalCertificateSelectionCallback_t3696771181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (RemoteCertificateValidationCallback_t2756269959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (MatchEvaluator_t710107290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1503[3] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D2_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D3_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D4_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (U24ArrayTypeU24128_t116038555)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t116038555 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (U24ArrayTypeU2412_t3672778806)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778806 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (NetworkReachability_t1092747145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1510[4] = 
{
	NetworkReachability_t1092747145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (Application_t354826772), -1, sizeof(Application_t354826772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1511[3] = 
{
	Application_t354826772_StaticFields::get_offset_of_lowMemory_0(),
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandler_1(),
	Application_t354826772_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (LowMemoryCallback_t642977590), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (LogCallback_t1867914413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (AssetBundleCreateRequest_t1038783543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (AssetBundleRequest_t2674559435), sizeof(AssetBundleRequest_t2674559435_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (AssetBundle_t2054978754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (AsyncOperation_t3814632279), sizeof(AsyncOperation_t3814632279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1517[1] = 
{
	AsyncOperation_t3814632279::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (SystemInfo_t2353426895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (WaitForSeconds_t3839502067), sizeof(WaitForSeconds_t3839502067_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1519[1] = 
{
	WaitForSeconds_t3839502067::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (WaitForFixedUpdate_t3968615785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (WaitForEndOfFrame_t1785723201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (CustomYieldInstruction_t1786092740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (WaitUntil_t722209015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1523[1] = 
{
	WaitUntil_t722209015::get_offset_of_m_Predicate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (Coroutine_t2299508840), sizeof(Coroutine_t2299508840_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1524[1] = 
{
	Coroutine_t2299508840::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (ScriptableObject_t1975622470), sizeof(ScriptableObject_t1975622470_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (Behaviour_t955675639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (Camera_t189460977), -1, sizeof(Camera_t189460977_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1527[3] = 
{
	Camera_t189460977_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t189460977_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t189460977_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (StereoscopicEye_t1438019089)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1528[3] = 
{
	StereoscopicEye_t1438019089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (CameraCallback_t834278767), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (Component_t3819376471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (ComputeBuffer_t1827099467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1531[1] = 
{
	ComputeBuffer_t1827099467::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (UnhandledExceptionHandler_t1903422412), -1, sizeof(UnhandledExceptionHandler_t1903422412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1532[1] = 
{
	UnhandledExceptionHandler_t1903422412_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (CullingGroupEvent_t1057617917)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t1057617917 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1533[3] = 
{
	CullingGroupEvent_t1057617917::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1057617917::get_offset_of_m_PrevState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t1057617917::get_offset_of_m_ThisState_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (CullingGroup_t1091689465), sizeof(CullingGroup_t1091689465_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1534[2] = 
{
	CullingGroup_t1091689465::get_offset_of_m_Ptr_0(),
	CullingGroup_t1091689465::get_offset_of_m_OnStateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (StateChanged_t2480912210), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (CursorLockMode_t3372615096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1536[4] = 
{
	CursorLockMode_t3372615096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (Cursor_t873194084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (DebugLogHandler_t865810509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (Debug_t1368543263), -1, sizeof(Debug_t1368543263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1539[1] = 
{
	Debug_t1368543263_StaticFields::get_offset_of_s_Logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (Display_t3666191348), -1, sizeof(Display_t3666191348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1540[4] = 
{
	Display_t3666191348::get_offset_of_nativeDisplay_0(),
	Display_t3666191348_StaticFields::get_offset_of_displays_1(),
	Display_t3666191348_StaticFields::get_offset_of__mainDisplay_2(),
	Display_t3666191348_StaticFields::get_offset_of_onDisplaysUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (DisplaysUpdatedDelegate_t3423469815), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (FlareLayer_t1985082419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (GameCenterPlatform_t2156144444), -1, sizeof(GameCenterPlatform_t2156144444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1543[7] = 
{
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_AuthenticateCallback_0(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_adCache_1(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_friends_2(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_users_3(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_s_ResetAchievements_4(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_m_LocalUser_5(),
	GameCenterPlatform_t2156144444_StaticFields::get_offset_of_m_GcBoards_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1544[1] = 
{
	U3CUnityEngine_SocialPlatforms_ISocialPlatform_AuthenticateU3Ec__AnonStorey0_t1170095138::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (GcLeaderboard_t453887929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[2] = 
{
	GcLeaderboard_t453887929::get_offset_of_m_InternalLeaderboard_0(),
	GcLeaderboard_t453887929::get_offset_of_m_GenericLeaderboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (GameObject_t1756533147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (Gizmos_t2256232573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (Gradient_t3600583008), sizeof(Gradient_t3600583008_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1548[1] = 
{
	Gradient_t3600583008::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (RenderSettings_t1057812535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (QualitySettings_t3238033062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (MeshFilter_t3026937449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (LensFlare_t529161798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (Renderer_t257310565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (Projector_t1121484678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (Skybox_t2033495038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (TrailRenderer_t2490637367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (LineRenderer_t849157671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (MaterialPropertyBlock_t3303648957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[1] = 
{
	MaterialPropertyBlock_t3303648957::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (Graphics_t2412809155), -1, sizeof(Graphics_t2412809155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1559[1] = 
{
	Graphics_t2412809155_StaticFields::get_offset_of_kMaxDrawMeshInstanceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (Screen_t786852042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (GL_t1765937205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (MeshRenderer_t1268241104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (ImageEffectTransformsToLDR_t4052226528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (ImageEffectAllowedInSceneView_t3940936018), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (ImageEffectOpaque_t3094350515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (RectOffset_t3387826427), sizeof(RectOffset_t3387826427_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1566[2] = 
{
	RectOffset_t3387826427::get_offset_of_m_Ptr_0(),
	RectOffset_t3387826427::get_offset_of_m_SourceStyle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (GUIElement_t3381083099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (GUILayer_t3254902478), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188)+ sizeof (Il2CppObject), sizeof(TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1569[5] = 
{
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188::get_offset_of_keyboardType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188::get_offset_of_autocorrection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188::get_offset_of_multiline_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188::get_offset_of_secure_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188::get_offset_of_alert_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (TouchScreenKeyboard_t601950206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[1] = 
{
	TouchScreenKeyboard_t601950206::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (TouchPhase_t2458120420)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1571[6] = 
{
	TouchPhase_t2458120420::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (IMECompositionMode_t1898275508)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1572[4] = 
{
	IMECompositionMode_t1898275508::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (TouchType_t2732027771)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1573[4] = 
{
	TouchType_t2732027771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (Touch_t407273883)+ sizeof (Il2CppObject), sizeof(Touch_t407273883 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1574[14] = 
{
	Touch_t407273883::get_offset_of_m_FingerId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_Position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_RawPosition_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_PositionDelta_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_TimeDelta_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_TapCount_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_Phase_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_Type_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_Pressure_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_maximumPossiblePressure_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_Radius_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_RadiusVariance_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_AltitudeAngle_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t407273883::get_offset_of_m_AzimuthAngle_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (Gyroscope_t1705362817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (Input_t1785128008), -1, sizeof(Input_t1785128008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1576[1] = 
{
	Input_t1785128008_StaticFields::get_offset_of_m_MainGyro_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (LayerMask_t3188175821)+ sizeof (Il2CppObject), sizeof(LayerMask_t3188175821 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1577[1] = 
{
	LayerMask_t3188175821::get_offset_of_m_Mask_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (Light_t494725636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[1] = 
{
	Light_t494725636::get_offset_of_m_BakedIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (Vector3_t2243707580)+ sizeof (Il2CppObject), sizeof(Vector3_t2243707580 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1579[4] = 
{
	0,
	Vector3_t2243707580::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t2243707580::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t2243707580::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (Quaternion_t4030073918)+ sizeof (Il2CppObject), sizeof(Quaternion_t4030073918 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1580[5] = 
{
	Quaternion_t4030073918::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t4030073918::get_offset_of_w_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (Matrix4x4_t2933234003)+ sizeof (Il2CppObject), sizeof(Matrix4x4_t2933234003 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1581[16] = 
{
	Matrix4x4_t2933234003::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m10_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m20_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m30_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m01_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m21_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m31_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m02_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m12_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m32_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m03_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m13_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m23_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t2933234003::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (Bounds_t3033363703)+ sizeof (Il2CppObject), sizeof(Bounds_t3033363703 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1582[2] = 
{
	Bounds_t3033363703::get_offset_of_m_Center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Bounds_t3033363703::get_offset_of_m_Extents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (Mathf_t2336485820)+ sizeof (Il2CppObject), sizeof(Mathf_t2336485820 ), sizeof(Mathf_t2336485820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1583[1] = 
{
	Mathf_t2336485820_StaticFields::get_offset_of_Epsilon_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (Keyframe_t1449471340)+ sizeof (Il2CppObject), sizeof(Keyframe_t1449471340 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1584[4] = 
{
	Keyframe_t1449471340::get_offset_of_m_Time_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_Value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_InTangent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t1449471340::get_offset_of_m_OutTangent_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (WrapMode_t255797857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1585[7] = 
{
	WrapMode_t255797857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (AnimationCurve_t3306541151), sizeof(AnimationCurve_t3306541151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1586[1] = 
{
	AnimationCurve_t3306541151::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (Mesh_t1356156583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (InternalShaderChannel_t3331827198)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1588[9] = 
{
	InternalShaderChannel_t3331827198::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (InternalVertexChannelType_t2178520045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1589[3] = 
{
	InternalVertexChannelType_t2178520045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (MonoBehaviour_t1158329972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (PlayerPrefsException_t3229544204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (PlayerPrefs_t3325146001), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (Random_t1170710517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (ResourceRequest_t2560315377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1594[2] = 
{
	ResourceRequest_t2560315377::get_offset_of_m_Path_1(),
	ResourceRequest_t2560315377::get_offset_of_m_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (Resources_t339470017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (Shader_t2430389951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (Material_t193706927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (SortingLayer_t221838959)+ sizeof (Il2CppObject), sizeof(SortingLayer_t221838959 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1598[1] = 
{
	SortingLayer_t221838959::get_offset_of_m_Id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (SphericalHarmonicsL2_t364136731)+ sizeof (Il2CppObject), sizeof(SphericalHarmonicsL2_t364136731 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1599[27] = 
{
	SphericalHarmonicsL2_t364136731::get_offset_of_shr0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr4_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr5_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr6_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr7_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shr8_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg0_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg1_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg2_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg3_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg4_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg5_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg6_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg7_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shg8_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb0_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb1_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb2_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb3_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb4_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb5_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb6_24() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb7_25() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SphericalHarmonicsL2_t364136731::get_offset_of_shb8_26() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
