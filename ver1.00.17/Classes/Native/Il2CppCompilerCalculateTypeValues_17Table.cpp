﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete1381019216.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847.h"
#include "UnityEngine_UnityEngine_HumanLimit250797648.h"
#include "UnityEngine_UnityEngine_HumanBone1529896151.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3036622417.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anima641234490.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4062767676.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anima859920217.h"
#include "UnityEngine_UnityEngine_Motion2415020824.h"
#include "UnityEngine_UnityEngine_TerrainData1351141029.h"
#include "UnityEngine_UnityEngine_Terrain59182933.h"
#include "UnityEngine_UnityEngine_FontStyle2764949590.h"
#include "UnityEngine_UnityEngine_TextGenerationError780770201.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings2543476768.h"
#include "UnityEngine_UnityEngine_TextGenerator647235000.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2027154177.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode3668245347.h"
#include "UnityEngine_UnityEngine_Font4239498691.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal1272078033.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3522132132.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "UnityEngine_UnityEngine_CanvasRenderer261436805.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_EventModifiers2690251474.h"
#include "UnityEngine_UnityEngine_GUI4082743951.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_ScaleMode324459649.h"
#include "UnityEngine_UnityEngine_FocusType488772178.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type4024155706.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup3975363388.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup755788567.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry3828586629.h"
#include "UnityEngine_UnityEngine_GUIGridSizer636944578.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer1610158404.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility996096873.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCac3120781045.h"
#include "UnityEngine_UnityEngine_GUISettings622856320.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat3594822336.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "UnityEngine_UnityEngine_ImagePosition3491916276.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_TextClipping2573530411.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute863467180.h"
#include "UnityEngine_UnityEngine_ExitGUIException1618397098.h"
#include "UnityEngine_UnityEngine_GUIUtility3275770671.h"
#include "UnityEngine_UnityEngine_GUIClip3473260597.h"
#include "UnityEngine_UnityEngine_ScrollViewState3820542997.h"
#include "UnityEngine_UnityEngine_SliderState1595681032.h"
#include "UnityEngine_UnityEngine_SliderHandler3550500579.h"
#include "UnityEngine_UnityEngine_TextEditor3975561390.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1119726228.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType593718391.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments2834709342.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelec1327795077.h"
#include "UnityEngine_UnityEngineInternal_WebRequestUtils4100941042.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2120336905.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1520641178.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946.h"
#include "UnityEngine_UnityEngine_RemoteSettings392466225.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven3033456180.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer10059812.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_EventHan2685920451.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_ErrorEve3983973519.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_FrameRea2353988013.h"
#include "UnityEngine_UnityEngine_Video_VideoClip3831376279.h"
#include "UnityEngine_UnityEngine_ParticleEmitter4099167268.h"
#include "UnityEngine_UnityEngine_EllipsoidParticleEmitter3972767653.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_AddComponentMenu1099699699.h"
#include "UnityEngine_UnityEngine_ContextMenu2283362202.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3043633143.h"
#include "UnityEngine_UnityEngine_HideInInspector2503583610.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder2717914595.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttrib130316838.h"
#include "UnityEngine_UnityEngine_NativeClassAttribute1576243993.h"
#include "UnityEngine_UnityEngine_Scripting_GeneratedByOldBin107439586.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (AnimatorClipInfo_t3905751349)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t3905751349 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[2] = 
{
	AnimatorClipInfo_t3905751349::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t3905751349::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (AnimatorStateInfo_t2577870592)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t2577870592 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1701[9] = 
{
	AnimatorStateInfo_t2577870592::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (AnimatorTransitionInfo_t2410896200)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2410896200_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1702[6] = 
{
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (Animator_t69676727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (AnimatorControllerParameter_t1381019216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[5] = 
{
	AnimatorControllerParameter_t1381019216::get_offset_of_m_Name_0(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_Type_1(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_DefaultFloat_2(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_DefaultInt_3(),
	AnimatorControllerParameter_t1381019216::get_offset_of_m_DefaultBool_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (SkeletonBone_t345082847)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t345082847_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1705[5] = 
{
	SkeletonBone_t345082847::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_parentName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_rotation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (HumanLimit_t250797648)+ sizeof (Il2CppObject), sizeof(HumanLimit_t250797648 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1706[5] = 
{
	HumanLimit_t250797648::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (HumanBone_t1529896151)+ sizeof (Il2CppObject), sizeof(HumanBone_t1529896151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1707[3] = 
{
	HumanBone_t1529896151::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (RuntimeAnimatorController_t670468573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (AnimatorControllerPlayable_t4078305555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (AnimationMixerPlayable_t3036622417), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (AnimationLayerMixerPlayable_t641234490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (AnimationClipPlayable_t4062767676), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (AnimationPlayable_t1693994278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (AnimationOffsetPlayable_t859920217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (Motion_t2415020824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (TerrainData_t1351141029), -1, sizeof(TerrainData_t1351141029_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[8] = 
{
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumResolution_2(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMinimumDetailResolutionPerPatch_3(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumDetailResolutionPerPatch_4(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumDetailPatchCount_5(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMinimumAlphamapResolution_6(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumAlphamapResolution_7(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMinimumBaseMapResolution_8(),
	TerrainData_t1351141029_StaticFields::get_offset_of_kMaximumBaseMapResolution_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (Terrain_t59182933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (FontStyle_t2764949590)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1718[5] = 
{
	FontStyle_t2764949590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (TextGenerationError_t780770201)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1719[5] = 
{
	TextGenerationError_t780770201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (TextGenerationSettings_t2543476768)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[18] = 
{
	TextGenerationSettings_t2543476768::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (TextGenerator_t647235000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[11] = 
{
	TextGenerator_t647235000::get_offset_of_m_Ptr_0(),
	TextGenerator_t647235000::get_offset_of_m_LastString_1(),
	TextGenerator_t647235000::get_offset_of_m_LastSettings_2(),
	TextGenerator_t647235000::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t647235000::get_offset_of_m_LastValid_4(),
	TextGenerator_t647235000::get_offset_of_m_Verts_5(),
	TextGenerator_t647235000::get_offset_of_m_Characters_6(),
	TextGenerator_t647235000::get_offset_of_m_Lines_7(),
	TextGenerator_t647235000::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t647235000::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t647235000::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (TextAnchor_t112990806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1722[10] = 
{
	TextAnchor_t112990806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (HorizontalWrapMode_t2027154177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1723[3] = 
{
	HorizontalWrapMode_t2027154177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (VerticalWrapMode_t3668245347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[3] = 
{
	VerticalWrapMode_t3668245347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (Font_t4239498691), -1, sizeof(Font_t4239498691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1725[2] = 
{
	Font_t4239498691_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4239498691::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (FontTextureRebuildCallback_t1272078033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (UICharInfo_t3056636800)+ sizeof (Il2CppObject), sizeof(UICharInfo_t3056636800 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1727[2] = 
{
	UICharInfo_t3056636800::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t3056636800::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (UILineInfo_t3621277874)+ sizeof (Il2CppObject), sizeof(UILineInfo_t3621277874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1728[4] = 
{
	UILineInfo_t3621277874::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_leading_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (UIVertex_t1204258818)+ sizeof (Il2CppObject), sizeof(UIVertex_t1204258818 ), sizeof(UIVertex_t1204258818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1729[11] = 
{
	UIVertex_t1204258818::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_tangent_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultColor_8(),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultTangent_9(),
	UIVertex_t1204258818_StaticFields::get_offset_of_simpleVert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (RectTransformUtility_t2941082270), -1, sizeof(RectTransformUtility_t2941082270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1730[1] = 
{
	RectTransformUtility_t2941082270_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (RenderMode_t4280533217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1731[4] = 
{
	RenderMode_t4280533217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (Canvas_t209405766), -1, sizeof(Canvas_t209405766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1732[1] = 
{
	Canvas_t209405766_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (WillRenderCanvases_t3522132132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (CanvasGroup_t3296560743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (CanvasRenderer_t261436805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (Event_t3028476042), sizeof(Event_t3028476042_marshaled_pinvoke), sizeof(Event_t3028476042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1737[4] = 
{
	Event_t3028476042::get_offset_of_m_Ptr_0(),
	Event_t3028476042_StaticFields::get_offset_of_s_Current_1(),
	Event_t3028476042_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t3028476042_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (EventType_t3919834026)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1738[33] = 
{
	EventType_t3919834026::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (EventModifiers_t2690251474)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1739[9] = 
{
	EventModifiers_t2690251474::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (GUI_t4082743951), -1, sizeof(GUI_t4082743951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1740[14] = 
{
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollControlId_1(),
	GUI_t4082743951_StaticFields::get_offset_of_s_HotTextField_2(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BoxHash_3(),
	GUI_t4082743951_StaticFields::get_offset_of_s_RepeatButtonHash_4(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ToggleHash_5(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ButtonGridHash_6(),
	GUI_t4082743951_StaticFields::get_offset_of_s_SliderHash_7(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BeginGroupHash_8(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollviewHash_9(),
	GUI_t4082743951_StaticFields::get_offset_of_U3CscrollTroughSideU3Ek__BackingField_10(),
	GUI_t4082743951_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_11(),
	GUI_t4082743951_StaticFields::get_offset_of_s_Skin_12(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollViewStates_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (WindowFunction_t3486805455), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (GUIContent_t4210063000), -1, sizeof(GUIContent_t4210063000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1742[7] = 
{
	GUIContent_t4210063000::get_offset_of_m_Text_0(),
	GUIContent_t4210063000::get_offset_of_m_Image_1(),
	GUIContent_t4210063000::get_offset_of_m_Tooltip_2(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t4210063000_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (ScaleMode_t324459649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[4] = 
{
	ScaleMode_t324459649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (FocusType_t488772178)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1744[4] = 
{
	FocusType_t488772178::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (GUILayout_t2579273657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (GUILayoutOption_t4183744904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[2] = 
{
	GUILayoutOption_t4183744904::get_offset_of_type_0(),
	GUILayoutOption_t4183744904::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (Type_t4024155706)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[15] = 
{
	Type_t4024155706::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (GUILayoutGroup_t3975363388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[17] = 
{
	GUILayoutGroup_t3975363388::get_offset_of_entries_10(),
	GUILayoutGroup_t3975363388::get_offset_of_isVertical_11(),
	GUILayoutGroup_t3975363388::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t3975363388::get_offset_of_spacing_13(),
	GUILayoutGroup_t3975363388::get_offset_of_sameSize_14(),
	GUILayoutGroup_t3975363388::get_offset_of_isWindow_15(),
	GUILayoutGroup_t3975363388::get_offset_of_windowID_16(),
	GUILayoutGroup_t3975363388::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t3975363388::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t3975363388::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t3975363388::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t3975363388::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t3975363388::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (GUIScrollGroup_t755788567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[12] = 
{
	GUIScrollGroup_t755788567::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t755788567::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t755788567::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t755788567::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t755788567::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t755788567::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t755788567::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t755788567::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t755788567::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t755788567::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t755788567::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t755788567::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (GUILayoutEntry_t3828586629), -1, sizeof(GUILayoutEntry_t3828586629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1750[10] = 
{
	GUILayoutEntry_t3828586629::get_offset_of_minWidth_0(),
	GUILayoutEntry_t3828586629::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t3828586629::get_offset_of_minHeight_2(),
	GUILayoutEntry_t3828586629::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t3828586629::get_offset_of_rect_4(),
	GUILayoutEntry_t3828586629::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t3828586629::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t3828586629::get_offset_of_m_Style_7(),
	GUILayoutEntry_t3828586629_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t3828586629_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (GUIGridSizer_t636944578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[6] = 
{
	GUIGridSizer_t636944578::get_offset_of_m_Count_10(),
	GUIGridSizer_t636944578::get_offset_of_m_XCount_11(),
	GUIGridSizer_t636944578::get_offset_of_m_MinButtonWidth_12(),
	GUIGridSizer_t636944578::get_offset_of_m_MaxButtonWidth_13(),
	GUIGridSizer_t636944578::get_offset_of_m_MinButtonHeight_14(),
	GUIGridSizer_t636944578::get_offset_of_m_MaxButtonHeight_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (GUIWordWrapSizer_t1610158404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[3] = 
{
	GUIWordWrapSizer_t1610158404::get_offset_of_m_Content_10(),
	GUIWordWrapSizer_t1610158404::get_offset_of_m_ForcedMinHeight_11(),
	GUIWordWrapSizer_t1610158404::get_offset_of_m_ForcedMaxHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (GUILayoutUtility_t996096873), -1, sizeof(GUILayoutUtility_t996096873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1753[5] = 
{
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (LayoutCache_t3120781045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[3] = 
{
	LayoutCache_t3120781045::get_offset_of_topLevel_0(),
	LayoutCache_t3120781045::get_offset_of_layoutGroups_1(),
	LayoutCache_t3120781045::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (GUISettings_t622856320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[5] = 
{
	GUISettings_t622856320::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t622856320::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t622856320::get_offset_of_m_CursorColor_2(),
	GUISettings_t622856320::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t622856320::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (GUISkin_t1436893342), -1, sizeof(GUISkin_t1436893342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1756[27] = 
{
	GUISkin_t1436893342::get_offset_of_m_Font_2(),
	GUISkin_t1436893342::get_offset_of_m_box_3(),
	GUISkin_t1436893342::get_offset_of_m_button_4(),
	GUISkin_t1436893342::get_offset_of_m_toggle_5(),
	GUISkin_t1436893342::get_offset_of_m_label_6(),
	GUISkin_t1436893342::get_offset_of_m_textField_7(),
	GUISkin_t1436893342::get_offset_of_m_textArea_8(),
	GUISkin_t1436893342::get_offset_of_m_window_9(),
	GUISkin_t1436893342::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t1436893342::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t1436893342::get_offset_of_m_verticalSlider_12(),
	GUISkin_t1436893342::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t1436893342::get_offset_of_m_ScrollView_22(),
	GUISkin_t1436893342::get_offset_of_m_CustomStyles_23(),
	GUISkin_t1436893342::get_offset_of_m_Settings_24(),
	GUISkin_t1436893342_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t1436893342::get_offset_of_m_Styles_26(),
	GUISkin_t1436893342_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t1436893342_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (SkinChangedDelegate_t3594822336), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (GUIStyleState_t3801000545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[3] = 
{
	GUIStyleState_t3801000545::get_offset_of_m_Ptr_0(),
	GUIStyleState_t3801000545::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t3801000545::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (ImagePosition_t3491916276)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1759[5] = 
{
	ImagePosition_t3491916276::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (GUIStyle_t1799908754), -1, sizeof(GUIStyle_t1799908754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1760[16] = 
{
	GUIStyle_t1799908754::get_offset_of_m_Ptr_0(),
	GUIStyle_t1799908754::get_offset_of_m_Normal_1(),
	GUIStyle_t1799908754::get_offset_of_m_Hover_2(),
	GUIStyle_t1799908754::get_offset_of_m_Active_3(),
	GUIStyle_t1799908754::get_offset_of_m_Focused_4(),
	GUIStyle_t1799908754::get_offset_of_m_OnNormal_5(),
	GUIStyle_t1799908754::get_offset_of_m_OnHover_6(),
	GUIStyle_t1799908754::get_offset_of_m_OnActive_7(),
	GUIStyle_t1799908754::get_offset_of_m_OnFocused_8(),
	GUIStyle_t1799908754::get_offset_of_m_Border_9(),
	GUIStyle_t1799908754::get_offset_of_m_Padding_10(),
	GUIStyle_t1799908754::get_offset_of_m_Margin_11(),
	GUIStyle_t1799908754::get_offset_of_m_Overflow_12(),
	GUIStyle_t1799908754::get_offset_of_m_FontInternal_13(),
	GUIStyle_t1799908754_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t1799908754_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (TextClipping_t2573530411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1761[3] = 
{
	TextClipping_t2573530411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (GUITargetAttribute_t863467180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[1] = 
{
	GUITargetAttribute_t863467180::get_offset_of_displayMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (ExitGUIException_t1618397098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (GUIUtility_t3275770671), -1, sizeof(GUIUtility_t3275770671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1764[4] = 
{
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_U3CguiIsExitingU3Ek__BackingField_2(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_EditorScreenPointOffset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (GUIClip_t3473260597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (ScrollViewState_t3820542997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[5] = 
{
	ScrollViewState_t3820542997::get_offset_of_position_0(),
	ScrollViewState_t3820542997::get_offset_of_visibleRect_1(),
	ScrollViewState_t3820542997::get_offset_of_viewRect_2(),
	ScrollViewState_t3820542997::get_offset_of_scrollPosition_3(),
	ScrollViewState_t3820542997::get_offset_of_apply_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (SliderState_t1595681032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[3] = 
{
	SliderState_t1595681032::get_offset_of_dragStartPos_0(),
	SliderState_t1595681032::get_offset_of_dragStartValue_1(),
	SliderState_t1595681032::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (SliderHandler_t3550500579)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[9] = 
{
	SliderHandler_t3550500579::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_currentValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_start_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_end_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_slider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_thumb_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_horiz_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t3550500579::get_offset_of_id_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (TextEditor_t3975561390), -1, sizeof(TextEditor_t3975561390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1769[24] = 
{
	TextEditor_t3975561390::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t3975561390::get_offset_of_controlID_1(),
	TextEditor_t3975561390::get_offset_of_style_2(),
	TextEditor_t3975561390::get_offset_of_multiline_3(),
	TextEditor_t3975561390::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t3975561390::get_offset_of_isPasswordField_5(),
	TextEditor_t3975561390::get_offset_of_m_HasFocus_6(),
	TextEditor_t3975561390::get_offset_of_scrollOffset_7(),
	TextEditor_t3975561390::get_offset_of_m_Content_8(),
	TextEditor_t3975561390::get_offset_of_m_Position_9(),
	TextEditor_t3975561390::get_offset_of_m_CursorIndex_10(),
	TextEditor_t3975561390::get_offset_of_m_SelectIndex_11(),
	TextEditor_t3975561390::get_offset_of_m_RevealCursor_12(),
	TextEditor_t3975561390::get_offset_of_graphicalCursorPos_13(),
	TextEditor_t3975561390::get_offset_of_graphicalSelectCursorPos_14(),
	TextEditor_t3975561390::get_offset_of_m_MouseDragSelectsWholeWords_15(),
	TextEditor_t3975561390::get_offset_of_m_DblClickInitPos_16(),
	TextEditor_t3975561390::get_offset_of_m_DblClickSnap_17(),
	TextEditor_t3975561390::get_offset_of_m_bJustSelected_18(),
	TextEditor_t3975561390::get_offset_of_m_iAltCursorPos_19(),
	TextEditor_t3975561390::get_offset_of_oldText_20(),
	TextEditor_t3975561390::get_offset_of_oldPos_21(),
	TextEditor_t3975561390::get_offset_of_oldSelectPos_22(),
	TextEditor_t3975561390_StaticFields::get_offset_of_s_Keyactions_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (DblClickSnapping_t1119726228)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1770[3] = 
{
	DblClickSnapping_t1119726228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (CharacterType_t593718391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1771[5] = 
{
	CharacterType_t593718391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (TextEditOp_t3138797698)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1772[51] = 
{
	TextEditOp_t3138797698::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (Internal_DrawArguments_t2834709342)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t2834709342 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[6] = 
{
	Internal_DrawArguments_t2834709342::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (Internal_DrawWithTextSelectionArguments_t1327795077)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t1327795077 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1774[11] = 
{
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (WebRequestUtils_t4100941042), -1, sizeof(WebRequestUtils_t4100941042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1775[1] = 
{
	WebRequestUtils_t4100941042_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (DownloadHandler_t1216180266), sizeof(DownloadHandler_t1216180266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1776[1] = 
{
	DownloadHandler_t1216180266::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (DownloadHandlerScript_t2120336905), sizeof(DownloadHandlerScript_t2120336905_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (DownloadHandlerAudioClip_t1520641178), sizeof(DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (JsonUtility_t653638946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (RemoteSettings_t392466225), -1, sizeof(RemoteSettings_t392466225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1780[1] = 
{
	RemoteSettings_t392466225_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (UpdatedEventHandler_t3033456180), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (VideoPlayer_t10059812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[7] = 
{
	VideoPlayer_t10059812::get_offset_of_prepareCompleted_2(),
	VideoPlayer_t10059812::get_offset_of_loopPointReached_3(),
	VideoPlayer_t10059812::get_offset_of_started_4(),
	VideoPlayer_t10059812::get_offset_of_frameDropped_5(),
	VideoPlayer_t10059812::get_offset_of_errorReceived_6(),
	VideoPlayer_t10059812::get_offset_of_seekCompleted_7(),
	VideoPlayer_t10059812::get_offset_of_frameReady_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (EventHandler_t2685920451), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (ErrorEventHandler_t3983973519), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (FrameReadyEventHandler_t2353988013), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (VideoClip_t3831376279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (ParticleEmitter_t4099167268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (EllipsoidParticleEmitter_t3972767653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (AttributeHelperEngine_t958797062), -1, sizeof(AttributeHelperEngine_t958797062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1789[3] = 
{
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t958797062_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (DisallowMultipleComponent_t2656950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (RequireComponent_t864575032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[3] = 
{
	RequireComponent_t864575032::get_offset_of_m_Type0_0(),
	RequireComponent_t864575032::get_offset_of_m_Type1_1(),
	RequireComponent_t864575032::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (AddComponentMenu_t1099699699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[2] = 
{
	AddComponentMenu_t1099699699::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t1099699699::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (ContextMenu_t2283362202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[3] = 
{
	ContextMenu_t2283362202::get_offset_of_menuItem_0(),
	ContextMenu_t2283362202::get_offset_of_validate_1(),
	ContextMenu_t2283362202::get_offset_of_priority_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (ExecuteInEditMode_t3043633143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (HideInInspector_t2503583610), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (DefaultExecutionOrder_t2717914595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	DefaultExecutionOrder_t2717914595::get_offset_of_U3CorderU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (IL2CPPStructAlignmentAttribute_t130316838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	IL2CPPStructAlignmentAttribute_t130316838::get_offset_of_Align_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (NativeClassAttribute_t1576243993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[1] = 
{
	NativeClassAttribute_t1576243993::get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (GeneratedByOldBindingsGeneratorAttribute_t107439586), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
