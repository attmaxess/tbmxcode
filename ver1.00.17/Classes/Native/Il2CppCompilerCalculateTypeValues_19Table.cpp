﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe4226409784.h"
#include "UnityEngine_UnityEngine_Collections_ReadOnlyAttribu689702060.h"
#include "UnityEngine_UnityEngine_Collections_ReadWriteAttri3403607913.h"
#include "UnityEngine_UnityEngine_Collections_WriteOnlyAttribu14323075.h"
#include "UnityEngine_UnityEngine_Collections_DeallocateOnJob987733588.h"
#include "UnityEngine_UnityEngine_Collections_NativeContainer269240268.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine3267933728.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine1288953595.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Frame658788566.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection301283622.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3517219175.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2252784345.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2167079021.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection536719976.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio2291506050.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1899782350.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Rend984453155.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Scripting_APIUpdating_Moved922195725.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "Boo_Lang_U3CModuleU3E3783534214.h"
#include "Boo_Lang_Boo_Lang_Builtins3763248930.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa2227862569.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1307565918.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispat708950850.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1344382164.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Numer3345193737.h"
#include "Boo_Lang_Boo_Lang_Runtime_ExtensionRegistry1402255936.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CGetEx1600278906.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CCoerce752649758.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CEmitIm187060723.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa2240407071.h"
#include "DOTween_U3CModuleU3E3783534214.h"
#include "DOTween_DG_Tweening_AutoPlay2503223703.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "DOTween_DG_Tweening_EaseFunction3306356708.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_DOTween_U3CU3Ec__DisplayClass53467700650.h"
#include "DOTween_DG_Tweening_DOVirtual1722510550.h"
#include "DOTween_DG_Tweening_DOVirtual_U3CU3Ec__DisplayClas3724530167.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_EaseFactory4203735666.h"
#include "DOTween_DG_Tweening_EaseFactory_U3CU3Ec__DisplayCl2066423765.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "DOTween_DG_Tweening_RotateMode1177727514.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween_DG_Tweening_TweenExtensions405253783.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_Sequence110643099.h"
#include "DOTween_DG_Tweening_ShortcutExtensions3524050470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842963.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842866.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843029.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842932.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842831.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842734.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842897.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842800.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843227.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843130.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978348.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[3] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1911[5] = 
{
	0,
	Vector4_t2243707581::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (ThreadAndSerializationSafeAttribute_t4226409784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (ReadOnlyAttribute_t689702060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (ReadWriteAttribute_t3403607913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (WriteOnlyAttribute_t14323075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (DeallocateOnJobCompletionAttribute_t987733588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (NativeContainerAttribute_t269240268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (NativeContainerSupportsAtomicWriteAttribute_t3267933728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1288953595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1921[6] = 
{
	FrameData_t1120735295::get_offset_of_m_FrameID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_DeltaTime_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Weight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_EffectiveWeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_EffectiveSpeed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Flags_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Flags_t658788566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[3] = 
{
	Flags_t658788566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (MessageEventArgs_t301283622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[2] = 
{
	MessageEventArgs_t301283622::get_offset_of_playerId_0(),
	MessageEventArgs_t301283622::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (PlayerConnection_t3517219175), -1, sizeof(PlayerConnection_t3517219175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1930[3] = 
{
	PlayerConnection_t3517219175::get_offset_of_m_PlayerEditorConnectionEvents_2(),
	PlayerConnection_t3517219175::get_offset_of_m_connectedPlayers_3(),
	PlayerConnection_t3517219175_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (PlayerEditorConnectionEvents_t2252784345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[3] = 
{
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_messageTypeSubscribers_0(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_connectionEvent_1(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_disconnectionEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (MessageEvent_t2167079021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (ConnectionChangeEvent_t536719976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (MessageTypeSubscribers_t2291506050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[3] = 
{
	MessageTypeSubscribers_t2291506050::get_offset_of_m_messageTypeId_0(),
	MessageTypeSubscribers_t2291506050::get_offset_of_subscriberCount_1(),
	MessageTypeSubscribers_t2291506050::get_offset_of_messageCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[1] = 
{
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350::get_offset_of_messageId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (RenderPipelineManager_t984453155), -1, sizeof(RenderPipelineManager_t984453155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1938[2] = 
{
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_s_CurrentPipelineAsset_0(),
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (PreserveAttribute_t4182602970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (MovedFromAttribute_t922195725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[2] = 
{
	MovedFromAttribute_t922195725::get_offset_of_U3CNamespaceU3Ek__BackingField_0(),
	MovedFromAttribute_t922195725::get_offset_of_U3CIsInDifferentAssemblyU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1944[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (Builtins_t3763248930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (DispatcherCache_t2227862569), -1, sizeof(DispatcherCache_t2227862569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1954[1] = 
{
	DispatcherCache_t2227862569_StaticFields::get_offset_of__cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (DispatcherFactory_t1307565918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (DispatcherKey_t708950850), -1, sizeof(DispatcherKey_t708950850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1956[4] = 
{
	DispatcherKey_t708950850_StaticFields::get_offset_of_EqualityComparer_0(),
	DispatcherKey_t708950850::get_offset_of__type_1(),
	DispatcherKey_t708950850::get_offset_of__name_2(),
	DispatcherKey_t708950850::get_offset_of__arguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (_EqualityComparer_t1344382164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (NumericPromotions_t3345193737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (ExtensionRegistry_t1402255936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[2] = 
{
	ExtensionRegistry_t1402255936::get_offset_of__extensions_0(),
	ExtensionRegistry_t1402255936::get_offset_of__classLock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (RuntimeServices_t1910041954), -1, sizeof(RuntimeServices_t1910041954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1961[5] = 
{
	RuntimeServices_t1910041954_StaticFields::get_offset_of_NoArguments_0(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of_RuntimeServicesType_1(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of__cache_2(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of__extensions_3(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of_True_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[4] = 
{
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U3CU24s_49U3E__0_0(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U3CmemberU3E__1_1(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U24PC_2(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U24current_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (U3CCoerceU3Ec__AnonStorey1D_t752649758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[2] = 
{
	U3CCoerceU3Ec__AnonStorey1D_t752649758::get_offset_of_value_0(),
	U3CCoerceU3Ec__AnonStorey1D_t752649758::get_offset_of_toType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[1] = 
{
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (Dispatcher_t2240407071), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (AutoPlay_t2503223703)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1967[5] = 
{
	AutoPlay_t2503223703::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (AxisConstraint_t1244566668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1968[6] = 
{
	AxisConstraint_t1244566668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (Color2_t232726623)+ sizeof (Il2CppObject), sizeof(Color2_t232726623 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1969[2] = 
{
	Color2_t232726623::get_offset_of_ca_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color2_t232726623::get_offset_of_cb_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (TweenCallback_t3697142134), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (EaseFunction_t3306356708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (DOTween_t2276353038), -1, sizeof(DOTween_t2276353038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1973[24] = 
{
	DOTween_t2276353038_StaticFields::get_offset_of_Version_0(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t2276353038_StaticFields::get_offset_of_showUnityEditorReport_2(),
	DOTween_t2276353038_StaticFields::get_offset_of_timeScale_3(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSmoothDeltaTime_4(),
	DOTween_t2276353038_StaticFields::get_offset_of__logBehaviour_5(),
	DOTween_t2276353038_StaticFields::get_offset_of_drawGizmos_6(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultUpdateType_7(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultTimeScaleIndependent_8(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoPlay_9(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoKill_10(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultLoopType_11(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultRecyclable_12(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseType_13(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_14(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEasePeriod_15(),
	DOTween_t2276353038_StaticFields::get_offset_of_instance_16(),
	DOTween_t2276353038_StaticFields::get_offset_of_isUnityEditor_17(),
	DOTween_t2276353038_StaticFields::get_offset_of_isDebugBuild_18(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveTweenersReached_19(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveSequencesReached_20(),
	DOTween_t2276353038_StaticFields::get_offset_of_GizmosDelegates_21(),
	DOTween_t2276353038_StaticFields::get_offset_of_initialized_22(),
	DOTween_t2276353038_StaticFields::get_offset_of_isQuitting_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (U3CU3Ec__DisplayClass52_0_t3467700650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[2] = 
{
	U3CU3Ec__DisplayClass52_0_t3467700650::get_offset_of_v_0(),
	U3CU3Ec__DisplayClass52_0_t3467700650::get_offset_of_setter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (DOVirtual_t1722510550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (U3CU3Ec__DisplayClass0_0_t3724530167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[2] = 
{
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_val_0(),
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_onVirtualUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (Ease_t2502520296)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[39] = 
{
	Ease_t2502520296::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (EaseFactory_t4203735666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (U3CU3Ec__DisplayClass2_0_t2066423765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[2] = 
{
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_motionDelay_0(),
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_customEase_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (PathMode_t1545785466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1981[5] = 
{
	PathMode_t1545785466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (PathType_t2815988833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1982[3] = 
{
	PathType_t2815988833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (RotateMode_t1177727514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1983[5] = 
{
	RotateMode_t1177727514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (ScrambleMode_t385206138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1984[7] = 
{
	ScrambleMode_t385206138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (TweenExtensions_t405253783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (LoopType_t2249218064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[4] = 
{
	LoopType_t2249218064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (Sequence_t110643099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[3] = 
{
	Sequence_t110643099::get_offset_of_sequencedTweens_51(),
	Sequence_t110643099::get_offset_of__sequencedObjs_52(),
	Sequence_t110643099::get_offset_of_lastTweenInsertTime_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (ShortcutExtensions_t3524050470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (U3CU3Ec__DisplayClass0_0_t964842963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[1] = 
{
	U3CU3Ec__DisplayClass0_0_t964842963::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (U3CU3Ec__DisplayClass1_0_t964842866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[1] = 
{
	U3CU3Ec__DisplayClass1_0_t964842866::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (U3CU3Ec__DisplayClass2_0_t964843029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[1] = 
{
	U3CU3Ec__DisplayClass2_0_t964843029::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (U3CU3Ec__DisplayClass3_0_t964842932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[1] = 
{
	U3CU3Ec__DisplayClass3_0_t964842932::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (U3CU3Ec__DisplayClass4_0_t964842831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[1] = 
{
	U3CU3Ec__DisplayClass4_0_t964842831::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (U3CU3Ec__DisplayClass5_0_t964842734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[1] = 
{
	U3CU3Ec__DisplayClass5_0_t964842734::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (U3CU3Ec__DisplayClass6_0_t964842897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[1] = 
{
	U3CU3Ec__DisplayClass6_0_t964842897::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (U3CU3Ec__DisplayClass7_0_t964842800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[1] = 
{
	U3CU3Ec__DisplayClass7_0_t964842800::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (U3CU3Ec__DisplayClass8_0_t964843227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[1] = 
{
	U3CU3Ec__DisplayClass8_0_t964843227::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (U3CU3Ec__DisplayClass9_0_t964843130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[1] = 
{
	U3CU3Ec__DisplayClass9_0_t964843130::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (U3CU3Ec__DisplayClass10_0_t3010978348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[1] = 
{
	U3CU3Ec__DisplayClass10_0_t3010978348::get_offset_of_target_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
