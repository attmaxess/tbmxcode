﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691431.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653346.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366429.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628352.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341435.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303350.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678340.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391423.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978441.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691524.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653439.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366522.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628445.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341528.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303443.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016526.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391516.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978538.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691621.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653536.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366619.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628542.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341625.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303540.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016623.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678530.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391613.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978383.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691466.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653381.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366464.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628387.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303385.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016468.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678375.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391458.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978224.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691307.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653222.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366305.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628228.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341311.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303226.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678216.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391299.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978317.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691400.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653315.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366398.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628321.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341404.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303319.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016402.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391392.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978414.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691497.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653412.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366495.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628418.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341501.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303416.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016499.h"
#include "DOTween_DG_Tweening_TweenParams2944325381.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "DOTween_DG_Tweening_LogBehaviour3505725029.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"
#include "DOTween_DG_Tweening_Plugins_Color2Plugin3433430606.h"
#include "DOTween_DG_Tweening_Plugins_DoublePlugin266400784.h"
#include "DOTween_DG_Tweening_Plugins_LongPlugin1941283029.h"
#include "DOTween_DG_Tweening_Plugins_UlongPlugin3231465400.h"
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin2378569512.h"
#include "DOTween_DG_Tweening_Plugins_PathPlugin4171842066.h"
#include "DOTween_DG_Tweening_Plugins_ColorPlugin4063724482.h"
#include "DOTween_DG_Tweening_Plugins_IntPlugin180838436.h"
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin1696644323.h"
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin664509336.h"
#include "DOTween_DG_Tweening_Plugins_RectPlugin391797831.h"
#include "DOTween_DG_Tweening_Plugins_UintPlugin1040977389.h"
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin2164285386.h"
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin2164361360.h"
#include "DOTween_DG_Tweening_Plugins_StringPlugin3620786088.h"
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions3910942986.h"
#include "DOTween_DG_Tweening_Plugins_FloatPlugin3639480371.h"
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin2164530409.h"
#include "DOTween_DG_Tweening_Plugins_Options_OrientType1755667719.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (U3CU3Ec__DisplayClass11_0_t1424691431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[1] = 
{
	U3CU3Ec__DisplayClass11_0_t1424691431::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (U3CU3Ec__DisplayClass12_0_t2728653346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2728653346::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (U3CU3Ec__DisplayClass13_0_t1142366429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[1] = 
{
	U3CU3Ec__DisplayClass13_0_t1142366429::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (U3CU3Ec__DisplayClass14_0_t3575628352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[1] = 
{
	U3CU3Ec__DisplayClass14_0_t3575628352::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (U3CU3Ec__DisplayClass15_0_t1989341435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[1] = 
{
	U3CU3Ec__DisplayClass15_0_t1989341435::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (U3CU3Ec__DisplayClass16_0_t3293303350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3293303350::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (U3CU3Ec__DisplayClass17_0_t1707016433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[2] = 
{
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_startValue_0(),
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (U3CU3Ec__DisplayClass18_0_t1881678340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[1] = 
{
	U3CU3Ec__DisplayClass18_0_t1881678340::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CU3Ec__DisplayClass19_0_t295391423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (U3CU3Ec__DisplayClass20_0_t3010978441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[1] = 
{
	U3CU3Ec__DisplayClass20_0_t3010978441::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (U3CU3Ec__DisplayClass21_0_t1424691524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[2] = 
{
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (U3CU3Ec__DisplayClass22_0_t2728653439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[2] = 
{
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CU3Ec__DisplayClass23_0_t1142366522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[1] = 
{
	U3CU3Ec__DisplayClass23_0_t1142366522::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (U3CU3Ec__DisplayClass24_0_t3575628445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[2] = 
{
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (U3CU3Ec__DisplayClass25_0_t1989341528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[1] = 
{
	U3CU3Ec__DisplayClass25_0_t1989341528::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (U3CU3Ec__DisplayClass26_0_t3293303443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (U3CU3Ec__DisplayClass27_0_t1707016526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[2] = 
{
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (U3CU3Ec__DisplayClass28_0_t1881678433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1881678433::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (U3CU3Ec__DisplayClass29_0_t295391516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[1] = 
{
	U3CU3Ec__DisplayClass29_0_t295391516::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CU3Ec__DisplayClass30_0_t3010978538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[1] = 
{
	U3CU3Ec__DisplayClass30_0_t3010978538::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U3CU3Ec__DisplayClass31_0_t1424691621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[1] = 
{
	U3CU3Ec__DisplayClass31_0_t1424691621::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U3CU3Ec__DisplayClass32_0_t2728653536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[1] = 
{
	U3CU3Ec__DisplayClass32_0_t2728653536::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (U3CU3Ec__DisplayClass33_0_t1142366619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[1] = 
{
	U3CU3Ec__DisplayClass33_0_t1142366619::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U3CU3Ec__DisplayClass34_0_t3575628542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[6] = 
{
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U3CU3Ec__DisplayClass35_0_t1989341625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[1] = 
{
	U3CU3Ec__DisplayClass35_0_t1989341625::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (U3CU3Ec__DisplayClass36_0_t3293303540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[1] = 
{
	U3CU3Ec__DisplayClass36_0_t3293303540::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (U3CU3Ec__DisplayClass37_0_t1707016623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[1] = 
{
	U3CU3Ec__DisplayClass37_0_t1707016623::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (U3CU3Ec__DisplayClass38_0_t1881678530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[1] = 
{
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (U3CU3Ec__DisplayClass39_0_t295391613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[1] = 
{
	U3CU3Ec__DisplayClass39_0_t295391613::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (U3CU3Ec__DisplayClass40_0_t3010978383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[1] = 
{
	U3CU3Ec__DisplayClass40_0_t3010978383::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (U3CU3Ec__DisplayClass41_0_t1424691466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[1] = 
{
	U3CU3Ec__DisplayClass41_0_t1424691466::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (U3CU3Ec__DisplayClass42_0_t2728653381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[1] = 
{
	U3CU3Ec__DisplayClass42_0_t2728653381::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (U3CU3Ec__DisplayClass43_0_t1142366464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[1] = 
{
	U3CU3Ec__DisplayClass43_0_t1142366464::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (U3CU3Ec__DisplayClass44_0_t3575628387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[1] = 
{
	U3CU3Ec__DisplayClass44_0_t3575628387::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (U3CU3Ec__DisplayClass45_0_t1989341470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[1] = 
{
	U3CU3Ec__DisplayClass45_0_t1989341470::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (U3CU3Ec__DisplayClass46_0_t3293303385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[1] = 
{
	U3CU3Ec__DisplayClass46_0_t3293303385::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (U3CU3Ec__DisplayClass47_0_t1707016468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[1] = 
{
	U3CU3Ec__DisplayClass47_0_t1707016468::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (U3CU3Ec__DisplayClass48_0_t1881678375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[1] = 
{
	U3CU3Ec__DisplayClass48_0_t1881678375::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (U3CU3Ec__DisplayClass49_0_t295391458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[1] = 
{
	U3CU3Ec__DisplayClass49_0_t295391458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (U3CU3Ec__DisplayClass50_0_t3010978224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[1] = 
{
	U3CU3Ec__DisplayClass50_0_t3010978224::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (U3CU3Ec__DisplayClass51_0_t1424691307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[1] = 
{
	U3CU3Ec__DisplayClass51_0_t1424691307::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (U3CU3Ec__DisplayClass52_0_t2728653222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[1] = 
{
	U3CU3Ec__DisplayClass52_0_t2728653222::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (U3CU3Ec__DisplayClass53_0_t1142366305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	U3CU3Ec__DisplayClass53_0_t1142366305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (U3CU3Ec__DisplayClass54_0_t3575628228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	U3CU3Ec__DisplayClass54_0_t3575628228::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (U3CU3Ec__DisplayClass55_0_t1989341311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[1] = 
{
	U3CU3Ec__DisplayClass55_0_t1989341311::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (U3CU3Ec__DisplayClass56_0_t3293303226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[1] = 
{
	U3CU3Ec__DisplayClass56_0_t3293303226::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (U3CU3Ec__DisplayClass57_0_t1707016309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[1] = 
{
	U3CU3Ec__DisplayClass57_0_t1707016309::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (U3CU3Ec__DisplayClass58_0_t1881678216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	U3CU3Ec__DisplayClass58_0_t1881678216::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (U3CU3Ec__DisplayClass59_0_t295391299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[1] = 
{
	U3CU3Ec__DisplayClass59_0_t295391299::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (U3CU3Ec__DisplayClass60_0_t3010978317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[1] = 
{
	U3CU3Ec__DisplayClass60_0_t3010978317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (U3CU3Ec__DisplayClass61_0_t1424691400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[1] = 
{
	U3CU3Ec__DisplayClass61_0_t1424691400::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (U3CU3Ec__DisplayClass62_0_t2728653315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[1] = 
{
	U3CU3Ec__DisplayClass62_0_t2728653315::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (U3CU3Ec__DisplayClass63_0_t1142366398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[1] = 
{
	U3CU3Ec__DisplayClass63_0_t1142366398::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (U3CU3Ec__DisplayClass64_0_t3575628321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[6] = 
{
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (U3CU3Ec__DisplayClass65_0_t1989341404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[6] = 
{
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (U3CU3Ec__DisplayClass66_0_t3293303319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[1] = 
{
	U3CU3Ec__DisplayClass66_0_t3293303319::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (U3CU3Ec__DisplayClass67_0_t1707016402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[1] = 
{
	U3CU3Ec__DisplayClass67_0_t1707016402::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (U3CU3Ec__DisplayClass68_0_t1881678309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[1] = 
{
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (U3CU3Ec__DisplayClass69_0_t295391392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[1] = 
{
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (U3CU3Ec__DisplayClass70_0_t3010978414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[2] = 
{
	U3CU3Ec__DisplayClass70_0_t3010978414::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass70_0_t3010978414::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (U3CU3Ec__DisplayClass71_0_t1424691497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[2] = 
{
	U3CU3Ec__DisplayClass71_0_t1424691497::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass71_0_t1424691497::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (U3CU3Ec__DisplayClass72_0_t2728653412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[3] = 
{
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_property_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (U3CU3Ec__DisplayClass73_0_t1142366495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[2] = 
{
	U3CU3Ec__DisplayClass73_0_t1142366495::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass73_0_t1142366495::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (U3CU3Ec__DisplayClass74_0_t3575628418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[2] = 
{
	U3CU3Ec__DisplayClass74_0_t3575628418::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass74_0_t3575628418::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (U3CU3Ec__DisplayClass75_0_t1989341501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[2] = 
{
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (U3CU3Ec__DisplayClass76_0_t3293303416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[2] = 
{
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (U3CU3Ec__DisplayClass77_0_t1707016499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[2] = 
{
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (TweenParams_t2944325381), -1, sizeof(TweenParams_t2944325381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2067[24] = 
{
	TweenParams_t2944325381_StaticFields::get_offset_of_Params_0(),
	TweenParams_t2944325381::get_offset_of_id_1(),
	TweenParams_t2944325381::get_offset_of_target_2(),
	TweenParams_t2944325381::get_offset_of_updateType_3(),
	TweenParams_t2944325381::get_offset_of_isIndependentUpdate_4(),
	TweenParams_t2944325381::get_offset_of_onStart_5(),
	TweenParams_t2944325381::get_offset_of_onPlay_6(),
	TweenParams_t2944325381::get_offset_of_onRewind_7(),
	TweenParams_t2944325381::get_offset_of_onUpdate_8(),
	TweenParams_t2944325381::get_offset_of_onStepComplete_9(),
	TweenParams_t2944325381::get_offset_of_onComplete_10(),
	TweenParams_t2944325381::get_offset_of_onKill_11(),
	TweenParams_t2944325381::get_offset_of_onWaypointChange_12(),
	TweenParams_t2944325381::get_offset_of_isRecyclable_13(),
	TweenParams_t2944325381::get_offset_of_isSpeedBased_14(),
	TweenParams_t2944325381::get_offset_of_autoKill_15(),
	TweenParams_t2944325381::get_offset_of_loops_16(),
	TweenParams_t2944325381::get_offset_of_loopType_17(),
	TweenParams_t2944325381::get_offset_of_delay_18(),
	TweenParams_t2944325381::get_offset_of_isRelative_19(),
	TweenParams_t2944325381::get_offset_of_easeType_20(),
	TweenParams_t2944325381::get_offset_of_customEase_21(),
	TweenParams_t2944325381::get_offset_of_easeOvershootOrAmplitude_22(),
	TweenParams_t2944325381::get_offset_of_easePeriod_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (TweenSettingsExtensions_t2285462830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (LogBehaviour_t3505725029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2069[4] = 
{
	LogBehaviour_t3505725029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (Tween_t278478013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[47] = 
{
	Tween_t278478013::get_offset_of_timeScale_4(),
	Tween_t278478013::get_offset_of_isBackwards_5(),
	Tween_t278478013::get_offset_of_id_6(),
	Tween_t278478013::get_offset_of_target_7(),
	Tween_t278478013::get_offset_of_updateType_8(),
	Tween_t278478013::get_offset_of_isIndependentUpdate_9(),
	Tween_t278478013::get_offset_of_onPlay_10(),
	Tween_t278478013::get_offset_of_onPause_11(),
	Tween_t278478013::get_offset_of_onRewind_12(),
	Tween_t278478013::get_offset_of_onUpdate_13(),
	Tween_t278478013::get_offset_of_onStepComplete_14(),
	Tween_t278478013::get_offset_of_onComplete_15(),
	Tween_t278478013::get_offset_of_onKill_16(),
	Tween_t278478013::get_offset_of_onWaypointChange_17(),
	Tween_t278478013::get_offset_of_isFrom_18(),
	Tween_t278478013::get_offset_of_isBlendable_19(),
	Tween_t278478013::get_offset_of_isRecyclable_20(),
	Tween_t278478013::get_offset_of_isSpeedBased_21(),
	Tween_t278478013::get_offset_of_autoKill_22(),
	Tween_t278478013::get_offset_of_duration_23(),
	Tween_t278478013::get_offset_of_loops_24(),
	Tween_t278478013::get_offset_of_loopType_25(),
	Tween_t278478013::get_offset_of_delay_26(),
	Tween_t278478013::get_offset_of_isRelative_27(),
	Tween_t278478013::get_offset_of_easeType_28(),
	Tween_t278478013::get_offset_of_customEase_29(),
	Tween_t278478013::get_offset_of_easeOvershootOrAmplitude_30(),
	Tween_t278478013::get_offset_of_easePeriod_31(),
	Tween_t278478013::get_offset_of_typeofT1_32(),
	Tween_t278478013::get_offset_of_typeofT2_33(),
	Tween_t278478013::get_offset_of_typeofTPlugOptions_34(),
	Tween_t278478013::get_offset_of_active_35(),
	Tween_t278478013::get_offset_of_isSequenced_36(),
	Tween_t278478013::get_offset_of_sequenceParent_37(),
	Tween_t278478013::get_offset_of_activeId_38(),
	Tween_t278478013::get_offset_of_specialStartupMode_39(),
	Tween_t278478013::get_offset_of_creationLocked_40(),
	Tween_t278478013::get_offset_of_startupDone_41(),
	Tween_t278478013::get_offset_of_playedOnce_42(),
	Tween_t278478013::get_offset_of_position_43(),
	Tween_t278478013::get_offset_of_fullDuration_44(),
	Tween_t278478013::get_offset_of_completedLoops_45(),
	Tween_t278478013::get_offset_of_isPlaying_46(),
	Tween_t278478013::get_offset_of_isComplete_47(),
	Tween_t278478013::get_offset_of_elapsedDelay_48(),
	Tween_t278478013::get_offset_of_delayComplete_49(),
	Tween_t278478013::get_offset_of_miscInt_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (Tweener_t760404022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[2] = 
{
	Tweener_t760404022::get_offset_of_hasManuallySetStartValue_51(),
	Tweener_t760404022::get_offset_of_isFromAllowed_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (TweenType_t169444141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2072[4] = 
{
	TweenType_t169444141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (UpdateType_t3357224513)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2073[4] = 
{
	UpdateType_t3357224513::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (Color2Plugin_t3433430606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (DoublePlugin_t266400784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (LongPlugin_t1941283029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (UlongPlugin_t3231465400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (Vector3ArrayPlugin_t2378569512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (PathPlugin_t4171842066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (ColorPlugin_t4063724482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (IntPlugin_t180838436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (QuaternionPlugin_t1696644323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (RectOffsetPlugin_t664509336), -1, sizeof(RectOffsetPlugin_t664509336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2083[1] = 
{
	RectOffsetPlugin_t664509336_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (RectPlugin_t391797831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (UintPlugin_t1040977389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (Vector2Plugin_t2164285386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (Vector4Plugin_t2164361360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (StringPlugin_t3620786088), -1, sizeof(StringPlugin_t3620786088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2088[2] = 
{
	StringPlugin_t3620786088_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t3620786088_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (StringPluginExtensions_t3910942986), -1, sizeof(StringPluginExtensions_t3910942986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2089[5] = 
{
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (FloatPlugin_t3639480371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (Vector3Plugin_t2164530409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (OrientType_t1755667719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[5] = 
{
	OrientType_t1755667719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (PathOptions_t2659884781)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[14] = 
{
	PathOptions_t2659884781::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_orientType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockPositionAxis_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockRotationAxis_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isClosedPath_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtPosition_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtTransform_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAhead_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_hasCustomForwardDirection_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_forward_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_useLocalPosition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_parent_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupRot_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupZRot_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (QuaternionOptions_t466049668)+ sizeof (Il2CppObject), sizeof(QuaternionOptions_t466049668 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2094[3] = 
{
	QuaternionOptions_t466049668::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_up_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (UintOptions_t2267095136)+ sizeof (Il2CppObject), sizeof(UintOptions_t2267095136_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[1] = 
{
	UintOptions_t2267095136::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (Vector3ArrayOptions_t2672570171)+ sizeof (Il2CppObject), sizeof(Vector3ArrayOptions_t2672570171_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2096[3] = 
{
	Vector3ArrayOptions_t2672570171::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (NoOptions_t2508431845)+ sizeof (Il2CppObject), sizeof(NoOptions_t2508431845 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (ColorOptions_t2213017305)+ sizeof (Il2CppObject), sizeof(ColorOptions_t2213017305_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[1] = 
{
	ColorOptions_t2213017305::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (FloatOptions_t1421548266)+ sizeof (Il2CppObject), sizeof(FloatOptions_t1421548266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[1] = 
{
	FloatOptions_t1421548266::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
