﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUti2241999250.h"
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManager3052451537.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ControlPo168081159.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ABSPathD3294469411.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_CatmullR3014762178.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_LinearDe2073524639.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_Path2828565993.h"
#include "DOTween_DG_Tweening_CustomPlugins_PureQuaternionPl3400666973.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent696744215.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitFo639731943.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2782210651.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF1007991819.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF3988581919.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2034437344.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2341562412.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings873123119.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLo514961325.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800.h"
#include "DOTween_DG_Tweening_Core_SequenceCallback2782183128.h"
#include "DOTween_DG_Tweening_Core_TweenManager1979661952.h"
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncr1969140739.h"
#include "DOTween_DG_Tweening_Core_Utils2524017187.h"
#include "DOTween_DG_Tweening_Core_Enums_FilterType1425068526.h"
#include "DOTween_DG_Tweening_Core_Enums_OperationType2600045009.h"
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode1501334721.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice2468589887.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode2539919096.h"
#include "DOTween_DG_Tweening_Core_Easing_Bounce3273339050.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseManager1514337917.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseManager_U3CU3E1609106043.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseCurve1295352409.h"
#include "DOTween_DG_Tweening_Core_Easing_Flash1282698556.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Static978476011.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Stati3707359366.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Stati1468992140.h"
#include "Firebase_App_U3CModuleU3E3783534214.h"
#include "Firebase_App_MonoPInvokeCallbackAttribute1970456718.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE3515465473.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException1412130252.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2876249339.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2443153790.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGPendingEx4193433529.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelp549488518.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelpe18001273.h"
#include "Firebase_App_Firebase_AppUtil2859790353.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler2907300047.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_M323097478.h"
#include "Firebase_App_Firebase_FirebaseApp_DestroyDelegate3635929227.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext1051733884.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_S692674473.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1310454857.h"
#include "Firebase_App_Firebase_UnityHttpRequest967994112.h"
#include "Firebase_App_Firebase_UnityHttpRequest_StreamingDo1548287308.h"
#include "Firebase_App_Firebase_UnityDownloadStream3205965147.h"
#include "Firebase_App_Firebase_FirebaseHttpRequest1911360314.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (RectOptions_t3393635162)+ sizeof (Il2CppObject), sizeof(RectOptions_t3393635162_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[1] = 
{
	RectOptions_t3393635162::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (StringOptions_t2885323933)+ sizeof (Il2CppObject), sizeof(StringOptions_t2885323933_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2101[5] = 
{
	StringOptions_t2885323933::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (VectorOptions_t293385261)+ sizeof (Il2CppObject), sizeof(VectorOptions_t293385261_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2102[2] = 
{
	VectorOptions_t293385261::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VectorOptions_t293385261::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (SpecialPluginsUtils_t2241999250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (PluginsManager_t3052451537), -1, sizeof(PluginsManager_t3052451537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2108[18] = 
{
	PluginsManager_t3052451537_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__color2Plugin_15(),
	0,
	PluginsManager_t3052451537_StaticFields::get_offset_of__customPlugins_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (ControlPoint_t168081159)+ sizeof (Il2CppObject), sizeof(ControlPoint_t168081159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2109[2] = 
{
	ControlPoint_t168081159::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControlPoint_t168081159::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (ABSPathDecoder_t3294469411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (CatmullRomDecoder_t3014762178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (LinearDecoder_t2073524639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (Path_t2828565993), -1, sizeof(Path_t2828565993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2113[21] = 
{
	Path_t2828565993_StaticFields::get_offset_of__catmullRomDecoder_0(),
	Path_t2828565993_StaticFields::get_offset_of__linearDecoder_1(),
	Path_t2828565993::get_offset_of_type_2(),
	Path_t2828565993::get_offset_of_subdivisionsXSegment_3(),
	Path_t2828565993::get_offset_of_subdivisions_4(),
	Path_t2828565993::get_offset_of_wps_5(),
	Path_t2828565993::get_offset_of_controlPoints_6(),
	Path_t2828565993::get_offset_of_length_7(),
	Path_t2828565993::get_offset_of_wpLengths_8(),
	Path_t2828565993::get_offset_of_isFinalized_9(),
	Path_t2828565993::get_offset_of_timesTable_10(),
	Path_t2828565993::get_offset_of_lengthsTable_11(),
	Path_t2828565993::get_offset_of_linearWPIndex_12(),
	Path_t2828565993::get_offset_of__incrementalClone_13(),
	Path_t2828565993::get_offset_of__incrementalIndex_14(),
	Path_t2828565993::get_offset_of__decoder_15(),
	Path_t2828565993::get_offset_of__changed_16(),
	Path_t2828565993::get_offset_of_nonLinearDrawWps_17(),
	Path_t2828565993::get_offset_of_targetPosition_18(),
	Path_t2828565993::get_offset_of_lookAtPosition_19(),
	Path_t2828565993::get_offset_of_gizmoColor_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (PureQuaternionPlugin_t3400666973), -1, sizeof(PureQuaternionPlugin_t3400666973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2114[1] = 
{
	PureQuaternionPlugin_t3400666973_StaticFields::get_offset_of__plug_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (ABSSequentiable_t2284140720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[4] = 
{
	ABSSequentiable_t2284140720::get_offset_of_tweenType_0(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_t2284140720::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (Debugger_t1404542751), -1, sizeof(Debugger_t1404542751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2118[1] = 
{
	Debugger_t1404542751_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (DOTweenComponent_t696744215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[4] = 
{
	DOTweenComponent_t696744215::get_offset_of_inspectorUpdater_2(),
	DOTweenComponent_t696744215::get_offset_of__unscaledTime_3(),
	DOTweenComponent_t696744215::get_offset_of__unscaledDeltaTime_4(),
	DOTweenComponent_t696744215::get_offset_of__duplicateToDestroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (U3CWaitForCompletionU3Ed__13_t639731943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[3] = 
{
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (U3CWaitForRewindU3Ed__14_t2782210651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[3] = 
{
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (U3CWaitForKillU3Ed__15_t1007991819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[3] = 
{
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (U3CWaitForElapsedLoopsU3Ed__16_t3988581919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[4] = 
{
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_t_2(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_elapsedLoops_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (U3CWaitForPositionU3Ed__17_t2034437344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[4] = 
{
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_t_2(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (U3CWaitForStartU3Ed__18_t2341562412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[3] = 
{
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (DOTweenSettings_t873123119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[17] = 
{
	0,
	DOTweenSettings_t873123119::get_offset_of_useSafeMode_3(),
	DOTweenSettings_t873123119::get_offset_of_timeScale_4(),
	DOTweenSettings_t873123119::get_offset_of_useSmoothDeltaTime_5(),
	DOTweenSettings_t873123119::get_offset_of_showUnityEditorReport_6(),
	DOTweenSettings_t873123119::get_offset_of_logBehaviour_7(),
	DOTweenSettings_t873123119::get_offset_of_drawGizmos_8(),
	DOTweenSettings_t873123119::get_offset_of_defaultRecyclable_9(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoPlay_10(),
	DOTweenSettings_t873123119::get_offset_of_defaultUpdateType_11(),
	DOTweenSettings_t873123119::get_offset_of_defaultTimeScaleIndependent_12(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseType_13(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseOvershootOrAmplitude_14(),
	DOTweenSettings_t873123119::get_offset_of_defaultEasePeriod_15(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoKill_16(),
	DOTweenSettings_t873123119::get_offset_of_defaultLoopType_17(),
	DOTweenSettings_t873123119::get_offset_of_storeSettingsLocation_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (SettingsLocation_t514961325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2127[4] = 
{
	SettingsLocation_t514961325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (Extensions_t507052800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (SequenceCallback_t2782183128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (TweenManager_t1979661952), -1, sizeof(TweenManager_t1979661952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[31] = 
{
	0,
	0,
	0,
	TweenManager_t1979661952_StaticFields::get_offset_of_maxActive_3(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxTweeners_4(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxSequences_5(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveTweens_6(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveDefaultTweens_7(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveLateTweens_8(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveFixedTweens_9(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweens_10(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveDefaultTweens_11(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveLateTweens_12(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveFixedTweens_13(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweeners_14(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveSequences_15(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledTweeners_16(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledSequences_17(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totTweeners_18(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totSequences_19(),
	TweenManager_t1979661952_StaticFields::get_offset_of_isUpdateLoop_20(),
	TweenManager_t1979661952_StaticFields::get_offset_of__activeTweens_21(),
	TweenManager_t1979661952_StaticFields::get_offset_of__pooledTweeners_22(),
	TweenManager_t1979661952_StaticFields::get_offset_of__PooledSequences_23(),
	TweenManager_t1979661952_StaticFields::get_offset_of__KillList_24(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxActiveLookupId_25(),
	TweenManager_t1979661952_StaticFields::get_offset_of__requiresActiveReorganization_26(),
	TweenManager_t1979661952_StaticFields::get_offset_of__reorganizeFromId_27(),
	TweenManager_t1979661952_StaticFields::get_offset_of__minPooledTweenerId_28(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxPooledTweenerId_29(),
	TweenManager_t1979661952_StaticFields::get_offset_of__despawnAllCalledFromUpdateLoopCallback_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (CapacityIncreaseMode_t1969140739)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2131[4] = 
{
	CapacityIncreaseMode_t1969140739::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (Utils_t2524017187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (FilterType_t1425068526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2134[6] = 
{
	FilterType_t1425068526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (OperationType_t2600045009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2135[14] = 
{
	OperationType_t2600045009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (SpecialStartupMode_t1501334721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2136[6] = 
{
	SpecialStartupMode_t1501334721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (UpdateNotice_t2468589887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2137[3] = 
{
	UpdateNotice_t2468589887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (UpdateMode_t2539919096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2138[4] = 
{
	UpdateMode_t2539919096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (Bounce_t3273339050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (EaseManager_t1514337917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (U3CU3Ec_t1609106043), -1, sizeof(U3CU3Ec_t1609106043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2141[37] = 
{
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_1_2(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_2_3(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_3_4(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_4_5(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_5_6(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_6_7(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_7_8(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_8_9(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_9_10(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_10_11(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_11_12(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_12_13(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_13_14(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_14_15(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_15_16(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_16_17(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_17_18(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_18_19(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_19_20(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_20_21(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_21_22(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_22_23(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_23_24(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_24_25(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_25_26(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_26_27(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_27_28(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_28_29(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_29_30(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_30_31(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_31_32(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_32_33(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_33_34(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_34_35(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_35_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (EaseCurve_t1295352409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[1] = 
{
	EaseCurve_t1295352409::get_offset_of__animCurve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (Flash_t1282698556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2144[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (__StaticArrayInitTypeSizeU3D20_t978476011)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D20_t978476011 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (__StaticArrayInitTypeSizeU3D50_t3707359366)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D50_t3707359366 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (__StaticArrayInitTypeSizeU3D120_t1468992140)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D120_t1468992140 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (MonoPInvokeCallbackAttribute_t1970456718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (AppUtilPINVOKE_t3515465473), -1, sizeof(AppUtilPINVOKE_t3515465473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2150[2] = 
{
	AppUtilPINVOKE_t3515465473_StaticFields::get_offset_of_swigExceptionHelper_0(),
	AppUtilPINVOKE_t3515465473_StaticFields::get_offset_of_swigStringHelper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (SWIGExceptionHelper_t1412130252), -1, sizeof(SWIGExceptionHelper_t1412130252_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2151[14] = 
{
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_applicationDelegate_0(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_arithmeticDelegate_1(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_divideByZeroDelegate_2(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_indexOutOfRangeDelegate_3(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_invalidCastDelegate_4(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_invalidOperationDelegate_5(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_ioDelegate_6(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_nullReferenceDelegate_7(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_outOfMemoryDelegate_8(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_overflowDelegate_9(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_systemDelegate_10(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentDelegate_11(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentNullDelegate_12(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentOutOfRangeDelegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (ExceptionDelegate_t2876249339), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (ExceptionArgumentDelegate_t2443153790), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (SWIGPendingException_t4193433529), -1, sizeof(SWIGPendingException_t4193433529_StaticFields), sizeof(SWIGPendingException_t4193433529_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2154[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	SWIGPendingException_t4193433529_StaticFields::get_offset_of_numExceptionsPending_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (SWIGStringHelper_t549488518), -1, sizeof(SWIGStringHelper_t549488518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[1] = 
{
	SWIGStringHelper_t549488518_StaticFields::get_offset_of_stringDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (SWIGStringDelegate_t18001273), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (AppUtil_t2859790353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (LogLevel_t543421840)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2158[7] = 
{
	LogLevel_t543421840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (FirebaseApp_t210707726), -1, sizeof(FirebaseApp_t210707726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2159[6] = 
{
	FirebaseApp_t210707726::get_offset_of_swigCPtr_0(),
	FirebaseApp_t210707726::get_offset_of_swigCMemOwn_1(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_nameToProxy_2(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_cPtrToProxy_3(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_cPtrRefCount_4(),
	FirebaseApp_t210707726::get_offset_of_destroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (FirebaseHandler_t2907300047), -1, sizeof(FirebaseHandler_t2907300047_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2160[2] = 
{
	FirebaseHandler_t2907300047_StaticFields::get_offset_of_firebaseHandler_2(),
	FirebaseHandler_t2907300047_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (LogMessageDelegate_t1988210674), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (MonoPInvokeCallbackAttribute_t323097478), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (DestroyDelegate_t3635929227), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (UnitySynchronizationContext_t1051733884), -1, sizeof(UnitySynchronizationContext_t1051733884_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[5] = 
{
	UnitySynchronizationContext_t1051733884_StaticFields::get_offset_of__instance_1(),
	UnitySynchronizationContext_t1051733884::get_offset_of_queue_2(),
	UnitySynchronizationContext_t1051733884::get_offset_of_behavior_3(),
	UnitySynchronizationContext_t1051733884::get_offset_of_mainThreadId_4(),
	UnitySynchronizationContext_t1051733884_StaticFields::get_offset_of_signalDictionary_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (SynchronizationContextBehavoir_t692674473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	SynchronizationContextBehavoir_t692674473::get_offset_of_callbackQueue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (U3CStartU3Ec__Iterator0_t1310454857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[6] = 
{
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U3CentryU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (UnityHttpRequest_t967994112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[6] = 
{
	UnityHttpRequest_t967994112::get_offset_of__requestHeaders_5(),
	UnityHttpRequest_t967994112::get_offset_of__requestBody_6(),
	UnityHttpRequest_t967994112::get_offset_of__responseCode_7(),
	UnityHttpRequest_t967994112::get_offset_of__responseHeaders_8(),
	UnityHttpRequest_t967994112::get_offset_of__sync_9(),
	UnityHttpRequest_t967994112::get_offset_of__error_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (StreamingDownloadHandler_t1548287308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[4] = 
{
	StreamingDownloadHandler_t1548287308::get_offset_of__stream_1(),
	StreamingDownloadHandler_t1548287308::get_offset_of__fbRq_2(),
	StreamingDownloadHandler_t1548287308::get_offset_of__totalDownloaded_3(),
	StreamingDownloadHandler_t1548287308::get_offset_of__contentLength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (UnityDownloadStream_t3205965147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[5] = 
{
	UnityDownloadStream_t3205965147::get_offset_of__currentPacket_1(),
	UnityDownloadStream_t3205965147::get_offset_of__currentPacketPosition_2(),
	UnityDownloadStream_t3205965147::get_offset_of__packetQueue_3(),
	UnityDownloadStream_t3205965147::get_offset_of__size_4(),
	UnityDownloadStream_t3205965147::get_offset_of__isDisposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (FirebaseHttpRequest_t1911360314), -1, sizeof(FirebaseHttpRequest_t1911360314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2171[5] = 
{
	FirebaseHttpRequest_t1911360314_StaticFields::get_offset_of__factory_0(),
	FirebaseHttpRequest_t1911360314::get_offset_of__url_1(),
	FirebaseHttpRequest_t1911360314_StaticFields::get_offset_of_ContentLength_2(),
	FirebaseHttpRequest_t1911360314_StaticFields::get_offset_of_Timeout_3(),
	FirebaseHttpRequest_t1911360314_StaticFields::get_offset_of_NetworkUnavailable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2192[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_HasFocus_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2196[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2197[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
