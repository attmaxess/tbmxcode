﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1353336012.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial1630303189.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE3157325053.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "DOTween46_U3CModuleU3E3783534214.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940008.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2735155078.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039116993.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893.h"
#include "Firebase_Messaging_U3CModuleU3E3783534214.h"
#include "Firebase_Messaging_Firebase_Messaging_TokenReceive1675589073.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AFInAppEvents610589710.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyer2800548462.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyerTrackerCall3562121710.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdvertiserOptInAttrib239368999.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RangedTooltipAttribu1630163362.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TooltipAttribute2997764131.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Field1707169132.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Fields2991328907.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GAIHandler2642131905.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GoogleAnalyticsAndroi227590446.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GoogleAnalyticsMPV33464676905.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GoogleAnalyticsV4198817271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GoogleAnalyticsV4_De1779684248.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GoogleAnalyticsiOSV31127340329.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppViewHitBuilder93969158.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EventHitBuilder3458384960.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ExceptionHitBuilder2776428281.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ItemHitBuilder596710299.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SocialHitBuilder2172450373.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TimingHitBuilder1787450410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TransactionHitBuilde3262282092.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3842535002.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1749519406.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1746754562.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C900829694.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2691167515.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2157404822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3369627127.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2144252492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3675451859.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (SpriteState_t1353336012)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2301[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2303[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2305[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2314[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2316[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2318[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2320[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2322[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2323[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2335[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2336[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2342[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2347[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2355[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (ShortcutExtensions46_t705180652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (U3CU3Ec__DisplayClass0_0_t3371939845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[1] = 
{
	U3CU3Ec__DisplayClass0_0_t3371939845::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (U3CU3Ec__DisplayClass4_0_t3371939977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3371939977::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (U3CU3Ec__DisplayClass5_0_t3371940008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[1] = 
{
	U3CU3Ec__DisplayClass5_0_t3371940008::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (U3CU3Ec__DisplayClass14_0_t2735155078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[1] = 
{
	U3CU3Ec__DisplayClass14_0_t2735155078::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (U3CU3Ec__DisplayClass15_0_t4039116993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[1] = 
{
	U3CU3Ec__DisplayClass15_0_t4039116993::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (U3CU3Ec__DisplayClass22_0_t3582130305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[1] = 
{
	U3CU3Ec__DisplayClass22_0_t3582130305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (U3CU3Ec__DisplayClass33_0_t591124893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[1] = 
{
	U3CU3Ec__DisplayClass33_0_t591124893::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (TokenReceivedEventArgs_t1675589073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[1] = 
{
	TokenReceivedEventArgs_t1675589073::get_offset_of_U3CTokenU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (AFInAppEvents_t610589710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[61] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (AppsFlyer_t2800548462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (AppsFlyerTrackerCallbacks_t3562121710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (AdvertiserOptInAttribute_t239368999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (RangedTooltipAttribute_t1630163362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	RangedTooltipAttribute_t1630163362::get_offset_of_min_0(),
	RangedTooltipAttribute_t1630163362::get_offset_of_max_1(),
	RangedTooltipAttribute_t1630163362::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (TooltipAttribute_t2997764131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[1] = 
{
	TooltipAttribute_t2997764131::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (Field_t1707169132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[1] = 
{
	Field_t1707169132::get_offset_of_parameter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (Fields_t2991328907), -1, sizeof(Fields_t2991328907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2376[60] = 
{
	Fields_t2991328907_StaticFields::get_offset_of_ANONYMIZE_IP_0(),
	Fields_t2991328907_StaticFields::get_offset_of_HIT_TYPE_1(),
	Fields_t2991328907_StaticFields::get_offset_of_SESSION_CONTROL_2(),
	Fields_t2991328907_StaticFields::get_offset_of_SCREEN_NAME_3(),
	Fields_t2991328907_StaticFields::get_offset_of_LOCATION_4(),
	Fields_t2991328907_StaticFields::get_offset_of_REFERRER_5(),
	Fields_t2991328907_StaticFields::get_offset_of_PAGE_6(),
	Fields_t2991328907_StaticFields::get_offset_of_HOSTNAME_7(),
	Fields_t2991328907_StaticFields::get_offset_of_TITLE_8(),
	Fields_t2991328907_StaticFields::get_offset_of_LANGUAGE_9(),
	Fields_t2991328907_StaticFields::get_offset_of_ENCODING_10(),
	Fields_t2991328907_StaticFields::get_offset_of_SCREEN_COLORS_11(),
	Fields_t2991328907_StaticFields::get_offset_of_SCREEN_RESOLUTION_12(),
	Fields_t2991328907_StaticFields::get_offset_of_VIEWPORT_SIZE_13(),
	Fields_t2991328907_StaticFields::get_offset_of_APP_NAME_14(),
	Fields_t2991328907_StaticFields::get_offset_of_APP_ID_15(),
	Fields_t2991328907_StaticFields::get_offset_of_APP_INSTALLER_ID_16(),
	Fields_t2991328907_StaticFields::get_offset_of_APP_VERSION_17(),
	Fields_t2991328907_StaticFields::get_offset_of_CLIENT_ID_18(),
	Fields_t2991328907_StaticFields::get_offset_of_USER_ID_19(),
	Fields_t2991328907_StaticFields::get_offset_of_CAMPAIGN_NAME_20(),
	Fields_t2991328907_StaticFields::get_offset_of_CAMPAIGN_SOURCE_21(),
	Fields_t2991328907_StaticFields::get_offset_of_CAMPAIGN_MEDIUM_22(),
	Fields_t2991328907_StaticFields::get_offset_of_CAMPAIGN_KEYWORD_23(),
	Fields_t2991328907_StaticFields::get_offset_of_CAMPAIGN_CONTENT_24(),
	Fields_t2991328907_StaticFields::get_offset_of_CAMPAIGN_ID_25(),
	Fields_t2991328907_StaticFields::get_offset_of_GCLID_26(),
	Fields_t2991328907_StaticFields::get_offset_of_DCLID_27(),
	Fields_t2991328907_StaticFields::get_offset_of_EVENT_CATEGORY_28(),
	Fields_t2991328907_StaticFields::get_offset_of_EVENT_ACTION_29(),
	Fields_t2991328907_StaticFields::get_offset_of_EVENT_LABEL_30(),
	Fields_t2991328907_StaticFields::get_offset_of_EVENT_VALUE_31(),
	Fields_t2991328907_StaticFields::get_offset_of_SOCIAL_NETWORK_32(),
	Fields_t2991328907_StaticFields::get_offset_of_SOCIAL_ACTION_33(),
	Fields_t2991328907_StaticFields::get_offset_of_SOCIAL_TARGET_34(),
	Fields_t2991328907_StaticFields::get_offset_of_TIMING_VAR_35(),
	Fields_t2991328907_StaticFields::get_offset_of_TIMING_VALUE_36(),
	Fields_t2991328907_StaticFields::get_offset_of_TIMING_CATEGORY_37(),
	Fields_t2991328907_StaticFields::get_offset_of_TIMING_LABEL_38(),
	Fields_t2991328907_StaticFields::get_offset_of_EX_DESCRIPTION_39(),
	Fields_t2991328907_StaticFields::get_offset_of_EX_FATAL_40(),
	Fields_t2991328907_StaticFields::get_offset_of_CURRENCY_CODE_41(),
	Fields_t2991328907_StaticFields::get_offset_of_TRANSACTION_ID_42(),
	Fields_t2991328907_StaticFields::get_offset_of_TRANSACTION_AFFILIATION_43(),
	Fields_t2991328907_StaticFields::get_offset_of_TRANSACTION_SHIPPING_44(),
	Fields_t2991328907_StaticFields::get_offset_of_TRANSACTION_TAX_45(),
	Fields_t2991328907_StaticFields::get_offset_of_TRANSACTION_REVENUE_46(),
	Fields_t2991328907_StaticFields::get_offset_of_ITEM_SKU_47(),
	Fields_t2991328907_StaticFields::get_offset_of_ITEM_NAME_48(),
	Fields_t2991328907_StaticFields::get_offset_of_ITEM_CATEGORY_49(),
	Fields_t2991328907_StaticFields::get_offset_of_ITEM_PRICE_50(),
	Fields_t2991328907_StaticFields::get_offset_of_ITEM_QUANTITY_51(),
	Fields_t2991328907_StaticFields::get_offset_of_TRACKING_ID_52(),
	Fields_t2991328907_StaticFields::get_offset_of_SAMPLE_RATE_53(),
	Fields_t2991328907_StaticFields::get_offset_of_DEVELOPER_ID_54(),
	Fields_t2991328907_StaticFields::get_offset_of_CUSTOM_METRIC_55(),
	Fields_t2991328907_StaticFields::get_offset_of_CUSTOM_DIMENSION_56(),
	Fields_t2991328907_StaticFields::get_offset_of_ADID_57(),
	Fields_t2991328907_StaticFields::get_offset_of_IDFA_58(),
	Fields_t2991328907_StaticFields::get_offset_of_ATE_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (GAIHandler_t2642131905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (GoogleAnalyticsAndroidV4_t227590446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (GoogleAnalyticsMPV3_t3464676905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (GoogleAnalyticsV4_t198817271), -1, sizeof(GoogleAnalyticsV4_t198817271_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2380[30] = 
{
	GoogleAnalyticsV4_t198817271::get_offset_of_uncaughtExceptionStackTrace_2(),
	GoogleAnalyticsV4_t198817271::get_offset_of_initialized_3(),
	GoogleAnalyticsV4_t198817271::get_offset_of_androidTrackingCode_4(),
	GoogleAnalyticsV4_t198817271::get_offset_of_IOSTrackingCode_5(),
	GoogleAnalyticsV4_t198817271::get_offset_of_otherTrackingCode_6(),
	GoogleAnalyticsV4_t198817271::get_offset_of_productName_7(),
	GoogleAnalyticsV4_t198817271::get_offset_of_bundleIdentifier_8(),
	GoogleAnalyticsV4_t198817271::get_offset_of_bundleVersion_9(),
	GoogleAnalyticsV4_t198817271::get_offset_of_dispatchPeriod_10(),
	GoogleAnalyticsV4_t198817271::get_offset_of_sampleFrequency_11(),
	GoogleAnalyticsV4_t198817271::get_offset_of_logLevel_12(),
	GoogleAnalyticsV4_t198817271::get_offset_of_anonymizeIP_13(),
	GoogleAnalyticsV4_t198817271::get_offset_of_UncaughtExceptionReporting_14(),
	GoogleAnalyticsV4_t198817271::get_offset_of_sendLaunchEvent_15(),
	GoogleAnalyticsV4_t198817271::get_offset_of_dryRun_16(),
	GoogleAnalyticsV4_t198817271::get_offset_of_sessionTimeout_17(),
	GoogleAnalyticsV4_t198817271::get_offset_of_enableAdId_18(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_instance_19(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_currencySymbol_20(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_EVENT_HIT_21(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_APP_VIEW_22(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_SET_23(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_SET_ALL_24(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_SEND_25(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_ITEM_HIT_26(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_TRANSACTION_HIT_27(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_SOCIAL_HIT_28(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_TIMING_HIT_29(),
	GoogleAnalyticsV4_t198817271_StaticFields::get_offset_of_EXCEPTION_HIT_30(),
	GoogleAnalyticsV4_t198817271::get_offset_of_iosTracker_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (DebugMode_t1779684248)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2381[5] = 
{
	DebugMode_t1779684248::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (GoogleAnalyticsiOSV3_t1127340329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[11] = 
{
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_trackingCode_0(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_appName_1(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_bundleIdentifier_2(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_appVersion_3(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_dispatchPeriod_4(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_sampleFrequency_5(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_logLevel_6(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_anonymizeIP_7(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_adIdCollection_8(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_dryRun_9(),
	GoogleAnalyticsiOSV3_t1127340329::get_offset_of_handler_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (AppViewHitBuilder_t93969158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[1] = 
{
	AppViewHitBuilder_t93969158::get_offset_of_screenName_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (EventHitBuilder_t3458384960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[4] = 
{
	EventHitBuilder_t3458384960::get_offset_of_eventCategory_10(),
	EventHitBuilder_t3458384960::get_offset_of_eventAction_11(),
	EventHitBuilder_t3458384960::get_offset_of_eventLabel_12(),
	EventHitBuilder_t3458384960::get_offset_of_eventValue_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (ExceptionHitBuilder_t2776428281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[2] = 
{
	ExceptionHitBuilder_t2776428281::get_offset_of_exceptionDescription_10(),
	ExceptionHitBuilder_t2776428281::get_offset_of_fatal_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (ItemHitBuilder_t596710299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[7] = 
{
	ItemHitBuilder_t596710299::get_offset_of_transactionID_10(),
	ItemHitBuilder_t596710299::get_offset_of_name_11(),
	ItemHitBuilder_t596710299::get_offset_of_SKU_12(),
	ItemHitBuilder_t596710299::get_offset_of_price_13(),
	ItemHitBuilder_t596710299::get_offset_of_category_14(),
	ItemHitBuilder_t596710299::get_offset_of_quantity_15(),
	ItemHitBuilder_t596710299::get_offset_of_currencyCode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (SocialHitBuilder_t2172450373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[3] = 
{
	SocialHitBuilder_t2172450373::get_offset_of_socialNetwork_10(),
	SocialHitBuilder_t2172450373::get_offset_of_socialAction_11(),
	SocialHitBuilder_t2172450373::get_offset_of_socialTarget_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (TimingHitBuilder_t1787450410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[4] = 
{
	TimingHitBuilder_t1787450410::get_offset_of_timingCategory_10(),
	TimingHitBuilder_t1787450410::get_offset_of_timingInterval_11(),
	TimingHitBuilder_t1787450410::get_offset_of_timingName_12(),
	TimingHitBuilder_t1787450410::get_offset_of_timingLabel_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (TransactionHitBuilder_t3262282092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[6] = 
{
	TransactionHitBuilder_t3262282092::get_offset_of_transactionID_10(),
	TransactionHitBuilder_t3262282092::get_offset_of_affiliation_11(),
	TransactionHitBuilder_t3262282092::get_offset_of_revenue_12(),
	TransactionHitBuilder_t3262282092::get_offset_of_tax_13(),
	TransactionHitBuilder_t3262282092::get_offset_of_shipping_14(),
	TransactionHitBuilder_t3262282092::get_offset_of_currencyCode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (AxisTouchButton_t3842535002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[6] = 
{
	AxisTouchButton_t3842535002::get_offset_of_axisName_2(),
	AxisTouchButton_t3842535002::get_offset_of_axisValue_3(),
	AxisTouchButton_t3842535002::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3842535002::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3842535002::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3842535002::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (ButtonHandler_t1749519406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[1] = 
{
	ButtonHandler_t1749519406::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (CrossPlatformInputManager_t1746754562), -1, sizeof(CrossPlatformInputManager_t1746754562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2393[3] = 
{
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (ActiveInputMethod_t900829694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2394[3] = 
{
	ActiveInputMethod_t900829694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (VirtualAxis_t2691167515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[3] = 
{
	VirtualAxis_t2691167515::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t2691167515::get_offset_of_m_Value_1(),
	VirtualAxis_t2691167515::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (VirtualButton_t2157404822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[5] = 
{
	VirtualButton_t2157404822::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2157404822::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2157404822::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2157404822::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2157404822::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (InputAxisScrollbar_t3369627127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[1] = 
{
	InputAxisScrollbar_t3369627127::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (Joystick_t2144252492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[9] = 
{
	Joystick_t2144252492::get_offset_of_MovementRange_2(),
	Joystick_t2144252492::get_offset_of_axesToUse_3(),
	Joystick_t2144252492::get_offset_of_horizontalAxisName_4(),
	Joystick_t2144252492::get_offset_of_verticalAxisName_5(),
	Joystick_t2144252492::get_offset_of_m_StartPos_6(),
	Joystick_t2144252492::get_offset_of_m_UseX_7(),
	Joystick_t2144252492::get_offset_of_m_UseY_8(),
	Joystick_t2144252492::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t2144252492::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (AxisOption_t3675451859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2399[4] = 
{
	AxisOption_t3675451859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
