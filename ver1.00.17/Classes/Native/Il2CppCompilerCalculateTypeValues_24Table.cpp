﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3634411257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2607665220.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3273007553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3398611001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1039424009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cr69389957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4103805620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1952940174.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3887193949.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C113868641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3347016329.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_PostEffectsBase3757392499.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShaftsResolu2166148231.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ShaftsScreenBlen616022271.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_SunShafts482045181.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_MouseLook2643707144.h"
#include "AssemblyU2DCSharp_MouseLook_RotationAxes2150291266.h"
#include "AssemblyU2DCSharp_FadeInPixelLights2175031310.h"
#include "AssemblyU2DCSharp_GBSettings1771149548.h"
#include "AssemblyU2DCSharp_GBSettings_RaycastMode1488478401.h"
#include "AssemblyU2DCSharp_CombineChildrenAFS3843368254.h"
#include "AssemblyU2DCSharp_DebugNormalsInEditmode4099272891.h"
#include "AssemblyU2DCSharp_MeshCombineUtilityAFS538341238.h"
#include "AssemblyU2DCSharp_MeshCombineUtilityAFS_MeshInstan2175729061.h"
#include "AssemblyU2DCSharp_SetupAdvancedFoliageShader182079117.h"
#include "AssemblyU2DCSharp_touchBendingCollisionGS3063605690.h"
#include "AssemblyU2DCSharp_touchBendingPlayerListener2574995411.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_Authenti1276453517.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_Credenti3762395084.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_Digest59399582.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_DigestSto674131537.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_DigestSt3001516118.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheFileIn2858191078.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheFileLoc673754655.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheMaintan558833957.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_UriComparer3249471093.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheServic1690789871.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheServic2391743479.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPConnectionRecycledD3354195806.h"
#include "AssemblyU2DCSharp_BestHTTP_ConnectionBase2782190729.h"
#include "AssemblyU2DCSharp_BestHTTP_StreamList3066109018.h"
#include "AssemblyU2DCSharp_BestHTTP_FileConnection3022110980.h"
#include "AssemblyU2DCSharp_BestHTTP_Cookies_Cookie4162804382.h"
#include "AssemblyU2DCSharp_BestHTTP_Cookies_CookieJar756201495.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Crc_CRC322268741539.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Bloc2541392848.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl1224730655.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl1983720200.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl4191606113.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl3381668151.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl2274450459.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_GZip1683233742.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_InfT1475751651.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl3437229943.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl2450294045.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Inte3809646427.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infla996093859.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl3102396736.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl2519418742.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_ZTre1042194920.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Flus1182037460.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp4151391442.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp2530143933.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp2282214205.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_ZlibE421852804.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Shar2666276944.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Inte3631556964.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Stat2290192584.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Adler6455690.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_ZlibS953065013.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib3383394762.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib3915258526.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib1899545627.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib1670432928.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_Extensions957788964.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_Extensions_U2670832402.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_ExceptionHel2051805979.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeaderParser1994184764.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeaderValue822462144.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeartbeatMana895236645.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_KeyValuePair1715528642.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_WWWAuthentic1921593050.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPFieldData605100868.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPFormBase1912072923.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPFormUsage2139743243.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPMultiPartForm2201706314.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPUrlEncodedFor2052977551.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_UnityForm1710299297.h"
#include "AssemblyU2DCSharp_BestHTTP_KeepAliveHeader1933208365.h"
#include "AssemblyU2DCSharp_BestHTTP_RetryCauses456446704.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPConnection2777749290.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPConnectionStates1509261476.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPManager2983460817.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPManager_U3CSendRequ3256281814.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPMethods178420096.h"
#include "AssemblyU2DCSharp_BestHTTP_SupportedProtocols1503488249.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPProtocolFactory3310674324.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (MobileControlRig_t3634411257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (MobileInput_t2607665220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (StandaloneInput_t3273007553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (TiltInput_t3398611001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[5] = 
{
	TiltInput_t3398611001::get_offset_of_mapping_2(),
	TiltInput_t3398611001::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t3398611001::get_offset_of_fullTiltAngle_4(),
	TiltInput_t3398611001::get_offset_of_centreAngleOffset_5(),
	TiltInput_t3398611001::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (AxisOptions_t1039424009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2404[3] = 
{
	AxisOptions_t1039424009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (AxisMapping_t69389957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[2] = 
{
	AxisMapping_t69389957::get_offset_of_type_0(),
	AxisMapping_t69389957::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (MappingType_t4103805620)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2406[5] = 
{
	MappingType_t4103805620::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (TouchPad_t1952940174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[18] = 
{
	TouchPad_t1952940174::get_offset_of_axesToUse_2(),
	TouchPad_t1952940174::get_offset_of_controlStyle_3(),
	TouchPad_t1952940174::get_offset_of_horizontalAxisName_4(),
	TouchPad_t1952940174::get_offset_of_verticalAxisName_5(),
	TouchPad_t1952940174::get_offset_of_Xsensitivity_6(),
	TouchPad_t1952940174::get_offset_of_Ysensitivity_7(),
	TouchPad_t1952940174::get_offset_of_m_StartPos_8(),
	TouchPad_t1952940174::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t1952940174::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t1952940174::get_offset_of_m_UseX_11(),
	TouchPad_t1952940174::get_offset_of_m_UseY_12(),
	TouchPad_t1952940174::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t1952940174::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t1952940174::get_offset_of_m_Dragging_15(),
	TouchPad_t1952940174::get_offset_of_m_Id_16(),
	TouchPad_t1952940174::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t1952940174::get_offset_of_m_Center_18(),
	TouchPad_t1952940174::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (AxisOption_t3887193949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2408[4] = 
{
	AxisOption_t3887193949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (ControlStyle_t113868641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2409[4] = 
{
	ControlStyle_t113868641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (VirtualInput_t3347016329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[4] = 
{
	VirtualInput_t3347016329::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t3347016329::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t3347016329::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t3347016329::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (PostEffectsBase_t3757392499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[3] = 
{
	PostEffectsBase_t3757392499::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t3757392499::get_offset_of_supportDX11_3(),
	PostEffectsBase_t3757392499::get_offset_of_isSupported_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (SunShaftsResolution_t2166148231)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[4] = 
{
	SunShaftsResolution_t2166148231::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (ShaftsScreenBlendMode_t616022271)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2414[3] = 
{
	ShaftsScreenBlendMode_t616022271::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (SunShafts_t482045181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[14] = 
{
	SunShafts_t482045181::get_offset_of_resolution_5(),
	SunShafts_t482045181::get_offset_of_screenBlendMode_6(),
	SunShafts_t482045181::get_offset_of_sunTransform_7(),
	SunShafts_t482045181::get_offset_of_radialBlurIterations_8(),
	SunShafts_t482045181::get_offset_of_sunColor_9(),
	SunShafts_t482045181::get_offset_of_sunShaftBlurRadius_10(),
	SunShafts_t482045181::get_offset_of_sunShaftIntensity_11(),
	SunShafts_t482045181::get_offset_of_useSkyBoxAlpha_12(),
	SunShafts_t482045181::get_offset_of_maxRadius_13(),
	SunShafts_t482045181::get_offset_of_useDepthTexture_14(),
	SunShafts_t482045181::get_offset_of_sunShaftsShader_15(),
	SunShafts_t482045181::get_offset_of_sunShaftsMaterial_16(),
	SunShafts_t482045181::get_offset_of_simpleClearShader_17(),
	SunShafts_t482045181::get_offset_of_simpleClearMaterial_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (U3CModuleU3E_t3783534229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (MouseLook_t2643707144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[8] = 
{
	MouseLook_t2643707144::get_offset_of_axes_2(),
	MouseLook_t2643707144::get_offset_of_sensitivityX_3(),
	MouseLook_t2643707144::get_offset_of_sensitivityY_4(),
	MouseLook_t2643707144::get_offset_of_minimumX_5(),
	MouseLook_t2643707144::get_offset_of_maximumX_6(),
	MouseLook_t2643707144::get_offset_of_minimumY_7(),
	MouseLook_t2643707144::get_offset_of_maximumY_8(),
	MouseLook_t2643707144::get_offset_of_rotationY_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (RotationAxes_t2150291266)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2418[4] = 
{
	RotationAxes_t2150291266::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (FadeInPixelLights_t2175031310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[6] = 
{
	FadeInPixelLights_t2175031310::get_offset_of_MaxDistance_2(),
	FadeInPixelLights_t2175031310::get_offset_of_Fadelength_3(),
	FadeInPixelLights_t2175031310::get_offset_of_Intensity_4(),
	FadeInPixelLights_t2175031310::get_offset_of_distance_5(),
	FadeInPixelLights_t2175031310::get_offset_of_factor_6(),
	FadeInPixelLights_t2175031310::get_offset_of_lt_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (GBSettings_t1771149548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[30] = 
{
	GBSettings_t1771149548::get_offset_of_brushSize_2(),
	GBSettings_t1771149548::get_offset_of_fireFromCamera_3(),
	GBSettings_t1771149548::get_offset_of_minScale_4(),
	GBSettings_t1771149548::get_offset_of_maxScale_5(),
	GBSettings_t1771149548::get_offset_of_spacing_6(),
	GBSettings_t1771149548::get_offset_of_preventOverlap_7(),
	GBSettings_t1771149548::get_offset_of_yOffset_8(),
	GBSettings_t1771149548::get_offset_of_alignToNormal_9(),
	GBSettings_t1771149548::get_offset_of_randomRotX_10(),
	GBSettings_t1771149548::get_offset_of_randomRotY_11(),
	GBSettings_t1771149548::get_offset_of_randomRotZ_12(),
	GBSettings_t1771149548::get_offset_of_brushActive_13(),
	GBSettings_t1771149548::get_offset_of_randomRotation_14(),
	GBSettings_t1771149548::get_offset_of_delete_15(),
	GBSettings_t1771149548::get_offset_of_raycastMode_16(),
	GBSettings_t1771149548::get_offset_of_parentObject_17(),
	GBSettings_t1771149548::get_offset_of_minBrushSize_18(),
	GBSettings_t1771149548::get_offset_of_maxBrushSize_19(),
	GBSettings_t1771149548::get_offset_of_minMinScale_20(),
	GBSettings_t1771149548::get_offset_of_maxMinScale_21(),
	GBSettings_t1771149548::get_offset_of_minMaxScale_22(),
	GBSettings_t1771149548::get_offset_of_maxMaxScale_23(),
	GBSettings_t1771149548::get_offset_of_minYOffset_24(),
	GBSettings_t1771149548::get_offset_of_maxYOffset_25(),
	GBSettings_t1771149548::get_offset_of_minSpacing_26(),
	GBSettings_t1771149548::get_offset_of_maxSpacing_27(),
	GBSettings_t1771149548::get_offset_of_gizmoPos_28(),
	GBSettings_t1771149548::get_offset_of_gizmoActive_29(),
	GBSettings_t1771149548::get_offset_of_activeGeometry_30(),
	GBSettings_t1771149548::get_offset_of_activeGeometryPaths_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (RaycastMode_t1488478401)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2421[3] = 
{
	RaycastMode_t1488478401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (CombineChildrenAFS_t3843368254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[25] = 
{
	CombineChildrenAFS_t3843368254::get_offset_of_hideChildren_2(),
	CombineChildrenAFS_t3843368254::get_offset_of_UnderlayingTerrain_3(),
	CombineChildrenAFS_t3843368254::get_offset_of_GroundMaxDistance_4(),
	CombineChildrenAFS_t3843368254::get_offset_of_bakeGroundLightingGrass_5(),
	CombineChildrenAFS_t3843368254::get_offset_of_HealthyColor_6(),
	CombineChildrenAFS_t3843368254::get_offset_of_DryColor_7(),
	CombineChildrenAFS_t3843368254::get_offset_of_NoiseSpread_8(),
	CombineChildrenAFS_t3843368254::get_offset_of_bakeGroundLightingFoliage_9(),
	CombineChildrenAFS_t3843368254::get_offset_of_randomBrightness_10(),
	CombineChildrenAFS_t3843368254::get_offset_of_randomPulse_11(),
	CombineChildrenAFS_t3843368254::get_offset_of_randomBending_12(),
	CombineChildrenAFS_t3843368254::get_offset_of_randomFluttering_13(),
	CombineChildrenAFS_t3843368254::get_offset_of_NoiseSpreadFoliage_14(),
	CombineChildrenAFS_t3843368254::get_offset_of_bakeScale_15(),
	CombineChildrenAFS_t3843368254::get_offset_of_debugNormals_16(),
	CombineChildrenAFS_t3843368254::get_offset_of_destroyChildObjectsInPlaymode_17(),
	CombineChildrenAFS_t3843368254::get_offset_of_CastShadows_18(),
	CombineChildrenAFS_t3843368254::get_offset_of_UseLightprobes_19(),
	CombineChildrenAFS_t3843368254::get_offset_of_RealignGroundMaxDistance_20(),
	CombineChildrenAFS_t3843368254::get_offset_of_ForceRealignment_21(),
	CombineChildrenAFS_t3843368254::get_offset_of_createUniqueUV2_22(),
	CombineChildrenAFS_t3843368254::get_offset_of_createUniqueUV2playmode_23(),
	CombineChildrenAFS_t3843368254::get_offset_of_isStaticallyCombined_24(),
	CombineChildrenAFS_t3843368254::get_offset_of_simplyCombine_25(),
	CombineChildrenAFS_t3843368254::get_offset_of_useUV4_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (DebugNormalsInEditmode_t4099272891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[1] = 
{
	DebugNormalsInEditmode_t4099272891::get_offset_of_cc_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (MeshCombineUtilityAFS_t538341238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (MeshInstance_t2175729061)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[6] = 
{
	MeshInstance_t2175729061::get_offset_of_mesh_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshInstance_t2175729061::get_offset_of_subMeshIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshInstance_t2175729061::get_offset_of_transform_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshInstance_t2175729061::get_offset_of_groundNormal_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshInstance_t2175729061::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshInstance_t2175729061::get_offset_of_pivot_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (SetupAdvancedFoliageShader_t182079117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[78] = 
{
	SetupAdvancedFoliageShader_t182079117::get_offset_of_isLinear_2(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AmbientLightingSH_3(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_TerrianLight0_4(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_TerrianLight1_5(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_DirectionalLightReference_6(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_DirectlightDir_7(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_Sunlight_8(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_SunLightCol_9(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_SunLuminance_10(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_GrassApproxTrans_11(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AFSFog_Mode_12(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AFSShader_Folder_13(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_disableFoginShader_14(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_Wind_15(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WindFrequency_16(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WaveSizeFoliageShader_17(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_LeafTurbulenceFoliageShader_18(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WindMultiplierForGrassshader_19(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WaveSizeForGrassshader_20(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WindJitterFrequencyForGrassshader_21(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WindJitterScaleForGrassshader_22(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_SyncWindDir_23(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WindMuliplierForTreeShaderPrimary_24(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WindMuliplierForTreeShaderSecondary_25(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_WindMuliplierForTreeShader_26(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_temp_WindFrequency_27(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_temp_WindJitterFrequency_28(),
	0,
	SetupAdvancedFoliageShader_t182079117::get_offset_of_freqSpeed_30(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_domainTime_Wind_31(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_domainTime_2ndBending_32(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_domainTime_Grass_33(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_RainAmount_34(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_VertexLitAlphaCutOff_35(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_VertexLitTranslucencyColor_36(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_VertexLitTranslucencyViewDependency_37(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_VertexLitShadowStrength_38(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_VertexLitSpecularReflectivity_39(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AfsSpecFade_40(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_TerrainFoliageNrmSpecMap_41(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AutoSyncToTerrain_42(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_SyncedTerrain_43(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AutoSyncInPlaymode_44(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_DetailDistanceForGrassShader_45(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardStart_46(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardFadeLenght_47(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_GrassAnimateNormal_48(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_GrassWavingTint_49(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AFSTreeColor_50(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_MainCam_51(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_TreeBillboardShadows_52(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardFadeOutLength_53(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardAdjustToCamera_54(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardAngleLimit_55(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardShadowColor_56(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardAmbientLightFactor_57(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_BillboardAmbientLightDesaturationFactor_58(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AutosyncShadowColor_59(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_EnableCameraLayerCulling_60(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_SmallDetailsDistance_61(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_MediumDetailsDistance_62(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_AllGrassObjectsCombined_63(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_TempWind_64(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_GrassWindSpeed_65(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_SinusWave_66(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_TriangleWaves_67(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_Oscillation_68(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_CameraForward_69(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_ShadowCameraForward_70(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_CameraForwardVec_71(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_rollingX_72(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_rollingXShadow_73(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_lightDir_74(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_templightDir_75(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_CameraAngle_76(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_allTerrains_77(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_fLight_78(),
	SetupAdvancedFoliageShader_t182079117::get_offset_of_vCoeff_79(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (touchBendingCollisionGS_t3063605690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[35] = 
{
	touchBendingCollisionGS_t3063605690::get_offset_of_simpleBendingMaterial_2(),
	touchBendingCollisionGS_t3063605690::get_offset_of_touchBendingMaterial_3(),
	touchBendingCollisionGS_t3063605690::get_offset_of_stiffness_4(),
	touchBendingCollisionGS_t3063605690::get_offset_of_disturbance_5(),
	touchBendingCollisionGS_t3063605690::get_offset_of_duration_6(),
	touchBendingCollisionGS_t3063605690::get_offset_of_myTransform_7(),
	touchBendingCollisionGS_t3063605690::get_offset_of_myRenderer_8(),
	touchBendingCollisionGS_t3063605690::get_offset_of_myMatrix_9(),
	touchBendingCollisionGS_t3063605690::get_offset_of_axis_10(),
	touchBendingCollisionGS_t3063605690::get_offset_of_axis1_11(),
	touchBendingCollisionGS_t3063605690::get_offset_of_touched_12(),
	touchBendingCollisionGS_t3063605690::get_offset_of_doubletouched_13(),
	touchBendingCollisionGS_t3063605690::get_offset_of_left_14(),
	touchBendingCollisionGS_t3063605690::get_offset_of_finished_15(),
	touchBendingCollisionGS_t3063605690::get_offset_of_left1_16(),
	touchBendingCollisionGS_t3063605690::get_offset_of_finished1_17(),
	touchBendingCollisionGS_t3063605690::get_offset_of_intialTouchForce_18(),
	touchBendingCollisionGS_t3063605690::get_offset_of_touchBending_19(),
	touchBendingCollisionGS_t3063605690::get_offset_of_targetTouchBending_20(),
	touchBendingCollisionGS_t3063605690::get_offset_of_easingControl_21(),
	touchBendingCollisionGS_t3063605690::get_offset_of_intialTouchForce1_22(),
	touchBendingCollisionGS_t3063605690::get_offset_of_touchBending1_23(),
	touchBendingCollisionGS_t3063605690::get_offset_of_targetTouchBending1_24(),
	touchBendingCollisionGS_t3063605690::get_offset_of_easingControl1_25(),
	touchBendingCollisionGS_t3063605690::get_offset_of_Player_ID_26(),
	touchBendingCollisionGS_t3063605690::get_offset_of_PlayerVars_27(),
	touchBendingCollisionGS_t3063605690::get_offset_of_Player_Direction_28(),
	touchBendingCollisionGS_t3063605690::get_offset_of_Player_Speed_29(),
	touchBendingCollisionGS_t3063605690::get_offset_of_Player1_ID_30(),
	touchBendingCollisionGS_t3063605690::get_offset_of_PlayerVars1_31(),
	touchBendingCollisionGS_t3063605690::get_offset_of_Player_Direction1_32(),
	touchBendingCollisionGS_t3063605690::get_offset_of_Player_Speed1_33(),
	touchBendingCollisionGS_t3063605690::get_offset_of_timer_34(),
	touchBendingCollisionGS_t3063605690::get_offset_of_timer1_35(),
	touchBendingCollisionGS_t3063605690::get_offset_of_lerptime_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (touchBendingPlayerListener_t2574995411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[9] = 
{
	touchBendingPlayerListener_t2574995411::get_offset_of_maxSpeed_2(),
	touchBendingPlayerListener_t2574995411::get_offset_of_Player_DampSpeed_3(),
	touchBendingPlayerListener_t2574995411::get_offset_of_myTransform_4(),
	touchBendingPlayerListener_t2574995411::get_offset_of_Player_Position_5(),
	touchBendingPlayerListener_t2574995411::get_offset_of_Player_OldPosition_6(),
	touchBendingPlayerListener_t2574995411::get_offset_of_Player_Speed_7(),
	touchBendingPlayerListener_t2574995411::get_offset_of_Player_NewSpeed_8(),
	touchBendingPlayerListener_t2574995411::get_offset_of_Player_Direction_9(),
	touchBendingPlayerListener_t2574995411::get_offset_of_Update_PlayerVars_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (AuthenticationTypes_t1276453517)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2429[4] = 
{
	AuthenticationTypes_t1276453517::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (Credentials_t3762395084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[3] = 
{
	Credentials_t3762395084::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	Credentials_t3762395084::get_offset_of_U3CUserNameU3Ek__BackingField_1(),
	Credentials_t3762395084::get_offset_of_U3CPasswordU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (Digest_t59399582), -1, sizeof(Digest_t59399582_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2431[12] = 
{
	Digest_t59399582::get_offset_of_U3CUriU3Ek__BackingField_0(),
	Digest_t59399582::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Digest_t59399582::get_offset_of_U3CRealmU3Ek__BackingField_2(),
	Digest_t59399582::get_offset_of_U3CStaleU3Ek__BackingField_3(),
	Digest_t59399582::get_offset_of_U3CNonceU3Ek__BackingField_4(),
	Digest_t59399582::get_offset_of_U3COpaqueU3Ek__BackingField_5(),
	Digest_t59399582::get_offset_of_U3CAlgorithmU3Ek__BackingField_6(),
	Digest_t59399582::get_offset_of_U3CProtectedUrisU3Ek__BackingField_7(),
	Digest_t59399582::get_offset_of_U3CQualityOfProtectionsU3Ek__BackingField_8(),
	Digest_t59399582::get_offset_of_U3CNonceCountU3Ek__BackingField_9(),
	Digest_t59399582::get_offset_of_U3CHA1SessU3Ek__BackingField_10(),
	Digest_t59399582_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (DigestStore_t674131537), -1, sizeof(DigestStore_t674131537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[3] = 
{
	DigestStore_t674131537_StaticFields::get_offset_of_Digests_0(),
	DigestStore_t674131537_StaticFields::get_offset_of_Locker_1(),
	DigestStore_t674131537_StaticFields::get_offset_of_SupportedAlgorithms_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (U3CFindBestU3Ec__AnonStorey0_t3001516118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[1] = 
{
	U3CFindBestU3Ec__AnonStorey0_t3001516118::get_offset_of_i_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (HTTPCacheFileInfo_t2858191078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[13] = 
{
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CUriU3Ek__BackingField_0(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CLastAccessU3Ek__BackingField_1(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CBodyLengthU3Ek__BackingField_2(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CETagU3Ek__BackingField_3(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CLastModifiedU3Ek__BackingField_4(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CExpiresU3Ek__BackingField_5(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CAgeU3Ek__BackingField_6(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CMaxAgeU3Ek__BackingField_7(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CDateU3Ek__BackingField_8(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CMustRevalidateU3Ek__BackingField_9(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CReceivedU3Ek__BackingField_10(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CConstructedPathU3Ek__BackingField_11(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CMappedNameIDXU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (HTTPCacheFileLock_t673754655), -1, sizeof(HTTPCacheFileLock_t673754655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2435[2] = 
{
	HTTPCacheFileLock_t673754655_StaticFields::get_offset_of_FileLocks_0(),
	HTTPCacheFileLock_t673754655_StaticFields::get_offset_of_SyncRoot_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (HTTPCacheMaintananceParams_t558833957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[2] = 
{
	HTTPCacheMaintananceParams_t558833957::get_offset_of_U3CDeleteOlderU3Ek__BackingField_0(),
	HTTPCacheMaintananceParams_t558833957::get_offset_of_U3CMaxCacheSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (UriComparer_t3249471093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (HTTPCacheService_t1690789871), -1, sizeof(HTTPCacheService_t1690789871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2438[13] = 
{
	0,
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_isSupported_1(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_IsSupportCheckDone_2(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_library_3(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_UsedIndexes_4(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CCacheFolderU3Ek__BackingField_5(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CLibraryPathU3Ek__BackingField_6(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_InClearThread_7(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_InMaintainenceThread_8(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_NextNameIDX_9(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (U3CBeginMaintainenceU3Ec__AnonStorey0_t2391743479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[1] = 
{
	U3CBeginMaintainenceU3Ec__AnonStorey0_t2391743479::get_offset_of_maintananceParam_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (HTTPConnectionRecycledDelegate_t3354195806), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (ConnectionBase_t2782190729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[11] = 
{
	ConnectionBase_t2782190729::get_offset_of_U3CServerAddressU3Ek__BackingField_0(),
	ConnectionBase_t2782190729::get_offset_of_U3CStateU3Ek__BackingField_1(),
	ConnectionBase_t2782190729::get_offset_of_U3CCurrentRequestU3Ek__BackingField_2(),
	ConnectionBase_t2782190729::get_offset_of_U3CStartTimeU3Ek__BackingField_3(),
	ConnectionBase_t2782190729::get_offset_of_U3CTimedOutStartU3Ek__BackingField_4(),
	ConnectionBase_t2782190729::get_offset_of_U3CProxyU3Ek__BackingField_5(),
	ConnectionBase_t2782190729::get_offset_of_U3CLastProcessedUriU3Ek__BackingField_6(),
	ConnectionBase_t2782190729::get_offset_of_LastProcessTime_7(),
	ConnectionBase_t2782190729::get_offset_of_OnConnectionRecycled_8(),
	ConnectionBase_t2782190729::get_offset_of_IsThreaded_9(),
	ConnectionBase_t2782190729::get_offset_of_U3CIsDisposedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (StreamList_t3066109018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[2] = 
{
	StreamList_t3066109018::get_offset_of_Streams_1(),
	StreamList_t3066109018::get_offset_of_CurrentIdx_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (FileConnection_t3022110980), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (Cookie_t4162804382), -1, sizeof(Cookie_t4162804382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[13] = 
{
	0,
	Cookie_t4162804382::get_offset_of_U3CNameU3Ek__BackingField_1(),
	Cookie_t4162804382::get_offset_of_U3CValueU3Ek__BackingField_2(),
	Cookie_t4162804382::get_offset_of_U3CDateU3Ek__BackingField_3(),
	Cookie_t4162804382::get_offset_of_U3CLastAccessU3Ek__BackingField_4(),
	Cookie_t4162804382::get_offset_of_U3CExpiresU3Ek__BackingField_5(),
	Cookie_t4162804382::get_offset_of_U3CMaxAgeU3Ek__BackingField_6(),
	Cookie_t4162804382::get_offset_of_U3CIsSessionU3Ek__BackingField_7(),
	Cookie_t4162804382::get_offset_of_U3CDomainU3Ek__BackingField_8(),
	Cookie_t4162804382::get_offset_of_U3CPathU3Ek__BackingField_9(),
	Cookie_t4162804382::get_offset_of_U3CIsSecureU3Ek__BackingField_10(),
	Cookie_t4162804382::get_offset_of_U3CIsHttpOnlyU3Ek__BackingField_11(),
	Cookie_t4162804382_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (CookieJar_t756201495), -1, sizeof(CookieJar_t756201495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[8] = 
{
	0,
	CookieJar_t756201495_StaticFields::get_offset_of_Cookies_1(),
	CookieJar_t756201495_StaticFields::get_offset_of_U3CCookieFolderU3Ek__BackingField_2(),
	CookieJar_t756201495_StaticFields::get_offset_of_U3CLibraryPathU3Ek__BackingField_3(),
	CookieJar_t756201495_StaticFields::get_offset_of_Locker_4(),
	CookieJar_t756201495_StaticFields::get_offset_of__isSavingSupported_5(),
	CookieJar_t756201495_StaticFields::get_offset_of_IsSupportCheckDone_6(),
	CookieJar_t756201495_StaticFields::get_offset_of_Loaded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (CRC32_t2268741539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[6] = 
{
	CRC32_t2268741539::get_offset_of_dwPolynomial_0(),
	CRC32_t2268741539::get_offset_of__TotalBytesRead_1(),
	CRC32_t2268741539::get_offset_of_reverseBits_2(),
	CRC32_t2268741539::get_offset_of_crc32Table_3(),
	0,
	CRC32_t2268741539::get_offset_of__register_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (BlockState_t2541392848)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[5] = 
{
	BlockState_t2541392848::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (DeflateFlavor_t1224730655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2448[4] = 
{
	DeflateFlavor_t1224730655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (DeflateManager_t1983720200), -1, sizeof(DeflateManager_t1983720200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2449[74] = 
{
	DeflateManager_t1983720200_StaticFields::get_offset_of_MEM_LEVEL_MAX_0(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MEM_LEVEL_DEFAULT_1(),
	DeflateManager_t1983720200::get_offset_of_DeflateFunction_2(),
	DeflateManager_t1983720200_StaticFields::get_offset_of__ErrorMessage_3(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_PRESET_DICT_4(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_INIT_STATE_5(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_BUSY_STATE_6(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_FINISH_STATE_7(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_DEFLATED_8(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_STORED_BLOCK_9(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_STATIC_TREES_10(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_DYN_TREES_11(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_BINARY_12(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_ASCII_13(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_UNKNOWN_14(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Buf_size_15(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MIN_MATCH_16(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MAX_MATCH_17(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MIN_LOOKAHEAD_18(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_HEAP_SIZE_19(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_END_BLOCK_20(),
	DeflateManager_t1983720200::get_offset_of__codec_21(),
	DeflateManager_t1983720200::get_offset_of_status_22(),
	DeflateManager_t1983720200::get_offset_of_pending_23(),
	DeflateManager_t1983720200::get_offset_of_nextPending_24(),
	DeflateManager_t1983720200::get_offset_of_pendingCount_25(),
	DeflateManager_t1983720200::get_offset_of_data_type_26(),
	DeflateManager_t1983720200::get_offset_of_last_flush_27(),
	DeflateManager_t1983720200::get_offset_of_w_size_28(),
	DeflateManager_t1983720200::get_offset_of_w_bits_29(),
	DeflateManager_t1983720200::get_offset_of_w_mask_30(),
	DeflateManager_t1983720200::get_offset_of_window_31(),
	DeflateManager_t1983720200::get_offset_of_window_size_32(),
	DeflateManager_t1983720200::get_offset_of_prev_33(),
	DeflateManager_t1983720200::get_offset_of_head_34(),
	DeflateManager_t1983720200::get_offset_of_ins_h_35(),
	DeflateManager_t1983720200::get_offset_of_hash_size_36(),
	DeflateManager_t1983720200::get_offset_of_hash_bits_37(),
	DeflateManager_t1983720200::get_offset_of_hash_mask_38(),
	DeflateManager_t1983720200::get_offset_of_hash_shift_39(),
	DeflateManager_t1983720200::get_offset_of_block_start_40(),
	DeflateManager_t1983720200::get_offset_of_config_41(),
	DeflateManager_t1983720200::get_offset_of_match_length_42(),
	DeflateManager_t1983720200::get_offset_of_prev_match_43(),
	DeflateManager_t1983720200::get_offset_of_match_available_44(),
	DeflateManager_t1983720200::get_offset_of_strstart_45(),
	DeflateManager_t1983720200::get_offset_of_match_start_46(),
	DeflateManager_t1983720200::get_offset_of_lookahead_47(),
	DeflateManager_t1983720200::get_offset_of_prev_length_48(),
	DeflateManager_t1983720200::get_offset_of_compressionLevel_49(),
	DeflateManager_t1983720200::get_offset_of_compressionStrategy_50(),
	DeflateManager_t1983720200::get_offset_of_dyn_ltree_51(),
	DeflateManager_t1983720200::get_offset_of_dyn_dtree_52(),
	DeflateManager_t1983720200::get_offset_of_bl_tree_53(),
	DeflateManager_t1983720200::get_offset_of_treeLiterals_54(),
	DeflateManager_t1983720200::get_offset_of_treeDistances_55(),
	DeflateManager_t1983720200::get_offset_of_treeBitLengths_56(),
	DeflateManager_t1983720200::get_offset_of_bl_count_57(),
	DeflateManager_t1983720200::get_offset_of_heap_58(),
	DeflateManager_t1983720200::get_offset_of_heap_len_59(),
	DeflateManager_t1983720200::get_offset_of_heap_max_60(),
	DeflateManager_t1983720200::get_offset_of_depth_61(),
	DeflateManager_t1983720200::get_offset_of__lengthOffset_62(),
	DeflateManager_t1983720200::get_offset_of_lit_bufsize_63(),
	DeflateManager_t1983720200::get_offset_of_last_lit_64(),
	DeflateManager_t1983720200::get_offset_of__distanceOffset_65(),
	DeflateManager_t1983720200::get_offset_of_opt_len_66(),
	DeflateManager_t1983720200::get_offset_of_static_len_67(),
	DeflateManager_t1983720200::get_offset_of_matches_68(),
	DeflateManager_t1983720200::get_offset_of_last_eob_len_69(),
	DeflateManager_t1983720200::get_offset_of_bi_buf_70(),
	DeflateManager_t1983720200::get_offset_of_bi_valid_71(),
	DeflateManager_t1983720200::get_offset_of_Rfc1950BytesEmitted_72(),
	DeflateManager_t1983720200::get_offset_of__WantRfc1950HeaderBytes_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (CompressFunc_t4191606113), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (Config_t3381668151), -1, sizeof(Config_t3381668151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2451[6] = 
{
	Config_t3381668151::get_offset_of_GoodLength_0(),
	Config_t3381668151::get_offset_of_MaxLazy_1(),
	Config_t3381668151::get_offset_of_NiceLength_2(),
	Config_t3381668151::get_offset_of_MaxChainLength_3(),
	Config_t3381668151::get_offset_of_Flavor_4(),
	Config_t3381668151_StaticFields::get_offset_of_Table_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (DeflateStream_t2274450459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[3] = 
{
	DeflateStream_t2274450459::get_offset_of__baseStream_1(),
	DeflateStream_t2274450459::get_offset_of__innerStream_2(),
	DeflateStream_t2274450459::get_offset_of__disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (GZipStream_t1683233742), -1, sizeof(GZipStream_t1683233742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2453[10] = 
{
	GZipStream_t1683233742::get_offset_of_LastModified_1(),
	GZipStream_t1683233742::get_offset_of__headerByteCount_2(),
	GZipStream_t1683233742::get_offset_of__baseStream_3(),
	GZipStream_t1683233742::get_offset_of__disposed_4(),
	GZipStream_t1683233742::get_offset_of__firstReadDone_5(),
	GZipStream_t1683233742::get_offset_of__FileName_6(),
	GZipStream_t1683233742::get_offset_of__Comment_7(),
	GZipStream_t1683233742::get_offset_of__Crc32_8(),
	GZipStream_t1683233742_StaticFields::get_offset_of__unixEpoch_9(),
	GZipStream_t1683233742_StaticFields::get_offset_of_iso8859dash1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (InfTree_t1475751651), -1, sizeof(InfTree_t1475751651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfTree_t1475751651_StaticFields::get_offset_of_fixed_tl_12(),
	InfTree_t1475751651_StaticFields::get_offset_of_fixed_td_13(),
	InfTree_t1475751651_StaticFields::get_offset_of_cplens_14(),
	InfTree_t1475751651_StaticFields::get_offset_of_cplext_15(),
	InfTree_t1475751651_StaticFields::get_offset_of_cpdist_16(),
	InfTree_t1475751651_StaticFields::get_offset_of_cpdext_17(),
	0,
	InfTree_t1475751651::get_offset_of_hn_19(),
	InfTree_t1475751651::get_offset_of_v_20(),
	InfTree_t1475751651::get_offset_of_c_21(),
	InfTree_t1475751651::get_offset_of_r_22(),
	InfTree_t1475751651::get_offset_of_u_23(),
	InfTree_t1475751651::get_offset_of_x_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (InflateBlocks_t3437229943), -1, sizeof(InflateBlocks_t3437229943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2455[22] = 
{
	0,
	InflateBlocks_t3437229943_StaticFields::get_offset_of_border_1(),
	InflateBlocks_t3437229943::get_offset_of_mode_2(),
	InflateBlocks_t3437229943::get_offset_of_left_3(),
	InflateBlocks_t3437229943::get_offset_of_table_4(),
	InflateBlocks_t3437229943::get_offset_of_index_5(),
	InflateBlocks_t3437229943::get_offset_of_blens_6(),
	InflateBlocks_t3437229943::get_offset_of_bb_7(),
	InflateBlocks_t3437229943::get_offset_of_tb_8(),
	InflateBlocks_t3437229943::get_offset_of_codes_9(),
	InflateBlocks_t3437229943::get_offset_of_last_10(),
	InflateBlocks_t3437229943::get_offset_of__codec_11(),
	InflateBlocks_t3437229943::get_offset_of_bitk_12(),
	InflateBlocks_t3437229943::get_offset_of_bitb_13(),
	InflateBlocks_t3437229943::get_offset_of_hufts_14(),
	InflateBlocks_t3437229943::get_offset_of_window_15(),
	InflateBlocks_t3437229943::get_offset_of_end_16(),
	InflateBlocks_t3437229943::get_offset_of_readAt_17(),
	InflateBlocks_t3437229943::get_offset_of_writeAt_18(),
	InflateBlocks_t3437229943::get_offset_of_checkfn_19(),
	InflateBlocks_t3437229943::get_offset_of_check_20(),
	InflateBlocks_t3437229943::get_offset_of_inftree_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (InflateBlockMode_t2450294045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2456[11] = 
{
	InflateBlockMode_t2450294045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (InternalInflateConstants_t3809646427), -1, sizeof(InternalInflateConstants_t3809646427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2457[1] = 
{
	InternalInflateConstants_t3809646427_StaticFields::get_offset_of_InflateMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (InflateCodes_t996093859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InflateCodes_t996093859::get_offset_of_mode_10(),
	InflateCodes_t996093859::get_offset_of_len_11(),
	InflateCodes_t996093859::get_offset_of_tree_12(),
	InflateCodes_t996093859::get_offset_of_tree_index_13(),
	InflateCodes_t996093859::get_offset_of_need_14(),
	InflateCodes_t996093859::get_offset_of_lit_15(),
	InflateCodes_t996093859::get_offset_of_bitsToGet_16(),
	InflateCodes_t996093859::get_offset_of_dist_17(),
	InflateCodes_t996093859::get_offset_of_lbits_18(),
	InflateCodes_t996093859::get_offset_of_dbits_19(),
	InflateCodes_t996093859::get_offset_of_ltree_20(),
	InflateCodes_t996093859::get_offset_of_ltree_index_21(),
	InflateCodes_t996093859::get_offset_of_dtree_22(),
	InflateCodes_t996093859::get_offset_of_dtree_index_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (InflateManager_t3102396736), -1, sizeof(InflateManager_t3102396736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2459[12] = 
{
	0,
	0,
	InflateManager_t3102396736::get_offset_of_mode_2(),
	InflateManager_t3102396736::get_offset_of__codec_3(),
	InflateManager_t3102396736::get_offset_of_method_4(),
	InflateManager_t3102396736::get_offset_of_computedCheck_5(),
	InflateManager_t3102396736::get_offset_of_expectedCheck_6(),
	InflateManager_t3102396736::get_offset_of_marker_7(),
	InflateManager_t3102396736::get_offset_of__handleRfc1950HeaderBytes_8(),
	InflateManager_t3102396736::get_offset_of_wbits_9(),
	InflateManager_t3102396736::get_offset_of_blocks_10(),
	InflateManager_t3102396736_StaticFields::get_offset_of_mark_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (InflateManagerMode_t2519418742)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2460[15] = 
{
	InflateManagerMode_t2519418742::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (ZTree_t1042194920), -1, sizeof(ZTree_t1042194920_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2461[13] = 
{
	ZTree_t1042194920_StaticFields::get_offset_of_HEAP_SIZE_0(),
	ZTree_t1042194920_StaticFields::get_offset_of_ExtraLengthBits_1(),
	ZTree_t1042194920_StaticFields::get_offset_of_ExtraDistanceBits_2(),
	ZTree_t1042194920_StaticFields::get_offset_of_extra_blbits_3(),
	ZTree_t1042194920_StaticFields::get_offset_of_bl_order_4(),
	0,
	ZTree_t1042194920_StaticFields::get_offset_of__dist_code_6(),
	ZTree_t1042194920_StaticFields::get_offset_of_LengthCode_7(),
	ZTree_t1042194920_StaticFields::get_offset_of_LengthBase_8(),
	ZTree_t1042194920_StaticFields::get_offset_of_DistanceBase_9(),
	ZTree_t1042194920::get_offset_of_dyn_tree_10(),
	ZTree_t1042194920::get_offset_of_max_code_11(),
	ZTree_t1042194920::get_offset_of_staticTree_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (FlushType_t1182037460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2462[6] = 
{
	FlushType_t1182037460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (CompressionLevel_t4151391442)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2463[15] = 
{
	CompressionLevel_t4151391442::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (CompressionStrategy_t2530143933)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2464[4] = 
{
	CompressionStrategy_t2530143933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (CompressionMode_t2282214205)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2465[3] = 
{
	CompressionMode_t2282214205::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (ZlibException_t421852804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (SharedUtils_t2666276944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (InternalConstants_t3631556964), -1, sizeof(InternalConstants_t3631556964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2468[10] = 
{
	InternalConstants_t3631556964_StaticFields::get_offset_of_MAX_BITS_0(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_BL_CODES_1(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_D_CODES_2(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_LITERALS_3(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_LENGTH_CODES_4(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_L_CODES_5(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_MAX_BL_BITS_6(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_REP_3_6_7(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_REPZ_3_10_8(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_REPZ_11_138_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (StaticTree_t2290192584), -1, sizeof(StaticTree_t2290192584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2469[10] = 
{
	StaticTree_t2290192584_StaticFields::get_offset_of_lengthAndLiteralsTreeCodes_0(),
	StaticTree_t2290192584_StaticFields::get_offset_of_distTreeCodes_1(),
	StaticTree_t2290192584_StaticFields::get_offset_of_Literals_2(),
	StaticTree_t2290192584_StaticFields::get_offset_of_Distances_3(),
	StaticTree_t2290192584_StaticFields::get_offset_of_BitLengths_4(),
	StaticTree_t2290192584::get_offset_of_treeCodes_5(),
	StaticTree_t2290192584::get_offset_of_extraBits_6(),
	StaticTree_t2290192584::get_offset_of_extraBase_7(),
	StaticTree_t2290192584::get_offset_of_elems_8(),
	StaticTree_t2290192584::get_offset_of_maxLength_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (Adler_t6455690), -1, sizeof(Adler_t6455690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2470[2] = 
{
	Adler_t6455690_StaticFields::get_offset_of_BASE_0(),
	Adler_t6455690_StaticFields::get_offset_of_NMAX_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (ZlibStreamFlavor_t953065013)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[4] = 
{
	ZlibStreamFlavor_t953065013::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (ZlibBaseStream_t3383394762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[19] = 
{
	ZlibBaseStream_t3383394762::get_offset_of__z_1(),
	ZlibBaseStream_t3383394762::get_offset_of__streamMode_2(),
	ZlibBaseStream_t3383394762::get_offset_of__flushMode_3(),
	ZlibBaseStream_t3383394762::get_offset_of__flavor_4(),
	ZlibBaseStream_t3383394762::get_offset_of__compressionMode_5(),
	ZlibBaseStream_t3383394762::get_offset_of__level_6(),
	ZlibBaseStream_t3383394762::get_offset_of__leaveOpen_7(),
	ZlibBaseStream_t3383394762::get_offset_of__workingBuffer_8(),
	ZlibBaseStream_t3383394762::get_offset_of__bufferSize_9(),
	ZlibBaseStream_t3383394762::get_offset_of_windowBitsMax_10(),
	ZlibBaseStream_t3383394762::get_offset_of__buf1_11(),
	ZlibBaseStream_t3383394762::get_offset_of__stream_12(),
	ZlibBaseStream_t3383394762::get_offset_of_Strategy_13(),
	ZlibBaseStream_t3383394762::get_offset_of_crc_14(),
	ZlibBaseStream_t3383394762::get_offset_of__GzipFileName_15(),
	ZlibBaseStream_t3383394762::get_offset_of__GzipComment_16(),
	ZlibBaseStream_t3383394762::get_offset_of__GzipMtime_17(),
	ZlibBaseStream_t3383394762::get_offset_of__gzipHeaderByteCount_18(),
	ZlibBaseStream_t3383394762::get_offset_of_nomoreinput_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (StreamMode_t3915258526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2473[4] = 
{
	StreamMode_t3915258526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (ZlibCodec_t1899545627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[15] = 
{
	ZlibCodec_t1899545627::get_offset_of_InputBuffer_0(),
	ZlibCodec_t1899545627::get_offset_of_NextIn_1(),
	ZlibCodec_t1899545627::get_offset_of_AvailableBytesIn_2(),
	ZlibCodec_t1899545627::get_offset_of_TotalBytesIn_3(),
	ZlibCodec_t1899545627::get_offset_of_OutputBuffer_4(),
	ZlibCodec_t1899545627::get_offset_of_NextOut_5(),
	ZlibCodec_t1899545627::get_offset_of_AvailableBytesOut_6(),
	ZlibCodec_t1899545627::get_offset_of_TotalBytesOut_7(),
	ZlibCodec_t1899545627::get_offset_of_Message_8(),
	ZlibCodec_t1899545627::get_offset_of_dstate_9(),
	ZlibCodec_t1899545627::get_offset_of_istate_10(),
	ZlibCodec_t1899545627::get_offset_of__Adler32_11(),
	ZlibCodec_t1899545627::get_offset_of_CompressLevel_12(),
	ZlibCodec_t1899545627::get_offset_of_WindowBits_13(),
	ZlibCodec_t1899545627::get_offset_of_Strategy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (ZlibConstants_t1670432928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (Extensions_t957788964), -1, sizeof(Extensions_t957788964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2476[3] = 
{
	Extensions_t957788964_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	Extensions_t957788964_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	Extensions_t957788964_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (U3CReadU3Ec__AnonStorey0_t2670832402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[1] = 
{
	U3CReadU3Ec__AnonStorey0_t2670832402::get_offset_of_block_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (ExceptionHelper_t2051805979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (HeaderParser_t1994184764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (HeaderValue_t822462144), -1, sizeof(HeaderValue_t822462144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2480[4] = 
{
	HeaderValue_t822462144::get_offset_of_U3CKeyU3Ek__BackingField_0(),
	HeaderValue_t822462144::get_offset_of_U3CValueU3Ek__BackingField_1(),
	HeaderValue_t822462144::get_offset_of_U3COptionsU3Ek__BackingField_2(),
	HeaderValue_t822462144_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (HeartbeatManager_t895236645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[3] = 
{
	HeartbeatManager_t895236645::get_offset_of_Heartbeats_0(),
	HeartbeatManager_t895236645::get_offset_of_UpdateArray_1(),
	HeartbeatManager_t895236645::get_offset_of_LastUpdate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (KeyValuePairList_t1715528642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[1] = 
{
	KeyValuePairList_t1715528642::get_offset_of_U3CValuesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (WWWAuthenticateHeaderParser_t1921593050), -1, sizeof(WWWAuthenticateHeaderParser_t1921593050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2484[1] = 
{
	WWWAuthenticateHeaderParser_t1921593050_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (HTTPFieldData_t605100868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[6] = 
{
	HTTPFieldData_t605100868::get_offset_of_U3CNameU3Ek__BackingField_0(),
	HTTPFieldData_t605100868::get_offset_of_U3CFileNameU3Ek__BackingField_1(),
	HTTPFieldData_t605100868::get_offset_of_U3CMimeTypeU3Ek__BackingField_2(),
	HTTPFieldData_t605100868::get_offset_of_U3CEncodingU3Ek__BackingField_3(),
	HTTPFieldData_t605100868::get_offset_of_U3CTextU3Ek__BackingField_4(),
	HTTPFieldData_t605100868::get_offset_of_U3CBinaryU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (HTTPFormBase_t1912072923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[5] = 
{
	0,
	HTTPFormBase_t1912072923::get_offset_of_U3CFieldsU3Ek__BackingField_1(),
	HTTPFormBase_t1912072923::get_offset_of_U3CIsChangedU3Ek__BackingField_2(),
	HTTPFormBase_t1912072923::get_offset_of_U3CHasBinaryU3Ek__BackingField_3(),
	HTTPFormBase_t1912072923::get_offset_of_U3CHasLongValueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (HTTPFormUsage_t2139743243)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2487[5] = 
{
	HTTPFormUsage_t2139743243::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (HTTPMultiPartForm_t2201706314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[2] = 
{
	HTTPMultiPartForm_t2201706314::get_offset_of_Boundary_5(),
	HTTPMultiPartForm_t2201706314::get_offset_of_CachedData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (HTTPUrlEncodedForm_t2052977551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[2] = 
{
	0,
	HTTPUrlEncodedForm_t2052977551::get_offset_of_CachedData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (UnityForm_t1710299297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[1] = 
{
	UnityForm_t1710299297::get_offset_of_U3CFormU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (KeepAliveHeader_t1933208365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[2] = 
{
	KeepAliveHeader_t1933208365::get_offset_of_U3CTimeOutU3Ek__BackingField_0(),
	KeepAliveHeader_t1933208365::get_offset_of_U3CMaxRequestsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (RetryCauses_t456446704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2492[5] = 
{
	RetryCauses_t456446704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (HTTPConnection_t2777749290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[3] = 
{
	HTTPConnection_t2777749290::get_offset_of_Client_11(),
	HTTPConnection_t2777749290::get_offset_of_Stream_12(),
	HTTPConnection_t2777749290::get_offset_of_KeepAlive_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (HTTPConnectionStates_t1509261476)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2494[11] = 
{
	HTTPConnectionStates_t1509261476::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (HTTPManager_t2983460817), -1, sizeof(HTTPManager_t2983460817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2495[27] = 
{
	HTTPManager_t2983460817_StaticFields::get_offset_of_maxConnectionPerServer_0(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CKeepAliveDefaultValueU3Ek__BackingField_1(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CIsCachingDisabledU3Ek__BackingField_2(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CMaxConnectionIdleTimeU3Ek__BackingField_3(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_4(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CCookieJarSizeU3Ek__BackingField_5(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CEnablePrivateBrowsingU3Ek__BackingField_6(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_7(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CRequestTimeoutU3Ek__BackingField_8(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CRootCacheFolderProviderU3Ek__BackingField_9(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CProxyU3Ek__BackingField_10(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_heartbeats_11(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_logger_12(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CDefaultCertificateVerifyerU3Ek__BackingField_13(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CDefaultClientCredentialsProviderU3Ek__BackingField_14(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CUseAlternateSSLDefaultValueU3Ek__BackingField_15(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CMaxPathLengthU3Ek__BackingField_16(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_Connections_17(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_ActiveConnections_18(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_FreeConnections_19(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_RecycledConnections_20(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_RequestQueue_21(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_IsCallingCallbacks_22(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_Locker_23(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_24(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_25(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (U3CSendRequestImplU3Ec__AnonStorey0_t3256281814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[1] = 
{
	U3CSendRequestImplU3Ec__AnonStorey0_t3256281814::get_offset_of_conn_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (HTTPMethods_t178420096)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2497[8] = 
{
	HTTPMethods_t178420096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (SupportedProtocols_t1503488249)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2498[5] = 
{
	SupportedProtocols_t1503488249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (HTTPProtocolFactory_t3310674324), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
