﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPProxy2644053826.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRange1154458197.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequestStates63938943.h"
#include "AssemblyU2DCSharp_BestHTTP_OnRequestFinishedDelega3180754735.h"
#include "AssemblyU2DCSharp_BestHTTP_OnDownloadProgressDelega447146369.h"
#include "AssemblyU2DCSharp_BestHTTP_OnUploadProgressDelegat3063766470.h"
#include "AssemblyU2DCSharp_BestHTTP_OnBeforeRedirectionDeleg290558967.h"
#include "AssemblyU2DCSharp_BestHTTP_OnHeaderEnumerationDele3923304806.h"
#include "AssemblyU2DCSharp_BestHTTP_OnBeforeHeaderSendDeleg3915595400.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest_U3CEnumerat3794713947.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest_U3CSendHead3710205996.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPUpdateDelegator1331403296.h"
#include "AssemblyU2DCSharp_BestHTTP_JSON_Json3015811848.h"
#include "AssemblyU2DCSharp_BestHTTP_Logger_DefaultLogger639890009.h"
#include "AssemblyU2DCSharp_BestHTTP_Logger_Loglevels4278436247.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2314871951.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2640700633.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec3926133854.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec1087955584.h"
#include "AssemblyU2DCSharp_BestHTTP_PlatformSupport_TcpClie4173427386.h"
#include "AssemblyU2DCSharp_BestHTTP_PlatformSupport_TcpClien742700320.h"
#include "AssemblyU2DCSharp_BestHTTP_PlatformSupport_TcpClie2124838760.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Generat986026348.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Stream3208800844.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Encoda3447851422.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Encoda3471733113.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Excepti782059264.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1InputS2767940265.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Null813671072.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Object564283626.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1OctetS1486532927.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Output1632877576.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Parsing912539532.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Sequence54253652.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Sequen2695581227.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Set2420705913.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Set_As1422684887.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Set_De1051659365.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1TaggedO990853098.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Tags847429018.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerBitStri2786834273.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerGenerat1021114948.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerOctetSt2849212572.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSequenc3637877645.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSequenc1792950261.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSetGener493026546.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSetPars3373663962.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerTaggedO3812856247.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerApplica3974373185.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerApplica1533940994.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerOctetStr478504981.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerOutputSt868356256.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSequence24248732.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSet2720783323.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerTaggedOb282133382.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Constructe3468994639.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerExterna2315310584.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerExterna2840903253.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerGenerato951913470.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerOctetSt3154381398.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSequenc1671991799.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSetGene2437150008.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSetPars3304453660.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DefiniteLe1065419872.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerApplica2286556739.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerBmpStrin177902387.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerBitStri2717907355.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerBoolean857650049.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerEnumerat514019671.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerGeneral2384300942.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerGeneral1810943552.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerGraphic3982270410.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerIA5Strin460424437.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerInteger967720487.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerNull559715134.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerNumeric3931265727.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerObjectI3495876513.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerOctetSt2503196699.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerOutputS2807883870.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerPrintabl696797023.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSequenc4250014174.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerSet2720781401.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerStringB1343648701.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerT61Stri2556947549.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerTaggedO2520525900.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerUtcTime3495696610.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerUtf8Str1713298971.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerUnivers2286946381.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerVideote2380089812.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_DerVisibleS512908706.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (HTTPProxy_t2644053826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[5] = 
{
	HTTPProxy_t2644053826::get_offset_of_U3CAddressU3Ek__BackingField_0(),
	HTTPProxy_t2644053826::get_offset_of_U3CCredentialsU3Ek__BackingField_1(),
	HTTPProxy_t2644053826::get_offset_of_U3CIsTransparentU3Ek__BackingField_2(),
	HTTPProxy_t2644053826::get_offset_of_U3CSendWholeUriU3Ek__BackingField_3(),
	HTTPProxy_t2644053826::get_offset_of_U3CNonTransparentForHTTPSU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (HTTPRange_t1154458197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[4] = 
{
	HTTPRange_t1154458197::get_offset_of_U3CFirstBytePosU3Ek__BackingField_0(),
	HTTPRange_t1154458197::get_offset_of_U3CLastBytePosU3Ek__BackingField_1(),
	HTTPRange_t1154458197::get_offset_of_U3CContentLengthU3Ek__BackingField_2(),
	HTTPRange_t1154458197::get_offset_of_U3CIsValidU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (HTTPRequestStates_t63938943)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2502[9] = 
{
	HTTPRequestStates_t63938943::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (OnRequestFinishedDelegate_t3180754735), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (OnDownloadProgressDelegate_t447146369), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (OnUploadProgressDelegate_t3063766470), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (OnBeforeRedirectionDelegate_t290558967), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (OnHeaderEnumerationDelegate_t3923304806), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (OnBeforeHeaderSendDelegate_t3915595400), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (HTTPRequest_t138485887), -1, sizeof(HTTPRequest_t138485887_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2509[54] = 
{
	HTTPRequest_t138485887_StaticFields::get_offset_of_EOL_0(),
	HTTPRequest_t138485887_StaticFields::get_offset_of_MethodNames_1(),
	HTTPRequest_t138485887_StaticFields::get_offset_of_UploadChunkSize_2(),
	HTTPRequest_t138485887::get_offset_of_U3CUriU3Ek__BackingField_3(),
	HTTPRequest_t138485887::get_offset_of_U3CMethodTypeU3Ek__BackingField_4(),
	HTTPRequest_t138485887::get_offset_of_U3CRawDataU3Ek__BackingField_5(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadStreamU3Ek__BackingField_6(),
	HTTPRequest_t138485887::get_offset_of_U3CDisposeUploadStreamU3Ek__BackingField_7(),
	HTTPRequest_t138485887::get_offset_of_U3CUseUploadStreamLengthU3Ek__BackingField_8(),
	HTTPRequest_t138485887::get_offset_of_OnUploadProgress_9(),
	HTTPRequest_t138485887::get_offset_of_U3CCallbackU3Ek__BackingField_10(),
	HTTPRequest_t138485887::get_offset_of_OnProgress_11(),
	HTTPRequest_t138485887::get_offset_of_OnUpgraded_12(),
	HTTPRequest_t138485887::get_offset_of_U3CDisableRetryU3Ek__BackingField_13(),
	HTTPRequest_t138485887::get_offset_of_U3CIsRedirectedU3Ek__BackingField_14(),
	HTTPRequest_t138485887::get_offset_of_U3CRedirectUriU3Ek__BackingField_15(),
	HTTPRequest_t138485887::get_offset_of_U3CResponseU3Ek__BackingField_16(),
	HTTPRequest_t138485887::get_offset_of_U3CProxyResponseU3Ek__BackingField_17(),
	HTTPRequest_t138485887::get_offset_of_U3CExceptionU3Ek__BackingField_18(),
	HTTPRequest_t138485887::get_offset_of_U3CTagU3Ek__BackingField_19(),
	HTTPRequest_t138485887::get_offset_of_U3CCredentialsU3Ek__BackingField_20(),
	HTTPRequest_t138485887::get_offset_of_U3CProxyU3Ek__BackingField_21(),
	HTTPRequest_t138485887::get_offset_of_U3CMaxRedirectsU3Ek__BackingField_22(),
	HTTPRequest_t138485887::get_offset_of_U3CUseAlternateSSLU3Ek__BackingField_23(),
	HTTPRequest_t138485887::get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_24(),
	HTTPRequest_t138485887::get_offset_of_customCookies_25(),
	HTTPRequest_t138485887::get_offset_of_U3CFormUsageU3Ek__BackingField_26(),
	HTTPRequest_t138485887::get_offset_of_U3CStateU3Ek__BackingField_27(),
	HTTPRequest_t138485887::get_offset_of_U3CRedirectCountU3Ek__BackingField_28(),
	HTTPRequest_t138485887::get_offset_of_CustomCertificationValidator_29(),
	HTTPRequest_t138485887::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_30(),
	HTTPRequest_t138485887::get_offset_of_U3CTimeoutU3Ek__BackingField_31(),
	HTTPRequest_t138485887::get_offset_of_U3CEnableTimoutForStreamingU3Ek__BackingField_32(),
	HTTPRequest_t138485887::get_offset_of_U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_33(),
	HTTPRequest_t138485887::get_offset_of_U3CPriorityU3Ek__BackingField_34(),
	HTTPRequest_t138485887::get_offset_of_U3CCustomCertificateVerifyerU3Ek__BackingField_35(),
	HTTPRequest_t138485887::get_offset_of_U3CCustomClientCredentialsProviderU3Ek__BackingField_36(),
	HTTPRequest_t138485887::get_offset_of_U3CProtocolHandlerU3Ek__BackingField_37(),
	HTTPRequest_t138485887::get_offset_of_onBeforeRedirection_38(),
	HTTPRequest_t138485887::get_offset_of__onBeforeHeaderSend_39(),
	HTTPRequest_t138485887::get_offset_of_U3CDownloadedU3Ek__BackingField_40(),
	HTTPRequest_t138485887::get_offset_of_U3CDownloadLengthU3Ek__BackingField_41(),
	HTTPRequest_t138485887::get_offset_of_U3CDownloadProgressChangedU3Ek__BackingField_42(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadedU3Ek__BackingField_43(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadLengthU3Ek__BackingField_44(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadProgressChangedU3Ek__BackingField_45(),
	HTTPRequest_t138485887::get_offset_of_isKeepAlive_46(),
	HTTPRequest_t138485887::get_offset_of_disableCache_47(),
	HTTPRequest_t138485887::get_offset_of_cacheOnly_48(),
	HTTPRequest_t138485887::get_offset_of_streamFragmentSize_49(),
	HTTPRequest_t138485887::get_offset_of_useStreaming_50(),
	HTTPRequest_t138485887::get_offset_of_U3CHeadersU3Ek__BackingField_51(),
	HTTPRequest_t138485887::get_offset_of_FieldCollector_52(),
	HTTPRequest_t138485887::get_offset_of_FormImpl_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (U3CEnumerateHeadersU3Ec__AnonStorey0_t3794713947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	U3CEnumerateHeadersU3Ec__AnonStorey0_t3794713947::get_offset_of_customCookie_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (U3CSendHeadersU3Ec__AnonStorey1_t3710205996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	U3CSendHeadersU3Ec__AnonStorey1_t3710205996::get_offset_of_stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (HTTPResponse_t62748825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[31] = 
{
	0,
	0,
	0,
	HTTPResponse_t62748825::get_offset_of_U3CVersionMajorU3Ek__BackingField_3(),
	HTTPResponse_t62748825::get_offset_of_U3CVersionMinorU3Ek__BackingField_4(),
	HTTPResponse_t62748825::get_offset_of_U3CStatusCodeU3Ek__BackingField_5(),
	HTTPResponse_t62748825::get_offset_of_U3CMessageU3Ek__BackingField_6(),
	HTTPResponse_t62748825::get_offset_of_U3CIsStreamedU3Ek__BackingField_7(),
	HTTPResponse_t62748825::get_offset_of_U3CIsStreamingFinishedU3Ek__BackingField_8(),
	HTTPResponse_t62748825::get_offset_of_U3CIsFromCacheU3Ek__BackingField_9(),
	HTTPResponse_t62748825::get_offset_of_U3CCacheFileInfoU3Ek__BackingField_10(),
	HTTPResponse_t62748825::get_offset_of_U3CIsCacheOnlyU3Ek__BackingField_11(),
	HTTPResponse_t62748825::get_offset_of_U3CHeadersU3Ek__BackingField_12(),
	HTTPResponse_t62748825::get_offset_of_U3CDataU3Ek__BackingField_13(),
	HTTPResponse_t62748825::get_offset_of_U3CIsUpgradedU3Ek__BackingField_14(),
	HTTPResponse_t62748825::get_offset_of_U3CCookiesU3Ek__BackingField_15(),
	HTTPResponse_t62748825::get_offset_of_dataAsText_16(),
	HTTPResponse_t62748825::get_offset_of_texture_17(),
	HTTPResponse_t62748825::get_offset_of_U3CIsClosedManuallyU3Ek__BackingField_18(),
	HTTPResponse_t62748825::get_offset_of_baseRequest_19(),
	HTTPResponse_t62748825::get_offset_of_Stream_20(),
	HTTPResponse_t62748825::get_offset_of_streamedFragments_21(),
	HTTPResponse_t62748825::get_offset_of_SyncRoot_22(),
	HTTPResponse_t62748825::get_offset_of_fragmentBuffer_23(),
	HTTPResponse_t62748825::get_offset_of_fragmentBufferDataLength_24(),
	HTTPResponse_t62748825::get_offset_of_cacheStream_25(),
	HTTPResponse_t62748825::get_offset_of_allFragmentSize_26(),
	HTTPResponse_t62748825::get_offset_of_decompressorInputStream_27(),
	HTTPResponse_t62748825::get_offset_of_decompressorOutputStream_28(),
	HTTPResponse_t62748825::get_offset_of_decompressorGZipStream_29(),
	HTTPResponse_t62748825::get_offset_of_copyBuffer_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (HTTPUpdateDelegator_t1331403296), -1, sizeof(HTTPUpdateDelegator_t1331403296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2514[7] = 
{
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_2(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CIsCreatedU3Ek__BackingField_3(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CIsThreadedU3Ek__BackingField_4(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CIsThreadRunningU3Ek__BackingField_5(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CThreadFrequencyInMSU3Ek__BackingField_6(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_OnBeforeApplicationQuit_7(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_IsSetupCalled_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (Json_t3015811848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (DefaultLogger_t639890009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[6] = 
{
	DefaultLogger_t639890009::get_offset_of_U3CLevelU3Ek__BackingField_0(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatVerboseU3Ek__BackingField_1(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatInfoU3Ek__BackingField_2(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatWarnU3Ek__BackingField_3(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatErrU3Ek__BackingField_4(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatExU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (Loglevels_t4278436247)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2517[7] = 
{
	Loglevels_t4278436247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (NotifyCollectionChangedEventHandler_t2314871951), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (NotifyCollectionChangedAction_t2640700633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2522[6] = 
{
	NotifyCollectionChangedAction_t2640700633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (NotifyCollectionChangedEventArgs_t3926133854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[5] = 
{
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__action_1(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__newItems_2(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__oldItems_3(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__newStartingIndex_4(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__oldStartingIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (ReadOnlyList_t1087955584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[1] = 
{
	ReadOnlyList_t1087955584::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (TcpClient_t4173427386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[12] = 
{
	TcpClient_t4173427386::get_offset_of_stream_0(),
	TcpClient_t4173427386::get_offset_of_active_1(),
	TcpClient_t4173427386::get_offset_of_client_2(),
	TcpClient_t4173427386::get_offset_of_disposed_3(),
	TcpClient_t4173427386::get_offset_of_values_4(),
	TcpClient_t4173427386::get_offset_of_recv_timeout_5(),
	TcpClient_t4173427386::get_offset_of_send_timeout_6(),
	TcpClient_t4173427386::get_offset_of_recv_buffer_size_7(),
	TcpClient_t4173427386::get_offset_of_send_buffer_size_8(),
	TcpClient_t4173427386::get_offset_of_linger_state_9(),
	TcpClient_t4173427386::get_offset_of_no_delay_10(),
	TcpClient_t4173427386::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (Properties_t742700320)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2526[7] = 
{
	Properties_t742700320::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (U3CConnectU3Ec__AnonStorey0_t2124838760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[1] = 
{
	U3CConnectU3Ec__AnonStorey0_t2124838760::get_offset_of_mre_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (Asn1Generator_t986026348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[1] = 
{
	Asn1Generator_t986026348::get_offset_of__out_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (Asn1StreamParser_t3208800844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[3] = 
{
	Asn1StreamParser_t3208800844::get_offset_of__in_0(),
	Asn1StreamParser_t3208800844::get_offset_of__limit_1(),
	Asn1StreamParser_t3208800844::get_offset_of_tmpBuffers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (Asn1Encodable_t3447851422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (Asn1EncodableVector_t3471733113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[1] = 
{
	Asn1EncodableVector_t3471733113::get_offset_of_v_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (Asn1Exception_t782059264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (Asn1InputStream_t2767940265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[2] = 
{
	Asn1InputStream_t2767940265::get_offset_of_limit_2(),
	Asn1InputStream_t2767940265::get_offset_of_tmpBuffers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (Asn1Null_t813671072), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (Asn1Object_t564283626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (Asn1OctetString_t1486532927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[1] = 
{
	Asn1OctetString_t1486532927::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (Asn1OutputStream_t1632877576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (Asn1ParsingException_t912539532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (Asn1Sequence_t54253652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[1] = 
{
	Asn1Sequence_t54253652::get_offset_of_seq_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (Asn1SequenceParserImpl_t2695581227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[3] = 
{
	Asn1SequenceParserImpl_t2695581227::get_offset_of_outer_0(),
	Asn1SequenceParserImpl_t2695581227::get_offset_of_max_1(),
	Asn1SequenceParserImpl_t2695581227::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (Asn1Set_t2420705913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[1] = 
{
	Asn1Set_t2420705913::get_offset_of__set_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (Asn1SetParserImpl_t1422684887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[3] = 
{
	Asn1SetParserImpl_t1422684887::get_offset_of_outer_0(),
	Asn1SetParserImpl_t1422684887::get_offset_of_max_1(),
	Asn1SetParserImpl_t1422684887::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (DerComparer_t1051659365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (Asn1TaggedObject_t990853098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[3] = 
{
	Asn1TaggedObject_t990853098::get_offset_of_tagNo_2(),
	Asn1TaggedObject_t990853098::get_offset_of_explicitly_3(),
	Asn1TaggedObject_t990853098::get_offset_of_obj_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (Asn1Tags_t847429018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (BerBitString_t2786834273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (BerGenerator_t1021114948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[3] = 
{
	BerGenerator_t1021114948::get_offset_of__tagged_1(),
	BerGenerator_t1021114948::get_offset_of__isExplicit_2(),
	BerGenerator_t1021114948::get_offset_of__tagNo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (BerOctetStringParser_t2849212572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[1] = 
{
	BerOctetStringParser_t2849212572::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (BerSequenceGenerator_t3637877645), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (BerSequenceParser_t1792950261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[1] = 
{
	BerSequenceParser_t1792950261::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (BerSetGenerator_t493026546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (BerSetParser_t3373663962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[1] = 
{
	BerSetParser_t3373663962::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (BerTaggedObjectParser_t3812856247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[3] = 
{
	BerTaggedObjectParser_t3812856247::get_offset_of__constructed_0(),
	BerTaggedObjectParser_t3812856247::get_offset_of__tagNumber_1(),
	BerTaggedObjectParser_t3812856247::get_offset_of__parser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (BerApplicationSpecific_t3974373185), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (BerApplicationSpecificParser_t1533940994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[2] = 
{
	BerApplicationSpecificParser_t1533940994::get_offset_of_tag_0(),
	BerApplicationSpecificParser_t1533940994::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (BerOctetString_t478504981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[2] = 
{
	0,
	BerOctetString_t478504981::get_offset_of_octs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (BerOutputStream_t868356256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (BerSequence_t24248732), -1, sizeof(BerSequence_t24248732_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2562[1] = 
{
	BerSequence_t24248732_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (BerSet_t2720783323), -1, sizeof(BerSet_t2720783323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2563[1] = 
{
	BerSet_t2720783323_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (BerTaggedObject_t282133382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (ConstructedOctetStream_t3468994639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[3] = 
{
	ConstructedOctetStream_t3468994639::get_offset_of__parser_2(),
	ConstructedOctetStream_t3468994639::get_offset_of__first_3(),
	ConstructedOctetStream_t3468994639::get_offset_of__currentStream_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (DerExternal_t2315310584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[5] = 
{
	DerExternal_t2315310584::get_offset_of_directReference_2(),
	DerExternal_t2315310584::get_offset_of_indirectReference_3(),
	DerExternal_t2315310584::get_offset_of_dataValueDescriptor_4(),
	DerExternal_t2315310584::get_offset_of_encoding_5(),
	DerExternal_t2315310584::get_offset_of_externalContent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (DerExternalParser_t2840903253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[1] = 
{
	DerExternalParser_t2840903253::get_offset_of__parser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (DerGenerator_t951913470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[3] = 
{
	DerGenerator_t951913470::get_offset_of__tagged_1(),
	DerGenerator_t951913470::get_offset_of__isExplicit_2(),
	DerGenerator_t951913470::get_offset_of__tagNo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (DerOctetStringParser_t3154381398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[1] = 
{
	DerOctetStringParser_t3154381398::get_offset_of_stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (DerSequenceParser_t1671991799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[1] = 
{
	DerSequenceParser_t1671991799::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (DerSetGenerator_t2437150008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[1] = 
{
	DerSetGenerator_t2437150008::get_offset_of__bOut_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (DerSetParser_t3304453660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[1] = 
{
	DerSetParser_t3304453660::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (DefiniteLengthInputStream_t1065419872), -1, sizeof(DefiniteLengthInputStream_t1065419872_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2573[3] = 
{
	DefiniteLengthInputStream_t1065419872_StaticFields::get_offset_of_EmptyBytes_4(),
	DefiniteLengthInputStream_t1065419872::get_offset_of__originalLength_5(),
	DefiniteLengthInputStream_t1065419872::get_offset_of__remaining_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (DerApplicationSpecific_t2286556739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[3] = 
{
	DerApplicationSpecific_t2286556739::get_offset_of_isConstructed_2(),
	DerApplicationSpecific_t2286556739::get_offset_of_tag_3(),
	DerApplicationSpecific_t2286556739::get_offset_of_octets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (DerBmpString_t177902387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[1] = 
{
	DerBmpString_t177902387::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (DerBitString_t2717907355), -1, sizeof(DerBitString_t2717907355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2576[3] = 
{
	DerBitString_t2717907355_StaticFields::get_offset_of_table_2(),
	DerBitString_t2717907355::get_offset_of_mData_3(),
	DerBitString_t2717907355::get_offset_of_mPadBits_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (DerBoolean_t857650049), -1, sizeof(DerBoolean_t857650049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2577[3] = 
{
	DerBoolean_t857650049::get_offset_of_value_2(),
	DerBoolean_t857650049_StaticFields::get_offset_of_False_3(),
	DerBoolean_t857650049_StaticFields::get_offset_of_True_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (DerEnumerated_t514019671), -1, sizeof(DerEnumerated_t514019671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2578[2] = 
{
	DerEnumerated_t514019671::get_offset_of_bytes_2(),
	DerEnumerated_t514019671_StaticFields::get_offset_of_cache_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (DerGeneralString_t2384300942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[1] = 
{
	DerGeneralString_t2384300942::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (DerGeneralizedTime_t1810943552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[1] = 
{
	DerGeneralizedTime_t1810943552::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (DerGraphicString_t3982270410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[1] = 
{
	DerGraphicString_t3982270410::get_offset_of_mString_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (DerIA5String_t460424437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[1] = 
{
	DerIA5String_t460424437::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (DerInteger_t967720487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[1] = 
{
	DerInteger_t967720487::get_offset_of_bytes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (DerNull_t559715134), -1, sizeof(DerNull_t559715134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2584[2] = 
{
	DerNull_t559715134_StaticFields::get_offset_of_Instance_2(),
	DerNull_t559715134::get_offset_of_zeroBytes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (DerNumericString_t3931265727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[1] = 
{
	DerNumericString_t3931265727::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (DerObjectIdentifier_t3495876513), -1, sizeof(DerObjectIdentifier_t3495876513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2586[4] = 
{
	DerObjectIdentifier_t3495876513::get_offset_of_identifier_2(),
	DerObjectIdentifier_t3495876513::get_offset_of_body_3(),
	0,
	DerObjectIdentifier_t3495876513_StaticFields::get_offset_of_cache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (DerOctetString_t2503196699), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (DerOutputStream_t2807883870), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (DerPrintableString_t696797023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[1] = 
{
	DerPrintableString_t696797023::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (DerSequence_t4250014174), -1, sizeof(DerSequence_t4250014174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2590[1] = 
{
	DerSequence_t4250014174_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (DerSet_t2720781401), -1, sizeof(DerSet_t2720781401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[1] = 
{
	DerSet_t2720781401_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (DerStringBase_t1343648701), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (DerT61String_t2556947549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[1] = 
{
	DerT61String_t2556947549::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (DerTaggedObject_t2520525900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (DerUtcTime_t3495696610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	DerUtcTime_t3495696610::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (DerUtf8String_t1713298971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[1] = 
{
	DerUtf8String_t1713298971::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (DerUniversalString_t2286946381), -1, sizeof(DerUniversalString_t2286946381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2597[2] = 
{
	DerUniversalString_t2286946381_StaticFields::get_offset_of_table_2(),
	DerUniversalString_t2286946381::get_offset_of_str_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (DerVideotexString_t2380089812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[1] = 
{
	DerVideotexString_t2380089812::get_offset_of_mString_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (DerVisibleString_t512908706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[1] = 
{
	DerVisibleString_t512908706::get_offset_of_str_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
