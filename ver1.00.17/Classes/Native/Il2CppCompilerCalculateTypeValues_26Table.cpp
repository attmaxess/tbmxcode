﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Indefinite1999368943.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LazyAsn1In2434367547.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LazyDerSeq4187101828.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LazyDerSet1112924515.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_LimitedInpu781897436.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_OidTokeniz1891371977.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Anssi_Anss1870423751.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Anssi_AnssiN28633283.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Anssi_Anssi107688019.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_C378628739.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_1984881998.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_3870759210.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_2624318576.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_CryptoPro_4257955415.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Iana_IanaO2977840882.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_MiscOb808030413.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_Netsca668167463.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_Netsc2439051718.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Misc_Veris3134672961.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Nist_NistNa941520945.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Nist_NistO1742042649.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_OcspR2399692236.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_OcspR2886715370.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_Respon853298195.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Ocsp_Respon561211756.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Oiw_ElGama1755000518.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Oiw_OiwObj2484194348.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_Conte1565361645.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_DHPara310369835.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_PkcsO1103055686.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_Rsass1481623005.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Pkcs_Signe1315018810.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName558061916.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3753151333.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2526217440.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2159797956.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName170864723.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1154406297.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1153592480.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3459626543.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3762826772.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3762621805.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1547647928.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1546834111.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3240468911.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam3239722744.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam1133933760.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4048294485.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2345758350.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNamed14293759.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4037539526.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName944075595.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2343088644.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2342274827.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNamed10810236.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2355228822.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNamed23764231.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName164911864.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName164174145.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2003561066.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName170425007.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecName170160838.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2814401075.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam2813595706.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4201946253.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecNam4201208534.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Sec_SecObj3901542888.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_3126647113.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T983939287.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T983863441.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T166682126.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_T167037012.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1446647738.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1446858876.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_2284807557.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_2284875459.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1625039561.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1625250699.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1017516731.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_1017871617.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_4283766514.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_4284121400.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_TeleTrust_2978504789.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Utilities_4013099873.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Utilities_3440239854.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Algor2670781410.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Basic3459049714.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlDi3073089489.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlNu1759393128.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlRe3883542487.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Certi2288802675.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_DsaPar274855281.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Digest202014884.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Distri769724552.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Distr1765286135.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Genera294965175.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (IndefiniteLengthInputStream_t1999368943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[2] = 
{
	IndefiniteLengthInputStream_t1999368943::get_offset_of__lookAhead_4(),
	IndefiniteLengthInputStream_t1999368943::get_offset_of__eofOn00_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (LazyAsn1InputStream_t2434367547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (LazyDerSequence_t4187101828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[1] = 
{
	LazyDerSequence_t4187101828::get_offset_of_encoded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (LazyDerSet_t1112924515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[1] = 
{
	LazyDerSet_t1112924515::get_offset_of_encoded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (LimitedInputStream_t781897436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[2] = 
{
	LimitedInputStream_t781897436::get_offset_of__in_2(),
	LimitedInputStream_t781897436::get_offset_of__limit_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (OidTokenizer_t1891371977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[2] = 
{
	OidTokenizer_t1891371977::get_offset_of_oid_0(),
	OidTokenizer_t1891371977::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (AnssiNamedCurves_t1870423751), -1, sizeof(AnssiNamedCurves_t1870423751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2610[3] = 
{
	AnssiNamedCurves_t1870423751_StaticFields::get_offset_of_objIds_0(),
	AnssiNamedCurves_t1870423751_StaticFields::get_offset_of_curves_1(),
	AnssiNamedCurves_t1870423751_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (Frp256v1Holder_t28633283), -1, sizeof(Frp256v1Holder_t28633283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2611[1] = 
{
	Frp256v1Holder_t28633283_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (AnssiObjectIdentifiers_t107688019), -1, sizeof(AnssiObjectIdentifiers_t107688019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2612[1] = 
{
	AnssiObjectIdentifiers_t107688019_StaticFields::get_offset_of_FRP256v1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (CryptoProObjectIdentifiers_t378628739), -1, sizeof(CryptoProObjectIdentifiers_t378628739_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2613[24] = 
{
	0,
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411_1(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411Hmac_2(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR28147Cbc_3(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_ID_Gost28147_89_CryptoPro_A_ParamSet_4(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94_5(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001_6(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411x94WithGostR3410x94_7(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411x94WithGostR3410x2001_8(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3411x94CryptoProParamSet_9(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProA_10(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProB_11(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProC_12(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProD_13(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProXchA_14(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProXchB_15(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x94CryptoProXchC_16(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProA_17(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProB_18(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProC_19(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProXchA_20(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostR3410x2001CryptoProXchB_21(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostElSgDH3410Default_22(),
	CryptoProObjectIdentifiers_t378628739_StaticFields::get_offset_of_GostElSgDH3410x1_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (ECGost3410NamedCurves_t1984881998), -1, sizeof(ECGost3410NamedCurves_t1984881998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[3] = 
{
	ECGost3410NamedCurves_t1984881998_StaticFields::get_offset_of_objIds_0(),
	ECGost3410NamedCurves_t1984881998_StaticFields::get_offset_of_parameters_1(),
	ECGost3410NamedCurves_t1984881998_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (Gost3410NamedParameters_t3870759210), -1, sizeof(Gost3410NamedParameters_t3870759210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2615[5] = 
{
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_objIds_0(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_parameters_1(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_cryptoProA_2(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_cryptoProB_3(),
	Gost3410NamedParameters_t3870759210_StaticFields::get_offset_of_cryptoProXchA_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (Gost3410ParamSetParameters_t2624318576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[4] = 
{
	Gost3410ParamSetParameters_t2624318576::get_offset_of_keySize_2(),
	Gost3410ParamSetParameters_t2624318576::get_offset_of_p_3(),
	Gost3410ParamSetParameters_t2624318576::get_offset_of_q_4(),
	Gost3410ParamSetParameters_t2624318576::get_offset_of_a_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (Gost3410PublicKeyAlgParameters_t4257955415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[3] = 
{
	Gost3410PublicKeyAlgParameters_t4257955415::get_offset_of_publicKeyParamSet_2(),
	Gost3410PublicKeyAlgParameters_t4257955415::get_offset_of_digestParamSet_3(),
	Gost3410PublicKeyAlgParameters_t4257955415::get_offset_of_encryptionParamSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (IanaObjectIdentifiers_t2977840882), -1, sizeof(IanaObjectIdentifiers_t2977840882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2618[5] = 
{
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_IsakmpOakley_0(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacMD5_1(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacSha1_2(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacTiger_3(),
	IanaObjectIdentifiers_t2977840882_StaticFields::get_offset_of_HmacRipeMD160_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (MiscObjectIdentifiers_t808030413), -1, sizeof(MiscObjectIdentifiers_t808030413_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[32] = 
{
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Netscape_0(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCertType_1(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeBaseUrl_2(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeRevocationUrl_3(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCARevocationUrl_4(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeRenewalUrl_5(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCAPolicyUrl_6(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeSslServerName_7(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NetscapeCertComment_8(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Verisign_9(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignCzagExtension_10(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignPrivate_6_9_11(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignOnSiteJurisdictionHash_12(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignBitString_6_13_13(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignDnbDunsNumber_14(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_VerisignIssStrongCrypto_15(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Novell_16(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_NovellSecurityAttribs_17(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_Entrust_18(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_EntrustVersionExtension_19(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_as_sys_sec_alg_ideaCBC_20(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_cryptlib_21(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_cryptlib_algorithm_22(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_ECB_23(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_CBC_24(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_CFB_25(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_OFB_26(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_blake2_27(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_id_blake2b160_28(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_id_blake2b256_29(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_id_blake2b384_30(),
	MiscObjectIdentifiers_t808030413_StaticFields::get_offset_of_id_blake2b512_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (NetscapeCertType_t668167463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (NetscapeRevocationUrl_t2439051718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (VerisignCzagExtension_t3134672961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (NistNamedCurves_t941520945), -1, sizeof(NistNamedCurves_t941520945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2623[2] = 
{
	NistNamedCurves_t941520945_StaticFields::get_offset_of_objIds_0(),
	NistNamedCurves_t941520945_StaticFields::get_offset_of_names_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (NistObjectIdentifiers_t1742042649), -1, sizeof(NistObjectIdentifiers_t1742042649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2624[41] = 
{
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_NistAlgorithm_0(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_HashAlgs_1(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha256_2(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha384_3(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha512_4(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha224_5(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha512_224_6(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha512_256_7(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha3_224_8(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha3_256_9(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha3_384_10(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdSha3_512_11(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdShake128_12(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdShake256_13(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_Aes_14(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Ecb_15(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Cbc_16(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Ofb_17(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Cfb_18(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Wrap_19(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Gcm_20(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes128Ccm_21(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Ecb_22(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Cbc_23(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Ofb_24(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Cfb_25(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Wrap_26(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Gcm_27(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes192Ccm_28(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Ecb_29(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Cbc_30(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Ofb_31(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Cfb_32(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Wrap_33(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Gcm_34(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdAes256Ccm_35(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_IdDsaWithSha2_36(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha224_37(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha256_38(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha384_39(),
	NistObjectIdentifiers_t1742042649_StaticFields::get_offset_of_DsaWithSha512_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (OcspResponse_t2399692236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[2] = 
{
	OcspResponse_t2399692236::get_offset_of_responseStatus_2(),
	OcspResponse_t2399692236::get_offset_of_responseBytes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (OcspResponseStatus_t2886715370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (ResponderID_t853298195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[1] = 
{
	ResponderID_t853298195::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (ResponseBytes_t561211756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[2] = 
{
	ResponseBytes_t561211756::get_offset_of_responseType_2(),
	ResponseBytes_t561211756::get_offset_of_response_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (ElGamalParameter_t1755000518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[2] = 
{
	ElGamalParameter_t1755000518::get_offset_of_p_2(),
	ElGamalParameter_t1755000518::get_offset_of_g_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (OiwObjectIdentifiers_t2484194348), -1, sizeof(OiwObjectIdentifiers_t2484194348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2630[12] = 
{
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_MD4WithRsa_0(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_MD5WithRsa_1(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_MD4WithRsaEncryption_2(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesEcb_3(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesCbc_4(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesOfb_5(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesCfb_6(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DesEde_7(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_IdSha1_8(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_DsaWithSha1_9(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_Sha1WithRsa_10(),
	OiwObjectIdentifiers_t2484194348_StaticFields::get_offset_of_ElGamalAlgorithm_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (ContentInfo_t1565361645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[2] = 
{
	ContentInfo_t1565361645::get_offset_of_contentType_2(),
	ContentInfo_t1565361645::get_offset_of_content_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (DHParameter_t310369835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[3] = 
{
	DHParameter_t310369835::get_offset_of_p_2(),
	DHParameter_t310369835::get_offset_of_g_3(),
	DHParameter_t310369835::get_offset_of_l_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (PkcsObjectIdentifiers_t1103055686), -1, sizeof(PkcsObjectIdentifiers_t1103055686_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2633[135] = 
{
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_RsaEncryption_1(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD2WithRsaEncryption_2(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD4WithRsaEncryption_3(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD5WithRsaEncryption_4(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha1WithRsaEncryption_5(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SrsaOaepEncryptionSet_6(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdRsaesOaep_7(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdMgf1_8(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdPSpecified_9(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdRsassaPss_10(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha256WithRsaEncryption_11(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha384WithRsaEncryption_12(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha512WithRsaEncryption_13(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Sha224WithRsaEncryption_14(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_DhKeyAgreement_16(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD2AndDesCbc_18(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD2AndRC2Cbc_19(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD5AndDesCbc_20(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithMD5AndRC2Cbc_21(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithSha1AndDesCbc_22(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithSha1AndRC2Cbc_23(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdPbeS2_24(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdPbkdf2_25(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_DesEde3Cbc_27(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_RC2Cbc_28(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD2_30(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD4_31(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_MD5_32(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha1_33(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha224_34(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha256_35(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha384_36(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdHmacWithSha512_37(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Data_39(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SignedData_40(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_EnvelopedData_41(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SignedAndEnvelopedData_42(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_DigestedData_43(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_EncryptedData_44(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtEmailAddress_46(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtUnstructuredName_47(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtContentType_48(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtMessageDigest_49(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtSigningTime_50(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtCounterSignature_51(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtChallengePassword_52(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtUnstructuredAddress_53(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtExtendedCertificateAttributes_54(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtSigningDescription_55(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtExtensionRequest_56(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtSmimeCapabilities_57(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdSmime_58(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtFriendlyName_59(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs9AtLocalKeyID_60(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_X509CertType_61(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_X509Certificate_63(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SdsiCertificate_64(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_X509Crl_66(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlg_67(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgEsdh_68(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgCms3DesWrap_69(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgCmsRC2Wrap_70(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgPwriKek_71(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAlgSsdh_72(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdRsaKem_73(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PreferSignedData_74(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_CannotDecryptAny_75(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SmimeCapabilitiesVersions_76(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAReceiptRequest_77(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTAuthData_79(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTTstInfo_80(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTCompressedData_81(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTAuthEnvelopedData_82(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCTTimestampedData_83(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfOrigin_85(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfReceipt_86(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfDelivery_87(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfSender_88(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfApproval_89(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdCtiEtsProofOfCreation_90(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAContentHint_92(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAMsgSigDigest_93(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAContentReference_94(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEncrypKeyPref_95(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASigningCertificate_96(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASigningCertificateV2_97(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAContentIdentifier_98(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASignatureTimeStampToken_99(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsSigPolicyID_100(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCommitmentType_101(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsSignerLocation_102(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsSignerAttr_103(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsOtherSigCert_104(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsContentTimestamp_105(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCertificateRefs_106(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsRevocationRefs_107(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCertValues_108(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsRevocationValues_109(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsEscTimeStamp_110(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsCertCrlTimestamp_111(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAEtsArchiveTimestamp_112(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASigPolicyID_113(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAACommitmentType_114(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAASignerLocation_115(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdAAOtherSigCert_116(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdSpqEtsUri_118(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_IdSpqEtsUNotice_119(),
	0,
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_KeyBag_122(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_Pkcs8ShroudedKeyBag_123(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_CertBag_124(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_CrlBag_125(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SecretBag_126(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_SafeContentsBag_127(),
	0,
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd128BitRC4_129(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd40BitRC4_130(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd3KeyTripleDesCbc_131(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd2KeyTripleDesCbc_132(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbeWithShaAnd128BitRC2Cbc_133(),
	PkcsObjectIdentifiers_t1103055686_StaticFields::get_offset_of_PbewithShaAnd40BitRC2Cbc_134(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (RsassaPssParameters_t1481623005), -1, sizeof(RsassaPssParameters_t1481623005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2634[8] = 
{
	RsassaPssParameters_t1481623005::get_offset_of_hashAlgorithm_2(),
	RsassaPssParameters_t1481623005::get_offset_of_maskGenAlgorithm_3(),
	RsassaPssParameters_t1481623005::get_offset_of_saltLength_4(),
	RsassaPssParameters_t1481623005::get_offset_of_trailerField_5(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultHashAlgorithm_6(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultMaskGenFunction_7(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultSaltLength_8(),
	RsassaPssParameters_t1481623005_StaticFields::get_offset_of_DefaultTrailerField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (SignedData_t1315018810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[6] = 
{
	SignedData_t1315018810::get_offset_of_version_2(),
	SignedData_t1315018810::get_offset_of_digestAlgorithms_3(),
	SignedData_t1315018810::get_offset_of_contentInfo_4(),
	SignedData_t1315018810::get_offset_of_certificates_5(),
	SignedData_t1315018810::get_offset_of_crls_6(),
	SignedData_t1315018810::get_offset_of_signerInfos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (SecNamedCurves_t558061916), -1, sizeof(SecNamedCurves_t558061916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2636[3] = 
{
	SecNamedCurves_t558061916_StaticFields::get_offset_of_objIds_0(),
	SecNamedCurves_t558061916_StaticFields::get_offset_of_curves_1(),
	SecNamedCurves_t558061916_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (Secp112r1Holder_t3753151333), -1, sizeof(Secp112r1Holder_t3753151333_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2637[1] = 
{
	Secp112r1Holder_t3753151333_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (Secp112r2Holder_t2526217440), -1, sizeof(Secp112r2Holder_t2526217440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2638[1] = 
{
	Secp112r2Holder_t2526217440_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (Secp128r1Holder_t2159797956), -1, sizeof(Secp128r1Holder_t2159797956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2639[1] = 
{
	Secp128r1Holder_t2159797956_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (Secp128r2Holder_t170864723), -1, sizeof(Secp128r2Holder_t170864723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2640[1] = 
{
	Secp128r2Holder_t170864723_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (Secp160k1Holder_t1154406297), -1, sizeof(Secp160k1Holder_t1154406297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2641[1] = 
{
	Secp160k1Holder_t1154406297_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (Secp160r1Holder_t1153592480), -1, sizeof(Secp160r1Holder_t1153592480_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2642[1] = 
{
	Secp160r1Holder_t1153592480_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (Secp160r2Holder_t3459626543), -1, sizeof(Secp160r2Holder_t3459626543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2643[1] = 
{
	Secp160r2Holder_t3459626543_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (Secp192k1Holder_t3762826772), -1, sizeof(Secp192k1Holder_t3762826772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2644[1] = 
{
	Secp192k1Holder_t3762826772_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (Secp192r1Holder_t3762621805), -1, sizeof(Secp192r1Holder_t3762621805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2645[1] = 
{
	Secp192r1Holder_t3762621805_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (Secp224k1Holder_t1547647928), -1, sizeof(Secp224k1Holder_t1547647928_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2646[1] = 
{
	Secp224k1Holder_t1547647928_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (Secp224r1Holder_t1546834111), -1, sizeof(Secp224r1Holder_t1546834111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2647[1] = 
{
	Secp224r1Holder_t1546834111_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (Secp256k1Holder_t3240468911), -1, sizeof(Secp256k1Holder_t3240468911_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2648[1] = 
{
	Secp256k1Holder_t3240468911_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (Secp256r1Holder_t3239722744), -1, sizeof(Secp256r1Holder_t3239722744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2649[1] = 
{
	Secp256r1Holder_t3239722744_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (Secp384r1Holder_t1133933760), -1, sizeof(Secp384r1Holder_t1133933760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2650[1] = 
{
	Secp384r1Holder_t1133933760_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (Secp521r1Holder_t4048294485), -1, sizeof(Secp521r1Holder_t4048294485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[1] = 
{
	Secp521r1Holder_t4048294485_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (Sect113r1Holder_t2345758350), -1, sizeof(Sect113r1Holder_t2345758350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2652[3] = 
{
	Sect113r1Holder_t2345758350_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (Sect113r2Holder_t14293759), -1, sizeof(Sect113r2Holder_t14293759_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2653[3] = 
{
	Sect113r2Holder_t14293759_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (Sect131r1Holder_t4037539526), -1, sizeof(Sect131r1Holder_t4037539526_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2654[5] = 
{
	Sect131r1Holder_t4037539526_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (Sect131r2Holder_t944075595), -1, sizeof(Sect131r2Holder_t944075595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2655[5] = 
{
	Sect131r2Holder_t944075595_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (Sect163k1Holder_t2343088644), -1, sizeof(Sect163k1Holder_t2343088644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2656[5] = 
{
	Sect163k1Holder_t2343088644_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (Sect163r1Holder_t2342274827), -1, sizeof(Sect163r1Holder_t2342274827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2657[5] = 
{
	Sect163r1Holder_t2342274827_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (Sect163r2Holder_t10810236), -1, sizeof(Sect163r2Holder_t10810236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2658[5] = 
{
	Sect163r2Holder_t10810236_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (Sect193r1Holder_t2355228822), -1, sizeof(Sect193r1Holder_t2355228822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2659[3] = 
{
	Sect193r1Holder_t2355228822_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (Sect193r2Holder_t23764231), -1, sizeof(Sect193r2Holder_t23764231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2660[3] = 
{
	Sect193r2Holder_t23764231_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (Sect233k1Holder_t164911864), -1, sizeof(Sect233k1Holder_t164911864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2661[3] = 
{
	Sect233k1Holder_t164911864_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (Sect233r1Holder_t164174145), -1, sizeof(Sect233r1Holder_t164174145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2662[3] = 
{
	Sect233r1Holder_t164174145_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (Sect239k1Holder_t2003561066), -1, sizeof(Sect239k1Holder_t2003561066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2663[3] = 
{
	Sect239k1Holder_t2003561066_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (Sect283k1Holder_t170425007), -1, sizeof(Sect283k1Holder_t170425007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2664[5] = 
{
	Sect283k1Holder_t170425007_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (Sect283r1Holder_t170160838), -1, sizeof(Sect283r1Holder_t170160838_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2665[5] = 
{
	Sect283r1Holder_t170160838_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (Sect409k1Holder_t2814401075), -1, sizeof(Sect409k1Holder_t2814401075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2666[3] = 
{
	Sect409k1Holder_t2814401075_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (Sect409r1Holder_t2813595706), -1, sizeof(Sect409r1Holder_t2813595706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2667[3] = 
{
	Sect409r1Holder_t2813595706_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (Sect571k1Holder_t4201946253), -1, sizeof(Sect571k1Holder_t4201946253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2668[5] = 
{
	Sect571k1Holder_t4201946253_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (Sect571r1Holder_t4201208534), -1, sizeof(Sect571r1Holder_t4201208534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2669[5] = 
{
	Sect571r1Holder_t4201208534_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (SecObjectIdentifiers_t3901542888), -1, sizeof(SecObjectIdentifiers_t3901542888_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2670[34] = 
{
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_EllipticCurve_0(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT163k1_1(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT163r1_2(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT239k1_3(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT113r1_4(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT113r2_5(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP112r1_6(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP112r2_7(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP160r1_8(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP160k1_9(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP256k1_10(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT163r2_11(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT283k1_12(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT283r1_13(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT131r1_14(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT131r2_15(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT193r1_16(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT193r2_17(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT233k1_18(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT233r1_19(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP128r1_20(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP128r2_21(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP160r2_22(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP192k1_23(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP224k1_24(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP224r1_25(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP384r1_26(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP521r1_27(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT409k1_28(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT409r1_29(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT571k1_30(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecT571r1_31(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP192r1_32(),
	SecObjectIdentifiers_t3901542888_StaticFields::get_offset_of_SecP256r1_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (TeleTrusTNamedCurves_t3126647113), -1, sizeof(TeleTrusTNamedCurves_t3126647113_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2671[3] = 
{
	TeleTrusTNamedCurves_t3126647113_StaticFields::get_offset_of_objIds_0(),
	TeleTrusTNamedCurves_t3126647113_StaticFields::get_offset_of_curves_1(),
	TeleTrusTNamedCurves_t3126647113_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (BrainpoolP160r1Holder_t983939287), -1, sizeof(BrainpoolP160r1Holder_t983939287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2672[1] = 
{
	BrainpoolP160r1Holder_t983939287_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (BrainpoolP160t1Holder_t983863441), -1, sizeof(BrainpoolP160t1Holder_t983863441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2673[1] = 
{
	BrainpoolP160t1Holder_t983863441_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (BrainpoolP192r1Holder_t166682126), -1, sizeof(BrainpoolP192r1Holder_t166682126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2674[1] = 
{
	BrainpoolP192r1Holder_t166682126_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (BrainpoolP192t1Holder_t167037012), -1, sizeof(BrainpoolP192t1Holder_t167037012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2675[1] = 
{
	BrainpoolP192t1Holder_t167037012_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (BrainpoolP224r1Holder_t1446647738), -1, sizeof(BrainpoolP224r1Holder_t1446647738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2676[1] = 
{
	BrainpoolP224r1Holder_t1446647738_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (BrainpoolP224t1Holder_t1446858876), -1, sizeof(BrainpoolP224t1Holder_t1446858876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2677[1] = 
{
	BrainpoolP224t1Holder_t1446858876_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (BrainpoolP256r1Holder_t2284807557), -1, sizeof(BrainpoolP256r1Holder_t2284807557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2678[1] = 
{
	BrainpoolP256r1Holder_t2284807557_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (BrainpoolP256t1Holder_t2284875459), -1, sizeof(BrainpoolP256t1Holder_t2284875459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2679[1] = 
{
	BrainpoolP256t1Holder_t2284875459_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (BrainpoolP320r1Holder_t1625039561), -1, sizeof(BrainpoolP320r1Holder_t1625039561_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2680[1] = 
{
	BrainpoolP320r1Holder_t1625039561_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (BrainpoolP320t1Holder_t1625250699), -1, sizeof(BrainpoolP320t1Holder_t1625250699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2681[1] = 
{
	BrainpoolP320t1Holder_t1625250699_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (BrainpoolP384r1Holder_t1017516731), -1, sizeof(BrainpoolP384r1Holder_t1017516731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2682[1] = 
{
	BrainpoolP384r1Holder_t1017516731_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (BrainpoolP384t1Holder_t1017871617), -1, sizeof(BrainpoolP384t1Holder_t1017871617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2683[1] = 
{
	BrainpoolP384t1Holder_t1017871617_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (BrainpoolP512r1Holder_t4283766514), -1, sizeof(BrainpoolP512r1Holder_t4283766514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2684[1] = 
{
	BrainpoolP512r1Holder_t4283766514_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (BrainpoolP512t1Holder_t4284121400), -1, sizeof(BrainpoolP512t1Holder_t4284121400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2685[1] = 
{
	BrainpoolP512t1Holder_t4284121400_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (TeleTrusTObjectIdentifiers_t2978504789), -1, sizeof(TeleTrusTObjectIdentifiers_t2978504789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2686[28] = 
{
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_TeleTrusTAlgorithm_0(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RipeMD160_1(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RipeMD128_2(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RipeMD256_3(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_TeleTrusTRsaSignatureAlgorithm_4(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RsaSignatureWithRipeMD160_5(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RsaSignatureWithRipeMD128_6(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_RsaSignatureWithRipeMD256_7(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_ECSign_8(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_ECSignWithSha1_9(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_ECSignWithRipeMD160_10(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_EccBrainpool_11(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_EllipticCurve_12(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_VersionOne_13(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP160R1_14(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP160T1_15(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP192R1_16(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP192T1_17(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP224R1_18(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP224T1_19(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP256R1_20(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP256T1_21(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP320R1_22(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP320T1_23(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP384R1_24(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP384T1_25(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP512R1_26(),
	TeleTrusTObjectIdentifiers_t2978504789_StaticFields::get_offset_of_BrainpoolP512T1_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (Asn1Dump_t4013099873), -1, sizeof(Asn1Dump_t4013099873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2687[3] = 
{
	Asn1Dump_t4013099873_StaticFields::get_offset_of_NewLine_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (FilterStream_t3440239854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[1] = 
{
	FilterStream_t3440239854::get_offset_of_s_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (AlgorithmIdentifier_t2670781410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[2] = 
{
	AlgorithmIdentifier_t2670781410::get_offset_of_algorithm_2(),
	AlgorithmIdentifier_t2670781410::get_offset_of_parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (BasicConstraints_t3459049714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[2] = 
{
	BasicConstraints_t3459049714::get_offset_of_cA_2(),
	BasicConstraints_t3459049714::get_offset_of_pathLenConstraint_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (CrlDistPoint_t3073089489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[1] = 
{
	CrlDistPoint_t3073089489::get_offset_of_seq_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (CrlNumber_t1759393128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (CrlReason_t3883542487), -1, sizeof(CrlReason_t3883542487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2693[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	CrlReason_t3883542487_StaticFields::get_offset_of_ReasonString_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (CertificateList_t2288802675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[3] = 
{
	CertificateList_t2288802675::get_offset_of_tbsCertList_2(),
	CertificateList_t2288802675::get_offset_of_sigAlgID_3(),
	CertificateList_t2288802675::get_offset_of_sig_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (DsaParameter_t274855281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[3] = 
{
	DsaParameter_t274855281::get_offset_of_p_2(),
	DsaParameter_t274855281::get_offset_of_q_3(),
	DsaParameter_t274855281::get_offset_of_g_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (DigestInfo_t202014884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[2] = 
{
	DigestInfo_t202014884::get_offset_of_digest_2(),
	DigestInfo_t202014884::get_offset_of_algID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (DistributionPoint_t769724552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[3] = 
{
	DistributionPoint_t769724552::get_offset_of_distributionPoint_2(),
	DistributionPoint_t769724552::get_offset_of_reasons_3(),
	DistributionPoint_t769724552::get_offset_of_cRLIssuer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (DistributionPointName_t1765286135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[4] = 
{
	DistributionPointName_t1765286135::get_offset_of_name_2(),
	DistributionPointName_t1765286135::get_offset_of_type_3(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (GeneralName_t294965175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GeneralName_t294965175::get_offset_of_obj_11(),
	GeneralName_t294965175::get_offset_of_tag_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
