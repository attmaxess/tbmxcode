﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Gener1290955016.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Issui3776707264.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_KeyUsa785552764.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_RsaPub197361851.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Reason677892991.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Subje3547422518.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_CrlEn4200172927.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCer741385336.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCert64679979.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCe2937350180.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_TbsCe1391133771.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_Time2566907995.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509C3705285294.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509De518486245.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509E2020524125.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509E1384530060.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509N2936077305.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509N1972691209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509N3449226918.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X509_X509Ob346896425.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_DHDomai3489907266.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_DHPubli3181392102.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_DHValid4010119324.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_ECNamed3889450962.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2357232190.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1386193459.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1385946192.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1385986417.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam4025924969.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam4025965190.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam4025717927.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Name299104776.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1991767207.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1991663432.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam1991695209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3726533506.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2622364672.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2622117405.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2622157630.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3381865698.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3623772131.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3623731910.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3623700133.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3772636393.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3252909381.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3160589090.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam3617495651.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Nam2773896637.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X962Par1137405623.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Curve2327439944.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9ECPar1959966001.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9ECPar1566440261.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9ECPoi3031020555.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9FieldE706679609.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Field2012644846.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Integ1331659747.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_X9_X9Object773243058.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Asymmetr1825508216.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Asymmetr1663727050.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Buffered2590109294.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedA862890205.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedB711630611.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Buffered3241650255.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedI295552809.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_BufferedS702588694.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Check1143849288.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_CipherKe2313639529.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_CryptoEx2532638102.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_DataLengt677974397.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_InvalidCi962650396.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_KeyGenera650995725.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_MaxBytes2161670937.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_OutputLe3343085798.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_PbeParame148045164.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Agreemen3319300478.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Agreemen2812231168.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_3607278510.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_1877917918.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (GeneralNames_t1290955016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[1] = 
{
	GeneralNames_t1290955016::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (IssuingDistributionPoint_t3776707264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[7] = 
{
	IssuingDistributionPoint_t3776707264::get_offset_of__distributionPoint_2(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlyContainsUserCerts_3(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlyContainsCACerts_4(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlySomeReasons_5(),
	IssuingDistributionPoint_t3776707264::get_offset_of__indirectCRL_6(),
	IssuingDistributionPoint_t3776707264::get_offset_of__onlyContainsAttributeCerts_7(),
	IssuingDistributionPoint_t3776707264::get_offset_of_seq_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (KeyUsage_t785552764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (RsaPublicKeyStructure_t197361851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[2] = 
{
	RsaPublicKeyStructure_t197361851::get_offset_of_modulus_2(),
	RsaPublicKeyStructure_t197361851::get_offset_of_publicExponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (ReasonFlags_t677892991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (SubjectPublicKeyInfo_t3547422518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	SubjectPublicKeyInfo_t3547422518::get_offset_of_algID_2(),
	SubjectPublicKeyInfo_t3547422518::get_offset_of_keyData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (CrlEntry_t4200172927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[4] = 
{
	CrlEntry_t4200172927::get_offset_of_seq_2(),
	CrlEntry_t4200172927::get_offset_of_userCertificate_3(),
	CrlEntry_t4200172927::get_offset_of_revocationDate_4(),
	CrlEntry_t4200172927::get_offset_of_crlEntryExtensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (TbsCertificateList_t741385336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[8] = 
{
	TbsCertificateList_t741385336::get_offset_of_seq_2(),
	TbsCertificateList_t741385336::get_offset_of_version_3(),
	TbsCertificateList_t741385336::get_offset_of_signature_4(),
	TbsCertificateList_t741385336::get_offset_of_issuer_5(),
	TbsCertificateList_t741385336::get_offset_of_thisUpdate_6(),
	TbsCertificateList_t741385336::get_offset_of_nextUpdate_7(),
	TbsCertificateList_t741385336::get_offset_of_revokedCertificates_8(),
	TbsCertificateList_t741385336::get_offset_of_crlExtensions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (RevokedCertificatesEnumeration_t64679979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[1] = 
{
	RevokedCertificatesEnumeration_t64679979::get_offset_of_en_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (RevokedCertificatesEnumerator_t2937350180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[1] = 
{
	RevokedCertificatesEnumerator_t2937350180::get_offset_of_e_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (TbsCertificateStructure_t1391133771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[12] = 
{
	TbsCertificateStructure_t1391133771::get_offset_of_seq_2(),
	TbsCertificateStructure_t1391133771::get_offset_of_version_3(),
	TbsCertificateStructure_t1391133771::get_offset_of_serialNumber_4(),
	TbsCertificateStructure_t1391133771::get_offset_of_signature_5(),
	TbsCertificateStructure_t1391133771::get_offset_of_issuer_6(),
	TbsCertificateStructure_t1391133771::get_offset_of_startDate_7(),
	TbsCertificateStructure_t1391133771::get_offset_of_endDate_8(),
	TbsCertificateStructure_t1391133771::get_offset_of_subject_9(),
	TbsCertificateStructure_t1391133771::get_offset_of_subjectPublicKeyInfo_10(),
	TbsCertificateStructure_t1391133771::get_offset_of_issuerUniqueID_11(),
	TbsCertificateStructure_t1391133771::get_offset_of_subjectUniqueID_12(),
	TbsCertificateStructure_t1391133771::get_offset_of_extensions_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (Time_t2566907995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[1] = 
{
	Time_t2566907995::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (X509CertificateStructure_t3705285294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	X509CertificateStructure_t3705285294::get_offset_of_tbsCert_2(),
	X509CertificateStructure_t3705285294::get_offset_of_sigAlgID_3(),
	X509CertificateStructure_t3705285294::get_offset_of_sig_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (X509DefaultEntryConverter_t518486245), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (X509Extension_t2020524125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[2] = 
{
	X509Extension_t2020524125::get_offset_of_critical_0(),
	X509Extension_t2020524125::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (X509Extensions_t1384530060), -1, sizeof(X509Extensions_t1384530060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2715[33] = 
{
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectDirectoryAttributes_2(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectKeyIdentifier_3(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_KeyUsage_4(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_PrivateKeyUsagePeriod_5(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectAlternativeName_6(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_IssuerAlternativeName_7(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_BasicConstraints_8(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CrlNumber_9(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_ReasonCode_10(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_InstructionCode_11(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_InvalidityDate_12(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_DeltaCrlIndicator_13(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_IssuingDistributionPoint_14(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CertificateIssuer_15(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_NameConstraints_16(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CrlDistributionPoints_17(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_CertificatePolicies_18(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_PolicyMappings_19(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_AuthorityKeyIdentifier_20(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_PolicyConstraints_21(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_ExtendedKeyUsage_22(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_FreshestCrl_23(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_InhibitAnyPolicy_24(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_AuthorityInfoAccess_25(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_SubjectInfoAccess_26(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_LogoType_27(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_BiometricInfo_28(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_QCStatements_29(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_AuditIdentity_30(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_NoRevAvail_31(),
	X509Extensions_t1384530060_StaticFields::get_offset_of_TargetInformation_32(),
	X509Extensions_t1384530060::get_offset_of_extensions_33(),
	X509Extensions_t1384530060::get_offset_of_ordering_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (X509Name_t2936077305), -1, sizeof(X509Name_t2936077305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2716[44] = 
{
	X509Name_t2936077305_StaticFields::get_offset_of_C_2(),
	X509Name_t2936077305_StaticFields::get_offset_of_O_3(),
	X509Name_t2936077305_StaticFields::get_offset_of_OU_4(),
	X509Name_t2936077305_StaticFields::get_offset_of_T_5(),
	X509Name_t2936077305_StaticFields::get_offset_of_CN_6(),
	X509Name_t2936077305_StaticFields::get_offset_of_Street_7(),
	X509Name_t2936077305_StaticFields::get_offset_of_SerialNumber_8(),
	X509Name_t2936077305_StaticFields::get_offset_of_L_9(),
	X509Name_t2936077305_StaticFields::get_offset_of_ST_10(),
	X509Name_t2936077305_StaticFields::get_offset_of_Surname_11(),
	X509Name_t2936077305_StaticFields::get_offset_of_GivenName_12(),
	X509Name_t2936077305_StaticFields::get_offset_of_Initials_13(),
	X509Name_t2936077305_StaticFields::get_offset_of_Generation_14(),
	X509Name_t2936077305_StaticFields::get_offset_of_UniqueIdentifier_15(),
	X509Name_t2936077305_StaticFields::get_offset_of_BusinessCategory_16(),
	X509Name_t2936077305_StaticFields::get_offset_of_PostalCode_17(),
	X509Name_t2936077305_StaticFields::get_offset_of_DnQualifier_18(),
	X509Name_t2936077305_StaticFields::get_offset_of_Pseudonym_19(),
	X509Name_t2936077305_StaticFields::get_offset_of_DateOfBirth_20(),
	X509Name_t2936077305_StaticFields::get_offset_of_PlaceOfBirth_21(),
	X509Name_t2936077305_StaticFields::get_offset_of_Gender_22(),
	X509Name_t2936077305_StaticFields::get_offset_of_CountryOfCitizenship_23(),
	X509Name_t2936077305_StaticFields::get_offset_of_CountryOfResidence_24(),
	X509Name_t2936077305_StaticFields::get_offset_of_NameAtBirth_25(),
	X509Name_t2936077305_StaticFields::get_offset_of_PostalAddress_26(),
	X509Name_t2936077305_StaticFields::get_offset_of_DmdName_27(),
	X509Name_t2936077305_StaticFields::get_offset_of_TelephoneNumber_28(),
	X509Name_t2936077305_StaticFields::get_offset_of_Name_29(),
	X509Name_t2936077305_StaticFields::get_offset_of_EmailAddress_30(),
	X509Name_t2936077305_StaticFields::get_offset_of_UnstructuredName_31(),
	X509Name_t2936077305_StaticFields::get_offset_of_UnstructuredAddress_32(),
	X509Name_t2936077305_StaticFields::get_offset_of_E_33(),
	X509Name_t2936077305_StaticFields::get_offset_of_DC_34(),
	X509Name_t2936077305_StaticFields::get_offset_of_UID_35(),
	X509Name_t2936077305_StaticFields::get_offset_of_defaultReverse_36(),
	X509Name_t2936077305_StaticFields::get_offset_of_DefaultSymbols_37(),
	X509Name_t2936077305_StaticFields::get_offset_of_RFC2253Symbols_38(),
	X509Name_t2936077305_StaticFields::get_offset_of_RFC1779Symbols_39(),
	X509Name_t2936077305_StaticFields::get_offset_of_DefaultLookup_40(),
	X509Name_t2936077305::get_offset_of_ordering_41(),
	X509Name_t2936077305::get_offset_of_converter_42(),
	X509Name_t2936077305::get_offset_of_values_43(),
	X509Name_t2936077305::get_offset_of_added_44(),
	X509Name_t2936077305::get_offset_of_seq_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (X509NameEntryConverter_t1972691209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (X509NameTokenizer_t3449226918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[4] = 
{
	X509NameTokenizer_t3449226918::get_offset_of_value_0(),
	X509NameTokenizer_t3449226918::get_offset_of_index_1(),
	X509NameTokenizer_t3449226918::get_offset_of_separator_2(),
	X509NameTokenizer_t3449226918::get_offset_of_buffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (X509ObjectIdentifiers_t346896425), -1, sizeof(X509ObjectIdentifiers_t346896425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2719[20] = 
{
	0,
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_CommonName_1(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_CountryName_2(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_LocalityName_3(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_StateOrProvinceName_4(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_Organization_5(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_OrganizationalUnitName_6(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_id_at_telephoneNumber_7(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_id_at_name_8(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdSha1_9(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_RipeMD160_10(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_RipeMD160WithRsaEncryption_11(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdEARsa_12(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdPkix_13(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdPE_14(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdAD_15(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdADCAIssuers_16(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_IdADOcsp_17(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_OcspAccessMethod_18(),
	X509ObjectIdentifiers_t346896425_StaticFields::get_offset_of_CrlAccessMethod_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (DHDomainParameters_t3489907266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[5] = 
{
	DHDomainParameters_t3489907266::get_offset_of_p_2(),
	DHDomainParameters_t3489907266::get_offset_of_g_3(),
	DHDomainParameters_t3489907266::get_offset_of_q_4(),
	DHDomainParameters_t3489907266::get_offset_of_j_5(),
	DHDomainParameters_t3489907266::get_offset_of_validationParms_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (DHPublicKey_t3181392102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[1] = 
{
	DHPublicKey_t3181392102::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (DHValidationParms_t4010119324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	DHValidationParms_t4010119324::get_offset_of_seed_2(),
	DHValidationParms_t4010119324::get_offset_of_pgenCounter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (ECNamedCurveTable_t3889450962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (X962NamedCurves_t2357232190), -1, sizeof(X962NamedCurves_t2357232190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2724[3] = 
{
	X962NamedCurves_t2357232190_StaticFields::get_offset_of_objIds_0(),
	X962NamedCurves_t2357232190_StaticFields::get_offset_of_curves_1(),
	X962NamedCurves_t2357232190_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (Prime192v1Holder_t1386193459), -1, sizeof(Prime192v1Holder_t1386193459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2725[1] = 
{
	Prime192v1Holder_t1386193459_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (Prime192v2Holder_t1385946192), -1, sizeof(Prime192v2Holder_t1385946192_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2726[1] = 
{
	Prime192v2Holder_t1385946192_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (Prime192v3Holder_t1385986417), -1, sizeof(Prime192v3Holder_t1385986417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2727[1] = 
{
	Prime192v3Holder_t1385986417_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (Prime239v1Holder_t4025924969), -1, sizeof(Prime239v1Holder_t4025924969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2728[1] = 
{
	Prime239v1Holder_t4025924969_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (Prime239v2Holder_t4025965190), -1, sizeof(Prime239v2Holder_t4025965190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2729[1] = 
{
	Prime239v2Holder_t4025965190_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (Prime239v3Holder_t4025717927), -1, sizeof(Prime239v3Holder_t4025717927_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2730[1] = 
{
	Prime239v3Holder_t4025717927_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (Prime256v1Holder_t299104776), -1, sizeof(Prime256v1Holder_t299104776_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2731[1] = 
{
	Prime256v1Holder_t299104776_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (C2pnb163v1Holder_t1991767207), -1, sizeof(C2pnb163v1Holder_t1991767207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2732[1] = 
{
	C2pnb163v1Holder_t1991767207_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (C2pnb163v2Holder_t1991663432), -1, sizeof(C2pnb163v2Holder_t1991663432_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2733[1] = 
{
	C2pnb163v2Holder_t1991663432_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (C2pnb163v3Holder_t1991695209), -1, sizeof(C2pnb163v3Holder_t1991695209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2734[1] = 
{
	C2pnb163v3Holder_t1991695209_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (C2pnb176w1Holder_t3726533506), -1, sizeof(C2pnb176w1Holder_t3726533506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2735[1] = 
{
	C2pnb176w1Holder_t3726533506_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (C2tnb191v1Holder_t2622364672), -1, sizeof(C2tnb191v1Holder_t2622364672_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2736[1] = 
{
	C2tnb191v1Holder_t2622364672_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (C2tnb191v2Holder_t2622117405), -1, sizeof(C2tnb191v2Holder_t2622117405_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2737[1] = 
{
	C2tnb191v2Holder_t2622117405_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (C2tnb191v3Holder_t2622157630), -1, sizeof(C2tnb191v3Holder_t2622157630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2738[1] = 
{
	C2tnb191v3Holder_t2622157630_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (C2pnb208w1Holder_t3381865698), -1, sizeof(C2pnb208w1Holder_t3381865698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2739[1] = 
{
	C2pnb208w1Holder_t3381865698_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (C2tnb239v1Holder_t3623772131), -1, sizeof(C2tnb239v1Holder_t3623772131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2740[1] = 
{
	C2tnb239v1Holder_t3623772131_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (C2tnb239v2Holder_t3623731910), -1, sizeof(C2tnb239v2Holder_t3623731910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2741[1] = 
{
	C2tnb239v2Holder_t3623731910_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (C2tnb239v3Holder_t3623700133), -1, sizeof(C2tnb239v3Holder_t3623700133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2742[1] = 
{
	C2tnb239v3Holder_t3623700133_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (C2pnb272w1Holder_t3772636393), -1, sizeof(C2pnb272w1Holder_t3772636393_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2743[1] = 
{
	C2pnb272w1Holder_t3772636393_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (C2pnb304w1Holder_t3252909381), -1, sizeof(C2pnb304w1Holder_t3252909381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[1] = 
{
	C2pnb304w1Holder_t3252909381_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (C2tnb359v1Holder_t3160589090), -1, sizeof(C2tnb359v1Holder_t3160589090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[1] = 
{
	C2tnb359v1Holder_t3160589090_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (C2pnb368w1Holder_t3617495651), -1, sizeof(C2pnb368w1Holder_t3617495651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2746[1] = 
{
	C2pnb368w1Holder_t3617495651_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (C2tnb431r1Holder_t2773896637), -1, sizeof(C2tnb431r1Holder_t2773896637_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2747[1] = 
{
	C2tnb431r1Holder_t2773896637_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (X962Parameters_t1137405623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[1] = 
{
	X962Parameters_t1137405623::get_offset_of__params_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (X9Curve_t2327439944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[3] = 
{
	X9Curve_t2327439944::get_offset_of_curve_2(),
	X9Curve_t2327439944::get_offset_of_seed_3(),
	X9Curve_t2327439944::get_offset_of_fieldIdentifier_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (X9ECParameters_t1959966001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[6] = 
{
	X9ECParameters_t1959966001::get_offset_of_fieldID_2(),
	X9ECParameters_t1959966001::get_offset_of_curve_3(),
	X9ECParameters_t1959966001::get_offset_of_g_4(),
	X9ECParameters_t1959966001::get_offset_of_n_5(),
	X9ECParameters_t1959966001::get_offset_of_h_6(),
	X9ECParameters_t1959966001::get_offset_of_seed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (X9ECParametersHolder_t1566440261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[1] = 
{
	X9ECParametersHolder_t1566440261::get_offset_of_parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (X9ECPoint_t3031020555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[3] = 
{
	X9ECPoint_t3031020555::get_offset_of_encoding_2(),
	X9ECPoint_t3031020555::get_offset_of_c_3(),
	X9ECPoint_t3031020555::get_offset_of_p_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (X9FieldElement_t706679609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[1] = 
{
	X9FieldElement_t706679609::get_offset_of_f_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (X9FieldID_t2012644846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[2] = 
{
	X9FieldID_t2012644846::get_offset_of_id_2(),
	X9FieldID_t2012644846::get_offset_of_parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (X9IntegerConverter_t1331659747), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (X9ObjectIdentifiers_t773243058), -1, sizeof(X9ObjectIdentifiers_t773243058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2756[66] = 
{
	0,
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ansi_X9_62_1(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdFieldType_2(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_PrimeField_3(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_CharacteristicTwoField_4(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_GNBasis_5(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_TPBasis_6(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_PPBasis_7(),
	0,
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_id_ecSigType_9(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha1_10(),
	0,
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_id_publicKeyType_12(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdECPublicKey_13(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha2_14(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha224_15(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha256_16(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha384_17(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ECDsaWithSha512_18(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_EllipticCurve_19(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_CTwoCurve_20(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb163v1_21(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb163v2_22(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb163v3_23(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb176w1_24(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb191v1_25(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb191v2_26(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb191v3_27(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb191v4_28(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb191v5_29(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb208w1_30(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb239v1_31(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb239v2_32(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb239v3_33(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb239v4_34(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Onb239v5_35(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb272w1_36(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb304w1_37(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb359v1_38(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Pnb368w1_39(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_C2Tnb431r1_40(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_PrimeCurve_41(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime192v1_42(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime192v2_43(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime192v3_44(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime239v1_45(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime239v2_46(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime239v3_47(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Prime256v1_48(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdDsa_49(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_IdDsaWithSha1_50(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_X9x63Scheme_51(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHSinglePassStdDHSha1KdfScheme_52(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHSinglePassCofactorDHSha1KdfScheme_53(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_MqvSinglePassSha1KdfScheme_54(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_ansi_x9_42_55(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHPublicNumber_56(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_X9x42Schemes_57(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHStatic_58(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHEphem_59(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHOneFlow_60(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHHybrid1_61(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHHybrid2_62(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_DHHybridOneFlow_63(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Mqv2_64(),
	X9ObjectIdentifiers_t773243058_StaticFields::get_offset_of_Mqv1_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (AsymmetricCipherKeyPair_t1825508216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[2] = 
{
	AsymmetricCipherKeyPair_t1825508216::get_offset_of_publicParameter_0(),
	AsymmetricCipherKeyPair_t1825508216::get_offset_of_privateParameter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (AsymmetricKeyParameter_t1663727050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[1] = 
{
	AsymmetricKeyParameter_t1663727050::get_offset_of_privateKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (BufferedAeadBlockCipher_t2590109294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[1] = 
{
	BufferedAeadBlockCipher_t2590109294::get_offset_of_cipher_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (BufferedAsymmetricBlockCipher_t862890205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[3] = 
{
	BufferedAsymmetricBlockCipher_t862890205::get_offset_of_cipher_1(),
	BufferedAsymmetricBlockCipher_t862890205::get_offset_of_buffer_2(),
	BufferedAsymmetricBlockCipher_t862890205::get_offset_of_bufOff_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (BufferedBlockCipher_t711630611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[4] = 
{
	BufferedBlockCipher_t711630611::get_offset_of_buf_1(),
	BufferedBlockCipher_t711630611::get_offset_of_bufOff_2(),
	BufferedBlockCipher_t711630611::get_offset_of_forEncryption_3(),
	BufferedBlockCipher_t711630611::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (BufferedCipherBase_t3241650255), -1, sizeof(BufferedCipherBase_t3241650255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2762[1] = 
{
	BufferedCipherBase_t3241650255_StaticFields::get_offset_of_EmptyBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (BufferedIesCipher_t295552809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[3] = 
{
	BufferedIesCipher_t295552809::get_offset_of_engine_1(),
	BufferedIesCipher_t295552809::get_offset_of_forEncryption_2(),
	BufferedIesCipher_t295552809::get_offset_of_buffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (BufferedStreamCipher_t702588694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[1] = 
{
	BufferedStreamCipher_t702588694::get_offset_of_cipher_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (Check_t1143849288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (CipherKeyGenerator_t2313639529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[4] = 
{
	CipherKeyGenerator_t2313639529::get_offset_of_random_0(),
	CipherKeyGenerator_t2313639529::get_offset_of_strength_1(),
	CipherKeyGenerator_t2313639529::get_offset_of_uninitialised_2(),
	CipherKeyGenerator_t2313639529::get_offset_of_defaultStrength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (CryptoException_t2532638102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (DataLengthException_t677974397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (InvalidCipherTextException_t962650396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (KeyGenerationParameters_t650995725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[2] = 
{
	KeyGenerationParameters_t650995725::get_offset_of_random_0(),
	KeyGenerationParameters_t650995725::get_offset_of_strength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (MaxBytesExceededException_t2161670937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (OutputLengthException_t3343085798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (PbeParametersGenerator_t148045164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[3] = 
{
	PbeParametersGenerator_t148045164::get_offset_of_mPassword_0(),
	PbeParametersGenerator_t148045164::get_offset_of_mSalt_1(),
	PbeParametersGenerator_t148045164::get_offset_of_mIterationCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (DHBasicAgreement_t3319300478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[2] = 
{
	DHBasicAgreement_t3319300478::get_offset_of_key_0(),
	DHBasicAgreement_t3319300478::get_offset_of_dhParams_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (ECDHBasicAgreement_t2812231168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[1] = 
{
	ECDHBasicAgreement_t2812231168::get_offset_of_privKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (Gost3411Digest_t3607278510), -1, sizeof(Gost3411Digest_t3607278510_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2798[20] = 
{
	0,
	Gost3411Digest_t3607278510::get_offset_of_H_1(),
	Gost3411Digest_t3607278510::get_offset_of_L_2(),
	Gost3411Digest_t3607278510::get_offset_of_M_3(),
	Gost3411Digest_t3607278510::get_offset_of_Sum_4(),
	Gost3411Digest_t3607278510::get_offset_of_C_5(),
	Gost3411Digest_t3607278510::get_offset_of_xBuf_6(),
	Gost3411Digest_t3607278510::get_offset_of_xBufOff_7(),
	Gost3411Digest_t3607278510::get_offset_of_byteCount_8(),
	Gost3411Digest_t3607278510::get_offset_of_cipher_9(),
	Gost3411Digest_t3607278510::get_offset_of_sBox_10(),
	Gost3411Digest_t3607278510::get_offset_of_K_11(),
	Gost3411Digest_t3607278510::get_offset_of_a_12(),
	Gost3411Digest_t3607278510::get_offset_of_wS_13(),
	Gost3411Digest_t3607278510::get_offset_of_w_S_14(),
	Gost3411Digest_t3607278510::get_offset_of_S_15(),
	Gost3411Digest_t3607278510::get_offset_of_U_16(),
	Gost3411Digest_t3607278510::get_offset_of_V_17(),
	Gost3411Digest_t3607278510::get_offset_of_W_18(),
	Gost3411Digest_t3607278510_StaticFields::get_offset_of_C2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (GeneralDigest_t1877917918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[4] = 
{
	0,
	GeneralDigest_t1877917918::get_offset_of_xBuf_1(),
	GeneralDigest_t1877917918::get_offset_of_xBufOff_2(),
	GeneralDigest_t1877917918::get_offset_of_byteCount_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
