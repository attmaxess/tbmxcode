﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato2293935318.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3004251550.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato1267912930.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3060446089.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3799708011.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3263126028.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_CMa4184605658.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Cbc1613178881.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Mac3850993954.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_CfbB471961930.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Gos1934182742.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_HMa2564819335.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_ISO9911519921.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Pol3186287769.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Sip1664512494.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Vmpc976793133.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Cbc424533760.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Cc2534265899.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Cf1093564787.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ct3347056426.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ea1200038734.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ea3159821658.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc1966957103.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_GOf591890526.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ocb427870536.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Of2804280439.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Op1145356922.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Si3693949395.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc2204846149.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc1216265862.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc1725665332.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operator1210696522.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operator3562239414.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operator2866294513.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operator3817728361.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operators250825146.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operators549551221.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operator3555759188.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operator3509947265.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Operator3967385528.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings3010076608.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings_609688802.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings4046604239.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings2353579457.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings3233791910.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings2660499907.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings1951118083.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2995549197.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1952252333.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter508500971.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter431035336.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3120746414.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1544976430.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2841123689.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter597696274.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4026919406.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4209685231.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2298980877.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2550649858.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1082923572.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3405498240.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2732896323.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3939864474.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3475787761.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1064568751.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3632960452.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter572706344.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter303785198.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter352675504.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1215309569.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2762984431.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3740889069.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1164823134.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter602285121.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2071462699.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter521829201.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2048269886.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter964742789.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4148998567.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter206966936.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1337840219.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter215653120.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2816354209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4146726485.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2760900877.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameters16149445.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameters30556752.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter433842492.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter852146563.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter139417372.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2682613251.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2592312329.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3425534311.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3532059937.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Prng_Cryp405007133.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Prng_Dig1445396782.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (DHParametersHelper_t2293935318), -1, sizeof(DHParametersHelper_t2293935318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2900[4] = 
{
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_Six_0(),
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_primeLists_1(),
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_primeProducts_2(),
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_BigPrimeProducts_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (DsaKeyPairGenerator_t3004251550), -1, sizeof(DsaKeyPairGenerator_t3004251550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2901[2] = 
{
	DsaKeyPairGenerator_t3004251550_StaticFields::get_offset_of_One_0(),
	DsaKeyPairGenerator_t3004251550::get_offset_of_param_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (ECKeyPairGenerator_t1267912930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2902[4] = 
{
	ECKeyPairGenerator_t1267912930::get_offset_of_algorithm_0(),
	ECKeyPairGenerator_t1267912930::get_offset_of_parameters_1(),
	ECKeyPairGenerator_t1267912930::get_offset_of_publicKeyParamSet_2(),
	ECKeyPairGenerator_t1267912930::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (ElGamalKeyPairGenerator_t3060446089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2903[1] = 
{
	ElGamalKeyPairGenerator_t3060446089::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (Poly1305KeyGenerator_t3799708011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (RsaKeyPairGenerator_t3263126028), -1, sizeof(RsaKeyPairGenerator_t3263126028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2905[7] = 
{
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_SPECIAL_E_VALUES_0(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_SPECIAL_E_HIGHEST_1(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_SPECIAL_E_BITS_2(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_One_3(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_DefaultPublicExponent_4(),
	0,
	RsaKeyPairGenerator_t3263126028::get_offset_of_parameters_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (CMac_t4184605658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[11] = 
{
	0,
	0,
	CMac_t4184605658::get_offset_of_ZEROES_2(),
	CMac_t4184605658::get_offset_of_mac_3(),
	CMac_t4184605658::get_offset_of_buf_4(),
	CMac_t4184605658::get_offset_of_bufOff_5(),
	CMac_t4184605658::get_offset_of_cipher_6(),
	CMac_t4184605658::get_offset_of_macSize_7(),
	CMac_t4184605658::get_offset_of_L_8(),
	CMac_t4184605658::get_offset_of_Lu_9(),
	CMac_t4184605658::get_offset_of_Lu2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (CbcBlockCipherMac_t1613178881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[5] = 
{
	CbcBlockCipherMac_t1613178881::get_offset_of_buf_0(),
	CbcBlockCipherMac_t1613178881::get_offset_of_bufOff_1(),
	CbcBlockCipherMac_t1613178881::get_offset_of_cipher_2(),
	CbcBlockCipherMac_t1613178881::get_offset_of_padding_3(),
	CbcBlockCipherMac_t1613178881::get_offset_of_macSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (MacCFBBlockCipher_t3850993954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[5] = 
{
	MacCFBBlockCipher_t3850993954::get_offset_of_IV_0(),
	MacCFBBlockCipher_t3850993954::get_offset_of_cfbV_1(),
	MacCFBBlockCipher_t3850993954::get_offset_of_cfbOutV_2(),
	MacCFBBlockCipher_t3850993954::get_offset_of_blockSize_3(),
	MacCFBBlockCipher_t3850993954::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (CfbBlockCipherMac_t471961930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[6] = 
{
	CfbBlockCipherMac_t471961930::get_offset_of_mac_0(),
	CfbBlockCipherMac_t471961930::get_offset_of_Buffer_1(),
	CfbBlockCipherMac_t471961930::get_offset_of_bufOff_2(),
	CfbBlockCipherMac_t471961930::get_offset_of_cipher_3(),
	CfbBlockCipherMac_t471961930::get_offset_of_padding_4(),
	CfbBlockCipherMac_t471961930::get_offset_of_macSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (Gost28147Mac_t1934182742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[8] = 
{
	0,
	0,
	Gost28147Mac_t1934182742::get_offset_of_bufOff_2(),
	Gost28147Mac_t1934182742::get_offset_of_buf_3(),
	Gost28147Mac_t1934182742::get_offset_of_mac_4(),
	Gost28147Mac_t1934182742::get_offset_of_firstStep_5(),
	Gost28147Mac_t1934182742::get_offset_of_workingKey_6(),
	Gost28147Mac_t1934182742::get_offset_of_S_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (HMac_t2564819335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[9] = 
{
	0,
	0,
	HMac_t2564819335::get_offset_of_digest_2(),
	HMac_t2564819335::get_offset_of_digestSize_3(),
	HMac_t2564819335::get_offset_of_blockLength_4(),
	HMac_t2564819335::get_offset_of_ipadState_5(),
	HMac_t2564819335::get_offset_of_opadState_6(),
	HMac_t2564819335::get_offset_of_inputPad_7(),
	HMac_t2564819335::get_offset_of_outputBuf_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (ISO9797Alg3Mac_t911519921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[8] = 
{
	ISO9797Alg3Mac_t911519921::get_offset_of_mac_0(),
	ISO9797Alg3Mac_t911519921::get_offset_of_buf_1(),
	ISO9797Alg3Mac_t911519921::get_offset_of_bufOff_2(),
	ISO9797Alg3Mac_t911519921::get_offset_of_cipher_3(),
	ISO9797Alg3Mac_t911519921::get_offset_of_padding_4(),
	ISO9797Alg3Mac_t911519921::get_offset_of_macSize_5(),
	ISO9797Alg3Mac_t911519921::get_offset_of_lastKey2_6(),
	ISO9797Alg3Mac_t911519921::get_offset_of_lastKey3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (Poly1305_t3186287769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[23] = 
{
	0,
	Poly1305_t3186287769::get_offset_of_cipher_1(),
	Poly1305_t3186287769::get_offset_of_singleByte_2(),
	Poly1305_t3186287769::get_offset_of_r0_3(),
	Poly1305_t3186287769::get_offset_of_r1_4(),
	Poly1305_t3186287769::get_offset_of_r2_5(),
	Poly1305_t3186287769::get_offset_of_r3_6(),
	Poly1305_t3186287769::get_offset_of_r4_7(),
	Poly1305_t3186287769::get_offset_of_s1_8(),
	Poly1305_t3186287769::get_offset_of_s2_9(),
	Poly1305_t3186287769::get_offset_of_s3_10(),
	Poly1305_t3186287769::get_offset_of_s4_11(),
	Poly1305_t3186287769::get_offset_of_k0_12(),
	Poly1305_t3186287769::get_offset_of_k1_13(),
	Poly1305_t3186287769::get_offset_of_k2_14(),
	Poly1305_t3186287769::get_offset_of_k3_15(),
	Poly1305_t3186287769::get_offset_of_currentBlock_16(),
	Poly1305_t3186287769::get_offset_of_currentBlockOffset_17(),
	Poly1305_t3186287769::get_offset_of_h0_18(),
	Poly1305_t3186287769::get_offset_of_h1_19(),
	Poly1305_t3186287769::get_offset_of_h2_20(),
	Poly1305_t3186287769::get_offset_of_h3_21(),
	Poly1305_t3186287769::get_offset_of_h4_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (SipHash_t1664512494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[11] = 
{
	SipHash_t1664512494::get_offset_of_c_0(),
	SipHash_t1664512494::get_offset_of_d_1(),
	SipHash_t1664512494::get_offset_of_k0_2(),
	SipHash_t1664512494::get_offset_of_k1_3(),
	SipHash_t1664512494::get_offset_of_v0_4(),
	SipHash_t1664512494::get_offset_of_v1_5(),
	SipHash_t1664512494::get_offset_of_v2_6(),
	SipHash_t1664512494::get_offset_of_v3_7(),
	SipHash_t1664512494::get_offset_of_m_8(),
	SipHash_t1664512494::get_offset_of_wordPos_9(),
	SipHash_t1664512494::get_offset_of_wordCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (VmpcMac_t976793133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[11] = 
{
	VmpcMac_t976793133::get_offset_of_g_0(),
	VmpcMac_t976793133::get_offset_of_n_1(),
	VmpcMac_t976793133::get_offset_of_P_2(),
	VmpcMac_t976793133::get_offset_of_s_3(),
	VmpcMac_t976793133::get_offset_of_T_4(),
	VmpcMac_t976793133::get_offset_of_workingIV_5(),
	VmpcMac_t976793133::get_offset_of_workingKey_6(),
	VmpcMac_t976793133::get_offset_of_x1_7(),
	VmpcMac_t976793133::get_offset_of_x2_8(),
	VmpcMac_t976793133::get_offset_of_x3_9(),
	VmpcMac_t976793133::get_offset_of_x4_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (CbcBlockCipher_t424533760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[6] = 
{
	CbcBlockCipher_t424533760::get_offset_of_IV_0(),
	CbcBlockCipher_t424533760::get_offset_of_cbcV_1(),
	CbcBlockCipher_t424533760::get_offset_of_cbcNextV_2(),
	CbcBlockCipher_t424533760::get_offset_of_blockSize_3(),
	CbcBlockCipher_t424533760::get_offset_of_cipher_4(),
	CbcBlockCipher_t424533760::get_offset_of_encrypting_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (CcmBlockCipher_t2534265899), -1, sizeof(CcmBlockCipher_t2534265899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2917[10] = 
{
	CcmBlockCipher_t2534265899_StaticFields::get_offset_of_BlockSize_0(),
	CcmBlockCipher_t2534265899::get_offset_of_cipher_1(),
	CcmBlockCipher_t2534265899::get_offset_of_macBlock_2(),
	CcmBlockCipher_t2534265899::get_offset_of_forEncryption_3(),
	CcmBlockCipher_t2534265899::get_offset_of_nonce_4(),
	CcmBlockCipher_t2534265899::get_offset_of_initialAssociatedText_5(),
	CcmBlockCipher_t2534265899::get_offset_of_macSize_6(),
	CcmBlockCipher_t2534265899::get_offset_of_keyParam_7(),
	CcmBlockCipher_t2534265899::get_offset_of_associatedText_8(),
	CcmBlockCipher_t2534265899::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (CfbBlockCipher_t1093564787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[6] = 
{
	CfbBlockCipher_t1093564787::get_offset_of_IV_0(),
	CfbBlockCipher_t1093564787::get_offset_of_cfbV_1(),
	CfbBlockCipher_t1093564787::get_offset_of_cfbOutV_2(),
	CfbBlockCipher_t1093564787::get_offset_of_encrypting_3(),
	CfbBlockCipher_t1093564787::get_offset_of_blockSize_4(),
	CfbBlockCipher_t1093564787::get_offset_of_cipher_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (CtsBlockCipher_t3347056426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[1] = 
{
	CtsBlockCipher_t3347056426::get_offset_of_blockSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (EaxBlockCipher_t1200038734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[12] = 
{
	EaxBlockCipher_t1200038734::get_offset_of_cipher_0(),
	EaxBlockCipher_t1200038734::get_offset_of_forEncryption_1(),
	EaxBlockCipher_t1200038734::get_offset_of_blockSize_2(),
	EaxBlockCipher_t1200038734::get_offset_of_mac_3(),
	EaxBlockCipher_t1200038734::get_offset_of_nonceMac_4(),
	EaxBlockCipher_t1200038734::get_offset_of_associatedTextMac_5(),
	EaxBlockCipher_t1200038734::get_offset_of_macBlock_6(),
	EaxBlockCipher_t1200038734::get_offset_of_macSize_7(),
	EaxBlockCipher_t1200038734::get_offset_of_bufBlock_8(),
	EaxBlockCipher_t1200038734::get_offset_of_bufOff_9(),
	EaxBlockCipher_t1200038734::get_offset_of_cipherInitialized_10(),
	EaxBlockCipher_t1200038734::get_offset_of_initialAssociatedText_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (Tag_t3159821658)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2921[4] = 
{
	Tag_t3159821658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (GcmBlockCipher_t1966957103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[23] = 
{
	0,
	GcmBlockCipher_t1966957103::get_offset_of_cipher_1(),
	GcmBlockCipher_t1966957103::get_offset_of_multiplier_2(),
	GcmBlockCipher_t1966957103::get_offset_of_exp_3(),
	GcmBlockCipher_t1966957103::get_offset_of_forEncryption_4(),
	GcmBlockCipher_t1966957103::get_offset_of_macSize_5(),
	GcmBlockCipher_t1966957103::get_offset_of_nonce_6(),
	GcmBlockCipher_t1966957103::get_offset_of_initialAssociatedText_7(),
	GcmBlockCipher_t1966957103::get_offset_of_H_8(),
	GcmBlockCipher_t1966957103::get_offset_of_J0_9(),
	GcmBlockCipher_t1966957103::get_offset_of_bufBlock_10(),
	GcmBlockCipher_t1966957103::get_offset_of_macBlock_11(),
	GcmBlockCipher_t1966957103::get_offset_of_S_12(),
	GcmBlockCipher_t1966957103::get_offset_of_S_at_13(),
	GcmBlockCipher_t1966957103::get_offset_of_S_atPre_14(),
	GcmBlockCipher_t1966957103::get_offset_of_counter_15(),
	GcmBlockCipher_t1966957103::get_offset_of_blocksRemaining_16(),
	GcmBlockCipher_t1966957103::get_offset_of_bufOff_17(),
	GcmBlockCipher_t1966957103::get_offset_of_totalLength_18(),
	GcmBlockCipher_t1966957103::get_offset_of_atBlock_19(),
	GcmBlockCipher_t1966957103::get_offset_of_atBlockPos_20(),
	GcmBlockCipher_t1966957103::get_offset_of_atLength_21(),
	GcmBlockCipher_t1966957103::get_offset_of_atLengthPre_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (GOfbBlockCipher_t591890526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[10] = 
{
	GOfbBlockCipher_t591890526::get_offset_of_IV_0(),
	GOfbBlockCipher_t591890526::get_offset_of_ofbV_1(),
	GOfbBlockCipher_t591890526::get_offset_of_ofbOutV_2(),
	GOfbBlockCipher_t591890526::get_offset_of_blockSize_3(),
	GOfbBlockCipher_t591890526::get_offset_of_cipher_4(),
	GOfbBlockCipher_t591890526::get_offset_of_firstStep_5(),
	GOfbBlockCipher_t591890526::get_offset_of_N3_6(),
	GOfbBlockCipher_t591890526::get_offset_of_N4_7(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (OcbBlockCipher_t427870536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[23] = 
{
	0,
	OcbBlockCipher_t427870536::get_offset_of_hashCipher_1(),
	OcbBlockCipher_t427870536::get_offset_of_mainCipher_2(),
	OcbBlockCipher_t427870536::get_offset_of_forEncryption_3(),
	OcbBlockCipher_t427870536::get_offset_of_macSize_4(),
	OcbBlockCipher_t427870536::get_offset_of_initialAssociatedText_5(),
	OcbBlockCipher_t427870536::get_offset_of_L_6(),
	OcbBlockCipher_t427870536::get_offset_of_L_Asterisk_7(),
	OcbBlockCipher_t427870536::get_offset_of_L_Dollar_8(),
	OcbBlockCipher_t427870536::get_offset_of_KtopInput_9(),
	OcbBlockCipher_t427870536::get_offset_of_Stretch_10(),
	OcbBlockCipher_t427870536::get_offset_of_OffsetMAIN_0_11(),
	OcbBlockCipher_t427870536::get_offset_of_hashBlock_12(),
	OcbBlockCipher_t427870536::get_offset_of_mainBlock_13(),
	OcbBlockCipher_t427870536::get_offset_of_hashBlockPos_14(),
	OcbBlockCipher_t427870536::get_offset_of_mainBlockPos_15(),
	OcbBlockCipher_t427870536::get_offset_of_hashBlockCount_16(),
	OcbBlockCipher_t427870536::get_offset_of_mainBlockCount_17(),
	OcbBlockCipher_t427870536::get_offset_of_OffsetHASH_18(),
	OcbBlockCipher_t427870536::get_offset_of_Sum_19(),
	OcbBlockCipher_t427870536::get_offset_of_OffsetMAIN_20(),
	OcbBlockCipher_t427870536::get_offset_of_Checksum_21(),
	OcbBlockCipher_t427870536::get_offset_of_macBlock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (OfbBlockCipher_t2804280439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[5] = 
{
	OfbBlockCipher_t2804280439::get_offset_of_IV_0(),
	OfbBlockCipher_t2804280439::get_offset_of_ofbV_1(),
	OfbBlockCipher_t2804280439::get_offset_of_ofbOutV_2(),
	OfbBlockCipher_t2804280439::get_offset_of_blockSize_3(),
	OfbBlockCipher_t2804280439::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (OpenPgpCfbBlockCipher_t1145356922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[7] = 
{
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_IV_0(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_FR_1(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_FRE_2(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_cipher_3(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_blockSize_4(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_count_5(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_forEncryption_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (SicBlockCipher_t3693949395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[5] = 
{
	SicBlockCipher_t3693949395::get_offset_of_cipher_0(),
	SicBlockCipher_t3693949395::get_offset_of_blockSize_1(),
	SicBlockCipher_t3693949395::get_offset_of_counter_2(),
	SicBlockCipher_t3693949395::get_offset_of_counterOut_3(),
	SicBlockCipher_t3693949395::get_offset_of_IV_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (GcmUtilities_t2204846149), -1, sizeof(GcmUtilities_t2204846149_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2929[3] = 
{
	0,
	0,
	GcmUtilities_t2204846149_StaticFields::get_offset_of_LOOKUP_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (Tables1kGcmExponentiator_t1216265862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[1] = 
{
	Tables1kGcmExponentiator_t1216265862::get_offset_of_lookupPowX2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (Tables8kGcmMultiplier_t1725665332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[2] = 
{
	Tables8kGcmMultiplier_t1725665332::get_offset_of_H_0(),
	Tables8kGcmMultiplier_t1725665332::get_offset_of_M_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (X509Utilities_t1210696522), -1, sizeof(X509Utilities_t1210696522_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2934[4] = 
{
	X509Utilities_t1210696522_StaticFields::get_offset_of_derNull_0(),
	X509Utilities_t1210696522_StaticFields::get_offset_of_algorithms_1(),
	X509Utilities_t1210696522_StaticFields::get_offset_of_exParams_2(),
	X509Utilities_t1210696522_StaticFields::get_offset_of_noParams_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (SignerBucket_t3562239414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[1] = 
{
	SignerBucket_t3562239414::get_offset_of_signer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (Asn1SignatureFactory_t2866294513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[4] = 
{
	Asn1SignatureFactory_t2866294513::get_offset_of_algID_0(),
	Asn1SignatureFactory_t2866294513::get_offset_of_algorithm_1(),
	Asn1SignatureFactory_t2866294513::get_offset_of_privateKey_2(),
	Asn1SignatureFactory_t2866294513::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (SigCalculator_t3817728361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[2] = 
{
	SigCalculator_t3817728361::get_offset_of_sig_0(),
	SigCalculator_t3817728361::get_offset_of_stream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (SigResult_t250825146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[1] = 
{
	SigResult_t250825146::get_offset_of_sig_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (Asn1VerifierFactory_t549551221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[2] = 
{
	Asn1VerifierFactory_t549551221::get_offset_of_algID_0(),
	Asn1VerifierFactory_t549551221::get_offset_of_publicKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (VerifierCalculator_t3555759188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[2] = 
{
	VerifierCalculator_t3555759188::get_offset_of_sig_0(),
	VerifierCalculator_t3555759188::get_offset_of_stream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (VerifierResult_t3509947265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[1] = 
{
	VerifierResult_t3509947265::get_offset_of_sig_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (Asn1VerifierFactoryProvider_t3967385528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[1] = 
{
	Asn1VerifierFactoryProvider_t3967385528::get_offset_of_publicKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (ISO10126d2Padding_t3010076608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[1] = 
{
	ISO10126d2Padding_t3010076608::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (ISO7816d4Padding_t609688802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (PaddedBufferedBlockCipher_t4046604239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[1] = 
{
	PaddedBufferedBlockCipher_t4046604239::get_offset_of_padding_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (Pkcs7Padding_t2353579457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (TbcPadding_t3233791910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (X923Padding_t2660499907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[1] = 
{
	X923Padding_t2660499907::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (ZeroBytePadding_t1951118083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (AeadParameters_t2995549197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[4] = 
{
	AeadParameters_t2995549197::get_offset_of_associatedText_0(),
	AeadParameters_t2995549197::get_offset_of_nonce_1(),
	AeadParameters_t2995549197::get_offset_of_key_2(),
	AeadParameters_t2995549197::get_offset_of_macSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (DHKeyGenerationParameters_t1952252333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[1] = 
{
	DHKeyGenerationParameters_t1952252333::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (DHKeyParameters_t508500971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[2] = 
{
	DHKeyParameters_t508500971::get_offset_of_parameters_1(),
	DHKeyParameters_t508500971::get_offset_of_algorithmOid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (DHParameters_t431035336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[8] = 
{
	0,
	DHParameters_t431035336::get_offset_of_p_1(),
	DHParameters_t431035336::get_offset_of_g_2(),
	DHParameters_t431035336::get_offset_of_q_3(),
	DHParameters_t431035336::get_offset_of_j_4(),
	DHParameters_t431035336::get_offset_of_m_5(),
	DHParameters_t431035336::get_offset_of_l_6(),
	DHParameters_t431035336::get_offset_of_validation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (DHPrivateKeyParameters_t3120746414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[1] = 
{
	DHPrivateKeyParameters_t3120746414::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (DHPublicKeyParameters_t1544976430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[1] = 
{
	DHPublicKeyParameters_t1544976430::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (DHValidationParameters_t2841123689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[2] = 
{
	DHValidationParameters_t2841123689::get_offset_of_seed_0(),
	DHValidationParameters_t2841123689::get_offset_of_counter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (DesEdeParameters_t597696274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (DesParameters_t4026919406), -1, sizeof(DesParameters_t4026919406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2959[3] = 
{
	0,
	0,
	DesParameters_t4026919406_StaticFields::get_offset_of_DES_weak_keys_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (DsaKeyGenerationParameters_t4209685231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[1] = 
{
	DsaKeyGenerationParameters_t4209685231::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (DsaKeyParameters_t2298980877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[1] = 
{
	DsaKeyParameters_t2298980877::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (DsaParameters_t2550649858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[4] = 
{
	DsaParameters_t2550649858::get_offset_of_p_0(),
	DsaParameters_t2550649858::get_offset_of_q_1(),
	DsaParameters_t2550649858::get_offset_of_g_2(),
	DsaParameters_t2550649858::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (DsaPrivateKeyParameters_t1082923572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[1] = 
{
	DsaPrivateKeyParameters_t1082923572::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (DsaPublicKeyParameters_t3405498240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[1] = 
{
	DsaPublicKeyParameters_t3405498240::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (DsaValidationParameters_t2732896323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[3] = 
{
	DsaValidationParameters_t2732896323::get_offset_of_seed_0(),
	DsaValidationParameters_t2732896323::get_offset_of_counter_1(),
	DsaValidationParameters_t2732896323::get_offset_of_usageIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (ECDomainParameters_t3939864474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[5] = 
{
	ECDomainParameters_t3939864474::get_offset_of_curve_0(),
	ECDomainParameters_t3939864474::get_offset_of_seed_1(),
	ECDomainParameters_t3939864474::get_offset_of_g_2(),
	ECDomainParameters_t3939864474::get_offset_of_n_3(),
	ECDomainParameters_t3939864474::get_offset_of_h_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (ECKeyGenerationParameters_t3475787761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[2] = 
{
	ECKeyGenerationParameters_t3475787761::get_offset_of_domainParams_2(),
	ECKeyGenerationParameters_t3475787761::get_offset_of_publicKeyParamSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (ECKeyParameters_t1064568751), -1, sizeof(ECKeyParameters_t1064568751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2968[4] = 
{
	ECKeyParameters_t1064568751_StaticFields::get_offset_of_algorithms_1(),
	ECKeyParameters_t1064568751::get_offset_of_algorithm_2(),
	ECKeyParameters_t1064568751::get_offset_of_parameters_3(),
	ECKeyParameters_t1064568751::get_offset_of_publicKeyParamSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (ECPrivateKeyParameters_t3632960452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[1] = 
{
	ECPrivateKeyParameters_t3632960452::get_offset_of_d_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (ECPublicKeyParameters_t572706344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[1] = 
{
	ECPublicKeyParameters_t572706344::get_offset_of_q_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (ElGamalKeyGenerationParameters_t303785198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[1] = 
{
	ElGamalKeyGenerationParameters_t303785198::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (ElGamalKeyParameters_t352675504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2972[1] = 
{
	ElGamalKeyParameters_t352675504::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (ElGamalParameters_t1215309569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[3] = 
{
	ElGamalParameters_t1215309569::get_offset_of_p_0(),
	ElGamalParameters_t1215309569::get_offset_of_g_1(),
	ElGamalParameters_t1215309569::get_offset_of_l_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (ElGamalPrivateKeyParameters_t2762984431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[1] = 
{
	ElGamalPrivateKeyParameters_t2762984431::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (ElGamalPublicKeyParameters_t3740889069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[1] = 
{
	ElGamalPublicKeyParameters_t3740889069::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (Gost3410KeyParameters_t1164823134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[2] = 
{
	Gost3410KeyParameters_t1164823134::get_offset_of_parameters_1(),
	Gost3410KeyParameters_t1164823134::get_offset_of_publicKeyParamSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (Gost3410Parameters_t602285121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[4] = 
{
	Gost3410Parameters_t602285121::get_offset_of_p_0(),
	Gost3410Parameters_t602285121::get_offset_of_q_1(),
	Gost3410Parameters_t602285121::get_offset_of_a_2(),
	Gost3410Parameters_t602285121::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (Gost3410PrivateKeyParameters_t2071462699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[1] = 
{
	Gost3410PrivateKeyParameters_t2071462699::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (Gost3410PublicKeyParameters_t521829201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[1] = 
{
	Gost3410PublicKeyParameters_t521829201::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (Gost3410ValidationParameters_t2048269886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[4] = 
{
	Gost3410ValidationParameters_t2048269886::get_offset_of_x0_0(),
	Gost3410ValidationParameters_t2048269886::get_offset_of_c_1(),
	Gost3410ValidationParameters_t2048269886::get_offset_of_x0L_2(),
	Gost3410ValidationParameters_t2048269886::get_offset_of_cL_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (Iso18033KdfParameters_t964742789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2981[1] = 
{
	Iso18033KdfParameters_t964742789::get_offset_of_seed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (IesParameters_t4148998567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2982[3] = 
{
	IesParameters_t4148998567::get_offset_of_derivation_0(),
	IesParameters_t4148998567::get_offset_of_encoding_1(),
	IesParameters_t4148998567::get_offset_of_macKeySize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (IesWithCipherParameters_t206966936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[1] = 
{
	IesWithCipherParameters_t206966936::get_offset_of_cipherKeySize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (KdfParameters_t1337840219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[2] = 
{
	KdfParameters_t1337840219::get_offset_of_iv_0(),
	KdfParameters_t1337840219::get_offset_of_shared_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (KeyParameter_t215653120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[1] = 
{
	KeyParameter_t215653120::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (MqvPrivateParameters_t2816354209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[3] = 
{
	MqvPrivateParameters_t2816354209::get_offset_of_staticPrivateKey_0(),
	MqvPrivateParameters_t2816354209::get_offset_of_ephemeralPrivateKey_1(),
	MqvPrivateParameters_t2816354209::get_offset_of_ephemeralPublicKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (MqvPublicParameters_t4146726485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2987[2] = 
{
	MqvPublicParameters_t4146726485::get_offset_of_staticPublicKey_0(),
	MqvPublicParameters_t4146726485::get_offset_of_ephemeralPublicKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (ParametersWithIV_t2760900877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[2] = 
{
	ParametersWithIV_t2760900877::get_offset_of_parameters_0(),
	ParametersWithIV_t2760900877::get_offset_of_iv_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (ParametersWithRandom_t16149445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[2] = 
{
	ParametersWithRandom_t16149445::get_offset_of_parameters_0(),
	ParametersWithRandom_t16149445::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (ParametersWithSBox_t30556752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[2] = 
{
	ParametersWithSBox_t30556752::get_offset_of_parameters_0(),
	ParametersWithSBox_t30556752::get_offset_of_sBox_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (ParametersWithSalt_t433842492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[2] = 
{
	ParametersWithSalt_t433842492::get_offset_of_salt_0(),
	ParametersWithSalt_t433842492::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (RC2Parameters_t852146563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[1] = 
{
	RC2Parameters_t852146563::get_offset_of_bits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (RC5Parameters_t139417372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[1] = 
{
	RC5Parameters_t139417372::get_offset_of_rounds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (RsaBlindingParameters_t2682613251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[2] = 
{
	RsaBlindingParameters_t2682613251::get_offset_of_publicKey_0(),
	RsaBlindingParameters_t2682613251::get_offset_of_blindingFactor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (RsaKeyGenerationParameters_t2592312329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[2] = 
{
	RsaKeyGenerationParameters_t2592312329::get_offset_of_publicExponent_2(),
	RsaKeyGenerationParameters_t2592312329::get_offset_of_certainty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (RsaKeyParameters_t3425534311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[2] = 
{
	RsaKeyParameters_t3425534311::get_offset_of_modulus_1(),
	RsaKeyParameters_t3425534311::get_offset_of_exponent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (RsaPrivateCrtKeyParameters_t3532059937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[6] = 
{
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_e_3(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_p_4(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_q_5(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_dP_6(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_dQ_7(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_qInv_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (CryptoApiRandomGenerator_t405007133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[1] = 
{
	CryptoApiRandomGenerator_t405007133::get_offset_of_rndProv_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (DigestRandomGenerator_t1445396782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[6] = 
{
	0,
	DigestRandomGenerator_t1445396782::get_offset_of_stateCounter_1(),
	DigestRandomGenerator_t1445396782::get_offset_of_seedCounter_2(),
	DigestRandomGenerator_t1445396782::get_offset_of_digest_3(),
	DigestRandomGenerator_t1445396782::get_offset_of_state_4(),
	DigestRandomGenerator_t1445396782::get_offset_of_seed_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
