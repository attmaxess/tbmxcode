﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_4145682572.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1646699940.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1823299568.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_EC27564669.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_2702239436.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_3543322195.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1378799819.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_G601621931.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_4170685704.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1034645512.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_2065783217.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1670054114.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_R600049064.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_3187127386.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_X244497657.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst2715111877.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3831412532.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst4187022390.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst1662285836.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3594632315.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3246029418.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3351798479.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst1629639237.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3501337442.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abstr463031607.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3577453307.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Aler1943690406.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Aler2418630078.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Alway671795399.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Byte1600245655.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Byte1673337995.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert3711240521.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert2775016569.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert4188827490.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert1829945713.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert2118695048.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert1123924209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Chach588232263.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Chan1387977466.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Ciph2647370547.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Ciph3625443269.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Clie4001196324.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Comb1177273743.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Comp3132420437.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Conn3056634225.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cont1560631153.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Defau744769081.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Defa1499842933.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Defe1571847761.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Dige3911248312.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Dige2878944447.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Digi2312486481.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_ECBa3945911148.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_ECCu2024367045.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_ECPo3531342969.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Encr1304858704.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Expor839268261.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Exte2022447001.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_FiniteFie85465.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hand4067128167.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hash2960667477.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Heart950151469.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hear4074124803.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hear3199822323.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_KeyE1390198871.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Lega3599698926.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Lega1589283767.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_MacA2921466062.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_MaxFr108152128.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_NameT221825315.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Named630294042.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_NewS3489773180.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Ocsp1991562814.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_PrfA4218585047.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Prot3273908466.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Recor911577901.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Secu3985528004.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Serv3525792961.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Serv2635557658.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Serve405134486.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sessi833892266.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sess1698926946.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign2898832071.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign3350051566.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign3777817038.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign2455255744.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Ssl31535524432.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Supp1084707380.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsA2267292935.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsBl317591639.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (DsaDigestSigner_t4145682572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[3] = 
{
	DsaDigestSigner_t4145682572::get_offset_of_digest_0(),
	DsaDigestSigner_t4145682572::get_offset_of_dsaSigner_1(),
	DsaDigestSigner_t4145682572::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (DsaSigner_t1646699940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[3] = 
{
	DsaSigner_t1646699940::get_offset_of_kCalculator_0(),
	DsaSigner_t1646699940::get_offset_of_key_1(),
	DsaSigner_t1646699940::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (ECDsaSigner_t1823299568), -1, sizeof(ECDsaSigner_t1823299568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3003[4] = 
{
	ECDsaSigner_t1823299568_StaticFields::get_offset_of_Eight_0(),
	ECDsaSigner_t1823299568::get_offset_of_kCalculator_1(),
	ECDsaSigner_t1823299568::get_offset_of_key_2(),
	ECDsaSigner_t1823299568::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (ECGost3410Signer_t27564669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[2] = 
{
	ECGost3410Signer_t27564669::get_offset_of_key_0(),
	ECGost3410Signer_t27564669::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (ECNRSigner_t2702239436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[3] = 
{
	ECNRSigner_t2702239436::get_offset_of_forSigning_0(),
	ECNRSigner_t2702239436::get_offset_of_key_1(),
	ECNRSigner_t2702239436::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (Gost3410DigestSigner_t3543322195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[3] = 
{
	Gost3410DigestSigner_t3543322195::get_offset_of_digest_0(),
	Gost3410DigestSigner_t3543322195::get_offset_of_dsaSigner_1(),
	Gost3410DigestSigner_t3543322195::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (Gost3410Signer_t1378799819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[2] = 
{
	Gost3410Signer_t1378799819::get_offset_of_key_0(),
	Gost3410Signer_t1378799819::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (GenericSigner_t601621931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[3] = 
{
	GenericSigner_t601621931::get_offset_of_engine_0(),
	GenericSigner_t601621931::get_offset_of_digest_1(),
	GenericSigner_t601621931::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (HMacDsaKCalculator_t4170685704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[4] = 
{
	HMacDsaKCalculator_t4170685704::get_offset_of_hMac_0(),
	HMacDsaKCalculator_t4170685704::get_offset_of_K_1(),
	HMacDsaKCalculator_t4170685704::get_offset_of_V_2(),
	HMacDsaKCalculator_t4170685704::get_offset_of_n_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (Iso9796d2Signer_t1034645512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Iso9796d2Signer_t1034645512::get_offset_of_digest_8(),
	Iso9796d2Signer_t1034645512::get_offset_of_cipher_9(),
	Iso9796d2Signer_t1034645512::get_offset_of_trailer_10(),
	Iso9796d2Signer_t1034645512::get_offset_of_keyBits_11(),
	Iso9796d2Signer_t1034645512::get_offset_of_block_12(),
	Iso9796d2Signer_t1034645512::get_offset_of_mBuf_13(),
	Iso9796d2Signer_t1034645512::get_offset_of_messageLength_14(),
	Iso9796d2Signer_t1034645512::get_offset_of_fullMessage_15(),
	Iso9796d2Signer_t1034645512::get_offset_of_recoveredMessage_16(),
	Iso9796d2Signer_t1034645512::get_offset_of_preSig_17(),
	Iso9796d2Signer_t1034645512::get_offset_of_preBlock_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (IsoTrailers_t2065783217), -1, sizeof(IsoTrailers_t2065783217_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3012[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	IsoTrailers_t2065783217_StaticFields::get_offset_of_trailerMap_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (PssSigner_t1670054114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[15] = 
{
	0,
	PssSigner_t1670054114::get_offset_of_contentDigest1_1(),
	PssSigner_t1670054114::get_offset_of_contentDigest2_2(),
	PssSigner_t1670054114::get_offset_of_mgfDigest_3(),
	PssSigner_t1670054114::get_offset_of_cipher_4(),
	PssSigner_t1670054114::get_offset_of_random_5(),
	PssSigner_t1670054114::get_offset_of_hLen_6(),
	PssSigner_t1670054114::get_offset_of_mgfhLen_7(),
	PssSigner_t1670054114::get_offset_of_sLen_8(),
	PssSigner_t1670054114::get_offset_of_sSet_9(),
	PssSigner_t1670054114::get_offset_of_emBits_10(),
	PssSigner_t1670054114::get_offset_of_salt_11(),
	PssSigner_t1670054114::get_offset_of_mDash_12(),
	PssSigner_t1670054114::get_offset_of_block_13(),
	PssSigner_t1670054114::get_offset_of_trailer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (RandomDsaKCalculator_t600049064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[2] = 
{
	RandomDsaKCalculator_t600049064::get_offset_of_q_0(),
	RandomDsaKCalculator_t600049064::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (RsaDigestSigner_t3187127386), -1, sizeof(RsaDigestSigner_t3187127386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3015[5] = 
{
	RsaDigestSigner_t3187127386::get_offset_of_rsaEngine_0(),
	RsaDigestSigner_t3187127386::get_offset_of_algId_1(),
	RsaDigestSigner_t3187127386::get_offset_of_digest_2(),
	RsaDigestSigner_t3187127386::get_offset_of_forSigning_3(),
	RsaDigestSigner_t3187127386_StaticFields::get_offset_of_oidMap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (X931Signer_t244497657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	X931Signer_t244497657::get_offset_of_digest_9(),
	X931Signer_t244497657::get_offset_of_cipher_10(),
	X931Signer_t244497657::get_offset_of_kParam_11(),
	X931Signer_t244497657::get_offset_of_trailer_12(),
	X931Signer_t244497657::get_offset_of_keyBits_13(),
	X931Signer_t244497657::get_offset_of_block_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (AbstractTlsAgreementCredentials_t2715111877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (AbstractTlsCipherFactory_t3831412532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (AbstractTlsClient_t4187022390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[9] = 
{
	AbstractTlsClient_t4187022390::get_offset_of_mCipherFactory_0(),
	AbstractTlsClient_t4187022390::get_offset_of_mContext_1(),
	AbstractTlsClient_t4187022390::get_offset_of_mSupportedSignatureAlgorithms_2(),
	AbstractTlsClient_t4187022390::get_offset_of_mNamedCurves_3(),
	AbstractTlsClient_t4187022390::get_offset_of_mClientECPointFormats_4(),
	AbstractTlsClient_t4187022390::get_offset_of_mServerECPointFormats_5(),
	AbstractTlsClient_t4187022390::get_offset_of_mSelectedCipherSuite_6(),
	AbstractTlsClient_t4187022390::get_offset_of_mSelectedCompressionMethod_7(),
	AbstractTlsClient_t4187022390::get_offset_of_U3CHostNamesU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (AbstractTlsContext_t1662285836), -1, sizeof(AbstractTlsContext_t1662285836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3020[8] = 
{
	AbstractTlsContext_t1662285836_StaticFields::get_offset_of_counter_0(),
	AbstractTlsContext_t1662285836::get_offset_of_mNonceRandom_1(),
	AbstractTlsContext_t1662285836::get_offset_of_mSecureRandom_2(),
	AbstractTlsContext_t1662285836::get_offset_of_mSecurityParameters_3(),
	AbstractTlsContext_t1662285836::get_offset_of_mClientVersion_4(),
	AbstractTlsContext_t1662285836::get_offset_of_mServerVersion_5(),
	AbstractTlsContext_t1662285836::get_offset_of_mSession_6(),
	AbstractTlsContext_t1662285836::get_offset_of_mUserObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (AbstractTlsCredentials_t3594632315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (AbstractTlsEncryptionCredentials_t3246029418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (AbstractTlsKeyExchange_t3351798479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[3] = 
{
	AbstractTlsKeyExchange_t3351798479::get_offset_of_mKeyExchange_0(),
	AbstractTlsKeyExchange_t3351798479::get_offset_of_mSupportedSignatureAlgorithms_1(),
	AbstractTlsKeyExchange_t3351798479::get_offset_of_mContext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (AbstractTlsPeer_t1629639237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (AbstractTlsServer_t3501337442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[18] = 
{
	AbstractTlsServer_t3501337442::get_offset_of_mCipherFactory_0(),
	AbstractTlsServer_t3501337442::get_offset_of_mContext_1(),
	AbstractTlsServer_t3501337442::get_offset_of_mClientVersion_2(),
	AbstractTlsServer_t3501337442::get_offset_of_mOfferedCipherSuites_3(),
	AbstractTlsServer_t3501337442::get_offset_of_mOfferedCompressionMethods_4(),
	AbstractTlsServer_t3501337442::get_offset_of_mClientExtensions_5(),
	AbstractTlsServer_t3501337442::get_offset_of_mEncryptThenMacOffered_6(),
	AbstractTlsServer_t3501337442::get_offset_of_mMaxFragmentLengthOffered_7(),
	AbstractTlsServer_t3501337442::get_offset_of_mTruncatedHMacOffered_8(),
	AbstractTlsServer_t3501337442::get_offset_of_mSupportedSignatureAlgorithms_9(),
	AbstractTlsServer_t3501337442::get_offset_of_mEccCipherSuitesOffered_10(),
	AbstractTlsServer_t3501337442::get_offset_of_mNamedCurves_11(),
	AbstractTlsServer_t3501337442::get_offset_of_mClientECPointFormats_12(),
	AbstractTlsServer_t3501337442::get_offset_of_mServerECPointFormats_13(),
	AbstractTlsServer_t3501337442::get_offset_of_mServerVersion_14(),
	AbstractTlsServer_t3501337442::get_offset_of_mSelectedCipherSuite_15(),
	AbstractTlsServer_t3501337442::get_offset_of_mSelectedCompressionMethod_16(),
	AbstractTlsServer_t3501337442::get_offset_of_mServerExtensions_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (AbstractTlsSigner_t463031607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[1] = 
{
	AbstractTlsSigner_t463031607::get_offset_of_mContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (AbstractTlsSignerCredentials_t3577453307), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (AlertDescription_t1943690406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (AlertLevel_t2418630078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (AlwaysValidVerifyer_t671795399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (ByteQueue_t1600245655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[4] = 
{
	0,
	ByteQueue_t1600245655::get_offset_of_databuf_1(),
	ByteQueue_t1600245655::get_offset_of_skipped_2(),
	ByteQueue_t1600245655::get_offset_of_available_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (ByteQueueStream_t1673337995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[1] = 
{
	ByteQueueStream_t1673337995::get_offset_of_buffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (CertChainType_t3711240521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (Certificate_t2775016569), -1, sizeof(Certificate_t2775016569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3034[2] = 
{
	Certificate_t2775016569_StaticFields::get_offset_of_EmptyChain_0(),
	Certificate_t2775016569::get_offset_of_mCertificateList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (CertificateRequest_t4188827490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[3] = 
{
	CertificateRequest_t4188827490::get_offset_of_mCertificateTypes_0(),
	CertificateRequest_t4188827490::get_offset_of_mSupportedSignatureAlgorithms_1(),
	CertificateRequest_t4188827490::get_offset_of_mCertificateAuthorities_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (CertificateStatus_t1829945713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[2] = 
{
	CertificateStatus_t1829945713::get_offset_of_mStatusType_0(),
	CertificateStatus_t1829945713::get_offset_of_mResponse_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (CertificateStatusRequest_t2118695048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[2] = 
{
	CertificateStatusRequest_t2118695048::get_offset_of_mStatusType_0(),
	CertificateStatusRequest_t2118695048::get_offset_of_mRequest_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (CertificateStatusType_t1123924209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (Chacha20Poly1305_t588232263), -1, sizeof(Chacha20Poly1305_t588232263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3039[6] = 
{
	Chacha20Poly1305_t588232263_StaticFields::get_offset_of_Zeroes_0(),
	Chacha20Poly1305_t588232263::get_offset_of_context_1(),
	Chacha20Poly1305_t588232263::get_offset_of_encryptCipher_2(),
	Chacha20Poly1305_t588232263::get_offset_of_decryptCipher_3(),
	Chacha20Poly1305_t588232263::get_offset_of_encryptIV_4(),
	Chacha20Poly1305_t588232263::get_offset_of_decryptIV_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (ChangeCipherSpec_t1387977466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (CipherSuite_t2647370547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[270] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (CipherType_t3625443269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (ClientCertificateType_t4001196324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (CombinedHash_t1177273743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[3] = 
{
	CombinedHash_t1177273743::get_offset_of_mContext_0(),
	CombinedHash_t1177273743::get_offset_of_mMd5_1(),
	CombinedHash_t1177273743::get_offset_of_mSha1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (CompressionMethod_t3132420437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (ConnectionEnd_t3056634225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (ContentType_t1560631153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (DefaultTlsCipherFactory_t744769081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (DefaultTlsClient_t1499842933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (DeferredHash_t1571847761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[5] = 
{
	0,
	DeferredHash_t1571847761::get_offset_of_mContext_1(),
	DeferredHash_t1571847761::get_offset_of_mBuf_2(),
	DeferredHash_t1571847761::get_offset_of_mHashes_3(),
	DeferredHash_t1571847761::get_offset_of_mPrfHashAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (DigestInputBuffer_t3911248312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (DigStream_t2878944447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[1] = 
{
	DigStream_t2878944447::get_offset_of_d_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (DigitallySigned_t2312486481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3054[2] = 
{
	DigitallySigned_t2312486481::get_offset_of_mAlgorithm_0(),
	DigitallySigned_t2312486481::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (ECBasisType_t3945911148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (ECCurveType_t2024367045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (ECPointFormat_t3531342969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (EncryptionAlgorithm_t1304858704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3058[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (ExporterLabel_t839268261), -1, sizeof(ExporterLabel_t839268261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3059[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ExporterLabel_t839268261_StaticFields::get_offset_of_extended_master_secret_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (ExtensionType_t2022447001), -1, sizeof(ExtensionType_t2022447001_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3060[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ExtensionType_t2022447001_StaticFields::get_offset_of_negotiated_ff_dhe_groups_26(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (FiniteFieldDheGroup_t85465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (HandshakeType_t4067128167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (HashAlgorithm_t2960667477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (HeartbeatExtension_t950151469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[1] = 
{
	HeartbeatExtension_t950151469::get_offset_of_mMode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (HeartbeatMessageType_t4074124803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (HeartbeatMode_t3199822323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (KeyExchangeAlgorithm_t1390198871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3068[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (LegacyTlsAuthentication_t3599698926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[3] = 
{
	LegacyTlsAuthentication_t3599698926::get_offset_of_verifyer_0(),
	LegacyTlsAuthentication_t3599698926::get_offset_of_credProvider_1(),
	LegacyTlsAuthentication_t3599698926::get_offset_of_TargetUri_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (LegacyTlsClient_t1589283767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[3] = 
{
	LegacyTlsClient_t1589283767::get_offset_of_TargetUri_9(),
	LegacyTlsClient_t1589283767::get_offset_of_verifyer_10(),
	LegacyTlsClient_t1589283767::get_offset_of_credProvider_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (MacAlgorithm_t2921466062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (MaxFragmentLength_t108152128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (NameType_t221825315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (NamedCurve_t630294042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (NewSessionTicket_t3489773180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[2] = 
{
	NewSessionTicket_t3489773180::get_offset_of_mTicketLifetimeHint_0(),
	NewSessionTicket_t3489773180::get_offset_of_mTicket_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (OcspStatusRequest_t1991562814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[2] = 
{
	OcspStatusRequest_t1991562814::get_offset_of_mResponderIDList_0(),
	OcspStatusRequest_t1991562814::get_offset_of_mRequestExtensions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (PrfAlgorithm_t4218585047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (ProtocolVersion_t3273908466), -1, sizeof(ProtocolVersion_t3273908466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3079[8] = 
{
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_SSLv3_0(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_TLSv10_1(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_TLSv11_2(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_TLSv12_3(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_DTLSv10_4(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_DTLSv12_5(),
	ProtocolVersion_t3273908466::get_offset_of_version_6(),
	ProtocolVersion_t3273908466::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (RecordStream_t911577901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[24] = 
{
	0,
	0,
	0,
	0,
	0,
	RecordStream_t911577901::get_offset_of_mHandler_5(),
	RecordStream_t911577901::get_offset_of_mInput_6(),
	RecordStream_t911577901::get_offset_of_mOutput_7(),
	RecordStream_t911577901::get_offset_of_mPendingCompression_8(),
	RecordStream_t911577901::get_offset_of_mReadCompression_9(),
	RecordStream_t911577901::get_offset_of_mWriteCompression_10(),
	RecordStream_t911577901::get_offset_of_mPendingCipher_11(),
	RecordStream_t911577901::get_offset_of_mReadCipher_12(),
	RecordStream_t911577901::get_offset_of_mWriteCipher_13(),
	RecordStream_t911577901::get_offset_of_mReadSeqNo_14(),
	RecordStream_t911577901::get_offset_of_mWriteSeqNo_15(),
	RecordStream_t911577901::get_offset_of_mBuffer_16(),
	RecordStream_t911577901::get_offset_of_mHandshakeHash_17(),
	RecordStream_t911577901::get_offset_of_mReadVersion_18(),
	RecordStream_t911577901::get_offset_of_mWriteVersion_19(),
	RecordStream_t911577901::get_offset_of_mRestrictReadVersion_20(),
	RecordStream_t911577901::get_offset_of_mPlaintextLimit_21(),
	RecordStream_t911577901::get_offset_of_mCompressedLimit_22(),
	RecordStream_t911577901::get_offset_of_mCiphertextLimit_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (SecurityParameters_t3985528004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[15] = 
{
	SecurityParameters_t3985528004::get_offset_of_entity_0(),
	SecurityParameters_t3985528004::get_offset_of_cipherSuite_1(),
	SecurityParameters_t3985528004::get_offset_of_compressionAlgorithm_2(),
	SecurityParameters_t3985528004::get_offset_of_prfAlgorithm_3(),
	SecurityParameters_t3985528004::get_offset_of_verifyDataLength_4(),
	SecurityParameters_t3985528004::get_offset_of_masterSecret_5(),
	SecurityParameters_t3985528004::get_offset_of_clientRandom_6(),
	SecurityParameters_t3985528004::get_offset_of_serverRandom_7(),
	SecurityParameters_t3985528004::get_offset_of_sessionHash_8(),
	SecurityParameters_t3985528004::get_offset_of_pskIdentity_9(),
	SecurityParameters_t3985528004::get_offset_of_srpIdentity_10(),
	SecurityParameters_t3985528004::get_offset_of_maxFragmentLength_11(),
	SecurityParameters_t3985528004::get_offset_of_truncatedHMac_12(),
	SecurityParameters_t3985528004::get_offset_of_encryptThenMac_13(),
	SecurityParameters_t3985528004::get_offset_of_extendedMasterSecret_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (ServerDHParams_t3525792961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[1] = 
{
	ServerDHParams_t3525792961::get_offset_of_mPublicKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (ServerName_t2635557658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[2] = 
{
	ServerName_t2635557658::get_offset_of_mNameType_0(),
	ServerName_t2635557658::get_offset_of_mName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (ServerNameList_t405134486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[1] = 
{
	ServerNameList_t405134486::get_offset_of_mServerNameList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (SessionParameters_t833892266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[7] = 
{
	SessionParameters_t833892266::get_offset_of_mCipherSuite_0(),
	SessionParameters_t833892266::get_offset_of_mCompressionAlgorithm_1(),
	SessionParameters_t833892266::get_offset_of_mMasterSecret_2(),
	SessionParameters_t833892266::get_offset_of_mPeerCertificate_3(),
	SessionParameters_t833892266::get_offset_of_mPskIdentity_4(),
	SessionParameters_t833892266::get_offset_of_mSrpIdentity_5(),
	SessionParameters_t833892266::get_offset_of_mEncodedServerExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (Builder_t1698926946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3086[7] = 
{
	Builder_t1698926946::get_offset_of_mCipherSuite_0(),
	Builder_t1698926946::get_offset_of_mCompressionAlgorithm_1(),
	Builder_t1698926946::get_offset_of_mMasterSecret_2(),
	Builder_t1698926946::get_offset_of_mPeerCertificate_3(),
	Builder_t1698926946::get_offset_of_mPskIdentity_4(),
	Builder_t1698926946::get_offset_of_mSrpIdentity_5(),
	Builder_t1698926946::get_offset_of_mEncodedServerExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (SignatureAlgorithm_t2898832071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (SignatureAndHashAlgorithm_t3350051566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[2] = 
{
	SignatureAndHashAlgorithm_t3350051566::get_offset_of_mHash_0(),
	SignatureAndHashAlgorithm_t3350051566::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (SignerInputBuffer_t3777817038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (SigStream_t2455255744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[1] = 
{
	SigStream_t2455255744::get_offset_of_s_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (Ssl3Mac_t1535524432), -1, sizeof(Ssl3Mac_t1535524432_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3091[7] = 
{
	0,
	0,
	Ssl3Mac_t1535524432_StaticFields::get_offset_of_IPAD_2(),
	Ssl3Mac_t1535524432_StaticFields::get_offset_of_OPAD_3(),
	Ssl3Mac_t1535524432::get_offset_of_digest_4(),
	Ssl3Mac_t1535524432::get_offset_of_padLength_5(),
	Ssl3Mac_t1535524432::get_offset_of_secret_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (SupplementalDataEntry_t1084707380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[2] = 
{
	SupplementalDataEntry_t1084707380::get_offset_of_mDataType_0(),
	SupplementalDataEntry_t1084707380::get_offset_of_mData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (TlsAeadCipher_t2267292935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3093[10] = 
{
	0,
	0,
	TlsAeadCipher_t2267292935::get_offset_of_context_2(),
	TlsAeadCipher_t2267292935::get_offset_of_macSize_3(),
	TlsAeadCipher_t2267292935::get_offset_of_record_iv_length_4(),
	TlsAeadCipher_t2267292935::get_offset_of_encryptCipher_5(),
	TlsAeadCipher_t2267292935::get_offset_of_decryptCipher_6(),
	TlsAeadCipher_t2267292935::get_offset_of_encryptImplicitNonce_7(),
	TlsAeadCipher_t2267292935::get_offset_of_decryptImplicitNonce_8(),
	TlsAeadCipher_t2267292935::get_offset_of_nonceMode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (TlsBlockCipher_t317591639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[8] = 
{
	TlsBlockCipher_t317591639::get_offset_of_context_0(),
	TlsBlockCipher_t317591639::get_offset_of_randomData_1(),
	TlsBlockCipher_t317591639::get_offset_of_useExplicitIV_2(),
	TlsBlockCipher_t317591639::get_offset_of_encryptThenMac_3(),
	TlsBlockCipher_t317591639::get_offset_of_encryptCipher_4(),
	TlsBlockCipher_t317591639::get_offset_of_decryptCipher_5(),
	TlsBlockCipher_t317591639::get_offset_of_mWriteMac_6(),
	TlsBlockCipher_t317591639::get_offset_of_mReadMac_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
