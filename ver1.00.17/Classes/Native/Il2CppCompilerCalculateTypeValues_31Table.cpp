﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsC2876508357.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsCl400461164.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsDH169955883.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsD2113062801.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsDe206447956.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsD4293756753.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsD3136059924.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsD3313030453.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsD2995283323.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsE3594646817.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsE1244283552.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsEC294737663.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsE1394749888.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsE3164696939.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsF2782940439.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsM4072537602.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsNul95059113.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsN2084920392.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsP2348540693.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsP3117171608.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsR1676354575.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsR1265999015.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsRs887753625.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsS4126653537.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsS4078687945.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsSt477156699.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsS2175288028.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_TlsUt441503077.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Utilitie3721483105.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_BigInteger4268922522.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_ECAlgor1431241052.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_ECCurve140895757.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_ECCurve_C72665314.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Abstrac2530650717.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_FpCurve1098608013.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Abstrac4137102866.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_F2mCurv2204106640.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_ECField1092946118.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_FpField2948427972.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_F2mFiel1860364149.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_ECPoint626351532.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_ECPoint3119694375.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Abstrac4047682300.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_FpPoint301080990.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Abstract883694769.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_F2mPoin2324796161.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_LongArra194261203.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_ScaleXP2361229496.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Abc_Sim2857025313.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Abc_Tna1139038785.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Abc_ZTa2571810054.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1415304183.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_D734603359.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2608249339.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4149081277.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S701087298.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2282534695.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3090633281.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4111842189.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3346222387.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2602261220.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3458717606.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3262303627.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S716987725.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2574505713.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3458716517.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3262302542.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3725019470.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2574504756.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3087408126.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1160771483.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1844544417.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3884936429.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2656147371.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2630330398.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1581606366.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S969689792.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S594014466.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2003641865.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2302259227.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4145020727.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S706509685.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2287957350.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_Se69573822.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4117265220.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3465175809.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (TlsClientContextImpl_t2876508357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (TlsClientProtocol_t400461164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3102[7] = 
{
	TlsClientProtocol_t400461164::get_offset_of_mTlsClient_50(),
	TlsClientProtocol_t400461164::get_offset_of_mTlsClientContext_51(),
	TlsClientProtocol_t400461164::get_offset_of_mSelectedSessionID_52(),
	TlsClientProtocol_t400461164::get_offset_of_mKeyExchange_53(),
	TlsClientProtocol_t400461164::get_offset_of_mAuthentication_54(),
	TlsClientProtocol_t400461164::get_offset_of_mCertificateStatus_55(),
	TlsClientProtocol_t400461164::get_offset_of_mCertificateRequest_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (TlsDHKeyExchange_t169955883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[6] = 
{
	TlsDHKeyExchange_t169955883::get_offset_of_mTlsSigner_3(),
	TlsDHKeyExchange_t169955883::get_offset_of_mDHParameters_4(),
	TlsDHKeyExchange_t169955883::get_offset_of_mServerPublicKey_5(),
	TlsDHKeyExchange_t169955883::get_offset_of_mAgreementCredentials_6(),
	TlsDHKeyExchange_t169955883::get_offset_of_mDHAgreePrivateKey_7(),
	TlsDHKeyExchange_t169955883::get_offset_of_mDHAgreePublicKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (TlsDHUtilities_t2113062801), -1, sizeof(TlsDHUtilities_t2113062801_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3107[11] = 
{
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_Two_0(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe2432_p_1(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe2432_2(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe3072_p_3(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe3072_4(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe4096_p_5(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe4096_6(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe6144_p_7(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe6144_8(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe8192_p_9(),
	TlsDHUtilities_t2113062801_StaticFields::get_offset_of_draft_ffdhe8192_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (TlsDeflateCompression_t206447956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[6] = 
{
	0,
	0,
	0,
	0,
	TlsDeflateCompression_t206447956::get_offset_of_zIn_4(),
	TlsDeflateCompression_t206447956::get_offset_of_zOut_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (DeflateOutputStream_t4293756753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (TlsDheKeyExchange_t3136059924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[1] = 
{
	TlsDheKeyExchange_t3136059924::get_offset_of_mServerCredentials_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (TlsDsaSigner_t3313030453), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (TlsDssSigner_t2995283323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (TlsECDHKeyExchange_t3594646817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[8] = 
{
	TlsECDHKeyExchange_t3594646817::get_offset_of_mTlsSigner_3(),
	TlsECDHKeyExchange_t3594646817::get_offset_of_mNamedCurves_4(),
	TlsECDHKeyExchange_t3594646817::get_offset_of_mClientECPointFormats_5(),
	TlsECDHKeyExchange_t3594646817::get_offset_of_mServerECPointFormats_6(),
	TlsECDHKeyExchange_t3594646817::get_offset_of_mServerPublicKey_7(),
	TlsECDHKeyExchange_t3594646817::get_offset_of_mAgreementCredentials_8(),
	TlsECDHKeyExchange_t3594646817::get_offset_of_mECAgreePrivateKey_9(),
	TlsECDHKeyExchange_t3594646817::get_offset_of_mECAgreePublicKey_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (TlsECDheKeyExchange_t1244283552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[1] = 
{
	TlsECDheKeyExchange_t1244283552::get_offset_of_mServerCredentials_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (TlsECDsaSigner_t294737663), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (TlsEccUtilities_t1394749888), -1, sizeof(TlsEccUtilities_t1394749888_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3116[1] = 
{
	TlsEccUtilities_t1394749888_StaticFields::get_offset_of_CurveNames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (TlsExtensionsUtilities_t3164696939), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (TlsFatalAlert_t2782940439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[1] = 
{
	TlsFatalAlert_t2782940439::get_offset_of_alertDescription_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (TlsMac_t4072537602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[6] = 
{
	TlsMac_t4072537602::get_offset_of_context_0(),
	TlsMac_t4072537602::get_offset_of_secret_1(),
	TlsMac_t4072537602::get_offset_of_mac_2(),
	TlsMac_t4072537602::get_offset_of_digestBlockSize_3(),
	TlsMac_t4072537602::get_offset_of_digestOverhead_4(),
	TlsMac_t4072537602::get_offset_of_macLength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (TlsNullCipher_t95059113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[3] = 
{
	TlsNullCipher_t95059113::get_offset_of_context_0(),
	TlsNullCipher_t95059113::get_offset_of_writeMac_1(),
	TlsNullCipher_t95059113::get_offset_of_readMac_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (TlsNullCompression_t2084920392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (TlsProtocol_t2348540693), -1, sizeof(TlsProtocol_t2348540693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3126[50] = 
{
	TlsProtocol_t2348540693_StaticFields::get_offset_of_TLS_ERROR_MESSAGE_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TlsProtocol_t2348540693::get_offset_of_mApplicationDataQueue_21(),
	TlsProtocol_t2348540693::get_offset_of_mAlertQueue_22(),
	TlsProtocol_t2348540693::get_offset_of_mHandshakeQueue_23(),
	TlsProtocol_t2348540693::get_offset_of_mRecordStream_24(),
	TlsProtocol_t2348540693::get_offset_of_mSecureRandom_25(),
	TlsProtocol_t2348540693::get_offset_of_mTlsStream_26(),
	TlsProtocol_t2348540693::get_offset_of_mClosed_27(),
	TlsProtocol_t2348540693::get_offset_of_mFailedWithError_28(),
	TlsProtocol_t2348540693::get_offset_of_mAppDataReady_29(),
	TlsProtocol_t2348540693::get_offset_of_mAppDataSplitEnabled_30(),
	TlsProtocol_t2348540693::get_offset_of_mAppDataSplitMode_31(),
	TlsProtocol_t2348540693::get_offset_of_mExpectedVerifyData_32(),
	TlsProtocol_t2348540693::get_offset_of_mTlsSession_33(),
	TlsProtocol_t2348540693::get_offset_of_mSessionParameters_34(),
	TlsProtocol_t2348540693::get_offset_of_mSecurityParameters_35(),
	TlsProtocol_t2348540693::get_offset_of_mPeerCertificate_36(),
	TlsProtocol_t2348540693::get_offset_of_mOfferedCipherSuites_37(),
	TlsProtocol_t2348540693::get_offset_of_mOfferedCompressionMethods_38(),
	TlsProtocol_t2348540693::get_offset_of_mClientExtensions_39(),
	TlsProtocol_t2348540693::get_offset_of_mServerExtensions_40(),
	TlsProtocol_t2348540693::get_offset_of_mConnectionState_41(),
	TlsProtocol_t2348540693::get_offset_of_mResumedSession_42(),
	TlsProtocol_t2348540693::get_offset_of_mReceivedChangeCipherSpec_43(),
	TlsProtocol_t2348540693::get_offset_of_mSecureRenegotiation_44(),
	TlsProtocol_t2348540693::get_offset_of_mAllowCertificateStatus_45(),
	TlsProtocol_t2348540693::get_offset_of_mExpectSessionTicket_46(),
	TlsProtocol_t2348540693::get_offset_of_mBlocking_47(),
	TlsProtocol_t2348540693::get_offset_of_mInputBuffers_48(),
	TlsProtocol_t2348540693::get_offset_of_mOutputBuffer_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (HandshakeMessage_t3117171608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (TlsRsaKeyExchange_t1676354575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[4] = 
{
	TlsRsaKeyExchange_t1676354575::get_offset_of_mServerPublicKey_3(),
	TlsRsaKeyExchange_t1676354575::get_offset_of_mRsaServerPublicKey_4(),
	TlsRsaKeyExchange_t1676354575::get_offset_of_mServerCredentials_5(),
	TlsRsaKeyExchange_t1676354575::get_offset_of_mPremasterSecret_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (TlsRsaSigner_t1265999015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (TlsRsaUtilities_t887753625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (TlsServerContextImpl_t4126653537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (TlsSessionImpl_t4078687945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[2] = 
{
	TlsSessionImpl_t4078687945::get_offset_of_mSessionID_0(),
	TlsSessionImpl_t4078687945::get_offset_of_mSessionParameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (TlsStream_t477156699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[1] = 
{
	TlsStream_t477156699::get_offset_of_handler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (TlsStreamCipher_t2175288028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[6] = 
{
	TlsStreamCipher_t2175288028::get_offset_of_context_0(),
	TlsStreamCipher_t2175288028::get_offset_of_encryptCipher_1(),
	TlsStreamCipher_t2175288028::get_offset_of_decryptCipher_2(),
	TlsStreamCipher_t2175288028::get_offset_of_writeMac_3(),
	TlsStreamCipher_t2175288028::get_offset_of_readMac_4(),
	TlsStreamCipher_t2175288028::get_offset_of_usesNonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (TlsUtilities_t441503077), -1, sizeof(TlsUtilities_t441503077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3140[7] = 
{
	TlsUtilities_t441503077_StaticFields::get_offset_of_EmptyBytes_0(),
	TlsUtilities_t441503077_StaticFields::get_offset_of_EmptyShorts_1(),
	TlsUtilities_t441503077_StaticFields::get_offset_of_EmptyInts_2(),
	TlsUtilities_t441503077_StaticFields::get_offset_of_EmptyLongs_3(),
	TlsUtilities_t441503077_StaticFields::get_offset_of_SSL_CLIENT_4(),
	TlsUtilities_t441503077_StaticFields::get_offset_of_SSL_SERVER_5(),
	TlsUtilities_t441503077_StaticFields::get_offset_of_SSL3_CONST_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (Pack_t3721483105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (BigInteger_t4268922522), -1, sizeof(BigInteger_t4268922522_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3142[35] = 
{
	BigInteger_t4268922522_StaticFields::get_offset_of_primeLists_0(),
	BigInteger_t4268922522_StaticFields::get_offset_of_primeProducts_1(),
	0,
	0,
	BigInteger_t4268922522_StaticFields::get_offset_of_ZeroMagnitude_4(),
	BigInteger_t4268922522_StaticFields::get_offset_of_ZeroEncoding_5(),
	BigInteger_t4268922522_StaticFields::get_offset_of_SMALL_CONSTANTS_6(),
	BigInteger_t4268922522_StaticFields::get_offset_of_Zero_7(),
	BigInteger_t4268922522_StaticFields::get_offset_of_One_8(),
	BigInteger_t4268922522_StaticFields::get_offset_of_Two_9(),
	BigInteger_t4268922522_StaticFields::get_offset_of_Three_10(),
	BigInteger_t4268922522_StaticFields::get_offset_of_Ten_11(),
	BigInteger_t4268922522_StaticFields::get_offset_of_BitLengthTable_12(),
	0,
	0,
	0,
	0,
	BigInteger_t4268922522_StaticFields::get_offset_of_radix2_17(),
	BigInteger_t4268922522_StaticFields::get_offset_of_radix2E_18(),
	BigInteger_t4268922522_StaticFields::get_offset_of_radix8_19(),
	BigInteger_t4268922522_StaticFields::get_offset_of_radix8E_20(),
	BigInteger_t4268922522_StaticFields::get_offset_of_radix10_21(),
	BigInteger_t4268922522_StaticFields::get_offset_of_radix10E_22(),
	BigInteger_t4268922522_StaticFields::get_offset_of_radix16_23(),
	BigInteger_t4268922522_StaticFields::get_offset_of_radix16E_24(),
	BigInteger_t4268922522_StaticFields::get_offset_of_RandomSource_25(),
	BigInteger_t4268922522_StaticFields::get_offset_of_ExpWindowThresholds_26(),
	0,
	0,
	0,
	BigInteger_t4268922522::get_offset_of_magnitude_30(),
	BigInteger_t4268922522::get_offset_of_sign_31(),
	BigInteger_t4268922522::get_offset_of_nBits_32(),
	BigInteger_t4268922522::get_offset_of_nBitLength_33(),
	BigInteger_t4268922522::get_offset_of_mQuote_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (ECAlgorithms_t1431241052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (ECCurve_t140895757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ECCurve_t140895757::get_offset_of_m_field_8(),
	ECCurve_t140895757::get_offset_of_m_a_9(),
	ECCurve_t140895757::get_offset_of_m_b_10(),
	ECCurve_t140895757::get_offset_of_m_order_11(),
	ECCurve_t140895757::get_offset_of_m_cofactor_12(),
	ECCurve_t140895757::get_offset_of_m_coord_13(),
	ECCurve_t140895757::get_offset_of_m_endomorphism_14(),
	ECCurve_t140895757::get_offset_of_m_multiplier_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (Config_t72665314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3145[4] = 
{
	Config_t72665314::get_offset_of_outer_0(),
	Config_t72665314::get_offset_of_coord_1(),
	Config_t72665314::get_offset_of_endomorphism_2(),
	Config_t72665314::get_offset_of_multiplier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (AbstractFpCurve_t2530650717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (FpCurve_t1098608013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[4] = 
{
	0,
	FpCurve_t1098608013::get_offset_of_m_q_17(),
	FpCurve_t1098608013::get_offset_of_m_r_18(),
	FpCurve_t1098608013::get_offset_of_m_infinity_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (AbstractF2mCurve_t4137102866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[1] = 
{
	AbstractF2mCurve_t4137102866::get_offset_of_si_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (F2mCurve_t2204106640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[6] = 
{
	0,
	F2mCurve_t2204106640::get_offset_of_m_18(),
	F2mCurve_t2204106640::get_offset_of_k1_19(),
	F2mCurve_t2204106640::get_offset_of_k2_20(),
	F2mCurve_t2204106640::get_offset_of_k3_21(),
	F2mCurve_t2204106640::get_offset_of_m_infinity_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (ECFieldElement_t1092946118), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (FpFieldElement_t2948427972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[3] = 
{
	FpFieldElement_t2948427972::get_offset_of_q_0(),
	FpFieldElement_t2948427972::get_offset_of_r_1(),
	FpFieldElement_t2948427972::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (F2mFieldElement_t1860364149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[7] = 
{
	0,
	0,
	0,
	F2mFieldElement_t1860364149::get_offset_of_representation_3(),
	F2mFieldElement_t1860364149::get_offset_of_m_4(),
	F2mFieldElement_t1860364149::get_offset_of_ks_5(),
	F2mFieldElement_t1860364149::get_offset_of_x_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (ECPoint_t626351532), -1, sizeof(ECPoint_t626351532_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3153[7] = 
{
	ECPoint_t626351532_StaticFields::get_offset_of_EMPTY_ZS_0(),
	ECPoint_t626351532::get_offset_of_m_curve_1(),
	ECPoint_t626351532::get_offset_of_m_x_2(),
	ECPoint_t626351532::get_offset_of_m_y_3(),
	ECPoint_t626351532::get_offset_of_m_zs_4(),
	ECPoint_t626351532::get_offset_of_m_withCompression_5(),
	ECPoint_t626351532::get_offset_of_m_preCompTable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (ECPointBase_t3119694375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (AbstractFpPoint_t4047682300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (FpPoint_t301080990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (AbstractF2mPoint_t883694769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (F2mPoint_t2324796161), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (LongArray_t194261203), -1, sizeof(LongArray_t194261203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3160[8] = 
{
	LongArray_t194261203_StaticFields::get_offset_of_INTERLEAVE2_TABLE_0(),
	LongArray_t194261203_StaticFields::get_offset_of_INTERLEAVE3_TABLE_1(),
	LongArray_t194261203_StaticFields::get_offset_of_INTERLEAVE4_TABLE_2(),
	LongArray_t194261203_StaticFields::get_offset_of_INTERLEAVE5_TABLE_3(),
	LongArray_t194261203_StaticFields::get_offset_of_INTERLEAVE7_TABLE_4(),
	0,
	LongArray_t194261203_StaticFields::get_offset_of_BitLengths_6(),
	LongArray_t194261203::get_offset_of_m_ints_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (ScaleXPointMap_t2361229496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[1] = 
{
	ScaleXPointMap_t2361229496::get_offset_of_scale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (SimpleBigDecimal_t2857025313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[2] = 
{
	SimpleBigDecimal_t2857025313::get_offset_of_bigInt_0(),
	SimpleBigDecimal_t2857025313::get_offset_of_scale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (Tnaf_t1139038785), -1, sizeof(Tnaf_t1139038785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3163[10] = 
{
	Tnaf_t1139038785_StaticFields::get_offset_of_MinusOne_0(),
	Tnaf_t1139038785_StaticFields::get_offset_of_MinusTwo_1(),
	Tnaf_t1139038785_StaticFields::get_offset_of_MinusThree_2(),
	Tnaf_t1139038785_StaticFields::get_offset_of_Four_3(),
	0,
	0,
	Tnaf_t1139038785_StaticFields::get_offset_of_Alpha0_6(),
	Tnaf_t1139038785_StaticFields::get_offset_of_Alpha0Tnaf_7(),
	Tnaf_t1139038785_StaticFields::get_offset_of_Alpha1_8(),
	Tnaf_t1139038785_StaticFields::get_offset_of_Alpha1Tnaf_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (ZTauElement_t2571810054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[2] = 
{
	ZTauElement_t2571810054::get_offset_of_u_0(),
	ZTauElement_t2571810054::get_offset_of_v_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (Curve25519_t1415304183), -1, sizeof(Curve25519_t1415304183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3165[3] = 
{
	Curve25519_t1415304183_StaticFields::get_offset_of_q_16(),
	0,
	Curve25519_t1415304183::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (Curve25519Field_t734603359), -1, sizeof(Curve25519Field_t734603359_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3166[4] = 
{
	Curve25519Field_t734603359_StaticFields::get_offset_of_P_0(),
	0,
	Curve25519Field_t734603359_StaticFields::get_offset_of_PExt_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (Curve25519FieldElement_t2608249339), -1, sizeof(Curve25519FieldElement_t2608249339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3167[3] = 
{
	Curve25519FieldElement_t2608249339_StaticFields::get_offset_of_Q_0(),
	Curve25519FieldElement_t2608249339_StaticFields::get_offset_of_PRECOMP_POW2_1(),
	Curve25519FieldElement_t2608249339::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (Curve25519Point_t4149081277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (SecP128R1Curve_t701087298), -1, sizeof(SecP128R1Curve_t701087298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3169[3] = 
{
	SecP128R1Curve_t701087298_StaticFields::get_offset_of_q_16(),
	0,
	SecP128R1Curve_t701087298::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (SecP128R1Field_t2282534695), -1, sizeof(SecP128R1Field_t2282534695_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3170[5] = 
{
	SecP128R1Field_t2282534695_StaticFields::get_offset_of_P_0(),
	SecP128R1Field_t2282534695_StaticFields::get_offset_of_PExt_1(),
	SecP128R1Field_t2282534695_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (SecP128R1FieldElement_t3090633281), -1, sizeof(SecP128R1FieldElement_t3090633281_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3171[2] = 
{
	SecP128R1FieldElement_t3090633281_StaticFields::get_offset_of_Q_0(),
	SecP128R1FieldElement_t3090633281::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (SecP128R1Point_t4111842189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (SecP160K1Curve_t3346222387), -1, sizeof(SecP160K1Curve_t3346222387_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3173[3] = 
{
	SecP160K1Curve_t3346222387_StaticFields::get_offset_of_q_16(),
	0,
	SecP160K1Curve_t3346222387::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (SecP160K1Point_t2602261220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (SecP160R1Curve_t3458717606), -1, sizeof(SecP160R1Curve_t3458717606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3175[3] = 
{
	SecP160R1Curve_t3458717606_StaticFields::get_offset_of_q_16(),
	0,
	SecP160R1Curve_t3458717606::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (SecP160R1Field_t3262303627), -1, sizeof(SecP160R1Field_t3262303627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3176[6] = 
{
	SecP160R1Field_t3262303627_StaticFields::get_offset_of_P_0(),
	SecP160R1Field_t3262303627_StaticFields::get_offset_of_PExt_1(),
	SecP160R1Field_t3262303627_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (SecP160R1FieldElement_t716987725), -1, sizeof(SecP160R1FieldElement_t716987725_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3177[2] = 
{
	SecP160R1FieldElement_t716987725_StaticFields::get_offset_of_Q_0(),
	SecP160R1FieldElement_t716987725::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (SecP160R1Point_t2574505713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (SecP160R2Curve_t3458716517), -1, sizeof(SecP160R2Curve_t3458716517_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3179[3] = 
{
	SecP160R2Curve_t3458716517_StaticFields::get_offset_of_q_16(),
	0,
	SecP160R2Curve_t3458716517::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (SecP160R2Field_t3262302542), -1, sizeof(SecP160R2Field_t3262302542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3180[6] = 
{
	SecP160R2Field_t3262302542_StaticFields::get_offset_of_P_0(),
	SecP160R2Field_t3262302542_StaticFields::get_offset_of_PExt_1(),
	SecP160R2Field_t3262302542_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (SecP160R2FieldElement_t3725019470), -1, sizeof(SecP160R2FieldElement_t3725019470_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3181[2] = 
{
	SecP160R2FieldElement_t3725019470_StaticFields::get_offset_of_Q_0(),
	SecP160R2FieldElement_t3725019470::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (SecP160R2Point_t2574504756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (SecP192K1Curve_t3087408126), -1, sizeof(SecP192K1Curve_t3087408126_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3183[3] = 
{
	SecP192K1Curve_t3087408126_StaticFields::get_offset_of_q_16(),
	0,
	SecP192K1Curve_t3087408126::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (SecP192K1Field_t1160771483), -1, sizeof(SecP192K1Field_t1160771483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3184[6] = 
{
	SecP192K1Field_t1160771483_StaticFields::get_offset_of_P_0(),
	SecP192K1Field_t1160771483_StaticFields::get_offset_of_PExt_1(),
	SecP192K1Field_t1160771483_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (SecP192K1FieldElement_t1844544417), -1, sizeof(SecP192K1FieldElement_t1844544417_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3185[2] = 
{
	SecP192K1FieldElement_t1844544417_StaticFields::get_offset_of_Q_0(),
	SecP192K1FieldElement_t1844544417::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (SecP192K1Point_t3884936429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (SecP192R1Curve_t2656147371), -1, sizeof(SecP192R1Curve_t2656147371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3187[3] = 
{
	SecP192R1Curve_t2656147371_StaticFields::get_offset_of_q_16(),
	0,
	SecP192R1Curve_t2656147371::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (SecP192R1Field_t2630330398), -1, sizeof(SecP192R1Field_t2630330398_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3188[5] = 
{
	SecP192R1Field_t2630330398_StaticFields::get_offset_of_P_0(),
	SecP192R1Field_t2630330398_StaticFields::get_offset_of_PExt_1(),
	SecP192R1Field_t2630330398_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (SecP192R1FieldElement_t1581606366), -1, sizeof(SecP192R1FieldElement_t1581606366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3189[2] = 
{
	SecP192R1FieldElement_t1581606366_StaticFields::get_offset_of_Q_0(),
	SecP192R1FieldElement_t1581606366::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (SecP192R1Point_t969689792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (SecP224K1Curve_t594014466), -1, sizeof(SecP224K1Curve_t594014466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3191[3] = 
{
	SecP224K1Curve_t594014466_StaticFields::get_offset_of_q_16(),
	0,
	SecP224K1Curve_t594014466::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (SecP224K1Field_t2003641865), -1, sizeof(SecP224K1Field_t2003641865_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3192[6] = 
{
	SecP224K1Field_t2003641865_StaticFields::get_offset_of_P_0(),
	SecP224K1Field_t2003641865_StaticFields::get_offset_of_PExt_1(),
	SecP224K1Field_t2003641865_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (SecP224K1FieldElement_t2302259227), -1, sizeof(SecP224K1FieldElement_t2302259227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3193[3] = 
{
	SecP224K1FieldElement_t2302259227_StaticFields::get_offset_of_Q_0(),
	SecP224K1FieldElement_t2302259227_StaticFields::get_offset_of_PRECOMP_POW2_1(),
	SecP224K1FieldElement_t2302259227::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (SecP224K1Point_t4145020727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (SecP224R1Curve_t706509685), -1, sizeof(SecP224R1Curve_t706509685_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3195[3] = 
{
	SecP224R1Curve_t706509685_StaticFields::get_offset_of_q_16(),
	0,
	SecP224R1Curve_t706509685::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (SecP224R1Field_t2287957350), -1, sizeof(SecP224R1Field_t2287957350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3196[5] = 
{
	SecP224R1Field_t2287957350_StaticFields::get_offset_of_P_0(),
	SecP224R1Field_t2287957350_StaticFields::get_offset_of_PExt_1(),
	SecP224R1Field_t2287957350_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (SecP224R1FieldElement_t69573822), -1, sizeof(SecP224R1FieldElement_t69573822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3197[2] = 
{
	SecP224R1FieldElement_t69573822_StaticFields::get_offset_of_Q_0(),
	SecP224R1FieldElement_t69573822::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (SecP224R1Point_t4117265220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (SecP256K1Curve_t3465175809), -1, sizeof(SecP256K1Curve_t3465175809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3199[3] = 
{
	SecP256K1Curve_t3465175809_StaticFields::get_offset_of_q_16(),
	0,
	SecP256K1Curve_t3465175809::get_offset_of_m_infinity_18(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
