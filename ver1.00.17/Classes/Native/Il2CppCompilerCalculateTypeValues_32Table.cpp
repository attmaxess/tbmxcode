﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4055644322.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3640352222.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2226336700.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1633071870.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4124360053.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1196028363.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1982981439.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S771642946.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2917740615.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3721703645.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3774205409.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S696024791.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2277472452.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4038813788.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4106780194.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S369756392.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2764394868.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1172060768.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1521970469.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1172053409.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1521962978.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4283726064.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2707684392.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2372832748.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1833811373.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2372829481.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1833808238.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4196096651.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1728460721.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S132931622.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3683937883.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S245426841.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3656182376.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S245419482.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3656174885.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3217661632.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1707604124.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S663653176.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3272162893.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S663645817.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3272155402.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4283899217.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1894758407.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4210650542.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3860742633.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2378546603.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1839525484.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4283905371.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S598377489.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_4211010036.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3861101235.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_2432108964.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1170406292.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1564258929.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1967734354.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3077597332.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1785192879.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3878961688.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3079341752.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3466749709.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1833857086.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3579244928.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_Se28239691.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_S686715528.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3353873688.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1442275455.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_1092367290.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3905138812.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Custom_3366117437.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Endo_Gl2692134550.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Endo_Glv500309695.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl2477536561.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli762726336.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli404341464.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl4261770478.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli643117704.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl2813560483.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipli485024160.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl1510522174.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl1999582299.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_EC_Multipl3900730752.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_Finit239290640.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_GF2P3071691857.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_Gene3677945350.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Field_Prime623492065.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Interl4139004379.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Mod3464319674.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat2301520307.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat128435875714.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat16079645350.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat1923211813225.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat224745051907.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (SecP256K1Field_t4055644322), -1, sizeof(SecP256K1Field_t4055644322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3200[6] = 
{
	SecP256K1Field_t4055644322_StaticFields::get_offset_of_P_0(),
	SecP256K1Field_t4055644322_StaticFields::get_offset_of_PExt_1(),
	SecP256K1Field_t4055644322_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (SecP256K1FieldElement_t3640352222), -1, sizeof(SecP256K1FieldElement_t3640352222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3201[2] = 
{
	SecP256K1FieldElement_t3640352222_StaticFields::get_offset_of_Q_0(),
	SecP256K1FieldElement_t3640352222::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (SecP256K1Point_t2226336700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (SecP256R1Curve_t1633071870), -1, sizeof(SecP256R1Curve_t1633071870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3203[3] = 
{
	SecP256R1Curve_t1633071870_StaticFields::get_offset_of_q_16(),
	0,
	SecP256R1Curve_t1633071870::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (SecP256R1Field_t4124360053), -1, sizeof(SecP256R1Field_t4124360053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3204[4] = 
{
	SecP256R1Field_t4124360053_StaticFields::get_offset_of_P_0(),
	SecP256R1Field_t4124360053_StaticFields::get_offset_of_PExt_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (SecP256R1FieldElement_t1196028363), -1, sizeof(SecP256R1FieldElement_t1196028363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3205[2] = 
{
	SecP256R1FieldElement_t1196028363_StaticFields::get_offset_of_Q_0(),
	SecP256R1FieldElement_t1196028363::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (SecP256R1Point_t1982981439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (SecP384R1Curve_t771642946), -1, sizeof(SecP384R1Curve_t771642946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3207[3] = 
{
	SecP384R1Curve_t771642946_StaticFields::get_offset_of_q_16(),
	0,
	SecP384R1Curve_t771642946::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (SecP384R1Field_t2917740615), -1, sizeof(SecP384R1Field_t2917740615_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3208[5] = 
{
	SecP384R1Field_t2917740615_StaticFields::get_offset_of_P_0(),
	SecP384R1Field_t2917740615_StaticFields::get_offset_of_PExt_1(),
	SecP384R1Field_t2917740615_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (SecP384R1FieldElement_t3721703645), -1, sizeof(SecP384R1FieldElement_t3721703645_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3209[2] = 
{
	SecP384R1FieldElement_t3721703645_StaticFields::get_offset_of_Q_0(),
	SecP384R1FieldElement_t3721703645::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (SecP384R1Point_t3774205409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (SecP521R1Curve_t696024791), -1, sizeof(SecP521R1Curve_t696024791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3211[3] = 
{
	SecP521R1Curve_t696024791_StaticFields::get_offset_of_q_16(),
	0,
	SecP521R1Curve_t696024791::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (SecP521R1Field_t2277472452), -1, sizeof(SecP521R1Field_t2277472452_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3212[2] = 
{
	SecP521R1Field_t2277472452_StaticFields::get_offset_of_P_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (SecP521R1FieldElement_t4038813788), -1, sizeof(SecP521R1FieldElement_t4038813788_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3213[2] = 
{
	SecP521R1FieldElement_t4038813788_StaticFields::get_offset_of_Q_0(),
	SecP521R1FieldElement_t4038813788::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (SecP521R1Point_t4106780194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (SecT113Field_t369756392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3215[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (SecT113FieldElement_t2764394868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[1] = 
{
	SecT113FieldElement_t2764394868::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (SecT113R1Curve_t1172060768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3217[2] = 
{
	0,
	SecT113R1Curve_t1172060768::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (SecT113R1Point_t1521970469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (SecT113R2Curve_t1172053409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3219[2] = 
{
	0,
	SecT113R2Curve_t1172053409::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (SecT113R2Point_t1521962978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (SecT131Field_t4283726064), -1, sizeof(SecT131Field_t4283726064_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3221[3] = 
{
	0,
	0,
	SecT131Field_t4283726064_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (SecT131FieldElement_t2707684392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3222[1] = 
{
	SecT131FieldElement_t2707684392::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (SecT131R1Curve_t2372832748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3223[2] = 
{
	0,
	SecT131R1Curve_t2372832748::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (SecT131R1Point_t1833811373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (SecT131R2Curve_t2372829481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[2] = 
{
	0,
	SecT131R2Curve_t2372829481::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (SecT131R2Point_t1833808238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (SecT163Field_t4196096651), -1, sizeof(SecT163Field_t4196096651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3227[3] = 
{
	0,
	0,
	SecT163Field_t4196096651_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (SecT163FieldElement_t1728460721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[1] = 
{
	SecT163FieldElement_t1728460721::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (SecT163K1Curve_t132931622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[2] = 
{
	0,
	SecT163K1Curve_t132931622::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (SecT163K1Point_t3683937883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (SecT163R1Curve_t245426841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[2] = 
{
	0,
	SecT163R1Curve_t245426841::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (SecT163R1Point_t3656182376), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (SecT163R2Curve_t245419482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3233[2] = 
{
	0,
	SecT163R2Curve_t245419482::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (SecT163R2Point_t3656174885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (SecT193Field_t3217661632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3235[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (SecT193FieldElement_t1707604124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3236[1] = 
{
	SecT193FieldElement_t1707604124::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (SecT193R1Curve_t663653176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[2] = 
{
	0,
	SecT193R1Curve_t663653176::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (SecT193R1Point_t3272162893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (SecT193R2Curve_t663645817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[2] = 
{
	0,
	SecT193R2Curve_t663645817::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (SecT193R2Point_t3272155402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (SecT233Field_t4283899217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3241[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (SecT233FieldElement_t1894758407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[1] = 
{
	SecT233FieldElement_t1894758407::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (SecT233K1Curve_t4210650542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3243[2] = 
{
	0,
	SecT233K1Curve_t4210650542::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (SecT233K1Point_t3860742633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (SecT233R1Curve_t2378546603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[2] = 
{
	0,
	SecT233R1Curve_t2378546603::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (SecT233R1Point_t1839525484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (SecT239Field_t4283905371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3247[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (SecT239FieldElement_t598377489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3248[1] = 
{
	SecT239FieldElement_t598377489::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (SecT239K1Curve_t4211010036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[2] = 
{
	0,
	SecT239K1Curve_t4211010036::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (SecT239K1Point_t3861101235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (SecT283Field_t2432108964), -1, sizeof(SecT283Field_t2432108964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3251[3] = 
{
	0,
	0,
	SecT283Field_t2432108964_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (SecT283FieldElement_t1170406292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[1] = 
{
	SecT283FieldElement_t1170406292::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (SecT283K1Curve_t1564258929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[2] = 
{
	0,
	SecT283K1Curve_t1564258929::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (SecT283K1Point_t1967734354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (SecT283R1Curve_t3077597332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[2] = 
{
	0,
	SecT283R1Curve_t3077597332::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (SecT283R1Point_t1785192879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (SecT409Field_t3878961688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3257[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (SecT409FieldElement_t3079341752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[1] = 
{
	SecT409FieldElement_t3079341752::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (SecT409K1Curve_t3466749709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3259[2] = 
{
	0,
	SecT409K1Curve_t3466749709::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (SecT409K1Point_t1833857086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (SecT409R1Curve_t3579244928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3261[2] = 
{
	0,
	SecT409R1Curve_t3579244928::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (SecT409R1Point_t28239691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (SecT571Field_t686715528), -1, sizeof(SecT571Field_t686715528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3263[3] = 
{
	0,
	0,
	SecT571Field_t686715528_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (SecT571FieldElement_t3353873688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3264[1] = 
{
	SecT571FieldElement_t3353873688::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (SecT571K1Curve_t1442275455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3265[2] = 
{
	0,
	SecT571K1Curve_t1442275455::get_offset_of_m_infinity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (SecT571K1Point_t1092367290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (SecT571R1Curve_t3905138812), -1, sizeof(SecT571R1Curve_t3905138812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3267[4] = 
{
	0,
	SecT571R1Curve_t3905138812::get_offset_of_m_infinity_18(),
	SecT571R1Curve_t3905138812_StaticFields::get_offset_of_SecT571R1_B_19(),
	SecT571R1Curve_t3905138812_StaticFields::get_offset_of_SecT571R1_B_SQRT_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (SecT571R1Point_t3366117437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (GlvTypeBEndomorphism_t2692134550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3271[3] = 
{
	GlvTypeBEndomorphism_t2692134550::get_offset_of_m_curve_0(),
	GlvTypeBEndomorphism_t2692134550::get_offset_of_m_parameters_1(),
	GlvTypeBEndomorphism_t2692134550::get_offset_of_m_pointMap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (GlvTypeBParameters_t500309695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3272[7] = 
{
	GlvTypeBParameters_t500309695::get_offset_of_m_beta_0(),
	GlvTypeBParameters_t500309695::get_offset_of_m_lambda_1(),
	GlvTypeBParameters_t500309695::get_offset_of_m_v1_2(),
	GlvTypeBParameters_t500309695::get_offset_of_m_v2_3(),
	GlvTypeBParameters_t500309695::get_offset_of_m_g1_4(),
	GlvTypeBParameters_t500309695::get_offset_of_m_g2_5(),
	GlvTypeBParameters_t500309695::get_offset_of_m_bits_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (AbstractECMultiplier_t2477536561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (FixedPointCombMultiplier_t762726336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (FixedPointPreCompInfo_t404341464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3276[2] = 
{
	FixedPointPreCompInfo_t404341464::get_offset_of_m_preComp_0(),
	FixedPointPreCompInfo_t404341464::get_offset_of_m_width_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (FixedPointUtilities_t4261770478), -1, sizeof(FixedPointUtilities_t4261770478_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3277[1] = 
{
	FixedPointUtilities_t4261770478_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (GlvMultiplier_t643117704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3278[2] = 
{
	GlvMultiplier_t643117704::get_offset_of_curve_0(),
	GlvMultiplier_t643117704::get_offset_of_glvEndomorphism_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (WNafL2RMultiplier_t2813560483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (WNafPreCompInfo_t485024160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3281[3] = 
{
	WNafPreCompInfo_t485024160::get_offset_of_m_preComp_0(),
	WNafPreCompInfo_t485024160::get_offset_of_m_preCompNeg_1(),
	WNafPreCompInfo_t485024160::get_offset_of_m_twice_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (WNafUtilities_t1510522174), -1, sizeof(WNafUtilities_t1510522174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3282[5] = 
{
	WNafUtilities_t1510522174_StaticFields::get_offset_of_PRECOMP_NAME_0(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_EMPTY_BYTES_2(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_EMPTY_INTS_3(),
	WNafUtilities_t1510522174_StaticFields::get_offset_of_EMPTY_POINTS_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (WTauNafMultiplier_t1999582299), -1, sizeof(WTauNafMultiplier_t1999582299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3283[1] = 
{
	WTauNafMultiplier_t1999582299_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (WTauNafPreCompInfo_t3900730752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[1] = 
{
	WTauNafPreCompInfo_t3900730752::get_offset_of_m_preComp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (FiniteFields_t239290640), -1, sizeof(FiniteFields_t239290640_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3285[2] = 
{
	FiniteFields_t239290640_StaticFields::get_offset_of_GF_2_0(),
	FiniteFields_t239290640_StaticFields::get_offset_of_GF_3_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (GF2Polynomial_t3071691857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3286[1] = 
{
	GF2Polynomial_t3071691857::get_offset_of_exponents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (GenericPolynomialExtensionField_t3677945350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3287[2] = 
{
	GenericPolynomialExtensionField_t3677945350::get_offset_of_subfield_0(),
	GenericPolynomialExtensionField_t3677945350::get_offset_of_minimalPolynomial_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (PrimeField_t623492065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[1] = 
{
	PrimeField_t623492065::get_offset_of_characteristic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (Interleave_t4139004379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (Mod_t3464319674), -1, sizeof(Mod_t3464319674_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3294[1] = 
{
	Mod_t3464319674_StaticFields::get_offset_of_RandomSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (Nat_t2301520307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3295[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (Nat128_t435875714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (Nat160_t79645350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3297[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (Nat192_t3211813225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3298[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (Nat224_t745051907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3299[1] = 
{
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
