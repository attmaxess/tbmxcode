﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat2561907851314.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat320361970356.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat3842331338814.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat4483709206091.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat5123776463237.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Math_Raw_Nat5761807094767.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_DigestUti7273606.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Digest2809291156.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Genera4050103391.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Invali4238978569.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Invali2090211591.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_KeyExce751530378.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_MacUtil821348233.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Public2165232312.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Secure3117234712.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Securit855024565.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Signatu679135775.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Signer1398406104.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Certifi381714059.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Certif1746631886.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Certif1755277959.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Certif1118447835.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Certif1209698340.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Security_Certif1436677746.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Array1391654744.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_BigInt825470227.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Enums1566591880.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Integ2148414311.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Memoa3856404536.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Platfo985240101.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_String679430088.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Times1298011468.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Colle2694412802.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Colle3712092847.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Colle3994676183.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Colle3447065104.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Colle3608575830.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Date_1544531432.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Date_3547118125.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Encod2489710407.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Encode781552331.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Encod3743762079.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Encode675371731.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Ba1872372563.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Base91853118.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Fil137877680.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Pu2874148781.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_St3837429295.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_St3420021211.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Te2454987356.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Tee889422123.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Pem922144303.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Pe2776589125.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Pe1142728083.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Pe2807178499.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_IO_Pe2248157217.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Net_I2975647479.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_2375507285.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_De77159307.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_1249383685.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_I706751929.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_2579876053.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_3604104823.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_I807188643.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_3797748839.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_S561547724.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_Z597810407.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_Z708755204.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Utilities_Zlib_4080091066.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_PemParser1487846991.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_X509Certif2250736941.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_X509Certif3646547314.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_X509Crl2621233049.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_X509CrlEnt1400823213.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_X509CrlPars447353842.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_X509Extens1429324694.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_X509Signatu179355160.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_X509_Extension_3063786951.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_States3624480836.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_OnGene2459579164.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_OnMess4204746873.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_OnError676891988.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_OnRetr1334454456.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_OnEvent790674770.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_OnStat1222973807.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_EventS3924127377.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_EventS2287402344.h"
#include "AssemblyU2DCSharp_BestHTTP_ServerSentEvents_Messag1650395211.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Authentication_4078916882.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Authentication_1617501265.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnNonHubMessage1922405057.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnConnectedDele3283761253.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnClosedDelegate587495364.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnErrorDelegate3605384424.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (Nat256_t1907851314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (Nat320_t361970356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (Nat384_t2331338814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (Nat448_t3709206091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (Nat512_t3776463237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (Nat576_t1807094767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (DigestUtilities_t7273606), -1, sizeof(DigestUtilities_t7273606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3306[2] = 
{
	DigestUtilities_t7273606_StaticFields::get_offset_of_algorithms_0(),
	DigestUtilities_t7273606_StaticFields::get_offset_of_oids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (DigestAlgorithm_t2809291156)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3307[29] = 
{
	DigestAlgorithm_t2809291156::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (GeneralSecurityException_t4050103391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (InvalidKeyException_t4238978569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (InvalidParameterException_t2090211591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (KeyException_t751530378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (MacUtilities_t821348233), -1, sizeof(MacUtilities_t821348233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3312[1] = 
{
	MacUtilities_t821348233_StaticFields::get_offset_of_algorithms_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (PublicKeyFactory_t2165232312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (SecureRandom_t3117234712), -1, sizeof(SecureRandom_t3117234712_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3314[4] = 
{
	SecureRandom_t3117234712_StaticFields::get_offset_of_counter_3(),
	SecureRandom_t3117234712_StaticFields::get_offset_of_master_4(),
	SecureRandom_t3117234712::get_offset_of_generator_5(),
	SecureRandom_t3117234712_StaticFields::get_offset_of_DoubleScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (SecurityUtilityException_t855024565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (SignatureException_t679135775), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (SignerUtilities_t1398406104), -1, sizeof(SignerUtilities_t1398406104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3317[2] = 
{
	SignerUtilities_t1398406104_StaticFields::get_offset_of_algorithms_0(),
	SignerUtilities_t1398406104_StaticFields::get_offset_of_oids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (CertificateEncodingException_t381714059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (CertificateException_t1746631886), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (CertificateExpiredException_t1755277959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (CertificateNotYetValidException_t1118447835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (CertificateParsingException_t1209698340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (CrlException_t1436677746), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (Arrays_t1391654744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (BigIntegers_t825470227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (Enums_t1566591880), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (Integers_t2148414311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (MemoableResetException_t3856404536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (Platform_t985240101), -1, sizeof(Platform_t985240101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3330[2] = 
{
	Platform_t985240101_StaticFields::get_offset_of_InvariantCompareInfo_0(),
	Platform_t985240101_StaticFields::get_offset_of_NewLine_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (Strings_t679430088), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (Times_t1298011468), -1, sizeof(Times_t1298011468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3332[1] = 
{
	Times_t1298011468_StaticFields::get_offset_of_NanosecondsPerTick_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (CollectionUtilities_t2694412802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (EmptyEnumerable_t3712092847), -1, sizeof(EmptyEnumerable_t3712092847_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3334[1] = 
{
	EmptyEnumerable_t3712092847_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (EmptyEnumerator_t3994676183), -1, sizeof(EmptyEnumerator_t3994676183_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3335[1] = 
{
	EmptyEnumerator_t3994676183_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (EnumerableProxy_t3447065104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[1] = 
{
	EnumerableProxy_t3447065104::get_offset_of_inner_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (HashSet_t3608575830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[1] = 
{
	HashSet_t3608575830::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (DateTimeObject_t1544531432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3339[1] = 
{
	DateTimeObject_t1544531432::get_offset_of_dt_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (DateTimeUtilities_t3547118125), -1, sizeof(DateTimeUtilities_t3547118125_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3340[1] = 
{
	DateTimeUtilities_t3547118125_StaticFields::get_offset_of_UnixEpoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (Base64_t2489710407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (Base64Encoder_t781552331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[3] = 
{
	Base64Encoder_t781552331::get_offset_of_encodingTable_0(),
	Base64Encoder_t781552331::get_offset_of_padding_1(),
	Base64Encoder_t781552331::get_offset_of_decodingTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (Hex_t3743762079), -1, sizeof(Hex_t3743762079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3343[1] = 
{
	Hex_t3743762079_StaticFields::get_offset_of_encoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (HexEncoder_t675371731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[2] = 
{
	HexEncoder_t675371731::get_offset_of_encodingTable_0(),
	HexEncoder_t675371731::get_offset_of_decodingTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (BaseInputStream_t1872372563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[1] = 
{
	BaseInputStream_t1872372563::get_offset_of_closed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (BaseOutputStream_t91853118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[1] = 
{
	BaseOutputStream_t91853118::get_offset_of_closed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (FilterStream_t137877680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[1] = 
{
	FilterStream_t137877680::get_offset_of_s_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (PushbackStream_t2874148781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3349[1] = 
{
	PushbackStream_t2874148781::get_offset_of_buf_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (StreamOverflowException_t3837429295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (Streams_t3420021211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (TeeInputStream_t2454987356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[2] = 
{
	TeeInputStream_t2454987356::get_offset_of_input_2(),
	TeeInputStream_t2454987356::get_offset_of_tee_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (TeeOutputStream_t889422123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[2] = 
{
	TeeOutputStream_t889422123::get_offset_of_output_2(),
	TeeOutputStream_t889422123::get_offset_of_tee_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (PemGenerationException_t922144303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (PemHeader_t2776589125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3355[2] = 
{
	PemHeader_t2776589125::get_offset_of_name_0(),
	PemHeader_t2776589125::get_offset_of_val_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (PemObject_t1142728083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[3] = 
{
	PemObject_t1142728083::get_offset_of_type_0(),
	PemObject_t1142728083::get_offset_of_headers_1(),
	PemObject_t1142728083::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (PemReader_t2807178499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[3] = 
{
	0,
	0,
	PemReader_t2807178499::get_offset_of_reader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (PemWriter_t2248157217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[4] = 
{
	0,
	PemWriter_t2248157217::get_offset_of_writer_1(),
	PemWriter_t2248157217::get_offset_of_nlLength_2(),
	PemWriter_t2248157217::get_offset_of_buf_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (IPAddress_t2975647479), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (Adler32_t2375507285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3361[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (Deflate_t77159307), -1, sizeof(Deflate_t77159307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3362[113] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Deflate_t77159307_StaticFields::get_offset_of_config_table_7(),
	Deflate_t77159307_StaticFields::get_offset_of_z_errmsg_8(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Deflate_t77159307::get_offset_of_strm_56(),
	Deflate_t77159307::get_offset_of_status_57(),
	Deflate_t77159307::get_offset_of_pending_buf_58(),
	Deflate_t77159307::get_offset_of_pending_buf_size_59(),
	Deflate_t77159307::get_offset_of_pending_out_60(),
	Deflate_t77159307::get_offset_of_pending_61(),
	Deflate_t77159307::get_offset_of_noheader_62(),
	Deflate_t77159307::get_offset_of_data_type_63(),
	Deflate_t77159307::get_offset_of_method_64(),
	Deflate_t77159307::get_offset_of_last_flush_65(),
	Deflate_t77159307::get_offset_of_w_size_66(),
	Deflate_t77159307::get_offset_of_w_bits_67(),
	Deflate_t77159307::get_offset_of_w_mask_68(),
	Deflate_t77159307::get_offset_of_window_69(),
	Deflate_t77159307::get_offset_of_window_size_70(),
	Deflate_t77159307::get_offset_of_prev_71(),
	Deflate_t77159307::get_offset_of_head_72(),
	Deflate_t77159307::get_offset_of_ins_h_73(),
	Deflate_t77159307::get_offset_of_hash_size_74(),
	Deflate_t77159307::get_offset_of_hash_bits_75(),
	Deflate_t77159307::get_offset_of_hash_mask_76(),
	Deflate_t77159307::get_offset_of_hash_shift_77(),
	Deflate_t77159307::get_offset_of_block_start_78(),
	Deflate_t77159307::get_offset_of_match_length_79(),
	Deflate_t77159307::get_offset_of_prev_match_80(),
	Deflate_t77159307::get_offset_of_match_available_81(),
	Deflate_t77159307::get_offset_of_strstart_82(),
	Deflate_t77159307::get_offset_of_match_start_83(),
	Deflate_t77159307::get_offset_of_lookahead_84(),
	Deflate_t77159307::get_offset_of_prev_length_85(),
	Deflate_t77159307::get_offset_of_max_chain_length_86(),
	Deflate_t77159307::get_offset_of_max_lazy_match_87(),
	Deflate_t77159307::get_offset_of_level_88(),
	Deflate_t77159307::get_offset_of_strategy_89(),
	Deflate_t77159307::get_offset_of_good_match_90(),
	Deflate_t77159307::get_offset_of_nice_match_91(),
	Deflate_t77159307::get_offset_of_dyn_ltree_92(),
	Deflate_t77159307::get_offset_of_dyn_dtree_93(),
	Deflate_t77159307::get_offset_of_bl_tree_94(),
	Deflate_t77159307::get_offset_of_l_desc_95(),
	Deflate_t77159307::get_offset_of_d_desc_96(),
	Deflate_t77159307::get_offset_of_bl_desc_97(),
	Deflate_t77159307::get_offset_of_bl_count_98(),
	Deflate_t77159307::get_offset_of_heap_99(),
	Deflate_t77159307::get_offset_of_heap_len_100(),
	Deflate_t77159307::get_offset_of_heap_max_101(),
	Deflate_t77159307::get_offset_of_depth_102(),
	Deflate_t77159307::get_offset_of_l_buf_103(),
	Deflate_t77159307::get_offset_of_lit_bufsize_104(),
	Deflate_t77159307::get_offset_of_last_lit_105(),
	Deflate_t77159307::get_offset_of_d_buf_106(),
	Deflate_t77159307::get_offset_of_opt_len_107(),
	Deflate_t77159307::get_offset_of_static_len_108(),
	Deflate_t77159307::get_offset_of_matches_109(),
	Deflate_t77159307::get_offset_of_last_eob_len_110(),
	Deflate_t77159307::get_offset_of_bi_buf_111(),
	Deflate_t77159307::get_offset_of_bi_valid_112(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (Config_t1249383685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[5] = 
{
	Config_t1249383685::get_offset_of_good_length_0(),
	Config_t1249383685::get_offset_of_max_lazy_1(),
	Config_t1249383685::get_offset_of_nice_length_2(),
	Config_t1249383685::get_offset_of_max_chain_3(),
	Config_t1249383685::get_offset_of_func_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (InfBlocks_t706751929), -1, sizeof(InfBlocks_t706751929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3364[41] = 
{
	0,
	InfBlocks_t706751929_StaticFields::get_offset_of_inflate_mask_1(),
	InfBlocks_t706751929_StaticFields::get_offset_of_border_2(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfBlocks_t706751929::get_offset_of_mode_22(),
	InfBlocks_t706751929::get_offset_of_left_23(),
	InfBlocks_t706751929::get_offset_of_table_24(),
	InfBlocks_t706751929::get_offset_of_index_25(),
	InfBlocks_t706751929::get_offset_of_blens_26(),
	InfBlocks_t706751929::get_offset_of_bb_27(),
	InfBlocks_t706751929::get_offset_of_tb_28(),
	InfBlocks_t706751929::get_offset_of_codes_29(),
	InfBlocks_t706751929::get_offset_of_last_30(),
	InfBlocks_t706751929::get_offset_of_bitk_31(),
	InfBlocks_t706751929::get_offset_of_bitb_32(),
	InfBlocks_t706751929::get_offset_of_hufts_33(),
	InfBlocks_t706751929::get_offset_of_window_34(),
	InfBlocks_t706751929::get_offset_of_end_35(),
	InfBlocks_t706751929::get_offset_of_read_36(),
	InfBlocks_t706751929::get_offset_of_write_37(),
	InfBlocks_t706751929::get_offset_of_checkfn_38(),
	InfBlocks_t706751929::get_offset_of_check_39(),
	InfBlocks_t706751929::get_offset_of_inftree_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (InfCodes_t2579876053), -1, sizeof(InfCodes_t2579876053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3365[34] = 
{
	InfCodes_t2579876053_StaticFields::get_offset_of_inflate_mask_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfCodes_t2579876053::get_offset_of_mode_20(),
	InfCodes_t2579876053::get_offset_of_len_21(),
	InfCodes_t2579876053::get_offset_of_tree_22(),
	InfCodes_t2579876053::get_offset_of_tree_index_23(),
	InfCodes_t2579876053::get_offset_of_need_24(),
	InfCodes_t2579876053::get_offset_of_lit_25(),
	InfCodes_t2579876053::get_offset_of_get_26(),
	InfCodes_t2579876053::get_offset_of_dist_27(),
	InfCodes_t2579876053::get_offset_of_lbits_28(),
	InfCodes_t2579876053::get_offset_of_dbits_29(),
	InfCodes_t2579876053::get_offset_of_ltree_30(),
	InfCodes_t2579876053::get_offset_of_ltree_index_31(),
	InfCodes_t2579876053::get_offset_of_dtree_32(),
	InfCodes_t2579876053::get_offset_of_dtree_index_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (InfTree_t3604104823), -1, sizeof(InfTree_t3604104823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3366[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfTree_t3604104823_StaticFields::get_offset_of_fixed_tl_12(),
	InfTree_t3604104823_StaticFields::get_offset_of_fixed_td_13(),
	InfTree_t3604104823_StaticFields::get_offset_of_cplens_14(),
	InfTree_t3604104823_StaticFields::get_offset_of_cplext_15(),
	InfTree_t3604104823_StaticFields::get_offset_of_cpdist_16(),
	InfTree_t3604104823_StaticFields::get_offset_of_cpdext_17(),
	0,
	InfTree_t3604104823::get_offset_of_hn_19(),
	InfTree_t3604104823::get_offset_of_v_20(),
	InfTree_t3604104823::get_offset_of_c_21(),
	InfTree_t3604104823::get_offset_of_r_22(),
	InfTree_t3604104823::get_offset_of_u_23(),
	InfTree_t3604104823::get_offset_of_x_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (Inflate_t807188643), -1, sizeof(Inflate_t807188643_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3367[40] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Inflate_t807188643::get_offset_of_mode_31(),
	Inflate_t807188643::get_offset_of_method_32(),
	Inflate_t807188643::get_offset_of_was_33(),
	Inflate_t807188643::get_offset_of_need_34(),
	Inflate_t807188643::get_offset_of_marker_35(),
	Inflate_t807188643::get_offset_of_nowrap_36(),
	Inflate_t807188643::get_offset_of_wbits_37(),
	Inflate_t807188643::get_offset_of_blocks_38(),
	Inflate_t807188643_StaticFields::get_offset_of_mark_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (JZlib_t3797748839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[22] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (StaticTree_t561547724), -1, sizeof(StaticTree_t561547724_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3369[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	StaticTree_t561547724_StaticFields::get_offset_of_static_ltree_7(),
	StaticTree_t561547724_StaticFields::get_offset_of_static_dtree_8(),
	StaticTree_t561547724_StaticFields::get_offset_of_static_l_desc_9(),
	StaticTree_t561547724_StaticFields::get_offset_of_static_d_desc_10(),
	StaticTree_t561547724_StaticFields::get_offset_of_static_bl_desc_11(),
	StaticTree_t561547724::get_offset_of_static_tree_12(),
	StaticTree_t561547724::get_offset_of_extra_bits_13(),
	StaticTree_t561547724::get_offset_of_extra_base_14(),
	StaticTree_t561547724::get_offset_of_elems_15(),
	StaticTree_t561547724::get_offset_of_max_length_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (ZOutputStream_t597810407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[8] = 
{
	0,
	ZOutputStream_t597810407::get_offset_of_z_2(),
	ZOutputStream_t597810407::get_offset_of_flushLevel_3(),
	ZOutputStream_t597810407::get_offset_of_buf_4(),
	ZOutputStream_t597810407::get_offset_of_buf1_5(),
	ZOutputStream_t597810407::get_offset_of_compress_6(),
	ZOutputStream_t597810407::get_offset_of_output_7(),
	ZOutputStream_t597810407::get_offset_of_closed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (ZStream_t708755204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ZStream_t708755204::get_offset_of_next_in_17(),
	ZStream_t708755204::get_offset_of_next_in_index_18(),
	ZStream_t708755204::get_offset_of_avail_in_19(),
	ZStream_t708755204::get_offset_of_total_in_20(),
	ZStream_t708755204::get_offset_of_next_out_21(),
	ZStream_t708755204::get_offset_of_next_out_index_22(),
	ZStream_t708755204::get_offset_of_avail_out_23(),
	ZStream_t708755204::get_offset_of_total_out_24(),
	ZStream_t708755204::get_offset_of_msg_25(),
	ZStream_t708755204::get_offset_of_dstate_26(),
	ZStream_t708755204::get_offset_of_istate_27(),
	ZStream_t708755204::get_offset_of_data_type_28(),
	ZStream_t708755204::get_offset_of_adler_29(),
	ZStream_t708755204::get_offset_of__adler_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (ZTree_t4080091066), -1, sizeof(ZTree_t4080091066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3372[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ZTree_t4080091066_StaticFields::get_offset_of_extra_lbits_12(),
	ZTree_t4080091066_StaticFields::get_offset_of_extra_dbits_13(),
	ZTree_t4080091066_StaticFields::get_offset_of_extra_blbits_14(),
	ZTree_t4080091066_StaticFields::get_offset_of_bl_order_15(),
	0,
	0,
	ZTree_t4080091066_StaticFields::get_offset_of__dist_code_18(),
	ZTree_t4080091066_StaticFields::get_offset_of__length_code_19(),
	ZTree_t4080091066_StaticFields::get_offset_of_base_length_20(),
	ZTree_t4080091066_StaticFields::get_offset_of_base_dist_21(),
	ZTree_t4080091066::get_offset_of_dyn_tree_22(),
	ZTree_t4080091066::get_offset_of_max_code_23(),
	ZTree_t4080091066::get_offset_of_stat_desc_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (PemParser_t1487846991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[4] = 
{
	PemParser_t1487846991::get_offset_of__header1_0(),
	PemParser_t1487846991::get_offset_of__header2_1(),
	PemParser_t1487846991::get_offset_of__footer1_2(),
	PemParser_t1487846991::get_offset_of__footer2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (X509Certificate_t2250736941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[5] = 
{
	X509Certificate_t2250736941::get_offset_of_c_0(),
	X509Certificate_t2250736941::get_offset_of_basicConstraints_1(),
	X509Certificate_t2250736941::get_offset_of_keyUsage_2(),
	X509Certificate_t2250736941::get_offset_of_hashValueSet_3(),
	X509Certificate_t2250736941::get_offset_of_hashValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (X509CertificateParser_t3646547314), -1, sizeof(X509CertificateParser_t3646547314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3376[4] = 
{
	X509CertificateParser_t3646547314_StaticFields::get_offset_of_PemCertParser_0(),
	X509CertificateParser_t3646547314::get_offset_of_sData_1(),
	X509CertificateParser_t3646547314::get_offset_of_sDataObjectCount_2(),
	X509CertificateParser_t3646547314::get_offset_of_currentStream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (X509Crl_t2621233049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3377[4] = 
{
	X509Crl_t2621233049::get_offset_of_c_0(),
	X509Crl_t2621233049::get_offset_of_sigAlgName_1(),
	X509Crl_t2621233049::get_offset_of_sigAlgParams_2(),
	X509Crl_t2621233049::get_offset_of_isIndirect_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (X509CrlEntry_t1400823213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[4] = 
{
	X509CrlEntry_t1400823213::get_offset_of_c_0(),
	X509CrlEntry_t1400823213::get_offset_of_isIndirect_1(),
	X509CrlEntry_t1400823213::get_offset_of_previousCertificateIssuer_2(),
	X509CrlEntry_t1400823213::get_offset_of_certificateIssuer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (X509CrlParser_t447353842), -1, sizeof(X509CrlParser_t447353842_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3379[5] = 
{
	X509CrlParser_t447353842_StaticFields::get_offset_of_PemCrlParser_0(),
	X509CrlParser_t447353842::get_offset_of_lazyAsn1_1(),
	X509CrlParser_t447353842::get_offset_of_sCrlData_2(),
	X509CrlParser_t447353842::get_offset_of_sCrlDataObjectCount_3(),
	X509CrlParser_t447353842::get_offset_of_currentCrlStream_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (X509ExtensionBase_t1429324694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (X509SignatureUtilities_t179355160), -1, sizeof(X509SignatureUtilities_t179355160_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3381[1] = 
{
	X509SignatureUtilities_t179355160_StaticFields::get_offset_of_derNull_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (X509ExtensionUtilities_t3063786951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (States_t3624480836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3383[7] = 
{
	States_t3624480836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (OnGeneralEventDelegate_t2459579164), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (OnMessageDelegate_t4204746873), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (OnErrorDelegate_t676891988), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (OnRetryDelegate_t1334454456), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (OnEventDelegate_t790674770), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (OnStateChangedDelegate_t1222973807), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (EventSource_t3924127377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[14] = 
{
	EventSource_t3924127377::get_offset_of_U3CUriU3Ek__BackingField_0(),
	EventSource_t3924127377::get_offset_of__state_1(),
	EventSource_t3924127377::get_offset_of_U3CReconnectionTimeU3Ek__BackingField_2(),
	EventSource_t3924127377::get_offset_of_U3CLastEventIdU3Ek__BackingField_3(),
	EventSource_t3924127377::get_offset_of_U3CInternalRequestU3Ek__BackingField_4(),
	EventSource_t3924127377::get_offset_of_OnOpen_5(),
	EventSource_t3924127377::get_offset_of_OnMessage_6(),
	EventSource_t3924127377::get_offset_of_OnError_7(),
	EventSource_t3924127377::get_offset_of_OnRetry_8(),
	EventSource_t3924127377::get_offset_of_OnClosed_9(),
	EventSource_t3924127377::get_offset_of_OnStateChanged_10(),
	EventSource_t3924127377::get_offset_of_EventTable_11(),
	EventSource_t3924127377::get_offset_of_RetryCount_12(),
	EventSource_t3924127377::get_offset_of_RetryCalled_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (EventSourceResponse_t2287402344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[8] = 
{
	EventSourceResponse_t2287402344::get_offset_of_U3CIsClosedU3Ek__BackingField_31(),
	EventSourceResponse_t2287402344::get_offset_of_OnMessage_32(),
	EventSourceResponse_t2287402344::get_offset_of_OnClosed_33(),
	EventSourceResponse_t2287402344::get_offset_of_FrameLock_34(),
	EventSourceResponse_t2287402344::get_offset_of_LineBuffer_35(),
	EventSourceResponse_t2287402344::get_offset_of_LineBufferPos_36(),
	EventSourceResponse_t2287402344::get_offset_of_CurrentMessage_37(),
	EventSourceResponse_t2287402344::get_offset_of_CompletedMessages_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (Message_t1650395211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[4] = 
{
	Message_t1650395211::get_offset_of_U3CIdU3Ek__BackingField_0(),
	Message_t1650395211::get_offset_of_U3CEventU3Ek__BackingField_1(),
	Message_t1650395211::get_offset_of_U3CDataU3Ek__BackingField_2(),
	Message_t1650395211::get_offset_of_U3CRetryU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (OnAuthenticationSuccededDelegate_t4078916882), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (OnAuthenticationFailedDelegate_t1617501265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (OnNonHubMessageDelegate_t1922405057), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (OnConnectedDelegate_t3283761253), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (OnClosedDelegate_t587495364), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (OnErrorDelegate_t3605384424), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
