﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnStateChanged3950199048.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_OnPrepareReques3434123680.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_ProtocolVersion4229923159.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Connection2915781690.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_TransportTypes614909816.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_MessageTypes615080634.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_ConnectionStates420400692.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_RequestTypes1771348820.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_TransportStates2802087367.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodCa1474974379.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodCa3483117754.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodRe3666295392.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodFa3007711612.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_OnMethodPr4205605290.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Hubs_Hub272719679.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_JsonEncoders_Defa78830025.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_ClientM624279968.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_KeepAl3806624343.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_MultiMe207384742.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_DataMes333379451.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Method1119839878.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Result3036240250.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Failur2793948643.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Messages_Progre1081190384.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_NegotiationData3059020807.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Poll1976236230.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Post4137891006.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Serv1441151459.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_OnTr1888872800.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_Trans148904526.h"
#include "AssemblyU2DCSharp_BestHTTP_SignalR_Transports_WebS3045819522.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_TransportEvent2410498534.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketIOEventT2290781438.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketIOErrors567512470.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Error1148500814.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_SocketIOC88619200.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_SocketIOA53599143.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventDe4057040835.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventNam236102534.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventTa3321213846.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Events_EventTab356852809.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_HandshakeData1703965475.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_JsonEncoders_D1361426165.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Packet1309324146.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Packet_Payload2318637743.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Socket2716624701.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketManager3470268644.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketManager_3533834126.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_SocketOptions3023631931.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Tra1768294366.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Tra1004880669.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Pol2868132702.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Pol1181910081.h"
#include "AssemblyU2DCSharp_BestHTTP_SocketIO_Transports_Web1308710816.h"
#include "AssemblyU2DCSharp_BestHTTP_Statistics_StatisticsQu1999199910.h"
#include "AssemblyU2DCSharp_BestHTTP_Statistics_GeneralStati1610391941.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Extensions_Pe1814721980.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSoc4163283394.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSock549273869.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSoc3499449257.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketOp4018547189.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketMes730459590.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketBi3919072826.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketCl2679686585.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketEr1328789641.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketEr2599777013.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_OnWebSocketIn2035490966.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocket71448861.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocketResp3376763264.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocketStau2332119393.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_AssetBundleSam4142830892.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_AssetBundleSam1982816999.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_AssetBundleSam3058815025.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_LargeFileDownl3427736475.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_TextureDownload853801945.h"
#include "AssemblyU2DCSharp_UploadStream108391441.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_CodeBlocks753507477.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_GUIHelper1081137527.h"
#include "AssemblyU2DCSharp_BestHTTP_Examples_GUIMessageList2719866248.h"
#include "AssemblyU2DCSharp_LitJson_JsonType3145703806.h"
#include "AssemblyU2DCSharp_LitJson_JsonData269267574.h"
#include "AssemblyU2DCSharp_LitJson_OrderedDictionaryEnumera3437478891.h"
#include "AssemblyU2DCSharp_LitJson_JsonException613047007.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc408878057.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (OnStateChanged_t3950199048), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (OnPrepareRequestDelegate_t3434123680), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (ProtocolVersions_t4229923159)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3403[4] = 
{
	ProtocolVersions_t4229923159::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (Connection_t2915781690), -1, sizeof(Connection_t2915781690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3404[39] = 
{
	Connection_t2915781690_StaticFields::get_offset_of_DefaultEncoder_0(),
	Connection_t2915781690::get_offset_of_U3CUriU3Ek__BackingField_1(),
	Connection_t2915781690::get_offset_of__state_2(),
	Connection_t2915781690::get_offset_of_U3CNegotiationResultU3Ek__BackingField_3(),
	Connection_t2915781690::get_offset_of_U3CHubsU3Ek__BackingField_4(),
	Connection_t2915781690::get_offset_of_U3CTransportU3Ek__BackingField_5(),
	Connection_t2915781690::get_offset_of_U3CProtocolU3Ek__BackingField_6(),
	Connection_t2915781690::get_offset_of_additionalQueryParams_7(),
	Connection_t2915781690::get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8(),
	Connection_t2915781690::get_offset_of_U3CJsonEncoderU3Ek__BackingField_9(),
	Connection_t2915781690::get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_10(),
	Connection_t2915781690::get_offset_of_U3CPingIntervalU3Ek__BackingField_11(),
	Connection_t2915781690::get_offset_of_U3CReconnectDelayU3Ek__BackingField_12(),
	Connection_t2915781690::get_offset_of_OnConnected_13(),
	Connection_t2915781690::get_offset_of_OnClosed_14(),
	Connection_t2915781690::get_offset_of_OnError_15(),
	Connection_t2915781690::get_offset_of_OnReconnecting_16(),
	Connection_t2915781690::get_offset_of_OnReconnected_17(),
	Connection_t2915781690::get_offset_of_OnStateChanged_18(),
	Connection_t2915781690::get_offset_of_OnNonHubMessage_19(),
	Connection_t2915781690::get_offset_of_U3CRequestPreparatorU3Ek__BackingField_20(),
	Connection_t2915781690::get_offset_of_SyncRoot_21(),
	Connection_t2915781690::get_offset_of_U3CClientMessageCounterU3Ek__BackingField_22(),
	Connection_t2915781690::get_offset_of_ClientProtocols_23(),
	Connection_t2915781690::get_offset_of_RequestCounter_24(),
	Connection_t2915781690::get_offset_of_LastReceivedMessage_25(),
	Connection_t2915781690::get_offset_of_GroupsToken_26(),
	Connection_t2915781690::get_offset_of_BufferedMessages_27(),
	Connection_t2915781690::get_offset_of_LastMessageReceivedAt_28(),
	Connection_t2915781690::get_offset_of_ReconnectStartedAt_29(),
	Connection_t2915781690::get_offset_of_ReconnectDelayStartedAt_30(),
	Connection_t2915781690::get_offset_of_ReconnectStarted_31(),
	Connection_t2915781690::get_offset_of_LastPingSentAt_32(),
	Connection_t2915781690::get_offset_of_PingRequest_33(),
	Connection_t2915781690::get_offset_of_TransportConnectionStartedAt_34(),
	Connection_t2915781690::get_offset_of_queryBuilder_35(),
	Connection_t2915781690::get_offset_of_BuiltConnectionData_36(),
	Connection_t2915781690::get_offset_of_BuiltQueryParams_37(),
	Connection_t2915781690::get_offset_of_NextProtocolToTry_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (TransportTypes_t614909816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3405[4] = 
{
	TransportTypes_t614909816::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (MessageTypes_t615080634)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3406[8] = 
{
	MessageTypes_t615080634::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (ConnectionStates_t420400692)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3407[8] = 
{
	ConnectionStates_t420400692::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (RequestTypes_t1771348820)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3408[9] = 
{
	RequestTypes_t1771348820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (TransportStates_t2802087367)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3409[8] = 
{
	TransportStates_t2802087367::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (OnMethodCallDelegate_t1474974379), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (OnMethodCallCallbackDelegate_t3483117754), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (OnMethodResultDelegate_t3666295392), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (OnMethodFailedDelegate_t3007711612), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (OnMethodProgressDelegate_t4205605290), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (Hub_t272719679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[7] = 
{
	Hub_t272719679::get_offset_of_U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_0(),
	Hub_t272719679::get_offset_of_U3CNameU3Ek__BackingField_1(),
	Hub_t272719679::get_offset_of_state_2(),
	Hub_t272719679::get_offset_of_OnMethodCall_3(),
	Hub_t272719679::get_offset_of_SentMessages_4(),
	Hub_t272719679::get_offset_of_MethodTable_5(),
	Hub_t272719679::get_offset_of_builder_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (DefaultJsonEncoder_t78830025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (ClientMessage_t624279968)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3419[7] = 
{
	ClientMessage_t624279968::get_offset_of_Hub_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_Method_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_Args_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_CallIdx_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_ResultCallback_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_ResultErrorCallback_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClientMessage_t624279968::get_offset_of_ProgressCallback_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (KeepAliveMessage_t3806624343), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (MultiMessage_t207384742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[6] = 
{
	MultiMessage_t207384742::get_offset_of_U3CMessageIdU3Ek__BackingField_0(),
	MultiMessage_t207384742::get_offset_of_U3CIsInitializationU3Ek__BackingField_1(),
	MultiMessage_t207384742::get_offset_of_U3CGroupsTokenU3Ek__BackingField_2(),
	MultiMessage_t207384742::get_offset_of_U3CShouldReconnectU3Ek__BackingField_3(),
	MultiMessage_t207384742::get_offset_of_U3CPollDelayU3Ek__BackingField_4(),
	MultiMessage_t207384742::get_offset_of_U3CDataU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (DataMessage_t333379451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3424[1] = 
{
	DataMessage_t333379451::get_offset_of_U3CDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (MethodCallMessage_t1119839878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3425[4] = 
{
	MethodCallMessage_t1119839878::get_offset_of_U3CHubU3Ek__BackingField_0(),
	MethodCallMessage_t1119839878::get_offset_of_U3CMethodU3Ek__BackingField_1(),
	MethodCallMessage_t1119839878::get_offset_of_U3CArgumentsU3Ek__BackingField_2(),
	MethodCallMessage_t1119839878::get_offset_of_U3CStateU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (ResultMessage_t3036240250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3426[3] = 
{
	ResultMessage_t3036240250::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	ResultMessage_t3036240250::get_offset_of_U3CReturnValueU3Ek__BackingField_1(),
	ResultMessage_t3036240250::get_offset_of_U3CStateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (FailureMessage_t2793948643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[6] = 
{
	FailureMessage_t2793948643::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	FailureMessage_t2793948643::get_offset_of_U3CIsHubErrorU3Ek__BackingField_1(),
	FailureMessage_t2793948643::get_offset_of_U3CErrorMessageU3Ek__BackingField_2(),
	FailureMessage_t2793948643::get_offset_of_U3CAdditionalDataU3Ek__BackingField_3(),
	FailureMessage_t2793948643::get_offset_of_U3CStackTraceU3Ek__BackingField_4(),
	FailureMessage_t2793948643::get_offset_of_U3CStateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (ProgressMessage_t1081190384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3428[2] = 
{
	ProgressMessage_t1081190384::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	ProgressMessage_t1081190384::get_offset_of_U3CProgressU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (NegotiationData_t3059020807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3429[15] = 
{
	NegotiationData_t3059020807::get_offset_of_U3CUrlU3Ek__BackingField_0(),
	NegotiationData_t3059020807::get_offset_of_U3CWebSocketServerUrlU3Ek__BackingField_1(),
	NegotiationData_t3059020807::get_offset_of_U3CConnectionTokenU3Ek__BackingField_2(),
	NegotiationData_t3059020807::get_offset_of_U3CConnectionIdU3Ek__BackingField_3(),
	NegotiationData_t3059020807::get_offset_of_U3CKeepAliveTimeoutU3Ek__BackingField_4(),
	NegotiationData_t3059020807::get_offset_of_U3CDisconnectTimeoutU3Ek__BackingField_5(),
	NegotiationData_t3059020807::get_offset_of_U3CConnectionTimeoutU3Ek__BackingField_6(),
	NegotiationData_t3059020807::get_offset_of_U3CTryWebSocketsU3Ek__BackingField_7(),
	NegotiationData_t3059020807::get_offset_of_U3CProtocolVersionU3Ek__BackingField_8(),
	NegotiationData_t3059020807::get_offset_of_U3CTransportConnectTimeoutU3Ek__BackingField_9(),
	NegotiationData_t3059020807::get_offset_of_U3CLongPollDelayU3Ek__BackingField_10(),
	NegotiationData_t3059020807::get_offset_of_OnReceived_11(),
	NegotiationData_t3059020807::get_offset_of_OnError_12(),
	NegotiationData_t3059020807::get_offset_of_NegotiationRequest_13(),
	NegotiationData_t3059020807::get_offset_of_Connection_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (PollingTransport_t1976236230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3430[4] = 
{
	PollingTransport_t1976236230::get_offset_of_LastPoll_6(),
	PollingTransport_t1976236230::get_offset_of_PollDelay_7(),
	PollingTransport_t1976236230::get_offset_of_PollTimeout_8(),
	PollingTransport_t1976236230::get_offset_of_pollRequest_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (PostSendTransportBase_t4137891006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[1] = 
{
	PostSendTransportBase_t4137891006::get_offset_of_sendRequestQueue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (ServerSentEventsTransport_t1441151459), -1, sizeof(ServerSentEventsTransport_t1441151459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3432[2] = 
{
	ServerSentEventsTransport_t1441151459::get_offset_of_EventSource_6(),
	ServerSentEventsTransport_t1441151459_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (OnTransportStateChangedDelegate_t1888872800), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (TransportBase_t148904526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[5] = 
{
	0,
	TransportBase_t148904526::get_offset_of_U3CNameU3Ek__BackingField_1(),
	TransportBase_t148904526::get_offset_of_U3CConnectionU3Ek__BackingField_2(),
	TransportBase_t148904526::get_offset_of__state_3(),
	TransportBase_t148904526::get_offset_of_OnStateChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (WebSocketTransport_t3045819522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[1] = 
{
	WebSocketTransport_t3045819522::get_offset_of_wSocket_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (TransportEventTypes_t2410498534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3436[9] = 
{
	TransportEventTypes_t2410498534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (SocketIOEventTypes_t2290781438)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3437[9] = 
{
	SocketIOEventTypes_t2290781438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (SocketIOErrors_t567512470)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3438[8] = 
{
	SocketIOErrors_t567512470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (Error_t1148500814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3439[2] = 
{
	Error_t1148500814::get_offset_of_U3CCodeU3Ek__BackingField_0(),
	Error_t1148500814::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (SocketIOCallback_t88619200), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (SocketIOAckCallback_t53599143), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (EventDescriptor_t4057040835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[4] = 
{
	EventDescriptor_t4057040835::get_offset_of_U3CCallbacksU3Ek__BackingField_0(),
	EventDescriptor_t4057040835::get_offset_of_U3COnlyOnceU3Ek__BackingField_1(),
	EventDescriptor_t4057040835::get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_2(),
	EventDescriptor_t4057040835::get_offset_of_CallbackArray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (EventNames_t236102534), -1, sizeof(EventNames_t236102534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3443[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	EventNames_t236102534_StaticFields::get_offset_of_SocketIONames_7(),
	EventNames_t236102534_StaticFields::get_offset_of_TransportNames_8(),
	EventNames_t236102534_StaticFields::get_offset_of_BlacklistedEvents_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (EventTable_t3321213846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[2] = 
{
	EventTable_t3321213846::get_offset_of_U3CSocketU3Ek__BackingField_0(),
	EventTable_t3321213846::get_offset_of_Table_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (U3CRegisterU3Ec__AnonStorey0_t356852809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[2] = 
{
	U3CRegisterU3Ec__AnonStorey0_t356852809::get_offset_of_onlyOnce_0(),
	U3CRegisterU3Ec__AnonStorey0_t356852809::get_offset_of_autoDecodePayload_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (HandshakeData_t1703965475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3446[4] = 
{
	HandshakeData_t1703965475::get_offset_of_U3CSidU3Ek__BackingField_0(),
	HandshakeData_t1703965475::get_offset_of_U3CUpgradesU3Ek__BackingField_1(),
	HandshakeData_t1703965475::get_offset_of_U3CPingIntervalU3Ek__BackingField_2(),
	HandshakeData_t1703965475::get_offset_of_U3CPingTimeoutU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (DefaultJSonEncoder_t1361426165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (Packet_t1309324146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[11] = 
{
	0,
	Packet_t1309324146::get_offset_of_U3CTransportEventU3Ek__BackingField_1(),
	Packet_t1309324146::get_offset_of_U3CSocketIOEventU3Ek__BackingField_2(),
	Packet_t1309324146::get_offset_of_U3CAttachmentCountU3Ek__BackingField_3(),
	Packet_t1309324146::get_offset_of_U3CIdU3Ek__BackingField_4(),
	Packet_t1309324146::get_offset_of_U3CNamespaceU3Ek__BackingField_5(),
	Packet_t1309324146::get_offset_of_U3CPayloadU3Ek__BackingField_6(),
	Packet_t1309324146::get_offset_of_U3CEventNameU3Ek__BackingField_7(),
	Packet_t1309324146::get_offset_of_attachments_8(),
	Packet_t1309324146::get_offset_of_U3CIsDecodedU3Ek__BackingField_9(),
	Packet_t1309324146::get_offset_of_U3CDecodedArgsU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (PayloadTypes_t2318637743)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3452[3] = 
{
	PayloadTypes_t2318637743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (Socket_t2716624701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3453[7] = 
{
	Socket_t2716624701::get_offset_of_U3CManagerU3Ek__BackingField_0(),
	Socket_t2716624701::get_offset_of_U3CNamespaceU3Ek__BackingField_1(),
	Socket_t2716624701::get_offset_of_U3CIsOpenU3Ek__BackingField_2(),
	Socket_t2716624701::get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_3(),
	Socket_t2716624701::get_offset_of_AckCallbacks_4(),
	Socket_t2716624701::get_offset_of_EventCallbacks_5(),
	Socket_t2716624701::get_offset_of_arguments_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (SocketManager_t3470268644), -1, sizeof(SocketManager_t3470268644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3454[21] = 
{
	SocketManager_t3470268644_StaticFields::get_offset_of_DefaultEncoder_0(),
	0,
	SocketManager_t3470268644::get_offset_of_state_2(),
	SocketManager_t3470268644::get_offset_of_U3COptionsU3Ek__BackingField_3(),
	SocketManager_t3470268644::get_offset_of_U3CUriU3Ek__BackingField_4(),
	SocketManager_t3470268644::get_offset_of_U3CHandshakeU3Ek__BackingField_5(),
	SocketManager_t3470268644::get_offset_of_U3CTransportU3Ek__BackingField_6(),
	SocketManager_t3470268644::get_offset_of_U3CRequestCounterU3Ek__BackingField_7(),
	SocketManager_t3470268644::get_offset_of_U3CReconnectAttemptsU3Ek__BackingField_8(),
	SocketManager_t3470268644::get_offset_of_U3CEncoderU3Ek__BackingField_9(),
	SocketManager_t3470268644::get_offset_of_nextAckId_10(),
	SocketManager_t3470268644::get_offset_of_U3CPreviousStateU3Ek__BackingField_11(),
	SocketManager_t3470268644::get_offset_of_U3CUpgradingTransportU3Ek__BackingField_12(),
	SocketManager_t3470268644::get_offset_of_Namespaces_13(),
	SocketManager_t3470268644::get_offset_of_Sockets_14(),
	SocketManager_t3470268644::get_offset_of_OfflinePackets_15(),
	SocketManager_t3470268644::get_offset_of_LastHeartbeat_16(),
	SocketManager_t3470268644::get_offset_of_LastPongReceived_17(),
	SocketManager_t3470268644::get_offset_of_ReconnectAt_18(),
	SocketManager_t3470268644::get_offset_of_ConnectionStarted_19(),
	SocketManager_t3470268644::get_offset_of_closing_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (States_t3533834126)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3455[7] = 
{
	States_t3533834126::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (SocketOptions_t3023631931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3456[11] = 
{
	SocketOptions_t3023631931::get_offset_of_U3CConnectWithU3Ek__BackingField_0(),
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionU3Ek__BackingField_1(),
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_2(),
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionDelayU3Ek__BackingField_3(),
	SocketOptions_t3023631931::get_offset_of_U3CReconnectionDelayMaxU3Ek__BackingField_4(),
	SocketOptions_t3023631931::get_offset_of_randomizationFactor_5(),
	SocketOptions_t3023631931::get_offset_of_U3CTimeoutU3Ek__BackingField_6(),
	SocketOptions_t3023631931::get_offset_of_U3CAutoConnectU3Ek__BackingField_7(),
	SocketOptions_t3023631931::get_offset_of_additionalQueryParams_8(),
	SocketOptions_t3023631931::get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9(),
	SocketOptions_t3023631931::get_offset_of_BuiltQueryParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (TransportTypes_t1768294366)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3457[3] = 
{
	TransportTypes_t1768294366::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (TransportStates_t1004880669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3458[6] = 
{
	TransportStates_t1004880669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (PollingTransport_t2868132702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3460[6] = 
{
	PollingTransport_t2868132702::get_offset_of_U3CStateU3Ek__BackingField_0(),
	PollingTransport_t2868132702::get_offset_of_U3CManagerU3Ek__BackingField_1(),
	PollingTransport_t2868132702::get_offset_of_LastRequest_2(),
	PollingTransport_t2868132702::get_offset_of_PollRequest_3(),
	PollingTransport_t2868132702::get_offset_of_PacketWithAttachment_4(),
	PollingTransport_t2868132702::get_offset_of_lonelyPacketList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (PayloadTypes_t1181910081)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3461[3] = 
{
	PayloadTypes_t1181910081::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (WebSocketTransport_t1308710816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3462[5] = 
{
	WebSocketTransport_t1308710816::get_offset_of_U3CStateU3Ek__BackingField_0(),
	WebSocketTransport_t1308710816::get_offset_of_U3CManagerU3Ek__BackingField_1(),
	WebSocketTransport_t1308710816::get_offset_of_U3CImplementationU3Ek__BackingField_2(),
	WebSocketTransport_t1308710816::get_offset_of_PacketWithAttachment_3(),
	WebSocketTransport_t1308710816::get_offset_of_Buffer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (StatisticsQueryFlags_t1999199910)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3463[5] = 
{
	StatisticsQueryFlags_t1999199910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (GeneralStatistics_t1610391941)+ sizeof (Il2CppObject), sizeof(GeneralStatistics_t1610391941 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3464[10] = 
{
	GeneralStatistics_t1610391941::get_offset_of_QueryFlags_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_Connections_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_ActiveConnections_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_FreeConnections_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_RecycledConnections_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_RequestsInQueue_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CacheEntityCount_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CacheSize_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CookieCount_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GeneralStatistics_t1610391941::get_offset_of_CookieJarSize_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (PerMessageCompression_t1814721980), -1, sizeof(PerMessageCompression_t1814721980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3466[13] = 
{
	PerMessageCompression_t1814721980_StaticFields::get_offset_of_Trailer_0(),
	PerMessageCompression_t1814721980::get_offset_of_U3CClientNoContextTakeoverU3Ek__BackingField_1(),
	PerMessageCompression_t1814721980::get_offset_of_U3CServerNoContextTakeoverU3Ek__BackingField_2(),
	PerMessageCompression_t1814721980::get_offset_of_U3CClientMaxWindowBitsU3Ek__BackingField_3(),
	PerMessageCompression_t1814721980::get_offset_of_U3CServerMaxWindowBitsU3Ek__BackingField_4(),
	PerMessageCompression_t1814721980::get_offset_of_U3CLevelU3Ek__BackingField_5(),
	PerMessageCompression_t1814721980::get_offset_of_U3CMinimumDataLegthToCompressU3Ek__BackingField_6(),
	PerMessageCompression_t1814721980::get_offset_of_compressorOutputStream_7(),
	PerMessageCompression_t1814721980::get_offset_of_compressorDeflateStream_8(),
	PerMessageCompression_t1814721980::get_offset_of_decompressorInputStream_9(),
	PerMessageCompression_t1814721980::get_offset_of_decompressorOutputStream_10(),
	PerMessageCompression_t1814721980::get_offset_of_decompressorDeflateStream_11(),
	PerMessageCompression_t1814721980::get_offset_of_copyBuffer_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (WebSocketFrame_t4163283394), -1, sizeof(WebSocketFrame_t4163283394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3467[6] = 
{
	WebSocketFrame_t4163283394_StaticFields::get_offset_of_NoData_0(),
	WebSocketFrame_t4163283394::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	WebSocketFrame_t4163283394::get_offset_of_U3CIsFinalU3Ek__BackingField_2(),
	WebSocketFrame_t4163283394::get_offset_of_U3CHeaderU3Ek__BackingField_3(),
	WebSocketFrame_t4163283394::get_offset_of_U3CDataU3Ek__BackingField_4(),
	WebSocketFrame_t4163283394::get_offset_of_U3CUseExtensionsU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (WebSocketFrameReader_t549273869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[8] = 
{
	WebSocketFrameReader_t549273869::get_offset_of_U3CHeaderU3Ek__BackingField_0(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CIsFinalU3Ek__BackingField_1(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CHasMaskU3Ek__BackingField_3(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CLengthU3Ek__BackingField_4(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CMaskU3Ek__BackingField_5(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CDataU3Ek__BackingField_6(),
	WebSocketFrameReader_t549273869::get_offset_of_U3CDataAsTextU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (WebSocketFrameTypes_t3499449257)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3469[7] = 
{
	WebSocketFrameTypes_t3499449257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (OnWebSocketOpenDelegate_t4018547189), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (OnWebSocketMessageDelegate_t730459590), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (OnWebSocketBinaryDelegate_t3919072826), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (OnWebSocketClosedDelegate_t2679686585), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (OnWebSocketErrorDelegate_t1328789641), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (OnWebSocketErrorDescriptionDelegate_t2599777013), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (OnWebSocketIncompleteFrameDelegate_t2035490966), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (WebSocket_t71448861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3477[13] = 
{
	WebSocket_t71448861::get_offset_of_U3CStartPingThreadU3Ek__BackingField_0(),
	WebSocket_t71448861::get_offset_of_U3CPingFrequencyU3Ek__BackingField_1(),
	WebSocket_t71448861::get_offset_of_U3CInternalRequestU3Ek__BackingField_2(),
	WebSocket_t71448861::get_offset_of_U3CExtensionsU3Ek__BackingField_3(),
	WebSocket_t71448861::get_offset_of_OnOpen_4(),
	WebSocket_t71448861::get_offset_of_OnMessage_5(),
	WebSocket_t71448861::get_offset_of_OnBinary_6(),
	WebSocket_t71448861::get_offset_of_OnClosed_7(),
	WebSocket_t71448861::get_offset_of_OnError_8(),
	WebSocket_t71448861::get_offset_of_OnErrorDesc_9(),
	WebSocket_t71448861::get_offset_of_OnIncompleteFrame_10(),
	WebSocket_t71448861::get_offset_of_requestSent_11(),
	WebSocket_t71448861::get_offset_of_webSocket_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (WebSocketResponse_t3376763264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[19] = 
{
	WebSocketResponse_t3376763264::get_offset_of_U3CWebSocketU3Ek__BackingField_31(),
	WebSocketResponse_t3376763264::get_offset_of_OnText_32(),
	WebSocketResponse_t3376763264::get_offset_of_OnBinary_33(),
	WebSocketResponse_t3376763264::get_offset_of_OnIncompleteFrame_34(),
	WebSocketResponse_t3376763264::get_offset_of_OnClosed_35(),
	WebSocketResponse_t3376763264::get_offset_of_U3CPingFrequnecyU3Ek__BackingField_36(),
	WebSocketResponse_t3376763264::get_offset_of_U3CMaxFragmentSizeU3Ek__BackingField_37(),
	WebSocketResponse_t3376763264::get_offset_of__bufferedAmount_38(),
	WebSocketResponse_t3376763264::get_offset_of_IncompleteFrames_39(),
	WebSocketResponse_t3376763264::get_offset_of_CompletedFrames_40(),
	WebSocketResponse_t3376763264::get_offset_of_CloseFrame_41(),
	WebSocketResponse_t3376763264::get_offset_of_FrameLock_42(),
	WebSocketResponse_t3376763264::get_offset_of_SendLock_43(),
	WebSocketResponse_t3376763264::get_offset_of_unsentFrames_44(),
	WebSocketResponse_t3376763264::get_offset_of_newFrameSignal_45(),
	WebSocketResponse_t3376763264::get_offset_of_sendThreadCreated_46(),
	WebSocketResponse_t3376763264::get_offset_of_closeSent_47(),
	WebSocketResponse_t3376763264::get_offset_of_closed_48(),
	WebSocketResponse_t3376763264::get_offset_of_lastPing_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (WebSocketStausCodes_t2332119393)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3479[14] = 
{
	WebSocketStausCodes_t2332119393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (AssetBundleSample_t4142830892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3480[5] = 
{
	0,
	AssetBundleSample_t4142830892::get_offset_of_status_3(),
	AssetBundleSample_t4142830892::get_offset_of_cachedBundle_4(),
	AssetBundleSample_t4142830892::get_offset_of_texture_5(),
	AssetBundleSample_t4142830892::get_offset_of_downloading_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[7] = 
{
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U3CrequestU3E__0_0(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24locvar0_1(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U3CasyncU3E__1_2(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24this_3(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24current_4(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24disposing_5(),
	U3CDownloadAssetBundleU3Ec__Iterator0_t1982816999::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (U3CProcessAssetBundleU3Ec__Iterator1_t3058815025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3482[6] = 
{
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_bundle_0(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U3CasyncAssetU3E__0_1(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24this_2(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24current_3(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24disposing_4(),
	U3CProcessAssetBundleU3Ec__Iterator1_t3058815025::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (LargeFileDownloadSample_t3427736475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[5] = 
{
	0,
	LargeFileDownloadSample_t3427736475::get_offset_of_request_3(),
	LargeFileDownloadSample_t3427736475::get_offset_of_status_4(),
	LargeFileDownloadSample_t3427736475::get_offset_of_progress_5(),
	LargeFileDownloadSample_t3427736475::get_offset_of_fragmentSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (TextureDownloadSample_t853801945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[6] = 
{
	0,
	TextureDownloadSample_t853801945::get_offset_of_Images_3(),
	TextureDownloadSample_t853801945::get_offset_of_Textures_4(),
	TextureDownloadSample_t853801945::get_offset_of_allDownloadedFromLocalCache_5(),
	TextureDownloadSample_t853801945::get_offset_of_finishedCount_6(),
	TextureDownloadSample_t853801945::get_offset_of_scrollPos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (UploadStream_t108391441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3485[6] = 
{
	UploadStream_t108391441::get_offset_of_ReadBuffer_1(),
	UploadStream_t108391441::get_offset_of_WriteBuffer_2(),
	UploadStream_t108391441::get_offset_of_noMoreData_3(),
	UploadStream_t108391441::get_offset_of_ARE_4(),
	UploadStream_t108391441::get_offset_of_locker_5(),
	UploadStream_t108391441::get_offset_of_U3CNameU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (CodeBlocks_t753507477), -1, sizeof(CodeBlocks_t753507477_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3486[12] = 
{
	CodeBlocks_t753507477_StaticFields::get_offset_of_TextureDownloadSample_0(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_WebSocketSample_1(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_AssetBundleSample_2(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_LargeFileDownloadSample_3(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SocketIOChatSample_4(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SocketIOWePlaySample_5(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_SimpleStreamingSample_6(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_ConnectionAPISample_7(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_ConnectionStatusSample_8(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_DemoHubSample_9(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_SignalR_AuthenticationSample_10(),
	CodeBlocks_t753507477_StaticFields::get_offset_of_CacheMaintenanceSample_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (GUIHelper_t1081137527), -1, sizeof(GUIHelper_t1081137527_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3487[3] = 
{
	GUIHelper_t1081137527_StaticFields::get_offset_of_centerAlignedLabel_0(),
	GUIHelper_t1081137527_StaticFields::get_offset_of_rightAlignedLabel_1(),
	GUIHelper_t1081137527_StaticFields::get_offset_of_ClientArea_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (GUIMessageList_t2719866248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3488[2] = 
{
	GUIMessageList_t2719866248::get_offset_of_messages_0(),
	GUIMessageList_t2719866248::get_offset_of_scrollPos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (JsonType_t3145703806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3489[9] = 
{
	JsonType_t3145703806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (JsonData_t269267574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3492[10] = 
{
	JsonData_t269267574::get_offset_of_inst_array_0(),
	JsonData_t269267574::get_offset_of_inst_boolean_1(),
	JsonData_t269267574::get_offset_of_inst_double_2(),
	JsonData_t269267574::get_offset_of_inst_int_3(),
	JsonData_t269267574::get_offset_of_inst_long_4(),
	JsonData_t269267574::get_offset_of_inst_object_5(),
	JsonData_t269267574::get_offset_of_inst_string_6(),
	JsonData_t269267574::get_offset_of_json_7(),
	JsonData_t269267574::get_offset_of_type_8(),
	JsonData_t269267574::get_offset_of_object_list_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (OrderedDictionaryEnumerator_t3437478891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3493[1] = 
{
	OrderedDictionaryEnumerator_t3437478891::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (JsonException_t613047007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (PropertyMetadata_t3693826136)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3495[3] = 
{
	PropertyMetadata_t3693826136::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (ArrayMetadata_t2008834462)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3496[3] = 
{
	ArrayMetadata_t2008834462::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t2008834462::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t2008834462::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (ObjectMetadata_t3995922398)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3497[3] = 
{
	ObjectMetadata_t3995922398::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3995922398::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3995922398::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (ExporterFunc_t408878057), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
