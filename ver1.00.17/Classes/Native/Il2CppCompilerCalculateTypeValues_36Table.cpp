﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Scro2022402836.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha2375706558.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1134221400.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha2247048082.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha2445864820.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha2721813267.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enhan993038288.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1678467666.h"
#include "AssemblyU2DCSharp_EnhancedUI_EnhancedScroller_Enha1104668249.h"
#include "AssemblyU2DCSharp_exampleSceneScript2674309127.h"
#include "AssemblyU2DCSharp_PlayerPrefsUtility259601127.h"
#include "AssemblyU2DCSharp_CountryCode2172753331.h"
#include "AssemblyU2DCSharp_CountryCodeData1765037603.h"
#include "AssemblyU2DCSharp_DefaultCode3729787656.h"
#include "AssemblyU2DCSharp_DefaultCodeData2288261144.h"
#include "AssemblyU2DCSharp_Language4035666274.h"
#include "AssemblyU2DCSharp_LanguageData336865618.h"
#include "AssemblyU2DCSharp_ExposePropertyAttribute2962772241.h"
#include "AssemblyU2DCSharp_UnityQuickSheet_ConvertExt2170613788.h"
#include "AssemblyU2DCSharp_CausticDecal1220814515.h"
#include "AssemblyU2DCSharp_BoatSimulator958302968.h"
#include "AssemblyU2DCSharp_CubeGenerator3593187370.h"
#include "AssemblyU2DCSharp_DemoGUIWater2357801351.h"
#include "AssemblyU2DCSharp_FPS3691620867.h"
#include "AssemblyU2DCSharp_Buoyancy2565452554.h"
#include "AssemblyU2DCSharp_DepthFix3105587052.h"
#include "AssemblyU2DCSharp_DistortionMobileCamera1665251130.h"
#include "AssemblyU2DCSharp_DistortionMobileCamera_U3CRepeat1481196283.h"
#include "AssemblyU2DCSharp_DistortionMobileCamera_U3CRepeatC513960583.h"
#include "AssemblyU2DCSharp_DistortionMobileCamera_U3CInitia2039585614.h"
#include "AssemblyU2DCSharp_MoveCameraToCharacter626587642.h"
#include "AssemblyU2DCSharp_MoveWaterToCamera2641437726.h"
#include "AssemblyU2DCSharp_ProjectorMatrix3999868417.h"
#include "AssemblyU2DCSharp_ReflectionCamera2451359806.h"
#include "AssemblyU2DCSharp_ReflectionCamera_U3CRepeatCameraM480973731.h"
#include "AssemblyU2DCSharp_ReflectionCamera_U3CRepeatCamera1427316527.h"
#include "AssemblyU2DCSharp_RippleCreator693494862.h"
#include "AssemblyU2DCSharp_RippleCreator_ReversedRipple2344809957.h"
#include "AssemblyU2DCSharp_TextureScale2270433493.h"
#include "AssemblyU2DCSharp_TextureScale_ThreadData1483341464.h"
#include "AssemblyU2DCSharp_Underwater1731910439.h"
#include "AssemblyU2DCSharp_UnderwaterPostEffects3806762907.h"
#include "AssemblyU2DCSharp_WaterRipples656902750.h"
#include "AssemblyU2DCSharp_RSLBinStreamReader3906823767.h"
#include "AssemblyU2DCSharp_RSLTextStreamReader35157931.h"
#include "AssemblyU2DCSharp_RuntimeStlLoader3120181086.h"
#include "AssemblyU2DCSharp_RuntimeStlLoader_OnCompleteLoadUr162185122.h"
#include "AssemblyU2DCSharp_RuntimeStlLoader_EN_STL_FILE_FORM140015201.h"
#include "AssemblyU2DCSharp_RuntimeStlLoader_EN_TEXT_READ_ST3037801663.h"
#include "AssemblyU2DCSharp_RuntimeStlLoader_U3CdownloadStlU3423783099.h"
#include "AssemblyU2DCSharp_LightBeamsControlScript3229501182.h"
#include "AssemblyU2DCSharp_LookAtBehaviour4158660721.h"
#include "AssemblyU2DCSharp_LookAtCameraBehaviour2105143148.h"
#include "AssemblyU2DCSharp_RayBehavior3027150638.h"
#include "AssemblyU2DCSharp_RotateBehaviour4262005326.h"
#include "AssemblyU2DCSharp_ScrollBehaviour508640740.h"
#include "AssemblyU2DCSharp_CameraMoveController2396952510.h"
#include "AssemblyU2DCSharp_DataPreprocessor1430341971.h"
#include "AssemblyU2DCSharp_DataPreprocessor2137727781.h"
#include "AssemblyU2DCSharp_HousePreprocessor12930992782.h"
#include "AssemblyU2DCSharp_HousePreprocessor23334277309.h"
#include "AssemblyU2DCSharp_InstantateAsyncManager2235219776.h"
#include "AssemblyU2DCSharp_InstantateAsyncManager_U3CInstan2368190183.h"
#include "AssemblyU2DCSharp_MeshCreator2210653353.h"
#include "AssemblyU2DCSharp_MeshCreator23500628227.h"
#include "AssemblyU2DCSharp_MeshCreatorHouse14142519292.h"
#include "AssemblyU2DCSharp_MeshCreatorHouse1_MeshData1378722312.h"
#include "AssemblyU2DCSharp_MeshCreatorHouse24142519291.h"
#include "AssemblyU2DCSharp_MeshCreatorHouse2_MeshData1377470923.h"
#include "AssemblyU2DCSharp_MeshCreatorSTL2712395408.h"
#include "AssemblyU2DCSharp_MeshReadHouse13589368014.h"
#include "AssemblyU2DCSharp_PoolCreator3422687654.h"
#include "AssemblyU2DCSharp_STLPreprocessor935022954.h"
#include "AssemblyU2DCSharp_TEST47870864.h"
#include "AssemblyU2DCSharp_Vertex3813209450.h"
#include "AssemblyU2DCSharp_VertexComparer3600798327.h"
#include "AssemblyU2DCSharp_WaterRipples23410533264.h"
#include "AssemblyU2DCSharp_WaterRipplesHouse1929798042.h"
#include "AssemblyU2DCSharp_WaterRipplesSTL2890341929.h"
#include "AssemblyU2DCSharp_newWaterCreate2230692855.h"
#include "AssemblyU2DCSharp_newWaterCreate_U3CwaitsecU3Ec__I4033075513.h"
#include "AssemblyU2DCSharp_newWaterCreate_U3CLoadWaterU3Ec_1246754637.h"
#include "AssemblyU2DCSharp_MiniJSON_Json4020371770.h"
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser1915358011.h"
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKEN2182318091.h"
#include "AssemblyU2DCSharp_MiniJSON_Json_Serializer4088787656.h"
#include "AssemblyU2DCSharp_AIMode3831275989.h"
#include "AssemblyU2DCSharp_AIMode_U3CLeverandRollFingerU3Ec2942268378.h"
#include "AssemblyU2DCSharp_AIMode_U3CCircleSetLoopU3Ec__Ite3430727466.h"
#include "AssemblyU2DCSharp_AIMove3831276007.h"
#include "AssemblyU2DCSharp_EvasionNav3997284964.h"
#include "AssemblyU2DCSharp_FlagStop3791804538.h"
#include "AssemblyU2DCSharp_NavigateCaption2735808323.h"
#include "AssemblyU2DCSharp_Champion3225559621.h"
#include "AssemblyU2DCSharp_TCommon2231335163.h"
#include "AssemblyU2DCSharp_ApiSearchFriendManager2339757037.h"
#include "AssemblyU2DCSharp_ApiSearchHistory3591623188.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (ScrollerTweeningChangedDelegate_t2022402836), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (EnhancedScroller_t2375706558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3601[53] = 
{
	EnhancedScroller_t2375706558::get_offset_of_scrollDirection_2(),
	EnhancedScroller_t2375706558::get_offset_of_spacing_3(),
	EnhancedScroller_t2375706558::get_offset_of_padding_4(),
	EnhancedScroller_t2375706558::get_offset_of_loop_5(),
	EnhancedScroller_t2375706558::get_offset_of_scrollbarVisibility_6(),
	EnhancedScroller_t2375706558::get_offset_of_snapping_7(),
	EnhancedScroller_t2375706558::get_offset_of_snapVelocityThreshold_8(),
	EnhancedScroller_t2375706558::get_offset_of_snapWatchOffset_9(),
	EnhancedScroller_t2375706558::get_offset_of_snapJumpToOffset_10(),
	EnhancedScroller_t2375706558::get_offset_of_snapCellCenterOffset_11(),
	EnhancedScroller_t2375706558::get_offset_of_snapUseCellSpacing_12(),
	EnhancedScroller_t2375706558::get_offset_of_snapTweenType_13(),
	EnhancedScroller_t2375706558::get_offset_of_snapTweenTime_14(),
	EnhancedScroller_t2375706558::get_offset_of_cellViewVisibilityChanged_15(),
	EnhancedScroller_t2375706558::get_offset_of_cellViewWillRecycle_16(),
	EnhancedScroller_t2375706558::get_offset_of_scrollerScrolled_17(),
	EnhancedScroller_t2375706558::get_offset_of_scrollerSnapped_18(),
	EnhancedScroller_t2375706558::get_offset_of_scrollerScrollingChanged_19(),
	EnhancedScroller_t2375706558::get_offset_of_scrollerTweeningChanged_20(),
	EnhancedScroller_t2375706558::get_offset_of_U3CIsScrollingU3Ek__BackingField_21(),
	EnhancedScroller_t2375706558::get_offset_of_U3CIsTweeningU3Ek__BackingField_22(),
	EnhancedScroller_t2375706558::get_offset_of__scrollRect_23(),
	EnhancedScroller_t2375706558::get_offset_of__scrollRectTransform_24(),
	EnhancedScroller_t2375706558::get_offset_of__scrollbar_25(),
	EnhancedScroller_t2375706558::get_offset_of__container_26(),
	EnhancedScroller_t2375706558::get_offset_of__layoutGroup_27(),
	EnhancedScroller_t2375706558::get_offset_of__delegate_28(),
	EnhancedScroller_t2375706558::get_offset_of__reloadData_29(),
	EnhancedScroller_t2375706558::get_offset_of__refreshActive_30(),
	EnhancedScroller_t2375706558::get_offset_of__recycledCellViews_31(),
	EnhancedScroller_t2375706558::get_offset_of__firstPadder_32(),
	EnhancedScroller_t2375706558::get_offset_of__lastPadder_33(),
	EnhancedScroller_t2375706558::get_offset_of__recycledCellViewContainer_34(),
	EnhancedScroller_t2375706558::get_offset_of__cellViewSizeArray_35(),
	EnhancedScroller_t2375706558::get_offset_of__cellViewOffsetArray_36(),
	EnhancedScroller_t2375706558::get_offset_of__scrollPosition_37(),
	EnhancedScroller_t2375706558::get_offset_of__activeCellViews_38(),
	EnhancedScroller_t2375706558::get_offset_of__activeCellViewsStartIndex_39(),
	EnhancedScroller_t2375706558::get_offset_of__activeCellViewsEndIndex_40(),
	EnhancedScroller_t2375706558::get_offset_of__loopFirstCellIndex_41(),
	EnhancedScroller_t2375706558::get_offset_of__loopLastCellIndex_42(),
	EnhancedScroller_t2375706558::get_offset_of__loopFirstScrollPosition_43(),
	EnhancedScroller_t2375706558::get_offset_of__loopLastScrollPosition_44(),
	EnhancedScroller_t2375706558::get_offset_of__loopFirstJumpTrigger_45(),
	EnhancedScroller_t2375706558::get_offset_of__loopLastJumpTrigger_46(),
	EnhancedScroller_t2375706558::get_offset_of__lastScrollRectSize_47(),
	EnhancedScroller_t2375706558::get_offset_of__lastLoop_48(),
	EnhancedScroller_t2375706558::get_offset_of__snapCellViewIndex_49(),
	EnhancedScroller_t2375706558::get_offset_of__snapDataIndex_50(),
	EnhancedScroller_t2375706558::get_offset_of__snapJumping_51(),
	EnhancedScroller_t2375706558::get_offset_of__snapInertia_52(),
	EnhancedScroller_t2375706558::get_offset_of__lastScrollbarVisibility_53(),
	EnhancedScroller_t2375706558::get_offset_of__tweenTimeLeft_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (ScrollDirectionEnum_t1134221400)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3602[3] = 
{
	ScrollDirectionEnum_t1134221400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (CellViewPositionEnum_t2247048082)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3603[3] = 
{
	CellViewPositionEnum_t2247048082::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (ScrollbarVisibilityEnum_t2445864820)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3604[4] = 
{
	ScrollbarVisibilityEnum_t2445864820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (ListPositionEnum_t2721813267)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3605[3] = 
{
	ListPositionEnum_t2721813267::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (TweenType_t993038288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3606[34] = 
{
	TweenType_t993038288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (U3CTweenPositionU3Ec__Iterator0_t1678467666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3607[10] = 
{
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_tweenType_0(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_time_1(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_end_2(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_U3CnewPositionU3E__1_3(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_start_4(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_tweenComplete_5(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_U24this_6(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_U24current_7(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_U24disposing_8(),
	U3CTweenPositionU3Ec__Iterator0_t1678467666::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (EnhancedScrollerCellView_t1104668249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3608[4] = 
{
	EnhancedScrollerCellView_t1104668249::get_offset_of_cellIdentifier_2(),
	EnhancedScrollerCellView_t1104668249::get_offset_of_cellIndex_3(),
	EnhancedScrollerCellView_t1104668249::get_offset_of_dataIndex_4(),
	EnhancedScrollerCellView_t1104668249::get_offset_of_active_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (exampleSceneScript_t2674309127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3611[7] = 
{
	exampleSceneScript_t2674309127::get_offset_of_scale_2(),
	exampleSceneScript_t2674309127::get_offset_of_intensity_3(),
	exampleSceneScript_t2674309127::get_offset_of_alpha_4(),
	exampleSceneScript_t2674309127::get_offset_of_alphasub_5(),
	exampleSceneScript_t2674309127::get_offset_of_pow_6(),
	exampleSceneScript_t2674309127::get_offset_of_color_7(),
	exampleSceneScript_t2674309127::get_offset_of_fogMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (PlayerPrefsUtility_t259601127), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (CountryCode_t2172753331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3613[3] = 
{
	CountryCode_t2172753331::get_offset_of_SheetName_2(),
	CountryCode_t2172753331::get_offset_of_WorksheetName_3(),
	CountryCode_t2172753331::get_offset_of_dataArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (CountryCodeData_t1765037603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[1] = 
{
	CountryCodeData_t1765037603::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (DefaultCode_t3729787656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[3] = 
{
	DefaultCode_t3729787656::get_offset_of_SheetName_2(),
	DefaultCode_t3729787656::get_offset_of_WorksheetName_3(),
	DefaultCode_t3729787656::get_offset_of_dataArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (DefaultCodeData_t2288261144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[1] = 
{
	DefaultCodeData_t2288261144::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (Language_t4035666274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3617[3] = 
{
	Language_t4035666274::get_offset_of_SheetName_2(),
	Language_t4035666274::get_offset_of_WorksheetName_3(),
	Language_t4035666274::get_offset_of_dataArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (LanguageData_t336865618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3618[5] = 
{
	LanguageData_t336865618::get_offset_of_key_0(),
	LanguageData_t336865618::get_offset_of_setupjp_1(),
	LanguageData_t336865618::get_offset_of_jp_2(),
	LanguageData_t336865618::get_offset_of_setupus_3(),
	LanguageData_t336865618::get_offset_of_us_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (ExposePropertyAttribute_t2962772241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (ConvertExt_t2170613788), -1, sizeof(ConvertExt_t2170613788_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3620[13] = 
{
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_9(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_10(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_11(),
	ConvertExt_t2170613788_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (CausticDecal_t1220814515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[3] = 
{
	CausticDecal_t1220814515::get_offset_of_maxAngle_2(),
	CausticDecal_t1220814515::get_offset_of_pushDistance_3(),
	CausticDecal_t1220814515::get_offset_of_affectedLayers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (BoatSimulator_t958302968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[5] = 
{
	BoatSimulator_t958302968::get_offset_of_rigid_2(),
	BoatSimulator_t958302968::get_offset_of_keyPressedW_3(),
	BoatSimulator_t958302968::get_offset_of_keyPressedA_4(),
	BoatSimulator_t958302968::get_offset_of_keyPressedS_5(),
	BoatSimulator_t958302968::get_offset_of_keyPressedD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (CubeGenerator_t3593187370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[1] = 
{
	CubeGenerator_t3593187370::get_offset_of_cubes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (DemoGUIWater_t2357801351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[32] = 
{
	DemoGUIWater_t2357801351::get_offset_of_UpdateInterval_2(),
	DemoGUIWater_t2357801351::get_offset_of_MaxScenes_3(),
	DemoGUIWater_t2357801351::get_offset_of_IsMobileScene_4(),
	DemoGUIWater_t2357801351::get_offset_of_Sun_5(),
	DemoGUIWater_t2357801351::get_offset_of_SunTransform_6(),
	DemoGUIWater_t2357801351::get_offset_of_Boat_7(),
	DemoGUIWater_t2357801351::get_offset_of_water1_8(),
	DemoGUIWater_t2357801351::get_offset_of_water2_9(),
	DemoGUIWater_t2357801351::get_offset_of_angle_10(),
	DemoGUIWater_t2357801351::get_offset_of_canUpdateTestMaterial_11(),
	DemoGUIWater_t2357801351::get_offset_of_cam_12(),
	DemoGUIWater_t2357801351::get_offset_of_guiStyleHeader_13(),
	DemoGUIWater_t2357801351::get_offset_of_currentWaterMaterial_14(),
	DemoGUIWater_t2357801351::get_offset_of_causticMaterial_15(),
	DemoGUIWater_t2357801351::get_offset_of_currentWater_16(),
	DemoGUIWater_t2357801351::get_offset_of_transparent_17(),
	DemoGUIWater_t2357801351::get_offset_of_fadeBlend_18(),
	DemoGUIWater_t2357801351::get_offset_of_refl_19(),
	DemoGUIWater_t2357801351::get_offset_of_refraction_20(),
	DemoGUIWater_t2357801351::get_offset_of_waterWaveScaleXZ_21(),
	DemoGUIWater_t2357801351::get_offset_of_waterDirection_22(),
	DemoGUIWater_t2357801351::get_offset_of_causticDirection_23(),
	DemoGUIWater_t2357801351::get_offset_of_foamDirection_24(),
	DemoGUIWater_t2357801351::get_offset_of_ABDirection_25(),
	DemoGUIWater_t2357801351::get_offset_of_CDDirection_26(),
	DemoGUIWater_t2357801351::get_offset_of_direction_27(),
	DemoGUIWater_t2357801351::get_offset_of_reflectionColor_28(),
	DemoGUIWater_t2357801351::get_offset_of_oldCausticScale_29(),
	DemoGUIWater_t2357801351::get_offset_of_oldTextureScale_30(),
	DemoGUIWater_t2357801351::get_offset_of_oldWaveScale_31(),
	DemoGUIWater_t2357801351::get_offset_of_caustic_32(),
	DemoGUIWater_t2357801351::get_offset_of_startSunIntencity_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (FPS_t3691620867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3625[4] = 
{
	FPS_t3691620867::get_offset_of_guiStyleHeader_2(),
	FPS_t3691620867::get_offset_of_timeleft_3(),
	FPS_t3691620867::get_offset_of_fps_4(),
	FPS_t3691620867::get_offset_of_frames_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (Buoyancy_t2565452554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[13] = 
{
	Buoyancy_t2565452554::get_offset_of_Density_2(),
	Buoyancy_t2565452554::get_offset_of_SlicesPerAxis_3(),
	Buoyancy_t2565452554::get_offset_of_IsConcave_4(),
	Buoyancy_t2565452554::get_offset_of_VoxelsLimit_5(),
	Buoyancy_t2565452554::get_offset_of_WaveVelocity_6(),
	0,
	0,
	Buoyancy_t2565452554::get_offset_of_voxelHalfHeight_9(),
	Buoyancy_t2565452554::get_offset_of_localArchimedesForce_10(),
	Buoyancy_t2565452554::get_offset_of_voxels_11(),
	Buoyancy_t2565452554::get_offset_of_isMeshCollider_12(),
	Buoyancy_t2565452554::get_offset_of_forces_13(),
	Buoyancy_t2565452554::get_offset_of_waterRipples_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (DepthFix_t3105587052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (DistortionMobileCamera_t1665251130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[19] = 
{
	DistortionMobileCamera_t1665251130::get_offset_of_TextureScale_2(),
	DistortionMobileCamera_t1665251130::get_offset_of_RenderTextureFormat_3(),
	DistortionMobileCamera_t1665251130::get_offset_of_FilterMode_4(),
	DistortionMobileCamera_t1665251130::get_offset_of_CullingMask_5(),
	DistortionMobileCamera_t1665251130::get_offset_of_RenderingPath_6(),
	DistortionMobileCamera_t1665251130::get_offset_of_FPSWhenMoveCamera_7(),
	DistortionMobileCamera_t1665251130::get_offset_of_FPSWhenStaticCamera_8(),
	DistortionMobileCamera_t1665251130::get_offset_of_renderTexture_9(),
	DistortionMobileCamera_t1665251130::get_offset_of_cameraInstance_10(),
	DistortionMobileCamera_t1665251130::get_offset_of_goCamera_11(),
	DistortionMobileCamera_t1665251130::get_offset_of_oldPosition_12(),
	DistortionMobileCamera_t1665251130::get_offset_of_oldRotation_13(),
	DistortionMobileCamera_t1665251130::get_offset_of_instanceCameraTransform_14(),
	DistortionMobileCamera_t1665251130::get_offset_of_canUpdateCamera_15(),
	DistortionMobileCamera_t1665251130::get_offset_of_isStaticUpdate_16(),
	DistortionMobileCamera_t1665251130::get_offset_of_fpsMove_17(),
	DistortionMobileCamera_t1665251130::get_offset_of_fpsStatic_18(),
	0,
	DistortionMobileCamera_t1665251130::get_offset_of_frameCountWhenCameraIsStatic_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (U3CRepeatCameraMoveU3Ec__Iterator0_t1481196283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[4] = 
{
	U3CRepeatCameraMoveU3Ec__Iterator0_t1481196283::get_offset_of_U24this_0(),
	U3CRepeatCameraMoveU3Ec__Iterator0_t1481196283::get_offset_of_U24current_1(),
	U3CRepeatCameraMoveU3Ec__Iterator0_t1481196283::get_offset_of_U24disposing_2(),
	U3CRepeatCameraMoveU3Ec__Iterator0_t1481196283::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (U3CRepeatCameraStaticU3Ec__Iterator1_t513960583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[4] = 
{
	U3CRepeatCameraStaticU3Ec__Iterator1_t513960583::get_offset_of_U24this_0(),
	U3CRepeatCameraStaticU3Ec__Iterator1_t513960583::get_offset_of_U24current_1(),
	U3CRepeatCameraStaticU3Ec__Iterator1_t513960583::get_offset_of_U24disposing_2(),
	U3CRepeatCameraStaticU3Ec__Iterator1_t513960583::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (U3CInitializeU3Ec__Iterator2_t2039585614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[5] = 
{
	U3CInitializeU3Ec__Iterator2_t2039585614::get_offset_of_U3CcamU3E__0_0(),
	U3CInitializeU3Ec__Iterator2_t2039585614::get_offset_of_U24this_1(),
	U3CInitializeU3Ec__Iterator2_t2039585614::get_offset_of_U24current_2(),
	U3CInitializeU3Ec__Iterator2_t2039585614::get_offset_of_U24disposing_3(),
	U3CInitializeU3Ec__Iterator2_t2039585614::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (MoveCameraToCharacter_t626587642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[1] = 
{
	MoveCameraToCharacter_t626587642::get_offset_of_Target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (MoveWaterToCamera_t2641437726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[1] = 
{
	MoveWaterToCamera_t2641437726::get_offset_of_CurrenCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (ProjectorMatrix_t3999868417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3634[5] = 
{
	ProjectorMatrix_t3999868417::get_offset_of_GlobalMatrixNames_2(),
	ProjectorMatrix_t3999868417::get_offset_of_ProjectiveTranforms_3(),
	ProjectorMatrix_t3999868417::get_offset_of_UpdateOnStart_4(),
	ProjectorMatrix_t3999868417::get_offset_of_CanUpdate_5(),
	ProjectorMatrix_t3999868417::get_offset_of_t_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (ReflectionCamera_t2451359806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[24] = 
{
	ReflectionCamera_t2451359806::get_offset_of_m_ClipPlaneOffset_2(),
	ReflectionCamera_t2451359806::get_offset_of_CullingMask_3(),
	ReflectionCamera_t2451359806::get_offset_of_HDR_4(),
	ReflectionCamera_t2451359806::get_offset_of_OcclusionCulling_5(),
	ReflectionCamera_t2451359806::get_offset_of_TextureScale_6(),
	ReflectionCamera_t2451359806::get_offset_of_RenderTextureFormat_7(),
	ReflectionCamera_t2451359806::get_offset_of_FilterMode_8(),
	ReflectionCamera_t2451359806::get_offset_of_RenderingPath_9(),
	ReflectionCamera_t2451359806::get_offset_of_UseRealtimeUpdate_10(),
	ReflectionCamera_t2451359806::get_offset_of_FPSWhenMoveCamera_11(),
	ReflectionCamera_t2451359806::get_offset_of_FPSWhenStaticCamera_12(),
	ReflectionCamera_t2451359806::get_offset_of_renderTexture_13(),
	ReflectionCamera_t2451359806::get_offset_of_go_14(),
	ReflectionCamera_t2451359806::get_offset_of_reflectionCamera_15(),
	ReflectionCamera_t2451359806::get_offset_of_oldPosition_16(),
	ReflectionCamera_t2451359806::get_offset_of_oldRotation_17(),
	ReflectionCamera_t2451359806::get_offset_of_instanceCameraTransform_18(),
	ReflectionCamera_t2451359806::get_offset_of_frameCountWhenCameraIsStatic_19(),
	ReflectionCamera_t2451359806::get_offset_of_canUpdateCamera_20(),
	ReflectionCamera_t2451359806::get_offset_of_isStaticUpdate_21(),
	ReflectionCamera_t2451359806::get_offset_of_fpsMove_22(),
	ReflectionCamera_t2451359806::get_offset_of_fpsStatic_23(),
	0,
	ReflectionCamera_t2451359806::get_offset_of_currentCamera_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (U3CRepeatCameraMoveU3Ec__Iterator0_t480973731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3636[4] = 
{
	U3CRepeatCameraMoveU3Ec__Iterator0_t480973731::get_offset_of_U24this_0(),
	U3CRepeatCameraMoveU3Ec__Iterator0_t480973731::get_offset_of_U24current_1(),
	U3CRepeatCameraMoveU3Ec__Iterator0_t480973731::get_offset_of_U24disposing_2(),
	U3CRepeatCameraMoveU3Ec__Iterator0_t480973731::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (U3CRepeatCameraStaticU3Ec__Iterator1_t1427316527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3637[4] = 
{
	U3CRepeatCameraStaticU3Ec__Iterator1_t1427316527::get_offset_of_U24this_0(),
	U3CRepeatCameraStaticU3Ec__Iterator1_t1427316527::get_offset_of_U24current_1(),
	U3CRepeatCameraStaticU3Ec__Iterator1_t1427316527::get_offset_of_U24disposing_2(),
	U3CRepeatCameraStaticU3Ec__Iterator1_t1427316527::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (RippleCreator_t693494862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3638[22] = 
{
	RippleCreator_t693494862::get_offset_of_IsReversedRipple_2(),
	RippleCreator_t693494862::get_offset_of_RippleStrenght_3(),
	RippleCreator_t693494862::get_offset_of_MaxVelocity_4(),
	RippleCreator_t693494862::get_offset_of_RandomRipplesInterval_5(),
	RippleCreator_t693494862::get_offset_of_reversedRippleDelay_6(),
	RippleCreator_t693494862::get_offset_of_SplashEffect_7(),
	RippleCreator_t693494862::get_offset_of_SplashEffectMoved_8(),
	RippleCreator_t693494862::get_offset_of_SplashAudioSource_9(),
	RippleCreator_t693494862::get_offset_of_fadeInVelocityLimit_10(),
	RippleCreator_t693494862::get_offset_of_fadeInVelocity_11(),
	RippleCreator_t693494862::get_offset_of_waterRipples_12(),
	RippleCreator_t693494862::get_offset_of_oldPos_13(),
	RippleCreator_t693494862::get_offset_of_currentVelocity_14(),
	RippleCreator_t693494862::get_offset_of_t_15(),
	RippleCreator_t693494862::get_offset_of_reversedVelocityQueue_16(),
	RippleCreator_t693494862::get_offset_of_triggeredTime_17(),
	RippleCreator_t693494862::get_offset_of_canUpdate_18(),
	RippleCreator_t693494862::get_offset_of_randomRipplesCurrentTime_19(),
	RippleCreator_t693494862::get_offset_of_canInstantiateRandomRipple_20(),
	RippleCreator_t693494862::get_offset_of_splashMovedInstance_21(),
	RippleCreator_t693494862::get_offset_of_splashParticleSystem_22(),
	RippleCreator_t693494862::get_offset_of_splashSizeMultiplier_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (ReversedRipple_t2344809957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3639[2] = 
{
	ReversedRipple_t2344809957::get_offset_of_Position_0(),
	ReversedRipple_t2344809957::get_offset_of_Velocity_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (TextureScale_t2270433493), -1, sizeof(TextureScale_t2270433493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3640[7] = 
{
	TextureScale_t2270433493_StaticFields::get_offset_of_texColors_0(),
	TextureScale_t2270433493_StaticFields::get_offset_of_newColors_1(),
	TextureScale_t2270433493_StaticFields::get_offset_of_w_2(),
	TextureScale_t2270433493_StaticFields::get_offset_of_ratioX_3(),
	TextureScale_t2270433493_StaticFields::get_offset_of_ratioY_4(),
	TextureScale_t2270433493_StaticFields::get_offset_of_w2_5(),
	TextureScale_t2270433493_StaticFields::get_offset_of_finishCount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (ThreadData_t1483341464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[2] = 
{
	ThreadData_t1483341464::get_offset_of_start_0(),
	ThreadData_t1483341464::get_offset_of_end_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (Underwater_t1731910439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3642[10] = 
{
	Underwater_t1731910439::get_offset_of_UnderwaterLevel_2(),
	Underwater_t1731910439::get_offset_of_UnderwaterLevelx_3(),
	Underwater_t1731910439::get_offset_of_FogColor_4(),
	Underwater_t1731910439::get_offset_of_FogDensity_5(),
	Underwater_t1731910439::get_offset_of_FogMode_6(),
	Underwater_t1731910439::get_offset_of_defaultFog_7(),
	Underwater_t1731910439::get_offset_of_defaultFogColor_8(),
	Underwater_t1731910439::get_offset_of_defaultFogDensity_9(),
	Underwater_t1731910439::get_offset_of_defaultFogMod_10(),
	Underwater_t1731910439::get_offset_of_defaultSkybox_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (UnderwaterPostEffects_t3806762907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3643[8] = 
{
	UnderwaterPostEffects_t3806762907::get_offset_of_FogColor_2(),
	UnderwaterPostEffects_t3806762907::get_offset_of_FogDensity_3(),
	UnderwaterPostEffects_t3806762907::get_offset_of_UseSunShafts_4(),
	UnderwaterPostEffects_t3806762907::get_offset_of_SunShuftsIntensity_5(),
	UnderwaterPostEffects_t3806762907::get_offset_of_SunShuftsScreenBlendMode_6(),
	UnderwaterPostEffects_t3806762907::get_offset_of_SunShaftTargetPosition_7(),
	UnderwaterPostEffects_t3806762907::get_offset_of_cam_8(),
	UnderwaterPostEffects_t3806762907::get_offset_of_SunShafts_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (WaterRipples_t656902750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[35] = 
{
	WaterRipples_t656902750::get_offset_of_UpdateFPS_2(),
	WaterRipples_t656902750::get_offset_of_Multithreading_3(),
	WaterRipples_t656902750::get_offset_of_DisplacementResolution_4(),
	WaterRipples_t656902750::get_offset_of_Damping_5(),
	WaterRipples_t656902750::get_offset_of_Speed_6(),
	WaterRipples_t656902750::get_offset_of_UseSmoothWaves_7(),
	WaterRipples_t656902750::get_offset_of_UseProjectedWaves_8(),
	WaterRipples_t656902750::get_offset_of_CutOutTexture_9(),
	WaterRipples_t656902750::get_offset_of_t_10(),
	WaterRipples_t656902750::get_offset_of_textureColorMultiplier_11(),
	WaterRipples_t656902750::get_offset_of_displacementTexture_12(),
	WaterRipples_t656902750::get_offset_of_waveAcceleration_13(),
	WaterRipples_t656902750::get_offset_of_col_14(),
	WaterRipples_t656902750::get_offset_of_wavePoints_15(),
	WaterRipples_t656902750::get_offset_of_scaleBounds_16(),
	WaterRipples_t656902750::get_offset_of_inversedDamping_17(),
	WaterRipples_t656902750::get_offset_of_cutOutTextureGray_18(),
	WaterRipples_t656902750::get_offset_of_cutOutTextureInitialized_19(),
	WaterRipples_t656902750::get_offset_of_thread_20(),
	WaterRipples_t656902750::get_offset_of_canUpdate_21(),
	WaterRipples_t656902750::get_offset_of_threadDeltaTime_22(),
	WaterRipples_t656902750::get_offset_of_oldDateTime_23(),
	WaterRipples_t656902750::get_offset_of_movedObjPos_24(),
	WaterRipples_t656902750::get_offset_of_projectorPosition_25(),
	WaterRipples_t656902750::get_offset_of__GAmplitude_26(),
	WaterRipples_t656902750::get_offset_of__GFrequency_27(),
	WaterRipples_t656902750::get_offset_of__GSteepness_28(),
	WaterRipples_t656902750::get_offset_of__GSpeed_29(),
	WaterRipples_t656902750::get_offset_of__GDirectionAB_30(),
	WaterRipples_t656902750::get_offset_of__GDirectionCD_31(),
	WaterRipples_t656902750::get_offset_of_reader_32(),
	WaterRipples_t656902750::get_offset_of_waterHeight_33(),
	WaterRipples_t656902750::get_offset_of_isStarted_34(),
	WaterRipples_t656902750::get_offset_of_time_35(),
	WaterRipples_t656902750::get_offset_of_count_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (RSLBinStreamReader_t3906823767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (RSLTextStreamReader_t35157931), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (RuntimeStlLoader_t3120181086), -1, sizeof(RuntimeStlLoader_t3120181086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3647[5] = 
{
	RuntimeStlLoader_t3120181086::get_offset_of_m_vetex_limit_2(),
	RuntimeStlLoader_t3120181086::get_offset_of_m_tmp_meshes_3(),
	RuntimeStlLoader_t3120181086::get_offset_of_m_is_read_4(),
	RuntimeStlLoader_t3120181086::get_offset_of_m_text_regex_ptn_5(),
	RuntimeStlLoader_t3120181086_StaticFields::get_offset_of_m_instance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (OnCompleteLoadUrl_t162185122), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (EN_STL_FILE_FORMAT_t140015201)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3649[4] = 
{
	EN_STL_FILE_FORMAT_t140015201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (EN_TEXT_READ_STATE_t3037801663)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3650[8] = 
{
	EN_TEXT_READ_STATE_t3037801663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (U3CdownloadStlU3Ec__Iterator0_t3423783099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[11] = 
{
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_prefab_0(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_url_1(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_U3CwwwU3E__0_2(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_parent_3(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_U3CformatU3E__0_4(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_U3Cmesh_listU3E__0_5(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_func_6(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_U24this_7(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_U24current_8(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_U24disposing_9(),
	U3CdownloadStlU3Ec__Iterator0_t3423783099::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (LightBeamsControlScript_t3229501182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[16] = 
{
	LightBeamsControlScript_t3229501182::get_offset_of_SourceObject_2(),
	LightBeamsControlScript_t3229501182::get_offset_of_TargetObject_3(),
	LightBeamsControlScript_t3229501182::get_offset_of_RayPrefab_4(),
	LightBeamsControlScript_t3229501182::get_offset_of_RayColor_5(),
	LightBeamsControlScript_t3229501182::get_offset_of_PositionRange_6(),
	LightBeamsControlScript_t3229501182::get_offset_of_RadiusA_7(),
	LightBeamsControlScript_t3229501182::get_offset_of_RadiusB_8(),
	LightBeamsControlScript_t3229501182::get_offset_of_WidthA_9(),
	LightBeamsControlScript_t3229501182::get_offset_of_WidthB_10(),
	LightBeamsControlScript_t3229501182::get_offset_of_FadeSpeed_11(),
	LightBeamsControlScript_t3229501182::get_offset_of_NumRays_12(),
	LightBeamsControlScript_t3229501182::get_offset_of_Spawned_13(),
	LightBeamsControlScript_t3229501182::get_offset_of_TimeToSpawnAll_14(),
	LightBeamsControlScript_t3229501182::get_offset_of_spawnInterval_15(),
	LightBeamsControlScript_t3229501182::get_offset_of_currentCountdown_16(),
	LightBeamsControlScript_t3229501182::get_offset_of_rays_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (LookAtBehaviour_t4158660721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3653[1] = 
{
	LookAtBehaviour_t4158660721::get_offset_of_Target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (LookAtCameraBehaviour_t2105143148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (RayBehavior_t3027150638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[15] = 
{
	RayBehavior_t3027150638::get_offset_of_BeginLocation_2(),
	RayBehavior_t3027150638::get_offset_of_EndLocation_3(),
	RayBehavior_t3027150638::get_offset_of_BeginColor_4(),
	RayBehavior_t3027150638::get_offset_of_EndColor_5(),
	RayBehavior_t3027150638::get_offset_of_PositionRange_6(),
	RayBehavior_t3027150638::get_offset_of_WidthA_7(),
	RayBehavior_t3027150638::get_offset_of_WidthB_8(),
	RayBehavior_t3027150638::get_offset_of_RadiusA_9(),
	RayBehavior_t3027150638::get_offset_of_RadiusB_10(),
	RayBehavior_t3027150638::get_offset_of_Line_11(),
	RayBehavior_t3027150638::get_offset_of_Anim_12(),
	RayBehavior_t3027150638::get_offset_of_changed_13(),
	RayBehavior_t3027150638::get_offset_of_Offset_14(),
	RayBehavior_t3027150638::get_offset_of_AlphaCurve_15(),
	RayBehavior_t3027150638::get_offset_of_FadeSpeed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (RotateBehaviour_t4262005326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3656[1] = 
{
	RotateBehaviour_t4262005326::get_offset_of_RotationAmount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (ScrollBehaviour_t508640740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3657[4] = 
{
	ScrollBehaviour_t508640740::get_offset_of_materialIndex_2(),
	ScrollBehaviour_t508640740::get_offset_of_uvAnimationRate_3(),
	ScrollBehaviour_t508640740::get_offset_of_textureName_4(),
	ScrollBehaviour_t508640740::get_offset_of_uvOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (CameraMoveController_t2396952510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[8] = 
{
	CameraMoveController_t2396952510::get_offset_of_flySpeed_2(),
	CameraMoveController_t2396952510::get_offset_of_walkSpeed_3(),
	CameraMoveController_t2396952510::get_offset_of_rotateSpeed_4(),
	CameraMoveController_t2396952510::get_offset_of_characterController_5(),
	CameraMoveController_t2396952510::get_offset_of_moveHumanStartPos_6(),
	CameraMoveController_t2396952510::get_offset_of_moveParallelStartPos_7(),
	CameraMoveController_t2396952510::get_offset_of_rotateViewStartPos_8(),
	CameraMoveController_t2396952510::get_offset_of_lastInputDirection_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (DataPreprocessor_t1430341971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[1] = 
{
	DataPreprocessor_t1430341971::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (DataPreprocessor2_t137727781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[1] = 
{
	DataPreprocessor2_t137727781::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (HousePreprocessor1_t2930992782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[1] = 
{
	HousePreprocessor1_t2930992782::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (HousePreprocessor2_t3334277309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3662[1] = 
{
	HousePreprocessor2_t3334277309::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (InstantateAsyncManager_t2235219776), -1, sizeof(InstantateAsyncManager_t2235219776_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3663[1] = 
{
	InstantateAsyncManager_t2235219776_StaticFields::get_offset_of_Instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (U3CInstanceObjectsU3Ec__Iterator0_t2368190183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[9] = 
{
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_self_0(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_objects_1(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_U24locvar0_2(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_U24locvar1_3(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_U3CobjU3E__1_4(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_U3CitemU3E__2_5(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_U24current_6(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_U24disposing_7(),
	U3CInstanceObjectsU3Ec__Iterator0_t2368190183::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (MeshCreator_t2210653353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[1] = 
{
	MeshCreator_t2210653353::get_offset_of_material_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (MeshCreator2_t3500628227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[1] = 
{
	MeshCreator2_t3500628227::get_offset_of_material_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (MeshCreatorHouse1_t4142519292), -1, sizeof(MeshCreatorHouse1_t4142519292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3667[20] = 
{
	MeshCreatorHouse1_t4142519292::get_offset_of_material_2(),
	MeshCreatorHouse1_t4142519292::get_offset_of_prefab_3(),
	MeshCreatorHouse1_t4142519292::get_offset_of_parent_4(),
	MeshCreatorHouse1_t4142519292::get_offset_of_camera_5(),
	MeshCreatorHouse1_t4142519292::get_offset_of_scrollbar_6(),
	MeshCreatorHouse1_t4142519292::get_offset_of_SCT_7(),
	MeshCreatorHouse1_t4142519292::get_offset_of_ST_8(),
	MeshCreatorHouse1_t4142519292::get_offset_of_ET_9(),
	MeshCreatorHouse1_t4142519292::get_offset_of_ERT_10(),
	MeshCreatorHouse1_t4142519292_StaticFields::get_offset_of_readNum_11(),
	MeshCreatorHouse1_t4142519292_StaticFields::get_offset_of_nowNum_12(),
	MeshCreatorHouse1_t4142519292_StaticFields::get_offset_of_timeR_13(),
	MeshCreatorHouse1_t4142519292::get_offset_of_time_14(),
	MeshCreatorHouse1_t4142519292::get_offset_of_times_15(),
	MeshCreatorHouse1_t4142519292::get_offset_of_timee_16(),
	MeshCreatorHouse1_t4142519292::get_offset_of_timeStart_17(),
	MeshCreatorHouse1_t4142519292::get_offset_of_timeEnd_18(),
	MeshCreatorHouse1_t4142519292::get_offset_of_p_19(),
	MeshCreatorHouse1_t4142519292::get_offset_of_n_20(),
	MeshCreatorHouse1_t4142519292::get_offset_of_isStarted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (MeshData_t1378722312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[2] = 
{
	MeshData_t1378722312::get_offset_of_vertices_0(),
	MeshData_t1378722312::get_offset_of_triangles_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (MeshCreatorHouse2_t4142519291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[10] = 
{
	MeshCreatorHouse2_t4142519291::get_offset_of_material_2(),
	MeshCreatorHouse2_t4142519291::get_offset_of_prefab_3(),
	MeshCreatorHouse2_t4142519291::get_offset_of_parent_4(),
	MeshCreatorHouse2_t4142519291::get_offset_of_meshsp_5(),
	MeshCreatorHouse2_t4142519291::get_offset_of_p_6(),
	MeshCreatorHouse2_t4142519291::get_offset_of_timew_7(),
	MeshCreatorHouse2_t4142519291::get_offset_of_time_8(),
	MeshCreatorHouse2_t4142519291::get_offset_of_k_9(),
	MeshCreatorHouse2_t4142519291::get_offset_of_iw_10(),
	MeshCreatorHouse2_t4142519291::get_offset_of_isStarted_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (MeshData_t1377470923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3670[2] = 
{
	MeshData_t1377470923::get_offset_of_vertices_0(),
	MeshData_t1377470923::get_offset_of_triangles_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (MeshCreatorSTL_t2712395408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[1] = 
{
	MeshCreatorSTL_t2712395408::get_offset_of_material_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (MeshReadHouse1_t3589368014), -1, sizeof(MeshReadHouse1_t3589368014_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3672[25] = 
{
	MeshReadHouse1_t3589368014_StaticFields::get_offset_of_meshsp_2(),
	MeshReadHouse1_t3589368014::get_offset_of_time_3(),
	MeshReadHouse1_t3589368014_StaticFields::get_offset_of_k_4(),
	MeshReadHouse1_t3589368014::get_offset_of_a_5(),
	MeshReadHouse1_t3589368014::get_offset_of_a1_6(),
	MeshReadHouse1_t3589368014::get_offset_of_b_7(),
	MeshReadHouse1_t3589368014::get_offset_of_b1_8(),
	MeshReadHouse1_t3589368014::get_offset_of_c_9(),
	MeshReadHouse1_t3589368014::get_offset_of_c1_10(),
	MeshReadHouse1_t3589368014::get_offset_of_ds_11(),
	MeshReadHouse1_t3589368014::get_offset_of_es_12(),
	MeshReadHouse1_t3589368014::get_offset_of_numa_13(),
	MeshReadHouse1_t3589368014::get_offset_of_numOfVertex_14(),
	MeshReadHouse1_t3589368014::get_offset_of_triangles_15(),
	MeshReadHouse1_t3589368014::get_offset_of_vertices_16(),
	MeshReadHouse1_t3589368014::get_offset_of_reader_17(),
	MeshReadHouse1_t3589368014::get_offset_of_numOfTriangles_18(),
	MeshReadHouse1_t3589368014::get_offset_of_meshs_19(),
	MeshReadHouse1_t3589368014::get_offset_of_r1_20(),
	MeshReadHouse1_t3589368014::get_offset_of_r2_21(),
	MeshReadHouse1_t3589368014::get_offset_of_re_22(),
	MeshReadHouse1_t3589368014::get_offset_of_v_23(),
	MeshReadHouse1_t3589368014::get_offset_of_x_24(),
	MeshReadHouse1_t3589368014::get_offset_of_y_25(),
	MeshReadHouse1_t3589368014::get_offset_of_z_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (PoolCreator_t3422687654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[1] = 
{
	PoolCreator_t3422687654::get_offset_of_Water_TKM_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (STLPreprocessor_t935022954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[1] = 
{
	STLPreprocessor_t935022954::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (TEST_t47870864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[18] = 
{
	TEST_t47870864::get_offset_of_ttt_2(),
	TEST_t47870864::get_offset_of_obj_3(),
	TEST_t47870864::get_offset_of_Nts_4(),
	TEST_t47870864::get_offset_of_Ntmm_5(),
	TEST_t47870864::get_offset_of_Ets_6(),
	TEST_t47870864::get_offset_of_Etmm_7(),
	TEST_t47870864::get_offset_of_Ntm_8(),
	TEST_t47870864::get_offset_of_Etm_9(),
	TEST_t47870864::get_offset_of_NtM_10(),
	TEST_t47870864::get_offset_of_EtM_11(),
	TEST_t47870864::get_offset_of_ner_12(),
	TEST_t47870864::get_offset_of_time_13(),
	TEST_t47870864::get_offset_of_times_14(),
	TEST_t47870864::get_offset_of_timee_15(),
	TEST_t47870864::get_offset_of_timeStart_16(),
	TEST_t47870864::get_offset_of_timeStartt_17(),
	TEST_t47870864::get_offset_of_timeEnd_18(),
	TEST_t47870864::get_offset_of_number_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (Vertex_t3813209450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[3] = 
{
	Vertex_t3813209450::get_offset_of_x_0(),
	Vertex_t3813209450::get_offset_of_y_1(),
	Vertex_t3813209450::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (VertexComparer_t3600798327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (WaterRipples2_t3410533264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[35] = 
{
	WaterRipples2_t3410533264::get_offset_of_UpdateFPS_2(),
	WaterRipples2_t3410533264::get_offset_of_Multithreading_3(),
	WaterRipples2_t3410533264::get_offset_of_DisplacementResolution_4(),
	WaterRipples2_t3410533264::get_offset_of_Damping_5(),
	WaterRipples2_t3410533264::get_offset_of_Speed_6(),
	WaterRipples2_t3410533264::get_offset_of_UseSmoothWaves_7(),
	WaterRipples2_t3410533264::get_offset_of_UseProjectedWaves_8(),
	WaterRipples2_t3410533264::get_offset_of_CutOutTexture_9(),
	WaterRipples2_t3410533264::get_offset_of_t_10(),
	WaterRipples2_t3410533264::get_offset_of_textureColorMultiplier_11(),
	WaterRipples2_t3410533264::get_offset_of_displacementTexture_12(),
	WaterRipples2_t3410533264::get_offset_of_waveAcceleration_13(),
	WaterRipples2_t3410533264::get_offset_of_col_14(),
	WaterRipples2_t3410533264::get_offset_of_wavePoints_15(),
	WaterRipples2_t3410533264::get_offset_of_scaleBounds_16(),
	WaterRipples2_t3410533264::get_offset_of_inversedDamping_17(),
	WaterRipples2_t3410533264::get_offset_of_cutOutTextureGray_18(),
	WaterRipples2_t3410533264::get_offset_of_cutOutTextureInitialized_19(),
	WaterRipples2_t3410533264::get_offset_of_thread_20(),
	WaterRipples2_t3410533264::get_offset_of_canUpdate_21(),
	WaterRipples2_t3410533264::get_offset_of_threadDeltaTime_22(),
	WaterRipples2_t3410533264::get_offset_of_oldDateTime_23(),
	WaterRipples2_t3410533264::get_offset_of_movedObjPos_24(),
	WaterRipples2_t3410533264::get_offset_of_projectorPosition_25(),
	WaterRipples2_t3410533264::get_offset_of__GAmplitude_26(),
	WaterRipples2_t3410533264::get_offset_of__GFrequency_27(),
	WaterRipples2_t3410533264::get_offset_of__GSteepness_28(),
	WaterRipples2_t3410533264::get_offset_of__GSpeed_29(),
	WaterRipples2_t3410533264::get_offset_of__GDirectionAB_30(),
	WaterRipples2_t3410533264::get_offset_of__GDirectionCD_31(),
	WaterRipples2_t3410533264::get_offset_of_reader_32(),
	WaterRipples2_t3410533264::get_offset_of_waterHeight_33(),
	WaterRipples2_t3410533264::get_offset_of_isStarted_34(),
	WaterRipples2_t3410533264::get_offset_of_time_35(),
	WaterRipples2_t3410533264::get_offset_of_count_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (WaterRipplesHouse_t1929798042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[32] = 
{
	WaterRipplesHouse_t1929798042::get_offset_of_UpdateFPS_2(),
	WaterRipplesHouse_t1929798042::get_offset_of_Multithreading_3(),
	WaterRipplesHouse_t1929798042::get_offset_of_DisplacementResolution_4(),
	WaterRipplesHouse_t1929798042::get_offset_of_Damping_5(),
	WaterRipplesHouse_t1929798042::get_offset_of_Speed_6(),
	WaterRipplesHouse_t1929798042::get_offset_of_UseSmoothWaves_7(),
	WaterRipplesHouse_t1929798042::get_offset_of_UseProjectedWaves_8(),
	WaterRipplesHouse_t1929798042::get_offset_of_CutOutTexture_9(),
	WaterRipplesHouse_t1929798042::get_offset_of_t_10(),
	WaterRipplesHouse_t1929798042::get_offset_of_textureColorMultiplier_11(),
	WaterRipplesHouse_t1929798042::get_offset_of_displacementTexture_12(),
	WaterRipplesHouse_t1929798042::get_offset_of_waveAcceleration_13(),
	WaterRipplesHouse_t1929798042::get_offset_of_col_14(),
	WaterRipplesHouse_t1929798042::get_offset_of_wavePoints_15(),
	WaterRipplesHouse_t1929798042::get_offset_of_scaleBounds_16(),
	WaterRipplesHouse_t1929798042::get_offset_of_inversedDamping_17(),
	WaterRipplesHouse_t1929798042::get_offset_of_cutOutTextureGray_18(),
	WaterRipplesHouse_t1929798042::get_offset_of_cutOutTextureInitialized_19(),
	WaterRipplesHouse_t1929798042::get_offset_of_thread_20(),
	WaterRipplesHouse_t1929798042::get_offset_of_canUpdate_21(),
	WaterRipplesHouse_t1929798042::get_offset_of_threadDeltaTime_22(),
	WaterRipplesHouse_t1929798042::get_offset_of_oldDateTime_23(),
	WaterRipplesHouse_t1929798042::get_offset_of_movedObjPos_24(),
	WaterRipplesHouse_t1929798042::get_offset_of_projectorPosition_25(),
	WaterRipplesHouse_t1929798042::get_offset_of__GAmplitude_26(),
	WaterRipplesHouse_t1929798042::get_offset_of__GFrequency_27(),
	WaterRipplesHouse_t1929798042::get_offset_of__GSteepness_28(),
	WaterRipplesHouse_t1929798042::get_offset_of__GSpeed_29(),
	WaterRipplesHouse_t1929798042::get_offset_of__GDirectionAB_30(),
	WaterRipplesHouse_t1929798042::get_offset_of__GDirectionCD_31(),
	WaterRipplesHouse_t1929798042::get_offset_of_waterHeight_32(),
	WaterRipplesHouse_t1929798042::get_offset_of_isStarted_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (WaterRipplesSTL_t2890341929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[33] = 
{
	WaterRipplesSTL_t2890341929::get_offset_of_UpdateFPS_2(),
	WaterRipplesSTL_t2890341929::get_offset_of_Multithreading_3(),
	WaterRipplesSTL_t2890341929::get_offset_of_DisplacementResolution_4(),
	WaterRipplesSTL_t2890341929::get_offset_of_Damping_5(),
	WaterRipplesSTL_t2890341929::get_offset_of_Speed_6(),
	WaterRipplesSTL_t2890341929::get_offset_of_UseSmoothWaves_7(),
	WaterRipplesSTL_t2890341929::get_offset_of_UseProjectedWaves_8(),
	WaterRipplesSTL_t2890341929::get_offset_of_CutOutTexture_9(),
	WaterRipplesSTL_t2890341929::get_offset_of_t_10(),
	WaterRipplesSTL_t2890341929::get_offset_of_textureColorMultiplier_11(),
	WaterRipplesSTL_t2890341929::get_offset_of_displacementTexture_12(),
	WaterRipplesSTL_t2890341929::get_offset_of_waveAcceleration_13(),
	WaterRipplesSTL_t2890341929::get_offset_of_col_14(),
	WaterRipplesSTL_t2890341929::get_offset_of_wavePoints_15(),
	WaterRipplesSTL_t2890341929::get_offset_of_scaleBounds_16(),
	WaterRipplesSTL_t2890341929::get_offset_of_inversedDamping_17(),
	WaterRipplesSTL_t2890341929::get_offset_of_cutOutTextureGray_18(),
	WaterRipplesSTL_t2890341929::get_offset_of_cutOutTextureInitialized_19(),
	WaterRipplesSTL_t2890341929::get_offset_of_thread_20(),
	WaterRipplesSTL_t2890341929::get_offset_of_canUpdate_21(),
	WaterRipplesSTL_t2890341929::get_offset_of_threadDeltaTime_22(),
	WaterRipplesSTL_t2890341929::get_offset_of_oldDateTime_23(),
	WaterRipplesSTL_t2890341929::get_offset_of_movedObjPos_24(),
	WaterRipplesSTL_t2890341929::get_offset_of_projectorPosition_25(),
	WaterRipplesSTL_t2890341929::get_offset_of__GAmplitude_26(),
	WaterRipplesSTL_t2890341929::get_offset_of__GFrequency_27(),
	WaterRipplesSTL_t2890341929::get_offset_of__GSteepness_28(),
	WaterRipplesSTL_t2890341929::get_offset_of__GSpeed_29(),
	WaterRipplesSTL_t2890341929::get_offset_of__GDirectionAB_30(),
	WaterRipplesSTL_t2890341929::get_offset_of__GDirectionCD_31(),
	WaterRipplesSTL_t2890341929::get_offset_of_time_32(),
	WaterRipplesSTL_t2890341929::get_offset_of_k_33(),
	WaterRipplesSTL_t2890341929::get_offset_of_isStarted_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (newWaterCreate_t2230692855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[55] = 
{
	newWaterCreate_t2230692855::get_offset_of_ttt_2(),
	newWaterCreate_t2230692855::get_offset_of_ttn_3(),
	newWaterCreate_t2230692855::get_offset_of_SCTS_4(),
	newWaterCreate_t2230692855::get_offset_of_SCTE_5(),
	newWaterCreate_t2230692855::get_offset_of_obj_6(),
	newWaterCreate_t2230692855::get_offset_of_ttk_7(),
	newWaterCreate_t2230692855::get_offset_of_objc_8(),
	newWaterCreate_t2230692855::get_offset_of_obja_9(),
	newWaterCreate_t2230692855::get_offset_of_underwater_10(),
	newWaterCreate_t2230692855::get_offset_of_cam_11(),
	newWaterCreate_t2230692855::get_offset_of_playT_12(),
	newWaterCreate_t2230692855::get_offset_of_WaterPath_13(),
	newWaterCreate_t2230692855::get_offset_of_Nts_14(),
	newWaterCreate_t2230692855::get_offset_of_Ntmm_15(),
	newWaterCreate_t2230692855::get_offset_of_Ets_16(),
	newWaterCreate_t2230692855::get_offset_of_Etmm_17(),
	newWaterCreate_t2230692855::get_offset_of_per_18(),
	newWaterCreate_t2230692855::get_offset_of_Ntm_19(),
	newWaterCreate_t2230692855::get_offset_of_Etm_20(),
	newWaterCreate_t2230692855::get_offset_of_NtM_21(),
	newWaterCreate_t2230692855::get_offset_of_EtM_22(),
	newWaterCreate_t2230692855::get_offset_of_ner_23(),
	newWaterCreate_t2230692855::get_offset_of_sizebool_24(),
	newWaterCreate_t2230692855::get_offset_of_time_25(),
	newWaterCreate_t2230692855::get_offset_of_times_26(),
	newWaterCreate_t2230692855::get_offset_of_timee_27(),
	newWaterCreate_t2230692855::get_offset_of_timeStart_28(),
	newWaterCreate_t2230692855::get_offset_of_timeStartt_29(),
	newWaterCreate_t2230692855::get_offset_of_timeEnd_30(),
	newWaterCreate_t2230692855::get_offset_of_timesize_31(),
	newWaterCreate_t2230692855::get_offset_of_n_32(),
	newWaterCreate_t2230692855::get_offset_of_nper_33(),
	newWaterCreate_t2230692855::get_offset_of_r_34(),
	newWaterCreate_t2230692855::get_offset_of_number_35(),
	newWaterCreate_t2230692855::get_offset_of_preRead_36(),
	newWaterCreate_t2230692855::get_offset_of_pertime_37(),
	newWaterCreate_t2230692855::get_offset_of_WaterShader_38(),
	newWaterCreate_t2230692855::get_offset_of_WaterMaterial_39(),
	newWaterCreate_t2230692855::get_offset_of_TT_40(),
	newWaterCreate_t2230692855::get_offset_of_ST_41(),
	newWaterCreate_t2230692855::get_offset_of_ET_42(),
	newWaterCreate_t2230692855::get_offset_of_ERT_43(),
	newWaterCreate_t2230692855::get_offset_of_meshSize_44(),
	newWaterCreate_t2230692855::get_offset_of_readbefore_45(),
	newWaterCreate_t2230692855::get_offset_of_watercam_46(),
	newWaterCreate_t2230692855::get_offset_of_meshxobjc_47(),
	newWaterCreate_t2230692855::get_offset_of_meshxmain_48(),
	newWaterCreate_t2230692855::get_offset_of_waterreturn_49(),
	newWaterCreate_t2230692855::get_offset_of_isplay_50(),
	newWaterCreate_t2230692855::get_offset_of_err_51(),
	newWaterCreate_t2230692855::get_offset_of_k_52(),
	newWaterCreate_t2230692855::get_offset_of_ln_53(),
	newWaterCreate_t2230692855::get_offset_of_rn_54(),
	newWaterCreate_t2230692855::get_offset_of_nk_55(),
	newWaterCreate_t2230692855::get_offset_of_readT_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (U3CwaitsecU3Ec__Iterator0_t4033075513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[3] = 
{
	U3CwaitsecU3Ec__Iterator0_t4033075513::get_offset_of_U24current_0(),
	U3CwaitsecU3Ec__Iterator0_t4033075513::get_offset_of_U24disposing_1(),
	U3CwaitsecU3Ec__Iterator0_t4033075513::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (U3CLoadWaterU3Ec__Iterator1_t1246754637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3683[7] = 
{
	U3CLoadWaterU3Ec__Iterator1_t1246754637::get_offset_of_U3CfilenameU3E__0_0(),
	U3CLoadWaterU3Ec__Iterator1_t1246754637::get_offset_of_n_1(),
	U3CLoadWaterU3Ec__Iterator1_t1246754637::get_offset_of_U3CrequestU3E__0_2(),
	U3CLoadWaterU3Ec__Iterator1_t1246754637::get_offset_of_U24this_3(),
	U3CLoadWaterU3Ec__Iterator1_t1246754637::get_offset_of_U24current_4(),
	U3CLoadWaterU3Ec__Iterator1_t1246754637::get_offset_of_U24disposing_5(),
	U3CLoadWaterU3Ec__Iterator1_t1246754637::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (Json_t4020371770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (Parser_t1915358011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3685[2] = 
{
	0,
	Parser_t1915358011::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (TOKEN_t2182318091)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3686[13] = 
{
	TOKEN_t2182318091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (Serializer_t4088787656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[1] = 
{
	Serializer_t4088787656::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (AIMode_t3831275989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[13] = 
{
	AIMode_t3831275989::get_offset_of_m_Planet_3(),
	AIMode_t3831275989::get_offset_of_m_gCore_4(),
	AIMode_t3831275989::get_offset_of_AIButton_5(),
	AIMode_t3831275989::get_offset_of_AIFlagMode_6(),
	AIMode_t3831275989::get_offset_of_OnAIMode_7(),
	AIMode_t3831275989::get_offset_of_AIFlagDrop_8(),
	AIMode_t3831275989::get_offset_of_GoalFlag_9(),
	AIMode_t3831275989::get_offset_of_FlagDirectControl_10(),
	AIMode_t3831275989::get_offset_of_RollControl_11(),
	AIMode_t3831275989::get_offset_of_FingerRectTran_12(),
	AIMode_t3831275989::get_offset_of_CircleRectTran_13(),
	AIMode_t3831275989::get_offset_of_fingerTw_14(),
	AIMode_t3831275989::get_offset_of_circleTw_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (U3CLeverandRollFingerU3Ec__Iterator0_t2942268378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[4] = 
{
	U3CLeverandRollFingerU3Ec__Iterator0_t2942268378::get_offset_of_U24this_0(),
	U3CLeverandRollFingerU3Ec__Iterator0_t2942268378::get_offset_of_U24current_1(),
	U3CLeverandRollFingerU3Ec__Iterator0_t2942268378::get_offset_of_U24disposing_2(),
	U3CLeverandRollFingerU3Ec__Iterator0_t2942268378::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (U3CCircleSetLoopU3Ec__Iterator1_t3430727466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3690[6] = 
{
	U3CCircleSetLoopU3Ec__Iterator1_t3430727466::get_offset_of_target_0(),
	U3CCircleSetLoopU3Ec__Iterator1_t3430727466::get_offset_of_cam_1(),
	U3CCircleSetLoopU3Ec__Iterator1_t3430727466::get_offset_of_U24this_2(),
	U3CCircleSetLoopU3Ec__Iterator1_t3430727466::get_offset_of_U24current_3(),
	U3CCircleSetLoopU3Ec__Iterator1_t3430727466::get_offset_of_U24disposing_4(),
	U3CCircleSetLoopU3Ec__Iterator1_t3430727466::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (AIMove_t3831276007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[19] = 
{
	AIMove_t3831276007::get_offset_of_DesertMoveSpeed_2(),
	AIMove_t3831276007::get_offset_of_IceMoveSpeed_3(),
	AIMove_t3831276007::get_offset_of_FlagMoveSpeed_4(),
	AIMove_t3831276007::get_offset_of_DefultMoveSpeed_5(),
	AIMove_t3831276007::get_offset_of_m_fMoveSpeedAI_6(),
	AIMove_t3831276007::get_offset_of_m_fAzimuth_7(),
	AIMove_t3831276007::get_offset_of_m_sBtnStore_8(),
	AIMove_t3831276007::get_offset_of_m_sBSCount_9(),
	AIMove_t3831276007::get_offset_of_m_bMoveCorutine_10(),
	AIMove_t3831276007::get_offset_of_m_fObjRigid_11(),
	AIMove_t3831276007::get_offset_of_m_NavObj_12(),
	AIMove_t3831276007::get_offset_of_m_MoveDirect_13(),
	AIMove_t3831276007::get_offset_of_m_CheckObj_14(),
	AIMove_t3831276007::get_offset_of_m_Mobilmo_15(),
	AIMove_t3831276007::get_offset_of_m_fMoveValueX_16(),
	AIMove_t3831276007::get_offset_of_m_fMoveValueY_17(),
	AIMove_t3831276007::get_offset_of_m_RandomAnim_18(),
	AIMove_t3831276007::get_offset_of_m_iCountAIMove_19(),
	AIMove_t3831276007::get_offset_of_m_Anim_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (EvasionNav_t3997284964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[1] = 
{
	EvasionNav_t3997284964::get_offset_of_NavAngle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (FlagStop_t3791804538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3693[3] = 
{
	FlagStop_t3791804538::get_offset_of_PlayerObj_2(),
	FlagStop_t3791804538::get_offset_of_planet_3(),
	FlagStop_t3791804538::get_offset_of_accelerationScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (NavigateCaption_t2735808323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[1] = 
{
	NavigateCaption_t2735808323::get_offset_of_Caption_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (Champion_t3225559621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[1] = 
{
	Champion_t3225559621::get_offset_of_test_Champion_Json_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (TCommon_t2231335163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3697[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (ApiSearchFriendManager_t2339757037), -1, sizeof(ApiSearchFriendManager_t2339757037_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3698[5] = 
{
	ApiSearchFriendManager_t2339757037_StaticFields::get_offset_of_OnGetDataUserEvent_3(),
	ApiSearchFriendManager_t2339757037_StaticFields::get_offset_of_OnAnalizeDataEvent_4(),
	ApiSearchFriendManager_t2339757037_StaticFields::get_offset_of_OnNoResultEvent_5(),
	ApiSearchFriendManager_t2339757037_StaticFields::get_offset_of_ShowHistoryTextEvent_6(),
	ApiSearchFriendManager_t2339757037::get_offset_of_isReachedResult_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (ApiSearchHistory_t3591623188), -1, sizeof(ApiSearchHistory_t3591623188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3699[2] = 
{
	ApiSearchHistory_t3591623188_StaticFields::get_offset_of_OnGetDataUser_3(),
	ApiSearchHistory_t3591623188::get_offset_of_userId_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
