﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ApiShowSearchHistory2915241013.h"
#include "AssemblyU2DCSharp_APIManager2595496257.h"
#include "AssemblyU2DCSharp_ApiAuthenticationsManager1221451852.h"
#include "AssemblyU2DCSharp_ApiAuthenticationsManager_GetAuth993482173.h"
#include "AssemblyU2DCSharp_ApiChampionManager192998666.h"
#include "AssemblyU2DCSharp_ApiChampionManager_U3CRecreateCha450119550.h"
#include "AssemblyU2DCSharp_ApiComponentManager1194997966.h"
#include "AssemblyU2DCSharp_ApiContactManager3961479189.h"
#include "AssemblyU2DCSharp_ApiContactManager_GetContactJson2261725530.h"
#include "AssemblyU2DCSharp_ApiDnaManager2705918952.h"
#include "AssemblyU2DCSharp_ApiEventManager2080410209.h"
#include "AssemblyU2DCSharp_ApiEventManager_GetEncountJson1749213704.h"
#include "AssemblyU2DCSharp_ApiEventManager_GetEventhistoryJ3523655858.h"
#include "AssemblyU2DCSharp_ApiMobilityManager1284346316.h"
#include "AssemblyU2DCSharp_ApiMobilityManager_GetChildParts2416430853.h"
#include "AssemblyU2DCSharp_ApiMobilityManager_GetMobilityDe1883011669.h"
#include "AssemblyU2DCSharp_ApiMobilityManager_GetMobilityUp3216313957.h"
#include "AssemblyU2DCSharp_ApiMobilityManager_GetMobilityPr1651198200.h"
#include "AssemblyU2DCSharp_ApiModuleManager3939700127.h"
#include "AssemblyU2DCSharp_ApiModuleManager_GetModuleChildP3198568368.h"
#include "AssemblyU2DCSharp_ApiModuleManager_GetModuleDelJson837825929.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager2960242393.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager_GetModule3026021381.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager_GetModuleM125704211.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager_GetModule1121789947.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager_U3COnGetM2120637661.h"
#include "AssemblyU2DCSharp_ApiModuleMotionManager_U3Cmodule1418277765.h"
#include "AssemblyU2DCSharp_ApiNearmobManager3144099613.h"
#include "AssemblyU2DCSharp_ApiNoticeManager3224317815.h"
#include "AssemblyU2DCSharp_ApiNoticeManager_GetNoticeDetail155866005.h"
#include "AssemblyU2DCSharp_ApiPrizedManager4212047241.h"
#include "AssemblyU2DCSharp_ApiPrizedManager_GetPrizedJson3849689266.h"
#include "AssemblyU2DCSharp_ApiPrizedManager_GetEventhistory4277162862.h"
#include "AssemblyU2DCSharp_ApiRaderManager2058631757.h"
#include "AssemblyU2DCSharp_ApiRaderManager_U3CWaitCreateRad2734378261.h"
#include "AssemblyU2DCSharp_ApiReactionManager2857272874.h"
#include "AssemblyU2DCSharp_ApiReactionManager_GetReactionLi3285480050.h"
#include "AssemblyU2DCSharp_ApiTokenManager4213246208.h"
#include "AssemblyU2DCSharp_ApiTokenManager_GetTokenNewJson2848869592.h"
#include "AssemblyU2DCSharp_ApiUserManager2556825810.h"
#include "AssemblyU2DCSharp_ApiUserManager_GetUserNewJson1716347878.h"
#include "AssemblyU2DCSharp_ApiUserManager_GetUserUpdateJson2263613893.h"
#include "AssemblyU2DCSharp_ApiUserManager_CountryData1135367575.h"
#include "AssemblyU2DCSharp_ApiMigrate2586426963.h"
#include "AssemblyU2DCSharp_MotionManager3855993783.h"
#include "AssemblyU2DCSharp_MotionManager_SerializationCopy3211118703.h"
#include "AssemblyU2DCSharp_MotionManager_GetItemsData1181624410.h"
#include "AssemblyU2DCSharp_MotionManager_GetMotionsData2417923611.h"
#include "AssemblyU2DCSharp_MotionManager_GetPoseData2785975099.h"
#include "AssemblyU2DCSharp_MotionManager_GetItemsDataInWorl1654137477.h"
#include "AssemblyU2DCSharp_MotionManager_GetMotionsDataInWo2072611844.h"
#include "AssemblyU2DCSharp_MotionManager_GetPoseDataInWorld1846296182.h"
#include "AssemblyU2DCSharp_MotionManager_U3CchangeMotionU3Ec472664859.h"
#include "AssemblyU2DCSharp_ContactSetting3049199920.h"
#include "AssemblyU2DCSharp_MyPageMobilmoItem346175087.h"
#include "AssemblyU2DCSharp_ReactionList1054868761.h"
#include "AssemblyU2DCSharp_MyPageManager2103198340.h"
#include "AssemblyU2DCSharp_MyPageManager_U3CMobilityToWorld3575254946.h"
#include "AssemblyU2DCSharp_MyPageManager_U3C_LoopNextFloorU2772912709.h"
#include "AssemblyU2DCSharp_MyPageManager_U3C_LoopPrevFloorU2980329754.h"
#include "AssemblyU2DCSharp_RecordingManager3075979958.h"
#include "AssemblyU2DCSharp_RecordingManager_GetItemsData174665839.h"
#include "AssemblyU2DCSharp_RecordingManager_GetRecordingNew4070061385.h"
#include "AssemblyU2DCSharp_RecordingManager_CopyModuleData4206646110.h"
#include "AssemblyU2DCSharp_RecordingManager_CopyRecModuleDa1848073189.h"
#include "AssemblyU2DCSharp_RecordingManager_recordingMotion1704213756.h"
#include "AssemblyU2DCSharp_PlayerInfoLike852303790.h"
#include "AssemblyU2DCSharp_ReactionHistory2538361761.h"
#include "AssemblyU2DCSharp_GetChampionItemModel353148757.h"
#include "AssemblyU2DCSharp_ChampionItemModel975870399.h"
#include "AssemblyU2DCSharp_ChampionMobilityModel1041611931.h"
#include "AssemblyU2DCSharp_GetChampionMobilityReactionModel2714706038.h"
#include "AssemblyU2DCSharp_GetChampionMobilityReactionsModel283662167.h"
#include "AssemblyU2DCSharp_GetChampionMobilityEncountListMo3804924867.h"
#include "AssemblyU2DCSharp_GetChampionMobilityRegisterdMoti1560983880.h"
#include "AssemblyU2DCSharp_GetChampionMobilityRegisterMotio2014295608.h"
#include "AssemblyU2DCSharp_GetChampionMobilityRegisterMotio3691775360.h"
#include "AssemblyU2DCSharp_GetChampionMobilityRecordedMotio1893561664.h"
#include "AssemblyU2DCSharp_GetChampionMobilityRecordedMotio3064563391.h"
#include "AssemblyU2DCSharp_GetChampionMobilityChildPartsLis1181557729.h"
#include "AssemblyU2DCSharp_GetComponentBaseModel567317181.h"
#include "AssemblyU2DCSharp_GetComponentModel1600826744.h"
#include "AssemblyU2DCSharp_GetComponentListModel2407555168.h"
#include "AssemblyU2DCSharp_GetDnaModel3440451250.h"
#include "AssemblyU2DCSharp_GetDnaChildrenListModel2978945125.h"
#include "AssemblyU2DCSharp_ErrorModel3145739141.h"
#include "AssemblyU2DCSharp_EventModel594587933.h"
#include "AssemblyU2DCSharp_GetFeedModel607373445.h"
#include "AssemblyU2DCSharp_GetReactionModel3327617864.h"
#include "AssemblyU2DCSharp_JsonObject1432680743.h"
#include "AssemblyU2DCSharp_GetMigrateCodeModel2576627179.h"
#include "AssemblyU2DCSharp_GetMobilityBaseList1712726784.h"
#include "AssemblyU2DCSharp_GetMobilityModel1064189486.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (ApiShowSearchHistory_t2915241013), -1, sizeof(ApiShowSearchHistory_t2915241013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3700[3] = 
{
	ApiShowSearchHistory_t2915241013_StaticFields::get_offset_of_OnGetDataUser_3(),
	ApiShowSearchHistory_t2915241013_StaticFields::get_offset_of_ShowHistoryText_4(),
	ApiShowSearchHistory_t2915241013::get_offset_of_userId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (APIManager_t2595496257), -1, sizeof(APIManager_t2595496257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3701[16] = 
{
	APIManager_t2595496257::get_offset_of_OFFLINE_3(),
	APIManager_t2595496257::get_offset_of_uuidName_4(),
	APIManager_t2595496257::get_offset_of_contentTypeName_5(),
	APIManager_t2595496257::get_offset_of_userAgentName_6(),
	APIManager_t2595496257::get_offset_of_authkeyName_7(),
	APIManager_t2595496257::get_offset_of_uuid_8(),
	APIManager_t2595496257::get_offset_of_contentType_9(),
	APIManager_t2595496257::get_offset_of_userAgent_10(),
	APIManager_t2595496257::get_offset_of_authkey_11(),
	APIManager_t2595496257::get_offset_of__titleLogin_12(),
	APIManager_t2595496257::get_offset_of_isNotReachable_13(),
	APIManager_t2595496257::get_offset_of_m_VersionTextAsset_14(),
	APIManager_t2595496257::get_offset_of_m_VersionString_15(),
	APIManager_t2595496257::get_offset_of_m_DeviceModel_16(),
	APIManager_t2595496257_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
	APIManager_t2595496257_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (ApiAuthenticationsManager_t1221451852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[1] = 
{
	ApiAuthenticationsManager_t1221451852::get_offset_of_authenticationJson_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (GetAutheicationJson_t993482173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[1] = 
{
	GetAutheicationJson_t993482173::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (ApiChampionManager_t192998666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[32] = 
{
	ApiChampionManager_t192998666::get_offset_of_championJson_3(),
	ApiChampionManager_t192998666::get_offset_of_m_CreObj_4(),
	ApiChampionManager_t192998666::get_offset_of_corePastsChildTra_5(),
	ApiChampionManager_t192998666::get_offset_of_childDrillerPositionX_6(),
	ApiChampionManager_t192998666::get_offset_of_childDrillerPositionY_7(),
	ApiChampionManager_t192998666::get_offset_of_childDrillerPositionZ_8(),
	ApiChampionManager_t192998666::get_offset_of_childCenterRotationX_9(),
	ApiChampionManager_t192998666::get_offset_of_childCenterRotationY_10(),
	ApiChampionManager_t192998666::get_offset_of_childCenterRotationZ_11(),
	ApiChampionManager_t192998666::get_offset_of_childPartsPositionX_12(),
	ApiChampionManager_t192998666::get_offset_of_childPartsPositionY_13(),
	ApiChampionManager_t192998666::get_offset_of_childPartsPositionZ_14(),
	ApiChampionManager_t192998666::get_offset_of_childPartsRotationX_15(),
	ApiChampionManager_t192998666::get_offset_of_childPartsRotationY_16(),
	ApiChampionManager_t192998666::get_offset_of_childPartsRotationZ_17(),
	ApiChampionManager_t192998666::get_offset_of_childPartsScaleX_18(),
	ApiChampionManager_t192998666::get_offset_of_childPartsScaleY_19(),
	ApiChampionManager_t192998666::get_offset_of_childPartsScaleZ_20(),
	ApiChampionManager_t192998666::get_offset_of_parentPartsJointNo_21(),
	ApiChampionManager_t192998666::get_offset_of_childPartsJointNo_22(),
	ApiChampionManager_t192998666::get_offset_of_mobilityObject_23(),
	ApiChampionManager_t192998666::get_offset_of_rollObj_24(),
	ApiChampionManager_t192998666::get_offset_of_contentRanking_25(),
	ApiChampionManager_t192998666::get_offset_of__mobilityPrefab_26(),
	ApiChampionManager_t192998666::get_offset_of_rankingObjList_27(),
	ApiChampionManager_t192998666::get_offset_of_mobilityCnt_28(),
	ApiChampionManager_t192998666::get_offset_of_mobilmo_29(),
	ApiChampionManager_t192998666::get_offset_of_childPartsObj_30(),
	ApiChampionManager_t192998666::get_offset_of_ChildCenter_31(),
	ApiChampionManager_t192998666::get_offset_of_ChildLeap_32(),
	ApiChampionManager_t192998666::get_offset_of_recreatedModuleObj_33(),
	ApiChampionManager_t192998666::get_offset_of_ModuleCenter_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550), -1, sizeof(U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3705[40] = 
{
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_json_0(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3C_getResultModelU3E__0_2(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3C_getChampionItemModelU3E__0_3(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3C_championItemModelU3E__0_4(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3C_championMobilityModelU3E__0_5(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3C_getChampionMobilityEncountListModelU3E__0_6(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24locvar0_7(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CitemModelU3E__1_8(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CmobilmoRigidU3E__2_9(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3Cm_mobilimoU3E__2_10(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CmobilFixedU3E__2_11(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CChildObjU3E__2_12(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CChildObjRigidU3E__2_13(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CChildObjFixedU3E__2_14(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3Cm_CreObjU3E__2_15(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24locvar1_16(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CchildU3E__3_17(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CModuleLeapU3E__4_18(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CmoduleMgrU3E__4_19(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CmoduleJsonU3E__4_20(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CparentPartsJointNoU3E__4_21(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CmodulePartsCntU3E__4_22(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CpartsIdIndexU3E__4_23(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CpartsIdListU3E__4_24(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CmodulePartsListU3E__4_25(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24locvar4_26(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24locvar5_27(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24locvar6_28(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24locvar7_29(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CrankMobilityU3E__2_30(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CuiTextU3E__2_31(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CrankNoU3E__2_32(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CresEmblemStU3E__2_33(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U3CjointParentsU3E__2_34(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24this_35(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24current_36(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24disposing_37(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550::get_offset_of_U24PC_38(),
	U3CRecreateChampionsMobilityU3Ec__Iterator0_t450119550_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (ApiComponentManager_t1194997966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (ApiContactManager_t3961479189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[4] = 
{
	ApiContactManager_t3961479189::get_offset_of_contactJson_3(),
	ApiContactManager_t3961479189::get_offset_of_SendToToast_4(),
	ApiContactManager_t3961479189::get_offset_of_isSended_5(),
	ApiContactManager_t3961479189::get_offset_of_m_contactPanel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (GetContactJson_t2261725530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3708[3] = 
{
	GetContactJson_t2261725530::get_offset_of_email_0(),
	GetContactJson_t2261725530::get_offset_of_nickName_1(),
	GetContactJson_t2261725530::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (ApiDnaManager_t2705918952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3709[12] = 
{
	ApiDnaManager_t2705918952::get_offset_of_test_dna_json_3(),
	ApiDnaManager_t2705918952::get_offset_of__dnaChildrenListData_4(),
	ApiDnaManager_t2705918952::get_offset_of__parentMobilityTransform_5(),
	ApiDnaManager_t2705918952::get_offset_of__myMobilityTransform_6(),
	ApiDnaManager_t2705918952::get_offset_of_contentChild_7(),
	ApiDnaManager_t2705918952::get_offset_of__mobilityPrefab_8(),
	ApiDnaManager_t2705918952::get_offset_of_childItemObjList_9(),
	ApiDnaManager_t2705918952::get_offset_of_myMobility_10(),
	ApiDnaManager_t2705918952::get_offset_of_mobilityParent_11(),
	ApiDnaManager_t2705918952::get_offset_of_dnaMobilmoObjList_12(),
	ApiDnaManager_t2705918952::get_offset_of_m_mobilmoDnaController_13(),
	ApiDnaManager_t2705918952::get_offset_of_dnaPanelText_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (ApiEventManager_t2080410209), -1, sizeof(ApiEventManager_t2080410209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3710[6] = 
{
	ApiEventManager_t2080410209::get_offset_of_encountJson_3(),
	ApiEventManager_t2080410209::get_offset_of_eventhistroyJson_4(),
	ApiEventManager_t2080410209::get_offset_of_rank_5(),
	ApiEventManager_t2080410209::get_offset_of_rankupPoint_6(),
	ApiEventManager_t2080410209::get_offset_of_prizedMasterId_7(),
	ApiEventManager_t2080410209_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (GetEncountJson_t1749213704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3711[2] = 
{
	GetEncountJson_t1749213704::get_offset_of_userId_0(),
	GetEncountJson_t1749213704::get_offset_of_encountMobilityId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (GetEventhistoryJson_t3523655858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3712[5] = 
{
	GetEventhistoryJson_t3523655858::get_offset_of_mobilityId_0(),
	GetEventhistoryJson_t3523655858::get_offset_of_point_1(),
	GetEventhistoryJson_t3523655858::get_offset_of_time_2(),
	GetEventhistoryJson_t3523655858::get_offset_of_score_3(),
	GetEventhistoryJson_t3523655858::get_offset_of_eventId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (ApiMobilityManager_t1284346316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3713[13] = 
{
	ApiMobilityManager_t1284346316::get_offset_of_mobilityDelJson_3(),
	ApiMobilityManager_t1284346316::get_offset_of_mobilityNewJson_4(),
	ApiMobilityManager_t1284346316::get_offset_of_mobilityUpdateJson_5(),
	ApiMobilityManager_t1284346316::get_offset_of_mobilityPrizeJson_6(),
	ApiMobilityManager_t1284346316::get_offset_of_myMobilityJson_7(),
	ApiMobilityManager_t1284346316::get_offset_of_createNewMobilityId_8(),
	ApiMobilityManager_t1284346316::get_offset_of_mobilityName_9(),
	ApiMobilityManager_t1284346316::get_offset_of_getAction_10(),
	ApiMobilityManager_t1284346316::get_offset_of_getUser_11(),
	ApiMobilityManager_t1284346316::get_offset_of_corePartsColorId_12(),
	ApiMobilityManager_t1284346316::get_offset_of_newChildPartslist_13(),
	ApiMobilityManager_t1284346316::get_offset_of_newMobilityPartsList_14(),
	ApiMobilityManager_t1284346316::get_offset_of__mobilmoValues_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (GetChildPartsList_t2416430853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[38] = 
{
	GetChildPartsList_t2416430853::get_offset_of_partsType_0(),
	GetChildPartsList_t2416430853::get_offset_of_moduleId_1(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsName_2(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsColor_3(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsJointNo_4(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsId_5(),
	GetChildPartsList_t2416430853::get_offset_of_parentPartsName_6(),
	GetChildPartsList_t2416430853::get_offset_of_parentPartsId_7(),
	GetChildPartsList_t2416430853::get_offset_of_parentPartsJointNo_8(),
	GetChildPartsList_t2416430853::get_offset_of_jointChildPartsName_9(),
	GetChildPartsList_t2416430853::get_offset_of_childObjPositionX_10(),
	GetChildPartsList_t2416430853::get_offset_of_childObjPositionY_11(),
	GetChildPartsList_t2416430853::get_offset_of_childObjPositionZ_12(),
	GetChildPartsList_t2416430853::get_offset_of_childLeapRotationX_13(),
	GetChildPartsList_t2416430853::get_offset_of_childLeapRotationY_14(),
	GetChildPartsList_t2416430853::get_offset_of_childLeapRotationZ_15(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsPositionX_16(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsPositionY_17(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsPositionZ_18(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsRotationX_19(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsRotationY_20(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsRotationZ_21(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsScaleX_22(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsScaleY_23(),
	GetChildPartsList_t2416430853::get_offset_of_childPartsScaleZ_24(),
	GetChildPartsList_t2416430853::get_offset_of_moduleScaleX_25(),
	GetChildPartsList_t2416430853::get_offset_of_moduleScaleY_26(),
	GetChildPartsList_t2416430853::get_offset_of_moduleScaleZ_27(),
	GetChildPartsList_t2416430853::get_offset_of_moduleCenterX_28(),
	GetChildPartsList_t2416430853::get_offset_of_moduleCenterY_29(),
	GetChildPartsList_t2416430853::get_offset_of_moduleCenterZ_30(),
	GetChildPartsList_t2416430853::get_offset_of_moduleLeapX_31(),
	GetChildPartsList_t2416430853::get_offset_of_moduleLeapY_32(),
	GetChildPartsList_t2416430853::get_offset_of_moduleLeapZ_33(),
	GetChildPartsList_t2416430853::get_offset_of_moduleRollX_34(),
	GetChildPartsList_t2416430853::get_offset_of_moduleRollY_35(),
	GetChildPartsList_t2416430853::get_offset_of_moduleRollZ_36(),
	GetChildPartsList_t2416430853::get_offset_of_parentModuleId_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (GetMobilityDelJson_t1883011669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3716[1] = 
{
	GetMobilityDelJson_t1883011669::get_offset_of_mobilityId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (GetMobilityUpdateJson_t3216313957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[4] = 
{
	GetMobilityUpdateJson_t3216313957::get_offset_of_mobilityId_0(),
	GetMobilityUpdateJson_t3216313957::get_offset_of_name_1(),
	GetMobilityUpdateJson_t3216313957::get_offset_of_situation_2(),
	GetMobilityUpdateJson_t3216313957::get_offset_of_description_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (GetMobilityPrizeJson_t1651198200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3718[5] = 
{
	GetMobilityPrizeJson_t1651198200::get_offset_of_prizedTypeId_0(),
	GetMobilityPrizeJson_t1651198200::get_offset_of_mobilityId_1(),
	GetMobilityPrizeJson_t1651198200::get_offset_of_eventId_2(),
	GetMobilityPrizeJson_t1651198200::get_offset_of_point_3(),
	GetMobilityPrizeJson_t1651198200::get_offset_of_time_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (ApiModuleManager_t3939700127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[8] = 
{
	ApiModuleManager_t3939700127::get_offset_of_moduleNewJson_3(),
	ApiModuleManager_t3939700127::get_offset_of_moduleDelJson_4(),
	ApiModuleManager_t3939700127::get_offset_of_getModuleJson_5(),
	ApiModuleManager_t3939700127::get_offset_of_moduleId_6(),
	ApiModuleManager_t3939700127::get_offset_of_newModuleChildPartslist_7(),
	ApiModuleManager_t3939700127::get_offset_of_newModuleChildPartsDic_8(),
	ApiModuleManager_t3939700127::get_offset_of_myModuleIdList_9(),
	ApiModuleManager_t3939700127::get_offset_of__values_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3720[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (GetModuleChildPartsList_t3198568368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3721[23] = 
{
	GetModuleChildPartsList_t3198568368::get_offset_of_parentPartsJointNo_0(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsJointNo_1(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childObjPositionX_2(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childObjPositionY_3(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childObjPositionZ_4(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsId_5(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsName_6(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsColor_7(),
	GetModuleChildPartsList_t3198568368::get_offset_of_parentPartsName_8(),
	GetModuleChildPartsList_t3198568368::get_offset_of_parentPartsId_9(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsPositionX_10(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsPositionY_11(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsPositionZ_12(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsRotationX_13(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsRotationY_14(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsRotationZ_15(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsScaleX_16(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsScaleY_17(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childPartsScaleZ_18(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childLeapRotationX_19(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childLeapRotationY_20(),
	GetModuleChildPartsList_t3198568368::get_offset_of_childLeapRotationZ_21(),
	GetModuleChildPartsList_t3198568368::get_offset_of_LeapFixed_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (GetModuleDelJson_t837825929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3722[1] = 
{
	GetModuleDelJson_t837825929::get_offset_of_moduleId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (ApiModuleMotionManager_t2960242393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[10] = 
{
	ApiModuleMotionManager_t2960242393::get_offset_of_moduleMotionNewJson_3(),
	ApiModuleMotionManager_t2960242393::get_offset_of_moduleActionId_4(),
	ApiModuleMotionManager_t2960242393::get_offset_of_m_pCurveDic1_5(),
	ApiModuleMotionManager_t2960242393::get_offset_of_curve_6(),
	ApiModuleMotionManager_t2960242393::get_offset_of_ks_7(),
	ApiModuleMotionManager_t2960242393::get_offset_of_path_8(),
	ApiModuleMotionManager_t2960242393::get_offset_of_property_9(),
	ApiModuleMotionManager_t2960242393::get_offset_of_moduleAnimClip_10(),
	ApiModuleMotionManager_t2960242393::get_offset_of_ischangeClip_11(),
	ApiModuleMotionManager_t2960242393::get_offset_of_modulePoseDataCount_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3724[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (GetModuleMotionItemData_t3026021381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3725[1] = 
{
	GetModuleMotionItemData_t3026021381::get_offset_of_motionData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (GetModuleMotionsData_t125704211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3726[2] = 
{
	GetModuleMotionsData_t125704211::get_offset_of_posePath_0(),
	GetModuleMotionsData_t125704211::get_offset_of_poseData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (GetModulePoseData_t1121789947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[2] = 
{
	GetModulePoseData_t1121789947::get_offset_of_poseKeyTime_0(),
	GetModulePoseData_t1121789947::get_offset_of_poseKeyValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[10] = 
{
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U3CNumU3E__0_0(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_json_1(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U3CwwwDataU3E__0_2(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U3C_getMotionBaseU3E__0_3(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U24locvar0_4(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_moduleObj_5(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U24this_6(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U24current_7(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U24disposing_8(),
	U3COnGetModuleMotionSuccessU3Ec__Iterator0_t2120637661::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (U3CmoduleAddClipU3Ec__Iterator1_t1418277765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3729[6] = 
{
	U3CmoduleAddClipU3Ec__Iterator1_t1418277765::get_offset_of_animDic_0(),
	U3CmoduleAddClipU3Ec__Iterator1_t1418277765::get_offset_of_coreAnim_1(),
	U3CmoduleAddClipU3Ec__Iterator1_t1418277765::get_offset_of_U24this_2(),
	U3CmoduleAddClipU3Ec__Iterator1_t1418277765::get_offset_of_U24current_3(),
	U3CmoduleAddClipU3Ec__Iterator1_t1418277765::get_offset_of_U24disposing_4(),
	U3CmoduleAddClipU3Ec__Iterator1_t1418277765::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (ApiNearmobManager_t3144099613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3730[3] = 
{
	ApiNearmobManager_t3144099613::get_offset_of_RespawnTime_3(),
	ApiNearmobManager_t3144099613::get_offset_of_RecMobNum_4(),
	ApiNearmobManager_t3144099613::get_offset_of_createdUserId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (ApiNoticeManager_t3224317815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3731[7] = 
{
	ApiNoticeManager_t3224317815::get_offset_of_Id_3(),
	ApiNoticeManager_t3224317815::get_offset_of_Notice_4(),
	ApiNoticeManager_t3224317815::get_offset_of_CreatedAt_5(),
	ApiNoticeManager_t3224317815::get_offset_of_Title_6(),
	ApiNoticeManager_t3224317815::get_offset_of_Url_7(),
	ApiNoticeManager_t3224317815::get_offset_of__noticeListData_8(),
	ApiNoticeManager_t3224317815::get_offset_of_NoticeDetail_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (GetNoticeDetail_t155866005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3732[5] = 
{
	GetNoticeDetail_t155866005::get_offset_of_Id_0(),
	GetNoticeDetail_t155866005::get_offset_of_Notice_1(),
	GetNoticeDetail_t155866005::get_offset_of_CreatedAt_2(),
	GetNoticeDetail_t155866005::get_offset_of_Title_3(),
	GetNoticeDetail_t155866005::get_offset_of_Url_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (ApiPrizedManager_t4212047241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3733[2] = 
{
	ApiPrizedManager_t4212047241::get_offset_of_prizedJson_3(),
	ApiPrizedManager_t4212047241::get_offset_of_eventhistoryJson_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (GetPrizedJson_t3849689266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3734[2] = 
{
	GetPrizedJson_t3849689266::get_offset_of_prizedMasterId_0(),
	GetPrizedJson_t3849689266::get_offset_of_mobilityId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (GetEventhistoryJson_t4277162862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3735[3] = 
{
	GetEventhistoryJson_t4277162862::get_offset_of_mobilityId_0(),
	GetEventhistoryJson_t4277162862::get_offset_of_point_1(),
	GetEventhistoryJson_t4277162862::get_offset_of_eventId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (ApiRaderManager_t2058631757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[6] = 
{
	ApiRaderManager_t2058631757::get_offset_of_raderAreaPos_3(),
	ApiRaderManager_t2058631757::get_offset_of_raderMobilmoObj_4(),
	ApiRaderManager_t2058631757::get_offset_of_raderArea_5(),
	ApiRaderManager_t2058631757::get_offset_of_raderMobCnt_6(),
	ApiRaderManager_t2058631757::get_offset_of_raderObjList_7(),
	ApiRaderManager_t2058631757::get_offset_of_selectedModId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (U3CWaitCreateRaderMobilmoU3Ec__Iterator0_t2734378261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3737[5] = 
{
	U3CWaitCreateRaderMobilmoU3Ec__Iterator0_t2734378261::get_offset_of_U3CobjU3E__1_0(),
	U3CWaitCreateRaderMobilmoU3Ec__Iterator0_t2734378261::get_offset_of_U24this_1(),
	U3CWaitCreateRaderMobilmoU3Ec__Iterator0_t2734378261::get_offset_of_U24current_2(),
	U3CWaitCreateRaderMobilmoU3Ec__Iterator0_t2734378261::get_offset_of_U24disposing_3(),
	U3CWaitCreateRaderMobilmoU3Ec__Iterator0_t2734378261::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (ApiReactionManager_t2857272874), -1, sizeof(ApiReactionManager_t2857272874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3738[23] = 
{
	ApiReactionManager_t2857272874::get_offset_of_reactionAddJsonData_3(),
	ApiReactionManager_t2857272874::get_offset_of_reactionNewJson_4(),
	ApiReactionManager_t2857272874::get_offset_of_FUNNYBtn_5(),
	ApiReactionManager_t2857272874::get_offset_of_AMAZINGBtn_6(),
	ApiReactionManager_t2857272874::get_offset_of_CUTEBtn_7(),
	ApiReactionManager_t2857272874::get_offset_of_COOLBtn_8(),
	ApiReactionManager_t2857272874::get_offset_of_LIKEBtn_9(),
	ApiReactionManager_t2857272874::get_offset_of_m_ifunny_10(),
	ApiReactionManager_t2857272874::get_offset_of_m_iamazing_11(),
	ApiReactionManager_t2857272874::get_offset_of_m_icute_12(),
	ApiReactionManager_t2857272874::get_offset_of_m_icool_13(),
	ApiReactionManager_t2857272874::get_offset_of_m_ilike_14(),
	ApiReactionManager_t2857272874_StaticFields::get_offset_of_m_sFunny_15(),
	ApiReactionManager_t2857272874_StaticFields::get_offset_of_m_sAmazing_16(),
	ApiReactionManager_t2857272874_StaticFields::get_offset_of_m_sCute_17(),
	ApiReactionManager_t2857272874_StaticFields::get_offset_of_m_sCool_18(),
	ApiReactionManager_t2857272874_StaticFields::get_offset_of_m_sLike_19(),
	ApiReactionManager_t2857272874::get_offset_of_reactionAllCnt_20(),
	ApiReactionManager_t2857272874::get_offset_of_heartCntText_21(),
	ApiReactionManager_t2857272874::get_offset_of_selectRecMobilmoObj_22(),
	ApiReactionManager_t2857272874::get_offset_of_recationMobimoId_23(),
	ApiReactionManager_t2857272874::get_offset_of_newReactionlist_24(),
	ApiReactionManager_t2857272874::get_offset_of_newReactionDataList_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (GetReactionList_t3285480050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3740[2] = 
{
	GetReactionList_t3285480050::get_offset_of_reactionType_0(),
	GetReactionList_t3285480050::get_offset_of_flg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (ApiTokenManager_t4213246208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3741[4] = 
{
	ApiTokenManager_t4213246208::get_offset_of_deviceToken_3(),
	ApiTokenManager_t4213246208::get_offset_of_tokenJson_4(),
	ApiTokenManager_t4213246208::get_offset_of_tokenSent_5(),
	ApiTokenManager_t4213246208::get_offset_of_tokendisp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (GetTokenNewJson_t2848869592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3742[1] = 
{
	GetTokenNewJson_t2848869592::get_offset_of_deviceToken_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (ApiUserManager_t2556825810), -1, sizeof(ApiUserManager_t2556825810_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3743[43] = 
{
	ApiUserManager_t2556825810::get_offset_of_usernewJson_3(),
	ApiUserManager_t2556825810::get_offset_of_userupdateJson_4(),
	ApiUserManager_t2556825810::get_offset_of_loginedUserId_5(),
	ApiUserManager_t2556825810::get_offset_of_userName_6(),
	ApiUserManager_t2556825810::get_offset_of_languageNo_7(),
	ApiUserManager_t2556825810::get_offset_of_countryNo_8(),
	ApiUserManager_t2556825810::get_offset_of_userRank_9(),
	ApiUserManager_t2556825810::get_offset_of_userPoint_10(),
	ApiUserManager_t2556825810::get_offset_of_userRankupPoint_11(),
	ApiUserManager_t2556825810::get_offset_of_userCopiedCount_12(),
	ApiUserManager_t2556825810::get_offset_of_mainMobilityId_13(),
	ApiUserManager_t2556825810::get_offset_of_mobilityCount_14(),
	ApiUserManager_t2556825810::get_offset_of_startPositionX_15(),
	ApiUserManager_t2556825810::get_offset_of_startPositionY_16(),
	ApiUserManager_t2556825810::get_offset_of_startPositionZ_17(),
	ApiUserManager_t2556825810::get_offset_of_startRotationX_18(),
	ApiUserManager_t2556825810::get_offset_of_startRotationY_19(),
	ApiUserManager_t2556825810::get_offset_of_startRotationZ_20(),
	ApiUserManager_t2556825810::get_offset_of__userNameText_21(),
	ApiUserManager_t2556825810::get_offset_of__userRankText_22(),
	ApiUserManager_t2556825810::get_offset_of__userPointText_23(),
	ApiUserManager_t2556825810::get_offset_of__recUserNameText_24(),
	ApiUserManager_t2556825810::get_offset_of__recUserRankText_25(),
	ApiUserManager_t2556825810::get_offset_of__recUserCountoryText_26(),
	ApiUserManager_t2556825810::get_offset_of__userRankupPointText_27(),
	ApiUserManager_t2556825810::get_offset_of__EmblemImage_28(),
	ApiUserManager_t2556825810::get_offset_of__RankGageImage_29(),
	ApiUserManager_t2556825810::get_offset_of_userNameInputField_30(),
	ApiUserManager_t2556825810::get_offset_of_createNewMobName_31(),
	ApiUserManager_t2556825810::get_offset_of_editUserName_32(),
	ApiUserManager_t2556825810::get_offset_of_userNameCreator_33(),
	ApiUserManager_t2556825810::get_offset_of_NameNewPanel_34(),
	ApiUserManager_t2556825810::get_offset_of_NameEditPanel_35(),
	ApiUserManager_t2556825810::get_offset_of_userUpdateList_36(),
	ApiUserManager_t2556825810::get_offset_of_m_UserSetting_37(),
	ApiUserManager_t2556825810::get_offset_of_isNgword_38(),
	ApiUserManager_t2556825810::get_offset_of_m_countyDataList_39(),
	ApiUserManager_t2556825810_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_40(),
	ApiUserManager_t2556825810_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_41(),
	ApiUserManager_t2556825810_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_42(),
	ApiUserManager_t2556825810_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_43(),
	ApiUserManager_t2556825810_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_44(),
	ApiUserManager_t2556825810_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (GetUserNewJson_t1716347878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3744[3] = 
{
	GetUserNewJson_t1716347878::get_offset_of_name_0(),
	GetUserNewJson_t1716347878::get_offset_of_countryNo_1(),
	GetUserNewJson_t1716347878::get_offset_of_languageNo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (GetUserUpdateJson_t2263613893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3745[11] = 
{
	GetUserUpdateJson_t2263613893::get_offset_of_userId_0(),
	GetUserUpdateJson_t2263613893::get_offset_of_name_1(),
	GetUserUpdateJson_t2263613893::get_offset_of_countryNo_2(),
	GetUserUpdateJson_t2263613893::get_offset_of_languageNo_3(),
	GetUserUpdateJson_t2263613893::get_offset_of_mainmobilityId_4(),
	GetUserUpdateJson_t2263613893::get_offset_of_startPositionX_5(),
	GetUserUpdateJson_t2263613893::get_offset_of_startPositionY_6(),
	GetUserUpdateJson_t2263613893::get_offset_of_startPositionZ_7(),
	GetUserUpdateJson_t2263613893::get_offset_of_startRotationX_8(),
	GetUserUpdateJson_t2263613893::get_offset_of_startRotationY_9(),
	GetUserUpdateJson_t2263613893::get_offset_of_startRotationZ_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (CountryData_t1135367575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3746[2] = 
{
	CountryData_t1135367575::get_offset_of_countryName_0(),
	CountryData_t1135367575::get_offset_of_countryNo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (ApiMigrate_t2586426963), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (MotionManager_t3855993783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3748[21] = 
{
	MotionManager_t3855993783::get_offset_of_actionChangeTimer_3(),
	MotionManager_t3855993783::get_offset_of_motionJson_4(),
	MotionManager_t3855993783::get_offset_of_motionNewJson_5(),
	MotionManager_t3855993783::get_offset_of_m_MobId_6(),
	MotionManager_t3855993783::get_offset_of_m_Mobilmo_7(),
	MotionManager_t3855993783::get_offset_of_m_pCurveDic1_8(),
	MotionManager_t3855993783::get_offset_of_m_pCurveDic2_9(),
	MotionManager_t3855993783::get_offset_of_m_pCurveDic3_10(),
	MotionManager_t3855993783::get_offset_of_m_pCurveDic4_11(),
	MotionManager_t3855993783::get_offset_of_curve_12(),
	MotionManager_t3855993783::get_offset_of_ks_13(),
	MotionManager_t3855993783::get_offset_of_path_14(),
	MotionManager_t3855993783::get_offset_of_property_15(),
	MotionManager_t3855993783::get_offset_of_animClip_16(),
	MotionManager_t3855993783::get_offset_of_recordingAnimClip_17(),
	MotionManager_t3855993783::get_offset_of_anim_18(),
	MotionManager_t3855993783::get_offset_of_isPlaying_19(),
	MotionManager_t3855993783::get_offset_of_isSuccess_20(),
	MotionManager_t3855993783::get_offset_of_motionNum_21(),
	MotionManager_t3855993783::get_offset_of_ischangeClip_22(),
	MotionManager_t3855993783::get_offset_of_m_recMobilmo_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (SerializationCopy_t3211118703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3750[2] = 
{
	SerializationCopy_t3211118703::get_offset_of_mobilityId_0(),
	SerializationCopy_t3211118703::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (GetItemsData_t1181624410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3751[1] = 
{
	GetItemsData_t1181624410::get_offset_of_motionData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (GetMotionsData_t2417923611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[2] = 
{
	GetMotionsData_t2417923611::get_offset_of_posePath_0(),
	GetMotionsData_t2417923611::get_offset_of_poseData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (GetPoseData_t2785975099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[2] = 
{
	GetPoseData_t2785975099::get_offset_of_poseKeyTime_0(),
	GetPoseData_t2785975099::get_offset_of_poseKeyValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3754[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (GetItemsDataInWorldActionSetting_t1654137477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3755[1] = 
{
	GetItemsDataInWorldActionSetting_t1654137477::get_offset_of_motionData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (GetMotionsDataInWorldActionSetting_t2072611844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[2] = 
{
	GetMotionsDataInWorldActionSetting_t2072611844::get_offset_of_posePath_0(),
	GetMotionsDataInWorldActionSetting_t2072611844::get_offset_of_poseData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (GetPoseDataInWorldActionSetting_t1846296182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[2] = 
{
	GetPoseDataInWorldActionSetting_t1846296182::get_offset_of_poseKeyTime_0(),
	GetPoseDataInWorldActionSetting_t1846296182::get_offset_of_poseKeyValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (U3CchangeMotionU3Ec__Iterator0_t472664859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[10] = 
{
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_U3CclipU3E__0_0(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_U3CclipNameU3E__0_1(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_ischange_2(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_motionNum_3(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_coreAnim_4(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_actionChangeTimer_5(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_U24this_6(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_U24current_7(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_U24disposing_8(),
	U3CchangeMotionU3Ec__Iterator0_t472664859::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (ContactSetting_t3049199920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3759[3] = 
{
	ContactSetting_t3049199920::get_offset_of_Email_0(),
	ContactSetting_t3049199920::get_offset_of_NickName_1(),
	ContactSetting_t3049199920::get_offset_of_Text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (MyPageMobilmoItem_t346175087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[10] = 
{
	MyPageMobilmoItem_t346175087::get_offset_of_Name_0(),
	MyPageMobilmoItem_t346175087::get_offset_of_Description_1(),
	MyPageMobilmoItem_t346175087::get_offset_of_Obj_2(),
	MyPageMobilmoItem_t346175087::get_offset_of_Id_3(),
	MyPageMobilmoItem_t346175087::get_offset_of_SelectNo_4(),
	MyPageMobilmoItem_t346175087::get_offset_of_ReactionAll_5(),
	MyPageMobilmoItem_t346175087::get_offset_of_Prized_6(),
	MyPageMobilmoItem_t346175087::get_offset_of_UsedPartsCount_7(),
	MyPageMobilmoItem_t346175087::get_offset_of_ReactionList_8(),
	MyPageMobilmoItem_t346175087::get_offset_of_PrizedMasterIdList_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (ReactionList_t1054868761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[2] = 
{
	ReactionList_t1054868761::get_offset_of_ReactionTypeList_0(),
	ReactionList_t1054868761::get_offset_of_ReactionCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (MyPageManager_t2103198340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[21] = 
{
	MyPageManager_t2103198340::get_offset_of_Contents_3(),
	MyPageManager_t2103198340::get_offset_of_Location_4(),
	MyPageManager_t2103198340::get_offset_of_MyMobilmoItemList_5(),
	MyPageManager_t2103198340::get_offset_of_SelectMoblityId_6(),
	MyPageManager_t2103198340::get_offset_of_Contact_7(),
	MyPageManager_t2103198340::get_offset_of_createPanel_8(),
	MyPageManager_t2103198340::get_offset_of_CurrentMobility_9(),
	MyPageManager_t2103198340::get_offset_of_MobilityCount_10(),
	MyPageManager_t2103198340::get_offset_of_CurrentIndex_11(),
	MyPageManager_t2103198340::get_offset_of_userSettingPanel_12(),
	MyPageManager_t2103198340::get_offset_of_pressMyPageCount_13(),
	MyPageManager_t2103198340::get_offset_of_m_CurrentTween_14(),
	MyPageManager_t2103198340::get_offset_of_m_MovingTween_15(),
	MyPageManager_t2103198340::get_offset_of_isCreatedMobility_16(),
	MyPageManager_t2103198340::get_offset_of_pressCnt_17(),
	MyPageManager_t2103198340::get_offset_of_LinePos_18(),
	MyPageManager_t2103198340::get_offset_of_FloorPos_19(),
	MyPageManager_t2103198340::get_offset_of_LineIndex_20(),
	MyPageManager_t2103198340::get_offset_of_FloorIndex_21(),
	MyPageManager_t2103198340::get_offset_of_m_bSceneMoved_22(),
	MyPageManager_t2103198340::get_offset_of_isChangingMypage_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (U3CMobilityToWorldU3Ec__Iterator0_t3575254946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3763[5] = 
{
	U3CMobilityToWorldU3Ec__Iterator0_t3575254946::get_offset_of_U3CvPlanetPosU3E__0_0(),
	U3CMobilityToWorldU3Ec__Iterator0_t3575254946::get_offset_of_U3CmainCameraTransU3E__0_1(),
	U3CMobilityToWorldU3Ec__Iterator0_t3575254946::get_offset_of_U24current_2(),
	U3CMobilityToWorldU3Ec__Iterator0_t3575254946::get_offset_of_U24disposing_3(),
	U3CMobilityToWorldU3Ec__Iterator0_t3575254946::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (U3C_LoopNextFloorU3Ec__AnonStorey1_t2772912709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3764[2] = 
{
	U3C_LoopNextFloorU3Ec__AnonStorey1_t2772912709::get_offset_of_j_0(),
	U3C_LoopNextFloorU3Ec__AnonStorey1_t2772912709::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (U3C_LoopPrevFloorU3Ec__AnonStorey2_t2980329754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[2] = 
{
	U3C_LoopPrevFloorU3Ec__AnonStorey2_t2980329754::get_offset_of_j_0(),
	U3C_LoopPrevFloorU3Ec__AnonStorey2_t2980329754::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (RecordingManager_t3075979958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3766[43] = 
{
	RecordingManager_t3075979958::get_offset_of_recordedId_3(),
	RecordingManager_t3075979958::get_offset_of_startPointX_4(),
	RecordingManager_t3075979958::get_offset_of_startPointY_5(),
	RecordingManager_t3075979958::get_offset_of_startPointZ_6(),
	RecordingManager_t3075979958::get_offset_of_startRotateX_7(),
	RecordingManager_t3075979958::get_offset_of_startRotateY_8(),
	RecordingManager_t3075979958::get_offset_of_startRotateZ_9(),
	RecordingManager_t3075979958::get_offset_of_motionStartTime_10(),
	RecordingManager_t3075979958::get_offset_of_actionNo_11(),
	RecordingManager_t3075979958::get_offset_of_startRecordingTime_12(),
	RecordingManager_t3075979958::get_offset_of_timer_13(),
	RecordingManager_t3075979958::get_offset_of_isRecordingStart_14(),
	RecordingManager_t3075979958::get_offset_of_isActionButtonClick_15(),
	RecordingManager_t3075979958::get_offset_of_corePartsStartPointX_16(),
	RecordingManager_t3075979958::get_offset_of_corePartsStartPointY_17(),
	RecordingManager_t3075979958::get_offset_of_corePartsStartPointZ_18(),
	RecordingManager_t3075979958::get_offset_of_corePartsStartRotateX_19(),
	RecordingManager_t3075979958::get_offset_of_corePartsStartRotateY_20(),
	RecordingManager_t3075979958::get_offset_of_corePartsStartRotateZ_21(),
	RecordingManager_t3075979958::get_offset_of_mobilityJsonArray_22(),
	RecordingManager_t3075979958::get_offset_of_NPCMobGet_23(),
	RecordingManager_t3075979958::get_offset_of_pTranses_24(),
	RecordingManager_t3075979958::get_offset_of_pChildParts_25(),
	RecordingManager_t3075979958::get_offset_of_anim_26(),
	RecordingManager_t3075979958::get_offset_of_newRecording_27(),
	RecordingManager_t3075979958::get_offset_of_recordingMotionDataList_28(),
	RecordingManager_t3075979958::get_offset_of_getModuleIdList_29(),
	RecordingManager_t3075979958::get_offset_of_getOtherModuleIdList_30(),
	RecordingManager_t3075979958::get_offset_of_newItems_31(),
	RecordingManager_t3075979958::get_offset_of_plantObj_32(),
	RecordingManager_t3075979958::get_offset_of_moduleCopyJson_33(),
	RecordingManager_t3075979958::get_offset_of_copyedMobilityId_34(),
	RecordingManager_t3075979958::get_offset_of_recordingJson_35(),
	RecordingManager_t3075979958::get_offset_of_mobilmo_36(),
	RecordingManager_t3075979958::get_offset_of_RecMobilityList_37(),
	RecordingManager_t3075979958::get_offset_of_isGettingRecordedmodule_38(),
	RecordingManager_t3075979958::get_offset_of_Controller_39(),
	RecordingManager_t3075979958::get_offset_of_copyedModuleId_40(),
	RecordingManager_t3075979958::get_offset_of_RecordGuage_41(),
	RecordingManager_t3075979958::get_offset_of_Recording_42(),
	RecordingManager_t3075979958::get_offset_of_m_risingTween_43(),
	RecordingManager_t3075979958::get_offset_of_m_blinkTween_44(),
	RecordingManager_t3075979958::get_offset_of_pressRecoredCnt_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3767[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (GetItemsData_t174665839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3768[8] = 
{
	GetItemsData_t174665839::get_offset_of_recordedDate_0(),
	GetItemsData_t174665839::get_offset_of_startPointX_1(),
	GetItemsData_t174665839::get_offset_of_startPointY_2(),
	GetItemsData_t174665839::get_offset_of_startPointZ_3(),
	GetItemsData_t174665839::get_offset_of_startRotateX_4(),
	GetItemsData_t174665839::get_offset_of_startRotateY_5(),
	GetItemsData_t174665839::get_offset_of_startRotateZ_6(),
	GetItemsData_t174665839::get_offset_of_motions_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (GetRecordingNewMotions_t4070061385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3769[2] = 
{
	GetRecordingNewMotions_t4070061385::get_offset_of_motionNum_0(),
	GetRecordingNewMotions_t4070061385::get_offset_of_motionStartTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (CopyModuleData_t4206646110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3770[3] = 
{
	CopyModuleData_t4206646110::get_offset_of_userId_0(),
	CopyModuleData_t4206646110::get_offset_of_mobilityId_1(),
	CopyModuleData_t4206646110::get_offset_of_moduleId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (CopyRecModuleDataInWorld_t1848073189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3771[2] = 
{
	CopyRecModuleDataInWorld_t1848073189::get_offset_of_userId_0(),
	CopyRecModuleDataInWorld_t1848073189::get_offset_of_moduleId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (recordingMotionData_t1704213756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3772[3] = 
{
	recordingMotionData_t1704213756::get_offset_of_motionStartTime_0(),
	recordingMotionData_t1704213756::get_offset_of_motionNum_1(),
	recordingMotionData_t1704213756::get_offset_of_mobilmo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (PlayerInfoLike_t852303790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3773[3] = 
{
	PlayerInfoLike_t852303790::get_offset_of_iconEmotion_2(),
	PlayerInfoLike_t852303790::get_offset_of_iconRank_3(),
	PlayerInfoLike_t852303790::get_offset_of_nameUsertxt_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (ReactionHistory_t2538361761), -1, sizeof(ReactionHistory_t2538361761_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3774[2] = 
{
	ReactionHistory_t2538361761_StaticFields::get_offset_of_OnGetDataUserLikeEvent_3(),
	ReactionHistory_t2538361761_StaticFields::get_offset_of_OnAnalizeDataEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (GetChampionItemModel_t353148757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3775[1] = 
{
	GetChampionItemModel_t353148757::get_offset_of_U3C_champion_itemModel_listU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (ChampionItemModel_t975870399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3776[11] = 
{
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_prized_typeU3Ek__BackingField_0(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_eventU3Ek__BackingField_1(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_pointU3Ek__BackingField_2(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_prized_dateU3Ek__BackingField_3(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_user_nameU3Ek__BackingField_4(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_user_countryU3Ek__BackingField_5(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_timeU3Ek__BackingField_6(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_user_rankU3Ek__BackingField_7(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_scoreU3Ek__BackingField_8(),
	ChampionItemModel_t975870399::get_offset_of_U3Cchampion_item_createdUser_rankU3Ek__BackingField_9(),
	ChampionItemModel_t975870399::get_offset_of_U3C_champion_mobility_modelU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (ChampionMobilityModel_t1041611931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3777[20] = 
{
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_idU3Ek__BackingField_0(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_dateU3Ek__BackingField_1(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_nameU3Ek__BackingField_2(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_situationU3Ek__BackingField_3(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_descriptionU3Ek__BackingField_4(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_copied_countU3Ek__BackingField_5(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_parent_idU3Ek__BackingField_6(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_core_parts_idU3Ek__BackingField_7(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_core_parts_colorU3Ek__BackingField_8(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_core_parts_scale_xU3Ek__BackingField_9(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_core_parts_scale_yU3Ek__BackingField_10(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_core_parts_scale_zU3Ek__BackingField_11(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_core_parts_nameU3Ek__BackingField_12(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3Cchampion_mobility_created_user_idU3Ek__BackingField_13(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3C_champion_mobility_reactionU3Ek__BackingField_14(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3C_champion_mobility_prized_id_listU3Ek__BackingField_15(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3C_champion_mobility_ecount_listU3Ek__BackingField_16(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3C_champion_mobility_registerd_motion_listU3Ek__BackingField_17(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3C_champion_mobility_recorded_motion_listU3Ek__BackingField_18(),
	ChampionMobilityModel_t1041611931::get_offset_of_U3C_champion_mobility_child_parts_listU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (GetChampionMobilityReactionModel_t2714706038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3778[2] = 
{
	GetChampionMobilityReactionModel_t2714706038::get_offset_of_U3Cchampion_mobility_reaction_allCountU3Ek__BackingField_0(),
	GetChampionMobilityReactionModel_t2714706038::get_offset_of_U3C_champion_mobility_reaction_listReactionsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (GetChampionMobilityReactionsModel_t283662167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3779[2] = 
{
	GetChampionMobilityReactionsModel_t283662167::get_offset_of_U3Cchampion_mobility_reaction_items_reactionTypeU3Ek__BackingField_0(),
	GetChampionMobilityReactionsModel_t283662167::get_offset_of_U3Cchampion_mobility_items_reaction_items_countU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (GetChampionMobilityEncountListModel_t3804924867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3780[3] = 
{
	GetChampionMobilityEncountListModel_t3804924867::get_offset_of_U3Cchampion_mobility_encountList_encountUser_IdU3Ek__BackingField_0(),
	GetChampionMobilityEncountListModel_t3804924867::get_offset_of_U3Cchampion_mobility_encountList_pointU3Ek__BackingField_1(),
	GetChampionMobilityEncountListModel_t3804924867::get_offset_of_U3Cchampion_mobility_encountList_encount_timeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (GetChampionMobilityRegisterdMotionListModel_t1560983880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3781[2] = 
{
	GetChampionMobilityRegisterdMotionListModel_t1560983880::get_offset_of_U3Cchampion_mobility_registerdMotionList_motion_IdU3Ek__BackingField_0(),
	GetChampionMobilityRegisterdMotionListModel_t1560983880::get_offset_of_U3C_champion_mobility_registerMotionList_motionDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3782[2] = 
{
	GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608::get_offset_of_U3Cchampion_mobility_registerdMotionList_motionData_pose_IdU3Ek__BackingField_0(),
	GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608::get_offset_of_U3C_champion_mobility_registerMotionList_poseDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (GetChampionMobilityRegisterMotionListPoseData_t3691775360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3783[2] = 
{
	GetChampionMobilityRegisterMotionListPoseData_t3691775360::get_offset_of_U3Cchampion_mobility_registerdMotionList_poseData_poseKeyTimeU3Ek__BackingField_0(),
	GetChampionMobilityRegisterMotionListPoseData_t3691775360::get_offset_of_U3Cchampion_mobility_registerdMotionList_poseData_poseKeyValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (GetChampionMobilityRecordedMotionList_t1893561664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3784[6] = 
{
	GetChampionMobilityRecordedMotionList_t1893561664::get_offset_of_U3Cchampion_mobility_recordedMotionList_recorded_IdU3Ek__BackingField_0(),
	GetChampionMobilityRecordedMotionList_t1893561664::get_offset_of_U3Cchampion_mobility_recordedMotionList_recorded_DateU3Ek__BackingField_1(),
	GetChampionMobilityRecordedMotionList_t1893561664::get_offset_of_U3Cchampion_mobility_recordedMotionList_startPoint_XU3Ek__BackingField_2(),
	GetChampionMobilityRecordedMotionList_t1893561664::get_offset_of_U3Cchampion_mobility_recordedMotionList_startPoint_YU3Ek__BackingField_3(),
	GetChampionMobilityRecordedMotionList_t1893561664::get_offset_of_U3Cchampion_mobility_recordedMotionList_startPoint_ZU3Ek__BackingField_4(),
	GetChampionMobilityRecordedMotionList_t1893561664::get_offset_of_U3C_champion_mobility_recordedMotionList_motionsDataU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { sizeof (GetChampionMobilityRecordedMotionListMotions_t3064563391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3785[2] = 
{
	GetChampionMobilityRecordedMotionListMotions_t3064563391::get_offset_of_U3Cchampion_mobility_recordedMotionList_motions_motionStartTimeU3Ek__BackingField_0(),
	GetChampionMobilityRecordedMotionListMotions_t3064563391::get_offset_of_U3Cchampion_mobility_recordedMotionList_motions_motion_IdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { sizeof (GetChampionMobilityChildPartsListModel_t1181557729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3786[37] = 
{
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_Parts_TypeU3Ek__BackingField_0(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_Module_IdU3Ek__BackingField_1(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_IdU3Ek__BackingField_2(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_parentParts_IdU3Ek__BackingField_3(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_parentPartsJoint_NoU3Ek__BackingField_4(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childPartsJoint_NoU3Ek__BackingField_5(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_parentParts_NameU3Ek__BackingField_6(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_NameU3Ek__BackingField_7(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_ColorU3Ek__BackingField_8(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childRoll_rotation_XU3Ek__BackingField_9(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childRoll_rotation_YU3Ek__BackingField_10(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childRoll_rotation_ZU3Ek__BackingField_11(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childObj_rotation_XU3Ek__BackingField_12(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childObj_rotation_YU3Ek__BackingField_13(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childObj_rotation_ZU3Ek__BackingField_14(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childLeap_rotation_XU3Ek__BackingField_15(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childLeap_rotation_YU3Ek__BackingField_16(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childLeap_rotation_ZU3Ek__BackingField_17(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_position_XU3Ek__BackingField_18(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_position_YU3Ek__BackingField_19(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_position_ZU3Ek__BackingField_20(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_rotation_XU3Ek__BackingField_21(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_rotation_YU3Ek__BackingField_22(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_rotation_ZU3Ek__BackingField_23(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_scale_XU3Ek__BackingField_24(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_scale_YU3Ek__BackingField_25(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_childParts_scale_ZU3Ek__BackingField_26(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_module_scale_XU3Ek__BackingField_27(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_module_scale_YU3Ek__BackingField_28(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_module_scale_ZU3Ek__BackingField_29(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_moduleCenter_position_XU3Ek__BackingField_30(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_moduleCenter_position_YU3Ek__BackingField_31(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_moduleCenter_position_ZU3Ek__BackingField_32(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_moduleLeap_rotation_XU3Ek__BackingField_33(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_moduleLeap_rotation_YU3Ek__BackingField_34(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_moduleLeap_rotation_ZU3Ek__BackingField_35(),
	GetChampionMobilityChildPartsListModel_t1181557729::get_offset_of_U3Cchampion_childPartsList_parentModule_IdU3Ek__BackingField_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (GetComponentBaseModel_t567317181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3787[1] = 
{
	GetComponentBaseModel_t567317181::get_offset_of_U3Cget_component_model_listU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (GetComponentModel_t1600826744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3788[5] = 
{
	GetComponentModel_t1600826744::get_offset_of_U3C_component_IdU3Ek__BackingField_0(),
	GetComponentModel_t1600826744::get_offset_of_U3C_component_NameU3Ek__BackingField_1(),
	GetComponentModel_t1600826744::get_offset_of_U3C_component_DescriptionU3Ek__BackingField_2(),
	GetComponentModel_t1600826744::get_offset_of_U3C_component_Category_IdU3Ek__BackingField_3(),
	GetComponentModel_t1600826744::get_offset_of__parameter_list_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (GetComponentListModel_t2407555168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3789[9] = 
{
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_MassU3Ek__BackingField_0(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_ForceU3Ek__BackingField_1(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_BounceU3Ek__BackingField_2(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_FloatForceU3Ek__BackingField_3(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_ElasticitySpeedU3Ek__BackingField_4(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_FloatBounceU3Ek__BackingField_5(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_FloatFrictionU3Ek__BackingField_6(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_FrictionU3Ek__BackingField_7(),
	GetComponentListModel_t2407555168::get_offset_of_U3C_component_Parameter_RotationSpeedU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (GetDnaModel_t3440451250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3790[5] = 
{
	GetDnaModel_t3440451250::get_offset_of_U3C_dna_ParentIdU3Ek__BackingField_0(),
	GetDnaModel_t3440451250::get_offset_of_U3C_dna_ParentNameU3Ek__BackingField_1(),
	GetDnaModel_t3440451250::get_offset_of_U3C_dna_MobIdU3Ek__BackingField_2(),
	GetDnaModel_t3440451250::get_offset_of_U3C_dna_MobNameU3Ek__BackingField_3(),
	GetDnaModel_t3440451250::get_offset_of_U3C_dna_children_listU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (GetDnaChildrenListModel_t2978945125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3791[2] = 
{
	GetDnaChildrenListModel_t2978945125::get_offset_of_U3C_dna_Children_ChildIdU3Ek__BackingField_0(),
	GetDnaChildrenListModel_t2978945125::get_offset_of_U3C_dna_Children_ChildNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (ErrorModel_t3145739141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[2] = 
{
	ErrorModel_t3145739141::get_offset_of_U3C_error_codeU3Ek__BackingField_0(),
	ErrorModel_t3145739141::get_offset_of_U3C_error_messageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (EventModel_t594587933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3793[3] = 
{
	EventModel_t594587933::get_offset_of_U3C_event_rankU3Ek__BackingField_0(),
	EventModel_t594587933::get_offset_of_U3C_event_rankupPointU3Ek__BackingField_1(),
	EventModel_t594587933::get_offset_of_U3C_event_prizedMasterIdU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (GetFeedModel_t607373445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3794[8] = 
{
	GetFeedModel_t607373445::get_offset_of_U3C_feed_IdU3Ek__BackingField_0(),
	GetFeedModel_t607373445::get_offset_of_U3C_feed_DateU3Ek__BackingField_1(),
	GetFeedModel_t607373445::get_offset_of_U3C_feed_NameU3Ek__BackingField_2(),
	GetFeedModel_t607373445::get_offset_of_U3C_feed_SituationU3Ek__BackingField_3(),
	GetFeedModel_t607373445::get_offset_of_U3C_feed_Reaction_AllCountU3Ek__BackingField_4(),
	GetFeedModel_t607373445::get_offset_of_U3C_feed_UserIdU3Ek__BackingField_5(),
	GetFeedModel_t607373445::get_offset_of_U3C_feed_CopiedCountU3Ek__BackingField_6(),
	GetFeedModel_t607373445::get_offset_of_U3C_feed_ThumbnailU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (GetReactionModel_t3327617864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3795[6] = 
{
	GetReactionModel_t3327617864::get_offset_of_U3C_feed_Reaction_AllCountU3Ek__BackingField_0(),
	GetReactionModel_t3327617864::get_offset_of_U3C_feed_FunnyU3Ek__BackingField_1(),
	GetReactionModel_t3327617864::get_offset_of_U3C_feed_AmazingU3Ek__BackingField_2(),
	GetReactionModel_t3327617864::get_offset_of_U3C_feed_CuteU3Ek__BackingField_3(),
	GetReactionModel_t3327617864::get_offset_of_U3C_feed_CoolU3Ek__BackingField_4(),
	GetReactionModel_t3327617864::get_offset_of_U3C_feed_LikeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (JsonObject_t1432680743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (GetMigrateCodeModel_t2576627179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3797[1] = 
{
	GetMigrateCodeModel_t2576627179::get_offset_of_U3C_migrate_codeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (GetMobilityBaseList_t1712726784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (GetMobilityModel_t1064189486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3799[22] = 
{
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_Mobility_IdU3Ek__BackingField_0(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_dateU3Ek__BackingField_1(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_nameU3Ek__BackingField_2(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_situationU3Ek__BackingField_3(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_descriptionU3Ek__BackingField_4(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_parent_IdU3Ek__BackingField_5(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_coreParts_IdU3Ek__BackingField_6(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_coreParts_ColorU3Ek__BackingField_7(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_corePartsScale_XU3Ek__BackingField_8(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_corePartsScale_YU3Ek__BackingField_9(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_corePartsScale_ZU3Ek__BackingField_10(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_coreParts_NameU3Ek__BackingField_11(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_createdUser_IdU3Ek__BackingField_12(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_createdUser_NameU3Ek__BackingField_13(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_createdUser_RankU3Ek__BackingField_14(),
	GetMobilityModel_t1064189486::get_offset_of_U3Cmobility_reaction_allcountU3Ek__BackingField_15(),
	GetMobilityModel_t1064189486::get_offset_of_mobility_prized_list_16(),
	GetMobilityModel_t1064189486::get_offset_of_U3C_mobility_reaction_items_listU3Ek__BackingField_17(),
	GetMobilityModel_t1064189486::get_offset_of_U3C_mobility_encount_listU3Ek__BackingField_18(),
	GetMobilityModel_t1064189486::get_offset_of_U3C_mobility_registerMotion_listU3Ek__BackingField_19(),
	GetMobilityModel_t1064189486::get_offset_of_U3C_mobility_recordedMotion_listU3Ek__BackingField_20(),
	GetMobilityModel_t1064189486::get_offset_of_U3C_mobility_childParts_listU3Ek__BackingField_21(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
