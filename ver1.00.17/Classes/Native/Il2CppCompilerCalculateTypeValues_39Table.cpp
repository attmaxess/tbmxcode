﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DefsJsonKey_Module313020820.h"
#include "AssemblyU2DCSharp_DefsJsonKey_ModuleMotion2002280314.h"
#include "AssemblyU2DCSharp_DefsJsonKey_GetCountries4172618058.h"
#include "AssemblyU2DCSharp_DefsJsonKey_PostMobilityNew1434085915.h"
#include "AssemblyU2DCSharp_DefsJsonKey_PostRecordingNew374625147.h"
#include "AssemblyU2DCSharp_DefsJsonKey_PostUserNew3265085945.h"
#include "AssemblyU2DCSharp_DefsJsonKey_PostUserUpdate2575366490.h"
#include "AssemblyU2DCSharp_DownLoadAssetbundle3666029200.h"
#include "AssemblyU2DCSharp_DownLoadAssetbundle_U3CDownFileU1466681690.h"
#include "AssemblyU2DCSharp_LoadAssetBundle379001212.h"
#include "AssemblyU2DCSharp_LoadAssetBundle_U3CInitializeAss2797697157.h"
#include "AssemblyU2DCSharp_LoadAssetBundle_U3CSetAssetBundl3527147099.h"
#include "AssemblyU2DCSharp_LoadAssetBundle_U3CReLoadAssetBu1146541457.h"
#include "AssemblyU2DCSharp_LoadAssetBundle_U3CReLoadAssetBu1683670451.h"
#include "AssemblyU2DCSharp_AreaObjs582166865.h"
#include "AssemblyU2DCSharp_CameraAreaMonitor370298340.h"
#include "AssemblyU2DCSharp_CameraEffect3191348726.h"
#include "AssemblyU2DCSharp_CameraEffect_U3CsetAreaEffectU3E3116979964.h"
#include "AssemblyU2DCSharp_CameraFollow1493855402.h"
#include "AssemblyU2DCSharp_CameraRotater122375930.h"
#include "AssemblyU2DCSharp_CameraSettings3536359094.h"
#include "AssemblyU2DCSharp_CameraZoom1174393664.h"
#include "AssemblyU2DCSharp_SubCameraHandler2753085375.h"
#include "AssemblyU2DCSharp_SubCameraHandler_U3CDispSubCamer3258629182.h"
#include "AssemblyU2DCSharp_SubCameraHandler_U3CDispMainCame2482052376.h"
#include "AssemblyU2DCSharp_SubCameraHandler_U3CHideSubCamer2825804828.h"
#include "AssemblyU2DCSharp_SubCameraHandler_U3CHideMainCame1339147994.h"
#include "AssemblyU2DCSharp_PartsCapture2562628060.h"
#include "AssemblyU2DCSharp_PartsCapture_U3CCaptureToOrderU32225335291.h"
#include "AssemblyU2DCSharp_PartsCapture_U3CCaptureU3Ec__Ite3002243375.h"
#include "AssemblyU2DCSharp_ActionLever131660742.h"
#include "AssemblyU2DCSharp_ActionPartsPanelController3710630626.h"
#include "AssemblyU2DCSharp_ActionPartsPanelController_U3CWa3801418970.h"
#include "AssemblyU2DCSharp_Lever866853776.h"
#include "AssemblyU2DCSharp_EventManager_EventDispatcher1543274979.h"
#include "AssemblyU2DCSharp_EventManager_EventDispatcherExte2317368000.h"
#include "AssemblyU2DCSharp_eEventID711132804.h"
#include "AssemblyU2DCSharp_GameObjectExtensions1406008347.h"
#include "AssemblyU2DCSharp_GameObjectExtensions_U3CGetChild1107910596.h"
#include "AssemblyU2DCSharp_GameObjectExtensions_U3CGetChild2673994537.h"
#include "AssemblyU2DCSharp_ImageExtensions1485547703.h"
#include "AssemblyU2DCSharp_RawImageExtensions4149046211.h"
#include "AssemblyU2DCSharp_StringExtensions1398449501.h"
#include "AssemblyU2DCSharp_TextExtensions665271833.h"
#include "AssemblyU2DCSharp_TransformExtensions834738118.h"
#include "AssemblyU2DCSharp_UIBehaviourExtensions3966032777.h"
#include "AssemblyU2DCSharp_UIBehaviourExtensions_U3CRemoveA1887097533.h"
#include "AssemblyU2DCSharp_Localization_CountryCodeKey3757133144.h"
#include "AssemblyU2DCSharp_Localization_LocalizeKey3348421234.h"
#include "AssemblyU2DCSharp_UILocalizer2245030347.h"
#include "AssemblyU2DCSharp_UILocalizer_LocalizeString1691720564.h"
#include "AssemblyU2DCSharp_Icon4013200341.h"
#include "AssemblyU2DCSharp_ActionManager1367723175.h"
#include "AssemblyU2DCSharp_ActionManager_U3CAnimPlayTimeU3E2431106101.h"
#include "AssemblyU2DCSharp_AnalyticsManager1593654123.h"
#include "AssemblyU2DCSharp_eAUDIOBGM4173725093.h"
#include "AssemblyU2DCSharp_eAUDIOSE638655723.h"
#include "AssemblyU2DCSharp_eAUDIOVOICE2836738047.h"
#include "AssemblyU2DCSharp_AudioManager4222704959.h"
#include "AssemblyU2DCSharp_AudioManager_U3CFeedPlayBGMU3Ec_3064604589.h"
#include "AssemblyU2DCSharp_CameraManager2379859346.h"
#include "AssemblyU2DCSharp_CameraManager_U3CPosisioningPanel977640555.h"
#include "AssemblyU2DCSharp_ColorManager1666568646.h"
#include "AssemblyU2DCSharp_ColorManager_U3CColorButtonInitU3276323827.h"
#include "AssemblyU2DCSharp_ConstantManager2577827963.h"
#include "AssemblyU2DCSharp_eCreateType1183446867.h"
#include "AssemblyU2DCSharp_CreateTypeExtensions2951264486.h"
#include "AssemblyU2DCSharp_CreateManager3918627545.h"
#include "AssemblyU2DCSharp_CreateManager_U3C_AnchorSettingU2219086597.h"
#include "AssemblyU2DCSharp_CreateManager_U3CObjSelectU3Ec__2382599615.h"
#include "AssemblyU2DCSharp_CreateManager_U3CEditPartsRotatio477756611.h"
#include "AssemblyU2DCSharp_CreateManager_U3CEditModuleRotat2496885042.h"
#include "AssemblyU2DCSharp_CreateManager_U3CCheckMobilmoOfP1636254617.h"
#include "AssemblyU2DCSharp_CreateManager_U3CFlashingAlphaU33181075276.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_InputManager1610719423.h"
#include "AssemblyU2DCSharp_eLIGHTTYPE1469766701.h"
#include "AssemblyU2DCSharp_lightObj383130471.h"
#include "AssemblyU2DCSharp_LightManager3427519171.h"
#include "AssemblyU2DCSharp_LoadingManager2398813851.h"
#include "AssemblyU2DCSharp_LocalizeTextContents1443703366.h"
#include "AssemblyU2DCSharp_LocalizeItem998829008.h"
#include "AssemblyU2DCSharp_LocalizeManager1264687742.h"
#include "AssemblyU2DCSharp_MINIGAME3871825659.h"
#include "AssemblyU2DCSharp_SCORETYPE2692914394.h"
#include "AssemblyU2DCSharp_MiniGameManager849864952.h"
#include "AssemblyU2DCSharp_MiniGameManager_U3CMiniGameOverDi214630277.h"
#include "AssemblyU2DCSharp_MobilmoManager1293766190.h"
#include "AssemblyU2DCSharp_MobilmoManager_U3CreCreateMobilm1623790344.h"
#include "AssemblyU2DCSharp_MobilmoManager_U3CreCreateMyMobi4084074203.h"
#include "AssemblyU2DCSharp_MobilmoManager_U3CrecordingRecre3985505739.h"
#include "AssemblyU2DCSharp_MobilmoManager_U3CreCreateAction2372253177.h"
#include "AssemblyU2DCSharp_eMYMODULETYPE2778483377.h"
#include "AssemblyU2DCSharp_ModuleTypeExtensions3787989386.h"
#include "AssemblyU2DCSharp_ModuleManager1065445307.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CWaitCreateDefau3833490157.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CbeforehandCreate924205912.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CreCreateModuleU2731074232.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (Module_t313020820), -1, sizeof(Module_t313020820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3900[44] = 
{
	Module_t313020820_StaticFields::get_offset_of_MODULE_NEW_ID_0(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_ID_1(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CREATE_USER_ID_2(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_PARENT_ID_3(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_COREPARTS_ID_4(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_COREPARTS_COLOR_5(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_COREPARTS_NAME_6(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_COREPARTS_SCALE_X_7(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_COREPARTS_SCALE_Y_8(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_COREPARTS_SCALE_Z_9(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_PARENTPARTSCHILD_NAME_10(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_MOBILMO_ROTATION_X_11(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_MOBILMO_ROTATION_Y_12(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_MOBILMO_ROTATION_Z_13(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTSLIST_14(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_ID_15(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDOBJ_POSITION_X_16(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDOBJ_POSITION_Y_17(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDOBJ_POSITION_Z_18(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_COLOR_19(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_PARENTPARTS_ID_20(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_NAME_21(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_PARENTPARTS_NAME_22(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_PARENTPARTS_JOINT_NO_23(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_JOINT_NO_24(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDCENTER_ROTATION_X_25(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDCENTER_ROTATION_Y_26(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDCENTER_ROTATION_Z_27(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDCENTER_POSITION_X_28(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDCENTER_POSITION_Y_29(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDCENTER_POSITION_Z_30(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_POSITION_X_31(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_POSITION_Y_32(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_POSITION_Z_33(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_ROTATION_X_34(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_ROTATION_Y_35(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_ROTATION_Z_36(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_SCALE_X_37(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_SCALE_Y_38(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDPARTS_SCALE_Z_39(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDLEAP_ROTATION_X_40(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDLEAP_ROTATION_Y_41(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_CHILDLEAP_ROTATION_Z_42(),
	Module_t313020820_StaticFields::get_offset_of_MODULE_LEAP_FIXED_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (ModuleMotion_t2002280314), -1, sizeof(ModuleMotion_t2002280314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3901[10] = 
{
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MODULE_REGISTERMOTION_0(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MODULE_ID_1(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_ACTION_ID_2(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MOTION_ID_3(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_4(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_ID_5(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_PATH_6(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_7(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_TIME_8(),
	ModuleMotion_t2002280314_StaticFields::get_offset_of_MOTION_ITEM_MOTION_MOTION_DATA_POSE_DATA_VALUE_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (GetCountries_t4172618058), -1, sizeof(GetCountries_t4172618058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3902[2] = 
{
	GetCountries_t4172618058_StaticFields::get_offset_of_COUNTRY_NAME_0(),
	GetCountries_t4172618058_StaticFields::get_offset_of_COUNTRY_NO_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (PostMobilityNew_t1434085915), -1, sizeof(PostMobilityNew_t1434085915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3903[1] = 
{
	PostMobilityNew_t1434085915_StaticFields::get_offset_of_MOBILITY_NEW_ID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (PostRecordingNew_t374625147), -1, sizeof(PostRecordingNew_t374625147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3904[1] = 
{
	PostRecordingNew_t374625147_StaticFields::get_offset_of_RECORDING_NEW_ID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (PostUserNew_t3265085945), -1, sizeof(PostUserNew_t3265085945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3905[1] = 
{
	PostUserNew_t3265085945_StaticFields::get_offset_of_USER_NEW_ID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (PostUserUpdate_t2575366490), -1, sizeof(PostUserUpdate_t2575366490_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3906[2] = 
{
	PostUserUpdate_t2575366490_StaticFields::get_offset_of_USER_UPDATE_RANK_0(),
	PostUserUpdate_t2575366490_StaticFields::get_offset_of_USER_UPDATE_RANKUPPOINT_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (DownLoadAssetbundle_t3666029200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3907[4] = 
{
	DownLoadAssetbundle_t3666029200::get_offset_of_file_SaveUrl_3(),
	DownLoadAssetbundle_t3666029200::get_offset_of_downLoadAssetName_4(),
	DownLoadAssetbundle_t3666029200::get_offset_of_m_preLoadingManager_5(),
	DownLoadAssetbundle_t3666029200::get_offset_of_downLoadCnt_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (U3CDownFileU3Ec__Iterator0_t1466681690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3908[7] = 
{
	U3CDownFileU3Ec__Iterator0_t1466681690::get_offset_of_url_0(),
	U3CDownFileU3Ec__Iterator0_t1466681690::get_offset_of_name_1(),
	U3CDownFileU3Ec__Iterator0_t1466681690::get_offset_of_U3CwwwU3E__0_2(),
	U3CDownFileU3Ec__Iterator0_t1466681690::get_offset_of_U24this_3(),
	U3CDownFileU3Ec__Iterator0_t1466681690::get_offset_of_U24current_4(),
	U3CDownFileU3Ec__Iterator0_t1466681690::get_offset_of_U24disposing_5(),
	U3CDownFileU3Ec__Iterator0_t1466681690::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (LoadAssetBundle_t379001212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3909[6] = 
{
	LoadAssetBundle_t379001212::get_offset_of_url_3(),
	LoadAssetBundle_t379001212::get_offset_of_version_4(),
	LoadAssetBundle_t379001212::get_offset_of_manifestName_5(),
	LoadAssetBundle_t379001212::get_offset_of_loadedAudioClip_6(),
	LoadAssetBundle_t379001212::get_offset_of_www_7(),
	LoadAssetBundle_t379001212::get_offset_of_mab_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (U3CInitializeAssetBundlePathU3Ec__Iterator0_t2797697157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3910[4] = 
{
	U3CInitializeAssetBundlePathU3Ec__Iterator0_t2797697157::get_offset_of_U24this_0(),
	U3CInitializeAssetBundlePathU3Ec__Iterator0_t2797697157::get_offset_of_U24current_1(),
	U3CInitializeAssetBundlePathU3Ec__Iterator0_t2797697157::get_offset_of_U24disposing_2(),
	U3CInitializeAssetBundlePathU3Ec__Iterator0_t2797697157::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (U3CSetAssetBundleU3Ec__Iterator1_t3527147099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3911[4] = 
{
	U3CSetAssetBundleU3Ec__Iterator1_t3527147099::get_offset_of_U24this_0(),
	U3CSetAssetBundleU3Ec__Iterator1_t3527147099::get_offset_of_U24current_1(),
	U3CSetAssetBundleU3Ec__Iterator1_t3527147099::get_offset_of_U24disposing_2(),
	U3CSetAssetBundleU3Ec__Iterator1_t3527147099::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3912[8] = 
{
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_assetBundleName_0(),
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_U3CabU3E__0_1(),
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_assetName_2(),
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_bgm_3(),
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_U24this_4(),
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_U24current_5(),
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_U24disposing_6(),
	U3CReLoadAssetBundleAsBgmU3Ec__Iterator2_t1146541457::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3913[10] = 
{
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_areaType_0(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_assetName_1(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_assetBundleName_2(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_U3CbundleLoadRequestU3E__0_3(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_U3CmyLoadedAssetbundleU3E__0_4(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_U3CassetLoadRequestU3E__1_5(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_U24this_6(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_U24current_7(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_U24disposing_8(),
	U3CReLoadAssetBundleAsAreaBgmU3Ec__Iterator3_t1683670451::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (AreaObjs_t582166865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3914[7] = 
{
	AreaObjs_t582166865::get_offset_of_PartsArea_0(),
	AreaObjs_t582166865::get_offset_of_StartObjArea_1(),
	AreaObjs_t582166865::get_offset_of_StartArea_2(),
	AreaObjs_t582166865::get_offset_of_MobilityArea_3(),
	AreaObjs_t582166865::get_offset_of_CreateArea_4(),
	AreaObjs_t582166865::get_offset_of_MiniMapArea_5(),
	AreaObjs_t582166865::get_offset_of_MyPageArea_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (CameraAreaMonitor_t370298340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3915[1] = 
{
	CameraAreaMonitor_t370298340::get_offset_of_AreaObjs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (CameraEffect_t3191348726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3916[3] = 
{
	CameraEffect_t3191348726::get_offset_of_m_FSEffect_3(),
	CameraEffect_t3191348726::get_offset_of_m_SkyBoxColorUp_4(),
	CameraEffect_t3191348726::get_offset_of_m_SkyBoxColorDw_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3917[3] = 
{
	U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964::get_offset_of_defColorUp_0(),
	U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964::get_offset_of_defColorDw_1(),
	U3CsetAreaEffectU3Ec__AnonStorey0_t3116979964::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (CameraFollow_t1493855402), -1, sizeof(CameraFollow_t1493855402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3918[23] = 
{
	CameraFollow_t1493855402_StaticFields::get_offset_of_s_fLimitTime_2(),
	CameraFollow_t1493855402_StaticFields::get_offset_of_s_fLimitDefaultTime_3(),
	CameraFollow_t1493855402_StaticFields::get_offset_of_s_fDamping_4(),
	CameraFollow_t1493855402_StaticFields::get_offset_of_s_fMoveSpeedLimit_5(),
	CameraFollow_t1493855402::get_offset_of_Enable_6(),
	CameraFollow_t1493855402::get_offset_of_m_bAuto_7(),
	CameraFollow_t1493855402::get_offset_of_m_bFollowReady_8(),
	CameraFollow_t1493855402::get_offset_of_m_bFollowActionReady_9(),
	CameraFollow_t1493855402::get_offset_of_m_bReset_10(),
	CameraFollow_t1493855402::get_offset_of_m_vCurrentPos_11(),
	CameraFollow_t1493855402::get_offset_of_m_vPrevPos_12(),
	CameraFollow_t1493855402::get_offset_of_m_vMoveDirection_13(),
	CameraFollow_t1493855402::get_offset_of_m_fTimeCountToPrevPos_14(),
	CameraFollow_t1493855402::get_offset_of_m_fTimeCountToDirection_15(),
	CameraFollow_t1493855402::get_offset_of_m_fTimeCountToDefault_16(),
	CameraFollow_t1493855402::get_offset_of_m_TargetRigidBody_17(),
	CameraFollow_t1493855402::get_offset_of_m_fPlanetRadius_18(),
	CameraFollow_t1493855402::get_offset_of_m_vPlanetPos_19(),
	CameraFollow_t1493855402::get_offset_of_m_vDirectionFromPlanetCenterToMobilmo_20(),
	CameraFollow_t1493855402::get_offset_of_m_vHeightPos_21(),
	CameraFollow_t1493855402::get_offset_of_m_fFollowDistanceOffset_22(),
	CameraFollow_t1493855402::get_offset_of_m_fHeightOffset_23(),
	CameraFollow_t1493855402::get_offset_of_leapTime_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (CameraRotater_t122375930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3919[1] = 
{
	CameraRotater_t122375930::get_offset_of_m_bRotateSwipe_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (CameraSettings_t3536359094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (CameraZoom_t1174393664), -1, sizeof(CameraZoom_t1174393664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3921[4] = 
{
	CameraZoom_t1174393664_StaticFields::get_offset_of_s_LimitZoomMin_2(),
	CameraZoom_t1174393664_StaticFields::get_offset_of_s_LimitZoomMax_3(),
	CameraZoom_t1174393664_StaticFields::get_offset_of_s_LimitZoomMinToWorld_4(),
	CameraZoom_t1174393664_StaticFields::get_offset_of_s_LimitZoomMaxToWorld_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (SubCameraHandler_t2753085375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[2] = 
{
	SubCameraHandler_t2753085375::get_offset_of_PartsPanelSliders_2(),
	SubCameraHandler_t2753085375::get_offset_of_Speed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (U3CDispSubCameraU3Ec__AnonStorey0_t3258629182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3923[2] = 
{
	U3CDispSubCameraU3Ec__AnonStorey0_t3258629182::get_offset_of_rectX_0(),
	U3CDispSubCameraU3Ec__AnonStorey0_t3258629182::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (U3CDispMainCameraU3Ec__AnonStorey1_t2482052376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3924[2] = 
{
	U3CDispMainCameraU3Ec__AnonStorey1_t2482052376::get_offset_of_vCam_0(),
	U3CDispMainCameraU3Ec__AnonStorey1_t2482052376::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (U3CHideSubCameraU3Ec__AnonStorey2_t2825804828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[2] = 
{
	U3CHideSubCameraU3Ec__AnonStorey2_t2825804828::get_offset_of_rectX_0(),
	U3CHideSubCameraU3Ec__AnonStorey2_t2825804828::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (U3CHideMainCameraU3Ec__AnonStorey3_t1339147994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3926[2] = 
{
	U3CHideMainCameraU3Ec__AnonStorey3_t1339147994::get_offset_of_vCam_0(),
	U3CHideMainCameraU3Ec__AnonStorey3_t1339147994::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (PartsCapture_t2562628060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3927[5] = 
{
	PartsCapture_t2562628060::get_offset_of_CaptureCamera_2(),
	PartsCapture_t2562628060::get_offset_of_Name_3(),
	PartsCapture_t2562628060::get_offset_of_Width_4(),
	PartsCapture_t2562628060::get_offset_of_Height_5(),
	PartsCapture_t2562628060::get_offset_of_Captures_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (U3CCaptureToOrderU3Ec__Iterator0_t2225335291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3928[7] = 
{
	U3CCaptureToOrderU3Ec__Iterator0_t2225335291::get_offset_of_U3CpartsU3E__0_0(),
	U3CCaptureToOrderU3Ec__Iterator0_t2225335291::get_offset_of_U3CiU3E__1_1(),
	U3CCaptureToOrderU3Ec__Iterator0_t2225335291::get_offset_of_U3CobjU3E__2_2(),
	U3CCaptureToOrderU3Ec__Iterator0_t2225335291::get_offset_of_U24this_3(),
	U3CCaptureToOrderU3Ec__Iterator0_t2225335291::get_offset_of_U24current_4(),
	U3CCaptureToOrderU3Ec__Iterator0_t2225335291::get_offset_of_U24disposing_5(),
	U3CCaptureToOrderU3Ec__Iterator0_t2225335291::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (U3CCaptureU3Ec__Iterator1_t3002243375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3929[10] = 
{
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U3CrtU3E__0_0(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U3CtexU3E__0_1(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U3CbytesU3E__0_2(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U3CdirPathU3E__0_3(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_name_4(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U3CsavePathU3E__0_5(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U24this_6(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U24current_7(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U24disposing_8(),
	U3CCaptureU3Ec__Iterator1_t3002243375::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (ActionLever_t131660742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3930[3] = 
{
	ActionLever_t131660742::get_offset_of_LeverUp_2(),
	ActionLever_t131660742::get_offset_of_LeverDown_3(),
	ActionLever_t131660742::get_offset_of_Levers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (ActionPartsPanelController_t3710630626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3931[10] = 
{
	ActionPartsPanelController_t3710630626::get_offset_of_m_ActionPartsUI_2(),
	ActionPartsPanelController_t3710630626::get_offset_of_m_ActionPartsPanel_3(),
	ActionPartsPanelController_t3710630626::get_offset_of_m_ActionPartsDnaPanel_4(),
	ActionPartsPanelController_t3710630626::get_offset_of_m_ActionPartsDnaUI_5(),
	ActionPartsPanelController_t3710630626::get_offset_of_m_SelectButtons_6(),
	ActionPartsPanelController_t3710630626::get_offset_of_m_SelectButtonSprite_7(),
	ActionPartsPanelController_t3710630626::get_offset_of_m_MenuButtons_8(),
	ActionPartsPanelController_t3710630626::get_offset_of_m_selectPanel_9(),
	ActionPartsPanelController_t3710630626::get_offset_of_actionCntPanel_10(),
	ActionPartsPanelController_t3710630626::get_offset_of_actionCntText_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970), -1, sizeof(U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3932[5] = 
{
	U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970::get_offset_of_U24this_0(),
	U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970::get_offset_of_U24current_1(),
	U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970::get_offset_of_U24disposing_2(),
	U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970::get_offset_of_U24PC_3(),
	U3CWaitRecreateEditModuleU3Ec__Iterator0_t3801418970_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (Lever_t866853776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3933[5] = 
{
	Lever_t866853776::get_offset_of_LeverNum_2(),
	Lever_t866853776::get_offset_of_m_Lever_3(),
	Lever_t866853776::get_offset_of_m_ActionLever_4(),
	Lever_t866853776::get_offset_of_m_fValue_5(),
	Lever_t866853776::get_offset_of_pre_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (EventDispatcher_t1543274979), -1, sizeof(EventDispatcher_t1543274979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3934[1] = 
{
	EventDispatcher_t1543274979_StaticFields::get_offset_of__listenersDict_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (EventDispatcherExtension_t2317368000), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (eEventID_t711132804)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3936[5] = 
{
	eEventID_t711132804::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (GameObjectExtensions_t1406008347), -1, sizeof(GameObjectExtensions_t1406008347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3937[2] = 
{
	GameObjectExtensions_t1406008347_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	GameObjectExtensions_t1406008347_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (U3CGetChildrenU3Ec__AnonStorey0_t1107910596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3938[1] = 
{
	U3CGetChildrenU3Ec__AnonStorey0_t1107910596::get_offset_of_self_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (U3CGetChildrenU3Ec__AnonStorey1_t2673994537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3939[1] = 
{
	U3CGetChildrenU3Ec__AnonStorey1_t2673994537::get_offset_of_self_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3940[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (ImageExtensions_t1485547703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (RawImageExtensions_t4149046211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (StringExtensions_t1398449501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (TextExtensions_t665271833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (TransformExtensions_t834738118), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (UIBehaviourExtensions_t3966032777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (U3CRemoveAllListenersU3Ec__AnonStorey0_t1887097533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3948[1] = 
{
	U3CRemoveAllListenersU3Ec__AnonStorey0_t1887097533::get_offset_of_eventID_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (CountryCodeKey_t3757133144)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3949[3] = 
{
	CountryCodeKey_t3757133144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (LocalizeKey_t3348421234)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3950[250] = 
{
	LocalizeKey_t3348421234::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (UILocalizer_t2245030347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3951[10] = 
{
	UILocalizer_t2245030347::get_offset_of_m_Key_2(),
	UILocalizer_t2245030347::get_offset_of_Text_3(),
	UILocalizer_t2245030347::get_offset_of_LetterSpacing_4(),
	UILocalizer_t2245030347::get_offset_of_Font_5(),
	UILocalizer_t2245030347::get_offset_of_Size_6(),
	UILocalizer_t2245030347::get_offset_of_Kerning_7(),
	UILocalizer_t2245030347::get_offset_of_Linespacing_8(),
	UILocalizer_t2245030347::get_offset_of_Content_9(),
	UILocalizer_t2245030347::get_offset_of_IsNotTextUpdate_10(),
	UILocalizer_t2245030347::get_offset_of_IsUsedRegexHypertext_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (LocalizeString_t1691720564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (Icon_t4013200341)+ sizeof (Il2CppObject), sizeof(Icon_t4013200341_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3953[2] = 
{
	Icon_t4013200341::get_offset_of_key_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Icon_t4013200341::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (ActionManager_t1367723175), -1, sizeof(ActionManager_t1367723175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3954[20] = 
{
	ActionManager_t1367723175::get_offset_of_ActionSetUI_3(),
	ActionManager_t1367723175::get_offset_of_Poses_4(),
	ActionManager_t1367723175::get_offset_of_ActionUI_5(),
	ActionManager_t1367723175::get_offset_of_ActionButtons_6(),
	ActionManager_t1367723175::get_offset_of_NonActionSetBackground_7(),
	ActionManager_t1367723175::get_offset_of_PoseSelect_8(),
	ActionManager_t1367723175::get_offset_of_PoseNonSelect_9(),
	ActionManager_t1367723175::get_offset_of_createCamera_10(),
	ActionManager_t1367723175::get_offset_of_ActionIconNum_11(),
	ActionManager_t1367723175::get_offset_of_IconList_12(),
	ActionManager_t1367723175::get_offset_of_m_Icons_13(),
	ActionManager_t1367723175::get_offset_of_posetex_14(),
	ActionManager_t1367723175::get_offset_of_texturePoses_15(),
	ActionManager_t1367723175::get_offset_of_m_ModuleObj_16(),
	ActionManager_t1367723175::get_offset_of_m_Module_17(),
	ActionManager_t1367723175::get_offset_of_m_sSelectPoseNum_18(),
	ActionManager_t1367723175::get_offset_of_m_sSelectActionNum_19(),
	ActionManager_t1367723175::get_offset_of_m_bIsPlay_20(),
	ActionManager_t1367723175::get_offset_of_m_ActionButtons_21(),
	ActionManager_t1367723175_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (U3CAnimPlayTimeU3Ec__Iterator0_t2431106101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3955[7] = 
{
	U3CAnimPlayTimeU3Ec__Iterator0_t2431106101::get_offset_of_U3CtimeU3E__0_0(),
	U3CAnimPlayTimeU3Ec__Iterator0_t2431106101::get_offset_of_U3CfColorValueU3E__0_1(),
	U3CAnimPlayTimeU3Ec__Iterator0_t2431106101::get_offset_of_U3CbColorReverseU3E__0_2(),
	U3CAnimPlayTimeU3Ec__Iterator0_t2431106101::get_offset_of_U24this_3(),
	U3CAnimPlayTimeU3Ec__Iterator0_t2431106101::get_offset_of_U24current_4(),
	U3CAnimPlayTimeU3Ec__Iterator0_t2431106101::get_offset_of_U24disposing_5(),
	U3CAnimPlayTimeU3Ec__Iterator0_t2431106101::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (AnalyticsManager_t1593654123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[1] = 
{
	AnalyticsManager_t1593654123::get_offset_of_googleAnalytics_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (eAUDIOBGM_t4173725093)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3957[9] = 
{
	eAUDIOBGM_t4173725093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (eAUDIOSE_t638655723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3958[19] = 
{
	eAUDIOSE_t638655723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (eAUDIOVOICE_t2836738047)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3959[14] = 
{
	eAUDIOVOICE_t2836738047::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (AudioManager_t4222704959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3960[8] = 
{
	AudioManager_t4222704959::get_offset_of_MainAudio_3(),
	AudioManager_t4222704959::get_offset_of_SelectClip_4(),
	AudioManager_t4222704959::get_offset_of_m_vMaxVolume_5(),
	AudioManager_t4222704959::get_offset_of_BGMDic_6(),
	AudioManager_t4222704959::get_offset_of_AreaBGMDic_7(),
	AudioManager_t4222704959::get_offset_of_SEDic_8(),
	AudioManager_t4222704959::get_offset_of_VoiceDic_9(),
	AudioManager_t4222704959::get_offset_of_AreaSEDic_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (U3CFeedPlayBGMU3Ec__Iterator0_t3064604589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3961[4] = 
{
	U3CFeedPlayBGMU3Ec__Iterator0_t3064604589::get_offset_of_U24this_0(),
	U3CFeedPlayBGMU3Ec__Iterator0_t3064604589::get_offset_of_U24current_1(),
	U3CFeedPlayBGMU3Ec__Iterator0_t3064604589::get_offset_of_U24disposing_2(),
	U3CFeedPlayBGMU3Ec__Iterator0_t3064604589::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (CameraManager_t2379859346), -1, sizeof(CameraManager_t2379859346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3962[32] = 
{
	CameraManager_t2379859346::get_offset_of_MainCamera_3(),
	CameraManager_t2379859346::get_offset_of_SubCamera_4(),
	CameraManager_t2379859346::get_offset_of_ModelingCamera_5(),
	CameraManager_t2379859346::get_offset_of_FrontSubCamera_6(),
	CameraManager_t2379859346::get_offset_of_MobilityCamera_7(),
	CameraManager_t2379859346::get_offset_of_PoseCaptureCamera_8(),
	CameraManager_t2379859346::get_offset_of_StartCamera_9(),
	CameraManager_t2379859346::get_offset_of_MiniMapCamera_10(),
	CameraManager_t2379859346::get_offset_of_WorldMapCamera_11(),
	CameraManager_t2379859346::get_offset_of_MiniWorldCamera_12(),
	CameraManager_t2379859346::get_offset_of_ModelingCameraCenter_13(),
	CameraManager_t2379859346::get_offset_of_MobilmoCenter_14(),
	CameraManager_t2379859346::get_offset_of_CameraParent_15(),
	CameraManager_t2379859346::get_offset_of_TitleParentPos_16(),
	CameraManager_t2379859346::get_offset_of_EditParentPos_17(),
	CameraManager_t2379859346::get_offset_of_MypageParentPos_18(),
	CameraManager_t2379859346::get_offset_of_BlurFileter_19(),
	CameraManager_t2379859346::get_offset_of_ModuleTrans_20(),
	CameraManager_t2379859346::get_offset_of_m_bSubCamera_21(),
	CameraManager_t2379859346::get_offset_of_m_bMobilityCamera_22(),
	CameraManager_t2379859346::get_offset_of_isCanLogEvevt_23(),
	CameraManager_t2379859346::get_offset_of_m_fUpDownValue_24(),
	CameraManager_t2379859346::get_offset_of_m_blur_25(),
	CameraManager_t2379859346::get_offset_of_CameraFollow_26(),
	CameraManager_t2379859346::get_offset_of_CameraZoom_27(),
	CameraManager_t2379859346::get_offset_of_CameraRotater_28(),
	CameraManager_t2379859346::get_offset_of_CameraEffect_29(),
	CameraManager_t2379859346::get_offset_of_SubCameraHandler_30(),
	CameraManager_t2379859346::get_offset_of_CameraSettings_31(),
	CameraManager_t2379859346::get_offset_of_CameraAreaMonitor_32(),
	CameraManager_t2379859346_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_33(),
	CameraManager_t2379859346_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (U3CPosisioningPanelCameraU3Ec__AnonStorey0_t977640555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3963[2] = 
{
	U3CPosisioningPanelCameraU3Ec__AnonStorey0_t977640555::get_offset_of_fCamX_0(),
	U3CPosisioningPanelCameraU3Ec__AnonStorey0_t977640555::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (ColorManager_t1666568646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3964[10] = 
{
	ColorManager_t1666568646::get_offset_of_m_selectObject_3(),
	ColorManager_t1666568646::get_offset_of_m_selectObjMaterial_4(),
	ColorManager_t1666568646::get_offset_of_m_fBright_5(),
	ColorManager_t1666568646::get_offset_of_m_fBrightSpeed_6(),
	ColorManager_t1666568646::get_offset_of_m_bSwithon_7(),
	ColorManager_t1666568646::get_offset_of_m_bColorSelect_8(),
	ColorManager_t1666568646::get_offset_of_ColorListObj_9(),
	ColorManager_t1666568646::get_offset_of_m_DefaultColorButton_10(),
	ColorManager_t1666568646::get_offset_of_ColorRGBA_11(),
	ColorManager_t1666568646::get_offset_of_TutorialSetColorNum_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (U3CColorButtonInitU3Ec__AnonStorey0_t3276323827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[2] = 
{
	U3CColorButtonInitU3Ec__AnonStorey0_t3276323827::get_offset_of_num_0(),
	U3CColorButtonInitU3Ec__AnonStorey0_t3276323827::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (ConstantManager_t2577827963), -1, sizeof(ConstantManager_t2577827963_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3966[38] = 
{
	ConstantManager_t2577827963_StaticFields::get_offset_of_CreateUserEvent_0(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_SelectCorePartEvent_1(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_ResetCameraEvent_2(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_PartsDeleteEvent_3(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_SelectPrimitivePartsEvent_4(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_ChangeColorEvent_5(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_ChangeRotateEvent_6(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_ChangeSlopeXEvent_7(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_ChangeSlopeYEvent_8(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_AIModeEvent_9(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_SelectWorldMapEvent_10(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_ChallengeEvent_11(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_LeverControlEvent_12(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_RollControlEvent_13(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_RecordTiming_14(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_EncountMobility_15(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_CreateEvent_16(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_CreateMobilmoEvent_17(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_CreateModuleEvent_18(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToWorldEvent_19(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToPartsListEvent_20(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToActionSetEvent_21(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToBilmoListEvent_22(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_EditMobilmoEvent_23(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_DeleteMobilmoEvent_24(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToNewsEvent_25(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToBilmoRaderEvent_26(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_BackToMyPageEvent_27(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToOtherEvent_28(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MoveToSearchPlayer_29(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_GetModuleEvent_30(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_RemakeModule_31(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_PrizedEventCount_32(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_UserRankUpCount_33(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_ReactionCount_34(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_TutorialFinishCount_35(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_MyPageCount_36(),
	ConstantManager_t2577827963_StaticFields::get_offset_of_RecordFootPrint_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (eCreateType_t1183446867)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3967[3] = 
{
	eCreateType_t1183446867::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (CreateTypeExtensions_t2951264486), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (CreateManager_t3918627545), -1, sizeof(CreateManager_t3918627545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3969[54] = 
{
	CreateManager_t3918627545::get_offset_of_m_PartsArea_3(),
	CreateManager_t3918627545::get_offset_of_m_CreateArea_4(),
	CreateManager_t3918627545::get_offset_of_m_MobilityArea_5(),
	CreateManager_t3918627545::get_offset_of_GridPrefab_6(),
	CreateManager_t3918627545::get_offset_of_SelectJoint_7(),
	CreateManager_t3918627545::get_offset_of_NoSelectJoint_8(),
	CreateManager_t3918627545::get_offset_of_JointLine_9(),
	CreateManager_t3918627545::get_offset_of_PartsConfigs_10(),
	CreateManager_t3918627545::get_offset_of_CrePartsScaleText_11(),
	CreateManager_t3918627545::get_offset_of_corePartsName_12(),
	CreateManager_t3918627545::get_offset_of_MobilityPhysicMaterial_13(),
	CreateManager_t3918627545::get_offset_of_Mobilmo_14(),
	CreateManager_t3918627545::get_offset_of_Module_15(),
	CreateManager_t3918627545::get_offset_of_MobilmoLimitErrorObj_16(),
	CreateManager_t3918627545::get_offset_of_ModuleLimitErrorObj_17(),
	CreateManager_t3918627545::get_offset_of_m_ModelParent_18(),
	CreateManager_t3918627545::get_offset_of_m_Core_19(),
	CreateManager_t3918627545::get_offset_of_m_Module_20(),
	CreateManager_t3918627545::get_offset_of_CreObj_21(),
	CreateManager_t3918627545::get_offset_of_m_GridObj_22(),
	CreateManager_t3918627545::get_offset_of_m_SelectParts_23(),
	CreateManager_t3918627545::get_offset_of_m_eCreateType_24(),
	CreateManager_t3918627545::get_offset_of_rollCount_25(),
	CreateManager_t3918627545::get_offset_of_m_parentModNum_26(),
	CreateManager_t3918627545::get_offset_of_MobilmoConect_27(),
	CreateManager_t3918627545::get_offset_of_ModuleConect_28(),
	CreateManager_t3918627545::get_offset_of_m_ClickJoint_29(),
	CreateManager_t3918627545::get_offset_of_m_ParentParts_30(),
	CreateManager_t3918627545::get_offset_of_m_sParentJointNum_31(),
	CreateManager_t3918627545::get_offset_of_m_ChildParts_32(),
	CreateManager_t3918627545::get_offset_of_m_sChildJointNum_33(),
	CreateManager_t3918627545::get_offset_of_m_sPartsNo_34(),
	CreateManager_t3918627545::get_offset_of_m_RotateInput_35(),
	CreateManager_t3918627545::get_offset_of_m_ColorMgr_36(),
	CreateManager_t3918627545::get_offset_of_m_MobilmoPanel_37(),
	CreateManager_t3918627545::get_offset_of_m_ModulePanel_38(),
	CreateManager_t3918627545::get_offset_of_m_SubCameraHandler_39(),
	CreateManager_t3918627545::get_offset_of_m_createModuleId_40(),
	CreateManager_t3918627545::get_offset_of_childPartsNo_41(),
	CreateManager_t3918627545::get_offset_of_m_sTutorialStep_42(),
	CreateManager_t3918627545_StaticFields::get_offset_of_LimitPartsNum_43(),
	CreateManager_t3918627545::get_offset_of_LimitOverNum_44(),
	CreateManager_t3918627545::get_offset_of_FlashingTWDic_45(),
	CreateManager_t3918627545::get_offset_of_isEditing_46(),
	CreateManager_t3918627545::get_offset_of_DragSelectTran_47(),
	CreateManager_t3918627545::get_offset_of_modCnt_48(),
	CreateManager_t3918627545::get_offset_of_m_parentMoudleId_49(),
	CreateManager_t3918627545::get_offset_of__keys_50(),
	CreateManager_t3918627545::get_offset_of_moduleCnt_51(),
	CreateManager_t3918627545_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_52(),
	CreateManager_t3918627545_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_53(),
	CreateManager_t3918627545_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_54(),
	CreateManager_t3918627545_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_55(),
	CreateManager_t3918627545_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (U3C_AnchorSettingU3Ec__Iterator0_t2219086597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3970[5] = 
{
	U3C_AnchorSettingU3Ec__Iterator0_t2219086597::get_offset_of_conJoint_0(),
	U3C_AnchorSettingU3Ec__Iterator0_t2219086597::get_offset_of_value_1(),
	U3C_AnchorSettingU3Ec__Iterator0_t2219086597::get_offset_of_U24current_2(),
	U3C_AnchorSettingU3Ec__Iterator0_t2219086597::get_offset_of_U24disposing_3(),
	U3C_AnchorSettingU3Ec__Iterator0_t2219086597::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (U3CObjSelectU3Ec__Iterator1_t2382599615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3971[6] = 
{
	U3CObjSelectU3Ec__Iterator1_t2382599615::get_offset_of_partsObj_0(),
	U3CObjSelectU3Ec__Iterator1_t2382599615::get_offset_of_U3CjointParentsU3E__0_1(),
	U3CObjSelectU3Ec__Iterator1_t2382599615::get_offset_of_U24this_2(),
	U3CObjSelectU3Ec__Iterator1_t2382599615::get_offset_of_U24current_3(),
	U3CObjSelectU3Ec__Iterator1_t2382599615::get_offset_of_U24disposing_4(),
	U3CObjSelectU3Ec__Iterator1_t2382599615::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (U3CEditPartsRotationDataU3Ec__Iterator2_t477756611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3972[4] = 
{
	U3CEditPartsRotationDataU3Ec__Iterator2_t477756611::get_offset_of_selectPartsTran_0(),
	U3CEditPartsRotationDataU3Ec__Iterator2_t477756611::get_offset_of_U24current_1(),
	U3CEditPartsRotationDataU3Ec__Iterator2_t477756611::get_offset_of_U24disposing_2(),
	U3CEditPartsRotationDataU3Ec__Iterator2_t477756611::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3973[5] = 
{
	U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042::get_offset_of_selectPartsObj_0(),
	U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042::get_offset_of_selectPartsTran_1(),
	U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042::get_offset_of_U24current_2(),
	U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042::get_offset_of_U24disposing_3(),
	U3CEditModuleRotationDataU3Ec__Iterator3_t2496885042::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3974[7] = 
{
	U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617::get_offset_of_U3CisHavedU3E__0_0(),
	U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617::get_offset_of_U24locvar0_1(),
	U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617::get_offset_of_U24locvar1_2(),
	U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617::get_offset_of_U24this_3(),
	U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617::get_offset_of_U24current_4(),
	U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617::get_offset_of_U24disposing_5(),
	U3CCheckMobilmoOfPartsU3Ec__Iterator4_t1636254617::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (U3CFlashingAlphaU3Ec__AnonStorey5_t3181075276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3975[2] = 
{
	U3CFlashingAlphaU3Ec__AnonStorey5_t3181075276::get_offset_of_mats_0(),
	U3CFlashingAlphaU3Ec__AnonStorey5_t3181075276::get_offset_of_defColor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (GameManager_t2252321495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3976[13] = 
{
	GameManager_t2252321495::get_offset_of_TitleAnimation_3(),
	GameManager_t2252321495::get_offset_of_CameraFollow_4(),
	GameManager_t2252321495::get_offset_of_SkipRegisterAnimation_5(),
	GameManager_t2252321495::get_offset_of_SkipDynamicTutorial_6(),
	GameManager_t2252321495::get_offset_of_DebugLogON_7(),
	GameManager_t2252321495::get_offset_of_m_TitleAnimator_8(),
	GameManager_t2252321495::get_offset_of_m_CameraFollow_9(),
	GameManager_t2252321495::get_offset_of_m_bgObj_10(),
	GameManager_t2252321495::get_offset_of_m_miniGameCharas_11(),
	GameManager_t2252321495::get_offset_of_allLightSources_12(),
	GameManager_t2252321495::get_offset_of_allRigidbodys_13(),
	GameManager_t2252321495::get_offset_of_allColliders_14(),
	GameManager_t2252321495::get_offset_of_m_collisionObj_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (InputManager_t1610719423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3977[10] = 
{
	InputManager_t1610719423::get_offset_of_m_StartDis_3(),
	InputManager_t1610719423::get_offset_of_m_fPinch_4(),
	InputManager_t1610719423::get_offset_of_m_vSwipeX_5(),
	InputManager_t1610719423::get_offset_of_m_vSwipeY_6(),
	InputManager_t1610719423::get_offset_of_m_vPrevClickPos_7(),
	InputManager_t1610719423::get_offset_of_m_vFlickX_8(),
	InputManager_t1610719423::get_offset_of_m_vFlickY_9(),
	InputManager_t1610719423::get_offset_of_m_vPrevMousePos_10(),
	InputManager_t1610719423::get_offset_of_m_bIsDoubleTap_11(),
	InputManager_t1610719423::get_offset_of_m_fDoubleTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (eLIGHTTYPE_t1469766701)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3978[7] = 
{
	eLIGHTTYPE_t1469766701::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (lightObj_t383130471)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3979[2] = 
{
	lightObj_t383130471::get_offset_of_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	lightObj_t383130471::get_offset_of_light_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (LightManager_t3427519171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3980[5] = 
{
	LightManager_t3427519171::get_offset_of_LightList_3(),
	LightManager_t3427519171::get_offset_of_lightDic_4(),
	LightManager_t3427519171::get_offset_of_defaultPosition_5(),
	LightManager_t3427519171::get_offset_of_defaultLightPosition_6(),
	LightManager_t3427519171::get_offset_of_defaultLightRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (LoadingManager_t2398813851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3981[3] = 
{
	LoadingManager_t2398813851::get_offset_of_Navit_3(),
	LoadingManager_t2398813851::get_offset_of_NavitSpeed_4(),
	LoadingManager_t2398813851::get_offset_of_m_vRotNavit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (LocalizeTextContents_t1443703366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3982[5] = 
{
	LocalizeTextContents_t1443703366::get_offset_of_Font_0(),
	LocalizeTextContents_t1443703366::get_offset_of_Size_1(),
	LocalizeTextContents_t1443703366::get_offset_of_Kerning_2(),
	LocalizeTextContents_t1443703366::get_offset_of_Linespacing_3(),
	LocalizeTextContents_t1443703366::get_offset_of_Content_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (LocalizeItem_t998829008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3983[2] = 
{
	LocalizeItem_t998829008::get_offset_of_Key_0(),
	LocalizeItem_t998829008::get_offset_of_Item_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (LocalizeManager_t1264687742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3984[12] = 
{
	LocalizeManager_t1264687742::get_offset_of_DefaultSheet_3(),
	LocalizeManager_t1264687742::get_offset_of_CountrySheet_4(),
	LocalizeManager_t1264687742::get_offset_of_LanguageSheet_5(),
	LocalizeManager_t1264687742::get_offset_of_Fonts_6(),
	LocalizeManager_t1264687742::get_offset_of_UnusedKernig_7(),
	LocalizeManager_t1264687742::get_offset_of_LanguageNo_8(),
	LocalizeManager_t1264687742::get_offset_of_m_DefaultCountryCode_9(),
	LocalizeManager_t1264687742::get_offset_of_m_CurrentCountryKey_10(),
	LocalizeManager_t1264687742::get_offset_of_m_DefaultCodeSheetData_11(),
	LocalizeManager_t1264687742::get_offset_of_m_CountryCodeSheetData_12(),
	LocalizeManager_t1264687742::get_offset_of_m_LanguageSheetData_13(),
	LocalizeManager_t1264687742::get_offset_of_m_LocalizeTextList_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (MINIGAME_t3871825659)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3985[4] = 
{
	MINIGAME_t3871825659::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (SCORETYPE_t2692914394)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3986[3] = 
{
	SCORETYPE_t2692914394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (MiniGameManager_t849864952), -1, sizeof(MiniGameManager_t849864952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3987[18] = 
{
	MiniGameManager_t849864952::get_offset_of_Text_GameTitle_3(),
	MiniGameManager_t849864952::get_offset_of_Text_GameDescription_4(),
	MiniGameManager_t849864952::get_offset_of_playContents_5(),
	MiniGameManager_t849864952::get_offset_of_ClearPannel_6(),
	MiniGameManager_t849864952::get_offset_of_GameOverPannel_7(),
	MiniGameManager_t849864952::get_offset_of_ResultChallengerValue_8(),
	MiniGameManager_t849864952::get_offset_of_ResultChampionValue_9(),
	MiniGameManager_t849864952::get_offset_of_GameOverText_10(),
	MiniGameManager_t849864952::get_offset_of_m_bIsGamePlay_11(),
	MiniGameManager_t849864952::get_offset_of_IsGameFinish_12(),
	MiniGameManager_t849864952::get_offset_of_RankingObj_13(),
	MiniGameManager_t849864952::get_offset_of_MobilityName_14(),
	MiniGameManager_t849864952::get_offset_of_UserName_15(),
	MiniGameManager_t849864952::get_offset_of_RankerTime_16(),
	MiniGameManager_t849864952::get_offset_of_PrizedType_17(),
	MiniGameManager_t849864952::get_offset_of_gameChara_18(),
	MiniGameManager_t849864952::get_offset_of_isCanTap_19(),
	MiniGameManager_t849864952_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (U3CMiniGameOverDialogU3Ec__AnonStorey0_t214630277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3988[2] = 
{
	U3CMiniGameOverDialogU3Ec__AnonStorey0_t214630277::get_offset_of_ReasonText_0(),
	U3CMiniGameOverDialogU3Ec__AnonStorey0_t214630277::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (MobilmoManager_t1293766190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3989[31] = 
{
	MobilmoManager_t1293766190::get_offset_of_mobilmoJson_3(),
	MobilmoManager_t1293766190::get_offset_of_myMobilmoJson_4(),
	MobilmoManager_t1293766190::get_offset_of_recreatedModuleObj_5(),
	MobilmoManager_t1293766190::get_offset_of_recoredModuleObj_6(),
	MobilmoManager_t1293766190::get_offset_of_worldMobilmoObj_7(),
	MobilmoManager_t1293766190::get_offset_of_testJson_8(),
	MobilmoManager_t1293766190::get_offset_of_recMobilityId_9(),
	MobilmoManager_t1293766190::get_offset_of_nearmobObjList_10(),
	MobilmoManager_t1293766190::get_offset_of_deleteModuleList_11(),
	MobilmoManager_t1293766190::get_offset_of_recMobilmo_12(),
	MobilmoManager_t1293766190::get_offset_of_m_moduleCount_13(),
	MobilmoManager_t1293766190::get_offset_of_m_recModuleCount_14(),
	MobilmoManager_t1293766190::get_offset_of_moduleScale_15(),
	MobilmoManager_t1293766190::get_offset_of_reRecoredTimer_16(),
	MobilmoManager_t1293766190::get_offset_of_m_iPartsCount_17(),
	MobilmoManager_t1293766190::get_offset_of_isTouchRecoredObject_18(),
	MobilmoManager_t1293766190::get_offset_of_m_EditMobilmo_19(),
	MobilmoManager_t1293766190::get_offset_of_reModCnt_20(),
	MobilmoManager_t1293766190::get_offset_of_mobilityCnt_21(),
	MobilmoManager_t1293766190::get_offset_of_mobilmo_22(),
	MobilmoManager_t1293766190::get_offset_of_childPartsObj_23(),
	MobilmoManager_t1293766190::get_offset_of_ChildCenter_24(),
	MobilmoManager_t1293766190::get_offset_of_ChildLeap_25(),
	MobilmoManager_t1293766190::get_offset_of_ChildRoll_26(),
	MobilmoManager_t1293766190::get_offset_of_ModuleCenter_27(),
	MobilmoManager_t1293766190::get_offset_of_partsNameAL_28(),
	MobilmoManager_t1293766190::get_offset_of_m_mobilmoCount_29(),
	MobilmoManager_t1293766190::get_offset_of_myMobilmoArea_30(),
	MobilmoManager_t1293766190::get_offset_of_recordingPoseDataCount_31(),
	MobilmoManager_t1293766190::get_offset_of_recordedChildPartsObj_32(),
	MobilmoManager_t1293766190::get_offset_of_isRecreateRecoredObjEnd_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (U3CreCreateMobilmoU3Ec__Iterator0_t1623790344), -1, sizeof(U3CreCreateMobilmoU3Ec__Iterator0_t1623790344_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3990[38] = 
{
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_json_0(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3C_getMobilityModelU3E__0_2(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CmobilmoU3E__0_3(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CmobilmoRigidU3E__0_4(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3Cm_mobilimoU3E__0_5(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CmobilFixedU3E__0_6(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar0_7(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CcoreChildObjU3E__0_8(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3Cm_CreObjU3E__0_9(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CcoreMatU3E__0_10(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar1_11(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CchildU3E__1_12(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CModuleCenterU3E__2_13(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CModuleLeapU3E__2_14(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CModuleRollU3E__2_15(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CmoduleMgrU3E__2_16(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CparentPartsJointNoU3E__2_17(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CmoduleJsonU3E__2_18(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CmodulePartsCntU3E__2_19(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CpartsIdIndexU3E__2_20(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CpartsIdListU3E__2_21(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CmodulePartsListU3E__2_22(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar6_23(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar7_24(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar8_25(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar9_26(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U3CjointParentsU3E__0_27(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar12_28(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar13_29(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar14_30(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24locvar15_31(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_callback_32(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24this_33(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24current_34(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24disposing_35(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344::get_offset_of_U24PC_36(),
	U3CreCreateMobilmoU3Ec__Iterator0_t1623790344_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203), -1, sizeof(U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3991[37] = 
{
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_json_0(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3C_getMyMobilityModelU3E__0_2(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmobilityListU3E__0_3(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_isRader_4(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24locvar0_5(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CitemModelU3E__1_6(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmobilmoU3E__2_7(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmobilmoRigidU3E__2_8(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3Cm_mobilimoU3E__2_9(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmobilFixedU3E__2_10(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24locvar1_11(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CChildObjU3E__2_12(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3Cm_CreObjU3E__2_13(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CcoreMatU3E__2_14(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24locvar2_15(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CchildU3E__3_16(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CModuleLeapU3E__4_17(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CModuleRollU3E__4_18(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmoduleMgrU3E__4_19(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmoduleJsonU3E__4_20(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CparentPartsJointNoU3E__4_21(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CrecModuleRigdU3E__4_22(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmodulePartsCntU3E__4_23(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CpartsIdIndexU3E__4_24(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CpartsIdListU3E__4_25(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CmodulePartsListU3E__4_26(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24locvar7_27(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24locvar8_28(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24locvar9_29(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24locvarA_30(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U3CjointParentsU3E__2_31(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24this_32(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24current_33(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24disposing_34(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203::get_offset_of_U24PC_35(),
	U3CreCreateMyMobilmoU3Ec__Iterator1_t4084074203_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739), -1, sizeof(U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3992[40] = 
{
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_json_0(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3C_getNearModelU3E__0_2(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar0_3(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CitemModelU3E__1_4(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CcreateUserIdU3E__2_5(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CmobilmoU3E__2_6(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CmobilmoRigidU3E__2_7(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3Cm_mobilimoU3E__2_8(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CmobilFixedU3E__2_9(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CcoreChildObjU3E__2_10(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3Cm_CreObjU3E__2_11(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CcoreMatU3E__2_12(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar1_13(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CchildU3E__3_14(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CreModuleCenterU3E__4_15(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CmoduleJsonU3E__4_16(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CModuleLeapU3E__4_17(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CModuleRollU3E__4_18(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CmoduleMgrU3E__4_19(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CparentPartsJointNoU3E__4_20(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar8_21(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar9_22(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar12_23(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar13_24(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar14_25(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar15_26(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar16_27(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar17_28(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CrecMobilmoStatusU3E__2_29(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CjointParentsU3E__2_30(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U3CpartsTraU3E__2_31(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar18_32(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar19_33(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24locvar1A_34(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24this_35(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24current_36(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24disposing_37(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739::get_offset_of_U24PC_38(),
	U3CrecordingRecreateMobilityU3Ec__Iterator2_t3985505739_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177), -1, sizeof(U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3993[35] = 
{
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_json_0(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3C_getMobilityModelU3E__0_2(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CmobilmoU3E__0_3(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CmobilmoRigidU3E__0_4(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3Cm_mobilimoU3E__0_5(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CmobilFixedU3E__0_6(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar0_7(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CcoreChildObjU3E__0_8(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3Cm_CreObjU3E__0_9(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CcoreMatU3E__0_10(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar1_11(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CchildU3E__1_12(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CModuleCenterU3E__2_13(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CModuleLeapU3E__2_14(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CModuleRollU3E__2_15(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CmoduleMgrU3E__2_16(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CparentPartsJointNoU3E__2_17(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CmoduleJsonU3E__2_18(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CmodulePartsCntU3E__2_19(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CpartsIdIndexU3E__2_20(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CpartsIdListU3E__2_21(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CmodulePartsListU3E__2_22(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar6_23(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar7_24(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar8_25(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar9_26(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U3CjointParentsU3E__0_27(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar12_28(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24locvar13_29(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24this_30(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24current_31(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24disposing_32(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177::get_offset_of_U24PC_33(),
	U3CreCreateActionSetingMobilmoU3Ec__Iterator3_t2372253177_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (eMYMODULETYPE_t2778483377)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3994[5] = 
{
	eMYMODULETYPE_t2778483377::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (ModuleTypeExtensions_t3787989386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (ModuleManager_t1065445307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3996[37] = 
{
	ModuleManager_t1065445307::get_offset_of_moduleObj_3(),
	ModuleManager_t1065445307::get_offset_of_corePartsObj_4(),
	ModuleManager_t1065445307::get_offset_of__moduleObject_5(),
	ModuleManager_t1065445307::get_offset_of_m_getModuleObject_6(),
	ModuleManager_t1065445307::get_offset_of_moduleDic_7(),
	ModuleManager_t1065445307::get_offset_of_defaultModuleDic_8(),
	ModuleManager_t1065445307::get_offset_of_json_9(),
	ModuleManager_t1065445307::get_offset_of_myModuleArea_10(),
	ModuleManager_t1065445307::get_offset_of_actionPartsPanel_11(),
	ModuleManager_t1065445307::get_offset_of_selectModuleArea_12(),
	ModuleManager_t1065445307::get_offset_of_selectActionPartsPanel_13(),
	ModuleManager_t1065445307::get_offset_of_dnaActionPartsArea_14(),
	ModuleManager_t1065445307::get_offset_of_dnaActionPartsContent_15(),
	ModuleManager_t1065445307::get_offset_of_modulePoseDataCount_16(),
	ModuleManager_t1065445307::get_offset_of_defJsonText_17(),
	ModuleManager_t1065445307::get_offset_of_defualtModuleJsons_18(),
	ModuleManager_t1065445307::get_offset_of_editActionPartsArea_19(),
	ModuleManager_t1065445307::get_offset_of_defualtModuleObj_20(),
	ModuleManager_t1065445307::get_offset_of_createdModuleObj_21(),
	ModuleManager_t1065445307::get_offset_of_editModuleObj_22(),
	ModuleManager_t1065445307::get_offset_of_moduleCoreParts_23(),
	ModuleManager_t1065445307::get_offset_of_isCreatingNewModule_24(),
	ModuleManager_t1065445307::get_offset_of_isPlayingInActionPanel_25(),
	ModuleManager_t1065445307::get_offset_of_isPlayingInCreatePanel_26(),
	ModuleManager_t1065445307::get_offset_of_selectModuleId_27(),
	ModuleManager_t1065445307::get_offset_of_getModuleArea_28(),
	ModuleManager_t1065445307::get_offset_of_getModulePanelContent_29(),
	ModuleManager_t1065445307::get_offset_of_getModuleCnt_30(),
	ModuleManager_t1065445307::get_offset_of_moduleJsonList_31(),
	ModuleManager_t1065445307::get_offset_of_isClickedActionParts_32(),
	ModuleManager_t1065445307::get_offset_of_m_actionPartsPanelController_33(),
	ModuleManager_t1065445307::get_offset_of_isDeletedModule_34(),
	ModuleManager_t1065445307::get_offset_of_moduleScale_35(),
	ModuleManager_t1065445307::get_offset_of_actionPartsListMenuButtons_36(),
	ModuleManager_t1065445307::get_offset_of_cnt_37(),
	ModuleManager_t1065445307::get_offset_of_partsCount_38(),
	ModuleManager_t1065445307::get_offset_of_m_oldAreaImage_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3997[7] = 
{
	U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157::get_offset_of_U24locvar0_0(),
	U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157::get_offset_of_U24locvar1_1(),
	U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157::get_offset_of_U3CjsonU3E__1_2(),
	U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157::get_offset_of_U24this_3(),
	U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157::get_offset_of_U24current_4(),
	U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157::get_offset_of_U24disposing_5(),
	U3CWaitCreateDefaultModuleU3Ec__Iterator0_t3833490157::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (U3CbeforehandCreateModuleU3Ec__Iterator1_t924205912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3998[6] = 
{
	U3CbeforehandCreateModuleU3Ec__Iterator1_t924205912::get_offset_of_U24locvar0_0(),
	U3CbeforehandCreateModuleU3Ec__Iterator1_t924205912::get_offset_of_U3CjsonU3E__1_1(),
	U3CbeforehandCreateModuleU3Ec__Iterator1_t924205912::get_offset_of_U24this_2(),
	U3CbeforehandCreateModuleU3Ec__Iterator1_t924205912::get_offset_of_U24current_3(),
	U3CbeforehandCreateModuleU3Ec__Iterator1_t924205912::get_offset_of_U24disposing_4(),
	U3CbeforehandCreateModuleU3Ec__Iterator1_t924205912::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (U3CreCreateModuleU3Ec__Iterator2_t2731074232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3999[5] = 
{
	U3CreCreateModuleU3Ec__Iterator2_t2731074232::get_offset_of_key_0(),
	U3CreCreateModuleU3Ec__Iterator2_t2731074232::get_offset_of_U24this_1(),
	U3CreCreateModuleU3Ec__Iterator2_t2731074232::get_offset_of_U24current_2(),
	U3CreCreateModuleU3Ec__Iterator2_t2731074232::get_offset_of_U24disposing_3(),
	U3CreCreateModuleU3Ec__Iterator2_t2731074232::get_offset_of_U24PC_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
