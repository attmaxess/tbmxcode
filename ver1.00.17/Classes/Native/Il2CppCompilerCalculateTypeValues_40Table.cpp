﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CwaitCreateModule625301358.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CreCreateMyModul3947313414.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CclickActionPart1003365316.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CreCreateMyModul3089021601.h"
#include "AssemblyU2DCSharp_ModuleManager_U3C_CreateActionPa4141216334.h"
#include "AssemblyU2DCSharp_ModuleManager_U3C_CreatePartsPane300757049.h"
#include "AssemblyU2DCSharp_ModuleManager_U3CreCreateModuleO3298334019.h"
#include "AssemblyU2DCSharp_PlayerMobilmoManager2341644285.h"
#include "AssemblyU2DCSharp_PreloadingManager3491463554.h"
#include "AssemblyU2DCSharp_PreloadingManager_U3CPreloadingU3199063565.h"
#include "AssemblyU2DCSharp_PrizedManager3323701717.h"
#include "AssemblyU2DCSharp_RayManager3612671535.h"
#include "AssemblyU2DCSharp_RayManager_OPERATION_TYPE1833855786.h"
#include "AssemblyU2DCSharp_SceneFadeManager1728942829.h"
#include "AssemblyU2DCSharp_SceneFadeManager_OnComplete2805533292.h"
#include "AssemblyU2DCSharp_SceneFadeManager_U3C_FadeUpdateU1411678873.h"
#include "AssemblyU2DCSharp_eSCENETYPE4122992453.h"
#include "AssemblyU2DCSharp_SceneManager1824971699.h"
#include "AssemblyU2DCSharp_SceneManager_U3C_FPSCheckU3Ec__It130709559.h"
#include "AssemblyU2DCSharp_TeleportManager4218394618.h"
#include "AssemblyU2DCSharp_TutorialManager2168024773.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CLeverandRollFin31138930.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CCircleSetU3Ec2608721195.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CCircleSetU3Ec2608721196.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CCircleSetLoop3353285022.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CFingerSetU3Ec1674958284.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CFingerSetU3Ec1674958283.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CFingerSetU3Ec1674958290.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CJointCircleU31644323487.h"
#include "AssemblyU2DCSharp_TutorialManager_U3CDistanceMeasu2793382657.h"
#include "AssemblyU2DCSharp_UIObject4279159643.h"
#include "AssemblyU2DCSharp_UIManager2519183485.h"
#include "AssemblyU2DCSharp_UIManager_U3CUIChangeU3Ec__AnonS3981264818.h"
#include "AssemblyU2DCSharp_UIManager_U3CUIAdditiveU3Ec__Ano1385390251.h"
#include "AssemblyU2DCSharp_UIManager_U3CwaitCloseLoadingUIU3507306122.h"
#include "AssemblyU2DCSharp_eAREATYPE1739175762.h"
#include "AssemblyU2DCSharp_LonLatitude2793245363.h"
#include "AssemblyU2DCSharp_AreaParentObject3918549470.h"
#include "AssemblyU2DCSharp_WorldManager3923509627.h"
#include "AssemblyU2DCSharp_WorldManager_U3CRandomRockAreaU31715171824.h"
#include "AssemblyU2DCSharp_WorldMapManager3256198739.h"
#include "AssemblyU2DCSharp_EditMobilmo2535788613.h"
#include "AssemblyU2DCSharp_EditMobilmo_U3CreEditMobilmoU3Ec4018953849.h"
#include "AssemblyU2DCSharp_EditMobilmo_U3CWaitEditMobilmoU32383036814.h"
#include "AssemblyU2DCSharp_Joint826707408.h"
#include "AssemblyU2DCSharp_MobPhysics1476770297.h"
#include "AssemblyU2DCSharp_Mobilmo370754809.h"
#include "AssemblyU2DCSharp_Mobilmo_U3CchangeMotionU3Ec__Ite1783254865.h"
#include "AssemblyU2DCSharp_Module3140434828.h"
#include "AssemblyU2DCSharp_Parts3804168686.h"
#include "AssemblyU2DCSharp_Parts_SaveMobilityJsonData2430892781.h"
#include "AssemblyU2DCSharp_Parts_SaveModuleChildPartsData3597262878.h"
#include "AssemblyU2DCSharp_PartsConfig872073448.h"
#include "AssemblyU2DCSharp_PartsConfig_U3C_CreatePartsConfi1113296555.h"
#include "AssemblyU2DCSharp_SlideInOut958296806.h"
#include "AssemblyU2DCSharp_eSCALINGSELECTSTATE1537437653.h"
#include "AssemblyU2DCSharp_DoubleTapPartsScaling180404427.h"
#include "AssemblyU2DCSharp_ActionButtons2868854693.h"
#include "AssemblyU2DCSharp_ActionButtons_U3CCreateActionIte2884540407.h"
#include "AssemblyU2DCSharp_ActionConfigPanel3304127690.h"
#include "AssemblyU2DCSharp_ActionIconList2910912037.h"
#include "AssemblyU2DCSharp_Contents3730146386.h"
#include "AssemblyU2DCSharp_ActionListContetns3613658512.h"
#include "AssemblyU2DCSharp_ActionSetContetns415653720.h"
#include "AssemblyU2DCSharp_ActionObj2993311507.h"
#include "AssemblyU2DCSharp_ActionSet264428022.h"
#include "AssemblyU2DCSharp_ActionSet_U3CPlayingMovieU3Ec__I3713250252.h"
#include "AssemblyU2DCSharp_Pose686168059.h"
#include "AssemblyU2DCSharp_BilmoListPanel1619006723.h"
#include "AssemblyU2DCSharp_ButtonOnClickChangeScene4293177719.h"
#include "AssemblyU2DCSharp_CloneActionParts3277884483.h"
#include "AssemblyU2DCSharp_CoreContent4281623508.h"
#include "AssemblyU2DCSharp_CoreObj3180529948.h"
#include "AssemblyU2DCSharp_CoreSelect1755339513.h"
#include "AssemblyU2DCSharp_CreateGrid2336839420.h"
#include "AssemblyU2DCSharp_CreateModeSelect1995266573.h"
#include "AssemblyU2DCSharp_DemoWorld457704051.h"
#include "AssemblyU2DCSharp_MobilmoPartsConectContents1795215227.h"
#include "AssemblyU2DCSharp_MobilimoPartsConect3466715044.h"
#include "AssemblyU2DCSharp_MobilimoPartsConect_U3CwaitOpenP2029102973.h"
#include "AssemblyU2DCSharp_MobilmoPanel222294755.h"
#include "AssemblyU2DCSharp_ModulePanel1015180434.h"
#include "AssemblyU2DCSharp_ModulePartsConectContents560460772.h"
#include "AssemblyU2DCSharp_ModulePartsConect3833785310.h"
#include "AssemblyU2DCSharp_ModulePartsConect_U3CPlayingMovie111206440.h"
#include "AssemblyU2DCSharp_PremitivePartContent3696216029.h"
#include "AssemblyU2DCSharp_PartsManager197384025.h"
#include "AssemblyU2DCSharp_PartsPanelSliders4209932670.h"
#include "AssemblyU2DCSharp_PartsScroll2740354319.h"
#include "AssemblyU2DCSharp_PartsScroll_U3CCreatePartsSelectU176489707.h"
#include "AssemblyU2DCSharp_PartsScroll_U3CCreatePartsSelect4248801268.h"
#include "AssemblyU2DCSharp_PartsSelector3170319883.h"
#include "AssemblyU2DCSharp_PartsViewPanel2613620109.h"
#include "AssemblyU2DCSharp_RotationController3824016438.h"
#include "AssemblyU2DCSharp_SelectPartsPanel3077390372.h"
#include "AssemblyU2DCSharp_Toast3649705739.h"
#include "AssemblyU2DCSharp_Toast_U3CwaitReturnOtherU3Ec__It1809526660.h"
#include "AssemblyU2DCSharp_Toast_U3CwaitHideMessageU3Ec__It3715463830.h"
#include "AssemblyU2DCSharp_MobilityChildItem544399382.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4000[16] = 
{
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_json_0(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U3C_moduleModelU3E__0_2(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U3CchildCoreObjU3E__1_3(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U3CcoreMatU3E__1_4(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_isCreatingMobInMyPage_5(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24locvar0_6(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_isRecorded_7(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U3CpartsTraU3E__1_8(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24locvar3_9(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24locvar4_10(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24locvar5_11(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24this_12(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24current_13(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24disposing_14(),
	U3CwaitCreateModuleObjectU3Ec__Iterator3_t625301358::get_offset_of_U24PC_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4001[5] = 
{
	U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414::get_offset_of_U24locvar0_0(),
	U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414::get_offset_of_U24this_1(),
	U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414::get_offset_of_U24current_2(),
	U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414::get_offset_of_U24disposing_3(),
	U3CreCreateMyModuleInMyPageU3Ec__Iterator4_t3947313414::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4002[8] = 
{
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_obj_0(),
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_U24locvar0_1(),
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_U24locvar1_2(),
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_U3CTestAnimU3E__0_3(),
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_U24this_4(),
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_U24current_5(),
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_U24disposing_6(),
	U3CclickActionPartsInMyPageU3Ec__Iterator5_t1003365316::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (U3CreCreateMyModuleInCreatePanelU3Ec__Iterator6_t3089021601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4003[4] = 
{
	U3CreCreateMyModuleInCreatePanelU3Ec__Iterator6_t3089021601::get_offset_of_U24this_0(),
	U3CreCreateMyModuleInCreatePanelU3Ec__Iterator6_t3089021601::get_offset_of_U24current_1(),
	U3CreCreateMyModuleInCreatePanelU3Ec__Iterator6_t3089021601::get_offset_of_U24disposing_2(),
	U3CreCreateMyModuleInCreatePanelU3Ec__Iterator6_t3089021601::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (U3C_CreateActionPartsPanelScrollObjectU3Ec__AnonStorey8_t4141216334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4004[4] = 
{
	U3C_CreateActionPartsPanelScrollObjectU3Ec__AnonStorey8_t4141216334::get_offset_of_actionPartsObj_0(),
	U3C_CreateActionPartsPanelScrollObjectU3Ec__AnonStorey8_t4141216334::get_offset_of_moduleAera_1(),
	U3C_CreateActionPartsPanelScrollObjectU3Ec__AnonStorey8_t4141216334::get_offset_of_obj_2(),
	U3C_CreateActionPartsPanelScrollObjectU3Ec__AnonStorey8_t4141216334::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4005[3] = 
{
	U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049::get_offset_of_actionPartsObj_0(),
	U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049::get_offset_of_obj_1(),
	U3C_CreatePartsPanelScrollObjectU3Ec__AnonStorey9_t300757049::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4006[18] = 
{
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_json_0(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U3C_moduleModelU3E__0_2(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U3CmoduleObjU3E__1_3(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U3CchildCoreObjU3E__1_4(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U3CcorePartsObjU3E__1_5(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U3CcoreMatU3E__1_6(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_isCreatingMobInMyPage_7(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24locvar0_8(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_isRecorded_9(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U3CpartsTraU3E__1_10(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24locvar3_11(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24locvar4_12(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24locvar5_13(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24this_14(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24current_15(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24disposing_16(),
	U3CreCreateModuleObjectInTitleU3Ec__Iterator7_t3298334019::get_offset_of_U24PC_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (PlayerMobilmoManager_t2341644285), -1, sizeof(PlayerMobilmoManager_t2341644285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4007[42] = 
{
	PlayerMobilmoManager_t2341644285::get_offset_of_ActionUI_3(),
	PlayerMobilmoManager_t2341644285::get_offset_of_demoWold_4(),
	PlayerMobilmoManager_t2341644285::get_offset_of_playerMobilmo_5(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_MobilmoObj_6(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_CoreObj_7(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_vSavePos_8(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_bIsMobilityBreak_9(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_fLeverValue_10(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_bActionLever_11(),
	PlayerMobilmoManager_t2341644285::get_offset_of_ResetPosCoolTime_12(),
	PlayerMobilmoManager_t2341644285::get_offset_of_nearmobObjList_13(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_RecordingCreObj_14(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_MyPageCreObj_15(),
	PlayerMobilmoManager_t2341644285::get_offset_of_corePastsChildTra_16(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childDrillerPositionX_17(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childDrillerPositionY_18(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childDrillerPositionZ_19(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childCenterRotationX_20(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childCenterRotationY_21(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childCenterRotationZ_22(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsPositionX_23(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsPositionY_24(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsPositionZ_25(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsRotationX_26(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsRotationY_27(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsRotationZ_28(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsScaleX_29(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsScaleY_30(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsScaleZ_31(),
	PlayerMobilmoManager_t2341644285::get_offset_of_parentPartsJointNo_32(),
	PlayerMobilmoManager_t2341644285::get_offset_of_childPartsJointNo_33(),
	PlayerMobilmoManager_t2341644285::get_offset_of_mobilityObject_34(),
	PlayerMobilmoManager_t2341644285::get_offset_of_selectingcopyObj_35(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_bPlayAnim_36(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_bTutorialLever_37(),
	PlayerMobilmoManager_t2341644285::get_offset_of_m_fLeverPermission_38(),
	PlayerMobilmoManager_t2341644285::get_offset_of_recreatedModuleObj_39(),
	PlayerMobilmoManager_t2341644285::get_offset_of_MobilityNameText_40(),
	PlayerMobilmoManager_t2341644285::get_offset_of_MobilityReactionText_41(),
	PlayerMobilmoManager_t2341644285::get_offset_of_BreakedTextObj_42(),
	PlayerMobilmoManager_t2341644285_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_43(),
	PlayerMobilmoManager_t2341644285_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (PreloadingManager_t3491463554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4008[11] = 
{
	PreloadingManager_t3491463554::get_offset_of_urlPath_2(),
	PreloadingManager_t3491463554::get_offset_of_Navit_3(),
	PreloadingManager_t3491463554::get_offset_of_Guage_4(),
	PreloadingManager_t3491463554::get_offset_of_Raito_5(),
	PreloadingManager_t3491463554::get_offset_of_PercentRectTrans_6(),
	PreloadingManager_t3491463554::get_offset_of_NavitSpeed_7(),
	PreloadingManager_t3491463554::get_offset_of_CompletePanel_8(),
	PreloadingManager_t3491463554::get_offset_of_m_vRotNavit_9(),
	PreloadingManager_t3491463554::get_offset_of_m_Async_10(),
	PreloadingManager_t3491463554::get_offset_of_m_DownLoadAssetbundle_11(),
	PreloadingManager_t3491463554::get_offset_of_verText_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (U3CPreloadingU3Ec__Iterator0_t3199063565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4009[7] = 
{
	U3CPreloadingU3Ec__Iterator0_t3199063565::get_offset_of_U24locvar0_0(),
	U3CPreloadingU3Ec__Iterator0_t3199063565::get_offset_of_U24locvar1_1(),
	U3CPreloadingU3Ec__Iterator0_t3199063565::get_offset_of_U3CnameU3E__1_2(),
	U3CPreloadingU3Ec__Iterator0_t3199063565::get_offset_of_U24this_3(),
	U3CPreloadingU3Ec__Iterator0_t3199063565::get_offset_of_U24current_4(),
	U3CPreloadingU3Ec__Iterator0_t3199063565::get_offset_of_U24disposing_5(),
	U3CPreloadingU3Ec__Iterator0_t3199063565::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (PrizedManager_t3323701717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4010[3] = 
{
	PrizedManager_t3323701717::get_offset_of__prizedList_3(),
	PrizedManager_t3323701717::get_offset_of__mobilmoDic_4(),
	PrizedManager_t3323701717::get_offset_of_PrizedChargeList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (RayManager_t3612671535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4011[6] = 
{
	RayManager_t3612671535::get_offset_of_m_RayCast_3(),
	RayManager_t3612671535::get_offset_of_m_mainCamera_4(),
	RayManager_t3612671535::get_offset_of_m_CreObj_5(),
	RayManager_t3612671535::get_offset_of_m_vMousePos_6(),
	RayManager_t3612671535::get_offset_of_m_PriType_7(),
	RayManager_t3612671535::get_offset_of_m_eOpeType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (OPERATION_TYPE_t1833855786)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4012[4] = 
{
	OPERATION_TYPE_t1833855786::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (SceneFadeManager_t1728942829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4013[9] = 
{
	SceneFadeManager_t1728942829::get_offset_of_Active_3(),
	SceneFadeManager_t1728942829::get_offset_of_IsFading_4(),
	SceneFadeManager_t1728942829::get_offset_of_m_Texture_5(),
	SceneFadeManager_t1728942829::get_offset_of_m_Sequence_6(),
	SceneFadeManager_t1728942829::get_offset_of_m_FromColor_7(),
	SceneFadeManager_t1728942829::get_offset_of_m_ToColor_8(),
	SceneFadeManager_t1728942829::get_offset_of_m_NowColor_9(),
	SceneFadeManager_t1728942829::get_offset_of_m_fTime_10(),
	SceneFadeManager_t1728942829::get_offset_of_m_CompleteAction_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (OnComplete_t2805533292), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (U3C_FadeUpdateU3Ec__Iterator0_t1411678873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4015[5] = 
{
	U3C_FadeUpdateU3Ec__Iterator0_t1411678873::get_offset_of_U3CtimeU3E__0_0(),
	U3C_FadeUpdateU3Ec__Iterator0_t1411678873::get_offset_of_U24this_1(),
	U3C_FadeUpdateU3Ec__Iterator0_t1411678873::get_offset_of_U24current_2(),
	U3C_FadeUpdateU3Ec__Iterator0_t1411678873::get_offset_of_U24disposing_3(),
	U3C_FadeUpdateU3Ec__Iterator0_t1411678873::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (eSCENETYPE_t4122992453)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4016[39] = 
{
	eSCENETYPE_t4122992453::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (SceneManager_t1824971699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4017[4] = 
{
	SceneManager_t1824971699::get_offset_of_m_SceneType_3(),
	SceneManager_t1824971699::get_offset_of_fpsText_4(),
	SceneManager_t1824971699::get_offset_of_fps_5(),
	SceneManager_t1824971699::get_offset_of_previousSceneType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (U3C_FPSCheckU3Ec__Iterator0_t130709559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4018[4] = 
{
	U3C_FPSCheckU3Ec__Iterator0_t130709559::get_offset_of_U24this_0(),
	U3C_FPSCheckU3Ec__Iterator0_t130709559::get_offset_of_U24current_1(),
	U3C_FPSCheckU3Ec__Iterator0_t130709559::get_offset_of_U24disposing_2(),
	U3C_FPSCheckU3Ec__Iterator0_t130709559::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4019[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (TeleportManager_t4218394618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4020[2] = 
{
	TeleportManager_t4218394618::get_offset_of_Teleporters_3(),
	TeleportManager_t4218394618::get_offset_of_IsMove_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (TutorialManager_t2168024773), -1, sizeof(TutorialManager_t2168024773_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4021[54] = 
{
	TutorialManager_t2168024773::get_offset_of_m_bIsTutorial_3(),
	TutorialManager_t2168024773::get_offset_of_m_bIsCreateTutorial_4(),
	TutorialManager_t2168024773::get_offset_of_m_bIsSkip_5(),
	TutorialManager_t2168024773::get_offset_of_m_bEndTutorial_6(),
	TutorialManager_t2168024773::get_offset_of_m_bEndCreateTutorial_7(),
	TutorialManager_t2168024773::get_offset_of_m_bEndTutorialAction_8(),
	TutorialManager_t2168024773::get_offset_of_m_sTutorialNum_9(),
	TutorialManager_t2168024773::get_offset_of_TalkPanelObj_10(),
	TutorialManager_t2168024773::get_offset_of_m_TalkPanel_11(),
	TutorialManager_t2168024773::get_offset_of_talkButton_12(),
	TutorialManager_t2168024773::get_offset_of_TalkText_13(),
	TutorialManager_t2168024773::get_offset_of_talkNextButton_14(),
	TutorialManager_t2168024773::get_offset_of_DebugTutorialOFF_15(),
	TutorialManager_t2168024773::get_offset_of_myRecordOn_16(),
	TutorialManager_t2168024773::get_offset_of_DummyStartRecord_17(),
	TutorialManager_t2168024773::get_offset_of_DummyRecord_18(),
	TutorialManager_t2168024773::get_offset_of_CircleRectTran_19(),
	TutorialManager_t2168024773::get_offset_of_circleTw_20(),
	TutorialManager_t2168024773::get_offset_of_circleCoroutine_21(),
	TutorialManager_t2168024773::get_offset_of_FingerRectTran_22(),
	TutorialManager_t2168024773::get_offset_of_fingerTw_23(),
	TutorialManager_t2168024773::get_offset_of_MobilmoArea_24(),
	TutorialManager_t2168024773::get_offset_of_t_CanvasRayCaster_25(),
	TutorialManager_t2168024773::get_offset_of_t_CoreSelect_26(),
	TutorialManager_t2168024773::get_offset_of_t_PartsConect_27(),
	TutorialManager_t2168024773::get_offset_of_t_MoveSet_28(),
	TutorialManager_t2168024773::get_offset_of_t_WorldContents_29(),
	TutorialManager_t2168024773::get_offset_of_t_RecodeMobility_30(),
	TutorialManager_t2168024773::get_offset_of_t_MobilmoAction_31(),
	TutorialManager_t2168024773::get_offset_of_t_DemoWorld_32(),
	TutorialManager_t2168024773::get_offset_of_t_MobiSele_33(),
	TutorialManager_t2168024773::get_offset_of_t_PartsScroll_34(),
	TutorialManager_t2168024773::get_offset_of_PartsListScroll_35(),
	TutorialManager_t2168024773::get_offset_of_partsListButtonTran_36(),
	TutorialManager_t2168024773::get_offset_of_PartsConfigButton_37(),
	TutorialManager_t2168024773::get_offset_of_modelingCompleteButton_38(),
	TutorialManager_t2168024773::get_offset_of_createButton_39(),
	TutorialManager_t2168024773::get_offset_of_WorldButton_40(),
	TutorialManager_t2168024773::get_offset_of_LeverX_41(),
	TutorialManager_t2168024773::get_offset_of_RollControl_42(),
	TutorialManager_t2168024773::get_offset_of_RecButton_43(),
	TutorialManager_t2168024773::get_offset_of_SkipButton_44(),
	TutorialManager_t2168024773::get_offset_of_NomalTabButton_45(),
	TutorialManager_t2168024773::get_offset_of_mobilmoDnaCon_46(),
	TutorialManager_t2168024773::get_offset_of_m_sWardsNum_47(),
	TutorialManager_t2168024773::get_offset_of_m_pTalkWards_48(),
	TutorialManager_t2168024773::get_offset_of_TutrialAction_49(),
	TutorialManager_t2168024773::get_offset_of_m_TalkFader_50(),
	TutorialManager_t2168024773::get_offset_of_m_pSerifs_51(),
	TutorialManager_t2168024773::get_offset_of_m_pSerifsMobilmo_52(),
	TutorialManager_t2168024773::get_offset_of_m_pSerifsAction_53(),
	TutorialManager_t2168024773::get_offset_of_VoiceNum_54(),
	TutorialManager_t2168024773::get_offset_of_MoveDis_55(),
	TutorialManager_t2168024773_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (U3CLeverandRollFingerU3Ec__Iterator0_t31138930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4022[4] = 
{
	U3CLeverandRollFingerU3Ec__Iterator0_t31138930::get_offset_of_U24this_0(),
	U3CLeverandRollFingerU3Ec__Iterator0_t31138930::get_offset_of_U24current_1(),
	U3CLeverandRollFingerU3Ec__Iterator0_t31138930::get_offset_of_U24disposing_2(),
	U3CLeverandRollFingerU3Ec__Iterator0_t31138930::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (U3CCircleSetU3Ec__AnonStorey4_t2608721195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4023[3] = 
{
	U3CCircleSetU3Ec__AnonStorey4_t2608721195::get_offset_of_cam_0(),
	U3CCircleSetU3Ec__AnonStorey4_t2608721195::get_offset_of_target_1(),
	U3CCircleSetU3Ec__AnonStorey4_t2608721195::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (U3CCircleSetU3Ec__AnonStorey5_t2608721196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4024[2] = 
{
	U3CCircleSetU3Ec__AnonStorey5_t2608721196::get_offset_of_tran_0(),
	U3CCircleSetU3Ec__AnonStorey5_t2608721196::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (U3CCircleSetLoopU3Ec__Iterator1_t3353285022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4025[6] = 
{
	U3CCircleSetLoopU3Ec__Iterator1_t3353285022::get_offset_of_target_0(),
	U3CCircleSetLoopU3Ec__Iterator1_t3353285022::get_offset_of_cam_1(),
	U3CCircleSetLoopU3Ec__Iterator1_t3353285022::get_offset_of_U24this_2(),
	U3CCircleSetLoopU3Ec__Iterator1_t3353285022::get_offset_of_U24current_3(),
	U3CCircleSetLoopU3Ec__Iterator1_t3353285022::get_offset_of_U24disposing_4(),
	U3CCircleSetLoopU3Ec__Iterator1_t3353285022::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (U3CFingerSetU3Ec__AnonStorey6_t1674958284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4026[3] = 
{
	U3CFingerSetU3Ec__AnonStorey6_t1674958284::get_offset_of_cam_0(),
	U3CFingerSetU3Ec__AnonStorey6_t1674958284::get_offset_of_target_1(),
	U3CFingerSetU3Ec__AnonStorey6_t1674958284::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (U3CFingerSetU3Ec__AnonStorey7_t1674958283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4027[3] = 
{
	U3CFingerSetU3Ec__AnonStorey7_t1674958283::get_offset_of_tran_0(),
	U3CFingerSetU3Ec__AnonStorey7_t1674958283::get_offset_of_pos_1(),
	U3CFingerSetU3Ec__AnonStorey7_t1674958283::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (U3CFingerSetU3Ec__AnonStorey8_t1674958290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4028[3] = 
{
	U3CFingerSetU3Ec__AnonStorey8_t1674958290::get_offset_of_startpos_0(),
	U3CFingerSetU3Ec__AnonStorey8_t1674958290::get_offset_of_endPos_1(),
	U3CFingerSetU3Ec__AnonStorey8_t1674958290::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (U3CJointCircleU3Ec__Iterator2_t1644323487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4029[7] = 
{
	U3CJointCircleU3Ec__Iterator2_t1644323487::get_offset_of_size_0(),
	U3CJointCircleU3Ec__Iterator2_t1644323487::get_offset_of_startPos_1(),
	U3CJointCircleU3Ec__Iterator2_t1644323487::get_offset_of_endPos_2(),
	U3CJointCircleU3Ec__Iterator2_t1644323487::get_offset_of_U24this_3(),
	U3CJointCircleU3Ec__Iterator2_t1644323487::get_offset_of_U24current_4(),
	U3CJointCircleU3Ec__Iterator2_t1644323487::get_offset_of_U24disposing_5(),
	U3CJointCircleU3Ec__Iterator2_t1644323487::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (U3CDistanceMeasureU3Ec__Iterator3_t2793382657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4030[9] = 
{
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_taget_0(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_U3CprevposU3E__0_1(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_U3CNowPosU3E__0_2(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_maxDis_3(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_EndAction_4(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_U24this_5(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_U24current_6(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_U24disposing_7(),
	U3CDistanceMeasureU3Ec__Iterator3_t2793382657::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (UIObject_t4279159643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[4] = 
{
	UIObject_t4279159643::get_offset_of_SceneType_0(),
	UIObject_t4279159643::get_offset_of_objs_1(),
	UIObject_t4279159643::get_offset_of_objs_ignore_2(),
	UIObject_t4279159643::get_offset_of_isDefault_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (UIManager_t2519183485), -1, sizeof(UIManager_t2519183485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4032[40] = 
{
	UIManager_t2519183485::get_offset_of_m_sUINum_3(),
	UIManager_t2519183485::get_offset_of_UImodules_4(),
	UIManager_t2519183485::get_offset_of_m_UIList_5(),
	UIManager_t2519183485::get_offset_of_panels_6(),
	UIManager_t2519183485::get_offset_of_ControlUI_7(),
	UIManager_t2519183485::get_offset_of_ActionButton_8(),
	UIManager_t2519183485::get_offset_of_Rollstick_9(),
	UIManager_t2519183485::get_offset_of_LoadingUI_10(),
	UIManager_t2519183485::get_offset_of_m_WorldMapUI_11(),
	UIManager_t2519183485::get_offset_of_ActionPartsPanelCntr_12(),
	UIManager_t2519183485::get_offset_of_m_CreateModeSelect_13(),
	UIManager_t2519183485::get_offset_of_m_CoreSelectUIGO_14(),
	UIManager_t2519183485::get_offset_of_m_CoreSelectModuleUIGO_15(),
	UIManager_t2519183485::get_offset_of_m_MobilmoPartsConect_16(),
	UIManager_t2519183485::get_offset_of_m_ModulePartsConect_17(),
	UIManager_t2519183485::get_offset_of_m_gBackCoreSelectButton_18(),
	UIManager_t2519183485::get_offset_of_m_gBackSelectButton_19(),
	UIManager_t2519183485::get_offset_of_m_gCompleteButton_20(),
	UIManager_t2519183485::get_offset_of_m_gBackCoreSelectModuleButton_21(),
	UIManager_t2519183485::get_offset_of_m_gBackSelectModuleButton_22(),
	UIManager_t2519183485::get_offset_of_m_gCompleteModuleButton_23(),
	UIManager_t2519183485::get_offset_of_m_MoveSet_24(),
	UIManager_t2519183485::get_offset_of_m_ActionSet_25(),
	UIManager_t2519183485::get_offset_of_m_ActionSlidePanel_26(),
	UIManager_t2519183485::get_offset_of_m_ActionNamePanel_27(),
	UIManager_t2519183485::get_offset_of_m_ActionTitle_28(),
	UIManager_t2519183485::get_offset_of_m_PoseTitle_29(),
	UIManager_t2519183485::get_offset_of_m_RaderCtrl_30(),
	UIManager_t2519183485::get_offset_of_m_DemoWorldCOntents_31(),
	UIManager_t2519183485::get_offset_of_m_MyPageContents_32(),
	UIManager_t2519183485::get_offset_of_m_bUIPoint_33(),
	UIManager_t2519183485::get_offset_of_m_bUIClick_34(),
	UIManager_t2519183485::get_offset_of_dialog_35(),
	UIManager_t2519183485::get_offset_of_prized_36(),
	UIManager_t2519183485::get_offset_of_BackgroundScrollValue_37(),
	UIManager_t2519183485::get_offset_of_WorldMapBackgroundScrollValue_38(),
	UIManager_t2519183485::get_offset_of_RotCircles_39(),
	UIManager_t2519183485::get_offset_of_IsCirclesSave_40(),
	UIManager_t2519183485::get_offset_of_test_41(),
	UIManager_t2519183485_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (U3CUIChangeU3Ec__AnonStorey1_t3981264818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4033[1] = 
{
	U3CUIChangeU3Ec__AnonStorey1_t3981264818::get_offset_of_uiType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (U3CUIAdditiveU3Ec__AnonStorey2_t1385390251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4034[1] = 
{
	U3CUIAdditiveU3Ec__AnonStorey2_t1385390251::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (U3CwaitCloseLoadingUIU3Ec__Iterator0_t3507306122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4035[5] = 
{
	U3CwaitCloseLoadingUIU3Ec__Iterator0_t3507306122::get_offset_of_on_0(),
	U3CwaitCloseLoadingUIU3Ec__Iterator0_t3507306122::get_offset_of_U24this_1(),
	U3CwaitCloseLoadingUIU3Ec__Iterator0_t3507306122::get_offset_of_U24current_2(),
	U3CwaitCloseLoadingUIU3Ec__Iterator0_t3507306122::get_offset_of_U24disposing_3(),
	U3CwaitCloseLoadingUIU3Ec__Iterator0_t3507306122::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (eAREATYPE_t1739175762)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4036[11] = 
{
	eAREATYPE_t1739175762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (LonLatitude_t2793245363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4037[3] = 
{
	LonLatitude_t2793245363::get_offset_of_areaType_0(),
	LonLatitude_t2793245363::get_offset_of_min_1(),
	LonLatitude_t2793245363::get_offset_of_max_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (AreaParentObject_t3918549470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4038[2] = 
{
	AreaParentObject_t3918549470::get_offset_of_areaType_0(),
	AreaParentObject_t3918549470::get_offset_of_ParentObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (WorldManager_t3923509627), -1, sizeof(WorldManager_t3923509627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4039[41] = 
{
	WorldManager_t3923509627::get_offset_of_m_Planet_3(),
	WorldManager_t3923509627::get_offset_of_PlanetMap_4(),
	WorldManager_t3923509627::get_offset_of_TitleMiniPlanet_5(),
	WorldManager_t3923509627::get_offset_of_MiniPlanet_6(),
	WorldManager_t3923509627::get_offset_of_MyPageMiniPlanet_7(),
	WorldManager_t3923509627::get_offset_of_m_DounutRock_8(),
	WorldManager_t3923509627::get_offset_of_m_WaterSpace_9(),
	WorldManager_t3923509627::get_offset_of_SnowEffect_10(),
	WorldManager_t3923509627::get_offset_of_PenStorm_11(),
	WorldManager_t3923509627::get_offset_of_m_OaWaterSpace_12(),
	WorldManager_t3923509627::get_offset_of_m_PenginArea_13(),
	WorldManager_t3923509627::get_offset_of_Contents_14(),
	WorldManager_t3923509627::get_offset_of_GetModulePanel_15(),
	WorldManager_t3923509627::get_offset_of_DnaUiList_16(),
	WorldManager_t3923509627::get_offset_of_EreaFloor_17(),
	WorldManager_t3923509627::get_offset_of_AreaParentObject_18(),
	WorldManager_t3923509627::get_offset_of_areaObjects_19(),
	WorldManager_t3923509627::get_offset_of_SelectSpot_20(),
	WorldManager_t3923509627::get_offset_of_Latitude_21(),
	WorldManager_t3923509627::get_offset_of_Longitude_22(),
	WorldManager_t3923509627::get_offset_of_PlanetRadius_23(),
	WorldManager_t3923509627::get_offset_of_CubeRock_24(),
	WorldManager_t3923509627::get_offset_of_DounutRock_25(),
	WorldManager_t3923509627::get_offset_of_DounutArea_26(),
	WorldManager_t3923509627::get_offset_of_rockNum_27(),
	WorldManager_t3923509627::get_offset_of_Ramlat_28(),
	WorldManager_t3923509627::get_offset_of_Ramlon_29(),
	WorldManager_t3923509627::get_offset_of_inWater_30(),
	WorldManager_t3923509627::get_offset_of_rockArea_31(),
	WorldManager_t3923509627::get_offset_of_lat_32(),
	WorldManager_t3923509627::get_offset_of_lng_33(),
	WorldManager_t3923509627::get_offset_of_lonlat_34(),
	WorldManager_t3923509627::get_offset_of_m_DounutRocks_35(),
	WorldManager_t3923509627::get_offset_of_LonlatData_36(),
	WorldManager_t3923509627::get_offset_of_isActionSettingInWorld_37(),
	WorldManager_t3923509627::get_offset_of_rockDic_38(),
	WorldManager_t3923509627::get_offset_of_m_areaNames_39(),
	WorldManager_t3923509627::get_offset_of_m_areaLeads_40(),
	WorldManager_t3923509627::get_offset_of_NowArea_41(),
	WorldManager_t3923509627::get_offset_of_NextArea_42(),
	WorldManager_t3923509627_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (U3CRandomRockAreaU3Ec__Iterator0_t1715171824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4040[8] = 
{
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U3CcountU3E__0_0(),
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U24locvar0_1(),
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U3ClonlatU3E__1_2(),
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U3CrockU3E__2_3(),
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U24this_4(),
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U24current_5(),
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U24disposing_6(),
	U3CRandomRockAreaU3Ec__Iterator0_t1715171824::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (WorldMapManager_t3256198739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4041[11] = 
{
	WorldMapManager_t3256198739::get_offset_of_WorldMapSkydome_3(),
	WorldMapManager_t3256198739::get_offset_of_MiniAreas_4(),
	WorldMapManager_t3256198739::get_offset_of_SkydomeRate_5(),
	WorldMapManager_t3256198739::get_offset_of_IsCreatedMiniMap_6(),
	WorldMapManager_t3256198739::get_offset_of_AutoRotation_7(),
	WorldMapManager_t3256198739::get_offset_of_Reverse_8(),
	WorldMapManager_t3256198739::get_offset_of_AutoSpinX_9(),
	WorldMapManager_t3256198739::get_offset_of_AutoSpinY_10(),
	WorldMapManager_t3256198739::get_offset_of_AutoSpinReverseX_11(),
	WorldMapManager_t3256198739::get_offset_of_AutoSpinReverseY_12(),
	WorldMapManager_t3256198739::get_offset_of_vAreaPoints_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (EditMobilmo_t2535788613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4042[4] = 
{
	EditMobilmo_t2535788613::get_offset_of_editModuleObject_2(),
	EditMobilmo_t2535788613::get_offset_of_mobilmo_3(),
	EditMobilmo_t2535788613::get_offset_of_m_CreObj_4(),
	EditMobilmo_t2535788613::get_offset_of_m_moduleCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (U3CreEditMobilmoU3Ec__Iterator0_t4018953849), -1, sizeof(U3CreEditMobilmoU3Ec__Iterator0_t4018953849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4043[32] = 
{
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_json_0(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CwwwDataU3E__0_1(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3C_getMobilityModelU3E__0_2(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CmobilmoRigidU3E__0_3(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3Cm_mobilimoU3E__0_4(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CmobilFixedU3E__0_5(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CcoreChildObjU3E__0_6(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CcoreMatU3E__0_7(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24locvar0_8(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CchildU3E__1_9(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CModuleCenterU3E__2_10(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CModuleLeapU3E__2_11(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CModuleRollU3E__2_12(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CmoduleMgrU3E__2_13(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CparentPartsJointNoU3E__2_14(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CmoduleJsonU3E__2_15(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CjointParentsU3E__2_16(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CmodulePartsCntU3E__2_17(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CpartsIdIndexU3E__2_18(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CpartsIdListU3E__2_19(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U3CmodulePartsListU3E__2_20(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24locvar5_21(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24locvar6_22(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24locvar7_23(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24locvar8_24(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24locvar11_25(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24locvar12_26(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24this_27(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24current_28(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24disposing_29(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849::get_offset_of_U24PC_30(),
	U3CreEditMobilmoU3Ec__Iterator0_t4018953849_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (U3CWaitEditMobilmoU3Ec__Iterator1_t2383036814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4044[4] = 
{
	U3CWaitEditMobilmoU3Ec__Iterator1_t2383036814::get_offset_of_U24this_0(),
	U3CWaitEditMobilmoU3Ec__Iterator1_t2383036814::get_offset_of_U24current_1(),
	U3CWaitEditMobilmoU3Ec__Iterator1_t2383036814::get_offset_of_U24disposing_2(),
	U3CWaitEditMobilmoU3Ec__Iterator1_t2383036814::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (Joint_t826707408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4045[9] = 
{
	Joint_t826707408::get_offset_of_JointNum_2(),
	Joint_t826707408::get_offset_of_m_ParentPartsObj_3(),
	Joint_t826707408::get_offset_of_m_bIsSelect_4(),
	Joint_t826707408::get_offset_of_m_bIsConnect_5(),
	Joint_t826707408::get_offset_of_m_ParentParts_6(),
	Joint_t826707408::get_offset_of_m_SpriteObj_7(),
	Joint_t826707408::get_offset_of_m_vOriginalScale_8(),
	Joint_t826707408::get_offset_of_m_Sprite_9(),
	Joint_t826707408::get_offset_of_m_DefaultColor_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (MobPhysics_t1476770297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (Mobilmo_t370754809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4047[22] = 
{
	Mobilmo_t370754809::get_offset_of_startPosition_2(),
	Mobilmo_t370754809::get_offset_of_startRotation_3(),
	Mobilmo_t370754809::get_offset_of_mobilmoId_4(),
	Mobilmo_t370754809::get_offset_of_mobilityName_5(),
	Mobilmo_t370754809::get_offset_of_m_nearMobUserId_6(),
	Mobilmo_t370754809::get_offset_of_prizedIdListCnt_7(),
	Mobilmo_t370754809::get_offset_of_createdUserId_8(),
	Mobilmo_t370754809::get_offset_of_m_userName_9(),
	Mobilmo_t370754809::get_offset_of_m_userRank_10(),
	Mobilmo_t370754809::get_offset_of_PrizedMasterIdList_11(),
	Mobilmo_t370754809::get_offset_of_ActionA_12(),
	Mobilmo_t370754809::get_offset_of_ActionB_13(),
	Mobilmo_t370754809::get_offset_of_ActionX_14(),
	Mobilmo_t370754809::get_offset_of_ActionY_15(),
	Mobilmo_t370754809::get_offset_of_moduleList_16(),
	Mobilmo_t370754809::get_offset_of_m_CoreObj_17(),
	Mobilmo_t370754809::get_offset_of_m_bRecMobility_18(),
	Mobilmo_t370754809::get_offset_of_m_bCoreNone_19(),
	Mobilmo_t370754809::get_offset_of_m_bTutorialCloneMobilmo_20(),
	Mobilmo_t370754809::get_offset_of_m_fDefaultMoveTime_21(),
	Mobilmo_t370754809::get_offset_of_m_sMotionNum_22(),
	Mobilmo_t370754809::get_offset_of_recordingMotionDataList_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (U3CchangeMotionU3Ec__Iterator0_t1783254865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4048[5] = 
{
	U3CchangeMotionU3Ec__Iterator0_t1783254865::get_offset_of_U3CiU3E__1_0(),
	U3CchangeMotionU3Ec__Iterator0_t1783254865::get_offset_of_U24this_1(),
	U3CchangeMotionU3Ec__Iterator0_t1783254865::get_offset_of_U24current_2(),
	U3CchangeMotionU3Ec__Iterator0_t1783254865::get_offset_of_U24disposing_3(),
	U3CchangeMotionU3Ec__Iterator0_t1783254865::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (Module_t3140434828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4049[29] = 
{
	Module_t3140434828::get_offset_of_m_ModuleObj_2(),
	Module_t3140434828::get_offset_of_m_CoreObj_3(),
	Module_t3140434828::get_offset_of_ParentParts_4(),
	Module_t3140434828::get_offset_of_m_Anim_5(),
	Module_t3140434828::get_offset_of_m_ModuleAnim_6(),
	Module_t3140434828::get_offset_of_m_ModuleReverseAnim_7(),
	Module_t3140434828::get_offset_of_m_AnimCurveDic_8(),
	Module_t3140434828::get_offset_of_m_ReverseCurveDic_9(),
	Module_t3140434828::get_offset_of_m_pDefaultCurve_10(),
	Module_t3140434828::get_offset_of_m_DefaultClip_11(),
	Module_t3140434828::get_offset_of_iconKey_12(),
	Module_t3140434828::get_offset_of_m_pEndPoseCurve_13(),
	Module_t3140434828::get_offset_of_m_EndPoseClip_14(),
	Module_t3140434828::get_offset_of_PlayModeNum_15(),
	Module_t3140434828::get_offset_of_m_bIsActionPlay_16(),
	Module_t3140434828::get_offset_of_m_bisTestAnim_17(),
	Module_t3140434828::get_offset_of_m_bIsJointConnect_18(),
	Module_t3140434828::get_offset_of_moduleJson_19(),
	Module_t3140434828::get_offset_of_m_moduleId_20(),
	Module_t3140434828::get_offset_of_m_moduleAnimId_21(),
	Module_t3140434828::get_offset_of_m_parentModuleNo_22(),
	Module_t3140434828::get_offset_of_m_ModuleNo_23(),
	Module_t3140434828::get_offset_of_childPartsName_24(),
	Module_t3140434828::get_offset_of_recModule_25(),
	Module_t3140434828::get_offset_of_ParentMobilmo_26(),
	Module_t3140434828::get_offset_of_parentMobilmoId_27(),
	Module_t3140434828::get_offset_of_JointChildParts_28(),
	Module_t3140434828::get_offset_of_moduleColorId_29(),
	Module_t3140434828::get_offset_of_cloneModuleCreateUserId_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (Parts_t3804168686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4050[34] = 
{
	Parts_t3804168686::get_offset_of_parentJoint_2(),
	Parts_t3804168686::get_offset_of_actionPartsParentJoint_3(),
	Parts_t3804168686::get_offset_of_mobilityName_4(),
	Parts_t3804168686::get_offset_of_PartsId_5(),
	Parts_t3804168686::get_offset_of_ColorId_6(),
	Parts_t3804168686::get_offset_of_PartsName_7(),
	Parts_t3804168686::get_offset_of_parentNo_8(),
	Parts_t3804168686::get_offset_of_rollId_9(),
	Parts_t3804168686::get_offset_of_mobilityId_10(),
	Parts_t3804168686::get_offset_of_createdUserId_11(),
	Parts_t3804168686::get_offset_of_nearmobObjectName_12(),
	Parts_t3804168686::get_offset_of_nearMobool_13(),
	Parts_t3804168686::get_offset_of_m_SaveMobilityJsonData_14(),
	Parts_t3804168686::get_offset_of_m_SaveModuleChildPartsData_15(),
	Parts_t3804168686::get_offset_of_additionalRotate_16(),
	Parts_t3804168686::get_offset_of_addRotateParameter_17(),
	Parts_t3804168686::get_offset_of_pose1AddRotate_18(),
	Parts_t3804168686::get_offset_of_pose2AddRotate_19(),
	Parts_t3804168686::get_offset_of_pose3AddRotate_20(),
	Parts_t3804168686::get_offset_of_EnagyParticle_21(),
	Parts_t3804168686::get_offset_of_ChildObj_22(),
	Parts_t3804168686::get_offset_of_m_RollTrans_23(),
	Parts_t3804168686::get_offset_of_m_PartsJointList_24(),
	Parts_t3804168686::get_offset_of_m_sParentConectJointNum_25(),
	Parts_t3804168686::get_offset_of_mobilityLevel_26(),
	Parts_t3804168686::get_offset_of_EnagyValue_27(),
	Parts_t3804168686::get_offset_of_mobilityCountryText_28(),
	Parts_t3804168686::get_offset_of_mobilityReactionText_29(),
	Parts_t3804168686::get_offset_of_m_bIsConnect_30(),
	Parts_t3804168686::get_offset_of_m_PartsLight_31(),
	Parts_t3804168686::get_offset_of_m_bLeapFixed_32(),
	Parts_t3804168686::get_offset_of_startRotate_33(),
	Parts_t3804168686::get_offset_of_endRotate_34(),
	Parts_t3804168686::get_offset_of_m_parentMobilmoId_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (SaveMobilityJsonData_t2430892781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4051[38] = 
{
	SaveMobilityJsonData_t2430892781::get_offset_of_partsType_0(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleId_1(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsName_2(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsColor_3(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsJointNo_4(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsId_5(),
	SaveMobilityJsonData_t2430892781::get_offset_of_parentPartsName_6(),
	SaveMobilityJsonData_t2430892781::get_offset_of_parentPartsId_7(),
	SaveMobilityJsonData_t2430892781::get_offset_of_parentPartsJointNo_8(),
	SaveMobilityJsonData_t2430892781::get_offset_of_jointChildPartsName_9(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childObjPositionX_10(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childObjPositionY_11(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childObjPositionZ_12(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childLeapRotationX_13(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childLeapRotationY_14(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childLeapRotationZ_15(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsPositionX_16(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsPositionY_17(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsPositionZ_18(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsRotationX_19(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsRotationY_20(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsRotationZ_21(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsScaleX_22(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsScaleY_23(),
	SaveMobilityJsonData_t2430892781::get_offset_of_childPartsScaleZ_24(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleScaleX_25(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleScaleY_26(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleScaleZ_27(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleCenterX_28(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleCenterY_29(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleCenterZ_30(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleLeapX_31(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleLeapY_32(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleLeapZ_33(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleRollX_34(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleRollY_35(),
	SaveMobilityJsonData_t2430892781::get_offset_of_moduleRollZ_36(),
	SaveMobilityJsonData_t2430892781::get_offset_of_parentModuleId_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (SaveModuleChildPartsData_t3597262878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4052[26] = 
{
	SaveModuleChildPartsData_t3597262878::get_offset_of_parentPartsJointNum_0(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsJointNum_1(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childObjPositionX_2(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childObjPositionY_3(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childObjPositionZ_4(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsId_5(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsName_6(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsColor_7(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_parentPartsName_8(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_parentPartsId_9(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsPositionX_10(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsPositionY_11(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsPositionZ_12(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsRotationX_13(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsRotationY_14(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsRotationZ_15(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsScaleX_16(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsScaleY_17(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childPartsScaleZ_18(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childLeapRotationX_19(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childLeapRotationY_20(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childLeapRotationZ_21(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childRollRotationX_22(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childRollRotationY_23(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_childRollRotationZ_24(),
	SaveModuleChildPartsData_t3597262878::get_offset_of_leapFixed_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (PartsConfig_t872073448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4053[6] = 
{
	PartsConfig_t872073448::get_offset_of_ParentToPartsConfigList_2(),
	PartsConfig_t872073448::get_offset_of_ConfigPartsPrefab_3(),
	PartsConfig_t872073448::get_offset_of_m_bOffsetStart_4(),
	PartsConfig_t872073448::get_offset_of_m_sItemOffset_5(),
	PartsConfig_t872073448::get_offset_of_m_pConfigPartsMap_6(),
	PartsConfig_t872073448::get_offset_of_m_CanvasGroup_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (U3C_CreatePartsConfigU3Ec__AnonStorey0_t1113296555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4054[3] = 
{
	U3C_CreatePartsConfigU3Ec__AnonStorey0_t1113296555::get_offset_of_obj_0(),
	U3C_CreatePartsConfigU3Ec__AnonStorey0_t1113296555::get_offset_of_keyObj_1(),
	U3C_CreatePartsConfigU3Ec__AnonStorey0_t1113296555::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (SlideInOut_t958296806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4055[7] = 
{
	SlideInOut_t958296806::get_offset_of_Enable_2(),
	SlideInOut_t958296806::get_offset_of_ActivePos_3(),
	SlideInOut_t958296806::get_offset_of_NonActive_4(),
	SlideInOut_t958296806::get_offset_of_Speed_5(),
	SlideInOut_t958296806::get_offset_of_Raito_6(),
	SlideInOut_t958296806::get_offset_of_EaseType_7(),
	SlideInOut_t958296806::get_offset_of_m_RectTransform_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (eSCALINGSELECTSTATE_t1537437653)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4056[4] = 
{
	eSCALINGSELECTSTATE_t1537437653::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (DoubleTapPartsScaling_t180404427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4057[11] = 
{
	DoubleTapPartsScaling_t180404427::get_offset_of_IsPunchMoving_2(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_fRaito_3(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_ScalingSelectState_4(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_bChild_5(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_CoreObj_6(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_PrevObj_7(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_fCurrentRaito_8(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_sRaitoIndex_9(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_vOriginalScale_10(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_bOriginal_11(),
	DoubleTapPartsScaling_t180404427::get_offset_of_m_fDefScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (ActionButtons_t2868854693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4058[7] = 
{
	ActionButtons_t2868854693::get_offset_of_ActionIconButtonPrefab_2(),
	ActionButtons_t2868854693::get_offset_of_ActionButton_3(),
	ActionButtons_t2868854693::get_offset_of_Buttons_4(),
	ActionButtons_t2868854693::get_offset_of_ActionTitle_5(),
	ActionButtons_t2868854693::get_offset_of_PoseTitle_6(),
	ActionButtons_t2868854693::get_offset_of_ActionListPanel_7(),
	ActionButtons_t2868854693::get_offset_of_ActionButtonName_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4059[3] = 
{
	U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407::get_offset_of_sIndex_0(),
	U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407::get_offset_of_sKey_1(),
	U3CCreateActionItemsU3Ec__AnonStorey0_t2884540407::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (ActionConfigPanel_t3304127690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4060[2] = 
{
	ActionConfigPanel_t3304127690::get_offset_of_ConfigPanel_2(),
	ActionConfigPanel_t3304127690::get_offset_of_m_Slider_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (ActionIconList_t2910912037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4061[3] = 
{
	ActionIconList_t2910912037::get_offset_of_Icons_2(),
	ActionIconList_t2910912037::get_offset_of_Width_3(),
	ActionIconList_t2910912037::get_offset_of_ActionIcon_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (Contents_t3730146386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4062[2] = 
{
	Contents_t3730146386::get_offset_of_List_0(),
	Contents_t3730146386::get_offset_of_Set_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (ActionListContetns_t3613658512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4063[3] = 
{
	ActionListContetns_t3613658512::get_offset_of_Background_0(),
	ActionListContetns_t3613658512::get_offset_of_Image_1(),
	ActionListContetns_t3613658512::get_offset_of_Text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (ActionSetContetns_t415653720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4064[4] = 
{
	ActionSetContetns_t415653720::get_offset_of_Background_0(),
	ActionSetContetns_t415653720::get_offset_of_Image_1(),
	ActionSetContetns_t415653720::get_offset_of_Text_2(),
	ActionSetContetns_t415653720::get_offset_of_DeleteButton_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (ActionObj_t2993311507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4065[4] = 
{
	ActionObj_t2993311507::get_offset_of_Contents_2(),
	ActionObj_t2993311507::get_offset_of_ActionListContetns_3(),
	ActionObj_t2993311507::get_offset_of_ActionSetContetns_4(),
	ActionObj_t2993311507::get_offset_of_Module_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (ActionSet_t264428022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4066[16] = 
{
	ActionSet_t264428022::get_offset_of_BackButton_2(),
	ActionSet_t264428022::get_offset_of_CompleteButton_3(),
	ActionSet_t264428022::get_offset_of_ActionButton_4(),
	ActionSet_t264428022::get_offset_of_PlayButton_5(),
	ActionSet_t264428022::get_offset_of_ConfigButton_6(),
	ActionSet_t264428022::get_offset_of_ConfigCompleteButton_7(),
	ActionSet_t264428022::get_offset_of_ActionConfigPanel_8(),
	ActionSet_t264428022::get_offset_of_ActionTitle_9(),
	ActionSet_t264428022::get_offset_of_PoseTitle_10(),
	ActionSet_t264428022::get_offset_of_ActionListPanel_11(),
	ActionSet_t264428022::get_offset_of_PlayButtonGroup_12(),
	ActionSet_t264428022::get_offset_of_ActionButtons_13(),
	ActionSet_t264428022::get_offset_of_MovieObj_14(),
	ActionSet_t264428022::get_offset_of_TutorialVideoPlayer_15(),
	ActionSet_t264428022::get_offset_of_TutorialTW_16(),
	ActionSet_t264428022::get_offset_of_m_bFadeOut_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (U3CPlayingMovieU3Ec__Iterator0_t3713250252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4067[4] = 
{
	U3CPlayingMovieU3Ec__Iterator0_t3713250252::get_offset_of_U24this_0(),
	U3CPlayingMovieU3Ec__Iterator0_t3713250252::get_offset_of_U24current_1(),
	U3CPlayingMovieU3Ec__Iterator0_t3713250252::get_offset_of_U24disposing_2(),
	U3CPlayingMovieU3Ec__Iterator0_t3713250252::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (Pose_t686168059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4068[6] = 
{
	Pose_t686168059::get_offset_of_Button_2(),
	Pose_t686168059::get_offset_of_Frame_3(),
	Pose_t686168059::get_offset_of_Image_4(),
	Pose_t686168059::get_offset_of_Delete_5(),
	Pose_t686168059::get_offset_of_Plus_6(),
	Pose_t686168059::get_offset_of_PlusImage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (BilmoListPanel_t1619006723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4069[1] = 
{
	BilmoListPanel_t1619006723::get_offset_of__mobilityObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (ButtonOnClickChangeScene_t4293177719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4070[2] = 
{
	ButtonOnClickChangeScene_t4293177719::get_offset_of_sceneTypeNext_2(),
	ButtonOnClickChangeScene_t4293177719::get_offset_of_button_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (CloneActionParts_t3277884483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (CoreContent_t4281623508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4072[1] = 
{
	CoreContent_t4281623508::get_offset_of_Prefab_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (CoreObj_t3180529948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4073[2] = 
{
	CoreObj_t3180529948::get_offset_of_coreInfo_0(),
	CoreObj_t3180529948::get_offset_of_Obj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (CoreSelect_t1755339513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4074[15] = 
{
	CoreSelect_t1755339513::get_offset_of_selectCores_2(),
	CoreSelect_t1755339513::get_offset_of_ModelingPanel_3(),
	CoreSelect_t1755339513::get_offset_of_leftSelect_4(),
	CoreSelect_t1755339513::get_offset_of_rightSelect_5(),
	CoreSelect_t1755339513::get_offset_of_preprevObj_6(),
	CoreSelect_t1755339513::get_offset_of_prevObj_7(),
	CoreSelect_t1755339513::get_offset_of_nowObj_8(),
	CoreSelect_t1755339513::get_offset_of_nextObj_9(),
	CoreSelect_t1755339513::get_offset_of_nexnextObj_10(),
	CoreSelect_t1755339513::get_offset_of_selectNum_11(),
	CoreSelect_t1755339513::get_offset_of_vPosNow_12(),
	CoreSelect_t1755339513::get_offset_of_vPosNext_13(),
	CoreSelect_t1755339513::get_offset_of_vPosPrev_14(),
	CoreSelect_t1755339513::get_offset_of_vPosAfterNext_15(),
	CoreSelect_t1755339513::get_offset_of_vPosBeforePrev_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (CreateGrid_t2336839420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4075[1] = 
{
	CreateGrid_t2336839420::get_offset_of_model_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (CreateModeSelect_t1995266573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (DemoWorld_t457704051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4077[2] = 
{
	DemoWorld_t457704051::get_offset_of_m_DemoField_2(),
	DemoWorld_t457704051::get_offset_of_FieldPrefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (MobilmoPartsConectContents_t1795215227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4078[6] = 
{
	MobilmoPartsConectContents_t1795215227::get_offset_of_CameraResetButton_0(),
	MobilmoPartsConectContents_t1795215227::get_offset_of_PartsDeleteButton_1(),
	MobilmoPartsConectContents_t1795215227::get_offset_of_ResetPanel_2(),
	MobilmoPartsConectContents_t1795215227::get_offset_of_ResetPanelRectTrans_3(),
	MobilmoPartsConectContents_t1795215227::get_offset_of_PosisionPanel_4(),
	MobilmoPartsConectContents_t1795215227::get_offset_of_StatusPanel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (MobilimoPartsConect_t3466715044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4079[8] = 
{
	MobilimoPartsConect_t3466715044::get_offset_of_Contents_2(),
	MobilimoPartsConect_t3466715044::get_offset_of_Sequences_3(),
	MobilimoPartsConect_t3466715044::get_offset_of_PartsViewPanel_4(),
	MobilimoPartsConect_t3466715044::get_offset_of_PartsBar_5(),
	MobilimoPartsConect_t3466715044::get_offset_of_Sliders_6(),
	MobilimoPartsConect_t3466715044::get_offset_of_ConfigPanel_7(),
	MobilimoPartsConect_t3466715044::get_offset_of_TutorialStep_8(),
	MobilimoPartsConect_t3466715044::get_offset_of_CreateTutorialStep_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (U3CwaitOpenPartsListU3Ec__Iterator0_t2029102973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4080[4] = 
{
	U3CwaitOpenPartsListU3Ec__Iterator0_t2029102973::get_offset_of_U24this_0(),
	U3CwaitOpenPartsListU3Ec__Iterator0_t2029102973::get_offset_of_U24current_1(),
	U3CwaitOpenPartsListU3Ec__Iterator0_t2029102973::get_offset_of_U24disposing_2(),
	U3CwaitOpenPartsListU3Ec__Iterator0_t2029102973::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (MobilmoPanel_t222294755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4081[3] = 
{
	MobilmoPanel_t222294755::get_offset_of_CoreSelect_2(),
	MobilmoPanel_t222294755::get_offset_of_PartsConect_3(),
	MobilmoPanel_t222294755::get_offset_of_CompleteButton_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (ModulePanel_t1015180434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4082[2] = 
{
	ModulePanel_t1015180434::get_offset_of_ModuleSelect_2(),
	ModulePanel_t1015180434::get_offset_of_PartsConect_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (ModulePartsConectContents_t560460772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4083[6] = 
{
	ModulePartsConectContents_t560460772::get_offset_of_CameraResetButton_0(),
	ModulePartsConectContents_t560460772::get_offset_of_PartsDeleteButton_1(),
	ModulePartsConectContents_t560460772::get_offset_of_ResetPanel_2(),
	ModulePartsConectContents_t560460772::get_offset_of_ResetPanelRectTrans_3(),
	ModulePartsConectContents_t560460772::get_offset_of_PosisionPanel_4(),
	ModulePartsConectContents_t560460772::get_offset_of_StatusPanel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (ModulePartsConect_t3833785310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4084[7] = 
{
	ModulePartsConect_t3833785310::get_offset_of_Contents_2(),
	ModulePartsConect_t3833785310::get_offset_of_Sequences_3(),
	ModulePartsConect_t3833785310::get_offset_of_PartsViewPanel_4(),
	ModulePartsConect_t3833785310::get_offset_of_Sliders_5(),
	ModulePartsConect_t3833785310::get_offset_of_ConfigPanel_6(),
	ModulePartsConect_t3833785310::get_offset_of_MovieObj_7(),
	ModulePartsConect_t3833785310::get_offset_of_TutorialVideoPlayer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (U3CPlayingMovieU3Ec__Iterator0_t111206440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4085[4] = 
{
	U3CPlayingMovieU3Ec__Iterator0_t111206440::get_offset_of_U24this_0(),
	U3CPlayingMovieU3Ec__Iterator0_t111206440::get_offset_of_U24current_1(),
	U3CPlayingMovieU3Ec__Iterator0_t111206440::get_offset_of_U24disposing_2(),
	U3CPlayingMovieU3Ec__Iterator0_t111206440::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (PremitivePartContent_t3696216029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4086[2] = 
{
	PremitivePartContent_t3696216029::get_offset_of_name_0(),
	PremitivePartContent_t3696216029::get_offset_of_image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (PartsManager_t197384025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4087[1] = 
{
	PartsManager_t197384025::get_offset_of_partsList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (PartsPanelSliders_t4209932670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4088[11] = 
{
	PartsPanelSliders_t4209932670::get_offset_of_PartsConfigs_2(),
	PartsPanelSliders_t4209932670::get_offset_of_PartsPanel_3(),
	PartsPanelSliders_t4209932670::get_offset_of_ActionPartsPanel_4(),
	PartsPanelSliders_t4209932670::get_offset_of_PartsBarButton_5(),
	PartsPanelSliders_t4209932670::get_offset_of_ConfigButton_6(),
	PartsPanelSliders_t4209932670::get_offset_of_ConfigPanel_7(),
	PartsPanelSliders_t4209932670::get_offset_of_BackCoreSelectButton_8(),
	PartsPanelSliders_t4209932670::get_offset_of_BackButton_9(),
	PartsPanelSliders_t4209932670::get_offset_of_CompleteButton_10(),
	PartsPanelSliders_t4209932670::get_offset_of_PartsViewPanel_11(),
	PartsPanelSliders_t4209932670::get_offset_of_IsSubWindow_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (PartsScroll_t2740354319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4089[4] = 
{
	PartsScroll_t2740354319::get_offset_of_PartsSelectSample_2(),
	PartsScroll_t2740354319::get_offset_of_PartsSelectList_3(),
	PartsScroll_t2740354319::get_offset_of_m_sCurrent_4(),
	PartsScroll_t2740354319::get_offset_of_m_PartsList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (U3CCreatePartsSelectU3Ec__Iterator0_t176489707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4090[7] = 
{
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707::get_offset_of_U3CiU3E__1_0(),
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707::get_offset_of_U3CobjU3E__2_1(),
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707::get_offset_of_U24this_2(),
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707::get_offset_of_U24current_3(),
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707::get_offset_of_U24disposing_4(),
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707::get_offset_of_U24PC_5(),
	U3CCreatePartsSelectU3Ec__Iterator0_t176489707::get_offset_of_U24locvar0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { sizeof (U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4091[2] = 
{
	U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268::get_offset_of__partsName_0(),
	U3CCreatePartsSelectU3Ec__AnonStorey1_t4248801268::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { sizeof (PartsSelector_t3170319883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4092[2] = 
{
	PartsSelector_t3170319883::get_offset_of_m_coreParts_2(),
	PartsSelector_t3170319883::get_offset_of_SelectParts_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (PartsViewPanel_t2613620109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4093[7] = 
{
	PartsViewPanel_t2613620109::get_offset_of_ActionPartsPanel_2(),
	PartsViewPanel_t2613620109::get_offset_of_BackButton_3(),
	PartsViewPanel_t2613620109::get_offset_of_CancelButton_4(),
	PartsViewPanel_t2613620109::get_offset_of_ScaleButtons_5(),
	PartsViewPanel_t2613620109::get_offset_of_CompleteButton_6(),
	PartsViewPanel_t2613620109::get_offset_of_m_Scaler_7(),
	PartsViewPanel_t2613620109::get_offset_of_m_Slider_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (RotationController_t3824016438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4094[22] = 
{
	RotationController_t3824016438::get_offset_of_OffsetSpeedX_3(),
	RotationController_t3824016438::get_offset_of_OffsetSpeedY_4(),
	RotationController_t3824016438::get_offset_of_TouchObject_5(),
	RotationController_t3824016438::get_offset_of_selectObject_6(),
	RotationController_t3824016438::get_offset_of_m_partsObject_7(),
	RotationController_t3824016438::get_offset_of_m_slidePartsObject_8(),
	RotationController_t3824016438::get_offset_of_m_poleDecisionObject_9(),
	RotationController_t3824016438::get_offset_of_m_fXangle_10(),
	RotationController_t3824016438::get_offset_of_m_fZangle_11(),
	RotationController_t3824016438::get_offset_of_m_fClickStartPos_12(),
	RotationController_t3824016438::get_offset_of_m_bPoleShift_13(),
	RotationController_t3824016438::get_offset_of_mousePos_14(),
	RotationController_t3824016438::get_offset_of_m_bMouseDirect_15(),
	RotationController_t3824016438::get_offset_of_startPos_16(),
	RotationController_t3824016438::get_offset_of_endPos_17(),
	RotationController_t3824016438::get_offset_of_SEtimer_18(),
	RotationController_t3824016438::get_offset_of_swipeDistX_19(),
	RotationController_t3824016438::get_offset_of_swipeDistY_20(),
	RotationController_t3824016438::get_offset_of_SignValueX_21(),
	RotationController_t3824016438::get_offset_of_SignValueY_22(),
	RotationController_t3824016438::get_offset_of_minSwipeDistX_23(),
	RotationController_t3824016438::get_offset_of_minSwipeDistY_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { sizeof (SelectPartsPanel_t3077390372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4095[4] = 
{
	SelectPartsPanel_t3077390372::get_offset_of_PartsBarSwitch_2(),
	SelectPartsPanel_t3077390372::get_offset_of_Scrolls_3(),
	SelectPartsPanel_t3077390372::get_offset_of_Buttons_4(),
	SelectPartsPanel_t3077390372::get_offset_of_SelectButons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { sizeof (Toast_t3649705739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4096[3] = 
{
	Toast_t3649705739::get_offset_of_ShowSecond_2(),
	Toast_t3649705739::get_offset_of_Message_3(),
	Toast_t3649705739::get_offset_of_m_fTimeElapsed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { sizeof (U3CwaitReturnOtherU3Ec__Iterator0_t1809526660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4097[4] = 
{
	U3CwaitReturnOtherU3Ec__Iterator0_t1809526660::get_offset_of_U24this_0(),
	U3CwaitReturnOtherU3Ec__Iterator0_t1809526660::get_offset_of_U24current_1(),
	U3CwaitReturnOtherU3Ec__Iterator0_t1809526660::get_offset_of_U24disposing_2(),
	U3CwaitReturnOtherU3Ec__Iterator0_t1809526660::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { sizeof (U3CwaitHideMessageU3Ec__Iterator1_t3715463830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4098[4] = 
{
	U3CwaitHideMessageU3Ec__Iterator1_t3715463830::get_offset_of_U24this_0(),
	U3CwaitHideMessageU3Ec__Iterator1_t3715463830::get_offset_of_U24current_1(),
	U3CwaitHideMessageU3Ec__Iterator1_t3715463830::get_offset_of_U24disposing_2(),
	U3CwaitHideMessageU3Ec__Iterator1_t3715463830::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (MobilityChildItem_t544399382), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
