﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HelloWorldPanel280472022.h"
#include "AssemblyU2DCSharp_HelloWorldPanel_U3CShowHelloWorl2519494053.h"
#include "AssemblyU2DCSharp_TalkPanel2596865392.h"
#include "AssemblyU2DCSharp_TalkTextFader1511400589.h"
#include "AssemblyU2DCSharp_TutorialAnimator3154002773.h"
#include "AssemblyU2DCSharp_TutorialCircles1868423179.h"
#include "AssemblyU2DCSharp_TutorialPointCircle4017332636.h"
#include "AssemblyU2DCSharp_UVScroll959786502.h"
#include "AssemblyU2DCSharp_UIPanel1795085332.h"
#include "AssemblyU2DCSharp_AreaButton1637812991.h"
#include "AssemblyU2DCSharp_AreaButton_U3C_PlayAndBlinkU3Ec_3854145987.h"
#include "AssemblyU2DCSharp_AreaInfo2759954667.h"
#include "AssemblyU2DCSharp_AreaInfo_U3CFadeInU3Ec__AnonStor3951633184.h"
#include "AssemblyU2DCSharp_AreaInfoCrossFader818510077.h"
#include "AssemblyU2DCSharp_AreaPoint3787172297.h"
#include "AssemblyU2DCSharp_AreaSelectPanel4230688919.h"
#include "AssemblyU2DCSharp_FeedbackEffect3658613102.h"
#include "AssemblyU2DCSharp_SelectAreaButtonListener2714250369.h"
#include "AssemblyU2DCSharp_SelectAreaButtonListener_U3C_Atta589740500.h"
#include "AssemblyU2DCSharp_SelectAreaButtonListener_U3C_Att2155824441.h"
#include "AssemblyU2DCSharp_SelectAreaButtonListener_U3C_Conf685583199.h"
#include "AssemblyU2DCSharp_SelectAreaButtonListener_U3C_OnSe489855987.h"
#include "AssemblyU2DCSharp_SelectAreaButtonListener_U3C_Fee3572158030.h"
#include "AssemblyU2DCSharp_WorldMapBackgroundScroll1215783667.h"
#include "AssemblyU2DCSharp_WorldMapPanel4256308156.h"
#include "AssemblyU2DCSharp_WorldMapPanel_U3CFadeOutU3Ec__Anon40377208.h"
#include "AssemblyU2DCSharp_WorldMapPanel_U3C_ShowOrderButto2723465398.h"
#include "AssemblyU2DCSharp_WorldMapPanel_U3C_ShowU3Ec__Anon2070401948.h"
#include "AssemblyU2DCSharp_DIALOG_BACKGROUND_COLOR3428445225.h"
#include "AssemblyU2DCSharp_DialogBackgroundColorExtensions215273951.h"
#include "AssemblyU2DCSharp_Dialog1378192732.h"
#include "AssemblyU2DCSharp_Prized3165049840.h"
#include "AssemblyU2DCSharp_ScrollChange47291989.h"
#include "AssemblyU2DCSharp_SliderControl94665898.h"
#include "AssemblyU2DCSharp_SliderControl_U3CResetSliderU3Ec3486515126.h"
#include "AssemblyU2DCSharp_ePartsParameter3849904106.h"
#include "AssemblyU2DCSharp_ConfigPanel537490728.h"
#include "AssemblyU2DCSharp_SliderMaster3139894563.h"
#include "AssemblyU2DCSharp_VersionNotation432231050.h"
#include "AssemblyU2DCSharp_RentalMobilmoContent501695126.h"
#include "AssemblyU2DCSharp_TutorialMobilmoSelect578122297.h"
#include "AssemblyU2DCSharp_TutorialMobilmoSelect_RentalMobi1706493197.h"
#include "AssemblyU2DCSharp_TutorialMobilmoSelect_U3CInitU3E1116120425.h"
#include "AssemblyU2DCSharp_BypassButtonEvent1736230992.h"
#include "AssemblyU2DCSharp_EnumStringBase3052423213.h"
#include "AssemblyU2DCSharp_FlowLayoutGroup655567405.h"
#include "AssemblyU2DCSharp_FlowLayoutGroup_Corner4280654597.h"
#include "AssemblyU2DCSharp_FlowLayoutGroup_Constraint4153657639.h"
#include "AssemblyU2DCSharp_RegexExample500320031.h"
#include "AssemblyU2DCSharp_RegexHypertext2320066936.h"
#include "AssemblyU2DCSharp_RegexHypertext_Entry1526823907.h"
#include "AssemblyU2DCSharp_HypertextBase3973674968.h"
#include "AssemblyU2DCSharp_HypertextBase_ClickableEntry4031026959.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_LetterSpacing3080148137.h"
#include "AssemblyU2DCSharp_RegexHypertextLinker2463221871.h"
#include "AssemblyU2DCSharp_UnityEngine_UI_RichOutline1614726946.h"
#include "AssemblyU2DCSharp_BoxOutline944150083.h"
#include "AssemblyU2DCSharp_CircleOutline2613177674.h"
#include "AssemblyU2DCSharp_ModifiedShadow522613381.h"
#include "AssemblyU2DCSharp_Outline81741273998.h"
#include "AssemblyU2DCSharp_AreaGround2255653338.h"
#include "AssemblyU2DCSharp_CombineScript1251438134.h"
#include "AssemblyU2DCSharp_FakeMobRotation4234200567.h"
#include "AssemblyU2DCSharp_IceSliderSlip1796685352.h"
#include "AssemblyU2DCSharp_PlacePrized1099533685.h"
#include "AssemblyU2DCSharp_PlanetGravity2098207264.h"
#include "AssemblyU2DCSharp_PointToGps3385345589.h"
#include "AssemblyU2DCSharp_RecordMobility2685217900.h"
#include "AssemblyU2DCSharp_RockGravity2705146017.h"
#include "AssemblyU2DCSharp_SurfaceTension3962841259.h"
#include "AssemblyU2DCSharp_TurnStick3528168681.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Water_Water1562849653.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Water_Water_2303503718.h"
#include "AssemblyU2DCSharp_WaterCamera1875834828.h"
#include "AssemblyU2DCSharp_WaterCamera_eCAMERAFOG194227.h"
#include "AssemblyU2DCSharp_WorldContents112941966.h"
#include "AssemblyU2DCSharp_WorldWall284693796.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1254291669.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3706046914.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3259153605.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2543710313.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4077469257.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1274006168.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3629569411.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1989801646.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3790432936.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_496834990.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3850268328.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_681392339.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2913820361.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_579860294.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2174076389.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2318278682.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3313275655.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2905677972.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (HelloWorldPanel_t280472022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4200[5] = 
{
	HelloWorldPanel_t280472022::get_offset_of_HelloWorldText_2(),
	HelloWorldPanel_t280472022::get_offset_of_Frames_3(),
	HelloWorldPanel_t280472022::get_offset_of_LogoText_4(),
	HelloWorldPanel_t280472022::get_offset_of_TextSpeed_5(),
	HelloWorldPanel_t280472022::get_offset_of_IsComplete_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (U3CShowHelloWorldU3Ec__AnonStorey0_t2519494053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4201[2] = 
{
	U3CShowHelloWorldU3Ec__AnonStorey0_t2519494053::get_offset_of_sChar_0(),
	U3CShowHelloWorldU3Ec__AnonStorey0_t2519494053::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (TalkPanel_t2596865392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4202[5] = 
{
	TalkPanel_t2596865392::get_offset_of_Navit_2(),
	TalkPanel_t2596865392::get_offset_of_TalkField_3(),
	TalkPanel_t2596865392::get_offset_of_NavitName_4(),
	TalkPanel_t2596865392::get_offset_of_TalkText_5(),
	TalkPanel_t2596865392::get_offset_of_CanvasGroup_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (TalkTextFader_t1511400589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4203[3] = 
{
	TalkTextFader_t1511400589::get_offset_of_IsComplete_2(),
	TalkTextFader_t1511400589::get_offset_of_m_bIsFade_3(),
	TalkTextFader_t1511400589::get_offset_of_m_Text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (TutorialAnimator_t3154002773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4204[7] = 
{
	TutorialAnimator_t3154002773::get_offset_of_Circles_2(),
	TutorialAnimator_t3154002773::get_offset_of_HelloWorldPanel_3(),
	TutorialAnimator_t3154002773::get_offset_of_TalkPanel_4(),
	TutorialAnimator_t3154002773::get_offset_of_FrameTime_5(),
	TutorialAnimator_t3154002773::get_offset_of_m_HelloWorldPanel_6(),
	TutorialAnimator_t3154002773::get_offset_of_m_bStart_7(),
	TutorialAnimator_t3154002773::get_offset_of_m_bIsAnimation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (TutorialCircles_t1868423179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4205[2] = 
{
	TutorialCircles_t1868423179::get_offset_of_CircleObjs_2(),
	TutorialCircles_t1868423179::get_offset_of_CircleSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (TutorialPointCircle_t4017332636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4206[1] = 
{
	TutorialPointCircle_t4017332636::get_offset_of_Circle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (UVScroll_t959786502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4207[2] = 
{
	UVScroll_t959786502::get_offset_of_m_Background_2(),
	UVScroll_t959786502::get_offset_of_m_vUV_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (UIPanel_t1795085332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (AreaButton_t1637812991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4209[3] = 
{
	AreaButton_t1637812991::get_offset_of_Frame_2(),
	AreaButton_t1637812991::get_offset_of_Decided_3(),
	AreaButton_t1637812991::get_offset_of_m_CrossFadeTween_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (U3C_PlayAndBlinkU3Ec__AnonStorey0_t3854145987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4210[2] = 
{
	U3C_PlayAndBlinkU3Ec__AnonStorey0_t3854145987::get_offset_of_blinkTime_0(),
	U3C_PlayAndBlinkU3Ec__AnonStorey0_t3854145987::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (AreaInfo_t2759954667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4211[5] = 
{
	AreaInfo_t2759954667::get_offset_of_Infos_2(),
	AreaInfo_t2759954667::get_offset_of_Icon_3(),
	AreaInfo_t2759954667::get_offset_of_AreaName_4(),
	AreaInfo_t2759954667::get_offset_of_AreaLeads_5(),
	AreaInfo_t2759954667::get_offset_of_AreaNameOutline_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (U3CFadeInU3Ec__AnonStorey0_t3951633184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4212[3] = 
{
	U3CFadeInU3Ec__AnonStorey0_t3951633184::get_offset_of_normal_0(),
	U3CFadeInU3Ec__AnonStorey0_t3951633184::get_offset_of_time_1(),
	U3CFadeInU3Ec__AnonStorey0_t3951633184::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (AreaInfoCrossFader_t818510077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4213[3] = 
{
	AreaInfoCrossFader_t818510077::get_offset_of_AreaInfos_2(),
	AreaInfoCrossFader_t818510077::get_offset_of_CurrentIndex_3(),
	AreaInfoCrossFader_t818510077::get_offset_of_First_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (AreaPoint_t3787172297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4214[8] = 
{
	AreaPoint_t3787172297::get_offset_of_Trigger_2(),
	AreaPoint_t3787172297::get_offset_of_AreaPoints_3(),
	AreaPoint_t3787172297::get_offset_of_Unselected_4(),
	AreaPoint_t3787172297::get_offset_of_Selection_5(),
	AreaPoint_t3787172297::get_offset_of_UnselectedName_6(),
	AreaPoint_t3787172297::get_offset_of_SelectionName_7(),
	AreaPoint_t3787172297::get_offset_of_Line_8(),
	AreaPoint_t3787172297::get_offset_of_AreaPointButton_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (AreaSelectPanel_t4230688919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4215[1] = 
{
	AreaSelectPanel_t4230688919::get_offset_of_m_Fader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (FeedbackEffect_t3658613102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4216[13] = 
{
	FeedbackEffect_t3658613102::get_offset_of_Blend_2(),
	FeedbackEffect_t3658613102::get_offset_of_Shader_3(),
	FeedbackEffect_t3658613102::get_offset_of_Center_4(),
	FeedbackEffect_t3658613102::get_offset_of_m_fAspect_5(),
	FeedbackEffect_t3658613102::get_offset_of_m_fAlpha_6(),
	FeedbackEffect_t3658613102::get_offset_of_m_fScale_7(),
	FeedbackEffect_t3658613102::get_offset_of_m_fRotation_8(),
	FeedbackEffect_t3658613102::get_offset_of_m_AccumTexture_9(),
	FeedbackEffect_t3658613102::get_offset_of_m_sMainTexID_10(),
	FeedbackEffect_t3658613102::get_offset_of_m_sBlendID_11(),
	FeedbackEffect_t3658613102::get_offset_of_m_sRotateID_12(),
	FeedbackEffect_t3658613102::get_offset_of_m_sCenterID_13(),
	FeedbackEffect_t3658613102::get_offset_of_m_Material_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (SelectAreaButtonListener_t2714250369), -1, sizeof(SelectAreaButtonListener_t2714250369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4217[6] = 
{
	SelectAreaButtonListener_t2714250369::get_offset_of_GoButton_2(),
	SelectAreaButtonListener_t2714250369::get_offset_of_GoButtonTriggers_3(),
	SelectAreaButtonListener_t2714250369::get_offset_of_AreaButtons_4(),
	SelectAreaButtonListener_t2714250369::get_offset_of_AreaButtonTriggers_5(),
	SelectAreaButtonListener_t2714250369::get_offset_of_Panel_6(),
	SelectAreaButtonListener_t2714250369_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { sizeof (U3C_AttachAreaSelectU3Ec__AnonStorey0_t589740500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4218[2] = 
{
	U3C_AttachAreaSelectU3Ec__AnonStorey0_t589740500::get_offset_of_index_0(),
	U3C_AttachAreaSelectU3Ec__AnonStorey0_t589740500::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (U3C_AttachAreaSelectU3Ec__AnonStorey1_t2155824441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4219[2] = 
{
	U3C_AttachAreaSelectU3Ec__AnonStorey1_t2155824441::get_offset_of_index_0(),
	U3C_AttachAreaSelectU3Ec__AnonStorey1_t2155824441::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (U3C_ConfirmTereportU3Ec__AnonStorey2_t685583199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4220[2] = 
{
	U3C_ConfirmTereportU3Ec__AnonStorey2_t685583199::get_offset_of_index_0(),
	U3C_ConfirmTereportU3Ec__AnonStorey2_t685583199::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (U3C_OnSelectMoveU3Ec__AnonStorey3_t489855987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4221[2] = 
{
	U3C_OnSelectMoveU3Ec__AnonStorey3_t489855987::get_offset_of_index_0(),
	U3C_OnSelectMoveU3Ec__AnonStorey3_t489855987::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4222[3] = 
{
	U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030::get_offset_of_scale_0(),
	U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030::get_offset_of_index_1(),
	U3C_FeedbackEffectU3Ec__AnonStorey4_t3572158030::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (WorldMapBackgroundScroll_t1215783667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4223[2] = 
{
	WorldMapBackgroundScroll_t1215783667::get_offset_of_m_Background_2(),
	WorldMapBackgroundScroll_t1215783667::get_offset_of_m_vUV_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (WorldMapPanel_t4256308156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4224[23] = 
{
	WorldMapPanel_t4256308156::get_offset_of_CloseButtonTrigger_2(),
	WorldMapPanel_t4256308156::get_offset_of_Go_3(),
	WorldMapPanel_t4256308156::get_offset_of_GoOutline_4(),
	WorldMapPanel_t4256308156::get_offset_of_Frames_5(),
	WorldMapPanel_t4256308156::get_offset_of_AreaIcons_6(),
	WorldMapPanel_t4256308156::get_offset_of_GoButtonGroup_7(),
	WorldMapPanel_t4256308156::get_offset_of_SelectButtons_8(),
	WorldMapPanel_t4256308156::get_offset_of_AreaButtons_9(),
	WorldMapPanel_t4256308156::get_offset_of_SelectPanel_10(),
	WorldMapPanel_t4256308156::get_offset_of_SelectPanelGroup_11(),
	WorldMapPanel_t4256308156::get_offset_of_AreaPoints_12(),
	WorldMapPanel_t4256308156::get_offset_of_IndexArea_13(),
	WorldMapPanel_t4256308156::get_offset_of_Offset_14(),
	WorldMapPanel_t4256308156::get_offset_of_OneBounceEasing_15(),
	WorldMapPanel_t4256308156::get_offset_of_WorldMapEffect_16(),
	WorldMapPanel_t4256308156::get_offset_of_Panel_17(),
	WorldMapPanel_t4256308156::get_offset_of_FadeOutTime_18(),
	WorldMapPanel_t4256308156::get_offset_of_m_RectTransSelectButtons_19(),
	WorldMapPanel_t4256308156::get_offset_of_m_AreaButtonsGroup_20(),
	WorldMapPanel_t4256308156::get_offset_of_m_bSelectAreaTween_21(),
	WorldMapPanel_t4256308156::get_offset_of_m_RectTransCloseButton_22(),
	WorldMapPanel_t4256308156::get_offset_of_m_RectTransFrames_23(),
	WorldMapPanel_t4256308156::get_offset_of_m_bSkip_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (U3CFadeOutU3Ec__AnonStorey0_t40377208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4225[2] = 
{
	U3CFadeOutU3Ec__AnonStorey0_t40377208::get_offset_of_alpha_0(),
	U3CFadeOutU3Ec__AnonStorey0_t40377208::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (U3C_ShowOrderButtonsU3Ec__AnonStorey1_t2723465398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4226[2] = 
{
	U3C_ShowOrderButtonsU3Ec__AnonStorey1_t2723465398::get_offset_of_blinkTime_0(),
	U3C_ShowOrderButtonsU3Ec__AnonStorey1_t2723465398::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (U3C_ShowU3Ec__AnonStorey2_t2070401948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4227[3] = 
{
	U3C_ShowU3Ec__AnonStorey2_t2070401948::get_offset_of_normal_0(),
	U3C_ShowU3Ec__AnonStorey2_t2070401948::get_offset_of_time_1(),
	U3C_ShowU3Ec__AnonStorey2_t2070401948::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (DIALOG_BACKGROUND_COLOR_t3428445225)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4228[3] = 
{
	DIALOG_BACKGROUND_COLOR_t3428445225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (DialogBackgroundColorExtensions_t215273951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (Dialog_t1378192732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4230[15] = 
{
	Dialog_t1378192732::get_offset_of_Background_2(),
	Dialog_t1378192732::get_offset_of_FadeTime_3(),
	Dialog_t1378192732::get_offset_of_TitleText_4(),
	Dialog_t1378192732::get_offset_of_InfoText_5(),
	Dialog_t1378192732::get_offset_of_Panel_6(),
	Dialog_t1378192732::get_offset_of_OkButton_7(),
	Dialog_t1378192732::get_offset_of_NgButton_8(),
	Dialog_t1378192732::get_offset_of_BackButton_9(),
	Dialog_t1378192732::get_offset_of_BackgroundBlur_10(),
	Dialog_t1378192732::get_offset_of_Material_11(),
	Dialog_t1378192732::get_offset_of_OkAction_12(),
	Dialog_t1378192732::get_offset_of_NgAction_13(),
	Dialog_t1378192732::get_offset_of_m_DialogBackground_14(),
	Dialog_t1378192732::get_offset_of_m_bFade_15(),
	Dialog_t1378192732::get_offset_of_m_fMaterialRadius_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { sizeof (Prized_t3165049840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4231[7] = 
{
	Prized_t3165049840::get_offset_of_PrizeImage_2(),
	Prized_t3165049840::get_offset_of_PrizedText_3(),
	Prized_t3165049840::get_offset_of_Background_4(),
	Prized_t3165049840::get_offset_of_RotateSpeed_5(),
	Prized_t3165049840::get_offset_of_FadeTime_6(),
	Prized_t3165049840::get_offset_of_prizedIdcol_7(),
	Prized_t3165049840::get_offset_of_IsDisp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (ScrollChange_t47291989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4232[5] = 
{
	ScrollChange_t47291989::get_offset_of_rectran_2(),
	ScrollChange_t47291989::get_offset_of_recver_3(),
	ScrollChange_t47291989::get_offset_of_mousePos_4(),
	ScrollChange_t47291989::get_offset_of_scrollON_5(),
	ScrollChange_t47291989::get_offset_of_m_fClickStartPos_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (SliderControl_t94665898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4233[6] = 
{
	SliderControl_t94665898::get_offset_of_sliderVol_2(),
	SliderControl_t94665898::get_offset_of_RotetoVal_3(),
	SliderControl_t94665898::get_offset_of_Pole_4(),
	SliderControl_t94665898::get_offset_of_m_modelingConfigPanel_5(),
	SliderControl_t94665898::get_offset_of_slideber_6(),
	SliderControl_t94665898::get_offset_of_prevValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (U3CResetSliderU3Ec__Iterator0_t3486515126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4234[4] = 
{
	U3CResetSliderU3Ec__Iterator0_t3486515126::get_offset_of_U24this_0(),
	U3CResetSliderU3Ec__Iterator0_t3486515126::get_offset_of_U24current_1(),
	U3CResetSliderU3Ec__Iterator0_t3486515126::get_offset_of_U24disposing_2(),
	U3CResetSliderU3Ec__Iterator0_t3486515126::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (ePartsParameter_t3849904106)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4235[3] = 
{
	ePartsParameter_t3849904106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (ConfigPanel_t537490728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4236[3] = 
{
	ConfigPanel_t537490728::get_offset_of_Rotation_0(),
	ConfigPanel_t537490728::get_offset_of_SlopeX_1(),
	ConfigPanel_t537490728::get_offset_of_SlopeY_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (SliderMaster_t3139894563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4237[4] = 
{
	SliderMaster_t3139894563::get_offset_of_ConfigPanels_3(),
	SliderMaster_t3139894563::get_offset_of_EnagySlider_4(),
	SliderMaster_t3139894563::get_offset_of_ActionEnagySlider_5(),
	SliderMaster_t3139894563::get_offset_of_PanelOn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (VersionNotation_t432231050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4238[2] = 
{
	VersionNotation_t432231050::get_offset_of_m_VersionTextAsset_2(),
	VersionNotation_t432231050::get_offset_of_m_Version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (RentalMobilmoContent_t501695126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4239[4] = 
{
	RentalMobilmoContent_t501695126::get_offset_of_Name_2(),
	RentalMobilmoContent_t501695126::get_offset_of_Image_3(),
	RentalMobilmoContent_t501695126::get_offset_of_Info_4(),
	RentalMobilmoContent_t501695126::get_offset_of_SelectButton_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (TutorialMobilmoSelect_t578122297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4240[11] = 
{
	TutorialMobilmoSelect_t578122297::get_offset_of_Info01_2(),
	TutorialMobilmoSelect_t578122297::get_offset_of_Info02_3(),
	TutorialMobilmoSelect_t578122297::get_offset_of_RentalMobilmoSelect_4(),
	TutorialMobilmoSelect_t578122297::get_offset_of_SelectMobilmoPanel_5(),
	TutorialMobilmoSelect_t578122297::get_offset_of_SelectedNameText_6(),
	TutorialMobilmoSelect_t578122297::get_offset_of_SelectedImage_7(),
	TutorialMobilmoSelect_t578122297::get_offset_of_RentalMobilmoListParent_8(),
	TutorialMobilmoSelect_t578122297::get_offset_of_RentalMobi_9(),
	TutorialMobilmoSelect_t578122297::get_offset_of_SelectMobilmoNum_10(),
	TutorialMobilmoSelect_t578122297::get_offset_of_RentalContentSample_11(),
	TutorialMobilmoSelect_t578122297::get_offset_of_m_Canvas_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (RentalMobilmo_t1706493197)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4241[10] = 
{
	RentalMobilmo_t1706493197::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_Image_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_MobilmoJson_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_RecodingJson_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_CreateMobilmoJson_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_CreateMotionJson_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_CustomJointParentNum_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_CustomJointChildNum_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_Info_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RentalMobilmo_t1706493197::get_offset_of_content_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (U3CInitU3Ec__AnonStorey0_t1116120425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4242[2] = 
{
	U3CInitU3Ec__AnonStorey0_t1116120425::get_offset_of_n_0(),
	U3CInitU3Ec__AnonStorey0_t1116120425::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (BypassButtonEvent_t1736230992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4243[1] = 
{
	BypassButtonEvent_t1736230992::get_offset_of_Target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (EnumStringBase_t3052423213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4244[1] = 
{
	EnumStringBase_t3052423213::get_offset_of_m_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4245[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4246[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { sizeof (FlowLayoutGroup_t655567405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4247[10] = 
{
	FlowLayoutGroup_t655567405::get_offset_of_m_CellSize_10(),
	FlowLayoutGroup_t655567405::get_offset_of_m_Spacing_11(),
	FlowLayoutGroup_t655567405::get_offset_of_cellsPerMainAxis_12(),
	FlowLayoutGroup_t655567405::get_offset_of_actualCellCountX_13(),
	FlowLayoutGroup_t655567405::get_offset_of_actualCellCountY_14(),
	FlowLayoutGroup_t655567405::get_offset_of_positionX_15(),
	FlowLayoutGroup_t655567405::get_offset_of_positionY_16(),
	FlowLayoutGroup_t655567405::get_offset_of_totalWidth_17(),
	FlowLayoutGroup_t655567405::get_offset_of_totalHeight_18(),
	FlowLayoutGroup_t655567405::get_offset_of_lastMaxHeight_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { sizeof (Corner_t4280654597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4248[5] = 
{
	Corner_t4280654597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (Constraint_t4153657639)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4249[4] = 
{
	Constraint_t4153657639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (RegexExample_t500320031), -1, sizeof(RegexExample_t500320031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4250[5] = 
{
	RegexExample_t500320031::get_offset_of__text_2(),
	0,
	0,
	RegexExample_t500320031_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	RegexExample_t500320031_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (RegexHypertext_t2320066936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4251[1] = 
{
	RegexHypertext_t2320066936::get_offset_of__entryTable_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (Entry_t1526823907)+ sizeof (Il2CppObject), sizeof(Entry_t1526823907_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4252[3] = 
{
	Entry_t1526823907::get_offset_of_RegexPattern_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t1526823907::get_offset_of_Color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t1526823907::get_offset_of_OnClick_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (HypertextBase_t3973674968), -1, sizeof(HypertextBase_t3973674968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4253[4] = 
{
	HypertextBase_t3973674968::get_offset_of__rootCanvas_35(),
	0,
	HypertextBase_t3973674968::get_offset_of__entries_37(),
	HypertextBase_t3973674968_StaticFields::get_offset_of__verticesPool_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (ClickableEntry_t4031026959)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4254[5] = 
{
	ClickableEntry_t4031026959::get_offset_of_Word_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClickableEntry_t4031026959::get_offset_of_StartIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClickableEntry_t4031026959::get_offset_of_Color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClickableEntry_t4031026959::get_offset_of_OnClick_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ClickableEntry_t4031026959::get_offset_of_Rects_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4255[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (LetterSpacing_t3080148137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4256[3] = 
{
	0,
	LetterSpacing_t3080148137::get_offset_of_useRichText_4(),
	LetterSpacing_t3080148137::get_offset_of_m_spacing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (RegexHypertextLinker_t2463221871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4257[2] = 
{
	RegexHypertextLinker_t2463221871::get_offset_of_Text_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (RichOutline_t1614726946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4258[1] = 
{
	RichOutline_t1614726946::get_offset_of_copyCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (BoxOutline_t944150083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4259[3] = 
{
	0,
	BoxOutline_t944150083::get_offset_of_m_halfSampleCountX_8(),
	BoxOutline_t944150083::get_offset_of_m_halfSampleCountY_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (CircleOutline_t2613177674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4260[3] = 
{
	CircleOutline_t2613177674::get_offset_of_m_circleCount_7(),
	CircleOutline_t2613177674::get_offset_of_m_firstSample_8(),
	CircleOutline_t2613177674::get_offset_of_m_sampleIncrement_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4261[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (ModifiedShadow_t522613381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4263[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (Outline8_t1741273998), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (AreaGround_t2255653338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4265[2] = 
{
	AreaGround_t2255653338::get_offset_of_areaType_2(),
	AreaGround_t2255653338::get_offset_of_SEtimer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (CombineScript_t1251438134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4266[1] = 
{
	CombineScript_t1251438134::get_offset_of_targetMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (FakeMobRotation_t4234200567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4267[6] = 
{
	FakeMobRotation_t4234200567::get_offset_of_x_2(),
	FakeMobRotation_t4234200567::get_offset_of_y_3(),
	FakeMobRotation_t4234200567::get_offset_of_z_4(),
	FakeMobRotation_t4234200567::get_offset_of_rotateVal_5(),
	FakeMobRotation_t4234200567::get_offset_of_hands_6(),
	FakeMobRotation_t4234200567::get_offset_of_movechange_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (IceSliderSlip_t1796685352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4268[2] = 
{
	IceSliderSlip_t1796685352::get_offset_of_PlayerMoblimoCol_2(),
	IceSliderSlip_t1796685352::get_offset_of_SliderTarget_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (PlacePrized_t1099533685), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { sizeof (PlanetGravity_t2098207264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4270[9] = 
{
	PlanetGravity_t2098207264::get_offset_of_planet_2(),
	PlanetGravity_t2098207264::get_offset_of_m_fGravity_3(),
	PlanetGravity_t2098207264::get_offset_of_gravityLevel_4(),
	PlanetGravity_t2098207264::get_offset_of_accelerationScale_5(),
	PlanetGravity_t2098207264::get_offset_of_windScale_6(),
	PlanetGravity_t2098207264::get_offset_of_inWater_7(),
	PlanetGravity_t2098207264::get_offset_of_direction_8(),
	PlanetGravity_t2098207264::get_offset_of_waterSoundTimer_9(),
	PlanetGravity_t2098207264::get_offset_of_colList_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { sizeof (PointToGps_t3385345589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (RecordMobility_t2685217900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4272[11] = 
{
	RecordMobility_t2685217900::get_offset_of_m_MobilityArea_2(),
	RecordMobility_t2685217900::get_offset_of_Controller_3(),
	RecordMobility_t2685217900::get_offset_of_MobilityJson_4(),
	RecordMobility_t2685217900::get_offset_of_FakeMob_5(),
	RecordMobility_t2685217900::get_offset_of_m_CopyObj_6(),
	RecordMobility_t2685217900::get_offset_of_corePastsChildTra_7(),
	RecordMobility_t2685217900::get_offset_of_m_mobilmoDnaController_8(),
	RecordMobility_t2685217900::get_offset_of_Rank_9(),
	RecordMobility_t2685217900::get_offset_of_MobilityName_10(),
	RecordMobility_t2685217900::get_offset_of_UserName_11(),
	RecordMobility_t2685217900::get_offset_of_Reaction_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (RockGravity_t2705146017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4273[3] = 
{
	RockGravity_t2705146017::get_offset_of_m_fAccelerationScale_2(),
	RockGravity_t2705146017::get_offset_of_m_gPlanet_3(),
	RockGravity_t2705146017::get_offset_of_rockDrop_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { sizeof (SurfaceTension_t3962841259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4274[3] = 
{
	SurfaceTension_t3962841259::get_offset_of_Tension_2(),
	SurfaceTension_t3962841259::get_offset_of_defaultTension_3(),
	SurfaceTension_t3962841259::get_offset_of_off_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (TurnStick_t3528168681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4275[4] = 
{
	TurnStick_t3528168681::get_offset_of_TurnUI_2(),
	TurnStick_t3528168681::get_offset_of_m_vStartPos_3(),
	TurnStick_t3528168681::get_offset_of_m_TurnStick_4(),
	TurnStick_t3528168681::get_offset_of_m_bTurnButton_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { sizeof (Water_t1562849653), -1, sizeof(Water_t1562849653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4276[14] = 
{
	Water_t1562849653::get_offset_of_waterMode_2(),
	Water_t1562849653::get_offset_of_disablePixelLights_3(),
	Water_t1562849653::get_offset_of_textureSize_4(),
	Water_t1562849653::get_offset_of_clipPlaneOffset_5(),
	Water_t1562849653::get_offset_of_reflectLayers_6(),
	Water_t1562849653::get_offset_of_refractLayers_7(),
	Water_t1562849653::get_offset_of_m_ReflectionCameras_8(),
	Water_t1562849653::get_offset_of_m_RefractionCameras_9(),
	Water_t1562849653::get_offset_of_m_ReflectionTexture_10(),
	Water_t1562849653::get_offset_of_m_RefractionTexture_11(),
	Water_t1562849653::get_offset_of_m_HardwareWaterSupport_12(),
	Water_t1562849653::get_offset_of_m_OldReflectionTextureSize_13(),
	Water_t1562849653::get_offset_of_m_OldRefractionTextureSize_14(),
	Water_t1562849653_StaticFields::get_offset_of_s_InsideWater_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { sizeof (WaterMode_t2303503718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4277[4] = 
{
	WaterMode_t2303503718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (WaterCamera_t1875834828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4278[9] = 
{
	WaterCamera_t1875834828::get_offset_of_nowCameraFog_2(),
	WaterCamera_t1875834828::get_offset_of_nextCameraFog_3(),
	WaterCamera_t1875834828::get_offset_of_WaterFillter_4(),
	WaterCamera_t1875834828::get_offset_of_direction_5(),
	WaterCamera_t1875834828::get_offset_of_dist_6(),
	WaterCamera_t1875834828::get_offset_of_inWater_7(),
	WaterCamera_t1875834828::get_offset_of_planet_8(),
	WaterCamera_t1875834828::get_offset_of_outWater_9(),
	WaterCamera_t1875834828::get_offset_of_underWater_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { sizeof (eCAMERAFOG_t194227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4279[4] = 
{
	eCAMERAFOG_t194227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (WorldContents_t112941966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4280[6] = 
{
	WorldContents_t112941966::get_offset_of_MyPageButton_2(),
	WorldContents_t112941966::get_offset_of_Menus_3(),
	WorldContents_t112941966::get_offset_of_WorldMapButton_4(),
	WorldContents_t112941966::get_offset_of_MiniMap_5(),
	WorldContents_t112941966::get_offset_of_Navigate_6(),
	WorldContents_t112941966::get_offset_of_m_bisTutorialRecordReady_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (WorldWall_t284693796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4281[1] = 
{
	WorldWall_t284693796::get_offset_of_Distance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { sizeof (AAMode_t1254291669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4282[8] = 
{
	AAMode_t1254291669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { sizeof (Antialiasing_t3706046914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4283[22] = 
{
	Antialiasing_t3706046914::get_offset_of_mode_6(),
	Antialiasing_t3706046914::get_offset_of_showGeneratedNormals_7(),
	Antialiasing_t3706046914::get_offset_of_offsetScale_8(),
	Antialiasing_t3706046914::get_offset_of_blurRadius_9(),
	Antialiasing_t3706046914::get_offset_of_edgeThresholdMin_10(),
	Antialiasing_t3706046914::get_offset_of_edgeThreshold_11(),
	Antialiasing_t3706046914::get_offset_of_edgeSharpness_12(),
	Antialiasing_t3706046914::get_offset_of_dlaaSharp_13(),
	Antialiasing_t3706046914::get_offset_of_ssaaShader_14(),
	Antialiasing_t3706046914::get_offset_of_ssaa_15(),
	Antialiasing_t3706046914::get_offset_of_dlaaShader_16(),
	Antialiasing_t3706046914::get_offset_of_dlaa_17(),
	Antialiasing_t3706046914::get_offset_of_nfaaShader_18(),
	Antialiasing_t3706046914::get_offset_of_nfaa_19(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAPreset2_20(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAPreset2_21(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAPreset3_22(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAPreset3_23(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAII_24(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAII_25(),
	Antialiasing_t3706046914::get_offset_of_shaderFXAAIII_26(),
	Antialiasing_t3706046914::get_offset_of_materialFXAAIII_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (Bloom_t3259153605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4284[30] = 
{
	Bloom_t3259153605::get_offset_of_tweakMode_6(),
	Bloom_t3259153605::get_offset_of_screenBlendMode_7(),
	Bloom_t3259153605::get_offset_of_hdr_8(),
	Bloom_t3259153605::get_offset_of_doHdr_9(),
	Bloom_t3259153605::get_offset_of_sepBlurSpread_10(),
	Bloom_t3259153605::get_offset_of_quality_11(),
	Bloom_t3259153605::get_offset_of_bloomIntensity_12(),
	Bloom_t3259153605::get_offset_of_bloomThreshold_13(),
	Bloom_t3259153605::get_offset_of_bloomThresholdColor_14(),
	Bloom_t3259153605::get_offset_of_bloomBlurIterations_15(),
	Bloom_t3259153605::get_offset_of_hollywoodFlareBlurIterations_16(),
	Bloom_t3259153605::get_offset_of_flareRotation_17(),
	Bloom_t3259153605::get_offset_of_lensflareMode_18(),
	Bloom_t3259153605::get_offset_of_hollyStretchWidth_19(),
	Bloom_t3259153605::get_offset_of_lensflareIntensity_20(),
	Bloom_t3259153605::get_offset_of_lensflareThreshold_21(),
	Bloom_t3259153605::get_offset_of_lensFlareSaturation_22(),
	Bloom_t3259153605::get_offset_of_flareColorA_23(),
	Bloom_t3259153605::get_offset_of_flareColorB_24(),
	Bloom_t3259153605::get_offset_of_flareColorC_25(),
	Bloom_t3259153605::get_offset_of_flareColorD_26(),
	Bloom_t3259153605::get_offset_of_lensFlareVignetteMask_27(),
	Bloom_t3259153605::get_offset_of_lensFlareShader_28(),
	Bloom_t3259153605::get_offset_of_lensFlareMaterial_29(),
	Bloom_t3259153605::get_offset_of_screenBlendShader_30(),
	Bloom_t3259153605::get_offset_of_screenBlend_31(),
	Bloom_t3259153605::get_offset_of_blurAndFlaresShader_32(),
	Bloom_t3259153605::get_offset_of_blurAndFlaresMaterial_33(),
	Bloom_t3259153605::get_offset_of_brightPassFilterShader_34(),
	Bloom_t3259153605::get_offset_of_brightPassFilterMaterial_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (LensFlareStyle_t2543710313)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4285[4] = 
{
	LensFlareStyle_t2543710313::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { sizeof (TweakMode_t4077469257)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4286[3] = 
{
	TweakMode_t4077469257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (HDRBloomMode_t1274006168)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4287[4] = 
{
	HDRBloomMode_t1274006168::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (BloomScreenBlendMode_t3629569411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4288[3] = 
{
	BloomScreenBlendMode_t3629569411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (BloomQuality_t1989801646)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4289[3] = 
{
	BloomQuality_t1989801646::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (LensflareStyle34_t3790432936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4290[4] = 
{
	LensflareStyle34_t3790432936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { sizeof (TweakMode34_t496834990)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4291[3] = 
{
	TweakMode34_t496834990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { sizeof (HDRBloomMode_t3850268328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4292[4] = 
{
	HDRBloomMode_t3850268328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { sizeof (BloomScreenBlendMode_t681392339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4293[3] = 
{
	BloomScreenBlendMode_t681392339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { sizeof (BloomAndFlares_t2913820361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4294[34] = 
{
	BloomAndFlares_t2913820361::get_offset_of_tweakMode_6(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlendMode_7(),
	BloomAndFlares_t2913820361::get_offset_of_hdr_8(),
	BloomAndFlares_t2913820361::get_offset_of_doHdr_9(),
	BloomAndFlares_t2913820361::get_offset_of_sepBlurSpread_10(),
	BloomAndFlares_t2913820361::get_offset_of_useSrcAlphaAsMask_11(),
	BloomAndFlares_t2913820361::get_offset_of_bloomIntensity_12(),
	BloomAndFlares_t2913820361::get_offset_of_bloomThreshold_13(),
	BloomAndFlares_t2913820361::get_offset_of_bloomBlurIterations_14(),
	BloomAndFlares_t2913820361::get_offset_of_lensflares_15(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlareBlurIterations_16(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareMode_17(),
	BloomAndFlares_t2913820361::get_offset_of_hollyStretchWidth_18(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareIntensity_19(),
	BloomAndFlares_t2913820361::get_offset_of_lensflareThreshold_20(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorA_21(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorB_22(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorC_23(),
	BloomAndFlares_t2913820361::get_offset_of_flareColorD_24(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareVignetteMask_25(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareShader_26(),
	BloomAndFlares_t2913820361::get_offset_of_lensFlareMaterial_27(),
	BloomAndFlares_t2913820361::get_offset_of_vignetteShader_28(),
	BloomAndFlares_t2913820361::get_offset_of_vignetteMaterial_29(),
	BloomAndFlares_t2913820361::get_offset_of_separableBlurShader_30(),
	BloomAndFlares_t2913820361::get_offset_of_separableBlurMaterial_31(),
	BloomAndFlares_t2913820361::get_offset_of_addBrightStuffOneOneShader_32(),
	BloomAndFlares_t2913820361::get_offset_of_addBrightStuffBlendOneOneMaterial_33(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlendShader_34(),
	BloomAndFlares_t2913820361::get_offset_of_screenBlend_35(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlaresShader_36(),
	BloomAndFlares_t2913820361::get_offset_of_hollywoodFlaresMaterial_37(),
	BloomAndFlares_t2913820361::get_offset_of_brightPassFilterShader_38(),
	BloomAndFlares_t2913820361::get_offset_of_brightPassFilterMaterial_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { sizeof (BloomOptimized_t579860294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4295[8] = 
{
	BloomOptimized_t579860294::get_offset_of_threshold_6(),
	BloomOptimized_t579860294::get_offset_of_intensity_7(),
	BloomOptimized_t579860294::get_offset_of_blurSize_8(),
	BloomOptimized_t579860294::get_offset_of_resolution_9(),
	BloomOptimized_t579860294::get_offset_of_blurIterations_10(),
	BloomOptimized_t579860294::get_offset_of_blurType_11(),
	BloomOptimized_t579860294::get_offset_of_fastBloomShader_12(),
	BloomOptimized_t579860294::get_offset_of_fastBloomMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { sizeof (Resolution_t2174076389)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4296[3] = 
{
	Resolution_t2174076389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { sizeof (BlurType_t2318278682)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4297[3] = 
{
	BlurType_t2318278682::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { sizeof (Blur_t3313275655), -1, sizeof(Blur_t3313275655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4298[4] = 
{
	Blur_t3313275655::get_offset_of_iterations_2(),
	Blur_t3313275655::get_offset_of_blurSpread_3(),
	Blur_t3313275655::get_offset_of_blurShader_4(),
	Blur_t3313275655_StaticFields::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { sizeof (BlurOptimized_t2905677972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4299[6] = 
{
	BlurOptimized_t2905677972::get_offset_of_downsample_6(),
	BlurOptimized_t2905677972::get_offset_of_blurSize_7(),
	BlurOptimized_t2905677972::get_offset_of_blurIterations_8(),
	BlurOptimized_t2905677972::get_offset_of_blurType_9(),
	BlurOptimized_t2905677972::get_offset_of_blurShader_10(),
	BlurOptimized_t2905677972::get_offset_of_blurMaterial_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
