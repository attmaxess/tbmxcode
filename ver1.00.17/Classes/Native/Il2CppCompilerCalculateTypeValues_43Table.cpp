﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2925629392.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3591885494.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3964716834.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3523901841.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3694918262.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2385315007.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1672908095.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_163796146.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3133742431.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3095868303.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3961919654.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2459464934.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3059663215.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3904103385.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3902263335.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_457390305.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1921949650.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1282715729.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_785857862.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_859433922.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_863619287.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3948012467.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4036041399.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_517806655.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2907318477.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1887427701.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2899312312.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3375869057.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2152133263.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1845967802.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_761286994.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3972324734.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_962057283.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3563669597.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2806915419.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3465217523.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3901407784.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2916142869.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1836015611.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4146040027.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3401316463.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_430511954.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2215595694.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1171761296.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3310062628.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1008153775.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1046072227.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1381705816.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3322560050.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3949418959.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4170634026.h"
#include "AssemblyU2DCSharp_Fujioka_FSDirLight1852219918.h"
#include "AssemblyU2DCSharp_Fujioka_FSEffect3187928436.h"
#include "AssemblyU2DCSharp_Fujioka_FSEffect_ENABLE567653665.h"
#include "AssemblyU2DCSharp_Fujioka_FSEffect_ParamBackup1404423237.h"
#include "AssemblyU2DCSharp_Fujioka_FSLensDirt1842000292.h"
#include "AssemblyU2DCSharp_Fujioka_FSPlanet2551900013.h"
#include "AssemblyU2DCSharp_Fujioka_FSSkyDome3093560541.h"
#include "AssemblyU2DCSharp_Fujioka_FSSpotLight2693339535.h"
#include "AssemblyU2DCSharp_Fujioka_FSSpotLightCone1384075014.h"
#include "AssemblyU2DCSharp_Fujioka_FSTestCameraControl3583991567.h"
#include "AssemblyU2DCSharp_Fujioka_FSUnderwater2566916842.h"
#include "AssemblyU2DCSharp_CrossSection582606797.h"
#include "AssemblyU2DCSharp_Section3430619939.h"
#include "AssemblyU2DCSharp_HorizontalClippingSection3861440939.h"
#include "AssemblyU2DCSharp_SectionSetter3682766552.h"
#include "AssemblyU2DCSharp_maxCamera3827295319.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2075964317.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3113433889.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864769.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864773.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236543.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206768.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3740780830.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3740780702.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3066379751.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2882533397.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1459944466.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637719.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1500295684.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3698097168.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379926265.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637715.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206766.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236537.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236541.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864765.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1459944470.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068660.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437128.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2217126741.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437126.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206772.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236547.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068658.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4300 = { sizeof (BlurType_t2925629392)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4300[3] = 
{
	BlurType_t2925629392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4301 = { sizeof (CameraMotionBlur_t3591885494), -1, sizeof(CameraMotionBlur_t3591885494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4301[32] = 
{
	CameraMotionBlur_t3591885494_StaticFields::get_offset_of_MAX_RADIUS_6(),
	CameraMotionBlur_t3591885494::get_offset_of_filterType_7(),
	CameraMotionBlur_t3591885494::get_offset_of_preview_8(),
	CameraMotionBlur_t3591885494::get_offset_of_previewScale_9(),
	CameraMotionBlur_t3591885494::get_offset_of_movementScale_10(),
	CameraMotionBlur_t3591885494::get_offset_of_rotationScale_11(),
	CameraMotionBlur_t3591885494::get_offset_of_maxVelocity_12(),
	CameraMotionBlur_t3591885494::get_offset_of_minVelocity_13(),
	CameraMotionBlur_t3591885494::get_offset_of_velocityScale_14(),
	CameraMotionBlur_t3591885494::get_offset_of_softZDistance_15(),
	CameraMotionBlur_t3591885494::get_offset_of_velocityDownsample_16(),
	CameraMotionBlur_t3591885494::get_offset_of_excludeLayers_17(),
	CameraMotionBlur_t3591885494::get_offset_of_tmpCam_18(),
	CameraMotionBlur_t3591885494::get_offset_of_shader_19(),
	CameraMotionBlur_t3591885494::get_offset_of_dx11MotionBlurShader_20(),
	CameraMotionBlur_t3591885494::get_offset_of_replacementClear_21(),
	CameraMotionBlur_t3591885494::get_offset_of_motionBlurMaterial_22(),
	CameraMotionBlur_t3591885494::get_offset_of_dx11MotionBlurMaterial_23(),
	CameraMotionBlur_t3591885494::get_offset_of_noiseTexture_24(),
	CameraMotionBlur_t3591885494::get_offset_of_jitter_25(),
	CameraMotionBlur_t3591885494::get_offset_of_showVelocity_26(),
	CameraMotionBlur_t3591885494::get_offset_of_showVelocityScale_27(),
	CameraMotionBlur_t3591885494::get_offset_of_currentViewProjMat_28(),
	CameraMotionBlur_t3591885494::get_offset_of_currentStereoViewProjMat_29(),
	CameraMotionBlur_t3591885494::get_offset_of_prevViewProjMat_30(),
	CameraMotionBlur_t3591885494::get_offset_of_prevStereoViewProjMat_31(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameCount_32(),
	CameraMotionBlur_t3591885494::get_offset_of_wasActive_33(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameForward_34(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFrameUp_35(),
	CameraMotionBlur_t3591885494::get_offset_of_prevFramePos_36(),
	CameraMotionBlur_t3591885494::get_offset_of__camera_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4302 = { sizeof (MotionBlurFilter_t3964716834)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4302[6] = 
{
	MotionBlurFilter_t3964716834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4303 = { sizeof (ColorCorrectionCurves_t3523901841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4303[24] = 
{
	ColorCorrectionCurves_t3523901841::get_offset_of_redChannel_6(),
	ColorCorrectionCurves_t3523901841::get_offset_of_greenChannel_7(),
	ColorCorrectionCurves_t3523901841::get_offset_of_blueChannel_8(),
	ColorCorrectionCurves_t3523901841::get_offset_of_useDepthCorrection_9(),
	ColorCorrectionCurves_t3523901841::get_offset_of_zCurve_10(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthRedChannel_11(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthGreenChannel_12(),
	ColorCorrectionCurves_t3523901841::get_offset_of_depthBlueChannel_13(),
	ColorCorrectionCurves_t3523901841::get_offset_of_ccMaterial_14(),
	ColorCorrectionCurves_t3523901841::get_offset_of_ccDepthMaterial_15(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveCcMaterial_16(),
	ColorCorrectionCurves_t3523901841::get_offset_of_rgbChannelTex_17(),
	ColorCorrectionCurves_t3523901841::get_offset_of_rgbDepthChannelTex_18(),
	ColorCorrectionCurves_t3523901841::get_offset_of_zCurveTex_19(),
	ColorCorrectionCurves_t3523901841::get_offset_of_saturation_20(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveCc_21(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveFromColor_22(),
	ColorCorrectionCurves_t3523901841::get_offset_of_selectiveToColor_23(),
	ColorCorrectionCurves_t3523901841::get_offset_of_mode_24(),
	ColorCorrectionCurves_t3523901841::get_offset_of_updateTextures_25(),
	ColorCorrectionCurves_t3523901841::get_offset_of_colorCorrectionCurvesShader_26(),
	ColorCorrectionCurves_t3523901841::get_offset_of_simpleColorCorrectionCurvesShader_27(),
	ColorCorrectionCurves_t3523901841::get_offset_of_colorCorrectionSelectiveShader_28(),
	ColorCorrectionCurves_t3523901841::get_offset_of_updateTexturesOnStartup_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4304 = { sizeof (ColorCorrectionMode_t3694918262)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4304[3] = 
{
	ColorCorrectionMode_t3694918262::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4305 = { sizeof (ColorCorrectionLookup_t2385315007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4305[4] = 
{
	ColorCorrectionLookup_t2385315007::get_offset_of_shader_6(),
	ColorCorrectionLookup_t2385315007::get_offset_of_material_7(),
	ColorCorrectionLookup_t2385315007::get_offset_of_converted3DLut_8(),
	ColorCorrectionLookup_t2385315007::get_offset_of_basedOnTempTex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4306 = { sizeof (ColorCorrectionRamp_t1672908095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4306[1] = 
{
	ColorCorrectionRamp_t1672908095::get_offset_of_textureRamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4307 = { sizeof (ContrastEnhance_t163796146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4307[7] = 
{
	ContrastEnhance_t163796146::get_offset_of_intensity_6(),
	ContrastEnhance_t163796146::get_offset_of_threshold_7(),
	ContrastEnhance_t163796146::get_offset_of_separableBlurMaterial_8(),
	ContrastEnhance_t163796146::get_offset_of_contrastCompositeMaterial_9(),
	ContrastEnhance_t163796146::get_offset_of_blurSpread_10(),
	ContrastEnhance_t163796146::get_offset_of_separableBlurShader_11(),
	ContrastEnhance_t163796146::get_offset_of_contrastCompositeShader_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4308 = { sizeof (ContrastStretch_t3133742431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4308[13] = 
{
	ContrastStretch_t3133742431::get_offset_of_adaptationSpeed_2(),
	ContrastStretch_t3133742431::get_offset_of_limitMinimum_3(),
	ContrastStretch_t3133742431::get_offset_of_limitMaximum_4(),
	ContrastStretch_t3133742431::get_offset_of_adaptRenderTex_5(),
	ContrastStretch_t3133742431::get_offset_of_curAdaptIndex_6(),
	ContrastStretch_t3133742431::get_offset_of_shaderLum_7(),
	ContrastStretch_t3133742431::get_offset_of_m_materialLum_8(),
	ContrastStretch_t3133742431::get_offset_of_shaderReduce_9(),
	ContrastStretch_t3133742431::get_offset_of_m_materialReduce_10(),
	ContrastStretch_t3133742431::get_offset_of_shaderAdapt_11(),
	ContrastStretch_t3133742431::get_offset_of_m_materialAdapt_12(),
	ContrastStretch_t3133742431::get_offset_of_shaderApply_13(),
	ContrastStretch_t3133742431::get_offset_of_m_materialApply_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4309 = { sizeof (CreaseShading_t3095868303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4309[9] = 
{
	CreaseShading_t3095868303::get_offset_of_intensity_6(),
	CreaseShading_t3095868303::get_offset_of_softness_7(),
	CreaseShading_t3095868303::get_offset_of_spread_8(),
	CreaseShading_t3095868303::get_offset_of_blurShader_9(),
	CreaseShading_t3095868303::get_offset_of_blurMaterial_10(),
	CreaseShading_t3095868303::get_offset_of_depthFetchShader_11(),
	CreaseShading_t3095868303::get_offset_of_depthFetchMaterial_12(),
	CreaseShading_t3095868303::get_offset_of_creaseApplyShader_13(),
	CreaseShading_t3095868303::get_offset_of_creaseApplyMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4310 = { sizeof (DepthOfField_t3961919654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4310[25] = 
{
	DepthOfField_t3961919654::get_offset_of_visualizeFocus_6(),
	DepthOfField_t3961919654::get_offset_of_focalLength_7(),
	DepthOfField_t3961919654::get_offset_of_focalSize_8(),
	DepthOfField_t3961919654::get_offset_of_aperture_9(),
	DepthOfField_t3961919654::get_offset_of_focalTransform_10(),
	DepthOfField_t3961919654::get_offset_of_maxBlurSize_11(),
	DepthOfField_t3961919654::get_offset_of_highResolution_12(),
	DepthOfField_t3961919654::get_offset_of_blurType_13(),
	DepthOfField_t3961919654::get_offset_of_blurSampleCount_14(),
	DepthOfField_t3961919654::get_offset_of_nearBlur_15(),
	DepthOfField_t3961919654::get_offset_of_foregroundOverlap_16(),
	DepthOfField_t3961919654::get_offset_of_dofHdrShader_17(),
	DepthOfField_t3961919654::get_offset_of_dofHdrMaterial_18(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehShader_19(),
	DepthOfField_t3961919654::get_offset_of_dx11bokehMaterial_20(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehThreshold_21(),
	DepthOfField_t3961919654::get_offset_of_dx11SpawnHeuristic_22(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehTexture_23(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehScale_24(),
	DepthOfField_t3961919654::get_offset_of_dx11BokehIntensity_25(),
	DepthOfField_t3961919654::get_offset_of_focalDistance01_26(),
	DepthOfField_t3961919654::get_offset_of_cbDrawArgs_27(),
	DepthOfField_t3961919654::get_offset_of_cbPoints_28(),
	DepthOfField_t3961919654::get_offset_of_internalBlurWidth_29(),
	DepthOfField_t3961919654::get_offset_of_cachedCamera_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4311 = { sizeof (BlurType_t2459464934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4311[3] = 
{
	BlurType_t2459464934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4312 = { sizeof (BlurSampleCount_t3059663215)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4312[4] = 
{
	BlurSampleCount_t3059663215::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4313 = { sizeof (DepthOfFieldDeprecated_t3904103385), -1, sizeof(DepthOfFieldDeprecated_t3904103385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4313[43] = 
{
	DepthOfFieldDeprecated_t3904103385_StaticFields::get_offset_of_SMOOTH_DOWNSAMPLE_PASS_6(),
	DepthOfFieldDeprecated_t3904103385_StaticFields::get_offset_of_BOKEH_EXTRA_BLUR_7(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_quality_8(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_resolution_9(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_simpleTweakMode_10(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalPoint_11(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_smoothness_12(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZDistance_13(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZStartCurve_14(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalZEndCurve_15(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalStartCurve_16(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalEndCurve_17(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalDistance01_18(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_objectFocus_19(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_focalSize_20(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bluriness_21(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_maxBlurSpread_22(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_foregroundBlurExtrude_23(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofBlurShader_24(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofBlurMaterial_25(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofShader_26(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_dofMaterial_27(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_visualize_28(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehDestination_29(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_widthOverHeight_30(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_oneOverBaseSize_31(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokeh_32(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSupport_33(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehShader_34(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehTexture_35(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehScale_36(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehIntensity_37(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehThresholdContrast_38(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehThresholdLuminance_39(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehDownsample_40(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehMaterial_41(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of__camera_42(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_foregroundTexture_43(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_mediumRezWorkTexture_44(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_finalDefocus_45(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_lowRezWorkTexture_46(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSource_47(),
	DepthOfFieldDeprecated_t3904103385::get_offset_of_bokehSource2_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4314 = { sizeof (Dof34QualitySetting_t3902263335)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4314[3] = 
{
	Dof34QualitySetting_t3902263335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4315 = { sizeof (DofResolution_t457390305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4315[4] = 
{
	DofResolution_t457390305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4316 = { sizeof (DofBlurriness_t1921949650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4316[4] = 
{
	DofBlurriness_t1921949650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4317 = { sizeof (BokehDestination_t1282715729)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4317[4] = 
{
	BokehDestination_t1282715729::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4318 = { sizeof (EdgeDetection_t785857862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4318[11] = 
{
	EdgeDetection_t785857862::get_offset_of_mode_6(),
	EdgeDetection_t785857862::get_offset_of_sensitivityDepth_7(),
	EdgeDetection_t785857862::get_offset_of_sensitivityNormals_8(),
	EdgeDetection_t785857862::get_offset_of_lumThreshold_9(),
	EdgeDetection_t785857862::get_offset_of_edgeExp_10(),
	EdgeDetection_t785857862::get_offset_of_sampleDist_11(),
	EdgeDetection_t785857862::get_offset_of_edgesOnly_12(),
	EdgeDetection_t785857862::get_offset_of_edgesOnlyBgColor_13(),
	EdgeDetection_t785857862::get_offset_of_edgeDetectShader_14(),
	EdgeDetection_t785857862::get_offset_of_edgeDetectMaterial_15(),
	EdgeDetection_t785857862::get_offset_of_oldMode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4319 = { sizeof (EdgeDetectMode_t859433922)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4319[6] = 
{
	EdgeDetectMode_t859433922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4320 = { sizeof (Fisheye_t863619287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4320[4] = 
{
	Fisheye_t863619287::get_offset_of_strengthX_6(),
	Fisheye_t863619287::get_offset_of_strengthY_7(),
	Fisheye_t863619287::get_offset_of_fishEyeShader_8(),
	Fisheye_t863619287::get_offset_of_fisheyeMaterial_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4321 = { sizeof (GlobalFog_t3948012467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4321[9] = 
{
	GlobalFog_t3948012467::get_offset_of_distanceFog_6(),
	GlobalFog_t3948012467::get_offset_of_excludeFarPixels_7(),
	GlobalFog_t3948012467::get_offset_of_useRadialDistance_8(),
	GlobalFog_t3948012467::get_offset_of_heightFog_9(),
	GlobalFog_t3948012467::get_offset_of_height_10(),
	GlobalFog_t3948012467::get_offset_of_heightDensity_11(),
	GlobalFog_t3948012467::get_offset_of_startDistance_12(),
	GlobalFog_t3948012467::get_offset_of_fogShader_13(),
	GlobalFog_t3948012467::get_offset_of_fogMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4322 = { sizeof (Grayscale_t4036041399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4322[2] = 
{
	Grayscale_t4036041399::get_offset_of_textureRamp_4(),
	Grayscale_t4036041399::get_offset_of_rampOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4323 = { sizeof (ImageEffectBase_t517806655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4323[2] = 
{
	ImageEffectBase_t517806655::get_offset_of_shader_2(),
	ImageEffectBase_t517806655::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4324 = { sizeof (ImageEffects_t2907318477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4325 = { sizeof (MotionBlur_t1887427701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4325[3] = 
{
	MotionBlur_t1887427701::get_offset_of_blurAmount_4(),
	MotionBlur_t1887427701::get_offset_of_extraBlur_5(),
	MotionBlur_t1887427701::get_offset_of_accumTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4326 = { sizeof (NoiseAndGrain_t2899312312), -1, sizeof(NoiseAndGrain_t2899312312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4326[19] = 
{
	NoiseAndGrain_t2899312312::get_offset_of_intensityMultiplier_6(),
	NoiseAndGrain_t2899312312::get_offset_of_generalIntensity_7(),
	NoiseAndGrain_t2899312312::get_offset_of_blackIntensity_8(),
	NoiseAndGrain_t2899312312::get_offset_of_whiteIntensity_9(),
	NoiseAndGrain_t2899312312::get_offset_of_midGrey_10(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11Grain_11(),
	NoiseAndGrain_t2899312312::get_offset_of_softness_12(),
	NoiseAndGrain_t2899312312::get_offset_of_monochrome_13(),
	NoiseAndGrain_t2899312312::get_offset_of_intensities_14(),
	NoiseAndGrain_t2899312312::get_offset_of_tiling_15(),
	NoiseAndGrain_t2899312312::get_offset_of_monochromeTiling_16(),
	NoiseAndGrain_t2899312312::get_offset_of_filterMode_17(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseTexture_18(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseShader_19(),
	NoiseAndGrain_t2899312312::get_offset_of_noiseMaterial_20(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11NoiseShader_21(),
	NoiseAndGrain_t2899312312::get_offset_of_dx11NoiseMaterial_22(),
	NoiseAndGrain_t2899312312_StaticFields::get_offset_of_TILE_AMOUNT_23(),
	NoiseAndGrain_t2899312312::get_offset_of_mesh_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4327 = { sizeof (NoiseAndScratches_t3375869057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4327[18] = 
{
	NoiseAndScratches_t3375869057::get_offset_of_monochrome_2(),
	NoiseAndScratches_t3375869057::get_offset_of_rgbFallback_3(),
	NoiseAndScratches_t3375869057::get_offset_of_grainIntensityMin_4(),
	NoiseAndScratches_t3375869057::get_offset_of_grainIntensityMax_5(),
	NoiseAndScratches_t3375869057::get_offset_of_grainSize_6(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchIntensityMin_7(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchIntensityMax_8(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchFPS_9(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchJitter_10(),
	NoiseAndScratches_t3375869057::get_offset_of_grainTexture_11(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchTexture_12(),
	NoiseAndScratches_t3375869057::get_offset_of_shaderRGB_13(),
	NoiseAndScratches_t3375869057::get_offset_of_shaderYUV_14(),
	NoiseAndScratches_t3375869057::get_offset_of_m_MaterialRGB_15(),
	NoiseAndScratches_t3375869057::get_offset_of_m_MaterialYUV_16(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchTimeLeft_17(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchX_18(),
	NoiseAndScratches_t3375869057::get_offset_of_scratchY_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4328 = { sizeof (PostEffectsBase_t2152133263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4328[4] = 
{
	PostEffectsBase_t2152133263::get_offset_of_supportHDRTextures_2(),
	PostEffectsBase_t2152133263::get_offset_of_supportDX11_3(),
	PostEffectsBase_t2152133263::get_offset_of_isSupported_4(),
	PostEffectsBase_t2152133263::get_offset_of_createdMaterials_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4329 = { sizeof (PostEffectsHelper_t1845967802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4330 = { sizeof (Quads_t761286994), -1, sizeof(Quads_t761286994_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4330[2] = 
{
	Quads_t761286994_StaticFields::get_offset_of_meshes_0(),
	Quads_t761286994_StaticFields::get_offset_of_currentQuads_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4331 = { sizeof (ScreenOverlay_t3972324734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4331[5] = 
{
	ScreenOverlay_t3972324734::get_offset_of_blendMode_6(),
	ScreenOverlay_t3972324734::get_offset_of_intensity_7(),
	ScreenOverlay_t3972324734::get_offset_of_texture_8(),
	ScreenOverlay_t3972324734::get_offset_of_overlayShader_9(),
	ScreenOverlay_t3972324734::get_offset_of_overlayMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4332 = { sizeof (OverlayBlendMode_t962057283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4332[6] = 
{
	OverlayBlendMode_t962057283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4333 = { sizeof (ScreenSpaceAmbientObscurance_t3563669597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4333[8] = 
{
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_intensity_6(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_radius_7(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_blurIterations_8(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_blurFilterDistance_9(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_downsample_10(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_rand_11(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_aoShader_12(),
	ScreenSpaceAmbientObscurance_t3563669597::get_offset_of_aoMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4334 = { sizeof (ScreenSpaceAmbientOcclusion_t2806915419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4334[11] = 
{
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Radius_2(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SampleCount_3(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_OcclusionIntensity_4(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Blur_5(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Downsampling_6(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_OcclusionAttenuation_7(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_MinZ_8(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SSAOShader_9(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_SSAOMaterial_10(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_RandomTexture_11(),
	ScreenSpaceAmbientOcclusion_t2806915419::get_offset_of_m_Supported_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4335 = { sizeof (SSAOSamples_t3465217523)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4335[4] = 
{
	SSAOSamples_t3465217523::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4336 = { sizeof (SepiaTone_t3901407784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4337 = { sizeof (SunShafts_t2916142869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4337[14] = 
{
	SunShafts_t2916142869::get_offset_of_resolution_6(),
	SunShafts_t2916142869::get_offset_of_screenBlendMode_7(),
	SunShafts_t2916142869::get_offset_of_sunTransform_8(),
	SunShafts_t2916142869::get_offset_of_radialBlurIterations_9(),
	SunShafts_t2916142869::get_offset_of_sunColor_10(),
	SunShafts_t2916142869::get_offset_of_sunThreshold_11(),
	SunShafts_t2916142869::get_offset_of_sunShaftBlurRadius_12(),
	SunShafts_t2916142869::get_offset_of_sunShaftIntensity_13(),
	SunShafts_t2916142869::get_offset_of_maxRadius_14(),
	SunShafts_t2916142869::get_offset_of_useDepthTexture_15(),
	SunShafts_t2916142869::get_offset_of_sunShaftsShader_16(),
	SunShafts_t2916142869::get_offset_of_sunShaftsMaterial_17(),
	SunShafts_t2916142869::get_offset_of_simpleClearShader_18(),
	SunShafts_t2916142869::get_offset_of_simpleClearMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4338 = { sizeof (SunShaftsResolution_t1836015611)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4338[4] = 
{
	SunShaftsResolution_t1836015611::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4339 = { sizeof (ShaftsScreenBlendMode_t4146040027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4339[3] = 
{
	ShaftsScreenBlendMode_t4146040027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4340 = { sizeof (TiltShift_t3401316463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4340[7] = 
{
	TiltShift_t3401316463::get_offset_of_mode_6(),
	TiltShift_t3401316463::get_offset_of_quality_7(),
	TiltShift_t3401316463::get_offset_of_blurArea_8(),
	TiltShift_t3401316463::get_offset_of_maxBlurSize_9(),
	TiltShift_t3401316463::get_offset_of_downsample_10(),
	TiltShift_t3401316463::get_offset_of_tiltShiftShader_11(),
	TiltShift_t3401316463::get_offset_of_tiltShiftMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4341 = { sizeof (TiltShiftMode_t430511954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4341[3] = 
{
	TiltShiftMode_t430511954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4342 = { sizeof (TiltShiftQuality_t2215595694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4342[4] = 
{
	TiltShiftQuality_t2215595694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4343 = { sizeof (Tonemapping_t1171761296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4343[13] = 
{
	Tonemapping_t1171761296::get_offset_of_type_6(),
	Tonemapping_t1171761296::get_offset_of_adaptiveTextureSize_7(),
	Tonemapping_t1171761296::get_offset_of_remapCurve_8(),
	Tonemapping_t1171761296::get_offset_of_curveTex_9(),
	Tonemapping_t1171761296::get_offset_of_exposureAdjustment_10(),
	Tonemapping_t1171761296::get_offset_of_middleGrey_11(),
	Tonemapping_t1171761296::get_offset_of_white_12(),
	Tonemapping_t1171761296::get_offset_of_adaptionSpeed_13(),
	Tonemapping_t1171761296::get_offset_of_tonemapper_14(),
	Tonemapping_t1171761296::get_offset_of_validRenderTextureFormat_15(),
	Tonemapping_t1171761296::get_offset_of_tonemapMaterial_16(),
	Tonemapping_t1171761296::get_offset_of_rt_17(),
	Tonemapping_t1171761296::get_offset_of_rtFormat_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4344 = { sizeof (TonemapperType_t3310062628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4344[8] = 
{
	TonemapperType_t3310062628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4345 = { sizeof (AdaptiveTexSize_t1008153775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4345[8] = 
{
	AdaptiveTexSize_t1008153775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4346 = { sizeof (Triangles_t1046072227), -1, sizeof(Triangles_t1046072227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4346[2] = 
{
	Triangles_t1046072227_StaticFields::get_offset_of_meshes_0(),
	Triangles_t1046072227_StaticFields::get_offset_of_currentTris_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4347 = { sizeof (Twirl_t1381705816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4347[3] = 
{
	Twirl_t1381705816::get_offset_of_radius_4(),
	Twirl_t1381705816::get_offset_of_angle_5(),
	Twirl_t1381705816::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4348 = { sizeof (VignetteAndChromaticAberration_t3322560050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4348[14] = 
{
	VignetteAndChromaticAberration_t3322560050::get_offset_of_mode_6(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_intensity_7(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromaticAberration_8(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_axialAberration_9(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blur_10(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurSpread_11(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_luminanceDependency_12(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurDistance_13(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_vignetteShader_14(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_separableBlurShader_15(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromAberrationShader_16(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_VignetteMaterial_17(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_SeparableBlurMaterial_18(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_ChromAberrationMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4349 = { sizeof (AberrationMode_t3949418959)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4349[3] = 
{
	AberrationMode_t3949418959::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4350 = { sizeof (Vortex_t4170634026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4350[3] = 
{
	Vortex_t4170634026::get_offset_of_radius_4(),
	Vortex_t4170634026::get_offset_of_angle_5(),
	Vortex_t4170634026::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4351 = { sizeof (FSDirLight_t1852219918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4351[3] = 
{
	FSDirLight_t1852219918::get_offset_of_go__2(),
	FSDirLight_t1852219918::get_offset_of_dirLight_3(),
	FSDirLight_t1852219918::get_offset_of_flare_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4352 = { sizeof (FSEffect_t3187928436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4352[48] = 
{
	FSEffect_t3187928436::get_offset_of_enableFlags_2(),
	FSEffect_t3187928436::get_offset_of_SHADER_PROPERTY_STARTLUT_3(),
	FSEffect_t3187928436::get_offset_of_SHADER_PROPERTY_ENDLUT_4(),
	0,
	0,
	FSEffect_t3187928436::get_offset_of_atmosphere_7(),
	FSEffect_t3187928436::get_offset_of_ambientGrdColor_8(),
	FSEffect_t3187928436::get_offset_of_ambientColor_9(),
	FSEffect_t3187928436::get_offset_of_atmosNearColor_10(),
	FSEffect_t3187928436::get_offset_of_atmosFarColor_11(),
	FSEffect_t3187928436::get_offset_of_atmosIntensity_12(),
	FSEffect_t3187928436::get_offset_of_atmosStartDist_13(),
	FSEffect_t3187928436::get_offset_of_atmosEndDist_14(),
	FSEffect_t3187928436::get_offset_of_lightMap_15(),
	FSEffect_t3187928436::get_offset_of_metallic_16(),
	FSEffect_t3187928436::get_offset_of_dirLight_17(),
	FSEffect_t3187928436::get_offset_of_lightColor_18(),
	FSEffect_t3187928436::get_offset_of_lightIntensity_19(),
	FSEffect_t3187928436::get_offset_of_shadowStrength_20(),
	FSEffect_t3187928436::get_offset_of_shadowDistance_21(),
	FSEffect_t3187928436::get_offset_of_lightFlareBrightness_22(),
	FSEffect_t3187928436::get_offset_of_skybox_23(),
	FSEffect_t3187928436::get_offset_of_skyUpColor_24(),
	FSEffect_t3187928436::get_offset_of_skyDwColor_25(),
	FSEffect_t3187928436::get_offset_of_skyGradientStart_26(),
	FSEffect_t3187928436::get_offset_of_skyGradientRange_27(),
	FSEffect_t3187928436::get_offset_of_skyGradientBloom_28(),
	FSEffect_t3187928436::get_offset_of_softFocus_29(),
	FSEffect_t3187928436::get_offset_of_bufferDownPercent_30(),
	FSEffect_t3187928436::get_offset_of_softBlurSize_31(),
	FSEffect_t3187928436::get_offset_of_bloom_32(),
	FSEffect_t3187928436::get_offset_of_bloomBlurSize_33(),
	FSEffect_t3187928436::get_offset_of_bloomThreshold_34(),
	FSEffect_t3187928436::get_offset_of_bloomIntensity_35(),
	FSEffect_t3187928436::get_offset_of_colorGrading_36(),
	FSEffect_t3187928436::get_offset_of_colorGradingLerpAmount_37(),
	FSEffect_t3187928436::get_offset_of_colorGradingStartLUT_38(),
	FSEffect_t3187928436::get_offset_of_colorGradingEndLUT_39(),
	FSEffect_t3187928436::get_offset_of_antiAlias_40(),
	FSEffect_t3187928436::get_offset_of_colorGradingMaterial_41(),
	FSEffect_t3187928436::get_offset_of_gaussianBlurMaterial_42(),
	FSEffect_t3187928436::get_offset_of_camera__43(),
	FSEffect_t3187928436::get_offset_of_dirLightCtrl_44(),
	FSEffect_t3187928436::get_offset_of_spotLightCtrl_45(),
	FSEffect_t3187928436::get_offset_of_planetCtrl_46(),
	FSEffect_t3187928436::get_offset_of_dirLightTrans_47(),
	FSEffect_t3187928436::get_offset_of_dirLightRot_48(),
	FSEffect_t3187928436::get_offset_of_backup_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4353 = { sizeof (ENABLE_t567653665)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4353[3] = 
{
	ENABLE_t567653665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4354 = { sizeof (ParamBackup_t1404423237)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4354[5] = 
{
	ParamBackup_t1404423237::get_offset_of_shadowDistance_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParamBackup_t1404423237::get_offset_of_shadowProjection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParamBackup_t1404423237::get_offset_of_fogMode_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParamBackup_t1404423237::get_offset_of_skyboxMaterial_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ParamBackup_t1404423237::get_offset_of_clearFlags_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4355 = { sizeof (FSLensDirt_t1842000292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4355[6] = 
{
	FSLensDirt_t1842000292::get_offset_of_SHADER_PROPERTY_PARAM_2(),
	FSLensDirt_t1842000292::get_offset_of_SHADER_PROPERTY_MAINTEX_3(),
	FSLensDirt_t1842000292::get_offset_of_cam_4(),
	FSLensDirt_t1842000292::get_offset_of_go__5(),
	FSLensDirt_t1842000292::get_offset_of_trans__6(),
	FSLensDirt_t1842000292::get_offset_of_material_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4356 = { sizeof (FSPlanet_t2551900013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4356[1] = 
{
	FSPlanet_t2551900013::get_offset_of_dirLight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4357 = { sizeof (FSSkyDome_t3093560541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4357[17] = 
{
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_STARTTEX_2(),
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_ENDTEX_3(),
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_LERPAMOUNT_4(),
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_ROTATEMATRIX_5(),
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_SCALEMATRIX_6(),
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_MAINCOLOR_7(),
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_GRADCOLOR_8(),
	FSSkyDome_t3093560541::get_offset_of_SHADER_PROPERTY_GRADIENT_9(),
	FSSkyDome_t3093560541::get_offset_of_go__10(),
	FSSkyDome_t3093560541::get_offset_of_trans__11(),
	FSSkyDome_t3093560541::get_offset_of_perspectiveScale_12(),
	FSSkyDome_t3093560541::get_offset_of_shellLength_13(),
	FSSkyDome_t3093560541::get_offset_of_shell_14(),
	FSSkyDome_t3093560541::get_offset_of_shellAxis_15(),
	FSSkyDome_t3093560541::get_offset_of_shellOmega_16(),
	FSSkyDome_t3093560541::get_offset_of_shellAngle_17(),
	FSSkyDome_t3093560541::get_offset_of_shellMpb_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4358 = { sizeof (FSSpotLight_t2693339535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4358[5] = 
{
	FSSpotLight_t2693339535::get_offset_of_maxFarDistance_2(),
	FSSpotLight_t2693339535::get_offset_of_cameraTrans_3(),
	FSSpotLight_t2693339535::get_offset_of_trans__4(),
	FSSpotLight_t2693339535::get_offset_of_spotLight_5(),
	FSSpotLight_t2693339535::get_offset_of_lensFlare_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4359 = { sizeof (FSSpotLightCone_t1384075014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4360 = { sizeof (FSTestCameraControl_t3583991567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4360[12] = 
{
	0,
	FSTestCameraControl_t3583991567::get_offset_of_trans__3(),
	FSTestCameraControl_t3583991567::get_offset_of_rigid_4(),
	FSTestCameraControl_t3583991567::get_offset_of_targetUp_5(),
	FSTestCameraControl_t3583991567::get_offset_of_upVelocity_6(),
	FSTestCameraControl_t3583991567::get_offset_of_rootUpVelocity_7(),
	FSTestCameraControl_t3583991567::get_offset_of_root_8(),
	FSTestCameraControl_t3583991567::get_offset_of_camTrans_9(),
	FSTestCameraControl_t3583991567::get_offset_of_camRot_10(),
	FSTestCameraControl_t3583991567::get_offset_of_defaultCamRot_11(),
	FSTestCameraControl_t3583991567::get_offset_of_defaultCamPosition_12(),
	FSTestCameraControl_t3583991567::get_offset_of_fsEffect_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4361 = { sizeof (FSUnderwater_t2566916842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4361[1] = 
{
	FSUnderwater_t2566916842::get_offset_of_upTrans_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4362 = { sizeof (CrossSection_t582606797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4362[7] = 
{
	CrossSection_t582606797::get_offset_of_allMaterials_2(),
	CrossSection_t582606797::get_offset_of_allMatInstances_3(),
	CrossSection_t582606797::get_offset_of_renderers_4(),
	CrossSection_t582606797::get_offset_of_setter_5(),
	CrossSection_t582606797::get_offset_of_boundsCentre_6(),
	CrossSection_t582606797::get_offset_of_sectionplane_7(),
	CrossSection_t582606797::get_offset_of_sectVars_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4363 = { sizeof (Section_t3430619939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4363[4] = 
{
	Section_t3430619939::get_offset_of_rot_x_0(),
	Section_t3430619939::get_offset_of_rot_z_1(),
	Section_t3430619939::get_offset_of_offset_2(),
	Section_t3430619939::get_offset_of_offsetRange_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4364 = { sizeof (HorizontalClippingSection_t3861440939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4364[15] = 
{
	HorizontalClippingSection_t3861440939::get_offset_of_skin1_2(),
	HorizontalClippingSection_t3861440939::get_offset_of_normalMaterials_3(),
	HorizontalClippingSection_t3861440939::get_offset_of_clippingMaterials_4(),
	HorizontalClippingSection_t3861440939::get_offset_of_renderers_5(),
	HorizontalClippingSection_t3861440939::get_offset_of_boundsCentre_6(),
	HorizontalClippingSection_t3861440939::get_offset_of_sectionplane_7(),
	HorizontalClippingSection_t3861440939::get_offset_of_sectVars_8(),
	HorizontalClippingSection_t3861440939::get_offset_of_zeroLevelOffset_9(),
	HorizontalClippingSection_t3861440939::get_offset_of_sliderRange_10(),
	HorizontalClippingSection_t3861440939::get_offset_of_labelRect_11(),
	HorizontalClippingSection_t3861440939::get_offset_of_slidersRect_12(),
	HorizontalClippingSection_t3861440939::get_offset_of_topSliderRect_13(),
	HorizontalClippingSection_t3861440939::get_offset_of_midSliderRect_14(),
	HorizontalClippingSection_t3861440939::get_offset_of_bottomSliderRect_15(),
	HorizontalClippingSection_t3861440939::get_offset_of_mouseOverGui_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4365 = { sizeof (SectionSetter_t3682766552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4365[5] = 
{
	SectionSetter_t3682766552::get_offset_of_toBeSectioned_2(),
	SectionSetter_t3682766552::get_offset_of_crossSectionShaders_3(),
	SectionSetter_t3682766552::get_offset_of_skin1_4(),
	SectionSetter_t3682766552::get_offset_of_orbitScript_5(),
	SectionSetter_t3682766552::get_offset_of_section_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4366 = { sizeof (maxCamera_t3827295319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4366[20] = 
{
	maxCamera_t3827295319::get_offset_of_target_2(),
	maxCamera_t3827295319::get_offset_of_targetOffset_3(),
	maxCamera_t3827295319::get_offset_of_distance_4(),
	maxCamera_t3827295319::get_offset_of_maxDistance_5(),
	maxCamera_t3827295319::get_offset_of_minDistance_6(),
	maxCamera_t3827295319::get_offset_of_xSpeed_7(),
	maxCamera_t3827295319::get_offset_of_ySpeed_8(),
	maxCamera_t3827295319::get_offset_of_yMinLimit_9(),
	maxCamera_t3827295319::get_offset_of_yMaxLimit_10(),
	maxCamera_t3827295319::get_offset_of_zoomRate_11(),
	maxCamera_t3827295319::get_offset_of_panSpeed_12(),
	maxCamera_t3827295319::get_offset_of_zoomDampening_13(),
	maxCamera_t3827295319::get_offset_of_xDeg_14(),
	maxCamera_t3827295319::get_offset_of_yDeg_15(),
	maxCamera_t3827295319::get_offset_of_currentDistance_16(),
	maxCamera_t3827295319::get_offset_of_desiredDistance_17(),
	maxCamera_t3827295319::get_offset_of_currentRotation_18(),
	maxCamera_t3827295319::get_offset_of_desiredRotation_19(),
	maxCamera_t3827295319::get_offset_of_rotation_20(),
	maxCamera_t3827295319::get_offset_of_position_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4367 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4367[230] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_0(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_1(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_2(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_3(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_4(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_5(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_6(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_7(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_8(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_9(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_10(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_11(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_12(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_13(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_14(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_15(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_16(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5EF06A1239E95C60F8D2C1DD8AE8028FC3742401_17(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_18(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_19(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_20(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_21(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_22(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_23(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_24(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_25(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_26(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_27(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D27C3AB82CCA6CE2F199F4F670BF19513A3825B87_28(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D30F65A149AF7DE938A9287048498B966AEBE54D4_29(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_30(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_31(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_32(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_33(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_34(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D102C522344FCAC1545BDA50C0FC675C502FFEC53_35(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_36(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D95CA85749ADCFBF9A2B82C0381DBCF95D175524C_37(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD9C221237B647EC215A7BCDED447349810E6BF9C_38(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE43F6BA634642FB90FEEE1A8F9905E957741960C_39(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D04D2A79C8A779AFAA779125335E9334C245EBB46_40(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D643A9D76937E94519B73BE072D65E79BAFF3C213_41(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_42(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6F576E8737EAAC9B6B12BDFC370048CD205E2CDD_43(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_44(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1C1237F52E2ED7B4D229AE3978DA144B9E653F5E_45(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_46(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_47(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_48(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_49(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_50(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D37454D933508E238CFB980F1077B24ADA4A480F4_51(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_52(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DEF813A47B13574822D335279EF445343654A4F04_53(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA47DBBBB6655DAA5E024374BB9C8AA44FF40D444_54(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D84F6B4137736E2F6671FC8787A500AC5C6E1D6AC_55(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D17651A9FA4DEA6C24D1287324CF4A640D080FE8E_56(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D7FF0A15672FF2807983AB77C0DA74928986427C0_57(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_58(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6DFD60679846B0E2064404E9EA3E1DDCE2C5709D_59(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_60(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_61(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_62(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_63(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_64(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_65(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_66(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_67(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_68(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_69(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_70(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_71(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_72(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_73(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_74(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_75(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_76(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_77(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_78(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_79(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAF3E960D2F0572119C78CAF04B900985A299000E_80(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D0392525BCB01691D1F319D89F2C12BF93A478467_81(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5DBC1A420B61F594A834C83E3DDC229C8AB77FDC_82(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8AC10B57091AFE182D9E4375C05E3FA037FFB3FA_83(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DED1175D10394A9CC0BC1E46183E287124AE4EB44_84(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_85(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D18F46E3C2ECF4BAAE65361632BA7C1B4B9028827_86(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_87(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D47A4F979AF156CDA62313B97411B4C6CE62B8B5A_88(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D3E8E493888B1BFB763D7553A6CC1978C17C198A3_89(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D3045D5B29BA900304981918A153806D371B02549_90(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF3E701C38098B41D23FA88977423371B3C00C0D1_91(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6D19ABD7FD0986E382242A9B88C8D8B5F52598DF_92(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_93(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_94(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_95(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_96(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_97(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D21A3E0CD2847E2F12EEE37749A9E206494A55100_98(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_99(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1D3E73DD251585C4908CBA58A179E1911834C891_100(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D31CDD717843C5C2B207F235634E6726898D4858A_101(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_102(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_103(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_104(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_105(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_106(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_107(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_108(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_109(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_110(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_111(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_112(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_113(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_114(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_115(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_116(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_117(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_118(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_119(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_120(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_121(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_122(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_123(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_124(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_125(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_126(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_127(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_128(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_129(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_130(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_131(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_132(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_133(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_134(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_135(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_136(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_137(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_138(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_139(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_140(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_141(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_142(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_143(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_144(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_145(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_146(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_147(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_148(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_149(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_150(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_151(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_152(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_153(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_154(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_155(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_156(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_157(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_158(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_159(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_160(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_161(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_162(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_163(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_164(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_165(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_166(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_167(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_168(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_169(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_170(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_171(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_172(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D643F7C1D25ADAFA2F5ED135D8331618A14714ED2_173(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_174(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_175(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_176(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_177(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_178(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_179(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_180(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_181(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_182(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_183(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_184(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_185(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_186(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_187(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_188(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_189(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_190(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_191(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_192(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_193(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_194(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_195(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_196(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_197(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_198(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_199(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_200(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_201(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_202(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_203(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_204(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_205(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_206(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_207(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_208(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_209(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_210(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_211(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_212(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_213(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_214(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_215(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_216(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_217(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_218(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D0953DF544832295E4A5B19928F95C351F25DA86A_219(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_220(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_221(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_222(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_223(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_224(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_225(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_226(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_227(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D96FD1582D90BAAE271E24B2C90FAD4656F6AB2D8_228(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DA4B78EAD1B8A5DFF70E5253A597D6146E8AB2F74_229(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4368 = { sizeof (U24ArrayTypeU3D6144_t2075964317)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D6144_t2075964317 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4369 = { sizeof (U24ArrayTypeU3D384_t3113433889)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D384_t3113433889 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4370 = { sizeof (U24ArrayTypeU3D124_t2306864769)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D124_t2306864769 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4371 = { sizeof (U24ArrayTypeU3D120_t2306864773)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D120_t2306864773 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4372 = { sizeof (U24ArrayTypeU3D76_t3894236543)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D76_t3894236543 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4373 = { sizeof (U24ArrayTypeU3D68_t2375206768)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D68_t2375206768 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4374 = { sizeof (U24ArrayTypeU3D116_t740780830)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D116_t740780830 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4375 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4376 = { sizeof (U24ArrayTypeU3D512_t740780702)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D512_t740780702 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4377 = { sizeof (U24ArrayTypeU3D256_t3066379751)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D256_t3066379751 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4378 = { sizeof (U24ArrayTypeU3D1152_t2882533397)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1152_t2882533397 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4379 = { sizeof (U24ArrayTypeU3D8_t1459944466)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D8_t1459944466 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4380 = { sizeof (U24ArrayTypeU3D32_t1568637719)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637719 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4381 = { sizeof (U24ArrayTypeU3D640_t1500295684)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D640_t1500295684 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4382 = { sizeof (U24ArrayTypeU3D2048_t698097168)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D2048_t698097168 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4383 = { sizeof (U24ArrayTypeU3D1024_t3379926265)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1024_t3379926265 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4384 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4385 = { sizeof (U24ArrayTypeU3D72_t1568637715)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D72_t1568637715 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4386 = { sizeof (U24ArrayTypeU3D48_t2375206766)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t2375206766 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4387 = { sizeof (U24ArrayTypeU3D96_t3894236537)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D96_t3894236537 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4388 = { sizeof (U24ArrayTypeU3D56_t3894236541)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D56_t3894236541 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4389 = { sizeof (U24ArrayTypeU3D128_t2306864765)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D128_t2306864765 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4390 = { sizeof (U24ArrayTypeU3D4_t1459944470)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4_t1459944470 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4391 = { sizeof (U24ArrayTypeU3D64_t762068660)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t762068660 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4392 = { sizeof (U24ArrayTypeU3D60_t2731437128)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D60_t2731437128 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4393 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4394 = { sizeof (U24ArrayTypeU3D4096_t2217126741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4096_t2217126741 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4395 = { sizeof (U24ArrayTypeU3D40_t2731437126)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D40_t2731437126 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4396 = { sizeof (U24ArrayTypeU3D28_t2375206772)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D28_t2375206772 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4397 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4398 = { sizeof (U24ArrayTypeU3D36_t3894236547)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D36_t3894236547 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4399 = { sizeof (U24ArrayTypeU3D44_t762068658)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D44_t762068658 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
