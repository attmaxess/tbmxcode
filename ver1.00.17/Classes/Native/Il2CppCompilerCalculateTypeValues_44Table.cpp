﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3740780834.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovement944153967.h"
#include "AssemblyU2DUnityScript_MovementTransferOnJump3438008145.h"
#include "AssemblyU2DUnityScript_CharacterMotorJumping1708272304.h"
#include "AssemblyU2DUnityScript_CharacterMotorMovingPlatform365475463.h"
#include "AssemblyU2DUnityScript_CharacterMotorSliding3749388514.h"
#include "AssemblyU2DUnityScript_CharacterMotor262030084.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN1999783645.h"
#include "AssemblyU2DUnityScript_CharacterMotor_U24SubtractN2847579946.h"
#include "AssemblyU2DUnityScript_FPSInputController4241249601.h"
#include "AssemblyU2DUnityScript_PlatformInputController4273899755.h"
#include "AssemblyU2DUnityScript_ThirdPersonCamera2751132817.h"
#include "AssemblyU2DUnityScript_CharacterState1314841520.h"
#include "AssemblyU2DUnityScript_ThirdPersonController1841729452.h"
#include "AssemblyU2DUnityScript_CameraFlyScript1126393843.h"
#include "AssemblyU2DUnityScript_MouseOrbit143029581.h"
#include "AssemblyU2DUnityScript_ScrollUV2335303814.h"
#include "AssemblyU2DUnityScript_moveMe881802511.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (U24ArrayTypeU3D112_t740780834)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D112_t740780834 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4401[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4402[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4403[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (U3CModuleU3E_t3783534230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (CharacterMotorMovement_t944153967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4405[13] = 
{
	CharacterMotorMovement_t944153967::get_offset_of_maxForwardSpeed_0(),
	CharacterMotorMovement_t944153967::get_offset_of_maxSidewaysSpeed_1(),
	CharacterMotorMovement_t944153967::get_offset_of_maxBackwardsSpeed_2(),
	CharacterMotorMovement_t944153967::get_offset_of_slopeSpeedMultiplier_3(),
	CharacterMotorMovement_t944153967::get_offset_of_maxGroundAcceleration_4(),
	CharacterMotorMovement_t944153967::get_offset_of_maxAirAcceleration_5(),
	CharacterMotorMovement_t944153967::get_offset_of_gravity_6(),
	CharacterMotorMovement_t944153967::get_offset_of_maxFallSpeed_7(),
	CharacterMotorMovement_t944153967::get_offset_of_collisionFlags_8(),
	CharacterMotorMovement_t944153967::get_offset_of_velocity_9(),
	CharacterMotorMovement_t944153967::get_offset_of_frameVelocity_10(),
	CharacterMotorMovement_t944153967::get_offset_of_hitPoint_11(),
	CharacterMotorMovement_t944153967::get_offset_of_lastHitPoint_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (MovementTransferOnJump_t3438008145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4406[5] = 
{
	MovementTransferOnJump_t3438008145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (CharacterMotorJumping_t1708272304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4407[10] = 
{
	CharacterMotorJumping_t1708272304::get_offset_of_enabled_0(),
	CharacterMotorJumping_t1708272304::get_offset_of_baseHeight_1(),
	CharacterMotorJumping_t1708272304::get_offset_of_extraHeight_2(),
	CharacterMotorJumping_t1708272304::get_offset_of_perpAmount_3(),
	CharacterMotorJumping_t1708272304::get_offset_of_steepPerpAmount_4(),
	CharacterMotorJumping_t1708272304::get_offset_of_jumping_5(),
	CharacterMotorJumping_t1708272304::get_offset_of_holdingJumpButton_6(),
	CharacterMotorJumping_t1708272304::get_offset_of_lastStartTime_7(),
	CharacterMotorJumping_t1708272304::get_offset_of_lastButtonDownTime_8(),
	CharacterMotorJumping_t1708272304::get_offset_of_jumpDir_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (CharacterMotorMovingPlatform_t365475463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4408[11] = 
{
	CharacterMotorMovingPlatform_t365475463::get_offset_of_enabled_0(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_movementTransfer_1(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_hitPlatform_2(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activePlatform_3(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeLocalPoint_4(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeGlobalPoint_5(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeLocalRotation_6(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_activeGlobalRotation_7(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_lastMatrix_8(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_platformVelocity_9(),
	CharacterMotorMovingPlatform_t365475463::get_offset_of_newPlatform_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (CharacterMotorSliding_t3749388514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[4] = 
{
	CharacterMotorSliding_t3749388514::get_offset_of_enabled_0(),
	CharacterMotorSliding_t3749388514::get_offset_of_slidingSpeed_1(),
	CharacterMotorSliding_t3749388514::get_offset_of_sidewaysControl_2(),
	CharacterMotorSliding_t3749388514::get_offset_of_speedControl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (CharacterMotor_t262030084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4410[13] = 
{
	CharacterMotor_t262030084::get_offset_of_canControl_2(),
	CharacterMotor_t262030084::get_offset_of_useFixedUpdate_3(),
	CharacterMotor_t262030084::get_offset_of_inputMoveDirection_4(),
	CharacterMotor_t262030084::get_offset_of_inputJump_5(),
	CharacterMotor_t262030084::get_offset_of_movement_6(),
	CharacterMotor_t262030084::get_offset_of_jumping_7(),
	CharacterMotor_t262030084::get_offset_of_movingPlatform_8(),
	CharacterMotor_t262030084::get_offset_of_sliding_9(),
	CharacterMotor_t262030084::get_offset_of_grounded_10(),
	CharacterMotor_t262030084::get_offset_of_groundNormal_11(),
	CharacterMotor_t262030084::get_offset_of_lastGroundNormal_12(),
	CharacterMotor_t262030084::get_offset_of_tr_13(),
	CharacterMotor_t262030084::get_offset_of_controller_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (U24SubtractNewPlatformVelocityU2417_t1999783645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4411[1] = 
{
	U24SubtractNewPlatformVelocityU2417_t1999783645::get_offset_of_U24self_U2420_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (U24_t2847579946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4412[2] = 
{
	U24_t2847579946::get_offset_of_U24platformU2418_2(),
	U24_t2847579946::get_offset_of_U24self_U2419_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (FPSInputController_t4241249601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4413[1] = 
{
	FPSInputController_t4241249601::get_offset_of_motor_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (PlatformInputController_t4273899755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4414[3] = 
{
	PlatformInputController_t4273899755::get_offset_of_autoRotate_2(),
	PlatformInputController_t4273899755::get_offset_of_maxRotationSpeed_3(),
	PlatformInputController_t4273899755::get_offset_of_motor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (ThirdPersonCamera_t2751132817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4415[18] = 
{
	ThirdPersonCamera_t2751132817::get_offset_of_cameraTransform_2(),
	ThirdPersonCamera_t2751132817::get_offset_of__target_3(),
	ThirdPersonCamera_t2751132817::get_offset_of_distance_4(),
	ThirdPersonCamera_t2751132817::get_offset_of_height_5(),
	ThirdPersonCamera_t2751132817::get_offset_of_angularSmoothLag_6(),
	ThirdPersonCamera_t2751132817::get_offset_of_angularMaxSpeed_7(),
	ThirdPersonCamera_t2751132817::get_offset_of_heightSmoothLag_8(),
	ThirdPersonCamera_t2751132817::get_offset_of_snapSmoothLag_9(),
	ThirdPersonCamera_t2751132817::get_offset_of_snapMaxSpeed_10(),
	ThirdPersonCamera_t2751132817::get_offset_of_clampHeadPositionScreenSpace_11(),
	ThirdPersonCamera_t2751132817::get_offset_of_lockCameraTimeout_12(),
	ThirdPersonCamera_t2751132817::get_offset_of_headOffset_13(),
	ThirdPersonCamera_t2751132817::get_offset_of_centerOffset_14(),
	ThirdPersonCamera_t2751132817::get_offset_of_heightVelocity_15(),
	ThirdPersonCamera_t2751132817::get_offset_of_angleVelocity_16(),
	ThirdPersonCamera_t2751132817::get_offset_of_snap_17(),
	ThirdPersonCamera_t2751132817::get_offset_of_controller_18(),
	ThirdPersonCamera_t2751132817::get_offset_of_targetHeight_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { sizeof (CharacterState_t1314841520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4416[6] = 
{
	CharacterState_t1314841520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (ThirdPersonController_t1841729452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4417[40] = 
{
	ThirdPersonController_t1841729452::get_offset_of_idleAnimation_2(),
	ThirdPersonController_t1841729452::get_offset_of_walkAnimation_3(),
	ThirdPersonController_t1841729452::get_offset_of_runAnimation_4(),
	ThirdPersonController_t1841729452::get_offset_of_jumpPoseAnimation_5(),
	ThirdPersonController_t1841729452::get_offset_of_walkMaxAnimationSpeed_6(),
	ThirdPersonController_t1841729452::get_offset_of_trotMaxAnimationSpeed_7(),
	ThirdPersonController_t1841729452::get_offset_of_runMaxAnimationSpeed_8(),
	ThirdPersonController_t1841729452::get_offset_of_jumpAnimationSpeed_9(),
	ThirdPersonController_t1841729452::get_offset_of_landAnimationSpeed_10(),
	ThirdPersonController_t1841729452::get_offset_of__animation_11(),
	ThirdPersonController_t1841729452::get_offset_of__characterState_12(),
	ThirdPersonController_t1841729452::get_offset_of_walkSpeed_13(),
	ThirdPersonController_t1841729452::get_offset_of_trotSpeed_14(),
	ThirdPersonController_t1841729452::get_offset_of_runSpeed_15(),
	ThirdPersonController_t1841729452::get_offset_of_inAirControlAcceleration_16(),
	ThirdPersonController_t1841729452::get_offset_of_jumpHeight_17(),
	ThirdPersonController_t1841729452::get_offset_of_gravity_18(),
	ThirdPersonController_t1841729452::get_offset_of_speedSmoothing_19(),
	ThirdPersonController_t1841729452::get_offset_of_rotateSpeed_20(),
	ThirdPersonController_t1841729452::get_offset_of_trotAfterSeconds_21(),
	ThirdPersonController_t1841729452::get_offset_of_canJump_22(),
	ThirdPersonController_t1841729452::get_offset_of_jumpRepeatTime_23(),
	ThirdPersonController_t1841729452::get_offset_of_jumpTimeout_24(),
	ThirdPersonController_t1841729452::get_offset_of_groundedTimeout_25(),
	ThirdPersonController_t1841729452::get_offset_of_lockCameraTimer_26(),
	ThirdPersonController_t1841729452::get_offset_of_moveDirection_27(),
	ThirdPersonController_t1841729452::get_offset_of_verticalSpeed_28(),
	ThirdPersonController_t1841729452::get_offset_of_moveSpeed_29(),
	ThirdPersonController_t1841729452::get_offset_of_collisionFlags_30(),
	ThirdPersonController_t1841729452::get_offset_of_jumping_31(),
	ThirdPersonController_t1841729452::get_offset_of_jumpingReachedApex_32(),
	ThirdPersonController_t1841729452::get_offset_of_movingBack_33(),
	ThirdPersonController_t1841729452::get_offset_of_isMoving_34(),
	ThirdPersonController_t1841729452::get_offset_of_walkTimeStart_35(),
	ThirdPersonController_t1841729452::get_offset_of_lastJumpButtonTime_36(),
	ThirdPersonController_t1841729452::get_offset_of_lastJumpTime_37(),
	ThirdPersonController_t1841729452::get_offset_of_lastJumpStartHeight_38(),
	ThirdPersonController_t1841729452::get_offset_of_inAirVelocity_39(),
	ThirdPersonController_t1841729452::get_offset_of_lastGroundedTime_40(),
	ThirdPersonController_t1841729452::get_offset_of_isControllable_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { sizeof (CameraFlyScript_t1126393843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4418[6] = 
{
	CameraFlyScript_t1126393843::get_offset_of_mainSpeed_2(),
	CameraFlyScript_t1126393843::get_offset_of_shiftAdd_3(),
	CameraFlyScript_t1126393843::get_offset_of_maxShift_4(),
	CameraFlyScript_t1126393843::get_offset_of_camSens_5(),
	CameraFlyScript_t1126393843::get_offset_of_lastMouse_6(),
	CameraFlyScript_t1126393843::get_offset_of_totalRun_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (MouseOrbit_t143029581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4419[8] = 
{
	MouseOrbit_t143029581::get_offset_of_target_2(),
	MouseOrbit_t143029581::get_offset_of_distance_3(),
	MouseOrbit_t143029581::get_offset_of_xSpeed_4(),
	MouseOrbit_t143029581::get_offset_of_ySpeed_5(),
	MouseOrbit_t143029581::get_offset_of_yMinLimit_6(),
	MouseOrbit_t143029581::get_offset_of_yMaxLimit_7(),
	MouseOrbit_t143029581::get_offset_of_x_8(),
	MouseOrbit_t143029581::get_offset_of_y_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (ScrollUV_t2335303814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4420[2] = 
{
	ScrollUV_t2335303814::get_offset_of_scrollSpeed_X_2(),
	ScrollUV_t2335303814::get_offset_of_scrollSpeed_Y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (moveMe_t881802511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4421[1] = 
{
	moveMe_t881802511::get_offset_of_oneToThree_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
