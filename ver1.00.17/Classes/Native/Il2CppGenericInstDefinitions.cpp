﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"





extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t2430923913_0_0_0;
static const Il2CppType* GenInst__EventInfo_t2430923913_0_0_0_Types[] = { &_EventInfo_t2430923913_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t2430923913_0_0_0 = { 1, GenInst__EventInfo_t2430923913_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
extern const Il2CppType Link_t2723257478_0_0_0;
static const Il2CppType* GenInst_Link_t2723257478_0_0_0_Types[] = { &Link_t2723257478_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType Slot_t2022531261_0_0_0;
static const Il2CppType* GenInst_Slot_t2022531261_0_0_0_Types[] = { &Slot_t2022531261_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
extern const Il2CppType Slot_t2267560602_0_0_0;
static const Il2CppType* GenInst_Slot_t2267560602_0_0_0_Types[] = { &Slot_t2267560602_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Calendar_t585061108_0_0_0;
static const Il2CppType* GenInst_Calendar_t585061108_0_0_0_Types[] = { &Calendar_t585061108_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
extern const Il2CppType CultureInfo_t3500843524_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t3500843524_0_0_0_Types[] = { &CultureInfo_t3500843524_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t3500843524_0_0_0 = { 1, GenInst_CultureInfo_t3500843524_0_0_0_Types };
extern const Il2CppType IFormatProvider_t2849799027_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t2849799027_0_0_0_Types[] = { &IFormatProvider_t2849799027_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t2849799027_0_0_0 = { 1, GenInst_IFormatProvider_t2849799027_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType MonoResource_t3127387157_0_0_0;
static const Il2CppType* GenInst_MonoResource_t3127387157_0_0_0_Types[] = { &MonoResource_t3127387157_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t3127387157_0_0_0 = { 1, GenInst_MonoResource_t3127387157_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType IList_1_t1844743827_0_0_0;
static const Il2CppType* GenInst_IList_1_t1844743827_0_0_0_Types[] = { &IList_1_t1844743827_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1844743827_0_0_0 = { 1, GenInst_IList_1_t1844743827_0_0_0_Types };
extern const Il2CppType ICollection_1_t2255878531_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2255878531_0_0_0_Types[] = { &ICollection_1_t2255878531_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2255878531_0_0_0 = { 1, GenInst_ICollection_1_t2255878531_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1595930271_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1595930271_0_0_0_Types[] = { &IEnumerable_1_t1595930271_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1595930271_0_0_0 = { 1, GenInst_IEnumerable_1_t1595930271_0_0_0_Types };
extern const Il2CppType IList_1_t3952977575_0_0_0;
static const Il2CppType* GenInst_IList_1_t3952977575_0_0_0_Types[] = { &IList_1_t3952977575_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3952977575_0_0_0 = { 1, GenInst_IList_1_t3952977575_0_0_0_Types };
extern const Il2CppType ICollection_1_t69144983_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t69144983_0_0_0_Types[] = { &ICollection_1_t69144983_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t69144983_0_0_0 = { 1, GenInst_ICollection_1_t69144983_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3704164019_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3704164019_0_0_0_Types[] = { &IEnumerable_1_t3704164019_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3704164019_0_0_0 = { 1, GenInst_IEnumerable_1_t3704164019_0_0_0_Types };
extern const Il2CppType IList_1_t643717440_0_0_0;
static const Il2CppType* GenInst_IList_1_t643717440_0_0_0_Types[] = { &IList_1_t643717440_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t643717440_0_0_0 = { 1, GenInst_IList_1_t643717440_0_0_0_Types };
extern const Il2CppType ICollection_1_t1054852144_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1054852144_0_0_0_Types[] = { &ICollection_1_t1054852144_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1054852144_0_0_0 = { 1, GenInst_ICollection_1_t1054852144_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t394903884_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t394903884_0_0_0_Types[] = { &IEnumerable_1_t394903884_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t394903884_0_0_0 = { 1, GenInst_IEnumerable_1_t394903884_0_0_0_Types };
extern const Il2CppType IList_1_t289070565_0_0_0;
static const Il2CppType* GenInst_IList_1_t289070565_0_0_0_Types[] = { &IList_1_t289070565_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t289070565_0_0_0 = { 1, GenInst_IList_1_t289070565_0_0_0_Types };
extern const Il2CppType ICollection_1_t700205269_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t700205269_0_0_0_Types[] = { &ICollection_1_t700205269_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t700205269_0_0_0 = { 1, GenInst_ICollection_1_t700205269_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t40257009_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t40257009_0_0_0_Types[] = { &IEnumerable_1_t40257009_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t40257009_0_0_0 = { 1, GenInst_IEnumerable_1_t40257009_0_0_0_Types };
extern const Il2CppType IList_1_t1043143288_0_0_0;
static const Il2CppType* GenInst_IList_1_t1043143288_0_0_0_Types[] = { &IList_1_t1043143288_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1043143288_0_0_0 = { 1, GenInst_IList_1_t1043143288_0_0_0_Types };
extern const Il2CppType ICollection_1_t1454277992_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1454277992_0_0_0_Types[] = { &ICollection_1_t1454277992_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1454277992_0_0_0 = { 1, GenInst_ICollection_1_t1454277992_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t794329732_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t794329732_0_0_0_Types[] = { &IEnumerable_1_t794329732_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t794329732_0_0_0 = { 1, GenInst_IEnumerable_1_t794329732_0_0_0_Types };
extern const Il2CppType IList_1_t873662762_0_0_0;
static const Il2CppType* GenInst_IList_1_t873662762_0_0_0_Types[] = { &IList_1_t873662762_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t873662762_0_0_0 = { 1, GenInst_IList_1_t873662762_0_0_0_Types };
extern const Il2CppType ICollection_1_t1284797466_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1284797466_0_0_0_Types[] = { &ICollection_1_t1284797466_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1284797466_0_0_0 = { 1, GenInst_ICollection_1_t1284797466_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t624849206_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t624849206_0_0_0_Types[] = { &IEnumerable_1_t624849206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t624849206_0_0_0 = { 1, GenInst_IEnumerable_1_t624849206_0_0_0_Types };
extern const Il2CppType IList_1_t3230389896_0_0_0;
static const Il2CppType* GenInst_IList_1_t3230389896_0_0_0_Types[] = { &IList_1_t3230389896_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3230389896_0_0_0 = { 1, GenInst_IList_1_t3230389896_0_0_0_Types };
extern const Il2CppType ICollection_1_t3641524600_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3641524600_0_0_0_Types[] = { &ICollection_1_t3641524600_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3641524600_0_0_0 = { 1, GenInst_ICollection_1_t3641524600_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2981576340_0_0_0_Types[] = { &IEnumerable_1_t2981576340_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2981576340_0_0_0 = { 1, GenInst_IEnumerable_1_t2981576340_0_0_0_Types };
extern const Il2CppType LocalBuilder_t2116499186_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t2116499186_0_0_0_Types[] = { &LocalBuilder_t2116499186_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t2116499186_0_0_0 = { 1, GenInst_LocalBuilder_t2116499186_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t61912499_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t61912499_0_0_0_Types[] = { &_LocalBuilder_t61912499_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t61912499_0_0_0 = { 1, GenInst__LocalBuilder_t61912499_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t1749284021_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t1749284021_0_0_0_Types[] = { &LocalVariableInfo_t1749284021_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t1749284021_0_0_0 = { 1, GenInst_LocalVariableInfo_t1749284021_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { &PropertyBuilder_t3694255912_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { &_PropertyBuilder_t3341912621_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { &ResourceInfo_t3933049236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { &ResourceCacheItem_t333236149_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType TypeTag_t141209596_0_0_0;
static const Il2CppType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { &TypeTag_t141209596_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType IBuiltInEvidence_t1114073477_0_0_0;
static const Il2CppType* GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types[] = { &IBuiltInEvidence_t1114073477_0_0_0 };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t1114073477_0_0_0 = { 1, GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types };
extern const Il2CppType IIdentityPermissionFactory_t2988326850_0_0_0;
static const Il2CppType* GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types[] = { &IIdentityPermissionFactory_t2988326850_0_0_0 };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t2988326850_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types };
extern const Il2CppType EncodingInfo_t2546178797_0_0_0;
static const Il2CppType* GenInst_EncodingInfo_t2546178797_0_0_0_Types[] = { &EncodingInfo_t2546178797_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodingInfo_t2546178797_0_0_0 = { 1, GenInst_EncodingInfo_t2546178797_0_0_0_Types };
extern const Il2CppType WaitHandle_t677569169_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t677569169_0_0_0_Types[] = { &WaitHandle_t677569169_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t677569169_0_0_0 = { 1, GenInst_WaitHandle_t677569169_0_0_0_Types };
extern const Il2CppType IDisposable_t2427283555_0_0_0;
static const Il2CppType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { &IDisposable_t2427283555_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1285298191_0_0_0_Types[] = { &MarshalByRefObject_t1285298191_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1285298191_0_0_0 = { 1, GenInst_MarshalByRefObject_t1285298191_0_0_0_Types };
extern const Il2CppType Assembly_t4268412390_0_0_0;
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0 = { 1, GenInst_Assembly_t4268412390_0_0_0_Types };
extern const Il2CppType _Assembly_t2937922309_0_0_0;
static const Il2CppType* GenInst__Assembly_t2937922309_0_0_0_Types[] = { &_Assembly_t2937922309_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t2937922309_0_0_0 = { 1, GenInst__Assembly_t2937922309_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType Guid_t_0_0_0;
static const Il2CppType* GenInst_Guid_t_0_0_0_Types[] = { &Guid_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType IList_1_t4224045037_0_0_0;
static const Il2CppType* GenInst_IList_1_t4224045037_0_0_0_Types[] = { &IList_1_t4224045037_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4224045037_0_0_0 = { 1, GenInst_IList_1_t4224045037_0_0_0_Types };
extern const Il2CppType ICollection_1_t340212445_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t340212445_0_0_0_Types[] = { &ICollection_1_t340212445_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t340212445_0_0_0 = { 1, GenInst_ICollection_1_t340212445_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3975231481_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3975231481_0_0_0_Types[] = { &IEnumerable_1_t3975231481_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3975231481_0_0_0 = { 1, GenInst_IEnumerable_1_t3975231481_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
extern const Il2CppType Link_t865133271_0_0_0;
static const Il2CppType* GenInst_Link_t865133271_0_0_0_Types[] = { &Link_t865133271_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t4250402154_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t4250402154_0_0_0_Types[] = { &PropertyDescriptor_t4250402154_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t4250402154_0_0_0 = { 1, GenInst_PropertyDescriptor_t4250402154_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t3749827553_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t3749827553_0_0_0_Types[] = { &MemberDescriptor_t3749827553_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t3749827553_0_0_0 = { 1, GenInst_MemberDescriptor_t3749827553_0_0_0_Types };
extern const Il2CppType IComponent_t1000253244_0_0_0;
static const Il2CppType* GenInst_IComponent_t1000253244_0_0_0_Types[] = { &IComponent_t1000253244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComponent_t1000253244_0_0_0 = { 1, GenInst_IComponent_t1000253244_0_0_0_Types };
extern const Il2CppType Enum_t2459695545_0_0_0;
static const Il2CppType* GenInst_Enum_t2459695545_0_0_0_Types[] = { &Enum_t2459695545_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2459695545_0_0_0 = { 1, GenInst_Enum_t2459695545_0_0_0_Types };
extern const Il2CppType IFormattable_t1523031934_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1523031934_0_0_0_Types[] = { &IFormattable_t1523031934_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1523031934_0_0_0 = { 1, GenInst_IFormattable_t1523031934_0_0_0_Types };
extern const Il2CppType ValueType_t3507792607_0_0_0;
static const Il2CppType* GenInst_ValueType_t3507792607_0_0_0_Types[] = { &ValueType_t3507792607_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3507792607_0_0_0 = { 1, GenInst_ValueType_t3507792607_0_0_0_Types };
extern const Il2CppType EventDescriptor_t962731901_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t962731901_0_0_0_Types[] = { &EventDescriptor_t962731901_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t962731901_0_0_0 = { 1, GenInst_EventDescriptor_t962731901_0_0_0_Types };
extern const Il2CppType ListSortDescription_t3194554012_0_0_0;
static const Il2CppType* GenInst_ListSortDescription_t3194554012_0_0_0_Types[] = { &ListSortDescription_t3194554012_0_0_0 };
extern const Il2CppGenericInst GenInst_ListSortDescription_t3194554012_0_0_0 = { 1, GenInst_ListSortDescription_t3194554012_0_0_0_Types };
extern const Il2CppType AttributeU5BU5D_t4255796347_0_0_0;
static const Il2CppType* GenInst_AttributeU5BU5D_t4255796347_0_0_0_Types[] = { &AttributeU5BU5D_t4255796347_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeU5BU5D_t4255796347_0_0_0 = { 1, GenInst_AttributeU5BU5D_t4255796347_0_0_0_Types };
extern const Il2CppType IList_1_t1083584199_0_0_0;
static const Il2CppType* GenInst_IList_1_t1083584199_0_0_0_Types[] = { &IList_1_t1083584199_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1083584199_0_0_0 = { 1, GenInst_IList_1_t1083584199_0_0_0_Types };
extern const Il2CppType ICollection_1_t1494718903_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1494718903_0_0_0_Types[] = { &ICollection_1_t1494718903_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1494718903_0_0_0 = { 1, GenInst_ICollection_1_t1494718903_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t834770643_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t834770643_0_0_0_Types[] = { &IEnumerable_1_t834770643_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t834770643_0_0_0 = { 1, GenInst_IEnumerable_1_t834770643_0_0_0_Types };
extern const Il2CppType IList_1_t2098604900_0_0_0;
static const Il2CppType* GenInst_IList_1_t2098604900_0_0_0_Types[] = { &IList_1_t2098604900_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2098604900_0_0_0 = { 1, GenInst_IList_1_t2098604900_0_0_0_Types };
extern const Il2CppType ICollection_1_t2509739604_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2509739604_0_0_0_Types[] = { &ICollection_1_t2509739604_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2509739604_0_0_0 = { 1, GenInst_ICollection_1_t2509739604_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1849791344_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1849791344_0_0_0_Types[] = { &IEnumerable_1_t1849791344_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1849791344_0_0_0 = { 1, GenInst_IEnumerable_1_t1849791344_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2743332604_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t2438624375_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types[] = { &TypeDescriptionProvider_t2438624375_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t2438624375_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LinkedList_1_t2743332604_0_0_0_Types[] = { &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2743332604_0_0_0 = { 1, GenInst_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2438035723_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t2438035723_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0, &KeyValuePair_2_t2438035723_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t2438035723_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t2438035723_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2438035723_0_0_0_Types[] = { &KeyValuePair_2_t2438035723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2438035723_0_0_0 = { 1, GenInst_KeyValuePair_2_t2438035723_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t2012978780_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0 = { 1, GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3261256129_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t3261256129_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0, &KeyValuePair_2_t3261256129_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t3261256129_0_0_0 = { 3, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t3261256129_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3261256129_0_0_0_Types[] = { &KeyValuePair_2_t3261256129_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3261256129_0_0_0 = { 1, GenInst_KeyValuePair_2_t3261256129_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType IPAddress_t1399971723_0_0_0;
static const Il2CppType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { &IPAddress_t1399971723_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { &ArraySegment_1_t2594217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
extern const Il2CppType Mark_t2724874473_0_0_0;
static const Il2CppType* GenInst_Mark_t2724874473_0_0_0_Types[] = { &Mark_t2724874473_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
extern const Il2CppType UriScheme_t1876590943_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { &UriScheme_t1876590943_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { &AchievementDescription_t3110978151_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { &IAchievementDescription_t3498529102_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
extern const Il2CppType UserProfile_t3365630962_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { &UserProfile_t3365630962_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { &IUserProfile_t4108565527_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { &GcLeaderboard_t453887929_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4083280315_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { &IAchievementU5BU5D_t2709554645_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
extern const Il2CppType IAchievement_t1752291260_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { &IAchievement_t1752291260_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { &GcAchievementData_t1754866149_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
extern const Il2CppType Achievement_t1333316625_0_0_0;
static const Il2CppType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { &Achievement_t1333316625_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { &IScoreU5BU5D_t3237304636_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
extern const Il2CppType IScore_t513966369_0_0_0;
static const Il2CppType* GenInst_IScore_t513966369_0_0_0_Types[] = { &IScore_t513966369_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { &GcScoreData_t3676783238_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
extern const Il2CppType Score_t2307748940_0_0_0;
static const Il2CppType* GenInst_Score_t2307748940_0_0_0_Types[] = { &Score_t2307748940_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { &IUserProfileU5BU5D_t3461248430_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
extern const Il2CppType GameObject_t1756533147_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType Material_t193706927_0_0_0;
static const Il2CppType* GenInst_Material_t193706927_0_0_0_Types[] = { &Material_t193706927_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t193706927_0_0_0 = { 1, GenInst_Material_t193706927_0_0_0_Types };
extern const Il2CppType Vector2_t2243707579_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType Touch_t407273883_0_0_0;
static const Il2CppType* GenInst_Touch_t407273883_0_0_0_Types[] = { &Touch_t407273883_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t407273883_0_0_0 = { 1, GenInst_Touch_t407273883_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Vector3_t2243707580_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType Vector4_t2243707581_0_0_0;
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType Color_t2020392075_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
extern const Il2CppType Color32_t874517518_0_0_0;
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
extern const Il2CppType CombineInstance_t64595210_0_0_0;
static const Il2CppType* GenInst_CombineInstance_t64595210_0_0_0_Types[] = { &CombineInstance_t64595210_0_0_0 };
extern const Il2CppGenericInst GenInst_CombineInstance_t64595210_0_0_0 = { 1, GenInst_CombineInstance_t64595210_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType AnimatorClipInfo_t3905751349_0_0_0;
static const Il2CppType* GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types[] = { &AnimatorClipInfo_t3905751349_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3905751349_0_0_0 = { 1, GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types };
extern const Il2CppType AnimatorControllerParameter_t1381019216_0_0_0;
static const Il2CppType* GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types[] = { &AnimatorControllerParameter_t1381019216_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t1381019216_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types };
extern const Il2CppType UIVertex_t1204258818_0_0_0;
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUIContent_t4210063000_0_0_0;
static const Il2CppType* GenInst_GUIContent_t4210063000_0_0_0_Types[] = { &GUIContent_t4210063000_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIContent_t4210063000_0_0_0 = { 1, GenInst_GUIContent_t4210063000_0_0_0_Types };
extern const Il2CppType Rect_t3681755626_0_0_0;
static const Il2CppType* GenInst_Rect_t3681755626_0_0_0_Types[] = { &Rect_t3681755626_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0 = { 1, GenInst_Rect_t3681755626_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType Texture_t2243626319_0_0_0;
static const Il2CppType* GenInst_Texture_t2243626319_0_0_0_Types[] = { &Texture_t2243626319_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t2243626319_0_0_0 = { 1, GenInst_Texture_t2243626319_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t3120781045_0_0_0_Types[] = { &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t3120781045_0_0_0 = { 1, GenInst_LayoutCache_t3120781045_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
extern const Il2CppType Event_t3028476042_0_0_0;
extern const Il2CppType TextEditOp_t3138797698_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t488203048_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0 = { 1, GenInst_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0 = { 1, GenInst_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_Types[] = { &Event_t3028476042_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0 = { 1, GenInst_Event_t3028476042_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3799506081_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3799506081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3799506081_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
extern const Il2CppType MessageTypeSubscribers_t2291506050_0_0_0;
static const Il2CppType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types[] = { &MessageTypeSubscribers_t2291506050_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &MessageTypeSubscribers_t2291506050_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType MessageEventArgs_t301283622_0_0_0;
static const Il2CppType* GenInst_MessageEventArgs_t301283622_0_0_0_Types[] = { &MessageEventArgs_t301283622_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t301283622_0_0_0 = { 1, GenInst_MessageEventArgs_t301283622_0_0_0_Types };
extern const Il2CppType List_1_t61287617_0_0_0;
static const Il2CppType* GenInst_List_1_t61287617_0_0_0_Types[] = { &List_1_t61287617_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t61287617_0_0_0 = { 1, GenInst_List_1_t61287617_0_0_0_Types };
extern const Il2CppType DispatcherKey_t708950850_0_0_0;
extern const Il2CppType Dispatcher_t2240407071_0_0_0;
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0, &Dispatcher_t2240407071_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0 = { 2, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0 = { 1, GenInst_DispatcherKey_t708950850_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0, &Dispatcher_t2240407071_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dispatcher_t2240407071_0_0_0_Types[] = { &Dispatcher_t2240407071_0_0_0 };
extern const Il2CppGenericInst GenInst_Dispatcher_t2240407071_0_0_0 = { 1, GenInst_Dispatcher_t2240407071_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3846770086_0_0_0;
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0, &Dispatcher_t2240407071_0_0_0, &KeyValuePair_2_t3846770086_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0 = { 3, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3846770086_0_0_0_Types[] = { &KeyValuePair_2_t3846770086_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3846770086_0_0_0 = { 1, GenInst_KeyValuePair_2_t3846770086_0_0_0_Types };
extern const Il2CppType TweenCallback_t3697142134_0_0_0;
static const Il2CppType* GenInst_TweenCallback_t3697142134_0_0_0_Types[] = { &TweenCallback_t3697142134_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenCallback_t3697142134_0_0_0 = { 1, GenInst_TweenCallback_t3697142134_0_0_0_Types };
extern const Il2CppType LogBehaviour_t3505725029_0_0_0;
static const Il2CppType* GenInst_LogBehaviour_t3505725029_0_0_0_Types[] = { &LogBehaviour_t3505725029_0_0_0 };
extern const Il2CppGenericInst GenInst_LogBehaviour_t3505725029_0_0_0 = { 1, GenInst_LogBehaviour_t3505725029_0_0_0_Types };
extern const Il2CppType FloatOptions_t1421548266_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0, &FloatOptions_t1421548266_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0 = { 3, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0_Types };
extern const Il2CppType Quaternion_t4030073918_0_0_0;
extern const Il2CppType QuaternionOptions_t466049668_0_0_0;
static const Il2CppType* GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0_Types[] = { &Quaternion_t4030073918_0_0_0, &Vector3_t2243707580_0_0_0, &QuaternionOptions_t466049668_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0 = { 3, GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0_Types };
static const Il2CppType* GenInst_Quaternion_t4030073918_0_0_0_Types[] = { &Quaternion_t4030073918_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t4030073918_0_0_0 = { 1, GenInst_Quaternion_t4030073918_0_0_0_Types };
extern const Il2CppType Vector3U5BU5D_t1172311765_0_0_0;
extern const Il2CppType Vector3ArrayOptions_t2672570171_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3U5BU5D_t1172311765_0_0_0, &Vector3ArrayOptions_t2672570171_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0, &Vector3ArrayOptions_t2672570171_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0_Types };
extern const Il2CppType Tween_t278478013_0_0_0;
static const Il2CppType* GenInst_Tween_t278478013_0_0_0_Types[] = { &Tween_t278478013_0_0_0 };
extern const Il2CppGenericInst GenInst_Tween_t278478013_0_0_0 = { 1, GenInst_Tween_t278478013_0_0_0_Types };
extern const Il2CppType ABSSequentiable_t2284140720_0_0_0;
static const Il2CppType* GenInst_ABSSequentiable_t2284140720_0_0_0_Types[] = { &ABSSequentiable_t2284140720_0_0_0 };
extern const Il2CppGenericInst GenInst_ABSSequentiable_t2284140720_0_0_0 = { 1, GenInst_ABSSequentiable_t2284140720_0_0_0_Types };
extern const Il2CppType NoOptions_t2508431845_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Double_t4078015681_0_0_0, &Double_t4078015681_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0_Types };
extern const Il2CppType UintOptions_t2267095136_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0, &UInt32_t2149682021_0_0_0, &UintOptions_t2267095136_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0 = { 3, GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int64_t909078037_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &UInt64_t2909196914_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0_Types };
extern const Il2CppType StringOptions_t2885323933_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &StringOptions_t2885323933_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &StringOptions_t2885323933_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0_Types };
extern const Il2CppType VectorOptions_t293385261_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0, &VectorOptions_t293385261_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0 = { 3, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0, &VectorOptions_t293385261_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0, &VectorOptions_t293385261_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0 = { 3, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0_Types };
extern const Il2CppType ColorOptions_t2213017305_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0_Types[] = { &Color_t2020392075_0_0_0, &Color_t2020392075_0_0_0, &ColorOptions_t2213017305_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0 = { 3, GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0_Types };
extern const Il2CppType RectOptions_t3393635162_0_0_0;
static const Il2CppType* GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0_Types[] = { &Rect_t3681755626_0_0_0, &Rect_t3681755626_0_0_0, &RectOptions_t3393635162_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0 = { 3, GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0_Types };
extern const Il2CppType RectOffset_t3387826427_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0, &RectOffset_t3387826427_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0_Types };
extern const Il2CppType Color2_t232726623_0_0_0;
static const Il2CppType* GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0_Types[] = { &Color2_t232726623_0_0_0, &Color2_t232726623_0_0_0, &ColorOptions_t2213017305_0_0_0 };
extern const Il2CppGenericInst GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0 = { 3, GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0_Types };
static const Il2CppType* GenInst_Color2_t232726623_0_0_0_Types[] = { &Color2_t232726623_0_0_0 };
extern const Il2CppGenericInst GenInst_Color2_t232726623_0_0_0 = { 1, GenInst_Color2_t232726623_0_0_0_Types };
extern const Il2CppType Ease_t2502520296_0_0_0;
static const Il2CppType* GenInst_Ease_t2502520296_0_0_0_Types[] = { &Ease_t2502520296_0_0_0 };
extern const Il2CppGenericInst GenInst_Ease_t2502520296_0_0_0 = { 1, GenInst_Ease_t2502520296_0_0_0_Types };
extern const Il2CppType Path_t2828565993_0_0_0;
extern const Il2CppType PathOptions_t2659884781_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Path_t2828565993_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types };
static const Il2CppType* GenInst_Quaternion_t4030073918_0_0_0_Quaternion_t4030073918_0_0_0_NoOptions_t2508431845_0_0_0_Types[] = { &Quaternion_t4030073918_0_0_0, &Quaternion_t4030073918_0_0_0, &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t4030073918_0_0_0_Quaternion_t4030073918_0_0_0_NoOptions_t2508431845_0_0_0 = { 3, GenInst_Quaternion_t4030073918_0_0_0_Quaternion_t4030073918_0_0_0_NoOptions_t2508431845_0_0_0_Types };
extern const Il2CppType LoopType_t2249218064_0_0_0;
static const Il2CppType* GenInst_LoopType_t2249218064_0_0_0_Types[] = { &LoopType_t2249218064_0_0_0 };
extern const Il2CppGenericInst GenInst_LoopType_t2249218064_0_0_0 = { 1, GenInst_LoopType_t2249218064_0_0_0_Types };
extern const Il2CppType ITweenPlugin_t2991430675_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_Types[] = { &Type_t_0_0_0, &ITweenPlugin_t2991430675_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0 = { 2, GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ITweenPlugin_t2991430675_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ITweenPlugin_t2991430675_0_0_0_Types[] = { &ITweenPlugin_t2991430675_0_0_0 };
extern const Il2CppGenericInst GenInst_ITweenPlugin_t2991430675_0_0_0 = { 1, GenInst_ITweenPlugin_t2991430675_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2686133794_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0_Types[] = { &Type_t_0_0_0, &ITweenPlugin_t2991430675_0_0_0, &KeyValuePair_2_t2686133794_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0 = { 3, GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2686133794_0_0_0_Types[] = { &KeyValuePair_2_t2686133794_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2686133794_0_0_0 = { 1, GenInst_KeyValuePair_2_t2686133794_0_0_0_Types };
extern const Il2CppType ControlPoint_t168081159_0_0_0;
static const Il2CppType* GenInst_ControlPoint_t168081159_0_0_0_Types[] = { &ControlPoint_t168081159_0_0_0 };
extern const Il2CppGenericInst GenInst_ControlPoint_t168081159_0_0_0 = { 1, GenInst_ControlPoint_t168081159_0_0_0_Types };
extern const Il2CppType FirebaseApp_t210707726_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseApp_t210707726_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0 = { 2, GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseApp_t210707726_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FirebaseApp_t210707726_0_0_0_Types[] = { &FirebaseApp_t210707726_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseApp_t210707726_0_0_0 = { 1, GenInst_FirebaseApp_t210707726_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4177799506_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t4177799506_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseApp_t210707726_0_0_0, &KeyValuePair_2_t4177799506_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t4177799506_0_0_0 = { 3, GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t4177799506_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4177799506_0_0_0_Types[] = { &KeyValuePair_2_t4177799506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4177799506_0_0_0 = { 1, GenInst_KeyValuePair_2_t4177799506_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FirebaseApp_t210707726_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t888819835_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0 = { 1, GenInst_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FirebaseApp_t210707726_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2705045562_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t2705045562_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FirebaseApp_t210707726_0_0_0, &KeyValuePair_2_t2705045562_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t2705045562_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t2705045562_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2705045562_0_0_0_Types[] = { &KeyValuePair_2_t2705045562_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2705045562_0_0_0 = { 1, GenInst_KeyValuePair_2_t2705045562_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t271247988_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t271247988_0_0_0_Types[] = { &KeyValuePair_2_t271247988_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t271247988_0_0_0 = { 1, GenInst_KeyValuePair_2_t271247988_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t271247988_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types };
extern const Il2CppType Tuple_2_t460172552_0_0_0;
static const Il2CppType* GenInst_Tuple_2_t460172552_0_0_0_Types[] = { &Tuple_2_t460172552_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_2_t460172552_0_0_0 = { 1, GenInst_Tuple_2_t460172552_0_0_0_Types };
extern const Il2CppType SendOrPostCallback_t296893742_0_0_0;
static const Il2CppType* GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0_Types[] = { &SendOrPostCallback_t296893742_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ManualResetEvent_t926074657_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &ManualResetEvent_t926074657_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &ManualResetEvent_t926074657_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ManualResetEvent_t926074657_0_0_0_Types[] = { &ManualResetEvent_t926074657_0_0_0 };
extern const Il2CppGenericInst GenInst_ManualResetEvent_t926074657_0_0_0 = { 1, GenInst_ManualResetEvent_t926074657_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1986212810_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_KeyValuePair_2_t1986212810_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &ManualResetEvent_t926074657_0_0_0, &KeyValuePair_2_t1986212810_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_KeyValuePair_2_t1986212810_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_KeyValuePair_2_t1986212810_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1986212810_0_0_0_Types[] = { &KeyValuePair_2_t1986212810_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1986212810_0_0_0 = { 1, GenInst_KeyValuePair_2_t1986212810_0_0_0_Types };
extern const Il2CppType Uri_t19570940_0_0_0;
extern const Il2CppType FirebaseHttpRequest_t1911360314_0_0_0;
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_FirebaseHttpRequest_t1911360314_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &FirebaseHttpRequest_t1911360314_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_FirebaseHttpRequest_t1911360314_0_0_0 = { 2, GenInst_Uri_t19570940_0_0_0_FirebaseHttpRequest_t1911360314_0_0_0_Types };
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { &BaseInputModule_t1295781545_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
extern const Il2CppType RaycastResult_t21186376_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { &IDeselectHandler_t3182198310_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { &IEventSystemHandler_t2741188318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
extern const Il2CppType List_1_t2110309450_0_0_0;
static const Il2CppType* GenInst_List_1_t2110309450_0_0_0_Types[] = { &List_1_t2110309450_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
extern const Il2CppType List_1_t2058570427_0_0_0;
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_Types[] = { &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
extern const Il2CppType List_1_t3188497603_0_0_0;
static const Il2CppType* GenInst_List_1_t3188497603_0_0_0_Types[] = { &List_1_t3188497603_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { &ISelectHandler_t2812555161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { &BaseRaycaster_t2336171397_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
extern const Il2CppType Entry_t3365010046_0_0_0;
static const Il2CppType* GenInst_Entry_t3365010046_0_0_0_Types[] = { &Entry_t3365010046_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { &BaseEventData_t2681005625_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { &IPointerEnterHandler_t193164956_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { &IPointerExitHandler_t461019860_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { &IPointerDownHandler_t3929046918_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { &IPointerUpHandler_t1847764461_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { &IPointerClickHandler_t96169666_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { &IInitializePotentialDragHandler_t3350809087_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { &IBeginDragHandler_t3135127860_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { &IDragHandler_t2583993319_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { &IEndDragHandler_t1349123600_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { &IDropHandler_t2390101210_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { &IScrollHandler_t3834677510_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { &IUpdateSelectedHandler_t3778909353_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { &IMoveHandler_t2611925506_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { &ISubmitHandler_t525803901_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { &ICancelHandler_t1980319651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
extern const Il2CppType Transform_t3275118058_0_0_0;
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
extern const Il2CppType BaseInput_t621514313_0_0_0;
static const Il2CppType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { &BaseInput_t621514313_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { &UIBehaviour_t3960014691_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType ButtonState_t2688375492_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { &ButtonState_t2688375492_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { &ColorBlock_t2652774230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
extern const Il2CppType OptionData_t2420267500_0_0_0;
static const Il2CppType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { &OptionData_t2420267500_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { &DropdownItem_t4139978805_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
extern const Il2CppType FloatTween_t2986189219_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { &FloatTween_t2986189219_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
extern const Il2CppType Sprite_t309593783_0_0_0;
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Types[] = { &Sprite_t309593783_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
extern const Il2CppType Canvas_t209405766_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_Types[] = { &Canvas_t209405766_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
extern const Il2CppType List_1_t3873494194_0_0_0;
static const Il2CppType* GenInst_List_1_t3873494194_0_0_0_Types[] = { &List_1_t3873494194_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType Text_t356221433_0_0_0;
static const Il2CppType* GenInst_Text_t356221433_0_0_0_Types[] = { &Text_t356221433_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t2984649583_0_0_0_Types[] = { &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2984649583_0_0_0 = { 1, GenInst_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
extern const Il2CppType ColorTween_t3438117476_0_0_0;
static const Il2CppType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { &ColorTween_t3438117476_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
extern const Il2CppType Graphic_t2426225576_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t286373651_0_0_0_Types[] = { &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t286373651_0_0_0 = { 1, GenInst_IndexedSet_1_t286373651_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
extern const Il2CppType Type_t3352948571_0_0_0;
static const Il2CppType* GenInst_Type_t3352948571_0_0_0_Types[] = { &Type_t3352948571_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
extern const Il2CppType FillMethod_t1640962579_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { &FillMethod_t1640962579_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
extern const Il2CppType ContentType_t1028629049_0_0_0;
static const Il2CppType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { &ContentType_t1028629049_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
extern const Il2CppType LineType_t2931319356_0_0_0;
static const Il2CppType* GenInst_LineType_t2931319356_0_0_0_Types[] = { &LineType_t2931319356_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
extern const Il2CppType InputType_t1274231802_0_0_0;
static const Il2CppType* GenInst_InputType_t1274231802_0_0_0_Types[] = { &InputType_t1274231802_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { &TouchScreenKeyboardType_t875112366_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { &CharacterValidation_t3437478890_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
extern const Il2CppType Mask_t2977958238_0_0_0;
static const Il2CppType* GenInst_Mask_t2977958238_0_0_0_Types[] = { &Mask_t2977958238_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
extern const Il2CppType List_1_t2347079370_0_0_0;
static const Il2CppType* GenInst_List_1_t2347079370_0_0_0_Types[] = { &List_1_t2347079370_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { &RectMask2D_t1156185964_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
extern const Il2CppType List_1_t525307096_0_0_0;
static const Il2CppType* GenInst_List_1_t525307096_0_0_0_Types[] = { &List_1_t525307096_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
extern const Il2CppType Navigation_t1571958496_0_0_0;
static const Il2CppType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { &Navigation_t1571958496_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
extern const Il2CppType IClippable_t1941276057_0_0_0;
static const Il2CppType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { &IClippable_t1941276057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
extern const Il2CppType Direction_t3696775921_0_0_0;
static const Il2CppType* GenInst_Direction_t3696775921_0_0_0_Types[] = { &Direction_t3696775921_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
extern const Il2CppType Selectable_t1490392188_0_0_0;
static const Il2CppType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { &Selectable_t1490392188_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
extern const Il2CppType Transition_t605142169_0_0_0;
static const Il2CppType* GenInst_Transition_t605142169_0_0_0_Types[] = { &Transition_t605142169_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
extern const Il2CppType SpriteState_t1353336012_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { &SpriteState_t1353336012_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { &CanvasGroup_t3296560743_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
extern const Il2CppType Direction_t1525323322_0_0_0;
static const Il2CppType* GenInst_Direction_t1525323322_0_0_0_Types[] = { &Direction_t1525323322_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
extern const Il2CppType MatEntry_t3157325053_0_0_0;
static const Il2CppType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { &MatEntry_t3157325053_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
extern const Il2CppType Toggle_t3976754468_0_0_0;
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IClipper_t900477982_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Types[] = { &IClipper_t900477982_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
extern const Il2CppType AspectMode_t1166448724_0_0_0;
static const Il2CppType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { &AspectMode_t1166448724_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
extern const Il2CppType FitMode_t4030874534_0_0_0;
static const Il2CppType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { &FitMode_t4030874534_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
extern const Il2CppType RectTransform_t3349966182_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { &RectTransform_t3349966182_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { &LayoutRebuilder_t2155218138_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType List_1_t1612828712_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828712_0_0_0_Types[] = { &List_1_t1612828712_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
extern const Il2CppType List_1_t243638650_0_0_0;
static const Il2CppType* GenInst_List_1_t243638650_0_0_0_Types[] = { &List_1_t243638650_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
extern const Il2CppType List_1_t1612828711_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828711_0_0_0_Types[] = { &List_1_t1612828711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
extern const Il2CppType List_1_t1612828713_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828713_0_0_0_Types[] = { &List_1_t1612828713_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
extern const Il2CppType List_1_t1440998580_0_0_0;
static const Il2CppType* GenInst_List_1_t1440998580_0_0_0_Types[] = { &List_1_t1440998580_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
extern const Il2CppType List_1_t573379950_0_0_0;
static const Il2CppType* GenInst_List_1_t573379950_0_0_0_Types[] = { &List_1_t573379950_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3089358386_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0, &KeyValuePair_2_t3089358386_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3089358386_0_0_0_Types[] = { &KeyValuePair_2_t3089358386_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3089358386_0_0_0 = { 1, GenInst_KeyValuePair_2_t3089358386_0_0_0_Types };
extern const Il2CppType AppViewHitBuilder_t93969158_0_0_0;
static const Il2CppType* GenInst_AppViewHitBuilder_t93969158_0_0_0_Types[] = { &AppViewHitBuilder_t93969158_0_0_0 };
extern const Il2CppGenericInst GenInst_AppViewHitBuilder_t93969158_0_0_0 = { 1, GenInst_AppViewHitBuilder_t93969158_0_0_0_Types };
extern const Il2CppType EventHitBuilder_t3458384960_0_0_0;
static const Il2CppType* GenInst_EventHitBuilder_t3458384960_0_0_0_Types[] = { &EventHitBuilder_t3458384960_0_0_0 };
extern const Il2CppGenericInst GenInst_EventHitBuilder_t3458384960_0_0_0 = { 1, GenInst_EventHitBuilder_t3458384960_0_0_0_Types };
extern const Il2CppType TransactionHitBuilder_t3262282092_0_0_0;
static const Il2CppType* GenInst_TransactionHitBuilder_t3262282092_0_0_0_Types[] = { &TransactionHitBuilder_t3262282092_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionHitBuilder_t3262282092_0_0_0 = { 1, GenInst_TransactionHitBuilder_t3262282092_0_0_0_Types };
extern const Il2CppType ItemHitBuilder_t596710299_0_0_0;
static const Il2CppType* GenInst_ItemHitBuilder_t596710299_0_0_0_Types[] = { &ItemHitBuilder_t596710299_0_0_0 };
extern const Il2CppGenericInst GenInst_ItemHitBuilder_t596710299_0_0_0 = { 1, GenInst_ItemHitBuilder_t596710299_0_0_0_Types };
extern const Il2CppType ExceptionHitBuilder_t2776428281_0_0_0;
static const Il2CppType* GenInst_ExceptionHitBuilder_t2776428281_0_0_0_Types[] = { &ExceptionHitBuilder_t2776428281_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHitBuilder_t2776428281_0_0_0 = { 1, GenInst_ExceptionHitBuilder_t2776428281_0_0_0_Types };
extern const Il2CppType SocialHitBuilder_t2172450373_0_0_0;
static const Il2CppType* GenInst_SocialHitBuilder_t2172450373_0_0_0_Types[] = { &SocialHitBuilder_t2172450373_0_0_0 };
extern const Il2CppGenericInst GenInst_SocialHitBuilder_t2172450373_0_0_0 = { 1, GenInst_SocialHitBuilder_t2172450373_0_0_0_Types };
extern const Il2CppType TimingHitBuilder_t1787450410_0_0_0;
static const Il2CppType* GenInst_TimingHitBuilder_t1787450410_0_0_0_Types[] = { &TimingHitBuilder_t1787450410_0_0_0 };
extern const Il2CppGenericInst GenInst_TimingHitBuilder_t1787450410_0_0_0 = { 1, GenInst_TimingHitBuilder_t1787450410_0_0_0_Types };
extern const Il2CppType AxisTouchButton_t3842535002_0_0_0;
static const Il2CppType* GenInst_AxisTouchButton_t3842535002_0_0_0_Types[] = { &AxisTouchButton_t3842535002_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisTouchButton_t3842535002_0_0_0 = { 1, GenInst_AxisTouchButton_t3842535002_0_0_0_Types };
extern const Il2CppType VirtualAxis_t2691167515_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t2691167515_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0 = { 2, GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t2691167515_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_VirtualAxis_t2691167515_0_0_0_Types[] = { &VirtualAxis_t2691167515_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualAxis_t2691167515_0_0_0 = { 1, GenInst_VirtualAxis_t2691167515_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2363291999_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_KeyValuePair_2_t2363291999_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t2691167515_0_0_0, &KeyValuePair_2_t2363291999_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_KeyValuePair_2_t2363291999_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_KeyValuePair_2_t2363291999_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2363291999_0_0_0_Types[] = { &KeyValuePair_2_t2363291999_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2363291999_0_0_0 = { 1, GenInst_KeyValuePair_2_t2363291999_0_0_0_Types };
extern const Il2CppType VirtualButton_t2157404822_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2157404822_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0 = { 2, GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2157404822_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButton_t2157404822_0_0_0_Types[] = { &VirtualButton_t2157404822_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t2157404822_0_0_0 = { 1, GenInst_VirtualButton_t2157404822_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1829529306_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_KeyValuePair_2_t1829529306_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t2157404822_0_0_0, &KeyValuePair_2_t1829529306_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_KeyValuePair_2_t1829529306_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_KeyValuePair_2_t1829529306_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1829529306_0_0_0_Types[] = { &KeyValuePair_2_t1829529306_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1829529306_0_0_0 = { 1, GenInst_KeyValuePair_2_t1829529306_0_0_0_Types };
extern const Il2CppType MeshInstance_t2175729061_0_0_0;
static const Il2CppType* GenInst_MeshInstance_t2175729061_0_0_0_Types[] = { &MeshInstance_t2175729061_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshInstance_t2175729061_0_0_0 = { 1, GenInst_MeshInstance_t2175729061_0_0_0_Types };
extern const Il2CppType Terrain_t59182933_0_0_0;
static const Il2CppType* GenInst_Terrain_t59182933_0_0_0_Types[] = { &Terrain_t59182933_0_0_0 };
extern const Il2CppGenericInst GenInst_Terrain_t59182933_0_0_0 = { 1, GenInst_Terrain_t59182933_0_0_0_Types };
extern const Il2CppType HeaderValue_t822462144_0_0_0;
static const Il2CppType* GenInst_HeaderValue_t822462144_0_0_0_Types[] = { &HeaderValue_t822462144_0_0_0 };
extern const Il2CppGenericInst GenInst_HeaderValue_t822462144_0_0_0 = { 1, GenInst_HeaderValue_t822462144_0_0_0_Types };
extern const Il2CppType Digest_t59399582_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_Types[] = { &String_t_0_0_0, &Digest_t59399582_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Digest_t59399582_0_0_0 = { 2, GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Digest_t59399582_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Digest_t59399582_0_0_0_Types[] = { &Digest_t59399582_0_0_0 };
extern const Il2CppGenericInst GenInst_Digest_t59399582_0_0_0 = { 1, GenInst_Digest_t59399582_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4026491362_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_KeyValuePair_2_t4026491362_0_0_0_Types[] = { &String_t_0_0_0, &Digest_t59399582_0_0_0, &KeyValuePair_2_t4026491362_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_KeyValuePair_2_t4026491362_0_0_0 = { 3, GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_KeyValuePair_2_t4026491362_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4026491362_0_0_0_Types[] = { &KeyValuePair_2_t4026491362_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4026491362_0_0_0 = { 1, GenInst_KeyValuePair_2_t4026491362_0_0_0_Types };
extern const Il2CppType HTTPCacheFileInfo_t2858191078_0_0_0;
static const Il2CppType* GenInst_HTTPCacheFileInfo_t2858191078_0_0_0_Types[] = { &HTTPCacheFileInfo_t2858191078_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPCacheFileInfo_t2858191078_0_0_0 = { 1, GenInst_HTTPCacheFileInfo_t2858191078_0_0_0_Types };
extern const Il2CppType List_1_t1398341365_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1398341365_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1398341365_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1398341365_0_0_0_Types[] = { &List_1_t1398341365_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1398341365_0_0_0 = { 1, GenInst_List_1_t1398341365_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1070465849_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465849_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1398341365_0_0_0, &KeyValuePair_2_t1070465849_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465849_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465849_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1070465849_0_0_0_Types[] = { &KeyValuePair_2_t1070465849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1070465849_0_0_0 = { 1, GenInst_KeyValuePair_2_t1070465849_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Types[] = { &Uri_t19570940_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0 = { 1, GenInst_Uri_t19570940_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3930922740_0_0_0;
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3930922740_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3930922740_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3930922740_0_0_0 = { 3, GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3930922740_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3930922740_0_0_0_Types[] = { &KeyValuePair_2_t3930922740_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3930922740_0_0_0 = { 1, GenInst_KeyValuePair_2_t3930922740_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0 = { 2, GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4099664523_0_0_0;
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t4099664523_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0, &KeyValuePair_2_t4099664523_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t4099664523_0_0_0 = { 3, GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t4099664523_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4099664523_0_0_0_Types[] = { &KeyValuePair_2_t4099664523_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4099664523_0_0_0 = { 1, GenInst_KeyValuePair_2_t4099664523_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2387876582_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2387876582_0_0_0_Types[] = { &KeyValuePair_2_t2387876582_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387876582_0_0_0 = { 1, GenInst_KeyValuePair_2_t2387876582_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2387876582_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2556618365_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t2556618365_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0, &KeyValuePair_2_t2556618365_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t2556618365_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t2556618365_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2556618365_0_0_0_Types[] = { &KeyValuePair_2_t2556618365_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2556618365_0_0_0 = { 1, GenInst_KeyValuePair_2_t2556618365_0_0_0_Types };
extern const Il2CppType Stream_t3255436806_0_0_0;
static const Il2CppType* GenInst_Stream_t3255436806_0_0_0_Types[] = { &Stream_t3255436806_0_0_0 };
extern const Il2CppGenericInst GenInst_Stream_t3255436806_0_0_0 = { 1, GenInst_Stream_t3255436806_0_0_0_Types };
extern const Il2CppType Cookie_t4162804382_0_0_0;
static const Il2CppType* GenInst_Cookie_t4162804382_0_0_0_Types[] = { &Cookie_t4162804382_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t4162804382_0_0_0 = { 1, GenInst_Cookie_t4162804382_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Config_t3381668151_0_0_0;
static const Il2CppType* GenInst_Config_t3381668151_0_0_0_Types[] = { &Config_t3381668151_0_0_0 };
extern const Il2CppGenericInst GenInst_Config_t3381668151_0_0_0 = { 1, GenInst_Config_t3381668151_0_0_0_Types };
extern const Il2CppType Int32U5BU5D_t3030399641_0_0_0;
static const Il2CppType* GenInst_Int32U5BU5D_t3030399641_0_0_0_Types[] = { &Int32U5BU5D_t3030399641_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t3030399641_0_0_0 = { 1, GenInst_Int32U5BU5D_t3030399641_0_0_0_Types };
extern const Il2CppType IList_1_t2612818049_0_0_0;
static const Il2CppType* GenInst_IList_1_t2612818049_0_0_0_Types[] = { &IList_1_t2612818049_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2612818049_0_0_0 = { 1, GenInst_IList_1_t2612818049_0_0_0_Types };
extern const Il2CppType ICollection_1_t3023952753_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3023952753_0_0_0_Types[] = { &ICollection_1_t3023952753_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3023952753_0_0_0 = { 1, GenInst_ICollection_1_t3023952753_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2364004493_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2364004493_0_0_0_Types[] = { &IEnumerable_1_t2364004493_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2364004493_0_0_0 = { 1, GenInst_IEnumerable_1_t2364004493_0_0_0_Types };
extern const Il2CppType IHeartbeat_t3217346319_0_0_0;
static const Il2CppType* GenInst_IHeartbeat_t3217346319_0_0_0_Types[] = { &IHeartbeat_t3217346319_0_0_0 };
extern const Il2CppGenericInst GenInst_IHeartbeat_t3217346319_0_0_0 = { 1, GenInst_IHeartbeat_t3217346319_0_0_0_Types };
extern const Il2CppType HTTPFieldData_t605100868_0_0_0;
static const Il2CppType* GenInst_HTTPFieldData_t605100868_0_0_0_Types[] = { &HTTPFieldData_t605100868_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPFieldData_t605100868_0_0_0 = { 1, GenInst_HTTPFieldData_t605100868_0_0_0_Types };
extern const Il2CppType List_1_t2151311861_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2151311861_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_Types };
extern const Il2CppType ConnectionBase_t2782190729_0_0_0;
static const Il2CppType* GenInst_ConnectionBase_t2782190729_0_0_0_Types[] = { &ConnectionBase_t2782190729_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionBase_t2782190729_0_0_0 = { 1, GenInst_ConnectionBase_t2782190729_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2151311861_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2151311861_0_0_0_Types[] = { &List_1_t2151311861_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2151311861_0_0_0 = { 1, GenInst_List_1_t2151311861_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1823436345_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_KeyValuePair_2_t1823436345_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2151311861_0_0_0, &KeyValuePair_2_t1823436345_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_KeyValuePair_2_t1823436345_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_KeyValuePair_2_t1823436345_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1823436345_0_0_0_Types[] = { &KeyValuePair_2_t1823436345_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1823436345_0_0_0 = { 1, GenInst_KeyValuePair_2_t1823436345_0_0_0_Types };
extern const Il2CppType HTTPRequest_t138485887_0_0_0;
static const Il2CppType* GenInst_HTTPRequest_t138485887_0_0_0_Types[] = { &HTTPRequest_t138485887_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPRequest_t138485887_0_0_0 = { 1, GenInst_HTTPRequest_t138485887_0_0_0_Types };
extern const Il2CppType X509Chain_t777637347_0_0_0;
static const Il2CppType* GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &HTTPRequest_t138485887_0_0_0, &X509Certificate_t283079845_0_0_0, &X509Chain_t777637347_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0 = { 4, GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
extern const Il2CppType Asn1Encodable_t3447851422_0_0_0;
static const Il2CppType* GenInst_Asn1Encodable_t3447851422_0_0_0_Types[] = { &Asn1Encodable_t3447851422_0_0_0 };
extern const Il2CppGenericInst GenInst_Asn1Encodable_t3447851422_0_0_0 = { 1, GenInst_Asn1Encodable_t3447851422_0_0_0_Types };
extern const Il2CppType IAsn1Convertible_t983765413_0_0_0;
static const Il2CppType* GenInst_IAsn1Convertible_t983765413_0_0_0_Types[] = { &IAsn1Convertible_t983765413_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsn1Convertible_t983765413_0_0_0 = { 1, GenInst_IAsn1Convertible_t983765413_0_0_0_Types };
extern const Il2CppType DerEnumerated_t514019671_0_0_0;
static const Il2CppType* GenInst_DerEnumerated_t514019671_0_0_0_Types[] = { &DerEnumerated_t514019671_0_0_0 };
extern const Il2CppGenericInst GenInst_DerEnumerated_t514019671_0_0_0 = { 1, GenInst_DerEnumerated_t514019671_0_0_0_Types };
extern const Il2CppType Asn1Object_t564283626_0_0_0;
static const Il2CppType* GenInst_Asn1Object_t564283626_0_0_0_Types[] = { &Asn1Object_t564283626_0_0_0 };
extern const Il2CppGenericInst GenInst_Asn1Object_t564283626_0_0_0 = { 1, GenInst_Asn1Object_t564283626_0_0_0_Types };
extern const Il2CppType DerObjectIdentifier_t3495876513_0_0_0;
static const Il2CppType* GenInst_DerObjectIdentifier_t3495876513_0_0_0_Types[] = { &DerObjectIdentifier_t3495876513_0_0_0 };
extern const Il2CppGenericInst GenInst_DerObjectIdentifier_t3495876513_0_0_0 = { 1, GenInst_DerObjectIdentifier_t3495876513_0_0_0_Types };
extern const Il2CppType BigInteger_t4268922522_0_0_0;
static const Il2CppType* GenInst_BigInteger_t4268922522_0_0_0_Types[] = { &BigInteger_t4268922522_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t4268922522_0_0_0 = { 1, GenInst_BigInteger_t4268922522_0_0_0_Types };
extern const Il2CppType DistributionPoint_t769724552_0_0_0;
static const Il2CppType* GenInst_DistributionPoint_t769724552_0_0_0_Types[] = { &DistributionPoint_t769724552_0_0_0 };
extern const Il2CppGenericInst GenInst_DistributionPoint_t769724552_0_0_0 = { 1, GenInst_DistributionPoint_t769724552_0_0_0_Types };
extern const Il2CppType CrlEntry_t4200172927_0_0_0;
static const Il2CppType* GenInst_CrlEntry_t4200172927_0_0_0_Types[] = { &CrlEntry_t4200172927_0_0_0 };
extern const Il2CppGenericInst GenInst_CrlEntry_t4200172927_0_0_0 = { 1, GenInst_CrlEntry_t4200172927_0_0_0_Types };
extern const Il2CppType GeneralName_t294965175_0_0_0;
static const Il2CppType* GenInst_GeneralName_t294965175_0_0_0_Types[] = { &GeneralName_t294965175_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneralName_t294965175_0_0_0 = { 1, GenInst_GeneralName_t294965175_0_0_0_Types };
extern const Il2CppType IAsn1Choice_t4205079803_0_0_0;
static const Il2CppType* GenInst_IAsn1Choice_t4205079803_0_0_0_Types[] = { &IAsn1Choice_t4205079803_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsn1Choice_t4205079803_0_0_0 = { 1, GenInst_IAsn1Choice_t4205079803_0_0_0_Types };
extern const Il2CppType UInt32U5BU5D_t59386216_0_0_0;
static const Il2CppType* GenInst_UInt32U5BU5D_t59386216_0_0_0_Types[] = { &UInt32U5BU5D_t59386216_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32U5BU5D_t59386216_0_0_0 = { 1, GenInst_UInt32U5BU5D_t59386216_0_0_0_Types };
extern const Il2CppType IList_1_t2690622622_0_0_0;
static const Il2CppType* GenInst_IList_1_t2690622622_0_0_0_Types[] = { &IList_1_t2690622622_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2690622622_0_0_0 = { 1, GenInst_IList_1_t2690622622_0_0_0_Types };
extern const Il2CppType ICollection_1_t3101757326_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3101757326_0_0_0_Types[] = { &ICollection_1_t3101757326_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3101757326_0_0_0 = { 1, GenInst_ICollection_1_t3101757326_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2441809066_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2441809066_0_0_0_Types[] = { &IEnumerable_1_t2441809066_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2441809066_0_0_0 = { 1, GenInst_IEnumerable_1_t2441809066_0_0_0_Types };
extern const Il2CppType Int64U5BU5D_t717125112_0_0_0;
static const Il2CppType* GenInst_Int64U5BU5D_t717125112_0_0_0_Types[] = { &Int64U5BU5D_t717125112_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64U5BU5D_t717125112_0_0_0 = { 1, GenInst_Int64U5BU5D_t717125112_0_0_0_Types };
extern const Il2CppType IList_1_t1450018638_0_0_0;
static const Il2CppType* GenInst_IList_1_t1450018638_0_0_0_Types[] = { &IList_1_t1450018638_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1450018638_0_0_0 = { 1, GenInst_IList_1_t1450018638_0_0_0_Types };
extern const Il2CppType ICollection_1_t1861153342_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1861153342_0_0_0_Types[] = { &ICollection_1_t1861153342_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1861153342_0_0_0 = { 1, GenInst_ICollection_1_t1861153342_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1201205082_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1201205082_0_0_0_Types[] = { &IEnumerable_1_t1201205082_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1201205082_0_0_0 = { 1, GenInst_IEnumerable_1_t1201205082_0_0_0_Types };
extern const Il2CppType UInt32U5BU5DU5BU5D_t1156922361_0_0_0;
static const Il2CppType* GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0_Types[] = { &UInt32U5BU5DU5BU5D_t1156922361_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0 = { 1, GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0_Types };
extern const Il2CppType IList_1_t600326817_0_0_0;
static const Il2CppType* GenInst_IList_1_t600326817_0_0_0_Types[] = { &IList_1_t600326817_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t600326817_0_0_0 = { 1, GenInst_IList_1_t600326817_0_0_0_Types };
extern const Il2CppType ICollection_1_t1011461521_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1011461521_0_0_0_Types[] = { &ICollection_1_t1011461521_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1011461521_0_0_0 = { 1, GenInst_ICollection_1_t1011461521_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t351513261_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t351513261_0_0_0_Types[] = { &IEnumerable_1_t351513261_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t351513261_0_0_0 = { 1, GenInst_IEnumerable_1_t351513261_0_0_0_Types };
extern const Il2CppType IList_1_t75442244_0_0_0;
static const Il2CppType* GenInst_IList_1_t75442244_0_0_0_Types[] = { &IList_1_t75442244_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t75442244_0_0_0 = { 1, GenInst_IList_1_t75442244_0_0_0_Types };
extern const Il2CppType ICollection_1_t486576948_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t486576948_0_0_0_Types[] = { &ICollection_1_t486576948_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t486576948_0_0_0 = { 1, GenInst_ICollection_1_t486576948_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4121595984_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4121595984_0_0_0_Types[] = { &IEnumerable_1_t4121595984_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4121595984_0_0_0 = { 1, GenInst_IEnumerable_1_t4121595984_0_0_0_Types };
extern const Il2CppType IList_1_t3452350100_0_0_0;
static const Il2CppType* GenInst_IList_1_t3452350100_0_0_0_Types[] = { &IList_1_t3452350100_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3452350100_0_0_0 = { 1, GenInst_IList_1_t3452350100_0_0_0_Types };
extern const Il2CppType ICollection_1_t3863484804_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3863484804_0_0_0_Types[] = { &ICollection_1_t3863484804_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3863484804_0_0_0 = { 1, GenInst_ICollection_1_t3863484804_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3203536544_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3203536544_0_0_0_Types[] = { &IEnumerable_1_t3203536544_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3203536544_0_0_0 = { 1, GenInst_IEnumerable_1_t3203536544_0_0_0_Types };
extern const Il2CppType IList_1_t99252587_0_0_0;
static const Il2CppType* GenInst_IList_1_t99252587_0_0_0_Types[] = { &IList_1_t99252587_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t99252587_0_0_0 = { 1, GenInst_IList_1_t99252587_0_0_0_Types };
extern const Il2CppType ICollection_1_t510387291_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t510387291_0_0_0_Types[] = { &ICollection_1_t510387291_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t510387291_0_0_0 = { 1, GenInst_ICollection_1_t510387291_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4145406327_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4145406327_0_0_0_Types[] = { &IEnumerable_1_t4145406327_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4145406327_0_0_0 = { 1, GenInst_IEnumerable_1_t4145406327_0_0_0_Types };
extern const Il2CppType IList_1_t632609824_0_0_0;
static const Il2CppType* GenInst_IList_1_t632609824_0_0_0_Types[] = { &IList_1_t632609824_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t632609824_0_0_0 = { 1, GenInst_IList_1_t632609824_0_0_0_Types };
extern const Il2CppType ICollection_1_t1043744528_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1043744528_0_0_0_Types[] = { &ICollection_1_t1043744528_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1043744528_0_0_0 = { 1, GenInst_ICollection_1_t1043744528_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t383796268_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t383796268_0_0_0_Types[] = { &IEnumerable_1_t383796268_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t383796268_0_0_0 = { 1, GenInst_IEnumerable_1_t383796268_0_0_0_Types };
extern const Il2CppType IList_1_t3862439092_0_0_0;
static const Il2CppType* GenInst_IList_1_t3862439092_0_0_0_Types[] = { &IList_1_t3862439092_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3862439092_0_0_0 = { 1, GenInst_IList_1_t3862439092_0_0_0_Types };
extern const Il2CppType ICollection_1_t4273573796_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t4273573796_0_0_0_Types[] = { &ICollection_1_t4273573796_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t4273573796_0_0_0 = { 1, GenInst_ICollection_1_t4273573796_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3613625536_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3613625536_0_0_0_Types[] = { &IEnumerable_1_t3613625536_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3613625536_0_0_0 = { 1, GenInst_IEnumerable_1_t3613625536_0_0_0_Types };
extern const Il2CppType IList_1_t3231563223_0_0_0;
static const Il2CppType* GenInst_IList_1_t3231563223_0_0_0_Types[] = { &IList_1_t3231563223_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3231563223_0_0_0 = { 1, GenInst_IList_1_t3231563223_0_0_0_Types };
extern const Il2CppType ICollection_1_t3642697927_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3642697927_0_0_0_Types[] = { &ICollection_1_t3642697927_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3642697927_0_0_0 = { 1, GenInst_ICollection_1_t3642697927_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2982749667_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2982749667_0_0_0_Types[] = { &IEnumerable_1_t2982749667_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2982749667_0_0_0 = { 1, GenInst_IEnumerable_1_t2982749667_0_0_0_Types };
extern const Il2CppType IList_1_t3642697928_0_0_0;
static const Il2CppType* GenInst_IList_1_t3642697928_0_0_0_Types[] = { &IList_1_t3642697928_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3642697928_0_0_0 = { 1, GenInst_IList_1_t3642697928_0_0_0_Types };
extern const Il2CppType ICollection_1_t4053832631_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t4053832631_0_0_0_Types[] = { &ICollection_1_t4053832631_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t4053832631_0_0_0 = { 1, GenInst_ICollection_1_t4053832631_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3393884371_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3393884371_0_0_0_Types[] = { &IEnumerable_1_t3393884371_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3393884371_0_0_0 = { 1, GenInst_IEnumerable_1_t3393884371_0_0_0_Types };
extern const Il2CppType IList_1_t2982749668_0_0_0;
static const Il2CppType* GenInst_IList_1_t2982749668_0_0_0_Types[] = { &IList_1_t2982749668_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2982749668_0_0_0 = { 1, GenInst_IList_1_t2982749668_0_0_0_Types };
extern const Il2CppType ICollection_1_t3393884372_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3393884372_0_0_0_Types[] = { &ICollection_1_t3393884372_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3393884372_0_0_0 = { 1, GenInst_ICollection_1_t3393884372_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2733936111_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2733936111_0_0_0_Types[] = { &IEnumerable_1_t2733936111_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2733936111_0_0_0 = { 1, GenInst_IEnumerable_1_t2733936111_0_0_0_Types };
extern const Il2CppType ServerName_t2635557658_0_0_0;
static const Il2CppType* GenInst_ServerName_t2635557658_0_0_0_Types[] = { &ServerName_t2635557658_0_0_0 };
extern const Il2CppGenericInst GenInst_ServerName_t2635557658_0_0_0 = { 1, GenInst_ServerName_t2635557658_0_0_0_Types };
extern const Il2CppType X509CertificateStructure_t3705285294_0_0_0;
static const Il2CppType* GenInst_X509CertificateStructure_t3705285294_0_0_0_Types[] = { &X509CertificateStructure_t3705285294_0_0_0 };
extern const Il2CppGenericInst GenInst_X509CertificateStructure_t3705285294_0_0_0 = { 1, GenInst_X509CertificateStructure_t3705285294_0_0_0_Types };
extern const Il2CppType ECPoint_t626351532_0_0_0;
static const Il2CppType* GenInst_ECPoint_t626351532_0_0_0_Types[] = { &ECPoint_t626351532_0_0_0 };
extern const Il2CppGenericInst GenInst_ECPoint_t626351532_0_0_0 = { 1, GenInst_ECPoint_t626351532_0_0_0_Types };
extern const Il2CppType ECFieldElement_t1092946118_0_0_0;
static const Il2CppType* GenInst_ECFieldElement_t1092946118_0_0_0_Types[] = { &ECFieldElement_t1092946118_0_0_0 };
extern const Il2CppGenericInst GenInst_ECFieldElement_t1092946118_0_0_0 = { 1, GenInst_ECFieldElement_t1092946118_0_0_0_Types };
extern const Il2CppType WNafPreCompInfo_t485024160_0_0_0;
static const Il2CppType* GenInst_WNafPreCompInfo_t485024160_0_0_0_Types[] = { &WNafPreCompInfo_t485024160_0_0_0 };
extern const Il2CppGenericInst GenInst_WNafPreCompInfo_t485024160_0_0_0 = { 1, GenInst_WNafPreCompInfo_t485024160_0_0_0_Types };
extern const Il2CppType PreCompInfo_t1123315090_0_0_0;
static const Il2CppType* GenInst_PreCompInfo_t1123315090_0_0_0_Types[] = { &PreCompInfo_t1123315090_0_0_0 };
extern const Il2CppGenericInst GenInst_PreCompInfo_t1123315090_0_0_0 = { 1, GenInst_PreCompInfo_t1123315090_0_0_0_Types };
extern const Il2CppType LongArray_t194261203_0_0_0;
static const Il2CppType* GenInst_LongArray_t194261203_0_0_0_Types[] = { &LongArray_t194261203_0_0_0 };
extern const Il2CppGenericInst GenInst_LongArray_t194261203_0_0_0 = { 1, GenInst_LongArray_t194261203_0_0_0_Types };
extern const Il2CppType ZTauElement_t2571810054_0_0_0;
static const Il2CppType* GenInst_ZTauElement_t2571810054_0_0_0_Types[] = { &ZTauElement_t2571810054_0_0_0 };
extern const Il2CppGenericInst GenInst_ZTauElement_t2571810054_0_0_0 = { 1, GenInst_ZTauElement_t2571810054_0_0_0_Types };
extern const Il2CppType SByteU5BU5D_t3472287392_0_0_0;
static const Il2CppType* GenInst_SByteU5BU5D_t3472287392_0_0_0_Types[] = { &SByteU5BU5D_t3472287392_0_0_0 };
extern const Il2CppGenericInst GenInst_SByteU5BU5D_t3472287392_0_0_0 = { 1, GenInst_SByteU5BU5D_t3472287392_0_0_0_Types };
extern const Il2CppType IList_1_t995358150_0_0_0;
static const Il2CppType* GenInst_IList_1_t995358150_0_0_0_Types[] = { &IList_1_t995358150_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t995358150_0_0_0 = { 1, GenInst_IList_1_t995358150_0_0_0_Types };
extern const Il2CppType ICollection_1_t1406492854_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1406492854_0_0_0_Types[] = { &ICollection_1_t1406492854_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1406492854_0_0_0 = { 1, GenInst_ICollection_1_t1406492854_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t746544594_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t746544594_0_0_0_Types[] = { &IEnumerable_1_t746544594_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t746544594_0_0_0 = { 1, GenInst_IEnumerable_1_t746544594_0_0_0_Types };
extern const Il2CppType AbstractF2mPoint_t883694769_0_0_0;
static const Il2CppType* GenInst_AbstractF2mPoint_t883694769_0_0_0_Types[] = { &AbstractF2mPoint_t883694769_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractF2mPoint_t883694769_0_0_0 = { 1, GenInst_AbstractF2mPoint_t883694769_0_0_0_Types };
extern const Il2CppType ECPointBase_t3119694375_0_0_0;
static const Il2CppType* GenInst_ECPointBase_t3119694375_0_0_0_Types[] = { &ECPointBase_t3119694375_0_0_0 };
extern const Il2CppGenericInst GenInst_ECPointBase_t3119694375_0_0_0 = { 1, GenInst_ECPointBase_t3119694375_0_0_0_Types };
extern const Il2CppType Config_t1249383685_0_0_0;
static const Il2CppType* GenInst_Config_t1249383685_0_0_0_Types[] = { &Config_t1249383685_0_0_0 };
extern const Il2CppGenericInst GenInst_Config_t1249383685_0_0_0 = { 1, GenInst_Config_t1249383685_0_0_0_Types };
extern const Il2CppType OnEventDelegate_t790674770_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_Types[] = { &String_t_0_0_0, &OnEventDelegate_t790674770_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0 = { 2, GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &OnEventDelegate_t790674770_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_OnEventDelegate_t790674770_0_0_0_Types[] = { &OnEventDelegate_t790674770_0_0_0 };
extern const Il2CppGenericInst GenInst_OnEventDelegate_t790674770_0_0_0 = { 1, GenInst_OnEventDelegate_t790674770_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t462799254_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_KeyValuePair_2_t462799254_0_0_0_Types[] = { &String_t_0_0_0, &OnEventDelegate_t790674770_0_0_0, &KeyValuePair_2_t462799254_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_KeyValuePair_2_t462799254_0_0_0 = { 3, GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_KeyValuePair_2_t462799254_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t462799254_0_0_0_Types[] = { &KeyValuePair_2_t462799254_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t462799254_0_0_0 = { 1, GenInst_KeyValuePair_2_t462799254_0_0_0_Types };
extern const Il2CppType EventSourceResponse_t2287402344_0_0_0;
extern const Il2CppType Message_t1650395211_0_0_0;
static const Il2CppType* GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0_Types[] = { &EventSourceResponse_t2287402344_0_0_0, &Message_t1650395211_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0 = { 2, GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0_Types };
static const Il2CppType* GenInst_EventSourceResponse_t2287402344_0_0_0_Types[] = { &EventSourceResponse_t2287402344_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSourceResponse_t2287402344_0_0_0 = { 1, GenInst_EventSourceResponse_t2287402344_0_0_0_Types };
static const Il2CppType* GenInst_Message_t1650395211_0_0_0_Types[] = { &Message_t1650395211_0_0_0 };
extern const Il2CppGenericInst GenInst_Message_t1650395211_0_0_0 = { 1, GenInst_Message_t1650395211_0_0_0_Types };
extern const Il2CppType Hub_t272719679_0_0_0;
static const Il2CppType* GenInst_Hub_t272719679_0_0_0_Types[] = { &Hub_t272719679_0_0_0 };
extern const Il2CppGenericInst GenInst_Hub_t272719679_0_0_0 = { 1, GenInst_Hub_t272719679_0_0_0_Types };
extern const Il2CppType IHub_t3409721544_0_0_0;
static const Il2CppType* GenInst_IHub_t3409721544_0_0_0_Types[] = { &IHub_t3409721544_0_0_0 };
extern const Il2CppGenericInst GenInst_IHub_t3409721544_0_0_0 = { 1, GenInst_IHub_t3409721544_0_0_0_Types };
extern const Il2CppType IServerMessage_t2384143743_0_0_0;
static const Il2CppType* GenInst_IServerMessage_t2384143743_0_0_0_Types[] = { &IServerMessage_t2384143743_0_0_0 };
extern const Il2CppGenericInst GenInst_IServerMessage_t2384143743_0_0_0 = { 1, GenInst_IServerMessage_t2384143743_0_0_0_Types };
extern const Il2CppType NegotiationData_t3059020807_0_0_0;
static const Il2CppType* GenInst_NegotiationData_t3059020807_0_0_0_Types[] = { &NegotiationData_t3059020807_0_0_0 };
extern const Il2CppGenericInst GenInst_NegotiationData_t3059020807_0_0_0 = { 1, GenInst_NegotiationData_t3059020807_0_0_0_Types };
static const Il2CppType* GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0_Types[] = { &NegotiationData_t3059020807_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0 = { 2, GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ClientMessage_t624279968_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t322707255_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t322707255_0_0_0_Types[] = { &KeyValuePair_2_t322707255_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322707255_0_0_0 = { 1, GenInst_KeyValuePair_2_t322707255_0_0_0_Types };
static const Il2CppType* GenInst_ClientMessage_t624279968_0_0_0_Types[] = { &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientMessage_t624279968_0_0_0 = { 1, GenInst_ClientMessage_t624279968_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &KeyValuePair_2_t322707255_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types };
extern const Il2CppType OnMethodCallCallbackDelegate_t3483117754_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types[] = { &String_t_0_0_0, &OnMethodCallCallbackDelegate_t3483117754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0 = { 2, GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &OnMethodCallCallbackDelegate_t3483117754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types[] = { &OnMethodCallCallbackDelegate_t3483117754_0_0_0 };
extern const Il2CppGenericInst GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0 = { 1, GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3155242238_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_KeyValuePair_2_t3155242238_0_0_0_Types[] = { &String_t_0_0_0, &OnMethodCallCallbackDelegate_t3483117754_0_0_0, &KeyValuePair_2_t3155242238_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_KeyValuePair_2_t3155242238_0_0_0 = { 3, GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_KeyValuePair_2_t3155242238_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3155242238_0_0_0_Types[] = { &KeyValuePair_2_t3155242238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3155242238_0_0_0 = { 1, GenInst_KeyValuePair_2_t3155242238_0_0_0_Types };
extern const Il2CppType SocketIOCallback_t88619200_0_0_0;
static const Il2CppType* GenInst_SocketIOCallback_t88619200_0_0_0_Types[] = { &SocketIOCallback_t88619200_0_0_0 };
extern const Il2CppGenericInst GenInst_SocketIOCallback_t88619200_0_0_0 = { 1, GenInst_SocketIOCallback_t88619200_0_0_0_Types };
extern const Il2CppType List_1_t3426161967_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3426161967_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_Types };
extern const Il2CppType EventDescriptor_t4057040835_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t4057040835_0_0_0_Types[] = { &EventDescriptor_t4057040835_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t4057040835_0_0_0 = { 1, GenInst_EventDescriptor_t4057040835_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3426161967_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t3426161967_0_0_0_Types[] = { &List_1_t3426161967_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3426161967_0_0_0 = { 1, GenInst_List_1_t3426161967_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3098286451_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_KeyValuePair_2_t3098286451_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3426161967_0_0_0, &KeyValuePair_2_t3098286451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_KeyValuePair_2_t3098286451_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_KeyValuePair_2_t3098286451_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3098286451_0_0_0_Types[] = { &KeyValuePair_2_t3098286451_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3098286451_0_0_0 = { 1, GenInst_KeyValuePair_2_t3098286451_0_0_0_Types };
extern const Il2CppType Dictionary_2_t309261261_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0 = { 2, GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Types };
extern const Il2CppType SocketIOAckCallback_t53599143_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SocketIOAckCallback_t53599143_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SocketIOAckCallback_t53599143_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_SocketIOAckCallback_t53599143_0_0_0_Types[] = { &SocketIOAckCallback_t53599143_0_0_0 };
extern const Il2CppGenericInst GenInst_SocketIOAckCallback_t53599143_0_0_0 = { 1, GenInst_SocketIOAckCallback_t53599143_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1113737296_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_KeyValuePair_2_t1113737296_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SocketIOAckCallback_t53599143_0_0_0, &KeyValuePair_2_t1113737296_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_KeyValuePair_2_t1113737296_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_KeyValuePair_2_t1113737296_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1113737296_0_0_0_Types[] = { &KeyValuePair_2_t1113737296_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1113737296_0_0_0 = { 1, GenInst_KeyValuePair_2_t1113737296_0_0_0_Types };
extern const Il2CppType Socket_t2716624701_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_Types[] = { &String_t_0_0_0, &Socket_t2716624701_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0 = { 2, GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Socket_t2716624701_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Socket_t2716624701_0_0_0_Types[] = { &Socket_t2716624701_0_0_0 };
extern const Il2CppGenericInst GenInst_Socket_t2716624701_0_0_0 = { 1, GenInst_Socket_t2716624701_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2388749185_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_KeyValuePair_2_t2388749185_0_0_0_Types[] = { &String_t_0_0_0, &Socket_t2716624701_0_0_0, &KeyValuePair_2_t2388749185_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_KeyValuePair_2_t2388749185_0_0_0 = { 3, GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_KeyValuePair_2_t2388749185_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2388749185_0_0_0_Types[] = { &KeyValuePair_2_t2388749185_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2388749185_0_0_0 = { 1, GenInst_KeyValuePair_2_t2388749185_0_0_0_Types };
extern const Il2CppType Packet_t1309324146_0_0_0;
static const Il2CppType* GenInst_Packet_t1309324146_0_0_0_Types[] = { &Packet_t1309324146_0_0_0 };
extern const Il2CppGenericInst GenInst_Packet_t1309324146_0_0_0 = { 1, GenInst_Packet_t1309324146_0_0_0_Types };
extern const Il2CppType IExtension_t2171905938_0_0_0;
static const Il2CppType* GenInst_IExtension_t2171905938_0_0_0_Types[] = { &IExtension_t2171905938_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtension_t2171905938_0_0_0 = { 1, GenInst_IExtension_t2171905938_0_0_0_Types };
extern const Il2CppType WebSocketFrame_t4163283394_0_0_0;
static const Il2CppType* GenInst_WebSocketFrame_t4163283394_0_0_0_Types[] = { &WebSocketFrame_t4163283394_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketFrame_t4163283394_0_0_0 = { 1, GenInst_WebSocketFrame_t4163283394_0_0_0_Types };
extern const Il2CppType WebSocketFrameReader_t549273869_0_0_0;
static const Il2CppType* GenInst_WebSocketFrameReader_t549273869_0_0_0_Types[] = { &WebSocketFrameReader_t549273869_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketFrameReader_t549273869_0_0_0 = { 1, GenInst_WebSocketFrameReader_t549273869_0_0_0_Types };
extern const Il2CppType WebSocketResponse_t3376763264_0_0_0;
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0 = { 2, GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0 = { 2, GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types };
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &UInt16_t986882611_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0 = { 3, GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t986882611_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &WebSocketFrameReader_t549273869_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0 = { 2, GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0_Types };
extern const Il2CppType Texture2D_t3542995729_0_0_0;
static const Il2CppType* GenInst_Texture2D_t3542995729_0_0_0_Types[] = { &Texture2D_t3542995729_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t3542995729_0_0_0 = { 1, GenInst_Texture2D_t3542995729_0_0_0_Types };
extern const Il2CppType JsonData_t269267574_0_0_0;
static const Il2CppType* GenInst_JsonData_t269267574_0_0_0_Types[] = { &JsonData_t269267574_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonData_t269267574_0_0_0 = { 1, GenInst_JsonData_t269267574_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t269267574_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4236359354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4236359354_0_0_0_Types[] = { &KeyValuePair_2_t4236359354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4236359354_0_0_0 = { 1, GenInst_KeyValuePair_2_t4236359354_0_0_0_Types };
extern const Il2CppType IJsonWrapper_t3045908096_0_0_0;
static const Il2CppType* GenInst_IJsonWrapper_t3045908096_0_0_0_Types[] = { &IJsonWrapper_t3045908096_0_0_0 };
extern const Il2CppGenericInst GenInst_IJsonWrapper_t3045908096_0_0_0 = { 1, GenInst_IJsonWrapper_t3045908096_0_0_0_Types };
extern const Il2CppType IOrderedDictionary_t300723224_0_0_0;
static const Il2CppType* GenInst_IOrderedDictionary_t300723224_0_0_0_Types[] = { &IOrderedDictionary_t300723224_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedDictionary_t300723224_0_0_0 = { 1, GenInst_IOrderedDictionary_t300723224_0_0_0_Types };
extern const Il2CppType IDictionary_t596158605_0_0_0;
static const Il2CppType* GenInst_IDictionary_t596158605_0_0_0_Types[] = { &IDictionary_t596158605_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_t596158605_0_0_0 = { 1, GenInst_IDictionary_t596158605_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2473249743_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2473249743_0_0_0_Types[] = { &IEquatable_1_t2473249743_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2473249743_0_0_0 = { 1, GenInst_IEquatable_1_t2473249743_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t269267574_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_KeyValuePair_2_t4236359354_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t269267574_0_0_0, &KeyValuePair_2_t4236359354_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_KeyValuePair_2_t4236359354_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_KeyValuePair_2_t4236359354_0_0_0_Types };
extern const Il2CppType PropertyMetadata_t3693826136_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 2, GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1043231486_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1043231486_0_0_0_Types[] = { &KeyValuePair_2_t1043231486_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1043231486_0_0_0 = { 1, GenInst_KeyValuePair_2_t1043231486_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t3693826136_0_0_0_Types[] = { &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3693826136_0_0_0 = { 1, GenInst_PropertyMetadata_t3693826136_0_0_0_Types };
extern const Il2CppType ExporterFunc_t408878057_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t408878057_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0 = { 2, GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_Types };
static const Il2CppType* GenInst_ExporterFunc_t408878057_0_0_0_Types[] = { &ExporterFunc_t408878057_0_0_0 };
extern const Il2CppGenericInst GenInst_ExporterFunc_t408878057_0_0_0 = { 1, GenInst_ExporterFunc_t408878057_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2914292212_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t2914292212_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_Types };
extern const Il2CppType ImporterFunc_t2977850894_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t2977850894_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0 = { 2, GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_Types };
static const Il2CppType* GenInst_ImporterFunc_t2977850894_0_0_0_Types[] = { &ImporterFunc_t2977850894_0_0_0 };
extern const Il2CppGenericInst GenInst_ImporterFunc_t2977850894_0_0_0 = { 1, GenInst_ImporterFunc_t2977850894_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t2914292212_0_0_0_Types[] = { &IDictionary_2_t2914292212_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2914292212_0_0_0 = { 1, GenInst_IDictionary_2_t2914292212_0_0_0_Types };
extern const Il2CppType ArrayMetadata_t2008834462_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 2, GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3653207108_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3653207108_0_0_0_Types[] = { &KeyValuePair_2_t3653207108_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653207108_0_0_0 = { 1, GenInst_KeyValuePair_2_t3653207108_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t2008834462_0_0_0_Types[] = { &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t2008834462_0_0_0 = { 1, GenInst_ArrayMetadata_t2008834462_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3266987655_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3266987655_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3266987655_0_0_0_Types[] = { &IDictionary_2_t3266987655_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3266987655_0_0_0 = { 1, GenInst_IDictionary_2_t3266987655_0_0_0_Types };
extern const Il2CppType ObjectMetadata_t3995922398_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 2, GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1345327748_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1345327748_0_0_0_Types[] = { &KeyValuePair_2_t1345327748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1345327748_0_0_0 = { 1, GenInst_KeyValuePair_2_t1345327748_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t3995922398_0_0_0_Types[] = { &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t3995922398_0_0_0 = { 1, GenInst_ObjectMetadata_t3995922398_0_0_0_Types };
extern const Il2CppType IList_1_t4234766737_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t4234766737_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0 = { 2, GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_Types };
static const Il2CppType* GenInst_IList_1_t4234766737_0_0_0_Types[] = { &IList_1_t4234766737_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4234766737_0_0_0 = { 1, GenInst_IList_1_t4234766737_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &KeyValuePair_2_t3653207108_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1703537581_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t1703537581_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &KeyValuePair_2_t1703537581_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t1703537581_0_0_0 = { 3, GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t1703537581_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1703537581_0_0_0_Types[] = { &KeyValuePair_2_t1703537581_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1703537581_0_0_0 = { 1, GenInst_KeyValuePair_2_t1703537581_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3266987655_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2961690774_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3266987655_0_0_0, &KeyValuePair_2_t2961690774_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2961690774_0_0_0_Types[] = { &KeyValuePair_2_t2961690774_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2961690774_0_0_0 = { 1, GenInst_KeyValuePair_2_t2961690774_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &KeyValuePair_2_t1345327748_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3690625517_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t3690625517_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &KeyValuePair_2_t3690625517_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t3690625517_0_0_0 = { 3, GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t3690625517_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3690625517_0_0_0_Types[] = { &KeyValuePair_2_t3690625517_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3690625517_0_0_0 = { 1, GenInst_KeyValuePair_2_t3690625517_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t4234766737_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3929469856_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_KeyValuePair_2_t3929469856_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t4234766737_0_0_0, &KeyValuePair_2_t3929469856_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_KeyValuePair_2_t3929469856_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_KeyValuePair_2_t3929469856_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3929469856_0_0_0_Types[] = { &KeyValuePair_2_t3929469856_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3929469856_0_0_0 = { 1, GenInst_KeyValuePair_2_t3929469856_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t408878057_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t103581176_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_KeyValuePair_2_t103581176_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t408878057_0_0_0, &KeyValuePair_2_t103581176_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_KeyValuePair_2_t103581176_0_0_0 = { 3, GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_KeyValuePair_2_t103581176_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t103581176_0_0_0_Types[] = { &KeyValuePair_2_t103581176_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t103581176_0_0_0 = { 1, GenInst_KeyValuePair_2_t103581176_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t2914292212_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2608995331_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_KeyValuePair_2_t2608995331_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t2914292212_0_0_0, &KeyValuePair_2_t2608995331_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_KeyValuePair_2_t2608995331_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_KeyValuePair_2_t2608995331_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2608995331_0_0_0_Types[] = { &KeyValuePair_2_t2608995331_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2608995331_0_0_0 = { 1, GenInst_KeyValuePair_2_t2608995331_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &KeyValuePair_2_t1043231486_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3365950620_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t3365950620_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &KeyValuePair_2_t3365950620_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t3365950620_0_0_0 = { 3, GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t3365950620_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3365950620_0_0_0_Types[] = { &KeyValuePair_2_t3365950620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3365950620_0_0_0 = { 1, GenInst_KeyValuePair_2_t3365950620_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3025249456_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0, &KeyValuePair_2_t3025249456_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0 = { 3, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3025249456_0_0_0_Types[] = { &KeyValuePair_2_t3025249456_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3025249456_0_0_0 = { 1, GenInst_KeyValuePair_2_t3025249456_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t2977850894_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2672554013_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_KeyValuePair_2_t2672554013_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t2977850894_0_0_0, &KeyValuePair_2_t2672554013_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_KeyValuePair_2_t2672554013_0_0_0 = { 3, GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_KeyValuePair_2_t2672554013_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2672554013_0_0_0_Types[] = { &KeyValuePair_2_t2672554013_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2672554013_0_0_0 = { 1, GenInst_KeyValuePair_2_t2672554013_0_0_0_Types };
extern const Il2CppType IDictionary_2_t37308697_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IDictionary_2_t37308697_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32U5BU5D_t3030399641_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t37308697_0_0_0_Types[] = { &IDictionary_2_t37308697_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t37308697_0_0_0 = { 1, GenInst_IDictionary_2_t37308697_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IDictionary_2_t37308697_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1097446850_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_KeyValuePair_2_t1097446850_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IDictionary_2_t37308697_0_0_0, &KeyValuePair_2_t1097446850_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_KeyValuePair_2_t1097446850_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_KeyValuePair_2_t1097446850_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1097446850_0_0_0_Types[] = { &KeyValuePair_2_t1097446850_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1097446850_0_0_0 = { 1, GenInst_KeyValuePair_2_t1097446850_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32U5BU5D_t3030399641_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4090537794_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_KeyValuePair_2_t4090537794_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32U5BU5D_t3030399641_0_0_0, &KeyValuePair_2_t4090537794_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_KeyValuePair_2_t4090537794_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_KeyValuePair_2_t4090537794_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4090537794_0_0_0_Types[] = { &KeyValuePair_2_t4090537794_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4090537794_0_0_0 = { 1, GenInst_KeyValuePair_2_t4090537794_0_0_0_Types };
extern const Il2CppType WriterContext_t4137194742_0_0_0;
static const Il2CppType* GenInst_WriterContext_t4137194742_0_0_0_Types[] = { &WriterContext_t4137194742_0_0_0 };
extern const Il2CppGenericInst GenInst_WriterContext_t4137194742_0_0_0 = { 1, GenInst_WriterContext_t4137194742_0_0_0_Types };
extern const Il2CppType StateHandler_t387387051_0_0_0;
static const Il2CppType* GenInst_StateHandler_t387387051_0_0_0_Types[] = { &StateHandler_t387387051_0_0_0 };
extern const Il2CppGenericInst GenInst_StateHandler_t387387051_0_0_0 = { 1, GenInst_StateHandler_t387387051_0_0_0_Types };
extern const Il2CppType MulticastDelegate_t3201952435_0_0_0;
static const Il2CppType* GenInst_MulticastDelegate_t3201952435_0_0_0_Types[] = { &MulticastDelegate_t3201952435_0_0_0 };
extern const Il2CppGenericInst GenInst_MulticastDelegate_t3201952435_0_0_0 = { 1, GenInst_MulticastDelegate_t3201952435_0_0_0_Types };
extern const Il2CppType SampleDescriptor_t3285910703_0_0_0;
static const Il2CppType* GenInst_SampleDescriptor_t3285910703_0_0_0_Types[] = { &SampleDescriptor_t3285910703_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleDescriptor_t3285910703_0_0_0 = { 1, GenInst_SampleDescriptor_t3285910703_0_0_0_Types };
extern const Il2CppType MessageTypes_t223757197_0_0_0;
static const Il2CppType* GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0_Types[] = { &MessageTypes_t223757197_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0 = { 2, GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0_Types[] = { &MessageTypes_t223757197_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypes_t223757197_0_0_0_Types[] = { &MessageTypes_t223757197_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypes_t223757197_0_0_0 = { 1, GenInst_MessageTypes_t223757197_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType1_2_t377947286_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &U3CU3E__AnonType1_2_t377947286_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0_Types };
extern const Il2CppType ColorPicker_t3035206225_0_0_0;
static const Il2CppType* GenInst_ColorPicker_t3035206225_0_0_0_Types[] = { &ColorPicker_t3035206225_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorPicker_t3035206225_0_0_0 = { 1, GenInst_ColorPicker_t3035206225_0_0_0_Types };
static const Il2CppType* GenInst_ColorPicker_t3035206225_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ColorPicker_t3035206225_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorPicker_t3035206225_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ColorPicker_t3035206225_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Data_t407761726_0_0_0;
static const Il2CppType* GenInst_Data_t407761726_0_0_0_Types[] = { &Data_t407761726_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t407761726_0_0_0 = { 1, GenInst_Data_t407761726_0_0_0_Types };
extern const Il2CppType Data_t2523487274_0_0_0;
static const Il2CppType* GenInst_Data_t2523487274_0_0_0_Types[] = { &Data_t2523487274_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t2523487274_0_0_0 = { 1, GenInst_Data_t2523487274_0_0_0_Types };
extern const Il2CppType InventoryData_t3776523848_0_0_0;
static const Il2CppType* GenInst_InventoryData_t3776523848_0_0_0_Types[] = { &InventoryData_t3776523848_0_0_0 };
extern const Il2CppGenericInst GenInst_InventoryData_t3776523848_0_0_0 = { 1, GenInst_InventoryData_t3776523848_0_0_0_Types };
extern const Il2CppType Data_t4011291362_0_0_0;
static const Il2CppType* GenInst_Data_t4011291362_0_0_0_Types[] = { &Data_t4011291362_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t4011291362_0_0_0 = { 1, GenInst_Data_t4011291362_0_0_0_Types };
extern const Il2CppType Data_t3997843590_0_0_0;
static const Il2CppType* GenInst_Data_t3997843590_0_0_0_Types[] = { &Data_t3997843590_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t3997843590_0_0_0 = { 1, GenInst_Data_t3997843590_0_0_0_Types };
extern const Il2CppType SlotData_t2756082054_0_0_0;
static const Il2CppType* GenInst_SlotData_t2756082054_0_0_0_Types[] = { &SlotData_t2756082054_0_0_0 };
extern const Il2CppGenericInst GenInst_SlotData_t2756082054_0_0_0 = { 1, GenInst_SlotData_t2756082054_0_0_0_Types };
extern const Il2CppType SlotController_t2060013442_0_0_0;
static const Il2CppType* GenInst_SlotController_t2060013442_0_0_0_Types[] = { &SlotController_t2060013442_0_0_0 };
extern const Il2CppGenericInst GenInst_SlotController_t2060013442_0_0_0 = { 1, GenInst_SlotController_t2060013442_0_0_0_Types };
extern const Il2CppType IEnhancedScrollerDelegate_t2066056166_0_0_0;
static const Il2CppType* GenInst_IEnhancedScrollerDelegate_t2066056166_0_0_0_Types[] = { &IEnhancedScrollerDelegate_t2066056166_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnhancedScrollerDelegate_t2066056166_0_0_0 = { 1, GenInst_IEnhancedScrollerDelegate_t2066056166_0_0_0_Types };
extern const Il2CppType Data_t1832928442_0_0_0;
static const Il2CppType* GenInst_Data_t1832928442_0_0_0_Types[] = { &Data_t1832928442_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t1832928442_0_0_0 = { 1, GenInst_Data_t1832928442_0_0_0_Types };
extern const Il2CppType Data_t4174191502_0_0_0;
static const Il2CppType* GenInst_Data_t4174191502_0_0_0_Types[] = { &Data_t4174191502_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t4174191502_0_0_0 = { 1, GenInst_Data_t4174191502_0_0_0_Types };
extern const Il2CppType Data_t2429895758_0_0_0;
static const Il2CppType* GenInst_Data_t2429895758_0_0_0_Types[] = { &Data_t2429895758_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t2429895758_0_0_0 = { 1, GenInst_Data_t2429895758_0_0_0_Types };
extern const Il2CppType EnhancedScrollerCellView_t1104668249_0_0_0;
static const Il2CppType* GenInst_EnhancedScrollerCellView_t1104668249_0_0_0_Types[] = { &EnhancedScrollerCellView_t1104668249_0_0_0 };
extern const Il2CppGenericInst GenInst_EnhancedScrollerCellView_t1104668249_0_0_0 = { 1, GenInst_EnhancedScrollerCellView_t1104668249_0_0_0_Types };
extern const Il2CppType CountryCodeData_t1765037603_0_0_0;
static const Il2CppType* GenInst_CountryCodeData_t1765037603_0_0_0_Types[] = { &CountryCodeData_t1765037603_0_0_0 };
extern const Il2CppGenericInst GenInst_CountryCodeData_t1765037603_0_0_0 = { 1, GenInst_CountryCodeData_t1765037603_0_0_0_Types };
extern const Il2CppType DefaultCodeData_t2288261144_0_0_0;
static const Il2CppType* GenInst_DefaultCodeData_t2288261144_0_0_0_Types[] = { &DefaultCodeData_t2288261144_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultCodeData_t2288261144_0_0_0 = { 1, GenInst_DefaultCodeData_t2288261144_0_0_0_Types };
extern const Il2CppType LanguageData_t336865618_0_0_0;
static const Il2CppType* GenInst_LanguageData_t336865618_0_0_0_Types[] = { &LanguageData_t336865618_0_0_0 };
extern const Il2CppGenericInst GenInst_LanguageData_t336865618_0_0_0 = { 1, GenInst_LanguageData_t336865618_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Double_t4078015681_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Double_t4078015681_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Double_t4078015681_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_String_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Vector3U5BU5D_t1172311765_0_0_0_Types[] = { &Vector3U5BU5D_t1172311765_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3U5BU5D_t1172311765_0_0_0 = { 1, GenInst_Vector3U5BU5D_t1172311765_0_0_0_Types };
extern const Il2CppType ReversedRipple_t2344809957_0_0_0;
static const Il2CppType* GenInst_ReversedRipple_t2344809957_0_0_0_Types[] = { &ReversedRipple_t2344809957_0_0_0 };
extern const Il2CppGenericInst GenInst_ReversedRipple_t2344809957_0_0_0 = { 1, GenInst_ReversedRipple_t2344809957_0_0_0_Types };
extern const Il2CppType Mesh_t1356156583_0_0_0;
static const Il2CppType* GenInst_Mesh_t1356156583_0_0_0_Types[] = { &Mesh_t1356156583_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_t1356156583_0_0_0 = { 1, GenInst_Mesh_t1356156583_0_0_0_Types };
extern const Il2CppType Regex_t1803876613_0_0_0;
static const Il2CppType* GenInst_Regex_t1803876613_0_0_0_Types[] = { &Regex_t1803876613_0_0_0 };
extern const Il2CppGenericInst GenInst_Regex_t1803876613_0_0_0 = { 1, GenInst_Regex_t1803876613_0_0_0_Types };
extern const Il2CppType RayBehavior_t3027150638_0_0_0;
static const Il2CppType* GenInst_RayBehavior_t3027150638_0_0_0_Types[] = { &RayBehavior_t3027150638_0_0_0 };
extern const Il2CppGenericInst GenInst_RayBehavior_t3027150638_0_0_0 = { 1, GenInst_RayBehavior_t3027150638_0_0_0_Types };
extern const Il2CppType CameraManager_t2379859346_0_0_0;
static const Il2CppType* GenInst_CameraManager_t2379859346_0_0_0_Types[] = { &CameraManager_t2379859346_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraManager_t2379859346_0_0_0 = { 1, GenInst_CameraManager_t2379859346_0_0_0_Types };
extern const Il2CppType Vertex_t3813209450_0_0_0;
static const Il2CppType* GenInst_Vertex_t3813209450_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Vertex_t3813209450_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Vertex_t3813209450_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Vertex_t3813209450_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Vertex_t3813209450_0_0_0_Types[] = { &Vertex_t3813209450_0_0_0 };
extern const Il2CppGenericInst GenInst_Vertex_t3813209450_0_0_0 = { 1, GenInst_Vertex_t3813209450_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3796471559_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3796471559_0_0_0_Types[] = { &KeyValuePair_2_t3796471559_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3796471559_0_0_0 = { 1, GenInst_KeyValuePair_2_t3796471559_0_0_0_Types };
extern const Il2CppType MeshData_t1378722312_0_0_0;
static const Il2CppType* GenInst_MeshData_t1378722312_0_0_0_Types[] = { &MeshData_t1378722312_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshData_t1378722312_0_0_0 = { 1, GenInst_MeshData_t1378722312_0_0_0_Types };
extern const Il2CppType List_1_t747843444_0_0_0;
static const Il2CppType* GenInst_List_1_t747843444_0_0_0_Types[] = { &List_1_t747843444_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t747843444_0_0_0 = { 1, GenInst_List_1_t747843444_0_0_0_Types };
extern const Il2CppType List_1_t746592055_0_0_0;
static const Il2CppType* GenInst_List_1_t746592055_0_0_0_Types[] = { &List_1_t746592055_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t746592055_0_0_0 = { 1, GenInst_List_1_t746592055_0_0_0_Types };
extern const Il2CppType MeshData_t1377470923_0_0_0;
static const Il2CppType* GenInst_MeshData_t1377470923_0_0_0_Types[] = { &MeshData_t1377470923_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshData_t1377470923_0_0_0 = { 1, GenInst_MeshData_t1377470923_0_0_0_Types };
extern const Il2CppType AIMode_t3831275989_0_0_0;
static const Il2CppType* GenInst_AIMode_t3831275989_0_0_0_Types[] = { &AIMode_t3831275989_0_0_0 };
extern const Il2CppGenericInst GenInst_AIMode_t3831275989_0_0_0 = { 1, GenInst_AIMode_t3831275989_0_0_0_Types };
extern const Il2CppType UIManager_t2519183485_0_0_0;
static const Il2CppType* GenInst_UIManager_t2519183485_0_0_0_Types[] = { &UIManager_t2519183485_0_0_0 };
extern const Il2CppGenericInst GenInst_UIManager_t2519183485_0_0_0 = { 1, GenInst_UIManager_t2519183485_0_0_0_Types };
extern const Il2CppType Module_t3140434828_0_0_0;
static const Il2CppType* GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Module_t3140434828_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Module_t3140434828_0_0_0_Types[] = { &Module_t3140434828_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t3140434828_0_0_0 = { 1, GenInst_Module_t3140434828_0_0_0_Types };
static const Il2CppType* GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Module_t3140434828_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2178748957_0_0_0;
static const Il2CppType* GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t2178748957_0_0_0_Types[] = { &Module_t3140434828_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t2178748957_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t2178748957_0_0_0 = { 3, GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t2178748957_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2178748957_0_0_0_Types[] = { &KeyValuePair_2_t2178748957_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2178748957_0_0_0 = { 1, GenInst_KeyValuePair_2_t2178748957_0_0_0_Types };
extern const Il2CppType SceneManager_t1824971699_0_0_0;
static const Il2CppType* GenInst_SceneManager_t1824971699_0_0_0_Types[] = { &SceneManager_t1824971699_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneManager_t1824971699_0_0_0 = { 1, GenInst_SceneManager_t1824971699_0_0_0_Types };
extern const Il2CppType PlayerMobilmoManager_t2341644285_0_0_0;
static const Il2CppType* GenInst_PlayerMobilmoManager_t2341644285_0_0_0_Types[] = { &PlayerMobilmoManager_t2341644285_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerMobilmoManager_t2341644285_0_0_0 = { 1, GenInst_PlayerMobilmoManager_t2341644285_0_0_0_Types };
extern const Il2CppType ActionManager_t1367723175_0_0_0;
static const Il2CppType* GenInst_ActionManager_t1367723175_0_0_0_Types[] = { &ActionManager_t1367723175_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionManager_t1367723175_0_0_0 = { 1, GenInst_ActionManager_t1367723175_0_0_0_Types };
extern const Il2CppType WorldManager_t3923509627_0_0_0;
static const Il2CppType* GenInst_WorldManager_t3923509627_0_0_0_Types[] = { &WorldManager_t3923509627_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldManager_t3923509627_0_0_0 = { 1, GenInst_WorldManager_t3923509627_0_0_0_Types };
extern const Il2CppType InputManager_t1610719423_0_0_0;
static const Il2CppType* GenInst_InputManager_t1610719423_0_0_0_Types[] = { &InputManager_t1610719423_0_0_0 };
extern const Il2CppGenericInst GenInst_InputManager_t1610719423_0_0_0 = { 1, GenInst_InputManager_t1610719423_0_0_0_Types };
extern const Il2CppType RayManager_t3612671535_0_0_0;
static const Il2CppType* GenInst_RayManager_t3612671535_0_0_0_Types[] = { &RayManager_t3612671535_0_0_0 };
extern const Il2CppGenericInst GenInst_RayManager_t3612671535_0_0_0 = { 1, GenInst_RayManager_t3612671535_0_0_0_Types };
static const Il2CppType* GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Types[] = { &Module_t3140434828_0_0_0, &Int32_t2071877448_0_0_0, &Module_t3140434828_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0 = { 3, GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Types };
extern const Il2CppType EvasionNav_t3997284964_0_0_0;
static const Il2CppType* GenInst_EvasionNav_t3997284964_0_0_0_Types[] = { &EvasionNav_t3997284964_0_0_0 };
extern const Il2CppGenericInst GenInst_EvasionNav_t3997284964_0_0_0 = { 1, GenInst_EvasionNav_t3997284964_0_0_0_Types };
extern const Il2CppType PrizedManager_t3323701717_0_0_0;
static const Il2CppType* GenInst_PrizedManager_t3323701717_0_0_0_Types[] = { &PrizedManager_t3323701717_0_0_0 };
extern const Il2CppGenericInst GenInst_PrizedManager_t3323701717_0_0_0 = { 1, GenInst_PrizedManager_t3323701717_0_0_0_Types };
extern const Il2CppType APIManager_t2595496257_0_0_0;
static const Il2CppType* GenInst_APIManager_t2595496257_0_0_0_Types[] = { &APIManager_t2595496257_0_0_0 };
extern const Il2CppGenericInst GenInst_APIManager_t2595496257_0_0_0 = { 1, GenInst_APIManager_t2595496257_0_0_0_Types };
extern const Il2CppType WWWManager_t1701787038_0_0_0;
static const Il2CppType* GenInst_WWWManager_t1701787038_0_0_0_Types[] = { &WWWManager_t1701787038_0_0_0 };
extern const Il2CppGenericInst GenInst_WWWManager_t1701787038_0_0_0 = { 1, GenInst_WWWManager_t1701787038_0_0_0_Types };
extern const Il2CppType ApiSearchFriendManager_t2339757037_0_0_0;
static const Il2CppType* GenInst_ApiSearchFriendManager_t2339757037_0_0_0_Types[] = { &ApiSearchFriendManager_t2339757037_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiSearchFriendManager_t2339757037_0_0_0 = { 1, GenInst_ApiSearchFriendManager_t2339757037_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ApiSearchHistory_t3591623188_0_0_0;
static const Il2CppType* GenInst_ApiSearchHistory_t3591623188_0_0_0_Types[] = { &ApiSearchHistory_t3591623188_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiSearchHistory_t3591623188_0_0_0 = { 1, GenInst_ApiSearchHistory_t3591623188_0_0_0_Types };
extern const Il2CppType ApiShowSearchHistory_t2915241013_0_0_0;
static const Il2CppType* GenInst_ApiShowSearchHistory_t2915241013_0_0_0_Types[] = { &ApiShowSearchHistory_t2915241013_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiShowSearchHistory_t2915241013_0_0_0 = { 1, GenInst_ApiShowSearchHistory_t2915241013_0_0_0_Types };
extern const Il2CppType MyPageManager_t2103198340_0_0_0;
static const Il2CppType* GenInst_MyPageManager_t2103198340_0_0_0_Types[] = { &MyPageManager_t2103198340_0_0_0 };
extern const Il2CppGenericInst GenInst_MyPageManager_t2103198340_0_0_0 = { 1, GenInst_MyPageManager_t2103198340_0_0_0_Types };
extern const Il2CppType ApiAuthenticationsManager_t1221451852_0_0_0;
static const Il2CppType* GenInst_ApiAuthenticationsManager_t1221451852_0_0_0_Types[] = { &ApiAuthenticationsManager_t1221451852_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiAuthenticationsManager_t1221451852_0_0_0 = { 1, GenInst_ApiAuthenticationsManager_t1221451852_0_0_0_Types };
extern const Il2CppType ApiChampionManager_t192998666_0_0_0;
static const Il2CppType* GenInst_ApiChampionManager_t192998666_0_0_0_Types[] = { &ApiChampionManager_t192998666_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiChampionManager_t192998666_0_0_0 = { 1, GenInst_ApiChampionManager_t192998666_0_0_0_Types };
extern const Il2CppType ColorManager_t1666568646_0_0_0;
static const Il2CppType* GenInst_ColorManager_t1666568646_0_0_0_Types[] = { &ColorManager_t1666568646_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorManager_t1666568646_0_0_0 = { 1, GenInst_ColorManager_t1666568646_0_0_0_Types };
extern const Il2CppType ChampionItemModel_t975870399_0_0_0;
static const Il2CppType* GenInst_ChampionItemModel_t975870399_0_0_0_Types[] = { &ChampionItemModel_t975870399_0_0_0 };
extern const Il2CppGenericInst GenInst_ChampionItemModel_t975870399_0_0_0 = { 1, GenInst_ChampionItemModel_t975870399_0_0_0_Types };
extern const Il2CppType GetChampionMobilityChildPartsListModel_t1181557729_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityChildPartsListModel_t1181557729_0_0_0_Types[] = { &GetChampionMobilityChildPartsListModel_t1181557729_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityChildPartsListModel_t1181557729_0_0_0 = { 1, GenInst_GetChampionMobilityChildPartsListModel_t1181557729_0_0_0_Types };
extern const Il2CppType Joint_t826707408_0_0_0;
static const Il2CppType* GenInst_Joint_t826707408_0_0_0_Types[] = { &Joint_t826707408_0_0_0 };
extern const Il2CppGenericInst GenInst_Joint_t826707408_0_0_0 = { 1, GenInst_Joint_t826707408_0_0_0_Types };
extern const Il2CppType ApiRaderManager_t2058631757_0_0_0;
static const Il2CppType* GenInst_ApiRaderManager_t2058631757_0_0_0_Types[] = { &ApiRaderManager_t2058631757_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiRaderManager_t2058631757_0_0_0 = { 1, GenInst_ApiRaderManager_t2058631757_0_0_0_Types };
extern const Il2CppType MobilmoManager_t1293766190_0_0_0;
static const Il2CppType* GenInst_MobilmoManager_t1293766190_0_0_0_Types[] = { &MobilmoManager_t1293766190_0_0_0 };
extern const Il2CppGenericInst GenInst_MobilmoManager_t1293766190_0_0_0 = { 1, GenInst_MobilmoManager_t1293766190_0_0_0_Types };
extern const Il2CppType ModuleManager_t1065445307_0_0_0;
static const Il2CppType* GenInst_ModuleManager_t1065445307_0_0_0_Types[] = { &ModuleManager_t1065445307_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleManager_t1065445307_0_0_0 = { 1, GenInst_ModuleManager_t1065445307_0_0_0_Types };
extern const Il2CppType ApiModuleManager_t3939700127_0_0_0;
static const Il2CppType* GenInst_ApiModuleManager_t3939700127_0_0_0_Types[] = { &ApiModuleManager_t3939700127_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiModuleManager_t3939700127_0_0_0 = { 1, GenInst_ApiModuleManager_t3939700127_0_0_0_Types };
extern const Il2CppType MiniGameManager_t849864952_0_0_0;
static const Il2CppType* GenInst_MiniGameManager_t849864952_0_0_0_Types[] = { &MiniGameManager_t849864952_0_0_0 };
extern const Il2CppGenericInst GenInst_MiniGameManager_t849864952_0_0_0 = { 1, GenInst_MiniGameManager_t849864952_0_0_0_Types };
extern const Il2CppType ApiComponentManager_t1194997966_0_0_0;
static const Il2CppType* GenInst_ApiComponentManager_t1194997966_0_0_0_Types[] = { &ApiComponentManager_t1194997966_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiComponentManager_t1194997966_0_0_0 = { 1, GenInst_ApiComponentManager_t1194997966_0_0_0_Types };
extern const Il2CppType ApiContactManager_t3961479189_0_0_0;
static const Il2CppType* GenInst_ApiContactManager_t3961479189_0_0_0_Types[] = { &ApiContactManager_t3961479189_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiContactManager_t3961479189_0_0_0 = { 1, GenInst_ApiContactManager_t3961479189_0_0_0_Types };
extern const Il2CppType LocalizeManager_t1264687742_0_0_0;
static const Il2CppType* GenInst_LocalizeManager_t1264687742_0_0_0_Types[] = { &LocalizeManager_t1264687742_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeManager_t1264687742_0_0_0 = { 1, GenInst_LocalizeManager_t1264687742_0_0_0_Types };
extern const Il2CppType ApiDnaManager_t2705918952_0_0_0;
static const Il2CppType* GenInst_ApiDnaManager_t2705918952_0_0_0_Types[] = { &ApiDnaManager_t2705918952_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiDnaManager_t2705918952_0_0_0 = { 1, GenInst_ApiDnaManager_t2705918952_0_0_0_Types };
extern const Il2CppType GetDnaChildrenListModel_t2978945125_0_0_0;
static const Il2CppType* GenInst_GetDnaChildrenListModel_t2978945125_0_0_0_Types[] = { &GetDnaChildrenListModel_t2978945125_0_0_0 };
extern const Il2CppGenericInst GenInst_GetDnaChildrenListModel_t2978945125_0_0_0 = { 1, GenInst_GetDnaChildrenListModel_t2978945125_0_0_0_Types };
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0 = { 1, GenInst_ILayoutElement_t1975293769_0_0_0_Types };
extern const Il2CppType MaskableGraphic_t540192618_0_0_0;
static const Il2CppType* GenInst_MaskableGraphic_t540192618_0_0_0_Types[] = { &MaskableGraphic_t540192618_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t540192618_0_0_0 = { 1, GenInst_MaskableGraphic_t540192618_0_0_0_Types };
extern const Il2CppType IMaskable_t1431842707_0_0_0;
static const Il2CppType* GenInst_IMaskable_t1431842707_0_0_0_Types[] = { &IMaskable_t1431842707_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaskable_t1431842707_0_0_0 = { 1, GenInst_IMaskable_t1431842707_0_0_0_Types };
extern const Il2CppType IMaterialModifier_t3028564983_0_0_0;
static const Il2CppType* GenInst_IMaterialModifier_t3028564983_0_0_0_Types[] = { &IMaterialModifier_t3028564983_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t3028564983_0_0_0 = { 1, GenInst_IMaterialModifier_t3028564983_0_0_0_Types };
extern const Il2CppType ApiEventManager_t2080410209_0_0_0;
static const Il2CppType* GenInst_ApiEventManager_t2080410209_0_0_0_Types[] = { &ApiEventManager_t2080410209_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiEventManager_t2080410209_0_0_0 = { 1, GenInst_ApiEventManager_t2080410209_0_0_0_Types };
extern const Il2CppType ApiUserManager_t2556825810_0_0_0;
static const Il2CppType* GenInst_ApiUserManager_t2556825810_0_0_0_Types[] = { &ApiUserManager_t2556825810_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiUserManager_t2556825810_0_0_0 = { 1, GenInst_ApiUserManager_t2556825810_0_0_0_Types };
extern const Il2CppType ApiMobilityManager_t1284346316_0_0_0;
static const Il2CppType* GenInst_ApiMobilityManager_t1284346316_0_0_0_Types[] = { &ApiMobilityManager_t1284346316_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiMobilityManager_t1284346316_0_0_0 = { 1, GenInst_ApiMobilityManager_t1284346316_0_0_0_Types };
extern const Il2CppType GetChildPartsList_t2416430853_0_0_0;
static const Il2CppType* GenInst_GetChildPartsList_t2416430853_0_0_0_Types[] = { &GetChildPartsList_t2416430853_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChildPartsList_t2416430853_0_0_0 = { 1, GenInst_GetChildPartsList_t2416430853_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_Types[] = { &String_t_0_0_0, &GetChildPartsList_t2416430853_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0 = { 2, GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GetChildPartsList_t2416430853_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2088555337_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_KeyValuePair_2_t2088555337_0_0_0_Types[] = { &String_t_0_0_0, &GetChildPartsList_t2416430853_0_0_0, &KeyValuePair_2_t2088555337_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_KeyValuePair_2_t2088555337_0_0_0 = { 3, GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_KeyValuePair_2_t2088555337_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2088555337_0_0_0_Types[] = { &KeyValuePair_2_t2088555337_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2088555337_0_0_0 = { 1, GenInst_KeyValuePair_2_t2088555337_0_0_0_Types };
extern const Il2CppType AnalyticsManager_t1593654123_0_0_0;
static const Il2CppType* GenInst_AnalyticsManager_t1593654123_0_0_0_Types[] = { &AnalyticsManager_t1593654123_0_0_0 };
extern const Il2CppGenericInst GenInst_AnalyticsManager_t1593654123_0_0_0 = { 1, GenInst_AnalyticsManager_t1593654123_0_0_0_Types };
extern const Il2CppType AudioManager_t4222704959_0_0_0;
static const Il2CppType* GenInst_AudioManager_t4222704959_0_0_0_Types[] = { &AudioManager_t4222704959_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioManager_t4222704959_0_0_0 = { 1, GenInst_AudioManager_t4222704959_0_0_0_Types };
extern const Il2CppType RecordingManager_t3075979958_0_0_0;
static const Il2CppType* GenInst_RecordingManager_t3075979958_0_0_0_Types[] = { &RecordingManager_t3075979958_0_0_0 };
extern const Il2CppGenericInst GenInst_RecordingManager_t3075979958_0_0_0 = { 1, GenInst_RecordingManager_t3075979958_0_0_0_Types };
extern const Il2CppType CreateManager_t3918627545_0_0_0;
static const Il2CppType* GenInst_CreateManager_t3918627545_0_0_0_Types[] = { &CreateManager_t3918627545_0_0_0 };
extern const Il2CppGenericInst GenInst_CreateManager_t3918627545_0_0_0 = { 1, GenInst_CreateManager_t3918627545_0_0_0_Types };
extern const Il2CppType TutorialManager_t2168024773_0_0_0;
static const Il2CppType* GenInst_TutorialManager_t2168024773_0_0_0_Types[] = { &TutorialManager_t2168024773_0_0_0 };
extern const Il2CppGenericInst GenInst_TutorialManager_t2168024773_0_0_0 = { 1, GenInst_TutorialManager_t2168024773_0_0_0_Types };
extern const Il2CppType MotionManager_t3855993783_0_0_0;
static const Il2CppType* GenInst_MotionManager_t3855993783_0_0_0_Types[] = { &MotionManager_t3855993783_0_0_0 };
extern const Il2CppGenericInst GenInst_MotionManager_t3855993783_0_0_0 = { 1, GenInst_MotionManager_t3855993783_0_0_0_Types };
extern const Il2CppType GetModuleChildPartsList_t3198568368_0_0_0;
static const Il2CppType* GenInst_GetModuleChildPartsList_t3198568368_0_0_0_Types[] = { &GetModuleChildPartsList_t3198568368_0_0_0 };
extern const Il2CppGenericInst GenInst_GetModuleChildPartsList_t3198568368_0_0_0 = { 1, GenInst_GetModuleChildPartsList_t3198568368_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_Types[] = { &String_t_0_0_0, &GetModuleChildPartsList_t3198568368_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0 = { 2, GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GetModuleChildPartsList_t3198568368_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2870692852_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_KeyValuePair_2_t2870692852_0_0_0_Types[] = { &String_t_0_0_0, &GetModuleChildPartsList_t3198568368_0_0_0, &KeyValuePair_2_t2870692852_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_KeyValuePair_2_t2870692852_0_0_0 = { 3, GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_KeyValuePair_2_t2870692852_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2870692852_0_0_0_Types[] = { &KeyValuePair_2_t2870692852_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2870692852_0_0_0 = { 1, GenInst_KeyValuePair_2_t2870692852_0_0_0_Types };
extern const Il2CppType ApiModuleMotionManager_t2960242393_0_0_0;
static const Il2CppType* GenInst_ApiModuleMotionManager_t2960242393_0_0_0_Types[] = { &ApiModuleMotionManager_t2960242393_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiModuleMotionManager_t2960242393_0_0_0 = { 1, GenInst_ApiModuleMotionManager_t2960242393_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1428657631_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1428657631_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t1756533147_0_0_0, &KeyValuePair_2_t1428657631_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1428657631_0_0_0 = { 3, GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1428657631_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1428657631_0_0_0_Types[] = { &KeyValuePair_2_t1428657631_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1428657631_0_0_0 = { 1, GenInst_KeyValuePair_2_t1428657631_0_0_0_Types };
extern const Il2CppType AnimationCurve_t3306541151_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_Types[] = { &String_t_0_0_0, &AnimationCurve_t3306541151_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0 = { 2, GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &AnimationCurve_t3306541151_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AnimationCurve_t3306541151_0_0_0_Types[] = { &AnimationCurve_t3306541151_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationCurve_t3306541151_0_0_0 = { 1, GenInst_AnimationCurve_t3306541151_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2978665635_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_KeyValuePair_2_t2978665635_0_0_0_Types[] = { &String_t_0_0_0, &AnimationCurve_t3306541151_0_0_0, &KeyValuePair_2_t2978665635_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_KeyValuePair_2_t2978665635_0_0_0 = { 3, GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_KeyValuePair_2_t2978665635_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2978665635_0_0_0_Types[] = { &KeyValuePair_2_t2978665635_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2978665635_0_0_0 = { 1, GenInst_KeyValuePair_2_t2978665635_0_0_0_Types };
extern const Il2CppType AnimationClip_t3510324950_0_0_0;
static const Il2CppType* GenInst_AnimationClip_t3510324950_0_0_0_Types[] = { &AnimationClip_t3510324950_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationClip_t3510324950_0_0_0 = { 1, GenInst_AnimationClip_t3510324950_0_0_0_Types };
extern const Il2CppType Motion_t2415020824_0_0_0;
static const Il2CppType* GenInst_Motion_t2415020824_0_0_0_Types[] = { &Motion_t2415020824_0_0_0 };
extern const Il2CppGenericInst GenInst_Motion_t2415020824_0_0_0 = { 1, GenInst_Motion_t2415020824_0_0_0_Types };
extern const Il2CppType GetModuleMotionsData_t125704211_0_0_0;
static const Il2CppType* GenInst_GetModuleMotionsData_t125704211_0_0_0_Types[] = { &GetModuleMotionsData_t125704211_0_0_0 };
extern const Il2CppGenericInst GenInst_GetModuleMotionsData_t125704211_0_0_0 = { 1, GenInst_GetModuleMotionsData_t125704211_0_0_0_Types };
extern const Il2CppType GetModulePoseData_t1121789947_0_0_0;
static const Il2CppType* GenInst_GetModulePoseData_t1121789947_0_0_0_Types[] = { &GetModulePoseData_t1121789947_0_0_0 };
extern const Il2CppGenericInst GenInst_GetModulePoseData_t1121789947_0_0_0 = { 1, GenInst_GetModulePoseData_t1121789947_0_0_0_Types };
extern const Il2CppType GetModuleMotionItemData_t3026021381_0_0_0;
static const Il2CppType* GenInst_GetModuleMotionItemData_t3026021381_0_0_0_Types[] = { &GetModuleMotionItemData_t3026021381_0_0_0 };
extern const Il2CppGenericInst GenInst_GetModuleMotionItemData_t3026021381_0_0_0 = { 1, GenInst_GetModuleMotionItemData_t3026021381_0_0_0_Types };
extern const Il2CppType GetMotionModel_t2157978365_0_0_0;
static const Il2CppType* GenInst_GetMotionModel_t2157978365_0_0_0_Types[] = { &GetMotionModel_t2157978365_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMotionModel_t2157978365_0_0_0 = { 1, GenInst_GetMotionModel_t2157978365_0_0_0_Types };
extern const Il2CppType GetMotionDataModel_t3220649645_0_0_0;
static const Il2CppType* GenInst_GetMotionDataModel_t3220649645_0_0_0_Types[] = { &GetMotionDataModel_t3220649645_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMotionDataModel_t3220649645_0_0_0 = { 1, GenInst_GetMotionDataModel_t3220649645_0_0_0_Types };
extern const Il2CppType GetPoseDataModel_t3123973518_0_0_0;
static const Il2CppType* GenInst_GetPoseDataModel_t3123973518_0_0_0_Types[] = { &GetPoseDataModel_t3123973518_0_0_0 };
extern const Il2CppGenericInst GenInst_GetPoseDataModel_t3123973518_0_0_0 = { 1, GenInst_GetPoseDataModel_t3123973518_0_0_0_Types };
extern const Il2CppType ApiNearmobManager_t3144099613_0_0_0;
static const Il2CppType* GenInst_ApiNearmobManager_t3144099613_0_0_0_Types[] = { &ApiNearmobManager_t3144099613_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiNearmobManager_t3144099613_0_0_0 = { 1, GenInst_ApiNearmobManager_t3144099613_0_0_0_Types };
extern const Il2CppType ApiNoticeManager_t3224317815_0_0_0;
static const Il2CppType* GenInst_ApiNoticeManager_t3224317815_0_0_0_Types[] = { &ApiNoticeManager_t3224317815_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiNoticeManager_t3224317815_0_0_0 = { 1, GenInst_ApiNoticeManager_t3224317815_0_0_0_Types };
extern const Il2CppType NoticeListModel_t1317032645_0_0_0;
static const Il2CppType* GenInst_NoticeListModel_t1317032645_0_0_0_Types[] = { &NoticeListModel_t1317032645_0_0_0 };
extern const Il2CppGenericInst GenInst_NoticeListModel_t1317032645_0_0_0 = { 1, GenInst_NoticeListModel_t1317032645_0_0_0_Types };
extern const Il2CppType GetNoticeDetail_t155866005_0_0_0;
static const Il2CppType* GenInst_GetNoticeDetail_t155866005_0_0_0_Types[] = { &GetNoticeDetail_t155866005_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNoticeDetail_t155866005_0_0_0 = { 1, GenInst_GetNoticeDetail_t155866005_0_0_0_Types };
extern const Il2CppType ApiPrizedManager_t4212047241_0_0_0;
static const Il2CppType* GenInst_ApiPrizedManager_t4212047241_0_0_0_Types[] = { &ApiPrizedManager_t4212047241_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiPrizedManager_t4212047241_0_0_0 = { 1, GenInst_ApiPrizedManager_t4212047241_0_0_0_Types };
extern const Il2CppType ApiReactionManager_t2857272874_0_0_0;
static const Il2CppType* GenInst_ApiReactionManager_t2857272874_0_0_0_Types[] = { &ApiReactionManager_t2857272874_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiReactionManager_t2857272874_0_0_0 = { 1, GenInst_ApiReactionManager_t2857272874_0_0_0_Types };
extern const Il2CppType GetReactionList_t3285480050_0_0_0;
static const Il2CppType* GenInst_GetReactionList_t3285480050_0_0_0_Types[] = { &GetReactionList_t3285480050_0_0_0 };
extern const Il2CppGenericInst GenInst_GetReactionList_t3285480050_0_0_0 = { 1, GenInst_GetReactionList_t3285480050_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_Types[] = { &String_t_0_0_0, &GetReactionList_t3285480050_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0 = { 2, GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GetReactionList_t3285480050_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2957604534_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_KeyValuePair_2_t2957604534_0_0_0_Types[] = { &String_t_0_0_0, &GetReactionList_t3285480050_0_0_0, &KeyValuePair_2_t2957604534_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_KeyValuePair_2_t2957604534_0_0_0 = { 3, GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_KeyValuePair_2_t2957604534_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2957604534_0_0_0_Types[] = { &KeyValuePair_2_t2957604534_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2957604534_0_0_0 = { 1, GenInst_KeyValuePair_2_t2957604534_0_0_0_Types };
extern const Il2CppType ApiTokenManager_t4213246208_0_0_0;
static const Il2CppType* GenInst_ApiTokenManager_t4213246208_0_0_0_Types[] = { &ApiTokenManager_t4213246208_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiTokenManager_t4213246208_0_0_0 = { 1, GenInst_ApiTokenManager_t4213246208_0_0_0_Types };
extern const Il2CppType GetUserUpdateJson_t2263613893_0_0_0;
static const Il2CppType* GenInst_GetUserUpdateJson_t2263613893_0_0_0_Types[] = { &GetUserUpdateJson_t2263613893_0_0_0 };
extern const Il2CppGenericInst GenInst_GetUserUpdateJson_t2263613893_0_0_0 = { 1, GenInst_GetUserUpdateJson_t2263613893_0_0_0_Types };
extern const Il2CppType CountryData_t1135367575_0_0_0;
static const Il2CppType* GenInst_CountryData_t1135367575_0_0_0_Types[] = { &CountryData_t1135367575_0_0_0 };
extern const Il2CppGenericInst GenInst_CountryData_t1135367575_0_0_0 = { 1, GenInst_CountryData_t1135367575_0_0_0_Types };
extern const Il2CppType LoadAssetBundle_t379001212_0_0_0;
static const Il2CppType* GenInst_LoadAssetBundle_t379001212_0_0_0_Types[] = { &LoadAssetBundle_t379001212_0_0_0 };
extern const Il2CppGenericInst GenInst_LoadAssetBundle_t379001212_0_0_0 = { 1, GenInst_LoadAssetBundle_t379001212_0_0_0_Types };
extern const Il2CppType CountriesModel_t400828943_0_0_0;
static const Il2CppType* GenInst_CountriesModel_t400828943_0_0_0_Types[] = { &CountriesModel_t400828943_0_0_0 };
extern const Il2CppGenericInst GenInst_CountriesModel_t400828943_0_0_0 = { 1, GenInst_CountriesModel_t400828943_0_0_0_Types };
extern const Il2CppType ApiMigrate_t2586426963_0_0_0;
static const Il2CppType* GenInst_ApiMigrate_t2586426963_0_0_0_Types[] = { &ApiMigrate_t2586426963_0_0_0 };
extern const Il2CppGenericInst GenInst_ApiMigrate_t2586426963_0_0_0 = { 1, GenInst_ApiMigrate_t2586426963_0_0_0_Types };
extern const Il2CppType recordingMotionData_t1704213756_0_0_0;
static const Il2CppType* GenInst_recordingMotionData_t1704213756_0_0_0_Types[] = { &recordingMotionData_t1704213756_0_0_0 };
extern const Il2CppGenericInst GenInst_recordingMotionData_t1704213756_0_0_0 = { 1, GenInst_recordingMotionData_t1704213756_0_0_0_Types };
extern const Il2CppType GetMotionsData_t2417923611_0_0_0;
static const Il2CppType* GenInst_GetMotionsData_t2417923611_0_0_0_Types[] = { &GetMotionsData_t2417923611_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMotionsData_t2417923611_0_0_0 = { 1, GenInst_GetMotionsData_t2417923611_0_0_0_Types };
extern const Il2CppType GetPoseData_t2785975099_0_0_0;
static const Il2CppType* GenInst_GetPoseData_t2785975099_0_0_0_Types[] = { &GetPoseData_t2785975099_0_0_0 };
extern const Il2CppGenericInst GenInst_GetPoseData_t2785975099_0_0_0 = { 1, GenInst_GetPoseData_t2785975099_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Module_t3140434828_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Module_t3140434828_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType GetItemsData_t1181624410_0_0_0;
static const Il2CppType* GenInst_GetItemsData_t1181624410_0_0_0_Types[] = { &GetItemsData_t1181624410_0_0_0 };
extern const Il2CppGenericInst GenInst_GetItemsData_t1181624410_0_0_0 = { 1, GenInst_GetItemsData_t1181624410_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4200572981_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_KeyValuePair_2_t4200572981_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Module_t3140434828_0_0_0, &KeyValuePair_2_t4200572981_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_KeyValuePair_2_t4200572981_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_KeyValuePair_2_t4200572981_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4200572981_0_0_0_Types[] = { &KeyValuePair_2_t4200572981_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4200572981_0_0_0 = { 1, GenInst_KeyValuePair_2_t4200572981_0_0_0_Types };
extern const Il2CppType GetMotionsDataInWorldActionSetting_t2072611844_0_0_0;
static const Il2CppType* GenInst_GetMotionsDataInWorldActionSetting_t2072611844_0_0_0_Types[] = { &GetMotionsDataInWorldActionSetting_t2072611844_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMotionsDataInWorldActionSetting_t2072611844_0_0_0 = { 1, GenInst_GetMotionsDataInWorldActionSetting_t2072611844_0_0_0_Types };
extern const Il2CppType GetPoseDataInWorldActionSetting_t1846296182_0_0_0;
static const Il2CppType* GenInst_GetPoseDataInWorldActionSetting_t1846296182_0_0_0_Types[] = { &GetPoseDataInWorldActionSetting_t1846296182_0_0_0 };
extern const Il2CppGenericInst GenInst_GetPoseDataInWorldActionSetting_t1846296182_0_0_0 = { 1, GenInst_GetPoseDataInWorldActionSetting_t1846296182_0_0_0_Types };
extern const Il2CppType GetItemsDataInWorldActionSetting_t1654137477_0_0_0;
static const Il2CppType* GenInst_GetItemsDataInWorldActionSetting_t1654137477_0_0_0_Types[] = { &GetItemsDataInWorldActionSetting_t1654137477_0_0_0 };
extern const Il2CppGenericInst GenInst_GetItemsDataInWorldActionSetting_t1654137477_0_0_0 = { 1, GenInst_GetItemsDataInWorldActionSetting_t1654137477_0_0_0_Types };
extern const Il2CppType ReactionList_t1054868761_0_0_0;
static const Il2CppType* GenInst_ReactionList_t1054868761_0_0_0_Types[] = { &ReactionList_t1054868761_0_0_0 };
extern const Il2CppGenericInst GenInst_ReactionList_t1054868761_0_0_0 = { 1, GenInst_ReactionList_t1054868761_0_0_0_Types };
extern const Il2CppType MyPageMobilmoItem_t346175087_0_0_0;
static const Il2CppType* GenInst_MyPageMobilmoItem_t346175087_0_0_0_Types[] = { &MyPageMobilmoItem_t346175087_0_0_0 };
extern const Il2CppGenericInst GenInst_MyPageMobilmoItem_t346175087_0_0_0 = { 1, GenInst_MyPageMobilmoItem_t346175087_0_0_0_Types };
extern const Il2CppType MypageMenuItem_t3200819439_0_0_0;
static const Il2CppType* GenInst_MypageMenuItem_t3200819439_0_0_0_Types[] = { &MypageMenuItem_t3200819439_0_0_0 };
extern const Il2CppGenericInst GenInst_MypageMenuItem_t3200819439_0_0_0 = { 1, GenInst_MypageMenuItem_t3200819439_0_0_0_Types };
extern const Il2CppType MyPageFloor_t1585319381_0_0_0;
static const Il2CppType* GenInst_MyPageFloor_t1585319381_0_0_0_Types[] = { &MyPageFloor_t1585319381_0_0_0 };
extern const Il2CppGenericInst GenInst_MyPageFloor_t1585319381_0_0_0 = { 1, GenInst_MyPageFloor_t1585319381_0_0_0_Types };
extern const Il2CppType GetRecordingNewMotions_t4070061385_0_0_0;
static const Il2CppType* GenInst_GetRecordingNewMotions_t4070061385_0_0_0_Types[] = { &GetRecordingNewMotions_t4070061385_0_0_0 };
extern const Il2CppGenericInst GenInst_GetRecordingNewMotions_t4070061385_0_0_0 = { 1, GenInst_GetRecordingNewMotions_t4070061385_0_0_0_Types };
extern const Il2CppType GetItemsData_t174665839_0_0_0;
static const Il2CppType* GenInst_GetItemsData_t174665839_0_0_0_Types[] = { &GetItemsData_t174665839_0_0_0 };
extern const Il2CppGenericInst GenInst_GetItemsData_t174665839_0_0_0 = { 1, GenInst_GetItemsData_t174665839_0_0_0_Types };
extern const Il2CppType GetMobilityChildPartsListModel_t2691836378_0_0_0;
static const Il2CppType* GenInst_GetMobilityChildPartsListModel_t2691836378_0_0_0_Types[] = { &GetMobilityChildPartsListModel_t2691836378_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityChildPartsListModel_t2691836378_0_0_0 = { 1, GenInst_GetMobilityChildPartsListModel_t2691836378_0_0_0_Types };
extern const Il2CppType ReactionHistory_t2538361761_0_0_0;
static const Il2CppType* GenInst_ReactionHistory_t2538361761_0_0_0_Types[] = { &ReactionHistory_t2538361761_0_0_0 };
extern const Il2CppGenericInst GenInst_ReactionHistory_t2538361761_0_0_0 = { 1, GenInst_ReactionHistory_t2538361761_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t309261261_0_0_0_Types[] = { &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t309261261_0_0_0 = { 1, GenInst_Dictionary_2_t309261261_0_0_0_Types };
extern const Il2CppType GetChampionMobilityEncountListModel_t3804924867_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityEncountListModel_t3804924867_0_0_0_Types[] = { &GetChampionMobilityEncountListModel_t3804924867_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityEncountListModel_t3804924867_0_0_0 = { 1, GenInst_GetChampionMobilityEncountListModel_t3804924867_0_0_0_Types };
extern const Il2CppType GetChampionMobilityRegisterdMotionListModel_t1560983880_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityRegisterdMotionListModel_t1560983880_0_0_0_Types[] = { &GetChampionMobilityRegisterdMotionListModel_t1560983880_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityRegisterdMotionListModel_t1560983880_0_0_0 = { 1, GenInst_GetChampionMobilityRegisterdMotionListModel_t1560983880_0_0_0_Types };
extern const Il2CppType GetChampionMobilityRecordedMotionList_t1893561664_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityRecordedMotionList_t1893561664_0_0_0_Types[] = { &GetChampionMobilityRecordedMotionList_t1893561664_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityRecordedMotionList_t1893561664_0_0_0 = { 1, GenInst_GetChampionMobilityRecordedMotionList_t1893561664_0_0_0_Types };
extern const Il2CppType GetChampionMobilityReactionsModel_t283662167_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityReactionsModel_t283662167_0_0_0_Types[] = { &GetChampionMobilityReactionsModel_t283662167_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityReactionsModel_t283662167_0_0_0 = { 1, GenInst_GetChampionMobilityReactionsModel_t283662167_0_0_0_Types };
extern const Il2CppType GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608_0_0_0_Types[] = { &GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608_0_0_0 = { 1, GenInst_GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608_0_0_0_Types };
extern const Il2CppType GetChampionMobilityRegisterMotionListPoseData_t3691775360_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityRegisterMotionListPoseData_t3691775360_0_0_0_Types[] = { &GetChampionMobilityRegisterMotionListPoseData_t3691775360_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityRegisterMotionListPoseData_t3691775360_0_0_0 = { 1, GenInst_GetChampionMobilityRegisterMotionListPoseData_t3691775360_0_0_0_Types };
extern const Il2CppType GetChampionMobilityRecordedMotionListMotions_t3064563391_0_0_0;
static const Il2CppType* GenInst_GetChampionMobilityRecordedMotionListMotions_t3064563391_0_0_0_Types[] = { &GetChampionMobilityRecordedMotionListMotions_t3064563391_0_0_0 };
extern const Il2CppGenericInst GenInst_GetChampionMobilityRecordedMotionListMotions_t3064563391_0_0_0 = { 1, GenInst_GetChampionMobilityRecordedMotionListMotions_t3064563391_0_0_0_Types };
extern const Il2CppType GetComponentModel_t1600826744_0_0_0;
static const Il2CppType* GenInst_GetComponentModel_t1600826744_0_0_0_Types[] = { &GetComponentModel_t1600826744_0_0_0 };
extern const Il2CppGenericInst GenInst_GetComponentModel_t1600826744_0_0_0 = { 1, GenInst_GetComponentModel_t1600826744_0_0_0_Types };
extern const Il2CppType GetMobilityItemsReactionItemsList_t2580483176_0_0_0;
static const Il2CppType* GenInst_GetMobilityItemsReactionItemsList_t2580483176_0_0_0_Types[] = { &GetMobilityItemsReactionItemsList_t2580483176_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityItemsReactionItemsList_t2580483176_0_0_0 = { 1, GenInst_GetMobilityItemsReactionItemsList_t2580483176_0_0_0_Types };
extern const Il2CppType GetMobilityEncountList_t1045358299_0_0_0;
static const Il2CppType* GenInst_GetMobilityEncountList_t1045358299_0_0_0_Types[] = { &GetMobilityEncountList_t1045358299_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityEncountList_t1045358299_0_0_0 = { 1, GenInst_GetMobilityEncountList_t1045358299_0_0_0_Types };
extern const Il2CppType GetMobilityRegisterdMotionList_t4139392582_0_0_0;
static const Il2CppType* GenInst_GetMobilityRegisterdMotionList_t4139392582_0_0_0_Types[] = { &GetMobilityRegisterdMotionList_t4139392582_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityRegisterdMotionList_t4139392582_0_0_0 = { 1, GenInst_GetMobilityRegisterdMotionList_t4139392582_0_0_0_Types };
extern const Il2CppType GetMobilityRecordedMotionList_t2169087681_0_0_0;
static const Il2CppType* GenInst_GetMobilityRecordedMotionList_t2169087681_0_0_0_Types[] = { &GetMobilityRecordedMotionList_t2169087681_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityRecordedMotionList_t2169087681_0_0_0 = { 1, GenInst_GetMobilityRecordedMotionList_t2169087681_0_0_0_Types };
extern const Il2CppType GetMobilityRegisterMotionListMotionData_t2404558376_0_0_0;
static const Il2CppType* GenInst_GetMobilityRegisterMotionListMotionData_t2404558376_0_0_0_Types[] = { &GetMobilityRegisterMotionListMotionData_t2404558376_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityRegisterMotionListMotionData_t2404558376_0_0_0 = { 1, GenInst_GetMobilityRegisterMotionListMotionData_t2404558376_0_0_0_Types };
extern const Il2CppType GetMobilityRegisterMotionListPoseData_t2674609285_0_0_0;
static const Il2CppType* GenInst_GetMobilityRegisterMotionListPoseData_t2674609285_0_0_0_Types[] = { &GetMobilityRegisterMotionListPoseData_t2674609285_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityRegisterMotionListPoseData_t2674609285_0_0_0 = { 1, GenInst_GetMobilityRegisterMotionListPoseData_t2674609285_0_0_0_Types };
extern const Il2CppType GetMobilityRecordedMotionListMotions_t4271290930_0_0_0;
static const Il2CppType* GenInst_GetMobilityRecordedMotionListMotions_t4271290930_0_0_0_Types[] = { &GetMobilityRecordedMotionListMotions_t4271290930_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMobilityRecordedMotionListMotions_t4271290930_0_0_0 = { 1, GenInst_GetMobilityRecordedMotionListMotions_t4271290930_0_0_0_Types };
extern const Il2CppType GetModuleChildPartsListModel_t3559781865_0_0_0;
static const Il2CppType* GenInst_GetModuleChildPartsListModel_t3559781865_0_0_0_Types[] = { &GetModuleChildPartsListModel_t3559781865_0_0_0 };
extern const Il2CppGenericInst GenInst_GetModuleChildPartsListModel_t3559781865_0_0_0 = { 1, GenInst_GetModuleChildPartsListModel_t3559781865_0_0_0_Types };
extern const Il2CppType GetModuleRegisterMotionListMotionData_t2309758741_0_0_0;
static const Il2CppType* GenInst_GetModuleRegisterMotionListMotionData_t2309758741_0_0_0_Types[] = { &GetModuleRegisterMotionListMotionData_t2309758741_0_0_0 };
extern const Il2CppGenericInst GenInst_GetModuleRegisterMotionListMotionData_t2309758741_0_0_0 = { 1, GenInst_GetModuleRegisterMotionListMotionData_t2309758741_0_0_0_Types };
extern const Il2CppType GetModuleRegisterMotionListPoseData_t154016370_0_0_0;
static const Il2CppType* GenInst_GetModuleRegisterMotionListPoseData_t154016370_0_0_0_Types[] = { &GetModuleRegisterMotionListPoseData_t154016370_0_0_0 };
extern const Il2CppGenericInst GenInst_GetModuleRegisterMotionListPoseData_t154016370_0_0_0 = { 1, GenInst_GetModuleRegisterMotionListPoseData_t154016370_0_0_0_Types };
extern const Il2CppType MyMobilityItemsModel_t400881186_0_0_0;
static const Il2CppType* GenInst_MyMobilityItemsModel_t400881186_0_0_0_Types[] = { &MyMobilityItemsModel_t400881186_0_0_0 };
extern const Il2CppGenericInst GenInst_MyMobilityItemsModel_t400881186_0_0_0 = { 1, GenInst_MyMobilityItemsModel_t400881186_0_0_0_Types };
extern const Il2CppType GetMyMobilityItemsReactionItemsList_t157728542_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityItemsReactionItemsList_t157728542_0_0_0_Types[] = { &GetMyMobilityItemsReactionItemsList_t157728542_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityItemsReactionItemsList_t157728542_0_0_0 = { 1, GenInst_GetMyMobilityItemsReactionItemsList_t157728542_0_0_0_Types };
extern const Il2CppType GetMyMobilityEncountList_t4033961011_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityEncountList_t4033961011_0_0_0_Types[] = { &GetMyMobilityEncountList_t4033961011_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityEncountList_t4033961011_0_0_0 = { 1, GenInst_GetMyMobilityEncountList_t4033961011_0_0_0_Types };
extern const Il2CppType GetMyMobilityRegisterdMotionList_t939659276_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityRegisterdMotionList_t939659276_0_0_0_Types[] = { &GetMyMobilityRegisterdMotionList_t939659276_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityRegisterdMotionList_t939659276_0_0_0 = { 1, GenInst_GetMyMobilityRegisterdMotionList_t939659276_0_0_0_Types };
extern const Il2CppType GetMyMobilityRecordedMotionList_t67098073_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityRecordedMotionList_t67098073_0_0_0_Types[] = { &GetMyMobilityRecordedMotionList_t67098073_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityRecordedMotionList_t67098073_0_0_0 = { 1, GenInst_GetMyMobilityRecordedMotionList_t67098073_0_0_0_Types };
extern const Il2CppType GetMyMobilityChildPartsListModel_t3080641068_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityChildPartsListModel_t3080641068_0_0_0_Types[] = { &GetMyMobilityChildPartsListModel_t3080641068_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityChildPartsListModel_t3080641068_0_0_0 = { 1, GenInst_GetMyMobilityChildPartsListModel_t3080641068_0_0_0_Types };
extern const Il2CppType GetMyMobilityRegisterMotionListMotionData_t2380437702_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityRegisterMotionListMotionData_t2380437702_0_0_0_Types[] = { &GetMyMobilityRegisterMotionListMotionData_t2380437702_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityRegisterMotionListMotionData_t2380437702_0_0_0 = { 1, GenInst_GetMyMobilityRegisterMotionListMotionData_t2380437702_0_0_0_Types };
extern const Il2CppType GetMyMobilityRegisterMotionListPoseData_t3613137541_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityRegisterMotionListPoseData_t3613137541_0_0_0_Types[] = { &GetMyMobilityRegisterMotionListPoseData_t3613137541_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityRegisterMotionListPoseData_t3613137541_0_0_0 = { 1, GenInst_GetMyMobilityRegisterMotionListPoseData_t3613137541_0_0_0_Types };
extern const Il2CppType GetMyMobilityRecordedMotionListMotions_t1565847752_0_0_0;
static const Il2CppType* GenInst_GetMyMobilityRecordedMotionListMotions_t1565847752_0_0_0_Types[] = { &GetMyMobilityRecordedMotionListMotions_t1565847752_0_0_0 };
extern const Il2CppGenericInst GenInst_GetMyMobilityRecordedMotionListMotions_t1565847752_0_0_0 = { 1, GenInst_GetMyMobilityRecordedMotionListMotions_t1565847752_0_0_0_Types };
extern const Il2CppType NearmobItemsModel_t1641537963_0_0_0;
static const Il2CppType* GenInst_NearmobItemsModel_t1641537963_0_0_0_Types[] = { &NearmobItemsModel_t1641537963_0_0_0 };
extern const Il2CppGenericInst GenInst_NearmobItemsModel_t1641537963_0_0_0 = { 1, GenInst_NearmobItemsModel_t1641537963_0_0_0_Types };
extern const Il2CppType GetNearmobRecordedMotionListMotions_t4266963713_0_0_0;
static const Il2CppType* GenInst_GetNearmobRecordedMotionListMotions_t4266963713_0_0_0_Types[] = { &GetNearmobRecordedMotionListMotions_t4266963713_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobRecordedMotionListMotions_t4266963713_0_0_0 = { 1, GenInst_GetNearmobRecordedMotionListMotions_t4266963713_0_0_0_Types };
extern const Il2CppType GetNearmobItemsReactionItemsList_t586382143_0_0_0;
static const Il2CppType* GenInst_GetNearmobItemsReactionItemsList_t586382143_0_0_0_Types[] = { &GetNearmobItemsReactionItemsList_t586382143_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobItemsReactionItemsList_t586382143_0_0_0 = { 1, GenInst_GetNearmobItemsReactionItemsList_t586382143_0_0_0_Types };
extern const Il2CppType GetNearmobEncountList_t2518104622_0_0_0;
static const Il2CppType* GenInst_GetNearmobEncountList_t2518104622_0_0_0_Types[] = { &GetNearmobEncountList_t2518104622_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobEncountList_t2518104622_0_0_0 = { 1, GenInst_GetNearmobEncountList_t2518104622_0_0_0_Types };
extern const Il2CppType GetNearmobRegisterdMotionList_t3457217205_0_0_0;
static const Il2CppType* GenInst_GetNearmobRegisterdMotionList_t3457217205_0_0_0_Types[] = { &GetNearmobRegisterdMotionList_t3457217205_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobRegisterdMotionList_t3457217205_0_0_0 = { 1, GenInst_GetNearmobRegisterdMotionList_t3457217205_0_0_0_Types };
extern const Il2CppType GetNearmobRecordedMotionList_t64870246_0_0_0;
static const Il2CppType* GenInst_GetNearmobRecordedMotionList_t64870246_0_0_0_Types[] = { &GetNearmobRecordedMotionList_t64870246_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobRecordedMotionList_t64870246_0_0_0 = { 1, GenInst_GetNearmobRecordedMotionList_t64870246_0_0_0_Types };
extern const Il2CppType GetNearmobChildPartsListModel_t3662882787_0_0_0;
static const Il2CppType* GenInst_GetNearmobChildPartsListModel_t3662882787_0_0_0_Types[] = { &GetNearmobChildPartsListModel_t3662882787_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobChildPartsListModel_t3662882787_0_0_0 = { 1, GenInst_GetNearmobChildPartsListModel_t3662882787_0_0_0_Types };
extern const Il2CppType GetNearmobRegisterMotionListMotionData_t1259926239_0_0_0;
static const Il2CppType* GenInst_GetNearmobRegisterMotionListMotionData_t1259926239_0_0_0_Types[] = { &GetNearmobRegisterMotionListMotionData_t1259926239_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobRegisterMotionListMotionData_t1259926239_0_0_0 = { 1, GenInst_GetNearmobRegisterMotionListMotionData_t1259926239_0_0_0_Types };
extern const Il2CppType GetNearmobRegisterMotionListPoseData_t1612634570_0_0_0;
static const Il2CppType* GenInst_GetNearmobRegisterMotionListPoseData_t1612634570_0_0_0_Types[] = { &GetNearmobRegisterMotionListPoseData_t1612634570_0_0_0 };
extern const Il2CppGenericInst GenInst_GetNearmobRegisterMotionListPoseData_t1612634570_0_0_0 = { 1, GenInst_GetNearmobRegisterMotionListPoseData_t1612634570_0_0_0_Types };
extern const Il2CppType GetPartsCategoryItems_t105269994_0_0_0;
static const Il2CppType* GenInst_GetPartsCategoryItems_t105269994_0_0_0_Types[] = { &GetPartsCategoryItems_t105269994_0_0_0 };
extern const Il2CppGenericInst GenInst_GetPartsCategoryItems_t105269994_0_0_0 = { 1, GenInst_GetPartsCategoryItems_t105269994_0_0_0_Types };
extern const Il2CppType GetPartsList_t1367059906_0_0_0;
static const Il2CppType* GenInst_GetPartsList_t1367059906_0_0_0_Types[] = { &GetPartsList_t1367059906_0_0_0 };
extern const Il2CppGenericInst GenInst_GetPartsList_t1367059906_0_0_0 = { 1, GenInst_GetPartsList_t1367059906_0_0_0_Types };
extern const Il2CppType GetRecordingModelMotions_t2111205999_0_0_0;
static const Il2CppType* GenInst_GetRecordingModelMotions_t2111205999_0_0_0_Types[] = { &GetRecordingModelMotions_t2111205999_0_0_0 };
extern const Il2CppGenericInst GenInst_GetRecordingModelMotions_t2111205999_0_0_0 = { 1, GenInst_GetRecordingModelMotions_t2111205999_0_0_0_Types };
extern const Il2CppType DescriptionAttribute_t3207779672_0_0_0;
static const Il2CppType* GenInst_DescriptionAttribute_t3207779672_0_0_0_Types[] = { &DescriptionAttribute_t3207779672_0_0_0 };
extern const Il2CppGenericInst GenInst_DescriptionAttribute_t3207779672_0_0_0 = { 1, GenInst_DescriptionAttribute_t3207779672_0_0_0_Types };
extern const Il2CppType DownLoadAssetbundle_t3666029200_0_0_0;
static const Il2CppType* GenInst_DownLoadAssetbundle_t3666029200_0_0_0_Types[] = { &DownLoadAssetbundle_t3666029200_0_0_0 };
extern const Il2CppGenericInst GenInst_DownLoadAssetbundle_t3666029200_0_0_0 = { 1, GenInst_DownLoadAssetbundle_t3666029200_0_0_0_Types };
extern const Il2CppType eAUDIOBGM_t4173725093_0_0_0;
extern const Il2CppType AudioClip_t1932558630_0_0_0;
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0 = { 2, GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t98719111_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t98719111_0_0_0_Types[] = { &KeyValuePair_2_t98719111_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t98719111_0_0_0 = { 1, GenInst_KeyValuePair_2_t98719111_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0 = { 1, GenInst_eAUDIOBGM_t4173725093_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_eAUDIOBGM_t4173725093_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &Il2CppObject_0_0_0, &eAUDIOBGM_t4173725093_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_eAUDIOBGM_t4173725093_0_0_0 = { 3, GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_eAUDIOBGM_t4173725093_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t98719111_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t98719111_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t98719111_0_0_0 = { 3, GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t98719111_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &AudioClip_t1932558630_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AudioClip_t1932558630_0_0_0_Types[] = { &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioClip_t1932558630_0_0_0 = { 1, GenInst_AudioClip_t1932558630_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3636795742_0_0_0;
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3636795742_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &AudioClip_t1932558630_0_0_0, &KeyValuePair_2_t3636795742_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3636795742_0_0_0 = { 3, GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3636795742_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3636795742_0_0_0_Types[] = { &KeyValuePair_2_t3636795742_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3636795742_0_0_0 = { 1, GenInst_KeyValuePair_2_t3636795742_0_0_0_Types };
extern const Il2CppType eAREATYPE_t1739175762_0_0_0;
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0 = { 2, GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1025746566_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1025746566_0_0_0_Types[] = { &KeyValuePair_2_t1025746566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1025746566_0_0_0 = { 1, GenInst_KeyValuePair_2_t1025746566_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0 = { 1, GenInst_eAREATYPE_t1739175762_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_eAREATYPE_t1739175762_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &Il2CppObject_0_0_0, &eAREATYPE_t1739175762_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_eAREATYPE_t1739175762_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_eAREATYPE_t1739175762_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1025746566_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1025746566_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1025746566_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1025746566_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &AudioClip_t1932558630_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t268855901_0_0_0;
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t268855901_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &AudioClip_t1932558630_0_0_0, &KeyValuePair_2_t268855901_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t268855901_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t268855901_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t268855901_0_0_0_Types[] = { &KeyValuePair_2_t268855901_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t268855901_0_0_0 = { 1, GenInst_KeyValuePair_2_t268855901_0_0_0_Types };
extern const Il2CppType CameraEffect_t3191348726_0_0_0;
static const Il2CppType* GenInst_CameraEffect_t3191348726_0_0_0_Types[] = { &CameraEffect_t3191348726_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraEffect_t3191348726_0_0_0 = { 1, GenInst_CameraEffect_t3191348726_0_0_0_Types };
extern const Il2CppType Slider_t297367283_0_0_0;
static const Il2CppType* GenInst_Slider_t297367283_0_0_0_Types[] = { &Slider_t297367283_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t297367283_0_0_0 = { 1, GenInst_Slider_t297367283_0_0_0_Types };
extern const Il2CppType LightManager_t3427519171_0_0_0;
static const Il2CppType* GenInst_LightManager_t3427519171_0_0_0_Types[] = { &LightManager_t3427519171_0_0_0 };
extern const Il2CppGenericInst GenInst_LightManager_t3427519171_0_0_0 = { 1, GenInst_LightManager_t3427519171_0_0_0_Types };
extern const Il2CppType eEventID_t711132804_0_0_0;
extern const Il2CppType List_1_t1361608608_0_0_0;
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &List_1_t1361608608_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0 = { 2, GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_Types };
extern const Il2CppType Action_2_t1992487476_0_0_0;
static const Il2CppType* GenInst_Action_2_t1992487476_0_0_0_Types[] = { &Action_2_t1992487476_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1992487476_0_0_0 = { 1, GenInst_Action_2_t1992487476_0_0_0_Types };
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Il2CppObject_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_MonoBehaviour_t1158329972_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3835431500_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3835431500_0_0_0_Types[] = { &KeyValuePair_2_t3835431500_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3835431500_0_0_0 = { 1, GenInst_KeyValuePair_2_t3835431500_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_Types[] = { &eEventID_t711132804_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0 = { 1, GenInst_eEventID_t711132804_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_eEventID_t711132804_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &Il2CppObject_0_0_0, &eEventID_t711132804_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_eEventID_t711132804_0_0_0 = { 3, GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_eEventID_t711132804_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3835431500_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3835431500_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3835431500_0_0_0 = { 3, GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3835431500_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &List_1_t1361608608_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1361608608_0_0_0_Types[] = { &List_1_t1361608608_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1361608608_0_0_0 = { 1, GenInst_List_1_t1361608608_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2507590813_0_0_0;
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_KeyValuePair_2_t2507590813_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &List_1_t1361608608_0_0_0, &KeyValuePair_2_t2507590813_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_KeyValuePair_2_t2507590813_0_0_0 = { 3, GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_KeyValuePair_2_t2507590813_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2507590813_0_0_0_Types[] = { &KeyValuePair_2_t2507590813_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2507590813_0_0_0 = { 1, GenInst_KeyValuePair_2_t2507590813_0_0_0_Types };
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &Transform_t3275118058_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_Transform_t3275118058_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Transform_t3275118058_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Transform_t3275118058_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType LocalizeKey_t3348421234_0_0_0;
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0 = { 1, GenInst_LocalizeKey_t3348421234_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_Types[] = { &String_t_0_0_0, &LocalizeKey_t3348421234_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0 = { 2, GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Types[] = { &Il2CppObject_0_0_0, &LocalizeKey_t3348421234_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t697826584_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t697826584_0_0_0_Types[] = { &KeyValuePair_2_t697826584_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t697826584_0_0_0 = { 1, GenInst_KeyValuePair_2_t697826584_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &LocalizeKey_t3348421234_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0_Types[] = { &Il2CppObject_0_0_0, &LocalizeKey_t3348421234_0_0_0, &LocalizeKey_t3348421234_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &LocalizeKey_t3348421234_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t697826584_0_0_0_Types[] = { &Il2CppObject_0_0_0, &LocalizeKey_t3348421234_0_0_0, &KeyValuePair_2_t697826584_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t697826584_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t697826584_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &LocalizeKey_t3348421234_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3020545718_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t3020545718_0_0_0_Types[] = { &String_t_0_0_0, &LocalizeKey_t3348421234_0_0_0, &KeyValuePair_2_t3020545718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t3020545718_0_0_0 = { 3, GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t3020545718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3020545718_0_0_0_Types[] = { &KeyValuePair_2_t3020545718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3020545718_0_0_0 = { 1, GenInst_KeyValuePair_2_t3020545718_0_0_0_Types };
extern const Il2CppType Pose_t686168059_0_0_0;
static const Il2CppType* GenInst_Pose_t686168059_0_0_0_Types[] = { &Pose_t686168059_0_0_0 };
extern const Il2CppGenericInst GenInst_Pose_t686168059_0_0_0 = { 1, GenInst_Pose_t686168059_0_0_0_Types };
extern const Il2CppType Icon_t4013200341_0_0_0;
static const Il2CppType* GenInst_Icon_t4013200341_0_0_0_Types[] = { &Icon_t4013200341_0_0_0 };
extern const Il2CppGenericInst GenInst_Icon_t4013200341_0_0_0 = { 1, GenInst_Icon_t4013200341_0_0_0_Types };
extern const Il2CppType Texture2DU5BU5D_t2724090252_0_0_0;
static const Il2CppType* GenInst_Texture2DU5BU5D_t2724090252_0_0_0_Types[] = { &Texture2DU5BU5D_t2724090252_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2DU5BU5D_t2724090252_0_0_0 = { 1, GenInst_Texture2DU5BU5D_t2724090252_0_0_0_Types };
extern const Il2CppType Parts_t3804168686_0_0_0;
static const Il2CppType* GenInst_Parts_t3804168686_0_0_0_Types[] = { &Parts_t3804168686_0_0_0 };
extern const Il2CppGenericInst GenInst_Parts_t3804168686_0_0_0 = { 1, GenInst_Parts_t3804168686_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_AnimationCurve_t3306541151_0_0_0_Types[] = { &String_t_0_0_0, &AnimationCurve_t3306541151_0_0_0, &AnimationCurve_t3306541151_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_AnimationCurve_t3306541151_0_0_0 = { 3, GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_AnimationCurve_t3306541151_0_0_0_Types };
extern const Il2CppType RotationController_t3824016438_0_0_0;
static const Il2CppType* GenInst_RotationController_t3824016438_0_0_0_Types[] = { &RotationController_t3824016438_0_0_0 };
extern const Il2CppGenericInst GenInst_RotationController_t3824016438_0_0_0 = { 1, GenInst_RotationController_t3824016438_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &AnimationCurve_t3306541151_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType eAUDIOSE_t638655723_0_0_0;
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0 = { 2, GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2169266793_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2169266793_0_0_0_Types[] = { &KeyValuePair_2_t2169266793_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2169266793_0_0_0 = { 1, GenInst_KeyValuePair_2_t2169266793_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0 = { 1, GenInst_eAUDIOSE_t638655723_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_eAUDIOSE_t638655723_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &Il2CppObject_0_0_0, &eAUDIOSE_t638655723_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_eAUDIOSE_t638655723_0_0_0 = { 3, GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_eAUDIOSE_t638655723_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2169266793_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2169266793_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2169266793_0_0_0 = { 3, GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2169266793_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &AudioClip_t1932558630_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1412376128_0_0_0;
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t1412376128_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &AudioClip_t1932558630_0_0_0, &KeyValuePair_2_t1412376128_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t1412376128_0_0_0 = { 3, GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t1412376128_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1412376128_0_0_0_Types[] = { &KeyValuePair_2_t1412376128_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1412376128_0_0_0 = { 1, GenInst_KeyValuePair_2_t1412376128_0_0_0_Types };
extern const Il2CppType eAUDIOVOICE_t2836738047_0_0_0;
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &AudioClip_t1932558630_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0 = { 2, GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4217592965_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4217592965_0_0_0_Types[] = { &KeyValuePair_2_t4217592965_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4217592965_0_0_0 = { 1, GenInst_KeyValuePair_2_t4217592965_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0 = { 1, GenInst_eAUDIOVOICE_t2836738047_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_eAUDIOVOICE_t2836738047_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &Il2CppObject_0_0_0, &eAUDIOVOICE_t2836738047_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_eAUDIOVOICE_t2836738047_0_0_0 = { 3, GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_eAUDIOVOICE_t2836738047_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4217592965_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4217592965_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4217592965_0_0_0 = { 3, GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4217592965_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &AudioClip_t1932558630_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3460702300_0_0_0;
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3460702300_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &AudioClip_t1932558630_0_0_0, &KeyValuePair_2_t3460702300_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3460702300_0_0_0 = { 3, GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3460702300_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3460702300_0_0_0_Types[] = { &KeyValuePair_2_t3460702300_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3460702300_0_0_0 = { 1, GenInst_KeyValuePair_2_t3460702300_0_0_0_Types };
extern const Il2CppType SliderMaster_t3139894563_0_0_0;
static const Il2CppType* GenInst_SliderMaster_t3139894563_0_0_0_Types[] = { &SliderMaster_t3139894563_0_0_0 };
extern const Il2CppGenericInst GenInst_SliderMaster_t3139894563_0_0_0 = { 1, GenInst_SliderMaster_t3139894563_0_0_0_Types };
extern const Il2CppType PartsConfig_t872073448_0_0_0;
static const Il2CppType* GenInst_PartsConfig_t872073448_0_0_0_Types[] = { &PartsConfig_t872073448_0_0_0 };
extern const Il2CppGenericInst GenInst_PartsConfig_t872073448_0_0_0 = { 1, GenInst_PartsConfig_t872073448_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &Tween_t278478013_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0 = { 2, GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &Tween_t278478013_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1366168679_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_KeyValuePair_2_t1366168679_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &Tween_t278478013_0_0_0, &KeyValuePair_2_t1366168679_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_KeyValuePair_2_t1366168679_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_KeyValuePair_2_t1366168679_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1366168679_0_0_0_Types[] = { &KeyValuePair_2_t1366168679_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1366168679_0_0_0 = { 1, GenInst_KeyValuePair_2_t1366168679_0_0_0_Types };
extern const Il2CppType SlideInOut_t958296806_0_0_0;
static const Il2CppType* GenInst_SlideInOut_t958296806_0_0_0_Types[] = { &SlideInOut_t958296806_0_0_0 };
extern const Il2CppGenericInst GenInst_SlideInOut_t958296806_0_0_0 = { 1, GenInst_SlideInOut_t958296806_0_0_0_Types };
extern const Il2CppType Renderer_t257310565_0_0_0;
static const Il2CppType* GenInst_Renderer_t257310565_0_0_0_Types[] = { &Renderer_t257310565_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0 = { 1, GenInst_Renderer_t257310565_0_0_0_Types };
extern const Il2CppType GameManager_t2252321495_0_0_0;
static const Il2CppType* GenInst_GameManager_t2252321495_0_0_0_Types[] = { &GameManager_t2252321495_0_0_0 };
extern const Il2CppGenericInst GenInst_GameManager_t2252321495_0_0_0 = { 1, GenInst_GameManager_t2252321495_0_0_0_Types };
extern const Il2CppType MeshRenderer_t1268241104_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t1268241104_0_0_0_Types[] = { &MeshRenderer_t1268241104_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1268241104_0_0_0 = { 1, GenInst_MeshRenderer_t1268241104_0_0_0_Types };
extern const Il2CppType Light_t494725636_0_0_0;
static const Il2CppType* GenInst_Light_t494725636_0_0_0_Types[] = { &Light_t494725636_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t494725636_0_0_0 = { 1, GenInst_Light_t494725636_0_0_0_Types };
extern const Il2CppType Collider_t3497673348_0_0_0;
static const Il2CppType* GenInst_Collider_t3497673348_0_0_0_Types[] = { &Collider_t3497673348_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t3497673348_0_0_0 = { 1, GenInst_Collider_t3497673348_0_0_0_Types };
extern const Il2CppType lightObj_t383130471_0_0_0;
static const Il2CppType* GenInst_lightObj_t383130471_0_0_0_Types[] = { &lightObj_t383130471_0_0_0 };
extern const Il2CppGenericInst GenInst_lightObj_t383130471_0_0_0 = { 1, GenInst_lightObj_t383130471_0_0_0_Types };
extern const Il2CppType eLIGHTTYPE_t1469766701_0_0_0;
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Light_t494725636_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0 = { 2, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t194954207_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t194954207_0_0_0_Types[] = { &KeyValuePair_2_t194954207_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t194954207_0_0_0 = { 1, GenInst_KeyValuePair_2_t194954207_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0 = { 1, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_eLIGHTTYPE_t1469766701_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Il2CppObject_0_0_0, &eLIGHTTYPE_t1469766701_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_eLIGHTTYPE_t1469766701_0_0_0 = { 3, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_eLIGHTTYPE_t1469766701_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t194954207_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t194954207_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t194954207_0_0_0 = { 3, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t194954207_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Light_t494725636_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2295197844_0_0_0;
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_KeyValuePair_2_t2295197844_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &Light_t494725636_0_0_0, &KeyValuePair_2_t2295197844_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_KeyValuePair_2_t2295197844_0_0_0 = { 3, GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_KeyValuePair_2_t2295197844_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2295197844_0_0_0_Types[] = { &KeyValuePair_2_t2295197844_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2295197844_0_0_0 = { 1, GenInst_KeyValuePair_2_t2295197844_0_0_0_Types };
extern const Il2CppType LoadingManager_t2398813851_0_0_0;
static const Il2CppType* GenInst_LoadingManager_t2398813851_0_0_0_Types[] = { &LoadingManager_t2398813851_0_0_0 };
extern const Il2CppGenericInst GenInst_LoadingManager_t2398813851_0_0_0 = { 1, GenInst_LoadingManager_t2398813851_0_0_0_Types };
extern const Il2CppType LocalizeTextContents_t1443703366_0_0_0;
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &LocalizeTextContents_t1443703366_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0 = { 2, GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3838112486_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3838112486_0_0_0_Types[] = { &KeyValuePair_2_t3838112486_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3838112486_0_0_0 = { 1, GenInst_KeyValuePair_2_t3838112486_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &Il2CppObject_0_0_0, &LocalizeKey_t3348421234_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0 = { 3, GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3838112486_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3838112486_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3838112486_0_0_0 = { 3, GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3838112486_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &LocalizeTextContents_t1443703366_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeTextContents_t1443703366_0_0_0_Types[] = { &LocalizeTextContents_t1443703366_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeTextContents_t1443703366_0_0_0 = { 1, GenInst_LocalizeTextContents_t1443703366_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2592366557_0_0_0;
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_KeyValuePair_2_t2592366557_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &LocalizeTextContents_t1443703366_0_0_0, &KeyValuePair_2_t2592366557_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_KeyValuePair_2_t2592366557_0_0_0 = { 3, GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_KeyValuePair_2_t2592366557_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2592366557_0_0_0_Types[] = { &KeyValuePair_2_t2592366557_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2592366557_0_0_0 = { 1, GenInst_KeyValuePair_2_t2592366557_0_0_0_Types };
extern const Il2CppType LocalizeItem_t998829008_0_0_0;
static const Il2CppType* GenInst_LocalizeItem_t998829008_0_0_0_Types[] = { &LocalizeItem_t998829008_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeItem_t998829008_0_0_0 = { 1, GenInst_LocalizeItem_t998829008_0_0_0_Types };
extern const Il2CppType UILocalizer_t2245030347_0_0_0;
static const Il2CppType* GenInst_UILocalizer_t2245030347_0_0_0_Types[] = { &UILocalizer_t2245030347_0_0_0 };
extern const Il2CppGenericInst GenInst_UILocalizer_t2245030347_0_0_0 = { 1, GenInst_UILocalizer_t2245030347_0_0_0_Types };
extern const Il2CppType SCORETYPE_t2692914394_0_0_0;
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t37182331_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t37182331_0_0_0_Types[] = { &KeyValuePair_2_t37182331_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t37182331_0_0_0 = { 1, GenInst_KeyValuePair_2_t37182331_0_0_0_Types };
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0 = { 1, GenInst_SCORETYPE_t2692914394_0_0_0_Types };
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_SCORETYPE_t2692914394_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0, &Single_t2076509932_0_0_0, &SCORETYPE_t2692914394_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_SCORETYPE_t2692914394_0_0_0 = { 3, GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_SCORETYPE_t2692914394_0_0_0_Types };
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0, &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 3, GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0, &Single_t2076509932_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t37182331_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0, &Single_t2076509932_0_0_0, &KeyValuePair_2_t37182331_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t37182331_0_0_0 = { 3, GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t37182331_0_0_0_Types };
extern const Il2CppType Image_t2042527209_0_0_0;
static const Il2CppType* GenInst_Image_t2042527209_0_0_0_Types[] = { &Image_t2042527209_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
extern const Il2CppType ISerializationCallbackReceiver_t1665913161_0_0_0;
static const Il2CppType* GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0_Types[] = { &ISerializationCallbackReceiver_t1665913161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0 = { 1, GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0_Types };
extern const Il2CppType ICanvasRaycastFilter_t1367822892_0_0_0;
static const Il2CppType* GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types[] = { &ICanvasRaycastFilter_t1367822892_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t1367822892_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types };
extern const Il2CppType TeleportManager_t4218394618_0_0_0;
static const Il2CppType* GenInst_TeleportManager_t4218394618_0_0_0_Types[] = { &TeleportManager_t4218394618_0_0_0 };
extern const Il2CppGenericInst GenInst_TeleportManager_t4218394618_0_0_0 = { 1, GenInst_TeleportManager_t4218394618_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Module_t3140434828_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Module_t3140434828_0_0_0, &Module_t3140434828_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Module_t3140434828_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Module_t3140434828_0_0_0_Types };
extern const Il2CppType SceneFadeManager_t1728942829_0_0_0;
static const Il2CppType* GenInst_SceneFadeManager_t1728942829_0_0_0_Types[] = { &SceneFadeManager_t1728942829_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneFadeManager_t1728942829_0_0_0 = { 1, GenInst_SceneFadeManager_t1728942829_0_0_0_Types };
extern const Il2CppType WorldMapManager_t3256198739_0_0_0;
static const Il2CppType* GenInst_WorldMapManager_t3256198739_0_0_0_Types[] = { &WorldMapManager_t3256198739_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldMapManager_t3256198739_0_0_0 = { 1, GenInst_WorldMapManager_t3256198739_0_0_0_Types };
extern const Il2CppType Action_t3226471752_0_0_0;
static const Il2CppType* GenInst_Action_t3226471752_0_0_0_Types[] = { &Action_t3226471752_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
extern const Il2CppType PartsManager_t197384025_0_0_0;
static const Il2CppType* GenInst_PartsManager_t197384025_0_0_0_Types[] = { &PartsManager_t197384025_0_0_0 };
extern const Il2CppGenericInst GenInst_PartsManager_t197384025_0_0_0 = { 1, GenInst_PartsManager_t197384025_0_0_0_Types };
extern const Il2CppType PremitivePartContent_t3696216029_0_0_0;
static const Il2CppType* GenInst_PremitivePartContent_t3696216029_0_0_0_Types[] = { &PremitivePartContent_t3696216029_0_0_0 };
extern const Il2CppGenericInst GenInst_PremitivePartContent_t3696216029_0_0_0 = { 1, GenInst_PremitivePartContent_t3696216029_0_0_0_Types };
extern const Il2CppType UIPanel_t1795085332_0_0_0;
static const Il2CppType* GenInst_UIPanel_t1795085332_0_0_0_Types[] = { &UIPanel_t1795085332_0_0_0 };
extern const Il2CppGenericInst GenInst_UIPanel_t1795085332_0_0_0 = { 1, GenInst_UIPanel_t1795085332_0_0_0_Types };
extern const Il2CppType UIObject_t4279159643_0_0_0;
static const Il2CppType* GenInst_UIObject_t4279159643_0_0_0_Types[] = { &UIObject_t4279159643_0_0_0 };
extern const Il2CppGenericInst GenInst_UIObject_t4279159643_0_0_0 = { 1, GenInst_UIObject_t4279159643_0_0_0_Types };
extern const Il2CppType AreaParentObject_t3918549470_0_0_0;
static const Il2CppType* GenInst_AreaParentObject_t3918549470_0_0_0_Types[] = { &AreaParentObject_t3918549470_0_0_0 };
extern const Il2CppGenericInst GenInst_AreaParentObject_t3918549470_0_0_0 = { 1, GenInst_AreaParentObject_t3918549470_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t92830418_0_0_0;
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t92830418_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &GameObject_t1756533147_0_0_0, &KeyValuePair_2_t92830418_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t92830418_0_0_0 = { 3, GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t92830418_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t92830418_0_0_0_Types[] = { &KeyValuePair_2_t92830418_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t92830418_0_0_0 = { 1, GenInst_KeyValuePair_2_t92830418_0_0_0_Types };
extern const Il2CppType LonLatitude_t2793245363_0_0_0;
static const Il2CppType* GenInst_LonLatitude_t2793245363_0_0_0_Types[] = { &LonLatitude_t2793245363_0_0_0 };
extern const Il2CppGenericInst GenInst_LonLatitude_t2793245363_0_0_0 = { 1, GenInst_LonLatitude_t2793245363_0_0_0_Types };
extern const Il2CppType FixedJoint_t3848069458_0_0_0;
static const Il2CppType* GenInst_FixedJoint_t3848069458_0_0_0_Types[] = { &FixedJoint_t3848069458_0_0_0 };
extern const Il2CppGenericInst GenInst_FixedJoint_t3848069458_0_0_0 = { 1, GenInst_FixedJoint_t3848069458_0_0_0_Types };
extern const Il2CppType Joint_t454317436_0_0_0;
static const Il2CppType* GenInst_Joint_t454317436_0_0_0_Types[] = { &Joint_t454317436_0_0_0 };
extern const Il2CppGenericInst GenInst_Joint_t454317436_0_0_0 = { 1, GenInst_Joint_t454317436_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2844223813_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0, &KeyValuePair_2_t2844223813_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2844223813_0_0_0_Types[] = { &KeyValuePair_2_t2844223813_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2844223813_0_0_0 = { 1, GenInst_KeyValuePair_2_t2844223813_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType CoreContent_t4281623508_0_0_0;
static const Il2CppType* GenInst_CoreContent_t4281623508_0_0_0_Types[] = { &CoreContent_t4281623508_0_0_0 };
extern const Il2CppGenericInst GenInst_CoreContent_t4281623508_0_0_0 = { 1, GenInst_CoreContent_t4281623508_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Module_t3140434828_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Button_t2872111280_0_0_0;
static const Il2CppType* GenInst_Button_t2872111280_0_0_0_Types[] = { &Button_t2872111280_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
extern const Il2CppType PlaceData_t1476161629_0_0_0;
static const Il2CppType* GenInst_PlaceData_t1476161629_0_0_0_Types[] = { &PlaceData_t1476161629_0_0_0 };
extern const Il2CppGenericInst GenInst_PlaceData_t1476161629_0_0_0 = { 1, GenInst_PlaceData_t1476161629_0_0_0_Types };
extern const Il2CppType ParticleSystem_t3394631041_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t3394631041_0_0_0_Types[] = { &ParticleSystem_t3394631041_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t3394631041_0_0_0 = { 1, GenInst_ParticleSystem_t3394631041_0_0_0_Types };
extern const Il2CppType TimeAttackPoint_t1479403935_0_0_0;
static const Il2CppType* GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &TimeAttackPoint_t1479403935_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_TimeAttackPoint_t1479403935_0_0_0_Types[] = { &TimeAttackPoint_t1479403935_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeAttackPoint_t1479403935_0_0_0 = { 1, GenInst_TimeAttackPoint_t1479403935_0_0_0_Types };
static const Il2CppType* GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &TimeAttackPoint_t1479403935_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4188421297_0_0_0;
static const Il2CppType* GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t4188421297_0_0_0_Types[] = { &TimeAttackPoint_t1479403935_0_0_0, &GameObject_t1756533147_0_0_0, &KeyValuePair_2_t4188421297_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t4188421297_0_0_0 = { 3, GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t4188421297_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4188421297_0_0_0_Types[] = { &KeyValuePair_2_t4188421297_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4188421297_0_0_0 = { 1, GenInst_KeyValuePair_2_t4188421297_0_0_0_Types };
static const Il2CppType* GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &TimeAttackPoint_t1479403935_0_0_0, &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0 = { 3, GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2816671300_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2816671300_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GameObject_t1756533147_0_0_0, &KeyValuePair_2_t2816671300_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2816671300_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2816671300_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2816671300_0_0_0_Types[] = { &KeyValuePair_2_t2816671300_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2816671300_0_0_0 = { 1, GenInst_KeyValuePair_2_t2816671300_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GameObject_t1756533147_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType SliderControl_t94665898_0_0_0;
static const Il2CppType* GenInst_SliderControl_t94665898_0_0_0_Types[] = { &SliderControl_t94665898_0_0_0 };
extern const Il2CppGenericInst GenInst_SliderControl_t94665898_0_0_0 = { 1, GenInst_SliderControl_t94665898_0_0_0_Types };
extern const Il2CppType ContactField_t897925540_0_0_0;
static const Il2CppType* GenInst_ContactField_t897925540_0_0_0_Types[] = { &ContactField_t897925540_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactField_t897925540_0_0_0 = { 1, GenInst_ContactField_t897925540_0_0_0_Types };
extern const Il2CppType NamePanelItem_t3250109188_0_0_0;
static const Il2CppType* GenInst_NamePanelItem_t3250109188_0_0_0_Types[] = { &NamePanelItem_t3250109188_0_0_0 };
extern const Il2CppGenericInst GenInst_NamePanelItem_t3250109188_0_0_0 = { 1, GenInst_NamePanelItem_t3250109188_0_0_0_Types };
extern const Il2CppType CircleItem_t3720335645_0_0_0;
static const Il2CppType* GenInst_CircleItem_t3720335645_0_0_0_Types[] = { &CircleItem_t3720335645_0_0_0 };
extern const Il2CppGenericInst GenInst_CircleItem_t3720335645_0_0_0 = { 1, GenInst_CircleItem_t3720335645_0_0_0_Types };
extern const Il2CppType InputPanelItem_t642391251_0_0_0;
static const Il2CppType* GenInst_InputPanelItem_t642391251_0_0_0_Types[] = { &InputPanelItem_t642391251_0_0_0 };
extern const Il2CppGenericInst GenInst_InputPanelItem_t642391251_0_0_0 = { 1, GenInst_InputPanelItem_t642391251_0_0_0_Types };
extern const Il2CppType TitlePanelItem_t1000476553_0_0_0;
static const Il2CppType* GenInst_TitlePanelItem_t1000476553_0_0_0_Types[] = { &TitlePanelItem_t1000476553_0_0_0 };
extern const Il2CppGenericInst GenInst_TitlePanelItem_t1000476553_0_0_0 = { 1, GenInst_TitlePanelItem_t1000476553_0_0_0_Types };
extern const Il2CppType AreaInfo_t2759954667_0_0_0;
static const Il2CppType* GenInst_AreaInfo_t2759954667_0_0_0_Types[] = { &AreaInfo_t2759954667_0_0_0 };
extern const Il2CppGenericInst GenInst_AreaInfo_t2759954667_0_0_0 = { 1, GenInst_AreaInfo_t2759954667_0_0_0_Types };
extern const Il2CppType EventTrigger_t1967201810_0_0_0;
static const Il2CppType* GenInst_EventTrigger_t1967201810_0_0_0_Types[] = { &EventTrigger_t1967201810_0_0_0 };
extern const Il2CppGenericInst GenInst_EventTrigger_t1967201810_0_0_0 = { 1, GenInst_EventTrigger_t1967201810_0_0_0_Types };
extern const Il2CppType AreaPoint_t3787172297_0_0_0;
static const Il2CppType* GenInst_AreaPoint_t3787172297_0_0_0_Types[] = { &AreaPoint_t3787172297_0_0_0 };
extern const Il2CppGenericInst GenInst_AreaPoint_t3787172297_0_0_0 = { 1, GenInst_AreaPoint_t3787172297_0_0_0_Types };
extern const Il2CppType AreaButton_t1637812991_0_0_0;
static const Il2CppType* GenInst_AreaButton_t1637812991_0_0_0_Types[] = { &AreaButton_t1637812991_0_0_0 };
extern const Il2CppGenericInst GenInst_AreaButton_t1637812991_0_0_0 = { 1, GenInst_AreaButton_t1637812991_0_0_0_Types };
extern const Il2CppType ConfigPanel_t537490728_0_0_0;
static const Il2CppType* GenInst_ConfigPanel_t537490728_0_0_0_Types[] = { &ConfigPanel_t537490728_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigPanel_t537490728_0_0_0 = { 1, GenInst_ConfigPanel_t537490728_0_0_0_Types };
extern const Il2CppType RentalMobilmo_t1706493197_0_0_0;
static const Il2CppType* GenInst_RentalMobilmo_t1706493197_0_0_0_Types[] = { &RentalMobilmo_t1706493197_0_0_0 };
extern const Il2CppGenericInst GenInst_RentalMobilmo_t1706493197_0_0_0 = { 1, GenInst_RentalMobilmo_t1706493197_0_0_0_Types };
extern const Il2CppType Entry_t1526823907_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_Types[] = { &String_t_0_0_0, &Entry_t1526823907_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0 = { 2, GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Entry_t1526823907_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3171196553_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3171196553_0_0_0_Types[] = { &KeyValuePair_2_t3171196553_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3171196553_0_0_0 = { 1, GenInst_KeyValuePair_2_t3171196553_0_0_0_Types };
static const Il2CppType* GenInst_Entry_t1526823907_0_0_0_Types[] = { &Entry_t1526823907_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t1526823907_0_0_0 = { 1, GenInst_Entry_t1526823907_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Entry_t1526823907_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Entry_t1526823907_0_0_0, &Entry_t1526823907_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Entry_t1526823907_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t3171196553_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Entry_t1526823907_0_0_0, &KeyValuePair_2_t3171196553_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t3171196553_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t3171196553_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Entry_t1526823907_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1198948391_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t1198948391_0_0_0_Types[] = { &String_t_0_0_0, &Entry_t1526823907_0_0_0, &KeyValuePair_2_t1198948391_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t1198948391_0_0_0 = { 3, GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t1198948391_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1198948391_0_0_0_Types[] = { &KeyValuePair_2_t1198948391_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1198948391_0_0_0 = { 1, GenInst_KeyValuePair_2_t1198948391_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0_Types[] = { &String_t_0_0_0, &Entry_t1526823907_0_0_0, &Entry_t1526823907_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0 = { 3, GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0_Types };
extern const Il2CppType ClickableEntry_t4031026959_0_0_0;
static const Il2CppType* GenInst_ClickableEntry_t4031026959_0_0_0_Types[] = { &ClickableEntry_t4031026959_0_0_0 };
extern const Il2CppGenericInst GenInst_ClickableEntry_t4031026959_0_0_0 = { 1, GenInst_ClickableEntry_t4031026959_0_0_0_Types };
extern const Il2CppType List_1_t3050876758_0_0_0;
static const Il2CppType* GenInst_List_1_t3050876758_0_0_0_Types[] = { &List_1_t3050876758_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3050876758_0_0_0 = { 1, GenInst_List_1_t3050876758_0_0_0_Types };
extern const Il2CppType PointToGps_t3385345589_0_0_0;
static const Il2CppType* GenInst_PointToGps_t3385345589_0_0_0_Types[] = { &PointToGps_t3385345589_0_0_0 };
extern const Il2CppGenericInst GenInst_PointToGps_t3385345589_0_0_0 = { 1, GenInst_PointToGps_t3385345589_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0, &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0 = { 2, GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Camera_t189460977_0_0_0, &Camera_t189460977_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1298964269_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_KeyValuePair_2_t1298964269_0_0_0_Types[] = { &Camera_t189460977_0_0_0, &Camera_t189460977_0_0_0, &KeyValuePair_2_t1298964269_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_KeyValuePair_2_t1298964269_0_0_0 = { 3, GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_KeyValuePair_2_t1298964269_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1298964269_0_0_0_Types[] = { &KeyValuePair_2_t1298964269_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1298964269_0_0_0 = { 1, GenInst_KeyValuePair_2_t1298964269_0_0_0_Types };
extern const Il2CppType Matrix4x4_t2933234003_0_0_0;
static const Il2CppType* GenInst_Matrix4x4_t2933234003_0_0_0_Types[] = { &Matrix4x4_t2933234003_0_0_0 };
extern const Il2CppGenericInst GenInst_Matrix4x4_t2933234003_0_0_0 = { 1, GenInst_Matrix4x4_t2933234003_0_0_0_Types };
extern const Il2CppType RenderTexture_t2666733923_0_0_0;
static const Il2CppType* GenInst_RenderTexture_t2666733923_0_0_0_Types[] = { &RenderTexture_t2666733923_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTexture_t2666733923_0_0_0 = { 1, GenInst_RenderTexture_t2666733923_0_0_0_Types };
extern const Il2CppType MaterialPropertyBlock_t3303648957_0_0_0;
static const Il2CppType* GenInst_MaterialPropertyBlock_t3303648957_0_0_0_Types[] = { &MaterialPropertyBlock_t3303648957_0_0_0 };
extern const Il2CppGenericInst GenInst_MaterialPropertyBlock_t3303648957_0_0_0 = { 1, GenInst_MaterialPropertyBlock_t3303648957_0_0_0_Types };
extern const Il2CppType MaterialU5BU5D_t3123989686_0_0_0;
static const Il2CppType* GenInst_MaterialU5BU5D_t3123989686_0_0_0_Types[] = { &MaterialU5BU5D_t3123989686_0_0_0 };
extern const Il2CppGenericInst GenInst_MaterialU5BU5D_t3123989686_0_0_0 = { 1, GenInst_MaterialU5BU5D_t3123989686_0_0_0_Types };
extern const Il2CppType IList_1_t734647528_0_0_0;
static const Il2CppType* GenInst_IList_1_t734647528_0_0_0_Types[] = { &IList_1_t734647528_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t734647528_0_0_0 = { 1, GenInst_IList_1_t734647528_0_0_0_Types };
extern const Il2CppType ICollection_1_t1145782232_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1145782232_0_0_0_Types[] = { &ICollection_1_t1145782232_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1145782232_0_0_0 = { 1, GenInst_ICollection_1_t1145782232_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t485833972_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t485833972_0_0_0_Types[] = { &IEnumerable_1_t485833972_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t485833972_0_0_0 = { 1, GenInst_IEnumerable_1_t485833972_0_0_0_Types };
extern const Il2CppType IList_1_t1562542718_0_0_0;
static const Il2CppType* GenInst_IList_1_t1562542718_0_0_0_Types[] = { &IList_1_t1562542718_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1562542718_0_0_0 = { 1, GenInst_IList_1_t1562542718_0_0_0_Types };
extern const Il2CppType ICollection_1_t1973677422_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1973677422_0_0_0_Types[] = { &ICollection_1_t1973677422_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1973677422_0_0_0 = { 1, GenInst_ICollection_1_t1973677422_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1313729162_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1313729162_0_0_0_Types[] = { &IEnumerable_1_t1313729162_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1313729162_0_0_0 = { 1, GenInst_IEnumerable_1_t1313729162_0_0_0_Types };
extern const Il2CppType Shader_t2430389951_0_0_0;
static const Il2CppType* GenInst_Shader_t2430389951_0_0_0_Types[] = { &Shader_t2430389951_0_0_0 };
extern const Il2CppGenericInst GenInst_Shader_t2430389951_0_0_0 = { 1, GenInst_Shader_t2430389951_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0, &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0, &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0, &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0, &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3546416104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types[] = { &Array_Sort_m3546416104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3546416104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m940423571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m940423571_gp_0_0_0_0_Types[] = { &Array_compare_m940423571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m940423571_gp_0_0_0_0 = { 1, GenInst_Array_compare_m940423571_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { &DefaultComparer_t3074655092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3895203923_gp_0_0_0_0, &ShimEnumerator_t3895203923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { &DefaultComparer_t1766400012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1001032761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { &HashSet_1_t2624254809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { &Enumerator_t2109956843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { &PrimeHelper_t3424417428_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m665396702_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types[] = { &Enumerable_Any_m665396702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m665396702_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m2739389357_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m4171187007_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m4171187007_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m522457978_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m522457978_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0_Types[] = { &Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ElementAt_m258442918_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0_Types[] = { &Enumerable_ElementAt_m258442918_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0 = { 1, GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ElementAt_m714932326_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0_Types[] = { &Enumerable_ElementAt_m714932326_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0 = { 1, GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0, &Enumerable_OrderBy_m920500904_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0, &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0, &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m2343256994_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m2343256994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m261161385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m261161385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0_Types[] = { &U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0 = { 1, GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t641749975_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t641749975_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t753306046_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t753306046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0, &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1290221672_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types[] = { &QuickSort_1_t1290221672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1290221672_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t4088581714_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types[] = { &SortContext_1_t4088581714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t4088581714_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0, &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3556217344_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types[] = { &LinkedList_1_t3556217344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3556217344_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4145643798_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types[] = { &Enumerator_t4145643798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4145643798_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2172356692_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t2172356692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { &Queue_1_t1458930734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { &Enumerator_t4000919638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
extern const Il2CppType SortedList_2_t3146765111_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3146765111_gp_0_0_0_0_Types[] = { &SortedList_2_t3146765111_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3146765111_gp_0_0_0_0 = { 1, GenInst_SortedList_2_t3146765111_gp_0_0_0_0_Types };
extern const Il2CppType SortedList_2_t3146765111_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3146765111_gp_1_0_0_0_Types[] = { &SortedList_2_t3146765111_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3146765111_gp_1_0_0_0 = { 1, GenInst_SortedList_2_t3146765111_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0_Types[] = { &SortedList_2_t3146765111_gp_0_0_0_0, &SortedList_2_t3146765111_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0 = { 2, GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4007177354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4007177354_0_0_0_Types[] = { &KeyValuePair_2_t4007177354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4007177354_0_0_0 = { 1, GenInst_KeyValuePair_2_t4007177354_0_0_0_Types };
extern const Il2CppType EnumeratorMode_t215745978_gp_0_0_0_0;
extern const Il2CppType EnumeratorMode_t215745978_gp_1_0_0_0;
static const Il2CppType* GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0_Types[] = { &EnumeratorMode_t215745978_gp_0_0_0_0, &EnumeratorMode_t215745978_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0 = { 2, GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1609510401_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1609510401_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0_Types[] = { &Enumerator_t1609510401_gp_0_0_0_0, &Enumerator_t1609510401_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0_Types };
extern const Il2CppType KeyEnumerator_t3557860136_gp_0_0_0_0;
extern const Il2CppType KeyEnumerator_t3557860136_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0_Types[] = { &KeyEnumerator_t3557860136_gp_0_0_0_0, &KeyEnumerator_t3557860136_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0 = { 2, GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_Types[] = { &KeyEnumerator_t3557860136_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0 = { 1, GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_Types };
extern const Il2CppType ValueEnumerator_t2021630038_gp_0_0_0_0;
extern const Il2CppType ValueEnumerator_t2021630038_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0_Types[] = { &ValueEnumerator_t2021630038_gp_0_0_0_0, &ValueEnumerator_t2021630038_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0 = { 2, GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0_Types[] = { &ValueEnumerator_t2021630038_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0 = { 1, GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0_Types };
extern const Il2CppType ListKeys_t708716807_gp_0_0_0_0;
extern const Il2CppType ListKeys_t708716807_gp_1_0_0_0;
static const Il2CppType* GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0_Types[] = { &ListKeys_t708716807_gp_0_0_0_0, &ListKeys_t708716807_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0 = { 2, GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListKeys_t708716807_gp_0_0_0_0_Types[] = { &ListKeys_t708716807_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t708716807_gp_0_0_0_0 = { 1, GenInst_ListKeys_t708716807_gp_0_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0_Types };
extern const Il2CppType ListValues_t4054146799_gp_0_0_0_0;
extern const Il2CppType ListValues_t4054146799_gp_1_0_0_0;
static const Il2CppType* GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0_Types[] = { &ListValues_t4054146799_gp_0_0_0_0, &ListValues_t4054146799_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0 = { 2, GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListValues_t4054146799_gp_1_0_0_0_Types[] = { &ListValues_t4054146799_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t4054146799_gp_1_0_0_0 = { 1, GenInst_ListValues_t4054146799_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3522999862_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3522999862_0_0_0_Types[] = { &KeyValuePair_2_t3522999862_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3522999862_0_0_0 = { 1, GenInst_KeyValuePair_2_t3522999862_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0, &U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0 = { 2, GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t533259906_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t533259906_0_0_0_Types[] = { &KeyValuePair_2_t533259906_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t533259906_0_0_0 = { 1, GenInst_KeyValuePair_2_t533259906_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType Tuple_2_t1951933832_gp_0_0_0_0;
extern const Il2CppType Tuple_2_t1951933832_gp_1_0_0_0;
static const Il2CppType* GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0_Types[] = { &Tuple_2_t1951933832_gp_0_0_0_0, &Tuple_2_t1951933832_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0 = { 2, GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2621570726_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2621570726_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m3101579087_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m3101579087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m3999848894_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m4171325764_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m2530741872_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types[] = { &Object_Instantiate_m2530741872_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { &UnityAction_1_t2490859068_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
extern const Il2CppType GenericGenerator_1_t2598493761_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0_Types[] = { &GenericGenerator_1_t2598493761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0 = { 1, GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0_Types };
extern const Il2CppType GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0_Types[] = { &GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0 = { 1, GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2244783541_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2244783541_gp_0_0_0_0_Types[] = { &List_1_t2244783541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2244783541_gp_0_0_0_0 = { 1, GenInst_List_1_t2244783541_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t264254312_0_0_0;
static const Il2CppType* GenInst_List_1_t264254312_0_0_0_Types[] = { &List_1_t264254312_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t264254312_0_0_0 = { 1, GenInst_List_1_t264254312_0_0_0_Types };
extern const Il2CppType DOTween_To_m3442317795_gp_0_0_0_0;
extern const Il2CppType DOTween_To_m3442317795_gp_1_0_0_0;
extern const Il2CppType DOTween_To_m3442317795_gp_2_0_0_0;
static const Il2CppType* GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0_Types[] = { &DOTween_To_m3442317795_gp_0_0_0_0, &DOTween_To_m3442317795_gp_1_0_0_0, &DOTween_To_m3442317795_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0 = { 3, GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_DOTween_To_m3442317795_gp_0_0_0_0_Types[] = { &DOTween_To_m3442317795_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_To_m3442317795_gp_0_0_0_0 = { 1, GenInst_DOTween_To_m3442317795_gp_0_0_0_0_Types };
extern const Il2CppType DOTween_ApplyTo_m4058900771_gp_0_0_0_0;
static const Il2CppType* GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_Types[] = { &DOTween_ApplyTo_m4058900771_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0 = { 1, GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_Types };
extern const Il2CppType DOTween_ApplyTo_m4058900771_gp_1_0_0_0;
extern const Il2CppType DOTween_ApplyTo_m4058900771_gp_2_0_0_0;
static const Il2CppType* GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0_Types[] = { &DOTween_ApplyTo_m4058900771_gp_0_0_0_0, &DOTween_ApplyTo_m4058900771_gp_1_0_0_0, &DOTween_ApplyTo_m4058900771_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0 = { 3, GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0_Types };
extern const Il2CppType Tween_OnTweenCallback_m4208167374_gp_0_0_0_0;
static const Il2CppType* GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0_Types[] = { &Tween_OnTweenCallback_m4208167374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0 = { 1, GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_Setup_m1281370100_gp_0_0_0_0;
extern const Il2CppType Tweener_Setup_m1281370100_gp_1_0_0_0;
extern const Il2CppType Tweener_Setup_m1281370100_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0_Types[] = { &Tweener_Setup_m1281370100_gp_0_0_0_0, &Tweener_Setup_m1281370100_gp_1_0_0_0, &Tweener_Setup_m1281370100_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0 = { 3, GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Types[] = { &Tweener_Setup_m1281370100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0 = { 1, GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0;
extern const Il2CppType Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0;
extern const Il2CppType Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0_Types[] = { &Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0, &Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0, &Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0 = { 3, GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0_Types };
extern const Il2CppType Tweener_DoStartup_m3383447813_gp_0_0_0_0;
extern const Il2CppType Tweener_DoStartup_m3383447813_gp_1_0_0_0;
extern const Il2CppType Tweener_DoStartup_m3383447813_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0_Types[] = { &Tweener_DoStartup_m3383447813_gp_0_0_0_0, &Tweener_DoStartup_m3383447813_gp_1_0_0_0, &Tweener_DoStartup_m3383447813_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0 = { 3, GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Types[] = { &Tweener_DoStartup_m3383447813_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0 = { 1, GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_DoChangeStartValue_m1941438293_gp_0_0_0_0;
extern const Il2CppType Tweener_DoChangeStartValue_m1941438293_gp_1_0_0_0;
extern const Il2CppType Tweener_DoChangeStartValue_m1941438293_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoChangeStartValue_m1941438293_gp_0_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_1_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_2_0_0_0_Types[] = { &Tweener_DoChangeStartValue_m1941438293_gp_0_0_0_0, &Tweener_DoChangeStartValue_m1941438293_gp_1_0_0_0, &Tweener_DoChangeStartValue_m1941438293_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoChangeStartValue_m1941438293_gp_0_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_1_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_2_0_0_0 = { 3, GenInst_Tweener_DoChangeStartValue_m1941438293_gp_0_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_1_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_2_0_0_0_Types };
extern const Il2CppType Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0;
extern const Il2CppType Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0;
extern const Il2CppType Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0_Types[] = { &Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0, &Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0, &Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0 = { 3, GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Types[] = { &Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0 = { 1, GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Types };
extern const Il2CppType Tweener_DoChangeValues_m3618223102_gp_0_0_0_0;
extern const Il2CppType Tweener_DoChangeValues_m3618223102_gp_1_0_0_0;
extern const Il2CppType Tweener_DoChangeValues_m3618223102_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DoChangeValues_m3618223102_gp_0_0_0_0_Tweener_DoChangeValues_m3618223102_gp_1_0_0_0_Tweener_DoChangeValues_m3618223102_gp_2_0_0_0_Types[] = { &Tweener_DoChangeValues_m3618223102_gp_0_0_0_0, &Tweener_DoChangeValues_m3618223102_gp_1_0_0_0, &Tweener_DoChangeValues_m3618223102_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DoChangeValues_m3618223102_gp_0_0_0_0_Tweener_DoChangeValues_m3618223102_gp_1_0_0_0_Tweener_DoChangeValues_m3618223102_gp_2_0_0_0 = { 3, GenInst_Tweener_DoChangeValues_m3618223102_gp_0_0_0_0_Tweener_DoChangeValues_m3618223102_gp_1_0_0_0_Tweener_DoChangeValues_m3618223102_gp_2_0_0_0_Types };
extern const Il2CppType Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0;
extern const Il2CppType Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0;
extern const Il2CppType Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0_Types[] = { &Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0, &Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0, &Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0 = { 3, GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0_Types };
extern const Il2CppType Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0;
extern const Il2CppType Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0;
extern const Il2CppType Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0;
static const Il2CppType* GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0_Types[] = { &Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0, &Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0, &Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0 = { 3, GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0_Types };
extern const Il2CppType IPlugSetter_4_t1442750740_gp_0_0_0_0;
static const Il2CppType* GenInst_IPlugSetter_4_t1442750740_gp_0_0_0_0_Types[] = { &IPlugSetter_4_t1442750740_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IPlugSetter_4_t1442750740_gp_0_0_0_0 = { 1, GenInst_IPlugSetter_4_t1442750740_gp_0_0_0_0_Types };
extern const Il2CppType ABSTweenPlugin_3_t670396699_gp_0_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t670396699_gp_1_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t670396699_gp_2_0_0_0;
static const Il2CppType* GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0_Types[] = { &ABSTweenPlugin_3_t670396699_gp_0_0_0_0, &ABSTweenPlugin_3_t670396699_gp_1_0_0_0, &ABSTweenPlugin_3_t670396699_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0 = { 3, GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_Types[] = { &ABSTweenPlugin_3_t670396699_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0 = { 1, GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_Types };
extern const Il2CppType PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0;
extern const Il2CppType PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0;
extern const Il2CppType PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0;
static const Il2CppType* GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0_Types[] = { &PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0, &PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0, &PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0 = { 3, GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0_Types };
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0;
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0;
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0;
static const Il2CppType* GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0_Types[] = { &PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0, &PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0, &PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0 = { 3, GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0_Types };
extern const Il2CppType PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0;
static const Il2CppType* GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0_Types[] = { &PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0 = { 1, GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_NoFrom_m2168138502_gp_0_0_0_0;
extern const Il2CppType Extensions_NoFrom_m2168138502_gp_1_0_0_0;
extern const Il2CppType Extensions_NoFrom_m2168138502_gp_2_0_0_0;
static const Il2CppType* GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0_Types[] = { &Extensions_NoFrom_m2168138502_gp_0_0_0_0, &Extensions_NoFrom_m2168138502_gp_1_0_0_0, &Extensions_NoFrom_m2168138502_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0 = { 3, GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0_Types };
extern const Il2CppType Extensions_Blendable_m1526939834_gp_0_0_0_0;
extern const Il2CppType Extensions_Blendable_m1526939834_gp_1_0_0_0;
extern const Il2CppType Extensions_Blendable_m1526939834_gp_2_0_0_0;
static const Il2CppType* GenInst_Extensions_Blendable_m1526939834_gp_0_0_0_0_Extensions_Blendable_m1526939834_gp_1_0_0_0_Extensions_Blendable_m1526939834_gp_2_0_0_0_Types[] = { &Extensions_Blendable_m1526939834_gp_0_0_0_0, &Extensions_Blendable_m1526939834_gp_1_0_0_0, &Extensions_Blendable_m1526939834_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_Blendable_m1526939834_gp_0_0_0_0_Extensions_Blendable_m1526939834_gp_1_0_0_0_Extensions_Blendable_m1526939834_gp_2_0_0_0 = { 3, GenInst_Extensions_Blendable_m1526939834_gp_0_0_0_0_Extensions_Blendable_m1526939834_gp_1_0_0_0_Extensions_Blendable_m1526939834_gp_2_0_0_0_Types };
extern const Il2CppType TweenManager_GetTweener_m346233533_gp_0_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m346233533_gp_1_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m346233533_gp_2_0_0_0;
static const Il2CppType* GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0_Types[] = { &TweenManager_GetTweener_m346233533_gp_0_0_0_0, &TweenManager_GetTweener_m346233533_gp_1_0_0_0, &TweenManager_GetTweener_m346233533_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0 = { 3, GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3922192912_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_Types[] = { &TweenerCore_3_t3922192912_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0 = { 1, GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3922192912_gp_1_0_0_0;
extern const Il2CppType TweenerCore_3_t3922192912_gp_2_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0_Types[] = { &TweenerCore_3_t3922192912_gp_0_0_0_0, &TweenerCore_3_t3922192912_gp_1_0_0_0, &TweenerCore_3_t3922192912_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0 = { 3, GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_TweenerCore_3_t3922192912_gp_2_0_0_0_Types[] = { &TweenerCore_3_t3922192912_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3922192912_gp_2_0_0_0 = { 1, GenInst_TweenerCore_3_t3922192912_gp_2_0_0_0_Types };
extern const Il2CppType BlockingCollection_1_t2635972927_gp_0_0_0_0;
static const Il2CppType* GenInst_BlockingCollection_1_t2635972927_gp_0_0_0_0_Types[] = { &BlockingCollection_1_t2635972927_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BlockingCollection_1_t2635972927_gp_0_0_0_0 = { 1, GenInst_BlockingCollection_1_t2635972927_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m1961163955_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2584777480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { &ListPool_1_t1984115411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2000868992_0_0_0;
static const Il2CppType* GenInst_List_1_t2000868992_0_0_0_Types[] = { &List_1_t2000868992_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { &ObjectPool_1_t4265859154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
extern const Il2CppType GAIHandler__buildCustomDimensionsDictionary_m2637270120_gp_0_0_0_0;
static const Il2CppType* GenInst_GAIHandler__buildCustomDimensionsDictionary_m2637270120_gp_0_0_0_0_Types[] = { &GAIHandler__buildCustomDimensionsDictionary_m2637270120_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GAIHandler__buildCustomDimensionsDictionary_m2637270120_gp_0_0_0_0 = { 1, GenInst_GAIHandler__buildCustomDimensionsDictionary_m2637270120_gp_0_0_0_0_Types };
extern const Il2CppType GAIHandler__buildCustomMetricsDictionary_m3851546778_gp_0_0_0_0;
static const Il2CppType* GenInst_GAIHandler__buildCustomMetricsDictionary_m3851546778_gp_0_0_0_0_Types[] = { &GAIHandler__buildCustomMetricsDictionary_m3851546778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GAIHandler__buildCustomMetricsDictionary_m3851546778_gp_0_0_0_0 = { 1, GenInst_GAIHandler__buildCustomMetricsDictionary_m3851546778_gp_0_0_0_0_Types };
extern const Il2CppType GAIHandler__buildCampaignParametersDictionary_m3600528208_gp_0_0_0_0;
static const Il2CppType* GenInst_GAIHandler__buildCampaignParametersDictionary_m3600528208_gp_0_0_0_0_Types[] = { &GAIHandler__buildCampaignParametersDictionary_m3600528208_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GAIHandler__buildCampaignParametersDictionary_m3600528208_gp_0_0_0_0 = { 1, GenInst_GAIHandler__buildCampaignParametersDictionary_m3600528208_gp_0_0_0_0_Types };
extern const Il2CppType HitBuilder_1_t4172956431_gp_0_0_0_0;
static const Il2CppType* GenInst_HitBuilder_1_t4172956431_gp_0_0_0_0_Types[] = { &HitBuilder_1_t4172956431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HitBuilder_1_t4172956431_gp_0_0_0_0 = { 1, GenInst_HitBuilder_1_t4172956431_gp_0_0_0_0_Types };
extern const Il2CppType ObservableDictionary_2_t1567152659_gp_0_0_0_0;
extern const Il2CppType ObservableDictionary_2_t1567152659_gp_1_0_0_0;
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_0_0_0_0, &ObservableDictionary_2_t1567152659_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0 = { 2, GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0 = { 1, GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0 = { 1, GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t512346522_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t512346522_0_0_0_Types[] = { &KeyValuePair_2_t512346522_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t512346522_0_0_0 = { 1, GenInst_KeyValuePair_2_t512346522_0_0_0_Types };
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0_Types[] = { &JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0 = { 1, GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0_Types };
extern const Il2CppType JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0;
extern const Il2CppType JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0;
static const Il2CppType* GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0_Types[] = { &JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0, &JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0 = { 2, GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0_Types };
extern const Il2CppType U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0_Types[] = { &U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0 = { 1, GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0_Types };
extern const Il2CppType U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0;
extern const Il2CppType U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0_Types[] = { &U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0, &U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0 = { 2, GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0_Types };
extern const Il2CppType SmallList_1_t834712290_gp_0_0_0_0;
static const Il2CppType* GenInst_SmallList_1_t834712290_gp_0_0_0_0_Types[] = { &SmallList_1_t834712290_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmallList_1_t834712290_gp_0_0_0_0 = { 1, GenInst_SmallList_1_t834712290_gp_0_0_0_0_Types };
extern const Il2CppType PlayerPrefsUtility_SaveList_m1945505813_gp_0_0_0_0;
static const Il2CppType* GenInst_PlayerPrefsUtility_SaveList_m1945505813_gp_0_0_0_0_Types[] = { &PlayerPrefsUtility_SaveList_m1945505813_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerPrefsUtility_SaveList_m1945505813_gp_0_0_0_0 = { 1, GenInst_PlayerPrefsUtility_SaveList_m1945505813_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2674394750_0_0_0;
static const Il2CppType* GenInst_List_1_t2674394750_0_0_0_Types[] = { &List_1_t2674394750_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2674394750_0_0_0 = { 1, GenInst_List_1_t2674394750_0_0_0_Types };
extern const Il2CppType PlayerPrefsUtility_SaveDict_m2305377966_gp_0_0_0_0;
extern const Il2CppType PlayerPrefsUtility_SaveDict_m2305377966_gp_1_0_0_0;
static const Il2CppType* GenInst_PlayerPrefsUtility_SaveDict_m2305377966_gp_0_0_0_0_PlayerPrefsUtility_SaveDict_m2305377966_gp_1_0_0_0_Types[] = { &PlayerPrefsUtility_SaveDict_m2305377966_gp_0_0_0_0, &PlayerPrefsUtility_SaveDict_m2305377966_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerPrefsUtility_SaveDict_m2305377966_gp_0_0_0_0_PlayerPrefsUtility_SaveDict_m2305377966_gp_1_0_0_0 = { 2, GenInst_PlayerPrefsUtility_SaveDict_m2305377966_gp_0_0_0_0_PlayerPrefsUtility_SaveDict_m2305377966_gp_1_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3552680572_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3552680572_0_0_0_Types[] = { &Dictionary_2_t3552680572_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3552680572_0_0_0 = { 1, GenInst_Dictionary_2_t3552680572_0_0_0_Types };
extern const Il2CppType PlayerPrefsUtility_LoadList_m1363645293_gp_0_0_0_0;
static const Il2CppType* GenInst_PlayerPrefsUtility_LoadList_m1363645293_gp_0_0_0_0_Types[] = { &PlayerPrefsUtility_LoadList_m1363645293_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerPrefsUtility_LoadList_m1363645293_gp_0_0_0_0 = { 1, GenInst_PlayerPrefsUtility_LoadList_m1363645293_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2249969993_0_0_0;
static const Il2CppType* GenInst_List_1_t2249969993_0_0_0_Types[] = { &List_1_t2249969993_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2249969993_0_0_0 = { 1, GenInst_List_1_t2249969993_0_0_0_Types };
extern const Il2CppType PlayerPrefsUtility_LoadDict_m2416465162_gp_0_0_0_0;
extern const Il2CppType PlayerPrefsUtility_LoadDict_m2416465162_gp_1_0_0_0;
static const Il2CppType* GenInst_PlayerPrefsUtility_LoadDict_m2416465162_gp_0_0_0_0_PlayerPrefsUtility_LoadDict_m2416465162_gp_1_0_0_0_Types[] = { &PlayerPrefsUtility_LoadDict_m2416465162_gp_0_0_0_0, &PlayerPrefsUtility_LoadDict_m2416465162_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerPrefsUtility_LoadDict_m2416465162_gp_0_0_0_0_PlayerPrefsUtility_LoadDict_m2416465162_gp_1_0_0_0 = { 2, GenInst_PlayerPrefsUtility_LoadDict_m2416465162_gp_0_0_0_0_PlayerPrefsUtility_LoadDict_m2416465162_gp_1_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3754310832_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3754310832_0_0_0_Types[] = { &Dictionary_2_t3754310832_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3754310832_0_0_0 = { 1, GenInst_Dictionary_2_t3754310832_0_0_0_Types };
extern const Il2CppType Serialization_1_t112460908_gp_0_0_0_0;
static const Il2CppType* GenInst_Serialization_1_t112460908_gp_0_0_0_0_Types[] = { &Serialization_1_t112460908_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Serialization_1_t112460908_gp_0_0_0_0 = { 1, GenInst_Serialization_1_t112460908_gp_0_0_0_0_Types };
extern const Il2CppType Serialization_1_t3382072283_gp_0_0_0_0;
static const Il2CppType* GenInst_Serialization_1_t3382072283_gp_0_0_0_0_Types[] = { &Serialization_1_t3382072283_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Serialization_1_t3382072283_gp_0_0_0_0 = { 1, GenInst_Serialization_1_t3382072283_gp_0_0_0_0_Types };
extern const Il2CppType Serialization_1_t3962975162_gp_0_0_0_0;
static const Il2CppType* GenInst_Serialization_1_t3962975162_gp_0_0_0_0_Types[] = { &Serialization_1_t3962975162_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Serialization_1_t3962975162_gp_0_0_0_0 = { 1, GenInst_Serialization_1_t3962975162_gp_0_0_0_0_Types };
extern const Il2CppType Serialization_1_t335558755_gp_0_0_0_0;
static const Il2CppType* GenInst_Serialization_1_t335558755_gp_0_0_0_0_Types[] = { &Serialization_1_t335558755_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Serialization_1_t335558755_gp_0_0_0_0 = { 1, GenInst_Serialization_1_t335558755_gp_0_0_0_0_Types };
extern const Il2CppType SerializationInWorldActionSetting_1_t2067658136_gp_0_0_0_0;
static const Il2CppType* GenInst_SerializationInWorldActionSetting_1_t2067658136_gp_0_0_0_0_Types[] = { &SerializationInWorldActionSetting_1_t2067658136_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationInWorldActionSetting_1_t2067658136_gp_0_0_0_0 = { 1, GenInst_SerializationInWorldActionSetting_1_t2067658136_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_SafeGetComponent_m3427069941_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_SafeGetComponent_m3427069941_gp_0_0_0_0_Types[] = { &GameObjectExtensions_SafeGetComponent_m3427069941_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_SafeGetComponent_m3427069941_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_SafeGetComponent_m3427069941_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_SafeGetComponent_m3013269245_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_SafeGetComponent_m3013269245_gp_0_0_0_0_Types[] = { &GameObjectExtensions_SafeGetComponent_m3013269245_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_SafeGetComponent_m3013269245_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_SafeGetComponent_m3013269245_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0_Types[] = { &GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0_Types[] = { &GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_RemoveComponent_m2646591243_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_RemoveComponent_m2646591243_gp_0_0_0_0_Types[] = { &GameObjectExtensions_RemoveComponent_m2646591243_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_RemoveComponent_m2646591243_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_RemoveComponent_m2646591243_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_RemoveComponent_m4030745331_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_RemoveComponent_m4030745331_gp_0_0_0_0_Types[] = { &GameObjectExtensions_RemoveComponent_m4030745331_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_RemoveComponent_m4030745331_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_RemoveComponent_m4030745331_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_RemoveComponentImmediate_m2434492150_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_RemoveComponentImmediate_m2434492150_gp_0_0_0_0_Types[] = { &GameObjectExtensions_RemoveComponentImmediate_m2434492150_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_RemoveComponentImmediate_m2434492150_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_RemoveComponentImmediate_m2434492150_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_RemoveComponentImmediate_m1785965088_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_RemoveComponentImmediate_m1785965088_gp_0_0_0_0_Types[] = { &GameObjectExtensions_RemoveComponentImmediate_m1785965088_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_RemoveComponentImmediate_m1785965088_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_RemoveComponentImmediate_m1785965088_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_RemoveComponents_m3543111202_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_RemoveComponents_m3543111202_gp_0_0_0_0_Types[] = { &GameObjectExtensions_RemoveComponents_m3543111202_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_RemoveComponents_m3543111202_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_RemoveComponents_m3543111202_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_RemoveComponents_m324396248_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_RemoveComponents_m324396248_gp_0_0_0_0_Types[] = { &GameObjectExtensions_RemoveComponents_m324396248_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_RemoveComponents_m324396248_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_RemoveComponents_m324396248_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_HasComponent_m1163434775_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_HasComponent_m1163434775_gp_0_0_0_0_Types[] = { &GameObjectExtensions_HasComponent_m1163434775_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_HasComponent_m1163434775_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_HasComponent_m1163434775_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_HasComponent_m286777439_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_HasComponent_m286777439_gp_0_0_0_0_Types[] = { &GameObjectExtensions_HasComponent_m286777439_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_HasComponent_m286777439_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_HasComponent_m286777439_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_FindComponent_m4187209893_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_FindComponent_m4187209893_gp_0_0_0_0_Types[] = { &GameObjectExtensions_FindComponent_m4187209893_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_FindComponent_m4187209893_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_FindComponent_m4187209893_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_FindComponent_m3063844745_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_FindComponent_m3063844745_gp_0_0_0_0_Types[] = { &GameObjectExtensions_FindComponent_m3063844745_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_FindComponent_m3063844745_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_FindComponent_m3063844745_gp_0_0_0_0_Types };
extern const Il2CppType SingletonMonoBehaviour_1_t2064948090_gp_0_0_0_0;
static const Il2CppType* GenInst_SingletonMonoBehaviour_1_t2064948090_gp_0_0_0_0_Types[] = { &SingletonMonoBehaviour_1_t2064948090_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SingletonMonoBehaviour_1_t2064948090_gp_0_0_0_0 = { 1, GenInst_SingletonMonoBehaviour_1_t2064948090_gp_0_0_0_0_Types };
extern const Il2CppType EnumString_1_t696969061_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumString_1_t696969061_gp_0_0_0_0_Types[] = { &EnumString_1_t696969061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumString_1_t696969061_gp_0_0_0_0 = { 1, GenInst_EnumString_1_t696969061_gp_0_0_0_0_Types };
extern const Il2CppType EnumTableCache_1_t3449384724_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumTableCache_1_t3449384724_gp_0_0_0_0_Types[] = { &EnumTableCache_1_t3449384724_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumTableCache_1_t3449384724_gp_0_0_0_0 = { 1, GenInst_EnumTableCache_1_t3449384724_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_EnumTableCache_1_t3449384724_gp_0_0_0_0_Types[] = { &String_t_0_0_0, &EnumTableCache_1_t3449384724_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_EnumTableCache_1_t3449384724_gp_0_0_0_0 = { 2, GenInst_String_t_0_0_0_EnumTableCache_1_t3449384724_gp_0_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t129996502_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t129996502_gp_0_0_0_0_Types[] = { &ObjectPool_1_t129996502_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t129996502_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t129996502_gp_0_0_0_0_Types };
extern const Il2CppType ListPool_1_t2291257409_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t2291257409_gp_0_0_0_0_Types[] = { &ListPool_1_t2291257409_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t2291257409_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t2291257409_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2308010990_0_0_0;
static const Il2CppType* GenInst_List_1_t2308010990_0_0_0_Types[] = { &List_1_t2308010990_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2308010990_0_0_0 = { 1, GenInst_List_1_t2308010990_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t938908698_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t938908698_gp_0_0_0_0_Types[] = { &ObjectPool_1_t938908698_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t938908698_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t938908698_gp_0_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0;
extern const Il2CppType U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0, &U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 = { 2, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0 = { 1, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 = { 1, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0;
extern const Il2CppType U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0, &U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 = { 2, GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_Types[] = { &U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0 = { 1, GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 = { 1, GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0;
extern const Il2CppType U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0;
extern const Il2CppType U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0;
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0, &U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0, &U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 = { 3, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0 = { 1, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0 = { 1, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 = { 1, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { &DefaultExecutionOrder_t2717914595_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
extern const Il2CppType PlayerConnection_t3517219175_0_0_0;
static const Il2CppType* GenInst_PlayerConnection_t3517219175_0_0_0_Types[] = { &PlayerConnection_t3517219175_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3517219175_0_0_0 = { 1, GenInst_PlayerConnection_t3517219175_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
extern const Il2CppType DOTweenComponent_t696744215_0_0_0;
static const Il2CppType* GenInst_DOTweenComponent_t696744215_0_0_0_Types[] = { &DOTweenComponent_t696744215_0_0_0 };
extern const Il2CppGenericInst GenInst_DOTweenComponent_t696744215_0_0_0 = { 1, GenInst_DOTweenComponent_t696744215_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3793077019_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3793077019_0_0_0_Types[] = { &TweenerCore_3_t3793077019_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3793077019_0_0_0 = { 1, GenInst_TweenerCore_3_t3793077019_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t2279406887_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t2279406887_0_0_0_Types[] = { &TweenerCore_3_t2279406887_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t2279406887_0_0_0 = { 1, GenInst_TweenerCore_3_t2279406887_0_0_0_Types };
extern const Il2CppType Sequence_t110643099_0_0_0;
static const Il2CppType* GenInst_Sequence_t110643099_0_0_0_Types[] = { &Sequence_t110643099_0_0_0 };
extern const Il2CppGenericInst GenInst_Sequence_t110643099_0_0_0 = { 1, GenInst_Sequence_t110643099_0_0_0_Types };
extern const Il2CppType PathPlugin_t4171842066_0_0_0;
static const Il2CppType* GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &PathPlugin_t4171842066_0_0_0, &Vector3_t2243707580_0_0_0, &Path_t2828565993_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0 = { 4, GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t2998039394_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t2998039394_0_0_0_Types[] = { &TweenerCore_3_t2998039394_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t2998039394_0_0_0 = { 1, GenInst_TweenerCore_3_t2998039394_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3065187631_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3065187631_0_0_0_Types[] = { &TweenerCore_3_t3065187631_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3065187631_0_0_0 = { 1, GenInst_TweenerCore_3_t3065187631_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3925803634_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3925803634_0_0_0_Types[] = { &TweenerCore_3_t3925803634_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3925803634_0_0_0 = { 1, GenInst_TweenerCore_3_t3925803634_0_0_0_Types };
extern const Il2CppType Tweener_t760404022_0_0_0;
static const Il2CppType* GenInst_Tweener_t760404022_0_0_0_Types[] = { &Tweener_t760404022_0_0_0 };
extern const Il2CppGenericInst GenInst_Tweener_t760404022_0_0_0 = { 1, GenInst_Tweener_t760404022_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3250868854_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3250868854_0_0_0_Types[] = { &TweenerCore_3_t3250868854_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3250868854_0_0_0 = { 1, GenInst_TweenerCore_3_t3250868854_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3261425374_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3261425374_0_0_0_Types[] = { &TweenerCore_3_t3261425374_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3261425374_0_0_0 = { 1, GenInst_TweenerCore_3_t3261425374_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t1672798003_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t1672798003_0_0_0_Types[] = { &TweenerCore_3_t1672798003_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1672798003_0_0_0 = { 1, GenInst_TweenerCore_3_t1672798003_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t102288586_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t102288586_0_0_0_Types[] = { &TweenerCore_3_t102288586_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t102288586_0_0_0 = { 1, GenInst_TweenerCore_3_t102288586_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t1108663466_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t1108663466_0_0_0_Types[] = { &TweenerCore_3_t1108663466_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t1108663466_0_0_0 = { 1, GenInst_TweenerCore_3_t1108663466_0_0_0_Types };
extern const Il2CppType TweenerCore_3_t3035488489_0_0_0;
static const Il2CppType* GenInst_TweenerCore_3_t3035488489_0_0_0_Types[] = { &TweenerCore_3_t3035488489_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenerCore_3_t3035488489_0_0_0 = { 1, GenInst_TweenerCore_3_t3035488489_0_0_0_Types };
extern const Il2CppType FirebaseHandler_t2907300047_0_0_0;
static const Il2CppType* GenInst_FirebaseHandler_t2907300047_0_0_0_Types[] = { &FirebaseHandler_t2907300047_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseHandler_t2907300047_0_0_0 = { 1, GenInst_FirebaseHandler_t2907300047_0_0_0_Types };
extern const Il2CppType SynchronizationContextBehavoir_t692674473_0_0_0;
static const Il2CppType* GenInst_SynchronizationContextBehavoir_t692674473_0_0_0_Types[] = { &SynchronizationContextBehavoir_t692674473_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizationContextBehavoir_t692674473_0_0_0 = { 1, GenInst_SynchronizationContextBehavoir_t692674473_0_0_0_Types };
extern const Il2CppType EventSystem_t3466835263_0_0_0;
static const Il2CppType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { &EventSystem_t3466835263_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { &AxisEventData_t1524870173_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { &SpriteRenderer_t1209076198_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
extern const Il2CppType RawImage_t2749640213_0_0_0;
static const Il2CppType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { &RawImage_t2749640213_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
extern const Il2CppType Scrollbar_t3248359358_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t3248359358_0_0_0_Types[] = { &Scrollbar_t3248359358_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t3248359358_0_0_0 = { 1, GenInst_Scrollbar_t3248359358_0_0_0_Types };
extern const Il2CppType InputField_t1631627530_0_0_0;
static const Il2CppType* GenInst_InputField_t1631627530_0_0_0_Types[] = { &InputField_t1631627530_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t1631627530_0_0_0 = { 1, GenInst_InputField_t1631627530_0_0_0_Types };
extern const Il2CppType ScrollRect_t1199013257_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t1199013257_0_0_0_Types[] = { &ScrollRect_t1199013257_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t1199013257_0_0_0 = { 1, GenInst_ScrollRect_t1199013257_0_0_0_Types };
extern const Il2CppType Dropdown_t1985816271_0_0_0;
static const Il2CppType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { &Dropdown_t1985816271_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { &GraphicRaycaster_t410733016_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { &CanvasRenderer_t261436805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
extern const Il2CppType Corner_t1077473318_0_0_0;
static const Il2CppType* GenInst_Corner_t1077473318_0_0_0_Types[] = { &Corner_t1077473318_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
extern const Il2CppType Axis_t1431825778_0_0_0;
static const Il2CppType* GenInst_Axis_t1431825778_0_0_0_Types[] = { &Axis_t1431825778_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
extern const Il2CppType Constraint_t3558160636_0_0_0;
static const Il2CppType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { &Constraint_t3558160636_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { &SubmitEvent_t907918422_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { &OnChangeEvent_t2863344003_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { &OnValidateInput_t1946318473_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { &LayoutElement_t2808691390_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
extern const Il2CppType TextAnchor_t112990806_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { &TextAnchor_t112990806_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { &AnimationTriggers_t3244928895_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
extern const Il2CppType Animator_t69676727_0_0_0;
static const Il2CppType* GenInst_Animator_t69676727_0_0_0_Types[] = { &Animator_t69676727_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
extern const Il2CppType StandaloneInputModule_t70867863_0_0_0;
static const Il2CppType* GenInst_StandaloneInputModule_t70867863_0_0_0_Types[] = { &StandaloneInputModule_t70867863_0_0_0 };
extern const Il2CppGenericInst GenInst_StandaloneInputModule_t70867863_0_0_0 = { 1, GenInst_StandaloneInputModule_t70867863_0_0_0_Types };
extern const Il2CppType ActionIconList_t2910912037_0_0_0;
static const Il2CppType* GenInst_ActionIconList_t2910912037_0_0_0_Types[] = { &ActionIconList_t2910912037_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionIconList_t2910912037_0_0_0 = { 1, GenInst_ActionIconList_t2910912037_0_0_0_Types };
extern const Il2CppType ActionButtons_t2868854693_0_0_0;
static const Il2CppType* GenInst_ActionButtons_t2868854693_0_0_0_Types[] = { &ActionButtons_t2868854693_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionButtons_t2868854693_0_0_0 = { 1, GenInst_ActionButtons_t2868854693_0_0_0_Types };
extern const Il2CppType Rigidbody_t4233889191_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { &Rigidbody_t4233889191_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
extern const Il2CppType CreateGrid_t2336839420_0_0_0;
static const Il2CppType* GenInst_CreateGrid_t2336839420_0_0_0_Types[] = { &CreateGrid_t2336839420_0_0_0 };
extern const Il2CppGenericInst GenInst_CreateGrid_t2336839420_0_0_0 = { 1, GenInst_CreateGrid_t2336839420_0_0_0_Types };
extern const Il2CppType DoubleTapPartsScaling_t180404427_0_0_0;
static const Il2CppType* GenInst_DoubleTapPartsScaling_t180404427_0_0_0_Types[] = { &DoubleTapPartsScaling_t180404427_0_0_0 };
extern const Il2CppGenericInst GenInst_DoubleTapPartsScaling_t180404427_0_0_0 = { 1, GenInst_DoubleTapPartsScaling_t180404427_0_0_0_Types };
extern const Il2CppType AIMove_t3831276007_0_0_0;
static const Il2CppType* GenInst_AIMove_t3831276007_0_0_0_Types[] = { &AIMove_t3831276007_0_0_0 };
extern const Il2CppGenericInst GenInst_AIMove_t3831276007_0_0_0 = { 1, GenInst_AIMove_t3831276007_0_0_0_Types };
extern const Il2CppType NavigateCaption_t2735808323_0_0_0;
static const Il2CppType* GenInst_NavigateCaption_t2735808323_0_0_0_Types[] = { &NavigateCaption_t2735808323_0_0_0 };
extern const Il2CppGenericInst GenInst_NavigateCaption_t2735808323_0_0_0 = { 1, GenInst_NavigateCaption_t2735808323_0_0_0_Types };
extern const Il2CppType FlagStop_t3791804538_0_0_0;
static const Il2CppType* GenInst_FlagStop_t3791804538_0_0_0_Types[] = { &FlagStop_t3791804538_0_0_0 };
extern const Il2CppGenericInst GenInst_FlagStop_t3791804538_0_0_0 = { 1, GenInst_FlagStop_t3791804538_0_0_0_Types };
extern const Il2CppType Mobilmo_t370754809_0_0_0;
static const Il2CppType* GenInst_Mobilmo_t370754809_0_0_0_Types[] = { &Mobilmo_t370754809_0_0_0 };
extern const Il2CppGenericInst GenInst_Mobilmo_t370754809_0_0_0 = { 1, GenInst_Mobilmo_t370754809_0_0_0_Types };
extern const Il2CppType MiniGameChampionsData_t2446501291_0_0_0;
static const Il2CppType* GenInst_MiniGameChampionsData_t2446501291_0_0_0_Types[] = { &MiniGameChampionsData_t2446501291_0_0_0 };
extern const Il2CppGenericInst GenInst_MiniGameChampionsData_t2446501291_0_0_0 = { 1, GenInst_MiniGameChampionsData_t2446501291_0_0_0_Types };
extern const Il2CppType MobilityItemGo_t3232108924_0_0_0;
static const Il2CppType* GenInst_MobilityItemGo_t3232108924_0_0_0_Types[] = { &MobilityItemGo_t3232108924_0_0_0 };
extern const Il2CppGenericInst GenInst_MobilityItemGo_t3232108924_0_0_0 = { 1, GenInst_MobilityItemGo_t3232108924_0_0_0_Types };
extern const Il2CppType CoreSelect_t1755339513_0_0_0;
static const Il2CppType* GenInst_CoreSelect_t1755339513_0_0_0_Types[] = { &CoreSelect_t1755339513_0_0_0 };
extern const Il2CppGenericInst GenInst_CoreSelect_t1755339513_0_0_0 = { 1, GenInst_CoreSelect_t1755339513_0_0_0_Types };
extern const Il2CppType Animation_t2068071072_0_0_0;
static const Il2CppType* GenInst_Animation_t2068071072_0_0_0_Types[] = { &Animation_t2068071072_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t2068071072_0_0_0 = { 1, GenInst_Animation_t2068071072_0_0_0_Types };
extern const Il2CppType UserSetting_t3323821485_0_0_0;
static const Il2CppType* GenInst_UserSetting_t3323821485_0_0_0_Types[] = { &UserSetting_t3323821485_0_0_0 };
extern const Il2CppGenericInst GenInst_UserSetting_t3323821485_0_0_0 = { 1, GenInst_UserSetting_t3323821485_0_0_0_Types };
extern const Il2CppType FruitsSlider_t2064329978_0_0_0;
static const Il2CppType* GenInst_FruitsSlider_t2064329978_0_0_0_Types[] = { &FruitsSlider_t2064329978_0_0_0 };
extern const Il2CppGenericInst GenInst_FruitsSlider_t2064329978_0_0_0 = { 1, GenInst_FruitsSlider_t2064329978_0_0_0_Types };
extern const Il2CppType HTTPUpdateDelegator_t1331403296_0_0_0;
static const Il2CppType* GenInst_HTTPUpdateDelegator_t1331403296_0_0_0_Types[] = { &HTTPUpdateDelegator_t1331403296_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPUpdateDelegator_t1331403296_0_0_0 = { 1, GenInst_HTTPUpdateDelegator_t1331403296_0_0_0_Types };
extern const Il2CppType Blur_t3313275655_0_0_0;
static const Il2CppType* GenInst_Blur_t3313275655_0_0_0_Types[] = { &Blur_t3313275655_0_0_0 };
extern const Il2CppGenericInst GenInst_Blur_t3313275655_0_0_0 = { 1, GenInst_Blur_t3313275655_0_0_0_Types };
extern const Il2CppType MeshCollider_t2718867283_0_0_0;
static const Il2CppType* GenInst_MeshCollider_t2718867283_0_0_0_Types[] = { &MeshCollider_t2718867283_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshCollider_t2718867283_0_0_0 = { 1, GenInst_MeshCollider_t2718867283_0_0_0_Types };
extern const Il2CppType WaterRipples_t656902750_0_0_0;
static const Il2CppType* GenInst_WaterRipples_t656902750_0_0_0_Types[] = { &WaterRipples_t656902750_0_0_0 };
extern const Il2CppGenericInst GenInst_WaterRipples_t656902750_0_0_0 = { 1, GenInst_WaterRipples_t656902750_0_0_0_Types };
extern const Il2CppType FSEffect_t3187928436_0_0_0;
static const Il2CppType* GenInst_FSEffect_t3187928436_0_0_0_Types[] = { &FSEffect_t3187928436_0_0_0 };
extern const Il2CppGenericInst GenInst_FSEffect_t3187928436_0_0_0 = { 1, GenInst_FSEffect_t3187928436_0_0_0_Types };
extern const Il2CppType CameraFollow_t1493855402_0_0_0;
static const Il2CppType* GenInst_CameraFollow_t1493855402_0_0_0_Types[] = { &CameraFollow_t1493855402_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraFollow_t1493855402_0_0_0 = { 1, GenInst_CameraFollow_t1493855402_0_0_0_Types };
extern const Il2CppType CameraZoom_t1174393664_0_0_0;
static const Il2CppType* GenInst_CameraZoom_t1174393664_0_0_0_Types[] = { &CameraZoom_t1174393664_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraZoom_t1174393664_0_0_0 = { 1, GenInst_CameraZoom_t1174393664_0_0_0_Types };
extern const Il2CppType CameraRotater_t122375930_0_0_0;
static const Il2CppType* GenInst_CameraRotater_t122375930_0_0_0_Types[] = { &CameraRotater_t122375930_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraRotater_t122375930_0_0_0 = { 1, GenInst_CameraRotater_t122375930_0_0_0_Types };
extern const Il2CppType SubCameraHandler_t2753085375_0_0_0;
static const Il2CppType* GenInst_SubCameraHandler_t2753085375_0_0_0_Types[] = { &SubCameraHandler_t2753085375_0_0_0 };
extern const Il2CppGenericInst GenInst_SubCameraHandler_t2753085375_0_0_0 = { 1, GenInst_SubCameraHandler_t2753085375_0_0_0_Types };
extern const Il2CppType CameraSettings_t3536359094_0_0_0;
static const Il2CppType* GenInst_CameraSettings_t3536359094_0_0_0_Types[] = { &CameraSettings_t3536359094_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraSettings_t3536359094_0_0_0 = { 1, GenInst_CameraSettings_t3536359094_0_0_0_Types };
extern const Il2CppType CameraAreaMonitor_t370298340_0_0_0;
static const Il2CppType* GenInst_CameraAreaMonitor_t370298340_0_0_0_Types[] = { &CameraAreaMonitor_t370298340_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraAreaMonitor_t370298340_0_0_0 = { 1, GenInst_CameraAreaMonitor_t370298340_0_0_0_Types };
extern const Il2CppType BlurFilter_t2530667_0_0_0;
static const Il2CppType* GenInst_BlurFilter_t2530667_0_0_0_Types[] = { &BlurFilter_t2530667_0_0_0 };
extern const Il2CppGenericInst GenInst_BlurFilter_t2530667_0_0_0 = { 1, GenInst_BlurFilter_t2530667_0_0_0_Types };
extern const Il2CppType HingeJoint_t2745110831_0_0_0;
static const Il2CppType* GenInst_HingeJoint_t2745110831_0_0_0_Types[] = { &HingeJoint_t2745110831_0_0_0 };
extern const Il2CppGenericInst GenInst_HingeJoint_t2745110831_0_0_0 = { 1, GenInst_HingeJoint_t2745110831_0_0_0_Types };
extern const Il2CppType CharacterController_t4094781467_0_0_0;
static const Il2CppType* GenInst_CharacterController_t4094781467_0_0_0_Types[] = { &CharacterController_t4094781467_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterController_t4094781467_0_0_0 = { 1, GenInst_CharacterController_t4094781467_0_0_0_Types };
extern const Il2CppType CandyBall_t3642802284_0_0_0;
static const Il2CppType* GenInst_CandyBall_t3642802284_0_0_0_Types[] = { &CandyBall_t3642802284_0_0_0 };
extern const Il2CppGenericInst GenInst_CandyBall_t3642802284_0_0_0 = { 1, GenInst_CandyBall_t3642802284_0_0_0_Types };
extern const Il2CppType CarryingBall_t127229952_0_0_0;
static const Il2CppType* GenInst_CarryingBall_t127229952_0_0_0_Types[] = { &CarryingBall_t127229952_0_0_0 };
extern const Il2CppGenericInst GenInst_CarryingBall_t127229952_0_0_0 = { 1, GenInst_CarryingBall_t127229952_0_0_0_Types };
extern const Il2CppType MobPhysics_t1476770297_0_0_0;
static const Il2CppType* GenInst_MobPhysics_t1476770297_0_0_0_Types[] = { &MobPhysics_t1476770297_0_0_0 };
extern const Il2CppGenericInst GenInst_MobPhysics_t1476770297_0_0_0 = { 1, GenInst_MobPhysics_t1476770297_0_0_0_Types };
extern const Il2CppType DebugNormalsInEditmode_t4099272891_0_0_0;
static const Il2CppType* GenInst_DebugNormalsInEditmode_t4099272891_0_0_0_Types[] = { &DebugNormalsInEditmode_t4099272891_0_0_0 };
extern const Il2CppGenericInst GenInst_DebugNormalsInEditmode_t4099272891_0_0_0 = { 1, GenInst_DebugNormalsInEditmode_t4099272891_0_0_0_Types };
extern const Il2CppType MeshFilter_t3026937449_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3026937449_0_0_0_Types[] = { &MeshFilter_t3026937449_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3026937449_0_0_0 = { 1, GenInst_MeshFilter_t3026937449_0_0_0_Types };
extern const Il2CppType MobilmoPanel_t222294755_0_0_0;
static const Il2CppType* GenInst_MobilmoPanel_t222294755_0_0_0_Types[] = { &MobilmoPanel_t222294755_0_0_0 };
extern const Il2CppGenericInst GenInst_MobilmoPanel_t222294755_0_0_0 = { 1, GenInst_MobilmoPanel_t222294755_0_0_0_Types };
extern const Il2CppType ModulePanel_t1015180434_0_0_0;
static const Il2CppType* GenInst_ModulePanel_t1015180434_0_0_0_Types[] = { &ModulePanel_t1015180434_0_0_0 };
extern const Il2CppGenericInst GenInst_ModulePanel_t1015180434_0_0_0 = { 1, GenInst_ModulePanel_t1015180434_0_0_0_Types };
extern const Il2CppType MobilimoPartsConect_t3466715044_0_0_0;
static const Il2CppType* GenInst_MobilimoPartsConect_t3466715044_0_0_0_Types[] = { &MobilimoPartsConect_t3466715044_0_0_0 };
extern const Il2CppGenericInst GenInst_MobilimoPartsConect_t3466715044_0_0_0 = { 1, GenInst_MobilimoPartsConect_t3466715044_0_0_0_Types };
extern const Il2CppType ModulePartsConect_t3833785310_0_0_0;
static const Il2CppType* GenInst_ModulePartsConect_t3833785310_0_0_0_Types[] = { &ModulePartsConect_t3833785310_0_0_0 };
extern const Il2CppGenericInst GenInst_ModulePartsConect_t3833785310_0_0_0 = { 1, GenInst_ModulePartsConect_t3833785310_0_0_0_Types };
extern const Il2CppType ConfigurableJoint_t454307495_0_0_0;
static const Il2CppType* GenInst_ConfigurableJoint_t454307495_0_0_0_Types[] = { &ConfigurableJoint_t454307495_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurableJoint_t454307495_0_0_0 = { 1, GenInst_ConfigurableJoint_t454307495_0_0_0_Types };
extern const Il2CppType Buoyancy_t2565452554_0_0_0;
static const Il2CppType* GenInst_Buoyancy_t2565452554_0_0_0_Types[] = { &Buoyancy_t2565452554_0_0_0 };
extern const Il2CppGenericInst GenInst_Buoyancy_t2565452554_0_0_0 = { 1, GenInst_Buoyancy_t2565452554_0_0_0_Types };
extern const Il2CppType CombineChildrenAFS_t3843368254_0_0_0;
static const Il2CppType* GenInst_CombineChildrenAFS_t3843368254_0_0_0_Types[] = { &CombineChildrenAFS_t3843368254_0_0_0 };
extern const Il2CppGenericInst GenInst_CombineChildrenAFS_t3843368254_0_0_0 = { 1, GenInst_CombineChildrenAFS_t3843368254_0_0_0_Types };
extern const Il2CppType Projector_t1121484678_0_0_0;
static const Il2CppType* GenInst_Projector_t1121484678_0_0_0_Types[] = { &Projector_t1121484678_0_0_0 };
extern const Il2CppGenericInst GenInst_Projector_t1121484678_0_0_0 = { 1, GenInst_Projector_t1121484678_0_0_0_Types };
extern const Il2CppType ProjectorMatrix_t3999868417_0_0_0;
static const Il2CppType* GenInst_ProjectorMatrix_t3999868417_0_0_0_Types[] = { &ProjectorMatrix_t3999868417_0_0_0 };
extern const Il2CppGenericInst GenInst_ProjectorMatrix_t3999868417_0_0_0 = { 1, GenInst_ProjectorMatrix_t3999868417_0_0_0_Types };
extern const Il2CppType PreloadingManager_t3491463554_0_0_0;
static const Il2CppType* GenInst_PreloadingManager_t3491463554_0_0_0_Types[] = { &PreloadingManager_t3491463554_0_0_0 };
extern const Il2CppGenericInst GenInst_PreloadingManager_t3491463554_0_0_0 = { 1, GenInst_PreloadingManager_t3491463554_0_0_0_Types };
extern const Il2CppType VerticalLayoutGroup_t2468316403_0_0_0;
static const Il2CppType* GenInst_VerticalLayoutGroup_t2468316403_0_0_0_Types[] = { &VerticalLayoutGroup_t2468316403_0_0_0 };
extern const Il2CppGenericInst GenInst_VerticalLayoutGroup_t2468316403_0_0_0 = { 1, GenInst_VerticalLayoutGroup_t2468316403_0_0_0_Types };
extern const Il2CppType HorizontalLayoutGroup_t2875670365_0_0_0;
static const Il2CppType* GenInst_HorizontalLayoutGroup_t2875670365_0_0_0_Types[] = { &HorizontalLayoutGroup_t2875670365_0_0_0 };
extern const Il2CppGenericInst GenInst_HorizontalLayoutGroup_t2875670365_0_0_0 = { 1, GenInst_HorizontalLayoutGroup_t2875670365_0_0_0_Types };
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t1968298610_0_0_0;
static const Il2CppType* GenInst_HorizontalOrVerticalLayoutGroup_t1968298610_0_0_0_Types[] = { &HorizontalOrVerticalLayoutGroup_t1968298610_0_0_0 };
extern const Il2CppGenericInst GenInst_HorizontalOrVerticalLayoutGroup_t1968298610_0_0_0 = { 1, GenInst_HorizontalOrVerticalLayoutGroup_t1968298610_0_0_0_Types };
extern const Il2CppType LensFlare_t529161798_0_0_0;
static const Il2CppType* GenInst_LensFlare_t529161798_0_0_0_Types[] = { &LensFlare_t529161798_0_0_0 };
extern const Il2CppGenericInst GenInst_LensFlare_t529161798_0_0_0 = { 1, GenInst_LensFlare_t529161798_0_0_0_Types };
extern const Il2CppType FSPlanet_t2551900013_0_0_0;
static const Il2CppType* GenInst_FSPlanet_t2551900013_0_0_0_Types[] = { &FSPlanet_t2551900013_0_0_0 };
extern const Il2CppGenericInst GenInst_FSPlanet_t2551900013_0_0_0 = { 1, GenInst_FSPlanet_t2551900013_0_0_0_Types };
extern const Il2CppType SphereCollider_t1662511355_0_0_0;
static const Il2CppType* GenInst_SphereCollider_t1662511355_0_0_0_Types[] = { &SphereCollider_t1662511355_0_0_0 };
extern const Il2CppGenericInst GenInst_SphereCollider_t1662511355_0_0_0 = { 1, GenInst_SphereCollider_t1662511355_0_0_0_Types };
extern const Il2CppType ActionLever_t131660742_0_0_0;
static const Il2CppType* GenInst_ActionLever_t131660742_0_0_0_Types[] = { &ActionLever_t131660742_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionLever_t131660742_0_0_0 = { 1, GenInst_ActionLever_t131660742_0_0_0_Types };
extern const Il2CppType WaterRipplesHouse_t1929798042_0_0_0;
static const Il2CppType* GenInst_WaterRipplesHouse_t1929798042_0_0_0_Types[] = { &WaterRipplesHouse_t1929798042_0_0_0 };
extern const Il2CppGenericInst GenInst_WaterRipplesHouse_t1929798042_0_0_0 = { 1, GenInst_WaterRipplesHouse_t1929798042_0_0_0_Types };
extern const Il2CppType MiniGameChara_t2588887264_0_0_0;
static const Il2CppType* GenInst_MiniGameChara_t2588887264_0_0_0_Types[] = { &MiniGameChara_t2588887264_0_0_0 };
extern const Il2CppGenericInst GenInst_MiniGameChara_t2588887264_0_0_0 = { 1, GenInst_MiniGameChara_t2588887264_0_0_0_Types };
extern const Il2CppType TimeAttack_t1975281095_0_0_0;
static const Il2CppType* GenInst_TimeAttack_t1975281095_0_0_0_Types[] = { &TimeAttack_t1975281095_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeAttack_t1975281095_0_0_0 = { 1, GenInst_TimeAttack_t1975281095_0_0_0_Types };
extern const Il2CppType PlanetGravity_t2098207264_0_0_0;
static const Il2CppType* GenInst_PlanetGravity_t2098207264_0_0_0_Types[] = { &PlanetGravity_t2098207264_0_0_0 };
extern const Il2CppGenericInst GenInst_PlanetGravity_t2098207264_0_0_0 = { 1, GenInst_PlanetGravity_t2098207264_0_0_0_Types };
extern const Il2CppType ActionObj_t2993311507_0_0_0;
static const Il2CppType* GenInst_ActionObj_t2993311507_0_0_0_Types[] = { &ActionObj_t2993311507_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionObj_t2993311507_0_0_0 = { 1, GenInst_ActionObj_t2993311507_0_0_0_Types };
extern const Il2CppType RaderCirclesMover_t2837149066_0_0_0;
static const Il2CppType* GenInst_RaderCirclesMover_t2837149066_0_0_0_Types[] = { &RaderCirclesMover_t2837149066_0_0_0 };
extern const Il2CppGenericInst GenInst_RaderCirclesMover_t2837149066_0_0_0 = { 1, GenInst_RaderCirclesMover_t2837149066_0_0_0_Types };
extern const Il2CppType PlayerInfoLike_t852303790_0_0_0;
static const Il2CppType* GenInst_PlayerInfoLike_t852303790_0_0_0_Types[] = { &PlayerInfoLike_t852303790_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerInfoLike_t852303790_0_0_0 = { 1, GenInst_PlayerInfoLike_t852303790_0_0_0_Types };
extern const Il2CppType EditMobilmo_t2535788613_0_0_0;
static const Il2CppType* GenInst_EditMobilmo_t2535788613_0_0_0_Types[] = { &EditMobilmo_t2535788613_0_0_0 };
extern const Il2CppGenericInst GenInst_EditMobilmo_t2535788613_0_0_0 = { 1, GenInst_EditMobilmo_t2535788613_0_0_0_Types };
extern const Il2CppType EllipsoidParticleEmitter_t3972767653_0_0_0;
static const Il2CppType* GenInst_EllipsoidParticleEmitter_t3972767653_0_0_0_Types[] = { &EllipsoidParticleEmitter_t3972767653_0_0_0 };
extern const Il2CppGenericInst GenInst_EllipsoidParticleEmitter_t3972767653_0_0_0 = { 1, GenInst_EllipsoidParticleEmitter_t3972767653_0_0_0_Types };
extern const Il2CppType MobilmoDnaController_t1006271200_0_0_0;
static const Il2CppType* GenInst_MobilmoDnaController_t1006271200_0_0_0_Types[] = { &MobilmoDnaController_t1006271200_0_0_0 };
extern const Il2CppGenericInst GenInst_MobilmoDnaController_t1006271200_0_0_0 = { 1, GenInst_MobilmoDnaController_t1006271200_0_0_0_Types };
extern const Il2CppType LineRenderer_t849157671_0_0_0;
static const Il2CppType* GenInst_LineRenderer_t849157671_0_0_0_Types[] = { &LineRenderer_t849157671_0_0_0 };
extern const Il2CppGenericInst GenInst_LineRenderer_t849157671_0_0_0 = { 1, GenInst_LineRenderer_t849157671_0_0_0_Types };
extern const Il2CppType PlayerInfoPanel_t97551129_0_0_0;
static const Il2CppType* GenInst_PlayerInfoPanel_t97551129_0_0_0_Types[] = { &PlayerInfoPanel_t97551129_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerInfoPanel_t97551129_0_0_0 = { 1, GenInst_PlayerInfoPanel_t97551129_0_0_0_Types };
extern const Il2CppType CrossSection_t582606797_0_0_0;
static const Il2CppType* GenInst_CrossSection_t582606797_0_0_0_Types[] = { &CrossSection_t582606797_0_0_0 };
extern const Il2CppGenericInst GenInst_CrossSection_t582606797_0_0_0 = { 1, GenInst_CrossSection_t582606797_0_0_0_Types };
extern const Il2CppType SuccessfulCircles_t3791497435_0_0_0;
static const Il2CppType* GenInst_SuccessfulCircles_t3791497435_0_0_0_Types[] = { &SuccessfulCircles_t3791497435_0_0_0 };
extern const Il2CppGenericInst GenInst_SuccessfulCircles_t3791497435_0_0_0 = { 1, GenInst_SuccessfulCircles_t3791497435_0_0_0_Types };
extern const Il2CppType SystemInputCircles_t2127672316_0_0_0;
static const Il2CppType* GenInst_SystemInputCircles_t2127672316_0_0_0_Types[] = { &SystemInputCircles_t2127672316_0_0_0 };
extern const Il2CppGenericInst GenInst_SystemInputCircles_t2127672316_0_0_0 = { 1, GenInst_SystemInputCircles_t2127672316_0_0_0_Types };
extern const Il2CppType MiniPlanetRotator_t4060345590_0_0_0;
static const Il2CppType* GenInst_MiniPlanetRotator_t4060345590_0_0_0_Types[] = { &MiniPlanetRotator_t4060345590_0_0_0 };
extern const Il2CppGenericInst GenInst_MiniPlanetRotator_t4060345590_0_0_0 = { 1, GenInst_MiniPlanetRotator_t4060345590_0_0_0_Types };
extern const Il2CppType TitleAnimator_t1342483085_0_0_0;
static const Il2CppType* GenInst_TitleAnimator_t1342483085_0_0_0_Types[] = { &TitleAnimator_t1342483085_0_0_0 };
extern const Il2CppGenericInst GenInst_TitleAnimator_t1342483085_0_0_0 = { 1, GenInst_TitleAnimator_t1342483085_0_0_0_Types };
extern const Il2CppType touchBendingPlayerListener_t2574995411_0_0_0;
static const Il2CppType* GenInst_touchBendingPlayerListener_t2574995411_0_0_0_Types[] = { &touchBendingPlayerListener_t2574995411_0_0_0 };
extern const Il2CppGenericInst GenInst_touchBendingPlayerListener_t2574995411_0_0_0 = { 1, GenInst_touchBendingPlayerListener_t2574995411_0_0_0_Types };
extern const Il2CppType Joystick_t2144252492_0_0_0;
static const Il2CppType* GenInst_Joystick_t2144252492_0_0_0_Types[] = { &Joystick_t2144252492_0_0_0 };
extern const Il2CppGenericInst GenInst_Joystick_t2144252492_0_0_0 = { 1, GenInst_Joystick_t2144252492_0_0_0_Types };
extern const Il2CppType HelloWorldPanel_t280472022_0_0_0;
static const Il2CppType* GenInst_HelloWorldPanel_t280472022_0_0_0_Types[] = { &HelloWorldPanel_t280472022_0_0_0 };
extern const Il2CppGenericInst GenInst_HelloWorldPanel_t280472022_0_0_0 = { 1, GenInst_HelloWorldPanel_t280472022_0_0_0_Types };
extern const Il2CppType TalkTextFader_t1511400589_0_0_0;
static const Il2CppType* GenInst_TalkTextFader_t1511400589_0_0_0_Types[] = { &TalkTextFader_t1511400589_0_0_0 };
extern const Il2CppGenericInst GenInst_TalkTextFader_t1511400589_0_0_0 = { 1, GenInst_TalkTextFader_t1511400589_0_0_0_Types };
extern const Il2CppType TalkPanel_t2596865392_0_0_0;
static const Il2CppType* GenInst_TalkPanel_t2596865392_0_0_0_Types[] = { &TalkPanel_t2596865392_0_0_0 };
extern const Il2CppGenericInst GenInst_TalkPanel_t2596865392_0_0_0 = { 1, GenInst_TalkPanel_t2596865392_0_0_0_Types };
extern const Il2CppType RentalMobilmoContent_t501695126_0_0_0;
static const Il2CppType* GenInst_RentalMobilmoContent_t501695126_0_0_0_Types[] = { &RentalMobilmoContent_t501695126_0_0_0 };
extern const Il2CppGenericInst GenInst_RentalMobilmoContent_t501695126_0_0_0 = { 1, GenInst_RentalMobilmoContent_t501695126_0_0_0_Types };
extern const Il2CppType RegexHypertext_t2320066936_0_0_0;
static const Il2CppType* GenInst_RegexHypertext_t2320066936_0_0_0_Types[] = { &RegexHypertext_t2320066936_0_0_0 };
extern const Il2CppGenericInst GenInst_RegexHypertext_t2320066936_0_0_0 = { 1, GenInst_RegexHypertext_t2320066936_0_0_0_Types };
extern const Il2CppType LetterSpacing_t3080148137_0_0_0;
static const Il2CppType* GenInst_LetterSpacing_t3080148137_0_0_0_Types[] = { &LetterSpacing_t3080148137_0_0_0 };
extern const Il2CppGenericInst GenInst_LetterSpacing_t3080148137_0_0_0 = { 1, GenInst_LetterSpacing_t3080148137_0_0_0_Types };
extern const Il2CppType RecordMobility_t2685217900_0_0_0;
static const Il2CppType* GenInst_RecordMobility_t2685217900_0_0_0_Types[] = { &RecordMobility_t2685217900_0_0_0 };
extern const Il2CppGenericInst GenInst_RecordMobility_t2685217900_0_0_0 = { 1, GenInst_RecordMobility_t2685217900_0_0_0_Types };
extern const Il2CppType Lever_t866853776_0_0_0;
static const Il2CppType* GenInst_Lever_t866853776_0_0_0_Types[] = { &Lever_t866853776_0_0_0 };
extern const Il2CppGenericInst GenInst_Lever_t866853776_0_0_0 = { 1, GenInst_Lever_t866853776_0_0_0_Types };
extern const Il2CppType SunShafts_t482045181_0_0_0;
static const Il2CppType* GenInst_SunShafts_t482045181_0_0_0_Types[] = { &SunShafts_t482045181_0_0_0 };
extern const Il2CppGenericInst GenInst_SunShafts_t482045181_0_0_0 = { 1, GenInst_SunShafts_t482045181_0_0_0_Types };
extern const Il2CppType Underwater_t1731910439_0_0_0;
static const Il2CppType* GenInst_Underwater_t1731910439_0_0_0_Types[] = { &Underwater_t1731910439_0_0_0 };
extern const Il2CppGenericInst GenInst_Underwater_t1731910439_0_0_0 = { 1, GenInst_Underwater_t1731910439_0_0_0_Types };
extern const Il2CppType Skybox_t2033495038_0_0_0;
static const Il2CppType* GenInst_Skybox_t2033495038_0_0_0_Types[] = { &Skybox_t2033495038_0_0_0 };
extern const Il2CppGenericInst GenInst_Skybox_t2033495038_0_0_0 = { 1, GenInst_Skybox_t2033495038_0_0_0_Types };
extern const Il2CppType FlareLayer_t1985082419_0_0_0;
static const Il2CppType* GenInst_FlareLayer_t1985082419_0_0_0_Types[] = { &FlareLayer_t1985082419_0_0_0 };
extern const Il2CppGenericInst GenInst_FlareLayer_t1985082419_0_0_0 = { 1, GenInst_FlareLayer_t1985082419_0_0_0_Types };
extern const Il2CppType BackgroundFader_t837055620_0_0_0;
static const Il2CppType* GenInst_BackgroundFader_t837055620_0_0_0_Types[] = { &BackgroundFader_t837055620_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundFader_t837055620_0_0_0 = { 1, GenInst_BackgroundFader_t837055620_0_0_0_Types };
extern const Il2CppType NamePanel_t546707703_0_0_0;
static const Il2CppType* GenInst_NamePanel_t546707703_0_0_0_Types[] = { &NamePanel_t546707703_0_0_0 };
extern const Il2CppGenericInst GenInst_NamePanel_t546707703_0_0_0 = { 1, GenInst_NamePanel_t546707703_0_0_0_Types };
extern const Il2CppType InputMarkAnimator_t3461548916_0_0_0;
static const Il2CppType* GenInst_InputMarkAnimator_t3461548916_0_0_0_Types[] = { &InputMarkAnimator_t3461548916_0_0_0 };
extern const Il2CppGenericInst GenInst_InputMarkAnimator_t3461548916_0_0_0 = { 1, GenInst_InputMarkAnimator_t3461548916_0_0_0_Types };
extern const Il2CppType RegisterButtonCrossFader_t205138403_0_0_0;
static const Il2CppType* GenInst_RegisterButtonCrossFader_t205138403_0_0_0_Types[] = { &RegisterButtonCrossFader_t205138403_0_0_0 };
extern const Il2CppGenericInst GenInst_RegisterButtonCrossFader_t205138403_0_0_0 = { 1, GenInst_RegisterButtonCrossFader_t205138403_0_0_0_Types };
extern const Il2CppType MeshCreatorSTL_t2712395408_0_0_0;
static const Il2CppType* GenInst_MeshCreatorSTL_t2712395408_0_0_0_Types[] = { &MeshCreatorSTL_t2712395408_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshCreatorSTL_t2712395408_0_0_0 = { 1, GenInst_MeshCreatorSTL_t2712395408_0_0_0_Types };
extern const Il2CppType AreaGround_t2255653338_0_0_0;
static const Il2CppType* GenInst_AreaGround_t2255653338_0_0_0_Types[] = { &AreaGround_t2255653338_0_0_0 };
extern const Il2CppGenericInst GenInst_AreaGround_t2255653338_0_0_0 = { 1, GenInst_AreaGround_t2255653338_0_0_0_Types };
extern const Il2CppType FeedbackEffect_t3658613102_0_0_0;
static const Il2CppType* GenInst_FeedbackEffect_t3658613102_0_0_0_Types[] = { &FeedbackEffect_t3658613102_0_0_0 };
extern const Il2CppGenericInst GenInst_FeedbackEffect_t3658613102_0_0_0 = { 1, GenInst_FeedbackEffect_t3658613102_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0, &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0_Types };
static const Il2CppType* GenInst_ClickableEntry_t4031026959_0_0_0_ClickableEntry_t4031026959_0_0_0_Types[] = { &ClickableEntry_t4031026959_0_0_0, &ClickableEntry_t4031026959_0_0_0 };
extern const Il2CppGenericInst GenInst_ClickableEntry_t4031026959_0_0_0_ClickableEntry_t4031026959_0_0_0 = { 2, GenInst_ClickableEntry_t4031026959_0_0_0_ClickableEntry_t4031026959_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &PropertyMetadata_t3693826136_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 2, GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0, &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0, &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0 = { 2, GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_Types };
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0, &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0 = { 2, GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0, &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0, &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0_Types[] = { &AnimatorClipInfo_t3905751349_0_0_0, &AnimatorClipInfo_t3905751349_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0 = { 2, GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0, &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0, &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const Il2CppType* GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_Types[] = { &Rect_t3681755626_0_0_0, &Rect_t3681755626_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0 = { 2, GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0, &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0, &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0, &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const Il2CppType* GenInst_eAREATYPE_t1739175762_0_0_0_eAREATYPE_t1739175762_0_0_0_Types[] = { &eAREATYPE_t1739175762_0_0_0, &eAREATYPE_t1739175762_0_0_0 };
extern const Il2CppGenericInst GenInst_eAREATYPE_t1739175762_0_0_0_eAREATYPE_t1739175762_0_0_0 = { 2, GenInst_eAREATYPE_t1739175762_0_0_0_eAREATYPE_t1739175762_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1025746566_0_0_0_KeyValuePair_2_t1025746566_0_0_0_Types[] = { &KeyValuePair_2_t1025746566_0_0_0, &KeyValuePair_2_t1025746566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1025746566_0_0_0_KeyValuePair_2_t1025746566_0_0_0 = { 2, GenInst_KeyValuePair_2_t1025746566_0_0_0_KeyValuePair_2_t1025746566_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1025746566_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1025746566_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1025746566_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1025746566_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOBGM_t4173725093_0_0_0_eAUDIOBGM_t4173725093_0_0_0_Types[] = { &eAUDIOBGM_t4173725093_0_0_0, &eAUDIOBGM_t4173725093_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOBGM_t4173725093_0_0_0_eAUDIOBGM_t4173725093_0_0_0 = { 2, GenInst_eAUDIOBGM_t4173725093_0_0_0_eAUDIOBGM_t4173725093_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t98719111_0_0_0_KeyValuePair_2_t98719111_0_0_0_Types[] = { &KeyValuePair_2_t98719111_0_0_0, &KeyValuePair_2_t98719111_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t98719111_0_0_0_KeyValuePair_2_t98719111_0_0_0 = { 2, GenInst_KeyValuePair_2_t98719111_0_0_0_KeyValuePair_2_t98719111_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t98719111_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t98719111_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t98719111_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t98719111_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOSE_t638655723_0_0_0_eAUDIOSE_t638655723_0_0_0_Types[] = { &eAUDIOSE_t638655723_0_0_0, &eAUDIOSE_t638655723_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOSE_t638655723_0_0_0_eAUDIOSE_t638655723_0_0_0 = { 2, GenInst_eAUDIOSE_t638655723_0_0_0_eAUDIOSE_t638655723_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2169266793_0_0_0_KeyValuePair_2_t2169266793_0_0_0_Types[] = { &KeyValuePair_2_t2169266793_0_0_0, &KeyValuePair_2_t2169266793_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2169266793_0_0_0_KeyValuePair_2_t2169266793_0_0_0 = { 2, GenInst_KeyValuePair_2_t2169266793_0_0_0_KeyValuePair_2_t2169266793_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2169266793_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2169266793_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2169266793_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2169266793_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eAUDIOVOICE_t2836738047_0_0_0_eAUDIOVOICE_t2836738047_0_0_0_Types[] = { &eAUDIOVOICE_t2836738047_0_0_0, &eAUDIOVOICE_t2836738047_0_0_0 };
extern const Il2CppGenericInst GenInst_eAUDIOVOICE_t2836738047_0_0_0_eAUDIOVOICE_t2836738047_0_0_0 = { 2, GenInst_eAUDIOVOICE_t2836738047_0_0_0_eAUDIOVOICE_t2836738047_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4217592965_0_0_0_KeyValuePair_2_t4217592965_0_0_0_Types[] = { &KeyValuePair_2_t4217592965_0_0_0, &KeyValuePair_2_t4217592965_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4217592965_0_0_0_KeyValuePair_2_t4217592965_0_0_0 = { 2, GenInst_KeyValuePair_2_t4217592965_0_0_0_KeyValuePair_2_t4217592965_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4217592965_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4217592965_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4217592965_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4217592965_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eEventID_t711132804_0_0_0_eEventID_t711132804_0_0_0_Types[] = { &eEventID_t711132804_0_0_0, &eEventID_t711132804_0_0_0 };
extern const Il2CppGenericInst GenInst_eEventID_t711132804_0_0_0_eEventID_t711132804_0_0_0 = { 2, GenInst_eEventID_t711132804_0_0_0_eEventID_t711132804_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3835431500_0_0_0_KeyValuePair_2_t3835431500_0_0_0_Types[] = { &KeyValuePair_2_t3835431500_0_0_0, &KeyValuePair_2_t3835431500_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3835431500_0_0_0_KeyValuePair_2_t3835431500_0_0_0 = { 2, GenInst_KeyValuePair_2_t3835431500_0_0_0_KeyValuePair_2_t3835431500_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3835431500_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3835431500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3835431500_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3835431500_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_eLIGHTTYPE_t1469766701_0_0_0_eLIGHTTYPE_t1469766701_0_0_0_Types[] = { &eLIGHTTYPE_t1469766701_0_0_0, &eLIGHTTYPE_t1469766701_0_0_0 };
extern const Il2CppGenericInst GenInst_eLIGHTTYPE_t1469766701_0_0_0_eLIGHTTYPE_t1469766701_0_0_0 = { 2, GenInst_eLIGHTTYPE_t1469766701_0_0_0_eLIGHTTYPE_t1469766701_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t194954207_0_0_0_KeyValuePair_2_t194954207_0_0_0_Types[] = { &KeyValuePair_2_t194954207_0_0_0, &KeyValuePair_2_t194954207_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t194954207_0_0_0_KeyValuePair_2_t194954207_0_0_0 = { 2, GenInst_KeyValuePair_2_t194954207_0_0_0_KeyValuePair_2_t194954207_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t194954207_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t194954207_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t194954207_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t194954207_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0_Types[] = { &LocalizeKey_t3348421234_0_0_0, &LocalizeKey_t3348421234_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0 = { 2, GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3838112486_0_0_0_KeyValuePair_2_t3838112486_0_0_0_Types[] = { &KeyValuePair_2_t3838112486_0_0_0, &KeyValuePair_2_t3838112486_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3838112486_0_0_0_KeyValuePair_2_t3838112486_0_0_0 = { 2, GenInst_KeyValuePair_2_t3838112486_0_0_0_KeyValuePair_2_t3838112486_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3838112486_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3838112486_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3838112486_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3838112486_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_SCORETYPE_t2692914394_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0, &SCORETYPE_t2692914394_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0_SCORETYPE_t2692914394_0_0_0 = { 2, GenInst_SCORETYPE_t2692914394_0_0_0_SCORETYPE_t2692914394_0_0_0_Types };
static const Il2CppType* GenInst_SCORETYPE_t2692914394_0_0_0_Il2CppObject_0_0_0_Types[] = { &SCORETYPE_t2692914394_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_SCORETYPE_t2692914394_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_SCORETYPE_t2692914394_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t37182331_0_0_0_KeyValuePair_2_t37182331_0_0_0_Types[] = { &KeyValuePair_2_t37182331_0_0_0, &KeyValuePair_2_t37182331_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t37182331_0_0_0_KeyValuePair_2_t37182331_0_0_0 = { 2, GenInst_KeyValuePair_2_t37182331_0_0_0_KeyValuePair_2_t37182331_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t37182331_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t37182331_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t37182331_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t37182331_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types[] = { &KeyValuePair_2_t271247988_0_0_0, &KeyValuePair_2_t271247988_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0 = { 2, GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t271247988_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0, &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &ArrayMetadata_t2008834462_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 2, GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types[] = { &ArrayMetadata_t2008834462_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types[] = { &KeyValuePair_2_t3653207108_0_0_0, &KeyValuePair_2_t3653207108_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3653207108_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &ObjectMetadata_t3995922398_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 2, GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types[] = { &ObjectMetadata_t3995922398_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types[] = { &KeyValuePair_2_t1345327748_0_0_0, &KeyValuePair_2_t1345327748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0 = { 2, GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1345327748_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types[] = { &PropertyMetadata_t3693826136_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types[] = { &KeyValuePair_2_t1043231486_0_0_0, &KeyValuePair_2_t1043231486_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0 = { 2, GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1043231486_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t697826584_0_0_0_KeyValuePair_2_t697826584_0_0_0_Types[] = { &KeyValuePair_2_t697826584_0_0_0, &KeyValuePair_2_t697826584_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t697826584_0_0_0_KeyValuePair_2_t697826584_0_0_0 = { 2, GenInst_KeyValuePair_2_t697826584_0_0_0_KeyValuePair_2_t697826584_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t697826584_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t697826584_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t697826584_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t697826584_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0_Types[] = { &Entry_t1526823907_0_0_0, &Entry_t1526823907_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0 = { 2, GenInst_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0_Types };
static const Il2CppType* GenInst_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0_Types[] = { &Entry_t1526823907_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3171196553_0_0_0_KeyValuePair_2_t3171196553_0_0_0_Types[] = { &KeyValuePair_2_t3171196553_0_0_0, &KeyValuePair_2_t3171196553_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3171196553_0_0_0_KeyValuePair_2_t3171196553_0_0_0 = { 2, GenInst_KeyValuePair_2_t3171196553_0_0_0_KeyValuePair_2_t3171196553_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3171196553_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3171196553_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3171196553_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3171196553_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types[] = { &ClientMessage_t624279968_0_0_0, &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0 = { 2, GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types };
static const Il2CppType* GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0_Types[] = { &ClientMessage_t624279968_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types[] = { &KeyValuePair_2_t322707255_0_0_0, &KeyValuePair_2_t322707255_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0 = { 2, GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t322707255_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types[] = { &KeyValuePair_2_t2387876582_0_0_0, &KeyValuePair_2_t2387876582_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0 = { 2, GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2387876582_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ColorOptions_t2213017305_0_0_0_Types[] = { &ColorOptions_t2213017305_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorOptions_t2213017305_0_0_0 = { 1, GenInst_ColorOptions_t2213017305_0_0_0_Types };
static const Il2CppType* GenInst_FloatOptions_t1421548266_0_0_0_Types[] = { &FloatOptions_t1421548266_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatOptions_t1421548266_0_0_0 = { 1, GenInst_FloatOptions_t1421548266_0_0_0_Types };
static const Il2CppType* GenInst_NoOptions_t2508431845_0_0_0_Types[] = { &NoOptions_t2508431845_0_0_0 };
extern const Il2CppGenericInst GenInst_NoOptions_t2508431845_0_0_0 = { 1, GenInst_NoOptions_t2508431845_0_0_0_Types };
static const Il2CppType* GenInst_PathOptions_t2659884781_0_0_0_Types[] = { &PathOptions_t2659884781_0_0_0 };
extern const Il2CppGenericInst GenInst_PathOptions_t2659884781_0_0_0 = { 1, GenInst_PathOptions_t2659884781_0_0_0_Types };
static const Il2CppType* GenInst_QuaternionOptions_t466049668_0_0_0_Types[] = { &QuaternionOptions_t466049668_0_0_0 };
extern const Il2CppGenericInst GenInst_QuaternionOptions_t466049668_0_0_0 = { 1, GenInst_QuaternionOptions_t466049668_0_0_0_Types };
static const Il2CppType* GenInst_RectOptions_t3393635162_0_0_0_Types[] = { &RectOptions_t3393635162_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOptions_t3393635162_0_0_0 = { 1, GenInst_RectOptions_t3393635162_0_0_0_Types };
static const Il2CppType* GenInst_StringOptions_t2885323933_0_0_0_Types[] = { &StringOptions_t2885323933_0_0_0 };
extern const Il2CppGenericInst GenInst_StringOptions_t2885323933_0_0_0 = { 1, GenInst_StringOptions_t2885323933_0_0_0_Types };
static const Il2CppType* GenInst_UintOptions_t2267095136_0_0_0_Types[] = { &UintOptions_t2267095136_0_0_0 };
extern const Il2CppGenericInst GenInst_UintOptions_t2267095136_0_0_0 = { 1, GenInst_UintOptions_t2267095136_0_0_0_Types };
static const Il2CppType* GenInst_Vector3ArrayOptions_t2672570171_0_0_0_Types[] = { &Vector3ArrayOptions_t2672570171_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3ArrayOptions_t2672570171_0_0_0 = { 1, GenInst_Vector3ArrayOptions_t2672570171_0_0_0_Types };
static const Il2CppType* GenInst_VectorOptions_t293385261_0_0_0_Types[] = { &VectorOptions_t293385261_0_0_0 };
extern const Il2CppGenericInst GenInst_VectorOptions_t293385261_0_0_0 = { 1, GenInst_VectorOptions_t293385261_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1712] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t2430923913_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_CultureInfo_t3500843524_0_0_0,
	&GenInst_IFormatProvider_t2849799027_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_MonoResource_t3127387157_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_IList_1_t1844743827_0_0_0,
	&GenInst_ICollection_1_t2255878531_0_0_0,
	&GenInst_IEnumerable_1_t1595930271_0_0_0,
	&GenInst_IList_1_t3952977575_0_0_0,
	&GenInst_ICollection_1_t69144983_0_0_0,
	&GenInst_IEnumerable_1_t3704164019_0_0_0,
	&GenInst_IList_1_t643717440_0_0_0,
	&GenInst_ICollection_1_t1054852144_0_0_0,
	&GenInst_IEnumerable_1_t394903884_0_0_0,
	&GenInst_IList_1_t289070565_0_0_0,
	&GenInst_ICollection_1_t700205269_0_0_0,
	&GenInst_IEnumerable_1_t40257009_0_0_0,
	&GenInst_IList_1_t1043143288_0_0_0,
	&GenInst_ICollection_1_t1454277992_0_0_0,
	&GenInst_IEnumerable_1_t794329732_0_0_0,
	&GenInst_IList_1_t873662762_0_0_0,
	&GenInst_ICollection_1_t1284797466_0_0_0,
	&GenInst_IEnumerable_1_t624849206_0_0_0,
	&GenInst_IList_1_t3230389896_0_0_0,
	&GenInst_ICollection_1_t3641524600_0_0_0,
	&GenInst_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_LocalBuilder_t2116499186_0_0_0,
	&GenInst__LocalBuilder_t61912499_0_0_0,
	&GenInst_LocalVariableInfo_t1749284021_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_IBuiltInEvidence_t1114073477_0_0_0,
	&GenInst_IIdentityPermissionFactory_t2988326850_0_0_0,
	&GenInst_EncodingInfo_t2546178797_0_0_0,
	&GenInst_WaitHandle_t677569169_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_MarshalByRefObject_t1285298191_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0,
	&GenInst__Assembly_t2937922309_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_IList_1_t4224045037_0_0_0,
	&GenInst_ICollection_1_t340212445_0_0_0,
	&GenInst_IEnumerable_1_t3975231481_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_PropertyDescriptor_t4250402154_0_0_0,
	&GenInst_MemberDescriptor_t3749827553_0_0_0,
	&GenInst_IComponent_t1000253244_0_0_0,
	&GenInst_Enum_t2459695545_0_0_0,
	&GenInst_IFormattable_t1523031934_0_0_0,
	&GenInst_ValueType_t3507792607_0_0_0,
	&GenInst_EventDescriptor_t962731901_0_0_0,
	&GenInst_ListSortDescription_t3194554012_0_0_0,
	&GenInst_AttributeU5BU5D_t4255796347_0_0_0,
	&GenInst_IList_1_t1083584199_0_0_0,
	&GenInst_ICollection_1_t1494718903_0_0_0,
	&GenInst_IEnumerable_1_t834770643_0_0_0,
	&GenInst_IList_1_t2098604900_0_0_0,
	&GenInst_ICollection_1_t2509739604_0_0_0,
	&GenInst_IEnumerable_1_t1849791344_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_TypeDescriptionProvider_t2438624375_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LinkedList_1_t2743332604_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t2438035723_0_0_0,
	&GenInst_KeyValuePair_2_t2438035723_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_KeyValuePair_2_t3261256129_0_0_0,
	&GenInst_KeyValuePair_2_t3261256129_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_Material_t193706927_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Touch_t407273883_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_CombineInstance_t64595210_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_AnimatorClipInfo_t3905751349_0_0_0,
	&GenInst_AnimatorControllerParameter_t1381019216_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUIContent_t4210063000_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_Texture_t2243626319_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_Event_t3028476042_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MessageEventArgs_t301283622_0_0_0,
	&GenInst_List_1_t61287617_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dispatcher_t2240407071_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0,
	&GenInst_KeyValuePair_2_t3846770086_0_0_0,
	&GenInst_TweenCallback_t3697142134_0_0_0,
	&GenInst_LogBehaviour_t3505725029_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_FloatOptions_t1421548266_0_0_0,
	&GenInst_Quaternion_t4030073918_0_0_0_Vector3_t2243707580_0_0_0_QuaternionOptions_t466049668_0_0_0,
	&GenInst_Quaternion_t4030073918_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3U5BU5D_t1172311765_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Vector3ArrayOptions_t2672570171_0_0_0,
	&GenInst_Tween_t278478013_0_0_0,
	&GenInst_ABSSequentiable_t2284140720_0_0_0,
	&GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0_UInt32_t2149682021_0_0_0_UintOptions_t2267095136_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_StringOptions_t2885323933_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_StringOptions_t2885323933_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_VectorOptions_t293385261_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_VectorOptions_t293385261_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_VectorOptions_t293385261_0_0_0,
	&GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_ColorOptions_t2213017305_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0_RectOptions_t3393635162_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0_RectOffset_t3387826427_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_Color2_t232726623_0_0_0_Color2_t232726623_0_0_0_ColorOptions_t2213017305_0_0_0,
	&GenInst_Color2_t232726623_0_0_0,
	&GenInst_Ease_t2502520296_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_Quaternion_t4030073918_0_0_0_Quaternion_t4030073918_0_0_0_NoOptions_t2508431845_0_0_0,
	&GenInst_LoopType_t2249218064_0_0_0,
	&GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0,
	&GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ITweenPlugin_t2991430675_0_0_0,
	&GenInst_Type_t_0_0_0_ITweenPlugin_t2991430675_0_0_0_KeyValuePair_2_t2686133794_0_0_0,
	&GenInst_KeyValuePair_2_t2686133794_0_0_0,
	&GenInst_ControlPoint_t168081159_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FirebaseApp_t210707726_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t4177799506_0_0_0,
	&GenInst_KeyValuePair_2_t4177799506_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_KeyValuePair_2_t2705045562_0_0_0,
	&GenInst_KeyValuePair_2_t2705045562_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t271247988_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0,
	&GenInst_Tuple_2_t460172552_0_0_0,
	&GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ManualResetEvent_t926074657_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_KeyValuePair_2_t1986212810_0_0_0,
	&GenInst_KeyValuePair_2_t1986212810_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_FirebaseHttpRequest_t1911360314_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HashSet_1_t2984649583_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_AppViewHitBuilder_t93969158_0_0_0,
	&GenInst_EventHitBuilder_t3458384960_0_0_0,
	&GenInst_TransactionHitBuilder_t3262282092_0_0_0,
	&GenInst_ItemHitBuilder_t596710299_0_0_0,
	&GenInst_ExceptionHitBuilder_t2776428281_0_0_0,
	&GenInst_SocialHitBuilder_t2172450373_0_0_0,
	&GenInst_TimingHitBuilder_t1787450410_0_0_0,
	&GenInst_AxisTouchButton_t3842535002_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_VirtualAxis_t2691167515_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t2691167515_0_0_0_KeyValuePair_2_t2363291999_0_0_0,
	&GenInst_KeyValuePair_2_t2363291999_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_VirtualButton_t2157404822_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t2157404822_0_0_0_KeyValuePair_2_t1829529306_0_0_0,
	&GenInst_KeyValuePair_2_t1829529306_0_0_0,
	&GenInst_MeshInstance_t2175729061_0_0_0,
	&GenInst_Terrain_t59182933_0_0_0,
	&GenInst_HeaderValue_t822462144_0_0_0,
	&GenInst_String_t_0_0_0_Digest_t59399582_0_0_0,
	&GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Digest_t59399582_0_0_0,
	&GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_KeyValuePair_2_t4026491362_0_0_0,
	&GenInst_KeyValuePair_2_t4026491362_0_0_0,
	&GenInst_HTTPCacheFileInfo_t2858191078_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t1398341365_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465849_0_0_0,
	&GenInst_KeyValuePair_2_t1070465849_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Uri_t19570940_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3930922740_0_0_0,
	&GenInst_KeyValuePair_2_t3930922740_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t4099664523_0_0_0,
	&GenInst_KeyValuePair_2_t4099664523_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2387876582_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_KeyValuePair_2_t2556618365_0_0_0,
	&GenInst_KeyValuePair_2_t2556618365_0_0_0,
	&GenInst_Stream_t3255436806_0_0_0,
	&GenInst_Cookie_t4162804382_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Config_t3381668151_0_0_0,
	&GenInst_Int32U5BU5D_t3030399641_0_0_0,
	&GenInst_IList_1_t2612818049_0_0_0,
	&GenInst_ICollection_1_t3023952753_0_0_0,
	&GenInst_IEnumerable_1_t2364004493_0_0_0,
	&GenInst_IHeartbeat_t3217346319_0_0_0,
	&GenInst_HTTPFieldData_t605100868_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0,
	&GenInst_ConnectionBase_t2782190729_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2151311861_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_KeyValuePair_2_t1823436345_0_0_0,
	&GenInst_KeyValuePair_2_t1823436345_0_0_0,
	&GenInst_HTTPRequest_t138485887_0_0_0,
	&GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_Asn1Encodable_t3447851422_0_0_0,
	&GenInst_IAsn1Convertible_t983765413_0_0_0,
	&GenInst_DerEnumerated_t514019671_0_0_0,
	&GenInst_Asn1Object_t564283626_0_0_0,
	&GenInst_DerObjectIdentifier_t3495876513_0_0_0,
	&GenInst_BigInteger_t4268922522_0_0_0,
	&GenInst_DistributionPoint_t769724552_0_0_0,
	&GenInst_CrlEntry_t4200172927_0_0_0,
	&GenInst_GeneralName_t294965175_0_0_0,
	&GenInst_IAsn1Choice_t4205079803_0_0_0,
	&GenInst_UInt32U5BU5D_t59386216_0_0_0,
	&GenInst_IList_1_t2690622622_0_0_0,
	&GenInst_ICollection_1_t3101757326_0_0_0,
	&GenInst_IEnumerable_1_t2441809066_0_0_0,
	&GenInst_Int64U5BU5D_t717125112_0_0_0,
	&GenInst_IList_1_t1450018638_0_0_0,
	&GenInst_ICollection_1_t1861153342_0_0_0,
	&GenInst_IEnumerable_1_t1201205082_0_0_0,
	&GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0,
	&GenInst_IList_1_t600326817_0_0_0,
	&GenInst_ICollection_1_t1011461521_0_0_0,
	&GenInst_IEnumerable_1_t351513261_0_0_0,
	&GenInst_IList_1_t75442244_0_0_0,
	&GenInst_ICollection_1_t486576948_0_0_0,
	&GenInst_IEnumerable_1_t4121595984_0_0_0,
	&GenInst_IList_1_t3452350100_0_0_0,
	&GenInst_ICollection_1_t3863484804_0_0_0,
	&GenInst_IEnumerable_1_t3203536544_0_0_0,
	&GenInst_IList_1_t99252587_0_0_0,
	&GenInst_ICollection_1_t510387291_0_0_0,
	&GenInst_IEnumerable_1_t4145406327_0_0_0,
	&GenInst_IList_1_t632609824_0_0_0,
	&GenInst_ICollection_1_t1043744528_0_0_0,
	&GenInst_IEnumerable_1_t383796268_0_0_0,
	&GenInst_IList_1_t3862439092_0_0_0,
	&GenInst_ICollection_1_t4273573796_0_0_0,
	&GenInst_IEnumerable_1_t3613625536_0_0_0,
	&GenInst_IList_1_t3231563223_0_0_0,
	&GenInst_ICollection_1_t3642697927_0_0_0,
	&GenInst_IEnumerable_1_t2982749667_0_0_0,
	&GenInst_IList_1_t3642697928_0_0_0,
	&GenInst_ICollection_1_t4053832631_0_0_0,
	&GenInst_IEnumerable_1_t3393884371_0_0_0,
	&GenInst_IList_1_t2982749668_0_0_0,
	&GenInst_ICollection_1_t3393884372_0_0_0,
	&GenInst_IEnumerable_1_t2733936111_0_0_0,
	&GenInst_ServerName_t2635557658_0_0_0,
	&GenInst_X509CertificateStructure_t3705285294_0_0_0,
	&GenInst_ECPoint_t626351532_0_0_0,
	&GenInst_ECFieldElement_t1092946118_0_0_0,
	&GenInst_WNafPreCompInfo_t485024160_0_0_0,
	&GenInst_PreCompInfo_t1123315090_0_0_0,
	&GenInst_LongArray_t194261203_0_0_0,
	&GenInst_ZTauElement_t2571810054_0_0_0,
	&GenInst_SByteU5BU5D_t3472287392_0_0_0,
	&GenInst_IList_1_t995358150_0_0_0,
	&GenInst_ICollection_1_t1406492854_0_0_0,
	&GenInst_IEnumerable_1_t746544594_0_0_0,
	&GenInst_AbstractF2mPoint_t883694769_0_0_0,
	&GenInst_ECPointBase_t3119694375_0_0_0,
	&GenInst_Config_t1249383685_0_0_0,
	&GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0,
	&GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_OnEventDelegate_t790674770_0_0_0,
	&GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_KeyValuePair_2_t462799254_0_0_0,
	&GenInst_KeyValuePair_2_t462799254_0_0_0,
	&GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0,
	&GenInst_EventSourceResponse_t2287402344_0_0_0,
	&GenInst_Message_t1650395211_0_0_0,
	&GenInst_Hub_t272719679_0_0_0,
	&GenInst_IHub_t3409721544_0_0_0,
	&GenInst_IServerMessage_t2384143743_0_0_0,
	&GenInst_NegotiationData_t3059020807_0_0_0,
	&GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0,
	&GenInst_KeyValuePair_2_t322707255_0_0_0,
	&GenInst_ClientMessage_t624279968_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0,
	&GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0,
	&GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0,
	&GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_KeyValuePair_2_t3155242238_0_0_0,
	&GenInst_KeyValuePair_2_t3155242238_0_0_0,
	&GenInst_SocketIOCallback_t88619200_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0,
	&GenInst_EventDescriptor_t4057040835_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t3426161967_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_KeyValuePair_2_t3098286451_0_0_0,
	&GenInst_KeyValuePair_2_t3098286451_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_SocketIOAckCallback_t53599143_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_KeyValuePair_2_t1113737296_0_0_0,
	&GenInst_KeyValuePair_2_t1113737296_0_0_0,
	&GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0,
	&GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Socket_t2716624701_0_0_0,
	&GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_KeyValuePair_2_t2388749185_0_0_0,
	&GenInst_KeyValuePair_2_t2388749185_0_0_0,
	&GenInst_Packet_t1309324146_0_0_0,
	&GenInst_IExtension_t2171905938_0_0_0,
	&GenInst_WebSocketFrame_t4163283394_0_0_0,
	&GenInst_WebSocketFrameReader_t549273869_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0,
	&GenInst_Texture2D_t3542995729_0_0_0,
	&GenInst_JsonData_t269267574_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0,
	&GenInst_KeyValuePair_2_t4236359354_0_0_0,
	&GenInst_IJsonWrapper_t3045908096_0_0_0,
	&GenInst_IOrderedDictionary_t300723224_0_0_0,
	&GenInst_IDictionary_t596158605_0_0_0,
	&GenInst_IEquatable_1_t2473249743_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_KeyValuePair_2_t4236359354_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_KeyValuePair_2_t1043231486_0_0_0,
	&GenInst_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0,
	&GenInst_ExporterFunc_t408878057_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0,
	&GenInst_ImporterFunc_t2977850894_0_0_0,
	&GenInst_IDictionary_2_t2914292212_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_KeyValuePair_2_t3653207108_0_0_0,
	&GenInst_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0,
	&GenInst_IDictionary_2_t3266987655_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_KeyValuePair_2_t1345327748_0_0_0,
	&GenInst_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0,
	&GenInst_IList_1_t4234766737_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t1703537581_0_0_0,
	&GenInst_KeyValuePair_2_t1703537581_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0,
	&GenInst_KeyValuePair_2_t2961690774_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t3690625517_0_0_0,
	&GenInst_KeyValuePair_2_t3690625517_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_KeyValuePair_2_t3929469856_0_0_0,
	&GenInst_KeyValuePair_2_t3929469856_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_KeyValuePair_2_t103581176_0_0_0,
	&GenInst_KeyValuePair_2_t103581176_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_KeyValuePair_2_t2608995331_0_0_0,
	&GenInst_KeyValuePair_2_t2608995331_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t3365950620_0_0_0,
	&GenInst_KeyValuePair_2_t3365950620_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0,
	&GenInst_KeyValuePair_2_t3025249456_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_KeyValuePair_2_t2672554013_0_0_0,
	&GenInst_KeyValuePair_2_t2672554013_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0,
	&GenInst_IDictionary_2_t37308697_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_KeyValuePair_2_t1097446850_0_0_0,
	&GenInst_KeyValuePair_2_t1097446850_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_KeyValuePair_2_t4090537794_0_0_0,
	&GenInst_KeyValuePair_2_t4090537794_0_0_0,
	&GenInst_WriterContext_t4137194742_0_0_0,
	&GenInst_StateHandler_t387387051_0_0_0,
	&GenInst_MulticastDelegate_t3201952435_0_0_0,
	&GenInst_SampleDescriptor_t3285910703_0_0_0,
	&GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0,
	&GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0,
	&GenInst_MessageTypes_t223757197_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0,
	&GenInst_ColorPicker_t3035206225_0_0_0,
	&GenInst_ColorPicker_t3035206225_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Data_t407761726_0_0_0,
	&GenInst_Data_t2523487274_0_0_0,
	&GenInst_InventoryData_t3776523848_0_0_0,
	&GenInst_Data_t4011291362_0_0_0,
	&GenInst_Data_t3997843590_0_0_0,
	&GenInst_SlotData_t2756082054_0_0_0,
	&GenInst_SlotController_t2060013442_0_0_0,
	&GenInst_IEnhancedScrollerDelegate_t2066056166_0_0_0,
	&GenInst_Data_t1832928442_0_0_0,
	&GenInst_Data_t4174191502_0_0_0,
	&GenInst_Data_t2429895758_0_0_0,
	&GenInst_EnhancedScrollerCellView_t1104668249_0_0_0,
	&GenInst_CountryCodeData_t1765037603_0_0_0,
	&GenInst_DefaultCodeData_t2288261144_0_0_0,
	&GenInst_LanguageData_t336865618_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Double_t4078015681_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Il2CppObject_0_0_0_String_t_0_0_0,
	&GenInst_Vector3U5BU5D_t1172311765_0_0_0,
	&GenInst_ReversedRipple_t2344809957_0_0_0,
	&GenInst_Mesh_t1356156583_0_0_0,
	&GenInst_Regex_t1803876613_0_0_0,
	&GenInst_RayBehavior_t3027150638_0_0_0,
	&GenInst_CameraManager_t2379859346_0_0_0,
	&GenInst_Vertex_t3813209450_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Vertex_t3813209450_0_0_0,
	&GenInst_KeyValuePair_2_t3796471559_0_0_0,
	&GenInst_MeshData_t1378722312_0_0_0,
	&GenInst_List_1_t747843444_0_0_0,
	&GenInst_List_1_t746592055_0_0_0,
	&GenInst_MeshData_t1377470923_0_0_0,
	&GenInst_AIMode_t3831275989_0_0_0,
	&GenInst_UIManager_t2519183485_0_0_0,
	&GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Module_t3140434828_0_0_0,
	&GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t2178748957_0_0_0,
	&GenInst_KeyValuePair_2_t2178748957_0_0_0,
	&GenInst_SceneManager_t1824971699_0_0_0,
	&GenInst_PlayerMobilmoManager_t2341644285_0_0_0,
	&GenInst_ActionManager_t1367723175_0_0_0,
	&GenInst_WorldManager_t3923509627_0_0_0,
	&GenInst_InputManager_t1610719423_0_0_0,
	&GenInst_RayManager_t3612671535_0_0_0,
	&GenInst_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0,
	&GenInst_EvasionNav_t3997284964_0_0_0,
	&GenInst_PrizedManager_t3323701717_0_0_0,
	&GenInst_APIManager_t2595496257_0_0_0,
	&GenInst_WWWManager_t1701787038_0_0_0,
	&GenInst_ApiSearchFriendManager_t2339757037_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ApiSearchHistory_t3591623188_0_0_0,
	&GenInst_ApiShowSearchHistory_t2915241013_0_0_0,
	&GenInst_MyPageManager_t2103198340_0_0_0,
	&GenInst_ApiAuthenticationsManager_t1221451852_0_0_0,
	&GenInst_ApiChampionManager_t192998666_0_0_0,
	&GenInst_ColorManager_t1666568646_0_0_0,
	&GenInst_ChampionItemModel_t975870399_0_0_0,
	&GenInst_GetChampionMobilityChildPartsListModel_t1181557729_0_0_0,
	&GenInst_Joint_t826707408_0_0_0,
	&GenInst_ApiRaderManager_t2058631757_0_0_0,
	&GenInst_MobilmoManager_t1293766190_0_0_0,
	&GenInst_ModuleManager_t1065445307_0_0_0,
	&GenInst_ApiModuleManager_t3939700127_0_0_0,
	&GenInst_MiniGameManager_t849864952_0_0_0,
	&GenInst_ApiComponentManager_t1194997966_0_0_0,
	&GenInst_ApiContactManager_t3961479189_0_0_0,
	&GenInst_LocalizeManager_t1264687742_0_0_0,
	&GenInst_ApiDnaManager_t2705918952_0_0_0,
	&GenInst_GetDnaChildrenListModel_t2978945125_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0,
	&GenInst_MaskableGraphic_t540192618_0_0_0,
	&GenInst_IMaskable_t1431842707_0_0_0,
	&GenInst_IMaterialModifier_t3028564983_0_0_0,
	&GenInst_ApiEventManager_t2080410209_0_0_0,
	&GenInst_ApiUserManager_t2556825810_0_0_0,
	&GenInst_ApiMobilityManager_t1284346316_0_0_0,
	&GenInst_GetChildPartsList_t2416430853_0_0_0,
	&GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0,
	&GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_GetChildPartsList_t2416430853_0_0_0_KeyValuePair_2_t2088555337_0_0_0,
	&GenInst_KeyValuePair_2_t2088555337_0_0_0,
	&GenInst_AnalyticsManager_t1593654123_0_0_0,
	&GenInst_AudioManager_t4222704959_0_0_0,
	&GenInst_RecordingManager_t3075979958_0_0_0,
	&GenInst_CreateManager_t3918627545_0_0_0,
	&GenInst_TutorialManager_t2168024773_0_0_0,
	&GenInst_MotionManager_t3855993783_0_0_0,
	&GenInst_GetModuleChildPartsList_t3198568368_0_0_0,
	&GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0,
	&GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_GetModuleChildPartsList_t3198568368_0_0_0_KeyValuePair_2_t2870692852_0_0_0,
	&GenInst_KeyValuePair_2_t2870692852_0_0_0,
	&GenInst_ApiModuleMotionManager_t2960242393_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1428657631_0_0_0,
	&GenInst_KeyValuePair_2_t1428657631_0_0_0,
	&GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0,
	&GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AnimationCurve_t3306541151_0_0_0,
	&GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_KeyValuePair_2_t2978665635_0_0_0,
	&GenInst_KeyValuePair_2_t2978665635_0_0_0,
	&GenInst_AnimationClip_t3510324950_0_0_0,
	&GenInst_Motion_t2415020824_0_0_0,
	&GenInst_GetModuleMotionsData_t125704211_0_0_0,
	&GenInst_GetModulePoseData_t1121789947_0_0_0,
	&GenInst_GetModuleMotionItemData_t3026021381_0_0_0,
	&GenInst_GetMotionModel_t2157978365_0_0_0,
	&GenInst_GetMotionDataModel_t3220649645_0_0_0,
	&GenInst_GetPoseDataModel_t3123973518_0_0_0,
	&GenInst_ApiNearmobManager_t3144099613_0_0_0,
	&GenInst_ApiNoticeManager_t3224317815_0_0_0,
	&GenInst_NoticeListModel_t1317032645_0_0_0,
	&GenInst_GetNoticeDetail_t155866005_0_0_0,
	&GenInst_ApiPrizedManager_t4212047241_0_0_0,
	&GenInst_ApiReactionManager_t2857272874_0_0_0,
	&GenInst_GetReactionList_t3285480050_0_0_0,
	&GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0,
	&GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_GetReactionList_t3285480050_0_0_0_KeyValuePair_2_t2957604534_0_0_0,
	&GenInst_KeyValuePair_2_t2957604534_0_0_0,
	&GenInst_ApiTokenManager_t4213246208_0_0_0,
	&GenInst_GetUserUpdateJson_t2263613893_0_0_0,
	&GenInst_CountryData_t1135367575_0_0_0,
	&GenInst_LoadAssetBundle_t379001212_0_0_0,
	&GenInst_CountriesModel_t400828943_0_0_0,
	&GenInst_ApiMigrate_t2586426963_0_0_0,
	&GenInst_recordingMotionData_t1704213756_0_0_0,
	&GenInst_GetMotionsData_t2417923611_0_0_0,
	&GenInst_GetPoseData_t2785975099_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GetItemsData_t1181624410_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_KeyValuePair_2_t4200572981_0_0_0,
	&GenInst_KeyValuePair_2_t4200572981_0_0_0,
	&GenInst_GetMotionsDataInWorldActionSetting_t2072611844_0_0_0,
	&GenInst_GetPoseDataInWorldActionSetting_t1846296182_0_0_0,
	&GenInst_GetItemsDataInWorldActionSetting_t1654137477_0_0_0,
	&GenInst_ReactionList_t1054868761_0_0_0,
	&GenInst_MyPageMobilmoItem_t346175087_0_0_0,
	&GenInst_MypageMenuItem_t3200819439_0_0_0,
	&GenInst_MyPageFloor_t1585319381_0_0_0,
	&GenInst_GetRecordingNewMotions_t4070061385_0_0_0,
	&GenInst_GetItemsData_t174665839_0_0_0,
	&GenInst_GetMobilityChildPartsListModel_t2691836378_0_0_0,
	&GenInst_ReactionHistory_t2538361761_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_Dictionary_2_t309261261_0_0_0,
	&GenInst_GetChampionMobilityEncountListModel_t3804924867_0_0_0,
	&GenInst_GetChampionMobilityRegisterdMotionListModel_t1560983880_0_0_0,
	&GenInst_GetChampionMobilityRecordedMotionList_t1893561664_0_0_0,
	&GenInst_GetChampionMobilityReactionsModel_t283662167_0_0_0,
	&GenInst_GetChampionMobilityRegisterMotionListMotionDataModel_t2014295608_0_0_0,
	&GenInst_GetChampionMobilityRegisterMotionListPoseData_t3691775360_0_0_0,
	&GenInst_GetChampionMobilityRecordedMotionListMotions_t3064563391_0_0_0,
	&GenInst_GetComponentModel_t1600826744_0_0_0,
	&GenInst_GetMobilityItemsReactionItemsList_t2580483176_0_0_0,
	&GenInst_GetMobilityEncountList_t1045358299_0_0_0,
	&GenInst_GetMobilityRegisterdMotionList_t4139392582_0_0_0,
	&GenInst_GetMobilityRecordedMotionList_t2169087681_0_0_0,
	&GenInst_GetMobilityRegisterMotionListMotionData_t2404558376_0_0_0,
	&GenInst_GetMobilityRegisterMotionListPoseData_t2674609285_0_0_0,
	&GenInst_GetMobilityRecordedMotionListMotions_t4271290930_0_0_0,
	&GenInst_GetModuleChildPartsListModel_t3559781865_0_0_0,
	&GenInst_GetModuleRegisterMotionListMotionData_t2309758741_0_0_0,
	&GenInst_GetModuleRegisterMotionListPoseData_t154016370_0_0_0,
	&GenInst_MyMobilityItemsModel_t400881186_0_0_0,
	&GenInst_GetMyMobilityItemsReactionItemsList_t157728542_0_0_0,
	&GenInst_GetMyMobilityEncountList_t4033961011_0_0_0,
	&GenInst_GetMyMobilityRegisterdMotionList_t939659276_0_0_0,
	&GenInst_GetMyMobilityRecordedMotionList_t67098073_0_0_0,
	&GenInst_GetMyMobilityChildPartsListModel_t3080641068_0_0_0,
	&GenInst_GetMyMobilityRegisterMotionListMotionData_t2380437702_0_0_0,
	&GenInst_GetMyMobilityRegisterMotionListPoseData_t3613137541_0_0_0,
	&GenInst_GetMyMobilityRecordedMotionListMotions_t1565847752_0_0_0,
	&GenInst_NearmobItemsModel_t1641537963_0_0_0,
	&GenInst_GetNearmobRecordedMotionListMotions_t4266963713_0_0_0,
	&GenInst_GetNearmobItemsReactionItemsList_t586382143_0_0_0,
	&GenInst_GetNearmobEncountList_t2518104622_0_0_0,
	&GenInst_GetNearmobRegisterdMotionList_t3457217205_0_0_0,
	&GenInst_GetNearmobRecordedMotionList_t64870246_0_0_0,
	&GenInst_GetNearmobChildPartsListModel_t3662882787_0_0_0,
	&GenInst_GetNearmobRegisterMotionListMotionData_t1259926239_0_0_0,
	&GenInst_GetNearmobRegisterMotionListPoseData_t1612634570_0_0_0,
	&GenInst_GetPartsCategoryItems_t105269994_0_0_0,
	&GenInst_GetPartsList_t1367059906_0_0_0,
	&GenInst_GetRecordingModelMotions_t2111205999_0_0_0,
	&GenInst_DescriptionAttribute_t3207779672_0_0_0,
	&GenInst_DownLoadAssetbundle_t3666029200_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t98719111_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_eAUDIOBGM_t4173725093_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t98719111_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AudioClip_t1932558630_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3636795742_0_0_0,
	&GenInst_KeyValuePair_2_t3636795742_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1025746566_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_eAREATYPE_t1739175762_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1025746566_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t268855901_0_0_0,
	&GenInst_KeyValuePair_2_t268855901_0_0_0,
	&GenInst_CameraEffect_t3191348726_0_0_0,
	&GenInst_Slider_t297367283_0_0_0,
	&GenInst_LightManager_t3427519171_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0,
	&GenInst_Action_2_t1992487476_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3835431500_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_eEventID_t711132804_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3835431500_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t1361608608_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_List_1_t1361608608_0_0_0_KeyValuePair_2_t2507590813_0_0_0,
	&GenInst_KeyValuePair_2_t2507590813_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0,
	&GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0,
	&GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0,
	&GenInst_KeyValuePair_2_t697826584_0_0_0,
	&GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0,
	&GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t697826584_0_0_0,
	&GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_LocalizeKey_t3348421234_0_0_0_KeyValuePair_2_t3020545718_0_0_0,
	&GenInst_KeyValuePair_2_t3020545718_0_0_0,
	&GenInst_Pose_t686168059_0_0_0,
	&GenInst_Icon_t4013200341_0_0_0,
	&GenInst_Texture2DU5BU5D_t2724090252_0_0_0,
	&GenInst_Parts_t3804168686_0_0_0,
	&GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_AnimationCurve_t3306541151_0_0_0,
	&GenInst_RotationController_t3824016438_0_0_0,
	&GenInst_String_t_0_0_0_AnimationCurve_t3306541151_0_0_0_String_t_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2169266793_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_eAUDIOSE_t638655723_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2169266793_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t1412376128_0_0_0,
	&GenInst_KeyValuePair_2_t1412376128_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4217592965_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_eAUDIOVOICE_t2836738047_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4217592965_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_AudioClip_t1932558630_0_0_0_KeyValuePair_2_t3460702300_0_0_0,
	&GenInst_KeyValuePair_2_t3460702300_0_0_0,
	&GenInst_SliderMaster_t3139894563_0_0_0,
	&GenInst_PartsConfig_t872073448_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_Tween_t278478013_0_0_0_KeyValuePair_2_t1366168679_0_0_0,
	&GenInst_KeyValuePair_2_t1366168679_0_0_0,
	&GenInst_SlideInOut_t958296806_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0,
	&GenInst_GameManager_t2252321495_0_0_0,
	&GenInst_MeshRenderer_t1268241104_0_0_0,
	&GenInst_Light_t494725636_0_0_0,
	&GenInst_Collider_t3497673348_0_0_0,
	&GenInst_lightObj_t383130471_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t194954207_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_eLIGHTTYPE_t1469766701_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t194954207_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_Light_t494725636_0_0_0_KeyValuePair_2_t2295197844_0_0_0,
	&GenInst_KeyValuePair_2_t2295197844_0_0_0,
	&GenInst_LoadingManager_t2398813851_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3838112486_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_LocalizeKey_t3348421234_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3838112486_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LocalizeTextContents_t1443703366_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeTextContents_t1443703366_0_0_0_KeyValuePair_2_t2592366557_0_0_0,
	&GenInst_KeyValuePair_2_t2592366557_0_0_0,
	&GenInst_LocalizeItem_t998829008_0_0_0,
	&GenInst_UILocalizer_t2245030347_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_KeyValuePair_2_t37182331_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_SCORETYPE_t2692914394_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0_Single_t2076509932_0_0_0_KeyValuePair_2_t37182331_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_ISerializationCallbackReceiver_t1665913161_0_0_0,
	&GenInst_ICanvasRaycastFilter_t1367822892_0_0_0,
	&GenInst_TeleportManager_t4218394618_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Module_t3140434828_0_0_0,
	&GenInst_SceneFadeManager_t1728942829_0_0_0,
	&GenInst_WorldMapManager_t3256198739_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_PartsManager_t197384025_0_0_0,
	&GenInst_PremitivePartContent_t3696216029_0_0_0,
	&GenInst_UIPanel_t1795085332_0_0_0,
	&GenInst_UIObject_t4279159643_0_0_0,
	&GenInst_AreaParentObject_t3918549470_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t92830418_0_0_0,
	&GenInst_KeyValuePair_2_t92830418_0_0_0,
	&GenInst_LonLatitude_t2793245363_0_0_0,
	&GenInst_FixedJoint_t3848069458_0_0_0,
	&GenInst_Joint_t454317436_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2844223813_0_0_0,
	&GenInst_KeyValuePair_2_t2844223813_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_CoreContent_t4281623508_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Module_t3140434828_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_PlaceData_t1476161629_0_0_0,
	&GenInst_ParticleSystem_t3394631041_0_0_0,
	&GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_TimeAttackPoint_t1479403935_0_0_0,
	&GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t4188421297_0_0_0,
	&GenInst_KeyValuePair_2_t4188421297_0_0_0,
	&GenInst_TimeAttackPoint_t1479403935_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t2816671300_0_0_0,
	&GenInst_KeyValuePair_2_t2816671300_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_SliderControl_t94665898_0_0_0,
	&GenInst_ContactField_t897925540_0_0_0,
	&GenInst_NamePanelItem_t3250109188_0_0_0,
	&GenInst_CircleItem_t3720335645_0_0_0,
	&GenInst_InputPanelItem_t642391251_0_0_0,
	&GenInst_TitlePanelItem_t1000476553_0_0_0,
	&GenInst_AreaInfo_t2759954667_0_0_0,
	&GenInst_EventTrigger_t1967201810_0_0_0,
	&GenInst_AreaPoint_t3787172297_0_0_0,
	&GenInst_AreaButton_t1637812991_0_0_0,
	&GenInst_ConfigPanel_t537490728_0_0_0,
	&GenInst_RentalMobilmo_t1706493197_0_0_0,
	&GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0,
	&GenInst_KeyValuePair_2_t3171196553_0_0_0,
	&GenInst_Entry_t1526823907_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t3171196553_0_0_0,
	&GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_KeyValuePair_2_t1198948391_0_0_0,
	&GenInst_KeyValuePair_2_t1198948391_0_0_0,
	&GenInst_String_t_0_0_0_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0,
	&GenInst_ClickableEntry_t4031026959_0_0_0,
	&GenInst_List_1_t3050876758_0_0_0,
	&GenInst_PointToGps_t3385345589_0_0_0,
	&GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0,
	&GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Camera_t189460977_0_0_0_Camera_t189460977_0_0_0_KeyValuePair_2_t1298964269_0_0_0,
	&GenInst_KeyValuePair_2_t1298964269_0_0_0,
	&GenInst_Matrix4x4_t2933234003_0_0_0,
	&GenInst_RenderTexture_t2666733923_0_0_0,
	&GenInst_MaterialPropertyBlock_t3303648957_0_0_0,
	&GenInst_MaterialU5BU5D_t3123989686_0_0_0,
	&GenInst_IList_1_t734647528_0_0_0,
	&GenInst_ICollection_1_t1145782232_0_0_0,
	&GenInst_IEnumerable_1_t485833972_0_0_0,
	&GenInst_IList_1_t1562542718_0_0_0,
	&GenInst_ICollection_1_t1973677422_0_0_0,
	&GenInst_IEnumerable_1_t1313729162_0_0_0,
	&GenInst_Shader_t2430389951_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_Sort_m3546416104_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_compare_m940423571_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m665396702_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0,
	&GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0,
	&GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0,
	&GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0,
	&GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1290221672_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0,
	&GenInst_SortContext_1_t4088581714_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_LinkedList_1_t3556217344_gp_0_0_0_0,
	&GenInst_Enumerator_t4145643798_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_SortedList_2_t3146765111_gp_0_0_0_0,
	&GenInst_SortedList_2_t3146765111_gp_1_0_0_0,
	&GenInst_SortedList_2_t3146765111_gp_0_0_0_0_SortedList_2_t3146765111_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4007177354_0_0_0,
	&GenInst_EnumeratorMode_t215745978_gp_0_0_0_0_EnumeratorMode_t215745978_gp_1_0_0_0,
	&GenInst_Enumerator_t1609510401_gp_0_0_0_0_Enumerator_t1609510401_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0_KeyEnumerator_t3557860136_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t3557860136_gp_0_0_0_0,
	&GenInst_ValueEnumerator_t2021630038_gp_0_0_0_0_ValueEnumerator_t2021630038_gp_1_0_0_0,
	&GenInst_ValueEnumerator_t2021630038_gp_1_0_0_0,
	&GenInst_ListKeys_t708716807_gp_0_0_0_0_ListKeys_t708716807_gp_1_0_0_0,
	&GenInst_ListKeys_t708716807_gp_0_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t2910182623_gp_1_0_0_0,
	&GenInst_ListValues_t4054146799_gp_0_0_0_0_ListValues_t4054146799_gp_1_0_0_0,
	&GenInst_ListValues_t4054146799_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t2100474802_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3888417572_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3522999862_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3346222761_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t533259906_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0,
	&GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0,
	&GenInst_List_1_t2244783541_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0,
	&GenInst_List_1_t264254312_0_0_0,
	&GenInst_DOTween_To_m3442317795_gp_0_0_0_0_DOTween_To_m3442317795_gp_1_0_0_0_DOTween_To_m3442317795_gp_2_0_0_0,
	&GenInst_DOTween_To_m3442317795_gp_0_0_0_0,
	&GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0,
	&GenInst_DOTween_ApplyTo_m4058900771_gp_0_0_0_0_DOTween_ApplyTo_m4058900771_gp_1_0_0_0_DOTween_ApplyTo_m4058900771_gp_2_0_0_0,
	&GenInst_Tween_OnTweenCallback_m4208167374_gp_0_0_0_0,
	&GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0_Tweener_Setup_m1281370100_gp_1_0_0_0_Tweener_Setup_m1281370100_gp_2_0_0_0,
	&GenInst_Tweener_Setup_m1281370100_gp_0_0_0_0,
	&GenInst_Tweener_DoUpdateDelay_m1273907851_gp_0_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_1_0_0_0_Tweener_DoUpdateDelay_m1273907851_gp_2_0_0_0,
	&GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0_Tweener_DoStartup_m3383447813_gp_1_0_0_0_Tweener_DoStartup_m3383447813_gp_2_0_0_0,
	&GenInst_Tweener_DoStartup_m3383447813_gp_0_0_0_0,
	&GenInst_Tweener_DoChangeStartValue_m1941438293_gp_0_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_1_0_0_0_Tweener_DoChangeStartValue_m1941438293_gp_2_0_0_0,
	&GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_1_0_0_0_Tweener_DoChangeEndValue_m4040667481_gp_2_0_0_0,
	&GenInst_Tweener_DoChangeEndValue_m4040667481_gp_0_0_0_0,
	&GenInst_Tweener_DoChangeValues_m3618223102_gp_0_0_0_0_Tweener_DoChangeValues_m3618223102_gp_1_0_0_0_Tweener_DoChangeValues_m3618223102_gp_2_0_0_0,
	&GenInst_Tweener_DOStartupSpecials_m3457588195_gp_0_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_1_0_0_0_Tweener_DOStartupSpecials_m3457588195_gp_2_0_0_0,
	&GenInst_Tweener_DOStartupDurationBased_m2129745306_gp_0_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_1_0_0_0_Tweener_DOStartupDurationBased_m2129745306_gp_2_0_0_0,
	&GenInst_IPlugSetter_4_t1442750740_gp_0_0_0_0,
	&GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0_ABSTweenPlugin_3_t670396699_gp_1_0_0_0_ABSTweenPlugin_3_t670396699_gp_2_0_0_0,
	&GenInst_ABSTweenPlugin_3_t670396699_gp_0_0_0_0,
	&GenInst_PluginsManager_GetDefaultPlugin_m1820260679_gp_0_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_1_0_0_0_PluginsManager_GetDefaultPlugin_m1820260679_gp_2_0_0_0,
	&GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_1_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_2_0_0_0_PluginsManager_GetCustomPlugin_m2949628087_gp_3_0_0_0,
	&GenInst_PluginsManager_GetCustomPlugin_m2949628087_gp_0_0_0_0,
	&GenInst_Extensions_NoFrom_m2168138502_gp_0_0_0_0_Extensions_NoFrom_m2168138502_gp_1_0_0_0_Extensions_NoFrom_m2168138502_gp_2_0_0_0,
	&GenInst_Extensions_Blendable_m1526939834_gp_0_0_0_0_Extensions_Blendable_m1526939834_gp_1_0_0_0_Extensions_Blendable_m1526939834_gp_2_0_0_0,
	&GenInst_TweenManager_GetTweener_m346233533_gp_0_0_0_0_TweenManager_GetTweener_m346233533_gp_1_0_0_0_TweenManager_GetTweener_m346233533_gp_2_0_0_0,
	&GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0,
	&GenInst_TweenerCore_3_t3922192912_gp_0_0_0_0_TweenerCore_3_t3922192912_gp_1_0_0_0_TweenerCore_3_t3922192912_gp_2_0_0_0,
	&GenInst_TweenerCore_3_t3922192912_gp_2_0_0_0,
	&GenInst_BlockingCollection_1_t2635972927_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_GAIHandler__buildCustomDimensionsDictionary_m2637270120_gp_0_0_0_0,
	&GenInst_GAIHandler__buildCustomMetricsDictionary_m3851546778_gp_0_0_0_0,
	&GenInst_GAIHandler__buildCampaignParametersDictionary_m3600528208_gp_0_0_0_0,
	&GenInst_HitBuilder_1_t4172956431_gp_0_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t512346522_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0,
	&GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0,
	&GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0,
	&GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0,
	&GenInst_SmallList_1_t834712290_gp_0_0_0_0,
	&GenInst_PlayerPrefsUtility_SaveList_m1945505813_gp_0_0_0_0,
	&GenInst_List_1_t2674394750_0_0_0,
	&GenInst_PlayerPrefsUtility_SaveDict_m2305377966_gp_0_0_0_0_PlayerPrefsUtility_SaveDict_m2305377966_gp_1_0_0_0,
	&GenInst_Dictionary_2_t3552680572_0_0_0,
	&GenInst_PlayerPrefsUtility_LoadList_m1363645293_gp_0_0_0_0,
	&GenInst_List_1_t2249969993_0_0_0,
	&GenInst_PlayerPrefsUtility_LoadDict_m2416465162_gp_0_0_0_0_PlayerPrefsUtility_LoadDict_m2416465162_gp_1_0_0_0,
	&GenInst_Dictionary_2_t3754310832_0_0_0,
	&GenInst_Serialization_1_t112460908_gp_0_0_0_0,
	&GenInst_Serialization_1_t3382072283_gp_0_0_0_0,
	&GenInst_Serialization_1_t3962975162_gp_0_0_0_0,
	&GenInst_Serialization_1_t335558755_gp_0_0_0_0,
	&GenInst_SerializationInWorldActionSetting_1_t2067658136_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_SafeGetComponent_m3427069941_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_SafeGetComponent_m3013269245_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m1744908908_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_GetComponentsInChildrenWithoutSelf_m607618288_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_GameObjectExtensions_RemoveComponent_m2646591243_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_RemoveComponent_m4030745331_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_RemoveComponentImmediate_m2434492150_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_RemoveComponentImmediate_m1785965088_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_RemoveComponents_m3543111202_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_RemoveComponents_m324396248_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_HasComponent_m1163434775_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_HasComponent_m286777439_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_FindComponent_m4187209893_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_FindComponent_m3063844745_gp_0_0_0_0,
	&GenInst_SingletonMonoBehaviour_1_t2064948090_gp_0_0_0_0,
	&GenInst_EnumString_1_t696969061_gp_0_0_0_0,
	&GenInst_EnumTableCache_1_t3449384724_gp_0_0_0_0,
	&GenInst_String_t_0_0_0_EnumTableCache_1_t3449384724_gp_0_0_0_0,
	&GenInst_ObjectPool_1_t129996502_gp_0_0_0_0,
	&GenInst_ListPool_1_t2291257409_gp_0_0_0_0,
	&GenInst_List_1_t2308010990_0_0_0,
	&GenInst_ObjectPool_1_t938908698_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_PlayerConnection_t3517219175_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_DOTweenComponent_t696744215_0_0_0,
	&GenInst_TweenerCore_3_t3793077019_0_0_0,
	&GenInst_TweenerCore_3_t2279406887_0_0_0,
	&GenInst_Sequence_t110643099_0_0_0,
	&GenInst_PathPlugin_t4171842066_0_0_0_Vector3_t2243707580_0_0_0_Path_t2828565993_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_TweenerCore_3_t2998039394_0_0_0,
	&GenInst_TweenerCore_3_t3065187631_0_0_0,
	&GenInst_TweenerCore_3_t3925803634_0_0_0,
	&GenInst_Tweener_t760404022_0_0_0,
	&GenInst_TweenerCore_3_t3250868854_0_0_0,
	&GenInst_TweenerCore_3_t3261425374_0_0_0,
	&GenInst_TweenerCore_3_t1672798003_0_0_0,
	&GenInst_TweenerCore_3_t102288586_0_0_0,
	&GenInst_TweenerCore_3_t1108663466_0_0_0,
	&GenInst_TweenerCore_3_t3035488489_0_0_0,
	&GenInst_FirebaseHandler_t2907300047_0_0_0,
	&GenInst_SynchronizationContextBehavoir_t692674473_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_Scrollbar_t3248359358_0_0_0,
	&GenInst_InputField_t1631627530_0_0_0,
	&GenInst_ScrollRect_t1199013257_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_StandaloneInputModule_t70867863_0_0_0,
	&GenInst_ActionIconList_t2910912037_0_0_0,
	&GenInst_ActionButtons_t2868854693_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_CreateGrid_t2336839420_0_0_0,
	&GenInst_DoubleTapPartsScaling_t180404427_0_0_0,
	&GenInst_AIMove_t3831276007_0_0_0,
	&GenInst_NavigateCaption_t2735808323_0_0_0,
	&GenInst_FlagStop_t3791804538_0_0_0,
	&GenInst_Mobilmo_t370754809_0_0_0,
	&GenInst_MiniGameChampionsData_t2446501291_0_0_0,
	&GenInst_MobilityItemGo_t3232108924_0_0_0,
	&GenInst_CoreSelect_t1755339513_0_0_0,
	&GenInst_Animation_t2068071072_0_0_0,
	&GenInst_UserSetting_t3323821485_0_0_0,
	&GenInst_FruitsSlider_t2064329978_0_0_0,
	&GenInst_HTTPUpdateDelegator_t1331403296_0_0_0,
	&GenInst_Blur_t3313275655_0_0_0,
	&GenInst_MeshCollider_t2718867283_0_0_0,
	&GenInst_WaterRipples_t656902750_0_0_0,
	&GenInst_FSEffect_t3187928436_0_0_0,
	&GenInst_CameraFollow_t1493855402_0_0_0,
	&GenInst_CameraZoom_t1174393664_0_0_0,
	&GenInst_CameraRotater_t122375930_0_0_0,
	&GenInst_SubCameraHandler_t2753085375_0_0_0,
	&GenInst_CameraSettings_t3536359094_0_0_0,
	&GenInst_CameraAreaMonitor_t370298340_0_0_0,
	&GenInst_BlurFilter_t2530667_0_0_0,
	&GenInst_HingeJoint_t2745110831_0_0_0,
	&GenInst_CharacterController_t4094781467_0_0_0,
	&GenInst_CandyBall_t3642802284_0_0_0,
	&GenInst_CarryingBall_t127229952_0_0_0,
	&GenInst_MobPhysics_t1476770297_0_0_0,
	&GenInst_DebugNormalsInEditmode_t4099272891_0_0_0,
	&GenInst_MeshFilter_t3026937449_0_0_0,
	&GenInst_MobilmoPanel_t222294755_0_0_0,
	&GenInst_ModulePanel_t1015180434_0_0_0,
	&GenInst_MobilimoPartsConect_t3466715044_0_0_0,
	&GenInst_ModulePartsConect_t3833785310_0_0_0,
	&GenInst_ConfigurableJoint_t454307495_0_0_0,
	&GenInst_Buoyancy_t2565452554_0_0_0,
	&GenInst_CombineChildrenAFS_t3843368254_0_0_0,
	&GenInst_Projector_t1121484678_0_0_0,
	&GenInst_ProjectorMatrix_t3999868417_0_0_0,
	&GenInst_PreloadingManager_t3491463554_0_0_0,
	&GenInst_VerticalLayoutGroup_t2468316403_0_0_0,
	&GenInst_HorizontalLayoutGroup_t2875670365_0_0_0,
	&GenInst_HorizontalOrVerticalLayoutGroup_t1968298610_0_0_0,
	&GenInst_LensFlare_t529161798_0_0_0,
	&GenInst_FSPlanet_t2551900013_0_0_0,
	&GenInst_SphereCollider_t1662511355_0_0_0,
	&GenInst_ActionLever_t131660742_0_0_0,
	&GenInst_WaterRipplesHouse_t1929798042_0_0_0,
	&GenInst_MiniGameChara_t2588887264_0_0_0,
	&GenInst_TimeAttack_t1975281095_0_0_0,
	&GenInst_PlanetGravity_t2098207264_0_0_0,
	&GenInst_ActionObj_t2993311507_0_0_0,
	&GenInst_RaderCirclesMover_t2837149066_0_0_0,
	&GenInst_PlayerInfoLike_t852303790_0_0_0,
	&GenInst_EditMobilmo_t2535788613_0_0_0,
	&GenInst_EllipsoidParticleEmitter_t3972767653_0_0_0,
	&GenInst_MobilmoDnaController_t1006271200_0_0_0,
	&GenInst_LineRenderer_t849157671_0_0_0,
	&GenInst_PlayerInfoPanel_t97551129_0_0_0,
	&GenInst_CrossSection_t582606797_0_0_0,
	&GenInst_SuccessfulCircles_t3791497435_0_0_0,
	&GenInst_SystemInputCircles_t2127672316_0_0_0,
	&GenInst_MiniPlanetRotator_t4060345590_0_0_0,
	&GenInst_TitleAnimator_t1342483085_0_0_0,
	&GenInst_touchBendingPlayerListener_t2574995411_0_0_0,
	&GenInst_Joystick_t2144252492_0_0_0,
	&GenInst_HelloWorldPanel_t280472022_0_0_0,
	&GenInst_TalkTextFader_t1511400589_0_0_0,
	&GenInst_TalkPanel_t2596865392_0_0_0,
	&GenInst_RentalMobilmoContent_t501695126_0_0_0,
	&GenInst_RegexHypertext_t2320066936_0_0_0,
	&GenInst_LetterSpacing_t3080148137_0_0_0,
	&GenInst_RecordMobility_t2685217900_0_0_0,
	&GenInst_Lever_t866853776_0_0_0,
	&GenInst_SunShafts_t482045181_0_0_0,
	&GenInst_Underwater_t1731910439_0_0_0,
	&GenInst_Skybox_t2033495038_0_0_0,
	&GenInst_FlareLayer_t1985082419_0_0_0,
	&GenInst_BackgroundFader_t837055620_0_0_0,
	&GenInst_NamePanel_t546707703_0_0_0,
	&GenInst_InputMarkAnimator_t3461548916_0_0_0,
	&GenInst_RegisterButtonCrossFader_t205138403_0_0_0,
	&GenInst_MeshCreatorSTL_t2712395408_0_0_0,
	&GenInst_AreaGround_t2255653338_0_0_0,
	&GenInst_FeedbackEffect_t3658613102_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_PathOptions_t2659884781_0_0_0,
	&GenInst_ClickableEntry_t4031026959_0_0_0_ClickableEntry_t4031026959_0_0_0,
	&GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Double_t4078015681_0_0_0_Double_t4078015681_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0_Rect_t3681755626_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_eAREATYPE_t1739175762_0_0_0_eAREATYPE_t1739175762_0_0_0,
	&GenInst_KeyValuePair_2_t1025746566_0_0_0_KeyValuePair_2_t1025746566_0_0_0,
	&GenInst_KeyValuePair_2_t1025746566_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eAUDIOBGM_t4173725093_0_0_0_eAUDIOBGM_t4173725093_0_0_0,
	&GenInst_KeyValuePair_2_t98719111_0_0_0_KeyValuePair_2_t98719111_0_0_0,
	&GenInst_KeyValuePair_2_t98719111_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eAUDIOSE_t638655723_0_0_0_eAUDIOSE_t638655723_0_0_0,
	&GenInst_KeyValuePair_2_t2169266793_0_0_0_KeyValuePair_2_t2169266793_0_0_0,
	&GenInst_KeyValuePair_2_t2169266793_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eAUDIOVOICE_t2836738047_0_0_0_eAUDIOVOICE_t2836738047_0_0_0,
	&GenInst_KeyValuePair_2_t4217592965_0_0_0_KeyValuePair_2_t4217592965_0_0_0,
	&GenInst_KeyValuePair_2_t4217592965_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eEventID_t711132804_0_0_0_eEventID_t711132804_0_0_0,
	&GenInst_KeyValuePair_2_t3835431500_0_0_0_KeyValuePair_2_t3835431500_0_0_0,
	&GenInst_KeyValuePair_2_t3835431500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_eLIGHTTYPE_t1469766701_0_0_0_eLIGHTTYPE_t1469766701_0_0_0,
	&GenInst_KeyValuePair_2_t194954207_0_0_0_KeyValuePair_2_t194954207_0_0_0,
	&GenInst_KeyValuePair_2_t194954207_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LocalizeKey_t3348421234_0_0_0_LocalizeKey_t3348421234_0_0_0,
	&GenInst_KeyValuePair_2_t3838112486_0_0_0_KeyValuePair_2_t3838112486_0_0_0,
	&GenInst_KeyValuePair_2_t3838112486_0_0_0_Il2CppObject_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0_SCORETYPE_t2692914394_0_0_0,
	&GenInst_SCORETYPE_t2692914394_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t37182331_0_0_0_KeyValuePair_2_t37182331_0_0_0,
	&GenInst_KeyValuePair_2_t37182331_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0,
	&GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0,
	&GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0,
	&GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0,
	&GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t697826584_0_0_0_KeyValuePair_2_t697826584_0_0_0,
	&GenInst_KeyValuePair_2_t697826584_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Entry_t1526823907_0_0_0_Entry_t1526823907_0_0_0,
	&GenInst_Entry_t1526823907_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3171196553_0_0_0_KeyValuePair_2_t3171196553_0_0_0,
	&GenInst_KeyValuePair_2_t3171196553_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0,
	&GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0,
	&GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0,
	&GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0,
	&GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ColorOptions_t2213017305_0_0_0,
	&GenInst_FloatOptions_t1421548266_0_0_0,
	&GenInst_NoOptions_t2508431845_0_0_0,
	&GenInst_PathOptions_t2659884781_0_0_0,
	&GenInst_QuaternionOptions_t466049668_0_0_0,
	&GenInst_RectOptions_t3393635162_0_0_0,
	&GenInst_StringOptions_t2885323933_0_0_0,
	&GenInst_UintOptions_t2267095136_0_0_0,
	&GenInst_Vector3ArrayOptions_t2672570171_0_0_0,
	&GenInst_VectorOptions_t293385261_0_0_0,
};
