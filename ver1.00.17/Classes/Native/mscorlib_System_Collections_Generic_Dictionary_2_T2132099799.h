﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22592366557.h"
#include "AssemblyU2DCSharp_Localization_LocalizeKey3348421234.h"

// LocalizeTextContents
struct LocalizeTextContents_t1443703366;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<Localization.LocalizeKey,LocalizeTextContents,System.Collections.Generic.KeyValuePair`2<Localization.LocalizeKey,LocalizeTextContents>>
struct  Transform_1_t2132099799  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
