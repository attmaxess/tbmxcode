# Acknowledgements
This application makes use of the following third party libraries:

## Firebase

Copyright 2017 Google

## FirebaseAnalytics

Copyright 2017 Google

## FirebaseCore

Copyright 2017 Google

## FirebaseInstanceID

Copyright 2017 Google

## FirebaseMessaging

Copyright 2017 Google
Generated by CocoaPods - https://cocoapods.org
